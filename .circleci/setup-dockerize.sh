#!/bin/bash

set -ex

if [ -z ${DOCKERIZE_VERSION+z} ]; then
    echo "REQUIRED ENV VAR 'DOCKERIZE_VERSION' IS MISSING" >&2
    exit 1
fi

wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz
sudo tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz
rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz
