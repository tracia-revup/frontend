
cache-keys: &cache-keys
  keys:
    # This branch if available
    - v2-dep-{{ .Branch }}-
    # Default branch if not
    - v2-dep-master-
    # Any branch if there are none on the default branch - this
    # should be unnecessary if you have your default branch
    # configured correctly
    - v2-dep-

deploy-steps: &deploy-steps
  environment:
    PIP_USER: 1

  docker:
    - image: circleci/python:3.6.5
  working_directory: ~/revupsw/frontend
  steps:
    - checkout
    - restore_cache:
        <<: *cache-keys
    - run: echo 'export PATH=~/.local/bin:$PATH' >> $BASH_ENV
    - add_ssh_keys:  # add key from CircleCI account based on fingerprint
        fingerprints:
          - "1e:f1:6e:5c:ad:af:e7:17:c0:8a:b2:9d:b7:26:55:37"
    - run:
        name: Setup Heroku
        command: bash .circleci/setup-heroku.sh
    - run:
        name: Deploy
        command: ./scripts/heroku_deployv2.sh ${BUILD_APP} ${HEROKU_APP}


version: 2
jobs:
  build:
    working_directory: ~/revupsw/frontend

    environment:
      PIP_USER: 1

    docker:
      - image: circleci/python:3.6.5
    steps:
      - checkout
      - restore_cache:
          <<: *cache-keys

      - run: echo 'export PATH=~/.local/bin:$PATH' >> $BASH_ENV
      - run:
          name: install dockerize
          command: ./.circleci/setup-dockerize.sh
          environment:
            DOCKERIZE_VERSION: v0.3.0

      - run:
          name: Pre-dependency commands
          command: |-
            pip install codecov
            pip install codacy-coverage

      - run:
          name: Install Dependencies
          command: pip install -r reqs/test.txt

      # Save dependency cache
      - save_cache:
          key: v2-dep-{{ .Branch }}-{{ epoch }}
          paths:
            # This is a broad list of cache paths to include many possible development environments
            # You can probably delete some of these entries
            - vendor/bundle
            - ~/virtualenvs
            - ~/.m2
            - ~/.ivy2
            - ~/.bundle
            - ~/.go_workspace
            - ~/.gradle
            - ~/.cache/bower
            - ./node_modules
            - ".venv"
            - "~/.local"


  test-migrations:
    working_directory: ~/revupsw/frontend
    docker:
      - image: circleci/python:3.6.5
      - image: circleci/postgres:10-alpine-postgis-ram
        environment:
          POSTGRES_USER: ubuntu
          POSTGRES_DB: revup
      - image: circleci/mongo:3.6
      - image: circleci/redis:3

    steps:
      - checkout
      - restore_cache:
          <<: *cache-keys

      - run: echo 'export PATH=~/.local/bin:$PATH' >> $BASH_ENV
      - run:
          name: Install GDAL
          command: |-
            sudo apt update
            sudo apt install libgdal-dev
      - run:
          name: Wait for db
          command: dockerize -wait tcp://localhost:5432 -timeout 1m

      - run:
          name: Reset Database
          command: python manage.py reset_db --noinput
          environment:
            DJANGO_SETTINGS_MODULE: frontend.settings.test

      - run:
          name: Run Migrations
          command: python manage.py migrate --noinput
          environment:
            DJANGO_SETTINGS_MODULE: frontend.settings.test


  test:
    working_directory: ~/revupsw/frontend

    environment:
      CIRCLE_ARTIFACTS: /tmp/circleci-artifacts
      CIRCLE_TEST_REPORTS: /tmp/circleci-test-results
      PIP_USER: 1

    docker:
      - image: circleci/python:3.6.5
      - image: circleci/postgres:10-alpine-postgis-ram
        environment:
          POSTGRES_USER: ubuntu
          POSTGRES_DB: revup
      - image: circleci/mongo:3.6
      - image: circleci/redis:3

    steps:
      - checkout
      - run: mkdir -p $CIRCLE_ARTIFACTS $CIRCLE_TEST_REPORTS
      - restore_cache:
          <<: *cache-keys

      - run: echo 'export PATH=~/.local/bin:$PATH' >> $BASH_ENV
      - run:
          name: Install GDAL
          command: |-
            sudo apt update
            sudo apt install libgdal-dev
      - run:
          name: Wait for db
          command: dockerize -wait tcp://localhost:5432 -timeout 1m

      - run:
          name: Run Tests
          command: pytest -n 2 -c .circleci/pytest.ini --create-db --nomigrations frontend

      - run:
          name: Submit code coverage report
          command: |-
            codecov
            python-codacy-coverage -r $CIRCLE_TEST_REPORTS/coverage.xml || echo "Submitting coverage reports to codacy failed!"

      # Save test results
      - store_test_results:
          path: /tmp/circleci-test-results
      # Save artifacts
      - store_artifacts:
          path: /tmp/circleci-artifacts
      - store_artifacts:
          path: /tmp/circleci-test-results

  deploy-stage:
    <<: *deploy-steps
    environment:
      BUILD_APP: revup-pre-stage
      HEROKU_APP: revup-stage

  deploy-prod:
    <<: *deploy-steps
    environment:
      BUILD_APP: revup-pre-prod
      HEROKU_APP: revup-prod

  deploy-preview:
    <<: *deploy-steps
    environment:
      BUILD_APP: revup-pre-preview
      HEROKU_APP: revup-preview

  deploy-datadev:
    <<: *deploy-steps
    environment:
      BUILD_APP: revup-pre-datadev
      HEROKU_APP: revup-datadev

workflows:
  version: 2
  build-and-deploy:
    jobs:
      - build
      - test-migrations:
          requires:
            - build
      - test:
          requires:
            - build
      - deploy-stage:
          context: frontend-stage
          requires:
            - test
            - test-migrations
          filters:
            branches:
              only: master
      - deploy-preview:
          context: frontend-preview
          requires:
            - test
            - test-migrations
          filters:
            branches:
              only: preview
      - deploy-prod:
          context: frontend-prod
          requires:
            - test
            - test-migrations
          filters:
            branches:
              only: production
      - deploy-datadev:
          context: frontend-datadev
          requires:
            - test
            - test-migrations
          filters:
            branches:
              only: datadev
