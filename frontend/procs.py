
from collections import deque
import logging
from time import time
from threading import Thread, Lock

from hirefire.procs.celery import CeleryProc

from frontend.celery import app
from frontend.libs.utils.general_utils import Borg

LOGGER = logging.getLogger(__name__)
MAX_RETRIES = 10


def on_connection_error(exc, interval):
    LOGGER.info('Connection Error: {0!r}. Retry in {1}s.'.format(
        exc, interval))


class CeleryEventListener(Borg):
    app = app
    MAX_RETRIES = 10
    MAX_RETRY_WINDOW_SECS = 600
    LOCK = Lock()

    def __init__(self, connection):
        super(CeleryEventListener, self).__init__()
        with self.LOCK:
            if not hasattr(self, 'event_consumer'):
                self._init_state(connection)

    def _init_state(self, connection):
        self.event_connection = connection.clone()
        self.event_connection.ensure_connection(
            on_connection_error, self.app.conf.BROKER_CONNECTION_MAX_RETRIES)
        self.state = self.app.events.State()
        self.receiver = self.app.events.Receiver(
            self.event_connection,
            handlers={'*': self.state.event})
        self.event_consumer = t = Thread(target=self._capture_loop,
                                         name='Celery event consumer')
        t.daemon = True
        t.start()

    def _capture_loop(self):
        '''Event listener loop with error handling and retry support

        Kombu has a habbit of raising connection errors instead of handling
        them itself. This can result in the event consumer thread dying, which
        leads to an increased chance in successful analysis and contact imports
        failing. To combat this, catch unhandled exceptions and retry.

        This method keeps retrying until the receiver crashes 10 times within
        10 minutes.
        '''
        # retries is a sliding window of timestamps when the consumer crashed.
        retries = deque(maxlen=self.MAX_RETRIES)
        end_idx = self.MAX_RETRIES - 1
        while 1:
            try:
                for _ in self.receiver.itercapture():
                    pass
                # If capture ends naturally, it probably makes sense to break
                # the retry loop. Log the event at warning level since we don't
                # know under what circumstances this could happen.
                LOGGER.warn("Celery event consumer ended naturally. Ending loop")
                break
            except (KeyboardInterrupt, SystemExit):
                return
            except Exception as err:
                # Track timestamps of uncaught exceptions.
                retries.append(int(time()))
                # Determine the amount of time that has passed between the
                # oldest retry timestamp, and the last timestamp in the sliding
                # window. If there is an index error while trying to access the
                # "last" timestamp, that means that there hasn't been 10 error
                # yet.
                try:
                    error_span = retries[end_idx] - retries[0]
                except IndexError:
                    error_span = None

                # If there haven't been 10 errors yet, or the amount of time
                # between the most recent retry and the oldest tracked retry is
                # more than 10 minutes, log the event and retry. Otherwise give
                # up and re-raise the exception from celery.
                if error_span is None or error_span > self.MAX_RETRY_WINDOW_SECS:
                    LOGGER.warn("Celery event consumer-killing exception raised. "
                                "Restarting")
                else:
                    raise


class WorkerProc(CeleryProc):
    name = 'worker'
    queues = ['celery', 'analysis_queue', 'analysis_light', 'analysis_medium',
              'analysis_heavy', 'analysis_complete', 'contact_import', 'email',
              'contact_deletion', 'contact_merging']
    app = app

    def __init__(self, *args, **kwargs):
        super(WorkerProc, self).__init__(*args, **kwargs)
        # Wrap the channel._size method using the connection.ensure method
        # which automatically retries the method when a connection error
        # occurs.
        self.check_size = self.connection.ensure(self.channel,
                                                 self.channel._size,
                                                 errback=on_connection_error,
                                                 max_retries=MAX_RETRIES)
        self.event_listener = CeleryEventListener(self.connection)

    def quantity(self):
        return sum(self.check_size(queue) or 0 for queue in self.queues) +\
                sum(_.active or 0
                    for _ in self.event_listener.state.alive_workers()
                    if _.hostname.startswith(self.name))


class HeavyWorkerProc(WorkerProc):
    name = 'heavy'
    queues = ['batch_call_sheet', 'batch_csv']


class BackgroundWorkerProc(WorkerProc):
    name = 'background'
    queues = ['background']
