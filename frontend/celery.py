

import os

from celery import Celery as Celery_
from celery.signals import worker_process_shutdown, worker_process_init
from ddtrace import patch
from django.conf import settings
import pymongo


try:
    import raven
    from raven.contrib.celery import register_signal, register_logger_signal
except (ImportError, ModuleNotFoundError):
    raven = None


# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'frontend.settings.dev')


@worker_process_init.connect
def _on_process_init(*args, **kwargs):
    settings.MONGO_CLIENT = client = pymongo.MongoClient(
        settings.MONGODB_DATABASE_HOST)
    settings.MONGO_DB = client.get_default_database()


if raven is not None and hasattr(settings, 'RAVEN_CONFIG'):
    class Celery(Celery_):
        def on_configure(self):
            client = raven.Client(**settings.RAVEN_CONFIG)
            # register a custom filter to filter out duplicate logs
            register_logger_signal(client)

            # hook into the Celery error handler
            register_signal(client)
else:
    Celery = Celery_

patch(celery=True)
app = Celery('frontend')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
