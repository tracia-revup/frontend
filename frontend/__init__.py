
# This will make sure the app is always imported when Django starts so that
# shared_task will use this app.
# Without djcelery frontend.celery is never imported for some reason.
from .celery import app as celery_app # noqa
