import operator


_NONE = object()


class BaseRoute(object):
    def __init__(self, *funcs_or_conds, **kwargs):
        # A standard Route's behavior is AND.
        self.funcs_or_conds = funcs_or_conds
        self.reduce_op = kwargs.get('reduce_op', operator.and_)
        self.lazy_until = kwargs.get('lazy_until', False)
        self.negated = kwargs.get('negated')
        
    def evaluate_conditional(self, request, view):
        reduced_result = _NONE

        # Iterate over all necessary conditionals until we can determine if
        # we should route or not.
        for condition in self.funcs_or_conds:
            if hasattr(condition, 'should_route'):
                result = condition.should_route(request, view)
            else:
                result = condition(request, view)

            if reduced_result is _NONE:
                reduced_result = result
            else:
                reduced_result = self.reduce_op(reduced_result, result)

            if self.lazy_until is not None and \
                            self.lazy_until is reduced_result:
                break

        if reduced_result is not _NONE:
            return not reduced_result if self.negated else reduced_result

    def should_route(self, request, view):
        return self.evaluate_conditional(request, view)


class Route(BaseRoute):
    """A potential route for a view to take.

    Routes can be used to call variants of a dispatch
    method (get, post, etc.), or Routes can be merely used as a
    method of tagging a request as matching a specific Route.

    Usage:
    >>> def is_active(request, view):
    >>>     return request.user.is_active
    >>>
    >>> def is_staff(request, view):
    >>>     return request.user.is_staff
    >>>
    >>> class SomeView(RoutingView):
    >>>     routes = [Route(Or(is_active, is_staff),
    >>>                     suffix="active", name="active_route")
    >>>     ]
    >>>
    >>>     def get_active(self, request, *args, **kwargs):
    >>>         # Will be routed here if user is active and/or staff.
    >>>         # Note the suffix field of the Route and this method name.
    >>>         pass
    >>>
    >>>     def get(self, request, *args, **kwargs):
    >>>         # Will be routed here if no routes are matched, or there is
    >>>         # no 'get_active' method. I.e. the default.
    >>>         pass
    """
    def __init__(self, *funcs_or_conds, **kwargs):
        """
        :param funcs_or_conds: May be one or more functions or conditionals to
                    route on. See Usage above.
        :param suffix: a string to append to attribute names for routing
        :param name: The name of the Route. Useful for usage with the 'route'
                    template tag.
        """
        self._suffix = kwargs.get('suffix')
        if self._suffix == "f":
            # 'f' is reserved for forcing usage of an attribute name, and not
            # allowing for Route redirection.
            raise ValueError("The suffix 'f' is a reserved value.")
        self.name = kwargs.get('name')
        super(Route, self).__init__(*funcs_or_conds, **kwargs)

    @property
    def suffix(self):
        return "_{}".format(self._suffix) if self._suffix else ""

    def __eq__(self, other):
        if isinstance(other, str):
            return self.name == other
        else:
            return super(Route, self).__eq__(other)



