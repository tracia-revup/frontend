
from django import template
from django.template.defaulttags import IfNode, TemplateIfParser

register = template.Library()


@register.tag('route')
def do_route(parser, token):
    """Alter the template content based on the route taken by the view.

    Use the Route's name to select a block of content. More than one
    route name can be specified for an OR condition.

    {% route "Mustang" "Charger" "Challenger" %}
        Muscle Cars
    {% elifroute "car" %}
        Some other kind of car
    {% elseroute %}
        Not a car at all
    {% endroute %}
    """
    # This function creates a chain of conditionals, so we can utilize
    # Django's built-in IfNode (e.g. {% if %})
    def get_condition(routes_):
        if_routes = [("route", "==", route) for route in routes_]
        token_list = []
        for route in if_routes[:-1]:
            token_list.extend(route)
            token_list.append("or")
        token_list.extend(if_routes[-1])
        return TemplateIfParser(parser, token_list).parse()

    # {% route ... %}
    routes = token.split_contents()[1:]
    nodelist = parser.parse(('elifroute', 'elseroute', 'endroute',))
    routes_nodelists = [(get_condition(routes), nodelist)]
    token = parser.next_token()

    # {% elifroute ... %} (repeatable)
    while token.contents.startswith('elifroute'):
        routes = token.split_contents()[1:]
        nodelist = parser.parse(('elifroute', 'elseroute', 'endroute',))
        routes_nodelists.append((get_condition(routes), nodelist))
        token = parser.next_token()

    # {% elseroute %} (optional)
    if token.contents == 'elseroute':
        nodelist = parser.parse(('endroute',))
        routes_nodelists.append((None, nodelist))
        token = parser.next_token()

    # {% endroute %}
    assert token.contents == 'endroute'

    return IfNode(routes_nodelists)
