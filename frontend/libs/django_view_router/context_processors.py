

def route(request):
    """Add the current Route (if it exists) to the context"""
    return {'route': request.route if hasattr(request, "route") else None}
