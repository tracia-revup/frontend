
from django.views.generic import View, TemplateView, FormView


# These are the attributes that can be given a suffix for the view router
# to route to. Any attributes not listed below will not receive routing.
SUPPORTED_ATTRS = set(View.http_method_names)
SUPPORTED_ATTRS.update((
    "form_valid", "form_invalid", "form_class", "get_success_url",
    "get_queryset", "get_object", "model", "queryset", "template_name",))


class RoutingViewBase(object):
    routes = []

    def dispatch(self, request, *args, **kwargs):
        """Try to dispatch to the right method;
        if a method doesn't exist, defer to the error handler.
        Also defer to the error handler if the request method
        isn't on the approved list.

        IMPORTANT NOTE: Don't override this method. Use 'do_dispatch'
            If you need to treat 'dispatch' like a general initialization
            method (for all request types), you will typically want to
            override 'do_dispatch' instead.
            If you override 'dispatch', you will not have any of the routing
            features.
        """
        if request.method.lower() in self.http_method_names:
            method = request.method.lower()
            # Get the route and apply it to the request
            route = self._get_route(request)
            request.route = route
            handler = getattr(self, method, self.http_method_not_allowed)
        else:
            handler = self.http_method_not_allowed
        return self.do_dispatch(request, handler, *args, **kwargs)

    def _get_route(self, request):
        """Return the first accepted Route, or None."""
        for route in self.routes:
            if route.should_route(request, self):
                return route
        return None

    def do_dispatch(self, request, handler, *args, **kwargs):
        """A wrapper around the call to the handler method.

        This allows the user to do everything they would typically do in
        'dispatch' here instead, so the routing functionality isn't
        disrupted.
        """
        return handler(request, *args, **kwargs)

    def __getattribute__(self, item):
        # Programmer may want to force a specific method, instead of using
        # the route override. This supports that.
        # E.g. self.get_f(request, *args, **kwargs)
        if item.endswith("_f"):
            return super(RoutingViewBase, self).__getattribute__(item[:-2])

        # Every time an attr is requested, this checks if there is a route
        # override for that attr
        if item in SUPPORTED_ATTRS:
            try:
                route = self.request.route
                if route:
                    routed_attr = "{}{}".format(item, route.suffix)
                    if hasattr(self, routed_attr):
                        item = routed_attr
            except AttributeError:
                # request.route may not be defined yet
                pass
        return super(RoutingViewBase, self).__getattribute__(item)


# Supported View types
class RoutingView(RoutingViewBase, View): pass
class RoutingTemplateView(RoutingViewBase, TemplateView): pass
class RoutingFormView(RoutingViewBase, FormView): pass