"""
This code was "borrowed" from django_rest_condition
(https://github.com/caxap/rest_condition), and altered to fit
my needs. They did a good job with the conditionals, and I see
no reason to reinvent the wheel.
"""

import operator

from .route import BaseRoute

__all__ = ['Condition', 'C', 'And', 'Or', 'Not']


class Condition(BaseRoute):
    """
    Provides a simple way to define complex and multi-depth
    (with logic operators) routing-decision tree.

    Example of preferred usage:
    >>> cond = Or(And(func1, func2), And(func3, Not(func4)))

    Other possible usage:
    >>> cond = C(func1, func2) | C(func3, ~C(func4))
    """
    @classmethod
    def And(cls, *funcs_or_conds):
        return cls(reduce_op=operator.and_, lazy_until=False, *funcs_or_conds)

    @classmethod
    def Or(cls, *funcs_or_conds):
        return cls(reduce_op=operator.or_, lazy_until=True, *funcs_or_conds)

    @classmethod
    def Not(cls, *funcs_or_conds):
        return cls(negated=True, *funcs_or_conds)

    def __or__(self, perm_or_cond):
        return self.Or(self, perm_or_cond)

    def __ior__(self, perm_or_cond):
        return self.Or(self, perm_or_cond)

    def __and__(self, perm_or_cond):
        return self.And(self, perm_or_cond)

    def __iand__(self, perm_or_cond):
        return self.And(self, perm_or_cond)

    def __invert__(self):
        return self.Not(self)

    def __call__(self):
        return self

# Define some shortcuts
C, And, Or, Not = Condition, Condition.And, Condition.Or, Condition.Not

