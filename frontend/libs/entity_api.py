from collections import ChainMap
from itertools import chain, takewhile

from django.conf import settings
from django.db import connections
from funcy import (compact, keep, flatten, mapcat, itervalues, rpartial,
                   get_in, merge_with, join, lfilter, compose, cached_property)


def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return (
        dict(zip(columns, row))
        for row in cursor.fetchall()
    )


def transaction_status(txid, user_id):
    """Get the status counts for for contacts being processed as part of a
    specific transaction (ie import).

    Returns a tuple of the number of contacts seen for the transaction, and the
    number of contacts within the transaction that are finished processing.
    """
    connection = connections['contactdb']
    with connection.cursor() as cursor:
        # Find the number of contacts that have a link to the transaction being
        # processed.
        cursor.execute(
            """
            SELECT count(contact.id) AS count_1
            FROM contact
            WHERE
              contact.user_id = %(user_id)s AND
              (EXISTS
                (SELECT 1 FROM contact_transactionref, transaction_ref
                   WHERE contact.id = contact_transactionref.contact_id AND
                         transaction_ref.id = contact_transactionref.transaction_ref_id AND
                         transaction_ref.txid = %(txid)s))
            """, {'txid': txid, 'user_id': user_id})
        seen = cursor.fetchone()[0]
        if seen == 0:
            return 0, 0, (0,0,0)

        # Contacts that dead-ended. They were processed, but because they have
        # no identity records (as a result of having no names). There is no
        # further work to be done on them.
        cursor.execute(
            """
            SELECT count(contact.id) AS count_1
            FROM contact
            WHERE contact.user_id = %(user_id)s AND (EXISTS (SELECT 1
            FROM contact_transactionref, transaction_ref
            WHERE contact.id = contact_transactionref.contact_id AND
                transaction_ref.id = contact_transactionref.transaction_ref_id AND
                transaction_ref.txid = %(txid)s)) AND
                NOT (EXISTS (SELECT 1 FROM identity
                                WHERE contact.id = identity.contact_id))
            """, {'txid': txid, 'user_id': user_id})
        deadend_done = cursor.fetchone()[0]

        # The number of contacts that are associated with transaction, have at
        # least 1 identity, and none of the identities have a modified
        # timestamp newer than created timestamp on transactionref
        cursor.execute(
            """
           SELECT COUNT(contact_transactionref.contact_id)
           FROM contact_transactionref
           JOIN transaction_ref ON (
                transaction_ref.id = contact_transactionref.transaction_ref_id)
           WHERE transaction_ref.user_id = %(user_id)s AND
                 transaction_ref.txid = %(txid)s AND
                 (EXISTS (SELECT 1 FROM identity
                   WHERE contact_transactionref.contact_id = identity.contact_id)) AND
                 NOT (EXISTS (SELECT 1 FROM identity
                       WHERE contact_transactionref.contact_id = identity.contact_id AND
                       identity.updated >= transaction_ref.created))
           """, {'txid': txid, 'user_id': user_id})
        noupdates_done = cursor.fetchone()[0]

    result = _identity_status(txid, user_id)
    num_done_contacts = len([1 for data in list(result.values())
                             if not data['working']])

    done = num_done_contacts + noupdates_done + deadend_done
    return seen, done, (num_done_contacts, noupdates_done, deadend_done)


def updated_in_transaction(txid, user_id):
    """Get the composite_ids of contacts that were updated as part of a
    specific transaction (ie import)

    Contacts that were imported as part of the transaction that did not contain
    enough information to create a person entity, or contain any new
    information are not included.
    """
    result = _identity_status(txid, user_id)
    done_contacts = [composite_id
                     for composite_id, data in list(result.items())
                     if not data['working']]
    return done_contacts


def _identity_status(txid, user_id):
    """Utility to create a datastructure that maps contact composite_id to
    (identity, person_entity).

    We use this to track which identities are finished processing. We have to
    be mindful that there can be multiple Identity records for a single
    contact, and that a PersonEntity can include multiple identity records and
    additionally span multiple contacts. It is important we don't mark a
    contact as being done until all associated Identity records are finished
    being processed. The datastructure this function returns is used to
    determine that.
    """
    connection = connections['contactdb']
    with connection.cursor() as cursor:
        # All datetimes returned from queries should be in the UTC timezone.
        # Without this command, we get timezones in the client's timezone with
        # the timezone offset included. This would be fine, but we have Django
        # configured to be timezone unaware, and as a result it throws away
        # this timezone information. This causes problems in development
        # environments, where the timezone is usually set to pacific.
        cursor.execute("SET TIME ZONE 'UTC';")
        cursor.execute(
            "SELECT created FROM transaction_ref WHERE txid = %(txid)s",
            {'txid': txid})
        tref_created = cursor.fetchone()
        if tref_created is None:
            return {}
        else:
            tref_created = tref_created[0]

        # all identities that are associated with a contact that was affected
        # by transaction, and were updated after transaction was created.
        cursor.execute(
            """
            SELECT identity.id, identity.updated,
                    identity.contact_composite_id, identity.contact_id
            FROM identity JOIN contact ON contact.id = identity.contact_id
            WHERE contact.user_id = %(user_id)s AND (EXISTS (SELECT 1
            FROM contact_transactionref, transaction_ref
            WHERE contact.id = contact_transactionref.contact_id AND
                transaction_ref.id = contact_transactionref.transaction_ref_id AND
                transaction_ref.txid = %(txid)s AND
                identity.updated >= transaction_ref.created))
            """, {'txid': txid, 'user_id': user_id})
        identities = dictfetchall(cursor)

    # Find all person entities that have been updated since the transaction
    # we're checking was started.
    pe_cursor = settings.MONGO_DB[settings.PERSON_ENTITIES_COLLECTION].find(
        {'user_ids': user_id, 'date_modified': {'$gte': tref_created}})
    person_entities = {}
    for pe in pe_cursor:
        for identity_id in pe['identity_ids']:
            person_entities[identity_id] = pe

    result = {}
    for identity in identities:
        data = result.setdefault(identity['contact_composite_id'],
                                 {'done': [], 'working': []})
        pe = person_entities.get(identity['id'])
        # If the date_modified timestamp is newer than the updated timestamp on
        # the Identity, then processing for that identity is finished.
        if pe is not None and pe['date_modified'] >= identity['updated']:
            data['done'].append((identity, pe))
        else:
            data['working'].append((identity, pe))
    return result


class EntityResolver:
    """Resolve PersonEntity Records down to Contribution Ids

    The purpose of this class is to resolve PersonEntity records down to
    contribution ids while retaining a copy of all entity records found.
    Information beyond the contribution ids we need is being added to the
    entities, and we needed a way to get that data. The best way to do this was
    to store copies of the entities themselves along with the contribution ids
    on the contact wrapper.

    This class is meant to be backwards compatible with the old resolver signal
    action. It accepts all the same configuration parameters as
    `PersonEntityResolveContributionIds`.

    For each supported entity type, three properties are dynamically created
    and populated on the instance at request time. The format of the properties
    are:
    - <type>_entity_ids
    - <type>_entities
    - <type_ids

    So for example the property names for SEC data would be:
    - sec_entity_ids
    - sec_entities
    - sec_ids

    Attribute and item access are both supported.

    It is recommended to instantiate this class using the `factory()` class
    method for client entity support.
    """
    DEFAULT_CONFIG = {
        'entity_types': {'fused', 'fec', 'misp', 'municipal', 'non_profit',
                         'real_estate', 'sec', 'opencorp', 'client_data'},
        'fused_field_mapping': {'fec': 'fec_daily_id'},
        'collection_mapping': {},
    }
    CLIENT_DATA_TMPL = {
        'fused_field_mapping': 'client_data.{}._id',
        'collection_mapping': 'client_entities__{}',
    }

    def __init__(self, person_entities, mongo_db, account_key=None, **kwargs):
        self.db = mongo_db
        self.account_key = account_key
        definition = merge_with(join, self.DEFAULT_CONFIG, kwargs)
        self.entity_types = definition['entity_types']
        self.fused_field_mapping = definition['fused_field_mapping']
        self.collection_mapping = definition['collection_mapping']
        self.fused_entity_ids = self._extract_fused_entity_ids(person_entities)

    @cached_property
    def fused_client_summary(self):
        """Return client data summary information found in fused entities

        Filters out client data summaries that are empty.
        """
        if not self.account_key:
            return
        return lfilter(
            compose(any, itervalues),
            map(rpartial(get_in, ['client_data', self.account_key], {}),
                self.fused_entities))

    def get(self, name, default=None):
        """dict-like `get` method for backwards compatibility"""
        return getattr(self, name, default)

    def __getitem__(self, name):
        r = getattr(self, name, KeyError)
        if r is KeyError:
            raise KeyError(repr(name))
        return r

    def __getattr__(self, key):
        def _raise_attrib_error():
            raise AttributeError(
                "'{0}' object has no attribute '{1}'".format(
                    self.__class__.__name__, key))
        if key.endswith('_entity_ids'):
            # Entity ids
            entity_type = key[:-11]
            if entity_type not in self.entity_types:
                _raise_attrib_error()
            data = self._extract_entity_ids(entity_type, self.fused_entities)
        elif key.endswith('_entities'):
            # Entities themselves
            entity_type = key[:-9]
            if entity_type not in self.entity_types:
                _raise_attrib_error()
            entity_ids = getattr(self, '{}_entity_ids'.format(entity_type))
            data = self._fetch_entities(entity_type, entity_ids)
        elif key.endswith('_ids'):
            # Contrib ids
            entity_type = key[:-4]
            if entity_type not in self.entity_types:
                _raise_attrib_error()
            entities = getattr(self, '{}_entities'.format(entity_type))
            data = self._extract_contrib_ids(entities)
        elif key in self.entity_types:
            # Assume contrib ids were wanted for backwards compatibility.
            return getattr(self, '{}_ids'.format(key))
        else:
            _raise_attrib_error()

        # Cache results on instance
        setattr(self, key, data)
        return data

    @classmethod
    def factory_from_contact(cls, contact, mongo_db, **kwargs):
        """Helper factory method to start resolution from a contact

        This is a debugging and data exploration tool mainly for use from the
        REPL.
        """
        composite_ids = contact.get_composite_ids()
        # This is a hacky trick to use the `_query` instance method to lookup
        # person entities before we instantiate the class. We call the method
        # passing `None` for the "self" value, and pass mongo_db in manually.
        person_entities = list(cls._query(None, 'person_entities',
                                          composite_ids, 'composite_id',
                                          mongo_db=mongo_db))
        if 'account_key' in kwargs:
            to_call = cls.factory
        else:
            to_call = cls
        return to_call(person_entities, mongo_db, **kwargs)

    @classmethod
    def factory(cls, person_entities, mongo_db, account_key, **kwargs):
        """Helper to instantiate the class with client data config overrides
        """
        client_data_kwargs = {
            key: {'client_data': tmpl.format(account_key)}
            for key, tmpl in cls.CLIENT_DATA_TMPL.items()
        }
        return cls(person_entities, mongo_db, account_key=account_key,
                   **client_data_kwargs, **kwargs)

    def _query(self, collection_name, ids, query_field='_id', mongo_db=None):
        """Mongo query helper"""
        query = {query_field: {'$in': [id for id in ids if id]}}
        # This is a hacky trick to allow us to use this method in the
        # classmethod `factory_from_contact` before the class is instantiated.
        # If we see mongo_db is passed in as an optional kwarg, we use that
        # over the one on "self" (which may not exist yet...)
        results = (mongo_db or self.db)[collection_name].find(query)
        yield from results

    def _fetch_entities(self, entity_type, entity_ids):
        """Internal method for fetching entities

        Determines which collection to fetch the entities from using the entity
        type. Uses the collection name from the override map if present,
        otherwise assumes collection follows the standard naming convention and
        appends "_entities" to the entity type.

        Requires entity type and entity ids. 
        """
        stock_name = '{}_entities'.format(entity_type)
        collection_name = self.collection_mapping.get(entity_type, stock_name)
        source_entities = self._query(collection_name, entity_ids)
        return list(source_entities)

    def _extract_entity_ids(self, entity_type, source_entities):
        """Helper to extract entity ids from the fused entity for the given
        entity type

        Automatically determines the field the entity ids will be found under.
        Uses the field name from the override map if present, otherwise assumes
        entity follows the standard naming convention and appends "_id" to the
        entity type.
        """
        key = self.fused_field_mapping.get(
            entity_type, '{}_id'.format(entity_type))
        return self._extract_ids(key, source_entities)

    @classmethod
    def _extract_contrib_ids(cls, source_entities):
        """Helper to specifically extract contrib ids from sub-entities"""
        return cls._extract_ids('record_links', source_entities)

    @classmethod
    def _extract_ids(cls, key, source_entities):
        """DRY method for extracting ids from multiple records

        Supports fetching ids that are nested several layers deep from the
        records. To extract a nested field, join each key layer with a period.
        ex:

            _extract_ids('a.b.c', [{'a': {'b': {'c': [1]}}}])
        """
        return set(compact(mapcat(rpartial(get_in, key.split('.'), []),
                                  source_entities)))

    @classmethod
    def _extract_fused_entity_ids(cls, person_entities):
        """Extract fused entity ids from person entity records"""
        return set(flatten(mapcat(itervalues,
                                  keep(rpartial(get_in, ['node_quality']),
                                       person_entities))))
