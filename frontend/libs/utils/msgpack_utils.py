
import datetime
import dateutil
import gc

import msgpack


EXTTYPE_DATETIME = 70
EXTTYPE_DATE = 71


def _unpacker_hook(code, data):
    if code == EXTTYPE_DATETIME:
        values = loads(data)

        if len(values) == 8:  # we have timezone
            values[-1] = dateutil.tz.tzoffset(None, values[-1])
        return datetime.datetime(*values)
    elif code == EXTTYPE_DATE:
        values = loads(data)
        return datetime.date(*values)

    return msgpack.ExtType(code, data)


# This will only get called for unknown types
def _packer_unknown_handler(obj):
    if isinstance(obj, datetime.datetime):
        if obj.tzinfo:
            components = (obj.year, obj.month, obj.day, obj.hour, obj.minute, obj.second, obj.microsecond, int(obj.utcoffset().total_seconds()))
        else:
            components = (obj.year, obj.month, obj.day, obj.hour, obj.minute, obj.second, obj.microsecond)

        # we effectively double pack the values to "compress" them
        data = msgpack.ExtType(EXTTYPE_DATETIME, dumps(components))
        return data
    elif isinstance(obj, datetime.date):
        # we effectively double pack the values to "compress" them
        components = (obj.year, obj.month, obj.day)
        data = msgpack.ExtType(EXTTYPE_DATE, dumps(components))
        return data

    raise TypeError("Unknown type: {}".format(obj))


def dumps(obj, **kwargs):
    # we don't use a global packer because it wouldn't be re-entrant safe
    return msgpack.dumps(obj, use_bin_type=True, default=_packer_unknown_handler, **kwargs)


def loads(payload):
    try:
        # we temporarily disable gc during unpack to bump up perf: https://pypi.python.org/pypi/msgpack-python
        gc.disable()
        # This must match the above _packer parameters above.  NOTE: use_list is faster
        return msgpack.loads(payload, use_list=False, encoding='utf-8', ext_hook=_unpacker_hook)
    finally:
        gc.enable()
