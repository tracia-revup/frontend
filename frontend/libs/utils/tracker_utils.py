"""This file is home to utilities for django activity stream, our
user-activity tracker.
"""
from actstream import action


def _get_type(obj):
    return obj._meta.app_label, obj.__class__.__name__


def action_send(request, sender, **kwargs):
    # Track the action differently depending on user impersonation
    if hasattr(request.user, 'is_impersonate') and request.user.is_impersonate:
        # Track the action as one for the impersonating user, but keep track
        # of everything that was done in the extra data
        verb = "{} (imp.)".format(kwargs.pop("verb", ""))
        kwargs['orig_sender'] = sender.id
        kwargs['orig_sender_type'] = _get_type(sender)
        target = kwargs.pop('target', None)
        if target:
            kwargs["orig_target"] = target.id
            kwargs["orig_target_type"] = _get_type(target)
        action.send(request.impersonator, target=request.user,
                    verb=verb, public=False, **kwargs)
    else:
        action.send(sender, **kwargs)

