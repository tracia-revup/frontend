
from collections import Counter
import warnings

from expecter import expect as base_expect, _hidetraceback
from django.test import (TestCase as BaseTestCase, TransactionTestCase as
                         BaseTransactionTestCase, override_settings)
from rest_framework.test import APITestCase as APITestCaseBase


class expect(base_expect):
    def is_truthy(self):
        __tracebackhide__ = _hidetraceback()  # pylint: disable=unused-variable
        msg = "Expected a truthy value but got {!r}".format(self._actual)
        assert bool(self._actual), msg

    def is_falsey(self):
        __tracebackhide__ = _hidetraceback()  # pylint: disable=unused-variable
        msg = "Expected a falsey value but got {!r}".format(self._actual)
        assert not bool(self._actual), msg

    def items_equal(self, other):
        __tracebackhide__ = _hidetraceback()  # pylint: disable=unused-variable
        return expect(Counter(self._actual)) == Counter(other)


class RemoveShortDescription(object):
    def shortDescription(self):
        """Hack to prevent nosetests from showing docstrings instead of method
        and class name in test runner.

        See: http://www.saltycrane.com/blog/2012/07/how-prevent-nose-unittest-using-docstring-when-verbosity-2/
        """
        return None


class PytestTestRunner(object):
    """Runs pytest to discover and run tests."""

    def __init__(self, verbosity=1, failfast=False, keepdb=False, **kwargs):
        self.verbosity = verbosity
        self.failfast = failfast
        self.keepdb = keepdb

    def run_tests(self, test_labels):
        """Run pytest and return the exitcode.

        It translates some of Django's test command option to pytest's.
        """
        warnings.warn("This is not the preferred way to run unit tests! It is "
                      "better to call py.test directly.")
        import pytest

        argv = []
        if self.verbosity == 0:
            argv.append('--quiet')
        if self.verbosity == 2:
            argv.append('--verbose')
        if self.verbosity == 3:
            argv.append('-vv')
        if self.failfast:
            argv.append('--exitfirst')
        if self.keepdb:
            argv.append('--reuse-db')

        argv.extend(test_labels)
        return pytest.main(argv)


@override_settings(CELERY_ALWAYS_EAGER=True)
class TestCase(BaseTestCase):
    pass


@override_settings(CELERY_ALWAYS_EAGER=True)
class TransactionTestCase(BaseTransactionTestCase):
    pass


@override_settings(CELERY_ALWAYS_EAGER=True)
class APITestCase(APITestCaseBase):
    pass
