import logging
from smtplib import SMTPServerDisconnected
import time

from django.conf import settings
from django.core.mail import EmailMessage as EmailMessageBase
from django.template.loader import render_to_string


LOGGER = logging.getLogger(__name__)


def send_template_email(subject, email_template, context, email_to,
                        sender_address=settings.DEFAULT_FROM_EMAIL,
                        email_type='', extra_log=''):
    """Helper method to keep the code DRY."""
    # Generate the content for the email
    template = render_to_string(email_template, context)
    if not isinstance(email_to, (list, tuple, set)):
        email_to = [email_to]

    msg = EmailMessage(subject, template, sender_address,
                       email_to[:1], bcc=email_to[1:])
    msg.content_subtype = "html"  # Main content is now text/html

    # This actually sends the email. Use with caution
    msg.send()

    LOGGER.info(f"Sent a[n] {email_type} email to the address: "
                f"'{email_to[0]}' {extra_log}")


def send_user_email(user, subject, sender_address, email_template, context,
                     task_type_str, extra_log='', email_to=None):
    """Helper method to keep the tasks DRY."""
    context.update({'user': user})
    email_to = email_to or [user.email]

    send_template_email(subject, email_template, context, email_to,
                        sender_address=sender_address,
                        email_type=task_type_str, extra_log=extra_log)


class EmailMessage(EmailMessageBase):
    """Extends django core EmailMessage class in order to allow retries
    on email sending for SMTP Failures
    """
    def send(self, *args, **kwargs):
        error = None

        # Sometimes the Email server randomly disconnects. This is a little
        # retry code to give the SMTP server a chance to reset
        for _ in range(3):
            try:
                return super(EmailMessage, self).send(*args, **kwargs)

            except SMTPServerDisconnected as e:
                time.sleep(1)
                error = e
                continue

        LOGGER.info("Ran out of retry attempts when trying to send email.")
        if error is not None:
            raise error
