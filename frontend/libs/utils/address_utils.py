import re

import usaddress

from .string_utils import similarity


_scrub_value_table = {ord(c): None for c in "\t\n;, "}


def _compile_replacement_map(replacement_map):
    return re.compile("|".join([
            "|".join("((^|(?<= )){}(?= |\.|;|,|$))".format(replace)
                      for replace in map(re.escape, map_group))
                for map_group in replacement_map.values()]))


def _build_substitute_map(replacement_map):
    group_map = {}
    for replacement, map_group in replacement_map.items():
        for replace in map_group:
            group_map[replace] = replacement
    return group_map


class AddressParser(object):
    """ Quick wrapper to make usaddress easier to interface."""
    # Replace commonly abbreviate road types
    _road_type_map = {
        "avenue": ("ave",),
        "boulevard": ("blvd",),
        "bridge": ("brg", "brdg"),
        "circle": ("cir",),
        "court": ("ct",),
        "drive": ("dr",),
        "expressway": ("expy",),
        "heights": ("hts",),
        "highway": ("hwy",),
        "lane": ("ln",),
        "parkway": ("pkwy",),
        "point": ("pt",),
        "ridge": ("rdg",),
        "road": ("rd",),
        "route": ("rte",),
        "square": ("sq",),
        "street": ("st",),
        "terrace": ("ter", "terr"),
        "way": ("wy",)
    }
    # Build a regular expression that ORs all of the values from the road map
    _road_replace_pattern = _compile_replacement_map(_road_type_map)
    # We'll need this for the actual replacement algorithm
    _road_substitute_map = _build_substitute_map(_road_type_map)

    # Replace commonly abbreviated cardinal directions
    _direction_map = {
        "north": ("n",),
        "south": ("s",),
        "west": ("w",),
        "east": ("e",),
        "northeast": ("ne",),
        "northwest": ("nw",),
        "southeast": ("se",),
        "southwest": ("sw",),
    }
    _direction_replace_pattern = _compile_replacement_map(_direction_map)
    _direction_substitute_map = _build_substitute_map(_direction_map)

    # Replace commonly abbreviated street names
    _street_name_map = {
        "saint": ("st",),
        "first": ("1st",),
        "second": ("2nd",),
        "third": ("3rd",),
        "fourth": ("4th",),
        "fifth": ("5th",),
        "sixth": ("6th",),
        "seventh": ("7th",),
        "eighth": ("8th",),
        "ninth": ("9th",),
    }
    _street_name_replace_pattern = _compile_replacement_map(_street_name_map)
    _street_name_substitute_map = _build_substitute_map(_street_name_map)

    # Replace commonly abbreviated city names
    _city_name_map = {
        "saint": ("st.", "st",),
        "fort": ("ft.", "ft"),
    }

    _city_name_replace_pattern = _compile_replacement_map(_city_name_map)
    _city_name_substitute_map = _build_substitute_map(_city_name_map)

    _mapping = dict(
        building_name=['BuildingName', 'LandmarkName'],
        street_name='StreetName',
        street_number='AddressNumber',
        street_premodifier='StreetNamePreModifier',
        street_predirectional='StreetNamePreDirectional',
        street_pretype='StreetNamePreType',
        street_suffix='StreetNamePostType',
        street_postdirectional='StreetNamePostDirectional',
        occupancy_type='OccupancyType',
        occupancy_id='OccupancyIdentifier',
        po_box=['USPSBoxType', 'USPSBoxID'],
        city_name='PlaceName',
        post_code='ZipCode',
        state_name='StateName',
    )
    
    def __init__(self, input):
        self.addr_dict = {}
        self.parsed = usaddress.parse(input)
        for val, key in self.parsed:
            # usaddress breaks multi word values into separate tuples, undo
            # that but take care to space properly
            val = str(val).translate(_scrub_value_table)
            if key in self.addr_dict:
                self.addr_dict[key] += ' ' + val
            else:
                self.addr_dict[key] = val

    def __str__(self):
        return str(self.addr_dict)

    def __getattribute__(self, item):
        if item == "_mapping":
            return object.__getattribute__(self, item)
        elif item in self._mapping:
            mapping = self._mapping[item]
            if isinstance(mapping, (list, tuple)):
                return " ".join(list(filter(
                    None, [self._address_field(f) for f in mapping])))
            else:
                return self._address_field(mapping)
        else:
            return object.__getattribute__(self, item)

    def _address_field(self, field):
        return self.addr_dict.get(field, '')

    def get_street_first_line(self):
        return " ".join(list(filter(None, [self.building_name,
                                      self.street_number,
                                      self.street_premodifier,
                                      self.street_predirectional,
                                      self.street_pretype,
                                      self.street_name,
                                      self.street_suffix,
                                      self.street_postdirectional])))

    def get_full_street(self):
        return " ".join(list(filter(None, [self.get_street_first_line(),
                                      self.occupancy_type,
                                      self.occupancy_id,
                                      self.po_box])))

    def apples_to_apples(self, other, preprocess=True):
        """Compare addresses by the fields they do have

        Tokenize each address and only compare the fields that are present
        in -both- addresses.
        Example:
            123 Main St. Suite 123
            123 Main St, San Jose, CA

        In the above example, only "123 Main St." will be compared.
        This could lead to situations where a positive match is returned when
        the two addresses are not actually the same.
        Example:
            123 Main St, San Jose, CA
            123 Main St, 72305

        The above example would match; however, 72305 is not a zip code in
        San Jose.
        Use with caution.

        :param other: The other address to compare
        :param preprocess: This boolean will cause preprocessing code to be
                           executed on the addresses. Typically, these are
                           string replacements for abbreviation expansion
        """
        if not isinstance(other, self.__class__):
            if isinstance(other, str):
                other = self.__class__(other)
            else:
                raise TypeError("Unable to compare to type: "
                                "{}".format(type(other)))

        self_values = self._tokenize_and_process_address(preprocess)
        other_values = other._tokenize_and_process_address(preprocess)

        values = []
        for i, value in enumerate(self_values):
            other_value = other_values[i]
            if value and other_value:
                values.append((value, other_value))

        if not values:
            # Nothing in common
            return 0
        else:
            return similarity(" ".join(i for i, _ in values),
                              " ".join(j for _, j in values))

    def _tokenize_and_process_address(self, preprocess=True):
        if preprocess:
            field_modifiers = {
                "street_suffix": self.process_street_suffix,
                "street_predirectional": self.process_direction,
                "street_postdirectional": self.process_direction,
                "street_name": self.process_street_name,
                "city_name": self.process_city_name,
            }
        else:
            field_modifiers = {}

        values = []
        for field in self._mapping.keys():
            value = getattr(self, field).lower()
            if value:
                # Check for a field modifier
                if field in field_modifiers:
                    value = field_modifiers[field](value)
            values.append(value)
        return values

    @classmethod
    def _regex_substitute(cls, pattern, sub_map, string):
        return pattern.sub(lambda match: sub_map[match.group(0)], string)

    @classmethod
    def process_street_suffix(cls, suffix):
        return cls._regex_substitute(cls._road_replace_pattern,
                                     cls._road_substitute_map, suffix)

    @classmethod
    def process_direction(cls, direction):
        return cls._regex_substitute(cls._direction_replace_pattern,
                                     cls._direction_substitute_map, direction)

    @classmethod
    def process_street_name(cls, name):
        return cls._regex_substitute(cls._street_name_replace_pattern,
                                     cls._street_name_substitute_map, name)

    @classmethod
    def process_city_name(cls, name):
        return cls._regex_substitute(cls._city_name_replace_pattern,
                                     cls._city_name_substitute_map, name)
