
from abc import ABCMeta, abstractmethod


class AbstractConfigSetController(object, metaclass=ABCMeta):
    """Abstract base class for defining an interface for user configurable
    questions.
    """

    @abstractmethod
    def get_info(self, record, **kwargs):
        """Example:

            return dict(
                id=question_set.id,
                title=question_set.title,
                icon=question_set.icon,
                allow_multiple=question_set.allow_multiple,
                fields=self.get_fields(**kwargs),
            )
        """
        pass

    @abstractmethod
    def get_fields(self, **kwargs):
        """Should return a list of dicts containing at least the following
         keys: "label", "type", and "expected".

        "expected" should be a list of expected values to submit to the API.
        The values are dicts with two required keys: "field" and "type"

        Example format:
        [
            {"label": "First Name",
             "type": "text",
             "expected": [
                 {"field": "first_name",
                  "type": "text"}]
            },
            {"label": "Last Name",
             "type": "text",
             "expected": [
                 {"field": "last_name",
                  "type": "text"}]
            }
        ]
        """
        pass

    def get_expected(self, **kwargs):
        """Extract the expected items out of the get_fields method."""
        all_expected = []
        for field in self.get_fields(**kwargs):
            expected = field.get("expected")
            if expected:
                all_expected.append(expected)
        return all_expected

    def get_shallow_data(self, data):
        """This method can be used to modify the 'data' dict that is stored
           in the backend record. The value that is returned from here will
           be displayed to the user, typically in a "list view" API.
        """
        return data

    def get_deep_data(self, data):
        """This method can be used to modify the 'data' dict that is stored
            in the backend record. The value that is returned from here will
            be displayed to the user, typically in a "detail view" API.
         """
        return self.get_shallow_data(data)

    def process_data(self, data):
        """Process user input before persisting to backend
        """
        return data
