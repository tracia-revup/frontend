from django.conf import settings
from django.contrib.auth import decorators
from django.http import QueryDict
from django.shortcuts import render


class ContextOverrideMixin(object):
    context_overrides = {}

    def render(self, template_name, dictionary=None,
                           context_instance=None, dirs=None, *args, **kwargs):
        """A wrapper around render to allow for injecting context.

        Views should inherit this class and call self.render,
        instead of the Django shortcut.
        To activate context substitution, two things are needed:
           1) settings.DEBUG = True
           2) GET argument: ?override_context=true (or 1)
        """
        override = 'override_context' in self.request.GET
        # Check if we are eligible for context substitution
        if settings.DEBUG and override and self.context_overrides:
            # Inject any context variables
            dictionary.update(self.context_overrides)

        return render(template_name, dictionary, context_instance,
                                  dirs, *args, **kwargs)


def build_protocol(request=None):
    if request:
        secure = request.is_secure()
    else:
        secure = bool(settings.SECURE_PROXY_SSL_HEADER)
    return "http{}".format('s' if secure else '')


def build_protocol_host(request=None):
    if request:
        domain = request.get_host()
    else:
        domain = settings.DEFAULT_DOMAIN
    # Build the protocol and host string. E.g. https://www.revup.com
    return "{}://{}".format(build_protocol(request), domain)


def user_passes_test(test_func, login_url=None):
    """This is little util to redirect permission failures to "index"
    instead of the login page.
    """
    return decorators.user_passes_test(test_func,
                                       login_url=login_url or "/")


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip
