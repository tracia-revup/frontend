import string

from factory.fuzzy import BaseFuzzyAttribute


class FuzzyEmail(BaseFuzzyAttribute):
    """Generate a random email address.

    This should be used with factory_boy Factories as a field type.
    See libs.string_utils for more information about the randomization.
    """
    def __init__(self, user_length=16, domain_length=8, domain=None, **kwargs):
        super(FuzzyEmail, self).__init__(**kwargs)
        self.user_length = user_length
        self.domain_length = domain_length
        self.domain = domain

    def fuzz(self):
        from .string_utils import random_email
        return random_email(self.user_length, self.domain_length, self.domain)


class FuzzyPhone(BaseFuzzyAttribute):
    """Generate a random email address.

    This should be used with factory_boy Factories as a field type.
    """
    def __init__(self, number_length=10):
        self.number_length = number_length

    def fuzz(self):
        from .string_utils import random_string
        return random_string(length=self.number_length,
                             choices=string.digits)


class M2MMixin(object):
    @classmethod
    def _m2m(cls, create, extracted, queryset):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of objects were passed in, use them
            queryset.add(*extracted)