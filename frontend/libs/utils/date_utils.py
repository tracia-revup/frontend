import datetime
import calendar


def add_months(start_date, months):
    """Utility to add months to a given date in a non-naive way.
    Months that don't have a certain day (e.g. 31) will be rounded down.

    :param start_date: A date or datetime object to start from
    :param months: An integer number of months to add to start_date

    E.g.:
    >>> start_date = datetime.date(2018, 11, 30)

    >>> add_months(start_date, 3)
    datetime.date(2019, 2, 28)

    >>> add_months(start_date, 15)
    datetime.date(2020, 2, 29)

    This code was taken from stackoverflow:
    https://stackoverflow.com/a/4131114/4877075
    """
    month = start_date.month - 1 + months
    year = start_date.year + month // 12
    month = month % 12 + 1
    day = min(start_date.day, calendar.monthrange(year, month)[1])

    if isinstance(start_date, datetime.datetime):
        return datetime.datetime(year, month, day, start_date.hour,
                                 start_date.minute, start_date.second)
    else:
        return datetime.date(year, month, day)


def next_first_of_month(current_date=None):
    """Given a current date, or today if no current date is given, return the
    date of the first of the next month.
    """
    if not current_date:
        current_date = datetime.datetime.today()

    return add_months(current_date, 1).replace(day=1)
