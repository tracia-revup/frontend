import base64
from functools import reduce, wraps
import logging
from operator import attrgetter
import re
import traceback

from django import http
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.core.files.base import ContentFile
from django.db.models import Q
from django.http import QueryDict
from django.urls import reverse, NoReverseMatch
from django.utils.encoding import force_str
from rest_framework import (
    permissions, viewsets, serializers, fields, exceptions, renderers,
    response as drf_response)
from rest_framework.fields import empty
from rest_framework_extensions.cache.decorators import CacheResponse
from rest_framework_extensions.mixins import NestedViewSetMixin

from frontend.libs.utils.string_utils import random_uuid, str_to_bool
from frontend.libs.utils.general_utils import calc_cache_key


LOGGER = logging.getLogger(__name__)


class CRDModelViewSet(viewsets.mixins.CreateModelMixin,
                      viewsets.mixins.RetrieveModelMixin,
                      viewsets.mixins.DestroyModelMixin,
                      viewsets.mixins.ListModelMixin,
                      viewsets.GenericViewSet,):
    """A common ViewSet need is for CRUD operations, minus the "Update"."""
    pass


class CRUModelViewSet(viewsets.mixins.CreateModelMixin,
                       viewsets.mixins.RetrieveModelMixin,
                       viewsets.mixins.UpdateModelMixin,
                       viewsets.mixins.ListModelMixin,
                       viewsets.GenericViewSet,):
    """Another common ViewSet need is for CRUD operations, minus the "Delete".
    """
    pass


class RedirectViewSet(NestedViewSetMixin, viewsets.ViewSet):
    """A DRF viewset that provides a redirect on any api operation.

    The redirect target can be a url, a django url pattern name, or a DRF
    pattern base name. (ex: `user_contacts_api` as opposed to
    `user_contacts_api-detail`)

    `RedirectViewSet` has all the same configurable parameters as django's
    `RedirectView`: url, pattern_name, permanent, and query_string. It also
    includes the `get_redirect_url` method which is available for overriding in
    subclasses.

    See https://docs.djangoproject.com/en/1.7/ref/class-based-views/base/#redirectview
    for more details on the configuration parameters that match `RedirectView`.

    The only additional redirect-specific parameter that `RedirectViewSet` has
    is `base_pattern_name`. `base_pattern_name` is the value of base_name in
    the DRF router. `RedirectViewSet` will automagically append `-list` or
    `-detail` to the base_pattern_name based on whether an object primary key
    has been set.

    `RedirectViewSet` also supports everything that DRF's base ViewSet
    supports, such as: authentication_classes, throttle_classes, and
    permissions_classes.

    Example usage::

        class SeatContactsRedirectViewSet(RedirectViewSet):
            permanent = True
            base_pattern_name = 'user_contacts_api'
            query_string = True
            permission_classes = (And(permissions.IsAuthenticated,
                                      Or(IsOwner, IsAdminUser)),)

            def get_redirect_url(self, *args, **kwargs):
                seat_id = kwargs.pop(compose_parent_pk_kwarg_name('seat_id'))
                seat = get_object_or_404(Seat, pk=seat_id)
                kwargs[compose_parent_pk_kwarg_name('user_id')] = seat.user_id
                return super(SeatContactsRedirectViewSet, self)\
                        .get_redirect_url(*args, **kwargs)
    """
    url = None
    permanent = True
    base_pattern_name = None
    pattern_name = None
    query_string = False
    lookup_field = 'pk'

    def get_redirect_url(self, request=None, *args, **kwargs):
        """
        Return the URL redirect to. Keyword arguments from the
        URL pattern match generating the redirect request
        are provided as kwargs to this method.
        """
        url = None
        pattern_name = None

        if self.url:
            url = self.url % kwargs
        elif self.pattern_name:
            pattern_name = self.pattern_name
        elif self.base_pattern_name:
            if kwargs.get(self.lookup_field):
                pattern_name = self.base_pattern_name + '-detail'
            else:
                pattern_name = self.base_pattern_name + '-list'

        if pattern_name:
            try:
                url = reverse(pattern_name, args=args, kwargs=kwargs)
            except NoReverseMatch:
                LOGGER.exception(
                    "Error while converting pattern name '%s' into a url",
                    pattern_name)

        if request is not None:
            args = request.META.get('QUERY_STRING', '')
            if url and args and self.query_string:
                url = "%s?%s" % (url, args)
        return url

    def do_redirect(self, request, *args, **kwargs):
        url = self.get_redirect_url(request, *args, **kwargs)
        if url:
            if self.permanent:
                return http.HttpResponsePermanentRedirect(url)
            else:
                return http.HttpResponseRedirect(url)
        else:
            # XXX: Should we really do gone? maybe we should throw a 500
            # instead if reverse fails to find anything (since that is much
            # more likley to be a bug), and only return 410 Gone if an
            # attribute is explicitly set?
            return http.HttpResponseGone()

    def list(self, request, *args, **kwargs):
        return self.do_redirect(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        return self.do_redirect(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        return self.do_redirect(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        return self.do_redirect(request, *args, **kwargs)

    def partial_update(self, request, *args, **kwargs):
        return self.do_redirect(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        return self.do_redirect(request, *args, **kwargs)


class PartialUpdateMixin(viewsets.mixins.UpdateModelMixin):
    """Provides a boilerplate update method that makes all updates partial."""
    valid_update_fields = None

    def update(self, request, *args, **kwargs):
        # Force updates to be partial only, for safety reasons (even if it
        # is slightly less restful).
        kwargs["partial"] = True
        # If valid_update_fields is defined, only those fields may be updated.
        # valid_update_fields should contain field names from the serializer.
        if self.valid_update_fields is not None:
            invalid_fields = (set(request.data.keys()) -
                              set(self.valid_update_fields))
        else:
            invalid_fields = False

        if invalid_fields:
            raise exceptions.APIException(
                "Invalid fields in request: {}".format(
                    ", ".join(invalid_fields)))
        return super(PartialUpdateMixin, self).update(request, *args, **kwargs)


class IsAdminUser(permissions.IsAdminUser):
    def has_object_permission(self, request, view, obj):
        """Use the same rules for objects as for list views.

        The parent class always returns True, which is not correct.
        """
        return self.has_permission(request, view)

    def has_permission(self, request, view):
        """Add superuser to the has_permission permission.
        User can be either staff or superuser.
        """
        return request.user and request.user.is_admin


class IsOwner(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.

    Subclass this and override the constants below to extract and compare
    the user ids. Dot notation is supported.

    E.g. If 'obj' were a Contact, 'user_id' would contain the the user ID
    of the Contact's owner/importer.
    """
    OBJ_FIELD = 'user_id'
    REQUEST_FIELD = 'user.id'
    SAFE_METHODS = []

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in self.SAFE_METHODS:
            return True

        return (attrgetter(self.OBJ_FIELD)(obj) ==
                attrgetter(self.REQUEST_FIELD)(request))

    def has_permission(self, request, view, obj=None):
        """
        Check if the user is an owner of this query. Typically, this will be
        in the event of a sub-API. E.g. /users/<id>/contacts
        Subclasses may want to override this
        """
        api_parent = 'parent_lookup_{}'.format(self.OBJ_FIELD)
        if api_parent in view.kwargs:
            # Verify a the user in the url is the same as the one querying
            return (str(attrgetter(self.REQUEST_FIELD)(request)) ==
                    view.kwargs[api_parent])

        elif view.lookup_field in view.kwargs:
            # If a specific object is given, we'll let 'has_object_permission'
            # decide if the request has permission to view it.
            return True
        else:
            return False


class InitialMixin(object):
    """
    This mixin gives us access to the "initial" method, which is called
    from 'dispatch', so we don't have to override dispatch.
    Initial is where authentication, permissions, and throttling are done

    The intended purpose of this mixin is to give the programmer a place to
    initialize variables to be used throughout the view, including
    within the permissions, to reduce queries.
    See "PerformantPermissionMixin" for information on how to utilize it.
    """
    def _pre_permission_initial(self):
        """Override this method in your view.
        This method is called -before- authentication and permissions have
        been checked. Typically, you should only use this to initialize
        attributes used by a PerformantPermissionMixin permission.

        IMPORTANT NOTE: Use this method with caution. Only initialize
        attributes that are "safe" to query before authentication.
        """
        pass

    def _initial(self):
        """Override this method in your view.
        This method is called -after- authentication and permissions have
        been checked.

        This is where you should initialize instance variables that are not
        necessary for PerformantPermissionMixin.
        """
        pass

    def initial(self, request, *args, **kwargs):
        """Initial is where DRF sets up the request.

        IMPORTANT NOTE: Be careful using this method. 'initial' is where
           authentication or permissions are checked. Don't do anything here
           that could be used to cause harm by malicious requests.
        """
        # We want to call our _pre_permission_initial before super because
        # initial will do the permission checks
        self._pre_permission_initial()
        super(InitialMixin, self).initial(request, *args, **kwargs)
        # Use this to initialize attributes for use throughout the API.
        # We call this after super so all permission checks have already
        # completed.
        self._initial()


class PerformantPermissionMixin(object):
    """This is meant to be used with Permission classes.

    The intent is to allow us to query a field once in the view and
    read it from the view using 'view_instance_attribute', rather than
    query it again from every permission that uses it.
    """
    # Define this in your permission class. E.g. "seat"
    view_instance_attribute = None

    def _get_object(self, request, view):
        """Override this in your Permission class if your views using this
        permission won't always have the 'view_instance_attribute' defined.
        """
        raise NotImplementedError

    def get_object(self, request, view):
        """Checks the view for the 'view_instance_attribute', if the view
           does not have that attribute, this will attempt to call
           '_get_object' in the Permission class.
        """
        if self.view_instance_attribute and \
                hasattr(view, self.view_instance_attribute):
            return getattr(view, self.view_instance_attribute)
        else:
            try:
                return self._get_object(request, view)
            except NotImplementedError:
                raise ImproperlyConfigured(
                    "Permissions using this mixin must have either a "
                    "defined permission object, or a _get_object method")


class NameSearchMixin(object):
    """A mixin that adds helper methods for adding a name filter to the
    queryset.

    TODO: I would rather use a rest framework Filter, but a lot of code is
          fairly entrenched with this technique, and I can't fix them all
          right now. This will at least keep things DRY.
    """
    name_search_query_param = "search"
    name_search_prefix = ""

    def tokenize_query(self, filter_value):
        name_tokens = re.split('\s+', filter_value)
        name_tokens = list(filter(None, name_tokens))
        names = list()
        # If more than one name token, generate combination tuples
        if len(name_tokens) > 1:
            # Assume last token is meant as last name, use remaining as firsts
            for token in name_tokens[:-1]:
                names.append((token, name_tokens[-1]))
            # Assume first token is last name, use remaining as firsts
            for token in name_tokens[1:]:
                names.append((token, name_tokens[0]))
        # If only one name token, try it as first or last name
        elif name_tokens:
            names.append((name_tokens[0], ''))
            names.append(('', name_tokens[0]))
        return names

    def _prepare_name_filter(self, filter_value=None):
        # If filter_value is not provided, attempt to extract it
        if not filter_value:
            filter_value = self.request.query_params.get(
                self.name_search_query_param)

        if not filter_value:
            return Q()

        # Tokenize query on spaces
        names = self.tokenize_query(filter_value.lower())

        query = Q()
        prefix = self.name_search_prefix + "__" \
                 if self.name_search_prefix else ""

        if names:
            query = reduce(lambda q, name: q | Q(
                **{prefix + "first_name_lower__startswith": name[0].strip(),
                   prefix + "last_name_lower__startswith": name[1].strip()}),
                           names, query)
        return query


class ViewMethodChecker(permissions.BasePermission):
    def __init__(self, view_method_name):
        self.view_method_name = view_method_name

    def has_object_permission(self, request, view, obj):
        """Use the same rules for objects as for list views.
        """
        return self.has_permission(request, view)

    def has_permission(self, request, view):
        return getattr(view, self.view_method_name)(request)


class WritableMethodField(fields.Field):
    def __init__(self, read_method_name=None, write_method_name=None,
                 *args, **kwargs):
        # This gives us the actual object instead of the field value
        kwargs['source'] = '*'
        self.read_method_name = read_method_name
        self.write_method_name = write_method_name
        self._field_model = kwargs.pop('_field_model', None)
        super(WritableMethodField, self).__init__(*args, **kwargs)

    def bind(self, field_name, parent):
        super(WritableMethodField, self).bind(field_name, parent)
        # The method name should default to `get_{field_name}`.
        if self.read_method_name is None:
            self.read_method_name = 'get_{field_name}'.format(
                field_name=field_name)

        # Store the source attributes for later use (see `to_internal_value`)
        self._source_attrs = field_name.split('.')

    def to_internal_value(self, data):
        # This is a bit of a hack. If I don't set source = '*' up above, then
        # the read method receives the field value instead of the whole object,
        # which is not the behavior of SerializerMethodField. However, when
        # I set source='*', the write method no longer knows which field to
        # write to. This hack gives me both behaviors. See the `bind` method.
        self.source_attrs = self._source_attrs

        if self.write_method_name:
            data = getattr(self.parent, self.write_method_name)(data)
        return data
    
    def get_attribute(self, instance):
        # This is related to the note in `to_internal_value`, above. We need
        # to set the source_attrs back to an empty list before we call
        # `to_representation`, which is called from super.get_attribute
        self.source_attrs = []
        return super(WritableMethodField, self).get_attribute(instance)

    def to_representation(self, obj):
        value = getattr(self.parent, self.read_method_name)(obj)
        return value

    def run_validation(self, data=empty):
        """Sets serializer field to rest_framework's empty if data is None.
        This is necessary for the field to be ignored during validation even
        if required param is set to False
        """
        if self.field_name in ['dob', 'gender'] and data is None:
            data = empty
        return super().run_validation(data)


class exception_watcher(object):
    """Wrap API methods with this decorator to catch certain exception types
        and convert them into APIExceptions.

    Example:
        # This will convert ValueErrors and AttributeErrors into APIExceptions
         @error_watcher(ValueError, AttributeError)
         def get(self, request, *args, **kwargs): ...
    """
    def __init__(self, *args):
        self.watched_exceptions = args

    def __call__(self, func):
        @wraps(func)
        def wrapped(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except Exception as err:
                if isinstance(err, self.watched_exceptions):
                    raise exceptions.APIException(err)
                else:
                    raise err
        return wrapped


class AutoinjectUrlParamsIntoDataMixin(object):
    """This mixin is used to inject url parameters into request body.

    The use case for this mixin is to remove the need to specify fields in a
    json blob (for example) that are already specified in the url when creating
    or updating a database record.

    For example: ContactNotes requires a Contact id and an Account id. The api
    endpoint url to create a ContactNote is:
        `/rest/accounts/<account_id>/seats/<seat_id>/contacts/<contact_id>/notes/`

    It doesn't make sense to have to include contact_id and account_id when we
    can get these values from the url.

    A secondary purpose of this mixin is security related: fields like account
    and contact should not be editable, nor should they differ from the values
    found in the URL. This mixin enforces that by overriding any values found
    in the body with those extracted from the url.


    `AutoinjectUrlParamsIntoDataMixin` has three configuration parameters:

    `create_autoinject`
        enable autoinject when creating a new record

    `update_autoinject`
        enable autoinject when updating an existing record

    `autoinject_map`
        map of record key (defined in serializer) to parent query lookup key
        (defined in router)


    `AutoinjectUrlParamsIntoDataMixin` expects to be used with
    `NestedViewSetMixin` and `GenericViewSet`, or a `GenericViewSet` subclass.

    Example::

        class ContactNotesViewSet(AutoinjectUrlParamsIntoDataMixin,
                                  NestedViewSetMixin, viewsets.ModelViewSet):
            model = ContactNotes
            serializer_class = ContactNotesSerializer
            update_autoinject = False
            autoinject_map = {'account': 'account_id',
                              'contact': 'contact_id'}

    Example 2:
            autoinject_map = {'account': 'account_id',
                              'user': 'get_user_id'}

            def get_user_id(self):
                return self.request.user.id
    """
    create_autoinject = True
    update_autoinject = True
    autoinject_map = None

    def _do_autoinject(self, request, **kwargs):
        if self.autoinject_map is None:
            raise ImproperlyConfigured(
                "Autoinject is enabled, but autoinject_map on %s.%s "
                "is unconfigured" % (self.__module__, self.__class__.__name__))
        # inject params passed in through url into body
        for body_key, url_value in self._prepare_injection().items():
            if isinstance(request.data, QueryDict) and not request.data._mutable:
                request.data._mutable = True
                request.data[body_key] = url_value
                request.data._mutable = False
            else:
                request.data[body_key] = url_value
        return request

    def _prepare_injection(self):
        data = {}
        parent_kwargs = self.get_parents_query_dict()
        for body_key, url_key in self.autoinject_map.items():
            url_value = parent_kwargs.get(url_key)
            # Allows us to specify a view method to retrieve the value from.
            if url_value is None and hasattr(self, url_key):
                url_value = getattr(self, url_key)()
            data[body_key] = url_value
        return data

    def create(self, request, **kwargs):
        if self.create_autoinject:
            request = self._do_autoinject(request, **kwargs)
        return super(AutoinjectUrlParamsIntoDataMixin, self).create(request,
                                                                    **kwargs)

    def update(self, request, **kwargs):
        if self.update_autoinject:
            request = self._do_autoinject(request, **kwargs)
        return super(AutoinjectUrlParamsIntoDataMixin, self).update(request,
                                                                    **kwargs)


class Base64ImageField(serializers.ImageField):
    """Accepts a Base64 encoded image and converts it into Django's internal
       representation of an image file before passing it on to the model.
    """
    def __init__(self, *args, **kwargs):
        self.media_base = kwargs.pop("media_base", settings.MEDIA_URL)
        super(Base64ImageField, self).__init__(*args, **kwargs)

    def to_internal_value(self, data):
        """Convert the Base64 data into an image file. Generate a random
           filename using a uuid.
        """
        if isinstance(data, str) and data.startswith('data:image'):
            # E.g.: data:image/<img-type>;base64,<img-data>
            header, img_data = data.split(';base64,')
            # Guess the file extension
            ext = header.split('/')[-1]
            data = ContentFile(base64.b64decode(img_data),
                               name="{}.{}".format(random_uuid(), ext))
        return super(Base64ImageField, self).to_internal_value(data)

    def to_representation(self, value):
        """Display a path to the image, if the image is set. Otherwise
           return a blank string.
        """
        return ("".join((self.media_base, value.name))
                if value.name else "")


class CustomCacheResponse(CacheResponse):
    """Custom version of the drf_extensions CacheResponse decorator.

    Major changes:
    1) We only cache the response data, instead of the whole Response object.
       The Response can't be pickled in Django 1.7
    2) We only cache on successful requests (response 200)
    3) Override how the key is generated to better fit our needs
    4) Keep track of all caches in the namespace for invalidation purposes

    Per #4, we need to generate a namespace for this cache so it can be
    invalidated in a deterministic way. Since we hash the key for the request
    caches, we need to maintain an index of all the hashes, so we may
    invalidate them. Otherwise, the app will not be able to invalidate a
    request cache that doesn't match its own request exactly.
    """
    def __init__(self, *args, **kwargs):
        super(CustomCacheResponse, self).__init__(*args, **kwargs)
        # Override the key generator function with our own.
        self.calculate_key = self.key_calculator

    @classmethod
    def cache_namespace_calculator(cls, view_instance, view_method, user):
        """Create a cache key for the given view instance.
        This is so we can namespace this set of caches.
        """
        # `view_instance` may be instantiated or not, so we have to try
        # both places for the name of the class.
        if hasattr(view_instance, "__name__"):
            view_name = view_instance.__name__
        else:
            view_name = view_instance.__class__.__name__
        view_method = view_method.__name__
        user = user.id
        return "{}.{}.{}".format(view_name, view_method, user)

    @classmethod
    def key_calculator(cls, request, args, kwargs):
        """The following things will contribute to the cache key:
        1) URL arguments
        2) GET arguments
        3) POST arguments
        4) The current seat
        5) The seat's permissions. If view permissions change, we want to
           re-run the view code to make sure the user may still view the
           cached data.
        """
        params = dict(request.query_params)
        # We don't want the cache buster calculated in the key
        params.pop("cache", None)
        data = dict(request.data)
        seat = request.user.current_seat_id
        perms = request.user.current_seat.permissions if seat else []
        key = calc_cache_key(prefix="",
                             values=[params, data, seat, perms, args, kwargs])
        return key

    def process_cache_response(self, view_instance, view_method,
                               request, args, kwargs):
        """This is largely copy-and-pasted directly from the parent class.

        That was necessary for numbers 1, 2 and 4 in the class docstring.
        The changes to this method are all about cache management.
        """
        # This option will allow users to disable caching (or bust the cache).
        # This is mostly for debugging purposes.
        cache_opt = str_to_bool(request.query_params.get("cache", True))

        cache_namespace_key = self.cache_namespace_calculator(
            view_instance=view_instance,
            view_method=view_method,
            user=request.user,
        )
        key = self.calculate_key(
            request=request,
            args=args,
            kwargs=kwargs
        )
        # The cache key is a combination of namespace and request hash.
        cache_key = cache_namespace_key + key
        caches = self.cache.get_many([cache_namespace_key, cache_key],
                                     version=settings.PRODUCT_VERSION)

        response = caches.get(cache_key)
        cache_namespace = caches.get(cache_namespace_key, set())

        # If the cache key is not in the namespace, we have to assume this
        # particular response was invalidated. It will need to be re-cached.
        if not response or cache_key not in cache_namespace or not cache_opt:
            response = view_method(view_instance, request, *args, **kwargs)
            response = view_instance.finalize_response(request, response,
                                                       *args, **kwargs)
            # Should be rendered, before pickling while storing to cache
            response.render()

            # We don't want to cache anything that isn't a successful request
            if response.status_code != 200:
                return response
            else:
                response = response.data
                # Add the cache key to the cache index and save both.
                # Atomicity is only a minor problem here. Worst case scenario,
                # the user views two pages at once, and one page has its
                # cache invalidated.
                cache_namespace.add(cache_key)
                self.cache.set_many({cache_key: response,
                                     cache_namespace_key: cache_namespace},
                                    self.timeout,
                                    version=settings.PRODUCT_VERSION)

        return drf_response.Response(response)
cache_response = CustomCacheResponse


class HttpResponseLocked(http.HttpResponse):
    status_code = 423


class HttpResponseNoContent(http.HttpResponse):
    status_code = 204

    def __init__(self, content=b'', *args, **kwargs):
        # We're setting Content-Length to 0, so we need to enforce that.
        super().__init__(content=b'', *args, **kwargs)
        # Set the Content-Length header to 0. This is to resolve an issue with
        # Safari where it seems to hold the connection open, waiting for
        # content that will never come, if this is not specifically set to 0
        # https://stackoverflow.com/q/28684209
        self["Content-Length"] = 0


class APIException(exceptions.APIException):
    """Override APIException so the error is logged."""
    def __init__(self, logger, detail=None, request=None, log_detail=None):
        super(APIException, self).__init__(detail=detail)
        self.logger = logger
        self.request = request
        self.log_detail = log_detail
        # We only want to log a specific exception once.
        self._logged = False
        if not self._logged:
            self._log_error(self.detail)

    def _log_error(self, message):
        log_message = [
            "APIException: {}".format(message),
        ]
        if self.request:
            log_message.append(
                "{} {}".format(self.request.method,
                               force_str(self.request.get_full_path())))
        log_message.append("".join(traceback.format_stack(limit=10)))
        if self.log_detail:
            log_message.append(str(self.log_detail))
        self.logger.warning(force_str("\n".join(log_message)))
        self._logged = True


class PrintTemplateRenderer(renderers.TemplateHTMLRenderer):
    format = 'print'
