import datetime
import logging

from django.conf import settings
from django.core.cache import cache
import recurly

recurly.API_KEY = settings.RECURLY_PRIVATE_API_KEY
recurly.SUBDOMAIN = settings.RECURLY_SUBDOMAIN
recurly.DEFAULT_CURRENCY = "USD"
recurly.SOCKET_TIMEOUT_SECONDS = 5


objects_for_push_notification = recurly.objects_for_push_notification


LOGGER = logging.getLogger(__name__)


def list_subscription_plans():
    """Retrieves the list of plans from either the cache or the recurly API"""
    key = settings.GLOBAL_CACHE_KEYS.recurly_subscription_plans_cache
    plans_cache = cache.get(key, version=settings.PRODUCT_VERSION)

    # If the data in the cache is not expired, use it
    if plans_cache and plans_cache.get('timeout') > datetime.datetime.now():
        return plans_cache.get('data')
    else:
        try:
            plans = recurly.Plan.all()
        except Exception:
            LOGGER.exception("The request to get recurly subscription "
                             "plans failed.")
            # recurly failed with an exception - use the cached data
            if plans_cache:
                return plans_cache.get('data')
        else:
            # Extract the Plan fields we care about into a dict so it can
            # be cached. We get a pickling error if we don't do this.
            plans_data = [
                dict(
                    plan_code=p.plan_code,
                    unit_amount_in_cents=p.unit_amount_in_cents.currencies['USD'],
                    setup_fee_in_cents=p.setup_fee_in_cents.currencies['USD'],
                    name=p.name,
                )
                for p in plans
            ]
            # setting our own timeout in the data, instead of setting the
            # cache timeout so that we always have data available to use
            cache_data = dict(
                timeout=datetime.datetime.now() + datetime.timedelta(minutes=60),
                data=plans_data)
            cache.set(key, cache_data, version=settings.PRODUCT_VERSION)
            return plans_data
    return []


def create_account(account, cc_token=None, user=None):
    """Create an Account in Recurly.
     If a credit card token is supplied, the credit card will be added
    to the account.
    If a user is supplied, more detailed user identificatio information
    will be attached to the account.
    """
    r_account = recurly.Account(
        account_code=account.recurly_account_code,
        company_name=account.organization_name[:50],
    )
    if cc_token:
        r_account.billing_info = recurly.BillingInfo(token_id=cc_token)
    # If a user is supplied, we can add additional identification
    if user:
        r_account.email = user.email
        r_account.first_name = user.first_name
        r_account.last_name = user.last_name

    r_account.save()
    return r_account


def get_recurly_account(account_id):
    """Get a Recurly Account by ID."""
    try:
        return recurly.Account.get(account_id)
    except recurly.NotFoundError:
        return None


def get_billing_info(account):
    """
    Get Recurly billing info using the account.
    :param account:
    :return: billing dict
    """
    billing_info = account.billing_info
    billing_dict = {
        'first_name': billing_info.first_name,
        'last_name': billing_info.last_name,
        'address1': billing_info.address1,
        'city': billing_info.city,
        'state': billing_info.state,
        'zip': billing_info.zip,
        'country': billing_info.country,
        'phone': billing_info.phone,
        'year': billing_info.year,
        'month': billing_info.month,
        'last_four': billing_info.last_four
    }
    return billing_dict


def create_subscription(account, recurly_account, prorate_date=None):
    """Create a Recurly subscription.
     This will charge the user's account, so use with care.
     :param account: an Account model (E.g. PoliticalCampaign)
     :param recurly_account: A Recurly API Account object.
     :param prorate_date: The first renewal date for the subscription
    """
    product = account.account_profile.product
    data = dict(
        account=recurly_account,
        plan_code=product.recurly_plan_code,
        currency=recurly.DEFAULT_CURRENCY,
        quantity=1, #account.seat_count,
        starts_at=account.start_date,
    )
    if prorate_date:
        data['first_renewal_date'] = prorate_date

    subscription = recurly.Subscription(**data)
    subscription.save()
    return subscription


def get_subscription_plan(plan_code):
    """Get a Recurly Plan by code."""
    try:
        return recurly.Plan.get(plan_code)
    except recurly.NotFoundError:
        return None


def get_subscription(subscription_id):
    """Get a Recurly Subscription by ID."""
    try:
        return recurly.Subscription.get(subscription_id)
    except recurly.NotFoundError:
        return None


def get_revup_account(recurly_account_code):
    """Get revup account by recurly account code"""
    from frontend.apps.campaign.models.base import Account
    try:
        revup_account = Account.objects.get(recurly_account_code=recurly_account_code)
    except Account.DoesNotExist:
        return None
    return revup_account
