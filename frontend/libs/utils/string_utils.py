from difflib import SequenceMatcher
import random
import re
import string
import uuid

import inflection

DEFAULT_ENCODING = 'utf-8'


def random_string(length=16, choices=string.ascii_letters):
    """Generate a random string.

    >>> random_string(10)
    'vxnxikmhdc'
    >>> random_string(10)
    'ytqhdohksy'
    """
    return ''.join(random.choice(choices)
                   for i in range(length))


def random_email(user_length=16, domain_length=8, domain=None):
    """Generate a random email

    Odds are, the domain will not be a valid one, but the domain can
    be replaced with 'domain' if a valid domain is needed (might be
    useful if the app is checking MX records)
    """
    return "{}@{}".format(random_string(user_length),
                          domain or
                          "{}.com".format(random_string(domain_length)))


def str_to_bool(value, affirmatives=('1', 'true', 't', 'yes', 'y')):
    """Convert a string to a boolean value."""
    if not isinstance(value, (str, bytes)):
        return bool(value)

    if value.strip().lower() in affirmatives:
        return True
    else:
        return False


def random_uuid():
    """Generate a 32-byte uuid4 string. Should be secure for simple keys."""
    return uuid.uuid4().hex


def similarity(a, b):
    """Compare two strings.

    Will return a ratio of the similarity between the two strings.
    1.0 is a perfect match. 0.0 is no match.

    This function is case sensitive. E.g.:
    >>> similarity("john", "John")
    >>> 0.75
    >>> similarity("john", "john")
    >>> 1.0
    """
    return SequenceMatcher(None, a, b).ratio()


def increment_string(value, starting_index=1):
    """Add an increment to a string.

    E.g.: "Some string" -> "Some string (1)" -> "Some string (2)"
    """
    tokens = re.split("(^.*)\((\d+)\)\s*?$", value)
    if len(tokens) == 4:
        # If the regex matches, we get a set of tokens like:
        # ['', 'Some string', '1', '']
        value = tokens[1].rstrip()
        index = int(tokens[2]) + 1
    else:
        # If no match, we just get the string back:
        # ['Some string']
        value = tokens[0].rstrip()
        index = starting_index

    return "{} ({})".format(value, index)


def ensure_bytes(obj, encoding=None):
    """
    Convert ``obj`` to a byte string, if it isn't one already.

    The conversion method is as follows:

    1. If ``obj`` is a byte string, return it unchanged.
    2. If ``obj`` is an unicode string, encode it using ``encoding``.
    3. Otherwise, cast ``obj`` to unicode string and encode it using
       ``encoding``.

    If ``encoding`` is ``None``, the default encoding will be used.

    This function was copied from the project morphys:
    https://github.com/mkalinski/morphys
    """

    if isinstance(obj, bytes):
        return obj

    enc = encoding or DEFAULT_ENCODING #TODO: should this be changed to utf-8?

    if isinstance(obj, str):
        return obj.encode(enc)

    return str(obj).encode(enc)


def ensure_unicode(obj, encoding=('utf-8', 'cp1252')):
    """
    Convert ``obj`` to string, if it isn't one already.

    The conversion method is as follows:

    1. If ``obj`` is a string, return it unchanged.
    2. If ``obj`` is a byte string, decode it using ``encoding``.
    3. Otherwise, cat ``obj`` to string.

    If ``encoding`` is ``None``, the default encoding will be used.

    This function was copied from the project morphys:
    https://github.com/mkalinski/morphys
    """

    if isinstance(obj, str):
        return obj

    if isinstance(obj, bytes):
        if encoding is None:
            codecs_ = [DEFAULT_ENCODING]
        elif isinstance(encoding, str):
            codecs_ = [encoding]
        else:
            codecs_ = encoding
        result = None
        for codec_ in codecs_:
            try:
                return obj.decode(codec_)
            except UnicodeDecodeError as err:
                result = err
                continue
        if result is not None:
            raise result

    return str(obj)


_punctuation_to_space_table = dict((ord(char), ord(' '))
                                   for char in string.punctuation)
remove_punctuation_table = dict((ord(char), None)
                                for char in string.punctuation)
_scrub_table = dict((ord(char), None)
                    for char in string.punctuation+string.whitespace)
all_chars = [chr(i) for i in range(128)]
unprintable = set(all_chars).difference(set(string.printable))
unprintable_table = dict((ord(c), None) for c in unprintable)
vowels_lowercase = set('aeiou')
consonants_lowercase = set(string.ascii_lowercase).difference(vowels_lowercase)
numbers_and_uppercase_regex = re.compile("[0-9A-Z]+")


def strip_punctuation(str_, translate_table=None):
    """Strips out punctuation and replace with whitespace"""
    if translate_table is None:
        translate_table = _punctuation_to_space_table
    return str(str_).translate(translate_table)


def clean_name(str_):
    """Removes punctuation from the string"""
    str_ = str(str_)
    return str_.translate(remove_punctuation_table).strip()


def scrub_and_lowercase(str_):
    """Strips out punctuation and whitespace characters by replacing with
    None and returns the string in lower case
    """
    return str(str_).translate(_scrub_table).lower()


def swapcase(str_):
    """Swaps the case of a string. The purpose of this method is to have a
    function handle where necessary
    """
    return str_.swapcase()


def words(str_, to_lower=True):
    """Strips punctuation from the string by replacing it with whitespace,
    converts the string into lowercase and splits the string into words
    """
    str_ = strip_punctuation(str_)
    if to_lower:
        str_ = str_.lower()
    return str_.split()


def scrub_non_printable_characters(str_):
    """Scrubs any non printable characters from the string"""
    return str_.translate(unprintable_table)


def scrub_string(str_, scrub_chars=None, keep_chars=None, replace_char=None):
    """Scrubs the characters in `scrub_chars` from the given string `str_` and
    replaces it with `replace_char`. Scrubs any chars that are not in
    keep_chars and replaces them with `replace_char`
    """
    scrub_table = {}
    replacable_chars = set(str_).difference(keep_chars)
    # Add the characters that should be replaced to the `scrub_table`
    if replacable_chars:
        scrub_table.update(dict((ord(char), replace_char)
                                for char in replacable_chars))
    # Update the `scrub_table` with the characters specified in `scrub_chars`
    if scrub_chars:
        scrub_table.update(dict((ord(char), replace_char)
                                for char in scrub_chars))
    return str_.translate(scrub_table)


def initialism(str_):
    """Generates an initialism for a given string.

    "Initialism", commonly misnomered "acronym", is an abbreviation
    consisting of initial letters pronounced separately.

    This function will take a string and return its initials. If we encounter
    Non-ascii characters, we wil attempt to replace them with ascii characters.

    If the given string is already an initialism, we return the original
    string. This would be the case if the str_ is a single word in all caps or
    numbers. E.g.: "UPS", "3M"
    """
    clean_str = inflection.transliterate(scrub_non_printable_characters(str_))
    # Note: 'x' should never be null/empty, but I want to be defensive
    split_words = words(clean_str, False)
    # If there is only one word, we're going to check if it is already an
    # initialism/acronym.
    if len(split_words) == 1:
        if numbers_and_uppercase_regex.fullmatch(split_words[0]):
            split_words = list(split_words[0])

    return "".join(x[0] for x in split_words if x).upper()
