import json
import logging
import requests
from urllib.parse import urlencode

from django.conf import settings

ZENDESK_EMAIL = settings.ZENDESK_EMAIL
ZENDESK_PASSWORD = settings.ZENDESK_PASSWORD
ZENDESK_SUBDOMAIN = settings.ZENDESK_SUBDOMAIN
ZENDESK_HEADER = {'Content-Type': 'application/json'}
ZENDESK_BASE_URL = f'https://{ZENDESK_SUBDOMAIN}.zendesk.com/api/v2'

LOGGER = logging.getLogger(__name__)


def get_zendesk_url(entity, params=None, create_update=False):
    """Returns the url for querying the zendesk api"""
    url = f'{ZENDESK_BASE_URL}/{entity}'

    if create_update:
        url += '/create_or_update.json'
    else:
        url += '.json'

    if params:
        url += f'?{urlencode(params)}'

    return url


def get_zendesk_organization(name):
    """Checks to see if the organization exists in zendesk."""
    credentials = ZENDESK_EMAIL, ZENDESK_PASSWORD
    params = {
        'query': f'type:organization name:{name}'
    }
    url = get_zendesk_url('search', params=params)
    response = requests.get(url, auth=credentials)
    data = response.json()

    results = data.get('results')
    if results:
        result = results[0]
        return result.get('id')
    else:
        return None


def get_zendesk_user(email):
    """Checks to see if the user exists in zendesk."""
    credentials = ZENDESK_EMAIL, ZENDESK_PASSWORD
    params = {
        'query': f'type:user email:{email}'
    }
    url = get_zendesk_url('search', params=params)
    response = requests.get(url, auth=credentials)
    data = response.json()

    results = data.get('results')
    if results:
        result = results[0]
        return result.get('id')
    else:
        return None


def get_zendesk_ticket(subject, organization_name):
    """Check if the ticket already exists for the user/organization and has the
    status of open/new
    """
    params = {
        'query': f'type:ticket status:open subject:{subject} '
                 f'organization_name:{organization_name}'
    }
    url = get_zendesk_url('search', params=params)
    response = requests.get(url, auth=(ZENDESK_EMAIL, ZENDESK_PASSWORD))
    data = response.json()

    results = data.get('results')
    if results:
        result = results[0]
        return result.get('id')
    else:
        return None


def create_or_update_organization(data):
    """Create or update the organization in zendesk."""
    organization = {
        'organization': {'id': data.get('id'),
                         'name': data.get('organization_name'),
                         'organization_fields': data}
    }
    payload = json.dumps(organization)
    url = get_zendesk_url('organizations', create_update=True)
    response = requests.post(url, headers=ZENDESK_HEADER, data=payload,
                             auth=(ZENDESK_EMAIL, ZENDESK_PASSWORD))

    if response.status_code in [201, 200]:
        data = response.json()
        org_data = data.get('organization')
        if org_data:
            return org_data.get('id')
        else:
            LOGGER.error('Received a correct status code, however, no '
                         'organization data was returned in the response.')
    else:
        LOGGER.error('The create or update for organization was not '
                     'successful, status code returned was {}.  Attempting'
                     'to create organization_id: {} and organization_name: {} '
                     'The response was {}'.
                     format(response.status_code,
                            data.get('id'),
                            data.get('organization_name'),
                            response))

    return None


def create_or_update_user(data):
    """Creates or updates the user in zendesk."""
    user = {
        'user': {'name': data.get('full_name'),
                 'email': data.get('email'),
                 'organization': {'name': data.get('organization_name')},
                 'user_fields': data}
    }
    payload = json.dumps(user)
    url = get_zendesk_url('users', create_update=True)
    response = requests.post(url, headers=ZENDESK_HEADER, data=payload,
                             auth=(ZENDESK_EMAIL, ZENDESK_PASSWORD))

    if response.status_code in [201, 200]:
        data = response.json()
        user_data = data.get('user')
        if user_data:
            return user_data.get('id')
        else:
            LOGGER.error('Zendesk create or update user received a correct '
                         'status code, however, no user data was returned in '
                         'the response.  The full user name: {} and the '
                         'organization name: {}.'.
                         format(data.get('full_name'),
                                data.get('organization_name')))
    else:
        LOGGER.error('The create or update for user was not '
                     'successful, status code returned was {}.'
                     'Attempting to create user {} for organization_name: {}.'
                     'The response was {}'.
                     format(response.status_code,
                            data.get('full_name'),
                            data.get('organization_name'),
                            response))

    return None


def create_ticket(data):
    """Creates the new client ticket inside zendesk."""
    ticket = {
        'ticket': {'subject': data.get('subject'),
                   'requester_id': data.get('user'),
                   'type': data.get('type'),
                   'status': data.get('status'),
                   'organization_id': data.get('organization_id'),
                   'comment': {'body': data.get('description')}}
    }
    payload = json.dumps(ticket)
    url = get_zendesk_url('tickets')
    response = requests.post(url, headers=ZENDESK_HEADER, data=payload,
                             auth=(ZENDESK_EMAIL, ZENDESK_PASSWORD))

    if response.status_code == 201:
        data = response.json()
        ticket_data = data.get('ticket')
        if ticket_data:
            return ticket_data.get('id')
        else:
            LOGGER.error('Zendesk create ticket received a correct status '
                         'code, however, no ticket data was returned in the '
                         'response. The requester user id: {} and the '
                         'organization id: {}.'.
                         format(data.get('user'),
                                data.get('organization_id')))
    else:
        LOGGER.error('The create or update for organization was not '
                     'successful, status code returned was {}.'
                     'Attempting to create a ticket for organization id: {}'
                     'and response was {}'.
                     format(response.status_code,
                            data.get('organization_id'),
                            response))

    return None
