from functools import wraps
import inspect
import logging

from celery import current_task, states, exceptions

from .general_utils import update_progress as _update_progress


LOGGER = logging.getLogger(__name__)


# Build a set of all known celery exceptions, so they can be handled special
ALL_CELERY_EXCEPTIONS = tuple(
    e[1] for e in inspect.getmembers(exceptions, inspect.isclass)
    if issubclass(e[1], BaseException))


def update_progress(current, total):
    def _callback(percent):
        current_task.update_state(state=states.STARTED,
                                  meta={'percent': percent})
    _update_progress(_callback, total, current)


def calculate_step_size(start_percent, end_percent):
    """Derived from start_percent and end_percent using this formula:
    (start_percent * total + step_size * total) / total = end_percent"""
    if (end_percent <= start_percent):
        raise ValueError("Invalid step size")
    return (end_percent - start_percent) / 100.0


class CurrentTaskContextFilter:
    """Celery Current Task Log Context Injector

    Injects context about the currently running celery task into the log
    record. This allows us to tell which task a log message came from.

    The following information is what is injected:
        task_name: The name of the celery task
        task_id: The celery task id
        parent_id: The id of the task that immediately preceeded the current
            task (or None)
        root_id: The id of the first task in a multi-task chain.
    """
    def filter(self, record):
        if current_task and current_task.request:
            record.__dict__.update({
                'task_name': current_task.name,
                'task_id': current_task.request.id,
                'parent_id': current_task.request.parent_id or '',
                'root_id': current_task.request.root_id or '',
            })
        else:
            # Add null values if we aren't in a task to prevent the formatter
            # from crashing due to missing values.
            record.__dict__.update({
                'task_name': '',
                'task_id': '',
                'parent_id': '',
                'root_id': '',
            })
        return True


def get_task_record():
    from frontend.apps.authorize.models import RevUpTask
    if not current_task:
        return None
    try:
        task = RevUpTask.objects.get(
            task_id=current_task.request.id)
    except RevUpTask.DoesNotExist:
        # If no associated RevUpTask was found, the task has likely
        # expired and we should not run.
        LOGGER.warn("Could not find RevUpTask. Aborting.")
        return None

    if task.revoked or task.expired:
        LOGGER.debug("Task was cancelled. Aborting.")
        task.delete()
        return None
    else:
        return task


class log_and_dismiss_exception(object):
    """Wrap methods with this decorator to catch certain exception types and
    log the exception using exception-level logger so that the errors can be
    caught in sentry.
    """
    def __init__(self, log_exceptions, logger, default=None, *args, **kwargs):
        if not isinstance(log_exceptions, (list, tuple,)):
            log_exceptions = (log_exceptions,)
        self.log_exceptions = log_exceptions
        self.logger = logger
        self.default = default

    def __call__(self, func):
        @wraps(func)
        def wrapped(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except Exception as ex:
                if isinstance(ex, self.log_exceptions):
                    self.logger.exception(f"Dismissing exception: {ex}")
                    return self.default
                else:
                    raise ex
        return wrapped
