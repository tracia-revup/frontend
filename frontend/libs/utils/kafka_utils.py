import logging
import ssl

from django.conf import settings
from kafka import KafkaProducer

from .msgpack_utils import dumps

LOGGER = logging.getLogger(__name__)


def init_producer(serializer=dumps,
                  api_version_auto_timeout_ms=None):
    # Kafka causes us a lot of headaches in our dev machines, since many of
    # us don't have it setup. This is to quench those problems
    if not settings.ENABLE_KAFKA_PUBLISHING:
        import unittest.mock as mock
        LOGGER.warn("Mocking Kafka connection")
        return mock.MagicMock(
            # The real library returns a result object with a `failed` method
            # that can be used to determine if there was a failure in sending a
            # message to kafka. We need the output of the `failed` method to be
            # false so code doesn't think there was an error and blow up.
            **{'send.return_value.failed.return_value': False})

    if api_version_auto_timeout_ms is None:
        api_version_auto_timeout_ms = settings.BACKEND_KAFKA_API_VERSION_AUTO_TIMEOUT

    if settings.BACKEND_KAFKA_SECURITY_ENABLED:
        security_kwargs = {
            'security_protocol': 'SASL_SSL',
            'sasl_mechanism': 'PLAIN',
            'sasl_plain_username': settings.BACKEND_KAFKA_USERNAME,
            'sasl_plain_password': settings.BACKEND_KAFKA_PASSWORD,
            'ssl_context': ssl.create_default_context(),
        }
    else:
        security_kwargs = {}

    producer = KafkaProducer(
        value_serializer=serializer,
        retries=5,
        bootstrap_servers=settings.BACKEND_KAFKA_BOOTSTRAP_SERVERS,
        api_version_auto_timeout_ms=api_version_auto_timeout_ms,
        **security_kwargs
    )
    return producer
