
from ddtrace.sampler import RateSampler
from ddtrace.filters import FilterRequestsOnUrl


class RateSampleRequestsOnUrl(FilterRequestsOnUrl, RateSampler):
    def __init__(self, regexps, sample_rate=1):
        FilterRequestsOnUrl.__init__(self, regexps)
        RateSampler.__init__(self, sample_rate)

    def process_trace(self, trace):
        url_test_result = super().process_trace(trace)
        if url_test_result is None and not self.sample(trace):
            return
        return trace
