from collections import Iterable, Mapping
from copy import deepcopy
from datetime import datetime
import importlib
import logging

from audit_log.models import AuthStampedModel
from bson import ObjectId
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.aggregates.general import ArrayAgg as OrigArrayAgg
from django.contrib.postgres.fields import ArrayField
from django.db import models, transaction
from django.db.models.expressions import Func
from django.db.models.fields import Field, CharField
from django.db.models.functions import Length, Upper, Lower
from django.forms import MultipleChoiceField
from django.forms.models import model_to_dict
from django.utils.translation import ugettext_lazy
from django_extensions.db.models import TimeStampedModel, TitleDescriptionModel
import jsonfield.encoder

from .form_utils import USPhoneNumberField
from .general_utils import instance_cache


LOGGER = logging.getLogger(__name__)

models.CharField.register_lookup(Length)
models.CharField.register_lookup(Lower)
models.CharField.register_lookup(Upper)

DEFAULT_CLONE_EXCLUDE_FIELDS = (
    "id", "created", "modified", "created_by", "modified_by",
    "created_with_session_key", "modified_with_session_key",
)


def get_or_clone(model, locate_kwargs, base=None, base_locate_kwargs=None, **clone_kwargs):
    try:
        return model.objects.get(**locate_kwargs)
    except model.DoesNotExist:
        if not (base or base_locate_kwargs):
            raise ValueError("Required argument missing. Either `base` or "
                             "`base_locate_kwargs` must be supplied")
        elif not base:
            base = model.objects.get(**base_locate_kwargs)
        return clone_record(base, **clone_kwargs)


def get_record_data(record, m2m=False, exclude_fields=()):
    # Note: this implementation was written with Django 1.9. It will
    # probably need to be updated when we upgrade Django.
    if m2m:
        fields = record._meta.many_to_many
    else:
        fields = record._meta.fields
    return {f.name: getattr(record, f.name)
            for f in fields
            if f.name not in exclude_fields}


@transaction.atomic()
def clone_record(record, exclude_fields=None, deep_copy_fields=None,
                 append_suffix=None, **override_values):
    """Clone a db record for any django model.

    Supports:
    - Excluding specific fields from being copied into the new record (the
      default excluded fields are in the module variable
      `DEFAULT_CLONE_EXCLUDE_FIELDS`).
    - Using provided values in new record instead of those taken from the input
      record.
    - Deep copying child values. Values referred to by foreign key, as well as
      via ManyToMany relationships are both supported.
    - Providing custom arguments to nested `clone_record` calls when deep
      copying.
    - Appending a suffix to any field that has a string value.

    What is not supported:
    - Copying or deep-copying records that have a FK to the given record. In
      other words, if the only way to access the related record is via a
      reverse releated manager, then it won't be copied. ex: Running this
      function on a `Contact` record will not copy any email addresses, phone
      numbers, addresses, etc that have a FK to that contact.
    - `OneToOneField`s are also poorly supported at the moment. This was
      deliberatly skipped for the moment as it isn't necessary for our
      immediate goals. An error will be raised if the record being cloned has a
      required OneToOne field, and is not explicitly listed in
      `deep_copy_fields`. OneToOne fields that are not required and also not
      listed in `deep_copy_fields` are excluded from any processing.

    Arguments:
        `record`
            This is the record to clone.
            Type: django model

        `exclude_fields`
            Fields that should not be copied from the original record to the
            clone.
            Type: list, set, or tuple of strings

        `append_suffix`
            Suffixes that should be appended to fields, and the field name they
            go with.
            Note: This argument is not recursive. Fields specified only affect
            the top-level record being cloned.
            Type: Mapping of `field_name` to `suffix_string`

        `override_values`
            This is a kwargs that enables overriding arbitrary fields with a
            static value.

        `deep_copy_fields`
            Fields of type ManyToMany, OneToOne, or ForeignKey where the
            related record should be cloned instead of merely being assigned to
            the new copy.

            The most basic way to use this argument is to provide a list of the
            field names that should be cloned.

            More advanced usage is to use a mapping of field name to arguments
            that should be provided to `clone_record` when the records
            associated with that field are cloned. In this way it is possible
            to clone deeply nested relations.

            Example of cloning a FeatureConfig and associated SignalSetConfigs
            in a single call, and appending the client name to both:

                clone_record(
                    feat_config,
                    append_suffix={'title': ' - ClientA'},
                    deep_copy_fields={
                       'signal_set_configs': {
                           'deep_copy_fields': {
                               'signal_set_config': {
                                   'append_suffix': {'title': ' - ClientA'}
                               }
                           }
                       }
                    }
                )

            Verbose, yes, but it works. We can make it easier to use when we're
            not in a time crunch.

            See the unit tests for a more complex example.
    """
    # Fields that should not be copied. Many of these are auto-defined
    # and we don't want to override that.
    if exclude_fields is None:
        exclude_fields = set(DEFAULT_CLONE_EXCLUDE_FIELDS)
    if deep_copy_fields is None:
        deep_copy_fields = {}
    if append_suffix is None:
        append_suffix = {}
    elif not isinstance(append_suffix, Mapping):
        raise TypeError("The value for `append_suffix` is not a mapping type! "
                        "Recieved value of type `{}`".format(
                            type(append_suffix)))

    # For the sake of expediency, drop support for OneToOneFields. We don't use
    # them on any of our analysis config models anyway.
    # The way we drop support is by raising an error if any required OneToOne
    # fields are found (that aren't listed in deep_copy_fields), and droping
    # the referenced record in non-required OneToOne fields (that aren't listed
    # in deep_copy_fields).
    one_to_one_fields = [f for f in record._meta.fields
                         if isinstance(f, models.OneToOneField)]

    bad_one_to_one_fields = [f.name for f in one_to_one_fields
                             if (not f.blank and
                                 f.name not in deep_copy_fields)]
    if bad_one_to_one_fields:
        raise TypeError("Model has required OneToOne field(s) {} that wasn't "
                        "specified as a deep copy target.".format(
                            bad_one_to_one_fields))
    elif one_to_one_fields:
        exclude_fields.update([f.name for f in one_to_one_fields
                               if f.name not in deep_copy_fields])

    model = record._meta.model
    # Copy this model's data and create a config from it.
    data = get_record_data(record, m2m=False, exclude_fields=exclude_fields)
    data.update(override_values)
    for key in list(data.keys()):
        value = data[key]
        if value is None:
            continue
        elif key in deep_copy_fields:
            if not isinstance(value, models.Model):
                LOGGER.warn("Cannot deep copy value for key {} on model {}, "
                            "value is not a django model.".format(key, model))
            else:
                clone_kwargs = {}
                if isinstance(deep_copy_fields, Mapping):
                    dc_val = deep_copy_fields.get(key)
                    if dc_val and isinstance(dc_val, Mapping):
                        clone_kwargs = dc_val
                new_value = clone_record(value, **clone_kwargs)
                data[key] = new_value
        elif key in append_suffix:
            if not isinstance(value, str):
                LOGGER.warn("Cannot append suffix to value for field {} on "
                            "model {}. Value is not a string.".format(
                                key, model))
            else:
                data[key] = value + append_suffix[key]

    new_copy = model.objects.create(**data)

    # Migrate any m2m values over.
    for field, value_mgr in get_record_data(
            record, m2m=True, exclude_fields=exclude_fields).items():
        deep_copy = field in deep_copy_fields
        deep_copy_kwargs = {}
        # If deep_copy_fields is anything other than a Mapping, then the fields
        # it specifies to clone don't have any parameters to pass on to
        # recursive clone_record calls.
        if isinstance(deep_copy_fields, Mapping):
            dc_val = deep_copy_fields.get(field)
            # Ditto for dc_val
            if dc_val and isinstance(dc_val, Mapping):
                deep_copy_kwargs = deepcopy(dc_val)

        # If the through table was automatically created, then there are no
        # extra fields to copy, and we can use the relation manager.
        if value_mgr.through._meta.auto_created:
            values = value_mgr.all()
            if deep_copy:
                values = [clone_record(value, **deep_copy_kwargs)
                          for value in values]
            else:
                values = list(values)
            getattr(new_copy, field).set(values)
        else:
            # There's a custom through table, so we to create the through
            # records manually.
            through = value_mgr.through
            source_field_name = value_mgr.source_field_name
            values = through.objects.filter(**{source_field_name: record})
            # Update the record instance we just copied in the through record
            # with our new clone.
            clone_kwargs = {source_field_name: new_copy}
            # If the field being processed was specified as requiring
            # deep-copy, then we need to ensure that the record on the other
            # side of the through table is cloned as well.
            if deep_copy:
                target_field_name = value_mgr.target_field_name
                clone_kwargs.update(deep_copy_kwargs)
                dcf = clone_kwargs.get('deep_copy_fields')
                if dcf is None:
                    clone_kwargs['deep_copy_fields'] = [target_field_name]
                # If `target_field_name` is already in dcf, then there's
                # nothing further to do.
                elif target_field_name not in dcf:
                    if isinstance(dcf, Mapping):
                        dcf[target_field_name] = None
                    else:
                        dcf.append(target_field_name)

            for value in values:
                clone_record(value, **clone_kwargs)
    return new_copy


class ChoicesBase(type):
    def __new__(mcs, name, bases, attrs):
        try:
            choices_map = attrs.pop("choices_map")
            if not isinstance(choices_map, Iterable):
                raise ValueError("choices map must be an Iterable of tuples")
        except KeyError:
            # If the Choices class doesn't have a choices map, we should
            # try to inherit it from a parent
            choices_map = []
            for b in bases:
                # _raw_data is the parent's choices map. Inherit it.
                if issubclass(b, Choices) and b._raw_data:
                    choices_map = b._raw_data
                    break

        raw_data = []
        # Parse the choices map into usable pieces
        for item in choices_map:
            if len(item) == 3:
                value, attr, display = item
            elif len(item) == 2:
                value, attr = item
                display = attr
            else:
                raise ValueError("Choices Map must contain tuples of length "
                                 "2 or 3 of the format "
                                 "(value, attribute, [display])")
            raw_data.append((value, attr, display))
            attrs[attr] = value
        attrs["_raw_data"] = raw_data
        attrs["_translation"] = {value: (attr, display)
                                 for value, attr, display in raw_data}
        return super(ChoicesBase, mcs).__new__(mcs, name, bases, attrs)


class Choices(object, metaclass=ChoicesBase):
    """Simplifies the definition of choices states in a model.

    Subclasses should override the choices_map attribute with the following
    format:

    >> class States(Choices):
    >>     choices_map = [
    >>         ("PN", "PENDING", "Pending"),
    >>         ("AC", "ACTIVE"),
    >>     ]

    This automagic will allow you to do the following:

    >> States.PENDING
    >> "PN"
    >> States.translate(States.PENDING)
    >> "Pending"
    >> list(States.choices())
    >> [("PN", "Pending"), ("AC", "ACTIVE")]
    """

    choices_map = []

    @classmethod
    def choices(cls):
        # Return a generator comprehension of (value, display)
        return ((value, display) for value, attr, display in cls._raw_data)

    @classmethod
    def all(cls):
        return [value for value, attr, display in cls._raw_data]

    @classmethod
    def translate(cls, db_value, default=None):
        try:
            return cls._translation[db_value][1]
        except KeyError:
            return default

    @classmethod
    def attribute_name(cls, db_value):
        try:
            return cls._translation[db_value][0]
        except KeyError:
            return None


########## Model Fields ####################

class TruncatingCharField(models.CharField):
    """Automatically truncate a character field if it exceeds limit."""
    def get_prep_value(self, value):
        value = super(TruncatingCharField, self).get_prep_value(value)
        if value and len(value) > self.max_length:
            LOGGER.warn("Truncating field: '{} ({})'. Original: '{}'".format(
                self.name, self.model.__class__.__name__, value))
            return value[:self.max_length]
        return value


class ChoiceArrayField(ArrayField):
    """
    A field that allows us to store an array of choices.
    Uses Django's Postgres ArrayField
    and a MultipleChoiceField for its formfield.

    Copied this snippet from https://stackoverflow.com/a/39833588/4877075
    """
    def formfield(self, **kwargs):
        defaults = {
            'form_class': MultipleChoiceField,
            'choices': self.base_field.choices,
        }
        defaults.update(kwargs)
        # Skip our parent's formfield implementation completely as we don't
        # care for it.
        # pylint:disable=bad-super-call
        return super(ArrayField, self).formfield(**defaults)


class PhoneNumberField(CharField):

    description = ugettext_lazy("Phone number")

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 20
        super(PhoneNumberField, self).__init__(*args, **kwargs)

    def formfield(self, **kwargs):
        defaults = {'form_class': USPhoneNumberField}
        defaults.update(kwargs)
        return super(PhoneNumberField, self).formfield(**defaults)


############## Mixin Models ##################

class DynamicClassModelMixin(models.Model):
    """A mixin to provide dynamic class importing for a model instance.
    E.g.:
    module_name = "frontend.apps.<app_name>.<module_file>"
    class_name = "<DynamicClassName>"
    """
    module_name = models.CharField(max_length=256, blank=True)
    class_name = models.CharField(max_length=256, blank=True)

    class Meta:
        abstract = True

    @property
    def dynamic_class(self):
        if not hasattr(self, "_dynamic_class_cache"):
            self._dynamic_class_cache = self._build_class()
        return self._dynamic_class_cache

    @dynamic_class.setter
    def dynamic_class(self, cls):
        """Pass in a class or object to extract the module and class names
           and store them.
        """
        # If this is an object instance, we need to extract the class
        if not hasattr(cls, "__name__"):
            cls = cls.__class__

        self.module_name = cls.__module__
        self.class_name = cls.__name__
        self._dynamic_class_cache = cls

    def _build_class(self):
        """Reconstruct the class from the stored references. I.e. Import it."""
        # At least one of module and class must be defined
        if not (self.module_name or self.class_name):
            return None

        try:
            module = importlib.import_module(self.module_name) \
                         if self.module_name else None
            if module:
                cls = getattr(module, self.class_name)
            else:
                cls = globals()[self.class_name]
        except (ImportError, KeyError, AttributeError):
            # exception() will send a message and stack trace to the log
            LOGGER.exception("Failed to regenerate dynamic class")
            raise ValueError("No class named '{}'".format(
                ".".join([self.module_name, self.class_name])
            ))
        return cls

    # `instance_cache` is vital here because it ensure the -same-
    # `dynamic_instance` is retrieved, instead of a new one being created
    # every time this property is called, which defeats the purpose.
    @property
    @instance_cache
    def dynamic_instance(self):
        dc = self.dynamic_class
        try:
            return dc() if dc else None
        except TypeError:
            # The dynamic_class must have required arguments in the __init__,
            # and as a result we cannot provide a dynamic instance.
            return None


class ValidatingModel(models.Model):
    """Model abstract base class that runs the `full_clean()` method when the
    `save()` method is run.

    More info:
        http://stackoverflow.com/a/18876223/773976
        http://stackoverflow.com/q/4441539/773976
    """
    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.full_clean()
        return super(ValidatingModel, self).save()


class EntityModel(TimeStampedModel, AuthStampedModel):
    """Model abstract base class providing common fields and functionality for
    entity models
    """
    class Meta:
        abstract = True

    def __str__(self):
        if getattr(self, "title", None):
            return "{}: '{}'".format(self.title, self.pk)
        elif getattr(self, "name", None):
            return "{}: '{}'".format(self.name, self.pk)
        else:
            return "{}: '{}'".format(self.__class__.__name__, self.pk)


class OwnershipModel(models.Model):
    """Model abstract base class providing user and account ownership fields
    """
    account = models.ForeignKey('campaign.Account', blank=True, null=True,
                                related_name='+', on_delete=models.PROTECT)
    user = models.ForeignKey('authorize.RevUpUser', blank=True, null=True,
                             related_name='+', on_delete=models.PROTECT)

    class Meta:
        abstract = True


class ConfigModelBase(ValidatingModel, EntityModel, TitleDescriptionModel):
    """Model abstract base class providing common fields and functionality for
    configuration models.
    """
    class Meta:
        get_latest_by = 'modified'
        abstract = True

    def __str__(self):
        if self.title:
            return self.title
        else:
            return "{}: '{}'".format(self.__class__.__name__, self.pk)


class DynamicFormBase(DynamicClassModelMixin):
    """A DynamicFormBase gives us the ability to define a custom form and
    encapsulate its handlers.

    allow_multiple (bool): Whether the data for this form may have more than
            one instance. For example: An address dynamicform where the
            contact/user has multiple addresses.
    label: A label to display with the form. Often the title is not a
           label we would want to display, so we have this field also.
    """
    allow_multiple = models.BooleanField(default=False)
    label = models.CharField(max_length=255, blank=True)

    class Meta:
        abstract = True

    def get_fields(self):
        return self.dynamic_instance.get_fields()


class ModelDiffMixin(models.Model):
    """
    A model mixin that tracks model fields' values and provide some useful api
    to know what fields have been changed.

    If "mdm_watch_fields" is set, we only watch those fields for changes;
    otherwise, we watch all fields
    """
    class Meta:
        abstract = True

    mdm_watch_fields = None

    def __init__(self, *args, **kwargs):
        super(ModelDiffMixin, self).__init__(*args, **kwargs)
        self.__initial = self._dict

    @property
    def diff(self):
        d1 = self.__initial
        d2 = self._dict
        diffs = [(k, (v, d2[k])) for k, v in list(d1.items()) if v != d2[k]]
        return dict(diffs)

    @property
    def has_changed(self):
        return bool(self.diff)

    @property
    def changed_fields(self):
        return list(self.diff.keys())

    def get_field_diff(self, field_name):
        """
        Returns a diff for field if it's changed and None otherwise.
        """
        return self.diff.get(field_name, None)

    def save(self, *args, **kwargs):
        """
        Saves model and set initial state.
        """
        super(ModelDiffMixin, self).save(*args, **kwargs)
        self.__initial = self._dict

    @property
    def _dict(self):
        if self.mdm_watch_fields:
            fields = self.mdm_watch_fields
        else:
            fields = [field.name for field in self._meta.fields]
        return model_to_dict(self, fields=fields)


class ArchiveModelMixin(models.Model):
    """Mixin that provides archiving functionality to a model."""
    archived = models.DateTimeField(blank=True, null=True, default=None)

    class Meta:
        abstract = True

    def _archive_setter(self):
        """This can be overridden if datetime is not the appropriate field"""
        return datetime.now()

    def delete(self, using=None, keep_parents=False, force=False, **kwargs):
        """Override the delete method to archive instead."""
        if force:
            return super(ArchiveModelMixin, self).delete(
                using=using, keep_parents=keep_parents)
        elif not self.archived:
            self.archived = self._archive_setter()
            self.save(update_fields=["archived"])


class LockingModelMixin(object):
    def _get_locking_model_field(self):
        return "id"

    def _get_locking_model_id(self):
        return self.id

    def _get_locking_model_class(self):
        return self.__class__

    def lock(self):
        """Lock this model in the DB.
        This needs to be called from within a transaction.
        """
        return self._get_locking_model_class().objects.select_for_update().get(
            **{self._get_locking_model_field(): self._get_locking_model_id()})


class VirtualForeignObject(models.ForeignObject):
    def contribute_to_class(self, cls, name, **kwargs):
        # Force private_only to be true. This is necessary to ensure it does
        # not modify the database schema.
        kwargs['private_only'] = True
        super(VirtualForeignObject, self).contribute_to_class(cls, name,
                                                              **kwargs)


class NotEqual(models.Lookup):
    lookup_name = 'ne'

    def as_sql(self, qn, connection):
        lhs, lhs_params = self.process_lhs(qn, connection)
        rhs, rhs_params = self.process_rhs(qn, connection)
        params = lhs_params + rhs_params
        return '%s <> %s' % (lhs, rhs), params

Field.register_lookup(NotEqual)


class StartOf(models.Lookup):
    lookup_name = 'startof'

    def as_sql(self, qn, connection):
        lhs, lhs_params = self.process_lhs(qn, connection)
        rhs, rhs_params = self.process_rhs(qn, connection)
        params = lhs_params + rhs_params
        return '{lhs} = substr({rhs}, 1, length({lhs}))'.format(
            lhs=lhs, rhs=rhs), params

models.CharField.register_lookup(StartOf)


class ArrayAgg(OrigArrayAgg):
    template = '%(function)s(%(distinct)s%(expressions)s)'

    def __init__(self, col, distinct=False, **extra):
        super(ArrayAgg, self).__init__(
            col, distinct='DISTINCT ' if distinct else '', **extra)


class ARRAY(Func):
    function = 'ARRAY'
    template = '%(function)s[%(expressions)s]'


def custom_model_to_dict(instance, fields=None, exclude=None):
    """The regular 'model_to_dict' converts FK models into IDs.
    This function puts the original objects back in.
    """
    data = model_to_dict(instance, fields=fields, exclude=exclude)
    for field in instance._meta.get_fields():
        # If the field is a foreign key, put the model instance back
        if isinstance(field, models.ForeignKey):
            # We check data for the field in case it was excluded by the
            # 'include' and 'exclude' options
            if field.name in data:
                data[field.name] = getattr(instance, field.name)
    return data


class GenericForeignKeyQuerySet(models.QuerySet):
    def get_by_generic_object(self, obj):
        return self.filter_by_generic_obj(obj).get()

    def filter_by_generic_obj(self, obj):
        if obj:
            content_type = ContentType.objects.get_for_model(obj)
            obj_id = obj.id
        else:
            content_type = None
            obj_id = None
        return self.filter(**dict(_csource_content_type=content_type,
                                  _csource_id=obj_id))


class JSONEncoder(jsonfield.encoder.JSONEncoder):
    """A JSON encoder to add serialization support for custom types."""
    def default(self, obj):
        if isinstance(obj, ObjectId):
            return str(obj)
        return super(JSONEncoder, self).default(obj)



