from waffle.interface import flag_is_active, flag_is_active_for_user


class WaffleFlagsMixin(object):
    """Evaluate the waffle flags given in "flag_names" and return them
    in a dict from the method 'prepare_flags'
    """
    flag_names = []

    @classmethod
    def _waffle_method(cls):
        return flag_is_active

    @classmethod
    def _prepare_flags(cls, obj):
        return {flag_name: cls._waffle_method()(obj, flag_name)
                for flag_name in cls.flag_names}


CONTACT_IMPORT_FLAGS = ("Contact Import Name Expansion",
                        "Contact Import Alias Expansion",
                        "Contact Import Always Merge",
                        "Contact Import Backend Export",
                        "Contact Import Parallel Execute",
                        "Contact Import Merge Conflict Stacktrace",
                        "Contact Merge Separate Task",
                       )
ANALYSIS_FLAGS = ('Analysis Name Expansion',
                  'Analysis Alias Expansion',
                  'Analysis Parallelization',
                  'Analysis Zips Update',
                  'Analysis Verify Contribution Schema',
                  'FEC Use Daily Ids',
                  'Entity Analysis Enabled')


class UserFlags(WaffleFlagsMixin):
    """Utility class that prepares the analysis flags for a user."""
    flag_names = CONTACT_IMPORT_FLAGS + ANALYSIS_FLAGS

    def __init__(self, user):
        self.user = user

    @classmethod
    def _waffle_method(cls):
        return flag_is_active_for_user

    def prepare_flags(self):
        return self._prepare_flags(self.user)


class UserAnalysisFlags(UserFlags):
    """Utility class that prepares the analysis flags for a user."""
    flag_names = ANALYSIS_FLAGS


