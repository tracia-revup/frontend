import re

from django import forms
from django.contrib.postgres.forms import SimpleArrayField
from django.core.exceptions import ValidationError
from django.core.validators import EMPTY_VALUES
from django.forms.fields import CharField
from django.utils import six
from django.utils.encoding import smart_text
from django.utils.translation import string_concat, ugettext_lazy as _
from passwords.validators import ComplexityValidator
import requests


phone_digits_re = re.compile(r'^(?:1-?)?(\d{3})[-\.]?(\d{3})[-\.]?(\d{4})$')

class USPhoneNumberField(CharField):
    default_error_messages = {
        'invalid': _('Phone numbers must be in XXX-XXX-XXXX format.'),
    }

    def clean(self, value):
        super(USPhoneNumberField, self).clean(value)
        if value in EMPTY_VALUES:
            return ''
        value = re.sub('(\(|\)|\s+)', '', smart_text(value))
        m = phone_digits_re.search(value)
        if m:
            return '%s-%s-%s' % (m.group(1), m.group(2), m.group(3))
        raise ValidationError(self.error_messages['invalid'])


class CustomArrayWidget(forms.widgets.TextInput):
    def value_from_datadict(self, data, files, name):
        """CustomArrayField expects an array of data, not a string. Its
        default widget does not use 'getlist' to get the POST data, but
        uses 'get' instead, expecting a comma-delimited string.
        This does the opposite
        """
        return data.getlist(name, [])


class CustomArrayField(SimpleArrayField):
    """Retools the SimpleArrayField to expect a list instead of a
    comma-delimited string of values.
    """

    widget = CustomArrayWidget

    def prepare_value(self, value):
        """Override SAF to return a list of values instead of string."""
        if isinstance(value, list):
            return [six.text_type(self.base_field.prepare_value(v))
                    for v in value]
        return value

    def to_python(self, value):
        """This code is copied from SimpleArrayField.

        SimpleArrayField expects a comma-delimited string.
        CustomArrayField expects a list. This removes the string manipulations.
        """
        if not value:
            items = []
        else:
            items = value
        errors = []
        values = []
        for i, item in enumerate(items):
            try:
                values.append(self.base_field.to_python(item))
            except forms.ValidationError as e:
                for error in e.error_list:
                    errors.append(forms.ValidationError(
                        string_concat(self.error_messages['item_invalid'],
                                      error.message),
                        code='item_invalid',
                        params={'nth': i},
                    ))
        if errors:
            raise forms.ValidationError(errors)
        return values


class OrValidator(object):
    def __init__(self, *validators):
        self.validators = validators

    def __call__(self, value):
        if not self.validators:
            return

        errors = []
        for validator in self.validators:
            try:
                validator(value)
            except ValidationError as err:
                errors.append(err)
            else:
                return

        if errors:
            raise ValidationError("Either {}".format(
                ' OR '.join([e.message for e in errors])))


def verify_recaptcha(code, remote_ip):
    """
    When your users submit the form where you integrated reCAPTCHA, you'll get
     as part of the payload a string with the name "g-recaptcha-response".
     In order to check whether Google has verified that user,
     send a POST request with these parameters:
        URL: https://www.google.com/recaptcha/api/siteverify
        secret (required)	6LceMV0UAAAAAMMfSjJJ_iiKydouSdmDqObZ3hZa
        response (required)	The value of 'g-recaptcha-response'.
        remoteip	The end user's ip address.
    """
    data = dict(
        secret="6LceMV0UAAAAAMMfSjJJ_iiKydouSdmDqObZ3hZa",
        response=code,
        remote_ip=remote_ip)
    response = requests.post("https://www.google.com/recaptcha/api/siteverify",
                             data=data)
    return response.status_code == 200


class FlexibleComplexityValidator(ComplexityValidator):
    """This complexity validator allows us to specify multiple complexity
    options, and a number of which the user must meet.
    E.g. The user must have 3 out of 4 selected complexity requirements.

    Note: Unfortunately, much of this code had to be copy pasted from
            ComplexityValidator.
    """

    def __init__(self, complexities, required_count=None):
        """If required_count is not specified, then all options are required"""
        if complexities:
            # Filter out any '0' complexities because they are useless and
            # affect the length of the complexities structure
            complexities = {k: v for k, v in complexities.items() if v}
        super().__init__(complexities)

        num_complexities = len(self.complexities) if self.complexities else 0
        if required_count is None or required_count > num_complexities:
            required_count = num_complexities
        self.required_count = required_count

    def __call__(self, value):
        """Test the password value against the complexities."""
        if self.complexities is None:
            return

        uppercase, lowercase, letters = set(), set(), set()
        digits, special = set(), set()

        for character in value:
            if character.isupper():
                uppercase.add(character)
                letters.add(character)
            elif character.islower():
                lowercase.add(character)
                letters.add(character)
            elif character.isdigit():
                digits.add(character)
            elif not character.isspace():
                special.add(character)

        words = set(re.findall(r'\b\w+', value, re.UNICODE))

        errors = []
        if len(uppercase) < self.complexities.get("UPPER", 0):
            errors.append(
                _("%(UPPER)s or more unique uppercase characters") %
                self.complexities)
        if len(lowercase) < self.complexities.get("LOWER", 0):
            errors.append(
                _("%(LOWER)s or more unique lowercase characters") %
                self.complexities)
        if len(letters) < self.complexities.get("LETTERS", 0):
            errors.append(
                _("%(LETTERS)s or more unique letters") %
                self.complexities)
        if len(digits) < self.complexities.get("DIGITS", 0):
            errors.append(
                _("%(DIGITS)s or more unique digits") %
                self.complexities)
        if len(special) < self.complexities.get("SPECIAL", 0):
            errors.append(
                _("%(SPECIAL)s or more non unique special characters") %
                self.complexities)
        if len(words) < self.complexities.get("WORDS", 0):
            errors.append(
                _("%(WORDS)s or more unique words") %
                self.complexities)

        # Check if there are more errors than are allowed. The formula for
        # allowed errors is: num_complexities - required_count >= num_errors
        valid_items = len(self.complexities) - self.required_count - len(errors)
        if valid_items < 0:
            err_msg = _(f'must contain {abs(valid_items)} more of the following: ')
            raise ValidationError(self.message % (err_msg + ', '.join(errors),),
                                  code=self.code)
