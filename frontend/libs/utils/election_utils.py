
from datetime import date, timedelta
from enum import Enum, unique
from itertools import chain, starmap, repeat
from localflavor.us.us_states import US_STATES


DEFAULT_TERM_YEARS = 2


class GenericGetEnumMixin(object):
    @classmethod
    def get(kls, o):
        if isinstance(o, kls):
            return o
        if hasattr(kls, o):
            return kls[o]
        return kls(o)

@unique
class Office(GenericGetEnumMixin, Enum):
    House = 'H'
    Senate = 'S'
    President = 'P'

OfficeTerm = {Office.House: 2,
              Office.President: 4,
              Office.Senate: 6}

State = Enum('State', US_STATES, type=GenericGetEnumMixin)

# States that have elections when year % 6 == 0 or 2
zero_two = {State.MD, State.PA, State.WA, State.OH, State.UT, State.CA,
            State.ND, State.WI, State.NY, State.HI, State.VT, State.IN,
            State.AZ, State.FL, State.MO, State.NV, State.CT}

# States that have elections when year % 6 == 0 or 4
zero_four = {State.NH, State.CO, State.LA, State.AK, State.NC, State.AL,
             State.OR, State.KS, State.SC, State.AR, State.IL, State.GA,
             State.IA, State.OK, State.KY, State.ID, State.SD}

# States that have elections when year % 6 == 2 or 4
two_four = {State.ME, State.VA, State.NJ, State.MN, State.MA, State.NM,
            State.TX, State.MT, State.DE, State.MI, State.NE, State.TN,
            State.WV, State.MS, State.RI, State.WY}

# Map of state to tuple of year modulus when their election occurs
# ex: California's Senators were elected in 2010 (2010 % 6 == 0) and 2012
# (2012 % 6 == 2). The next time a California senate seat is up for exection is
# 2016 (2016 % 6 == 0), and then in 2018 (2016 % 6 == 2).
senate_moduli_map = dict(
    chain.from_iterable(
        starmap(zip, [(zero_four, repeat((0,4))),
                       (zero_two, repeat((0,2))),
                       (two_four, repeat((2,4)))])))


def _get_today():
    return date.today()


def _years_till_next_senate(input_mod, em):
    if input_mod < em[0]:
        till_next = em[0] - input_mod
    elif input_mod >= em[1]:
        till_next = (6 - input_mod) + em[0]
    elif input_mod >= em[0]:
        till_next = em[1] - input_mod
    return till_next


def _years_till_prev_senate(input_mod, em):
    if input_mod > em[1]:
        till_prev = input_mod - em[1]
    elif input_mod > em[0]:
        till_prev = input_mod - em[0]
    elif input_mod <= em[0]:
        till_prev = (6 - em[1]) + input_mod
    return till_prev


def _next_senate_election(input_date, state_election_moduli):
    input_mod = input_date.year % 6
    if input_mod in state_election_moduli:
        election = election_date_for_year(input_date)
        if election >= input_date:
            # next election later this year
            return election

    till_next = _years_till_next_senate(input_mod,
                                        sorted(state_election_moduli))

    election = election_date_for_year(input_date.year + till_next)
    return election


def _prev_senate_election(input_date, state_election_moduli):
    input_mod = input_date.year % 6
    if input_mod in state_election_moduli:
        election = election_date_for_year(input_date)
        if input_date > election:
            # prev election was earlier this year
            return election

    till_prev = _years_till_prev_senate(input_mod,
                                        sorted(state_election_moduli))

    election = election_date_for_year(input_date.year - till_prev)
    return election


def election_date_for_year(year):
    if isinstance(year, date):
        year = year.year
    nov_one = date(year, 11, 1)
    if nov_one.weekday() == 0:
        delta_days = 1
    else:
        delta_days = (6 - nov_one.weekday()) + 2

    delta = timedelta(days=delta_days)
    election = nov_one + delta
    return election


def next_election(term, input_date=None):
    """Get the next election from input_date.

    See 'prev_election' below for an "Important Note".
    """
    if input_date is None:
        input_date = _get_today()
    assert isinstance(input_date, date)
    year = input_date.year
    since_last = year % term
    if since_last > 0:
        year += term - since_last

    election = election_date_for_year(year)

    # make sure input date doesn't come after the calculated election date
    if input_date > election:
        return next_election(term, date(year + term, 1, 1))
    return election


def prev_election(term, input_date=None):
    """Get the previous election from input_date.

    Important Note: This function works perfectly well for 2-year cycles;
       however, 4-year and 6-year cycles have some ambiguity to them because
       we are unable to know if we are in the first half or the second half
       of the cycle.
       This algorithm uses a naive approach of a modulus, so it will
       always favor cycles that are divisible by the cycle length.
       For example: if it is year 2015 of a 6 year cycle, the returned
            election year will be 2010, not 2014 or 2012.

       The only real way to avoid this problem is to not use this algorithm
       at all for non-2-year cycles, and to keep a database of election dates
       for each non-2-year cycle.
    """
    if input_date is None:
        input_date = _get_today()
    assert isinstance(input_date, date)
    year = input_date.year
    since_last = year % term
    if since_last == 0:
        # If next election is this year, then input_date is sometime before
        # the election day of the current year
        if next_election(term, input_date).year == year:
            year -= term
    else:
        year -= since_last

    return election_date_for_year(year)


def next_default_election(input_date=None):
    return next_election(DEFAULT_TERM_YEARS, input_date)


def prev_default_election(input_date=None):
    """Get the previous election from today or the given input_date"""
    return prev_election(DEFAULT_TERM_YEARS, input_date)


def next_election_date_for_office(office, input_date=None, state=None):
    office = Office.get(office)
    if office is not Office.Senate:
        return next_election(OfficeTerm[office], input_date)
    else:
        if not state:
            raise ValueError('State must be set when office is Senate')
        else:
            if input_date is None:
                input_date = _get_today()
            state = State.get(state)
            year_moduli = senate_moduli_map[state]
            election = _next_senate_election(input_date, year_moduli)
            return election


def previous_election_date_for_office(office, input_date=None, state=None):
    office = Office.get(office)
    if office is not Office.Senate:
        next_election = next_election_date_for_office(office, input_date,
                                                      state)
        return election_date_for_year(
            date(next_election.year - OfficeTerm[office], 1, 1))
    else:
        if not state:
            raise ValueError('State must be set when office is Senate')
        else:
            if input_date is None:
                input_date = _get_today()
            state = State.get(state)
            year_moduli = senate_moduli_map[state]
            election = _prev_senate_election(input_date, year_moduli)
            return election


def election_period_for_office(office, input_date=None, state=None):
    start = previous_election_date_for_office(office, input_date, state)
    end = next_election_date_for_office(office, input_date, state) - \
            timedelta(days=1)
    return start, end


def contribution_limit(input_date):
    return 5400

