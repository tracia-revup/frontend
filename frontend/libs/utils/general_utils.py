from collections import Mapping, Set, Sequence
import csv
from functools import wraps
from hashlib import md5, sha1
from logging import getLogger
import sys
from types import GeneratorType

from ddtrace import tracer
from funcy import (join_with, iffy, partial, all, join, last, all_fn,
                   complement, isa, iterable, is_mapping, autocurry,
                   tap as _tap, any)

from frontend.libs.third_party.unionfind.unionfind import UnionFind
from frontend.libs.utils.string_utils import (ensure_unicode,
                                              scrub_non_printable_characters)

LOGGER = getLogger(__name__)

# Predicate to test value is a collection (or iterable), but not a string
is_collection = all_fn(complement(isa(str, bytes)), iterable)
# Curried version of tap. `tap` prints a value and then returns it. Useful to
# tap into a functional pipeline for debugging.
# By wrapping tap in autocurry, instead of having to do:
# `map(lambda x: tap(x, label='inside a map'), seq)`, we can do:
# `map(tap(label='inside a map'), seq)`
tap = autocurry(_tap)


class DictReaderInsensitive(csv.DictReader):
    """This class overrides the csv.fieldnames property.
    All fieldnames are without white space and in lower case
    """
    @property
    def fieldnames(self):
        if self._fieldnames is None:
            try:
                self._fieldnames = next(self.reader)
            except StopIteration:
                pass
        self.line_num = self.reader.line_num
        return [field.strip().lower() for field in self._fieldnames]

    def __next__(self):
        # Get the result from the original __next__, but store it in
        # DictInsensitive
        original = DictReaderInsensitive.__next__(self)

        # store all pairs from the old dict in the new, custom one
        insensitive = DictInsensitive(original)
        return insensitive


class DictInsensitive(dict):
    """This class overrides the __getitem__ method to automatically
        strip() and lower() the input key
    """
    def __getitem__(self, key):
        return dict.__getitem__(self, key.strip().lower())


def public(f):
    """A decorator to add the class/function to the __all__ definition
    This helps avoid retyping function/class names.

    *IMPORTANT NOTE*: Once you decorate one class in a module with this, you
        will need to decorate any others you want to be "public"

        "Public" just means they will be imported with the '*' notation

    E.g. The old way:
        > __all__ = ["SomeClass"]
        >
        > class SomeClass(object):  ...

    E.g. The new way:
        > @public
        > class SomeClass(object):  ...
    """
    all_ = sys.modules[f.__module__].__dict__.setdefault('__all__', [])
    if f.__name__ not in all_:  # Prevent duplicates if run from an IDE.
        all_.append(f.__name__)
    return f


class FunctionalBuffer(object):
    """A buffer that can be filled with objects to run a function on.

    If a `max_length` is defined, the function will be automatically
    executed when max_length objects have been added to the buffer.

    The processing function is called by the "flush" method, which may be
    called manually also. The buffer is cleared when it is flushed.

    If `max_length` is None, the buffer will not be automatically flushed.

    Example Usage:
    >>> f_buffer = FunctionalBuffer(FeatureResult.objects.bulk_create,
    >>>                             max_length=100)
    >>> f_buffer.push(FeatureResult(<some data>))
    """
    def __init__(self, processing_function, max_length=50):
        self._processing_function = processing_function
        if max_length is None:
            self._auto_flush = False
            self._max_length = None
        else:
            self._auto_flush = True
            self._max_length = int(max_length)
        self._buffer = []

    def __len__(self):
        return len(self._buffer)

    def push(self, *items):
        if items:
            self._buffer.extend(items)
        if self._auto_flush and len(self) >= self._max_length:
            self.flush()

    def flush(self):
        if self._buffer:
            self._processing_function(self._buffer)
            # Manually clean up the buffer, so we have no memory issues
            del self._buffer[:]


def instance_cache(f):
    '''Cache the result of a method on its class

    This is primarily meant to memoize getter methods with no arguments where
    the value returned by the getter will not change.

    The cache attribute name will be the name of the generating method with an
    underscore prefix. Ex: `zip3s()` will be stored in the attribute `_zip3s`

    Take special care with classmethods and methods with names already
    prefixed by an underscore.

    Note: This will not work on staticmethod.
    '''
    @wraps(f)
    def wrapper(self):
        cache_attr = '_' + f.__name__
        if hasattr(self, cache_attr):
            return getattr(self, cache_attr)
        result = f(self)
        setattr(self, cache_attr, result)
        return result
    return wrapper


def calc_cache_key(prefix, values):
    '''Generate cache key for a set of values by squashing all values together,
    and generating a md5sum

    Assumes the values have all already been normalized and deduped.
    '''
    if isinstance(values, str):
        values = [values]
    # Encode name to bytes and then get hash of it for use in cache key.
    # This is to prevent encoding issues from occuring within the cache
    # library.
    hasher = md5()
    for val in sorted(values):
        if not isinstance(val, str):
            val = str(val)
        hasher.update(val.encode('utf-8'))
    cache_key = '{}:{}'.format(prefix, hasher.hexdigest())
    return cache_key


def _flatten_data(data):
    """Flatten a nested data structure into a string representation

    This function flattens a nested data structure in a deterministic fashion.
    It was designed to generate ids compatible with ids generated by the csv
    contact importer. Dictionary keys are sorted, as are list values.

    Usage examples, and sample output:

        >>> _flatten_data(['c', 'd', 'a'])
        '(a,c,d)'

        >>> _flatten_data({'asd': ['c', 'd', 'a'], 'zxc': 1})
        '(asd,(a,c,d)),(zxc,1)'

        >>> _flatten_data({'one': {'asd': ['c', 'd', 'a'], 'zxc': 1},
                           'two': 'blarg'})
        '(one,(asd,(a,c,d)),(zxc,1)),(two,blarg)'
    """
    if isinstance(data, Mapping):
        result = []
        for key, value in sorted(data.items()):
            result.append("({},{})".format(ensure_unicode(key),
                                           _flatten_data(value)))
        result = ','.join(result)
    elif isinstance(data, (Set, Sequence)) and not isinstance(data,
                                                              str):
        result = []
        for item in data:
            result.append(_flatten_data(item))
        result = '(' + ','.join(sorted(result)) + ')'
    else:
        result = ensure_unicode(data if data is not None else '')
    return result


def deep_hash(data):
    """Generate a deterministic hash of a dictionary with a nested structure"""
    hasher = sha1()
    sdata = _flatten_data(data)
    # for compatability with hash_raw_contact
    sdata += ','
    hasher.update(sdata.encode('utf-8'))
    return hasher.hexdigest()


class BlankObject(object):
    """A Blank object to be able to store anything in.

    This is an arguably cleaner alternative to using a dict for things.
    It functions the same way, but instead of doing a['b'], you can
    do a.b
    """
    def __init__(self, **kwargs):
        for field, value in kwargs.items():
            setattr(self, field, value)


def update_progress(callback, total, current):
    """Track progress, running callback when progress percent changes"""
    if total != 0:
        # only save progress on new integer percent
        percent = int(100.0 * current / total)
        last_percent = int(100.0 * (current - 1) / total)
        if percent != last_percent:
            return callback(percent)


class Borg(object):
    __state = {}
    def __init__(self):
        self.__dict__ = self.__state


def flatten(col):
    for i in col:
        if isinstance(i, (Sequence, GeneratorType)):
            for j in flatten(i):
                yield j
        else:
            yield i


def split_at(col, n):
    if not isinstance(col, Sequence):
        col = list(col)
    return col[:n], col[n:]


@tracer.wrap()
def union_find_reducer(groups):
    """Performant method to merge sets that overlap

    This method uses a datastructure called Union-Find Disjoint Set which is
    designed for quickly locating sets that have an overlap, and merging all
    overlaping sets together.

    This datastructure makes merging large groups of contacts extremely quick,
    we went from 20+ minutes (sometimes much longer) to process ~174318 contact
    groups containing ~450,000 contacts with `set_reducer`, to about a minute
    using this function.

    Literature:
    https://www.hackerearth.com/practice/notes/disjoint-set-union-union-find/
    https://visualgo.net/en/ufds

    Source of library:
    https://github.com/deehzee/unionfind
    """
    uf = UnionFind()
    for group in groups:
        cnt = len(group)
        if cnt == 1:
            uf.add(*group)
        elif cnt == 2:
            uf.union(*group)
        else:
            (first,), rest = split_at(group, 1)
            for r in rest:
                uf.union(first, r)
    return uf.components(), uf


def set_reducer(groups):
    results = []
    for group in groups:
        similar = []
        for item in results:
            if not group.isdisjoint(item):
                similar.append(item)
        if len(similar) == 1:
            similar[0].update(group)
        elif similar:
            root, similar = similar[0], similar[1:]
            for s in similar:
                results.remove(s)
            root.update(group, *similar)
        else:
            results.append(group)
    return results


def retry_on_error(exc_type, retries):
    """Decorator to retry a method/function should a specific Exception occur

    Example usage:
        @retry_on_error(TransientNetworkError, 10)
        def network_task(self, blarg):
            ...
            # work

    The above example will re-run the method `network_task` up to 10 times
    should the exception `TransientNetworkError` be raised.
    """
    def wrapper(f):
        @wraps(f)
        def _inner(*args, **kwargs):
            last_error = None
            for _ in range(retries):
                try:
                    return f(*args, **kwargs)
                except exc_type as e:
                    LOGGER.exception("Retriable error occurred!")
                    last_error = e
            raise last_error
        return _inner
    return wrapper


def partition_into_batches(iterator, size):
    """Generator that partitions a collection into chunks of the specified
    size

    Parameters:
    iterator: a collection, iterator, or generator whose contents should be
    split.
    size: an integer of the size of the chunks

    Yields generators that emit the next `size` values from the input
    iterator, or until the input iterator is exhausted.
    """
    iterator = iter(iterator)

    def _inner(first):
        yield first
        for _ in range(size - 1):
            yield next(iterator)

    while 1:
        try:
            yield _inner(next(iterator))
        except StopIteration:
            break


def compare(a,b):
    return (a > b) - (a < b)


def dict_union(target, *args):
    """Merge multiple dictionaries together and return the result

    Without this function:
        >>> a = {'a': 1}
        >>> a.update({'b': 2})
        >>> a
        {'a': 1, 'b': 2}

    With this function:
        >>> dict_union({'a': 1}, {'b': 2})
        {'a': 1, 'b': 2}
    """
    for arg in args:
        target.update(arg)
    return target


def deepget(item, keys, default=None):
    """Retrieve value from deeply nested data structure

    Example usage:
        >>> deepget({'a': {'b': [{}, {'c': 4}]}}, ['a', 'b', 1, 'c'])
        4
    """
    try:
        return reduce(lambda x, y: x[y], keys, item)
    except (KeyError, IndexError, TypeError):
        return default


def deepcontains(item, *keys):
    """Containment check for nested data structures

    Example usage:
        >>> deepcontains({'a': {'b': [{}, {'c': 4}]}}, 'a', 'b', 1, 'c')
        True
    """
    assert keys
    for key in iter(keys):
        try:
            item = item[key]
        except (KeyError, IndexError, TypeError):
            return False
    return True


def deep_merge(*dicts, type_conflict_handler=None):
    """Merge multiple dicts combining values for the same key

    `deep_merge` expects each argument to be a separate dict.

    Nested dicts are merged recursively. Values that are lists, sets, tuples,
    or other collection type are joined together.

    For value types or combinations of value types where there is no obvious
    "correct" merge method, the last value is chosen by default. The
    functionality for this scenario can be customized by providing the kwarg
    `type_conflict_handler` with a callable. For example, set
    `type_conflict_handler=tuple` to keep all conflicting values.

    See the unit tests to see examples of how specific cases are handled.
    """
    return deep_join(dicts, type_conflict_handler=type_conflict_handler)


def deep_join(dicts, type_conflict_handler=None):
    """Merge multiple dicts combining values for the same key

    `deep_join` expects an iterable containing dicts.

    Nested dicts are merged recursively. Values that are lists, sets, tuples,
    or other collection type are joined together.

    For value types or combinations of value types where there is no obvious
    "correct" merge method, the last value is chosen by default. The
    functionality for this scenario can be customized by providing the kwarg
    `type_conflict_handler` with a callable. For example, set
    `type_conflict_handler=tuple` to keep all conflicting values.

    See the unit tests to see examples of how specific cases are handled.
    """
    if not type_conflict_handler:
        type_conflict_handler = last

    def _merge_handler(vals):
        if all(is_mapping, vals):
            # Recursively call `deep_join` if all values are dicts.
            return deep_join(vals, type_conflict_handler=type_conflict_handler)
        elif all(is_collection, vals) and not any(is_mapping, vals):
            # If all values are collections, and there are no strings or
            # Mapping types, concatenate the collection contents together. If
            # the collection types differ, the resulting type will match the
            # first.
            return join(vals)
        else:
            # When the the variation of types in `vals` is such that there is
            # no obvious way to merge, use configurable
            # `type_conflict_handler`.
            return type_conflict_handler(vals)

    # Combine input collections using the above defined functions
    return join_with(_merge_handler, dicts)


def clean_get_string(data, key, default=None):
    """Returns the required field from a dictionary after performing 'cleaning'
    operation i.e removes non printable characters from the field_name's value
    """
    field_val = data.get(key, default)
    if field_val is None or field_val == default:
        # Return the default value without further processing
        return default

    if not isinstance(field_val, str):
        field_val = str(field_val)

    return scrub_non_printable_characters(field_val)
