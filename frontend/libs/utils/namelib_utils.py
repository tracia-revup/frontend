from itertools import chain, cycle, repeat
import logging

from django.conf import settings
from django.core.cache import cache
from django.db import connections
from django.db.utils import ConnectionDoesNotExist

from frontend.libs.utils.general_utils import calc_cache_key

LOGGER = logging.getLogger(__name__)


class namelib(object):
    @classmethod
    def _prepare_name(cls, name):
        '''Prepare a name for peacock query by uppercasing it and convert into
        unicode
        '''
        if not isinstance(name, str):
            name = name.decode('utf8')
        name = name.strip().upper()
        return name

    @classmethod
    def _prepare_names(cls, names):
        '''Get the first/last name of all merged contacts

        Returns names already cleaned and in a tuple
        '''
        if isinstance(names, str):
            names = [names]
        elif not names:
            return None

        names = tuple(filter(None, set(map(cls._prepare_name, names))))
        # Check names again in case the cleaning removed all potential names
        return names

    @classmethod
    def family_name_ethnicity(cls, names):
        '''Get ethnicity for names from all last_names found for contact'''
        names = cls._prepare_names(names)
        if not names:
            return None

        cache_key = calc_cache_key('family_name_ethnicity', names)
        ethnicity = cache.get(cache_key)

        if ethnicity is None:
            try:
                connection = connections['peacock']
            except ConnectionDoesNotExist:
                if settings.ENV and settings.ENV != 'dev':
                    LOGGER.error("Peacock connection does not exist!")
                return None

            cols = ('white', 'black', 'hispanic', 'asian', 'native',
                    'multirace')

            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT DISTINCT {} FROM surname "
                    "WHERE name IN %(names)s ".format(', '.join(cols)),
                    {'names': names})

                ethnicity = dict(zip(cols, repeat(0)))
                # Merge all results into a single dict. If we have one last
                # name where the distribution is 90% white, and 3% asian, and
                # another last name where the distribution is 90% asian and 1%
                # white, the merged contact should take the max of each
                # ethnicity and we will end up with a dict of: 90% asian, 90%
                # white.
                for col, val in zip(cycle(cols), chain.from_iterable(cursor)):
                    ethnicity[col] = max(val, ethnicity.get(col, 0))

            cache.set(cache_key, ethnicity)

        return ethnicity

    @classmethod
    def given_name_origin(cls, names):
        '''Get the origin language for all given names of the contact'''
        names = cls._prepare_names(names)
        if not names:
            return None

        cache_key = calc_cache_key('given_name_origin', names)
        origin = cache.get(cache_key)

        if origin is None:
            try:
                connection = connections['peacock']
            except ConnectionDoesNotExist:
                if settings.ENV and settings.ENV != 'dev':
                    LOGGER.error("Peacock connection does not exist!")
                return None

            cols = ('english', 'afram', 'natam', 'spanish', 'basque',
                    'catalan', 'galacian', 'french', 'german', 'hindu',
                    'russian', 'persian', 'arabic', 'japanese', 'chinese',
                    'viet', 'korean', 'yiddish', 'hebrew', 'latin', 'greek')

            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT DISTINCT {} FROM gender "
                    "INNER JOIN g_usage ON (gender.usage = g_usage.uid) "
                    "WHERE name IN %(names)s "
                    "AND g_usage.notinuse <> 'X'".format(', '.join(cols)),
                    {'names': names})

                origin = dict(zip(cols, repeat(False)))
                # Merge all rows into a single dict. If we have one given name
                # that has an english origin, and another name with a spanish
                # origin, the merged dict should have True for both english and
                # spanish.
                for col, val in zip(cycle(cols), chain.from_iterable(cursor)):
                    # Any value that is not None, an empty string, or a string
                    # with a space is a True.
                    origin[col] |= (val not in ('', ' ', None))

            cache.set(cache_key, origin)

        return origin

    @classmethod
    def determine_gender(cls, names):
        '''Get the gender of the contact using first names of all merged
        contact records.

        We will prefer the gender from the usa_xard column because it prefers
        american names, then tries spanish, then other common languages, all
        the while filtering out archaic, rare unisex usages, and diminutive
        versions. If none of those matches, it falls back to the gender of that
        name across the entire world.

        Since we are searching using multiple names, it is possible to get more
        than one gender back. If this happens and we get M and U (for unisex)
        back, we will say the name is male. If we get back F and U, we say the
        name is female. If we get back M and F, we just say unisex. If we get
        nothing back, we say unisex.

        Possible returns from this method: 'M', 'F', 'U', None
        None is returned if the peacock database is not configured.
        '''
        names = cls._prepare_names(names)
        if not names:
            return None

        cache_key = calc_cache_key('determined_gender', names)
        gender = cache.get(cache_key)

        if gender is None:
            try:
                connection = connections['peacock']
            except ConnectionDoesNotExist:
                if settings.ENV and settings.ENV != 'dev':
                    LOGGER.error("Peacock connection does not exist!")
                return None

            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT DISTINCT usa_xard FROM gender "
                    "INNER JOIN g_usage ON (gender.usage = g_usage.uid) "
                    "WHERE name IN %(names)s "
                    "AND g_usage.notinuse <> 'X'",
                    {'names': names})
                genders = set(chain.from_iterable(cursor.fetchall()))
                if 'M' in genders and 'F' not in genders:
                    gender = 'M'
                elif 'F' in genders and 'M' not in genders:
                    gender = 'F'
                else:
                    gender = 'U'
            cache.set(cache_key, gender)

        return gender

    @classmethod
    def get_nicknames(cls, names, allow_empty_result=False):
        """Query Peacock data to locate nicknames and name variations for
        the provided names.

        Returns a list of lower-cased names in unicode if any names are found.
        If the name is not present in the peacock data, will return a list
        containing only the contact's lower-cased first name.

        If peacock database is unconfigured, will return None so code using
        this function will know to fall back to legacy search strategies.

        When allow_empty_result is False and no match is found for the names
        provided, the input names will be returned. When allow_empty_result is
        True, an empty list will be returned instead.
        """
        names = cls._prepare_names(names)
        if not names:
            return None

        cache_key = calc_cache_key('name_expansion', names)
        nicknames = cache.get(cache_key)
        if nicknames is False:
            if allow_empty_result:
                return []
            else:
                return [n.lower() for n in names]

        if nicknames is None:
            try:
                connection = connections['peacock']
            except ConnectionDoesNotExist:
                if settings.ENV and settings.ENV != 'dev':
                    LOGGER.error("Peacock connection does not exist!")
                return None

            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT DISTINCT name1, name2 FROM nicknames"
                    " INNER JOIN n_usage U1 ON (nicknames.usage1 = U1.uid)"
                    " INNER JOIN n_usage U2 ON (nicknames.usage2 = U2.uid)"
                    " WHERE (name1 IN %(names)s OR name2 IN %(names)s)"
                    " AND relflag IN ('1', '2', '3', '4')"
                    " AND reverse <> 'R'"
                    " AND english1 = 'E' AND english2 = 'E'"
                    " AND derived <> 'D'"
                    " AND U1.notinuse <> 'X'"
                    " AND U2.notinuse <> 'X'",
                    {'names': names})
                nicknames = [n.lower() for n in
                             set(chain.from_iterable(cursor.fetchall()))]
            if nicknames:
                cached_nicknames = nicknames
            else:
                # Store False in cache if we were unable to discover nicknames
                # for the provided names. This allows us to represent this
                # differently based on allow_empty_result without having to run
                # the query every time.
                cached_nicknames = False

            if not nicknames and allow_empty_result is False:
                nicknames = [n.lower() for n in names]

            cache.set(cache_key, cached_nicknames)
        return nicknames
