
from unittest.mock import patch

from django.conf.urls import url, include
from django.core.exceptions import ImproperlyConfigured
from django.http import HttpResponse
from django.test import override_settings
from django.urls import reverse, NoReverseMatch
from rest_framework import viewsets
from rest_framework.routers import SimpleRouter

from frontend.libs.utils.api_utils import (AutoinjectUrlParamsIntoDataMixin,
                                           RedirectViewSet, NameSearchMixin)
from frontend.libs.utils.test_utils import TestCase


class BasicViewSet(viewsets.ViewSet):
    def list(self, request, *args, **kwargs):
        return HttpResponse({'method': 'list'})

    def create(self, request, *args, **kwargs):
        return HttpResponse({'method': 'create'})

    def retrieve(self, request, *args, **kwargs):
        return HttpResponse({'method': 'retrieve'})

    def update(self, request, *args, **kwargs):
        return HttpResponse({'method': 'update'})

    def partial_update(self, request, *args, **kwargs):
        return HttpResponse({'method': 'partial_update'})

    def destroy(self, request, *args, **kwargs):
        return HttpResponse({'method': 'destroy'})


apirouter = SimpleRouter()
apirouter.register(r'basic', BasicViewSet, base_name="test_basic_api")


def null_view(request):
    return HttpResponse()

urlpatterns = [
    url(r'^null_view$', null_view, name='null_view'),
    url(r'^', include(apirouter.urls)),
]


class AutoinjectUrlParamsIntoDataMixinTests(TestCase):
    def test_error_when_improperly_configured(self):
        # instantiate mixin
        mixin = AutoinjectUrlParamsIntoDataMixin()

        # Ensure ImproperlyConfigured exception is raised
        with self.assertRaises(ImproperlyConfigured):
            mixin._do_autoinject(None)

    @patch('frontend.libs.utils.api_utils.AutoinjectUrlParamsIntoDataMixin._do_autoinject')
    def test_update_injection(self, autoinjectmock):
        mixin = AutoinjectUrlParamsIntoDataMixin()
        test_request = object()

        # Ensure _do_autoinject is not run when update_autoinject is set to
        # False
        mixin.update_autoinject = False
        with patch('builtins.super'):
            mixin.update(test_request)
        self.assertFalse(autoinjectmock.called)

        # Ensure _do_autoinject is run when update_autoinject is set to True
        mixin.update_autoinject = True
        with patch('builtins.super'):
            mixin.update(test_request)
        autoinjectmock.assert_called_once_with(test_request)

    @patch('frontend.libs.utils.api_utils.AutoinjectUrlParamsIntoDataMixin._do_autoinject')
    def test_create_injection(self, autoinjectmock):
        mixin = AutoinjectUrlParamsIntoDataMixin()
        test_request = object()

        # Ensure _do_autoinject is not run when create_autoinject is set to
        # False
        mixin.create_autoinject = False
        with patch('builtins.super'):
            mixin.create(test_request)
        self.assertFalse(autoinjectmock.called)

        # Ensure _do_autoinject is run when create_autoinject is set to True
        mixin.create_autoinject = True
        with patch('builtins.super'):
            mixin.create(test_request)
        autoinjectmock.assert_called_once_with(test_request)

    def test_injection(self):
        class test_request(object):
            def __init__(self, data=None):
                self.data = data or {}

        mixin = AutoinjectUrlParamsIntoDataMixin()
        mixin.autoinject_map = {'account': 'account_id',
                                'contact': 'contact_id'}

        # Ensure fields in request.data are populated with the proper values
        # from the URL even if the client didn't populate them.
        # all url keys from map are present in kwargs
        url_param_map = {'account_id': 1, 'contact_id': 2}
        with patch.object(mixin, 'get_parents_query_dict', create=True,
                          return_value=url_param_map):
            # request.data is empty
            request = mixin._do_autoinject(test_request())
        self.assertEqual(request.data, {'account': 1, 'contact': 2})

        # test 2
        # Ensure fields in request.data are populated with the proper values
        # from the URL even if the client submitted wrong values for the
        # fields.
        # all url keys from map are present in kwargs
        url_param_map = {'account_id': 3, 'contact_id': 4}
        # request.data has fields populated, but with values that are different
        # from what is in kwargs
        request_data = {'account': 'a', 'contact': 'b'}
        with patch.object(mixin, 'get_parents_query_dict', create=True,
                          return_value=url_param_map):
            request = mixin._do_autoinject(test_request(request_data.copy()))
        self.assertNotEqual(request.data, request_data)
        self.assertEqual(request.data, {'account': 3, 'contact': 4})

        # Prefer None to client-submitted data for fields specified in
        # autoinject_map. some url keys from map are present in kwargs
        url_param_map = {'account_id': 5}
        # request.data has fields populated, but with values that are different
        # from what is in kwargs
        request_data = {'account': 'a', 'contact': 'b'}
        with patch.object(mixin, 'get_parents_query_dict', create=True,
                          return_value=url_param_map):
            request = mixin._do_autoinject(test_request(request_data.copy()))
        # ensure request.data is good.
        self.assertNotEqual(request.data, request_data)
        self.assertEqual(request.data, {'account': 5, 'contact': None})


class RedirectViewSetTests(TestCase):
    urls = 'frontend.libs.utils.tests.test_api_utils'

    @classmethod
    def setUpClass(cls):
        cls._original_urlpatterns = urlpatterns[:]
        super(RedirectViewSetTests, cls).setUpClass()

    def setUp(self):
        class rvs(RedirectViewSet):
            permission_classes = []

        self.rvs_base_name = 'test_rvs'

        self.rvs = rvs
        self.router = SimpleRouter()
        self.router.register(r'rvs', rvs, base_name=self.rvs_base_name)

        urlpatterns.append(
            url(r'^RedirectViewSetTests/', include(self.router.urls))
        )

    def tearDown(self):
        urlpatterns[:] = self._original_urlpatterns

    def test_get_redirect_url(self):
        # instantiate RedirectViewSet
        rvs = RedirectViewSet()

        # set rvs.url to www.google.com
        rvs.url = 'http://www.google.com/'
        # set rvs.pattern_name to 'rvs_test_pattern_name' (resolves to: '/rvs/test/pattern/name')
        rvs.pattern_name = 'rvs_test_pattern_name'
        # set rvs.base_pattern_name to 'rvs_test_base_pattern_name' (resolves to: '/rvs_test_base_pattern_name/[<id>]')
        rvs.base_pattern_name = 'rvs_test_base_pattern_name'

        # Ensure rvs.url is preferred
        url = rvs.get_redirect_url()
        # ensure www.google.com is returned
        self.assertEqual(url, rvs.url)

        # Ensure rvs.pattern_name is preferred when rvs.url is unset
        rvs.url = None
        with patch('frontend.libs.utils.api_utils.reverse',
                   return_value='/rvs/test/pattern/name') as reversemock:
            url = rvs.get_redirect_url()
        # ensure '/rvs/test/pattern/name' is returned
        self.assertEqual(url, '/rvs/test/pattern/name')

        # Otherwise base_pattern_name is used.
        rvs.pattern_name = None
        # Ensure '-list' route is reversed on base_pattern_name if no primary
        # key is passed in
        with patch('frontend.libs.utils.api_utils.reverse',
                   return_value='/rvs_test_base_pattern_name/') as reversemock:
            url = rvs.get_redirect_url()
        reversemock.assert_called_once_with(rvs.base_pattern_name + '-list',
                                            args=(), kwargs={})
        # ensure '/rvs/test/base/pattern/name/' is returned
        self.assertEqual(url, '/rvs_test_base_pattern_name/')

        # Ensure '-detail' route is reversed on base_pattern_name if primary
        # key is passed in
        with patch('frontend.libs.utils.api_utils.reverse',
                   return_value='/rvs_test_base_pattern_name/1/') as reversemock:
            url = rvs.get_redirect_url(**{rvs.lookup_field: 1})
        reversemock.assert_called_once_with(rvs.base_pattern_name + '-detail',
                                            args=(),
                                            kwargs={rvs.lookup_field: 1})
        # Ensure '/rvs/test/base/pattern/name/<id>' is returned
        self.assertEqual(url, '/rvs_test_base_pattern_name/1/')


        # Ensure None is returned if no reverse match
        with patch('frontend.libs.utils.api_utils.reverse',
                   side_effect=NoReverseMatch("No work")) as reversemock:
            url = rvs.get_redirect_url(blarg=1)
        reversemock.assert_called_once_with(rvs.base_pattern_name + '-list',
                                            args=(), kwargs=dict(blarg=1))
        self.assertIsNone(url)

    @override_settings(ROOT_URLCONF=urls)
    def test_do_redirect_responses(self):
        self.rvs.pattern_name = 'null_view'
        redirect_target = reverse(self.rvs.pattern_name)
        testurl = reverse(self.rvs_base_name + '-list')

        # Ensure we get 301 status code if rvs.permanent is True
        self.rvs.permanent = True
        response = self.client.get(testurl, follow=False)
        self.assertRedirects(response, redirect_target, status_code=301,
                             fetch_redirect_response=False)

        # Ensure we get 302 status code if rvs.permanent is False
        self.rvs.permanent = False
        response = self.client.get(testurl, follow=False)
        self.assertRedirects(response, redirect_target, fetch_redirect_response=False)

        # Ensure we get a 410 Gone status code if get_redirect_url method returns None
        self.rvs.pattern_name = None
        response = self.client.get(testurl, follow=False)
        self.assertEqual(response.status_code, 410)

    @override_settings(ROOT_URLCONF=urls)
    def test_all_api_operations_redirect(self):
        list_url = reverse(self.rvs_base_name + '-list')
        detail_url = reverse(self.rvs_base_name + '-detail', args=(1,))

        self.rvs.url = redirect_target = 'http://www.google.com/'
        self.rvs.query_string = False
        self.rvs.permanent = False

        # test list
        response = self.client.get(list_url, follow=False)
        # ensure we get a 30X response
        self.assertRedirects(response, redirect_target, fetch_redirect_response=False)


        # test query strings are passed through
        query_string = '?asd=qwe&zxc=iuy'
        response = self.client.get(list_url + query_string, follow=False)
        # ensure we get a 30X response, and the target url DOES NOT contain query_string
        self.assertRedirects(response, redirect_target, fetch_redirect_response=False)

        self.rvs.query_string = True
        response = self.client.get(list_url + query_string, follow=False)
        # ensure we get a 30X response, and the target url DOES contains query_string
        self.assertRedirects(response, redirect_target + query_string, fetch_redirect_response=False)


        # test create
        response = self.client.post(list_url, follow=False)
        # ensure we get a 30X response
        self.assertRedirects(response, redirect_target, fetch_redirect_response=False)

        # test retrieve
        response = self.client.get(detail_url, follow=False)
        # ensure we get a 30X response
        self.assertRedirects(response, redirect_target, fetch_redirect_response=False)

        # test update
        response = self.client.put(detail_url, follow=False)
        # ensure we get a 30X response
        self.assertRedirects(response, redirect_target, fetch_redirect_response=False)

        # test destroy
        response = self.client.delete(detail_url, follow=False)
        # ensure we get a 30X response
        self.assertRedirects(response, redirect_target, fetch_redirect_response=False)


class TestNameSearchMixin(TestCase):
    def test_name_search_regular_name(self):
        # Test how a name is tokenized
        nsm = NameSearchMixin()

        # Test when the query_key has two tokens
        name_tokens = nsm.tokenize_query('Mc Obrien')
        self.assertTrue(any([x for x in name_tokens if "Mc" in x]))
        self.assertTrue(any([x for x in name_tokens if "Obrien" in x]))

        # Test when the query_key has two tokens and extra spaces
        name_tokens_2 = nsm.tokenize_query('Mc \t Obrien')
        self.assertTrue(any([x for x in name_tokens_2 if "Mc" in x]))
        self.assertTrue(any([x for x in name_tokens_2 if "Obrien" in x]))

        # Test when the filter_key has two tokens and special charcter
        name_tokens_3 = nsm.tokenize_query("Mc' O'brien")
        self.assertTrue(any([x for x in name_tokens_3 if "Mc'" in x]))
        self.assertTrue(any([x for x in name_tokens_3 if "O'brien" in x]))

        # Test when the filter_key has two tokens, special character in each token
        #   and extra spaces
        name_tokens_4 = nsm.tokenize_query("Mc' \t O'brien")
        self.assertTrue(any([x for x in name_tokens_4 if "Mc'" in x]))
        self.assertTrue(any([x for x in name_tokens_4 if "O'brien" in x]))

        # Test when the query_key has three tokens
        name_tokens = nsm.tokenize_query('Mc Donald Obrien')
        self.assertTrue(any([x for x in name_tokens if "Mc" in x]))
        self.assertTrue(any([x for x in name_tokens if "Donald" in x]))
        self.assertTrue(any([x for x in name_tokens if "Obrien" in x]))
