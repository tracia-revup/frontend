from frontend.libs.utils.csv_utils import csv_join, csv_split
from frontend.libs.utils.test_utils import TestCase


class CSVStringTests(TestCase):
    def setUp(self):
        self.items1 = ["Alpha", "Bravo", "Charlie", "Delta"]
        self.items2 = ["Alpha,Gamma", "Bravo", "Charlie,Echo", "Delta"]
        self.r1 = 'Alpha,Bravo,Charlie,Delta'
        self.r2 = 'Alpha|Bravo|Charlie|Delta'
        self.r3 = '"Alpha,Gamma",Bravo,"Charlie,Echo",Delta'
        self.r4 = "Alpha,Gamma|Bravo|Charlie,Echo|Delta"

    def test_csv_join(self):
        result = csv_join(self.items1)
        self.assertEqual(self.r1, result)
        result = csv_join(self.items1, delimiter='|')
        self.assertEqual(self.r2, result)

        result = csv_join(self.items2)
        self.assertEqual(self.r3, result)
        result = csv_join(self.items2, delimiter='|')
        self.assertEqual(self.r4, result)

    def test_csv_split(self):
        result = csv_split(self.r1)
        self.assertEqual(self.items1, result)
        result = csv_split(self.r2, delimiter='|')
        self.assertEqual(self.items1, result)

        result = csv_split(self.r3)
        self.assertEqual(self.items2, result)
        result = csv_split(self.r4, delimiter='|')
        self.assertEqual(self.items2, result)
