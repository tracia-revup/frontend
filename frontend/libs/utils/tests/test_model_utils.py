
from collections import Iterable, Counter
from re import escape as re_escape
import pytest

from django.db import models
import factory
from factory.fuzzy import FuzzyText, FuzzyInteger

from frontend.libs.utils.general_utils import dict_union
from frontend.libs.utils.model_utils import clone_record, DEFAULT_CLONE_EXCLUDE_FIELDS
from frontend.libs.utils.test_utils import TestCase


class LabeledBase(models.Model):
    class Meta:
        app_label = 'core'
        abstract = True
    label = models.CharField(max_length=30)

    def __str__(self):
        return u"{}: '{}'".format(self.label, self.id)


class OneToOneRemote(LabeledBase):
    pass


class OneToOneLocal(LabeledBase):
    oneto1 = models.OneToOneField(OneToOneRemote, blank =False, null=False,
                                  related_name='oneto1', on_delete=models.CASCADE)


class CopyTarget(LabeledBase):
    fk = models.ForeignKey('core.WillCopy1', on_delete=models.CASCADE, blank=True, null=True)
    oneto1 = models.OneToOneField('core.WillCopy2', blank=True, null=True, on_delete=models.CASCADE)
    plain_m2m = models.ManyToManyField('core.WillCopy3')
    custom_m2m = models.ManyToManyField('core.WillCopy4',
                                        through='core.CustomThrough')


class WontCopy(LabeledBase):
    fk = models.ForeignKey(CopyTarget, on_delete=models.CASCADE, related_name='wont_copy_fk_set',
                           blank=True, null=True)
    oneto1 = models.OneToOneField(CopyTarget, related_name='wont_copy_1to1',
                                  blank=True, null=True, on_delete=models.CASCADE)
    m2m = models.ManyToManyField(CopyTarget, related_name='wont_copy_m2m_set')


class WillCopy1(LabeledBase):
    fk = models.ForeignKey('core.WillCopy3', on_delete=models.CASCADE)


class WillCopy2(LabeledBase):
    fk = models.ForeignKey('core.WillCopy1', on_delete=models.CASCADE)


class WillCopy3(LabeledBase):
    pass


class WillCopy4(LabeledBase):
    plain_m2m = models.ManyToManyField('core.WillCopy3')


class CustomThrough(LabeledBase):
    order = models.IntegerField()
    copy_target = models.ForeignKey(CopyTarget, on_delete=models.CASCADE)
    will_copy = models.ForeignKey(WillCopy4, on_delete=models.CASCADE)


class BaseFactory(factory.DjangoModelFactory):
    label = FuzzyText()


class CopyTargetFactory(BaseFactory):
    class Meta:
        model = CopyTarget

    class Params:
        empty = factory.Trait(
            fk=None,
            oneto1=None,
            plain_m2m=None,
            custom_m2m=None,
            plain_m2m__num=0,
            custom_m2m__num=0,
        )

    wont_copy_fk_set = factory.RelatedFactory(
        'frontend.libs.utils.tests.test_model_utils.WontCopyFactory', 'fk',
        empty=True)
    wont_copy_1to1 = factory.RelatedFactory(
        'frontend.libs.utils.tests.test_model_utils.WontCopyFactory', 'oneto1',
        empty=True)
    wont_copy_m2m_set = factory.RelatedFactory(
        'frontend.libs.utils.tests.test_model_utils.WontCopyFactory', 'm2m',
        empty=True)

    fk = factory.SubFactory(
        'frontend.libs.utils.tests.test_model_utils.WillCopy1Factory')
    oneto1 = factory.SubFactory(
        'frontend.libs.utils.tests.test_model_utils.WillCopy2Factory')
    plain_m2m__num = 3
    custom_m2m__num = 4

    @factory.post_generation
    def plain_m2m(self, create, extracted, num=0, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted is not None:
            if not isinstance(extracted, Iterable):
                extracted = [extracted]
            self.plain_m2m.add(*extracted)
        elif num:
            self.plain_m2m.add(*(WillCopy3Factory()
                                 for _ in range(num)))

    @factory.post_generation
    def custom_m2m(self, create, extracted, num=0, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted is not None:
            if not isinstance(extracted, Iterable):
                extracted = [extracted]
            for i, extract in enumerate(extracted):
                CustomThroughFactory(
                    copy_target=self,
                    will_copy=extract,
                    order=((i + 1) * 10))
        elif num:
            for i in range(num):
                CustomThroughFactory(
                    copy_target=self,
                    order=((i + 1) * 10))


class WontCopyFactory(BaseFactory):
    class Meta:
        model = WontCopy

    class Params:
        empty = factory.Trait(
            fk=None,
            oneto1=None,
            m2m=None,
            m2m__num=0,
        )

    fk = factory.SubFactory(CopyTargetFactory, empty=True)
    oneto1 = factory.SubFactory(CopyTargetFactory, empty=True)
    m2m__num = 2

    @factory.post_generation
    def m2m(self, create, extracted, num=0, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted is not None:
            if not isinstance(extracted, Iterable):
                extracted = [extracted]
            self.m2m.add(*extracted)
        elif num:
            self.m2m.add(*(CopyTargetFactory(empty=True)
                           for _ in range(num)))


class WillCopy1Factory(BaseFactory):
    class Meta:
        model = WillCopy1
    fk = factory.SubFactory(
        'frontend.libs.utils.tests.test_model_utils.WillCopy3Factory')


class WillCopy2Factory(BaseFactory):
    class Meta:
        model = WillCopy2
    fk = factory.SubFactory(WillCopy1Factory)


class WillCopy3Factory(BaseFactory):
    class Meta:
        model = WillCopy3


class WillCopy4Factory(BaseFactory):
    class Meta:
        model = WillCopy4

    plain_m2m__num = 4

    @factory.post_generation
    def plain_m2m(self, create, extracted, num=0, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted is not None:
            if not isinstance(extracted, Iterable):
                extracted = [extracted]
            self.plain_m2m.add(*extracted)
        elif num:
            self.plain_m2m.add(*(WillCopy3Factory()
                                 for _ in range(num)))


class CustomThroughFactory(BaseFactory):
    class Meta:
        model = CustomThrough

    order = FuzzyInteger(500)

    copy_target = factory.SubFactory(CopyTargetFactory, empty=True)
    will_copy = factory.SubFactory(WillCopy4Factory)


class OneToOneRemoteFactory(factory.DjangoModelFactory):
    class Meta:
        model = OneToOneRemote
    oneto1 = factory.RelatedFactory(
        'frontend.libs.utils.tests.test_model_utils.OneToOneLocalFactory',
        'oneto1')


class OneToOneLocalFactory(factory.DjangoModelFactory):
    class Meta:
        model = OneToOneLocal
    oneto1 = factory.SubFactory(OneToOneRemoteFactory, oneto1=None)


class CloneRecordTests(TestCase):
    def setUp(self):
        self.initial_num_copytarget = CopyTarget.objects.count()
        self.initial_num_wontcopy = WontCopy.objects.count()
        self.initial_num_willcopy1 = WillCopy1.objects.count()
        self.initial_num_willcopy2 = WillCopy2.objects.count()
        self.initial_num_willcopy3 = WillCopy3.objects.count()
        self.initial_num_willcopy4 = WillCopy4.objects.count()
        self.initial_num_customthrough = CustomThrough.objects.count()

        self.copy_target = CopyTargetFactory()

        self.expected_num_copytarget = self.initial_num_copytarget + 1
        self.expected_num_wontcopy = self.initial_num_wontcopy + 3
        self.expected_num_willcopy1 = self.initial_num_willcopy1 + 2
        self.expected_num_willcopy2 = self.initial_num_willcopy2 + 1
        self.expected_num_willcopy3 = (self.initial_num_willcopy3 +
                                       CopyTargetFactory.plain_m2m__num +
                                       (CopyTargetFactory.custom_m2m__num *
                                        WillCopy4Factory.plain_m2m__num) +
                                       # each WillCopy1 references a separate
                                       # WillCopy3 instance
                                       self.expected_num_willcopy1)
        self.expected_num_willcopy4 = self.initial_num_willcopy4 + CopyTargetFactory.custom_m2m__num
        self.expected_num_customthrough = self.initial_num_customthrough + CopyTargetFactory.custom_m2m__num

        self.control_num_copytarget = CopyTarget.objects.count()
        self.control_num_wontcopy = WontCopy.objects.count()
        self.control_num_willcopy1 = WillCopy1.objects.count()
        self.control_num_willcopy2 = WillCopy2.objects.count()
        self.control_num_willcopy3 = WillCopy3.objects.count()
        self.control_num_willcopy4 = WillCopy4.objects.count()
        self.control_num_customthrough = CustomThrough.objects.count()

    @classmethod
    def xform(cls, record):
        return Counter(record.items())

    def test_controls(self):
        # control tests
        assert CopyTarget.objects.all().count() == 1
        found_ct = CopyTarget.objects.get()
        assert found_ct.id == self.copy_target.id
        assert found_ct.label == self.copy_target.label

    def test_deep_deep_copy(self):
        assert self.expected_num_copytarget == \
                         self.control_num_copytarget
        assert self.expected_num_wontcopy == \
                         self.control_num_wontcopy
        assert self.expected_num_willcopy1 == \
                         self.control_num_willcopy1
        assert self.expected_num_willcopy2 == \
                         self.control_num_willcopy2
        assert self.expected_num_willcopy3 == \
                         self.control_num_willcopy3
        assert self.expected_num_willcopy4 == \
                         self.control_num_willcopy4
        assert self.expected_num_customthrough == \
                         self.control_num_customthrough

        initial_copytarget_ids = list(CopyTarget.objects.values_list(
            'id', flat=True))
        initial_wontcopy_ids = list(WontCopy.objects.values_list(
            'id', flat=True))
        initial_willcopy1_ids = list(WillCopy1.objects.values_list(
            'id', flat=True))
        initial_willcopy2_ids = list(WillCopy2.objects.values_list(
            'id', flat=True))
        initial_willcopy3_ids = list(WillCopy3.objects.values_list(
            'id', flat=True))
        initial_willcopy4_ids = list(WillCopy4.objects.values_list(
            'id', flat=True))
        initial_customthrough_ids = list(CustomThrough.objects.values_list(
            'id', flat=True))

        label_suffix = ' - Blarg'
        inject_append_suffix = {'append_suffix': {'label': label_suffix}}
        new_record = clone_record(
            self.copy_target,
            deep_copy_fields={
                # Add WontCopy reverse fields just to ensure that they don't
                # get cloned no matter what.
                'wont_copy_fk_set': None,
                'wont_copy_1to1': None,
                'wont_copy_m2m_set': None,
                # here fk is a WillCopy1 instance
                'fk': dict_union({
                    'deep_copy_fields': {
                        # here fk is a WillCopy3 instance on the WillCopy1
                        # parent
                        'fk': dict_union({'label': "New Hotness"},
                                         inject_append_suffix)},
                }, inject_append_suffix),
                # here oneto1 is a WillCopy2 instance
                'oneto1': dict_union({
                    'deep_copy_fields': {
                        # here fk is a WillCopy1 instance on the WillCopy2
                        # parent
                        'fk': dict_union({
                            'exclude_fields': (DEFAULT_CLONE_EXCLUDE_FIELDS +
                                               ('label',)),
                            'deep_copy_fields': {
                                # here `fk` is a WillCopy3 instance on the
                                # WillCopy1 parent
                                'fk': dict_union({'label': "Old and Busted"},
                                                 inject_append_suffix)},
                        }, inject_append_suffix),
                    },
                }, inject_append_suffix),
                # Here `plain_m2m` is a M2M to `WillCopy3`
                'plain_m2m': inject_append_suffix,
                # Here `plain_m2m` is the custom through table for the M2M to
                # `WillCopy4`
                'custom_m2m': dict_union({
                    'deep_copy_fields': {
                        # Here `will_copy` is the `WillCopy4` record in the M2M
                        # with CopyTarget, and `plain_m2m` is a M2M from
                        # WillCopy4 to WillCopy3
                        'will_copy': dict_union(
                            {'deep_copy_fields': ['plain_m2m']},
                            inject_append_suffix)}
                }, inject_append_suffix),
            },
            **inject_append_suffix
        )

        num_copytarget = CopyTarget.objects.count()
        num_wontcopy = WontCopy.objects.count()
        num_willcopy1 = WillCopy1.objects.count()
        num_willcopy2 = WillCopy2.objects.count()
        num_willcopy3 = WillCopy3.objects.count()
        num_willcopy4 = WillCopy4.objects.count()
        num_customthrough = CustomThrough.objects.count()

        # This time only the WontCopy records aren't cloned.
        assert self.expected_num_wontcopy == num_wontcopy

        # Since I specified every record in the deep_copy_fields argument,
        # every single record is doubled.
        assert self.expected_num_willcopy1 * 2 == num_willcopy1
        assert self.expected_num_willcopy2 * 2 == num_willcopy2
        assert self.expected_num_willcopy3 * 2 == num_willcopy3
        assert self.expected_num_willcopy4 * 2 == num_willcopy4
        assert self.expected_num_copytarget * 2 == num_copytarget
        assert self.expected_num_customthrough * 2 == num_customthrough

        copytarget_records = list(CopyTarget.objects.exclude(
            id__in=initial_copytarget_ids))
        wontcopy_records = list(WontCopy.objects.exclude(
            id__in=initial_wontcopy_ids))
        willcopy1_records = list(WillCopy1.objects.exclude(
            id__in=initial_willcopy1_ids))
        willcopy2_records = list(WillCopy2.objects.exclude(
            id__in=initial_willcopy2_ids))
        willcopy3_records = list(WillCopy3.objects.exclude(
            id__in=initial_willcopy3_ids))
        willcopy4_records = list(WillCopy4.objects.exclude(
            id__in=initial_willcopy4_ids))
        customthrough_records = list(CustomThrough.objects.exclude(
            id__in=initial_customthrough_ids))

        assert len(wontcopy_records) == 0
        assert all([record.label.endswith(label_suffix)
                    for record in customthrough_records])
        assert all([record.label.endswith(label_suffix)
                    for record in willcopy4_records])
        assert all([record.label.endswith(label_suffix)
                    for record in willcopy2_records])
        assert all([record.label.endswith(label_suffix)
                    for record in copytarget_records])
        assert len([r for r in willcopy1_records
                    if r.label == '']) == 1
        assert len([r for r in willcopy1_records
                    if r.label.endswith(label_suffix)]) == 1
        # All the new WillCopy3 records will have the new suffix except for
        # those associated with WillCopy4 since we didn't define it there.
        assert all([record.label.endswith(label_suffix)
                    for record in willcopy3_records
                    if not record.willcopy4_set.exists()])
        assert not any([record.label.endswith(label_suffix)
                        for record in willcopy3_records
                        if record.willcopy4_set.exists()])
        customized_labels = {"New Hotness - Blarg", "Old and Busted - Blarg"}
        assert customized_labels.intersection(
            {r.label for r in willcopy3_records}) == customized_labels

        assert set(new_record.custom_m2m.values_list(
            'id', flat=True)).isdisjoint(
                set(self.copy_target.custom_m2m.values_list('id', flat=True)))

        assert set(new_record.plain_m2m.values_list(
            'id', flat=True)).isdisjoint(
                set(self.copy_target.plain_m2m.values_list('id', flat=True)))

    def test_simple_copy(self):
        assert self.expected_num_copytarget == \
                         self.control_num_copytarget
        assert self.expected_num_wontcopy == \
                         self.control_num_wontcopy
        assert self.expected_num_willcopy1 == \
                         self.control_num_willcopy1
        assert self.expected_num_willcopy2 == \
                         self.control_num_willcopy2
        assert self.expected_num_willcopy3 == \
                         self.control_num_willcopy3
        assert self.expected_num_willcopy4 == \
                         self.control_num_willcopy4
        assert self.expected_num_customthrough == \
                         self.control_num_customthrough

        new_record = clone_record(self.copy_target)

        num_copytarget = CopyTarget.objects.count()
        num_wontcopy = WontCopy.objects.count()
        num_willcopy1 = WillCopy1.objects.count()
        num_willcopy2 = WillCopy2.objects.count()
        num_willcopy3 = WillCopy3.objects.count()
        num_willcopy4 = WillCopy4.objects.count()
        num_customthrough = CustomThrough.objects.count()

        # No change
        assert self.expected_num_wontcopy == num_wontcopy
        assert self.expected_num_willcopy1 == num_willcopy1
        assert self.expected_num_willcopy2 == num_willcopy2
        assert self.expected_num_willcopy3 == num_willcopy3
        assert self.expected_num_willcopy4 == num_willcopy4
        # Only CopyTarget and CustomThrough should have new records
        assert self.expected_num_copytarget+1 == num_copytarget
        assert self.expected_num_customthrough*2 == num_customthrough

        assert list(map(self.xform, new_record.custom_m2m.values())) == \
                list(map(self.xform, self.copy_target.custom_m2m.values()))
        assert list(map(self.xform, new_record.plain_m2m.values())) == \
                list(map(self.xform, self.copy_target.plain_m2m.values()))

        # The fields that link to WontCopy records from CopyTarget are reverse
        # relation managers, which is purposefully not supported, and as a
        # result are now empty
        assert self.copy_target.wont_copy_fk_set.exists() is True
        assert new_record.wont_copy_fk_set.exists() is False
        assert self.copy_target.wont_copy_m2m_set.exists() is True
        assert new_record.wont_copy_m2m_set.exists() is False
        # Control test - verify `wont_copy_1to1` field on original record
        # doesn't throw an exception, and returns an actual WontCopy instance.
        assert isinstance(self.copy_target.wont_copy_1to1, WontCopy)
        with pytest.raises(CopyTarget.wont_copy_1to1.RelatedObjectDoesNotExist,
                           match="CopyTarget has no wont_copy_1to1."):
            new_record.wont_copy_1to1

        assert new_record.fk_id == self.copy_target.fk_id
        assert new_record.oneto1 is None

    def test_error_on_required_onetoone_without_deep_copy(self):
        local = OneToOneLocalFactory()
        remote = local.oneto1
        control_num_local = OneToOneLocal.objects.count()
        control_num_remote = OneToOneRemote.objects.count()

        with pytest.raises(TypeError,
                           match=re_escape(
                               "Model has required OneToOne field(s) "
                               "['oneto1'] that wasn't specified as a "
                               "deep copy target.")):
            clone_record(local)

        num_local = OneToOneLocal.objects.count()
        num_remote = OneToOneRemote.objects.count()
        assert control_num_local == num_local
        assert control_num_remote == num_remote

        new_local = clone_record(local, deep_copy_fields=['oneto1'])
        new_remote = new_local.oneto1
        assert local.id != new_local.id
        assert remote.id != new_remote.id
        num_local = OneToOneLocal.objects.count()
        num_remote = OneToOneRemote.objects.count()
        assert control_num_local + 1 == num_local
        assert control_num_remote + 1 == num_remote

    def test_onetoone_from_remote_side_does_nothing(self):
        remote = OneToOneRemoteFactory()
        control_num_local = OneToOneLocal.objects.count()
        control_num_remote = OneToOneRemote.objects.count()

        new_remote = clone_record(remote)
        assert remote.id != new_remote.id
        num_local = OneToOneLocal.objects.count()
        num_remote = OneToOneRemote.objects.count()
        assert control_num_local == num_local
        assert control_num_remote+1 == num_remote
        with pytest.raises(OneToOneRemote.oneto1.RelatedObjectDoesNotExist,
                           match="OneToOneRemote has no oneto1."):
            new_remote.oneto1
