from itertools import chain, permutations, product, repeat
from multiprocessing import Process

from funcy import remove
from pytest import fixture, mark, lazy_fixture, param

from frontend.libs.utils.general_utils import deep_hash, _flatten_data, deep_join, deep_merge
from frontend.libs.utils.test_utils import expect


def test_flatten_data():
    """Verfies the functionality of `_flatten_data`"""
    test_val_1 = ['c', 'd', 'a']
    expected_val_1 = '(a,c,d)'
    assert expected_val_1 == _flatten_data(test_val_1)

    test_val_2 = {'asd': ['c', 'd', 'a'], 'zxc': 1}
    expected_val_2 = '(asd,(a,c,d)),(zxc,1)'
    assert expected_val_2 == _flatten_data(test_val_2)

    test_val_3 = {'one': {'asd': ['c', 'd', 'a'], 'zxc': 1}, 'two': 'blarg'}
    expected_val_3 = '(one,(asd,(a,c,d)),(zxc,1)),(two,blarg)'
    assert expected_val_3 == _flatten_data(test_val_3)


def test_deep_hash():
    """Verifies the functionality of `deep_hash` ensuring that same value is
    returned every time the test is run.
    """
    test_val = {'one': {'asd': ['c', 'd', 'a'], 'zxc': 1}, 'two': 'blarg'}
    expected_val = 'ecdc6d7575572c6f4edcdb0cd5a0f068f6f68f97'
    assert expected_val == deep_hash(test_val)


def test_deep_hash_multi_process():
    """Verifies that `deep_hash` returns same value even when it is run from
    multiple processes.
    """
    for _ in range(2):
        p = Process(target=test_deep_hash())
        p.start()
    p.join()


def describe_deep_join_merge():
    val_names = [
        'pybool',
        'pydecimal',
        'pyfloat',
        'pyint',
        'pystr',
    ]

    col_names = [
        'pydict',
        'pylist',
        'pyset',
        'pytuple',
    ]

    @fixture
    def pybool(faker):
        return faker.pybool()

    @fixture
    def pybool2(faker):
        return faker.pybool()

    @fixture
    def pydecimal(faker):
        return faker.pydecimal()

    @fixture
    def pydecimal2(faker):
        return faker.pydecimal()

    @fixture
    def pyfloat(faker):
        return faker.pyfloat()

    @fixture
    def pyfloat2(faker):
        return faker.pyfloat()

    @fixture
    def pyint(faker):
        return faker.pyint()

    @fixture
    def pyint2(faker):
        return faker.pyint()

    @fixture
    def pystr(faker):
        return faker.pystr()

    @fixture
    def pystr2(faker):
        return faker.pystr()

    @fixture
    def pydict(faker):
        return faker.pydict()

    @fixture
    def pylist(faker):
        return faker.pylist()

    @fixture
    def pyset(faker):
        return faker.pyset()

    @fixture
    def pytuple(faker):
        return faker.pytuple()

    @fixture
    def pynone():
        return None

    @fixture(params=[
        param((lazy_fixture(loser), lazy_fixture(winner)),
              id='{}_{}'.format(loser, winner))
        for loser, winner in chain(
            # every basic type against another value of the same type
            ((vname, vname + '2') for vname in val_names),
            # every basic type against every other basic type
            permutations(val_names, 2),
            # every basic type against every collection type
            # basic first, collection second. collection wins
            product(val_names, col_names),
            # every collection type against every basic type
            # collection first, basic second, basic wins
            product(col_names, val_names),
            # every collection and basic type first, None last. None wins
            zip(chain(val_names, col_names), repeat('pynone')),
            # None first, all other types second. None loses.
            zip(repeat('pynone'), chain(val_names, col_names)),
        )
    ])
    def all_data_permutations(request):
        return request.param

    def the_last_value_wins_with_mixed_types(all_data_permutations):
        loser, winner = all_data_permutations
        expect(deep_merge({'a': loser}, {'a': winner}, {})) == {'a': winner}
        expect(deep_join([{'a': loser}, {'a': winner}, {}])) == {'a': winner}

    def keep_both(all_data_permutations):
        loser, winner = all_data_permutations
        expect(deep_merge({'a': loser}, {'a': winner}, {}, type_conflict_handler=tuple)) == {'a': (loser, winner)}
        expect(deep_join([{'a': loser}, {'a': winner}, {}], type_conflict_handler=tuple)) == {'a': (loser, winner)}

    def describe_collection_merging_type_handling():
        def list_first_wins():
            expect(deep_merge({'a': list()}, {'a': tuple()})['a']).isinstance(list)
            expect(deep_merge({'a': list()}, {'a': set()})['a']).isinstance(list)

        def tuple_first_wins():
            expect(deep_merge({'a': tuple()}, {'a': list()})['a']).isinstance(tuple)
            expect(deep_merge({'a': tuple()}, {'a': set()})['a']).isinstance(tuple)

        def set_first_wins():
            expect(deep_merge({'a': set()}, {'a': list()})['a']).isinstance(set)
            expect(deep_merge({'a': set()}, {'a': tuple()})['a']).isinstance(set)

    @mark.parametrize('first, second', [
        param(lazy_fixture(first), lazy_fixture(second), id='{}_{}'.format(first, second))
        for first, second in product(remove('pydict', col_names), repeat=2)
    ])
    def mixed_collection_merge_results(first, second):
        expect(deep_merge({'a': first}, {'a': second})) == {'a': type(first)(list(first) + list(second))}

    def complex_nested_merge():
        one = {
            'a': {
                'b': {
                    'd': [1,2],
                    'e': 'asd',
                    'n': 9,
                },
                'c': 1,
                'd': ['a','c'],
                'e': 'ewq',
            },
            'd': 88,
        }
        two = {
            'a': {
                'b': {
                    'd': [9,4,3],
                    'e': 'hjk',
                    'u': 33,
                },
                'd': ['g'],
                'e': 'zxc',
                'y': 88,
            },
            'd': 43,
        }
        result = {
            'a': {
                'b': {
                    'd': [1,2,9,4,3],
                    'e': 'hjk',
                    'u': 33,
                    'n': 9,
                },
                'c': 1,
                'd': ['a','c','g'],
                'e': 'zxc',
                'y': 88,
            },
            'd': 43,
        }
        expect(deep_merge(one, two)) == result
