
from unittest.mock import patch

from datetime import date

import frontend.libs.utils.election_utils as eu
from frontend.libs.utils.test_utils import TestCase


class ElectionUtilsTests(TestCase):
    def test_no_intersections_senate_list(self):
        # ensure no intersections in the static sets of states
        self.assertEqual(eu.zero_two.intersection(eu.zero_four), set())
        self.assertEqual(eu.zero_two.intersection(eu.two_four), set())
        self.assertEqual(eu.zero_four.intersection(eu.two_four), set())

    def test_all_states_in_senate_moduli_map(self):
        # the senate_moduli_map was correctly generated.
        self.assertEqual(len(eu.senate_moduli_map), 50)

    def test_years_till_next_senate(self):
        # ensure `_years_till_next_senate` function correctly calculates the
        # number of years until the next senate seat election for a state.

        # expected results (years till next election)
        # list index corresponds to input year modulus (year % 6)
        zero_two = [2, 1, 4, 3, 2, 1]
        zero_four = [4, 3, 2, 1, 2, 1]
        two_four = [2, 1, 2, 1, 4, 3]

        # (0,2) are the modulus (year % 6) of the years states like CA have
        # elections
        output = [eu._years_till_next_senate(_, (0,2)) for _ in range(6)]
        self.assertEqual(output, zero_two)

        # (0,4) are the modulus (year % 6) of the years states like AL have
        # elections
        output = [eu._years_till_next_senate(_, (0,4)) for _ in range(6)]
        self.assertEqual(output, zero_four)

        # (2,4) are the modulus (year % 6) of the years states like NJ have
        # elections
        output = [eu._years_till_next_senate(_, (2,4)) for _ in range(6)]
        self.assertEqual(output, two_four)

    def test_years_till_prev_senate(self):
        # ensure `_years_till_prev_senate` function correctly calculates the
        # number of years since the previous senate seat election for a state.

        # expected results (years since prev election)
        # list index corresponds to input year modulus (year % 6)
        zero_two = [4, 1, 2, 1, 2, 3]
        zero_four = [2, 1, 2, 3, 4, 1]
        two_four = [2, 3, 4, 1, 2, 1]

        # (0,2) are the modulus (year % 6) of the years states like CA have
        # elections
        output = [eu._years_till_prev_senate(_, (0,2)) for _ in range(6)]
        self.assertEqual(output, zero_two)

        # (0,4) are the modulus (year % 6) of the years states like AL have
        # elections
        output = [eu._years_till_prev_senate(_, (0,4)) for _ in range(6)]
        self.assertEqual(output, zero_four)

        # (2,4) are the modulus (year % 6) of the years states like NJ have
        # elections
        output = [eu._years_till_prev_senate(_, (2,4)) for _ in range(6)]
        self.assertEqual(output, two_four)

    def test_next_senate_election(self):
        # around election date test
        # ensure that `_next_senate_election` correctly handles edge case of an
        # input date that is on an election year, and days before the election.
        self.assertEqual(eu._next_senate_election(date(2010, 11, 1), (0,2)),
                         date(2010, 11, 2))

        # same as election day test
        # ensure that `_next_senate_election` correctly handles edge case of an
        # input date that is the next election date.
        self.assertEqual(eu._next_senate_election(date(2010, 11, 2), (0,2)),
                         date(2010, 11, 2))

        # after election date test
        # ensure that `_next_senate_election` correctly handles edge case of an
        # input date that is on an election year, but after the election day.
        self.assertEqual(eu._next_senate_election(date(2010, 11, 3), (0,2)),
                         date(2012, 11, 6))
        self.assertEqual(eu._next_senate_election(date(2010, 12, 3), (0,2)),
                         date(2012, 11, 6))


        # ensure consistent results from _prev_senate_election for states in
        # each of the 3 cycles. senate seats have an irregular cycle, and I
        # want to make sure the functions are tested thoroughly.

        #zero_two
        self.assertEqual(eu._next_senate_election(date(2009, 2, 4), (0,2)),
                         date(2010, 11, 2))
        self.assertEqual(eu._next_senate_election(date(2010, 2, 4), (0,2)),
                         date(2010, 11, 2))

        self.assertEqual(eu._next_senate_election(date(2011, 2, 4), (0,2)),
                         date(2012, 11, 6))
        self.assertEqual(eu._next_senate_election(date(2012, 2, 4), (0,2)),
                         date(2012, 11, 6))

        self.assertEqual(eu._next_senate_election(date(2013, 2, 4), (0,2)),
                         date(2016, 11, 8))
        self.assertEqual(eu._next_senate_election(date(2014, 2, 4), (0,2)),
                         date(2016, 11, 8))
        self.assertEqual(eu._next_senate_election(date(2015, 2, 4), (0,2)),
                         date(2016, 11, 8))
        self.assertEqual(eu._next_senate_election(date(2016, 2, 4), (0,2)),
                         date(2016, 11, 8))

        self.assertEqual(eu._next_senate_election(date(2017, 2, 4), (0,2)),
                         date(2018, 11, 6))
        self.assertEqual(eu._next_senate_election(date(2018, 2, 4), (0,2)),
                         date(2018, 11, 6))

        #zero_four
        self.assertEqual(eu._next_senate_election(date(2009, 2, 4), (0,4)),
                         date(2010, 11, 2))
        self.assertEqual(eu._next_senate_election(date(2010, 2, 4), (0,4)),
                         date(2010, 11, 2))

        self.assertEqual(eu._next_senate_election(date(2011, 2, 4), (0,4)),
                         date(2014, 11, 4))
        self.assertEqual(eu._next_senate_election(date(2012, 2, 4), (0,4)),
                         date(2014, 11, 4))
        self.assertEqual(eu._next_senate_election(date(2013, 2, 4), (0,4)),
                         date(2014, 11, 4))
        self.assertEqual(eu._next_senate_election(date(2014, 2, 4), (0,4)),
                         date(2014, 11, 4))

        self.assertEqual(eu._next_senate_election(date(2015, 2, 4), (0,4)),
                         date(2016, 11, 8))
        self.assertEqual(eu._next_senate_election(date(2016, 2, 4), (0,4)),
                         date(2016, 11, 8))

        self.assertEqual(eu._next_senate_election(date(2017, 2, 4), (0,4)),
                         date(2020, 11, 3))
        self.assertEqual(eu._next_senate_election(date(2018, 2, 4), (0,4)),
                         date(2020, 11, 3))
        self.assertEqual(eu._next_senate_election(date(2019, 2, 4), (0,4)),
                         date(2020, 11, 3))


        #two_four
        self.assertEqual(eu._next_senate_election(date(2009, 2, 4), (2,4)),
                         date(2012, 11, 6))
        self.assertEqual(eu._next_senate_election(date(2010, 2, 4), (2,4)),
                         date(2012, 11, 6))
        self.assertEqual(eu._next_senate_election(date(2011, 2, 4), (2,4)),
                         date(2012, 11, 6))
        self.assertEqual(eu._next_senate_election(date(2012, 2, 4), (2,4)),
                         date(2012, 11, 6))

        self.assertEqual(eu._next_senate_election(date(2013, 2, 4), (2,4)),
                         date(2014, 11, 4))
        self.assertEqual(eu._next_senate_election(date(2014, 2, 4), (2,4)),
                         date(2014, 11, 4))

        self.assertEqual(eu._next_senate_election(date(2015, 2, 4), (2,4)),
                         date(2018, 11, 6))
        self.assertEqual(eu._next_senate_election(date(2016, 2, 4), (2,4)),
                         date(2018, 11, 6))
        self.assertEqual(eu._next_senate_election(date(2017, 2, 4), (2,4)),
                         date(2018, 11, 6))
        self.assertEqual(eu._next_senate_election(date(2018, 2, 4), (2,4)),
                         date(2018, 11, 6))

        self.assertEqual(eu._next_senate_election(date(2019, 2, 4), (2,4)),
                         date(2020, 11, 3))

    def test_prev_senate_election(self):
        # around election date test
        # ensure that `_prev_senate_election` correctly handles edge case of an
        # input date that is on an election year, and days before the election.
        self.assertEqual(eu._prev_senate_election(date(2010, 11, 1), (0,2)),
                         date(2006, 11, 7))

        # same day as election
        # ensure that `_prev_senate_election` correctly handles edge case of an
        # input date that is an election day.
        self.assertEqual(eu._prev_senate_election(date(2010, 11, 2), (0,2)),
                         date(2006, 11, 7))

        # after election date test
        # ensure that `_prev_senate_election` correctly handles edge case of an
        # input date that is on an election year, but after the election day.
        self.assertEqual(eu._prev_senate_election(date(2010, 11, 3), (0,2)),
                         date(2010, 11, 2))
        self.assertEqual(eu._prev_senate_election(date(2010, 12, 3), (0,2)),
                         date(2010, 11, 2))


        # ensure consistent results from _prev_senate_election for states in
        # each of the 3 cycles. senate seats have an irregular cycle, and I
        # want to make sure the functions are tested thoroughly.

        #zero_two
        # 2006
        self.assertEqual(eu._prev_senate_election(date(2009, 2, 4), (0,2)),
                         date(2006, 11, 7))
        self.assertEqual(eu._prev_senate_election(date(2010, 2, 4), (0,2)),
                         date(2006, 11, 7))

        # 2010
        self.assertEqual(eu._prev_senate_election(date(2011, 2, 4), (0,2)),
                         date(2010, 11, 2))
        self.assertEqual(eu._prev_senate_election(date(2012, 2, 4), (0,2)),
                         date(2010, 11, 2))

        # 2012
        self.assertEqual(eu._prev_senate_election(date(2013, 2, 4), (0,2)),
                         date(2012, 11, 6))
        self.assertEqual(eu._prev_senate_election(date(2014, 2, 4), (0,2)),
                         date(2012, 11, 6))
        self.assertEqual(eu._prev_senate_election(date(2015, 2, 4), (0,2)),
                         date(2012, 11, 6))
        self.assertEqual(eu._prev_senate_election(date(2016, 2, 4), (0,2)),
                         date(2012, 11, 6))

        # 2016
        self.assertEqual(eu._prev_senate_election(date(2017, 2, 4), (0,2)),
                         date(2016, 11, 8))
        self.assertEqual(eu._prev_senate_election(date(2018, 2, 4), (0,2)),
                         date(2016, 11, 8))

        #zero_four
        # 2008
        self.assertEqual(eu._prev_senate_election(date(2009, 2, 4), (0,4)),
                         date(2008, 11, 4))
        self.assertEqual(eu._prev_senate_election(date(2010, 2, 4), (0,4)),
                         date(2008, 11, 4))

        # 2010
        self.assertEqual(eu._prev_senate_election(date(2011, 2, 4), (0,4)),
                         date(2010, 11, 2))
        self.assertEqual(eu._prev_senate_election(date(2012, 2, 4), (0,4)),
                         date(2010, 11, 2))
        self.assertEqual(eu._prev_senate_election(date(2013, 2, 4), (0,4)),
                         date(2010, 11, 2))
        self.assertEqual(eu._prev_senate_election(date(2014, 2, 4), (0,4)),
                         date(2010, 11, 2))

        # 2014
        self.assertEqual(eu._prev_senate_election(date(2015, 2, 4), (0,4)),
                         date(2014, 11, 4))
        self.assertEqual(eu._prev_senate_election(date(2016, 2, 4), (0,4)),
                         date(2014, 11, 4))

        # 2016
        self.assertEqual(eu._prev_senate_election(date(2017, 2, 4), (0,4)),
                         date(2016, 11, 8))
        self.assertEqual(eu._prev_senate_election(date(2018, 2, 4), (0,4)),
                         date(2016, 11, 8))
        self.assertEqual(eu._prev_senate_election(date(2019, 2, 4), (0,4)),
                         date(2016, 11, 8))


        #two_four
        # 2008
        self.assertEqual(eu._prev_senate_election(date(2009, 2, 4), (2,4)),
                         date(2008, 11, 4))
        self.assertEqual(eu._prev_senate_election(date(2010, 2, 4), (2,4)),
                         date(2008, 11, 4))
        self.assertEqual(eu._prev_senate_election(date(2011, 2, 4), (2,4)),
                         date(2008, 11, 4))
        self.assertEqual(eu._prev_senate_election(date(2012, 2, 4), (2,4)),
                         date(2008, 11, 4))

        # 2012
        self.assertEqual(eu._prev_senate_election(date(2013, 2, 4), (2,4)),
                         date(2012, 11, 6))
        self.assertEqual(eu._prev_senate_election(date(2014, 2, 4), (2,4)),
                         date(2012, 11, 6))

        # 2014
        self.assertEqual(eu._prev_senate_election(date(2015, 2, 4), (2,4)),
                         date(2014, 11, 4))
        self.assertEqual(eu._prev_senate_election(date(2016, 2, 4), (2,4)),
                         date(2014, 11, 4))
        self.assertEqual(eu._prev_senate_election(date(2017, 2, 4), (2,4)),
                         date(2014, 11, 4))
        self.assertEqual(eu._prev_senate_election(date(2018, 2, 4), (2,4)),
                         date(2014, 11, 4))

        # 2018
        self.assertEqual(eu._prev_senate_election(date(2019, 2, 4), (2,4)),
                         date(2018, 11, 6))

    def test_election_date_for_year(self):
        # ensure `election_date_for_year` always returns the correct election
        # day for a year, irrespective of terms or anything else.
        election_days = [date(2006, 11, 7),
                         date(2007, 11, 6),
                         date(2008, 11, 4),
                         date(2009, 11, 3),
                         date(2010, 11, 2),
                         date(2011, 11, 8),
                         date(2012, 11, 6),
                         date(2013, 11, 5),
                         date(2014, 11, 4),
                         date(2015, 11, 3),
                         date(2016, 11, 8),
                         date(2017, 11, 7),
                         date(2018, 11, 6)]

        result = [eu.election_date_for_year(_) for _ in range(2006, 2019, 1)]
        self.assertEqual(result, election_days)

    def test_next_election(self):
        # verify term is accounted for correctly
        self.assertEqual(
            eu.next_election(2, date(2009, 1, 1)),
            date(2010, 11, 2))

        self.assertEqual(
            eu.next_election(4, date(2009, 1, 1)),
            date(2012, 11, 6))


        # verify if no input date is given, date.today() is used
        with patch('frontend.libs.utils.election_utils._get_today',
                   return_value=date(2011, 1, 1)) as m:
            result = eu.next_election(2)
            m.assert_called_once_with()

        self.assertEqual(result, date(2012, 11, 6))


        # verify we get the correct election the same year as an election.
        self.assertEqual(
            eu.next_election(2, date(2014, 10, 7)),
            date(2014, 11, 4))

        self.assertEqual(
            eu.next_election(2, date(2014, 11, 1)),
            date(2014, 11, 4))

        self.assertEqual(
            eu.next_election(2, date(2014, 11, 3)),
            date(2014, 11, 4))

        self.assertEqual(
            eu.next_election(2, date(2014, 11, 4)),
            date(2014, 11, 4))


        # verify if input_date is the same year as an election, but the date is
        # after the election date, we get the next election, not the election
        # that happened earlier in the year.
        self.assertEqual(
            eu.next_election(2, date(2014, 11, 5)),
            date(2016, 11, 8))

        self.assertEqual(
            eu.next_election(2, date(2014, 12, 2)),
            date(2016, 11, 8))

    def test_next_default_election(self):
        # verify if no input date is given, date.today() is used
        with patch('frontend.libs.utils.election_utils._get_today',
                   return_value=date(2011, 1, 1)) as m:
            result = eu.next_default_election()
            m.assert_called_once_with()

        self.assertEqual(result, date(2012, 11, 6))

        # verify if an input date IS given, that it is used.
        self.assertEqual(eu.next_default_election(date(2006, 4, 2)),
                         date(2006, 11, 7))

    def test_next_election_date_for_office(self):
        # verify we get value error for invalid office inputs
        self.assertRaises(ValueError, eu.next_election_date_for_office,
                          'p')
        self.assertRaises(ValueError, eu.next_election_date_for_office,
                          'pres')
        self.assertRaises(ValueError, eu.next_election_date_for_office,
                          'h')
        self.assertRaises(ValueError, eu.next_election_date_for_office,
                          's')
        self.assertRaises(ValueError, eu.next_election_date_for_office,
                          'senate')

        # verify ValueError is thrown when office is senate and invalid (or no)
        # state input is given
        self.assertRaises(ValueError, eu.next_election_date_for_office,
                          'S', state=None)
        self.assertRaises(ValueError, eu.next_election_date_for_office,
                          'S', state='ca')
        self.assertRaises(ValueError, eu.next_election_date_for_office,
                          'S', state='cali')


        # verify valid President inputs work
        # abbreviation
        self.assertEqual(
            eu.next_election_date_for_office('P', date(2009, 1, 1)),
            date(2012, 11, 6))

        # enum input
        self.assertEqual(
            eu.next_election_date_for_office(eu.Office.President, date(2009, 1, 1)),
            date(2012, 11, 6))

        # full name
        self.assertEqual(
            eu.next_election_date_for_office('President', date(2009, 1, 1)),
            date(2012, 11, 6))

        # verify valid House inputs work
        # abbreviation
        self.assertEqual(
            eu.next_election_date_for_office('H', date(2009, 1, 1)),
            date(2010, 11, 2))

        # enum input
        self.assertEqual(
            eu.next_election_date_for_office(eu.Office.House, date(2009, 1, 1)),
            date(2010, 11, 2))

        # full name
        self.assertEqual(
            eu.next_election_date_for_office('House', date(2009, 1, 1)),
            date(2010, 11, 2))

        # verify valid Senate inputs work
        # abbreviation
        self.assertEqual(
            eu.next_election_date_for_office('S', date(2009, 1, 1), 'CA'),
            date(2010, 11, 2))

        # enum input
        self.assertEqual(
            eu.next_election_date_for_office(eu.Office.Senate, date(2009, 1, 1), 'CA'),
            date(2010, 11, 2))

        # full name
        self.assertEqual(
            eu.next_election_date_for_office('Senate', date(2009, 1, 1), 'CA'),
            date(2010, 11, 2))

        # verify valid state inputs work
        # full name
        self.assertEqual(
            eu.next_election_date_for_office('S', date(2009, 1, 1), 'California'),
            date(2010, 11, 2))

        # enum input
        self.assertEqual(
            eu.next_election_date_for_office('S', date(2009, 1, 1), eu.State.CA),
            date(2010, 11, 2))

        # abbreviation
        self.assertEqual(
            eu.next_election_date_for_office('S', date(2009, 1, 1), 'CA'),
            date(2010, 11, 2))

    def test_previous_election_date_for_office(self):
        # verify we get value error for invalid office inputs
        self.assertRaises(ValueError, eu.previous_election_date_for_office,
                          'p')
        self.assertRaises(ValueError, eu.previous_election_date_for_office,
                          'pres')
        self.assertRaises(ValueError, eu.previous_election_date_for_office,
                          'h')
        self.assertRaises(ValueError, eu.previous_election_date_for_office,
                          's')
        self.assertRaises(ValueError, eu.previous_election_date_for_office,
                          'Senate')

        # verify ValueError is thrown when office is Senate and invalid (or no)
        # state input is given
        self.assertRaises(ValueError, eu.previous_election_date_for_office,
                          'S', state=None)
        self.assertRaises(ValueError, eu.previous_election_date_for_office,
                          'S', state='ca')
        self.assertRaises(ValueError, eu.previous_election_date_for_office,
                          'S', state='cali')


        # same day as election
        self.assertEqual(
            eu.previous_election_date_for_office('H', date(2010, 11, 2)),
            date(2008, 11, 4))

        # verify President inputs work
        self.assertEqual(
            eu.previous_election_date_for_office('P', date(2011, 1, 1)),
            date(2008, 11, 4))

        self.assertEqual(
            eu.previous_election_date_for_office(eu.Office.President, date(2011, 1, 1)),
            date(2008, 11, 4))

        self.assertEqual(
            eu.previous_election_date_for_office('President', date(2011, 1, 1)),
            date(2008, 11, 4))

        # verify House inputs work
        self.assertEqual(
            eu.previous_election_date_for_office('H', date(2011, 1, 1)),
            date(2010, 11, 2))

        self.assertEqual(
            eu.previous_election_date_for_office(eu.Office.House, date(2011, 1, 1)),
            date(2010, 11, 2))

        self.assertEqual(
            eu.previous_election_date_for_office('House', date(2011, 1, 1)),
            date(2010, 11, 2))

        # verify Senate inputs work
        self.assertEqual(
            eu.previous_election_date_for_office('S', date(2011, 1, 1), 'CA'),
            date(2010, 11, 2))

        self.assertEqual(
            eu.previous_election_date_for_office(eu.Office.Senate, date(2011, 1, 1), 'CA'),
            date(2010, 11, 2))

        self.assertEqual(
            eu.previous_election_date_for_office('Senate', date(2011, 1, 1), 'CA'),
            date(2010, 11, 2))

        # verify state inputs work
        self.assertEqual(
            eu.previous_election_date_for_office('S', date(2011, 1, 1), 'California'),
            date(2010, 11, 2))

        self.assertEqual(
            eu.previous_election_date_for_office('S', date(2011, 1, 1), eu.State.CA),
            date(2010, 11, 2))

        self.assertEqual(
            eu.previous_election_date_for_office('S', date(2011, 1, 1), 'CA'),
            date(2010, 11, 2))

    def test_election_period_for_office(self):
        # verify `election_period_for_office` evalutes correctly for all
        # offices
        self.assertEqual(
            eu.election_period_for_office('H', date(2011, 1, 1)),
            (date(2010, 11, 2), date(2012, 11, 5)))

        self.assertEqual(
            eu.election_period_for_office('P', date(2011, 1, 1)),
            (date(2008, 11, 4), date(2012, 11, 5)))

        self.assertEqual(
            eu.election_period_for_office('S', date(2011, 1, 1), 'CA'),
            (date(2010, 11, 2), date(2012, 11, 5)))

        # verify that the irregular pattern of Senate seat elections per state
        # is correctly accounted for.
        self.assertEqual(
            eu.election_period_for_office('S', date(2013, 1, 1), 'CA'),
            (date(2012, 11, 6), date(2016, 11, 7)))

    def test_contribution_limit(self):
        # verify we always get 5400
        self.assertEqual(eu.contribution_limit(date(2010, 1, 1)), 5400)
        self.assertEqual(eu.contribution_limit(date(2011, 1, 1)), 5400)
        self.assertEqual(eu.contribution_limit(date(2012, 1, 1)), 5400)
        self.assertEqual(eu.contribution_limit(date(2013, 1, 1)), 5400)
        self.assertEqual(eu.contribution_limit(date(2014, 1, 1)), 5400)
        self.assertEqual(eu.contribution_limit(date(2015, 1, 1)), 5400)
        self.assertEqual(eu.contribution_limit(date(2016, 1, 1)), 5400)

    def testprev_election(self):
        # Verify current, past, and future cycles
        with patch("frontend.libs.utils.election_utils._get_today",
                   lambda: date(2016, 11, 1)):
            self.assertEqual(eu.prev_election(2), date(2014, 11, 4))
        self.assertEqual(eu.prev_election(2, date(2014, 6, 6)),
                         date(2012, 11, 6))
        self.assertEqual(eu.prev_election(2, date(2013, 6, 6)),
                         date(2012, 11, 6))
        self.assertEqual(eu.prev_election(2, date(2016, 12, 2)),
                         date(2016, 11, 8))

        ## Verify some edge cases
        # Same day
        self.assertEqual(eu.prev_election(2, date(2016, 11, 8)),
                         date(2014, 11, 4))
        # Next day
        self.assertEqual(eu.prev_election(2, date(2016, 11, 9)),
                         date(2016, 11, 8))
        # The year after, but an earlier November date
        self.assertEqual(eu.prev_election(2, date(2013, 11, 4)),
                         date(2012, 11, 6))
        # First and last days of the year
        self.assertEqual(eu.prev_election(2, date(2015, 1, 1)),
                         date(2014, 11, 4))
        self.assertEqual(eu.prev_election(2, date(2014, 12, 31)),
                         date(2014, 11, 4))

        # Verify some years that aren't "2"
        self.assertEqual(eu.prev_election(6, date(2015, 1, 1)),
                         date(2010, 11, 2))
        self.assertEqual(eu.prev_election(6, date(2013, 1, 1)),
                         date(2010, 11, 2))
        self.assertEqual(eu.prev_election(6, date(2011, 1, 1)),
                         date(2010, 11, 2))
        self.assertEqual(eu.prev_election(6, date(2021, 1, 1)),
                         date(2016, 11, 8))

        self.assertEqual(eu.prev_election(4, date(2015, 1, 1)),
                         date(2012, 11, 6))
        self.assertEqual(eu.prev_election(4, date(2016, 12, 1)),
                         date(2016, 11, 8))






