from datetime import datetime, date

from frontend.libs.utils.test_utils import TestCase
from ..date_utils import add_months, next_first_of_month


class AddMonthsTests(TestCase):
    def test_basic_date(self):
        """Verify the months are correctly added to a common day"""
        start_date = date(2018, 10, 15)
        assert add_months(start_date, 2) == date(2018, 12, 15)
        assert add_months(start_date, 3) == date(2019, 1, 15)
        assert add_months(start_date, 17) == date(2020, 3, 15)

        start_date = date(2017, 1, 1)
        assert add_months(start_date, 12) == date(2018, 1, 1)

    def test_tricky_date(self):
        """Verify dates are rounded down for shorter months"""
        start_date = date(2018, 10, 31)
        assert add_months(start_date, 1) == date(2018, 11, 30)
        assert add_months(start_date, 4) == date(2019, 2, 28)
        assert add_months(start_date, 16) == date(2020, 2, 29)

    def test_datetime(self):
        """Verify datetime works on a range of dates"""
        start_date = datetime(2018, 10, 15, 12, 13, 14)
        assert add_months(start_date, 2) == datetime(2018, 12, 15, 12, 13, 14)
        assert add_months(start_date, 3) == datetime(2019, 1, 15, 12, 13, 14)

        start_date = datetime(2018, 10, 31, 1, 2, 3)
        assert add_months(start_date, 1) == datetime(2018, 11, 30, 1, 2, 3)
        assert add_months(start_date, 4) == datetime(2019, 2, 28, 1, 2, 3)

    def test_next_month_same_year(self):
        test_date = datetime(2018, 10, 31, 1, 2, 3)
        assert next_first_of_month(test_date).year == test_date.year

    def test_next_month_january(self):
        start_date = datetime(2018, 12, 31, 1, 2, 3)
        assert next_first_of_month(start_date).month == 1

    def test_day_is_first(self):
        start_date = datetime(2018, 10, 31, 1, 2, 3)
        assert next_first_of_month(start_date).day == 1
