from django.core.exceptions import ValidationError

from ..form_utils import FlexibleComplexityValidator
from ..test_utils import TestCase


class FlexibleComplexityValidatorTests(TestCase):
    def test_init(self):
        """Verify the fields are initialized correctly given certain input"""
        validator = FlexibleComplexityValidator(None, None)
        assert validator.complexities is None
        assert validator.required_count == 0

        complexities = dict()
        validator = FlexibleComplexityValidator(complexities, None)
        assert validator.complexities == complexities
        assert validator.required_count == 0

        complexities = dict(UPPER=1, LOWER=1)
        validator = FlexibleComplexityValidator(complexities, None)
        assert validator.complexities == complexities
        assert validator.required_count == 2

        complexities = dict(UPPER=1, LOWER=1)
        validator = FlexibleComplexityValidator(complexities, 1)
        assert validator.complexities == complexities
        assert validator.required_count == 1
        # Verify required count isn't more than the len of complexities
        validator = FlexibleComplexityValidator(complexities, 3)
        assert validator.complexities == complexities
        assert validator.required_count == 2

        # Verify values of 0 are removed from the complexities dict
        complexities = dict(UPPER=1, LOWER=0)
        validator = FlexibleComplexityValidator(complexities)
        assert validator.complexities == dict(UPPER=1)
        assert validator.required_count == 1


    def test_call(self):
        """Verify passwords are validated correctly"""
        complexities = dict(
            UPPER=1,
            DIGITS=1,
            SPECIAL=1)
        validator = FlexibleComplexityValidator(complexities, 2)

        with self.assertRaises(ValidationError) as e:
            validator("abcdef")
        assert "must contain 2 more of the following" in str(e.exception)

        with self.assertRaises(ValidationError) as e:
            validator("abCDef")
        assert "must contain 1 more of the following" in str(e.exception)

        # Verify some positive cases
        validator("a!bcdeF")
        validator("Aa!bcdef8")

