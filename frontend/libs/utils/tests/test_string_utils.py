
from frontend.libs.utils.string_utils import initialism
from frontend.libs.utils.test_utils import TestCase


class StringUtilsTests(TestCase):

    def test_initialism(self):
        assert "RSI" == initialism("RevUp Software Inc.")
        assert "RSI" == initialism("RevUp .Software înc")
        assert "3YZ" == initialism("3xamine your Æzipper")
        assert "UPS" == initialism("UPS")
        assert "U" == initialism("Ups")
        assert "U" == initialism("UpS")
        assert "3M" == initialism("3M")
        assert "3" == initialism("3m")
        assert "UPS" == initialism("U p S")
