
from django.conf import settings

from frontend.libs.utils.address_utils import AddressParser
from frontend.libs.utils.string_utils import similarity
from frontend.libs.utils.test_utils import TestCase


THRESHOLD = settings.MERGING_ADDRESS_SIMILARITY_THRESHOLD


class AddressParserTests(TestCase):
    def test_parser(self):
        ap = AddressParser("Fictional Building 1234 Main St. NW; "
                           "suite 5, Redwood City, CA 95555")
        self.assertEqual(ap.building_name, "Fictional Building")
        self.assertEqual(ap.street_name, "Main")
        self.assertEqual(ap.street_number, "1234")
        self.assertEqual(ap.street_predirectional, "")
        self.assertEqual(ap.street_suffix, "St.")
        self.assertEqual(ap.street_postdirectional, "NW")
        self.assertEqual(ap.occupancy_type, "suite")
        self.assertEqual(ap.occupancy_id, "5")
        self.assertEqual(ap.get_full_street(),
                         "Fictional Building 1234 Main St. NW suite 5")
        self.assertEqual(ap.city_name, "Redwood City")
        self.assertEqual(ap.post_code, "95555")
        self.assertEqual(ap.state_name, "CA")

        ap = AddressParser("123 NW St. James Ave., PO Box 456, Redwood City, CA")
        self.assertEqual(ap.street_name, "St. James")
        self.assertEqual(ap.street_number, "123")
        self.assertEqual(ap.street_predirectional, "NW")
        self.assertEqual(ap.street_postdirectional, "")
        self.assertEqual(ap.street_suffix, "Ave.")
        self.assertEqual(ap.occupancy_type, "")
        self.assertEqual(ap.occupancy_id, "")
        self.assertEqual(ap.po_box, "PO Box 456")
        self.assertEqual(ap.get_full_street(), "123 NW St. James Ave. PO Box 456")
        self.assertEqual(ap.city_name, "Redwood City")
        self.assertEqual(ap.post_code, "")
        self.assertEqual(ap.state_name, "CA")

    def test_apples_to_apples(self):
        ap = AddressParser("123 NW Saint James St.")
        addr = " ".join(list(filter(None, ap._tokenize_and_process_address())))
        self.assertEqual("saint james 123 northwest street.", addr)

        addr2_string = "123 Northwest St. James Street; PO BOX 456"
        ap2 = AddressParser(addr2_string)
        addr = " ".join(list(filter(None, ap2._tokenize_and_process_address())))
        self.assertEqual("saint. james 123 northwest street po box 456", addr)

        self.assertTrue(ap.apples_to_apples(ap2) > THRESHOLD)
        # Verify without preprocessing, these addresses are too different
        self.assertFalse(ap.apples_to_apples(ap2, False) > THRESHOLD)

        # Verify this function works with a string as input
        self.assertTrue(ap.apples_to_apples(addr2_string) > THRESHOLD)
        # Verify it gives a type error for anything else
        with self.assertRaises(TypeError):
            ap.apples_to_apples(42)

        # Verify even with processing, these addresses are still too different
        ap2 = AddressParser("123 NE St. Mary Street")
        self.assertFalse(ap.apples_to_apples(ap2) > THRESHOLD)

    def test_apples_to_apples_vs_edit_distance(self):
        # Verify apples to apples has a different matching profile than simple
        # edit distance
        addr1 = "123 Main st; suite 4B"
        addr2 = "123 Main st."
        ap = AddressParser(addr1)

        # Verify similarity isn't enough to match these two addresses
        self.assertFalse(similarity(addr1, addr2) > THRESHOLD)
        # Verify a_to_a does match
        self.assertTrue(ap.apples_to_apples(addr2) > THRESHOLD)

        addr1 = "123 NW Main st"
        addr2 = "123 Northwest Main street"
        ap = AddressParser(addr1)
        # Verify similarity isn't enough to match these two addresses
        self.assertFalse(similarity(addr1, addr2) > THRESHOLD)
        # Verify a_to_a does match
        self.assertTrue(ap.apples_to_apples(addr2) > THRESHOLD)

        addr1 = "123 NW Main st"
        addr2 = "123 Main st SW"
        ap = AddressParser(addr1)
        # Verify similarity isn't enough to match these two addresses
        self.assertFalse(similarity(addr1, addr2) > THRESHOLD)
        # Verify a_to_a does match.
        self.assertTrue(ap.apples_to_apples(addr2) > THRESHOLD)

        addr1 = "456 5th st"
        addr2 = "456 Fifth st"
        ap = AddressParser(addr1)
        # Verify similarity isn't enough to match these two addresses
        self.assertFalse(similarity(addr1, addr2) > THRESHOLD)
        # Verify a_to_a does match.
        self.assertTrue(ap.apples_to_apples(addr2) > THRESHOLD)

