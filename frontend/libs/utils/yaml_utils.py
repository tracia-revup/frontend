
from collections import OrderedDict
import yaml


def dict_representer(dumper, data):
    return dumper.represent_dict(data.items())

yaml.add_representer(OrderedDict, dict_representer)
yaml.SafeDumper.add_representer(OrderedDict, dict_representer)


class SafeDumper(yaml.dumper.SafeDumper):
    def expect_block_sequence(self):
        if self.mapping_context:
            indentless = False
        else:
            indentless = not self.indention
        self.increase_indent(flow=False, indentless=indentless)
        self.state = self.expect_first_block_sequence_item


def safe_dump(data, stream=None, default_flow_style=False, indent=2,
              default_style=False, **kwds):
    """
    Serialize a Python object into a YAML stream.
    Produce only basic YAML tags.
    If stream is None, return the produced string instead.
    """
    return yaml.dump_all([data], stream, Dumper=SafeDumper,
                         default_flow_style=default_flow_style, indent=indent,
                         default_style=default_style, **kwds)
