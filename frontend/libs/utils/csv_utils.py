
import csv
import io

from frontend.libs.utils.string_utils import ensure_unicode


def standardize_newlines(content):
    if isinstance(content, bytes):
        content = ensure_unicode(content, ('utf-8', 'latin-1'))
    # This is a trick to make all different types of newlines readable.
    return "\n".join(content.splitlines())


def csv_join(iterable, delimiter=","):
    """Use a CSV writer to simulate a string join. This will prevent issues
     where the joined strings already contain the delimiter

    E.g. 'Smith. John, Jr,Robert Jones' vs. '"Smith. John, Jr",Robert Jones'
    """
    f = io.StringIO()
    csv.writer(f, delimiter=delimiter,
               lineterminator="").writerow(list(iterable))
    return f.getvalue()


def csv_split(string, delimiter=","):
    """Counterpart to the above CSV join. This will split a string that was
      joined using the CSV writer.
    """
    r = csv.reader([string], delimiter=delimiter)
    return next(r)


def clean_csv_bytes(content, return_bytes=True):
    # Remove the unicode null character
    content = content.replace(b'\x00', b'')
    content = standardize_newlines(content)
    content = content.replace('\ufeff', '')
    if return_bytes:
        return content.encode('utf-8')
    else:
        return content
