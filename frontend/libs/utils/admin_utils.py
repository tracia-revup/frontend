import logging

from django import forms
from django.contrib import admin
from django.db.models import Q
from django.forms.models import BaseInlineFormSet


LOGGER = logging.getLogger(__name__)


def generate_ownership_qs_filter(model_instance):
    """Compose a query for use on a QuerySet to filter results based on
    ownership of input record

    Input is expected to be an instance of a model that subclasses
    OwnershipModel.

    Returns a `Q` instance
    """
    account_id = model_instance.account_id
    user_id = model_instance.user_id
    fltr = Q(account__isnull=True)

    if account_id is not None:
        fltr = fltr | Q(account_id=account_id, user__isnull=True)

    if user_id is not None:
        fltr = fltr | Q(account_id=account_id, user_id=user_id)

    return fltr


class ChoiceLimitAdminForm(forms.ModelForm):
    """ChoiceLimitAdminForm is a special ModelForm that is used to filter
    relation choices based on parent record configuration.

    It was created because we want to filter available analysis configuration
    choices based on the ownership of the analysis configuration object we are
    modifying.

    It works by modifying the queryset used to populate the html form.
    Implementation based on this stackoverflow answer:
        http://stackoverflow.com/a/28901357/773976

    `choice_limit_filter_map` is a map of the field name with the queryset to
    modify with a value of either a dictionary, a Q object, or a callable
    returning a dictionary or Q object can be used. The callable should take a
    single argument, and will be passed the model instance for the form.
    """
    choice_limit_filter_map = None

    def get_parent_instance(self):
        """Overrideable method to get the parent model instance that is passed
        to callables found in `choice_limit_filter_map` map.
        """
        return self.instance

    def _apply_choice_limit_filters(self):
        if self.choice_limit_filter_map:
            for field, fltr in self.choice_limit_filter_map.items():
                if callable(fltr):
                    fltr = fltr(self.get_parent_instance())
                self.fields[field].queryset = \
                        self.fields[field].queryset.complex_filter(fltr)

    def __init__(self, *args, **kwargs):
        super(ChoiceLimitAdminForm, self).__init__(*args, **kwargs)
        try:
            if self.get_parent_instance():
                self._apply_choice_limit_filters()
        except Exception:
            LOGGER.exception("Error while trying to add filter to child "
                             "options! Most likely an expected parameter "
                             "is missing.")
            # XXX: It might be safe to just pass here. Can't be sure until we
            # see what (if anything) causes the above to throw an exception.
            raise


class ChoiceLimitAdminInlineFormSet(BaseInlineFormSet):
    """`ChoiceLimitAdminInlineFormSet` is meant to be used in conjunction with
    `ChoiceLimitAdminForm`.

    InlineFormSets are annoying and weird. They allow you to create new records
    based off another model that have a foreign key back to some parent record
    implicitly set. The inline form used to configure the fields on these new
    records do not have a reference to the parent record we need, they have a
    reference to the child record. For the most part this isn't an issue, we
    can just get the parent record via the child record's foreign key. Unless
    we're creating a new child record.

    When creating a new child record the form is initialized with an empty
    instance of the child model, and then the FormSet goes back and fills in
    the foreign key fields of the parent object. This unfortunately is too late
    for `ChoiceLimitAdminForm`, which needs this at instantiation time.
    """

    def _construct_form(self, i, **kwargs):
        """Override `_construct_form` to create a model instance with the
        foreign key already populated if we detect that this form field is for
        a new instance of the configured model.

        Vanilla django populates the foreign key field too late for
        `ChoiceLimitAdminForm` to use it.
        """
        if i >= self.initial_form_count() and not kwargs.get('instance'):
            kwargs['instance'] = self._preinit_model()
        return super(ChoiceLimitAdminInlineFormSet, self)\
                ._construct_form(i, **kwargs)

    def _preinit_model(self):
        """Initialize a new instance of this FormSet's model with the foreign
        key already set.

        The foreign key stuff comes directly from django itself in the
        django.forms.models:BaseInlineFormSet._construct_form() method.
        """
        fk_value = self.instance.pk
        if self.fk.remote_field.field_name != self.fk.remote_field.model._meta.pk.name:
            fk_value = getattr(self.instance, self.fk.remote_field.field_name)
            fk_value = getattr(fk_value, 'pk', fk_value)
        return self.model(**{self.fk.get_attname(): fk_value})

    @property
    def empty_form(self):
        """Override empty_form so the empty form it creates has a
        preinitialized model.
        """
        form = self.form(
            auto_id=self.auto_id,
            prefix=self.add_prefix('__prefix__'),
            empty_permitted=True,
            instance=self._preinit_model()
        )
        self.add_fields(form, None)
        return form


class UserAccountAdmin(admin.ModelAdmin):
    list_display = ('id', 'user_email', 'user_name', 'account_title')
    search_fields = ('user__email', 'user__last_name',
                     'account__organization_name')
    raw_id_fields = ("account", "user")

    def get_queryset(self, request):
        return super(UserAccountAdmin, self).get_queryset(request) \
            .select_related('user', 'account')

    def user_name(self, obj):
        return obj.user.name

    def user_email(self, obj):
        return obj.user.email

    def account_title(self, obj):
        return obj.account.title
