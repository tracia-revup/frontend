
from logging import getLogger
from multiprocessing import ProcessError

LOGGER = getLogger(__name__)


def multiprocess_joiner(processes):
    """Helper function to block until subprocesses are finished running

    Checks the child processes to see if any have exited unexpectedly, and if
    they have raise an exception. Otherwise this function blocks until all
    workers are finished.

    **NOTE** This function expects for all the child processes to have the
    attribute `daemon` set to True. If this is not done, then should this
    function raise an exception as a result of one of the children dying, the
    rest of the children won't die as well.
    """
    dead = set()
    while any(ps.is_alive() for ps in processes):
        for p in processes:
            p.join(timeout=15)
            if not p.is_alive():
                if p.exitcode != 0:
                    raise ProcessError(
                        "A worker process exited with non-zero status")
                elif p not in dead:
                    dead.add(p)
                    LOGGER.info("Worker subprocess finished. PID: %s",
                                p.pid)
