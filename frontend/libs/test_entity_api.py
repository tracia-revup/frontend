
from pytest import fixture, mark, lazy_fixture, param

from frontend.libs.utils.test_utils import expect
from .entity_api import EntityResolver


def describe_entity_resolver():
    entity_types = ('fec', 'misp', 'municipal', 'non_profit', 'real_estate',
                    'sec', 'opencorp', 'client_data')

    @fixture
    def account_key():
        return 'accountkey1'

    @fixture
    def person_entities():
        return [
            {},
            {
                "node_quality": {
                }
            },
            {
                "node_quality": {
                    "1": ["fusedid1", "fusedid2"],
                    "2": [],
                    "3": ["fusedid3"],
                    "4": ["fusedid3"],
                    "5": ["fusedid4"],
                    "6": ["fusedid5"],
                }
            },
            {
                "node_quality": {
                    "1": ["fusedid6", "fusedid7"],
                    "2": [],
                    "3": ["fusedid3"],
                    "4": ["fusedid5"],
                    "5": ["fusedid8"],
                    "6": ["fusedid9"],
                }
            }

        ]

    @fixture
    def fused_entity_ids():
        return [
            'fusedid1', 'fusedid2', 'fusedid3', 'fusedid4', 'fusedid5',
            'fusedid6', 'fusedid7', 'fusedid8', 'fusedid9',
        ]

    @fixture
    def fused_entities():
        return [
            {
                "_id": "fusedid2",
                "misp_id": ["mispentity1"],
                "fec_daily_id": ["fecentity1", "fecentity2"],
                "municipal_id": ["municipalentity1"],
                "non_profit_id": [],
                "real_estate_id": ["realestateentity1"],
                "sec_id": ["secentity1"],
                "opencorp_id": ["opencorpentity1", "opencorpentity2"],
                "client_data": {
                    "accountkey2": {
                        "_id": [
                            "wrong-acct-key1",
                            "wrong-acct-key2",
                        ]
                    }
                },
            },
            {
                "_id": "fusedid2",
                "misp_id": ["mispentity2", "mispentity1"],
                "fec_daily_id": [],
                "municipal_id": [],
                "non_profit_id": ["nfpentity1"],
                "real_estate_id": ["realestateentity2"],
                "sec_id": ["secentity2"],
                "opencorp_id": ["opencorpentity2", "opencorpentity3"],
                "client_data": {
                    "accountkey1": {
                        "_id": [
                            "cliententity1"
                        ]
                    }
                },
            }
        ]

    @fixture
    def misp_entity_ids():
        return ["mispentity1", "mispentity2"]

    @fixture
    def misp_entities():
        return [
            {
                "_id": "mispentity1",
                "record_links": [
                    "misp1",
                    "misp2",
                    "misp3"
                ]
            },
            {
                "_id": "mispentity2",
                "record_links": [
                    "misp3",
                    "misp4",
                    "misp5"
                ]
            }
        ]

    @fixture
    def misp_ids():
        return ["misp1", "misp2", "misp3", "misp4", "misp5",]

    @fixture
    def fec_entity_ids():
        return ["fecentity1", "fecentity2"]

    @fixture
    def fec_entities():
        return [
            {
                "_id": "fecentity1",
                "record_links": [
                    "fec1",
                    "fec2",
                    "fec3"
                ]
            },
            {
                "_id": "fecentity2",
                "record_links": [
                    "fec3",
                    "fec4",
                    "fec5"
                ]
            }
        ]

    @fixture
    def fec_ids():
        return ["fec1", "fec2", "fec3", "fec4", "fec5",]

    @fixture
    def municipal_entity_ids():
        return ["municipalentity1"]

    @fixture
    def municipal_entities():
        return [
            {
                "_id": "municipalentity1",
                "record_links": [
                    "municipal1",
                    "municipal2",
                    "municipal3"
                ]
            },
        ]

    @fixture
    def municipal_ids():
        return ["municipal1", "municipal2", "municipal3"]

    @fixture
    def non_profit_entity_ids():
        return ["nfpentity1"]

    @fixture
    def non_profit_entities():
        return [
            {
                "_id": "nfpentity1",
                "record_links": [
                    "nfp1",
                    "nfp2",
                    "nfp3"
                ]
            },
        ]

    @fixture
    def non_profit_ids():
        return ["nfp1", "nfp2", "nfp3"]

    @fixture
    def real_estate_entity_ids():
        return [ "realestateentity1", "realestateentity2", ]

    @fixture
    def real_estate_entities():
        return [
            {
                "_id": "realestateentity1",
                "record_links": [
                    "realestate1",
                    "realestate2",
                    "realestate3"
                ]
            },
            {
                "_id": "realestateentity2",
                "record_links": [
                    "realestate3",
                    "realestate4",
                    "realestate5"
                ]
            }
        ]

    @fixture
    def real_estate_ids():
        return ["realestate1", "realestate2", "realestate3", "realestate4", "realestate5",]

    @fixture
    def sec_entity_ids():
        return [ "secentity1", "secentity2", ]

    @fixture
    def sec_entities():
        return [
            {
                "_id": "secentity1",
                "record_links": [
                    "sec1",
                    "sec2",
                    "sec3"
                ]
            },
            {
                "_id": "secentity2",
                "record_links": [
                    "sec3",
                    "sec4",
                    "sec5"
                ]
            }
        ]

    @fixture
    def sec_ids():
        return ["sec1", "sec2", "sec3", "sec4", "sec5",]

    @fixture
    def opencorp_entity_ids():
        return [
            "opencorpentity1",
            "opencorpentity2",
            "opencorpentity3"
        ]

    @fixture
    def opencorp_entities():
        return [
            {
                "_id": "opencorpentity1",
                "record_links": [
                    "opencorp1",
                    "opencorp2",
                    "opencorp3"
                ]
            },
            {
                "_id": "opencorpentity2",
                "record_links": [
                    "opencorp3",
                    "opencorp4",
                    "opencorp5"
                ]
            },
            {
                "_id": "opencorpentity3",
                "record_links": [
                    "opencorp6",
                    "opencorp7",
                    "opencorp8"
                ]
            }
        ]

    @fixture
    def opencorp_ids():
        return [
            "opencorp1", "opencorp2", "opencorp3", "opencorp4", "opencorp5",
            "opencorp6", "opencorp7", "opencorp8",
        ]

    @fixture
    def client_data_entity_ids():
        return ["cliententity1"]

    @fixture
    def client_data_entities():
        return [
            {
                "_id": "cliententity1",
                "record_links": [
                    "client1",
                    "client2",
                    "client3"
                ]
            }
        ]

    @fixture
    def client_data_ids():
        return ["client1", "client2", "client3"]

    @fixture
    def query_mock(mocker, fused_entities, fec_entities, misp_entities,
                   municipal_entities, non_profit_entities,
                   real_estate_entities, sec_entities, opencorp_entities,
                   client_data_entities, account_key):
        def _query_sideeffect(collection_name, *args, **kwargs):
            return {
                'fused_entities': fused_entities,
                'fec_entities': fec_entities,
                'misp_entities': misp_entities,
                'municipal_entities': municipal_entities,
                'non_profit_entities': non_profit_entities,
                'real_estate_entities': real_estate_entities,
                'sec_entities': sec_entities,
                'opencorp_entities': opencorp_entities,
                'client_entities__{}'.format(account_key): client_data_entities,
            }[collection_name]
        query_mock = mocker.patch.object(EntityResolver, '_query',
                                         side_effect=_query_sideeffect)
        return query_mock

    @fixture
    def entity_resolver(mocker, person_entities, account_key):
        er = EntityResolver.factory(person_entities, mocker.MagicMock(),
                                    account_key=account_key)
        return er

    def it_extracts_and_dedupes_fused_entity_ids(person_entities,
                                                 fused_entity_ids):
        er = EntityResolver(person_entities, None)
        expect(er.fused_entity_ids).items_equal(fused_entity_ids)

    def it_fetches_fused_entities(query_mock, person_entities,
                                  fused_entity_ids, fused_entities):
        er = EntityResolver(person_entities, None)
        query_mock.assert_not_called()
        expect(er.fused_entities) == fused_entities
        query_mock.assert_called_once_with('fused_entities',
                                           set(fused_entity_ids))

    def it_displays_client_data_summaries_with_empties_removed(
            query_mock, entity_resolver, account_key, fused_entities):
        expect(entity_resolver.fused_client_summary) == [fused_entities[1]['client_data'][account_key]]

    # Run the decorated test once for each `param` in the list, providing the
    # arguments in `param` as arguments to the test.
    @mark.parametrize('entity_type, fused_field, entity_ids', [
        param('fec', 'fec_daily_id', lazy_fixture('fec_entity_ids'), id='fec'),
        param('misp', 'misp_id', lazy_fixture('misp_entity_ids'), id='misp'),
        param('municipal', 'municipal_id',
              lazy_fixture('municipal_entity_ids'), id='municipal'),
        param('non_profit', 'non_profit_id',
              lazy_fixture('non_profit_entity_ids'), id='non_profit'),
        param('real_estate', 'real_estate_id',
              lazy_fixture('real_estate_entity_ids'), id='real_estate'),
        param('sec', 'sec_id', lazy_fixture('sec_entity_ids'), id='sec'),
        param('opencorp', 'opencorp_id',
              lazy_fixture('opencorp_entity_ids'), id='opencorp'),
        param('client_data', 'client_data.{}._id'.format(account_key()),
              lazy_fixture('client_data_entity_ids'), id='client_data'),
    ])
    def it_extracts_entity_ids_from_fused(mocker, query_mock, entity_resolver,
                                          entity_type, fused_field,
                                          entity_ids):
        entity_id_extract_spy = mocker.spy(entity_resolver,
                                           '_extract_entity_ids')
        id_extract_spy = mocker.spy(entity_resolver, '_extract_ids')
        expect(getattr(entity_resolver,
                       '{}_entity_ids'.format(entity_type))).items_equal(entity_ids)
        # Verify the entity type is correctly extracted from the property name,
        # and the expected internal method is run.
        entity_id_extract_spy.assert_called_once_with(entity_type, mocker.ANY)
        # Verify that the entity type is transformed into the correct fused
        # field, and respects overrides specified in `fused_field_mapping`
        id_extract_spy.assert_called_once_with(fused_field, mocker.ANY)

    # Run the decorated test once for each `param` in the list, providing the
    # arguments in `param` as arguments to the test.
    @mark.parametrize('entity_type, entity_ids, entities', [
        param('fec', lazy_fixture('fec_entity_ids'),
              lazy_fixture('fec_entities'), id='fec'),
        param('misp', lazy_fixture('misp_entity_ids'),
              lazy_fixture('misp_entities'), id='misp'),
        param('municipal', lazy_fixture('municipal_entity_ids'),
              lazy_fixture('municipal_entities'), id='municipal'),
        param('non_profit', lazy_fixture('non_profit_entity_ids'),
              lazy_fixture('non_profit_entities'), id='non_profit'),
        param('real_estate', lazy_fixture('real_estate_entity_ids'),
              lazy_fixture('real_estate_entities'), id='real_estate'),
        param('sec', lazy_fixture('sec_entity_ids'),
              lazy_fixture('sec_entities'), id='sec'),
        param('opencorp', lazy_fixture('opencorp_entity_ids'),
              lazy_fixture('opencorp_entities'), id='opencorp'),
    ])
    def it_fetches_subentities_using_extracted_ids(query_mock, entity_resolver,
                                                   mocker, entity_type,
                                                   entity_ids, entities):
        # force fetch of fused entities
        entity_resolver.fused_entities
        query_mock.reset_mock()

        fetch_spy = mocker.spy(entity_resolver, '_fetch_entities')
        query_mock.assert_not_called()
        expect(getattr(entity_resolver, '{}_entities'.format(entity_type))) == entities
        fetch_spy.assert_called_once_with(entity_type, set(entity_ids))
        query_mock.assert_called_once_with('{}_entities'.format(entity_type), set(entity_ids))

    # Run the decorated test once for each `param` in the list, providing the
    # arguments in `param` as arguments to the test.
    @mark.parametrize('entity_type, contrib_ids', [
        param(entity_type, lazy_fixture('{}_ids'.format(entity_type)),
              id=entity_type)
        for entity_type in entity_types
    ])
    def it_extracts_contrib_ids(query_mock, entity_resolver, entity_type,
                                contrib_ids):
        # Verify recursive resolution by showing none of the intermediate
        # collections are present to begin with
        expect(entity_resolver.__dict__.keys()).excludes('fused_entities')
        expect(entity_resolver.__dict__.keys()).excludes('{}_entity_ids'.format(entity_type))
        expect(entity_resolver.__dict__.keys()).excludes('{}_entities'.format(entity_type))
        expect(entity_resolver.__dict__.keys()).excludes('{}_ids'.format(entity_type))
        expect(getattr(entity_resolver, '{}_ids'.format(entity_type))).items_equal(contrib_ids)
        # Backwards compatibility - just using the entity type works to get the
        # contrib ids as well. And dict-like access also works.
        expect(entity_resolver[entity_type]).items_equal(contrib_ids)
        expect(entity_resolver.get(entity_type)).items_equal(contrib_ids)
        # And now they are present
        expect(entity_resolver.__dict__.keys()).contains('fused_entities')
        expect(entity_resolver.__dict__.keys()).contains('{}_entity_ids'.format(entity_type))
        expect(entity_resolver.__dict__.keys()).contains('{}_entities'.format(entity_type))
        expect(entity_resolver.__dict__.keys()).contains('{}_ids'.format(entity_type))

    def it_raises_errors_on_invalid_attributes(query_mock, entity_resolver):
        # Random property names and entity property that contain an invalid
        # entity type all raise AttributeErrors
        with expect.raises(AttributeError):
            entity_resolver.qwelj
        with expect.raises(AttributeError):
            entity_resolver.fluffy_entity_ids
        with expect.raises(AttributeError):
            entity_resolver.blarg_entities
        with expect.raises(AttributeError):
            entity_resolver.honk_ids

    def it_raises_errors_on_invalid_items(query_mock, entity_resolver):
        # Random item keys and entity keys that contain an invalid
        # entity type all raise KeyErrors
        with expect.raises(KeyError):
            entity_resolver['qwelj']
        with expect.raises(KeyError):
            entity_resolver['fluffy_entity_ids']
        with expect.raises(KeyError):
            entity_resolver['blarg_entities']
        with expect.raises(KeyError):
            entity_resolver['honk_ids']
