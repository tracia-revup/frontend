import datetime
from django.conf import settings

import mongoengine


class CloudSpongeCache(mongoengine.Document):
    query_type = mongoengine.StringField()
    import_id = mongoengine.StringField()
    query_dt = mongoengine.DateTimeField(default=datetime.datetime.now)
    data = mongoengine.DictField()
    env = mongoengine.StringField(default=settings.ENV)

    extra_info = mongoengine.DictField()

    meta = {
        'indexes': [
            "import_id",
            "extra_info.user_id",
            "env",
            {'fields': ['query_dt'],
             "expireAfterSeconds": 604800}  # TTL one week
        ]
    }


