from collections import namedtuple

from django.conf import settings
import requests

#from .models import CloudSpongeCache


ContactEmail = namedtuple("ContactEmail", "address type primary")
ContactPhone = namedtuple("ContactPhone", "number type")
ContactAddress = namedtuple("ContactAddress",
                            "street city region country postal_code type")


class Contact(object):
    def __init__(self, json):
        jg = lambda field: json.get(field, "")
        self.json = json
        self.first_name = jg('first_name')
        self.last_name = jg('last_name')

        self.emails = [self._build_email(email) for email in jg("email")]
        self.phones = [self._build_phone(phone) for phone in jg("phone")]
        self.addresses = [self._build_address(address)
                          for address in jg("address")]

    @classmethod
    def _build_email(cls, email):
        return ContactEmail(email.get("address", ""), email.get("type", ""),
                            email.get("primary", False))

    @classmethod
    def _build_phone(cls, phone):
        # Numbers seem to be stored in two possible places. This checks both.
        num = phone.get("number") or phone.get("value", "")
        return ContactPhone(num, phone.get("type", ""))

    @classmethod
    def _build_address(cls, address):
        pg = lambda field: address.get(field, "")
        return ContactAddress(pg("street"), pg("city"), pg("region"),
                              pg("country"), pg("postal_code"), pg("type"))


class CloudSpongeAPIException(Exception): pass


class CloudSponge(object):
    _domain_key = settings.CLOUDSPONGE_DOMAIN_KEY
    _domain_password = settings.CLOUDSPONGE_DOMAIN_PASSWORD

    class URLs(object):
        contacts_json = "https://api.cloudsponge.com/contacts.json/{import_id}"

    def __init__(self, domain_key=_domain_key,
                 domain_password=_domain_password,
                 cache_query=settings.CLOUDSPONGE_CACHE_QUERY):
        self.domain_key = domain_key
        self.domain_password = domain_password
        self.cache_query = cache_query

    def _query_api(self, method, import_id, **kwargs):
        url = getattr(self.URLs, method).format(import_id=import_id)
        response = requests.get(
            url, params={"domain_key": self.domain_key,
                         "domain_password": self.domain_password})
        data = response.json()

        #if self.cache_query:
            #CloudSpongeCache.objects.create(
                #query_type=method,
                #import_id=import_id,
                #data=data,
                #extra_info=kwargs,
            #)
        error = data.get('error')
        if error:
            raise CloudSpongeAPIException(error)
        return data

    def import_contacts(self, import_id, **kwargs):
        json = self._query_api("contacts_json", import_id, **kwargs)
        for contact in json.get('contacts'):
            yield Contact(contact)
