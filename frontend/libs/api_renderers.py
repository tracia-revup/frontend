from collections import OrderedDict

from bson import ObjectId
from rest_framework import renderers
from rest_framework.utils.serializer_helpers import ReturnList
from rest_framework.utils import encoders


class JSONEncoder(encoders.JSONEncoder):
    """A JSON encoder to add serialization support for custom types."""
    def default(self, obj):
        if isinstance(obj, ObjectId):
            return str(obj)
        return super(JSONEncoder, self).default(obj)


class CustomJSONRenderer(renderers.JSONRenderer):
    """Simulate the behavior of pagination if not paginating.

    DRF returns a list of objects if the view is not paginated. This renderer
    changes that to always have the paginated format, even when not paginated.

    The point of this is to make API format consistent across APIs.
    """
    encoder_class = JSONEncoder

    def render(self, data, accepted_media_type=None, renderer_context=None):
        if data is None:
            data = []

        if isinstance(data, (list, tuple, set, ReturnList)):
            custom_data = OrderedDict((
                ('count', len(data)),
                ('next', None),
                ('previous', None),
                ('results', data)
            ))
        else:
            custom_data = data

        return super(CustomJSONRenderer, self).render(
            custom_data, accepted_media_type, renderer_context)


class NoHTMLFormBrowsableAPIRenderer(renderers.BrowsableAPIRenderer):
    """
        We don't want the HTML forms to be rendered because it can be
        really slow with large datasets
    """
    def get_rendered_html_form(self, *args, **kwargs):
        return ""
