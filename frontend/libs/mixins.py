

class SimpleUnicodeMixin(object):
    """Defines a simple unicode method for a Model.

    Example output:
        RevUpUser: <id>
    """
    def __str__(self):
        try:
            return "{}: '{}'".format(self.__class__.__name__,
                                      self.pk)
        except Exception:
            # If this isn't actually a model being called, let's
            # try to fail gracefully
            return str(self.__class__.__name__)
