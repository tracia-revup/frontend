from django.db.models import F

from frontend.apps.role.defaults import (Admin, AdminPermissions,
                                         FundraiserPermissions)



def run(apps=None, *args):
    """Update the Admin role with new permissions, and update any Admin seats.

    This is useful for when a new permission item has been added to the
    Admin role. Typically, this method is called from a migration, but
    could be called from the shell.
    """
    if apps:
        Role = apps.get_model('role', 'Role')
        Seat = apps.get_model('seat', 'Seat')
    else:
        from frontend.apps.role.models import Role
        from frontend.apps.seat.models import Seat

    admin_role = Admin()

    print("Updating Admin Seats")
    for admin in Role.objects.filter(name=admin_role['name']):
        # Update all admin roles with the new permissions
        admin.permissions = admin_role['permissions']
        admin.description = admin_role['description']
        admin.save()

        # Update every seat with the admin role with the new permissions
        for seat in Seat.objects.filter(_roles=admin):
            seat.permissions = admin.permissions
            seat.save()

    print("Update the account heads")
    # Update the account owners
    for seat in Seat.objects.filter(account__account_head=F("user")):
        seat.permissions = (FundraiserPermissions.all() +
                            AdminPermissions.all())
        seat.save()

    print("Done.")
