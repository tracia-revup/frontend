"""Define some very common roles to be used as defaults throughout the app.

Accounts of will be given a default set of roles to assign to
their fundraisers. Different types of accounts may each have a
different set of common roles.

When we define the "role" below, we are using a dict instead of
a Role instance because we don't want to pass around a global
Role. It will certainly get mishandled somewhere.
"""

from .permissions import AdminPermissions, FundraiserPermissions


def generate_role(name_, description_, permissions_):
    def role_dict_closure(name=name_, description=description_,
                          permissions=permissions_, default=False):
        return dict(name=name, description=description,
                    permissions=permissions,
                    is_a_seat_default=default)
    return role_dict_closure


Admin = generate_role(
    "Admin",
    "The user may perform any action within the Account Admin interface, "
    "including viewing all admin pages and editing all options and content. "
    "The user may also access all pages within the Fundraiser interface.",
    AdminPermissions.all() + FundraiserPermissions.all()
)

Fundraiser = generate_role(
    "Fundraiser",
    "The user may view any page or perform any action within the fundraiser "
    "portion of the site, including loading contacts and viewing their "
    "ranking, and they may view any data available to them.",
    FundraiserPermissions.all()
)

Restricted_Fundraiser = generate_role(
    "Restricted Fundraiser",
    "The user may view any page or perform any action within the fundraiser "
    "portion of the site, including loading contacts and viewing their "
    "ranking; however, their data visibility is restricted.",
    FundraiserPermissions.RESTRICTED
)
