
import factory
import factory.fuzzy

from frontend.apps.role.models import Role
from frontend.apps.role.permissions import FundraiserPermissions


@factory.use_strategy(factory.CREATE_STRATEGY)
class RoleFactory(factory.DjangoModelFactory):
    class Meta:
        model = Role

    account = factory.SubFactory(
        "frontend.apps.campaign.factories.PoliticalCampaignFactory")
    name = factory.fuzzy.FuzzyText()
    permissions = FundraiserPermissions.all()
