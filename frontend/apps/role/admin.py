
from django.contrib import admin

from .models import Role


class RoleAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'account_name')

    def get_queryset(self, request):
        return super(RoleAdmin, self).get_queryset(request).select_related(
            'account')

    def account_name(self, obj):
        return obj.account.organization_name if obj.account else ''


admin.site.register(Role, RoleAdmin)


