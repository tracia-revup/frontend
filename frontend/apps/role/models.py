from django.db import models

from frontend.libs.mixins import SimpleUnicodeMixin
from django.contrib.postgres import fields


class Role(models.Model, SimpleUnicodeMixin):
    account = models.ForeignKey("campaign.Account", on_delete=models.CASCADE,
                                related_name="roles", null=True)
    name = models.CharField(max_length=64, blank=False)
    description = models.TextField(blank=True)
    is_a_seat_default = models.BooleanField(
        blank=True, default=False,
        help_text="Should this role be used as a default when creating a new "
                  "seat, if no roles have been specified? Typically, this "
                  "would be for your base-level fundraisers. "
    )
    permissions = fields.ArrayField(
        models.CharField(max_length=3),
        default=[], blank=True,
        help_text="The permissions associated-with/assigned-to this role."
    )
