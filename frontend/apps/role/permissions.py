"""Contains the Permissions Choices classes.

IMPORTANT NOTE: Avoid collisions across all Choices classes. They
    are all lumped into one field called 'seat.permissions'. You may
    use up to 3 characters.
"""
from frontend.libs.utils.model_utils import Choices


class PermissionError(Exception): pass


class AdminPermissions(Choices):
    choices_map = [
        # Account Permissions
        ("AAC", "MOD_ACCOUNT", "Modify Account Info"),

        # Event Permissions
        ("AET", "EV_TRACKER", "View Event Tracker"),
        ("AME", "MOD_EVENTS", "Modify Events"),
        ("AVE", "VIEW_EVENTS", "View Account Events"),
        ("AMP", "MOD_PROSPECTS", "Modify Fundraisers' Prospects"),
        ("AVP", "VIEW_PROSPECTS", "View Fundraisers' Prospects"),

        # Fundraiser Permissions
        ("AMF", "MOD_FUNDRAISERS", "Modify Fundraisers"),
        ("AVF", "VIEW_FUNDRAISERS", "View All Fundraisers"),
        ("ATR", "FR_TRACKER", "View Fundraiser Tracker"),

        # Management Group Permissions
        ("AMG", "MOD_MG", "Manage Management-Groups"),

        # Role Permissions
        ("ACR", "CONFIG_ROLES", "Create/Delete Account Roles"),
        ("AMR", "MOD_ROLES", "Modify Seat Roles"),

        # Feature Permissions
        ("ACF", "CONFIG_FEATURES", "Modify Analysis Features"),

        # Skinning Permissions
        ("ACS", "CONFIG_SKINS", "Modify Skinning Settings"),

        # Account-level Contacts Permissions
        ("ACM", "MODIFY_CONTACTS", "Modify/Create Account-level Contacts"),
        ("ACV", "VIEW_CONTACTS", "View Account-level Contacts"),

        # Call Time Contacts Permissions
        ("ACG", "MODIFY_CALLGROUPS", "Modify/Create Call Time Groups"),
        ("AMT", "MODIFY_CALLTIME", "Modify/View Call Time Contacts"),

        # Manage Lists
        ("AML", "MODIFY_LISTS", "Modify Lists"),

        # Persona Permissions
        ("APM", "MODIFY_PERSONAS", "Modify/Create Personas"),

        # Other
        ("ALD", "LOAD_DATA", "Load External Data"),
    ]


class FundraiserPermissions(Choices):
    choices_map = [
        ("FID", "INTERNAL_DATA", "View Internal/Private Data"),
        ("FIS", "SUMMARY_DATA", "View Summaries of Internal Data"),
        ("FPD", "PUBLIC_DATA", "View Public Data"),
        # Export CSV Files
        ("FEC", "EXPORT_CSV_FILES", "Export CSV Files"),
    ]
    RESTRICTED = ("FPD", "FIS")
