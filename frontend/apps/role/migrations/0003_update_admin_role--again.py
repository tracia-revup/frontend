# -*- coding: utf-8 -*-


from django.db import models, migrations

from ..scripts import admin_role_forwards


class Migration(migrations.Migration):

    dependencies = [
        ('role', '0002_update_admin_role'),
    ]

    operations = [
        migrations.RunPython(admin_role_forwards.run,
                             reverse_code=lambda x,y: True)
    ]
