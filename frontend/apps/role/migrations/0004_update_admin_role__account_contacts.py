# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-08-05 14:06


from django.db import migrations

from ..scripts import admin_role_forwards


class Migration(migrations.Migration):

    dependencies = [
        ('role', '0003_update_admin_role--again'),
    ]

    operations = [
        migrations.RunPython(admin_role_forwards.run,
                             reverse_code=lambda x, y: True)
    ]
