# -*- coding: utf-8 -*-

import itertools

from django.db import models, migrations
import django.contrib.postgres.fields


def create_roles(apps, schema_editor):
    """For each existing account, we need to setup the default roles available
    to them.
    """
    from frontend.apps.role.defaults import Admin, Fundraiser
    PoliticalCampaign = apps.get_model('campaign', 'PoliticalCampaign')
    University = apps.get_model('campaign', 'University')
    Role = apps.get_model('role', 'Role')

    for account in itertools.chain(PoliticalCampaign.objects.all(),
                                   University.objects.all()):
        for role in (Fundraiser, Admin):
            kwargs = role()
            if role is Fundraiser:
                kwargs['is_a_seat_default'] = True
            r = Role(**kwargs)
            r.account_id = account.pk
            r.save()


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0011_university'),
    ]

    operations = [
        migrations.CreateModel(
            name='Role',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64)),
                ('description', models.TextField(blank=True)),
                ('is_a_seat_default', models.BooleanField(default=False, help_text='Should this role be used as a default when creating a new seat, if no roles have been specified? Typically, this would be for your base-level fundraisers. ')),
                ('permissions', django.contrib.postgres.fields.ArrayField(models.CharField(max_length=3), default=[], help_text='The permissions associated-with/assigned-to this role.', size=None, blank=True)),
                ('account', models.ForeignKey(related_name='roles', to='campaign.Account', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='role',
            unique_together=set([('account', 'name')]),
        ),
        migrations.RunPython(
            create_roles,
            reverse_code=lambda x, y: True
        )
    ]
