# -*- coding: utf-8 -*-


from django.db import migrations

from ..scripts import admin_role_forwards


class Migration(migrations.Migration):

    dependencies = [
        ('role', '0004_update_admin_role__account_contacts'),
    ]

    operations = [
        migrations.RunPython(admin_role_forwards.run,
                             reverse_code=lambda x, y: True)
    ]
