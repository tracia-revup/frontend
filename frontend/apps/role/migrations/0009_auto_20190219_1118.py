# Generated by Django 2.0.5 on 2019-02-19 11:18
from django.db import migrations
from frontend.apps.role.permissions import (
    AdminPermissions, FundraiserPermissions
)


def migrate_np_permissions(apps, schema_editor):
    """Migrate permissions for Nonprofit"""
    Role = apps.get_model("role", "Role")
    AccountProfile = apps.get_model("campaign", "AccountProfile")

    # Get the nonprofit account.
    np_account_profile = AccountProfile.objects.get(title="Non-Profit Premium")

    # Create the admin role for nonprofit.
    np_admin_role, _ = Role.objects.get_or_create(
        account=None,
        name="Admin",
        description="The user may perform any action within the "
                    "Account Admin interface, including viewing all admin "
                    "pages and editing all options and content. The user may "
                    "also access all pages within the Fundraiser interface.",
        is_a_seat_default=False,
        permissions=AdminPermissions.all() + FundraiserPermissions.all()
    )

    # Create the fundraisers/users role for nonprofit.
    np_fundraiser_role, _ = Role.objects.get_or_create(
        account=None,
        name="Fundraiser",
        description="The user may view any page or perform any action within "
                    "the fundraiser portion of the site, including viewing "
                    "their ranking and managing lists.",
        is_a_seat_default=False,
        permissions=[
            FundraiserPermissions.INTERNAL_DATA,
            FundraiserPermissions.SUMMARY_DATA,
            FundraiserPermissions.PUBLIC_DATA,
            AdminPermissions.MODIFY_LISTS,
            AdminPermissions.VIEW_CONTACTS
        ]
    )

    np_account_profile.roles.clear()
    np_account_profile.roles.add(np_admin_role)
    np_account_profile.roles.add(np_fundraiser_role)


def reverse_np_permissions(apps, schema_editor):
    """Reverse the create permissions for nonprofit"""
    Role = apps.get_model("role", "Role")
    AccountProfile = apps.get_model("campaign", "AccountProfile")

    # Get the nonprofit account.
    np_account_profile = AccountProfile.objects.get(title="Non-Profit Premium")
    np_account_profile.roles.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('role', '0008_auto_20190108_1056'),
    ]

    operations = [
        migrations.RunPython(
            migrate_np_permissions, reverse_np_permissions
        )
    ]
