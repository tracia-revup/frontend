# -*- coding: utf-8 -*-


from django.db import models, migrations

from ..scripts import admin_role_forwards


class Migration(migrations.Migration):

    dependencies = [
        ('role', '0001_initial'),
        ('seat', '0005_auto_20150423_2343'),
    ]

    operations = [
        migrations.RunPython(admin_role_forwards.run,
                             reverse_code=lambda x,y: True)
    ]
