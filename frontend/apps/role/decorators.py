import logging
from functools import wraps

from django.shortcuts import redirect
from django.urls import reverse
from django.utils.decorators import available_attrs


def _group_required(user, account=None, **kwargs):
    # TODO: I'm not sure this is a good idea, but I don't want to forget
    #      we used to allow this, so I will leave this code commented.
    # if user.is_admin:
    #     return True
    current_seat = user.current_seat
    # Active seat is required for access
    if current_seat:
        # Reject if an account is given and it doesn't match the current seat
        if account and current_seat.account_id != account:
            return False
        return current_seat.has_permissions(**kwargs)
    else:
        return False


def check_permissions(test_func):
    def decorator(view_func):
        @wraps(view_func, assigned=available_attrs(view_func))
        def _wrapped_view(request, *args, **kwargs):
            if test_func(request.user, *args, **kwargs):
                return view_func(request, *args, **kwargs)
            logging.error('Permission failure: {}'.format(request))
            return redirect(reverse('index') + '?msg=permission_failure')
        return _wrapped_view
    return decorator


def _build_group_required_decorator(group_required_kwarg, account_field,
                                    group_names):
    """Helper method to keep code DRY.
    This code was duplicated in the two group_required methods below, but the
    only difference was the group_required_kwarg.
    """
    def groups_in_seat(user, *args, **kwargs):

        if account_field:
            try:
                account = int(kwargs.get(account_field))
            except (TypeError, ValueError):
                # If the account_field option is specified, but no account was
                # given, we have to reject or we create a potential security
                # hole.
                return False
        else:
            account = None

        group_required_kwargs = {group_required_kwarg: group_names}
        return _group_required(user, account=account, **group_required_kwargs)
    return groups_in_seat


def any_group_required(*group_names, account_field=None):
    """Requires user's active-seat membership in any of the given
       permission groups.
    """
    return check_permissions(_build_group_required_decorator(
                                        "any", account_field, group_names))


def all_groups_required(*group_names, account_field=None):
    """Requires user's active-seat membership in all of the given
       permission groups.
    """
    return check_permissions(_build_group_required_decorator(
                                        "all", account_field, group_names))
