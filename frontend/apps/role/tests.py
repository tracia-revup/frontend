
from django.urls import reverse

from frontend.apps.authorize.test_utils import AuthorizedUserTestCase
from frontend.apps.campaign.factories import PoliticalCampaignFactory
from frontend.apps.role.factories import RoleFactory
from frontend.apps.role.models import Role
from frontend.apps.role.permissions import AdminPermissions


class AccountRolesViewSetTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        self.campaign = PoliticalCampaignFactory()
        self.campaign.roles.all().delete()
        self.role = RoleFactory(account=self.campaign)
        self.global_role = RoleFactory(account=None)
        self.campaign.account_profile.roles.add(self.role)
        super(AccountRolesViewSetTests, self).setUp(
            login_now=False, campaigns=[self.campaign])
        self.url = reverse('roles_api-list',
                           args=(self.campaign.id,))
        self.url2 = reverse('roles_api-detail',
                            args=(self.campaign.id, self.role.id))

    def test_permissions(self):
        self.assertEqual(self.campaign.roles.count(), 1)
        ## Verify any unauthenticated user is rejected
        result = self.client.get(self.url)
        self.assertNoCredentials(result)
        result = self.client.post(self.url, data={"abc": "123"})
        self.assertNoCredentials(result)
        result = self.client.get(self.url2)
        self.assertNoCredentials(result)

        # Login the user
        self._login(self.user)
        # Verify user is not an admin, or that would invalidate the test.
        self.assertFalse(self.user.is_admin)
        self.assertFalse(self.user.is_account_admin())

        ## Verify a standard user is rejected from viewing account roles
        result = self.client.get(self.url)
        self.assertNoPermission(result)
        ## Verify a standard user is rejected from adding new roles
        result = self.client.post(self.url, data={"abc": "123"})
        self.assertNoPermission(result)
        ## Verify a standard user is rejected from deleting roles
        result = self.client.delete(self.url2)
        self.assertNoPermission(result)

        # Add an invalid admin permission and verify access is not granted
        self._add_admin_group(permissions=AdminPermissions.MOD_ACCOUNT)
        result = self.client.get(self.url)
        self.assertNoPermission(result)

        # Add the correct admin permission and verify access is granted
        # For GET only
        self._add_admin_group(permissions=AdminPermissions.MOD_ROLES)
        result = self.client.get(self.url)
        self.assertContains(result, '"id":{}'.format(self.role.id))
        # Verify Mod roles cannot delete
        result = self.client.delete(self.url2)
        self.assertNoPermission(result)

        self.user.current_seat.permissions = []
        self.user.current_seat.save()
        self.assertFalse(self.user.is_account_admin())

        self._add_admin_group(permissions=AdminPermissions.CONFIG_ROLES)
        result = self.client.get(self.url)
        self.assertContains(result, '"id":{}'.format(self.role.id))
        self.assertContains(result, '"name":"{}"'.format(self.role.name))
        # Verify this permission can delete
        result = self.client.delete(self.url2)
        self.assertContains(result, "", status_code=204)
        self.assertFalse(self.campaign.roles.exists())

    def test_create(self):
        self._login()
        self._add_admin_group(permissions=AdminPermissions.CONFIG_ROLES)

        ## Verify error cases
        response = self.client.post(self.url, data={"abc": 123})
        self.assertContains(response, "'name' is required", status_code=500)

        name = "Test Role"
        description = "Test Description"
        is_a_seat_default = True
        permissions = AdminPermissions.all()

        # Verify valid permissions must be used
        response = self.client.post(self.url, data={
            "name": name,
            "description": description,
            "is_a_seat_default": is_a_seat_default,
            "permissions": permissions[:2] + ["FAK"]})
        self.assertContains(response, "Invalid permissions", status_code=500)

        # Verify positive cases
        response = self.client.post(self.url, data={
            "name": name,
            "description": description,
            "is_a_seat_default": is_a_seat_default,
            "permissions": permissions})
        self.assertContains(response, name)
        self.assertContains(response, description)
        self.assertTrue(response.data["is_a_seat_default"])
        for permission in permissions:
            self.assertContains(response, permission)

    def test_serializer(self):
        self._login_and_add_admin_group(permissions=AdminPermissions.MOD_ROLES)
        response = self.client.get(self.url)
        self.verify_serializer(response, (
            "id", "name", "permissions", "description", "is_a_seat_default"))

    def test_correct_roles_returned(self):
        self._login_and_add_admin_group(permissions=AdminPermissions.MOD_ROLES)
        response = self.client.get(self.url)

        role_ids = [r.get('id') for r in response.data]

        # test that global roles not in account profile are not returned
        roles_not_in_account_profile = \
            [r.id for r in Role.objects.
                exclude(id__in=self.campaign.account_profile.roles.all())]
        self.assertNotEqual(role_ids, roles_not_in_account_profile)

        # test roles in account profile are returned
        roles_in_account_profile = \
            [r.id for r in Role.objects.
                filter(id__in=self.campaign.account_profile.roles.all())]
        self.assertEqual(role_ids, roles_in_account_profile)

        # test roles not assigned to my account are not included
        roles_not_in_account = [r.id for r in Role.objects.exclude(
            account=self.campaign)]
        self.assertNotEqual(role_ids, roles_not_in_account)

        # test roles assigned to my account are included
        roles_in_account = [r.id for r in Role.objects.filter(
            account=self.campaign
        )]
        self.assertEqual(role_ids, roles_in_account)

