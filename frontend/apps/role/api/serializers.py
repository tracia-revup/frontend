from rest_framework import serializers

from frontend.apps.role.models import Role
from frontend.apps.seat.models import Seat


class RoleSerializerBase(serializers.ModelSerializer):
    class Meta:
        model = Role
        exclude = ('account',)


class RoleSerializer(RoleSerializerBase):
    permissions = serializers.SerializerMethodField("build_permissions")

    def build_permissions(self, role):
        return [(p, Seat.FundraiserPermissions.translate(p) or
                 Seat.AdminPermissions.translate(p))
                for p in role.permissions]