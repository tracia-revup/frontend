from django import db
from django.db.models import Q
from django.http import QueryDict
from django.shortcuts import get_object_or_404
from rest_condition.permissions import Or, And
from rest_framework import permissions, exceptions
from rest_framework.response import Response
from rest_framework_extensions.mixins import NestedViewSetMixin

from frontend.apps.campaign.models import Account
from frontend.apps.role.api.permissions import HasPermission
from frontend.apps.role.api.serializers import RoleSerializer
from frontend.apps.role.permissions import (AdminPermissions,
                                            FundraiserPermissions)
from frontend.apps.role.models import Role
from frontend.libs.utils.api_utils import IsAdminUser, CRDModelViewSet, \
    APIException, InitialMixin


class AccountRolesViewSet(NestedViewSetMixin, InitialMixin, CRDModelViewSet):
    parent_account_lookup = 'parent_lookup_account_id'
    serializer_class = RoleSerializer
    permission_classes = (
        And(permissions.IsAuthenticated,
            Or(IsAdminUser,
               Or(HasPermission(parent_account_lookup,
                                [AdminPermissions.CONFIG_ROLES,
                                 AdminPermissions.MOD_ROLES],
                                methods=["GET"]),
                  HasPermission(parent_account_lookup,
                                AdminPermissions.CONFIG_ROLES,
                                methods=["POST", "DELETE"]),
                  )
            ),
        ),
    )
    model = Role
    queryset = Role.objects.all()

    def _initial(self):
        self.account = \
            get_object_or_404(Account,
                              pk=self.kwargs.get(self.parent_account_lookup))

    def get_queryset(self):
        return Role.objects.filter(
            Q(account=self.account) |
            Q(id__in=self.account.account_profile.roles.all()))

    def create(self, request, *args, **kwargs):
        # Get the fields from the POST.
        name = request.data.get("name")
        if not name:
            raise exceptions.APIException(
                "'name' is required to create a new Role.")
        description = request.data.get("description", "")
        is_a_seat_default = request.data.get("is_a_seat_default", False)

        if isinstance(request.data, QueryDict):
            permissions = request.data.getlist("permissions", [])
        else:
            permissions = request.data.get("permissions", [])
        if isinstance(permissions, str):
            permissions = [permissions]

        # Verify permissions all match real permissions
        if not set(permissions).issubset(set(AdminPermissions.all() +
                                             FundraiserPermissions.all())):
            raise exceptions.APIException("Invalid permissions")

        try:
            role = Role.objects.create(
                account=self.account,
                description=description,
                name=name,
                is_a_seat_default=is_a_seat_default,
                permissions=permissions
            )
        except db.IntegrityError:
            raise exceptions.APIException("This account already has a role "
                                          "with the name: {}".format(name))
        return Response(RoleSerializer(role).data)

    def update(self, request, *args, **kwargs):
        # Get the fields from the POST.
        name = request.data.get("name", "")
        account = request.data.get("account")
        description = request.data.get("description", "")
        is_a_seat_default = request.data.get("is_a_seat_default", False)

        if isinstance(request.data, QueryDict):
            permissions = request.data.getlist("permissions", [])
        else:
            permissions = request.data.get("permissions", [])
        if isinstance(permissions, str):
            permissions = [permissions]

        try:
            # Check if this is not a global role, and if it isn't the update
            # is allowed.
            # If the name and permissions exist but the account IS null
            # this indicates this is a global role and we should create a
            # new account specific role.  The ObjectDoesNotExist exception
            # will be raised and we will create an account specific role.
            role = Role.objects.get(
                name=name,
                permissions=permissions,
                account__is_null=False
            )
            role.description = description
            role.is_a_seat_default = is_a_seat_default
            role.account = account
            role.save()
        except Role.DoesNotExist as e:
            try:
                # if the role was global or the name/permissions combination
                # didn't already exists we should create a new role specific
                # to this account now
                role = Role.objects.create(
                    name=name,
                    permissions=permissions,
                    description=description,
                    account=account,
                    is_a_seat_default=name == 'Fundraiser'
                )
            except db.IntegrityError as e:
                raise APIException('This account already has a role with that '
                                   'name.')

        return Response(RoleSerializer(role).data)
