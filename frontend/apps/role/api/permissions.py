
from rest_framework import permissions


class HasPermission(permissions.BasePermission):
    """Verify the user has any of the given permissions.

    This will first look in the user's current seat, then it will look more
    globally across all of the user's seats if the user doesn't have
    a current seat matching the query account.
    """
    def __init__(self, url_id_field, permissions_, methods=None, **kwargs):
        """Check if the user has a seat with the given permissions for the
        account in the query.

        If multiple permissions are supplied, it is treated as an OR
        relationship.
        """
        # If methods is supplied, only verify permissions for the given
        # methods. Other methods are automatically rejected (False)
        self.methods = [m.upper() for m in (methods or [])]
        if isinstance(permissions_, str):
            permissions_ = [permissions_]
        self.permissions = permissions_
        self.url_id_field = url_id_field

    def _get_account_id(self, view):
        try:
            # Sometimes the account_id isn't in the url, and several steps are
            # required in order to determine what the account_id is based on
            # available information. Check the view for the method
            # `get_account_id()` to do this transform.
            return view.get_account_id()
        except AttributeError:
            return view.kwargs.get(self.url_id_field)

    def has_object_permission(self, request, view, obj):
        """Use the same rules for objects as for list views.

        The parent class always returns True, which is not correct.
        """
        return self.has_permission(request, view)

    def has_permission(self, request, view):
        if self.methods and request.method.upper() not in self.methods:
            return False

        # Let's attempt to use the current seat because it will be the most
        # common case, and far more performant.
        try:
            current_seat = request.user.current_seat
        except AttributeError:
            current_seat = None

        # Look for the permissions in the current seat
        account_id = self._get_account_id(view)
        if current_seat and current_seat.account_id == int(account_id):
            return current_seat.has_permissions(self.permissions)
        else:
            # If the user doesn't have a current seat, or their current seat is
            # the wrong account, then we have to check their other accounts.
            return request.user.has_permissions(
                self.permissions, account_id)
