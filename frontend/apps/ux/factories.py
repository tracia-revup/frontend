import factory
from factory import fuzzy

from frontend.apps.ux.models import SkinSettings


@factory.use_strategy(factory.CREATE_STRATEGY)
class SkinSettingsFactory(factory.DjangoModelFactory):
    class Meta:
        model = SkinSettings

    title = fuzzy.FuzzyText()
