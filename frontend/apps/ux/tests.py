
from django.urls import reverse
import json

from frontend.apps.authorize.test_utils import (
    AuthorizedUserTestCase, PermissionTestMixin)
from frontend.apps.campaign.factories import (
    AccountFactory, AccountProfileFactory)
from frontend.apps.campaign.models import AccountSkinSettingsLink
from frontend.apps.role.permissions import AdminPermissions
from frontend.apps.ux.factories import SkinSettingsFactory
from frontend.libs.utils.string_utils import random_string


class SkinSettingsViewSetTests(AuthorizedUserTestCase, PermissionTestMixin):
    permissions = [AdminPermissions.CONFIG_SKINS]

    def setUp(self, **kwargs):
        super(SkinSettingsViewSetTests, self).setUp(login_now=False, **kwargs)
        self.account_profile = AccountProfileFactory()
        self.account = AccountFactory(account_profile=self.account_profile)
        self.user.current_seat.account = self.account
        self.user.current_seat.save()

        # Create an account-owned skin
        self.skin = SkinSettingsFactory(account=self.account)
        # Create a shared skin
        self.skin2 = SkinSettingsFactory()
        AccountSkinSettingsLink.objects.create(
            skin=self.skin2,
            account_profile=self.account_profile,
            is_default=True
        )
        # Create an unavailable skin
        self.skin3 = SkinSettingsFactory()

        self.url = reverse("ux_skins_api-list", args=(self.account.id,))
        self.url2 = reverse("ux_skins_api-detail",
                            args=(self.account.id, self.skin.id))
        self.url3 = reverse("ux_skins_api-detail",
                            args=(self.account.id, self.skin2.id))
        self.url4 = reverse("ux_skins_api-detail",
                            args=(self.account.id, self.skin3.id))

    def test_permissions(self):
        super(SkinSettingsViewSetTests, self).test_permissions()

        # Verify the user now has permission to post. We're not testing
        # the output here, just the permissions.
        response = self.client.post(self.url, data={'test': '123'})
        self.assertContains(response, "id")
        response = self.client.put(self.url2, data='{"test": "123"}',
                                   content_type='application/json')
        self.assertContains(response, "id")
        response = self.client.patch(self.url2, data='{"test": "123"}',
                                     content_type='application/json')
        self.assertContains(response, "id")
        response = self.client.delete(self.url2)
        self.assertContains(response, "", status_code=204)

        ## Verify user can access shared skin
        response = self.client.get(self.url3)
        self.assertContains(response, "")

        ## Verify user cannot access a unavailable skin
        response = self.client.get(self.url4)
        self.assertNoPermission(response)

    def test_serializer(self):
        self._login_and_add_admin_group(
            self.user, permissions=self.permissions)
        result = self.client.get(self.url)
        self.verify_serializer(
            result, ('id', 'title', 'description', 'settings', 'shared',
                     "banner", "branding_logo"))
        # Should have one of each
        self.assertContains(result, '"shared":true')
        self.assertContains(result, '"shared":false')

    def test_create(self):
        def worker(data, expected):
            response = self.client.post(self.url, data=json.dumps(data),
                                        content_type='application/json')
            self.verify_content(response, expected)
            return response

        self._login_and_add_admin_group(
            self.user, permissions=self.permissions)

        # Verify a basic case where no data is given
        skin_count = self.account.get_skins().count()
        worker({}, {"title": "Skin", "description": None,
                    "settings": {}, "shared": False, "banner": "",
                    "branding_logo": ""})
        self.assertEqual(self.account.get_skins().count(), skin_count + 1)
        # Create another and verify it gets an enumerated title
        worker({}, {"title": "Skin (1)", "description": None,
                    "settings": {}, "shared": False, "banner": "",
                    "branding_logo": ""})

        # Create a skin and verify it is created correctly
        data = {
            "title": "Test 1",
            "branding_logo": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA"
                             "AEAAAABCAIAAACQd1PeAAAAAXNSR0IArs4c6QAAAAxJREFU"
                             "CB1j+P//PwAF/gL+n8otEwAAAABJRU5ErkJggg==",
            "settings": {
                "backgroundColor": "somecolor"
            }
        }
        expected = data.copy()
        del expected['branding_logo']
        expected.update({
            "description": None, "shared": False, "banner": "",
        })
        response = worker(data, expected)
        # Verify the image was created
        self.assertRegex(response.content.decode('utf-8'),
                                 r'"branding_logo":"/media/\w+.png"')

    def test_update(self):
        def worker(data, expected, url):
            response = self.client.put(url, data=json.dumps(data),
                                       content_type='application/json')
            self.verify_content(response, expected)
            return response

        self._login_and_add_admin_group(
            self.user, permissions=self.permissions)

        ## Update an owned skin and verify it is updated
        title = random_string()
        skin_count = self.account.get_skins().count()
        self.assertNotEqual(self.skin.title, title)
        self.assertEqual(str(self.skin.branding_logo), "")
        data = {
            "title": title,
            "branding_logo": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA"
                             "AEAAAABCAIAAACQd1PeAAAAAXNSR0IArs4c6QAAAAxJREFU"
                             "CB1j+P//PwAF/gL+n8otEwAAAABJRU5ErkJggg==",
        }
        expected = data.copy()
        del expected['branding_logo']
        expected.update({
            "description": None, "shared": False, "banner": "",
            "id": self.skin.id
        })
        response = worker(data, expected, self.url2)
        # Verify the image was created
        self.assertRegex(response.content.decode('utf-8'),
                                 r'"branding_logo":"/media/\w+.png"')
        self.assertEqual(skin_count, self.account.get_skins().count())

        ## Update a global skin and verify a new one is cloned
        self.assertIsNone(self.skin2.account)
        self.assertIn(self.skin2, self.account.get_skins())
        title = random_string()
        data = {"title": title}
        expected = {
            "title": title, "description": None, "shared": False, "banner": "",
            "branding_logo": ""}
        response = worker(data, expected, self.url3)
        # Should create a new skin
        self.assertNotContains(response, '"id":{}'.format(self.skin2.id))
        self.assertEqual(skin_count + 1, self.account.get_skins().count())

        ## Verify user may not update an unavailable skin
        response = self.client.put(self.url4, data=data)
        self.assertNoPermission(response)

    def test_delete(self):
        self._login_and_add_admin_group(
            self.user, permissions=self.permissions)

        ## Verify current skin can't be deleted
        self.account.current_skin = self.skin
        self.account.save()
        response = self.client.delete(self.url2)
        self.assertContains(response, "Cannot delete the active skin",
                            status_code=500)

        ## Verify shared skin cannot be deleted
        response = self.client.delete(self.url3)
        self.assertContains(response, "Cannot delete a global skin",
                            status_code=500)

        ## Verify unavailable skin cannot be deleted
        response = self.client.delete(self.url4)
        self.assertNoPermission(response)

        ## Verify positive case
        self.account.current_skin = None
        self.account.save()
        response = self.client.delete(self.url2)
        self.assertContains(response, "", status_code=204)
