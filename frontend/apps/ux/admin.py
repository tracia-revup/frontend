
from django.contrib import admin

from .models import SkinSettings


@admin.register(SkinSettings)
class SkinSettingsAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'account')
    search_fields = ('title', 'account__organization_name')