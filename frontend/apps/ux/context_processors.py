
from django.contrib.auth.models import AnonymousUser


def current_skin(request):
    """Extract the current skin from the user's seat."""
    try:
        # Do nothing if user isn't logged in
        if isinstance(request.user, AnonymousUser):
            return {}

        cur_skin = request.user.current_seat.account.current_skin
        if cur_skin:
            return {'current_skin': cur_skin}
    except Exception:
        # Generally bad to use catch-alls, but the default behavior of
        # django templates is to fail silently
        pass
    return {}
