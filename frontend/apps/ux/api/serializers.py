
from rest_framework import serializers

from frontend.apps.ux.models import SkinSettings
from frontend.libs.utils.api_utils import Base64ImageField


MEGABYTE = 2**20


class SkinSettingsSerializer(serializers.ModelSerializer):
    shared = serializers.ReadOnlyField()
    banner = Base64ImageField(required=False, max_length=(10 * MEGABYTE),
                              allow_empty_file=True, allow_null=True)
    branding_logo = Base64ImageField(required=False, max_length=(1 * MEGABYTE),
                                     allow_empty_file=True, allow_null=True)
    # WritableField has code that handles the json dict better than the default
    settings = serializers.JSONField(required=False)

    class Meta:
        model = SkinSettings
        fields = ('id', 'title', 'description', 'settings', 'shared',
                  "banner", "branding_logo", "account")
        extra_kwargs = {'account': {'write_only': True}}

    def to_internal_value(self, data):
        # If a banner is not included, it is assumed to keep its same value
        if self.instance:
            for field in ("banner", "branding_logo"):
                if field not in data:
                    data[field] = getattr(self.instance, field)
                if not data[field]:
                    data[field] = None
        return super(SkinSettingsSerializer, self).to_internal_value(data)