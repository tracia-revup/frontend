
from django.shortcuts import get_object_or_404
from django.db.models import Q
from rest_condition.permissions import And, Or
from rest_framework import permissions, exceptions, viewsets
from rest_framework.response import Response

from frontend.apps.campaign.models import Account
from frontend.apps.role.permissions import AdminPermissions
from frontend.apps.role.api.permissions import HasPermission
from frontend.apps.ux.api.serializers import SkinSettingsSerializer
from frontend.apps.ux.models import SkinSettings
from frontend.libs.utils.api_utils import IsAdminUser
from frontend.libs.utils.string_utils import increment_string


class SkinSettingsBaseViewSet(viewsets.ModelViewSet):
    serializer_class = SkinSettingsSerializer
    model = SkinSettings
    queryset = SkinSettings.objects.all()


class SkinSettingsViewSet(SkinSettingsBaseViewSet):
    """An API for an Account's Skin Settings, which may include
       global/shared skins
    """
    parent_account_lookup = 'parent_lookup_account_id'
    permission_classes = (
        And(permissions.IsAuthenticated,
            Or(IsAdminUser,
               HasPermission(parent_account_lookup,
                             AdminPermissions.CONFIG_SKINS)
               )
            ),
    )

    def get_account(self):
        return get_object_or_404(
            Account, pk=self.kwargs.get(self.parent_account_lookup))

    def filter_queryset(self, queryset):
        account = self.get_account()
        if account:
            queryset = queryset.filter(Q(account=account) | Q(account=None))
        else:
            queryset = queryset.filter(account=None)
        return super(SkinSettingsViewSet, self).filter_queryset(queryset)

    def get_object(self):
        account = self.get_account()
        obj = super(SkinSettingsViewSet, self).get_object()

        # The requested skin must be available to the given account.
        if obj not in account.get_skins():
            raise exceptions.PermissionDenied()
        return obj

    def list(self, request, *args, **kwargs):
        account = self.get_account()
        return Response(SkinSettingsSerializer(account.get_skins(),
                                               many=True).data)

    def _create_or_update(self, **kwargs):
        skin = SkinSettingsSerializer(**kwargs)
        if skin.is_valid():
            skin.save()
            return Response(skin.data)
        else:
            raise exceptions.APIException(skin.errors)

    def create(self, request, *args, **kwargs):
        account = self.get_account()

        data = dict(request.data)
        title = data.get("title") or "Skin"
        # If the title is already taken, add an increment to it.
        titles = {s.title for s in account.get_skins()}
        while title in titles:
            title = increment_string(title)

        data["account"] = account.id
        data['title'] = title
        return self._create_or_update(data=data)

    def update(self, request, *args, **kwargs):
        skin = self.get_object()

        # If skin is global, redirect to create
        if skin.account is None:
            return self.create(request, *args, **kwargs)
        return self._create_or_update(instance=skin, data=request.data,
                                      partial=True)

    def destroy(self, request, *args, **kwargs):
        skin = self.get_object()
        account = self.get_account()

        # Global (unowned) skins may not be deleted
        if skin.account is None:
            raise exceptions.APIException("Cannot delete a global skin")
        elif skin == account.current_skin:
            raise exceptions.APIException(
                "Cannot delete the active skin. Activate another skin before "
                "deleting this one.")
        else:
            return super(SkinSettingsViewSet, self).destroy(
                request, *args, **kwargs)


class StaffSkinSettingsViewSet(SkinSettingsBaseViewSet):
    """An API for the staff to operate on global/shared skins."""
    permission_classes = (And(permissions.IsAuthenticated, IsAdminUser),)

    def get_queryset(self):
        queryset = super(StaffSkinSettingsViewSet, self).get_queryset()
        # Only return unowned skins
        return queryset.filter(account=None)

