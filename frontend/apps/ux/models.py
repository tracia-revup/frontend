
from django.db import models
from django_extensions.db.models import TitleDescriptionModel
from jsonfield import JSONField

from frontend.libs.utils.model_utils import OwnershipModel, EntityModel


class SkinSettingsRelatedManager(models.Manager):
    use_for_related_fields = True

    def active(self):
        """This query works on the through table for a m2m, and cannot
        be used elsewhere.
        """
        # When called through a many-to-many, 'self' will be a
        # "ManyRelatedManager". However, that is a dynamically generated
        # class, so I can't import it and inquire about it directly
        if self.__class__.__name__ != "ManyRelatedManager":
            raise TypeError("This method may only be called "
                            "through a many-to-many")
        return self.get_queryset().filter(through_reference__is_active=True)

    def get_default(self):
        """Get the default from the set of related objects in the join table.

        If there are more than one records marked default, this method will
        arbitrarily grab one. If there are no defaults, this will return
        None.
        """
        return self.active().filter(through_reference__is_default=True).first()


class SkinSettings(OwnershipModel, TitleDescriptionModel, EntityModel):
    """Store the settings for a skin of the application."""
    objects = SkinSettingsRelatedManager()

    settings = JSONField(blank=True, default={})
    banner = models.ImageField(blank=True)
    branding_logo = models.ImageField(blank=True)

    @property
    def shared(self):
        """Is this a shared or owned skin.

        If self.account is set, this skin is owned by a specific account.
        """
        return not bool(self.account)


