# -*- coding: utf-8 -*-


import audit_log.models.fields
from django.conf import settings
from django.db import models, migrations
import django.db.models.deletion
import django.utils.timezone
import django_extensions.db.fields
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('campaign', '0019_account_analysis_link'),
    ]

    operations = [
        migrations.CreateModel(
            name='SkinSettings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('settings', jsonfield.fields.JSONField(default={}, blank=True)),
                ('account', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='campaign.Account', null=True)),
                ('user', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('banner', models.ImageField(upload_to='', blank=True)),
                ('branding_logo', models.ImageField(upload_to='', blank=True)),

                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_ux_skinsettings_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('modified_by', audit_log.models.fields.LastUserField(related_name='modified_ux_skinsettings_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by')),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),

                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
