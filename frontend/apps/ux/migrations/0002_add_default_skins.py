# -*- coding: utf-8 -*-


from django.db import models, migrations


def add_skins(apps, schema_editor):
    SkinSettings = apps.get_model("ux", "SkinSettings")
    AccountProfile = apps.get_model("campaign", "AccountProfile")
    Account = apps.get_model("campaign", "Account")
    AccountSkinSettingsLink = apps.get_model("campaign",
                                             "AccountSkinSettingsLink")

    # Create the default skins for the political profiles
    try:
        political_skin = SkinSettings.objects.get(title="Political Default")
    except SkinSettings.DoesNotExist:
        political_skin = SkinSettings.objects.create(
            title="Political Default",
            description="The default skin for political product users",
            settings=political_skin_settings
        )

    committee = AccountProfile.objects.get(title="Political Committee")
    candidate = AccountProfile.objects.get(title="Political Candidate")

    AccountSkinSettingsLink.objects.get_or_create(
        account_profile=committee,
        skin=political_skin,
        is_default=True)
    AccountSkinSettingsLink.objects.get_or_create(
        account_profile=candidate,
        skin=political_skin,
        is_default=True)

    # Set the current skin to the default for all Accounts under these profiles
    Account.objects.filter(account_profile__in=[committee, candidate]).update(
        current_skin=political_skin)

    # Create the default skins for the academic profile
    try:
        academic_skin = SkinSettings.objects.get(title="Academic Default")
    except SkinSettings.DoesNotExist:
        academic_skin = SkinSettings.objects.create(
            title="Academic Default",
            description="The default skin for academic product users",
            settings=academic_skin_settings
        )

    academic = AccountProfile.objects.get(title="University")
    AccountSkinSettingsLink.objects.get_or_create(
        account_profile=academic,
        skin=academic_skin,
        is_default=True)

    # Set the current skin to the default for all Accounts under this profile
    Account.objects.filter(account_profile=academic).update(
        current_skin=academic_skin)


class Migration(migrations.Migration):

    dependencies = [
        ('ux', '0001_initial'),
        ('campaign', '0020_auto_20160223_1515')
    ]

    operations = [
        migrations.RunPython(add_skins, lambda x, y: True)
    ]


political_skin_settings = \
{
  "cssRules":{
    "primBtnColor":{
      "field":[
        {
          "colorVal":"#78ad42",
          "colorType":"border",
          "colorFor":"primaryBtnField"
        },
        {
          "colorVal":"#79a649",
          "colorType":"background",
          "colorFor":"primaryBtnField"
        }
      ],
      "cssRule":[
        ".revupBtnDefault",
        ".revupPrimaryBtnColor(important)",
        ".revupPagination .paginationDivInner .paginationPageNum.active"
      ]
    },
    "lnkColor":{
      "field":[
        {
          "colorVal":"#4d8fc2",
          "colorType":"color",
          "colorFor":"lnkField"
        }
      ],
      "cssRule":[
        "a",
        "a:hover(important)",
        ".revupLnk(important)",
        ".revupPagination .paginationDivInner .paginationPageNum"
      ]
    },
    "inactiveTabColor":{
      "field":[
        {
          "colorVal":"#e6e7e8",
          "colorType":"borderTab",
          "colorFor":"tabInactive"
        },
        {
          "colorVal":"#8dcb3f",
          "colorType":"background",
          "colorFor":"tabInactive"
        }
      ],
      "cssRule":".mainContent .container .sideNav .tab",
      "otherFields":[
        {
          "colorType":"color",
          "colorVal":"#8dcb3f",
          "cssRule":".pageLabel",
          "$field":{},
          "colorFor":"tabActive"
        },
        {
          "colorVal":"#8dcb3f",
          "colorType":"color",
          "$field":{
            "0":{},
            "length":1,
            "prevObject":{
              "0":{
                "jQuery221081757962820120161":{
                  "events":{
                    "revup":[
                      {
                        "type":"revup",
                        "origType":"revup",
                        "guid":53,
                        "namespace":"dialogClose"
                      }
                    ]
                  }
                }
              },
              "length":1,
              "prevObject":{
                "0":{
                  "jQuery221081757962820120161":{
                    "events":{
                      "revup":[
                        {
                          "type":"revup",
                          "origType":"revup",
                          "guid":53,
                          "namespace":"dialogClose"
                        }
                      ]
                    }
                  }
                },
                "length":1
              }
            },
            "selector":".previewSection .tabActive"
          },
          "colorFor":"tabActive"
        },
        {
          "colorType":"color",
          "colorVal":"#8dcb3f",
          "cssRule":".mainContent .container .sideNav .tab.active a .icon",
          "$field":{
            "0":{},
            "length":1,
            "prevObject":{
              "0":{
                "jQuery221081757962820120161":{
                  "events":{
                    "revup":[
                      {
                        "type":"revup",
                        "origType":"revup",
                        "guid":53,
                        "namespace":"dialogClose"
                      }
                    ]
                  }
                }
              },
              "length":1,
              "prevObject":{
                "0":{
                  "jQuery221081757962820120161":{
                    "events":{
                      "revup":[
                        {
                          "type":"revup",
                          "origType":"revup",
                          "guid":53,
                          "namespace":"dialogClose"
                        }
                      ]
                    }
                  }
                },
                "length":1
              }
            },
            "selector":".previewSection .tabActive .icon"
          },
          "colorFor":"tabActive icon"
        },
        {
          "colorType":"color",
          "colorVal":"#8dcb3f",
          "cssRule":[
            ".mainContent .container .sideNav .tab.active a .text",
            ".mainContent .container .sideNav .tab.active a:hover .text"
          ],
          "$field":{
            "0":{},
            "length":1,
            "prevObject":{
              "0":{
                "jQuery221081757962820120161":{
                  "events":{
                    "revup":[
                      {
                        "type":"revup",
                        "origType":"revup",
                        "guid":53,
                        "namespace":"dialogClose"
                      }
                    ]
                  }
                }
              },
              "length":1,
              "prevObject":{
                "0":{
                  "jQuery221081757962820120161":{
                    "events":{
                      "revup":[
                        {
                          "type":"revup",
                          "origType":"revup",
                          "guid":53,
                          "namespace":"dialogClose"
                        }
                      ]
                    }
                  }
                },
                "length":1
              }
            },
            "selector":".previewSection .tabActive .text"
          },
          "colorFor":"tabActive text"
        }
      ]
    },
    "backgroundColor":{
      "field":[
        {
          "colorVal":"#e6e7e8",
          "colorType":"background",
          "colorFor":"backgroundDiv"
        }
      ],
      "cssRule":".backgroundDiv"
    },
    "secBtnColor":{
      "field":[
        {
          "colorVal":"#848f94",
          "colorType":"border",
          "colorFor":"secBtnField"
        },
        {
          "colorVal":"#919ca1",
          "colorType":"background",
          "colorFor":"secBtnField"
        }
      ],
      "cssRule":[
        ".revupBtnDefault.secondary",
        ".revupSecondaryBtnColor(important)"
      ]
    }
  },
  "candidateIcon":{
    "candidateIconName":"Name",
    "candidateIconColor":"#90c3f0"
  }
}


academic_skin_settings = \
{
  "cssRules":{
    "backgroundColor":{
      "field":[
        {
          "colorType":"background",
          "colorVal":"#dbdbdb",
          "colorFor":"backgroundDiv"
        }
      ],
      "cssRule":".backgroundDiv"
    },
    "secBtnColor":{
      "field":[
        {
          "colorType":"border",
          "colorVal":"#717171",
          "colorFor":"secBtnField"
        },
        {
          "colorType":"background",
          "colorVal":"#8d8d8d",
          "colorFor":"secBtnField"
        }
      ],
      "cssRule":[
        ".revupBtnDefault.secondary",
        ".revupSecondaryBtnColor(important)"
      ]
    },
    "primBtnColor":{
      "field":[
        {
          "colorType":"border",
          "colorVal":"#920000",
          "colorFor":"primaryBtnField"
        },
        {
          "colorType":"background",
          "colorVal":"#b60000",
          "colorFor":"primaryBtnField"
        }
      ],
      "cssRule":[
        ".revupBtnDefault",
        ".revupPrimaryBtnColor(important)",
        ".revupPagination .paginationDivInner .paginationPageNum.active"
      ]
    },
    "lnkColor":{
      "field":[
        {
          "colorType":"color",
          "colorVal":"#6d87ba",
          "colorFor":"lnkField"
        }
      ],
      "cssRule":[
        "a",
        "a:hover(important)",
        ".revupLnk(important)",
        ".revupPagination .paginationDivInner .paginationPageNum"
      ]
    },
    "inactiveTabColor":{
      "field":[
        {
          "colorType":"borderTab",
          "colorVal":"#920000",
          "colorFor":"tabInactive"
        },
        {
          "colorType":"background",
          "colorVal":"#b60000",
          "colorFor":"tabInactive"
        }
      ],
      "otherFields":[
        {
          "colorType":"color",
          "colorVal":"#b60000",
          "cssRule":".pageLabel",
          "$field":{},
          "colorFor":"tabActive"
        },
        {
          "colorType":"color",
          "colorVal":"#b60000",
          "$field":{
            "prevObject":{
              "prevObject":{
                "length":1,
                "0":{
                  "jQuery221062837524502538141":{
                    "events":{
                      "revup":[
                        {
                          "type":"revup",
                          "origType":"revup",
                          "guid":126,
                          "namespace":"dialogClose"
                        }
                      ]
                    }
                  }
                }
              },
              "length":1,
              "0":{
                "jQuery221062837524502538141":{
                  "events":{
                    "revup":[
                      {
                        "type":"revup",
                        "origType":"revup",
                        "guid":126,
                        "namespace":"dialogClose"
                      }
                    ]
                  }
                }
              }
            },
            "length":1,
            "selector":".previewSection .tabActive",
            "0":{}
          },
          "colorFor":"tabActive"
        },
        {
          "colorType":"color",
          "colorVal":"#b60000",
          "cssRule":".mainContent .container .sideNav .tab.active a .icon",
          "$field":{
            "prevObject":{
              "prevObject":{
                "length":1,
                "0":{
                  "jQuery221062837524502538141":{
                    "events":{
                      "revup":[
                        {
                          "type":"revup",
                          "origType":"revup",
                          "guid":126,
                          "namespace":"dialogClose"
                        }
                      ]
                    }
                  }
                }
              },
              "length":1,
              "0":{
                "jQuery221062837524502538141":{
                  "events":{
                    "revup":[
                      {
                        "type":"revup",
                        "origType":"revup",
                        "guid":126,
                        "namespace":"dialogClose"
                      }
                    ]
                  }
                }
              }
            },
            "length":1,
            "selector":".previewSection .tabActive .icon",
            "0":{}
          },
          "colorFor":"tabActive icon"
        },
        {
          "colorType":"color",
          "colorVal":"#b60000",
          "cssRule":[
            ".mainContent .container .sideNav .tab.active a .text",
            ".mainContent .container .sideNav .tab.active a:hover .text"
          ],
          "$field":{
            "prevObject":{
              "prevObject":{
                "length":1,
                "0":{
                  "jQuery221062837524502538141":{
                    "events":{
                      "revup":[
                        {
                          "type":"revup",
                          "origType":"revup",
                          "guid":126,
                          "namespace":"dialogClose"
                        }
                      ]
                    }
                  }
                }
              },
              "length":1,
              "0":{
                "jQuery221062837524502538141":{
                  "events":{
                    "revup":[
                      {
                        "type":"revup",
                        "origType":"revup",
                        "guid":126,
                        "namespace":"dialogClose"
                      }
                    ]
                  }
                }
              }
            },
            "length":1,
            "selector":".previewSection .tabActive .text",
            "0":{}
          },
          "colorFor":"tabActive text"
        }
      ],
      "cssRule":".mainContent .container .sideNav .tab"
    }
  },
  "candidateIcon":{
    "candidateIconColor":"#b60000",
    "candidateIconName":"U"
  }
}