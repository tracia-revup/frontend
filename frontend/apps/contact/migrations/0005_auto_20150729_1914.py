# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings

import frontend.libs.utils.model_utils


def disconnect_social_auth(apps, schema_editor):
    """Disconnect existing social-auth accounts and create an ImportRecord
       for each of them.
    """
    import requests.exceptions
    from social_django.utils import load_strategy, load_backend
    from social_django.models import UserSocialAuth

    RevUpUser = apps.get_model("authorize", "RevUpUser")
    ImportRecord = apps.get_model("contact", "ImportRecord")
    for user in RevUpUser.objects.all():
        for social in UserSocialAuth.objects.filter(user=user)\
                                            .exclude(provider="email"):
            if social.provider == "linkedin-oauth2":
                import_type = "LI"
                label = "{} {}".format(social.extra_data.get("first_name", ""),
                                       social.extra_data.get("last_name", ""))
            elif social.provider == "google-oauth2":
                import_type = "GM"
                label = social.uid
            else:
                import_type = "OT"
                label = social.uid

            # Create an ImportRecord for existing social auth accounts
            ImportRecord.objects.create(user=user, import_type=import_type,
                                        label=label, uid=social.uid)

            # Disconnect the social-auth account
            try:
                strategy = load_strategy()
                print("Disconnecting social account '{}' for user ({})"
                      .format(social.provider, user.id))
                try:
                    social.refresh_token(strategy)
                except requests.exceptions.HTTPError:
                    pass

                # Attempt to revoke the access token for the social auth
                if 'access_token' in social.extra_data:
                    backend = load_backend(strategy, social.provider, None)
                    backend.revoke_token(social.extra_data['access_token'],
                                         social.uid)
            except Exception:
                import traceback
                # We don't want disconnection errors to stop the migration
                traceback.print_exc()
                print("Disconnect Failed")
            finally:
                # Delete the UserSocialAuth record from our DB
                social.delete()


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('contact', '0004_auto_20150512_0037'),
    ]

    operations = [
        migrations.CreateModel(
            name='ImportRecord',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('import_type', models.CharField(default='OT', max_length=2, choices=[('FB', 'Facebook'), ('GM', 'Gmail'), ('LI', 'LinkedIn'), ('TW', 'Twitter'), ('OU', 'Outlook'), ('AB', 'Address Book'), ('CV', 'CSV'), ('OT', 'Other')])),
                ('label', frontend.libs.utils.model_utils.TruncatingCharField(max_length=256, blank=True)),
                ('uid', models.CharField(help_text='This should contain something that uniquely identifies the social account that was imported from. E.g. gmail address', max_length=256, blank=True)),
                ('import_dt', models.DateTimeField(auto_now_add=True)),
                ('import_success', models.BooleanField(default=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterIndexTogether(
            name='importrecord',
            index_together=set([('import_type', 'uid')]),
        ),
        migrations.AddField(
            model_name='contactfileupload',
            name='file_name',
            field=frontend.libs.utils.model_utils.TruncatingCharField(
                max_length=256, blank=True),
            preserve_default=True,
        ),
        migrations.RunPython(disconnect_social_auth,
                             lambda x, y: True)
    ]
