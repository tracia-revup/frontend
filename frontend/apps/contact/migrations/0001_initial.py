# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion
import frontend.libs.mixins
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('address_type', models.CharField(max_length=128, blank=True)),
                ('label', models.CharField(max_length=64, blank=True)),
                ('primary', models.BooleanField(default=False)),
                ('agent', models.CharField(max_length=64, blank=True)),
                ('house_name', models.CharField(max_length=128, blank=True)),
                ('street', models.CharField(max_length=256, blank=True)),
                ('po_box', models.CharField(max_length=32, blank=True)),
                ('neighborhood', models.CharField(max_length=64, blank=True)),
                ('city', models.CharField(max_length=64, blank=True)),
                ('subregion', models.CharField(max_length=64, blank=True)),
                ('region', models.CharField(max_length=64, blank=True)),
                ('post_code', models.CharField(max_length=64, blank=True)),
                ('country', models.CharField(max_length=64, blank=True)),
            ],
            options={
            },
            bases=(models.Model, frontend.libs.mixins.SimpleUnicodeMixin),
        ),
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('contact_id', models.CharField(max_length=256, verbose_name='contact external id')),
                ('contact_type', models.CharField(max_length=128)),
                ('first_name', models.CharField(max_length=128, blank=True)),
                ('additional_name', models.CharField(max_length=128, blank=True)),
                ('last_name', models.CharField(max_length=128, blank=True)),
                ('name_prefix', models.CharField(max_length=128, blank=True)),
                ('name_suffix', models.CharField(max_length=128, blank=True)),
                ('first_name_lower', models.CharField(db_index=True, max_length=128, blank=True)),
                ('last_name_lower', models.CharField(db_index=True, max_length=128, blank=True)),
                ('is_primary', models.BooleanField(default=False)),
                ('location_name', models.CharField(max_length=128, blank=True)),
                ('location_id', models.CharField(help_text='The object ID from the mongo location collection', max_length=24, blank=True)),
                ('parent', models.ForeignKey(related_name='contacts', on_delete=django.db.models.deletion.CASCADE, blank=True, to='contact.Contact', null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model, frontend.libs.mixins.SimpleUnicodeMixin),
        ),
        migrations.CreateModel(
            name='EmailAddress',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('address', models.CharField(max_length=256)),
                ('display_name', models.CharField(max_length=128, blank=True)),
                ('label', models.CharField(max_length=128, blank=True)),
                ('primary', models.BooleanField(default=False)),
                ('contact', models.ForeignKey(related_name='email_addresses', to='contact.Contact', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model, frontend.libs.mixins.SimpleUnicodeMixin),
        ),
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=128, blank=True)),
                ('department', models.CharField(max_length=128, blank=True)),
                ('job_description', models.CharField(max_length=256, blank=True)),
                ('name', models.CharField(max_length=256, blank=True)),
                ('symbol', models.CharField(max_length=64, blank=True)),
                ('title', models.CharField(max_length=256, blank=True)),
                ('primary', models.BooleanField(default=False)),
                ('contact', models.ForeignKey(related_name='organizations', to='contact.Contact', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model, frontend.libs.mixins.SimpleUnicodeMixin),
        ),
        migrations.CreateModel(
            name='PhoneNumber',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=128, blank=True)),
                ('primary', models.BooleanField(default=False)),
                ('number', models.CharField(max_length=128)),
                ('contact', models.ForeignKey(related_name='phone_numbers', to='contact.Contact', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model, frontend.libs.mixins.SimpleUnicodeMixin),
        ),
        migrations.AlterIndexTogether(
            name='contact',
            index_together=set([('user', 'contact_id')]),
        ),
        migrations.AddField(
            model_name='address',
            name='contact',
            field=models.ForeignKey(related_name='addresses', to='contact.Contact', on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
    ]
