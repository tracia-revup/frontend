# -*- coding: utf-8 -*-


from django.db import models, migrations
import frontend.libs.utils.model_utils


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='address',
            name='address_type',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=128, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='address',
            name='agent',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=64, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='address',
            name='city',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=64, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='address',
            name='country',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=64, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='address',
            name='house_name',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=128, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='address',
            name='label',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=64, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='address',
            name='neighborhood',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=64, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='address',
            name='po_box',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=32, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='address',
            name='post_code',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=64, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='address',
            name='region',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=64, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='address',
            name='street',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=256, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='address',
            name='subregion',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=64, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contact',
            name='additional_name',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=128, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contact',
            name='first_name',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=128, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contact',
            name='first_name_lower',
            field=frontend.libs.utils.model_utils.TruncatingCharField(db_index=True, max_length=128, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contact',
            name='last_name',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=128, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contact',
            name='last_name_lower',
            field=frontend.libs.utils.model_utils.TruncatingCharField(db_index=True, max_length=128, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contact',
            name='location_id',
            field=frontend.libs.utils.model_utils.TruncatingCharField(help_text='The object ID from the mongo location collection', max_length=24, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contact',
            name='location_name',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=128, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contact',
            name='name_prefix',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=128, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contact',
            name='name_suffix',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=128, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='emailaddress',
            name='address',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=256),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='emailaddress',
            name='display_name',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=128, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='emailaddress',
            name='label',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=128, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='organization',
            name='department',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=128, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='organization',
            name='job_description',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=256, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='organization',
            name='label',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=128, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='organization',
            name='name',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=256, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='organization',
            name='symbol',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=64, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='organization',
            name='title',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=256, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='phonenumber',
            name='label',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=128, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='phonenumber',
            name='number',
            field=frontend.libs.utils.model_utils.TruncatingCharField(max_length=128),
            preserve_default=True,
        ),
    ]
