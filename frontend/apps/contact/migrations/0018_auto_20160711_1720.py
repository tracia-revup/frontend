# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-07-11 17:20


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0017_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='importrecord',
            name='import_type',
            field=models.CharField(choices=[('FB', 'Facebook'), ('GM', 'Gmail'), ('LI', 'LinkedIn'), ('TW', 'Twitter'), ('OU', 'Outlook'), ('AB', 'VCard'), ('CV', 'CSV'), ('OT', 'Other')], default='OT', max_length=2),
        ),
    ]
