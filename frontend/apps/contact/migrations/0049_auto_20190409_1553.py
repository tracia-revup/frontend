# Generated by Django 2.0.5 on 2019-04-09 15:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0048_auto_20190312_1147'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contactfileupload',
            name='data_file',
            field=models.FileField(max_length=512, upload_to=''),
        ),
    ]
