# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-02-17 12:08


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0026_fix_address_book_label'),
    ]

    operations = [
        migrations.AddField(
            model_name='imageurl',
            name='image',
            field=models.ImageField(blank=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='contactfileupload',
            name='file_type',
            field=models.CharField(choices=[('CSV', 'CSV File'), ('MOB', 'Unified Mobile')], db_index=True, default='CSV', max_length=3),
        ),
        migrations.AlterField(
            model_name='externalprofile',
            name='source',
            field=models.CharField(choices=[('FB', 'Facebook'), ('GM', 'Gmail'), ('LI', 'LinkedIn'), ('TW', 'Twitter'), ('OU', 'Outlook'), ('AB', 'VCard'), ('CV', 'CSV'), ('IP', 'iPhone'), ('AD', 'Android'), ('OT', 'Other')], max_length=2),
        ),
        migrations.AlterField(
            model_name='imageurl',
            name='source',
            field=models.CharField(choices=[('FB', 'Facebook'), ('GM', 'Gmail'), ('LI', 'LinkedIn'), ('TW', 'Twitter'), ('OU', 'Outlook'), ('AB', 'VCard'), ('CV', 'CSV'), ('IP', 'iPhone'), ('AD', 'Android'), ('OT', 'Other')], max_length=2),
        ),
        migrations.AlterField(
            model_name='importrecord',
            name='import_type',
            field=models.CharField(choices=[('FB', 'Facebook'), ('GM', 'Gmail'), ('LI', 'LinkedIn'), ('TW', 'Twitter'), ('OU', 'Outlook'), ('AB', 'VCard'), ('CV', 'CSV'), ('IP', 'iPhone'), ('AD', 'Android'), ('OT', 'Other')], default='OT', max_length=2),
        ),
    ]
