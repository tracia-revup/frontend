# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0002_auto_20150401_1624'),
        ('contact', '0002_data__standardize_phone'),
    ]

    operations = [
    ]
