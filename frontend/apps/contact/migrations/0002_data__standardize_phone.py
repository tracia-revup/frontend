# -*- coding: utf-8 -*-


from django.db import migrations
import phonenumbers


def standardize_phonenumbers(apps, schema_editor):
    PhoneNumber = apps.get_model("contact", "PhoneNumber")
    from frontend.apps.contact.models import PhoneNumber as P

    for phone in PhoneNumber.objects.all():
        try:
            phone.number = P.format_number(
                phone.number, phonenumbers.PhoneNumberFormat.RFC3966)
            phone.save()
        except Exception as err:
            print(err, phone.number)

class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(standardize_phonenumbers,
                             reverse_code=lambda x,y: True)
    ]
