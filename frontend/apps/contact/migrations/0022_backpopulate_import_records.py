# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-08-08 10:59

from datetime import datetime
import re

from django.db import migrations

from frontend.apps.contact.models import ImportRecord as IR


EPOCH = datetime(day=21, month=8, year=2015)


def fix_import_records(apps, schema_editor):
    ImportRecord = apps.get_model("contact", "ImportRecord")
    Contact = apps.get_model("contact", "Contact")
    RevUpUser = apps.get_model("authorize", "RevUpUser")

    pre_count = Contact.objects.filter(import_record=None).exclude(
        contact_type="merged").count()

    # Helper function to get or create ImportRecords
    def get_or_create_ir(label_, uid_, user_, import_type_):
        kwargs = dict(label=label_, uid=uid_, user=user_,
                      import_type=import_type_)
        # Select the oldest existing ImportRecord, so it is the closest
        # to accurate
        ir = ImportRecord.objects.filter(**kwargs).filter(import_success=True)\
                                 .order_by("import_dt").first()
        if ir is None:
            if user_.date_joined:
                # Try to get the import date as accurate as possible.
                kwargs["import_dt"] = user_.date_joined
            ir = ImportRecord.objects.create(**kwargs)
            created = True
        else:
            created = False
        return ir, created


    ## Fix the Gmail Records
    import_groups = set()
    # Iterate over all gmail records that don't have an import record, and
    # build a set of imports
    for c_id, u_id in Contact.objects.exclude(contact_id="").filter(
            contact_type="google-oauth2", import_record=None).values_list(
            "contact_id", "user_id"):
        email = None

        # Extract the email out of the contact_id string
        match = re.match(r".*?/([+\-_\.\w]+?%40[\w\.\-]+?)/.*?", c_id)
        if match:
            email = match.group(1).replace("%40", "@")

        if not email or "@" not in email:
            print("Failed to fix gmail: ", c_id)
            continue

        # Create distinct (email, user_id) items that will become ImportRecords
        import_groups.add((email, u_id))

    # Once we have a set of imports, we need to update the contacts.
    # We may, or may not, need to create ImportRecords
    for email, u_id in import_groups:
        user = RevUpUser.objects.get(id=u_id)
        import_record, created = get_or_create_ir(email, email,
                                                  user, IR.TaskType.GMAIL)
        Contact.objects.filter(
            contact_id__contains=email.replace("@", "%40"),
            contact_type="google-oauth2",
            user=u_id, import_record=None).update(import_record=import_record)


    ## Fix the Linkedin Records
    # Get all the users with missing linkedin import records.
    # If a user only has one linkedin source (which is the norm), we can
    # just assign that to all their contacts.
    # Note: This code assumes users only have one Linkedin source, which the
    #       production data supports
    for user in RevUpUser.objects.filter(
            contact__contact_type="linkedin-oauth2",
            contact__import_record=None).distinct():

        # Get the count of distinct linkedin import sources. There will
        # typically be 1 or 0. Anything other than 1 is excluded from cleaning.
        c = user.importrecord_set.filter(
            import_type=IR.TaskType.LINKEDIN).values("uid").distinct().count()
        if c == 1:
            ir = user.importrecord_set.filter(
                import_type=IR.TaskType.LINKEDIN).order_by("import_dt").first()
            user.contact_set.filter(contact_type="linkedin-oauth2",
                                    import_record=None)\
                            .update(import_record=ir)


    ## Fix the Outlook Records
    # Since there is only one "outlook" right now, we can just lump them
    # all together under one import record. Group-by "outlook" and user_id.
    for user in RevUpUser.objects.filter(
            contact__contact_type="outlook",
            contact__import_record=None).distinct():
        import_record, created = get_or_create_ir("Outlook", "outlook", user,
                                                  IR.TaskType.OUTLOOK)
        Contact.objects.filter(contact_type="outlook", import_record=None,
                               user=user).update(import_record=import_record)


    ## Fix the CSVs
    # To fix the CSVs we need to look at any user that was created since
    # Aug 21, 2015. If they have only one CSV import, we can assume all
    # CSV contacts are from that source
    for user in RevUpUser.objects.filter(date_joined__gt=EPOCH).filter(
            contact__contact_type="csv",
            contact__import_record=None).distinct():
        c = user.importrecord_set.filter(
            import_type=IR.TaskType.CSV).values("uid").distinct().count()
        if c == 1:
            ir = user.importrecord_set.filter(import_type=IR.TaskType.CSV)\
                                      .order_by("import_dt").first()
            user.contact_set.filter(contact_type="csv", import_record=None)\
                            .update(import_record=ir)


    post_count = Contact.objects.filter(import_record=None).exclude(
        contact_type="merged").count()
    print("\nCleaned: {}. Remaining Dirty: {}".format(pre_count-post_count,
                                                    post_count))
    print("Remaining: ")
    q = Contact.objects.filter(import_record=None)
    print("Gmail: {}".format(q.filter(contact_type="google-oauth2").count()))
    print("Linkedin: {}".format(q.filter(contact_type="linkedin-oauth2").count()))
    print("Outlook: {}".format(q.filter(contact_type="outlook").count()))
    print("CSV: {}".format(q.filter(contact_type="csv").count()))

    # raise Exception


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0021_backpopulate_contact_type'),
    ]

    operations = [
        migrations.RunPython(fix_import_records, lambda x,y: True)
    ]
