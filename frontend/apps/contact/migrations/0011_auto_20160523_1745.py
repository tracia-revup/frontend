# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0010_move_prospect_notes'),
    ]

    operations = [
        migrations.AddField(
            model_name='importrecord',
            name='num_created',
            field=models.IntegerField(default=0, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='importrecord',
            name='num_existing',
            field=models.IntegerField(default=0, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='importrecord',
            name='num_invalid',
            field=models.IntegerField(default=0, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='importrecord',
            name='num_merged',
            field=models.IntegerField(default=0, blank=True),
            preserve_default=True,
        ),
    ]
