# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0008_contact_import_record'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='contact',
            index_together=set([('user', 'contact_id'), ('user', 'last_name_lower', 'first_name_lower'), ('user', 'is_primary')]),
        ),
    ]
