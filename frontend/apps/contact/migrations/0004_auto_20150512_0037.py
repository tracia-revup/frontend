# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion
import frontend.libs.mixins
from django.conf import settings
import frontend.libs.utils.model_utils


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('contact', '0003_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='AlmaMater',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', frontend.libs.utils.model_utils.TruncatingCharField(max_length=256, blank=True)),
                ('major', frontend.libs.utils.model_utils.TruncatingCharField(max_length=256, blank=True)),
                ('degree', frontend.libs.utils.model_utils.TruncatingCharField(max_length=256, blank=True)),
                ('contact', models.ForeignKey(related_name='alma_maters', to='contact.Contact', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model, frontend.libs.mixins.SimpleUnicodeMixin),
        ),
        migrations.CreateModel(
            name='ContactFileUpload',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file_type', models.CharField(default='CSV', max_length=3, db_index=True, choices=[('CSV', 'CSV File')])),
                ('data_file', models.BinaryField()),
                ('user', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.CASCADE, help_text='The user who started this task')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RawContact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('raw_contact', models.TextField(help_text='This is the raw contact data')),
                ('contact', models.OneToOneField(related_name='_raw_contact', to='contact.Contact', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model, frontend.libs.mixins.SimpleUnicodeMixin),
        ),
        migrations.AlterIndexTogether(
            name='contact',
            index_together=set([('user', 'contact_id'), ('user', 'last_name_lower', 'first_name_lower')]),
        ),
    ]
