# -*- coding: utf-8 -*-


from django.db import migrations


def copy_notes(apps, schema_editor):
    """Copy Prospect notes over to the call-sheet notes."""
    Prospect = apps.get_model("campaign", "Prospect")
    ContactNotes = apps.get_model("contact", "ContactNotes")

    # Iterate over all prospects that have saved notes.
    for prospect in Prospect.objects.exclude(notes="").select_related(
            "event__account"):
        contact = prospect.contact
        account = prospect.event.account
        notes = prospect.notes
        try:
            # Check if the user has already created notes for this contact.
            contact_note = ContactNotes.objects.get(contact=contact,
                                                    account=account)
        except ContactNotes.DoesNotExist:
            contact_note = ContactNotes(contact=contact, account=account)

        new_notes = "Prospect Notes:<br/>{}".format(notes)
        if contact_note.notes2:
            if notes in contact_note.notes2:
                # The notes for this contact have already been copied over
                continue

            # If there are already notes in notes2, let's add a newline
            new_notes = "{}<br/>{}".format(contact_note.notes2, new_notes)

        contact_note.notes2 = new_notes
        contact_note.save()


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0009_auto_20160422_1159'),
        ('campaign', '0002_prospect_prospectcollision'),
    ]

    operations = [
        migrations.RunPython(copy_notes,
                             reverse_code=lambda x, y: True)
    ]
