# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0007_contactnotes'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='import_record',
            field=models.ForeignKey(related_name='contacts', on_delete=django.db.models.deletion.CASCADE, blank=True, to='contact.ImportRecord', null=True),
            preserve_default=True,
        ),
    ]
