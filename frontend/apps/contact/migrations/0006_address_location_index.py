# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0005_auto_20150729_1914'),
    ]

    operations = [
        # Add a case-insensitive index on region field
        migrations.RunSQL("CREATE INDEX contact_address_region_insensitive ON contact_address(upper(region))",
                          reverse_sql="DROP INDEX contact_address_region_insensitive"),
        # Add a case-insensitive index on city field
        migrations.RunSQL("CREATE INDEX contact_address_city_insensitive ON contact_address(upper(city))",
                          reverse_sql="DROP INDEX contact_address_city_insensitive")
    ]
