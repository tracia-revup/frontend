# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.utils.timezone
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0019_account_analysis_link'),
        ('contact', '0006_address_location_index'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContactNotes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('notes1', models.TextField(blank=True)),
                ('notes2', models.TextField(blank=True)),
                ('account', models.ForeignKey(related_name='+', to='campaign.Account', on_delete=django.db.models.deletion.CASCADE)),
                ('contact', models.ForeignKey(related_name='notes', to='contact.Contact', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='contactnotes',
            unique_together=set([('contact', 'account')]),
        ),
    ]
