from collections import defaultdict
import logging

from django.conf import settings

from frontend.apps.contact.utils import KafkaMixin
from frontend.libs.utils.kafka_utils import init_producer
from frontend.libs.utils.string_utils import random_uuid

LOGGER = logging.getLogger(__name__)


class KafkaTransactionBase(object):
    topic = None

    def __init__(self, send_contacts_to_backend=None, producer=None):
        if not settings.ENABLE_KAFKA_PUBLISHING:
            send_contacts_to_backend = False
        elif send_contacts_to_backend is None and settings.ENABLE_KAFKA_PUBLISHING:
            send_contacts_to_backend = True
        else:
            send_contacts_to_backend = bool(send_contacts_to_backend)

        self.transaction_id = str(random_uuid())
        self.send_contacts_to_backend = send_contacts_to_backend
        self.producer = producer

    def _action(self, contact, attribute, struct):
        # Only take action on contact attribute if it implements KafkaMixin
        if isinstance(attribute, KafkaMixin):
            data = attribute.format_for_kafka()
            struct[contact][attribute.kafka_key()].append(data)
        elif attribute is None:
            # if attribute is None, then this is a delete action
            struct[contact] = attribute

    def _build_contact_record(self, contact, action_name):
        """Template for contacts records."""
        return {
            'user_id': int(contact.user_id),
            'contact_type': str(contact.contact_type),
            'contact_id': str(contact.contact_id),
            'composite_id': str(contact.composite_id),
            'txid': str(self.transaction_id),
            'action': str(action_name),
            'value': None
        }

    def _process_actions(self, struct, action_name):
        """Iterate over the create and/or archive structures and build the
           contact_action objects.
        """
        action_records = []
        for contact, attributes in struct.items():
            contact_record = self._build_contact_record(contact, action_name)
            if attributes:
                value = defaultdict(list)
                for attribute, data in attributes.items():
                    value[attribute].extend(data)
                contact_record["value"] = dict(value)
            action_records.append(contact_record)
        return action_records

    def build_contact_actions(self):
        """Generate the contact action objects to send to Kafka
        See the Contact Schema wiki for more details.
            https://revupsoftware.atlassian.net/wiki/x/AwAKB
        """
        raise NotImplementedError

    def commit(self, flush=True):
        """Send the contact actions to Kafka.
        See the Contact Schema wiki for more details.
            https://revupsoftware.atlassian.net/wiki/x/AwAKB
        """
        contact_actions = self.build_contact_actions()
        if self.send_contacts_to_backend:
            producer = self.producer or init_producer()
            for contact_record in contact_actions:
                result_future = producer.send(
                    self.topic, contact_record,
                    key=bytes(contact_record["user_id"]))
                if result_future.failed():
                    LOGGER.error(
                        "Error publishing to kafka. Error: {}".format(
                            result_future.exception),
                        exc_info=result_future.exception)
                    raise result_future.exception
            if flush:
                producer.flush()


class ManualContactTransaction(KafkaTransactionBase):
    """Combine multiple edits, archives, creates, etc. to contacts into
    a single transaction.
    This also attempts to group multiple different edits to the same contact
    into one kafka "send".
    """

    def __init__(self, *args, **kwargs):
        self.topic = settings.BACKEND_CONTACT_UPDATES_TOPIC
        super(ManualContactTransaction, self).__init__(*args, **kwargs)
        self.contact_creates = defaultdict(lambda: defaultdict(list))
        self.contact_archives = defaultdict(lambda: defaultdict(list))

    @property
    def contacts(self):
        return set(list(self.contact_creates.keys()) + list(self.contact_archives.keys()))

    def create(self, contact, attribute):
        self._action(contact, attribute, self.contact_creates)

    def archive(self, contact, attribute):
        self._action(contact, attribute, self.contact_archives)

    def build_contact_actions(self):
        """Generate the contact action objects to send to Kafka."""
        # Build a list of archives and creates for attributes on a contact
        return (self._process_actions(self.contact_archives, 'archive') +
                self._process_actions(self.contact_creates, 'create'))


class ContactDeleteTransaction(KafkaTransactionBase):

    def __init__(self, *args, **kwargs):
        self.topic = settings.BACKEND_CONTACT_DELETE_TOPIC
        super(ContactDeleteTransaction, self).__init__(*args, **kwargs)
        self.contact_deletes = dict()

    def build_contact_actions(self):
        """Generate the delete actions."""
        return self._process_actions(self.contact_deletes, 'delete')

    def delete(self, contact):
        self._action(contact, None, self.contact_deletes)
