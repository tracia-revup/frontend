from frontend.libs.utils.celery_utils import (calculate_step_size,
                                              update_progress)

"""
The merge module provides functions for comparing and merging contacts
"""


def contacts_same(contact1, contact2, threshold):
    """
    Compare two contacts and return true if they are likely for the same
    entity relative to a given scoring threshold.
    """
    score = 0
    if contact1.last_name.lower() == contact2.last_name.lower():
        score += 50
        if contact1.first_name.lower() == contact2.first_name.lower():
            score += 50
    return score > threshold


def merge_contacts(contacts):
    """
    Compare a list of contacts with each other, and combine those that seem
    to be for the same entities into sub-lists.
    """

    def __same_indexes(index, target_contact):
        """
        Compare a contact in a list with all subsequent contacts in the list,
        returning the list indexes of the contact and all contacts that seem
        to be for the same entity.
        """
        return [index] + [
            list_index
            for list_index, item in enumerate(
                contacts[index + 1:], start=index + 1)
            if contacts_same(item, target_contact, 75)
        ]

    processed = set()
    results = []

    # Display progress between 5% and 10% for this step
    num_contacts = len(contacts)
    progress = 0.05 * num_contacts
    step_size = calculate_step_size(5, 10)

    for i, target in enumerate(contacts):
        if i not in processed:
            indexes = __same_indexes(i, target)
            processed.update(indexes)
            results.append([contacts[j] for j in indexes])
        progress += step_size
        update_progress(progress, num_contacts)
    return results
