
from collections import MutableMapping, defaultdict, namedtuple
from datetime import datetime
from enum import IntEnum
from functools import cmp_to_key, reduce, wraps
from itertools import groupby, product, combinations, chain
import logging
from multiprocessing import Process, Queue, ProcessError
from operator import attrgetter, mul
from queue import Empty
from time import sleep

from ddtrace import tracer
from django.conf import settings
from django.db import transaction, connection, connections
from django.db.models import Q, Count, F, Func, CharField
from django.db.models.functions import Lower, Substr, Coalesce
import networkx as nx
import phonenumbers

from frontend.apps.analysis.utils import ranking_counts_by_account
from frontend.apps.call_time.models import (
    CallTimeContact, CallTimeContactSet, CallLog)
from frontend.apps.campaign.models import Prospect, ProspectCollision, Account
from frontend.apps.contact.deletion import ContactDeleter
from frontend.apps.contact.models import (
    Contact, ContactNotes, EmailAddress, PhoneNumber, Address, LimitTracker)
from frontend.apps.contact.exceptions import MergeConflict
from frontend.apps.entities.models import Identity, ContactLink, PersonDimension
from frontend.apps.filtering.models import ContactIndex
from frontend.libs.utils.address_utils import AddressParser
from frontend.libs.utils.general_utils import (union_find_reducer, flatten,
                                               compare)
from frontend.libs.utils.model_utils import ArrayAgg, ARRAY
from frontend.libs.utils.namelib_utils import namelib
from frontend.libs.utils.string_utils import (similarity, strip_punctuation,
                                              ensure_unicode,
                                              scrub_and_lowercase)

logger = logging.getLogger(__name__)


class ResultProxy(object):
    def __init__(self, contacts, values=None):
        self.values = values
        self.contacts = contacts


class NoParentFound(Exception):
    pass


# These are used in Address partitioning
AddressTuple = namedtuple('AddressTuple', ['street', 'city', 'state', 'zip'])

class AddressMatch(IntEnum):
    match = 1
    neutral = 0
    differs = -1


class ResultRegistry(MutableMapping):
    """Result registry used by _groupify function

    This class is used to cover an odd edge case of linking to a previously
    discovered result, which may go several layers deep.

    This class expects keys and values to be IdHashableSet instances.
    """
    def __init__(self):
        self._register = {}

    def __getitem__(self, key):
        return self._register[key]

    def __setitem__(self, key, value):
        assert isinstance(key, IdHashableSet)
        assert isinstance(value, IdHashableSet)
        if key in self._register:
            # If we are updating the value for a key that already exists, it
            # means we found a link to a previous result. We can detect when a
            # "link to a previous result" was made by performing an identity
            # check between key and value. If they are not the same, then a
            # link was made to that key. By using the value as a key, we can
            # move up the tree of linked results. This is to guard against an
            # issue discovered during testing where contact ids were dropped,
            # or returned twice.
            oldvalue = self._register[key]
            value.update(oldvalue)
            if oldvalue is not key:
                self[oldvalue] = value
        return self._register.__setitem__(key, value)

    def __delitem__(self, key):
        return self._register.__delitem__(key)

    def __iter__(self):
        return self._register.__iter__()

    def __len__(self):
        return len(self._register)

    def iter_unique_values(self):
        # Return unique contact groups. We can check this by using an identity
        # check between key and value.
        for key, value in self._register.items():
            if key is value:
                yield value


class IdHashableSet(set):
    """This is a set subclass that can be used in a set or as a dict key.

    The hash is the id (aka the memory address) of the object, so it is stable.
    Additionally the set is still mutable, unlike frozenset.

    Another key difference from frozensets is that IdHashableSets won't be
    deduped based on contents. For example:

    >>> setofsets = set()
    >>> a = IdHashableSet([1,2])
    >>> b = IdHashableSet([1,2])
    >>> setofsets.add(a)
    >>> setofsets
    {{1, 2}}
    >>> setofsets.add(b)
    >>> setofsets
    {{1, 2}, {1, 2}}


    With frozensets:

    >>> q = frozenset([1,2])
    >>> w = frozenset([1,2])
    >>> setoffrozensets = set()
    >>> setoffrozensets.add(q)
    >>> setoffrozensets
    {frozenset({1, 2})}
    >>> setoffrozensets.add(w)
    >>> setoffrozensets
    {frozenset({1, 2})}
    """

    def __hash__(self):
        return id(self)


@tracer.wrap()
def _handle_first_name(result, pks, mega_last_name, first_name, names=None,
                       name_expansion=True):
    """Recursive function that updates the contact group with newly discovered
    primary keys, and updates the result registry to indicate a link between
    results.

    Performs nickname expansion for names found, as well as prefix matching.

    Returns a tuple of: (set(first_names), IdHashableSet(pks))
    """
    if names is None:
        names = set([first_name])
    else:
        names.add(first_name)
    found = mega_last_name[first_name]
    pks.update(found)
    if isinstance(found, IdHashableSet):
        # If the value in mega_last_name is an IdHashableSet, it means that
        # name has already been processed, and we can stop early. This is to
        # prevent infinite recursion.
        if found is not pks:
            # If found is not the same as our current result set, that means
            # that we found a link to a name that was previously processed. In
            # order to prevent a contact from ending up in two separate groups,
            # we need to slurp the results of the old group into our new group,
            # and update the result register to point to our new result group.
            # This tells _groupify not to return that value, and it also is how
            # we follow a chain of links to ensure no results are lost.
            result[found] = pks
        return names, pks

    # Replace the value of the name we're currently processing with pks (our
    # current result set) in order to mark that name as processed.
    mega_last_name[first_name] = pks

    # Prefix matching
    for fname in list(mega_last_name.keys()):
        if fname not in names and (fname.startswith(first_name) or
                                   first_name.startswith(fname)):
            _handle_first_name(result, pks, mega_last_name, fname, names,
                               name_expansion=name_expansion)

    if not name_expansion:
        return names, pks

    nicknames = namelib.get_nicknames(first_name, allow_empty_result=True)
    # If nicknames is None, name expansion is disabled
    if nicknames is None:
        return names, pks

    if not nicknames and ' ' in first_name:
        # If nicknames is an empty list, it means no results were found in the
        # peacock database. If we got no results and there is a space in our
        # first name, suspect we got a name that contains a middle name.
        parts = first_name.split()
        for name_part in parts:
            name_part = name_part.strip("\"'., ")
            if (len(name_part) > 2 and
                    name_part not in names and
                    name_part in mega_last_name):
                _handle_first_name(result, pks, mega_last_name, name_part,
                                   names, name_expansion=name_expansion)

    # Process nicknames from the peacock data (if any).
    for name in nicknames:
        if name not in names and name in mega_last_name:
            _handle_first_name(result, pks, mega_last_name, name, names,
                               name_expansion=name_expansion)
    return names, pks


@tracer.wrap()
def _groupify(megadict, name_expansion=True):
    """Group contacts based on their names

    Groups contacts on their names using multiple strategies:
    - Prefix matching
    - Name expansion
    - Alias expansion

    Expects a "megadict" created by the `_create_megadict_for_user` function in
    the form of: `{last_name: {first_name: [pks]}}`

    Returns a generator, where each value is a set of primary keys of contacts
    that should be grouped together.
    """
    logger.info("Starting groupify")
    result = ResultRegistry()
    last_names = list(megadict.keys())
    for last_name in last_names:
        mega_last_name = megadict[last_name]
        first_names = list(mega_last_name.keys())
        for first_name in first_names:
            if (isinstance(mega_last_name[first_name], IdHashableSet) and
                    mega_last_name[first_name] in result):
                # We need to check to see if first_name is still in
                # mega_last_name because we are mutating mega_last_name even as
                # we are iterating on it. This also happens to be a good spot
                # to see if the name has already been processed, and if so,
                # skip it.
                continue

            pks = IdHashableSet()
            result[pks] = pks

            names, _ = _handle_first_name(result, pks, mega_last_name,
                                          first_name,
                                          name_expansion=name_expansion)
            # Iterate through every first name we found, and use it to build a
            # query looking for aliases.
            alias_names = {}
            aliases = Identity.objects.aliases((last_name, names))
            for alias in aliases.iterator():
                alias_names.setdefault(
                    Contact._clean_and_lowercase_name(alias.family_name),
                    set()).add(Contact._clean_and_lowercase_name(
                        alias.given_name))

            for lname, fnames in alias_names.items():
                if lname in megadict:
                    if lname == last_name:
                        # If lname is the same as the last_name we're currently
                        # processing, provide names to _handle_first_name as a
                        # hint to cut down on reprocessing names
                        deduper = names
                    else:
                        deduper = None
                    mega_lname = megadict[lname]
                    for fname in fnames:
                        # If deduper is not None, it is names, so we can use it
                        # as a hint to determine whether we've processed the
                        # name we just found via aliases.
                        if fname in mega_lname and not (deduper is not None and
                                                        fname in deduper):
                            _handle_first_name(result, pks, mega_lname, fname,
                                               names=deduper,
                                               name_expansion=name_expansion)

    return result.iter_unique_values()


@tracer.wrap()
def _new_handle_first_name(mega_last_name, first_name, names=None):
    """Recursive function that updates the contact group with newly discovered
    primary keys, and updates the result registry to indicate a link between
    results.

    This version of `_handle_first_name` varies from the original version in
    that dependencies on ResultRegistery was removed, and nickname expansion
    was removed. See the docstring on `_new_groupify` for more details on the
    reasoning behind this.

    Returns a tuple of: (set(first_names), set(pks))
    """
    if names is None:
        names = set([first_name])
    else:
        names.add(first_name)
    pks = set(mega_last_name.pop(first_name, set()))
    if not pks:
        return names, pks

    # Prefix matching
    for fname in list(mega_last_name.keys()):
        if fname not in names and (fname.startswith(first_name) or
                                   first_name.startswith(fname)):
            ns, ps = _new_handle_first_name(mega_last_name, fname, names)
            names.update(ns)
            pks.update(ps)

    return names, pks


@tracer.wrap()
def _new_groupify(megadict):
    """Group contacts using prefix matching

    Expects a "megadict" created by the `_create_megadict_for_user` function in
    the form of: `{last_name: {first_name: [pks]}}`

    This version of groupify varies significantly from the original version in
    several ways:
    - ResultRegistry was removed, and no overlap correction occurs here. The
      reason for this is that ResultRegistry's performance is sub-optimal, and
      more performant ways of merging contacts has been discovered. In addition,
      we're going to have to fix overlaps caused by force-merge groups and alias
      expansion groups anyway, so it makes very little sense to do overlap
      correction until it is absolutely necessary.
    - Alias expansion was extracted to another method, and will be handled
      during a different step in order to keep the number of database queries
      performed to an absolute minimum.
    - Name expansion has been removed. This was done because we're not using it
      currently anyway, and until we can handle things like preventing people of
      different sexes from being merged, there's no point. When we're ready for
      it again, I'll implement it like I did with force merges and alias
      expansion, and do all lookups after initial prefix matching has been done,
      keeping db queries to the minimum necessary.

    These changes greatly increase the performance of the remerge tool, and the
    groupify function in general. Instead of waiting anywhere from 20 minutes
    to 1+ hours for prefix merging and alias expansion to finish, it went down
    to ~100 seconds.

    Returns a list of sets of contact ids.
    **NOTE** That is a very good chance that there will be groups containing
    overlap. The output of this function will need to be put through a function
    to merge groups with contacts in common together.
    """
    result = []
    last_names = list(megadict.keys())
    for last_name in last_names:
        if last_name not in megadict:
            continue
        mega_last_name = megadict[last_name]
        first_names = list(mega_last_name.keys())
        for first_name in first_names:
            if first_name not in mega_last_name:
                continue
            names, pks = _new_handle_first_name(mega_last_name, first_name)
            result.append(pks)
    return result


@tracer.wrap()
def _trie_groupify(megadict, split_size=3):
    """Use a trie to handle prefix matching

    This is basically the ultimate form of groupify. It offers a monumental
    performance leap over even `_new_groupify`, taking no more than 6 seconds
    to perform prefix matching on half a million contacts.
    """
    groups = set()
    indexes = []
    for last_name, last_name_group in megadict.items():
        index = PrefixMergeNode.populate_index(last_name_group,
                                               split_size=split_size)
        index.family_name = last_name
        groups_ = set(map(frozenset, index.iter_data()))
        indexes.append((index, groups_))
        groups.update(groups_)
    return groups, indexes


class PrefixMergeNode(MutableMapping):
    """This is basically a poor-man's Trie datastructure.

    Literature about tries':
    https://en.wikipedia.org/wiki/Trie
    https://www.hackerearth.com/practice/notes/trie-suffix-tree-suffix-array/

    We use this datastructure to apply name prefix matching rules. The nested
    nature of the datastructure greatly reduces the number of names we have to
    check for a match.
    """
    def __init__(self, max_key_size, key_size=None):
        self._data = frozenset()
        self._children = {}
        self._max_key_size = max_key_size
        self._key_size = key_size or max_key_size
        self._has_small_key = False
        self._small_keys = set()

    def __getitem__(self, key):
        r = self._children.get(key, KeyError)
        if r is KeyError:
            raise KeyError("'{0}'".format(key))
        return r

    def __setitem__(self, key, new_node):
        assert_msg = ("This is for assigning new keys only. Do not overwrite "
                      "existing values!")
        assert key not in self, assert_msg
        self._children[key] = new_node

    def __delitem__(self, key):
        key = self._lookup_key(key)
        self._children.__delitem__(key)

    def __len__(self):
        return len(self._children)

    def __contains__(self, key):
        return key in self._children

    def __iter__(self):
        for key in self._children:
            yield key

    def __repr__(self):
        return repr(self._children)

    @classmethod
    def populate_index(cls, items_to_index, split_size=3):
        top = cls(split_size)
        for key, value in items_to_index.items():
            top.index_item(key, value)
        return top

    def index_item(self, key, value):
        prefix = self._lookup_key(key)
        rest = key[len(prefix):]
        node = self.get_or_create(prefix)
        if not rest:
            node._update_data(value)
        else:
            node.index_item(rest, value)

    def get_or_create(self, key):
        # TODO: make thread safe
        if key in self:
            return self[key]
        else:
            key_size = len(key)
            node = None
            if key_size < self._key_size:
                node = self._clone(self._key_size - key_size)
                self._has_small_key = True
                self._small_keys.add(key)
                to_move = []
                for big_key in self:
                    if big_key.startswith(key):
                        to_move.append(big_key)

                for big_key in to_move:
                    if big_key in self._small_keys:
                        self._small_keys.remove(big_key)
                    suffix = big_key[key_size:]
                    other_value = self.pop(big_key)
                    node[suffix] = other_value

            elif self._has_small_key:
                node = self._clone()
                nest_dest_name = self._small_key_lookup(key)
                if nest_dest_name:
                    suffix = key[len(nest_dest_name):]
                    nest_dest = self[nest_dest_name]
                    nest_dest[suffix] = node
                    return

            if node is None:
                node = self._clone()
            self[key] = node
        return node

    def get_data(self):
        return list(self.iter_data())

    def iter_data(self):
        data = flatten(child.iter_data()
                       for child in self._children.values())
        if self._data:
            data = self._data.union(*data)
        return data

    def _clone(self, override=None):
        return self.__class__(self._max_key_size,
                              override or self._max_key_size)

    def _lookup_key(self, name):
        if name in self._children:
            return name
        if self._has_small_key:
            small_key = self._small_key_lookup(name)
            if small_key:
                return small_key
        if len(name) > self._key_size:
            name = name[:self._key_size]
        return name

    def _small_key_lookup(self, key):
        for small_key in self._small_keys:
            if key.startswith(small_key):
                return small_key

    def _update_data(self, data):
        self._data = self._data.union(data)
        return self._data


@tracer.wrap()
def _create_megadict_for_user(user):
    """Create a dictionary containing every contact pk for the user.
    This does a first-pass grouping of contacts with the same name.

    Returns a dict in the form of:
        {last_name: {first_name: [pk]}}
    """
    logger.info("Creating datastructure of all unique names")
    contacts = user.contact_set.exclude(
        Q(last_name_lower__length__lt=2) | Q(first_name_lower__length__lt=2) |
        Q(contact_type='manual') | Q(needs_attention=True)).values_list(
            'last_name_lower', 'first_name_lower', 'pk')

    megadict = {}

    for last_name, first_name, pk in contacts.iterator():
        megadict.setdefault(last_name, {})\
                .setdefault(first_name, [])\
                .append(pk)
    return megadict


@tracer.wrap()
def _fetch_force_merge_groups(user):
    groups = PersonDimension.dimensions.filter(
        dimension_type=PersonDimension.PersonDimensionTypes.FORCE_MERGE,
        user=user).annotate(
            contacts=ArrayAgg('contact_links__contact_id')).values_list(
                'contacts', flat=True)
    return [set(filter(None, g)) for g in groups]


@tracer.wrap()
def _fetch_alias_expansion_groups(user):
    """Fetch all knowledge about contact merges with a single command

    Moving alias expansion out of `_groupify()` provides significant speedups
    to the remerge tool. Reuse the same idea used in
    `_fetch_force_merge_groups`, where we create groups of contacts on this
    dimension, and then combine groups that have overlap to get us to a good
    state.
    """
    query = """
    SELECT array_agg(DISTINCT cc_out.id) as contact_ids
    FROM contact_contact cc_out
    JOIN "entities_identity" ident_output ON (
        cc_out.user_id = %(user_id)s AND
        cc_out.first_name = ident_output.given_name AND
        cc_out.last_name = ident_output.family_name)
    INNER JOIN "entities_persondimension" pd ON (
        ident_output."dimension_id" = pd."id")
    INNER JOIN "entities_identity" ident_input ON (
        pd."id" = ident_input."dimension_id")
    INNER JOIN contact_contact cc1 ON (
        cc1.user_id = %(user_id)s AND
        cc1.first_name = ident_input.given_name AND
        cc1.last_name = ident_input.family_name)
    WHERE
        cc_out.user_id = %(user_id)s AND
        cc_out.needs_attention <> true AND
        pd."dimension_type" = 'ALIAS'
    GROUP BY pd.id
    """
    with connection.cursor() as cursor:
        cursor.execute(query, {'user_id': user.id})
        groups = cursor.fetchall()
    groups = (filter(None, g) for (g,) in groups)
    return [set(g) for g in groups if g]


class ContactPartitioner(object):
    @staticmethod
    @tracer.wrap()
    def unique_sanity_check(sets):
        """Sanity check function to look for duplicates

        Given a collection of sets, returns False if a value is found in more
        than one set.
        """
        for a, b in combinations(sets, 2):
            if a.intersection(b):
                return False
        return True

    @classmethod
    @tracer.wrap()
    def group_using_function(cls, contacts, data_collect_func,
                             keep_singletons=False, multi_collect=True):
        """Group contacts using some piece of data they have in common

        Contacts with no data are not included in the output.

        Arguments:
        contacts: a collection of Contact instances
        data_collect_func: function which takes a contact as an argument, and
                           returns a set of data which is used to find
                           commonalities.
        keep_singletons: boolean used to determine whether groups containing
                         only a single contact are filtered out of the output.

        Returns:
            a set of frozen sets of contacts.
        """
        groups = cls._group_using_function(contacts, data_collect_func,
                                           multi_collect=multi_collect)
        return cls._group_using_function_finalize(iter(groups.values()),
                                                  keep_singletons)

    @classmethod
    def _group_using_function(cls, contacts, data_collect_func,
                              multi_collect=True):
        # Result collection. Keys will be an individual piece of data (ex:
        # email address) being used to find matches. Values will be a
        # ResultProxy which contains a pointer to the set of contacts with
        # that email address in common, and a pointer to another set which
        # contains all email addresses those contacts have in common.
        #
        # The proxy is used to cut down on the amount of work we do. Changing
        # the pointer in one key will update the set of contacts several keys
        # in the groups dict point to.
        groups = {}
        # Extract unique set of data (like email addresses) from contacts to
        # be used to find other contacts with the same data
        if multi_collect:
            contact_map = data_collect_func(contacts)
        else:
            contact_map = {contact: data_collect_func(contact)
                           for contact in contacts}
        for contact, values in contact_map.items():
            group = {contact}
            proxy = ResultProxy(group, values)
            # Make a copy of the values for the contact that we can modify for
            # purposes of iteration.
            values_to_check = values.copy()
            while 1:
                try:
                    value = values_to_check.pop()
                except KeyError:
                    break
                found = groups.setdefault(value, proxy)
                # If found.contacts is not our group, then we just found a
                # contact we have something in common with, and further actions
                # need to be performed in order to group the contacts together.
                if found.contacts is not group:
                    # If there are values (ie- email addresses) the other
                    # contact has that the contact we are currently processing
                    # does not have, we need to add those values to
                    # `values_to_check` so we iterate over them. We need to
                    # iterate over them in case there are deep links discovered
                    # (ex: A -> B -> C -> D), to ensure that we replace the
                    # contacts set on the proxy to use our new `group` value to
                    # prevent duplicates.
                    values_to_check.update(
                        found.values.difference(values))
                    values.update(found.values)
                    group.update(found.contacts)
                    found.values = values
                    found.contacts = group
        return groups

    @classmethod
    def _group_using_function_finalize(cls, groups, keep_singletons):
        # Extract the set of contacts from the proxies.
        result = set()
        for value in groups:
            # Discard contact sets that only have a single contact in them
            # unless keep_singletons is true. We discard singleton groups
            # because they don't tell us anything new, and can't be use to find
            # further groups.
            if keep_singletons or len(value.contacts) > 1:
                result.add(frozenset(value.contacts))

        # Perform a sanity check to ensure we don't return any contact sets
        # with a contact in common.
        assert cls.unique_sanity_check(result), (
            "A duplicate was found: {}".format(result))
        return result

    @staticmethod
    @tracer.wrap()
    def _get_contacts_emails(contacts):
        contact_id_map = {c.id: c for c in contacts}
        qs = EmailAddress.objects.filter(contact_id__in=list(contact_id_map.keys()))\
                .exclude(address='')\
                .filter(archived=None)\
                .values('contact_id')\
                .annotate(addresses=ArrayAgg(Lower('address'), distinct=True))\
                .values_list('contact_id', 'addresses')

        contact_emails_map = {contact_id_map[c_id]: set(emails)
                              for c_id, emails in qs if emails}
        return contact_emails_map

    @staticmethod
    @tracer.wrap()
    def _get_contact_emails(contact):
        """Get set of all email addresses for contact"""
        return set(contact.email_addresses.exclude(
            address='').filter(archived=None).annotate(
                address_lower=Lower('address')).distinct(
                    'address_lower').values_list(
                        'address_lower', flat=True))

    @classmethod
    @tracer.wrap()
    def group_by_email(cls, contacts, multi_collect=True):
        """Create groups of contacts that have an email address in common

        Returns a list of sets containing contacts. Each set will have at least
        one email address in common. Contacts with no email address records
        will not be included in the output.

        Groups with only a single contact will be filtered out since they do
        not help us.
        """
        if multi_collect:
            collect_func = cls._get_contacts_emails
        else:
            collect_func = cls._get_contact_emails
        return cls.group_using_function(contacts, collect_func,
                                        multi_collect=multi_collect)

    @staticmethod
    @tracer.wrap()
    def _get_contacts_phone_numbers(contacts):
        contact_id_map = {c.id: c for c in contacts}
        qs = PhoneNumber.objects.filter(contact_id__in=list(contact_id_map.keys()))\
                .exclude(number='')\
                .filter(archived=None)\
                .values('contact_id')\
                .annotate(numbers=ArrayAgg('number', distinct=True))\
                .values_list('contact_id', 'numbers')

        def _transform(numbers):
            for number in numbers:
                value = PhoneNumber.format_number(
                    number, phonenumbers.PhoneNumberFormat.NATIONAL)
                if value:
                    yield value

        contact_values_map = {}
        for contact_id, numbers in qs:
            contact = contact_id_map[contact_id]
            values = set(_transform(numbers))
            if values:
                contact_values_map[contact] = values
        return contact_values_map

    @staticmethod
    @tracer.wrap()
    def _get_contact_phone_numbers(contact):
        """Get set of all phone numbers for contact in common format"""
        phonenums = contact.phone_numbers.exclude(
            number='').filter(archived=None).distinct('number').only('number')
        numbers = set()
        for phone in phonenums:
            try:
                numbers.add(phone.national)
            except phonenumbers.NumberParseException:
                pass
        return numbers

    @classmethod
    @tracer.wrap()
    def group_by_phone_numbers(cls, contacts, multi_collect=True):
        """Create groups of contacts that have a phone number in common

        Returns a list of sets containing contacts. Each set will have at least
        one phone number in common. Contacts with no phone number records will
        not be included in the output.

        Groups with only a single contact will be filtered out since they do
        not help us.
        """
        if multi_collect:
            collect_func = cls._get_contacts_phone_numbers
        else:
            collect_func = cls._get_contact_phone_numbers
        return cls.group_using_function(contacts, collect_func,
                                        multi_collect=multi_collect)

    @staticmethod
    @tracer.wrap()
    def _get_contacts_addresses(contacts):
        contact_id_map = {c.id: c for c in contacts}
        qs = Address.objects.filter(contact_id__in=list(contact_id_map.keys()))\
                .exclude(street='', city='', region='', post_code='',
                         zip_key=None, city_key=None)\
                .filter(archived=None)\
                .values('contact_id')\
                .annotate(addresses=ArrayAgg(ARRAY(
                    Lower('street'),
                    Lower(Coalesce(F('city_key__name'), F('city'),
                                   output_field=CharField())),
                    Lower(Coalesce(F('city_key__state__abbr'), F('region'),
                                   output_field=CharField())),
                    Coalesce(F('zip_key__zipcode'), F('post_code'),
                             output_field=CharField())), distinct=True))\
                .values_list('contact_id', 'addresses')

        def _clean(str_):
            if str_ is None:
                return ""
            return " ".join(strip_punctuation(str_).split())

        def _transform(contact_addrs):
            for street, city, state, zipcode in contact_addrs:
                value = AddressTuple(_clean(street), _clean(city),
                                     _clean(state), _clean(zipcode)[:5])
                if any(value):
                    yield value

        contact_values_map = {}
        for contact_id, addrs in qs:
            contact = contact_id_map[contact_id]
            values = set(_transform(addrs))
            if values:
                contact_values_map[contact] = values
        return contact_values_map

    @staticmethod
    @tracer.wrap()
    def _get_contact_addresses(contact):
        """Return set of all addresses for contact

        Returns a set of (street_address, post_code) tuples
        """
        contact_addrs = contact.addresses.filter(archived=None).annotate(
            street_lower=Lower('street'),
            city_lower=Lower(Func(F('city_key__name'),
                                  F('city'),
                                  function='COALESCE',
                                  output_field=CharField())),
            state_lower=Lower(Func(F('city_key__state__abbr'),
                                   F('region'),
                                   function='COALESCE',
                                   output_field=CharField())),
            zipcode=Func(F('zip_key__zipcode'),
                         F('post_code'),
                         function='COALESCE',
                         output_field=CharField())
        ).distinct(
            'street_lower', 'city_lower', 'state_lower', 'zipcode').values_list(
            'street_lower', 'city_lower', 'state_lower', 'zipcode')
        # Cut out punctuation and remove excess whitespace from the address.
        # Limit the zip-code to 5 digits.
        def _clean(str_):
            if str_ is None:
                return ""
            return " ".join(strip_punctuation(str_).split())

        return set(AddressTuple(_clean(street), _clean(city), _clean(state),
                                _clean(zipcode)[:5])
                   for street, city, state, zipcode in contact_addrs)

    @classmethod
    def _matrix_pair(cls, id1, id2):
        """Return an ordered tuple."""
        return tuple(sorted((id1, id2)))

    @classmethod
    @tracer.wrap()
    def _build_address_matrix(cls, contacts, multi_collect=True):
        """For every contact, we extract its address information and store
        it in a dictionary. If a contact has multiple addresses, each
        address gets a different entry in the dict.
        """
        address_matrix = dict()
        if multi_collect:
            contact_map = cls._get_contacts_addresses(contacts)
        else:
            contact_map = {contact: cls._get_contact_addresses(contact)
                           for contact in contacts}
        for contact, address_tuples in contact_map.items():
            for i, address_tuple in enumerate(address_tuples):
                # We shouldn't get here without any info. Skip if we did
                if not any(address_tuple):
                    continue

                # The address matrix uses a tuple key so that each of a
                # contact's addresses has its own entry in the dict. Each
                # address will increment the second/index value in the tuple.
                address_matrix[(contact.id, i)] = address_tuple
        return address_matrix

    @classmethod
    @tracer.wrap()
    def _compare_address(cls, address1, address2):
        """Compare the address tuple field-by-field. If either addresses' field
          is blank, we label it "no opinion". We only compare fields when
          they both contain a value.

          :returns: a namedtuple (AddressTuple) containing the results of
                    the comparison of each field.
        """
        field_matches = []
        # Iterate over every field that is not street (index 0)
        for i, addr1_field in enumerate(address1):
            # If either field is blank, we don't compare them
            addr2_field = address2[i]
            if not all((addr1_field, addr2_field)):
                field_matches.append(AddressMatch.neutral)
                continue

            if i == 0:
                # Street gets a fuzzy match
                match = (similarity(addr1_field, addr2_field) >=
                         settings.MERGING_ADDRESS_SIMILARITY_THRESHOLD)
            else:
                match = addr1_field == addr2_field
            field_matches.append(AddressMatch.match if match else
                                 AddressMatch.differs)
        return AddressTuple(*field_matches)

    @classmethod
    @tracer.wrap()
    def _should_pair(cls, address1, address2):
        """Compare two addresses and determine if they should be paired"""
        addr_matches = cls._compare_address(address1, address2)
        # Convert the 3-state address matches into something we can examine
        has_matches = False
        has_differs = False
        has_neutrals = False
        for field in addr_matches:
            if field is AddressMatch.match:
                has_matches = True
            elif field is AddressMatch.neutral:
                has_neutrals = True
            elif field is AddressMatch.differs:
                has_differs = True

        # Compute the "weight" of a match. This is very rudimentary.
        # Assigning the weights street=6, city=2, state=1, zipcode=4
        weighted_matches = list(map(mul, (6, 2, 1, 4), addr_matches))
        weight = sum(weighted_matches)

        if has_matches and not has_differs:
            return True, weight
        elif has_neutrals and not any([has_differs, has_matches]):
            return None, weight
        else:
            # We want to be able to handle certain special cases. We've seen
            # contacts where everything matches except the zipcode. This is to
            # add a layer of "good enough" matching:
            # street & ( zip | ( city & state) )
            if addr_matches.zip is AddressMatch.match or \
                    (addr_matches.city is AddressMatch.match and
                     addr_matches.state is AddressMatch.match):
                if addr_matches.street is AddressMatch.match:
                    return True, weight
                # Compare the two streets by their parsed tokens. This is a
                # less specific comparison than the edit-distance algorithm
                # below, so it should only be used in this special
                # case comparison. This helps match addresses where one is
                # missing some information the other address has;
                # like a suite number or abbreviated information
                elif (AddressParser(address1[0]).apples_to_apples(address2[0])
                        >= settings.MERGING_ADDRESS_SIMILARITY_THRESHOLD):
                    return True, weight
        return False, weight

    @classmethod
    @tracer.wrap()
    def _build_address_matrix_edges(cls, address_matrix):
        """Compare every address in the address matrix to decide how/if their
           contacts should be paired.
        Based on the address information, it could be a valid pair,
        invalid pair, or indifferent.
        """
        matrix_keys = list(address_matrix.keys())

        valid_edges = set()
        invalid_edges = set()
        neutral_edges = set()
        # Use the contacts address info, field-by-field to look for
        # matching addresses, or conflicting addresses. Add them to their
        # respective sets.
        for i, focus_contact_key in enumerate(matrix_keys):
            for other_contact_key in matrix_keys[i+1:]:
                flag, weight = cls._should_pair(
                                            address_matrix[focus_contact_key],
                                            address_matrix[other_contact_key])
                ordered_pair = cls._matrix_pair(focus_contact_key,
                                                other_contact_key)
                # True flag means these addresses match, False means there is
                # conflicting information.
                if flag is None:
                    struct = neutral_edges
                elif flag:
                    struct = valid_edges
                else:
                    struct = invalid_edges
                struct.add((ordered_pair, weight))
        return valid_edges, invalid_edges, neutral_edges

    @classmethod
    @tracer.wrap()
    def _build_clique_web(cls, graph, clique, other_clique):
        """Given two cliques, generate a web of edges between every node, so
          that they are fully-connected.

        A "clique web" is just a fully connected subgraph between two cliques.
        That means every node in each clique will have an edge to every node
        in the other clique.
        For example, if we have the cliques {A, B, C} and {D, E, F}, this
        function will return the edges: (AD, AE, AF, BD, BE, BF, CD, CE, CF).

        We would use this when we want to forcibly combine two cliques into
        one. The most common usecase is when a single Contact has two different
        addresses in two different cliques. We know we can force those cliques
        together because the Contact informs us they are the same.
        """
        new_edges = set()
        for edge in product(clique, other_clique):
            # We need to check if the graph already has this edge because
            # the weight that we give here will overwrite an existing weight
            if not graph.has_edge(*edge):
                # If we encounter our counterpart address (from same Contact),
                # we want to give it a very strong edge so we don't break it
                # apart when we do edge-pruning later.
                weight = 1 if edge[0][0] != edge[1][0] else 100
                new_edges.add((cls._matrix_pair(*edge), weight))
        return new_edges

    @classmethod
    @tracer.wrap()
    def _reduce_multi_address_cliques(cls, graph):
        """Compare cliques in a graph. If any cliques have different addresses
         from the same contact, we want to merge those cliques into one.

         The way cliques can be merged is to create a web of edges connecting
         every node to every other node in the two cliques

        :returns: a set of edges that make-up the web connecting cliques
        """
        # Build a structure that looks at all node keys
        # (format of a key is (contact_id, address_index)) to see if
        # there are any contacts with multiple indexes (i.e. addresses)
        multi_addr_finder = defaultdict(set)
        for key in graph.nodes:
            multi_addr_finder[key[0]].add(key)
        # Extract any contacts that have multiple addresses
        # E.g. [ [(100,0), (100,1), (100,2)], [(105,0), (105,1)] ]
        multi_addr_contacts = [values
                               for values in multi_addr_finder.values()
                               if len(values) > 1]

        # A worker function that allows us to reduce cliques in a loop until
        # they are fully reduced.
        def reduce_cliques_worker(multi_addr_cliques):
            edges = set()
            # For every multi-addr contact, we will find its counterparts and
            # combine them into one clique
            for multi_addr_keys in multi_addr_contacts:
                for key, other_key in combinations(multi_addr_keys, 2):
                    if key is other_key or key[1] == other_key[1]:
                        continue

                    # Each keys has one or more cliques associated with it. We
                    # will iterate over every combination of cliques for those
                    # two keys, and build a web of edges between each clique.
                    cliques = multi_addr_cliques[key]
                    other_cliques = multi_addr_cliques[other_key]
                    for clique, other_clique in product(cliques, other_cliques):
                        if clique is other_clique:
                            continue
                        else:
                            edges.update(cls._build_clique_web(
                                graph, clique, other_clique))
            return edges

        new_edges = set()
        while True:
            # For every multi-address contact, we will find all the
            # cliques it is in
            multi_addr_cliques = nx.clique.cliques_containing_node(
                graph, nodes=list(chain.from_iterable(multi_addr_contacts)))
            edges = reduce_cliques_worker(multi_addr_cliques)
            # If no new edges are returned, the cliques are completely reduced
            if edges:
                cls._add_weighted_edges(graph, edges)
                new_edges.update(edges)
            else:
                break
        # Return the new edges mostly for testing reasons. The edges must
        # be added in the loop above, so the work is already done.
        return new_edges

    @classmethod
    @tracer.wrap()
    def _add_weighted_edges(cls, graph, edges):
        # Small helper function to keep code DRY.
        # Converts our edge format to networkx graph format.
        graph.add_weighted_edges_from((e[0], e[1], w) for e, w in edges)

    @classmethod
    @tracer.wrap()
    def _address_partitions(cls, address_matrix):
        valid_edges, invalid_edges, neutral_edges = \
                cls._build_address_matrix_edges(address_matrix)
        # If there are no valid edges, there won't be any groups
        if not valid_edges:
            return []
        all_contacts_w_addr = set(address_matrix.keys())
        neutral_edge_set = {key for key, w in neutral_edges}

        groups = []
        # Use a Clique graph algorithm to find clusters of addresses.
        # A Clique is a fully-connected/complete sub-graph.
        graph = nx.Graph()
        graph.add_nodes_from(all_contacts_w_addr)
        cls._add_weighted_edges(graph, valid_edges)

        # Combine multi-address cliques into one
        cls._reduce_multi_address_cliques(graph)

        # Add in neutral edges to increase clique size/reach
        cls._add_weighted_edges(graph, neutral_edges)

        # Prune overlapping edges:
        # Iterate over all of the overlaps and remove the lowest weight
        # edges until either the overlap cleanly fits into one clique,
        # or the overlapping contact has been fully extracted.
        while True:
            # Generate the groups using cliques.
            groups = list(set(group)
                          for group in nx.clique.find_cliques(graph))

            # Look for any cliques with overlapping addresses.
            overlaps = set()
            for group in groups:
                for other_group in groups:
                    if group is other_group:
                        continue
                    overlaps.update(group.intersection(other_group))

            # Once there are no overlaps, our work is done.
            if not overlaps:
                break

            # Iterate over the overlaps and eliminate any cliques that are
            # created from purely neutral edges
            remove_edges = set()
            overlap_cliques = nx.clique.cliques_containing_node(
                graph, nodes=list(overlaps))
            for cliques in overlap_cliques.values():
                for clique in cliques:
                    # Check if all the edges in the clique are neutrals.
                    edges = list(combinations(clique, 2))
                    neutral_comparison = [cls._matrix_pair(*e) in neutral_edge_set for e in edges]
                    # If the edges in the clique are all neutral, eliminate
                    # the clique by removing the edges.
                    if all(neutral_comparison):
                        remove_edges.update({cls._matrix_pair(*e)
                                             for e in edges})
            graph.remove_edges_from(remove_edges)

            # If we had cliques that were build purely with neutral edges,
            # we want to skip this step and reexamine the cliques
            if not remove_edges:
                # Sort overlap edges by their weights. Remove the edge[s] with
                # the lowest weight.
                custom_compare = lambda x, y: compare(x[2], y[2])
                overlap_edges = sorted(graph.edges(overlaps, data="weight"),
                                       key=cmp_to_key(custom_compare))
                weight = overlap_edges[0][2]
                for node1, node2, w in overlap_edges:
                    if w > weight:
                        break
                    graph.remove_edge(node1, node2)

        # Filter out the groups of length 1. They will be added as loners
        # later. This prevents multi-addr
        return [group for group in groups if len(group) > 1]

    @classmethod
    @tracer.wrap()
    def group_by_addresses(cls, contacts, multi_collect=True):
        """Group building: Combine all addresses that have intersect values.
        We look at each of the following fields: street, city, state, and zip
         for any matching values. We also look for values that contradict.
        If an address has field matches and not contradictions, that is a valid
        pair.

        We recursively reprocess the groups by excluding contacts that span
        across multiple partitions and regenerate the groups.

        If a contact has multiple addresses, it may bridge two groups that
        have nothing else in common.
        """
        contact_map = {c.id: c for c in contacts}
        address_matrix = cls._build_address_matrix(contacts,
                                                   multi_collect=multi_collect)

        # If there is only one contact, we have one of two choices:
        # Either it has an address, or it doesn't.
        if len(contacts) == 1:
            return [frozenset(contacts)] if address_matrix else []
        # If we have multiple contacts, but only 1 contact has any addresses,
        # there is nothing else to do
        all_contacts_w_addr = {cid for cid, _ in address_matrix.keys()}
        if len(all_contacts_w_addr) == 1:
            return [frozenset((contact_map[all_contacts_w_addr.pop()],))]

        # Build the address partitions
        groups = cls._address_partitions(address_matrix)

        # At this point, the contact IDs are still being stored as a tuple.
        # (see _build_address_matrix for more info).
        # We can now convert them back to solo IDs.
        for i, group in enumerate(groups):
            groups[i] = {cid for cid, _ in group}

        # Check for any contacts with addresses not included in a
        # group. Groupless contacts need to be returned as loners.
        all_grouped_contacts = set(chain(*groups))
        loners = all_contacts_w_addr - all_grouped_contacts

        # Replace the Contact-IDs with Contacts in the groups
        groups = [frozenset(contact_map[cid] for cid in group)
                  for group in groups if group]
        groups.extend(frozenset((contact_map[loner],)) for loner in loners)
        return groups

    @classmethod
    @tracer.wrap()
    def _middle_name_split_sanity_check(cls, mname_groups_map, groups):
        """Sanity check to verify we haven't merged contacts with different
        middle names together
        """
        if len(mname_groups_map) > 1:
            intersections = {}
            for grp, (mname, mname_grp) in product(map(frozenset, groups),
                                                   list(mname_groups_map.items())):
                if mname and not grp.isdisjoint(mname_grp):
                    intersections.setdefault(grp, []).append(mname)
            intersecting_group_names = []
            for grp, middle_names in intersections.items():
                if len(middle_names) > 1:
                    logger.warn("Possible merge error: Contacts with different "
                                "middle names %s were merged together. Contact "
                                "ids: %s", middle_names,
                                [c.id for c in grp])
                    intersecting_group_names.append(middle_names)
            if intersecting_group_names:
                return intersecting_group_names

    @classmethod
    @tracer.wrap()
    def partition_by_addresses_and_middle_names(cls, contacts, multi_collect=True):
        """Partition contacts by addresses and middle names

        Uses the following assumptions to partition contacts that do not have
        addresses or middle names in common, but still tries to do the logical
        and sensible thing:

        - contacts with different suffixes will never be the same
        - contacts with the same email address are the same.
        - contacts with the same phone number are the same.
        - contacts with the same address are the same.
        - contacts with different middle names will never be the same
        - contacts with different middle initials will never be the same
        - given a contact with a middle name and another contact with a middle
          initial, if the middle name does not start with the same letter as
          the initial, they will never be the same.
        - contacts with the same middle name are the same (same initial doesn't
          count. a lack of a middle name doesn't count)

        Lots of caveats and edge cases. Read the code comments for full
        details.
        """
        # Keep track of special case contacts
        manual_contacts = {c for c in contacts if c.contact_type == 'manual'}
        first_initial_contacts = {c for c in contacts
                                  if 2 > len(c.first_name_lower)}
        black_sheep_contacts = manual_contacts.union(first_initial_contacts)
        # Create groups of contacts that have some piece of data in common
        email_groups = cls.group_by_email(contacts, multi_collect=multi_collect)
        phone_groups = cls.group_by_phone_numbers(contacts, multi_collect=multi_collect)
        address_groups = cls.group_by_addresses(contacts, multi_collect=multi_collect)
        # Partition contacts by middle names. This does not include contacts
        # with no middle name, or only an initial.
        middle_name_groups_map = cls.partition_by_middle_names(
            contacts, return_keys=True)
        middle_name_groups = list(middle_name_groups_map.values())

        all_contacts = set(contacts)
        partitionable_contacts = set()
        partitionable_contacts.update(*middle_name_groups)

        if len(address_groups) > 1:
            # If there is only one address group, then partitioning by
            # addresses is unnecessary.
            partitionable_contacts.update(*address_groups)

        groups = set()
        groups.update(email_groups, phone_groups, address_groups,
                      map(frozenset, middle_name_groups))

        # Look for contacts present in multiple groups. When found, merge the
        # groups together.
        reduced_groups, _ = union_find_reducer(groups)

        # We do not want to create a partition between contacts if they do not
        # have any email addresses or phone numbers in common, we only want to
        # use that information to create links. So if we find a group of
        # contacts that does not contain a contact that was present in any of
        # the address groups or middle name groups, discard it.
        final_groups = []
        dropped_first_initial_contacts = set()
        dropped_manual_contacts = set()
        black_sheep_keepers = set()
        black_sheep_merge_clues = []
        other_merge_clue_groups = []
        for group in reduced_groups:
            if not group.difference(first_initial_contacts):
                # If a group only contains first initial contacts, discard it
                # and keep track of which contacts those were.
                dropped_first_initial_contacts.update(group)
            elif not group.difference(manual_contacts):
                # If a group only contains manual contacts, discard it and keep
                # track of which contacts those were.
                dropped_manual_contacts.update(group)
            elif partitionable_contacts.intersection(group):
                final_groups.append(group)
            elif not black_sheep_contacts.isdisjoint(group):
                # If a group contains a manual or initial contact and something
                # else, keep track of it.
                black_sheep_merge_clues.append(group)
                other_merge_clue_groups.append(group)
                black_sheep_keepers.update(
                    black_sheep_contacts.intersection(group))
            else:
                other_merge_clue_groups.append(group)

        if len(final_groups) > 1 and len(address_groups) > 1:
            contact = list(contacts)[0]
            logger.info("Contacts with name '{} {}' may be split as a result "
                        "of splitting on strict address matching".format(
                ensure_unicode(contact.first_name),
                ensure_unicode(contact.last_name)))

        # sanity check! We should never see data that merges contacts with
        # different middle names together.
        cls._middle_name_split_sanity_check(middle_name_groups_map, final_groups)
        grouped_contacts = set()
        grouped_contacts.update(*final_groups)
        ungrouped_contacts = all_contacts.difference(grouped_contacts)
        maybe_drop_first_initials = first_initial_contacts.difference(
            black_sheep_keepers)
        if not ungrouped_contacts.isdisjoint(maybe_drop_first_initials):
            # If there are any first initial contacts that did not end up in a
            # partition, or find a match with an unpartitioned contact, drop
            # it.
            to_drop = ungrouped_contacts.intersection(maybe_drop_first_initials)
            ungrouped_contacts.difference_update(to_drop)
            dropped_first_initial_contacts.update(to_drop)

        maybe_drop_manuals = manual_contacts.difference(black_sheep_keepers)
        if not ungrouped_contacts.isdisjoint(maybe_drop_manuals):
            # If there are any manual contacts that did not end up in a
            # partition, or find a match with an unpartitioned contact, drop
            # it.
            to_drop = ungrouped_contacts.intersection(maybe_drop_manuals)
            ungrouped_contacts.difference_update(to_drop)
            dropped_manual_contacts.update(to_drop)

        # Partition the remaining contacts that do not have an address by
        # middle initials (because obviously John M. Smith is not the same
        # person as John S. Smith).
        remaining_groups_map = cls.partition_by_middle_names(
            ungrouped_contacts, keep_initials=True, return_keys=True)

        # If all the remaining contacts have no middle name, and there is only
        # one partition group left, merge the two together.
        if (len(remaining_groups_map) == 1 and len(final_groups) == 1 and
                '' in remaining_groups_map):
            return [final_groups[0].union(*list(remaining_groups_map.values()))]

        # Apply remaining merge clues to our "minor" partitions of middle
        # initials. For the most part this should be merging contacts with no
        # middle name into contacts with a middle initial based on things like
        # common email or phone number.
        middle_initial_groups = [
            grp for key, grp in remaining_groups_map.items() if key]
        new_remaining_groups, _ = union_find_reducer(
            middle_initial_groups + other_merge_clue_groups)

        # sanity check! We should never see data that merges contacts with
        # different middle initials together.
        mi_intersecting_groups = cls._middle_name_split_sanity_check(
            remaining_groups_map, new_remaining_groups)
        mi_intersection_map = {mi: grp
                               for grp in mi_intersecting_groups
                               for mi in grp} if mi_intersecting_groups else None

        # Find our middle initial groups again associate them with the correct
        # middle initial. We do this so that we can merge a middle initial
        # "partition" with a middle name partition if possible.
        new_remaining_groups_map = {}
        for group in new_remaining_groups:
            for key, orig_value in remaining_groups_map.items():
                if not key:
                    # Don't copy contacts with no middle name/initial over yet.
                    # We need to extract the contacts that were merged into our
                    # middle initial groups first.
                    continue
                elif group is orig_value or group.issuperset(orig_value):
                    # We use `issuperset()` here instead of just looking for an
                    # intersection to ensure that we get the group that
                    # contains at  least every contact that was in the original
                    # group, and prevent us from getting a fragment.
                    new_remaining_groups_map[key] = group
                    # Breaking early is a guard against accidentally putting
                    # the same contacts in multiple groups, and a performance
                    # enhancement. Only do this when it turns out middle
                    # initials have been merged together.
                    if mi_intersection_map and key in mi_intersection_map:
                        continue
                    else:
                        break

        # Split the blank middle-name contacts that were merged into our middle
        # initial contacts out of the blank group.
        if '' in remaining_groups_map:
            new_remaining_groups_map[''] = remaining_groups_map[''].difference(
                set().union(*list(new_remaining_groups_map.values())))

        # Merge middle initial groups into a middle name group if possible.
        middle_initials = [mi for mi in remaining_groups_map if mi]
        for middle_initial in middle_initials:
            if middle_initial not in new_remaining_groups_map:
                continue
            matches = []
            # Find middle names that begin with the same letter as our middle
            # initials.
            for middle_name in middle_name_groups_map:
                if middle_name.startswith(middle_initial):
                    matches.append(middle_name)
            # If only one middle name starts with the same letter as our
            # contacts with a middle initial, merge our middle initial contacts
            # into the partition for that middle name.
            if len(matches) == 1:
                middle_name = matches[0]
                partition_group = middle_name_groups_map[middle_name]
                for group in final_groups:
                    if partition_group.issubset(group):
                        group.update(
                            new_remaining_groups_map.pop(middle_initial))
                        if (mi_intersection_map and
                                middle_initial in mi_intersection_map):
                            # If the middle initial is present in the
                            # mi_intersection_map, then it was merged with
                            # another initial. To prevent middle initials from
                            # overriding middle name partitioning or causing
                            # contacts to end up in multiple groups, remove the
                            # entries for the other initials from
                            # `new_remaining_groups_map`.
                            other_keys = mi_intersection_map[middle_initial]
                            for key in other_keys:
                                try:
                                    del new_remaining_groups_map[key]
                                except KeyError:
                                    continue
                        break

        result_groups = final_groups + list(set(map(
            frozenset, new_remaining_groups_map.values())))
        result_groups = [grp for grp in result_groups if grp]

        # This code is super complicated, so do a sanity check to make sure we
        # didn't lose a contact, or include the same contact in multiple
        # partitions.
        sanity_check_count = sum(len(x) for x in result_groups)
        sanity_check_count += len(dropped_first_initial_contacts)
        sanity_check_count += len(dropped_manual_contacts)
        total_contact_count = len(contacts)
        msg = "sum is {} and total size is {}".format(
            sanity_check_count, total_contact_count)
        assert sanity_check_count == total_contact_count, msg
        assert cls.unique_sanity_check(result_groups), (
            "A duplicate was found: {}".format(result_groups))
        return result_groups

    @classmethod
    @tracer.wrap()
    def partition_by_middle_names(cls, contacts, keep_initials=False,
                                  return_keys=False):
        """Partition contacts by middle name

        Meant to be used by `partition_by_addresses_and_middle_names`.

        When keep_initials is False (the default), contacts with no middle name
        and contacts with only a middle initial are filtered out of the
        results.

        When keep_initials is True, contacts with no middle name or only a
        middle initial are included in the results. Additionally, if we detect
        a situation where we end up with two groups, one having no middle name
        and the other only having a middle initial, we merge those groups
        together.

        The purpose of the keep_initials behavior is to create groups of
        contacts around facts we can be sure and certain of. To give an
        example, if we have 2 contacts with the name "John Michael Smith", we
        can be sure that they the same person. The problem is with contacts
        with middle initials and contacts with no middle name.

        The problem with middle initials is the scenario where we have a
        contact with the middle initial "G", a contact with the middle name
        "Gary", and a contact with the middle name of "George". With just the
        information of the middle initial "G", we cannot determine which full
        middle name "G" represents.

        Just because a contact doesn't have the middle name field filled out
        doesn't mean the person the contact represents doesn't have one, which
        leads us to the exact same problem as middle initials: which full
        middle name does the contact go with?

        These issues are addressed by cross-referencing contacts with other
        pieces of information in the `partition_by_addresses_and_middle_names`
        method.
        """
        groups = {}
        for contact in contacts:
            scrubbed = scrub_and_lowercase(contact.additional_name)
            if len(scrubbed) > 1 or keep_initials:
                groups.setdefault(scrubbed, set()).add(contact)
        if len(groups) > 1:
            if keep_initials and sum(map(len, list(groups.keys()))) <= 1:
                if return_keys:
                    key = list(filter(None, groups.keys()))[0]
                    result = {key: contacts}
                else:
                    result = [contacts]
                return result
            logger.info("Contacts with name '{} {}' split as a result of "
                        "middle name partitioning. middle names: {}".format(
                ensure_unicode(contact.first_name),
                ensure_unicode(contact.last_name),
                ensure_unicode(str(list(groups.keys())))))
        if return_keys:
            result = groups
        else:
            result = list(groups.values())
        return result

    @classmethod
    @tracer.wrap()
    def partition_by_generational_suffixes(cls, contacts):
        """Partition contacts by generational suffixes

        Groups contacts with the same generational suffix.
        """
        generational_groups = {}
        for contact in contacts:
            match = False
            for exemplar_suffix in generational_groups.keys():
                same_suffix = Contact.compare_generational_suffixes(
                    exemplar_suffix, contact.name_suffix)
                if same_suffix:
                    match = True
                    generational_groups[exemplar_suffix].add(contact)
                    break
            if not match:
                generational_groups[contact.name_suffix] = set([contact])
        if len(generational_groups) > 1:
            if len(generational_groups) == 2:
                blank_found = False
                for exemplar_suffix in generational_groups.keys():
                    same_suffix = Contact.compare_generational_suffixes(
                        exemplar_suffix, '')
                    if same_suffix:
                        blank_found = True
                if blank_found:
                    # If there are only two generational "groups" found, and
                    # one of the name suffixes has no generational suffix
                    # present, then don't bother with the partition at all.
                    return [contacts]

            logger.info("Contacts with name '{} {}' split as a result of "
                        "generational suffixes".format(
                ensure_unicode(contact.first_name),
                ensure_unicode(contact.last_name)))
        return list(generational_groups.values())

    @classmethod
    @tracer.wrap()
    def split_any_merged_only_groups(cls, groups):
        """Split any contact groups containing only merged contacts

        It is possible to end up with a contact group containing only merged
        contacts. We do not want to create any groups like this as it prevent
        the merged contacts from properly being flagged as "needs attention".
        """
        for group in groups:
            if all(c.contact_type == 'merged' for c in group):
                for contact in group:
                    yield {contact}
            else:
                yield group

    @classmethod
    @tracer.wrap()
    def process_contacts(cls, contacts, multi_collect=True):
        """Apply partitioning rules to contacts

        This method does not look at first or last names, and expects the input
        contacts to already have the same name.

        Partitions contacts by generational suffix, and then further partitions
        each group by address and middle name.

        Then attempt to reconnect split groups as the partitioning rules are
        extremely strict in their rules. Associate merge parents with their
        children if possible, and apply any force-merge rules.
        """
        contact_id_map = {c.id: c for c in contacts}

        def ids_to_contacts(group):
            return {contact_id_map[c_id]
                    for c_id in group if c_id in contact_id_map}

        flat_groups = []
        # Partition by generational suffix
        cgs = cls.partition_by_generational_suffixes(contacts)
        for generational_group in cgs:
            # Partition each generational group by address and middle name
            groups = cls.partition_by_addresses_and_middle_names(
                generational_group, multi_collect=multi_collect)
            # Move merge parent back with children if possible
            for group in partitioning_family_therapy(groups):
                # Query ContactLink to find any force merge information for the
                # contacts in this group. We turn all contacts into ids so we
                # don't have to fetch the entire contact from the db multiple
                # times, and potentially lose the previous relation data.
                links = set(ContactLink.objects.filter(
                    dimension__contact_links__contact__in=group).distinct(
                        'contact_id').values_list('contact_id', flat=True))
                ids = {c.id for c in group}.union(links)
                flat_groups.append(ids)

        # Merge groups with contacts in common together
        reduced_groups, _ = union_find_reducer(flat_groups)
        # Turn contact ids back into contacts
        reduced_groups = [ids_to_contacts(group) for group in reduced_groups]
        # Try to reassociate merge parents with childen one more time
        reduced_groups = partitioning_family_therapy(reduced_groups)
        reduced_groups = cls.split_any_merged_only_groups(reduced_groups)
        return reduced_groups


@tracer.wrap()
def partitioning_family_therapy(groups):
    '''Reassociate merge parents with their children

    The contact partitioner can cause a merge parent to be split from its
    children when two or more groups are detected within a set of contacts.
    This occurs because merged parents have no data themselves, so they don't
    get put into the same group as their children. As a result, this means that
    a new merge parent is created every time, even when no new splits are made.

    This function fixes the issue by checking partitioned groups for references
    to a merge parent. If a merge parent is only referred to by a single group,
    and the merge parent is present in one of the groups we receive, move the
    merge parent to be with its children.
    '''
    if len(groups) < 2:
        return groups
    new_groups = list(map(set, groups))
    # map of parent_id to index of groups that list it as a parent.
    merged_contact_location = {}
    parent_ref_map = {}
    for idx, group in enumerate(groups):
        for contact in group:
            if contact.parent_id:
                parent_ref_map.setdefault(contact.parent_id, set()).add(idx)
            if contact.contact_type == 'merged':
                merged_contact_location[contact.id] = (contact, idx)

    # find parent_ids that are only in 1 group
    for parent_id, idxes in parent_ref_map.items():
        if len(idxes) == 1 and parent_id in merged_contact_location:
            contact, group_idx = merged_contact_location[parent_id]
            idx = idxes.pop()
            if idx != group_idx:
                new_groups[group_idx].remove(contact)
                new_groups[idx].add(contact)
    return list(filter(None, new_groups))


@tracer.wrap()
def _process_contact_group(group, processed_pks, index_filters, dryrun=False):
    """Final processing for a partitioned group of contacts

    - Determines parent contact of partition
    - Persists changes (if any)

    Returns list of contact primary keys that were processed. If a parent for a
    given group could not be determined, those primary keys will not be
    returned.

    This function is meant to be used by the `process_contacts()` method.
    """
    parent, children = split_parent_children(group)
    if parent is None:
        logger.warn(
            "Unable to detect a parent for contacts: %s",
            ensure_unicode(str(children)))
        return processed_pks
    logger.debug("Detected parent is %s", parent)
    if dryrun:
        _dryrun_set_parent_children(parent, children)
    else:
        set_parent_children(parent, children,
                            index_filters=index_filters)
    processed_pks.append(parent.pk)
    if children:
        processed_pks.extend(c.pk for c in children)
    return processed_pks


@tracer.wrap()
def process_contact_ids(pks, user, dryrun=False, index_filters=None,
                        multi_collect=True):
    """Process grouped contact primary keys from the _groupify function.

    Converts group of primary keys into contact records and then passed
    contacts to the process_contacts function which does final partitioning and
    persistence.
    """
    contacts = list(Contact.objects.filter(pk__in=pks, user=user))
    return process_contacts(contacts, dryrun=dryrun,
                            index_filters=index_filters,
                            multi_collect=multi_collect)


@tracer.wrap()
def _record_previous_relations(contacts):
    """Goes through contacts and records relations on to parent

    We add the property `previous_children` to merge contacts that lists all
    children we detected.

    This is meant to be used in `process_contacts()`, and is run before any
    partitioning rules are processed. If the children of a merge parent are
    split off, we need to know what children were previously associated with
    the parent so we can tell the user about the split, and allow them to
    reassociate any user-data with the correct child.
    """
    merged_contacts_map = {c.id: c for c in contacts
                           if c.contact_type == 'merged'}
    preprocess_relation_map = {}
    for contact in contacts:
        if contact.parent_id:
            preprocess_relation_map.setdefault(
                contact.parent_id, []).append(contact.id)
    for p_id, child_ids in preprocess_relation_map.items():
        if p_id in merged_contacts_map:
            contact = merged_contacts_map[p_id]
            contact.previous_children = child_ids
    return contacts


@tracer.wrap()
def process_contacts(contacts, dryrun=False, index_filters=None,
                     multi_collect=True):
    """Process contacts for final disposition

    - Partitions contacts based on generational suffixes
    - Further partitions contacts based on middle name and address
    - Reassociate merge parent with children if possible
    - Apply force-merge rules.
    - Determines parent contact of each partition
    - Persists changes (if any)

    Returns list of contact primary keys that were processed. If a parent for a
    given group could not be determined, those primary keys will not be
    returned.
    """
    if len(contacts) == 1:
        logger.debug("Only got a single contact. short circuiting")
        if dryrun:
            _dryrun_set_parent_children(contacts[0])
        else:
            set_parent_children(contacts[0], index_filters=index_filters)
        return [contacts[0].pk]

    _record_previous_relations(contacts)

    processed_pks = []
    groups = ContactPartitioner.process_contacts(contacts,
                                                 multi_collect=multi_collect)

    for group in groups:
        _process_contact_group(group, processed_pks, index_filters,
                               dryrun=dryrun)
    return processed_pks


def _dryrun_set_parent_children(parent, children=None):
    if parent.contact_type == 'merged' and not children:
        logger.info("Got a merged parent contact %s with no children. "
                    "Disabling contact. Ids of previous children: %s",
                    parent, getattr(parent, 'previous_children', []))
    elif parent.contact_type != 'merged' and children:
        logger.info("Parent %s is not a merged contact, and there are "
                    "children. Creating a merged contact", parent)
    elif not parent.is_primary or parent.parent_id is not None:
        if children is None:
            if parent.parent_id is not None:
                if parent.parent_id != parent.id:
                    logger.info(
                        "Contact %s is now solo. Parent previously was: %s",
                        parent, parent.parent)
                else:
                    logger.info("Contact %s had self set as parent, unsetting",
                                parent)

            elif not parent.is_primary:
                logger.info("Contact %s was not set as primary", parent)
        else:
            logger.info("Promoting contact %s to parent, or fixing the "
                        "is_primary or parent field", parent)

    if children:
        children_to_update = [c for c in children
                              if c.is_primary or c.parent_id != parent.id]
        if children_to_update:
            logger.info("Setting parent on children %s to %s",
                        ensure_unicode(str(children_to_update)), parent)


@tracer.wrap()
def move_contactnotes(parent, children):
    """Move/merge any contact notes from children to the parent

    Concatenate any contact notes from children onto the parent contact note.
    The order that the notes are concatenated is based on the note creation
    order.

    Dedupe any notes.

    Once notes are merged, delete all child notes.
    """
    child_notes = ContactNotes.objects.filter(
        # 'children' may be Contacts or ContactWrappers, so we must use pk
        contact__in=(c.id for c in children)).order_by('account', 'created')

    parent_notes = {n.account_id: n for n in parent.notes.all()}
    for acct_id, notes in groupby(child_notes, attrgetter('account_id')):
        notes = list(notes)
        note1 = []
        note2 = []
        for note in notes:
            # Add notes to collection provided note is non-empty, and not
            # already present in the collection.
            if note.notes1 and note.notes1 not in note1:
                note1.append(note.notes1)
            if note.notes2 and note.notes2 not in note2:
                note2.append(note.notes2)
        if note1 or note2:
            parent_note = parent_notes.get(acct_id)
            if parent_note is None:
                ContactNotes.objects.create(
                    account_id=acct_id,
                    contact=parent,
                    notes1='\n\n'.join(note1),
                    notes2='\n\n'.join(note2))
            else:
                # If the parent contact note already exists, make sure that
                # none of the children notes have the same contents.
                if parent_note.notes1 in note1:
                    note1.pop(note1.index(parent_note.notes1))
                if parent_note.notes2 in note2:
                    note2.pop(note2.index(parent_note.notes2))
                if note1:
                    if parent_note.notes1:
                        parent_note.notes1 += '\n\n'
                    parent_note.notes1 += '\n\n'.join(note1)
                if note2:
                    if parent_note.notes2:
                        parent_note.notes2 += '\n\n'
                    parent_note.notes2 += '\n\n'.join(note2)
                parent_note.save(update_fields=['notes1', 'notes2'])

    child_notes.delete()


@tracer.wrap()
def move_prospectcollisions(parent, children):
    """Handle any prospect collisions on children

    Move any prospect collisions on children to parent, if any exist on the
    children, and the parent does not have a collision for the same event.

    We only move one prospect collision per event because (contact, event) is
    unique.

    Delete any child prospect collision that we do not move.

    **NOTE** This should only be run AFTER move_prospect!
    """
    # XXX: This should be run AFTER move_prospects!
    child_collisions = ProspectCollision.objects.filter(
        # 'children' may be Contacts or ContactWrappers, so we must use pk
        contact__in=(c.id for c in children)).order_by('event', 'id')

    parent_collisions = set(parent.prospectcollision_set.values_list(
        'event', flat=True))
    to_delete = []
    set_to_parent = []
    for event_id, g in groupby(child_collisions, attrgetter('event_id')):
        g = list(g)
        if event_id not in parent_collisions:
            set_to_parent.append(g.pop(0))
        to_delete.extend(g)

    if to_delete:
        ProspectCollision.objects.filter(
            pk__in=[p.pk for p in to_delete]).delete()

    if set_to_parent:
        ProspectCollision.objects.filter(
            pk__in=[p.pk for p in set_to_parent]).update(contact=parent)


@tracer.wrap()
def move_prospects(parent, children):
    """Handle any prospect records on children

    ** ASSUMPTIONS **
    - All contacts belong to the same user.
    - Parent was selected by using the split_parent_children function, and
      overlap checking was done there.

    Moves prospect records from children to parent if the parent has no
    prospect record for an event, or the parent prospect record is empty.

    If the parent prospect record does not exist, and there are non-empty
    prospect records on the children, move one to the parent. If the are only
    empty prospect records on the children, choose one to move to the parent.

    If the parent prospect record exists but is empty, and there is a non-empty
    prospect record on children, move one to the parent. If there are no
    non-empty prospect records on the children, move nothing.

    Deletes the remaining prospect records from the children.

    Updates any prospect collision records that reference prospect records we
    are about to delete to reference the new (or existing) parent prospect.
    """
    child_prospects = Prospect.objects.filter(
        # 'children' may be Contacts or ContactWrappers, so we must use pk
        contact__in=(c.id for c in children)).order_by('event', 'id')

    parent_prospects = {p.event_id: p for p in parent.prospect_set.all()}
    to_delete = []
    set_to_parent = []
    for event_id, g in groupby(child_prospects, attrgetter('event_id')):
        g = list(g)
        parent_prospect = parent_prospects.get(event_id)
        if parent_prospect is None or parent_prospect.is_empty():
            # If the parent does not have a Prospect for this event, or
            # the parent's Prospect is empty, look through the children
            # event prospect records for one with contents to give to
            # the parent.
            new_prospect = None
            for prospect in g:
                if not prospect.is_empty():
                    new_prospect = prospect
                    break
            if new_prospect:
                g.pop(g.index(new_prospect))
                if parent_prospect is not None:
                    # Queue the "empty" parent prospect for deletion.
                    to_delete.append(parent_prospect)
            elif parent_prospect is None:
                # If we cannot find one with contents to give to the
                # parent, then give one of the blank ones to the parent
                # provided the parent doesn't already have a blank one.
                new_prospect = g.pop(0)

            if new_prospect:
                parent_prospects[event_id] = new_prospect
                set_to_parent.append(new_prospect)
        to_delete.extend(g)

    if to_delete:
        for event_id, g in groupby(to_delete, attrgetter('event_id')):
            # Before we can delete any Prospect records, we need to
            # handle any ProspectCollision records that reference them.
            # Update the external_prospect field to point to the new
            # parent.
            g = list(g)
            ProspectCollision.objects.filter(
                external_prospect_id__in=[p.id for p in g]).update(
                    external_prospect=parent_prospects[event_id])
        Prospect.objects.filter(
            pk__in=[p.pk for p in to_delete]).delete()

    if set_to_parent:
        Prospect.objects.filter(
            pk__in=[p.pk for p in set_to_parent]).update(contact=parent)


@tracer.wrap()
def move_calltimecontacts(parent, children):
    """Handle any call-time contacts on children

    ** ASSUMPTIONS **
    - All contacts belong to the same user.
    - Parent was selected by using the split_parent_children function, and
     overlap checking was done there.

    Creates a new CallTimeContact (CTC) for the parent if it does not have one.
    Copies any CallLogs from the children CTCs into the parent CTC. Copied
    CallLogs will have a reference back to the CallLog they were created from.

    We preserve the children's CTC by marking them as "merged", just in case
    we need to unmerge, all of the pre-merge data exists.
    """
    # Intentionally leaving `active` out of this query
    child_ctcs_q = CallTimeContact.objects.filter(
        # 'children' may be Contacts or ContactWrappers, so we must use pk
        contact__in=(c.id for c in children), merged=None).order_by("id")
    child_ctcs = list(child_ctcs_q)
    if not child_ctcs:
        # Nothing to do
        return

    account_id = child_ctcs[0].account_id
    parent_ctc_q = list(CallTimeContact.objects.filter(contact=parent))
    if len(parent_ctc_q) > 1:
        # This shouldn't happen, but if it did somehow, we'll just clean it up
        parent_ctc = parent_ctc_q[0]
        child_ctcs.extend(parent_ctc_q[1:])
    else:
        parent_ctc = parent_ctc_q[0] if parent_ctc_q else None

    # If we don't have a parent call-time contact, we need one.
    if not parent_ctc:
        parent_ctc = CallTimeContact.objects.create(
            account_id=account_id, contact=parent)

    # Get all of the parent's logs, so we don't duplicate any
    parent_logs = set(parent_ctc.call_logs.values_list("copied_from",
                                                       flat=True))
    parent_ctc_changed = False
    copied_logs = []
    call_groups = {}
    for child_ctc in child_ctcs:
        # Copy all of the children's call logs.
        for l in child_ctc.call_logs.all():
            if l.id in parent_logs:
                # If this log has already been copied into the parent, skip it
                continue
            l.copied_from = l
            l.id = None
            l.pk = None
            l.ct_contact = parent_ctc
            copied_logs.append(l)

        # Copy the primary attributes, if none are set. If multiple children
        # have a primary set, this will select it arbitrarily based on order
        if not parent_ctc.primary_phone_id:
            parent_ctc.primary_phone_id = child_ctc.primary_phone_id
            parent_ctc_changed = True
        if not parent_ctc.primary_email_id:
            parent_ctc.primary_email_id = child_ctc.primary_email_id
            parent_ctc_changed = True

        # Search for any call groups with this child in it.
        for cg in CallTimeContactSet.objects.filter(contacts=child_ctc):
            # If we already have this callgroup queried, we want to reuse
            # that same object, so we don't overwrite
            cg = call_groups.setdefault(cg.id, cg)

            # Replace any reference to the child_ctc with the parent
            for i, c_id in enumerate(cg.contact_ordering):
                if c_id == child_ctc.id:
                    cg.contact_ordering[i] = parent_ctc.id

        # Delete the child from those call groups
        CallTimeContactSet.contacts.through.objects.filter(
            calltimecontact=child_ctc).delete()

    # Add the parent ct-contact to all of the call groups previously occupied
    # by the children. Don't add the parent to a call group it's already in.
    call_group_contacts = []
    for call_group in CallTimeContactSet.objects.filter(
                id__in=(cg.id for cg in call_groups.values())).exclude(
                contacts=parent_ctc):
        call_group_contacts.append(CallTimeContactSet.contacts.through(
            calltimecontactset=call_group,
            calltimecontact=parent_ctc))

    if call_group_contacts:
        # Using bulk_create so we can add the parent ct-contact to many
        # call groups at once.
        CallTimeContactSet.contacts.through.objects.bulk_create(
            call_group_contacts)

    # Clean the ordering (to remove possible duplicates) and save each
    for call_group in call_groups.values():
        call_group.clean_ordering()
        call_group.save(update_fields=["contact_ordering"])

    # Generate all of the call log copies
    CallLog.objects.bulk_create(copied_logs)
    child_ctcs_q.update(merged=datetime.now())
    if parent_ctc_changed:
        parent_ctc.save()


@tracer.wrap()
def _parent_update_and_deconflict(parent, is_primary=True, **update_kwargs):
    """There are two possibly values for updated_count: 0 and 1. If
    updated_count is 1, that means that the contact was updated as expected
    and everything is just fine.

    If the value of updated_count on the other hand is 0, that means that we
    could not find the contact we meant to update. Since we are searching for
    the contact by its primary key and merge version, that means that either
    the contact was deleted, or the merge_version doesn't match. If the merge
    version counter doesn't match, that means there is a merge conflict.

    """
    parent.is_primary = is_primary
    parent.parent_id = None
    updated_count = Contact.objects.filter(
        pk=parent.pk, merge_version=parent.merge_version).update(
            is_primary=is_primary, parent_id=None,
            merge_version=parent.merge_version + 1, **update_kwargs)
    if updated_count != 1:
        raise MergeConflict

    parent.merge_version += 1
    return parent


@tracer.wrap()
def _release_analyzed_limit(user, *contacts):
    """Helper function that reduces the 'analyzed_count' in the contact
    LimitTracker. This should be used when a contact is changing state that
    would affect its analysis Results. For example, deleting the Result, or
    a parent becoming a child.
    """
    from frontend.apps.analysis.models import Result
    if not contacts:
        return

    # Get the counts of unique rankings for each contact in all of their
    # analyzed accounts
    result_counts = dict(ranking_counts_by_account(c.id for c in contacts))
    if not result_counts:
        return

    # Reduce the tracker counts for each account
    trackers = LimitTracker.get_trackers(
        user, *Account.objects.filter(id__in=result_counts.keys()),
        select_for_update=True)
    for tracker in trackers:
        count = result_counts.get(tracker.account_id, 0)
        if count <= 0:
            continue

        tracker.analyzed_count -= count
        if tracker.analyzed_count < 0:
            # This shouldn't happen, but let's protect against it anyways
            tracker.analyzed_count = 0
        tracker.save()

    # Clean out the Result objects so we don't double-count in future merges
    Result.objects.filter(contact__in=contacts).delete()


@tracer.wrap()
@transaction.atomic
def set_parent_children(parent, children=None, index_filters=None):
    """Persists changes

    This saves any necessary changes. It also corrects any fields that are
    wrong (ex: children with is_primary, or parents that refer to themselves in
    the parent field).

    Once the contacts are updated, if there are any children handle any
    ContactNotes, Prospect, and ProspectCollisions found on the children as
    well.
    """
    if index_filters is None:
        index_filters = parent.user.get_index_filters()
    orig_parent = parent
    if parent.contact_type == 'merged' and not children:
        update_kwargs = {}
        previous_children = getattr(parent, 'previous_children', [])
        if previous_children:
            update_kwargs.update({
                'needs_attention': True,
                'attention_payload': {'demerge': previous_children}
            })
        manual_children = list(Contact.objects.filter(
            parent=parent, contact_type='manual').values_list('id', flat=True))
        if manual_children:
            # XXX: Remove this once we start handling manual contacts during
            # merging.
            if 'needs_attention' not in update_kwargs:
                update_kwargs.update({
                    'needs_attention': True,
                    'attention_payload': {'demerge': manual_children}
                })
            else:
                update_kwargs['attention_payload']['demerge'].extend(
                    manual_children)
            # Do not disable the merged contact since it does still have
            # children, despite the fact that we're not considering them during
            # merging.
            parent = _parent_update_and_deconflict(parent, **update_kwargs)
            # Re-index parent contact since the parent has obviously lost some
            # of its children.
            ContactIndex.update_index(index_filters, user_id=parent.user_id,
                                      contact=parent)
            return

        cd = ContactDeleter(parent.user, delete_children=False)
        if not cd.has_relevant_data(parent):
            logger.info("Got a merged parent contact %s with no children, "
                        "and no user data. Deleting contact.", parent)
            _release_analyzed_limit(parent.user, parent)
            cd.delete(parent)
            return
        logger.info("Got a merged parent contact %s with no children. "
                    "Disabling contact.", parent)
        parent = _parent_update_and_deconflict(parent, is_primary=False,
                                               **update_kwargs)
        # Delete the index for the contact since we're disabling it.
        ContactIndex.objects.filter(contact=parent).delete()
        return
    elif parent.contact_type != 'merged' and children:
        logger.info("Parent %s is not a merged contact, and there are "
                    "children. Creating a merged contact", parent)
        children.append(orig_parent)
        parent = Contact(
            user_id=orig_parent.user_id,
            first_name=orig_parent.first_name,
            last_name=orig_parent.last_name,
            additional_name=orig_parent.additional_name,
            first_name_lower=orig_parent.first_name_lower,
            last_name_lower=orig_parent.last_name_lower,
            name_prefix=orig_parent.name_prefix,
            name_suffix=orig_parent.name_suffix,
            contact_type="merged",
            is_primary=True)
        parent.save()
        _release_analyzed_limit(parent.user_id, *children)
    else:
        if not parent.is_primary or parent.parent_id is not None:
            if children is None:
                logger.info("Contact %s is now solo", parent)
            else:
                logger.info("Promoting contact %s to parent", parent)
        parent = _parent_update_and_deconflict(parent)

    if children:
        children_to_update = [c for c in children
                              if c.is_primary or c.parent_id != parent.id]
        if children_to_update:
            logger.info("Setting parent on children %s to %s",
                        ensure_unicode(str(children_to_update)), parent)

            to_update = [Q(pk=c.pk, merge_version=c.merge_version)
                         for c in children_to_update]
            to_update_qry = reduce(lambda a, b: a | b, to_update)

            expected_count = len(children_to_update)
            updated_count = Contact.objects.filter(to_update_qry).update(
                is_primary=False, parent=parent,
                merge_version=F('merge_version') + 1)
            if updated_count != expected_count:
                msg = ("Number of children updated ({}) does not equal "
                       "expected count ({})!".format(
                           updated_count, expected_count))
                raise MergeConflict(msg)
            else:
                # Update merge version on updated contacts to match value in
                # database. We do this just in case the same contact gets put
                # through contact merging again without being queried for first
                # (primarily this is for unit tests).
                for child in children_to_update:
                    child.merge_version += 1

        # Get the oldest child and copy its timestamp. Ignore child merged
        try:
            oldest = list(sorted(filter(None,
                                        (c.created for c in children
                                         if c.contact_type != "merged"))))[0]
        except IndexError:
            # This shouldn't happen, but let's cover this just in case.
            oldest = datetime.now()
        parent.created = oldest
        parent.save()

        # Migrate relevant data from the children to the parent
        move_prospects(parent, children)
        move_prospectcollisions(parent, children)
        move_contactnotes(parent, children)
        move_calltimecontacts(parent, children)
        ContactIndex.objects.filter(
            contact__in=[c.id for c in children]).delete()

    ContactIndex.update_index(index_filters, user_id=parent.user_id,
                              contact=parent)


@tracer.wrap()
def _resolve_parent_prospect_strategy(contacts):
    """Strategy for resolving a parent contact from a group of contacts based
    on prospect records.

    If we cannot detect a parent based on prospect records, raise
    NoParentFound. If a parent cannot be chosen based on conflicting data,
    return None to indicate nothing should be done.
    """
    contact_map = {c.pk: c for c in contacts}
    has_prospects = set()
    for contact in contacts:
        if contact.prospect_set.exists():
            has_prospects.add(contact)
    if has_prospects:
        if len(has_prospects) == 1:
            logger.info(
                "Parent detected because it is the only one with prospects")
            return has_prospects.pop()
        else:
            filled_prospects = set()
            for contact in has_prospects:
                if contact.prospect_set.exclude(soft_amt=0, hard_amt=0,
                                                in_amt=0).exists():
                    filled_prospects.add(contact)
            if filled_prospects:
                if len(filled_prospects) == 1:
                    logger.info("Parent detected because it is the only one "
                                "with a prospect that has contents")
                    return filled_prospects.pop()

            pks = [c.pk for c in has_prospects]
            prospects = list(Prospect.objects.filter(
                contact_id__in=pks).order_by('event'))
            overlap = False
            # Group prospects by event and look for event overlaps where the
            # prospect contents differ.
            for _, g in groupby(prospects, attrgetter('event_id')):
                g = list(g)
                if len(g) > 1:
                    content_groups = set()
                    for p in g:
                        content_groups.add(p.get_amt_values())
                    # If the contents of each prospect are the same or empty,
                    # we don't count this as an overlap. Any other condition is
                    # an overlap.
                    if not (len(content_groups) == 1 or
                            (len(content_groups) == 2 and
                             (0, 0, 0) in content_groups)):
                        overlap = True
                        break

            if not overlap:
                # If there is no overlap, the contact with the most
                # recently updated prospect wins.
                prospects = sorted(prospects, key=attrgetter('updated'),
                                   reverse=True)
                winner = contact_map[prospects[0].contact_id]
                logger.info("Parent detected because of all the candidates "
                            "that have prospects, there were no overlaps and "
                            "it had the most recently updated prospect")
                return winner
            else:
                logger.info("No parent could be resolved!")
                return None
    raise NoParentFound


@tracer.wrap()
def _resolve_parent_notes_strategy(contacts):
    """Strategy for selecting a parent from a group based on ContactNote

    If we cannot detect a parent based on this strategy, raise
    NoParentFound.
    """
    # Contact that has notes wins
    has_notes = set()
    populated_notes = set()
    for contact in contacts:
        if contact.notes.exists():
            has_notes.add(contact)
            if contact.notes.filter(Q(notes1__ne='') |
                                    Q(notes2__ne='')).exists():
                populated_notes.add(contact)

    if has_notes:
        if len(has_notes) == 1:
            logger.info(
                "Parent detected because it was the only one with a note")
            return has_notes.pop()
        elif len(populated_notes) == 1:
            logger.info("Parent detected because it was the only one with a "
                        "non-empty note")
            return populated_notes.pop()
    raise NoParentFound


@tracer.wrap()
def _resolve_parent_score_strategy(contacts):
    """Strategy for selecting a parent based on analysis score

    If we cannot detect a parent based on this strategy, raise
    NoParentFound.
    """
    contact_map = {c.pk: c for c in contacts}
    # Highest analysis score
    # Get the most recent analysis
    analysis = list(contacts)[0].user.analysis_set.order_by(
        '-modified').first()
    if analysis is not None:
        partition_set = analysis.partition_set
        if partition_set is None:
            # legacy analysis do not have a partition_set associated with them.
            # If a partition_set is not found, abort this strategy and go on to
            # the next.
            raise NoParentFound
        # Get the partition associated with the analysis that corresponds to
        # the largest time frame.
        part_id = partition_set.partition_configs.order_by(
            '-index').values_list('id', flat=True)[0]
        # Get the contact_id and score for the top two results.
        results = list(analysis.results.filter(
            partition_id=part_id, contact_id__in=list(contact_map.keys()))
            .order_by('-score')[:2].values('contact', 'score'))
        if results:
            if len(results) == 2:
                first, second = results
            else:
                first = results[0]
                second = None
            # If the score of the first and second place results are the same,
            # do not use analysis score to determine parent.
            if second is None or first['score'] != second['score']:
                logger.info("Parent detected because it had the highest "
                            "analysis score")
                return contact_map[first['contact']]
    raise NoParentFound


@tracer.wrap()
def _resolve_parent_children_count_strategy(contacts):
    """Strategy for selecting parent based on the number of child contacts

    If we cannot detect a parent based on this strategy, raise
    NoParentFound.
    """
    contact_map = {c.pk: c for c in contacts}
    # Most child/sub contacts
    if Contact.objects.filter(pk__in=list(contact_map.keys()),
                              contacts__isnull=False).exists():
        # Annotate each contact with the number of child contacts they contain,
        # then sort contacts by that count and get the top 2 contacts with the
        # most sub-contacts.
        results = list(Contact.objects.filter(
            pk__in=list(contact_map.keys())).annotate(
                child_count=Count('contacts')).order_by(
                    '-child_count')[:2].values('pk', 'child_count'))

        if len(results) == 2:
            first_child, second_child = results
        else:
            first_child = results[0]
            second_child = None

        # If the contacts from the above query happen to have the same number
        # of children, we cannot use number of children to determine a winner.
        if (second_child is None or
                first_child['child_count'] != second_child['child_count']):
            logger.info("Parent detected because it had the most children")
            return contact_map[first_child['pk']]
    raise NoParentFound


@tracer.wrap()
def _resolve_parent_oldest_strategy(contacts):
    """Strategy for selecting parent based on creation order
    """
    # Creation order
    logger.info("Parent detected because it was the oldest candidate")
    return sorted(contacts, key=attrgetter('id'))[0]


@tracer.wrap()
def resolve_parent(contacts):
    """Resolve parent from group of contacts

    Returns parent contact, or None to indicate a parent cannot be determined
    and user intervention is required.
    """
    strategies = (
        _resolve_parent_prospect_strategy,
        _resolve_parent_notes_strategy,
        _resolve_parent_score_strategy,
        _resolve_parent_children_count_strategy,
        _resolve_parent_oldest_strategy
    )
    for strategy in strategies:
        try:
            return strategy(contacts)
        except NoParentFound:
            continue


@tracer.wrap()
def split_parent_children(contacts):
    """Split parent away from children contacts

    Returns: tuple of (parent, children)
    If no parent can be determined, and user intervention is required parent
    will be None.
    If contacts only contains one contact, children will be None.

    Contacts with only a first initial are not allowed to be chosen as a parent
    contact. We want merge parents to always have a full name for two reasons:
    1. To prevent a scenario where we have two top-level contacts with the same
    first initial and last name, but represent two different people. ex: a `P
    Smith` that represents `Patrick Smith` and a `P Smith` that represents
    `Peter Smith`.

    2. To prevent merge parent thrashing. Since merge parents have no data, and
    we only merge a first initial contact with another contact when they have
    data in common with another contact, the next time merging was run on that
    contact a new parent would be created.
    """
    merged_type = set()
    rest_type = set()
    invalid_type = set()
    for contact in contacts:
        if len(contact.first_name_lower) < 2:
            invalid_type.add(contact)
        elif contact.contact_type == 'merged':
            merged_type.add(contact)
        else:
            rest_type.add(contact)

    if merged_type:
        if len(merged_type) == 1:
            logger.debug("Only 1 parent detected")
            return merged_type.pop(), list(rest_type | invalid_type)
        else:
            parent = resolve_parent(merged_type)
            return parent, list(merged_type.difference([parent]) | rest_type |
                                invalid_type)
    elif len(rest_type) == 1:
        # Short-circuit performance enhancement. Don't run resolve_parent if
        # there's only one contact.
        return rest_type.pop(), list(invalid_type) or None
    elif rest_type:
        # technically this won't be a parent, but will be the contact the
        # merged contact will be based on.
        parent = resolve_parent(rest_type)
        return parent, list(rest_type.difference([parent]) | invalid_type)
    else:
        # This is to handle the case where we are asked to choose a parent
        # contact out of a list of contacts that only have a first initial.
        #
        # We do not want to create a merge group containing no contacts with a
        # full first name as this would break the rule of never selecting a
        # first-initial contact as a parent.
        #
        # In any event, we shouldn't ever get to this point as earlier parts of
        # the code should prevent it.
        return None, list(invalid_type)


@tracer.wrap()
def _inject_potential_first_initial_contacts_into_groups(grouped_contacts, user):
    '''Inject first initial contacts into every group where they might merge.

    To give an example, if we have first-initial contacts with the name `J
    Smith`, and then groups of contacts with the names `John Smith` and `Jackie
    Smith`, since we don't know which group of contacts `J Smith` goes with, we
    add the ids for the first initial contacts into both groups. This is safe
    to do because first initial contacts are discarded with no changes made,
    unless a match is found on address, phone number, or email address.

    This function is expected to be run on the output of `_groupify()`, and to
    be run before sending the contact groups off to the contact partitioner.
    '''
    # Get the first name, last name, and id of every contact that only has an
    # initial for their first name. Group contact ids by (last_name,
    # first_name).
    logger.info("Injecting first initial contacts into potential groups")
    first_initial_contacts = user.contact_set.exclude(
        needs_attention=True).exclude(
            last_name_lower__length__lt=2).filter(
                first_name_lower__length=1).values(
                    'last_name_lower', 'first_name_lower').annotate(
                        contact_ids=ArrayAgg('id')).values_list(
                            'last_name_lower', 'first_name_lower', 'contact_ids')

    first_initial_contact_map = {
        (last_name, first_initial): set(contact_ids)
        for last_name, first_initial, contact_ids in first_initial_contacts}

    # If no first initial contacts are discovered, there's no work to be done
    # and we can exit.
    if not first_initial_contact_map:
        return grouped_contacts

    # Build query to locate the ids of full-name contacts that match our
    # initials.
    names = (Q(last_name_lower=last_name, first_initial=first_initial)
             for last_name, first_initial in first_initial_contact_map)
    names_qry = reduce(lambda a, b: a | b, names)

    # Group full-name contact ids by (last_name, first_initial).
    inject_recipients = user.contact_set.annotate(
        first_initial=Substr('first_name_lower', 1, 1)).exclude(
            first_name_lower__length__lt=2).filter(names_qry).values(
                'last_name_lower', 'first_initial').annotate(
                    contact_ids=ArrayAgg('id')).values_list(
                        'last_name_lower', 'first_initial', 'contact_ids')

    # Iterate through contact groups created by the `_groupify()` function, and
    # look for groups that intersect with the groups in `inject_recipients`. If
    # an intersection is found, that means that a first initial contact might
    # merge with one of the contacts in this group. For every group in
    # `grouped_contacts` that has an intersection with `inject_targets`, we
    # inject the first initial only contacts into that group.
    for last_name, first_initial, inject_targets in inject_recipients:
        to_inject = first_initial_contact_map[(last_name, first_initial)]
        inject_targets = set(inject_targets)
        for group in grouped_contacts:
            if not inject_targets.isdisjoint(group):
                group.update(to_inject)
    return grouped_contacts


@tracer.wrap()
def spawn_mp_worker(queue, user, dryrun, index_filters, multi_collect=True):
    connections.close_all()
    while 1:
        try:
            group = queue.get(timeout=5)
        except Empty:
            logger.info("Worker shutting down as queue is empty")
            break
        try:
            process_contact_ids(group, user, dryrun=dryrun,
                                index_filters=index_filters,
                                multi_collect=multi_collect)
        except Exception as ex:
            logger.exception("Error occurred while merging contacts: %s",
                             group)
            raise ex


@tracer.wrap()
def start_mp_mode(grouped_contacts, user, dryrun, index_filters,
                  num_processes=2, multi_collect=True):
    queue = Queue()
    for group in grouped_contacts:
        queue.put(group, block=False)
    processes = []
    for _ in range(num_processes):
        p = Process(target=spawn_mp_worker,
                    args=(queue, user, dryrun, index_filters),
                    kwargs=dict(multi_collect=multi_collect))
        processes.append(p)
        p.daemon = True
        p.start()
    dead = set()
    while any(ps.is_alive() for ps in processes):
        for p in processes:
            p.join(timeout=15)
            if not p.is_alive():
                if p.exitcode != 0:
                    raise ProcessError(
                        "A merging subprocess died unexpectedly!")
                elif p not in dead:
                    dead.add(p)
                    logger.info("Merging subprocess finished. PID: %s", p.pid)


@tracer.wrap()
def prepare_contact_sibling_groups(user):
    """Prepare contact sibling groups for user-wide contact merging

    This function separate all contacts owned by the input user into groups of
    siblings.  Groups are created by using prefix merging on first names (where
    the last name is the same), collecting force merge groups and alias
    expansion groups. All groups with contacts in common are merged, and then
    contacts with only a first initial are injected into potential groups.

    These steps are all grouped together in this function because they are
    batch operations that cannot be parallelized.

    All that remains to do after this function is apply the partitioning rules
    to each group of contacts, and then persist the changes.

    This function returns a list of sets of contact ids.
    """
    megadict = _create_megadict_for_user(user)
    grouped_contacts, _ = _trie_groupify(megadict)
    logger.info("Fetching force merges")
    force_merge_groups = _fetch_force_merge_groups(user)
    logger.info("Fetching alias expansion groups")
    alias_expansion_groups = _fetch_alias_expansion_groups(user)
    logger.info("Merging groups when overlap is detected")
    grouped_contacts, _ = union_find_reducer(
        chain(grouped_contacts, force_merge_groups, alias_expansion_groups))
    grouped_contacts = _inject_potential_first_initial_contacts_into_groups(
        grouped_contacts, user)
    return grouped_contacts


@tracer.wrap()
def remerge_contacts(user, dryrun=False, name_expansion=False,
                     num_processes=None, multi_collect=True):
    """Perform contact merging on user
    """
    connections.close_all()
    grouped_contacts = prepare_contact_sibling_groups(user)

    index_filters = user.get_index_filters()
    logger.info(
        "Starting going through contact groups to apply partitioning rules")
    if num_processes is not None and num_processes > 1:
        start_mp_mode(grouped_contacts, user, dryrun, index_filters,
                      num_processes, multi_collect=multi_collect)
    else:
        for group in grouped_contacts:
            process_contact_ids(group, user, dryrun=dryrun,
                                index_filters=index_filters,
                                multi_collect=multi_collect)


@tracer.wrap()
def merge_retry_wrapper(max_retries=10, raise_on_failure=False):
    def wrapper(f):
        @wraps(f)
        def inner(contacts, *args, **kwargs):
            merge_success = False
            last_error = Exception
            for i in range(max_retries):
                try:
                    # Wrap this processing with a transaction so in case
                    # something fails, we can unwind any changes.
                    with transaction.atomic():
                        result = f(contacts, *args, **kwargs)
                        merge_success = True
                except MergeConflict as e:
                    last_error = e
                    logger.info(
                        "Merge conflict detected when performing "
                        "merge for contact(s) %s sleeping and "
                        "retrying.", contacts)
                    # If we got here, another task is doing a contact merge on
                    # this same contact at the same time. Wait for up to 30s
                    # before trying again.
                    sleep(min((0.1 * (1 + i)), 30))
                else:
                    return result
            if not merge_success:
                logger.warning("Max retries: unable to merge contact(s) {} "
                               "due to conflicts".format(contacts))
                if raise_on_failure:
                    raise last_error
        return inner
    return wrapper
