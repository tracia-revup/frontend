
import logging

from frontend.apps.contact.models import Contact
from frontend.libs.utils.model_utils import custom_model_to_dict

LOGGER = logging.getLogger(__name__)


class ContactWrapper(object):
    """The purpose of this class is to wrap a Contact model and its
    one-to-many "special fields". There are cases in the code below where
    a Contact may be instantiated, but never saved, or where the
    "special fields" may be modified. Since the "special fields" are their
    own models, we only want to save or delete them in certain cases.
    This wrapper handles those cases.
    """
    SPECIAL_FIELDS = Contact.SPECIAL_FIELDS

    def __init__(self, contact=None, **kwargs):
        for field in self.SPECIAL_FIELDS:
            try:
                # Pop the special field kwarg so we don't pass it to Contact
                setattr(self, field, kwargs.pop(field))
            except KeyError:
                # If the special field wasn't given, its value shouldn't change
                self.__dict__[field] = self._get_special_obj(False, [])

        # Bypass setattr
        self.__dict__['contact'] = contact or Contact(**kwargs)

    def get_special_field(self, field):
        if field not in self.SPECIAL_FIELDS:
            raise AttributeError("not a special field")

        special = getattr(self, field, {})
        # If the special field is not dirty, then we'll look at the contact
        if special.get('dirty', False):
            return special.get('objects', [])
        else:
            return getattr(self.contact, field)

    def _get_special_obj(self, dirty, value):
        # This handles the special case where the programmer is passing
        # special field dicts around
        if isinstance(value, dict) and 'objects' in value:
            value = value['objects']

        value = {'dirty': dirty,
                 'objects': value or []}
        return value

    def __setattr__(self, key, value):
        if key in self.SPECIAL_FIELDS:
            value = self._get_special_obj(True, value)
            super(ContactWrapper, self).__setattr__(key, value)
        else:
            # Can't assign ContactWrapper as a foreign key. This is a fix.
            if key == 'parent' and isinstance(value, ContactWrapper):
                value = value.contact
            setattr(self.contact, key, value)

    def __getattr__(self, item):
        if not hasattr(self, 'contact'):
            raise AttributeError("Wrapped contact missing!")
        # This will be called if the requested object is in the Contact
        # instead of a special-field stored in this object
        try:
            return getattr(self.contact, item)
        except AttributeError as e:
            raise e

    def __eq__(self, other):
        if isinstance(other, Contact):
            # Compare the contacts using Django's comparison.
            return self.contact == other
        elif isinstance(other, ContactWrapper):
            # Utilize other's __eq__ method
            return other == self.contact
        else:
            return False

    def save(self):
        # Save the contact
        try:
            self.contact.save()
        except Exception as err:
            LOGGER.warn("Contact ({}) failed to save. Reason: {}".format(
                        self.contact, err))
            return

        # If one of the special fields has been updated, we update the old
        # records if we find a match on key fields, otherwise we create a new
        # record.
        for field in self.SPECIAL_FIELDS:
            special_field = self.__dict__.get(field, {})
            dirty = special_field.get('dirty', False)
            if dirty:
                related_field = getattr(self.contact, field)
                related_model = related_field.model

                # Handle ManyToMany and ManyToOne relations differently. If
                # there is a through attribute on the field, that means we are
                # dealing with a ManyToMany relation.
                if hasattr(related_field, 'through'):
                    # The django ORM automagically dedupes ManyToMany
                    # relations, so we can just use the simple `add` method on
                    # the related manager. Easy peasy. There also is no
                    # updating these records. Yay!
                    objs = special_field.get('objects', [])
                    related_field.add(*objs)
                else:
                    # We need special handling for our ManyToOne relations for
                    # two reasons: We need to populate the contact field
                    # ourselves because this could change (if we find a
                    # pre-existing contact, for example). Also these records
                    # could change on the source, and we want to keep up to
                    # date, but at the same time we don't want to throw away
                    # any old data.
                    #
                    # In order to accomodate those final two goals, we use the
                    # update_or_create manager method. match_fields is a tuple
                    # of field names that contains data that will not change
                    # (email address, phone number, address). Those fields are
                    # used to locate existing records that we will update with
                    # the new data passed via defaults kwarg.
                    match_fields = related_model.match_fields
                    for obj in special_field.get('objects', []):
                        obj.contact = self.contact
                        # Extract the model data as a dict, and separate out
                        # the data that needs to be updated
                        obj_data = custom_model_to_dict(obj, exclude=['archived'])
                        # Delete the ID, otherwise it saves a "None" and
                        # screws things up.
                        del obj_data["id"]
                        match_on = {match_field: obj_data.pop(match_field, "")
                                    for match_field in match_fields}
                        try:
                            related_field.update_or_create(defaults=obj_data,
                                                           **match_on)
                        except related_model.MultipleObjectsReturned:
                            try:
                                related_field.filter(**match_on).delete()
                                obj.save()
                            except Exception as err:
                                LOGGER.warn(
                                    "Failed to save '{}' for Contact ({}). "
                                    "Reason: {}".format(field, self.contact,
                                                         err))
                        except Exception as err:
                            LOGGER.warn(
                                "Failed to save '{}' for Contact ({}). "
                                "Reason: {}".format(field, self.contact, err))

                # Not dirty anymore
                special_field['dirty'] = False
                # Make sure it's preserved; bypass setattr
                self.__dict__[field] = special_field

    @classmethod
    def from_iterator(cls, iterator):
        return [cls(contact=item) for item in iterator]

    def __str__(self):
        try:
            return self.contact.__str__()
        except AttributeError:
            return super(ContactWrapper, self).__str__()

    def __repr__(self):
        return '<%s: %s>' % (self.__class__.__name__, str(self.contact))
