from collections import Mapping
from functools import reduce
from itertools import product
import logging
import random
from time import sleep
from threading import Thread

from celery import current_task, group
from ddtrace import tracer
from django.conf import settings
from django.db import models, transaction
from django.db.models import Q, F, Func, CharField
from django.db.models.functions import Lower, Substr

from frontend.apps.analysis.api.views import (
    ContactSetAnalysisContactResultsViewSet)
from frontend.apps.analysis.utils import submit_analysis
from frontend.apps.contact.models import (
    Contact, ImportRecord, ContactFileUpload, EmailAddress, PhoneNumber)
from frontend.apps.contact.analysis.remerge import (
    process_contacts, _parent_update_and_deconflict, ContactPartitioner)
from frontend.apps.contact.exceptions import MergeConflict
from frontend.apps.contact_set.models import ContactSet
from frontend.apps.entities.models import Identity
from frontend.apps.filtering.models import Filter, ContactIndex
from frontend.apps.seat.models import DelayedAnalysis
from frontend.libs.utils.celery_utils import (
    ALL_CELERY_EXCEPTIONS, update_progress, get_task_record)
from frontend.libs.utils.model_utils import ArrayAgg
from frontend.libs.utils.namelib_utils import namelib
from frontend.libs.utils.kafka_utils import init_producer
from frontend.libs.utils.general_utils import deep_hash, partition_into_batches
from frontend.libs.utils.string_utils import random_uuid
from .utils import ContactWrapper


LOGGER = logging.getLogger(__name__)


class BadContactError(Exception): pass


class ContactImportBase(object):
    MERGE_MAX_RETRY = 20

    def __init__(self, user, source, account=None, feature_flags=None,
                 *args, **kwargs):
        self.progress = 0
        self.total_results = None
        self.last_percent = None
        self.import_record = None
        self.feature_flags = feature_flags or {}
        # XXX: Note that feature flags are determined by the requesting user,
        # not the user that will end up owning the contacts.
        self.send_contacts_to_backend = (self.feature_flags.get(
            "Contact Import Backend Export", False) and
            settings.ENABLE_KAFKA_PUBLISHING)
        self.use_name_expansion = self.feature_flags.get(
            "Contact Import Name Expansion", False)
        self.use_alias_expansion = self.feature_flags.get(
            "Contact Import Alias Expansion", False)
        # TODO: change the always merge default back to False once we can
        # determine when an existing contact will be updated by the new
        # contact.
        self.always_merge = self.feature_flags.get(
            "Contact Import Always Merge", True)
        self.parallel_execution = self.feature_flags.get(
            "Contact Import Parallel Execute", False)
        self.merge_conflict_stacktrace = self.feature_flags.get(
            "Contact Import Merge Conflict Stacktrace", False)
        self.multi_collect = self.feature_flags.get(
            "Contact Import Partition Multi Collect", False)
        self.inline_merging = not self.feature_flags.get(
            "Contact Merge Separate Task", False)
        # Refresh the user so we don't have stale user data
        self.user = user
        if account:
            # Refresh the account object, so we don't have stale data
            self.account = account
            self.contact_user = account.account_user
            self.create_limit = account.create_limit
        else:
            self.account = None
            self.contact_user = user
            self.create_limit = settings.PERSONAL_CONTACT_CREATE_LIMIT

        self.source = source
        self.import_transaction_id = str(random_uuid())
        self.filters = self.contact_user.get_index_filters()

    def _update_progress(self, amount=1, multiplier=1.0):
        """Increment the progress by a certain percent.

        Note: The attribute self.total_results needs to be set somewhere in
              before this method will start setting the percent value.
              Otherwise, the percent will not be set.

        The multiplier will be multiplied against the amount.
        For example:
        If we iterate over the same list twice, then 1 progress-increment is
        really only 50%, so the multiplier should be 0.5
        """
        self.progress += (amount * multiplier)
        if not self.total_results:
            LOGGER.warn("Importer cannot update progress when total_results are "
                        "undefined.")
            return
        update_progress(self.progress, self.total_results)

    @tracer.wrap()
    def _get_existing(self, contact, extra_filters=None):
        """Search for an existing contact matching the given.

        Some types of import may not have a contact_id, so this method
        would need to be overridden.
        """
        if extra_filters is None:
            extra_filters = {}
        existing = ContactWrapper.from_iterator(
            Contact.objects.filter(user=contact.user_id,
                                   contact_id=contact.contact_id,
                                   **extra_filters))
        try:
            return existing[0]
        except IndexError:
            return None

    @classmethod
    @tracer.wrap()
    def _clean_and_lowercase_name(cls, name):
        return Contact._clean_and_lowercase_name(name)

    def generate_import_record(self):
        tt = ImportRecord.TaskType
        return ImportRecord(
            user=self.user,
            account=self.account,
            import_type=tt.source_map.get(self.source, tt.OTHER),
            transaction_id=self.import_transaction_id
        )

    def get_raw_contacts(self):
        """This should be overridden to query the appropriate API, and return
        an iterator of raw api objects (xml, json, etc.)
        """
        raise NotImplementedError

    def _verify_contact_kwargs(self, contact_kwargs):
        """Checks the kwargs for conditions that are universal for rejecting
           a contact.
       """
        if not contact_kwargs:
            return
        if not (contact_kwargs["first_name"] and contact_kwargs["last_name"]):
            # Contacts are required to have at least first and last name
            raise BadContactError("Missing first or last name")

    @tracer.wrap()
    def build_contact_kwargs(self, raw_contact):
        """Build and verify the kwargs that go into creating a contact."""
        contact_kwargs = self._build_contact_kwargs(raw_contact)
        try:
            self._verify_contact_kwargs(contact_kwargs)
        except BadContactError as error:
            LOGGER.info("Contact rejected. Reason: {}. Kwargs: {}".format(
                error, contact_kwargs))
            return None
        else:
            return contact_kwargs

    def _build_contact_kwargs(self, raw_contact):
        """Generate the kwarg data needed to create a Contact."""
        raise NotImplementedError()

    @tracer.wrap()
    def build_contact(self, raw_contact):
        """Convert a raw, api contact into a RevUp Contact.

        If the contact is invalid, this method should return None
        """
        contact_data = self.build_contact_kwargs(raw_contact)
        if contact_data:
            phone_numbers = contact_data.get('phone_numbers')
            if phone_numbers:
                for phone_number in phone_numbers:
                    phone_number.number = phone_number.rfc3966
            contact = ContactWrapper(**contact_data)
            contact.user = self.contact_user
            contact.import_record = self.import_record
            contact.contact_type = self.source
            contact.composite_id = Contact.calc_composite_id(contact)
            # The following may be modified or otherwise already stored, so we
            # don't want to overwrite them.
            if "raw_contact" not in contact_data:
                contact.raw_contact = raw_contact
            if "is_primary" not in contact_data:
                contact.is_primary = False
            return contact

    @tracer.wrap()
    def _update_if_exists(self, contact, may_create_new=True):
        """Upsert contact

        Checks to see if contact already exists first. If it does, the existing
        contact is updated with information from the new contact just imported.
        Otherwise the new contact is just saved.

        Returns a tuple of (saved_contact, bool(is_new))
        """
        # Check if this contact is already in the DB.
        existing = self._get_existing(contact)
        if existing:
            if (existing.first_name_lower != contact.first_name_lower or
                    existing.last_name_lower != contact.last_name_lower):
                # If the contact name changes, create an alias to keep
                # a record of the old name. This is necessary to
                # prevent a merged contact from being split if a name
                # changes (ex- taking spouse's family name after
                # marriage).
                existing.alias_to(
                    contact,
                    label="Name change link for contact id: {}".format(
                        existing.pk))
            existing.first_name = contact.first_name
            existing.last_name = contact.last_name
            existing.additional_name = contact.additional_name
            existing.first_name_lower = contact.first_name_lower
            existing.last_name_lower = contact.last_name_lower
            existing.name_prefix = contact.name_prefix
            existing.name_suffix = contact.name_suffix
            existing.contact_type = contact.contact_type
            existing.raw_contact = contact.raw_contact

            # Copy all of the special fields
            for field in Contact.SPECIAL_FIELDS:
                setattr(existing, field, getattr(contact, field))

            # If a Contact is so old that we weren't tracking ImportRecords,
            # we need to associate it with an ImportRecord, even if it isn't
            # the perfectly correct one.
            if not existing.import_record_id:
                existing.import_record = contact.import_record
            contact = existing

        if existing or may_create_new:
            contact.save()
        return contact, not existing

    @tracer.wrap()
    def execute(self):
        """Primary entry point for importer

        Used to start the analysis.

        If parallel_execution is enabled returns the AsyncResult for the
        subtask.
        """
        if self._start():
            if self.parallel_execution and current_task:
                return self._parallel_run()
            else:
                self._run()

    @tracer.wrap()
    def _start(self):
        """Perform any setup steps required before running the import.

        This is really a private method and should only be used by the importer
        itself.
        """
        task = get_task_record()
        if not task:
            return False
        if task.blocked():
            current_task.retry(countdown=30 + random.randint(-10, 10))

        # Generate an ImportRecord for this import job
        self.import_record = self.generate_import_record()
        if current_task:
            self.import_record.task_id = current_task.request.id
        self.import_record.save()

        # Create the ContactSets associated with this import
        self.create_import_contact_sets(self.import_record,
                                        user=self.contact_user,
                                        account=self.account)
        return True

    @classmethod
    def contactset_deleteable(self):
        """Returns True if a a ContactSet is deletable. By default all
        ContactSets would be deleteable"""
        return True

    @classmethod
    @tracer.wrap()
    def create_import_contact_sets(cls, import_record, user=None, account=None):
        """Create any ContactSets that would be generated from this import"""
        # Prepare all of the arguments needed to create the CS
        source = import_record.import_type
        # We don't have an analog for the "Other" import type
        if source == ImportRecord.TaskType.OTHER:
            raise TypeError("'OTHER'-type imports can not be used with "
                            "ContactSets. Define a new TaskType")

        if not user:
            user = import_record.user
        title = import_record.label
        source_uid = import_record.uid
        import_source_filter = Filter.objects.get(title="Import Source Detail")
        query_args = import_source_filter.dynamic_instance._prepare_index(
            [source_uid])
        filters = [import_source_filter]
        deletable = cls.contactset_deleteable()

        def _create_cs(acct):
            # If the CS already exists, just update its count. Otherwise,
            # create a new CS for this import source and account
            cs, created = ContactSet.get_or_create(
                acct, user,
                filter_kwargs=dict(source=source, source_uid=source_uid),
                create_kwargs=dict(
                    title=title, query_args=query_args, filters=filters,
                    source=source, source_uid=source_uid, deletable=deletable)
            )
            if not created:
                # Unarchive in case this contactset was unsuccessfully deleted
                cs.archived = None
                cs.save()
            return cs

        contact_sets = set()
        if account:
            # If this is an account_user, we only need to update the account
            cs = _create_cs(account)
            contact_sets.add(cs)
        else:
            # Update the contact sets and schedule an analysis for all of the
            # user's seats.
            seats_qs = user.seats.select_related(
                "account").assigned()

            # Users needs their ContactSets for their imports available to each
            # of their seats.
            for seat in seats_qs:
                # Create a new CS for this import source and account
                cs = _create_cs(seat.account)
                contact_sets.add(cs)

        return contact_sets

    def _update_contact_sets(self):
        # Note: There is an important distinction between `self.user` and
        # `self.contact_user`. The user in `self.user` is the user that
        # initiated the upload, while `self.contact_user` is the user that owns
        # the imported contacts. Normally those two variables both refer to the
        # same user. At the moment, the only time they differ is when a user is
        # importing account-level contacts.
        self.update_contact_sets(
            import_record=self.import_record,
            user=self.contact_user,
            feature_flags=self.feature_flags
        )

    @classmethod
    @tracer.wrap()
    def update_contact_sets(cls, import_record, user, feature_flags):
        """Update ContactSet counts and schedule analyses for ContactSets
            affected by this import.
        """
        contact_sets = set()
        if hasattr(user, 'account_user'):
            # If this is an account_user, we only need to update the account
            cs = cls._update_account_contact_sets(import_record, user,
                                                  feature_flags)
            contact_sets.add(cs)
        else:
            cs = cls._update_user_contact_sets(import_record, user,
                                               feature_flags)
            contact_sets.update(cs)

        # Update the counts of all this user's remaining ContactSets,
        # as they may have increased with the import.
        for cs in ContactSet.objects.active().filter(user=user) \
                .exclude(id__in=(c.id for c in contact_sets)):
            cs.update_count()
            cs.save()

    @classmethod
    def _update_account_contact_sets(cls, import_record, user, feature_flags):
        """Update ContactSet count and schedule analysis for account user

        Internal method used by `update_contact_sets` meant to be used on
        account users *only*.
        """
        cs = ContactSet.objects.get(account=user.account_user,
                                    user=user,
                                    source=import_record.import_type,
                                    source_uid=import_record.uid)
        # Update contactsets with meta info
        cs.process_import_meta()
        # This update_count is necessary for submit_analysis.
        cs.update_count()
        cs.save()

        submit_analysis(user, user.account_user, cs,
                        countdown=10, add_to_parent=False,
                        flags=feature_flags,
                        transaction_id=import_record.transaction_id,
                        expected_count=import_record.num_valid)
        return cs

    @classmethod
    def _update_user_contact_sets(cls, import_record, user, feature_flags):
        """Update ContactSet count and schedule analysis for normal users

        Internal method used by `update_contact_sets` meant to be used for
        normal users *only*.
        """
        # Update the contact sets and schedule an analysis for all of the
        # user's seats.
        seats_qs = user.seats.select_related("account").assigned()
        current_account = user.current_seat.account \
                          if user.current_seat else None

        bulk_schedule = set()
        contact_sets = set()
        # Users needs their ContactSets for their imports available to each
        # of their seats.
        for seat in seats_qs:
            cs = ContactSet.objects.get(account=seat.account,
                                        user=user,
                                        source=import_record.import_type,
                                        source_uid=import_record.uid)
            # Update contactsets with meta info
            cs.process_import_meta()
            # This update_count is necessary for submit_analysis.
            cs.update_count()
            cs.save()
            contact_sets.add(cs)

            # Trigger an analysis for each active Account
            if seat.account.active:
                # We want to start analyses for the current account
                # immediately. Otherwise, we'll schedule a delayed analysis
                if seat.account == current_account:
                    submit_analysis(
                        user, seat.account, cs,
                        countdown=10, add_to_parent=False,
                        flags=feature_flags,
                        transaction_id=import_record.transaction_id,
                        expected_count=import_record.num_valid)
                else:
                    bulk_schedule.add((seat, cs))

        # Schedule a delayed analysis for the user's other seats.
        # If the user is staff, we don't want to do delayed analysis
        # because it's very annoying when demoing or helping clients
        if bulk_schedule and not user.is_admin:
            seats, sources = list(zip(*bulk_schedule))
            DelayedAnalysis.bulk_schedule(seats, sources)
        return contact_sets

    @tracer.wrap()
    def complete(self, error, stats=None):
        """Finalize contact import

        Updates the import_record noting whether import was successful or
        failed.
        """
        # Update the ImportRecord from the database in case the status has been
        # modified by another task.
        self.import_record.refresh_from_db()
        # If import_success is False, then another worker has already recorded
        # the import status, and we don't need to record it again. This
        # primarily is to ensure that the `contact_import_parallel_complete`
        # task doesn't somehow get magically run when it shouldn't and somehow
        # reset the import_success value to True.
        if self.import_record.import_success is False:
            LOGGER.info("Import success is already recorded as a failure.")
            return

        if stats:
            if isinstance(stats, Mapping):
                stats = [stats]
            for stat in stats:
                self.import_record.num_created += stat['num_created']
                self.import_record.num_existing += stat['num_existing']
                self.import_record.num_merged += stat['num_merged']
                self.import_record.num_invalid += stat['num_invalid']
        self.import_record.import_success = not error

        # Only perform final post-processing tasks if the import completed
        # successfully.
        if not error:
            self.contact_user.applied_filters.set(self.filters)

            if self.inline_merging:
                # Clean up any childless parent contacts.
                Contact.objects.filter(
                    user=self.contact_user, contact_type='merged', is_primary=True,
                    contacts__isnull=True).update(is_primary=False)

            # Invalidate the results cache
            ContactSetAnalysisContactResultsViewSet.invalidate_cache(self.user)

            if self.inline_merging:
                # Create or update the ContactSets for this import type. Since
                # this operation requires contact merging to be performed
                # first, we only run it in the importer when merging is inline.
                self._update_contact_sets()
            else:
                # Queue up contact merging tasks.
                from frontend.apps.contact.task_utils import start_contact_merging
                start_contact_merging(user=self.contact_user, account=self.account,
                                      import_record=self.import_record,
                                      feature_flags=self.feature_flags)

        self.import_record.save()

    @tracer.wrap()
    def _run(self):
        """Runs the contact import in a single thread

        This is really a private method and should only be used by the importer
        itself.
        """
        LOGGER.info("Performing single-thread import of contacts")
        stats = None
        error = None
        try:
            raw_contacts = self.get_raw_contacts()
            stats = self.process_contacts(raw_contacts)
        # Normally, we shouldn't use catch-alls, but if this process is
        # killed, we need to clean up appropriately. Also, we re-raise below.
        except Exception as ex:
            error = ex
        finally:
            self.complete(error, stats)
            # Re-raise the exception after saving the import results
            if error is not None:
                raise error

    @tracer.wrap()
    def _parallel_run(self):
        """Parallel verison of the _run method

        This is really a private method and should only be used by the importer
        itself.

        Returns the AsyncResult for the completion subtask.
        """
        LOGGER.info("Performing parallel import of contacts")
        try:
            raw_contacts = self.get_raw_contacts()
            return self._parallel_process_contacts(raw_contacts)
        # Normally, we shouldn't use catch-alls, but if this process is
        # killed, we need to clean up appropriately. Also, we re-raise below.
        except:
            self.complete(error=True)
            # Re-raise the exception after saving the import results
            raise

    @tracer.wrap()
    def _parallel_process_contacts(self, contacts):
        """Generate the import subtasks

        This is really a private method and should only be used by the importer
        itself.
        """
        from frontend.apps.contact.tasks import (
            contact_import_parallel_process, contact_import_parallel_complete)

        batch_size = settings.CONTACT_IMPORT_PARALLEL_BATCH_SIZE
        tasks = (group(
            contact_import_parallel_process.s(self, list(batch))
            for batch in partition_into_batches(contacts, batch_size)) |
            contact_import_parallel_complete.s(self)
        ).apply_async(add_to_parent=True)
        # We need to return this async result so it gets stored in the root
        # celery result. Without this we have no way to track the final celery
        # task's progress.
        return tasks

    @tracer.wrap()
    def send_contact_to_backend(self, producer, contact):
        raw_contact = contact.raw_contact
        contact_record = {
            'user_id': self.contact_user.id,
            'contact_type': str(self.source),
            'value': raw_contact,
            'txid': self.import_transaction_id,
        }
        result = producer.send(settings.BACKEND_CONTACT_TOPIC,
                               contact_record)
        return result

    @tracer.wrap()
    def process_contacts(self, raw_contacts):
        """Process a collection of raw contacts

        This does all the real work. For each raw contact it:
        - Builds a Contact object
        - Looks for an existing record
        - Updates the existing record, or saves the new record
        - Performs a merge of the contact

        Each contact merge is done within a serializable transaction in order
        to detect merge conflicts. If we detect a merge conflict, we retry the
        merge a maximum of `MERGE_MAX_RETRY` (default 20) times. We sleep after
        every failure, starting with .1 sec and doubles after every failure to
        a max of 30 seconds..
        """
        may_create_new_contacts = (
            self.contact_user.contact_set.filter(is_primary=True).count() <
            self.create_limit
        )
        # Update the ImportRecord from the database in case the status has been
        # modified by another task.
        self.import_record.refresh_from_db()
        # If import_success is False, then another worker failed and has
        # already recorded the import status. In which case we somehow didn't
        # get the revoke message, and we should quit.
        if self.import_record.import_success is False:
            LOGGER.info("Import already failed, skipping any additional work.")
            return

        published_contacts = []
        contacts = []
        num_created = 0
        num_existing = 0
        num_merged = 0
        num_invalid = 0
        producer = None
        if self.send_contacts_to_backend:
            producer = init_producer()

        for raw_contact in raw_contacts:
            try:
                contact = self.build_contact(raw_contact)
                # If the contact can't have a composite_id, our system can't
                # process it, so it is effectively junk.
                if contact and not contact.composite_id:
                    raise BadContactError()
            except BadContactError:
                # In theory, this should be rare, so let's monitor closely
                LOGGER.error("Contact is missing one or more required values, and "
                             "was unable to generate a composite ID. "
                             "User: {}; Source: {}; Contact: {}".format(
                                 self.user, self.source, raw_contact))
                contact = None
            except Exception:
                LOGGER.warning("Uncaught exception in contact import. "
                               "User: {}, contact_user: {}, source: {}".format(
                                   self.user, self.contact_user, self.source),
                    exc_info=True)
                contact = None

            if not contact:
                num_invalid += 1
                # Some contacts may be invalid
                continue
            try:
                contact, is_new = self._update_if_exists(
                                            contact, may_create_new_contacts)
                # The user has been cut off from creating new contacts
                if is_new and not may_create_new_contacts:
                    num_invalid += 1
                    continue
                if is_new:
                    num_created += 1
                else:
                    num_existing += 1
                contacts.append((contact, is_new))
                # Increment the progress bar (if this is a celery task)
                self._update_progress(multiplier=0.4)
            except Exception as err:
                num_invalid += 1
                LOGGER.warning("Error while importing contact: {}".format(err),
                               exc_info=True)
                continue

            if self.send_contacts_to_backend:
                published_contacts.append(
                    self.send_contact_to_backend(producer, contact))

        flusher = None
        if self.send_contacts_to_backend:
            # Make sure none of the raw contacts have failed during sending
            # yet. If there's a failure, there's no point in continuing.
            for published_contact in published_contacts:
                if published_contact.failed():
                    raise published_contact.exception

            # All raw contacts for this batch have been added to the producer.
            # Perform a flush in the background while we continue processing
            # the contacts for the frontend.
            flusher = Thread(target=producer.flush)
            flusher.start()

        if self.inline_merging:
            num_merged = self._import_merge_processing(contacts)

        if self.send_contacts_to_backend:
            # Block until the kafka producer has finished flushing.
            flusher.join()
            # Check all the published contact futures from kafka producer to
            # make sure all succeeded.
            for published_contact in published_contacts:
                if published_contact.failed():
                    raise published_contact.exception

        return dict(
            num_created=num_created,
            num_existing=num_existing,
            num_merged=num_merged,
            num_invalid=num_invalid)

    @tracer.wrap()
    def _import_merge_processing(self, contacts):
        num_merged = 0
        already_merged = set()
        for contact, is_new in contacts:
            try:
                # To improve performance, we only attempt to merge new
                # contacts.  However, we can also use the waffle Flag to force
                # it.
                if ((is_new or self.always_merge) and
                        contact.id not in already_merged):
                    # Wrap this processing with a transaction in case something
                    # fails, we can unwind any changes.
                    merge_success = False
                    for i in range(self.MERGE_MAX_RETRY):
                        try:
                            with transaction.atomic():
                                # Merge the contact. If the contact is already
                                # in the all_merged set, that means it was
                                # already merged while processing earlier in
                                # this loop. No need to go again.
                                merged = self.merge_contact(contact)
                                already_merged.update(merged)
                                merge_success = True
                        except MergeConflict:
                            LOGGER.info(
                                "Merge conflict detected when performing "
                                "merge for contact %s sleeping and "
                                "retrying.", contact,
                                exc_info=self.merge_conflict_stacktrace)
                            # If we got here, another task is doing a
                            # contact merge on this same contact at the
                            # same time. Wait for up to 30s before trying
                            # again.
                            sleep(min((0.1 * (1 + i)), 30))
                        else:
                            break
                    if not merge_success:
                        LOGGER.warning("Max retries: unable to merge contact {} "
                                       "due to conflicts".format(contact))
                    else:
                        num_merged += 1

                elif contact.id in already_merged:
                    # Another contact in this file was already merged and
                    # matched with this one. We want to keep track of all
                    # contacts in a file that were merged together.
                    # Note: we can't use len(already_merged) because that
                    # includes contacts from other import sources
                    num_merged += 1

                # Increment the progress bar (if this is a celery task)
                self._update_progress(multiplier=0.6)
            except ALL_CELERY_EXCEPTIONS:
                # If we get a celery exception, let's raise it higher.
                raise
            except Exception as err:
                LOGGER.exception("Error while merging contact: {}. ERROR: {}".format(
                    contact, err))
        return num_merged

    @staticmethod
    def _build_first_name_qry(first_name):
        return (models.Q(first_name_lower__startswith=first_name) |
                models.Q(first_name_lower__startof=first_name))

    @classmethod
    @tracer.wrap()
    def _build_find_siblings_query(cls, contacts, use_name_expansion=True,
                                   use_alias_expansion=True):
        """Build a `Q()` object that can be used to locate merge siblings

        This function is primarily meant to be used by the `_find_siblings()`
        method.

        If no valid last names or first names are found in input contacts (i.e.
        longer than 1 character), then None is returned.
        """
        last_names = {c.last_name_lower for c in contacts
                      if c.last_name_lower}
        first_names = {c.first_name_lower for c in contacts
                       if len(c.first_name_lower) > 1}
        if not all([last_names, first_names, contacts]):
            # If we don't have any last names, any first names, or any
            # contacts, there's no point in going forward as only badness will
            # result. Either this function will throw an exception, or weird or
            # bad or too many results will found.
            return None
        nicknames = None
        aliases = []
        if use_name_expansion:
            nicknames = namelib.get_nicknames(first_names,
                                              allow_empty_result=True)
        if use_alias_expansion:
            ident_names = {}
            for contact in contacts:
                ident_names.setdefault(contact.last_name_lower, []).append(
                    contact.first_name_lower)
            aliases = Identity.objects.aliases(*list(ident_names.items()))

        queries = []
        if aliases:
            for identity in aliases:
                queries.append(models.Q(
                    first_name_lower=cls._clean_and_lowercase_name(
                        identity.given_name),
                    last_name_lower=cls._clean_and_lowercase_name(
                        identity.family_name)))

        # Find contact siblings/dupes via prefix matching and nickname
        # expansion (if available). Name prefix matching is now being done on
        # the db server, being made possible by the `startof` lookup. `startof`
        # is basically a `startswith` lookup with the arguments reversed.
        first_name_search = reduce(lambda a, b: a | b,
                                   list(map(cls._build_first_name_qry, first_names)))

        if nicknames:
            first_name_search |= models.Q(first_name_lower__in=nicknames)
        queries.append(models.Q(first_name_search, first_name_lower__ne='',
                                last_name_lower__in=last_names))

        name_queries = reduce(lambda a, b: a | b, queries)
        return name_queries

    @classmethod
    @tracer.wrap()
    def _find_siblings(cls, contact, use_name_expansion=True,
                       use_alias_expansion=True):
        """Returns other contacts describing the same person

        Contact "siblings" have the same last name and same (or variation)
        first name as the input contact.

        Use name expansion and alias expansion, if enabled, to help generate
        the query. Query generation was split off into a separate method to
        make the merge_contact method more readable, and to aid in testability.

        NOTE:
        - Input contact must have a full first name. (more than 1 character
          long)
        - This method DOES NOT return the input contact.
        """
        all_siblings = set()
        search_targets = [contact]
        user_id = contact.user_id
        exclude_ids = {contact.id}
        while 1:
            siblings = []
            # Force merge targets
            siblings.extend(Contact.objects.filter(
                contact_links__dimension__contact_links__contact__in=search_targets).exclude(
                    id__in=exclude_ids).distinct())
            # build query searching for contacts with similar names
            name_queries = cls._build_find_siblings_query(
                search_targets, use_name_expansion=use_name_expansion,
                use_alias_expansion=use_alias_expansion)
            if not name_queries:
                break
            siblings.extend(Contact.objects.filter(name_queries,
                                                   user_id=user_id).exclude(
                id__in=exclude_ids).exclude(contact_type='manual').exclude(
                    needs_attention=True))
            if not siblings:
                break
            all_siblings.update(siblings)
            exclude_ids.update(s.id for s in siblings)
            search_targets = siblings
        return all_siblings

    @classmethod
    @tracer.wrap()
    def _find_alt_base(cls, contact):
        """Find a suitable contact to use for locating merge candidates

        We do not want to use a contact that only has a first initial to locate
        merge candidates. It is useless in nickname expansion, and since we use
        prefix matching it will result in too many unrelated contacts being
        merged together. To fix this problem, we need to find a more suitable
        contact to use for locating merge candidates.

        A suitable alternate search base contact has:
        - A full first name
        - An attribute in common with our original first initial contact
          (email, phone number, or address)
        """
        alt_base = None
        emails = list(EmailAddress.objects.filter(
            contact=contact, archived=None).exclude(address='').distinct(
                'address').only('address'))
        phone_numbers = list(PhoneNumber.objects.filter(
            contact=contact, archived=None).exclude(number='').distinct(
                'number').only('number'))
        dimensions = []
        if emails:
            dimensions.append(Q(email_addresses__address__in=emails,
                                email_addresses__archived=None))
        if phone_numbers:
            dimensions.append(Q(phone_numbers__number__in=phone_numbers,
                                phone_numbers__archived=None))

        # Try to locate an alternate base contact that has a phone number or
        # email address in common with our first initial contact.
        if dimensions:
            qry = reduce(lambda a, b: a | b, dimensions)
            alt_base = Contact.objects.filter(
                qry,
                user_id=contact.user_id,
                last_name_lower=contact.last_name_lower,
                first_name_lower__startswith=contact.first_name_lower,
                first_name_lower__length__gte=2,
            ).exclude(id=contact.id).exclude(
                contact_type__in=['merged', 'manual']).first()

        # If the contact we were asked to locate an alternate base for either
        # has no email addresses and phone numbers, or no alternate contact was
        # found on either of those dimensions, try to locate an alt base using
        # postal addresses.
        if not alt_base and contact.addresses.exists():
            # TODO: switch to `_find_alt_base_by_address_new()` when we get
            # merged with Michael's branch
            alt_base = cls._find_alt_base_by_address(contact)
        return alt_base

    @classmethod
    @tracer.wrap()
    def _find_alt_base_possibilities_for_address_match(cls, contact):
        """Query for contacts that might have an address in common with input

        Returns None if input contact has no addresses with a city, state or
        zip value. Otherwise returns a queryset of potential contacts.
        """
        # Discover every unique city, state, and zipcode for our input contact.
        # Returns a dict that looks like:
        #     fields = {'cities': [u'san francisco'],
        #               'states': [u'ca'],
        #               'zipcodes': [u'94105', u'94177']}
        # Normalize discovered values to increase chances of a match, and
        # collect into an array.
        fields = contact.addresses.filter(archived=None).values('contact_id')\
                .aggregate(
                    cities=ArrayAgg(Lower(Func(F('city_key__name'), F('city'),
                                               function='COALESCE',
                                               output_field=CharField())),
                                    distinct=True),
                    states=ArrayAgg(Lower(Func(F('city_key__state__abbr'),
                                               F('region'),
                                               function='COALESCE',
                                               output_field=CharField())),
                                    distinct=True),
                    zipcodes=ArrayAgg(Substr(Func(F('zip_key__zipcode'),
                                                  F('post_code'),
                                                  function='COALESCE',
                                                  output_field=CharField()),
                                             1, 5),
                                      distinct=True))

        # Start building `Q()` object to filter contacts based on whether they
        # have a city, state, or zip code in common with input contact.
        key_trans = {
            'cities': 'city_lower__in',
            'states': 'state_lower__in',
            'zipcodes': 'zipcode__in',
        }
        dimensions = []
        for key, values in fields.items():
            values = list(filter(None, values))
            if values and key in key_trans:
                dimensions.append(Q(**{key_trans[key]: values}))

        # If no values for city, state, or zip were discovered for our input
        # contact, there's no point in doing any further work.
        if not dimensions:
            return

        addr_qry = reduce(lambda a, b: a | b, dimensions)

        # A valid potential contact:
        # - has a full name that starts with the same letter as our input
        #   contact
        # - has the same last name as input contact
        # - is owned by the same user
        # - is not a manual or merged contact
        # - already has one of the following in common: city, state, zip
        possibilities = Contact.objects.filter(
            user_id=contact.user_id,
            last_name_lower=contact.last_name_lower,
            first_name_lower__startswith=contact.first_name_lower,
            first_name_lower__length__gte=2,
        ).exclude(contact_type__in=['merged', 'manual']).annotate(
            archived=F('addresses__archived'),
            # Normalize matchable values the same way we got our input values.
            # By collecting all this data within a single `annotate()`, we also
            # ensure that there's only one join made from contact to address.
            city_lower=Lower(Func(F('addresses__city_key__name'),
                                  F('addresses__city'),
                                  function='COALESCE',
                                  output_field=CharField())),
            state_lower=Lower(Func(F('addresses__city_key__state__abbr'),
                                   F('addresses__region'),
                                   function='COALESCE',
                                   output_field=CharField())),
            zipcode=Substr(Func(F('addresses__zip_key__zipcode'),
                                F('addresses__post_code'),
                                function='COALESCE',
                                output_field=CharField()), 1, 5)
        ).filter(addr_qry, archived=None).distinct('id')
        return possibilities

    @classmethod
    @tracer.wrap()
    def _find_alt_base_by_address_new(cls, contact):
        """Identity alternate base contact by address comparison

        More efficient method of locating a contact that has the same or
        similar address. Iterate through potential contacts comparing addresses
        until we find a match on all possible address fields, or just the
        contact with an address that gets the closest.

        This first implementation only looks at the number of fields the two
        addresses have in common.
        """
        from frontend.apps.contact.analysis.remerge import AddressMatch
        possibilities = cls._find_alt_base_possibilities_for_address_match(
            contact)
        if possibilities is None:
            return
        alt_base = None
        high_score = 0
        self_addrs = ContactPartitioner._get_contact_addresses(contact)
        max_score = 0
        for self_addr in list(self_addrs):
            matchable_fields = sum(1 for f in self_addr if f)
            if matchable_fields == 0:
                # This is unlikely, but if there are no matchable fields in an
                # address, discard it.
                self_addrs.remove(self_addr)
            max_score |= matchable_fields
        if not max_score:
            # somehow all of the addresses associated with our contact contain
            # only empty strings. Since there's nothing to match against, save
            # some time and exit early.
            return
        for possibility in possibilities.iterator():
            other_addrs = ContactPartitioner._get_contact_addresses(possibility)
            for self_addr, other_addr in product(self_addrs, other_addrs):
                result = ContactPartitioner._compare_address(
                    self_addr, other_addr)
                if AddressMatch.differs in result:
                    # If any field between the two addresses differs, it is
                    # invalid and we should continue searching.
                    continue
                else:
                    # Find out how many fields the two addresses have in
                    # common.
                    score = sum(result)
                    if score == 4 or score >= max_score:
                        # If we get a perfect score, there's no point in
                        # searching any further.
                        return possibility
                    # TODO: use weighted scoring in a future iteration. For
                    # example, a match on street and zip is a better match than
                    # one on city, state and zip.
                    elif score > high_score:
                        alt_base = possibility
                        high_score = score
        return alt_base

    @classmethod
    @tracer.wrap()
    def _find_alt_base_by_address(cls, contact):
        """Identity alternate base contact by address comparison

        Identify alternate contact by running all potential contacts through
        group_by_addresses, and selecting a contact from the same group as our
        input contact.
        """
        possibilities = cls._find_alt_base_possibilities_for_address_match(
            contact)
        if possibilities is None:
            return
        alt_base = None
        possibilities = list(possibilities) + [contact]
        groups = ContactPartitioner.group_by_addresses(possibilities)
        for group in groups:
            if contact in group:
                if len(group) >= 2:
                    alt_base = set(group).difference([contact]).pop()
                break
        return alt_base

    @tracer.wrap()
    def merge_contact(self, contact):
        """Search for other contacts that are the same person as the given
        contact, and merge them together under one parent.
        """
        merged = set()

        # We don't do anything with nameless contacts. This should be handled
        # already, but just in case.
        if not (contact.last_name_lower and contact.first_name_lower):
            return merged

        # grab fresh copy of contact to ensure we have the most recent
        # merge_version
        contact = Contact.objects.get(pk=contact.pk)
        orig_contact = contact
        if len(contact.first_name_lower) < 2:
            # We have a contact with an initial as a first name. Find another
            # contact that we can use to find merge candidates.
            search_base = self._find_alt_base(contact)
            if search_base is None:
                # If no alternate search base can be found, then there is
                # nothing that can be done with the contact. Idempotently set
                # contact's state to be a solo contact.
                contact = _parent_update_and_deconflict(contact)
                ContactIndex.update_index(self.filters, user_id=contact.user_id,
                                          contact=contact)
                return merged
            else:
                contact = search_base

        # Find contact "siblings" which describe the same person.
        siblings = list(self._find_siblings(
            contact, use_name_expansion=self.use_name_expansion,
            use_alias_expansion=self.use_alias_expansion))

        if not siblings:
            contact = _parent_update_and_deconflict(contact)
            # Normally contact index would be created after merging was done,
            # but since we're short-circuiting here and not going into contact
            # merging, we need to create the index here.
            ContactIndex.update_index(self.filters, user_id=contact.user_id,
                                      contact=contact)
            return merged

        merged = process_contacts([contact] + siblings,
                                  index_filters=self.filters,
                                  multi_collect=self.multi_collect)
        return merged


class ContactFileImport(ContactImportBase):
    def __init__(self, user, source, source_uid, source_label,
                 contact_file_upload_id, *args, **kwargs):
        super(ContactFileImport, self).__init__(user, source, *args, **kwargs)
        if isinstance(contact_file_upload_id, ContactFileUpload):
            contact_file_upload_id = contact_file_upload_id.pk
        self.contact_file_upload_id = contact_file_upload_id
        self.source_uid = source_uid
        self.source_label = source_label

    def _get_extra_filters_for_existing(self):
        return None

    def complete(self, error, stats=None):
        try:
            return super(ContactFileImport, self).complete(error, stats=stats)
        finally:
            if not error:
                # Since the import completed, we will delete the uploaded file
                # for privacy.
                ContactFileUpload.objects.filter(
                    pk=self.contact_file_upload_id).delete()

    def generate_import_record(self):
        record = super(ContactFileImport, self).generate_import_record()
        record.uid = self.source_uid
        record.label = self.source_label
        return record

    @classmethod
    def hash_raw_contact(cls, raw_contact):
        return deep_hash(raw_contact)
