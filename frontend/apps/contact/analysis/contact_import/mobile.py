
from base64 import b64decode
import json

from django.core.files.base import ContentFile

from frontend.apps.contact.models import (
    Address, EmailAddress, PhoneNumber, Organization, ContactFileUpload,
    ImageUrl, ImportRecord)
from frontend.libs.utils.string_utils import random_uuid
from .base import ContactFileImport


class UnifiedMobileImport(ContactFileImport):
    def __init__(self, user, source, app_version, *args, **kwargs):
        self.app_version = app_version
        self.accessors = self._get_field_accessors()
        super(UnifiedMobileImport, self).__init__(user, source,
                                                  *args, **kwargs)

    def _get_field_accessors(self):
        if self.app_version >= 3:
            return Plugin2Accessors
        else:
            return Plugin1Accessors

    def _get_extra_filters_for_existing(self):
        return dict(import_record__label=self.import_record.label)

    def get_raw_contacts(self):
        cfu = ContactFileUpload.objects.get(id=self.contact_file_upload_id)
        contacts = json.load(cfu.data_file)
        self.total_results = len(contacts)
        return contacts

    def rdg(self, raw, field):
        """Raw-data-get: Get and clean a contact value."""
        val = raw.get(field, "")
        if isinstance(val, str):
            val = val.strip()
        return val

    def _build_contact_kwargs(self, raw_contact, *args, **kwargs):
        first_name = self.rdg(raw_contact, self.accessors.first_name)
        first_name_lower = self._clean_and_lowercase_name(first_name)
        last_name = self.rdg(raw_contact, self.accessors.last_name)
        last_name_lower = self._clean_and_lowercase_name(last_name)
        if not (first_name_lower and last_name_lower):
            # Contacts are required to have at least first and last name
            return None

        # Collect the occupation info, if available
        organization = self.rdg(raw_contact, self.accessors.organization)
        occupation = self.rdg(raw_contact, self.accessors.occupation) \
                     if self.accessors.occupation else ""
        organizations = [Organization(name=organization, title=occupation)] \
                         if (organization or occupation) else []
        image_urls = self._build_image_urls(raw_contact)

        return dict(
            first_name=first_name,
            first_name_lower=first_name_lower,
            last_name=last_name,
            last_name_lower=last_name_lower,
            organizations=organizations,
            addresses=self._build_addresses(
                self.rdg(raw_contact, self.accessors.addresses)),
            phone_numbers=self._build_phones(
                self.rdg(raw_contact, self.accessors.phone_numbers)),
            email_addresses=self._build_emails(
                self.rdg(raw_contact, self.accessors.email_addresses)),
            image_urls=image_urls,
            contact_id=self.rdg(raw_contact, self.accessors.contact_id),
            raw_contact=raw_contact)

    def _build_addresses(self, raw_addresses):
        addresses = []
        for address in raw_addresses:
            address_ = Address.build(
                street=self.rdg(address, self.accessors.street),
                city=self.rdg(address, self.accessors.city),
                region=self.rdg(address, self.accessors.region),
                post_code=self.rdg(address, self.accessors.post_code),
                country=self.rdg(address, self.accessors.country),
                address_type=self.rdg(address, self.accessors.address_type)
            )
            addresses.append(address_)
        return addresses

    def _build_phones(self, raw_phones):
        phones = []
        for phone in raw_phones:
            number = self.rdg(phone, self.accessors.phone_number)
            if number:
                phones.append(PhoneNumber(
                    label=self.rdg(phone, self.accessors.phone_type),
                    number=number))
        return phones

    def _build_emails(self, raw_emails):
        emails = []
        for email in raw_emails:
            addr = self.rdg(email, self.accessors.email_address)
            if addr:
                emails.append(EmailAddress(
                    address=addr,
                    label=self.rdg(email, self.accessors.email_type)))
        return emails

    def _build_image_urls(self, raw_contact):
        if self.rdg(raw_contact, self.accessors.has_image):
            b64_text = self.rdg(raw_contact, self.accessors.image_data)
            image_data = b64decode(b64_text)
            if image_data:
                image_file = ContentFile(image_data,
                                         '{}.png'.format(random_uuid()))
                return [ImageUrl(
                    image=image_file,
                    source=ImportRecord.TaskType.source_map[self.source])]
        return []


class Plugin1Accessors(object):
    contact_id = "identifier"
    first_name = "givenName"
    last_name = "familyName"
    addresses = "postalAddresses"
    phone_numbers = "phoneNumbers"
    email_addresses = "emailAddresses"
    organization = "organizationName"
    occupation = None
    street = "street"
    city = "city"
    region = "state"
    post_code = "postalCode"
    country = "country"
    address_type = "label"
    phone_number = "digits"
    phone_type = "label"
    email_address = "value"
    email_type = "label"
    has_image = "imageDataAvailable"
    image_data = "thumbnailImageData"


class Plugin2Accessors(object):
    contact_id = "recordID"
    first_name = "givenName"
    last_name = "familyName"
    addresses = "postalAddresses"
    phone_numbers = "phoneNumbers"
    email_addresses = "emailAddresses"
    organization = "company"
    occupation = "jobTitle"
    street = "street"
    city = "city"
    region = "state"
    post_code = "postCode"
    country = "country"
    address_type = "label"
    phone_number = "number"
    phone_type = "label"
    email_address = "email"
    email_type = "label"
    has_image = "hasThumbnail"
    image_data = "thumbnailImageData"
