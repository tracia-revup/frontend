import time

import logging
import requests
from urllib.error import HTTPError

from frontend.apps.contact.analysis.contact_import.base import \
    ContactImportBase
from frontend.apps.contact.models import (
    Address, EmailAddress, PhoneNumber)
from frontend.apps.external_app_integration.models import ExternalApp
from frontend.apps.locations.models import ZipCode
from frontend.libs.utils.general_utils import deep_hash
from frontend.libs.utils.string_utils import str_to_bool

LOGGER = logging.getLogger(__name__)


class NationBuilderImport(ContactImportBase):
    """Import contacts from nationbuilder app

    Link to NationBuilder API Docs
    https://nationbuilder.com/api_documentation"""
    def __init__(self, user, source, social=None, *args, **kwargs):
        super(NationBuilderImport, self).__init__(user, source, *args,
                                                  **kwargs)
        self.social = social
        if self.social:
            self.nation_slug = self.social.extra_data['nation_slug']
            self.access_token = self.social.extra_data['access_token']
        else:
            self.nation_slug = kwargs.get('nation_slug', None)
            self.access_token = self.get_access_token()

    def get_access_token(self):
        """Retrieve oauth token"""
        nation = self.contact_user.external_apps.filter(
            app=ExternalApp.App.NATIONBUILDER,
            external_id=self.nation_slug).latest('updated')
        if not nation:
            error_msg = ('Could not retrieve oauth token for the user : {0} '
                         'in the app : NATIONBUILDER with id : {1}').format(
                self.contact_user, self.nation_slug)
            raise AttributeError(error_msg)
        return nation.oauth_token

    def generate_import_record(self):
        record = super(NationBuilderImport, self).generate_import_record()
        if self.social:
            record.uid = record.label = self.social.uid
        else:
            record.uid = record.label = self.nation_slug
        return record

    def get_raw_contacts(self):
        """Import people from nationbuilder"""
        # Reguest nationbuilder for total number of people in this nation
        people_count_url = ('https://{nation_slug}.nationbuilder.com/api/v1/'
                            'people/count?access_token={access_token}').format(
            nation_slug=self.nation_slug, access_token=self.access_token)
        json_response = self.get_request(people_count_url)
        self.total_results = json_response['people_count']

        # Get all the people from nationbuilder in batches of size 100
        people_url = ('https://{nation_slug}.nationbuilder.com/api/v1/'
                      'people?limit=100&access_token={access_token}'
                      ).format(nation_slug=self.nation_slug,
                               access_token=self.access_token)
        results = []
        while len(results) < self.total_results:
            json_response = self.get_request(people_url, max_retries=3,
                                             delay=5)

            # Add nation_slug info to the raw contact
            json_results = json_response['results']
            for result in json_results:
                result['nation_slug'] = self.nation_slug

            results.extend(json_results)
            LOGGER.info(('Imported {current_count} out of {total_count} '
                         'contacts').format(current_count=len(results),
                                            total_count=self.total_results))

            next_url = json_response['next']
            if next_url is None:
                break
            else:
                people_url = ('https://{nation_slug}.nationbuilder.com'
                              '{next_url}&access_token={access_token}').format(
                    nation_slug=self.nation_slug, next_url=next_url+'&',
                    access_token=self.access_token)
        return results

    def _build_contact_kwargs(self, raw_contact):
        """Process the raw data returned by nationbuilder and return the fields
        required to create a Contact object
        """
        contact_id = self._get_contact_id(raw_contact)
        last_name = raw_contact['last_name']
        first_name = raw_contact['first_name']
        email_addresses = self._build_emails(raw_contact)

        addresses = self._build_addresses(raw_contact)

        phone_numbers = self._build_phones(raw_contact)

        return dict(
            contact_id=contact_id,
            first_name=first_name,
            last_name=last_name,
            first_name_lower=self._clean_and_lowercase_name(first_name),
            last_name_lower=self._clean_and_lowercase_name(last_name),
            email_addresses=email_addresses,
            addresses=addresses,
            phone_numbers=phone_numbers,
            raw_contact=raw_contact
        )

    def _get_contact_id(self, raw_contact):
        contact_id = raw_contact.get('id', None)
        nation_slug = raw_contact.get('nation_slug', None)
        if contact_id and nation_slug:
            return f'{nation_slug}:{contact_id}'
        else:
            return deep_hash(raw_contact)

    def _build_addresses(self, raw_contact):
        """Get all the different types of addresses from a contact"""
        addresses = []
        address_types = ('primary_address', 'registered_address',
                         'submitted_address', 'twitter_address',
                         'twitter_location', 'user_submitted_address',
                         'work_address', 'meetup_address', 'mailing_address',
                         'home_address', 'facebook_address', 'billing_address')
        for address_type in address_types:
            raw_address = raw_contact.get(address_type, None)
            if raw_address:
                addresses.append(self.get_address(raw_address))
        return addresses

    def get_address(self, raw_address):
        """
        Get address from nationbuilder.

        Sample address from nationbuilder
        "primary_address": {
                "address1": null,
                "address2": null,
                "address3": null,
                "city": "London",
                "county": null,
                "state": "England",
                "country_code": "GB",
                "zip": null,
                "lat": "51.5073509",
                "lng": "-0.1277583",
                "fips": null,
                "street_number": null,
                "street_prefix": null,
                "street_name": null,
                "street_type": null,
                "street_suffix": null,
                "unit_number": null,
                "zip4": null,
                "zip5": null,
                "sort_sequence": null,
                "delivery_point": null,
                "lot": null,
                "carrier_route": null
            }
        :param raw_address: primary_address returned by nationbuilder
        :return: Address object
        """
        # Build street address from street fields
        def get_street_fields(x):
            return raw_address.get(x, None)

        fields = ('street_number', 'street_prefix', 'street_name',
                  'street_type', 'street_suffix')
        street_fields = list(map(get_street_fields, fields))
        street = " ".join(filter(None, street_fields))

        def raw_address_get(field):
            """Returns empty string if the dictionary value is None"""
            val = raw_address.get(field, '')
            if val is not None:
                return val
            return ''

        city = raw_address_get('city')
        post_code = raw_address_get('zip')
        latitude = raw_address.get('lat', None)
        longitude = raw_address.get('lng', None)
        if not post_code and latitude and longitude:
            post_code = ZipCode.get_us_zipcode(latitude, longitude)
        country = raw_address.get('country_code', None)

        address = Address.build(street=street, city=city, post_code=post_code,
                                country=country)
        return address

    def _build_phones(self, raw_contact):
        """
        Build phone numbers from the fields returned by nationbuilder

        Sample phone fields returned by nationbuilder
        "phone": null,
        "work_phone_number": null
        "mobile": null,
        :return: list of PhoneNumber objects
        """
        phone_numbers = []
        if raw_contact['phone']:
            phone_numbers.append(PhoneNumber(label='phone', primary=True,
                                             number=raw_contact['phone']))
        if raw_contact['work_phone_number']:
            phone_numbers.append(PhoneNumber(label='work', primary=False,
                                             number=raw_contact[
                                                 'work_phone_number']))
        if raw_contact['mobile']:
            phone_numbers.append(PhoneNumber(label='mobile', primary=False,
                                             number=raw_contact['mobile']))
        return phone_numbers

    def _build_emails(self, raw_contact):
        """
        :param raw_contact: contact returned by nationbuilder
        :return: list of EmailAddress objects
        """
        raw_email_addresses = set()
        for x in range(1, 5):
            email = raw_contact.get('email{0}'.format(x), None)
            email_is_bad = str_to_bool(raw_contact.get('email{0}_is_bad'.format(
                x), None))

            if not email_is_bad and email:
                raw_email_addresses.add(email)

        raw_email_addresses.add(raw_contact['email'])

        email_addresses = []
        for raw_email in raw_email_addresses:
            email_addresses.append(EmailAddress(address=raw_email))
        return email_addresses

    def get_request(self, url, max_retries=0, delay=0):
        """Performs a get on the given url and returns json response
        :param max_retries: number of times to retry the request
        :param delay: number of secs to sleep before retrying
        """
        retry_count = 0
        while retry_count <= max_retries:
            try:
                raw_response = requests.get(url)
            except HTTPError:
                retry_count += 1
                LOGGER.error('Retry Count: {count} - Could not fulfill '
                             'request {url}'.format(count=retry_count,
                                                    url=url))
                if retry_count >= max_retries:
                    raise
                time.sleep(delay)
            else:
                break
        json_response = raw_response.json()
        return json_response
