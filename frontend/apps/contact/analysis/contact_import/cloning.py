
from ddtrace import tracer
from django.conf import settings
from django.db import models

from frontend.apps.contact.models import Contact
from frontend.apps.contact_set.models import ContactSet
from .base import ContactImportBase
from .utils import ContactWrapper


class CloningImport(ContactImportBase):
    def __init__(self, user, source, contact_source, *args, **kwargs):
        super(CloningImport, self).__init__(user, source, *args, **kwargs)
        # These are hard rules and should always be true
        assert self.account
        assert self.contact_user == self.account.account_user
        self.source_uid = self.user.username
        self.source_label = "{}'s Contacts".format(self.user.name)
        self.contact_source = contact_source

    def generate_import_record(self):
        record = super(CloningImport, self).generate_import_record()
        record.uid = self.source_uid
        record.label = self.source_label
        return record

    @tracer.wrap()
    def get_raw_contacts(self):
        if isinstance(self.contact_source, Contact):
            self.total_results = 1
            return [self.contact_source.id]
        elif isinstance(self.contact_source, ContactSet):
            self.total_results = self.contact_source.count
            return list(c.id for c in self.contact_source.get_contacts())
        else:
            self.total_results = 0
            return []

    @tracer.wrap()
    def build_contact(self, raw_contact):
        contact = Contact.objects.get(id=raw_contact)
        clone = ContactWrapper(**contact.decompose())
        clone.user = self.contact_user
        clone.import_record = self.import_record
        clone.is_primary = True
        clone.raw_contact = contact.raw_contact
        if clone.contact_type == "merged":
            # CSV is the closest analog we have to the options available to
            # all the attributes in a merged contact.
            clone.contact_type = "csv"
        clone.composite_id = Contact.calc_composite_id(clone)
        return clone

    def send_contact_to_backend(self, producer, contact):
        raw_contact, rawless = self.contact_backend_prep(contact)
        contact_record = {
            'user_id': self.contact_user.id,
            'contact_type': contact.contact_type,
            'contact_id': contact.contact_id,
            'rawless': rawless,
            'value': raw_contact,
            'txid': self.import_transaction_id,
        }
        result = producer.send(settings.BACKEND_CONTACT_TOPIC, contact_record)
        return result

    def contact_backend_prep(self, contact):
        """Generate a record Kafka can process."""
        if isinstance(contact, ContactWrapper):
            contact = contact.contact
        raw_contact = contact.raw_contact
        if raw_contact:
            return raw_contact, False

        value = {contact.kafka_key(): contact.format_for_kafka()}

        # Any Special Fields that define a 'format_for_kafka' method will
        # be processed and included in the "raw" record.
        for relation in Contact.SPECIAL_FIELDS:
            related_field = getattr(Contact, relation).field
            if isinstance(related_field, models.ForeignKey):
                # Many-to-one
                related_model = related_field.model
            else:
                # Many-to-Many
                related_model = related_field.target_field.model
            if not hasattr(related_model, "format_for_kafka"):
                continue

            values = [v.format_for_kafka()
                      for v in getattr(contact, relation).all()]
            if values:
                value[related_model.kafka_key()] = values
        return value, True
