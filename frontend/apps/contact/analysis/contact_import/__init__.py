from .cloning import CloningImport
from .cloudsponge import CloudSpongeImport
from .csv import CSVImport
from .housefile import HouseFileImport
from .mobile import UnifiedMobileImport
from .social_auth import (GMailImport, LinkedInImport, FacebookImport,
                          TwitterImport)
from .nationbuilder import NationBuilderImport

SOURCE_MAP = {
    "google-oauth2": GMailImport,
    "linkedin-oauth2": LinkedInImport,
    "facebook": FacebookImport,
    "twitter": TwitterImport,
    "outlook": CloudSpongeImport,
    "addressbook": CloudSpongeImport,
    "csv": CSVImport,
    "iphone": UnifiedMobileImport,
    "android": UnifiedMobileImport,
    "cloned": CloningImport,
    "nationbuilder": NationBuilderImport,
    "housefile": HouseFileImport
}


def run_import(user, account, source, *args, **kwargs):
    from frontend.apps.authorize.models import RevUpUser
    from frontend.apps.campaign.models import Account

    if not isinstance(user, RevUpUser):
        user = RevUpUser.objects.get(pk=user)
    if account and not isinstance(account, Account):
        account = Account.objects.get(id=account)

    source = source.lower()

    try:
        Importer = SOURCE_MAP[source]
    except KeyError:
        raise ValueError("Source '{}' is not supported".format(source))

    kwargs["account"] = account
    importer = Importer(user, source, *args, **kwargs)
    return importer.execute()
