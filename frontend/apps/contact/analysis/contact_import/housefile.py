from django.conf import settings
from logging import getLogger

from frontend.apps.campaign.models.base import AccountProfile
from frontend.apps.contact.analysis.contact_import.base import \
    ContactImportBase
from frontend.apps.contact.models import (
    Address, EmailAddress, ExternalId, Organization, PhoneNumber)
from frontend.libs.utils.address_utils import AddressParser


LOGGER = getLogger(__name__)


class HouseFileImport(ContactImportBase):
    def __init__(self, user, source, collection_name, *args, **kwargs):
        super().__init__(user, source, *args, **kwargs)
        self.collection_name = collection_name
        self.account = kwargs.get('account')
        self.record_label = self.get_record_label()

    def get_record_label(self):
        """Returns a label for the ImportRecord based on the type of the
        account
        """
        account_type = self.account.account_profile.account_type
        if account_type == AccountProfile.AccountType.POLITICAL:
            record_label = 'House File'
        elif account_type == AccountProfile.AccountType.NONPROFIT:
            record_label = 'Source Data File'
        else:
            record_label = 'Client Data'
        return record_label

    def generate_import_record(self):
        """Generates a record in ImportRecord for each import on a housefile"""
        record = super().generate_import_record()
        record.label = self.record_label
        record.uid = self.collection_name
        return record

    @classmethod
    def contactset_deleteable(self):
        """Returns False as housefile contact-sets should not be deletable"""
        return False

    def get_raw_contacts(self):
        """Returns the records from the mongo collection"""
        collection = settings.MONGO_DB[self.collection_name]
        return collection.find()

    def _build_contact_kwargs(self, raw_contact):
        """This method looks for info in raw_contact and builds a dictionary
        with the following keys: 'contact_id', 'first_name',
        'additional_name', 'last_name', 'name_prefix', 'name_suffix',
        'addresses', 'email_addresses', 'phone_numbers', 'organizations',
        'alma_maters', 'locations', 'image_urls', 'external_profiles',
        'external_ids'
        """
        first_name = raw_contact.get('parsed_given_name', '')
        last_name = raw_contact.get('parsed_last_name', '')

        first_name_lower = self._clean_and_lowercase_name(first_name)
        last_name_lower = self._clean_and_lowercase_name(last_name)
        if not (first_name_lower and last_name_lower):
            # Contacts are required to have at least first and last name
            return None

        contact_id = raw_contact.get('_id', '')
        additional_name = raw_contact.get('parsed_middle_name', '')
        addresses = self._build_addresses(raw_contact.get('n_address', None))
        email_addresses = self._build_emails(
            raw_contact.get('email_addresses', None))
        phone_numbers = self._build_phones(
            raw_contact.get('phone_numbers', None))
        organizations = self._build_organizations(
            raw_contact.get('employer', None), raw_contact.get('occupation', None))
        external_ids = self._build_external_ids(
            raw_contact.get('original_id', None))

        _contact = dict(
            contact_id=contact_id,
            first_name=first_name,
            additional_name=additional_name,
            last_name=last_name,
            first_name_lower=first_name_lower,
            last_name_lower=last_name_lower,
            email_addresses=email_addresses,
            addresses=addresses,
            phone_numbers=phone_numbers,
            organizations=organizations,
            external_ids=external_ids,
            raw_contact=raw_contact
        )
        return _contact

    def _build_addresses(self, raw_addresses):
        """Builds a list of Address objects from the raw addresses"""
        addresses = []

        if not raw_addresses:
            return addresses

        for addr in raw_addresses:
            # Parse the adddress using AddressParser
            ap = AddressParser(addr)
            house_name = ap.addr_dict.get('BuildingName', '') or \
                         ap.addr_dict.get('LandmarkName', '')
            street = ap.get_full_street()
            po_box = ap.addr_dict.get('USPSBoxType', '') or \
                     ap.addr_dict.get('USPSBoxID', '')
            city = ap.addr_dict.get('PlaceName', '')
            region = ap.addr_dict.get('StateName', '')
            post_code = ap.addr_dict.get('ZipCode', '')

            # Check if any of the address fields exist in the parsed address
            if not any((house_name, street, po_box, city, region, post_code,)):
                continue

            # Build the address using what is returned by AddressParser
            _address = Address.build(house_name=house_name,
                                     street=street,
                                     po_box=po_box,
                                     city=city,
                                     region=region,
                                     post_code=post_code)
            addresses.append(_address)
        return addresses

    def _build_phones(self, raw_phone_numbers):
        """Builds a list of PhoneNumber objects"""
        phone_numbers = []

        if not raw_phone_numbers:
            return phone_numbers

        for pn in raw_phone_numbers:
            if not pn:
                continue
            _phone_number = PhoneNumber(number=pn)
            phone_numbers.append(_phone_number)
        return phone_numbers

    def _build_emails(self, raw_email_addresses):
        """Builds a list of EmailAddress objects"""
        email_addresses = []

        if not raw_email_addresses:
            return email_addresses

        for em in raw_email_addresses:
            if not em:
                continue
            _email = EmailAddress(address=em)
            email_addresses.append(_email)
        return email_addresses

    def _build_organizations(self, raw_employers, raw_occupations):
        """Builds a list of Organization objects"""
        organizations = []

        if not raw_employers:
            return organizations

        # Verify that the length of list of employers and list of occupations
        # is the same. If not, create a dummy list of empty strings
        if len(raw_employers) != len(raw_occupations):
            raw_occupations = [''] * len(raw_employers)

        for index, emp_name in enumerate(raw_employers):
            occptn = raw_occupations[index]
            if not (emp_name and occptn):
                continue
            _org = Organization(name=emp_name, title=occptn)
            organizations.append(_org)
        return organizations

    def _build_external_ids(self, raw_external_id):
        """Builds a list of ExternalId objects"""
        external_ids = []
        if raw_external_id:
            _ext_id = ExternalId(
                source=self.record_label,
                value=raw_external_id)
            external_ids.append(_ext_id)
        return external_ids
