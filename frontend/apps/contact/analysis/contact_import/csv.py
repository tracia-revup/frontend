import csv
from io import StringIO

from frontend.apps.contact.models import (
    Address, EmailAddress, PhoneNumber, AlmaMater, Organization,
    ContactFileUpload, ExternalId)
from frontend.libs.utils.csv_utils import clean_csv_bytes
from .base import ContactFileImport


class CSVImport(ContactFileImport):
    def __init__(self, *args, **kwargs):
        super(CSVImport, self).__init__(*args, **kwargs)
        self.header_map = None

    def _get_extra_filters_for_existing(self):
        return dict(import_record__label=self.import_record.label)

    def get_raw_contacts(self):
        cfu = ContactFileUpload.objects.get(id=self.contact_file_upload_id)
        csv_file_bytes = cfu.data_file.read()
        clean_bytes = clean_csv_bytes(csv_file_bytes)
        csv_file_content = clean_bytes.decode('utf-8')
        csv_reader = csv.reader(StringIO(csv_file_content))
        header = next(csv_reader)
        self.header_map = {}
        for idx, name in enumerate(header):
            self.header_map.setdefault(
                name.lower().strip(), []).append(idx)
        contacts = list(csv_reader)
        self.total_results = len(contacts)
        return contacts

    def rdg(self, raw, field):
        """Raw-data-get: Get and clean a contact value."""
        try:
            for idx in self.header_map[field]:
                value = (raw[idx] or "").strip()
                if value:
                    return value
        except (IndexError, KeyError):
            pass
        return ""

    def _build_raw_contact_dict(self, raw_contact):
        contact_dict = {key: self.rdg(raw_contact, key)
                        for key in self.header_map.keys()}
        return contact_dict

    def _build_contact_kwargs(self, raw_contact, *args, **kwargs):
        first_name = self.rdg(raw_contact, "first name")
        first_name_lower = self._clean_and_lowercase_name(first_name)
        additional_name = self.rdg(raw_contact, "middle name")
        last_name = self.rdg(raw_contact, "last name")
        last_name_lower = self._clean_and_lowercase_name(last_name)
        if not (first_name_lower and last_name_lower):
            # Contacts are required to have at least first and last name
            return None

        # Collect the occupation info, if available
        occupation = self.rdg(raw_contact, "occupation")
        employer = self.rdg(raw_contact, "employer")
        department = self.rdg(raw_contact, "department")
        if occupation or employer or department:
            organizations = [
                Organization(
                    name=employer, department=department, title=occupation
                )]
        else:
            organizations = []

        contact_dict = self._build_raw_contact_dict(raw_contact)

        return dict(
            first_name=first_name,
            first_name_lower=first_name_lower,
            last_name=last_name,
            last_name_lower=last_name_lower,
            additional_name=additional_name,
            name_prefix=self.rdg(raw_contact, "name prefix"),
            name_suffix=self.rdg(raw_contact, "name suffix"),
            organizations=organizations,
            addresses=self._build_addresses(raw_contact),
            phone_numbers=self._build_phones(raw_contact),
            email_addresses=self._build_emails(raw_contact),
            alma_maters=self._build_alma_maters(raw_contact),
            external_ids=self._build_external_ids(raw_contact),
            contact_id=self.hash_raw_contact(contact_dict),
            # Store this as the dict form, so the headers are preserved
            raw_contact=contact_dict)

    def _build_addresses(self, raw_contact):
        addresses = []
        header_strings = [
            "address{} street", "address{} street2", "address{} po box",
            "address{} city", "address{} state", "address{} postal code",
            "address{} country", "address{} type"
        ]
        data = self._get_columns(raw_contact, header_strings)
        for address in data:
            address_ = Address.build(
                street="; ".join(
                    [a for a in (address.get("address street", ""),
                                 address.get("address street2", "")) if a]),
                po_box=address.get("address po box", ""),
                city=address.get("address city", ""),
                region=address.get("address state", ""),
                post_code=address.get("address postal code", ""),
                country=address.get("address country", ""),
                address_type=address.get("address type", "")
            )
            addresses.append(address_)
        return addresses

    def _build_phones(self, raw_contact):
        phones = []
        header_strings = ["phone{} number", "phone{} type"]
        data = self._get_columns(raw_contact, header_strings)
        for phone in data:
            if phone.get("phone number"):
                phones.append(PhoneNumber(
                    label=phone.get("phone type", ""),
                    number=phone.get("phone number")
                ))
        return phones

    def _build_external_ids(self, raw_contact):
        """Build the external ID"""
        external_ids = []
        header_strings = ["contact id{}"]
        data = self._get_columns(raw_contact, header_strings)
        for external_id in data:
            if external_id.get("contact id"):
                external_ids.append(ExternalId(
                    value=external_id.get("contact id"),
                    source="csv"
                ))
        return external_ids

    def _build_emails(self, raw_contact):
        emails = []
        header_strings = ["email{} address", "email{} type"]
        data = self._get_columns(raw_contact, header_strings)
        for email in data:
            if email.get("email address"):
                emails.append(EmailAddress(
                    address=email.get("email address"),
                    label=email.get("email type", "")
                ))
        return emails

    def _build_alma_maters(self, raw_contact):
        alma_maters = []
        header_strings = ["alma mater{} name", "alma mater{} major",
                          "alma mater{} degree"]
        data = self._get_columns(raw_contact, header_strings)
        for am in data:
            alma_maters.append(AlmaMater(
                name=am.get("alma mater name", ""),
                major=am.get("alma mater major", ""),
                degree=am.get("alma mater degree", "")
            ))
        return alma_maters

    def _get_columns(self, raw_contact, header_strings):
        """Extract the values from the given headers into a list of data
            groups.

        E.g.:
        address street, address city, address2 street, address2 city
        [{address street: <val>, address city: <val>},
         {address street: <val>, address city: <val>}]
        """
        header_set = []
        index = 0
        miss_count = 0
        while True:
            values = {}
            for header in header_strings:
                header = header.lower()
                f_header = header.format(index if index > 0 else "")
                value = self.rdg(raw_contact, f_header)
                if value:
                    # Assign the value with the header without the number
                    # "address2 street" --> "address street"
                    values[header.format("")] = value

            index += 1

            # We will keep accepting more entries as long as there are some.
            # Once the entries stop for a few in a row, we stop looking.
            # E.g. address, address2, address4, <break>
            if not values:
                miss_count += 1
                if miss_count > 2:
                    break
            else:
                header_set.append(values)
        return header_set

    def _update_if_exists(self, contact, may_create_new=True):
        """Override the base _update_if_exists method

        Since we now match csv contacts via a hash on the raw record, if the
        contact record already exists in the database we can be sure it is
        identical to the record we just imported, and an update is not
        required.
        """
        is_new = True
        assert contact.contact_id
        # Check if this contact is already in the DB.
        # If the contact comes from different import files, we are not
        # going to call them a match, even if they are identical. The primary
        # reason for this is we want contacts from different CSVs to still be
        # recognized as having come from each individual CSV.
        extra_filters = self._get_extra_filters_for_existing()
        existing = self._get_existing(contact, extra_filters=extra_filters)
        if existing:
            is_new = False
            contact = existing
        elif may_create_new:
            contact.save()
        return contact, is_new
