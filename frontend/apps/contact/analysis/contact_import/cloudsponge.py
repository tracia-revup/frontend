
from frontend.apps.contact.models import (Contact, Address, EmailAddress,
                                          PhoneNumber)
from frontend.libs.third_party.cloudsponge.api import CloudSponge
from frontend.libs.utils.general_utils import deep_hash
from .base import ContactImportBase
from .utils import ContactWrapper


class CloudSpongeImport(ContactImportBase):
    label_translations = {
        "addressbook": "Address Book"
    }

    def __init__(self, user, source, import_id, *args, **kwargs):
        super(CloudSpongeImport, self).__init__(user, source, *args, **kwargs)
        self.import_id = import_id

    def _get_existing(self, contact):
        # Get any previously-imported existing contact record having the
        # same external contact id
        if contact.contact_id:

            existing = ContactWrapper.from_iterator(
                Contact.objects.filter(user=contact.user,
                                       contact_id=contact.contact_id))
        else:
            existing = ContactWrapper.from_iterator(
                Contact.objects.filter(
                    user=contact.user,
                    first_name_lower=contact.first_name_lower,
                    last_name_lower=contact.last_name_lower,
                    contact_type=self.source)
            )
        for possible_match in existing:
            if Contact.compare(contact, possible_match):
                return possible_match
        return None

    def generate_import_record(self):
        record = super(CloudSpongeImport, self).generate_import_record()
        record.uid = self.source.lower()
        record.label = self.label_translations.get(self.source,
                                                   self.source.title())
        return record

    def get_raw_contacts(self):
        contacts = []
        for cs_contact in CloudSponge().import_contacts(self.import_id,
                                                        user_id=self.user.id):
            # Contacts without names are useless to us.
            if cs_contact.first_name and cs_contact.last_name:
                contacts.append(cs_contact)

        self.total_results = len(contacts)
        return contacts

    def _build_contact_kwargs(self, raw_contact):
        contact_id = getattr(raw_contact, "contact_id", None)
        if not contact_id:
            contact_id = deep_hash(raw_contact.json)

        emails = [EmailAddress(address=e.address, primary=e.primary,
                               label=e.type)
                  for e in raw_contact.emails]

        phone_numbers = [PhoneNumber(number=p.number, label=p.type)
                         for p in raw_contact.phones]
        addresses = [Address.build(street=a.street or '',
                                   city=a.city or '',
                                   region=a.region or '',
                                   post_code=a.postal_code or '',
                                   country=a.country or '',
                                   address_type=a.type or '')
                     for a in raw_contact.addresses
                     if any([a.street, a.city, a.region, a.postal_code,
                             a.country])]
        return dict(
            first_name=raw_contact.first_name,
            last_name=raw_contact.last_name,
            contact_id=contact_id,
            last_name_lower=self._clean_and_lowercase_name(
                raw_contact.last_name),
            first_name_lower=self._clean_and_lowercase_name(
                raw_contact.first_name),
            email_addresses=emails,
            phone_numbers=phone_numbers,
            addresses=addresses,
            raw_contact=raw_contact.json
        )
