"""
The contact_import module provides functions for importing contacts from other
services (Gmail, LinkedIn, NationBuilder, etc.)
"""
import logging
import time

from django.conf import settings
from gdata.contacts.client import ContactsClient, ContactsQuery
from gdata.gauth import OAuth2Token
from linkedin import linkedin
from lxml import etree
import nameparser
import requests
from social_django.utils import load_strategy

from frontend.apps.contact.models import (
    Address, EmailAddress, Organization, PhoneNumber,
    ImageUrl, ExternalProfile, ImportSources)
from frontend.apps.locations.models import LinkedInLocation
from frontend.libs.utils.general_utils import clean_get_string
from .base import ContactImportBase


LOGGER = logging.getLogger(__name__)


class SocialAuthImport(ContactImportBase):
    def __init__(self, user, source, social, *args, **kwargs):
        super(SocialAuthImport, self).__init__(user, source, *args, **kwargs)
        self.social = social

    def generate_import_record(self):
        record = super(SocialAuthImport, self).generate_import_record()
        record.uid = self.social.uid
        record.label = self.social.uid
        return record


class GMailImport(SocialAuthImport):
    def __init__(self, *args, **kwargs):
        super(GMailImport, self).__init__(*args, **kwargs)
        self.namespaces = {'a': 'http://www.w3.org/2005/Atom',
                           'o': 'http://a9.com/-/spec/opensearchrss/1.0/',
                           'g': 'http://schemas.google.com/g/2005'}
        self.parallel_process = False

    def get_raw_contacts(self):
        strategy = load_strategy()
        self.social.refresh_token(strategy)

        # Create OAuth2Token instance to authorize the client
        credentials = OAuth2Token(
            client_id=settings.SOCIAL_AUTH_GOOGLE_OAUTH2_KEY,
            client_secret=settings.SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET,
            scope=' '.join(settings.SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE),
            user_agent='foobar',
            access_token=self.social.extra_data['access_token'],
            refresh_token=self.social.extra_data.get('refresh_token')
        )
        # Create contacts client and authorize it with OAuth2 token
        client = ContactsClient(source='<var>API Project</var>')
        credentials.authorize(client)

        # Now you should be able to call any API in client
        query = ContactsQuery()

        query.max_results = 100
        total = query.max_results
        start_index = 1
        conn_attempts = settings.SOCIAL_AUTH_CONN_RETRIES + 1

        results = []
        while start_index <= total:
            try:
                query.start_index = start_index
                feed = client.GetContacts(q=query)
                xml = etree.fromstring(feed.ToString())

                total = int(xml.findtext("./o:totalResults",
                                         namespaces=self.namespaces,
                                         default=0))
                items_per_page = int(xml.findtext("./o:itemsPerPage",
                                                  namespaces=self.namespaces,
                                                  default=0))
                start_index += items_per_page
                LOGGER.info("Importing Gmail contacts - Total: {} "
                            "Start: {} Items/page: {}".format(
                                total, start_index, items_per_page))

                for entry in xml.xpath("/a:feed/a:entry[g:name]",
                                       namespaces=self.namespaces):
                    results.append(entry)

            except requests.exceptions.Timeout:
                time.sleep(settings.SOCIAL_AUTH_CONN_TIMEGAP)
                conn_attempts = conn_attempts - 1
                if conn_attempts <= 0:
                    raise Exception("exhausted retries yet timeout remains")

        self.total_results = len(results)
        return results

    def _parallel_process_contacts(self, contacts):
        # Serialize raw contacts to xml as etree structures are not
        # pickle-able, and set the attribute `parallel_process` to True so
        # `build_contact` knows to deserialize the raw contact.
        self.parallel_process = True
        contacts = (etree.tostring(contact, encoding="unicode")
                    for contact in contacts)
        return super(GMailImport, self)._parallel_process_contacts(contacts)

    def _build_contact_kwargs(self, raw_contact):
        raw_contact_string = None
        if self.parallel_process:
            # If `parallel_process` is true, then the raw contact is an xml
            # string, not an etree structure, and we need to parse it again.
            raw_contact_string = raw_contact
            raw_contact = etree.fromstring(raw_contact_string)

        contact_id = raw_contact.findtext("a:id", namespaces=self.namespaces)
        first_name = raw_contact.findtext("g:name/g:givenName",
                                          namespaces=self.namespaces,
                                          default="")
        additional_name = raw_contact.findtext("g:name/g:additionalName",
                                               namespaces=self.namespaces,
                                               default="")
        name_prefix = raw_contact.findtext("g:name/g:namePrefix",
                                           namespaces=self.namespaces,
                                           default="")
        name_suffix = raw_contact.findtext("g:name/g:nameSuffix",
                                           namespaces=self.namespaces,
                                           default="")
        last_name = raw_contact.findtext("g:name/g:familyName",
                                         namespaces=self.namespaces,
                                         default="")
        email_addresses = []
        for email_address in raw_contact.xpath("g:email",
                                               namespaces=self.namespaces):
            address = email_address.get("address", default="")
            display_name = email_address.get("displayName", default="")
            label = email_address.get("label", default="")
            primary = bool(email_address.get("primary", default=""))
            email_addresses.append(
                EmailAddress(address=address,
                             display_name=display_name,
                             label=label,
                             primary=primary))
        organizations = []
        for org in raw_contact.xpath("g:organization",
                                     namespaces=self.namespaces):
            label = org.get("label", default="")
            department = org.findtext("g:orgDepartment",
                                      namespaces=self.namespaces, default="")
            job_description = org.findtext("g:orgJobDescription",
                                           namespaces=self.namespaces,
                                           default="")
            name = org.findtext("g:orgName", namespaces=self.namespaces,
                                default="")
            symbol = org.findtext("g:orgSymbol", namespaces=self.namespaces,
                                  default="")
            title = org.findtext("g:orgTitle", namespaces=self.namespaces,
                                 default="")
            primary = bool(org.get("primary", default=""))
            organizations.append(
                Organization(label=label,
                             department=department,
                             job_description=job_description,
                             name=name, symbol=symbol,
                             title=title, primary=primary))
        phone_numbers = []
        for phone_number in raw_contact.xpath("g:phoneNumber",
                                              namespaces=self.namespaces):
            label = phone_number.get("label", default="")
            primary = bool(phone_number.get("primary", default=""))
            number = phone_number.findtext(".", default="")
            phone_numbers.append(
                PhoneNumber(label=label, primary=primary,
                            number=number))
        addresses = []
        for address in raw_contact.xpath("g:structuredPostalAddress",
                                         namespaces=self.namespaces):
            address_type = address.get("rel", default="")
            label = address.get("label", default="")
            primary = bool(address.get("primary", default=""))
            agent = address.findtext("g:agent", namespaces=self.namespaces,
                                     default="")
            house_name = address.findtext("g:housename",
                                          namespaces=self.namespaces,
                                          default="")
            street = address.findtext("g:street", namespaces=self.namespaces,
                                      default="")
            po_box = address.findtext("g:pobox", namespaces=self.namespaces,
                                      default="")
            neighborhood = address.findtext("g:neighborhood",
                                            namespaces=self.namespaces,
                                            default="")
            city = address.findtext("g:city", namespaces=self.namespaces,
                                    default="")
            subregion = address.findtext("g:subregion", default="",
                                         namespaces=self.namespaces)
            region = address.findtext("g:region", namespaces=self.namespaces,
                                      default="")
            post_code = address.findtext("g:postcode", default="",
                                         namespaces=self.namespaces)
            country = address.findtext("g:country",
                                       namespaces=self.namespaces, default="")
            addresses.append(
                Address.build(address_type=address_type, label=label,
                        primary=primary, agent=agent,
                        house_name=house_name, street=street,
                        po_box=po_box, neighborhood=neighborhood,
                        city=city, subregion=subregion,
                        region=region, post_code=post_code,
                        country=country))
        return dict(
            last_name=last_name,
            first_name=first_name,
            contact_id=contact_id,
            last_name_lower=self._clean_and_lowercase_name(last_name),
            first_name_lower=self._clean_and_lowercase_name(first_name),
            additional_name=additional_name,
            name_prefix=name_prefix,
            name_suffix=name_suffix,
            email_addresses=email_addresses,
            organizations=organizations,
            phone_numbers=phone_numbers,
            addresses=addresses,
            raw_contact=(raw_contact_string or
                         etree.tostring(raw_contact, encoding="unicode")))


class LinkedInImport(SocialAuthImport):
    def __init__(self, *args, **kwargs):
        super(LinkedInImport, self).__init__(*args, **kwargs)
        auth = linkedin.LinkedInAuthentication(
            settings.SOCIAL_AUTH_LINKEDIN_OAUTH2_KEY,
            settings.SOCIAL_AUTH_LINKEDIN_OAUTH2_SECRET,
            settings.SOCIAL_AUTH_NEW_ASSOCIATION_REDIRECT_URL,
            settings.SOCIAL_AUTH_LINKEDIN_OAUTH2_SCOPE)
        auth.token = linkedin.AccessToken(
            self.social.extra_data['access_token'], '')
        self.linkedin_client = linkedin.LinkedInApplication(auth)

    def generate_import_record(self):
        record = super(LinkedInImport, self).generate_import_record()
        first = self.social.extra_data.get("first_name", "")
        last = self.social.extra_data.get("last_name", "")
        if first or last:
            record.label = " ".join([first, last])
        return record

    def _get_own_profile(self):
        """
            Return the user's own LinkedIn profile.
        """
        # get own profile
        profile = self.linkedin_client.get_profile(
            selectors=['id', 'first-name',
                       'last-name',
                       'location',
                       'distance'])
        LOGGER.debug("Got own LinkedIn profile: {}".format(
            profile))
        if profile['distance'] != 0:
            LOGGER.error("Got nonzero distance when getting own LinkedIn "
                         "profile: {}".format(profile))
        return profile

    def _build_contact_kwargs(self, raw_contact):
        """Parses a LinkedIn profile and returns a Contact object."""
        contact_id = raw_contact['id']
        first_name = raw_contact['firstName']
        last_name = raw_contact['lastName']
        locations = []
        image_urls = []
        external_profiles = []
        positions = raw_contact.get('positions', None)
        organizations = []

        if raw_contact['location']['name'] is not None:
            location, _ = LinkedInLocation.objects.get_or_create(
                    name=raw_contact['location']['name'])
            locations.append(location)
        else:
            LOGGER.debug("No location name specified in profile")

        image_url = raw_contact.get("pictureUrl")
        if image_url:
            image_urls.append(
                ImageUrl(url=image_url, source=ImportSources.LINKEDIN))

        # external_profile = raw_contact.get("publicProfileUrl")
        external_profile = raw_contact.get("siteStandardProfileRequest",
                                           {}).get("url")
        if external_profile:
            external_profiles.append(
                ExternalProfile(url=external_profile,
                                source=ImportSources.LINKEDIN))
        if positions:
            organizations = self.build_organizations(positions)

        return dict(
            last_name=last_name,
            first_name=first_name,
            contact_id=contact_id,
            last_name_lower=self._clean_and_lowercase_name(last_name),
            first_name_lower=self._clean_and_lowercase_name(first_name),
            locations=locations,
            image_urls=image_urls,
            external_profiles=external_profiles,
            organizations=organizations
        )

    def build_organizations(self, positions):
        """Returns a list of Organization objects built from the raw_contact's
        linkedin profile
        """
        organizations = []
        _positions = positions.get('values', [])
        for each_position in _positions:
            # Available fields in a linkedin position are : id, title,
            # summary, start-date, end-date, is-current, company
            name = ''
            job_description = clean_get_string(each_position, 'summary', None)
            title = clean_get_string(each_position, 'title', '')
            primary = each_position.get('is-current', False)
            company = each_position.get('company', None)
            if company:
                # Avalilable fields in a company are : id, name, type,
                # industry, ticker
                name = clean_get_string(company, 'name', '')

            # If name and title are empty, ignore the position and continue
            if not (name or title):
                continue
            organizations.append(Organization(name=name, title=title,
                                              job_description=job_description,
                                              primary=primary))
        return organizations

    def get_raw_contacts(self):
        """Import LinkedIn contacts for a user."""
        # TODO: Re-introduce full contact import, if possible
        max_results = 500
        total = max_results
        start_index = 0
        private_count = 0
        imported_count = 0
        conn_attempts = settings.SOCIAL_AUTH_CONN_RETRIES + 1

        while start_index < total:
            try:
                result = self.linkedin_client.get_connections(
                    selectors=['id', 'first-name', 'last-name',
                               'formatted-name', 'maiden-name', 'headline',
                               'positions',
                               'industry', 'location', 'summary',
                               'picture-url', 'num-connections',
                               'num-connections-capped',
                               'site-standard-profile-request'],
                             # 'public-profile-url'],   API field currently broken
                    params={'start': start_index})

                total = result['_total']
                if not self.total_results:
                    self.total_results = total

                if '_count' in result:
                    count = result['_count']
                    LOGGER.info("LinkedIn response: Total: {} Count: {} Start: {}"
                                .format(result['_total'], result['_count'],
                                        result['_start']))
                else:
                    LOGGER.debug("No count key found in LinkedIn result")
                    LOGGER.debug("LinkedIn response: %s", result)
                    count = max_results
                LOGGER.debug(
                    "Fetching LinkedIn connections from %s of %s total "
                    "contact", start_index, total)

                start_index += count
                if 'values' not in result:
                    LOGGER.debug("No values key found in LinkedIn result")
                    LOGGER.debug("Result: %s", result)
                else:
                    for connection in result['values']:
                        if connection['id'] == 'private':
                            private_count += 1
                        else:
                            imported_count += 1
                            yield connection

            except requests.exceptions.Timeout:
                time.sleep(settings.SOCIAL_AUTH_CONN_TIMEGAP)
                conn_attempts -= 1
                if conn_attempts <= 0:
                    raise Exception("exhausted retries yet timeout remains")

        LOGGER.info("Fetched {} of {} connections, skipped {} private".format(
            imported_count, total, private_count))

        # Fundraiser LinkedIn imports should also import own profile
        own_profile = self._get_own_profile()
        if own_profile:
            yield own_profile


class FacebookImport(SocialAuthImport):
    """This is mostly just a stub, and is not tested."""
    def get_raw_contacts(self):
        # No retry implemented yet, add if completing this section
        tokens = [token for auth in self.social for token in auth.tokens]
        LOGGER.debug("Facebook tokens %s", tokens)
        token = tokens[0]
        graph = facebook.GraphAPI(token)
        profile = graph.get_object("me")
        LOGGER.debug("Facebook profile %s", profile)

        friends = graph.get_connections("me", "friends")
        LOGGER.debug("Facebook friends %s", friends)

        if 'data' in friends:
            self.total_results = len(friends['data'])
            for friend in friends['data']:
                yield friend
        else:
            LOGGER.warning("Facebook friends: no data for user %s!",
                           self.user)

    def _build_contact_kwargs(self, raw_contact):
        """
            Construct a data model Contact from a facebook friend
        """
        LOGGER.debug("Facebook friend %s", raw_contact)
        parsed_name = nameparser.HumanName(raw_contact['name'])
        parsed_name.capitalize()
        return dict(
            first_name=parsed_name.first,
            last_name=parsed_name.last,
            contact_id=raw_contact['id'],
            last_name_lower=self._clean_and_lowercase_name(parsed_name.last),
            first_name_lower=self._clean_and_lowercase_name(parsed_name.first))


class TwitterImport(SocialAuthImport):
    """This is mostly just a stub, and is not tested."""
    def _build_contact_kwargs(self, raw_contact):
        """
        Construct a data model Contact from a twitter friend or follower.
        """
        LOGGER.debug("twitter contact %s", raw_contact)
        parsed_name = nameparser.HumanName(raw_contact.name)
        parsed_name.capitalize()
        return dict(
            contact_id=str(raw_contact.id),
            last_name=parsed_name.last,
            first_name=parsed_name.first,
            last_name_lower=self._clean_and_lowercase_name(parsed_name.last),
            first_name_lower=self._clean_and_lowercase_name(parsed_name.first),
            additional_name=raw_contact.screen_name)

    def get_raw_contacts(self):
        # No retry implemented yet, add if completing this section
        """
        Import twitter contacts for a user.
        """
        tokens = [token for auth in self.social for token in auth.tokens]
        LOGGER.debug("twitter tokens %s", tokens)

        token = tokens[0]
        auth = tweepy.OAuthHandler(
            settings.SOCIAL_AUTH_TWITTER_KEY,
            settings.SOCIAL_AUTH_TWITTER_SECRET)
        auth.set_access_token(token['oauth_token'],
                              token['oauth_token_secret'])
        api = tweepy.API(auth)

        contacts = []
        for tu in list(tweepy.Cursor(api.friends).items()):
            contacts.append(tu)

        for tu in list(tweepy.Cursor(api.followers).items()):
            contacts.append(tu)

        # TODO: Investigate if twitter provides a 'count' field
        self.total_results = len(contacts)
        LOGGER.info("Fetched %s friends or followers", self.total_results)
        return contacts
