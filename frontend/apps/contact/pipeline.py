
from frontend.apps.contact.task_utils import start_contact_import
from frontend.apps.contact.utils import check_account_level_import_social
from frontend.apps.external_app_integration.models import ExternalApp
from frontend.libs.utils.string_utils import str_to_bool
from frontend.libs.utils.tracker_utils import action_send
from frontend.libs.utils.task_utils import (
    WaffleFlagsMixin, CONTACT_IMPORT_FLAGS, ANALYSIS_FLAGS)


class ImportContacts(WaffleFlagsMixin):
    flag_names = CONTACT_IMPORT_FLAGS + ANALYSIS_FLAGS

    @classmethod
    def import_contacts(cls, request, account, social, user,
                        backend_name):
        """This exists entirely so we can use the WaffleFlagsMixin."""
        # We only want to import on new connections, and never for regular
        # signup
        if backend_name != 'email':
            flags = cls._prepare_flags(request)
            start_contact_import(user, social, account, flags)
            action_send(request, user, verb="imported contacts",
                        source=social.provider)


def import_contacts(strategy, backend, request, details, *args, **kwargs):
    """Queue a task to import the contacts for this new account

    We only want to import contacts from OAuth linked accounts,
    and we only want to import contacts one time.
    """
    backend_name = backend.name
    user = kwargs.get('user', None)
    account = check_account_level_import_social(strategy, user)
    if backend.name == 'nationbuilder':
        oauth_token = kwargs.get('response').get('access_token')
        nb_user = account.account_user if account else user
        nation_slug = strategy.session_get('nation_slug')
        ExternalApp.objects.create(external_id=nation_slug,
                                   revup_user=nb_user,
                                   app=ExternalApp.App.NATIONBUILDER,
                                   oauth_token=oauth_token)
        contact_import = str_to_bool(strategy.session_get('import_contacts'))
        if not contact_import:
            return
    social = kwargs.get('social', None)
    request = strategy.request
    ImportContacts.import_contacts(request, account, social, user,
                                   backend_name)
