
class NoChildContacts(Exception):
    pass


class MergeConflict(Exception):
    pass
