from django.conf.urls import url

from frontend.apps.contact.api.views import ContactImportHF
from frontend.apps.contact.views import (
    ContactImportSocial, contact_list, ContactImportCS,
    ContactUploadView, ContactManagerView)


urlpatterns = [
    url(r'^/import/housefile/(?P<account_id>[^/]+)/$',
        ContactImportHF.as_view(), name='contacts_import_hf'),
    url(r'^/import/(?P<social_id>[^/]+)/$', ContactImportSocial.as_view(),
        name='contacts_import'),
    url(r'^/import/cs/(?P<source>[^/]+)/$',
        ContactImportCS.as_view(), name='contacts_import_cs'),
    url(r'^/manager/$', ContactManagerView.as_view(), name="contact_manager"),
    url(r'^/file_upload/$', ContactUploadView.as_view(),
        name="contact_file_upload"),
    url(r'^/$', contact_list, name='contact_list'),
]
