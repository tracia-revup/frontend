from ast import literal_eval
from datetime import datetime
import logging
from io import StringIO
from itertools import chain
import json
import string

from datetime import date, timedelta
from django.conf import settings
from django.contrib.postgres.fields import JSONField
from django.core.files.base import ContentFile
from django.db import models, transaction
from django.db.models import Q
from django.db.models import ExpressionWrapper, BooleanField
from django.dispatch import receiver
from django_extensions.db.models import TimeStampedModel, TitleDescriptionModel
import phonenumbers
from storages.backends.s3boto3 import S3Boto3Storage


from frontend.apps.contact.utils import (PhoneNumberMixin, KafkaMixin, CSVExportMixin)
from frontend.apps.entities.models import Identity, PersonDimension
from frontend.apps.locations.models import State, ZipCode, City, CityAlias
from frontend.libs.mixins import SimpleUnicodeMixin
from frontend.libs.utils.address_utils import AddressParser
from frontend.libs.utils.csv_utils import clean_csv_bytes
from frontend.libs.utils.general_utils import (DictReaderInsensitive,
                                               instance_cache, deep_hash)
from frontend.libs.utils.model_utils import (
    TruncatingCharField, Choices, OwnershipModel, EntityModel,
    ArchiveModelMixin, LockingModelMixin)
from frontend.libs.utils.namelib_utils import namelib
from frontend.libs.utils.string_utils import (similarity, strip_punctuation,
                                              ensure_unicode, ensure_bytes,
                                              scrub_and_lowercase, random_uuid)

LOGGER = logging.getLogger(__name__)


class ImportSources(Choices):
    choices_map = [
        ('FB', "FACEBOOK", 'Facebook'),
        ('GM', "GMAIL", 'Gmail'),
        ('LI', "LINKEDIN", 'LinkedIn'),
        ('TW', "TWITTER", 'Twitter'),
        ('OU', "OUTLOOK", "Outlook"),
        ('AB', "ADDRESS_BOOK", "VCard"),
        ('CV', "CSV", "CSV"),
        ('IP', "IPHONE", "iPhone"),
        ('AD', "ANDROID", "Android"),
        ('NB', "NationBuilder", "Nationbuilder"),
        ('HF', "HOUSEFILE", "HouseFile"),
        ('OT', "OTHER", 'Other'),
    ]
    source_map = {
        "google-oauth2": "GM",
        "linkedin-oauth2": "LI",
        "outlook": "OU",
        "csv": "CV",
        "addressbook": "AB",
        "iphone": "IP",
        "android": "AD",
        "nationbuilder": "NB",
        "housefile": "HF",
    }


def generate_csv_fields(fields, replace, total_count):
    """Replace csv fields with an incrementing value.
    E.g. address city -> address city, address2 city, address3 city
    """
    new_fields = []
    for i in range(1, total_count + 1):
        if i == 1:
            new_fields.extend(fields)
        else:
            for field in fields:
                new_fields.append(
                    field.replace(replace, "{}{}".format(replace, i)))
    return tuple(new_fields)


class Address(ArchiveModelMixin, models.Model, KafkaMixin, SimpleUnicodeMixin):
    contact = models.ForeignKey("Contact", on_delete=models.CASCADE,
                                related_name="addresses")
    address_type = TruncatingCharField(max_length=128, blank=True)
    label = TruncatingCharField(max_length=64, blank=True)
    primary = models.BooleanField(default=False, blank=True)
    agent = TruncatingCharField(max_length=64, blank=True)
    house_name = TruncatingCharField(max_length=128, blank=True)
    street = TruncatingCharField(max_length=256, blank=True)
    po_box = TruncatingCharField(max_length=32, blank=True)
    neighborhood = TruncatingCharField(max_length=64, blank=True)
    city = TruncatingCharField(max_length=64, blank=True)
    subregion = TruncatingCharField(max_length=64, blank=True)
    region = TruncatingCharField(max_length=64, blank=True)
    post_code = TruncatingCharField(max_length=64, blank=True)
    country = TruncatingCharField(max_length=64, blank=True)
    zip_key = models.ForeignKey(to="locations.ZipCode",
                                on_delete=models.CASCADE, null=True)
    city_key = models.ForeignKey(to="locations.City",
                                 on_delete=models.CASCADE, null=True)

    csv_fields = generate_csv_fields(
                 ("Address Street", "Address PO Box", "Address City",
                  "Address State", "Address Postal Code", "Address Country",
                  "Address type"), "Address", 3)

    match_fields = ('contact', 'street', 'po_box', 'neighborhood', 'city',
                    'subregion', 'region', 'post_code', 'country',
                    'zip_key', 'city_key')

    def format_for_kafka(self):
        extracted = self.extract_address()
        return {
            "address": str(self.format_street_address(extracted[0],
                                                      extracted[1])),
            "city": str(extracted[2]),
            "state": str(extracted[3]),
            "zip_code": str(extracted[4])
        }

    def extract_address(self):
        return (
            self.street, self.po_box,
            self.city_key.name if self.city_key_id else self.city,
            self.city_key.state.abbr if self.city_key_id else self.region,
            self.zip_key.zipcode if self.zip_key_id else self.post_code,
            self.country, self.address_type
        )

    @classmethod
    def build(cls, **kwargs):
        save = kwargs.pop('save', False)
        address = cls(**kwargs)
        address.splay_address()
        if save:
            address.save()
        return address

    @classmethod
    def format_for_csv(cls, contact):
        """Iter through all unique addresses for the given contact and
          format for a CSV.
        """
        def generator():
            for address in cls.iter_addresses(contact):
                yield address.extract_address()
        return list(zip(cls.csv_fields, chain(*generator())))

    @classmethod
    def iter_addresses(cls, contact):
        """Iter through all unique addresses for the given contact."""
        seen_addresses = set()
        contacts = contact.get_contacts()
        addresses = Address.objects.select_related(
            "city_key__state", "zip_key").filter(
            contact__in=contacts, archived=None).iterator()

        for address in addresses:
            f_addr = address.format_single_line()
            if f_addr:
                lf_addr = f_addr.lower()
                # Lower-case and hash the formatted address in order to
                # assist in finding duplicates.
                if lf_addr not in seen_addresses:
                    seen_addresses.add(lf_addr)
                    yield address

    def format_street_address(self, street=None, po_box=None):
        street = street or self.street
        po_box = po_box or self.po_box
        addy_parts = []

        if street:
            addy_parts.append(street)

        if po_box:
            # According to the link in the docstring it is correct to include
            # both P.O Box and street address.
            if po_box.isdigit():
                po_box = 'P.O. Box ' + po_box
            # XXX: Should we put an else and try and extract the po box number?
            # Or just pass the po_box through without any further processing?
            addy_parts.append(po_box)
        return ', '.join(filter(None, addy_parts))

    def format_single_line(self):
        """Format an address record into a single line.

        Follow the formatting for addresses here:
            http://howtogetapobox.com/what-is-the-correct-po-box-address-format/
        """
        addy_parts = [self.format_street_address()]
        addy_parts.extend((self.city,
                           ' '.join(filter(None,
                                            (self.region, self.post_code)))))
        return ', '.join(filter(None, addy_parts))

    def build_city_query(self, address_parser):
        """Builds a city query that tries to match all possible city names
        :param address_parser: instance of AddressParser
        :return: a query that tries to match all possible city names
        """
        city_q = Q()
        # similar_city_names has all possible replacements for abbrevations
        # defined in AddressParser._city_name_map
        similar_city_names = {self.city.strip().lower(),
            address_parser.city_name.strip().lower(),
            AddressParser.process_city_name(self.city.strip().lower()),
            AddressParser.process_city_name(
                address_parser.city_name.strip().lower())
        }

        for city_val in similar_city_names:
            if not city_val:
                continue
            city_q |= Q(name__iexact=city_val)
            city_q |= Q(aliases__alias__iexact=city_val)
        return city_q

    def splay_address(self):
        '''Create or update links from address out to zip code and city tables.
        Try to use the provided dictionary first, fall back to usaddress.
        '''
        # Parse the address information for use below.
        astr = "{}, {}, {} {}".format(self.street, self.city,
                                       self.region, self.post_code)
        ap = AddressParser(astr)
        dirty = False

        # Find a ZipCode instance
        # Build the query using the post_code field and the parsed postal code
        zip_q = Q()
        for postal_code in {self.post_code.strip()[:5],
                            ap.post_code.strip()[:5]}:
            if len(postal_code) < 5 or not postal_code.isdigit():
                continue
            zip_q |= Q(zipcode=postal_code)

        # Query for the zipcode
        if zip_q:
            try:
                zip_code = ZipCode.objects.distinct().get(zip_q)
            except (ZipCode.DoesNotExist, ZipCode.MultipleObjectsReturned):
                # If multiple are returned, we can't really trust either.
                zip_code = None

            if zip_code and zip_code != self.zip_key:
                self.zip_key = zip_code
                dirty = True

        # Build a city query
        city_q = self.build_city_query(ap)

        # If we have no city name, there is nothing else we can do.
        if city_q:
            city = None
            # Build a zip-based query. City-Zip will be the most accurate
            if self.zip_key:
                zip_q = city_q & Q(zips=self.zip_key)
                city = City.get_fuzzy_city(zip_q)

            # If we couldn't acquire a city via zip, try building a state query
            if not city:
                state_q = Q()
                for state_val in {self.region.strip().lower(),
                                  ap.state_name.strip().lower()}:
                    if not state_val:
                        continue
                    field = "state__{}__iexact".format(
                        "name" if len(state_val) > 2 else "abbr")
                    state_q |= (city_q & Q(**{field: state_val}))

                if state_q:
                    city = City.get_fuzzy_city(state_q)

            # If city was found either by zip or state, let's update it.
            if city and city != self.city_key:
                self.city_key = city
                dirty = True

        # If we weren't able to find a city, but we do have a zip, let's try
        # to get the city from the zip
        if not self.city_key and self.zip_key:
            cities = list(self.zip_key.city_set.all())
            # If the zip has more than one city, we can't safely pick one
            if len(cities) == 1 and cities[0] != self.city_key:
                self.city_key = cities[0]
                dirty = True
        return dirty


class EmailAddress(ArchiveModelMixin, models.Model, KafkaMixin,
                   SimpleUnicodeMixin):
    contact = models.ForeignKey("Contact", on_delete=models.CASCADE, related_name="email_addresses")
    address = TruncatingCharField(max_length=256)
    display_name = TruncatingCharField(max_length=128, blank=True)
    label = TruncatingCharField(max_length=128, blank=True)
    primary = models.BooleanField(default=False, blank=True)

    csv_fields = generate_csv_fields(
        ('Email Address', 'Email type'), "Email", 3)

    match_fields = ('contact', 'address')

    def format_for_kafka(self):
        return str(self.address)

    def delete(self, using=None, keep_parents=False,
               force=False, replace_with=None):
        from frontend.apps.call_time.models import CallTimeContact
        if not force:
            CallTimeContact.objects.filter(
                primary_email=self).update(primary_email=replace_with)
        return super(EmailAddress, self).delete(
            using=using, keep_parents=keep_parents, force=force)

    @classmethod
    def format_for_csv(cls, contact):
        """Iter through all unique email addresses for the given contact
           and format for a CSV
        """
        return list(zip(cls.csv_fields, chain(*cls.iter_emails(contact))))

    @classmethod
    def iter_emails(cls, contact):
        """Iter through all unique email addresses for the given contact."""
        seen_emails = set()
        contacts = contact.get_contacts()
        for email in EmailAddress.objects.filter(
                contact__in=contacts, archived=None).exclude(
                address='').iterator():
            if email.address:
                l_email = email.address.lower()
                # Lower-case and hash the email in order to
                # assist in finding duplicates.
                if l_email not in seen_emails:
                    seen_emails.add(l_email)
                    yield email.address, email.label


class Organization(ArchiveModelMixin, models.Model, KafkaMixin,
                   SimpleUnicodeMixin):
    contact = models.ForeignKey("Contact", on_delete=models.CASCADE,
                                related_name="organizations")
    label = TruncatingCharField(max_length=128, blank=True)
    department = TruncatingCharField(max_length=128, blank=True)
    job_description = models.TextField(blank=True, null=True)
    name = TruncatingCharField(max_length=256, blank=True)
    symbol = TruncatingCharField(max_length=64, blank=True)
    title = TruncatingCharField(max_length=256, blank=True)
    primary = models.BooleanField(default=False, blank=True)

    csv_fields = ('Occupation', 'Employer', 'Department')
    match_fields = ('contact', 'name')

    def format_for_kafka(self):
        return {
            'occupation': ensure_unicode(self.title),
            'employer': ensure_unicode(self.name)
        }

    @classmethod
    def kafka_key(cls):
        return "Occupation"

    @classmethod
    def format_for_csv(cls, contact):
        """Get the first organization and format it for csv.
        As of now, we only return one organization in the csv.
        """
        try:
            values = next(cls.iter_organizations(contact))
            return list(zip(cls.csv_fields, values))
        except StopIteration:
            return []

    @classmethod
    def iter_organizations(cls, contact):
        """Iter through all unique alma maters for the given contact."""
        seen_orgs = set()
        contacts = contact.get_contacts()
        for org in cls.objects.filter(
                contact__in=contacts, archived=None).exclude(
                department="", title="", name="").iterator():
            values = (org.title, org.name, org.department)
            if values not in seen_orgs:
                seen_orgs.add(values)
                yield values


class AlmaMater(ArchiveModelMixin, models.Model, SimpleUnicodeMixin,
                CSVExportMixin):
    contact = models.ForeignKey("Contact", on_delete=models.CASCADE, related_name="alma_maters")
    name = TruncatingCharField(max_length=256, blank=True)
    major = TruncatingCharField(max_length=256, blank=True)
    degree = TruncatingCharField(max_length=256, blank=True)

    csv_fields = generate_csv_fields(
        ('Alma Mater Name', 'Alma Mater Major', "Alma Mater Degree"),
        "Alma Mater", 3)
    match_fields = ('contact', 'name', 'major', 'degree')
    fields_to_iter = ('name', 'major', 'degree')


class ExternalId(ArchiveModelMixin, models.Model, SimpleUnicodeMixin,
                 CSVExportMixin):
    contact = models.ForeignKey("Contact", on_delete=models.CASCADE,
                                related_name="external_ids")
    source = models.CharField(max_length=256, blank=False)
    value = TruncatingCharField(max_length=512, blank=False)

    match_fields = ('contact', 'source', 'value')
    csv_fields = generate_csv_fields(
        ('External Id Source', 'External Id Value'), "External Id", 5)
    fields_to_iter = ('source', 'value')


class ExternalUrl(models.Model, SimpleUnicodeMixin):
    url = models.URLField(max_length=1024, blank=False)
    source = models.CharField(choices=ImportSources.choices(),
                              max_length=2, blank=False)
    # Only match on source because we only want one of these per source.
    # If we match on URL also, bad links would hang around.
    match_fields = ("source",)

    class Meta:
        abstract = True


class ImageUrl(ExternalUrl):
    contact = models.ForeignKey("Contact", on_delete=models.CASCADE, related_name="image_urls")
    # Some images are uploaded as base64, so we have to store them. This field
    # may be left blank
    image = models.ImageField(blank=True)


class ExternalProfile(ExternalUrl):
    contact = models.ForeignKey("Contact", on_delete=models.CASCADE, related_name="external_profiles")


class PhoneNumber(ArchiveModelMixin, models.Model, KafkaMixin,
                  SimpleUnicodeMixin, PhoneNumberMixin):
    contact = models.ForeignKey("Contact", on_delete=models.CASCADE, related_name="phone_numbers")
    label = TruncatingCharField(max_length=128, blank=True)
    primary = models.BooleanField(default=False, blank=True)
    number = TruncatingCharField(max_length=128)

    csv_fields = generate_csv_fields(
        ('Phone Number', 'Phone type'), "Phone", 3)

    match_fields = ('contact', 'number')

    def format_for_kafka(self):
        return str(self.rfc3966)

    def delete(self, using=None, keep_parents=False,
               force=False, replace_with=None):
        from frontend.apps.call_time.models import CallTimeContact
        if not force:
            CallTimeContact.objects.filter(
                primary_phone=self).update(primary_phone=replace_with)
        return super(PhoneNumber, self).delete(
            using=using, keep_parents=keep_parents, force=force)

    @classmethod
    def format_for_csv(cls, contact):
        """Iter through all unique phone numbers for the given contact
           and format for a CSV.
        """
        return list(zip(cls.csv_fields, chain(*cls.iter_phone_numbers(contact))))

    @classmethod
    def iter_phone_numbers(cls, contact):
        """Iter through all unique phone numbers for the given contact."""
        seen_phones = set()
        contacts = contact.get_contacts()
        for phone in PhoneNumber.objects.filter(
                contact__in=contacts, archived=None).iterator():
            if phone.number:
                try:
                    pn = phone.national
                except phonenumbers.NumberParseException:
                    pass
                else:
                    if pn not in seen_phones:
                        seen_phones.add(pn)
                        yield pn, phone.label


@receiver(models.signals.pre_save, sender=PhoneNumber)
def reformat_number(sender, instance, *args, **kwargs):
    # Save all the numbers in the same format.
    # E.g. u'tel:+1-510-555-5555;ext=1234'
    instance.number = instance.rfc3966


class Relationship(ArchiveModelMixin, TimeStampedModel, SimpleUnicodeMixin):
    contact = models.ForeignKey("Contact", on_delete=models.CASCADE,
                                related_name="relationships")
    relation = TruncatingCharField(max_length=128)
    name = TruncatingCharField(max_length=256)


class DateOfBirth(models.Model, SimpleUnicodeMixin):
    contact = models.OneToOneField("Contact", on_delete=models.CASCADE,
                                   related_name="dob")
    birth_date = models.DateField()
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)


class Gender(TimeStampedModel, SimpleUnicodeMixin):
    class GenderChoices(Choices):
        choices_map = [
            ("M", "MALE", "Male"),
            ("F", "FEMALE", "Female"),
            ("U", "UNKNOWN", "Unknown"),
        ]
    contact = models.OneToOneField("Contact", on_delete=models.CASCADE,
                                   related_name="gender")
    gender_type = models.CharField(choices=GenderChoices.choices(),
                                   max_length=1)


class RawContact(models.Model, SimpleUnicodeMixin):
    """Store the raw contact data for each contact.

    I put this in a separate table because we pass contacts around a lot.
    The bigger the contact is, the worse our performance will be, and this
    isn't a vital piece.

    Note: Typically, this shouldn't be accessed directly. Use the Contact
          property "raw_contact".
    """
    eval_sources = ImportSources.source_map.copy()
    del eval_sources["google-oauth2"]

    contact = models.OneToOneField("Contact", related_name="_raw_contact", on_delete=models.CASCADE)
    # Note: Postgres automatically compresses, so we don't need to do anything.
    raw_contact = models.TextField(help_text="This is the raw contact data")


class Contact(models.Model, KafkaMixin, LockingModelMixin, SimpleUnicodeMixin):
    user = models.ForeignKey('authorize.RevUpUser', on_delete=models.CASCADE)
    modified_by_user = models.DateTimeField(blank=True, null=True)
    # This field is nullable because merged contacts don't get a created
    # timestamp. They are assigned one of their childrens' timestamps
    created = models.DateTimeField(blank=True, default=datetime.now, null=True)
    contact_id = models.CharField(max_length=256,
                                  verbose_name='contact external id')
    contact_type = models.CharField(max_length=128)
    # A composite_id is a hash of the fields user, contact_id, and
    # contact_type. contact_id MUST NOT be empty. composite_ids are calculated
    # using `Contact.calc_composite_id()`
    composite_id = models.CharField(max_length=128)
    merge_version = models.IntegerField(default=0)
    first_name = TruncatingCharField(max_length=128, blank=True)
    additional_name = TruncatingCharField(max_length=128, blank=True)
    last_name = TruncatingCharField(max_length=128, blank=True)
    name_prefix = TruncatingCharField(max_length=128, blank=True)
    name_suffix = TruncatingCharField(max_length=128, blank=True)
    first_name_lower = TruncatingCharField(max_length=128, blank=True,
                                           db_index=True)
    last_name_lower = TruncatingCharField(max_length=128, blank=True,
                                          db_index=True)
    is_primary = models.BooleanField(default=False, blank=True)
    locations = models.ManyToManyField("locations.LinkedInLocation",
                                       blank=True, related_name="+")
    needs_attention = models.BooleanField(blank=True, default=False,
                                          db_index=True)
    attention_payload = JSONField(blank=True, default={})

    parent = models.ForeignKey("self", blank=True, on_delete=models.SET_NULL,
                               null=True, related_name="contacts", )
    import_record = models.ForeignKey('ImportRecord', on_delete=models.CASCADE, blank=True, null=True,
                                      related_name="contacts")

    class Meta:
        index_together = (
            ('user', 'contact_id'),
            ('user', 'last_name_lower', 'first_name_lower'),
            ('user', 'is_primary')
        )

    # Add all Contact-attribute models to this list. This is used during
    # contact import
    SPECIAL_FIELDS = ("addresses", "email_addresses", "phone_numbers",
                      "organizations", "alma_maters", "locations",
                      "image_urls", "external_profiles", "external_ids")

    NAME_CLEAN_CHARS = string.punctuation + string.whitespace

    @classmethod
    def _clean_and_lowercase_name(cls, name):
        return name.strip(cls.NAME_CLEAN_CHARS).lower()

    def __str__(self):
        return "({}){} {}  user_id: {}".format(self.id, self.first_name,
                                                self.last_name, self.user_id)

    def __eq__(self, other):
        from frontend.apps.contact.analysis.contact_import.utils import (
            ContactWrapper)
        # This will force usage of the ContactWrapper.__eq__
        if isinstance(other, ContactWrapper):
            return other == self
        else:
            return super(Contact, self).__eq__(other)

    def get_preferred_dob(self):
        """Returns recently modified dob from all the manual contacts in the
        contact tree. If there are no manual contacts in the tree, then most
        recently modified dob from all the contacts is returned
        """
        contacts = self.get_contacts()
        if isinstance(contacts, models.QuerySet):
            # Sort child contacts by whether they are manual, and then by the
            # modification date of the DOB record.
            _contact = (contacts.exclude(dob=None).annotate(
                is_manual=ExpressionWrapper(Q(contact_type='manual'),
                                            output_field=BooleanField())).
                        order_by('-is_manual', '-dob__modified').first())
        else:
            # If contacts is not a QuerySet, it is a list of one contact.
            _contact = contacts[0]

        if hasattr(_contact, 'dob'):
            return _contact.dob
        return None

    def get_preferred_gender(self):
        """Returns recently modified gender from all the manual contacts in the
        contact tree. If there are no manual contacts in the tree, then most
        recently modified gender from all the contacts is returned
        """
        contacts = self.get_contacts()
        if isinstance(contacts, models.QuerySet):
            # Sort child contacts by whether they are manual, and then by the
            # modification date of the Gender record.
            _contact = (contacts.exclude(gender=None).annotate(
                is_manual=ExpressionWrapper(Q(contact_type='manual'),
                                            output_field=BooleanField())).
                        order_by('-is_manual', '-gender__modified').first())
        else:
            # If contacts is not a QuerySet, it is a list of one contact.
            _contact = contacts[0]

        if hasattr(_contact, 'gender'):
            return _contact.gender.gender_type
        else:
            return self.predict_gender()

    @property
    def name(self):
        return string.capwords(" ".join((self.first_name, self.last_name,
                                         self.name_suffix)))

    @property
    def raw_contact(self):
        if hasattr(self, "_raw_contact"):
            raw_string = self._raw_contact.raw_contact
        elif hasattr(self, "_raw_contact_data"):
            raw_string = self._raw_contact_data
        else:
            return ""

        if self.contact_type in RawContact.eval_sources:
            try:
                raw_contact = literal_eval(raw_string)
                if self.contact_type == "csv":
                    raw_contact = {ensure_unicode(k): (ensure_unicode(v)
                                   if isinstance(v, str) else v)
                                   for k, v in raw_contact.items()}
                return raw_contact
            except (SyntaxError, ValueError):
                # Just return the raw string for values we cannot evaluate.
                # It's likely this code will never be reached, but just in case
                return raw_string
        return raw_string

    @raw_contact.setter
    def raw_contact(self, data):
        if isinstance(data, bytes):
            data = ensure_unicode(data)
        if hasattr(self, "_raw_contact"):
            self._raw_contact.raw_contact = data
        else:
            self._raw_contact_data = data
        self._raw_contact_dirty = True

    @property
    def is_a_singleton(self):
        """Is this contact a primary contact without children"""
        contacts = self.get_contacts()
        return len(contacts) == 1 and contacts[0] == self

    @instance_cache
    def get_contacts(self):
        """Query and return the embedded list of contacts."""
        topmost = self.get_oldest_ancestor()
        if topmost.contact_type == 'merged':
            # Check if this is a needs-attention contact. If so, include its
            # former children in the response
            contacts = topmost.contacts.all()
            former_children = topmost.attention_payload.get("demerge")
            if topmost.needs_attention and former_children:
                contacts |= Contact.objects.filter(id__in=former_children)

            if not contacts:
                LOGGER.error("Merged contact has no children: {}".format(
                    topmost))
                return [topmost]
            else:
                return contacts.distinct()
        else:
            return [topmost]

    def get_composite_ids(self):
        contacts = self.get_contacts()
        if isinstance(contacts, models.QuerySet):
            composite_ids = contacts.exclude(composite_id='').values_list(
                'composite_id', flat=True)
        else:
            composite_ids = [contact.composite_id
                             for contact in contacts]
        return composite_ids

    def get_location_names(self):
        """Returns a list of the contact's LinkedIn location names
        """
        contacts = self.get_contacts()
        if isinstance(contacts, models.QuerySet):
            return list(filter(None, contacts.values_list(
                'locations__name', flat=True).distinct()))
        else:
            return [loc.name for child in contacts
                    for loc in child.locations.all()]

    def get_oldest_ancestor(self):
        """Gets the primary contact."""
        if self.parent_id is None or self.parent_id == self.pk:
            return self
        else:
            return self.parent.get_oldest_ancestor()

    def get_aliases(self):
        contacts = self.get_contacts()
        if isinstance(contacts, models.QuerySet):
            names = contacts.distinct(
                'last_name_lower', 'first_name_lower').values_list(
                    'last_name_lower', 'first_name_lower')
        else:
            names = {(c.last_name_lower, c.first_name_lower)
                     for c in contacts}
        return Identity.objects.aliases(*names)

    def _get_names(self, name_part, filter_short=False):
        '''Get the first/last name of all merged contacts

        if filter_short is True, only names longer than 1 character are
        returned.
        '''
        contacts = self.get_contacts()
        if isinstance(contacts, models.QuerySet):
            names = contacts.distinct(name_part).values_list(name_part,
                                                             flat=True)
        else:
            names = [getattr(c, name_part) for c in contacts]
        if filter_short:
            names = (name for name in names
                     if len(name.strip(self.NAME_CLEAN_CHARS)) > 1)
        return names

    @instance_cache
    def get_known_first_names(self):
        '''Return set of all known first names for contact

        Only returns names longer than 1 character.
        '''
        names = set(self._get_names('first_name_lower', filter_short=True))
        return names

    @instance_cache
    def get_known_last_names(self):
        '''Return set of all known last names for contact

        Only returns names longer than 1 character.
        '''
        names = set(self._get_names('last_name_lower', filter_short=True))
        return names

    @instance_cache
    def get_family_name_ethnicity(self):
        '''Get ethnicity for contact from all last_names found for contact'''
        names = self._get_names('last_name_lower', filter_short=True)
        ethnicity = namelib.family_name_ethnicity(names)
        return ethnicity

    @instance_cache
    def get_given_name_origin(self):
        '''Get the origin language for all given names of the contact'''
        names = self._get_names('first_name_lower', filter_short=True)
        origin = namelib.given_name_origin(names)
        return origin

    @instance_cache
    def predict_gender(self):
        '''Get the gender of the contact using first names of all merged
        contact records.

        We will prefer the gender from the usa_xard column because it prefers
        american names, then tries spanish, then other common languages, all
        the while filtering out archaic, rare unisex usages, and diminutive
        versions. If none of those matches, it falls back to the gender of that
        name across the entire world.

        Since we are searching using multiple names, it is possible to get more
        than one gender back. If this happens and we get M and U (for unisex)
        back, we will say the name is male. If we get back F and U, we say the
        name is female. If we get back M and F, we just say unisex. If we get
        nothing back, we say unisex.

        Possible returns from this method: 'M', 'F', 'U', None
        None is returned if the peacock database is not configured.
        '''
        names = self._get_names('first_name_lower', filter_short=True)
        gender = namelib.determine_gender(names)
        return gender

    @instance_cache
    def get_nicknames(self):
        """Query Peacock data to locate nicknames and name variations for
        the first name of every merged contact.

        Returns a list of lower-cased names in unicode if any names are found.
        If the name is not present in the peacock data, will return a list
        containing only the contact's lower-cased first name.

        If peacock database is unconfigured, will return None so code using
        this function will know to fall back to legacy search strategies.
        """
        names = self._get_names('first_name_lower', filter_short=True)
        nicknames = namelib.get_nicknames(names)
        return nicknames

    def format_for_kafka(self):
        return {
            "given_name": str(self.first_name),
            "middle_name": str(self.additional_name),
            "family_name": str(self.last_name)
        }

    @classmethod
    def kafka_key(cls):
        return "ContactName"

    def is_same_person(self, other_contact):
        """Check if the other contact is the same person.

        Compare the aggregated contacts of each primary contact, and
        search for any overlap in contacts.
        Typically, this method should be used to compare contacts from
        two different users.
        """
        # Get the primary contact for self and other
        self_primary = self.get_oldest_ancestor()
        other_primary = other_contact.get_oldest_ancestor()

        def build_contacts_comparison_set(primary):
            contacts = primary.get_contacts()

            comparison_set = set()
            # Iterate over the contacts and get their identifying feature
            # based on which service they are from
            for contact in contacts:
                # linkedin, facebook, twitter, etc.
                if contact.contact_type in ('linkedin-oauth2',):
                    comparison_set.add(contact.contact_id)
                # Gmail, csv, outlook
                elif contact.contact_type != "merged":
                    # Contact doesn't have a contact_id, so use the email.
                    # Iterate over all the possible emails, adding each.
                    # for email in contact.emails:
                    #     comparison_set.add(email.lower())
                    for email in contact.email_addresses.all():
                        comparison_set.add(email.address.lower())

            return comparison_set

        # Put all of the subcontacts into categories to compare
        self_comp_set = build_contacts_comparison_set(self_primary)
        other_comp_set = build_contacts_comparison_set(other_primary)

        # If there is any intersection between the two sets, that means they
        # are the same person.
        return bool(self_comp_set.intersection(other_comp_set))

    def _all_attribute_worker(self, attribute_name, show_archived=False):
        """A worker and generator that will retrieve an attribute from an
           entire contact tree. E.g. emails, addresses, etc.
        """
        attribute_model = getattr(Contact, attribute_name).field.model
        try:
            has_archived = bool(attribute_model._meta.get_field("archived"))
        except models.FieldDoesNotExist:
            has_archived = False

        contacts = self.get_contacts()
        for contact in contacts:
            if contact.contact_type != "merged":
                attribute_qs = getattr(contact, attribute_name).all()
                if not show_archived and has_archived:
                    attribute_qs = attribute_qs.filter(archived=None)
                for attribute in attribute_qs:
                    yield attribute

    def all_emails(self):
        return self._all_attribute_worker("email_addresses")

    def all_phones(self):
        return self._all_attribute_worker("phone_numbers")

    def decompose(self):
        """Break a contact back into the pieces that made/make it"""
        special_fields = {}
        for attribute in self.SPECIAL_FIELDS:
            values = list(self._all_attribute_worker(attribute))
            for value in values:
                if attribute == "locations":
                    # Linkedin is a special case. The stored values are not
                    # unique to each contact, but are global
                    continue
                value.id = None
                value.pk = None
            if values:
                special_fields[attribute] = values

        data = dict(
            first_name=self.first_name,
            first_name_lower=self.first_name_lower,
            last_name=self.last_name,
            last_name_lower=self.last_name_lower,
            additional_name=self.additional_name,
            name_prefix=self.name_prefix,
            name_suffix=self.name_suffix,
            contact_id=self.contact_id,
            contact_type=self.contact_type,
            user=self.user,
            is_primary=self.is_primary,)
        data.update(special_fields)
        return data

    def get_info(self):
        contact_info = {'street': "", "email": "", "phone": ""}
        address_found = False
        email_found = False
        phone_found = False

        contacts = self.get_contacts()

        for contact_instance in contacts:
            if contact_instance.contact_type == "linkedin-oauth2":
                contact_info['locations'] = list(
                    contact_instance.locations.values_list('name', flat=True))
            elif contact_instance.contact_type != "merged":
                for address in contact_instance.addresses.all():
                    if not address_found or contact_info['street'] == "":
                        contact_info['street'] = address.street
                        contact_info['po_box'] = address.po_box
                        contact_info['city_state_zip'] = "{}, {} {}".format(
                            address.city, address.region, address.post_code)
                        contact_info['country'] = address.country
                    if address.primary == "true":
                        address_found = True
                for email in contact_instance.email_addresses.all():
                    if not email_found or contact_info['email'] == "":
                        contact_info['email'] = email.address
                    if email.primary == "true":
                        email_found = True
                for phone in contact_instance.phone_numbers.all():
                    if not phone_found or contact_info['phone'] == "":
                        contact_info['phone'] = phone.national
                    if phone.primary == "true":
                        phone_found = True
        return contact_info

    @classmethod
    def compare_middle_names(cls, middle1, middle2):
        '''Simple middle name comparison.
        '''

        if not middle1 or not middle2:
            return None

        scrubbed1 = scrub_and_lowercase(middle1)
        scrubbed2 = scrub_and_lowercase(middle2)

        if not scrubbed1 or not scrubbed2:
            return None

        # If either name is an initial, and they match, all we can say is they
        # might be the same person.
        if ((len(scrubbed1) == 1 or len(scrubbed2) == 1) and
                scrubbed1[:1] == scrubbed2[:1]):
            return None

        # Check two middle names in entirety
        if scrubbed1 == scrubbed2:
            return True

        # Default to no match
        return False

    @classmethod
    def _get_suffix_parts(cls, name_suffix):
        '''This method cleans and splits a name suffix into individual tokens.

        First we make the entire name suffix lowercase so we don't have to look
        for 'Jr', 'jr', and 'JR'.

        Second we replace all punctuation with spaces. This is done for two
        reason: first to clean up 'jr.' so we get a single token 'jr'. Second
        is to make splitting easier and account for the possibility that two
        suffixes got smooshed with no space between. ex: 'jr.,phd'

        Last we split on spaces and return a list of tokens.
        '''
        name_suffix = name_suffix.strip().lower() if name_suffix else None
        if not name_suffix:
            return []

        name_suffix = strip_punctuation(name_suffix)
        suffix_parts = name_suffix.split()
        return suffix_parts

    @classmethod
    def compare_generational_suffixes(cls, suffix1, suffix2):
        """Strictly compare suffix strings, return boolean

        Behavior:
            We compare each suffix to the set of suffixes below. If one of
            the suffixes matches on any of the groups below, but the other
            does not, we return False for non-match.

            Otherwise we return True in the following cases:
                1) Suffixes are either junk or empty
                2) Both suffixes match on the same suffix group.
        """
        suffix_groups = [
            {'jr', 'junior'},
            {'sr', 'senior'},
            {'iii', 'third', '3rd'},
            {'iv', 'fourth', '4th'}
        ]
        # Break the suffixes up into tokens
        s1_parts = set(cls._get_suffix_parts(suffix1))
        s2_parts = set(cls._get_suffix_parts(suffix2))

        if not (s1_parts or s2_parts):
            # Neither have a suffix, so they are the same
            return True

        # Iterate over each of the suffix groups and check for suffix matches
        for group in suffix_groups:
            s1_match = bool(group.intersection(s1_parts))
            s2_match = bool(group.intersection(s2_parts))

            if s1_match != s2_match:
                # If one suffix matches, but the other does not (i.e. they
                # aren't equal), then we have two different suffixes.
                return False
            elif s1_match and s2_match:
                # Both suffixes match, so we have a suffix match
                return True

        # If we get here, there was junk in the suffix field, but nothing
        # that we could use to say they are different suffixes.
        return True

    @classmethod
    def compare(cls, c1, c2):
        """Deep compare two contacts.
        This is a classmethod because it needs to be able to compare
        ContactWrappers too (see contact.analysis.contact_import).
        """
        def get_attr(contact, field):
            # Get the correct attribute depending on if this is a Contact or
            # a ContactWrapper
            try:
                attr = contact.get_special_field(field)
                try:
                    # A non-dirty special field
                    return attr.all()
                except AttributeError:
                    return attr
            except AttributeError:
                # Not a special field
                return getattr(contact, field)

        def phone_gen(phone_iter):
            for phone in phone_iter:
                try:
                    # Blanks don't count
                    if phone.number:
                        yield phone.rfc3966
                except phonenumbers.NumberParseException:
                    # Skip bad phone numbers
                    pass

        # If the external contact IDs are the same, we automatically say this
        # is the same contact.
        if (c1.contact_id and c2.contact_id and
                    c1.contact_id == c2.contact_id and
                    c1.contact_type == c2.contact_type):
            return True

        # If these contacts have different name suffixes, or middle names,
        # they are not the same contact.
        if not cls.compare_generational_suffixes(c1.name_suffix,
                                                 c2.name_suffix):
            return False
        if cls.compare_middle_names(c1.additional_name,
                                        c2.additional_name) == False:
            return False

        # First compare the name fields. If there is no similarity, it's hard
        # to imagine this could be the same contact
        first_name_similarity = similarity(c1.first_name_lower,
                                           c2.first_name_lower)
        last_name_similarity = similarity(c1.last_name_lower,
                                          c2.last_name_lower)
        if first_name_similarity < 0.5 and last_name_similarity < 0.5:
            return False

        # An obvious yes is comparing the emails of each contact
        c1_emails = set(email.address.lower()
                        for email in get_attr(c1, "email_addresses"))
        c2_emails = set(email.address.lower()
                        for email in get_attr(c2, "email_addresses"))
        if c1_emails.intersection(c2_emails):
            return True

        # At this point, it becomes a little less straightforward if the
        # contacts are the same. We need to compare various fields and give
        # the contact an overall score.

        # Compare phone numbers
        c1_phones = set(phone_gen(get_attr(c1, "phone_numbers")))
        c2_phones = set(phone_gen(get_attr(c2, "phone_numbers")))
        shortest = min(len(c1_phones), len(c2_phones))

        if c1_phones or c2_phones:
            phone_ratio = (len(c1_phones.intersection(c2_phones))
                           / shortest
                           if shortest else 0)
        else:
            # If there are no phones, then we don't include them
            phone_ratio = None

        # Compare addresses
        address_similarity = None
        c1_addresses = get_attr(c1, "addresses")
        c2_addresses = get_attr(c2, "addresses")
        if c1_addresses or c2_addresses:
            address_similarity = 0
            for address1 in c1_addresses:
                for address2 in c2_addresses:
                    a1 = "{} {} {}".format(address1.street, address1.city,
                                           address1.post_code).lower()
                    a2 = "{} {} {}".format(address2.street, address2.city,
                                           address2.post_code).lower()
                    # Pick the highest address match
                    address_similarity = max(address_similarity,
                                             similarity(a1, a2))

        # Average out all the similarity ratios. Any ratio that is None is
        # excluded from the results.
        sum = 0
        count = 0
        for ratio in (first_name_similarity, last_name_similarity,
                      phone_ratio, address_similarity):
            if ratio is not None:
                sum += ratio
                count += 1
        match_value = sum / count

        return match_value > 0.75

    def format_for_csv(self):
        return dict(chain(
            Address.format_for_csv(self),
            EmailAddress.format_for_csv(self),
            PhoneNumber.format_for_csv(self),
            Organization.format_for_csv(self),
            AlmaMater.format_for_csv(self),
            ExternalId.format_for_csv(self),
        ))

    @property
    @instance_cache
    def emails(self):
        return list(addr for addr, _ in EmailAddress.iter_emails(self))

    @property
    @instance_cache
    def zips(self):
        zips = set()
        for address in Address.iter_addresses(self):
            if address.zip_key:
                zips.add(address.zip_key.zipcode)
        return zips

    @property
    @instance_cache
    def phones(self):
        phone_clean_map = {ord(char): None for char in
                           chain(string.punctuation, string.whitespace)}
        phones = []
        for phone, _ in PhoneNumber.iter_phone_numbers(self):
            try:
                number = str(phone).translate(phone_clean_map)
                phones.append(number)
            except phonenumbers.NumberParseException:
                pass
        return phones

    @transaction.atomic
    def alias_to(self, other_contact, person_dimension=None, label=None):
        if person_dimension is None:
            person_dimension, _ = PersonDimension.dimensions.get_or_create(
                dimension_type=PersonDimension.PersonDimensionTypes.ALIAS,
                label=label or self.name)
        Identity.build_based_on_contact(self, dimension=person_dimension,
                                        get_or_create=True)
        Identity.build_based_on_contact(other_contact,
                                        dimension=person_dimension,
                                        get_or_create=True)

    @classmethod
    def calc_composite_id(cls, contact):
        composite_id_fields = (
            contact.user_id, contact.contact_id, contact.contact_type)
        # If any fields are blank/None, the calculated id will be invalid
        return cls._calc_composite_id(*composite_id_fields)

    @classmethod
    def _calc_composite_id(cls, user_id, contact_id, contact_type):
        if not all((user_id, contact_id, contact_type)):
            return ""
        else:
            return deep_hash({
                'user_id': user_id,
                'contact_id': contact_id,
                'contact_type': contact_type
            })


@receiver(models.signals.post_save, sender=Contact)
def save_raw_contact(sender, instance, *args, **kwargs):
    if getattr(instance, "_raw_contact_dirty", False):
        if hasattr(instance, "_raw_contact"):
            instance._raw_contact.save()
        elif hasattr(instance, "_raw_contact_data"):
            RawContact.objects.create(raw_contact=instance._raw_contact_data,
                                      contact=instance)


class FileFormatError(Exception): pass


def _select_storage():
    if settings.CONTACT_UPLOAD_S3_STORAGE_ENABLED:
        return S3Boto3Storage(bucket='revup-contact-upload')
    else:
        return None


def _set_expiration_date():
    return date.today() + timedelta(days=7)


class ContactFileUpload(models.Model):
    class FileType(Choices):
        choices_map = [
            ("CSV", "CSV", "CSV File"),
            ("MOB", "MOBILE", "Unified Mobile"),
        ]

    user = models.ForeignKey("authorize.RevUpUser", on_delete=models.CASCADE, related_name='+',
                             help_text="The user who started this task")
    file_name = TruncatingCharField(max_length=256, blank=True)
    file_type = models.CharField(max_length=3, db_index=True,
                                 choices=FileType.choices(),
                                 default=FileType.CSV)
    data_file = models.FileField(max_length=512, storage=_select_storage())
    expiration = models.DateField(blank=True, default=_set_expiration_date,
                                  help_text="Auto-set expiration date to 7 "
                                            "days in the future")

    @classmethod
    def create(cls, user, file_type, file_name, file_content):
        """Create an instance with a CSV file. Verify the file is valid."""
        if file_type == cls.FileType.CSV:
            cleaned_file = StringIO(clean_csv_bytes(file_content, return_bytes=False))
            csv_file = DictReaderInsensitive(cleaned_file)
            for field in ("first name", "last name"):
                if field not in csv_file.fieldnames:
                    raise FileFormatError(
                        "Contact import files are required to have a "
                        "'{}' column".format(field))
        elif file_type == cls.FileType.MOBILE:
            if not file_content:
                raise FileFormatError("No contacts were uploaded.")
            file_content = ensure_bytes(json.dumps(file_content))
        s3_filename = 'acc{0}_usr{1}_{2}_{3}'.format(
            user.current_seat.account_id, user.id, random_uuid(), file_name)
        data_file = ContentFile(file_content, s3_filename)
        return cls.objects.create(user=user,
                                  file_name=file_name,
                                  file_type=file_type,
                                  data_file=data_file)


class ImportRecord(models.Model):
    class TaskType(ImportSources):
        choices_map = [
            ('CL', "CLONED", 'Cloned'),
        ]
        source_map = dict(
            cloned="CL",
        )
        # Add all of the import sources to TaskType
        choices_map.extend(ImportSources._raw_data)
        # Add all of the source-map items to TaskType
        source_map.update(ImportSources.source_map)

    user = models.ForeignKey("authorize.RevUpUser", on_delete=models.CASCADE)
    account = models.ForeignKey("campaign.Account", on_delete=models.CASCADE, blank=True, null=True,
                                help_text="This will be populated if this "
                                          "import was for account-level.")
    import_type = models.CharField(choices=TaskType.choices(),
                                   default=TaskType.OTHER,
                                   max_length=2)
    label = TruncatingCharField(max_length=256, blank=True)
    uid = models.CharField(
        max_length=256, blank=True,
        help_text="This should contain something that uniquely identifies "
                  "the social account that was imported from. "
                  "E.g. gmail address")
    import_dt = models.DateTimeField(auto_now_add=True,
                                     blank=True)
    import_success = models.BooleanField(default=True, blank=True)
    task_id = models.CharField(max_length=255, null=True, blank=True)
    transaction_id = models.CharField(
        max_length=64, blank=True, default="",
        help_text="This ID corresponds to the transaction ID sent to the "
                  "Kafka pipeline with the imported contacts")

    # Import Stats
    num_created = models.IntegerField(default=0, blank=True)
    num_existing = models.IntegerField(default=0, blank=True)
    num_merged = models.IntegerField(default=0, blank=True)
    num_invalid = models.IntegerField(default=0, blank=True)

    @property
    def num_total(self):
        return self.num_created + self.num_existing + self.num_invalid

    @property
    def num_valid(self):
        """Total number of contacts that were actually made into objects."""
        return self.num_created + self.num_existing

    @property
    def num_effective(self):
        """This is the "effective" contacts that were created from this set.

        If a contact was merged, that means it became part of another contact,
        as a leaf. So, two contacts are effectively one when they are merged.
        """
        effective = self.num_created + self.num_existing - self.num_merged
        if effective < 0:
            # This ImportRecord is probably old and has incorrect `num_merged`
            effective = self.num_created + self.num_existing
        return effective

    # TODO: We should also link the RevUpTask once those become persistent

    class Meta:
        index_together = [("import_type", "uid")]


class ContactNotes(TimeStampedModel):
    class Meta:
        unique_together = (("contact", "account"),)

    contact = models.ForeignKey(Contact, on_delete=models.CASCADE, related_name="notes")
    account = models.ForeignKey("campaign.Account", on_delete=models.CASCADE, related_name="+")
    notes1 = models.TextField(blank=True)
    notes2 = models.TextField(blank=True)


class ImportConfig(TitleDescriptionModel, OwnershipModel, EntityModel):
    """This model allows us to configure a User's (or possible an Account's)
    contact import process. This is in a similar vein to the AnalysisConfig
    model.
    """
    filters = models.ManyToManyField("filtering.Filter",
                                     related_name="import_configs")

    # TODO: We can set optional things in here, like:
    # name expansion, merge algorithm, allowed import sources, etc.


class LimitTracker(models.Model):
    user = models.ForeignKey("authorize.RevUpUser", on_delete=models.CASCADE)
    account = models.ForeignKey("campaign.Account", on_delete=models.CASCADE)
    # These fields are necessary for Contact limits. We keep running totals
    # of analyzed and deleted. deleted_count is mostly required for tracking
    # purposes, but analyzed_count is an actual limit. analyzed_count may be
    # manually reduced to give the user more rankings.
    analyzed_count = models.PositiveIntegerField(blank=True, default=0)
    deleted_count = models.PositiveIntegerField(blank=True, default=0)

    class Meta:
        unique_together = (("user", "account"),)

    @classmethod
    def get_tracker(cls, user, account, select_for_update=False):
        return cls.get_trackers(user, account,
                                select_for_update=select_for_update)[0]

    @classmethod
    def get_trackers(cls, user, *accounts, select_for_update=False):
        """Get or Build a tracker object for the user and one or more accounts.

        Important Note: Order is not guaranteed. The LimitTrackers returned
           will probably not be in the same order as the given accounts.
        """
        trackers_qs = cls.objects.filter(user=user,
                                         account__in=(a.id for a in accounts))
        if select_for_update:
            trackers_qs = trackers_qs.select_for_update()
        trackers = {t.account_id: t for t in trackers_qs}
        for account in accounts:
            if account.id not in trackers:
                trackers[account.id] = cls(user=user, account=account)
        return list(trackers.values())
