from ast import literal_eval

from ..models import ExternalProfile, ImageUrl, Contact, ImportSources
from frontend.libs.utils.general_utils import FunctionalBuffer


def run():
    contacts = Contact.objects.filter(contact_type="linkedin-oauth2") \
                              .exclude(_raw_contact=None).distinct() \
                              .select_related("_raw_contact")
    urls = FunctionalBuffer(ExternalProfile.objects.bulk_create,
                            max_length=500)
    images = FunctionalBuffer(ImageUrl.objects.bulk_create,
                              max_length=500)

    for contact in contacts:
        raw_contact = contact.raw_contact

        profile_url = raw_contact.get("publicProfileUrl")
        profile_image = raw_contact.get("pictureUrl")

        if profile_url and "http" in profile_url:
            urls.push(ExternalProfile(
                contact=contact,
                url=profile_url,
                source=ImportSources.LINKEDIN
            ))
        if profile_image and "http" in profile_image:
            images.push(ImageUrl(
                contact=contact,
                url=profile_image,
                source=ImportSources.LINKEDIN
            ))

    urls.flush()
    images.flush()
