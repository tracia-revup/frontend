"""
Backfill the field `composite_id` on the `Contact` model

This script only calculates a composite_id for non-merged contacts.
If a contacts already has a composite_id, this will not recalculate.

NOTE: It may be a good idea to run 'generate_contact_id_for_contacts.py'
      before running this. That will backport `contact_id`.
"""
import sys

from frontend.apps.contact.models import Contact
from frontend.libs.utils.general_utils import update_progress
from frontend.libs.utils.string_utils import random_uuid


def _progress_callback(percent):
    sys.stdout.write('Progress: {}%\r'.format(percent))
    sys.stdout.flush()


def run():
    # This queries for all contacts without a composite ID, excluding any
    # non-manual contacts without a contact_id.
    contacts = Contact.objects.filter(composite_id='').exclude(
        contact_type='merged').exclude(
            contact_id='', contact_type__ne='manual').only(
                'pk', 'contact_type', 'contact_id', 'user_id')
    contact_count = contacts.count()
    for i, contact in enumerate(contacts.iterator()):
        update_fields = ['composite_id']
        if contact.contact_type == 'manual' and not contact.contact_id:
            contact.contact_id = random_uuid()
            update_fields.append('contact_id')
        contact.composite_id = Contact.calc_composite_id(contact)
        contact.save(update_fields=update_fields)
        update_progress(_progress_callback, contact_count, i)
