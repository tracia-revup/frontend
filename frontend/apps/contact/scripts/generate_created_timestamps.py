"""
This script is expecting to be called after migration 0036.

0036 added a 'created' timestamp to contacts. We need to go through and
backfill those dates as best we can. The most reliable way is to use the
ImportRecords. However, some contacts won't have an ImportRecord, so we'll
need to deal with that appropriately.

For 'merged' contacts, their created timestamp will be their oldest child.
'manual' contacts will have to be given datetime.now, since we can't know
when they were created.
"""
import textwrap

from django.db import connection


def run(import_record_model=None, contact_model=None, schema_editor=None):
    """This function may be called from a migration, so we want to be able to
    use the migration state.
    """
    if not import_record_model:
        from frontend.apps.contact.models import ImportRecord
        import_record_model = ImportRecord

    if not contact_model:
        from frontend.apps.contact.models import Contact
        contact_model = Contact

    # Iterate over all import records and update their contacts
    for import_record in import_record_model.objects.all().iterator():
        created = import_record.import_dt
        if not created:
            # This shouldn't happen, but we don't want to override the
            # timestamps with None if it does.
            continue
        contact_model.objects.filter(import_record=import_record).update(
            created=created)

    # Merged contacts that end up as a child of another contact should have
    # a null timestamp
    contact_model.objects.filter(contact_type="merged",
                                 is_primary=False).update(created=None)

    # Add the oldest child timestamp to the merged parent. We need to use
    # raw sql because the Django ORM can't handle a couple of these operations
    query = textwrap.dedent("""
        UPDATE contact_contact
        SET created=subquery.oldest_child
        FROM (
          SELECT
              "contact_contact"."id",
              MIN(T2."created") AS "oldest_child"
          FROM "contact_contact"
          LEFT OUTER JOIN "contact_contact" T2
              ON ("contact_contact"."id" = T2."parent_id")
          WHERE ("contact_contact"."contact_type" = 'merged' AND
                 "contact_contact"."is_primary" = True)
          GROUP BY "contact_contact"."id"
        ) AS subquery
    
        WHERE contact_contact.id=subquery.id;
    """)

    if schema_editor:
        schema_editor.execute(query)
    else:
        with connection.cursor() as cursor:
            cursor.execute(query)
