import logging
import sys

from django.db.models import F

from frontend.apps.contact.models import Contact

logger = logging.getLogger()


def run(user_id=None):
    if user_id:
        try:
            user_id = int(user_id)
        except ValueError:
            logger.error("The provided argument is not a valid user id!: %s",
                         user_id)
            sys.exit(1)

    contacts = Contact.objects.filter(
        parent__isnull=False,
        parent__last_name_lower__ne=F('last_name_lower')).distinct(
            'first_name_lower', 'last_name_lower').select_related('parent')

    for contact in contacts.iterator():
        logger.info("Creating a link between %s and %s", contact.parent,
                    contact)
        contact.parent.alias_to(
            contact,
            label="Name change link for contact id: {}".format(
                contact.parent.pk))
