"""
Script to export all raw contacts to backend.

Accepts a list of user_ids that can be used to restrict which user's contacts
are exported.

Example usage:

    # To export the contacts for users 1, 2, 3, and 4
    python manage.py runscript frontend.apps.contact.scripts.export_raw_contacts_to_kafka --script-args 1 2 3 4

    # To export the contacts for all users
    python manage.py runscript frontend.apps.contact.scripts.export_raw_contacts_to_kafka
"""
from ast import literal_eval
import sys

from django.conf import settings

from frontend.apps.contact.models import RawContact
from frontend.libs.utils.general_utils import update_progress
from frontend.libs.utils.kafka_utils import init_producer
from frontend.libs.utils.string_utils import ensure_unicode, random_uuid


def _progress_callback(percent):
    sys.stdout.write('Progress: {}%\r'.format(percent))
    sys.stdout.flush()


def run(*user_ids):
    raw_contacts_qs = RawContact.objects.exclude(raw_contact='').values_list(
        'contact__contact_type', 'contact__user_id', 'raw_contact',).filter(
            contact__contact_type__in=["google-oauth2", "linkedin-oauth2",
                                       "outlook", "addressbook", "csv",
                                       "iphone", "android", "nationbuilder",
                                       "housefile"])
    if user_ids:
        user_ids = [int(user_id) for user_id in user_ids]
        raw_contacts_qs = raw_contacts_qs.filter(contact__user_id__in=user_ids)
    contact_count = raw_contacts_qs.count()
    producer = init_producer()
    base_txid = 'export-{}-{{}}'.format(random_uuid())

    for i, (contact_type, user_id, value) in enumerate(
                                                raw_contacts_qs.iterator()):
        if contact_type in ("linkedin-oauth2", "outlook", "addressbook",
                            "csv", "iphone", "android", "nationbuilder",
                            "housefile"):
            try:
                value = literal_eval(value)
            except SyntaxError:
                # Skip contacts with values we cannot evaluate
                continue

            if contact_type == "csv":
                value = {ensure_unicode(k): (ensure_unicode(v)
                                             if isinstance(v, str)
                                             else v)
                         for k, v in value.items()}
        contact_record = {
            'user_id': user_id,
            'contact_type': ensure_unicode(contact_type),
            'value': value,
            'txid': base_txid.format(user_id),
        }
        result = producer.send(settings.BACKEND_CONTACT_TOPIC,
                               contact_record)
        if result.failed():
            raise result.exception
        update_progress(_progress_callback, contact_count, i)

    producer.flush()
