
from frontend.apps.contact.analysis.remerge import (
    move_calltimecontacts, move_prospects, move_prospectcollisions,
    move_contactnotes)
from frontend.apps.contact.deletion import ContactDeleter
from frontend.apps.contact.models import Contact
from frontend.apps.contact.analysis.contact_import.base import ContactImportBase


def run(*args):
    Contact.objects.filter(contact_type='merged', is_primary=True, parent=None,
                           contacts=None).update(is_primary=False)
    backfill_targets = Contact.objects.filter(contact_type='merged',
                                              is_primary=False, parent=None,
                                              needs_attention=False,
                                              first_name_lower__length__gte=2)\
                                      .select_related("user")
    for backfill_target in backfill_targets.iterator():
        cd = ContactDeleter(backfill_target.user, delete_children=False)
        if not cd.has_relevant_data(backfill_target):

            cd.delete(backfill_target)
            continue

        skip = False
        reason = ""
        all_siblings = ContactImportBase._find_siblings(
                            backfill_target, use_name_expansion=False)
        if not all_siblings:
            skip = True
            reason = "No siblings"

        # Exclude siblings with only a first initial
        siblings = [s for s in all_siblings if len(s.first_name_lower) > 1]
        if not siblings:
            skip = True
            reason = "All siblings first-initial only"

        if skip:
            backfill_target.attention_payload = {
                'siblings': [s.id for s in all_siblings],
                'backfill_skip': True,
                "skip_reason": reason
            }
            backfill_target.save(update_fields=['attention_payload'])
        else:
            # Check if the siblings all share the same parent (or are the parent)
            parent_check = {s.parent_id if s.parent_id else s.id
                            for s in siblings}
            # If there is only one parent, let's just move everything now
            if len(parent_check) == 1:
                movers = list(siblings) + [backfill_target]
                new_parent = Contact.objects.get(id=parent_check.pop())

                # This shouldn't happen. If it does, I want the script to blow up
                if new_parent == backfill_target:
                    raise RuntimeError("The Children are already children to the "
                                       "backfill_target: {}".format(backfill_target))

                # Move the CallTimeContact
                move_calltimecontacts(new_parent, movers)
                # Move the Prospects
                move_prospects(new_parent, movers)
                move_prospectcollisions(new_parent, movers)
                # Move the Notes
                move_contactnotes(new_parent, movers)

                # Delete the backfill_target now that it is devoid of data
                cd.delete(backfill_target)
                continue

            backfill_target.needs_attention = True
            backfill_target.attention_payload = {'demerge': [s.id
                                                             for s in siblings],
                                                 'backfilled': True}
            backfill_target.save(update_fields=['needs_attention',
                                                'attention_payload'])
