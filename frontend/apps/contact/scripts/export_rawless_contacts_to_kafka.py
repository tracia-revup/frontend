"""
Script to export all rawless contacts to backend.

Accepts a list of user_ids that can be used to restrict which user's contacts
are exported.

Example usage:

    # To export the contacts for users 1, 2, 3, and 4
    python manage.py runscript frontend.apps.contact.scripts.export_rawless_contacts_to_kafka --script-args 1 2 3 4

    # To export the contacts for all users
    python manage.py runscript frontend.apps.contact.scripts.export_rawless_contacts_to_kafka
"""
import sys

from django.conf import settings

from frontend.apps.contact.models import Contact
from frontend.libs.utils.general_utils import update_progress
from frontend.libs.utils.kafka_utils import init_producer
from frontend.libs.utils.string_utils import ensure_unicode, random_uuid


def _progress_callback(percent):
    sys.stdout.write('Progress: {}%\r'.format(percent))
    sys.stdout.flush()


def run(*user_ids):
    contacts_qs = Contact.objects.exclude(
        contact_type__in=['manual', 'merged']).filter(
            _raw_contact=None).exclude(contact_id='')

    if user_ids:
        user_ids = [int(user_id) for user_id in user_ids]
        contacts_qs = contacts_qs.filter(user_id__in=user_ids)

    contact_count = contacts_qs.count()
    producer = init_producer()

    base_txid = 'export-{}-{{}}'.format(random_uuid())

    for i, contact in enumerate(contacts_qs.iterator()):
        value = {
            contact.kafka_key(): contact.format_for_kafka(),
        }

        for relation in ("email_addresses", "phone_numbers", "addresses"):
            related_manager = getattr(contact, relation)
            key = related_manager.model.kafka_key()
            values = [v.format_for_kafka()
                      for v in related_manager.filter(archived=None)]
            if values:
                value[key] = values

        contact_record = {
            'user_id': contact.user_id,
            'contact_type': ensure_unicode(contact.contact_type),
            'contact_id': ensure_unicode(contact.contact_id),
            'rawless': True,
            'value': value,
            'txid': base_txid.format(contact.user_id),
        }
        result = producer.send(settings.BACKEND_CONTACT_TOPIC,
                               contact_record)
        if result.failed():
            raise result.exception
        update_progress(_progress_callback, contact_count, i)

    producer.flush()
