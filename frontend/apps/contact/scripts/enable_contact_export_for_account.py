"""
Script to enable contact export for an account(s), and then run contact export
for the users

Requires a list of account ids on which to run operations.

Will try to only export contacts for users who have not yet been exported. If
the export flag is already enabled for all the given accounts, will fall back
to exporting contacts for every user.

Example usage:

    # To export the contacts for all users with active seats on accounts 1, 2, 3, and 4
    python manage.py runscript frontend.apps.contact.scripts.enable_contact_export_for_account --script-args 1 2 3 4
"""

from django.contrib.auth.models import Group
from waffle.models import Flag

from frontend.apps.contact.scripts import (
    export_raw_contacts_to_kafka, export_rawless_contacts_to_kafka,
    send_manual_contact_actions_to_kafka)


def run(*account_ids):
    assert account_ids, "Account ids are required!"
    account_ids = [int(account_id) for account_id in account_ids]
    groups = Group.objects.filter(account_group__id__in=account_ids)
    export_flag = Flag.objects.get(name='Contact Import Backend Export')

    # Get list of users who have already had their contacts exported
    already_exported = set(export_flag.users.values_list('id', flat=True))
    already_exported.update(export_flag.groups.values_list('user', flat=True))

    # Add account groups to contact export waffle flag
    print("Enable contact export for accounts: {}".format(account_ids))
    export_flag.groups.add(*groups)

    user_ids = set(groups.values_list('user', flat=True))
    # Since users can have multiple seats, their contacts may have already been
    # exported. Remove users whose contacts have already been exported.
    need_export = user_ids.difference(already_exported)
    # If no users are left, then contact export was already enabled for
    # everyone and we have no way of determining who has already had their
    # contacts exported, so export contacts for everyone.
    if not need_export:
        need_export = user_ids

    print("Starting export for user ids: {}".format(need_export))
    print("Export manual contact actions")
    send_manual_contact_actions_to_kafka.run(*need_export)
    print("Export contacts that have no raw contact associated with them")
    export_rawless_contacts_to_kafka.run(*need_export)
    print("Export contacts with raw contacts")
    # We do this one last since it will cover the most contacts and take the
    # longest.
    export_raw_contacts_to_kafka.run(*need_export)
