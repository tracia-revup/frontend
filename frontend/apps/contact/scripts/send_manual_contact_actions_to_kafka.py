"""Send all manual contacts and archived attributes to Kafka"""
import pprint

from frontend.apps.contact.kafka import ManualContactTransaction
from frontend.apps.contact.models import (
    Contact, PhoneNumber, Address, EmailAddress, Organization)
from frontend.libs.utils.kafka_utils import init_producer
from frontend.libs.utils.string_utils import random_uuid


def run(*user_ids, **kwargs):
    commit = kwargs.get('commit', True)
    if user_ids:
        user_ids = [int(user_id) for user_id in user_ids]

    producer = init_producer() if commit else None
    # Query for all archived ("deleted") attributes
    for attribute_class in (Address, PhoneNumber, EmailAddress, Organization):
        attributes = attribute_class.objects.select_related(
            "contact").exclude(archived=None)
        if user_ids:
            attributes = attributes.filter(contact__user_id__in=user_ids)
        for attribute in attributes.iterator():
            # Check if the attribute has any siblings in its contact that
            # have duplicate data
            has_duplicates = False
            siblings = attribute_class.objects.filter(
                contact=attribute.contact, archived=None)
            instance_data = attribute.format_for_kafka()
            for sibling in siblings:
                if instance_data == sibling.format_for_kafka():
                    has_duplicates = True
                    break

            # Send the attribute deletion to Kafka
            if not has_duplicates:
                mct = ManualContactTransaction(producer=producer)
                mct.archive(attribute.contact, attribute)
                if commit:
                    mct.commit(flush=False)
                else:
                    pprint.pprint(mct.build_contact_actions())

    # Query for all edits and created contacts
    contacts = Contact.objects.filter(contact_type="manual")
    if user_ids:
        contacts = contacts.filter(user_id__in=user_ids)
    for contact in contacts.iterator():
        # Manual contacts should have a contact_id, but it's possible some
        # got skipped, or the script was never run
        if not contact.contact_id:
            contact.contact_id = random_uuid()
            if commit:
                contact.save(update_fields=["contact_id"])

        # Get all the manual contact attributes and submit them to kafka
        mct = ManualContactTransaction(producer=producer)
        mct.create(contact, contact)
        for relation in ("email_addresses", "phone_numbers", "addresses",
                         "organizations"):
            for attribute in getattr(contact, relation).filter(archived=None):
                mct.create(contact, attribute)

        if commit:
            mct.commit(flush=False)
        else:
            pprint.pprint(mct.build_contact_actions())

    if producer:
        producer.flush()
