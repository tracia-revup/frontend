"""
Script to backport employer information from raw_contact of source linkedin

Example Usage:
    python manage.py runscript frontend.apps.contact.scripts.extract_linkedin_employer_info
"""

import sys
import time

from frontend.apps.contact.models import Contact, Organization
from frontend.libs.utils.general_utils import (FunctionalBuffer,
                                               update_progress,
                                               clean_get_string)


def _progress_callback(percent):
    sys.stdout.write('Progress: {}%\r'.format(percent))
    sys.stdout.flush()


def run():
    # Start profiling
    start_time = time.time()

    # Get all linkedin contatcs
    contacts = Contact.objects.filter(contact_type="linkedin-oauth2") \
                              .exclude(_raw_contact=None) \
                              .select_related("_raw_contact")
    contacts_count = contacts.count()
    contacts_processed = 0
    contacts_with_positions_info = 0
    contacts_without_position_info = 0
    contacts_backported = 0
    contacts_updated = 0
    contacts_ignored = 0
    organizations = FunctionalBuffer(Organization.objects.bulk_create,
                                     max_length=500)

    for contact in contacts.iterator():
        contact_backported = False
        contact_updated = False
        have_positions_info = False
        raw_contact = contact.raw_contact
        # Get linkedin positions from raw_contact
        positions = raw_contact.get("positions", None)
        if positions:
            # Get organizations of the contact already stored in db
            current_organizations = contact.organizations

            positions_list = positions.get('values', [])
            if positions_list:
                have_positions_info = True
            for each_position in positions_list:
                # Available fields in a linkedin position are : id, title,
                # summary, start-date, end-date, is-current, company
                name = ''
                job_description = clean_get_string(each_position, 'summary', None)
                title = clean_get_string(each_position, 'title', '')
                primary = each_position.get('is-current', False)
                company = each_position.get('company', None)
                if company:
                    # Avalilable fields in a company are : id, name, type,
                    # industry, ticker
                    name = clean_get_string(company, 'name', '')

                # If name and title are empty, ignore the position and continue
                if not (name or title):
                    continue

                # Look for an existing organization with same name and title.
                # If a record is found, update the primary and job_description
                # fields on the record
                updated_orgs = current_organizations.filter(
                    name=name, title=title).update(
                    job_description=job_description, primary=primary)
                if updated_orgs == 0:
                    # If no existing record got updated (no record exists with
                    # same name and title), create a new record
                    organizations.push(Organization(contact=contact,
                                                    name=name,
                                                    title=title,
                                                    job_description=job_description,
                                                    primary=primary))
                    contact_backported = True
                else:
                    contact_updated = True

        if have_positions_info:
            contacts_with_positions_info += 1
            if contact_backported:
                contacts_backported += 1
            elif contact_updated:
                contacts_updated += 1
            else:
                contacts_ignored += 1
        else:
            contacts_without_position_info += 1

        contacts_processed += 1
        update_progress(_progress_callback, contacts_count, contacts_processed)

    organizations.flush()
    elapsed_time = time.time() - start_time
    print(f'Contacts Count: {contacts_count} \n\tContacts without positions '
          f'info: {contacts_without_position_info}\n\tContacts with positions '
          f'info: {contacts_with_positions_info}\n\t\tBackported Contacts: '
          f'{contacts_backported}\n\t\tUpdated Contacts: {contacts_updated}'
          f'\n\t\tIgnored Contacts: {contacts_ignored}\nTime Taken: '
          f'{elapsed_time} secs')
