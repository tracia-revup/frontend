"""Examine edited contacts to determine their original value via the
stored raw-contact.

Make the appropriate changes to recreate the original
content, then archive it. Move the edits over to a manual contact.
"""
from ast import literal_eval
from datetime import datetime
import logging

from django.db import transaction
from lxml import etree

from frontend.apps.contact.analysis.contact_import import SOURCE_MAP
from frontend.apps.contact.analysis.remerge import set_parent_children
from frontend.apps.contact.models import Contact
from frontend.libs.utils.general_utils import deep_hash
from frontend.libs.utils.string_utils import random_uuid
from frontend.libs.third_party.cloudsponge.api import Contact as CS_Contact


LOGGER = logging.getLogger(__name__)


def run(commit=True):
    # Query all contacts that have been modified, that are not manual contacts.
    # We also ignore contacts that don't have a raw_contact. It's useless.
    for contact in Contact.objects.select_related("_raw_contact") \
                                  .filter(is_primary=False) \
                                  .exclude(contact_type="manual") \
                                  .exclude(_raw_contact=None) \
                                  .exclude(_raw_contact__raw_contact='') \
                                  .exclude(modified_by_user=None).iterator():
        try:
            # Use the contact importer to rebuild the raw contact.
            Importer = SOURCE_MAP.get(contact.contact_type)
            if not Importer:
                LOGGER.warning("Invalid contact-type: '{}'. Contact: {}".format(
                    contact.contact_type, contact))
                continue

            importer = Importer(contact.user, contact.contact_type,
                                None, None, None, None)

            # Some sources need the raw contact to be handled differently
            if contact.contact_type == "google-oauth2":
                raw_contact = etree.fromstring(contact.raw_contact)
            else:
                raw_contact = contact.raw_contact
                # We process CSVs as lists instead of dicts, so we need to
                # reproduce that here.
                if contact.contact_type == "csv":
                    header_map = {}
                    rc = []
                    for idx, (key, value) in enumerate(raw_contact.items()):
                        header_map[key.lower().strip()] = idx
                        rc.append(value)
                    raw_contact = rc
                    importer.header_map = header_map
                elif contact.contact_type in ("addressbook", "outlook"):
                    raw_contact = CS_Contact(raw_contact)

            # Regenerate the original contact from the raw data
            orig_contact = importer.build_contact(raw_contact)
            if not orig_contact:
                LOGGER.warning("Parsed data rejected: {}({}). Data: {}".format(
                    contact, contact.contact_type, raw_contact))
                continue
        except Exception as err:
            LOGGER.warning("Failed to parse: {}. "
                           "Reason: {}".format(contact, err))
            continue

        manual = get_or_create_manual_contact(contact, commit=commit)

        # For the original and current contact, we need to build
        # data structures to diff the two
        deleted = []
        created = []
        for relation in ("email_addresses", "phone_numbers", "addresses"):
            contact_attrs = getattr(contact, relation).all()
            orig_attrs = getattr(orig_contact, relation)["objects"]
            manual_attrs = getattr(manual, relation).all() if manual else []

            # If neither has any attributes, there's nothing to do.
            if not (contact_attrs or orig_attrs):
                continue

            # Build data sets to compare data across contacts
            contact_data = _build_data(contact_attrs)
            orig_data = _build_data(orig_attrs)
            manual_data = _build_data(manual_attrs)

            # Find the data missing from the old and current to know what
            # we need to reproduce.
            # Filter out attributes that are already in the manual contact,
            # so we don't duplicate attribute creates
            created.extend(find_missing(contact_attrs.filter(archived=None),
                                        orig_data, manual_data))
            deleted.extend(find_missing(orig_attrs, contact_data))

        # If anything fails, we want to unwind the stack
        try:
            with transaction.atomic():
                # Archive the deleted attributes. We need to create them, then
                # archive them.
                for attribute in deleted:
                    if commit:
                        attribute = clone_prepare(attribute, contact)
                        attribute.archived = datetime.now()
                        attribute.save()

                    LOGGER.info("DELETE {} {} {}".format(
                        contact, attribute.__class__.__name__,
                        attribute.format_for_kafka()))

                # Change the owner of the "created" contact so it moves
                # over to the manual contact
                for attribute in created:
                    if commit:
                        attribute.contact = manual
                        attribute.save()
                    LOGGER.info("CREATE {} {} {}".format(
                        contact, attribute.__class__.__name__,
                        attribute.format_for_kafka()))

        except Exception as err:
            LOGGER.exception("Couldn't save changes: {}. Reason {}".format(
                contact, err))
            raise


def clone_prepare(attribute, contact):
    attribute.id = None
    attribute.pk = None
    if contact:
        attribute.contact = contact
    return attribute


def find_missing(attributes, other_data, exclude=None):
    """Return any data in 'attributes' that's not in other_data or exclude"""
    exclude = exclude or []
    missing = {}
    for attr in attributes:
        data = _prep_attribute(attr)
        if data and data not in other_data and data not in exclude:
            missing[data] = attr
    return iter(missing.values())


def _prep_attribute(attribute):
    """Extract the data from the attribute model (PhoneNumber, Address, etc)
    and return it.
    If the data is in a dict (Address), we want to hash the dict
    """
    data = attribute.format_for_kafka()
    # It was possible for attributes to have blank values in the past
    # (E.g. no email address). This if-statement will help clean those
    if not data:
        return None
    if isinstance(data, dict):
        if not list(filter(None, data.itervalues())):
            return None
        data = deep_hash(data)
    return data


def _build_data(attributes):
    """Build a data set while filtering out Nones and empty strings"""
    return set(filter(None, (_prep_attribute(attr) for attr in attributes)))


def get_or_create_manual_contact(contact, commit=True):
    """Search for a manual contact that is either already 'contact', or
        is a child/sibling of contact. If none exist, create one and
        return it.
    """
    # See if a manual contact already exists
    manual = None
    if contact:
        if contact.contact_type == "manual":
            manual = contact
        else:
            for child in contact.get_contacts():
                if child.contact_type == "manual":
                    manual = child
                    break

    # If we managed to find an existing manual contact, just return it.
    # Otherwise, we have to create one
    if manual:
        return manual
    elif commit:
        # If a contact instance already exists, we'll use that to
        # seed the new "manual" contact
        contact_data = dict(
            first_name=contact.first_name,
            last_name=contact.last_name,
            additional_name=contact.additional_name)
        manual = create_manual_contact(contact.user, contact_data)
        # We need to create a "merged" contact, and make
        # 'instance' and 'manual' children of it
        set_parent_children(contact.parent or contact, children=[manual])
        return manual
    else:
        return None


def create_manual_contact(user, contact_data):
    contact_data.update(dict(
        first_name_lower=Contact._clean_and_lowercase_name(
            contact_data.get('first_name', '')),
        last_name_lower=Contact._clean_and_lowercase_name(
            contact_data.get('last_name', '')),
        user=user,
        is_primary=False,
        contact_type="manual",
        contact_id=random_uuid(),
    ))
    contact_data["composite_id"] = Contact._calc_composite_id(
        contact_data["user"].id, contact_data["contact_id"],
        contact_data["contact_type"])
    return Contact.objects.create(**contact_data)
