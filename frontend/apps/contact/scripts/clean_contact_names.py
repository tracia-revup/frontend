
import logging

from django.db.models import Q
from django.db import transaction

from frontend.apps.contact.models import Contact
from frontend.apps.contact.analysis.contact_import.base import ContactImportBase

LOGGER = logging.getLogger(__name__)


def update_progress(current, total):
    if total != 0:
        # only save progress on new integer percent
        percent = int(100.0 * current / total)
        last_percent = int(100.0 * (current - 1) / total)
        if percent != last_percent:
            LOGGER.info("Percent done: %s", percent)


def run():
    contacts = Contact.objects.filter(
        Q(first_name_lower__regex='\W') |
        Q(last_name_lower__regex='\W')).only(
            'first_name_lower', 'last_name_lower')
    total = contacts.count()
    for num, contact in enumerate(contacts.iterator()):
        new_first = ContactImportBase._clean_and_lowercase_name(
            contact.first_name_lower)
        new_last = ContactImportBase._clean_and_lowercase_name(
            contact.last_name_lower)
        do_save = False
        if len(new_first) != len(contact.first_name_lower):
            do_save = True
            contact.first_name_lower = new_first
        if len(new_last) != len(contact.last_name_lower):
            do_save = True
            contact.last_name_lower = new_last
        if do_save:
            with transaction.atomic():
                contact.save()
        update_progress(num, total)

