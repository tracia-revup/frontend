
import sys

from django.db import models

from frontend.apps.contact.models import Address
from frontend.libs.utils.general_utils import update_progress


def _progress_callback(percent):
    sys.stdout.write('Progress: {}%\r'.format(percent))
    sys.stdout.flush()


def run(*user_ids):
    addresses = Address.objects.filter(
        models.Q(city_key=None) | models.Q(zip_key=None))
    if user_ids:
        user_ids = [int(uid) for uid in user_ids]
        addresses = addresses.filter(contact__user_id__in=user_ids)
    total = addresses.count()
    for num, address in enumerate(addresses.iterator()):
        dirty = address.splay_address()
        if dirty:
            # TODO: need a bulk update. this is going to take forever.
            address.save()
        update_progress(_progress_callback, total, num)
