"""
Backfill the field `contact_id` on the Contact model for cloudsponge  and csv
contacts

This script only calculates a contact_id for contacts that do not already have
one and have a raw contact. Cloudsponge contacts are identified by having the
contact_type "addressbook" or "outlook", while CSV contacts are identified by
the contact_type "csv".

We do this because contacts imported from csvs and cloudsponge do not come with
a contact_id, so we must calculate it outselves. We didn't always do this, so
now we must backfill as we now need it to link copies of a contact on the
frontend and backend.
"""
from ast import literal_eval
import sys

from frontend.apps.contact.models import Contact
from frontend.libs.utils.general_utils import update_progress, deep_hash


def _progress_callback(percent):
    sys.stdout.write('Progress: {}%\r'.format(percent))
    sys.stdout.flush()


def run():
    contacts = Contact.objects.filter(
        contact_type__in=['addressbook', 'outlook', 'csv'],
        contact_id='').exclude(_raw_contact__raw_contact='').exclude(
            _raw_contact=None).only('_raw_contact').select_related(
                '_raw_contact')
    contact_count = contacts.count()
    for i, contact in enumerate(contacts.iterator()):
        try:
            raw_contact = contact.raw_contact
        except Contact._raw_contact.RelatedObjectDoesNotExist:
            pass
        else:
            try:
                contact.contact_id = deep_hash(raw_contact)
            except UnicodeDecodeError:
                print("Failed to hash contact: {}".format(contact))
            else:
                contact.save(update_fields=['contact_id'])

        update_progress(_progress_callback, contact_count, i)
