
from frontend.apps.call_time.models import CallTimeContact
from frontend.apps.campaign.models import Account, Prospect
from frontend.apps.contact.analysis.remerge import ContactPartitioner
from frontend.apps.contact.exceptions import NoChildContacts
from frontend.apps.contact.models import ContactNotes
from frontend.apps.seat.models import Seat


def run():
    total_contact_count = 0
    total_split_count = 0
    total_has_data_count = 0

    # Get all account contacts
    accounts = Account.objects.filter(active=True)
    for account in accounts.iterator():
        user = account.account_user
        contacts = user.contact_set.filter(contact_type="merged",
                                           is_primary=True)
        contact_count = contacts.count()
        count, has_data_count = repartition(contacts)
        print("Total merged contacts: {}. Total splits: {}. Splits with data: " \
              "{}. Account: {}.".format(contact_count, count, has_data_count,
                                        account.organization_name))
        total_split_count += count
        total_has_data_count += has_data_count
        total_contact_count += contact_count

    print("\n\n")
    # Get all user contacts
    seats = Seat.objects.assigned().filter(account__active=True)
    for seat in seats.iterator():
        user = seat.user
        contacts = user.contact_set.filter(contact_type="merged",
                                           is_primary=True)
        contact_count = contacts.count()
        count, has_data_count = repartition(contacts)
        print("Total merged contacts: {}. Total splits: {}. Splits with data: " \
              "{}. User: {}.".format(contact_count, count, has_data_count,
                                     user.email))
        total_split_count += count
        total_has_data_count += has_data_count
        total_contact_count += contact_count

    print("Total merged contacts: {}. Total splits: {}. Splits with data: " \
          "{}".format(total_contact_count, total_split_count,
                      total_has_data_count))


def repartition(contacts):
    count = 0
    has_data_count = 0
    for c in contacts.iterator():
        try:
            c2 = list(c.get_contacts())
        except NoChildContacts:
            continue
        if c not in c2: c2.append(c)
        partition = \
            ContactPartitioner.partition_by_addresses_and_middle_names(c2)
        if len(partition) > 1:
            count += 1
            if CallTimeContact.objects.filter(
                    contact=c).exists() or ContactNotes.objects.filter(
                contact=c).exists() or Prospect.objects.filter(
                contact=c).exists():
                has_data_count += 1
    return count, has_data_count
