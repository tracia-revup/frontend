"""Usage:
    python manage.py runscript scripts.purdue_contact_fixture_importer --script-args $PATH_TO_CONTACT_FIXTURES $USER_ID

    $PATH_TO_CONTACT_FIXTURES is the path to the purdue contact fixtures that
    Dominic dumped.

    $USER_ID is the id of the user that should be set as the owner of the new
    contacts.

    This script needs to be used instead of the loadfixtures management command
    because it creates new primary keys for the contact records, and can change
    the owner user.
"""

from io import open
from os.path import basename

from django.core.serializers.python import Deserializer as pydeserializer
from django.db import transaction
import json

from frontend.apps.contact.analysis.contact_import.base import ContactImportBase
from frontend.apps.contact.models import Contact, ImportRecord
from frontend.apps.filtering.models import ContactIndex


def create_child_map(records):
    child_map = {}
    for rec in records:
        if not rec['model'].startswith('contact') or rec['model'] in \
               ('contact.contactnotes', 'contact.contactfileuploadcache',
                'contact.contactfileupload'):
            continue
        fields = rec['fields']
        key = 'contact' if 'contact' in fields else 'parent'
        child_map.setdefault(fields[key], []).append(rec)
    return child_map


def load(records, user_id, import_record):
    child_map = create_child_map(records)
    _load(child_map, None, None, user_id=user_id, import_record=import_record)


def _load(child_map, old_parent_id, new_parent_id, user_id, import_record):
    children = child_map.get(old_parent_id, [])
    for child in children:
        old_child_id = child.pop('pk')
        parent_key = 'contact' if 'contact' in child['fields'] else 'parent'
        fields = child['fields']
        fields[parent_key] = new_parent_id
        fields.pop('city_key', None)
        fields.pop('zip_key', None)

        if 'import_record' in fields:
            fields['import_record'] = import_record.id

        if 'user' in fields:
            fields['user'] = user_id

        if fields.pop('composite_id', None):
            fields['composite_id'] = Contact._calc_composite_id(
                user_id, fields['contact_id'], fields['contact_type'])

        child_obj = next(pydeserializer([child]))
        child_obj.save()
        new_child_id = child_obj.object.pk

        if child['model'] == 'contact.contact':
            _load(child_map, old_child_id, new_child_id, user_id=user_id,
                  import_record=import_record)


@transaction.atomic
def run(fixture_path, user_id):
    fixture_records = json.load(open(fixture_path))
    label_uid = 'contact fixtures - {}'.format(basename(fixture_path))
    import_record = ImportRecord.objects.create(
        user_id=user_id,
        import_success=True,
        num_created=len(fixture_records),
        import_type=ImportRecord.TaskType.CLONED,
        label=label_uid,
        uid=label_uid
    )
    load(fixture_records, user_id, import_record)
    top_contacts = import_record.contacts.filter(is_primary=True)
    filters = import_record.user.get_index_filters()
    ContactIndex.bulk_update_index(top_contacts, filters)
    ContactImportBase.create_import_contact_sets(import_record)
