
from django.contrib.postgres.aggregates.general import ArrayAgg
from django.db.models.functions import Substr, Length
from django.db.models import F, Max

from frontend.apps.contact.deletion import ContactDeleter
from frontend.apps.contact.models import Contact
from frontend.apps.filtering.models import ContactIndex


def run(*user_ids):
    user_ids = list(map(int, user_ids))
    all_contacts = Contact.objects.all()
    if user_ids:
        all_contacts = all_contacts.filter(user_id__in=user_ids)

    # Populate needs attention on contacts that will end up with no children
    # after the split operation is complete.
    requires_needs_attention = all_contacts.select_related("user").filter(
        contact_type='merged', first_name_lower__length=1).annotate(
            max_child_name_length=Max(Length('contacts__first_name_lower')))\
            .filter(max_child_name_length=1)\
            .annotate(child_ids=ArrayAgg('contacts__id'))
    to_deactivate = []
    for contact in requires_needs_attention:
        cd = ContactDeleter(contact.user, delete_children=False)
        if not cd.has_relevant_data(contact):
            cd.delete(contact)
            continue
        to_deactivate.append(contact.id)
        contact.is_primary = False
        contact.needs_attention = True
        contact.attention_payload = {'demerge': contact.child_ids}
        contact.save(update_fields=['needs_attention', 'attention_payload',
                                    'is_primary'])
    ContactIndex.objects.filter(contact_id__in=to_deactivate).delete()

    # Break all first initial contacts off to give us a good initial state.
    all_contacts.filter(first_name_lower__length=1).exclude(
        contact_type__in=['merged', 'manual']).exclude(
            parent_id=None, is_primary=True).update(
                is_primary=True, parent_id=None)


    # Give all merged contacts with a first initial name a full name based on
    # one of their children.
    first_initial_merged_contacts = all_contacts.filter(
        first_name_lower__length=1, contact_type='merged').exclude(
            contacts=None)
    for parent in first_initial_merged_contacts:
        new_name = parent.contacts.filter(first_name_lower__length__gt=1)\
                .values_list('first_name', 'last_name', 'first_name_lower',
                             'last_name_lower', 'additional_name',
                             'name_suffix').first()
        if new_name:
            (first_name, last_name, first_name_lower, last_name_lower,
             additional_name, name_suffix) = new_name
            parent.first_name = first_name
            parent.last_name = last_name
            parent.first_name_lower = first_name_lower
            parent.last_name_lower = last_name_lower
            parent.additional_name = additional_name
            parent.name_suffix = name_suffix
            parent.save()

    # Give all manual contacts that only have a first initial for a name a full
    # name based on their merge parent.
    first_initial_manual_contacts = all_contacts.filter(
        first_name_lower__length=1, contact_type='manual').exclude(
            parent_id=None).exclude(
                parent__first_name_lower__length__lt=2).select_related(
                    'parent')
    for contact in first_initial_manual_contacts:
        parent = contact.parent
        contact.first_name = parent.first_name
        contact.last_name = parent.last_name
        contact.first_name_lower = parent.first_name_lower
        contact.last_name_lower = parent.last_name_lower
        contact.name_suffix = parent.name_suffix
        contact.additional_name = parent.additional_name
        contact.save()
