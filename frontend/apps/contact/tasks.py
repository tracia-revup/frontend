#  pylint: disable=E1102
import collections
import logging

from celery import task, result, group
from social_core.actions import do_disconnect
from social_django.utils import load_strategy, load_backend
from social_django.models import UserSocialAuth

from frontend.apps.authorize.models import RevUpUser
from frontend.apps.campaign.models import Account
from frontend.apps.contact.analysis.contact_import import run_import
from frontend.apps.contact.analysis.remerge import process_contact_ids, prepare_contact_sibling_groups
from frontend.apps.contact.deletion import ContactDeleter
from frontend.apps.contact.models import Contact, ImportRecord


LOGGER = logging.getLogger(__name__)


def on_failure(task, exc, task_id, args, kwargs, einfo):
    """Failure handler for contact_import_parallel_process task

    Find all tasks in group and revoke them so we don't do more work than we
    have to.
    """
    parent_task = result.AsyncResult(args[0].import_record.task_id)
    callback_task = parent_task.result
    if callback_task:
        callback_task.revoke(terminate=True)
    group = parent_task.children[0]
    for task in group.children:
        if task.task_id == task_id or task.ready():
            continue
        task.revoke(terminate=True)


@task(max_retries=1000, default_retry_delay=10*60, queue='contact_import')
def social_auth_import_task(user_id, social_id, account_id, feature_flags):
    from frontend.apps.authorize.models import RevUpUser
    user = RevUpUser.objects.get(pk=user_id)
    social = UserSocialAuth.objects.get(pk=social_id)
    try:
        return run_import(user, account_id, social.provider, social,
                          feature_flags=feature_flags)
    finally:
        try:
            strategy = load_strategy()
            backend = load_backend(strategy, social.provider, None)
            do_disconnect(backend, user, social.id)
        except Exception:
            LOGGER.exception("Failed to disconnect social-auth ({})".format(
                social.id))
            try:
                # If we can't do a proper disconnect, let's at least delete
                # the social record so a new one can be generated
                social.delete()
            except Exception:
                LOGGER.exception("Failed to delete social auth ({})".format(
                    social.id))


@task(max_retries=1000, default_retry_delay=10*60, queue='contact_import')
def nb_import_task(user, account, source, nation_slug, feature_flags):
    """NationBuilder contact import task"""
    run_import(user, account, source, nation_slug=nation_slug,
               feature_flags=feature_flags)


@task(max_retries=1000, default_retry_delay=10*60, queue='contact_import')
def hf_import_task(user, account, source, collection_name, feature_flags):
    """Housefile contact import task"""
    run_import(user, account, source, collection_name=collection_name,
               feature_flags=feature_flags)


@task(max_retries=1000, default_retry_delay=10*60, queue='contact_import')
def cs_import_task(user_id, source, import_id, account_id,
                   feature_flags):
    return run_import(user_id, account_id, source, import_id,
                      feature_flags=feature_flags)


@task(max_retries=1000, default_retry_delay=10*60, queue='contact_import')
def upload_file_import_task(user_id, source, source_uid, source_label,
                            upload_id, account_id, feature_flags):
    return run_import(user_id, account_id, source, source_uid, source_label,
                      upload_id, feature_flags=feature_flags)


@task(max_retries=1000, default_retry_delay=10*60, queue='contact_import')
def upload_mobile_import_task(user_id, source, source_uid, source_label,
                            upload_id, account_id, app_version, feature_flags):
    return run_import(user_id, account_id, source, app_version, source_uid,
                      source_label, upload_id, feature_flags=feature_flags)


@task(on_failure=on_failure, queue='contact_import')
def contact_import_parallel_process(importer, contacts):
    try:
        # Set the total_results to the number of contacts being processed by
        # this task, so the task's progress percent will run from 0 to 100
        importer.total_results = len(contacts)
        return importer.process_contacts(contacts)
    except:
        # Ensure that import status is recorded as errored when a failure
        # occurs, then re-raise the exception so it doesn't get lost.
        importer.complete(error=True)
        raise


@task(queue='contact_import')
def contact_import_parallel_complete(stats, importer):
    # Yes, I know it makes no sense to have stats the first argument and
    # importer the second, but this is the way it has to be in order for the
    # stupid thing to work. Because celery reasons.
    importer.complete(error=False, stats=stats)


@task(queue="contact_deletion")
def delete_contacts_task(user_id, contact_ids,
                         delete_children=True, clean_parents=False):
    """Delete given contacts.
    This is mostly meant to be used us the parallel processor for ContactSet
    delete, but this can be used on its own.
    """
    user = RevUpUser.objects.get(id=user_id)
    if not isinstance(contact_ids, collections.Iterable):
        contact_ids = [contact_ids]
    contacts = Contact.objects.filter(id__in=contact_ids)
    LOGGER.info("Delete {} contacts for user: {}".format(
        len(contact_ids), contacts[0].user))

    results = ContactDeleter(user,
                             delete_children=delete_children,
                             clean_parents=clean_parents,
                             update_progress=True).delete(contacts)
    LOGGER.info(results)
    return results


@task(queue='contact_merging')
def contact_merging(user_id, import_record_id, feature_flags,
                    updated_contact_ids=None):
    """Run contact merging on a given user.

    After merging is complete contact set counts are updated and analysis is
    kicked off.

    `import_record_id` and `feature_flags` are present only so they may be
    passed along to analysis.

    Supports restricting contact merging only to sibling groups that have a
    contact id in common with a set of contact ids provided via
    `updated_contact_ids`.
    """
    updated_contact_ids = set(updated_contact_ids or [])
    user = RevUpUser.objects.get(id=user_id)
    grouped_contacts = prepare_contact_sibling_groups(user)
    tasks = (group(
        contact_sibling_group_merge_processing.s(user_id, grp)
        for grp in grouped_contacts
        if not updated_contact_ids or not updated_contact_ids.isdisjoint(grp)) |
        contact_merging_complete.si(user_id, import_record_id, feature_flags)
    ).apply_async(add_to_parent=True)
    return tasks


@task(queue='contact_merging')
def contact_sibling_group_merge_processing(user_id, contact_ids):
    """Process a contact sibling group

    Applies partitioning rules to a group of contacts, persists changes (if
    any), and updates the contact search index.
    """
    process_contact_ids(contact_ids, user_id)


@task(queue='contact_merging')
def contact_merging_complete(user_id, import_record_id, feature_flags):
    """Task to run after contact merging subtasks finish

    - Disables childless merge contacts.
    - Runs `ContactImportBase.update_contact_sets` which:
        - updates contact set counts.
        - queues analysis.
    """
    from frontend.apps.contact.analysis.contact_import.base import ContactImportBase
    user = RevUpUser.objects.get(id=user_id)
    import_record = ImportRecord.objects.get(id=import_record_id)
    # set is_primary to False on childless parents
    Contact.objects.filter(
        user=user, contact_type='merged', is_primary=True,
        contacts__isnull=True).update(is_primary=False)
    # update contact set counts
    # schedule analysis
    ContactImportBase.update_contact_sets(
        import_record=import_record,
        user=user,
        feature_flags=feature_flags
    )
