import logging

from celery.utils import uuid
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.http import (
    Http404, HttpResponse, HttpResponseForbidden, HttpResponseBadRequest)
from django.shortcuts import redirect, render
from django.template import RequestContext
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.generic import View
from rest_framework import exceptions
from social_django.models import UserSocialAuth

from frontend.apps.authorize.models import RevUpTask
from frontend.apps.campaign.routes import ROUTES
from frontend.apps.contact.models import ContactFileUpload, FileFormatError
from frontend.apps.contact.tasks import upload_file_import_task
from frontend.apps.contact.task_utils import (start_contact_import,
                                              start_cs_contact_import)
from frontend.apps.contact.utils import check_account_level_import
from frontend.apps.role.permissions import AdminPermissions
from frontend.libs.django_view_router import RoutingTemplateView
from frontend.libs.utils.api_utils import APIException
from frontend.libs.utils.tracker_utils import action_send
from frontend.libs.utils.task_utils import (
    WaffleFlagsMixin, CONTACT_IMPORT_FLAGS, ANALYSIS_FLAGS)


LOGGER = logging.getLogger(__name__)


class ContactImportSocial(View, WaffleFlagsMixin):
    flag_names = CONTACT_IMPORT_FLAGS + ANALYSIS_FLAGS
    
    @method_decorator(login_required)
    def get(self, request, social_id):
        try:
            account = check_account_level_import(request)
        except exceptions.PermissionDenied as err:
            return HttpResponseForbidden(str(err))
        social = UserSocialAuth.objects.get(pk=social_id)
        if request.user != social.user:
            LOGGER.error('User {} tried to access invalid social auth: '
                         '{}'.format(request.user, social))
            raise Http404
        start_contact_import(request.user, social, account,
                             self._prepare_flags(request))
        action_send(request, request.user, verb="imported contacts",
                    source=social.provider)
        return redirect(request.META['HTTP_REFERER'])


class ContactImportCS(View, WaffleFlagsMixin):
    flag_names = CONTACT_IMPORT_FLAGS + ANALYSIS_FLAGS

    @method_decorator(login_required)
    def post(self, request, source):
        try:
            account = check_account_level_import(request)
        except exceptions.PermissionDenied as err:
            return HttpResponseForbidden(str(err))
        import_id = request.POST.get('import_id')
        if not import_id:
            return HttpResponse("'import_id' required", status=400)
        start_cs_contact_import(request.user, source, import_id,
                                account, self._prepare_flags(request))
        action_send(request, request.user, verb="imported contacts",
                    source=source)
        return HttpResponse()


@login_required
def contact_list(request):
    associations = []
    for social in UserSocialAuth.get_social_auth_for_user(request.user):
        if social.provider == "google-oauth2":
            associations.append((social.uid, social))
        elif social.provider == "linkedin-oauth2":
            associations.append(("{} {}".format(
                social.extra_data['first_name'],
                social.extra_data['last_name']), social))
    return render(request, 'contact/contact_list.html',
                  RequestContext(request))


class ContactManagerView(RoutingTemplateView):
    routes = ROUTES

    template_name = "contact/contact_manager.html"

    @method_decorator(login_required)
    @method_decorator(ensure_csrf_cookie)
    def do_dispatch(self, request, *args, **kwargs):
        return super(ContactManagerView, self).do_dispatch(request,
                                                           *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ContactManagerView, self).get_context_data(**kwargs)
        user = self.request.user
        has_account_contact_permission = user.current_seat.has_permissions(
            any=[AdminPermissions.MODIFY_CONTACTS])
        context.update({
            "account": user.current_seat.account,
            "analysis": user.get_default_analysis(),
            "has_account_contact_permission": has_account_contact_permission,
        })
        return context


class ContactUploadView(View, WaffleFlagsMixin):
    flag_names = CONTACT_IMPORT_FLAGS + ANALYSIS_FLAGS

    @method_decorator(login_required)
    def post(self, request, **kwargs):
        try:
            account = check_account_level_import(request)
            CONTACT_IMPORT_TASKS = RevUpTask.TaskType.CONTACT_IMPORT_TASKS
            file_type = request.POST.get(
                'file_type', ContactFileUpload.FileType.CSV)
            file_ = request.FILES.get('file', None)
            if not file_type or not file_:
                raise APIException(
                    LOGGER,
                    'Upload external data expects a "file_type" and "file".',
                    request)

            if file_type not in ContactFileUpload.FileType.all():
                raise APIException(LOGGER, "Unsupported file type.", request,
                                   file_type)

            file_name = file_.name
            file_content = file_.read()
            if not file_content:
                raise APIException(
                    LOGGER, "The uploaded file is empty.", request)

            with transaction.atomic():
                task_lock = request.user.lock()
                request.user.clean_stale_tasks(
                    task_type=CONTACT_IMPORT_TASKS,
                    task_lock=task_lock)

                task_kwargs = dict(
                    label=file_name,
                    user=request.user, account=account,
                    task_type=RevUpTask.TaskType.CSV_IMPORT)

                # Prevent the same import file from scheduling twice
                tasks = RevUpTask.objects.filter(**task_kwargs)
                for task in tasks:
                    if task.pending():
                        raise APIException(LOGGER,
                                           "This file is already importing",
                                           request, task_kwargs)

                # Generate a task and save the uploaded file for processing
                task_id = uuid()
                RevUpTask.objects.create(task_id=task_id, **task_kwargs)
                try:
                    # Store the file for use by Celery
                    cfu = ContactFileUpload.create(request.user, file_type,
                                                   file_name, file_content)
                except FileFormatError as err:
                    raise APIException(LOGGER, str(err), request)

                if not (task_id and cfu):
                    raise RuntimeError("Failed to import tasks. Reason "
                                       "unknown. User: {}, filename: {}"
                                       .format(request.user, file_name))

                # Trigger a Celery task. Delay it so the transaction can commit
                upload_file_import_task.apply_async(
                    args=(request.user.id, file_type.lower(), file_name,
                          file_name, cfu.id,
                          account.id if account else None,
                          self._prepare_flags(request)),
                    task_id=task_id,
                    countdown=3)
        except exceptions.PermissionDenied as err:
            return HttpResponseForbidden(str(err))
        except APIException as err:
            return HttpResponseBadRequest(str(err))
        return HttpResponse()
