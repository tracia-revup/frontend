
from django.test import override_settings
from faker import Factory
from unittest.mock import patch, MagicMock, DEFAULT, call

from frontend.apps.authorize.factories import (RevUpUserFactory,
                                               UserSocialAuthFactory)
from frontend.apps.campaign.factories import PoliticalCampaignFactory
from frontend.apps.contact.analysis.contact_import import (
    social_auth, csv, cloudsponge, cloning)
from frontend.apps.contact.analysis.contact_import.base import (
    ContactImportBase)
from frontend.apps.contact.factories import (
    ImportRecordFactory, ContactFileUploadFactory, ContactFactory)
from frontend.apps.contact.models import ContactFileUpload
from frontend.apps.contact_set.factories import ContactSetFactory
from frontend.apps.seat.factories import SeatFactory
from frontend.apps.seat.models import Seat
from frontend.libs.utils.test_utils import TestCase, expect


class ContactImportBaseTests(TestCase):
    def setUp(self):
        from frontend.apps.filtering.filter_controllers.contacts import imports
        from frontend.apps.filtering.factories import FilterFactory, Filter
        try:
            Filter.objects.get(title="Import Source Detail")
        except Filter.DoesNotExist:
            self.filter = FilterFactory.build(title="Import Source Detail",
                                              filter_type="CN")
            self.filter.dynamic_class = imports.ImportSourceDetailFilter
            self.filter.save()

    def test_run_import(self):
        ## Verify the run_import method
        # Setup the test
        task_mock = MagicMock()
        task_mock.blocked.return_value = False
        with patch("frontend.apps.contact.analysis.contact_import"
                   ".base.get_task_record", MagicMock(return_value=task_mock)):
            user = RevUpUserFactory()
            source = "test"
            cib = ContactImportBase(user, source)

            # Verify import record is updated - positive case
            m = MagicMock()
            m.import_success = None
            c = MagicMock()
            cib.generate_import_record = MagicMock(return_value=m)
            cib.get_raw_contacts = MagicMock(return_value=[c])
            cib.build_contact = MagicMock(return_value=None)
            self.assertIsNone(m.import_success)
            cib.execute()
            self.assertTrue(m.import_success)
            self.assertTrue(m.save.called)
            # Verify import record is passed to build_contact as a kwarg
            cib.build_contact.assert_called_once_with(c)

            # Verify import record is updated - error case
            m.reset_mock()
            cib.get_raw_contacts = MagicMock(side_effect=ValueError)
            self.assertRaises(ValueError, cib.execute)
            self.assertFalse(m.import_success)
            self.assertTrue(m.save.called)

    @patch.multiple(ContactImportBase,
                    _update_account_contact_sets=DEFAULT,
                    _update_user_contact_sets=DEFAULT)
    def test_cs_update_routing(self, _update_account_contact_sets,
                               _update_user_contact_sets):
        _update_account_contact_sets.return_value = MagicMock(id=1)
        _update_user_contact_sets.return_value = [MagicMock(id=2)]
        account = PoliticalCampaignFactory()
        account_user = account.account_user
        normie_user = RevUpUserFactory()

        # try account user first
        ContactImportBase.update_contact_sets(None, account_user, None)
        _update_account_contact_sets.assert_called_once()
        _update_user_contact_sets.assert_not_called()

        # reset
        _update_account_contact_sets.reset_mock()
        _update_user_contact_sets.reset_mock()

        # try normie
        ContactImportBase.update_contact_sets(None, normie_user, None)
        _update_account_contact_sets.assert_not_called()
        _update_user_contact_sets.assert_called_once()

    @patch("frontend.apps.contact.analysis.contact_import.base.submit_analysis")
    @patch("frontend.apps.seat.models.DelayedAnalysis.bulk_schedule")
    @patch('frontend.apps.contact_set.models.ContactSet.update_count',
           autospec=True)
    def test_user_cs_update(self, update_count, bulk_schedule, submit_analysis):
        # Expectations:
        # - only contact sets for seat2, seat3, and seat4 should have
        # `update_count()` run on them and be returned.
        # - `submit_analysis` should only be run on the CS for the active seat
        # (seat4)
        # - Nothing is done with the CS for seat1 as seat1 is revoked.
        # - No analysis (delayed or immediate) should be scheduled for seat2 CS
        # as the account is not active.
        # - No delayed analysis scheduled for admin users.
        import_record = ImportRecordFactory()
        user = import_record.user
        seat1 = SeatFactory(user=user, state=Seat.States.REVOKED)
        cs1 = ContactSetFactory(user=user, account=seat1.account,
                                source=import_record.import_type,
                                source_uid=import_record.uid)
        seat2 = SeatFactory(user=user, account__active=False)
        cs2 = ContactSetFactory(user=user, account=seat2.account,
                                source=import_record.import_type,
                                source_uid=import_record.uid)
        seat3 = SeatFactory(user=user)
        cs3 = ContactSetFactory(user=user, account=seat3.account,
                                source=import_record.import_type,
                                source_uid=import_record.uid)
        seat4 = SeatFactory(user=user)
        cs4 = ContactSetFactory(user=user, account=seat4.account,
                                source=import_record.import_type,
                                source_uid=import_record.uid)
        user.current_seat = seat4
        user.save()

        # Test case for normal user
        updated_cs = ContactImportBase._update_user_contact_sets(import_record,
                                                                 user, None)
        # Verify `update_count()` was called for all CS except for the one on
        # the revoked seat.
        expect(updated_cs) == {cs2, cs3, cs4}
        expect(update_count.call_count) == 3
        update_count.assert_has_calls([call(cs2), call(cs3), call(cs4)],
                                      any_order=True)

        submit_analysis.assert_called_once_with(
            user, seat4.account, cs4, countdown=10, add_to_parent=False,
            flags=None, transaction_id=import_record.transaction_id,
            expected_count=import_record.num_valid)
        bulk_schedule.assert_called_once_with(tuple([seat3]), tuple([cs3]))

        # Reset mocks
        bulk_schedule.reset_mock()
        submit_analysis.reset_mock()
        update_count.reset_mock()

        # Test case for admin user
        # No delayed analysis should be scheduled
        user.is_staff = True
        user.save()
        updated_cs = ContactImportBase._update_user_contact_sets(import_record,
                                                                 user, None)
        expect(updated_cs) == {cs2, cs3, cs4}
        expect(update_count.call_count) == 3
        update_count.assert_has_calls([call(cs2), call(cs3), call(cs4)],
                                      any_order=True)
        submit_analysis.assert_called_once()
        bulk_schedule.assert_not_called()

    @patch("frontend.apps.contact.analysis.contact_import.base.submit_analysis")
    @patch('frontend.apps.contact_set.models.ContactSet.update_count',
           autospec=True)
    def test_account_cs_update(self, update_count, submit_analysis):
        account = PoliticalCampaignFactory()
        account_user = account.account_user
        import_record = ImportRecordFactory(account=account)
        cs = ContactSetFactory(user=account_user, account=account,
                               source=import_record.import_type,
                               source_uid=import_record.uid)
        updated_cs = ContactImportBase._update_account_contact_sets(
            import_record, account_user, None)
        expect(updated_cs) == cs
        update_count.assert_called_once_with(cs)
        submit_analysis.assert_called_once_with(
            account_user, account, cs, countdown=10, add_to_parent=False,
            flags=None, transaction_id=import_record.transaction_id,
            expected_count=import_record.num_valid)


class GMailImportTests(TestCase):
    def setUp(self):
        self.user = RevUpUserFactory()
        self.source = "google-oauth2"
        self.social = UserSocialAuthFactory(user=self.user,
                                            provider=self.source)
        self.importer = social_auth.GMailImport(
            self.user, self.source, self.social)

    def test_generate_import_record(self):
        import_record = self.importer.generate_import_record()
        self.assertEqual(import_record.user, self.user)
        self.assertEqual(import_record.import_type, "GM")
        self.assertEqual(import_record.uid, self.social.uid)
        self.assertEqual(import_record.label, self.social.uid)

    @patch('frontend.apps.contact.analysis.contact_import.social_auth.etree')
    def test_build_contact(self, *args):
        self.importer.import_record = import_record = ImportRecordFactory()
        faker = Factory.create()
        contact_data = {
            "a:id": faker.md5(),
            "g:name/g:givenName": faker.first_name(),
            "g:name/g:additionalName": faker.first_name(),
            "g:name/g:familyName": faker.last_name(),
        }

        def _data_getter(key, *args, **kwargs):
            return contact_data.get(key, kwargs.get('default'))

        raw_contact = MagicMock()
        raw_contact.findtext.side_effect = _data_getter
        raw_contact.xpath.return_value = []

        contact = self.importer.build_contact(raw_contact)

        self.assertEqual(contact.user, self.user)
        self.assertEqual(contact.contact_id,
                         contact_data["a:id"])
        self.assertEqual(contact.first_name,
                         contact_data["g:name/g:givenName"])
        self.assertEqual(contact.first_name_lower,
                         contact_data["g:name/g:givenName"].lower())
        self.assertEqual(contact.last_name,
                         contact_data["g:name/g:familyName"])
        self.assertEqual(contact.last_name_lower,
                         contact_data["g:name/g:familyName"].lower())
        self.assertIs(contact.import_record, import_record)


class LinkedInImportTests(TestCase):
    def setUp(self):
        self.user = RevUpUserFactory()
        self.source = "linkedin-oauth2"
        self.extra_data = {'first_name': "first", "last_name": "last",
                           "access_token": "token"}
        self.social = UserSocialAuthFactory(user=self.user,
                                            provider=self.source,
                                            extra_data=self.extra_data)

        self.importer = social_auth.LinkedInImport(
            self.user, self.source, self.social)

    def test_generate_import_record(self):
        import_record = self.importer.generate_import_record()
        self.assertEqual(import_record.user, self.user)
        self.assertEqual(import_record.import_type, "LI")
        self.assertEqual(import_record.uid, self.social.uid)
        self.assertEqual(import_record.label, "first last")

    def test_build_contact(self, *args):
        self.importer.import_record = import_record = ImportRecordFactory()
        faker = Factory.create()
        raw_contact = {
            'id': faker.md5(),
            'firstName': faker.first_name(),
            'lastName': faker.last_name(),
            'location': {'name': None},
        }

        contact = self.importer.build_contact(raw_contact)

        self.assertEqual(contact.user, self.user)
        self.assertEqual(contact.contact_id,
                         raw_contact["id"])
        self.assertEqual(contact.first_name,
                         raw_contact["firstName"])
        self.assertEqual(contact.first_name_lower,
                         raw_contact["firstName"].lower())
        self.assertEqual(contact.last_name,
                         raw_contact["lastName"])
        self.assertEqual(contact.last_name_lower,
                         raw_contact["lastName"].lower())
        self.assertIs(contact.import_record, import_record)


# Facebook tests disabled as we don't actually use the facebook importer.
# Having to fix tests for an importer we don't actually use is absurd and
# costing us money.
#class FacebookImportTests(TestCase):
    #def setUp(self):
        #self.user = RevUpUserFactory()
        #self.source = "facebook"
        #self.social = UserSocialAuthFactory(user=self.user,
                                            #provider=self.source)

        #self.importer = social_auth.FacebookImport(
            #self.user, self.source, self.social)

    #def test_generate_import_record(self):
        #import_record = self.importer.generate_import_record()
        #self.assertEqual(import_record.user, self.user)
        #self.assertEqual(import_record.import_type, "FB")
        #self.assertEqual(import_record.uid, self.social.uid)
        #self.assertEqual(import_record.label, self.social.uid)

    #def test_build_contact(self, *args):
        #self.importer.import_record = import_record = ImportRecordFactory()
        #friend1 = {'id': 'abc', 'name': "Wilber t. Smith"}
        #friend2 = {'id': '123', 'name': "Holmes, Oliver M, M.D."}

        #contact = self.importer.build_contact(friend1)
        #self.assertEqual(contact.last_name, 'Smith')
        #self.assertEqual(contact.last_name_lower, 'smith')
        #self.assertEqual(contact.first_name, 'Wilber')
        #self.assertEqual(contact.first_name_lower, 'wilber')
        #self.assertIs(contact.import_record, import_record)

        #contact = self.importer.build_contact(friend2)
        #self.assertEqual(contact.last_name, 'Holmes')
        #self.assertEqual(contact.last_name_lower, 'holmes')
        #self.assertEqual(contact.first_name, 'Oliver')
        #self.assertEqual(contact.first_name_lower, 'oliver')
        #self.assertIs(contact.import_record, import_record)


# Twitter tests disabled as we don't actually use the twitter importer.
# Having to fix tests for an importer we don't actually use is absurd and
# costing us money.
#class TwitterImportTests(TestCase):
    #def setUp(self):
        #self.user = RevUpUserFactory()
        #self.source = "twitter"
        #self.social = UserSocialAuthFactory(user=self.user,
                                            #provider=self.source)

        #self.importer = social_auth.TwitterImport(
            #self.user, self.source, self.social)

    #def test_generate_import_record(self):
        #import_record = self.importer.generate_import_record()
        #self.assertEqual(import_record.user, self.user)
        #self.assertEqual(import_record.import_type, "TW")
        #self.assertEqual(import_record.uid, self.social.uid)
        #self.assertEqual(import_record.label, self.social.uid)

    #def test_build_contact(self, *args):
        #self.importer.import_record = import_record = ImportRecordFactory()
        #mock_tu1 = MagicMock()
        #mock_tu2 = MagicMock()

        #mock_tu1.id = 'abc'
        #mock_tu1.name = "Wilber t. Smith"
        #mock_tu1.screen_name = 'Willy'
        #contact = self.importer.build_contact(mock_tu1)
        #self.assertEqual(contact.last_name, 'Smith')
        #self.assertEqual(contact.last_name_lower, 'smith')
        #self.assertEqual(contact.first_name, 'Wilber')
        #self.assertEqual(contact.first_name_lower, 'wilber')
        #self.assertIs(contact.import_record, import_record)

        #mock_tu2.id = '123'
        #mock_tu2.name = "Holmes, Oliver M, M.D."
        #mock_tu2.screen_name = 'Ollie'
        #contact = self.importer.build_contact(mock_tu2)
        #self.assertEqual(contact.last_name, 'Holmes')
        #self.assertEqual(contact.last_name_lower, 'holmes')
        #self.assertEqual(contact.first_name, 'Oliver')
        #self.assertEqual(contact.first_name_lower, 'oliver')
        #self.assertIs(contact.import_record, import_record)


class CSVImportTests(TestCase):
    def setUp(self):
        self.seat = SeatFactory(user__is_active=False)
        self.user = self.seat.user
        self.campaign = self.seat.account
        self.user.current_seat = self.seat
        self.user.save()

        self.source = "csv"
        self.contact_file_upload = ContactFileUploadFactory(user=self.user)
        self.importer = csv.CSVImport(
            self.user, self.source,
            self.contact_file_upload.file_name,
            self.contact_file_upload.file_name,
            self.contact_file_upload.pk)

    def test_create_import_file_name_exact_length(self):
        """This is a test for a file_name that is exactly 100 in length.
        This file_name should be unchanged."""
        self.setUp()
        file_name = '01234567890123456789012345678901234567890123456789' \
                    '0123456789012345678901234567890123456789012345.csv'
        contact_file_upload = ContactFileUpload.create(
            user=self.user,
            file_type='csv',
            file_name=file_name,
            file_content=''
        )
        self.assertEqual(len(contact_file_upload.file_name), len(file_name))

    def test_create_import_file_name_unchanged(self):
        """Test short file name, should be unchanged by the create method."""
        self.setUp()
        file_name = 'upload_file.csv'
        contact_file_upload = ContactFileUpload.create(
            user=self.user,
            file_type='csv',
            file_name=file_name,
            file_content=''
        )
        self.assertEqual(len(file_name), len(contact_file_upload.file_name))

    def test_create_import_long_file_name(self):
        """Test file_name too long.  Name should be unchanged and file creation
        should be successful as file_name length is up to 512."""
        self.setUp()
        file_name = 'lkjsldkjflkasjdlfkjalsdkjflaskdjflaksdjflaksdjflkjsdf' \
                    'lksjdlkfjslkdjflksjdflkjsdlkfjlskdjflksjdflksjdlkfj' \
                    'lkjsdlkfjlskdjflskjdflksjdflksjdflksjdlfkjslkdjflskdjf' \
                    'lksjdlkfjslkdjflskdjf.csv'
        contact_file_upload = ContactFileUpload.create(
            user=self.user,
            file_type='csv',
            file_name=file_name,
            file_content=''
        )
        self.assertEqual(len(contact_file_upload.file_name), len(file_name))

    def test_create_import_long_after_extension_file_name(self):
        """Tests file_name too long, specifically the extension is too long.
        Name should be unchanged and file creation should be successful as
        file_name length is up to 512."""
        self.setUp()
        file_name = 'lkjsldkjflkasjdlfkjalsdkjflaskdjflaksdjflaksdjflkjsdf' \
                    'lksjdlkfjslkdjflksjdflkjsdlkfjlskdjflksjdflksjdlkfj' \
                    'lkjsdlkfjlskdjflskjdflksjdflksjdflksjdlfkjslkdjflskdjf' \
                    'lksjdlkfjslkdjflskdjf.csv'
        contact_file_upload = ContactFileUpload.create(
            user=self.user,
            file_type='csv',
            file_name=file_name,
            file_content=''
        )
        self.assertEqual(len(contact_file_upload.file_name), len(file_name))

    def test_generate_import_record(self):
        import_record = self.importer.generate_import_record()
        self.assertEqual(import_record.user, self.user)
        self.assertEqual(import_record.import_type, "CV")
        self.assertEqual(import_record.uid, self.contact_file_upload.file_name)
        self.assertEqual(import_record.label,
                         self.contact_file_upload.file_name)

    def test_build_contact(self, *args):
        self.importer.import_record = import_record = ImportRecordFactory()
        faker = Factory.create()
        self.importer.header_map = {
            'first name': [0],
            'last name': [1],
        }
        raw_contact = [faker.first_name(), faker.last_name()]

        contact = self.importer.build_contact(raw_contact)

        self.assertEqual(contact.user, self.user)
        self.assertEqual(contact.first_name,
                         raw_contact[0])
        self.assertEqual(contact.first_name_lower,
                         raw_contact[0].lower())
        self.assertEqual(contact.last_name,
                         raw_contact[1])
        self.assertEqual(contact.last_name_lower,
                         raw_contact[1].lower())
        self.assertIs(contact.import_record, import_record)


class CloudspongeImportTests(TestCase):
    def setUp(self):
        self.user = RevUpUserFactory()
        self.source = "outlook"
        self.importer = cloudsponge.CloudSpongeImport(
            self.user, self.source, 'myimport_id')

    def test_generate_import_record(self):
        import_record = self.importer.generate_import_record()
        self.assertEqual(import_record.user, self.user)
        self.assertEqual(import_record.import_type, "OU")
        self.assertEqual(import_record.uid, "outlook")
        self.assertEqual(import_record.label, "Outlook")

    def test_build_contact(self, *args):
        self.importer.import_record = import_record = ImportRecordFactory()
        faker = Factory.create()
        raw_contact = MagicMock()
        raw_contact.first_name = faker.first_name()
        raw_contact.last_name = faker.last_name()
        raw_contact.contact_id = faker.md5()
        raw_contact.phones = []
        raw_contact.addresses = []
        raw_contact.emails = []

        contact = self.importer.build_contact(raw_contact)

        self.assertEqual(contact.user, self.user)
        self.assertEqual(contact.contact_id,
                         raw_contact.contact_id)
        self.assertEqual(contact.first_name,
                         raw_contact.first_name)
        self.assertEqual(contact.first_name_lower,
                         raw_contact.first_name.lower())
        self.assertEqual(contact.last_name,
                         raw_contact.last_name)
        self.assertEqual(contact.last_name_lower,
                         raw_contact.last_name.lower())
        self.assertIs(contact.import_record, import_record)


class CloningImportTests(TestCase):
    def setUp(self):
        from frontend.apps.contact_set.factories import ContactSetFactory
        self.contact_set = ContactSetFactory(source="GM")
        self.user = self.contact_set.user
        self.account = self.contact_set.account
        self.account_user = self.account.account_user
        self.assertNotEqual(self.user, self.account_user)
        self.assertTrue(self.contact_set.is_import_autogen)
        self.maxDiff = None

        self.source = "cloned"
        self.importer = cloning.CloningImport(
            self.user, self.source, self.contact_set, account=self.account)

    def test_generate_import_record(self):
        import_record = self.importer.generate_import_record()
        self.assertEqual(import_record.user, self.user)
        self.assertEqual(import_record.account, self.account)
        self.assertEqual(import_record.import_type, "CL")
        self.assertEqual(import_record.uid, self.user.username)
        self.assertEqual(import_record.label, "{}'s Contacts".format(
            self.user.name))

    def test_build_contact(self):
        contact = ContactFactory(user=self.user)
        b_contact = self.importer.build_contact(contact.id)
        self.assertCountEqual([
            contact.first_name,
            contact.last_name,
            contact.first_name_lower,
            contact.last_name_lower,
            contact.contact_id,
            contact.contact_type,
            contact.is_primary
        ],
        [
            b_contact.first_name,
            b_contact.last_name,
            b_contact.first_name_lower,
            b_contact.last_name_lower,
            b_contact.contact_id,
            b_contact.contact_type,
            b_contact.is_primary
        ])
        self.assertEqual(contact.user, self.user)
        self.assertEqual(b_contact.user, self.account_user)
        self.assertEqual(contact.addresses.first().format_for_kafka(),
                         b_contact.addresses["objects"][0].format_for_kafka())
        self.assertEqual(contact.phone_numbers.first().format_for_kafka(),
                         b_contact.phone_numbers["objects"][0].format_for_kafka())
        self.assertEqual(contact.email_addresses.first().format_for_kafka(),
                         b_contact.email_addresses["objects"][0].format_for_kafka())
