from faker import Factory as FakerFactory
from pytest import fixture, mark, raises
import unittest.mock as mock

from frontend.apps.authorize.factories import (
    RevUpUserFactory, UserSocialAuthFactory, RevUpTaskFactory)
from frontend.apps.authorize.models import RevUpTask, RevUpUser
from frontend.apps.contact.factories import (
    ContactFactory, Contact, PhoneNumberFactory, ImportRecordFactory,
    EmailAddress)
from frontend.apps.contact.tasks import social_auth_import_task
from frontend.apps.contact.analysis.contact_import.utils import ContactWrapper
from frontend.apps.contact.models import Address
from frontend.apps.contact.task_utils import start_contact_merging
from frontend.apps.locations.factories import (
    LinkedInLocationFactory, ZipCodeFactory, CityFactory, CityAliasFactory)
from frontend.apps.locations.models import ZipCode, City
from frontend.libs.utils.address_utils import AddressParser
from frontend.libs.utils.test_utils import TestCase


faker = FakerFactory.create()


class ContactModelTests(TestCase):
    def setUp(self):
        # Setup url where logged-in user owns a contact
        self.contact = ContactFactory(contact_type="merged",
                                      email_addresses=[])
        self.user = self.contact.user
        self.subcontacts = [ContactFactory(is_primary=False, user=self.user,
                                           parent=self.contact)
                            for i in range(3)]

    def test_get_oldest_ancestor(self):
        ## Verify oldest ancestor from a subcontact is the parent
        ancestor = self.subcontacts[0].get_oldest_ancestor()
        self.assertEqual(self.contact, ancestor)

        ## Verify another case
        ancestor = self.subcontacts[1].get_oldest_ancestor()
        self.assertEqual(self.contact, ancestor)

        ## Verify oldest ancestor of primary contact is itself
        ancestor = self.contact.get_oldest_ancestor()
        self.assertEqual(self.contact, ancestor)

    def test_get_contacts(self):
        """Verify get_contacts returns the correct contacts"""
        ## Verify the single contact case
        contact = ContactFactory()
        self.assertCountEqual(contact.get_contacts(), [contact])

        ## Verify the merged case
        self.assertCountEqual(self.contact.get_contacts(), self.subcontacts)

        ## Verify the childless-merged (zombie) case
        # Remove all children but don't make parent a zombie yet
        for sc in self.subcontacts:
            sc.parent = None
            sc.is_primary = True
            sc.save()
        del self.contact._get_contacts
        self.assertCountEqual(self.contact.get_contacts(), [self.contact])

        # Make parent a zombie and verify the children are returned
        self.contact.needs_attention = True
        self.contact.attention_payload = {"demerge":
                                              [c.id for c in self.subcontacts]}
        self.contact.is_primary = False
        self.contact.save()
        del self.contact._get_contacts
        self.contact.get_contacts()
        self.assertCountEqual(self.contact.get_contacts(), self.subcontacts)

    def test_truncation(self):
        long_string = "a" * 500
        self.contact.first_name = long_string
        self.contact.last_name = "normal_length"
        self.contact.save()

        contact = Contact.objects.get(id=self.contact.id)
        self.assertEqual(len(contact.first_name), 128)
        self.assertEqual(contact.last_name, self.contact.last_name)

    def test_is_same_person(self):
        ## Verify various different comparisons of contacts
        contact = ContactFactory(contact_type="merged",
                                 email_addresses=[])
        subcontact = ContactFactory(parent=contact,
                                    is_primary=False)

        ## Verify negative case
        self.assertFalse(contact.is_same_person(self.contact))
        subcontact.delete()
        ## Verify gmail positive case
        subcontact = ContactFactory(
            is_primary=False,
            contact_type="google-oauth2",
            parent=contact)
        e = subcontact.email_addresses.first()
        e.address = self.subcontacts[0].email_addresses.first().address
        e.save()

        del contact._get_contacts
        del self.contact._get_contacts
        self.assertTrue(contact.is_same_person(self.contact))
        subcontact.delete()

        ## Verify non gmail negative case
        subcontact = ContactFactory(contact_type="linkedin-oauth2",
                                    is_primary=False,
                                    parent=contact)
        del contact._get_contacts
        del self.contact._get_contacts
        self.assertFalse(contact.is_same_person(self.contact))

        ## Verify non gmail positive case
        self.subcontacts[0].contact_type = "linkedin-oauth2"
        self.subcontacts[0].contact_id = subcontact.contact_id
        self.subcontacts[0].save()
        del contact._get_contacts
        del self.contact._get_contacts
        self.assertTrue(contact.is_same_person(self.contact))

        ## Verify case where contact is primary
        contact = ContactFactory(contact_type="linkedin-oauth2",
                                 contact_id=self.subcontacts[0].contact_id)
        self.assertTrue(contact.is_same_person(self.contact))

    def test_equality(self):
        contact = ContactFactory()
        self.assertNotEqual(contact, self.contact)
        self.assertNotEqual(self.contact, contact)
        self.assertEqual(contact, contact)
        self.assertNotEqual(contact, None)
        self.assertNotEqual(contact, {})

        wrapper = ContactWrapper(contact=contact)
        self.assertNotEqual(self.contact, wrapper)
        self.assertNotEqual(wrapper, self.contact)
        self.assertEqual(contact, wrapper)
        self.assertEqual(wrapper, contact)
        self.assertNotEqual(wrapper, None)

        w2 = ContactWrapper(contact=contact)
        w3 = ContactWrapper(contact=self.contact)
        self.assertEqual(wrapper, w2)
        self.assertEqual(w2, wrapper)
        self.assertNotEqual(wrapper, w3)
        self.assertNotEqual(w3, wrapper)

    def test_compare_generational_suffixes(self):
        ccgs = Contact.compare_generational_suffixes
        self.assertTrue(ccgs("jr", "junior"))
        self.assertTrue(ccgs("jr", "jr"))
        self.assertTrue(ccgs("Sr.", "senior"))
        self.assertTrue(ccgs("iii", "III"))
        self.assertTrue(ccgs("iii", "3rd"))
        # Make sure we can pick Jr out even when it is surrounded by other
        # words and punctuation characters
        self.assertTrue(ccgs('MBA, (JR.), PHD', "junior"))
        self.assertTrue(ccgs(None, " "))

        self.assertFalse(ccgs('Sr.', "Jr."))
        self.assertFalse(ccgs(None, "III"))
        self.assertFalse(ccgs("jr", "III"))
        self.assertFalse(ccgs("sr", "iv"))
        self.assertFalse(ccgs("iii", "iv"))
        # Make sure we do not erroneously detect 'Jr' as being part of a name
        # if the letters 'jr' happen to be part of a larger word.
        self.assertFalse(ccgs('red herring somethingjrblah', "jr"))

    def test_compare_middle_names(self):
        ccmn = Contact.compare_middle_names
        self.assertFalse(ccmn("Frederick", "Florence"))
        self.assertFalse(ccmn("JAMEs", "JONEs"))
        self.assertFalse(ccmn("Stephen", "Wardell"))
        self.assertFalse(ccmn("F.", "Florance"))
        self.assertFalse(ccmn("J", "K"))
        self.assertFalse(ccmn("J", "Kevin"))
        # Should be specifically False, not None
        self.assertIsNotNone(ccmn("J", "K"))
        self.assertIsNotNone(ccmn("J", "Kevin"))

        self.assertIsNone(ccmn("J", ""))
        self.assertIsNone(ccmn("J", "JONEs"))
        self.assertIsNone(ccmn("J.", "JO"))
        self.assertTrue(ccmn("Stephen", "Stephen"))
        self.assertTrue(ccmn("St.GErmain", "stgermain."))

    def test_decompose(self):
        def get_and_strip_attributes(attr_name):
            attrs = self.contact._all_attribute_worker(attr_name)
            for attr in attrs:
                attr.id = None
                attr.pk = None
            return attrs

        contact_id = "sometestingidthatshouldn'tcollide"
        self.contact.contact_id = contact_id
        result = self.contact.decompose()
        expected = dict(
            first_name=self.contact.first_name,
            first_name_lower=self.contact.first_name_lower,
            last_name=self.contact.last_name,
            last_name_lower=self.contact.last_name_lower,
            additional_name=self.contact.additional_name,
            name_prefix=self.contact.name_prefix,
            name_suffix=self.contact.name_suffix,
            contact_id=contact_id,
            contact_type="merged",
            user=self.contact.user,
            is_primary=True,
            addresses=get_and_strip_attributes("addresses"),
            phone_numbers=get_and_strip_attributes("phone_numbers"),
            email_addresses=get_and_strip_attributes("email_addresses"),
            external_ids=get_and_strip_attributes("external_ids")
        )
        self.assertCountEqual(result, expected)

    def test_raw_contact(self):
        raw_contact = "{'raw-contact': 'text'}"
        contact = ContactFactory(user=self.user, contact_type="google-oauth2",
                                 raw_contact=raw_contact)
        self.assertEqual(contact.raw_contact, raw_contact)

        contact = ContactFactory(user=self.user,
                                 contact_type="linkedin-oauth2",
                                 raw_contact=raw_contact)
        self.assertNotEqual(contact.raw_contact, raw_contact)
        self.assertEqual(contact.raw_contact, {'raw-contact': 'text'})


class AddressParserTests(TestCase):
    def test_address_parser(self):
        expected = {'StreetNamePostType': 'Ave',
                    'PlaceName': 'Washington',
                    'StateName': 'DC',
                    'ZipCode': '20006',
                    'AddressNumber': '1600',
                    'StreetNamePostDirectional': 'NW',
                    'StreetName': 'Pennsylvania'}

        ap = AddressParser("1600 Pennsylvania Ave NW, Washington, DC 20006")
        self.assertEqual(ap.addr_dict, expected)
        self.assertEqual(ap.get_full_street(), '1600 Pennsylvania Ave NW')
        self.assertEqual(ap.city_name, 'Washington')
        self.assertEqual(ap.post_code, '20006')

    def test_splay_address(self):
        address = Address(street="1600 Pennsylvania Ave NW",
                          city="  WASHINGTON ",
                          region="DC",
                          post_code="20006")
        no_zip_address = Address(street="1600 Pennsylvania Ave NW",
                                 city="  WASHINGTON ",
                                 region="DC",
                                 post_code="")

        zipcode = ZipCodeFactory(zipcode='20006')
        city = CityFactory(name="Washington",
                           state__name='District of Columbia',
                           state__abbr='DC',
                           zips=[zipcode])

        # This city is a red herring
        CityFactory(name="Washington")

        # Control tests
        self.assertIsNone(address.zip_key)
        self.assertIsNone(address.city_key)
        self.assertIsNone(no_zip_address.zip_key)
        self.assertIsNone(no_zip_address.city_key)

        is_dirty = address.splay_address()
        self.assertTrue(is_dirty)
        self.assertEqual(address.zip_key, zipcode)
        self.assertEqual(address.city_key, city)

        is_dirty = no_zip_address.splay_address()
        self.assertTrue(is_dirty)
        self.assertEqual(no_zip_address.city_key, city)

        # Setup to test the cases where city name could be abbreviated
        zipcode = ZipCodeFactory(zipcode='46804')
        CityFactory(name="Fort Wayne",
                    state__name='Indiana',
                    state__abbr='IN',
                    zips=[zipcode])

        # Test the case when city name is correct and nothing is updated
        zipcode, _ = ZipCode.objects.get_or_create(zipcode="46804")
        city, _ = City.objects.get_or_create(name='Fort Wayne',
                           state__name='Indiana',
                           state__abbr='IN',
                           zips=zipcode)
        address = Address(street="5775 Coventry Ln",
                          city="Fort Wayne",
                          region="IN",
                          zip_key=zipcode,
                          city_key=city)
        self.assertEqual(address.zip_key, zipcode)
        self.assertEqual(address.city_key, city)
        is_dirty = address.splay_address()
        self.assertFalse(is_dirty)

        # Test the case when city name has abbreviation and gets updated to the
        # correct name
        address = Address(street="5775 Coventry Ln",
                          city="Ft. Wayne",
                          region="IN",
                          zip_key=zipcode)
        is_dirty = address.splay_address()
        self.assertEqual(address.zip_key, zipcode)
        self.assertEqual(address.city_key, city)
        self.assertTrue(is_dirty)

        # Test the case when city name has abbreviation and gets updated to the
        # correct name
        address = Address(street="5775 Coventry Ln",
                          city="Ft Wayne",
                          region="IN",
                          zip_key=zipcode)
        is_dirty = address.splay_address()
        self.assertEqual(address.zip_key, zipcode)
        self.assertEqual(address.city_key, city)
        self.assertTrue(is_dirty)

        # Test the case when city name has abbreviation and gets updated to the
        # correct name even if no zip_key or postal_code is entered
        address = Address(street="5775 Coventry Ln",
                          city="Ft Wayne",
                          region="IN")
        is_dirty = address.splay_address()
        self.assertEqual(address.city_key, city)
        self.assertTrue(is_dirty)

        # Test the case when the city name has an alias
        zipcode = ZipCodeFactory(zipcode='73301')
        CityFactory(name="Austin",
                    state__name='Texas',
                    state__abbr='TX',
                    zips=[zipcode])
        city, _ = City.objects.get_or_create(name='Austin',
                                             state__name='Texas',
                                             state__abbr='TX')

        address = Address(street="111 NW 1st Ave",
                          city="ATX",
                          region="TX")

        # Test before the alias is created that the address is not updated
        is_dirty = address.splay_address()
        self.assertNotEqual(address.city_key, city)
        self.assertFalse(is_dirty)

        CityAliasFactory(city=city,
                         alias="ATX")

        # Test after the alias is created and address is updated.
        is_dirty = address.splay_address()
        self.assertEqual(address.city_key, city)
        self.assertTrue(is_dirty)

        # Test the case when the city name is not found and there is an alias
        # but the state is different. No update to city occurs
        address = Address(street="111 NW 1st Ave",
                          city="ATX",
                          region="MN")
        is_dirty = address.splay_address()
        self.assertNotEqual(address.city_key, city)
        self.assertFalse(is_dirty)


class ContactWrapperTests(TestCase):
    maxDiff = None

    def test_special_related_many2many(self):
        locations = LinkedInLocationFactory.create_batch(1)
        contact = ContactFactory(locations=locations)
        # Control test
        self.assertCountEqual(locations, contact.locations.all())

        wrapped_contact = ContactWrapper(contact)
        more_locations = LinkedInLocationFactory.create_batch(4)
        wrapped_contact.locations = more_locations
        wrapped_contact.save()
        self.assertCountEqual(
            locations + more_locations,
            contact.locations.all())

    def test_special_related_many2one(self):
        contact = ContactFactory(email_addresses=[])
        self.assertFalse(contact.email_addresses.exists())
        wrapped_contact = ContactWrapper(contact)
        email_defs = []
        email_func = lambda x: EmailAddress(**dict.copy(x))
        for _ in range(4):
            email_defs.append(
                dict(address=faker.email(),
                     display_name=faker.name(),
                     label=faker.text(20),
                     primary=False))
        wrapped_contact.email_addresses = list(map(email_func, email_defs))
        wrapped_contact.save()
        self.assertTrue(contact.email_addresses.exists())
        self.assertCountEqual(
            email_defs,
            contact.email_addresses.values('address', 'display_name', 'label',
                                           'primary'))

        new_email_defs = list(map(dict.copy, email_defs))
        for email_def in new_email_defs:
            email_def.update(display_name=faker.name(),
                             label=faker.text(20),
                             primary=True)

        wrapped_contact.email_addresses = list(map(email_func, new_email_defs))
        wrapped_contact.save()
        self.assertCountEqual(
            new_email_defs,
            contact.email_addresses.values('address', 'display_name', 'label',
                                           'primary'))


class PhoneNumberTests(TestCase):
    def test_phone_number_parsing(self):
        def worker(number, expected):
            phone = PhoneNumberFactory.build(number=number)
            self.assertEqual(phone.rfc3966, expected)

        worker("555 444 3333", "tel:+1-555-444-3333")
        worker("(555) 444-3333", "tel:+1-555-444-3333")
        worker("(555)444-3333", "tel:+1-555-444-3333")
        worker("1 (555) 444-3333", "tel:+1-555-444-3333")
        worker("1(555)444-3333", "tel:+1-555-444-3333")
        worker("555-444-3333", "tel:+1-555-444-3333")
        worker("1-555-444-3333", "tel:+1-555-444-3333")
        worker("1-(555)-444-3333", "tel:+1-555-444-3333")
        worker("+1-555-444-3333", "tel:+1-555-444-3333")
        worker("15554443333", "tel:+1-555-444-3333")
        worker("+15554443333", "tel:+1-555-444-3333")
        worker("865554443333", "tel:+86-555-444-3333")
        worker("5554443333;34535", "tel:+1-555-444-3333;ext=34535")
        worker("(555)-444-3333;34535", "tel:+1-555-444-3333;ext=34535")
        worker("865554443333;34535", "tel:+86-555-444-3333;ext=34535")
        worker("86 555 444 3333;34535", "tel:+86-555-444-3333;ext=34535")
        worker("86-555-444-3333;34535", "tel:+86-555-444-3333;ext=34535")
        worker("tel:+86-555-444-3333", "tel:+86-555-444-3333")
        worker("tel:+86-555-444-3333;ext=34535",
               "tel:+86-555-444-3333;ext=34535")
        worker("+52 (55) 1234 5678", "tel:+52-55-1234-5678")
        worker("555.444.3333x222", "tel:+1-555-444-3333;ext=222")
        worker("(555) 444 3333 x 222", "tel:+1-555-444-3333;ext=222")
        worker("(555) 444 3333;x222", "tel:+1-555-444-3333;ext=222")
        worker("555.444.3333;x;222", "tel:+1-555-444-3333;ext=222")


class TaskTests(TestCase):
    @mock.patch("frontend.apps.contact.tasks.do_disconnect")
    @mock.patch("frontend.apps.contact.tasks.load_backend")
    @mock.patch("frontend.apps.contact.tasks.load_strategy")
    @mock.patch("frontend.apps.contact.tasks.run_import")
    def test_social_auth_import_task(self, mock_run_import, mock_load_strategy,
                                     mock_load_backend, mock_do_disconnect):
        # Verify a social auth account is disconnected after import.
        user = RevUpUserFactory()
        social = UserSocialAuthFactory(user=user)
        social_auth_import_task(user.id, social.id, False, {})
        mock_do_disconnect.assert_called_once_with(mock_load_backend(), user,
                                                   social.id)

        # Verify disconnect is still called on an error
        mock_do_disconnect.reset_mock()
        mock_do_disconnect.side_effect = ValueError
        social_auth_import_task(user.id, social.id, False, {})
        mock_do_disconnect.assert_called_once_with(mock_load_backend(), user,
                                                   social.id)


def describe_start_merge_task_util():
    @fixture
    def user(db):
        return RevUpUserFactory()

    @fixture
    def import_record(user):
        return ImportRecordFactory(user=user)

    @fixture
    def calling_import_task(user, import_record):
        return RevUpTaskFactory(user=user, task_id=import_record.task_id)

    @fixture
    def other_import_task(user):
        return RevUpTaskFactory(user=user)

    @fixture
    def merge_task(user):
        return RevUpTaskFactory(user=user,
                                task_type=RevUpTask.TaskType.CONTACT_MERGE)

    @fixture
    def task_mocks(mocker):
        mocker.patch.object(RevUpUser, 'clean_stale_tasks')
        pending_mock = mocker.patch.object(RevUpTask, 'pending',
                                           return_value=True, autospec=True)
        revoke_mock = mocker.patch.object(RevUpTask, 'revoke')
        merge_task_mock = mocker.patch(
            'frontend.apps.contact.tasks.contact_merging.apply_async')
        return pending_mock, revoke_mock, merge_task_mock

    @mark.django_db
    def it_isnt_blocked_by_calling_task(user, import_record,
                                        calling_import_task, task_mocks):
        pending_mock, revoke_mock, merge_task_mock = task_mocks
        start_contact_merging(user, import_record=import_record, account=None,
                              feature_flags=None)
        pending_mock.assert_not_called()
        revoke_mock.assert_not_called()
        merge_task_mock.assert_called()

    @mark.django_db
    def it_is_blocked_by_other_imports(user, import_record, other_import_task,
                                       task_mocks):
        pending_mock, revoke_mock, merge_task_mock = task_mocks
        start_contact_merging(user, import_record=import_record, account=None,
                              feature_flags=None)
        pending_mock.assert_called_once_with(other_import_task)
        revoke_mock.assert_not_called()
        merge_task_mock.assert_not_called()

    @mark.django_db
    def it_is_blocked_by_other_merges(user, import_record, merge_task,
                                      task_mocks):
        pending_mock, revoke_mock, merge_task_mock = task_mocks
        start_contact_merging(user, import_record=import_record, account=None,
                              feature_flags=None)
        pending_mock.assert_called_once_with(merge_task)
        revoke_mock.assert_not_called()
        merge_task_mock.assert_not_called()

    @mark.django_db
    def it_isnt_blocked_by_revoked_tasks(user, import_record,
                                         other_import_task, task_mocks):
        other_import_task.revoked = True
        other_import_task.save()
        pending_mock, revoke_mock, merge_task_mock = task_mocks
        start_contact_merging(user, import_record=import_record, account=None,
                              feature_flags=None)
        pending_mock.assert_not_called()
        revoke_mock.assert_not_called()
        merge_task_mock.assert_called()

    @mark.django_db
    def it_isnt_blocked_by_finished_tasks(user, import_record,
                                          other_import_task, task_mocks):
        pending_mock, revoke_mock, merge_task_mock = task_mocks
        pending_mock.return_value = False
        start_contact_merging(user, import_record=import_record, account=None,
                              feature_flags=None)
        pending_mock.assert_called_once_with(other_import_task)
        revoke_mock.assert_not_called()
        merge_task_mock.assert_called()

    @mark.django_db
    def it_doesnt_start_if_persist_fails(user, import_record, task_mocks,
                                         mocker):
        pending_mock, revoke_mock, merge_task_mock = task_mocks
        rut_create_mock = mocker.patch.object(RevUpTask.objects, 'create',
                                              return_value=None)
        start_contact_merging(user, import_record=import_record, account=None,
                              feature_flags=None)
        rut_create_mock.assert_called_once()
        pending_mock.assert_not_called()
        revoke_mock.assert_not_called()
        merge_task_mock.assert_not_called()

    @mark.django_db
    def it_revokes_rut_if_task_fails(user, import_record, task_mocks):
        pending_mock, revoke_mock, merge_task_mock = task_mocks
        merge_task_mock.side_effect = AttributeError('whatever')
        with raises(AttributeError):
            start_contact_merging(user, import_record=import_record,
                                  account=None, feature_flags=None)
        pending_mock.assert_not_called()
        revoke_mock.assert_called()
        merge_task_mock.assert_called()
