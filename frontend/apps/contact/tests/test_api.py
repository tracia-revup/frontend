
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import Permission, AnonymousUser
from django.contrib.contenttypes.models import ContentType
from django.test import Client, RequestFactory
from django.urls import reverse
import json
from unittest import mock

from frontend.apps.authorize.factories import RevUpUserFactory
from frontend.apps.authorize.models import RevUpUser
from frontend.apps.authorize.test_utils import AuthorizedUserTestCase
from frontend.apps.call_time.factories import CallTimeContactFactory
from frontend.apps.call_time.models import CallTimeContact
from frontend.apps.campaign.factories import (AccountFactory,
                                              AccountProfileFactory)
from frontend.apps.campaign.models.base import AccountProfile
from frontend.apps.contact.api.views import ContactImportHF
from frontend.apps.contact.factories import (
    ContactFactory, ImportRecordFactory)
from frontend.apps.contact.models import (
    ContactNotes, Contact, DateOfBirth, Gender)
from frontend.apps.contact_set.factories import ContactSetFactory
from frontend.apps.role.permissions import AdminPermissions
from frontend.apps.seat.factories import SeatFactory


@mock.patch('frontend.apps.contact.api.views.start_hf_contact_import',
            mock.MagicMock())
class ContactImportHfTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        # Calling super here will create a RevUpUser and log it in.
        super().setUp()
        self.test_account = AccountFactory()
        self.house_file_import_url = reverse(
            'contacts_import_hf', kwargs={'account_id': self.test_account.id})
        self.content_type = ContentType.objects.get_for_model(RevUpUser)
        self.manage_housefile_permission, _ = Permission.objects.get_or_create(
            name='Manage Housefile', codename='can_manage_housefiles',
            content_type=self.content_type)
        self.request_factory = RequestFactory()

    def test_housefile_contact_import_permissions_1(self):
        """Tests the housefile contact import API when an AnonymousUser tries
         to access the API
        """
        request = self.request_factory.get(self.house_file_import_url)
        request.user = AnonymousUser()
        response = ContactImportHF.as_view()(request)
        # Verify that the request is forbidden
        assert 403 == response.status_code

    @mock.patch('frontend.apps.contact.api.views.ContactImportHF.'
                '_get_or_validate_collection_name')
    def test_housefile_contact_import_permissions_2(self, mock_mongo_validate):
        """Tests the housefile contact import API when a user has
        'can_manage_housefiles' permission.
        """
        mock_mongo_validate.return_value = \
            self.test_account.client_entity_collection_name, ''
        # Add 'can_manage_housefile' permission to the user
        self.user.user_permissions.add(self.manage_housefile_permission)

        response = self.client.post(self.house_file_import_url,
                                    {'account_id': self.test_account.id})
        # Verify that the response is OK
        assert 200 == response.status_code

    def test_housefile_contact_import_permissions_3(self):
        """Tests the housefile contact import API when a user does not have
        'can_manage_housefiles' permission.
        """
        response = self.client.post(self.house_file_import_url,
                                    {'account_id': self.test_account.id,
                                     'collection_name': 'test'})
        # Verify that the request is forbidden
        assert 403 == response.status_code

    def test_housefile_contact_import_permissions_4(self):
        """Tests the housefile contact import API when the user is a staff
        member but does not have the permission 'can_manage_housefiles'
        """
        self.user.is_staff = True

        # Assert that the user is staff
        assert self.user.is_staff is True
        response = self.client.post(self.house_file_import_url,
                                    {'account_id': self.test_account.id})
        # Verify that the request is forbidden
        assert 403 == response.status_code

    @mock.patch('frontend.apps.contact.api.views.ContactImportHF.'
                '_get_or_validate_collection_name')
    def test_housefile_contact_import_permissions_5(self, mock_mongo_validate):
        """Tests the housefile contact import API when the user is a super
        user and does not have the permission 'can_manage_housefiles'
        """
        mock_mongo_validate.return_value = \
            self.test_account.client_entity_collection_name, ''
        self.user.is_superuser = True

        # Assert that the user is super_user
        assert self.user.is_superuser is True
        request = self.request_factory.get(self.house_file_import_url)
        request.user = self.user
        response = ContactImportHF.as_view()(request)
        # Verify that the superuser can access the API
        assert 403 != response.status_code

    def test_housefile_contact_import_1(self):
        """Tests the housefile contact import API when the default housefile
        collection doesn't exist.
        """
        # Add 'can_manage_housefile' permission to the user
        self.user.user_permissions.add(self.manage_housefile_permission)

        response = self.client.post(self.house_file_import_url)
        # Verify that response failed due to bad request
        assert 400 == response.status_code
        assert f'No collection exists with name ' \
               f'{self.test_account.client_entity_collection_name}' \
               in str(response.content)

    @mock.patch('frontend.apps.contact.api.views.ContactImportHF.'
                '_get_or_validate_collection_name')
    def test_housefile_contact_import_2(self, mock_mongo_validate):
        """Tests the housefile contact import API when the default housefile
        collection exists.
        """
        mock_mongo_validate.return_value = \
            self.test_account.client_entity_collection_name, ''
        # Add 'can_manage_housefile' permission to the user
        self.user.user_permissions.add(self.manage_housefile_permission)

        response = self.client.post(self.house_file_import_url)
        # Verify that response is OK
        assert 200 == response.status_code

    @mock.patch('frontend.apps.contact.api.views.ContactImportHF.'
                '_get_or_validate_collection_name')
    def test_housefile_contact_import_3(self, mock_mongo_validate):
        """Tests the housefile contact import API when a custom collection
        name is provided in the request and the collection exists.
        """
        collection_name = 'test'
        mock_mongo_validate.return_value = collection_name, ''
        # Add 'can_manage_housefile' permission to the user
        self.user.user_permissions.add(self.manage_housefile_permission)

        response = self.client.post(self.house_file_import_url,
                                    {'collection_name': collection_name})
        # Verify that the response is 200
        assert 200 == response.status_code

    def test_housefile_contact_import_4(self):
        """Tests the housefile contact import API when a custom collection
        name is provided in the request and the collection doesn't exist.
        """
        collection_name = 'test'
        # Add 'can_manage_housefile' permission to the user
        self.user.user_permissions.add(self.manage_housefile_permission)

        response = self.client.post(self.house_file_import_url,
                                    {'collection_name': collection_name})
        # Verify that the response is 200
        assert 400 == response.status_code
        assert f'No collection exists with name {collection_name}' in str(
            response.content)


class ContactImportHfTests2(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        # Calling super here will create a RevUpUser and log it in.
        super().setUp()
        self.political_account = AccountFactory()
        self.nfp_profile = AccountProfileFactory(
            account_type=AccountProfile.AccountType.NONPROFIT)
        self.academic_profile = AccountProfileFactory(
            account_type=AccountProfile.AccountType.ACADEMIC)
        self.nfp_account = AccountFactory(
            account_profile=self.nfp_profile)
        self.academic_account = AccountFactory(
            account_profile=self.academic_profile)
        self.house_file_import_url = reverse(
            'contacts_import_hf',
            kwargs={'account_id': self.political_account.id})
        self.source_data_file_import_url = reverse(
            'contacts_import_hf',
            kwargs={'account_id': self.nfp_account.id})
        self.client_contacts_import_url = reverse(
            'contacts_import_hf',
            kwargs={'account_id': self.academic_account.id})
        self.content_type = ContentType.objects.get_for_model(RevUpUser)
        self.manage_housefile_permission, _ = Permission.objects.get_or_create(
            name='Manage Housefile', codename='can_manage_housefiles',
            content_type=self.content_type)
        self.expected_key = 'task_label'

    @mock.patch('frontend.apps.contact.api.views.start_hf_contact_import')
    @mock.patch('frontend.apps.contact.api.views.ContactImportHF.'
                '_get_or_validate_collection_name')
    def test_housefile_import_task_label(self, mock_mongo_validate,
                                         mock_start_hf_contact_import):
        """Tests the client contact import task label on political accounts.
        """
        # Post a request to client contacts import url on a political account
        # and verify that the client contacts import task label is set to
        # `Housefile Import`
        mock_mongo_validate.return_value = \
            self.political_account.client_entity_collection_name, ''
        self.user.user_permissions.add(self.manage_housefile_permission)
        response = self.client.post(self.house_file_import_url)
        assert 200 == response.status_code
        # Verify that a housefile import was triggered
        mock_start_hf_contact_import.assert_called_once()
        actual_call_args = mock_start_hf_contact_import.call_args[1]
        expected_val = 'House File Import'
        assert self.expected_key in actual_call_args
        # Verify that the task_label is `House File Import` for political
        # account.
        assert expected_val == actual_call_args[self.expected_key]

    @mock.patch('frontend.apps.contact.api.views.start_hf_contact_import')
    @mock.patch('frontend.apps.contact.api.views.ContactImportHF.'
                '_get_or_validate_collection_name')
    def test_source_data_file_import_task_label(self, mock_mongo_validate,
                                                mock_start_hf_contact_import):
        """Tests the client contact import task label on NFP accounts."""
        # Post a request to client contacts import url on a nfp account and
        # verify that the client contacts import task label is set to
        # `Source Data File Import`
        mock_mongo_validate.return_value = \
            self.nfp_account.client_entity_collection_name, ''
        self.user.user_permissions.add(self.manage_housefile_permission)
        response = self.client.post(self.source_data_file_import_url)
        assert 200 == response.status_code
        # Verify that a source data file import was triggered
        mock_start_hf_contact_import.assert_called_once()
        actual_call_args = mock_start_hf_contact_import.call_args[1]
        expected_val = 'Source Data File Import'
        assert self.expected_key in actual_call_args
        # Verify that the task_label is `Source Data File Import` for NFP
        # account.
        assert expected_val == actual_call_args[self.expected_key]

    @mock.patch('frontend.apps.contact.api.views.start_hf_contact_import')
    @mock.patch('frontend.apps.contact.api.views.ContactImportHF.'
                '_get_or_validate_collection_name')
    def test_client_contacts_import_task_label(self, mock_mongo_validate,
                                               mock_start_hf_contact_import):
        """Tests the client contact import task label on academic accounts.
        """
        # Post a request to client contacts import url on a academic account
        # and verify that the client contacts import task label is set to
        # `Client Contacts Import`
        mock_mongo_validate.return_value = \
            self.nfp_account.client_entity_collection_name, ''
        self.user.user_permissions.add(self.manage_housefile_permission)
        response = self.client.post(self.client_contacts_import_url)
        assert 200 == response.status_code
        # Verify that a client contacts import was triggered
        mock_start_hf_contact_import.assert_called_once()
        actual_call_args = mock_start_hf_contact_import.call_args[1]
        expected_val = 'Client Contacts Import'
        assert self.expected_key in actual_call_args
        # Verify that the task_label is `Client Contact Import`
        assert expected_val == actual_call_args[self.expected_key]


@mock.patch.object(Contact, 'predict_gender', return_value='U')
class ContactsAPITests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        # Calling super here will create a RevUpUser and log it in.
        super(ContactsAPITests, self).setUp(login_now=False)

        # Setup url where logged-in user owns a contact
        self.contact = ContactFactory(user=self.user, external_ids=[])
        self.url = reverse('user_contacts_api-list',
                           args=(self.user.id,))

        # Setup url where non-logged in user owns a contact
        self.user2 = RevUpUserFactory()
        self.contact2 = ContactFactory(user=self.user2)
        self.url2 = reverse('user_contacts_api-list',
                            args=(self.contact2.user_id,))

        self.url3 = reverse('user_contacts_api-detail',
                            args=(self.user.id, self.contact.id,))

    def test_permissions(self, *args):
        ## Verify the API rejects a user that is not logged in
        response = self.client.get(self.url)
        self.assertNoCredentials(response)
        response = self.client.get(self.url2)
        self.assertNoCredentials(response)

        # Log in
        self._login(self.user)

        # Ensure api isn't getting permission from IsAdminUser
        self.assertFalse(self.user.is_admin)

        ## Verify IsOwner permission gives access to users contacts.
        response = self.client.get(self.url)
        results = response.json()['results']
        contact_ids = [c['id'] for c in results]
        self.assertIn(self.contact.id, contact_ids)
        # Verify there are no other contacts bleeding over.
        self.assertNotIn(self.contact2.id, contact_ids)

        ## Verify users can't access another user's contacts
        response = self.client.get(self.url2)
        self.assertNoPermission(response)

        ## Verify users can access specific contacts
        response = self.client.get(self.url3)
        self.assertEqual(self.contact.id, response.json()['id'])

    @mock.patch("frontend.apps.contact.deletion.ContactDeleteTransaction")
    def test_delete(self, _, *args):
        self._login(self.user)

        response = self.client.delete(self.url3 + "?dryrun=1")
        self.assertCountEqual(list(response.json().keys()), (
            "Contacts", "Prospects", "Call Time Contacts", "Call Groups",
            "Call Logs", "Notes"))

        response = self.client.delete(self.url3)
        self.assertEqual(response.status_code, 204)
        with self.assertRaises(ObjectDoesNotExist):
            self.contact.refresh_from_db()

    @mock.patch("frontend.apps.contact.api.views.ContactViewSet._submit_analysis")
    def test_update_existing(self, *args):
        self._login(self.user)

        # Control tests
        self.contact.refresh_from_db()
        assert self.contact.parent is None
        assert self.contact.external_ids.exists() is False

        response = self.client.put(self.url3, content_type="application/json",
                                   data=json.dumps({
                                       "id": self.contact.id,
                                       'last_name': self.contact.last_name,
                                       "external_ids": [
                                           {"source": "ngp",
                                            "value": "ngp123"},
                                           {"source": "nationbuilder",
                                            "value": "revup:cxz"},
                                       ],
                                   }))

        result_parent_id = response.data['id']
        self.contact.refresh_from_db()
        assert self.contact.parent_id == result_parent_id
        parent = self.contact.parent
        manual_child = parent.contacts.get(contact_type='manual')
        assert set(manual_child.external_ids.values_list('source', 'value')) == \
                {('ngp', 'ngp123'), ('nationbuilder', 'revup:cxz')}


    @mock.patch("frontend.apps.contact.api.views.ContactViewSet._submit_analysis")
    @mock.patch("frontend.apps.contact.api.serializers.ManualContactTransaction.archive")
    @mock.patch("frontend.apps.contact.api.serializers.ManualContactTransaction.create")
    def test_create(self, mct, mct_d, _, *args):
        self._login(self.user)
        response = self.client.post(self.url, content_type="application/json",
                                    data=json.dumps({
                                        "first_name": "TestFirst",
                                        "last_name": "TestLast",
                                        "needs_attention": False,
                                        "contact_type": "google-oauth2",
                                        "external_ids": [
                                            {"source": "ngp",
                                             "value": "ngp123"},
                                            {"source": "nationbuilder",
                                             "value": "revup:cxz"},
                                        ],
                                        "email_addresses": [{
                                            "label": "test",
                                            "address": "test@revup.com"
                                        }],
                                        "dob": "2000-12-31",
                                        "gender": "U"
                                    }))
        self.assertEqual(response.status_code, 201)

        # Verify the contact was created correctly
        c_id = response.json()["id"]
        c = Contact.objects.get(id=c_id)
        self.assertEqual(c.first_name, "TestFirst")
        self.assertEqual(c.last_name, "TestLast")
        self.assertEqual(c.first_name_lower, "testfirst")
        self.assertEqual(c.last_name_lower, "testlast")
        self.assertEqual(c.user.id, self.user.id)
        self.assertEqual(c.contact_type, "manual")
        self.assertEqual(c.needs_attention, False)
        self.assertTrue(c.is_primary)
        self.assertEqual(c.emails, ["test@revup.com"])
        self.assertEqual(c.composite_id, Contact._calc_composite_id(
            c.user_id, c.contact_id, "manual"
        ))
        # TODO-frontend: uncomment the code once the UI implementation
        # is in place. REVUP-1915 REVUP-1916
        self.assertEqual("2000-12-31", c.dob.birth_date.strftime('%Y-%m-%d'))
        self.assertEqual("U", c.gender.gender_type)
        self.assertEqual(set(c.external_ids.values_list('source', 'value')),
                         {('ngp', 'ngp123'), ('nationbuilder', 'revup:cxz')})

        # Verify Kafka was signalled correctly
        self.assertEqual(len(mct.call_args_list), 6)
        self.assertEqual(len(mct_d.call_args_list), 0)
        self.assertEqual(mct.call_args_list[0][0][1].__class__, Contact)

    @mock.patch("frontend.apps.contact.api.views.ContactViewSet._submit_analysis")
    @mock.patch("frontend.apps.contact.api.serializers.ManualContactTransaction.create")
    @mock.patch("frontend.apps.contact.api.serializers.ManualContactTransaction.archive")
    def test_update(self, mct_d, mct_c, _, *args):
        self._login(self.user)
        # Setup the test
        initial_birthdate = "2000-12-31"
        initial_gender = "U"
        response = self.client.post(self.url, content_type="application/json",
                                    data=json.dumps({
                                        "first_name": "TestFirst",
                                        "last_name": "TestLast",
                                        "needs_attention": False,
                                        "contact_type": "google-oauth2",
                                        "email_addresses": [{
                                            "label": "test",
                                            "address": "test@revup.com"
                                        }],
                                        "phone_numbers": [{
                                            "label": "",
                                            "number": "555-555-5555"
                                        }],
                                        "dob": initial_birthdate,
                                        "gender": initial_gender
                                    }))

        self.assertEqual(response.status_code, 201)
        # Verify the contact was created correctly
        c_id = response.json()["id"]
        c = Contact.objects.get(id=c_id)
        self.assertEqual(c.first_name, "TestFirst")
        self.assertEqual(c.last_name, "TestLast")
        self.assertEqual(c.contact_type, "manual")
        self.assertTrue(c.is_primary)
        self.assertEqual(c.dob.birth_date.strftime('%Y-%m-%d'),
                         initial_birthdate)
        self.assertEqual(c.gender.gender_type,
                         initial_gender)

        self.assertEqual(mct_c.call_args_list[0][0][1].__class__, Contact)
        mct_c.reset_mock()
        mct_d.reset_mock()

        # Update the contact
        update_url = "{}{}/".format(self.url, c_id)
        email = c.email_addresses.first()
        phone = c.phone_numbers.first()
        old_birthdate_id = c.dob.id
        new_birthdate = "1990-12-31"
        old_gender_id = c.gender.id
        new_gender = "F"
        response = self.client.put(update_url, content_type="application/json",
                                   data=json.dumps({
                                       "id": c_id,
                                       "first_name": "TestFirst",
                                       "last_name": "TestLast",
                                       "needs_attention": False,
                                       "contact_type": "google-oauth2",
                                       "email_addresses": [{
                                           "id": email.id,
                                           "label": "test",
                                           "address": "test@revup.com"
                                       }],
                                       "phone_numbers": [{
                                           "id": phone.id,
                                           "label": "",
                                           "number": "555-555-5554"
                                       }],
                                       "dob": new_birthdate,
                                       "gender": new_gender
                                   }))

        # Inspect the contact to verify it has been updated correctly
        self.assertEqual(response.status_code, 200)
        rj = response.json()
        self.assertEqual(rj["id"], c.id)
        self.assertEqual(rj["first_name"], "TestFirst")
        self.assertEqual(rj["last_name"], "TestLast")
        phone = c.phone_numbers.filter(archived=None).first()
        self.assertEqual(rj["phone_numbers"], [{"id": phone.id,
                                                "label": "",
                                                "number": "tel:+1-555-555-5554"}])
        self.assertEqual(rj["email_addresses"], [{"id": email.id,
                                                  "label": "test",
                                                  "address": "test@revup.com"}])
        self.assertEqual(new_birthdate, rj["dob"])
        self.assertEqual(new_gender, rj["gender"])

        # Verify the old number was archived
        self.assertEqual(c.phone_numbers.count(), 2)
        phone = c.phone_numbers.exclude(archived=None).get()
        self.assertEqual(phone.rfc3966, "tel:+1-555-555-5555")

        # Verify that the manual contact's dob record (same record before put
        # operation) got updated with the new birthdate
        birthdate_obj = DateOfBirth.objects.get(id=old_birthdate_id)
        self.assertEqual(birthdate_obj.birth_date.strftime('%Y-%m-%d'),
                         new_birthdate)

        # Verify that the manual contact's gender record (same record before
        # put operation) got updated with the new gender value
        gender_obj = Gender.objects.get(id=old_gender_id)
        self.assertEqual(gender_obj.gender_type,
                         new_gender)

        # Verify Kafka was signalled correctly
        self.assertEqual(len(mct_d.call_args_list), 1)
        self.assertEqual(mct_d.call_args_list[0][0][1].rfc3966,
                         "tel:+1-555-555-5555")
        self.assertEqual(len(mct_c.call_args_list), 1)
        self.assertEqual(mct_c.call_args_list[0][0][1].rfc3966,
                         "tel:+1-555-555-5554")

    @mock.patch("frontend.apps.contact.api.views.ContactViewSet._submit_analysis")
    @mock.patch("frontend.apps.contact.api.serializers.ManualContactTransaction.create")
    @mock.patch("frontend.apps.contact.api.serializers.ManualContactTransaction.archive")
    def test_delete_attributes(self, mct_d, mct_c, _, *args):
        self._login(self.user)
        # Setup the test
        initial_birth_date = "2000-12-31"
        initial_gender = "u"
        response = self.client.post(self.url, content_type="application/json",
                                    data=json.dumps({
                                        "first_name": "TestFirst",
                                        "last_name": "TestLast",
                                        "needs_attention": False,
                                        "contact_type": "google-oauth2",
                                        "email_addresses": [{
                                                "label": "test",
                                                "address": "test@revup.com"
                                            },
                                            {
                                                "label": "test2",
                                                "address": "test@revup.com"
                                            }
                                        ],
                                        "phone_numbers": [{
                                            "label": "",
                                            "number": "555-555-5555"
                                        }],
                                        "dob": initial_birth_date,
                                        "gender": initial_gender
                                    }))
        self.assertEqual(response.status_code, 201)
        # Verify the contact was created correctly
        c_id = response.json()["id"]
        c = Contact.objects.get(id=c_id)
        self.assertEqual(c.first_name, "TestFirst")
        self.assertEqual(c.last_name, "TestLast")
        self.assertEqual(c.contact_type, "manual")
        self.assertTrue(c.is_primary)
        self.assertEqual(c.dob.birth_date.strftime('%Y-%m-%d'),
                         initial_birth_date)
        self.assertEqual(c.gender.gender_type,
                         initial_gender)

        self.assertEqual(mct_c.call_args_list[0][0][1].__class__, Contact)
        mct_c.reset_mock()
        mct_d.reset_mock()

        ## Update the contact
        update_url = "{}{}/".format(self.url, c_id)
        phone = c.phone_numbers.first()
        email = c.email_addresses.get(label="test2")
        old_birthdate_id = c.dob.id
        new_birth_date = '1990-12-31'
        old_gender_id = c.gender.id
        new_gender = "m"
        response = self.client.put(update_url, content_type="application/json",
                                   data=json.dumps({
                                       "id": c_id,
                                       "first_name": "TestFirst",
                                       "last_name": "TestLast",
                                       "needs_attention": False,
                                       "contact_type": "google-oauth2",
                                       "email_addresses": [
                                           {
                                               "id": email.id,
                                               "label": "test2",
                                               "address": "test@revup.com"
                                           }
                                       ],
                                       "phone_numbers": [{
                                           "id": phone.id,
                                           "label": "",
                                           "number": "555-555-5554"
                                       }],
                                       "dob":  new_birth_date,
                                       "gender":  new_gender
                                   }))

        self.assertEqual(response.status_code, 200)
        rj = response.json()
        self.assertEqual(rj["id"], c.id)
        self.assertEqual(rj["first_name"], "TestFirst")
        self.assertEqual(rj["last_name"], "TestLast")
        phone = c.phone_numbers.filter(archived=None).first()
        self.assertEqual(rj["phone_numbers"], [{"id": phone.id,
                                                "label": "",
                                                "number": "tel:+1-555-555-5554"
                                                }])
        self.assertEqual(rj["email_addresses"], [{"id": email.id,
                                                  "label": "test2",
                                                  "address": "test@revup.com"
                                                  }])
        self.assertEqual(new_birth_date, rj["dob"])
        self.assertEqual(new_gender, rj["gender"])

        # Verify the old email was archived
        self.assertEqual(c.email_addresses.count(), 2)
        a_email = c.email_addresses.exclude(archived=None).get()
        self.assertNotEqual(email, a_email)
        self.assertEqual(a_email.address, "test@revup.com")
        self.assertEqual(a_email.label, "test")

        # Verify the old birth_date was updated
        birthdate_obj = DateOfBirth.objects.get(id=old_birthdate_id)
        self.assertEqual(birthdate_obj.birth_date.strftime('%Y-%m-%d'),
                         new_birth_date)

        # Verify the old gender object was updated
        gender_obj = Gender.objects.get(id=old_gender_id)
        self.assertEqual(gender_obj.gender_type, new_gender)

        # Verify Kafka was signalled correctly
        # Kafka should not be signalled with the deleted email because it
        # has a duplicate
        self.assertEqual(len(mct_d.call_args_list), 1)
        self.assertEqual(mct_d.call_args_list[0][0][1].rfc3966,
                         "tel:+1-555-555-5555")
        self.assertEqual(len(mct_c.call_args_list), 1)
        self.assertEqual(mct_c.call_args_list[0][0][1].rfc3966,
                         "tel:+1-555-555-5554")

        # Verify email is deleted from kafka when fully removed
        mct_c.reset_mock()
        mct_d.reset_mock()
        response = self.client.put(update_url, content_type="application/json",
                                   data=json.dumps({
                                       "id": c_id,
                                       "first_name": "TestFirst",
                                       "last_name": "TestLast",
                                       "needs_attention": False,
                                       "email_addresses": [],
                                       "phone_numbers": [{
                                           "id": phone.id,
                                           "label": "",
                                           "number": "555-555-5554"
                                       }]
                                   }))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(mct_c.call_args_list), 0)
        self.assertEqual(len(mct_d.call_args_list), 1)
        self.assertEqual(mct_d.call_args_list[0][0][1], email)

    def test_update_birthdate(self, *args):
        """Tests updating birthdate on a non-manual contact. In this case,
        contact of type google-oauth2"""
        # Setup the test
        self._login(self.user)
        first_name = "TestFirst"
        last_name = "TestLast"
        data = {
                "first_name": first_name,
                "last_name": last_name,
                "needs_attention": False,
                "contact_type": "google-oauth2",
                "user": self.user
        }
        c = ContactFactory(**data)
        birthdate_1 = c.dob.birth_date
        birthdate_1_modified_time = c.dob.modified
        contact_url = "{}{}/".format(self.url, c.id)

        # Update the contact with same birthdate info and verify that nothing
        # has changed
        response = self.client.put(contact_url, content_type="application/json",
                                   data=json.dumps({
                                       "id": c.id,
                                       "first_name": first_name,
                                       "last_name": last_name,
                                       "dob": birthdate_1
                                   }))
        self.assertEqual(response.status_code, 200)
        rj = response.json()
        self.assertEqual(rj["id"], c.id)
        self.assertEqual(birthdate_1, rj["dob"])

        # Update the contact
        birthdate_1_id = c.dob.id
        birthdate_2 = "1990-12-31"
        response = self.client.put(contact_url, content_type="application/json",
                                   data=json.dumps({
                                       "id": c.id,
                                       "first_name": "TestFirst",
                                       "last_name": "TestLast",
                                       "needs_attention": False,
                                       "dob": birthdate_2
                                   }))
        self.assertEqual(response.status_code, 200)
        rj = response.json()

        # Inspect the contact to verify that a new manual contact has been
        # created
        self.assertNotEqual(rj["id"], c.id)
        self.assertEqual(rj["first_name"], "TestFirst")
        self.assertEqual(rj["last_name"], "TestLast")
        self.assertEqual(birthdate_2, rj["dob"])

        # Verify that the old birthdate record still has the old birthdate
        # value and old modified timestamp
        old_birthdate_rec = DateOfBirth.objects.get(id=birthdate_1_id)
        self.assertEqual(old_birthdate_rec.birth_date.strftime('%Y-%m-%d'),
                         birthdate_1)
        self.assertEqual(birthdate_1_modified_time, old_birthdate_rec.modified)

        # Verify that the old contact is mapped to old dob record but
        # get_preferred_dob() returns the new birthdate
        c = Contact.objects.get(id=c.id)
        self.assertEqual(birthdate_1_id, c.dob.id)
        self.assertEqual(birthdate_2, c.get_preferred_dob().birth_date.
                         strftime('%Y-%m-%d'))

        # Verify that a GET on the contact returns the new birthdate
        response = self.client.get(contact_url, content_type="application/json")
        self.assertEqual(response.status_code, 200)
        rj = response.json()
        self.assertEqual(birthdate_2, rj['dob'])

    def test_update_gender(self, *args):
        """Tests updating gender on a non-manual contact. In this case,
        contact of type google-oauth2"""
        # Setup the test
        self._login(self.user)
        first_name = "TestFirst"
        last_name = "TestLast"
        data = {
                "first_name": first_name,
                "last_name": last_name,
                "needs_attention": False,
                "contact_type": "google-oauth2",
                "user": self.user
        }
        c = ContactFactory(**data)
        gender_1 = c.gender.gender_type
        gender_1_modified_time = c.gender.modified
        contact_url = "{}{}/".format(self.url, c.id)

        # Update the contact with same birthdate info and verify that nothing
        # has changed
        response = self.client.put(contact_url, content_type="application/json",
                                   data=json.dumps({
                                       "id": c.id,
                                       "first_name": first_name,
                                       "last_name": last_name,
                                       "gender": gender_1
                                   }))
        self.assertEqual(response.status_code, 200)
        rj = response.json()
        self.assertEqual(rj["id"], c.id)
        self.assertEqual(gender_1, rj["gender"])

        # Update the contact
        l1 = ['f', 'm', 'u']
        l1.remove(gender_1)
        gender_1_id = c.gender.id
        gender_2 = l1[0]
        response = self.client.put(contact_url, content_type="application/json",
                                   data=json.dumps({
                                       "id": c.id,
                                       "first_name": "TestFirst",
                                       "last_name": "TestLast",
                                       "needs_attention": False,
                                       "gender": gender_2
                                   }))
        self.assertEqual(response.status_code, 200)
        rj = response.json()

        # Inspect the contact to verify that a new manual contact has been
        # created
        self.assertNotEqual(rj["id"], c.id)
        self.assertEqual(rj["first_name"], "TestFirst")
        self.assertEqual(rj["last_name"], "TestLast")
        self.assertEqual(gender_2, rj["gender"])

        # Verify that the old gender record still has the old gender
        # value and old modified timestamp
        old_gender_rec = Gender.objects.get(id=gender_1_id)
        self.assertEqual(old_gender_rec.gender_type,
                         gender_1)
        self.assertEqual(gender_1_modified_time, old_gender_rec.modified)

        # Verify that the old contact is mapped to old gender record but
        # get_preferred_gender() returns the new gender
        c = Contact.objects.get(id=c.id)
        self.assertEqual(gender_1_id, c.gender.id)
        self.assertEqual(gender_2, c.get_preferred_gender())

        # Verify that a GET on the contact returns the new gender
        response = self.client.get(contact_url, content_type="application/json")
        self.assertEqual(response.status_code, 200)
        rj = response.json()
        self.assertEqual(gender_2, rj['gender'])


@mock.patch.object(Contact, 'get_preferred_gender', return_value='U')
class AccountContactsViewSetTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        # Calling super here will create a RevUpUser and log it in.
        super(AccountContactsViewSetTests, self).setUp(login_now=False)
        self.account = self.user.current_seat.account
        self.account_user = self.account.account_user

        # Setup url where logged-in user owns a contact
        self.contact = ContactFactory(user=self.account_user)
        self.url = reverse('account_contacts_api-list',
                           args=(self.account.id,))

        # Setup url where different account owns a contact
        self.account2 = AccountFactory()
        self.account_user2 = self.account2.account_user
        self.contact2 = ContactFactory(user=self.account_user2)
        self.url2 = reverse('account_contacts_api-list',
                            args=(self.account2.id,))

    def test_permissions(self, *args):
        ## Verify the API rejects a user that is not logged in
        response = self.client.get(self.url)
        self.assertNoCredentials(response)
        response = self.client.get(self.url2)
        self.assertNoCredentials(response)

        # Log in
        self._login(self.user)

        # Ensure api isn't getting permission from IsAdminUser
        self.assertFalse(self.user.is_admin)

        ## Verify user still does not have permission to view
        response = self.client.get(self.url)
        self.assertNoPermission(response)

        # Add an invalid admin permission and verify access is not granted
        self._add_admin_group(permissions=AdminPermissions.MOD_ACCOUNT)
        result = self.client.get(self.url)
        self.assertNoPermission(result)

        ## Add view permission to user and verify
        self._add_admin_group(permissions=AdminPermissions.VIEW_CONTACTS)
        result = self.client.get(self.url)
        self.assertContains(result, self.contact.id)

        # Verify user cannot create
        response = self.client.post(self.url, data={})
        self.assertNoPermission(response)

        ## Add mod permissions and verify
        self._add_admin_group(permissions=AdminPermissions.MODIFY_CONTACTS)
        response = self.client.post(self.url, data={
            "first_name": "test", "last_name": "test"})
        self.assertEqual(response.status_code, 201)

        ## Verify even with admin permission, user can't view another account
        response = self.client.get(self.url2)
        self.assertNoPermission(response)

    def test_get(self, *args):
        self._login_and_add_admin_group(
            permissions=AdminPermissions.VIEW_CONTACTS)

        response = self.client.get(self.url)
        results = response.json()['results']
        contact_ids = [c['id'] for c in results]
        self.assertIn(self.contact.id, contact_ids)
        # Verify there are no other contacts bleeding over.
        self.assertNotIn(self.contact2.id, contact_ids)

        ## Verify users can't access another user's contacts
        response = self.client.get(self.url2)
        self.assertNoPermission(response)

    def test_create(self, *args):
        self._login_and_add_admin_group(
            permissions=AdminPermissions.MODIFY_CONTACTS)
        response = self.client.post(self.url, data={
            "first_name": "TestFirst", "last_name": "TestLast"})
        self.assertEqual(response.status_code, 201)

        # Verify the contact was created correctly
        c_id = response.json()["id"]
        c = Contact.objects.get(id=c_id)
        self.assertEqual(c.first_name, "TestFirst")
        self.assertEqual(c.last_name, "TestLast")
        self.assertEqual(c.needs_attention, False)
        self.assertEqual(c.first_name_lower, "testfirst")
        self.assertEqual(c.last_name_lower, "testlast")
        self.assertEqual(c.user.id, self.account_user.id)
        self.assertEqual(c.contact_type, "manual")


class NeedsAttentionTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        # Calling super here will create a RevUpUser and log it in.
        super(NeedsAttentionTests, self).setUp(login_now=False)
        self.account = self.user.current_seat.account
        self.account_user = self.account.account_user

        # Setup url where logged-in user owns a contact
        self.parent = ContactFactory.build(
            contact_type="merged", user=self.account_user, email_addresses=[],
            phone_numbers=[], addresses=[],
            needs_attention=True, is_primary=False)
        self.child1 = ContactFactory.create(first_name=self.parent.first_name,
                                            last_name=self.parent.last_name,
                                            user=self.account_user)
        self.child2 = ContactFactory.create(first_name=self.parent.first_name,
                                            last_name=self.parent.last_name,
                                            user=self.account_user)

        child_ids = [self.child1.id, self.child2.id]
        self.parent.attention_payload = {'demerge': child_ids}
        self.parent.save()

        self.url = reverse('account_contacts_api-list',
                           args=(self.account.id,)) + "needs_attention_list/"
        self.url2 = reverse('account_contacts_api-detail',
                            args=(self.account.id, self.parent.id)) + \
                    "needs_attention/"

    def test_permissions(self):
        ## Verify the API rejects a user that is not logged in
        response = self.client.get(self.url)
        self.assertNoCredentials(response)
        response = self.client.get(self.url2)
        self.assertNoCredentials(response)

        # Log in
        self._login(self.user)

        # Ensure api isn't getting permission from IsAdminUser
        self.assertFalse(self.user.is_admin)

        ## Verify user still does not have permission to view
        response = self.client.get(self.url)
        self.assertNoPermission(response)

        # Add an invalid admin permission and verify access is not granted
        self._add_admin_group(permissions=AdminPermissions.MOD_ACCOUNT)
        result = self.client.get(self.url)
        self.assertNoPermission(result)

        ## Add view permission to user and verify
        self._add_admin_group(permissions=AdminPermissions.VIEW_CONTACTS)
        result = self.client.get(self.url)
        self.assertContains(result, self.parent.id)

        result = self.client.get(self.url2)
        self.assertContains(result, self.child1.id)
        self.assertContains(result, self.child2.id)

        # Verify user cannot create
        response = self.client.post(self.url, data={})
        self.assertNoPermission(response)
        response = self.client.post(self.url2, data={})
        self.assertNoPermission(response)

        ## Add mod permissions and verify
        self._add_admin_group(permissions=AdminPermissions.MODIFY_CONTACTS)
        response = self.client.post(self.url2,
                                    data={"contacts": [self.child1.id]},
                                    format="json")
        self.assertEqual(response.status_code, 201)

    def test_get(self):
        # Log in
        self._login_and_add_admin_group(
                    permissions=AdminPermissions.VIEW_CONTACTS)
        ct_contact = CallTimeContactFactory(contact=self.parent,
                                            account=self.account)

        # Verify the parent is in the list
        result = self.client.get(self.url)
        returned_ids = {r['id'] for r in result.data['results']}
        self.assertIn(self.parent.id, returned_ids)
        self.assertNotIn(self.child1.id, returned_ids)
        self.assertNotIn(self.child2.id, returned_ids)

        # Verify the children are in the detail view
        result = self.client.get(self.url2)
        returned_ids = {r['id'] for r in result.data['contacts']}
        self.assertIn(self.child1.id, returned_ids)
        self.assertIn(self.child2.id, returned_ids)

        # Verify other contents
        self.assertEqual(ct_contact.id, result.data['call_time_contact']['id'])

        for email in self.child1.emails:
            self.assertContains(result, email)

        # Verify the API returns only the parent of reorganized contacts
        p = ContactFactory(user=self.account_user, contact_type="merged")
        c1 = ContactFactory(user=self.account_user, parent=p)
        c2 = ContactFactory(user=self.account_user, parent=p)
        self.parent.attention_payload["demerge"].extend([c1.id, c2.id])
        self.parent.save()
        result = self.client.get(self.url2)
        json_ = result.json()
        cids = [c["id"] for c in json_["contacts"]]
        self.assertIn(p.id, cids)
        self.assertNotIn(c1.id, cids)
        self.assertNotIn(c2.id, cids)

    def test_post_with_calltime_simple(self):
        """Verify a simple CallTime Contact is moved from old parent"""
        self._login_and_add_admin_group(
            permissions=AdminPermissions.MODIFY_CONTACTS)
        ct_contact = CallTimeContactFactory(contact=self.parent,
                                            account=self.account)

        # Verify simple case where one contacts is submitted
        self.assertFalse(CallTimeContact.objects.filter(
                                            contact=self.child1).exists())
        self.client.post(self.url2, data={"contacts": [self.child1.id]},
                         format="json")
        self.assertTrue(CallTimeContact.objects.filter(
                                            contact=self.child1).exists())

    def test_post_with_calltime_remerged(self):
        """Verify more complicated test case where CallTimeContact is
           moved to children that have a new parent.
        """
        self._login_and_add_admin_group(
            permissions=AdminPermissions.MODIFY_CONTACTS)
        ct_contact = CallTimeContactFactory(contact=self.parent,
                                            account=self.account)

        new_parent = ContactFactory(user=self.account_user,
                                    contact_type="merged")
        self.child1.parent = new_parent
        self.child1.is_primary = False
        self.child1.save()
        self.child2.parent = new_parent
        self.child2.is_primary = False
        self.child2.save()

        self.assertFalse(CallTimeContact.objects.filter(contact=new_parent)
                                                .exists())
        self.client.post(self.url2, data={"contacts": [new_parent.id]},
                         format="json")
        self.assertTrue(CallTimeContact.objects.filter(contact=new_parent)
                                               .exists())

    @mock.patch.object(Contact, 'get_preferred_gender', return_value='U')
    def test_post_with_calltime_force_merged(self, *args):
        """Verify more complicated test case where CallTimeContact is
           moved to children that have a new parent.
        """
        self._login_and_add_admin_group(
            permissions=AdminPermissions.MODIFY_CONTACTS)
        ct_contact = CallTimeContactFactory(contact=self.parent,
                                            account=self.account)

        self.assertIsNone(self.child1.parent)
        self.assertIsNone(self.child2.parent)
        self.client.post(self.url2,
                         data={"contacts": [self.child1.id, self.child2.id]},
                         format="json")
        self.child1.refresh_from_db()
        self.child2.refresh_from_db()
        self.assertIsNotNone(self.child1.parent)
        self.assertIsNotNone(self.child2.parent)
        self.assertEqual(self.child1.parent, self.child2.parent)

        self.assertTrue(CallTimeContact.objects.filter(
            contact=self.child1.parent).exists())


class ImportRecordsViewSetTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        # Calling super here will create a RevUpUser and log it in.
        super(ImportRecordsViewSetTests, self).setUp(login_now=False)

        # Setup url where logged-in user owns an import record
        self.ir = ImportRecordFactory(user=self.user)
        self.url = reverse('user_import_records_api-list',
                           args=(self.user.id,))

        # Setup url where non-logged in user owns an import record
        self.user2 = RevUpUserFactory()
        self.ir2 = ImportRecordFactory(user=self.user2)
        self.url2 = reverse('user_import_records_api-list',
                            args=(self.ir2.user_id,))

    def test_permissions(self):
        # # Verify the API rejects a user that is not logged in
        response = self.client.get(self.url)
        self.assertNoCredentials(response)
        response = self.client.get(self.url2)
        self.assertNoCredentials(response)

        # Log in
        self._login(self.user)

        # Ensure api isn't getting permission from IsAdminUser
        self.assertFalse(self.user.is_admin)

        ## Verify IsOwner permission gives access to users imports.
        response = self.client.get(self.url)
        self.assertContains(response,
                            '"id":{}'.format(self.ir.id))
        # Verify there are no other import records bleeding over.
        self.assertNotContains(response,
                               '"id":{}'.format(self.ir2.id))

        ## Verify users can't access another user's imports
        response = self.client.get(self.url2)
        self.assertNoPermission(response)

        ## Verify IsAdminUser gives access
        self.user.is_staff = True
        self.user.save()
        response = self.client.get(self.url2)
        self.assertContains(response,
                            '"id":{}'.format(self.ir2.id))
        self.assertNotContains(response,
                               '"id":{}'.format(self.ir.id))

    def test_serializer(self):
        self._login()
        result = self.client.get(self.url)
        self.verify_serializer(
            result, ["id", "user", "import_type", "label", "uid",
                     "import_dt", "import_success", "num_created",
                     "num_existing", "num_invalid", "num_merged", "num_total",
                     "num_effective", "account"])


class ContactNotesAPITests(AuthorizedUserTestCase):
    def setUp(self):
        super(ContactNotesAPITests, self).setUp()
        self.contact = ContactFactory(user=self.user)
        self.seat = self.user.current_seat
        self.account = self.seat.account
        self.url = reverse('contact_notes_api-list',
                           args=(self.account.id, self.seat.id,
                                 self.contact.id,))

    def test_create(self):
        test_data = {'notes1': "lkjsdf", 'notes2': 'qwieou'}
        test_record = dict(contact=(self.contact.id + 1), **test_data)

        # Control test, make sure no settings exist yet
        self.assertFalse(self.contact.notes.exists())

        response = self.client.post(self.url, data=json.dumps(test_record),
                                    content_type="application/json")
        self.assertEqual(response.status_code, 201)
        self.assertTrue(self.contact.notes.exists())

        new_record = ContactNotes.objects.values(
            'account', 'contact', 'notes1', 'notes2')\
                .get(pk=response.data['id'])

        # Make sure that the values for account and contact are taken from the
        # url instead of the post body.
        self.assertNotEqual(new_record, test_record)
        self.assertEqual(new_record,
                         dict(account=self.account.id,
                              contact=self.contact.id, **test_data))

        # Ensure creating a duplicate fails
        response = self.client.post(self.url, data=json.dumps(test_record),
                                    content_type="application/json")

        self.assertContains(
            response,
            'The fields contact, account must make a unique set.',
            status_code=400)

    def test_update(self):
        contact_note = ContactNotes.objects.create(
            contact=self.contact, account=self.account,
            notes1="original notes1", notes2="original notes2")

        original_contact_note = ContactNotes.objects.values(
            'account', 'contact', 'notes1', 'notes2')\
                .get(pk=contact_note.id)

        test_record = dict(account=(self.account.id + 1),
                           contact=(self.contact.id + 1),
                           notes1="modified notes1",
                           notes2="")

        url = reverse('contact_notes_api-detail',
                      args=(self.account.id, self.seat.id,
                            self.contact.id, contact_note.id))

        response = self.client.put(url, data=json.dumps(test_record),
                                    content_type="application/json")

        self.assertEqual(response.status_code, 200)

        updated_contact_note = ContactNotes.objects.values(
            'account', 'contact', 'notes1', 'notes2')\
                .get(pk=contact_note.id)

        # Ensure the contact note was updated and does not match the original
        # record.
        self.assertNotEqual(updated_contact_note, original_contact_note)

        # Make sure that neither contact nor account were changed by the update
        self.assertNotEqual(updated_contact_note, test_record)

        expected_values = dict(account=self.account.id,
                               contact=self.contact.id,
                               notes1="modified notes1",
                               notes2="")
        self.assertEqual(updated_contact_note, expected_values)

    def test_permissions(self):
        other_seat = SeatFactory()
        other_user = other_seat.user
        other_account = other_seat.account
        other_contact = ContactFactory(user=other_user)

        contact_note = ContactNotes.objects.create(
            contact=self.contact, account=self.account,
            notes1="original notes1", notes2="original notes2")

        list_url = self.url
        detail_url = reverse('contact_notes_api-detail',
                             args=(self.account.id, self.seat.id,
                                   self.contact.id, contact_note.id))

        # Verify any unauthorized user is rejected
        self.client.logout()
        response = self.client.get(list_url)
        self.assertNoCredentials(response)

        response = self.client.get(detail_url)
        self.assertNoCredentials(response)

        # Login the user
        self._login(self.user)
        # Verify user is not an admin, or that would invalidate the test.
        self.assertFalse(self.user.is_admin)
        self.assertFalse(self.user.is_account_admin())

        # Contact must belong to user
        response = self.client.get(
            reverse('contact_notes_api-list',
                    args=(self.account.id, self.seat.id,
                          other_contact.id)))
        self.assertNoPermission(response)

        # User must be a member of the account
        response = self.client.get(
            reverse('contact_notes_api-list',
                    args=(other_account.id, self.seat.id,
                          self.contact.id)))
        self.assertNoPermission(response)


class ContactSetResultsContactNotesAPITests(AuthorizedUserTestCase):
    def setUp(self):
        super(ContactSetResultsContactNotesAPITests, self).setUp()
        self.contact = ContactFactory(user=self.user)
        self.seat = self.user.current_seat
        self.account = self.seat.account
        self.contact_set = ContactSetFactory(account=self.account,
                                             user=self.user)
        self.url = reverse('contact_set_results_notes_api-list',
                           args=(self.user.id, self.seat.id,
                                 self.contact_set.id, self.contact.id,))

    def test_create(self):
        test_data = {'notes1': "lkjsdf", 'notes2': 'qwieou'}
        test_record = dict(contact=(self.contact.id + 1), **test_data)

        # Control test, make sure no settings exist yet
        self.assertFalse(self.contact.notes.exists())

        response = self.client.post(self.url, data=json.dumps(test_record),
                                    content_type="application/json")
        self.assertEqual(response.status_code, 201)
        self.assertTrue(self.contact.notes.exists())

        new_record = ContactNotes.objects.values(
            'account', 'contact', 'notes1', 'notes2') \
            .get(pk=response.data['id'])

        # Make sure that the values for account and contact are taken from the
        # url instead of the post body.
        self.assertNotEqual(new_record, test_record)
        self.assertEqual(new_record,
                         dict(account=self.account.id,
                              contact=self.contact.id, **test_data))

        # Ensure creating a duplicate fails
        response = self.client.post(self.url, data=json.dumps(test_record),
                                    content_type="application/json")

        self.assertContains(
            response,
            'The fields contact, account must make a unique set.',
            status_code=400)

    def test_update(self):
        contact_note = ContactNotes.objects.create(
            contact=self.contact, account=self.account,
            notes1="original notes1", notes2="original notes2")

        original_contact_note = ContactNotes.objects.values(
            'account', 'contact', 'notes1', 'notes2') \
            .get(pk=contact_note.id)

        test_record = dict(account=(self.account.id + 1),
                           contact=(self.contact.id + 1),
                           notes1="modified notes1",
                           notes2="")

        url = reverse('contact_set_results_notes_api-detail',
                      args=(self.user.id, self.seat.id, self.contact_set.id,
                            self.contact.id, contact_note.id))

        response = self.client.put(url, data=json.dumps(test_record),
                                   content_type="application/json")

        self.assertEqual(response.status_code, 200)

        updated_contact_note = ContactNotes.objects.values(
            'account', 'contact', 'notes1', 'notes2') \
            .get(pk=contact_note.id)

        # Ensure the contact note was updated and does not match the original
        # record.
        self.assertNotEqual(updated_contact_note, original_contact_note)

        # Make sure that neither contact nor account were changed by the update
        self.assertNotEqual(updated_contact_note, test_record)

        expected_values = dict(account=self.account.id,
                               contact=self.contact.id,
                               notes1="modified notes1",
                               notes2="")
        self.assertEqual(updated_contact_note, expected_values)

    def test_permissions(self):
        other_seat = SeatFactory()
        other_user = other_seat.user
        other_contact = ContactFactory(user=other_user)

        contact_note = ContactNotes.objects.create(
            contact=self.contact, account=self.account,
            notes1="original notes1", notes2="original notes2")

        list_url = self.url
        detail_url = reverse('contact_set_results_notes_api-detail',
                             args=(self.user.id, self.seat.id,
                                   self.contact_set.id,
                                   self.contact.id, contact_note.id))

        # Verify user is not an admin, or that would invalidate the test.
        self.assertFalse(self.user.is_admin)
        self.assertFalse(self.user.is_account_admin())

        # Contact must belong to user
        response = self.client.get(
            reverse('contact_set_results_notes_api-list',
                    args=(self.user.id, self.seat.id,
                          self.contact_set.id, other_contact.id)))
        self.assertNoPermission(response)

        # Requesting user must be the same as the url user
        response = self.client.get(
            reverse('contact_set_results_notes_api-list',
                    args=(other_user.id, self.seat.id,
                          self.contact_set.id, self.contact.id)))
        self.assertEqual(response.status_code, 404)

        # Verify any unauthorized user is rejected
        self.client.logout()
        response = self.client.get(list_url)
        self.assertNoCredentials(response)

        response = self.client.get(detail_url)
        self.assertNoCredentials(response)

    def test_contact_set_permissions(self):
        # Verify two admin users can see the same notes, if the contact
        # is owned by the account
        other_admin = SeatFactory(account=self.account)
        self._add_admin_group(self.user,
                              permissions=AdminPermissions.VIEW_CONTACTS)

        contact = ContactFactory(user=self.account.account_user)
        contact_set = ContactSetFactory(account=self.account,
                                        user=self.account.account_user)

        # Verify one of the users can create a note
        test_data = {'notes1': "lkjsdf", 'notes2': 'qwieou'}
        url = reverse('contact_set_results_notes_api-list',
                      args=(self.user.id, self.seat.id,
                            contact_set.id, contact.id))
        response = self.client.post(url, data=json.dumps(test_data),
                                    content_type="application/json")
        self.assertEqual(response.status_code, 201)

        ## Verify a non-admin user cannot see the account notes
        contact_note_id = json.loads(response.content)["id"]
        client = Client()
        self._login(user=other_admin.user, client=client)
        url = reverse('contact_set_results_notes_api-detail',
                      args=(other_admin.user.id, other_admin.id,
                            contact_set.id, contact.id,
                            contact_note_id))
        response = client.get(url)
        self.assertNoPermission(response)

        ## Verify the other admin user can view and modify that note
        self._add_admin_group(other_admin.user,
                              permissions=AdminPermissions.VIEW_CONTACTS)
        response = client.get(url)
        self.assertContains(response, '"id":{}'.format(contact_note_id))
        self.assertContains(response, '"notes1":"lkjsdf"')
        self.assertContains(response, '"notes2":"qwieou"')


class SeatContactsRedirectViewSet(AuthorizedUserTestCase):
    def setUp(self):
        super(SeatContactsRedirectViewSet, self).setUp()
        self.contact = ContactFactory(user=self.user)
        self.seat = self.user.current_seat
        self.account = self.seat.account

    def test_redirect(self):
        list_url = reverse('seat_contacts_api-list', args=(
            self.account.id, self.seat.id))
        detail_url = reverse('seat_contacts_api-detail', args=(
            self.account.id, self.seat.id, self.contact.id))

        target_list_url = reverse('user_contacts_api-list', args=(
            self.user.id,))
        target_detail_url = reverse('user_contacts_api-detail', args=(
            self.user.id, self.contact.id))

        response = self.client.get(list_url, follow=False)
        self.assertRedirects(response, target_list_url, status_code=301,
                             fetch_redirect_response=False)

        response = self.client.get(detail_url, follow=False)
        self.assertRedirects(response, target_detail_url, status_code=301,
                             fetch_redirect_response=False)

    def test_permissions(self):
        other_seat = SeatFactory()
        other_user = other_seat.user
        other_account = other_seat.account
        other_contact = ContactFactory(user=other_user)

        list_url = reverse('seat_contacts_api-list',
                           args=(self.account.id, self.seat.id))
        detail_url = reverse('seat_contacts_api-detail',
                             args=(self.account.id, self.seat.id,
                                   self.contact.id))

        # is authenticated
        # Verify any unauthorized user is rejected
        self.client.logout()
        response = self.client.get(list_url, follow=False)
        self.assertNoCredentials(response)

        response = self.client.get(detail_url, follow=False)
        self.assertNoCredentials(response)

        # Login the user
        self._login(self.user)
        # Verify user is not an admin, or that would invalidate the test.
        self.assertFalse(self.user.is_admin)
        self.assertFalse(self.user.is_account_admin())

        # Is account memeber
        response = self.client.get(
            reverse('seat_contacts_api-list',
                    args=(other_account.id, self.seat.id)), follow=False)
        self.assertNoPermission(response)

        response = self.client.get(
            reverse('seat_contacts_api-detail',
                    args=(other_account.id, self.seat.id, self.contact.id)),
            follow=False)
        self.assertNoPermission(response)
