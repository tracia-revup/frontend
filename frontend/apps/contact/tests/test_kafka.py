from frontend.libs.utils.test_utils import TestCase
from ..kafka import ManualContactTransaction, ContactDeleteTransaction
from ..factories import (
    ContactFactory, AddressFactory, PhoneNumberFactory, EmailAddressFactory)


class ManualContactTransactionTests(TestCase):
    def setUp(self):
        self.mct = ManualContactTransaction(send_contacts_to_backend=False)
        self.contact1 = ContactFactory()
        self.user = self.contact1.user
        self.contact2 = ContactFactory(user=self.user)

    def test_create(self):
        address = AddressFactory(contact=self.contact1)
        phone1 = PhoneNumberFactory(contact=self.contact1)
        phone2 = PhoneNumberFactory(contact=self.contact2)
        self.mct.create(self.contact1, address)
        self.mct.create(self.contact1, phone1)
        self.mct.create(self.contact2, phone2)
        self.assertEqual(self.mct.contact_creates,
                         {self.contact1: dict(
                              Address=[
                                  {'city': str(address.city),
                                   'state': str(address.region),
                                   'zip_code': str(address.post_code),
                                   'address': str(address.street)}],
                              PhoneNumber=[str(phone1.rfc3966)]),
                          self.contact2: dict(
                              PhoneNumber=[str(phone2.rfc3966)]
                          )})

    def test_archive(self):
        email = EmailAddressFactory(contact=self.contact1)
        phone1 = PhoneNumberFactory(contact=self.contact1)
        phone2 = PhoneNumberFactory(contact=self.contact2)
        self.mct.archive(self.contact1, email)
        self.mct.archive(self.contact1, phone1)
        self.mct.archive(self.contact2, phone2)
        self.assertEqual(self.mct.contact_archives,
                         {self.contact1: dict(
                             EmailAddress=[email.address],
                             PhoneNumber=[str(phone1.rfc3966)]),
                          self.contact2: dict(
                             PhoneNumber=[str(phone2.rfc3966)])
                         })

    def test_build_contact_actions(self):
        address = AddressFactory(contact=self.contact1)
        email1 = EmailAddressFactory(contact=self.contact1)
        email2 = EmailAddressFactory(contact=self.contact2)
        phone1 = PhoneNumberFactory(contact=self.contact1)
        phone2 = PhoneNumberFactory(contact=self.contact2)
        self.mct.create(self.contact1, address)
        self.mct.create(self.contact1, phone1)
        self.mct.create(self.contact2, email2)
        self.mct.archive(self.contact2, phone2)
        self.mct.archive(self.contact1, email1)

        contact_actions = self.mct.build_contact_actions()
        self.maxDiff = None
        self.assertCountEqual(contact_actions, [
            {'user_id': self.user.id,
             'contact_type': str(self.contact1.contact_type),
             'value': {
                 'EmailAddress': [str(email1.address)]},
             'txid': self.mct.transaction_id,
             'contact_id': str(self.contact1.contact_id),
             'composite_id': str(self.contact1.composite_id),
             'action': 'archive'},

            {'user_id': self.user.id,
             'contact_type': str(self.contact2.contact_type),
             'value': {
                 'PhoneNumber': [str(phone2.rfc3966)]},
             'txid': self.mct.transaction_id,
             'contact_id': str(self.contact2.contact_id),
             'composite_id': str(self.contact2.composite_id),
             'action': 'archive'},

            {'user_id': self.user.id,
             'contact_type': str(self.contact1.contact_type),
             'value': {
                 'PhoneNumber': [str(phone1.rfc3966)],
                 'Address': [{
                     'city': str(address.city),
                     'state': str(address.region),
                     'zip_code': str(address.post_code),
                     'address': str(address.street)}]
             },
             'txid': self.mct.transaction_id,
             'contact_id': str(self.contact1.contact_id),
             'composite_id': str(self.contact1.composite_id),
             'action': 'create'},

            {'user_id': self.user.id,
             'contact_type': str(self.contact2.contact_type),
             'value': {
                 'EmailAddress': [str(email2.address)]},
             'txid': self.mct.transaction_id,
             'contact_id': str(self.contact2.contact_id),
             'composite_id': str(self.contact2.composite_id),
             'action': 'create'}
        ])


class ContactDeleteTransactionTests(TestCase):
    def setUp(self):
        self.cdt = ContactDeleteTransaction(send_contacts_to_backend=False)
        self.contact1 = ContactFactory()
        self.user = self.contact1.user
        self.contact2 = ContactFactory(user=self.user)
        self.contact3 = ContactFactory()
        self.maxDiff = None

    def test_delete(self):
        self.cdt.delete(self.contact1)
        self.cdt.delete(self.contact2)
        self.cdt.delete(self.contact3)
        self.assertCountEqual(self.cdt.contact_deletes,
                              {self.contact1: None,
                               self.contact2: None,
                               self.contact3: None})

    def test_build_contact_actions(self):
        self.cdt.delete(self.contact1)
        self.cdt.delete(self.contact2)
        self.cdt.delete(self.contact3)

        contact_actions = self.cdt.build_contact_actions()
        self.assertCountEqual(contact_actions, [
            {'user_id': self.user.id,
             'contact_type': str(self.contact1.contact_type),
             'value': None,
             'txid': self.cdt.transaction_id,
             'contact_id': str(self.contact1.contact_id),
             'composite_id': str(self.contact1.composite_id),
             'action': 'delete'},
            {'user_id': self.user.id,
             'contact_type': str(self.contact2.contact_type),
             'value': None,
             'txid': self.cdt.transaction_id,
             'contact_id': str(self.contact2.contact_id),
             'composite_id': str(self.contact2.composite_id),
             'action': 'delete'},
            {'user_id': self.contact3.user.id,
             'contact_type': str(self.contact3.contact_type),
             'value': None,
             'txid': self.cdt.transaction_id,
             'contact_id': str(self.contact3.contact_id),
             'composite_id': str(self.contact3.composite_id),
             'action': 'delete'},
        ])
