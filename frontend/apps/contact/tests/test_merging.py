
from collections import OrderedDict
import datetime
import itertools
import networkx
from operator import attrgetter

from faker import Factory
from unittest.mock import patch, MagicMock, DEFAULT, call, ANY

from frontend.apps.analysis.factories import (
    AnalysisFactory, ResultFactory, Result)
from frontend.apps.authorize.factories import RevUpUserFactory
from frontend.apps.call_time.factories import (
    CallTimeContactFactory, CallTimeContactSetFactory, CallLogFactory)
from frontend.apps.call_time.models import CallTimeContact
from frontend.apps.campaign.models import Prospect
from frontend.apps.campaign.factories import (
    ProspectFactory, ProspectCollisionFactory, EventFactory, AccountFactory)
from frontend.apps.contact.exceptions import MergeConflict
from frontend.apps.contact.factories import (
    ContactFactory, ContactNotesFactory, EmailAddressFactory,
    PhoneNumberFactory, AddressFactory)
from frontend.apps.contact.analysis import remerge
from frontend.apps.contact.analysis.contact_import.base import ContactImportBase
# Import _handle_first_name individually so we have a handle to the unmocked
# version of it. Used in a test below.
from frontend.apps.contact.analysis.remerge import _handle_first_name
from frontend.apps.contact.models import Contact, LimitTracker
from frontend.apps.entities.models import PersonDimension, ContactLink
from frontend.libs.utils.general_utils import union_find_reducer
from frontend.libs.utils.string_utils import strip_punctuation
from frontend.libs.utils.test_utils import (
    RemoveShortDescription, TestCase, TransactionTestCase)


@patch.multiple(remerge, _resolve_parent_prospect_strategy=DEFAULT,
                _resolve_parent_notes_strategy=DEFAULT,
                _resolve_parent_score_strategy=DEFAULT,
                _resolve_parent_children_count_strategy=DEFAULT,
                _resolve_parent_oldest_strategy=DEFAULT)
class ResolveParentTests(TestCase):
    """Test the strategy resolution order. Ensure that strategies are used in
    the correct order, and that if a strategy returns something that none of
    the other prospects are used.
    """

    def test_resolve_order_prospects(self, _resolve_parent_prospect_strategy,
                                     _resolve_parent_notes_strategy,
                                     _resolve_parent_score_strategy,
                                     _resolve_parent_children_count_strategy,
                                     _resolve_parent_oldest_strategy):
        fake_contacts = object()
        remerge.resolve_parent(fake_contacts)
        self.assertTrue(_resolve_parent_prospect_strategy.called)
        self.assertFalse(_resolve_parent_notes_strategy.called)
        self.assertFalse(_resolve_parent_score_strategy.called)
        self.assertFalse(_resolve_parent_children_count_strategy.called)
        self.assertFalse(_resolve_parent_oldest_strategy.called)

    def test_resolve_order_notes(self, _resolve_parent_prospect_strategy,
                                 _resolve_parent_notes_strategy,
                                 _resolve_parent_score_strategy,
                                 _resolve_parent_children_count_strategy,
                                 _resolve_parent_oldest_strategy):
        _resolve_parent_prospect_strategy.side_effect = remerge.NoParentFound
        fake_contacts = object()
        remerge.resolve_parent(fake_contacts)
        self.assertTrue(_resolve_parent_prospect_strategy.called)
        self.assertTrue(_resolve_parent_notes_strategy.called)
        self.assertFalse(_resolve_parent_score_strategy.called)
        self.assertFalse(_resolve_parent_children_count_strategy.called)
        self.assertFalse(_resolve_parent_oldest_strategy.called)

    def test_resolve_order_score(self, _resolve_parent_prospect_strategy,
                                 _resolve_parent_notes_strategy,
                                 _resolve_parent_score_strategy,
                                 _resolve_parent_children_count_strategy,
                                 _resolve_parent_oldest_strategy):
        _resolve_parent_prospect_strategy.side_effect = remerge.NoParentFound
        _resolve_parent_notes_strategy.side_effect = remerge.NoParentFound
        fake_contacts = object()
        remerge.resolve_parent(fake_contacts)
        self.assertTrue(_resolve_parent_prospect_strategy.called)
        self.assertTrue(_resolve_parent_notes_strategy.called)
        self.assertTrue(_resolve_parent_score_strategy.called)
        self.assertFalse(_resolve_parent_children_count_strategy.called)
        self.assertFalse(_resolve_parent_oldest_strategy.called)

    def test_resolve_order_child_count(self, _resolve_parent_prospect_strategy,
                                       _resolve_parent_notes_strategy,
                                       _resolve_parent_score_strategy,
                                       _resolve_parent_children_count_strategy,
                                       _resolve_parent_oldest_strategy):
        _resolve_parent_prospect_strategy.side_effect = remerge.NoParentFound
        _resolve_parent_notes_strategy.side_effect = remerge.NoParentFound
        _resolve_parent_score_strategy.side_effect = remerge.NoParentFound
        fake_contacts = object()
        remerge.resolve_parent(fake_contacts)
        self.assertTrue(_resolve_parent_prospect_strategy.called)
        self.assertTrue(_resolve_parent_notes_strategy.called)
        self.assertTrue(_resolve_parent_score_strategy.called)
        self.assertTrue(_resolve_parent_children_count_strategy.called)
        self.assertFalse(_resolve_parent_oldest_strategy.called)

    def test_resolve_order_oldest(self, _resolve_parent_prospect_strategy,
                                  _resolve_parent_notes_strategy,
                                  _resolve_parent_score_strategy,
                                  _resolve_parent_children_count_strategy,
                                  _resolve_parent_oldest_strategy):
        _resolve_parent_prospect_strategy.side_effect = remerge.NoParentFound
        _resolve_parent_notes_strategy.side_effect = remerge.NoParentFound
        _resolve_parent_score_strategy.side_effect = remerge.NoParentFound
        _resolve_parent_children_count_strategy.side_effect = remerge.NoParentFound
        fake_contacts = object()
        remerge.resolve_parent(fake_contacts)
        self.assertTrue(_resolve_parent_prospect_strategy.called)
        self.assertTrue(_resolve_parent_notes_strategy.called)
        self.assertTrue(_resolve_parent_score_strategy.called)
        self.assertTrue(_resolve_parent_children_count_strategy.called)
        self.assertTrue(_resolve_parent_oldest_strategy.called)


class ResolveParentOldestStrategyTests(TestCase):
    def test_oldest_strategy(self):
        lowest_contact = ContactFactory(pk=1001)
        contacts = [ContactFactory(pk=8001),
                    ContactFactory(pk=3001),
                    ContactFactory(pk=7001)]
        contacts.insert(2, lowest_contact)
        self.assertEqual(remerge._resolve_parent_oldest_strategy(contacts),
                         lowest_contact)


class ResolveParentChildCountStrategyTests(RemoveShortDescription, TestCase):
    def test_fail_on_no_children(self):
        """Verify this strategy does not return a parent when none of the
        contacts have children
        """
        contacts = [
            ContactFactory(),
            ContactFactory(),
            ContactFactory(),
            ContactFactory(),
        ]
        with self.assertRaises(remerge.NoParentFound):
            remerge._resolve_parent_children_count_strategy(contacts)

    def test_fail_on_same_children_count(self):
        """Verify this strategy does not return a parent when the top 2
        contacts have the same number of children.
        """
        contacts = [
            ContactFactory(),
            ContactFactory(contacts=[
                ContactFactory(),
                ContactFactory(),
                ContactFactory(),
            ]),
            ContactFactory(contacts=[
                ContactFactory(),
                ContactFactory(),
                ContactFactory(),
            ]),
            ContactFactory(),
            ContactFactory(),
        ]
        with self.assertRaises(remerge.NoParentFound):
            remerge._resolve_parent_children_count_strategy(contacts)

    def test_contact_with_most_children_wins(self):
        """Verify that if a contact that has more contacts than any other does
        exist, that it is the winner.
        """
        winner = ContactFactory(contacts=[
            ContactFactory(),
            ContactFactory(),
            ContactFactory(),
            ContactFactory(),
        ])
        contacts = [
            ContactFactory(),
            ContactFactory(contacts=[
                ContactFactory(),
                ContactFactory(),
                ContactFactory(),
            ]),
            ContactFactory(contacts=[
                ContactFactory(),
                ContactFactory(),
                ContactFactory(),
            ]),
            ContactFactory(contacts=[
                ContactFactory(),
                ContactFactory(),
            ]),
            ContactFactory(),
        ]
        contacts.insert(2, winner)

        self.assertIs(
            winner,
            remerge._resolve_parent_children_count_strategy(contacts))

    def test_only_contact_with_children_wins(self):
        """Test edge case of only one contact having any children.
        """
        winner = ContactFactory(contacts=[
            ContactFactory(),
            ContactFactory(),
            ContactFactory(),
            ContactFactory(),
        ])
        contacts = [
            ContactFactory(),
            ContactFactory(),
            ContactFactory(),
        ]
        contacts.insert(2, winner)

        self.assertIs(
            winner,
            remerge._resolve_parent_children_count_strategy(contacts))


class ResolveParentAnalysisScoreStrategyTests(RemoveShortDescription, TestCase):
    def test_fail_on_no_analysis(self):
        """Verify this strategy does not return a parent when no analysis is found.

        Also that the strategy doesn't just crash unexpectedly.
        """
        contacts = [
            ContactFactory(),
            ContactFactory(),
            ContactFactory(),
            ContactFactory(),
        ]
        with self.assertRaises(remerge.NoParentFound):
            remerge._resolve_parent_score_strategy(contacts)

    def test_fail_on_same_score(self):
        """Verify this strategy does not return a parent when the 2 top scoring
        contacts have the same score.
        """
        analysis = AnalysisFactory()
        contacts = [
            ContactFactory(user=analysis.user),
            ContactFactory(user=analysis.user),
            ContactFactory(user=analysis.user),
            ContactFactory(user=analysis.user),
        ]
        for c in contacts:
            ResultFactory(analysis=analysis, score=100, contact=c)

        with self.assertRaises(remerge.NoParentFound):
            remerge._resolve_parent_score_strategy(contacts)

    def test_contact_highest_score_wins(self):
        """Verify that if a contact with a highest score does exist, that it
        wins.
        """
        analysis = AnalysisFactory()
        contacts = [
            ContactFactory(user=analysis.user),
            ContactFactory(user=analysis.user),
            ContactFactory(user=analysis.user),
            ContactFactory(user=analysis.user),
        ]
        for i, c in enumerate(contacts):
            ResultFactory(analysis=analysis, score=((i + 1) * 10), contact=c)

        self.assertEqual(
            remerge._resolve_parent_score_strategy(contacts),
            contacts[-1])

    def test_contact_only_score_wins(self):
        """Test edge case with only contact having an analysis score
        """
        analysis = AnalysisFactory()
        contacts = [
            ContactFactory(user=analysis.user),
            ContactFactory(user=analysis.user),
            ContactFactory(user=analysis.user),
            ContactFactory(user=analysis.user),
        ]
        ResultFactory(analysis=analysis, contact=contacts[0])

        self.assertEqual(
            remerge._resolve_parent_score_strategy(contacts),
            contacts[0])

    def test_most_recent_analysis_used(self):
        """Verify that if there are multiple analysis available, that we choose
        the most recent analysis.
        """
        user = RevUpUserFactory()
        analysis_fail = AnalysisFactory(user=user,
                                        modified=datetime.date(2014, 1, 5))
        contacts = [
            ContactFactory(user=user),
            ContactFactory(user=user),
            ContactFactory(user=user),
            ContactFactory(user=user),
        ]
        # Give all contacts the same score on the older analysis. That way if
        # the wrong analysis is used, we'll know because an exception is
        # raised.
        for c in contacts:
            ResultFactory(analysis=analysis_fail, score=100, contact=c)

        analysis = AnalysisFactory(user=user,
                                   modified=datetime.date(2016, 1, 5))
        ResultFactory(analysis=analysis, contact=contacts[0])

        self.assertEqual(
            remerge._resolve_parent_score_strategy(contacts),
            contacts[0])


class ResolveParentNotesStrategyTests(RemoveShortDescription, TestCase):
    def setUp(self):
        self.contacts = [
            ContactFactory(),
            ContactFactory(),
            ContactFactory(),
            ContactFactory(),
        ]

    def test_fail_on_no_notes(self):
        """Verify this strategy does not return a parent when none of the
        contacts have have notes
        """
        with self.assertRaises(remerge.NoParentFound):
            remerge._resolve_parent_notes_strategy(self.contacts)

    def test_fail_on_no_filled_and_multiple_empty(self):
        """Verify this strategy does not return a parent when more than one
        contact have ContactNotes, but none of those notes have any contents.
        """
        for c in self.contacts:
            ContactNotesFactory(contact=c, notes1='', notes2='')

        with self.assertRaises(remerge.NoParentFound):
            remerge._resolve_parent_notes_strategy(self.contacts)

    def test_fail_on_multiple_notes(self):
        """Verify this strategy does not return a parent when multiple contacts
        have filled notes, and that when there are filled notes that empty
        notes are ignored.
        """
        for c in self.contacts:
            ContactNotesFactory(contact=c)
        ContactNotesFactory(contact=self.contacts[0], notes1='', notes2='')

        with self.assertRaises(remerge.NoParentFound):
            remerge._resolve_parent_notes_strategy(self.contacts)

    def test_only_contact_with_empty_note_wins(self):
        """Verify that if only one contact has notes, even if that note is
        empty, that contact wins.
        """
        ContactNotesFactory(contact=self.contacts[0], notes1='', notes2='')

        self.assertEqual(
            remerge._resolve_parent_notes_strategy(self.contacts),
            self.contacts[0])

    def test_only_contact_with_filled_note_wins_over_empty(self):
        """Verify that contacts with filled out notes always win over empty
        notes.
        """
        ContactNotesFactory(contact=self.contacts[0], notes1='', notes2='')
        ContactNotesFactory(contact=self.contacts[1])

        self.assertEqual(
            remerge._resolve_parent_notes_strategy(self.contacts),
            self.contacts[1])


class ResolveParentProspectStrategyTests(RemoveShortDescription, TestCase):
    def setUp(self):
        self.contacts = [
            ContactFactory(),
            ContactFactory(),
            ContactFactory(),
            ContactFactory(),
        ]

    def test_fail_on_no_prospects(self):
        """Verify that this strategy raises no parent when none have Prospect
        records.
        """
        with self.assertRaises(remerge.NoParentFound):
            remerge._resolve_parent_prospect_strategy(self.contacts)

    def test_only_one_with_prospect_wins(self):
        """Verify that if only one contact has a prospect, even if it is empty,
        that contact wins.
        """
        ProspectFactory(contact=self.contacts[0], soft_amt=0, hard_amt=0,
                        in_amt=0)
        self.assertEqual(
            remerge._resolve_parent_prospect_strategy(self.contacts),
            self.contacts[0])

    def test_only_one_with_filled_prospect_wins(self):
        """Verify that if multiple contacts have prospect records, but only one
        contact has a non-zero prospect record, that contact wins.
        """
        for c in self.contacts:
            ProspectFactory(contact=c, soft_amt=0, hard_amt=0, in_amt=0)
        ProspectFactory(contact=self.contacts[0])
        self.assertEqual(
            remerge._resolve_parent_prospect_strategy(self.contacts),
            self.contacts[0])

    def test_no_resolution_possible_on_overlap(self):
        """Verify that if multiple contacts have a non-zero prospect record of
        differing amounts for the same event, that `None` is returned to
        indicate that no parent can be determined for this set of contacts.
        This also prevents the `resolve_parent` function from trying the next
        strategy in line.
        """
        event = EventFactory()
        for c in self.contacts:
            ProspectFactory(contact=c, event=event)

        self.assertIsNone(
            remerge._resolve_parent_prospect_strategy(self.contacts))

    def test_most_recently_updated_wins_if_no_overlap(self):
        """Verify that if multiple contacts have prospects that have filled out
        contents for the same event, but there is no overlap with differing
        contents, that the contact with the most recently updated prospect
        wins.
        """
        event1 = EventFactory()
        event2 = EventFactory()
        for event in (event1, event2):
            for i, c in enumerate(self.contacts):
                if i % 2:
                    contents = dict(soft_amt=10, hard_amt=20, in_amt=30)
                else:
                    contents = dict(soft_amt=0, hard_amt=0, in_amt=0)
                prospect = ProspectFactory(contact=c, event=event, **contents)

        self.assertEqual(
            remerge._resolve_parent_prospect_strategy(self.contacts),
            prospect.contact)


@patch.multiple(remerge, set_parent_children=DEFAULT,
                _dryrun_set_parent_children=DEFAULT,
                split_parent_children=DEFAULT)
@patch.multiple(remerge.ContactPartitioner,
                partition_by_generational_suffixes=DEFAULT,
                partition_by_addresses_and_middle_names=DEFAULT)
class ProcessGroupTests(RemoveShortDescription, TestCase):
    def setUp(self):
        self.user = RevUpUserFactory()
        self.contacts = [
            ContactFactory(user=self.user),
            ContactFactory(user=self.user),
            ContactFactory(user=self.user),
            ContactFactory(user=self.user),
        ]
        self.contact_pks = [c.pk for c in self.contacts]

    def test_multiple_generational_suffixes(self, set_parent_children,
                                            split_parent_children,
                                            partition_by_generational_suffixes,
                                            partition_by_addresses_and_middle_names,
                                            **kwargs):
        """Verify process_contact_ids handles contacts being partitioned by
        generational suffixes properly
        """
        partition_by_generational_suffixes.side_effect = lambda x: [
            self.contacts[:1],
            self.contacts[1:]
        ]
        split_parent_children.side_effect = [
            (self.contacts[0], None),
            (self.contacts[1], self.contacts[2:])
        ]
        partition_by_addresses_and_middle_names.side_effect = lambda x, **_: [x]
        with patch.object(remerge.Contact.objects, 'filter',
                          return_value=self.contacts):
            remerge.process_contact_ids(self.contact_pks, self.user)
        partition_by_generational_suffixes.assert_called_once_with(
            self.contacts)
        self.assertCountEqual(
            split_parent_children.mock_calls,
            [call(set(self.contacts[:1])),
             call(set(self.contacts[1:]))])
        self.assertEqual(
            set_parent_children.mock_calls,
            [call(self.contacts[0], None, index_filters=None),
             call(self.contacts[1], self.contacts[2:], index_filters=None)])

    def test_normal_case(self, set_parent_children, split_parent_children,
                         partition_by_generational_suffixes,
                         partition_by_addresses_and_middle_names,
                         **kwargs):
        """Verify everything works as expected even during the normal case.
        """
        partition_by_generational_suffixes.side_effect = lambda x: [x]
        partition_by_addresses_and_middle_names.side_effect = lambda x, **_: [x]
        def _split_side_effect(group):
            group = sorted(group, key=attrgetter('id'))
            return group[0], group[1:]
        split_parent_children.side_effect = _split_side_effect
        with patch.object(remerge.Contact.objects, 'filter',
                          return_value=self.contacts):
            remerge.process_contact_ids(self.contact_pks, self.user)
        partition_by_generational_suffixes.assert_called_once_with(
            self.contacts)
        set_parent_children.assert_called_once_with(
            self.contacts[0], self.contacts[1:], index_filters=None)

    def test_no_parent_no_save(self, set_parent_children,
                               _dryrun_set_parent_children,
                               split_parent_children,
                               partition_by_generational_suffixes,
                               partition_by_addresses_and_middle_names,
                               **kwargs):
        """Verify that if no contact is determined to be parent, that the save
        function is never called.
        """
        partition_by_generational_suffixes.side_effect = lambda x: [x]
        partition_by_addresses_and_middle_names.side_effect = lambda x, **_: [x]
        split_parent_children.side_effect = lambda x: (None, x)
        remerge.process_contact_ids(self.contact_pks, self.user)
        self.assertFalse(set_parent_children.called)
        self.assertFalse(_dryrun_set_parent_children.called)

    def test_dry_run(self, _dryrun_set_parent_children, set_parent_children,
                     partition_by_generational_suffixes,
                     partition_by_addresses_and_middle_names,
                     split_parent_children, **kwargs):
        """Verify that when dryrun=True, the dryrun function is called instead
        of the regular save function.
        """
        remerge.process_contact_ids([self.contact_pks[0]], self.user, dryrun=True)
        _dryrun_set_parent_children.assert_called_once_with(self.contacts[0])
        self.assertFalse(set_parent_children.called)
        _dryrun_set_parent_children.reset_mock()

        partition_by_generational_suffixes.side_effect = lambda x: [x]
        partition_by_addresses_and_middle_names.side_effect = lambda x, **_: [x]
        def _split_side_effect(group):
            group = sorted(group, key=attrgetter('id'))
            return group[0], group[1:]
        split_parent_children.side_effect = _split_side_effect
        with patch.object(remerge.Contact.objects, 'filter',
                          return_value=self.contacts):
            remerge.process_contact_ids(self.contact_pks, self.user, dryrun=True)

        _dryrun_set_parent_children.assert_called_once_with(self.contacts[0],
                                                            self.contacts[1:])
        self.assertFalse(set_parent_children.called)

    def test_shortcircuit_on_one_contact(self, set_parent_children,
                                         partition_by_generational_suffixes,
                                         split_parent_children, **kwargs):
        """Verify process_contact_ids short-circuits correctly when only a single
        contact is passed in
        """
        remerge.process_contact_ids([self.contact_pks[0]], self.user)
        set_parent_children.assert_called_once_with(self.contacts[0],
                                                    index_filters=None)
        self.assertFalse(partition_by_generational_suffixes.called)
        self.assertFalse(split_parent_children.called)


@patch.multiple(remerge, Contact=DEFAULT, move_prospects=DEFAULT,
                move_prospectcollisions=DEFAULT, move_contactnotes=DEFAULT,
                move_calltimecontacts=DEFAULT, ContactIndex=DEFAULT)
class SetParentChildrenTests(RemoveShortDescription, TestCase):
    CONTACT_UPDATE_OUTPUT_KEY = 'filter.return_value.update.return_value'

    @patch.object(remerge, '_parent_update_and_deconflict',
                  side_effect=lambda x: x)
    def test_only_update_children_needing_updates(self,
                                                  _parent_update_and_deconflict,
                                                  Contact, **kwargs):
        """Verify that only children that need to be updated are updated.

        Need to be updated being defined as: wrong parent, no parent, or
        is_primary is True.
        """
        parent = ContactFactory(contact_type='merged', parent=None,
                                is_primary=True)
        children = [
            ContactFactory(is_primary=False, parent=parent),
            ContactFactory(is_primary=True, parent=parent),
            ContactFactory(is_primary=False, parent=None),
            ContactFactory(is_primary=True, parent=None),
        ]
        mock_contact_mgr = Contact.objects
        mock_contact_mgr.configure_mock(**{self.CONTACT_UPDATE_OUTPUT_KEY: 3})
        remerge.set_parent_children(parent, children)

        # Verify only the children with incorrect values for the fields
        # is_primary and parent are updated.
        # Also make sure we're doing bulk updates
        self.assertEqual(
            mock_contact_mgr.mock_calls,
            (call.filter(ANY).update(is_primary=False, parent=parent,
                                     merge_version=ANY)).call_list())

    @patch.object(remerge, '_parent_update_and_deconflict',
                  side_effect=lambda x: x)
    def test_related_records_moved_to_parent(self,
                                             _parent_update_and_deconflict,
                                             move_prospects,
                                             move_prospectcollisions,
                                             move_contactnotes,
                                             move_calltimecontacts, **kwargs):
        """Verify that all the move_* functions are called when there are
        children, and that they are run in the correct order.
        """
        # Create a parent mock and attach the move_* functions as children so
        # we can verify the call order.
        mover_parent = MagicMock()
        mover_parent.attach_mock(move_prospects, 'move_prospects')
        mover_parent.attach_mock(move_prospectcollisions,
                                 'move_prospectcollisions')
        mover_parent.attach_mock(move_contactnotes, 'move_contactnotes')
        mover_parent.attach_mock(move_calltimecontacts,
                                 'move_calltimecontacts')
        parent = MagicMock(contact_type='merged', parent_id=None,
                           is_primary=True)
        children = MagicMock()
        remerge.set_parent_children(parent, children)
        # Make sure all the move_* functions are called, and that they are
        # called in the correct order.
        self.assertEqual(mover_parent.mock_calls,
                         [call.move_prospects(parent, children),
                          call.move_prospectcollisions(parent, children),
                          call.move_contactnotes(parent, children),
                          call.move_calltimecontacts(parent, children)])

    def test_reset_primary_and_parent_attr_on_parent(self, Contact, **kwargs):
        """Verify that `set_parent_children` works correctly even when given
        only a parent, and that it correctly updates the `parent` and
        `is_primary` fields
        """
        parent = ContactFactory(is_primary=False, parent=None)
        mock_contact_mgr = Contact.objects
        mock_contact_mgr.configure_mock(**{self.CONTACT_UPDATE_OUTPUT_KEY: 1})

        orig_merge_version = parent.merge_version
        new_merge_version = orig_merge_version + 1
        remerge.set_parent_children(parent)

        self.assertEqual(
            mock_contact_mgr.mock_calls,
            (call.filter(pk=parent.pk, merge_version=orig_merge_version).update(
                is_primary=True, parent_id=None,
                merge_version=new_merge_version)).call_list())

    def test_new_merged_contact(self, Contact, move_prospects,
                                move_prospectcollisions, move_contactnotes,
                                move_calltimecontacts, **kwargs):
        """Verify that if the parent passed to `set_parent_children` is not a
        merged contact, that a new merged contact is created based on it.

        Also verify that that new merged contact is the one used as a parent
        when updating the children, and when using the move_* functions, and
        that the contact used in the parent argument is treated as a child.
        """
        parent = ContactFactory()
        children = (
            ContactFactory(),
            ContactFactory(),
            ContactFactory(),
            ContactFactory(),
        )
        with patch.object(remerge.Contact, 'objects') as mock:
            mock.configure_mock(**{self.CONTACT_UPDATE_OUTPUT_KEY: 5})
            remerge.set_parent_children(parent, list(children))

        Contact.assert_called_once_with(
            user_id=parent.user_id,
            first_name=parent.first_name,
            last_name=parent.last_name,
            additional_name=parent.additional_name,
            first_name_lower=parent.first_name.lower(),
            last_name_lower=parent.last_name.lower(),
            name_prefix=parent.name_prefix,
            name_suffix=parent.name_suffix,
            contact_type="merged",
            is_primary=True)

        # Verify the contact originally passed to `set_parent_children` as the
        # parent is treated like a child, and the new merged contact created is
        # used as the parent.
        result_children = list(children + (parent,))
        move_prospects.assert_called_once_with(
            Contact(), result_children)
        move_prospectcollisions.assert_called_once_with(
            Contact(), result_children)
        move_contactnotes.assert_called_once_with(
            Contact(), result_children)
        move_calltimecontacts.assert_called_once_with(
            Contact(), result_children)

        assert mock.mock_calls == (
            call.filter(ANY).update(is_primary=False, parent=Contact(),
                                     merge_version=ANY)).call_list()

    def test_merge_conflict_unmerged_childless(self, Contact, ContactIndex, **kwargs):
        parent = ContactFactory(is_primary=False)
        mock_contact_mgr = Contact.objects
        mock_contact_mgr.configure_mock(**{self.CONTACT_UPDATE_OUTPUT_KEY: 1})

        # Control test
        # We verify in the control test that MergeConflict is not raised, and
        # that ContactIndex is updated.
        remerge.set_parent_children(parent)
        self.assertEqual(ContactIndex.update_index.call_count, 1)

        ContactIndex.reset_mock()
        parent.is_primary = False
        mock_contact_mgr.configure_mock(**{self.CONTACT_UPDATE_OUTPUT_KEY: 0})
        with self.assertRaises(MergeConflict):
            remerge.set_parent_children(parent)
        self.assertFalse(ContactIndex.update_index.called)

    def test_merge_conflict_unmerged_childless_no_changes(self, Contact,
                                                          ContactIndex,
                                                          **kwargs):
        parent = ContactFactory(is_primary=True, parent_id=None)
        mock_contact_mgr = Contact.objects
        mock_contact_mgr.configure_mock(**{self.CONTACT_UPDATE_OUTPUT_KEY: 1})

        # Control test
        # We verify in the control test that MergeConflict is not raised, and
        # that ContactIndex is updated.
        remerge.set_parent_children(parent)
        self.assertEqual(ContactIndex.update_index.call_count, 1)

        # TODO: broken
        ContactIndex.reset_mock()
        parent.is_primary = True
        parent.parent_id = None
        mock_contact_mgr.configure_mock(**{self.CONTACT_UPDATE_OUTPUT_KEY: 0})
        with self.assertRaises(MergeConflict):
            remerge.set_parent_children(parent)
        self.assertFalse(ContactIndex.update_index.called)

    def test_merge_conflict_merged_childless_no_manual(self, Contact,
                                                       ContactIndex, **kwargs):
        parent = ContactFactory(contact_type='merged')
        notes = ContactNotesFactory(contact=parent)
        mock_contact_mgr = Contact.objects
        mock_contact_mgr.configure_mock(
            **{self.CONTACT_UPDATE_OUTPUT_KEY: 1,
               'filter.return_value.exists.return_value': False})

        # Control test
        # We verify in the control test that MergeConflict is not raised, and
        # that ContactIndex is updated.
        remerge.set_parent_children(parent)
        self.assertTrue(ContactIndex.mock_calls)
        self.assertEqual(
            ContactIndex.mock_calls,
            (call.objects.filter(contact=parent).delete()).call_list())

        ContactIndex.reset_mock()
        mock_contact_mgr.configure_mock(**{self.CONTACT_UPDATE_OUTPUT_KEY: 0})
        with self.assertRaises(MergeConflict):
            remerge.set_parent_children(parent)
        self.assertFalse(ContactIndex.mock_calls)

    def test_merge_conflict_merged_childless_has_manual(self, Contact,
                                                        ContactIndex,
                                                        **kwargs):
        parent = ContactFactory(contact_type='merged')
        notes = ContactNotesFactory(contact=parent)
        mock_contact_mgr = Contact.objects
        mock_contact_mgr.configure_mock(
            **{self.CONTACT_UPDATE_OUTPUT_KEY: 1,
               'filter.return_value.values_list.return_value': [888]})

        # Control test
        # We verify in the control test that MergeConflict is not raised, and
        # that ContactIndex is updated.
        remerge.set_parent_children(parent)
        self.assertEqual(ContactIndex.update_index.call_count, 1)

        # TODO: currently broken
        ContactIndex.reset_mock()
        mock_contact_mgr.configure_mock(**{self.CONTACT_UPDATE_OUTPUT_KEY: 0})
        with self.assertRaises(MergeConflict):
            remerge.set_parent_children(parent)
        self.assertFalse(ContactIndex.update_index.called)

    def test_merge_conflict_unmerged_has_children(self, Contact, ContactIndex,
                                                  move_prospects,
                                                  move_prospectcollisions,
                                                  move_contactnotes,
                                                  move_calltimecontacts,
                                                  **kwargs):
        parent = ContactFactory()
        children = (
            ContactFactory(),
            ContactFactory(),
            ContactFactory(),
            ContactFactory(),
        )

        mock_contact_mgr = Contact.objects
        mock_contact_mgr.configure_mock(**{self.CONTACT_UPDATE_OUTPUT_KEY: 0})
        with self.assertRaises(MergeConflict):
            remerge.set_parent_children(parent, list(children))
        self.assertFalse(ContactIndex.mock_calls)
        self.assertFalse(move_prospects.called)
        self.assertFalse(move_prospectcollisions.called)
        self.assertFalse(move_contactnotes.called)
        self.assertFalse(move_calltimecontacts.called)


class SetParentChildrenNoPatchTests(TestCase):
    def test_merged_contact_parent_created(self, **kwargs):
        """Verify that if the parent passed to `set_parent_children` is not a
        merged contact, that a new merged contact is created based on it.

        Also verify that that new merged contact is the one used as a parent
        when updating the children, and when using the move_* functions, and
        that the contact used in the parent argument is treated as a child.
        """
        faker = Factory.create()
        now = datetime.datetime.now()
        past = now - datetime.timedelta(days=400)
        oldest = past - datetime.timedelta(days=50)
        fake_date = faker.date_time_between_dates

        parent = ContactFactory()
        children = (
            ContactFactory(created=fake_date(past, now)),
            ContactFactory(created=fake_date(past, now)),
            ContactFactory(created=oldest),
            ContactFactory(created=None),
        )
        remerge.set_parent_children(parent, list(children))

        contacts = tuple([parent]) + children
        for contact in contacts:
            contact.refresh_from_db()

        parent_parent = parent.parent
        assert parent_parent.created == oldest
        assert all((p == parent_parent
                    for p in (c.parent for c in contacts)))

    def test_release_analyzed_limt(self, **kwargs):
        """Verify the function releases the correct amount of rankings"""
        # Setup the test
        user = RevUpUserFactory()
        contacts = [ContactFactory(user=user) for i in range(5)]
        account1 = AccountFactory()
        account2 = AccountFactory()
        analysis1 = AnalysisFactory(account=account1)
        analysis2 = AnalysisFactory(account=account2)
        ResultFactory(analysis=analysis1, contact=contacts[0])
        ResultFactory(analysis=analysis1, contact=contacts[1])
        ResultFactory(analysis=analysis2, contact=contacts[1])
        # Verify results exist
        assert Result.objects.filter(contact__in=contacts).count() == 3

        # Verify there are no limits currently
        trackers = LimitTracker.get_trackers(user, account1, account2)
        for tracker in trackers:
            assert tracker.analyzed_count == 0
            tracker.analyzed_count = 50
            tracker.save()

        remerge._release_analyzed_limit(user, *contacts)
        tracker = LimitTracker.get_tracker(user, account1)
        assert tracker.analyzed_count == 48
        tracker = LimitTracker.get_tracker(user, account2)
        assert tracker.analyzed_count == 49
        # Verify no other trackers were created
        assert LimitTracker.objects.filter(user=user).count() == 2
        # Verfiy the results were cleaned up
        assert not Result.objects.filter(contact__in=contacts)


@patch.object(remerge, 'resolve_parent')
class SplitParentChildren(RemoveShortDescription, TestCase):
    def test_shortcircuit_on_single_contact(self, *args):
        """Verify that `split_parent_children` handles the case of receiving
        only a single contact properly.
        """
        parent = MagicMock(first_name_lower='123')
        self.assertEqual(
            remerge.split_parent_children([parent]),
            (parent, None))

    def test_skip_resolve_when_only_one_merged_contact(self, resolve_parent):
        """Verify that if only a single merged contact is found in the contacts
        passed in, that it is used as the parent and resolve_parent is never
        used.
        """
        contacts = [
            MagicMock(contact_type='merged', first_name_lower='123'),
            MagicMock(first_name_lower='123'),
            MagicMock(first_name_lower='123'),
            MagicMock(first_name_lower='123'),
        ]
        parent, children = remerge.split_parent_children(contacts)
        self.assertEqual(parent, contacts[0])
        self.assertCountEqual(children, contacts[1:])
        self.assertFalse(resolve_parent.called)

    def test_resolve_only_merged_if_present(self, resolve_parent):
        """Verify that if there are multiple merged contacts, that only they
        are sent to `resolve_parent`
        """
        contacts = (
            MagicMock(contact_type='merged', first_name_lower='123'),
            MagicMock(contact_type='merged', first_name_lower='123'),
            MagicMock(contact_type='merged', first_name_lower='123'),
            MagicMock(first_name_lower='123'),
            MagicMock(first_name_lower='123'),
            MagicMock(first_name_lower='123'),
        )
        resolve_parent.side_effect = lambda x: contacts[0]
        result = remerge.split_parent_children(contacts)
        self.assertEqual(result[0], contacts[0])
        self.assertCountEqual(result[1], contacts[1:])
        self.assertNotIn(result[0], result[1])
        resolve_parent.assert_called_once_with(set(contacts[:3]))

    def test_resolve_everything_when_no_merged(self, resolve_parent):
        """Verify that if there are multiple contacts, and none of them are
        merged contacts, that they are sent to `resolve_parent` function.
        """
        contacts = (
            MagicMock(first_name_lower='123'),
            MagicMock(first_name_lower='123'),
            MagicMock(first_name_lower='123'),
        )
        resolve_parent.side_effect = lambda x: contacts[0]
        result = remerge.split_parent_children(contacts)
        self.assertEqual(result[0], contacts[0])
        self.assertCountEqual(result[1], contacts[1:])
        self.assertNotIn(result[0], result[1])
        resolve_parent.assert_called_once_with(set(contacts))

    def test_children_contains_all_when_parent_is_none(self, resolve_parent):
        """Verify that when no parent is determined, all contacts are returned
        in the children list.
        """
        contacts = (
            MagicMock(first_name_lower='123'),
            MagicMock(first_name_lower='123'),
            MagicMock(first_name_lower='123'),
        )
        resolve_parent.return_value = None

        result = remerge.split_parent_children(contacts)
        self.assertIsNone(result[0])
        self.assertCountEqual(result[1], contacts)

        contacts = (
            MagicMock(contact_type='merged', first_name_lower='123'),
            MagicMock(contact_type='merged', first_name_lower='123'),
            MagicMock(contact_type='merged', first_name_lower='123'),
            MagicMock(first_name_lower='123'),
            MagicMock(first_name_lower='123'),
            MagicMock(first_name_lower='123'),
        )
        result = remerge.split_parent_children(contacts)
        self.assertIsNone(result[0])
        self.assertCountEqual(result[1], contacts)


class ContactPartitionerTests(RemoveShortDescription, TestCase):
    maxDiff = None

    def setUp(self):
        self.user = RevUpUserFactory()
        self.faker = Factory.create()

    def test_group_using_function_simple(self):
        """Relatively simple test for `group_using_function`
        """
        group1 = {
            MagicMock(data={'a', 'b'}),
            MagicMock(data={'e', 'f'}),
            MagicMock(data={'a', 'g'}),
            MagicMock(data={'e', 'g'}),
        }
        group2 = {
            MagicMock(data={'c', 'd'}),
            MagicMock(data={'z', 'c'}),
        }
        group3 = {
            MagicMock(data={'q', 'w'}),
            MagicMock(data={'q', 'r'}),
        }
        contacts = set().union(group1, group2, group3)
        groups = remerge.ContactPartitioner.group_using_function(
            contacts, lambda x: x.data, multi_collect=False)
        self.assertCountEqual([group1, group2, group3],
                              groups)

    def test_group_using_function_complex(self):
        """Complex merge case for group_using_function

        This is to test a complex case where old groups are linked through
        friend-of-a-friend-of-a-friend edges. We are verifying the grouping
        code correctly links all edges, and does not result in the wrong number
        of groups returned, or duplicate edges returned.

        Below is a diagram of the links. Each x is a node, the number next to
        the x is the order the node was created. The arrow indicates the
        direction of the relationship discovered.

        1 x < \
          ^    \
          |     \
        2 x      \- x 4
          ^          ^
          |           \__ x 13
        3 x              \
                         v
        5 x              x 12
          ^              |
          |     10       |
        6 x <-- x <--\   v
                     |-- x
        7 x <-- x <--/  11
          ^     9
          |
        8 x

        As you can see from the diagram, node 3 and node 5 are extremely far
        away from each other. This test usually reveals bugs in the grouping
        code.
        """
        contacts = [
            # group 1
            MagicMock(data={1}),
            MagicMock(data={1, 2}),
            MagicMock(data={2, 3}),
            MagicMock(data={1, 4}),

            # group 2
            MagicMock(data={5}),
            MagicMock(data={5, 6}),
            MagicMock(data={6, 10}),

            # group 3
            MagicMock(data={7}),
            MagicMock(data={7, 8}),
            MagicMock(data={7, 9}),

            # group 4
            MagicMock(data={11, 12}),

            # link group 2, 3 and 4 together
            MagicMock(data={9, 10, 11}),

            # link group 1 with group 4, and through group 4 groups 2 and 3.
            MagicMock(data={4, 12, 13}),
        ]
        groups = remerge.ContactPartitioner.group_using_function(
            contacts, lambda x: x.data, multi_collect=False)

        self.assertEqual(len(groups), 1)
        self.assertCountEqual(contacts, next(iter(groups)))

    def test_get_contact_phone_numbers(self):
        """Verify the _get_contact_phone_numbers performs deduping, filtering
        and formatting as expected.
        """
        contact = ContactFactory(phone_numbers=None)
        phone_number_records = [
            PhoneNumberFactory(contact=contact),
            PhoneNumberFactory(contact=contact),
            PhoneNumberFactory(contact=contact),
            PhoneNumberFactory(contact=contact),
        ]
        # Fixture to test that duplicate phone numbers are filtered out
        PhoneNumberFactory(contact=contact,
                           number=phone_number_records[0].rfc3966.upper())
        # Fixture to test that empty phone numbers are filtered out
        PhoneNumberFactory(contact=contact, number='')
        result = remerge.ContactPartitioner._get_contact_phone_numbers(contact)
        self.assertEqual(len(result), len(phone_number_records))
        expected = [ph.national for ph in phone_number_records]
        self.assertCountEqual(expected, result)

    def test_group_by_phone_numbers(self):
        """Test the function ContactPartitioner.group_by_phone_numbers

        Verify:
            - Contacts with the same phone number are grouped together
            - Contacts with no phone number are not returned by the function
        """
        phone1 = '2222222222'
        phone2 = '9999999999'
        phone1_contacts = {
            ContactFactory(user=self.user, phone_numbers__number=phone1)
            for _ in range(4)
        }
        phone2_contacts = {
            ContactFactory(user=self.user, phone_numbers__number=phone2)
            for _ in range(4)
        }
        # Create a contact with no phone number to verify it is not returned by
        # the function.
        no_phone_contact = ContactFactory(user=self.user, phone_numbers=None)
        contacts = set().union(phone1_contacts, phone2_contacts,
                               [no_phone_contact])
        result = remerge.ContactPartitioner.group_by_phone_numbers(contacts)
        self.assertEqual(len(result), 2)
        self.assertCountEqual([phone1_contacts, phone2_contacts],
                              result)

        # Add phone1 as a second phone number to one of the phone2 contacts to
        # verify the groups are combined
        phone2_contact = next(iter(phone2_contacts))
        PhoneNumberFactory(contact=phone2_contact, number=phone1)
        result = remerge.ContactPartitioner.group_by_phone_numbers(contacts)
        self.assertEqual(len(result), 1)
        self.assertCountEqual([contacts.difference({no_phone_contact})],
                              result)

    def test_get_contact_addresses(self):
        """Verify the _get_contact_addresses performs deduping, filtering
        and formatting as expected.
        """
        contact = ContactFactory(addresses=None)
        address_records = [
            AddressFactory(contact=contact),
            AddressFactory(contact=contact),
            AddressFactory(contact=contact),
            AddressFactory(contact=contact),
        ]
        # Fixture to verify that dupes are not returned, even dupes with
        # different casing.
        AddressFactory(contact=contact,
                       street=address_records[0].street.upper(),
                       post_code=address_records[0].post_code)

        result = remerge.ContactPartitioner._get_contact_addresses(contact)
        self.assertEqual(len(result), len(address_records))
        expected = [(" ".join(strip_punctuation(a.street.lower()).split()),
                     a.city,
                     a.region,
                     a.post_code)
                    for a in address_records]
        self.assertCountEqual(expected, result)

    def test_group_by_addresses(self):
        """Test the function ContactPartitioner.group_by_addresses

        Verify:
            - Contacts with the same address are grouped together
            - Contacts with no address are not returned by the function
        """
        street1 = self.faker.street_address()
        post_code1 = self.faker.postalcode()
        street2 = self.faker.street_address()
        post_code2 = self.faker.postalcode()
        address1_contacts = {
            ContactFactory(user=self.user,
                           addresses__street=street1,
                           addresses__post_code=post_code1)
            for _ in range(4)
        }
        address2_contacts = {
            ContactFactory(user=self.user,
                           addresses__street=street2,
                           addresses__post_code=post_code2)
            for _ in range(4)
        }
        # Create a contact with no address to verify it is not returned by
        # the function.
        no_address_contact = ContactFactory(user=self.user, addresses=None)
        contacts = set().union(address1_contacts, address2_contacts,
                               {no_address_contact})
        result = remerge.ContactPartitioner.group_by_addresses(contacts)
        self.assertEqual(len(result), 2)
        self.assertCountEqual([address1_contacts, address2_contacts],
                              result)

        # Add address1 as a second address to one of the address2 contacts to
        # verify the groups are combined
        address2_contact = next(iter(address2_contacts))
        AddressFactory(contact=address2_contact, street=street1,
                       post_code=post_code1)
        result = remerge.ContactPartitioner.group_by_addresses(contacts)
        self.assertEqual(len(result), 1)
        self.assertCountEqual([contacts.difference({no_address_contact})],
                              result)

    def test_get_contact_email(self):
        """Verify the _get_contact_emails performs deduping, filtering
        and formatting as expected.
        """
        contact = ContactFactory(email_addresses=[])
        # Fixture to test that email addresses are lower-cased
        EmailAddressFactory(contact=contact, address='ABC@QWE.ZXC')
        # Fixture to test that dupes are removed, even differently cased dupes
        EmailAddressFactory(contact=contact, address='Abc@Qwe.Zxc')
        # Fixture to test that blank email addresses are not included
        EmailAddressFactory(contact=contact, address='')
        EmailAddressFactory(contact=contact, address='something@revup.com')

        result = remerge.ContactPartitioner._get_contact_emails(contact)
        self.assertEqual(len(result), 2)
        self.assertIn('abc@qwe.zxc', result)
        self.assertIn('something@revup.com', result)
        self.assertNotIn('ABC@QWE.ZXC', result)
        self.assertNotIn('Abc@Qwe.Zxc', result)

    def test_group_by_email(self):
        """Test the function ContactPartitioner.group_by_email

        Verify:
            - Contacts with the same email address are grouped together
            - Contacts with no email address are not returned by the function
        """
        email1 = self.faker.email()
        email2 = self.faker.email()
        email1_contacts = {
            ContactFactory(user=self.user, email_addresses__address=email1)
            for _ in range(4)
        }
        email2_contacts = {
            ContactFactory(user=self.user, email_addresses__address=email2)
            for _ in range(4)
        }
        # Create a contact with no address to verify it is not returned by
        # the function.
        no_email_contact = ContactFactory(user=self.user,
                                          email_addresses=None)
        contacts = set().union(email1_contacts, email2_contacts,
                               {no_email_contact})
        result = remerge.ContactPartitioner.group_by_email(contacts)
        self.assertEqual(len(result), 2)
        self.assertCountEqual([email1_contacts, email2_contacts],
                              result)

        # Add email1 as a second email address to one of the email2 contacts to
        # verify the groups are combined
        email2_contact = next(iter(email2_contacts))
        EmailAddressFactory(contact=email2_contact, address=email1)
        result = remerge.ContactPartitioner.group_by_email(contacts)
        self.assertEqual(len(result), 1)
        self.assertCountEqual([contacts.difference([no_email_contact])],
                              result)

    def test_partition_by_middle_names_no_initials(self):
        """Test the middle name partitioner with keep_initials=False

        Verify:
            - contacts with the same middle name are grouped together even if
              the names have different punctuation or capitalization.
            - contacts with no middle name are not returned if keep_initials is
              False
            - contacts with only a middle initial are not returned if
              keep_initals is False.
        """
        chad_contacts = {
            ContactFactory(user=self.user, additional_name='Chad'),
            ContactFactory(user=self.user, additional_name='CHAD-..'),
            ContactFactory(user=self.user, additional_name='chad!'),
            ContactFactory(user=self.user, additional_name='cHAd?'),
        }
        bob_contacts = {
            ContactFactory(user=self.user, additional_name='Bob+'),
            ContactFactory(user=self.user, additional_name='BOB'),
            ContactFactory(user=self.user, additional_name='bob'),
            ContactFactory(user=self.user, additional_name='bOB'),
        }
        rick_contacts = {
            ContactFactory(user=self.user, additional_name='Rick'),
            ContactFactory(user=self.user, additional_name='RICK&'),
            ContactFactory(user=self.user, additional_name='rick'),
            ContactFactory(user=self.user, additional_name='rI^CK'),
        }
        # Create contact fixtures with middle initials or no middle name at all
        # to verify they are not included in the result.
        contacts = {
            ContactFactory(user=self.user, additional_name='r.'),
            ContactFactory(user=self.user, additional_name='r'),
            ContactFactory(user=self.user, additional_name='b'),
            ContactFactory(user=self.user, additional_name=''),
        }
        contacts.update(rick_contacts, chad_contacts, bob_contacts)
        result = remerge.ContactPartitioner.partition_by_middle_names(contacts)
        self.assertCountEqual([rick_contacts, chad_contacts, bob_contacts],
                              result)

    def test_partition_by_middle_names_with_initials(self):
        """Test the middle name partitioner with keep_initials=True

        Verify that when keep_initials is True, every unique middle initial is
        a separate group, and contacts with no middle name are in their own
        group as well.
        """
        chad_contacts = {
            ContactFactory(user=self.user, additional_name='Chad'),
            ContactFactory(user=self.user, additional_name='CHAD'),
            ContactFactory(user=self.user, additional_name='chad'),
            ContactFactory(user=self.user, additional_name='cHAd'),
        }
        bob_contacts = {
            ContactFactory(user=self.user, additional_name='Bob'),
            ContactFactory(user=self.user, additional_name='BOB'),
            ContactFactory(user=self.user, additional_name='bob'),
            ContactFactory(user=self.user, additional_name='bOB'),
        }
        rick_contacts = {
            ContactFactory(user=self.user, additional_name='Rick'),
            ContactFactory(user=self.user, additional_name='RICK'),
            ContactFactory(user=self.user, additional_name='rick'),
            ContactFactory(user=self.user, additional_name='rICK'),
        }
        r_contacts = {
            ContactFactory(user=self.user, additional_name='r.'),
            ContactFactory(user=self.user, additional_name='r'),
        }
        b_contacts = {
            ContactFactory(user=self.user, additional_name='b'),
        }
        blank_contacts = {
            ContactFactory(user=self.user, additional_name=''),
        }
        contacts = set().union(rick_contacts, chad_contacts, bob_contacts,
                               r_contacts, b_contacts, blank_contacts)
        result = remerge.ContactPartitioner.partition_by_middle_names(
            contacts, keep_initials=True)
        self.assertCountEqual([rick_contacts, chad_contacts, bob_contacts,
                               r_contacts, b_contacts, blank_contacts],
                              result)

    def test_partition_by_middle_names_merge_single_initials_with_blank(self):
        """Verify that when keep_initials is True and the middle name
        partitioned is only given one unique middle initial and a contact with
        no middle name, those contacts are merged together into a single group.
        """
        contacts = {
            ContactFactory(user=self.user, additional_name='r.'),
            ContactFactory(user=self.user, additional_name='r'),
            ContactFactory(user=self.user, additional_name=''),
        }
        result = remerge.ContactPartitioner.partition_by_middle_names(
            contacts, keep_initials=True)
        self.assertCountEqual([contacts], result)


class GenerationalPartitionerTests(RemoveShortDescription, TestCase):

    def test_same_suffix_same_group(self):
        """
        Verify that we can group contacts no matter what the suffix
        looks like.

        Additionally verify that contacts with no suffix are lumped in with the
        generational group if there is only one generational group.
        """
        contacts = set([
            MagicMock(name_suffix="kjqwe sr qwe"),
            MagicMock(name_suffix="SR"),
            MagicMock(name_suffix=""),
            MagicMock(name_suffix="sr"),
            MagicMock(name_suffix="22 sr."),
        ])
        result = remerge.ContactPartitioner.partition_by_generational_suffixes(
            contacts)
        self.assertEqual(result, [set(contacts)])

    def test_group_per_suffix_generation(self):
        """
        Verify one group per suffix generation, and that a group will include
        all members no matter the order of the input contacts.

        Additionally verify that contacts with no suffix are split into their
        own group if there is more than one generational group found.
        """
        no_suffix_contact = MagicMock(name_suffix="")
        jr_contact = MagicMock(name_suffix="jr")
        iii_contact = MagicMock(name_suffix="iii")
        iv_contact = MagicMock(name_suffix="iv")
        contacts = [
            MagicMock(name_suffix="kjqwe sr qwe"),
            jr_contact,
            MagicMock(name_suffix="SR"),
            iii_contact,
            no_suffix_contact,
            MagicMock(name_suffix="sr"),
            iv_contact,
            MagicMock(name_suffix="22 sr."),
        ]
        sr_contacts = set(contacts).difference(
            [jr_contact, iii_contact, iv_contact, no_suffix_contact])
        result = remerge.ContactPartitioner.partition_by_generational_suffixes(
            contacts)
        # Verify that every single contact passed in as input is found in the
        # output.
        self.assertCountEqual(set().union(*result), contacts)
        # Verify result contains all groups, and that each group contains the
        # correct contents.
        self.assertCountEqual(result, [set([jr_contact]), set([iii_contact]),
                                       set([iv_contact]), sr_contacts,
                                       set([no_suffix_contact])])


class MiddleNameAndAddressPartitionerTests(RemoveShortDescription, TestCase):

    def setUp(self):
        self.user = RevUpUserFactory()
        self.faker = Factory.create()

    def test_no_phone_number_partitions(self):
        """Verify that partitions are not created based on phone number groups.
        """
        phone1 = '2222222222'
        phone2 = '9999999999'
        phone1_contacts = {ContactFactory(user=self.user,
                                          addresses=None,
                                          phone_numbers__number=phone1)
                           for _ in range(4)}
        phone2_contacts = {ContactFactory(user=self.user,
                                          addresses=None,
                                          phone_numbers__number=phone2)
                           for _ in range(4)}
        contacts = set().union(phone1_contacts, phone2_contacts)
        with patch.object(remerge.ContactPartitioner, 'group_by_phone_numbers',
                          return_value=list(map(frozenset, [phone1_contacts,
                                                       phone2_contacts]))):
            result = remerge.ContactPartitioner\
                .partition_by_addresses_and_middle_names(contacts)
        self.assertEqual(len(result), 1)
        self.assertCountEqual([contacts], result)

    def test_no_email_partitions(self):
        """Verify that partitions are not created based on email groups.
        """
        email1 = self.faker.email()
        email2 = self.faker.email()
        email1_contacts = {ContactFactory(user=self.user,
                                          addresses=None,
                                          email_addresses__address=email1)
                           for _ in range(4)}
        email2_contacts = {ContactFactory(user=self.user,
                                          addresses=None,
                                          email_addresses__address=email2)
                           for _ in range(4)}
        contacts = set().union(email1_contacts, email2_contacts)
        with patch.object(remerge.ContactPartitioner, 'group_by_email',
                          return_value=list(map(frozenset, [email1_contacts,
                                                       email2_contacts]))):
            result = remerge.ContactPartitioner\
                .partition_by_addresses_and_middle_names(contacts)
        self.assertEqual(len(result), 1)
        self.assertCountEqual([contacts], result)

    def test_partitions_by_address(self):
        """Verify contacts are partitioned by address
        """
        street1 = self.faker.street_address()
        post_code1 = self.faker.postalcode()
        street2 = self.faker.street_address()
        post_code2 = self.faker.postalcode()
        address1_contacts = {ContactFactory(user=self.user,
                                            addresses__street=street1,
                                            addresses__post_code=post_code1)
                             for _ in range(4)}
        address2_contacts = {ContactFactory(user=self.user,
                                            addresses__street=street2,
                                            addresses__post_code=post_code2)
                             for _ in range(4)}
        contacts = set().union(address1_contacts, address2_contacts)
        # Control test
        result = remerge.ContactPartitioner.group_by_addresses(contacts)
        self.assertEqual(len(result), 2)

        result = remerge.ContactPartitioner\
            .partition_by_addresses_and_middle_names(contacts)
        self.assertEqual(len(result), 2)
        self.assertCountEqual([address1_contacts, address2_contacts],
                              result)

    def test_partitions_by_middle_name(self):
        """Verify partitions are created for each middle name
        """
        name1 = 'chad'
        name2 = 'rick'
        name1_contacts = {ContactFactory(user=self.user,
                                         additional_name=name1,
                                         addresses=None) for _ in range(4)}
        name2_contacts = {ContactFactory(user=self.user,
                                         additional_name=name2,
                                         addresses=None) for _ in range(4)}
        contacts = set().union(name1_contacts, name2_contacts)

        # Control test
        result = remerge.ContactPartitioner.partition_by_middle_names(contacts)
        self.assertEqual(len(result), 2)

        result = remerge.ContactPartitioner\
            .partition_by_addresses_and_middle_names(contacts)
        self.assertEqual(len(result), 2)
        self.assertCountEqual([name1_contacts, name2_contacts],
                              result)

    def test_partition_by_address_and_middle_name(self):
        """Verify partitions are created for each middle name and each address
        if there is nothing to link them back together.
        """
        name1 = 'chad'
        name2 = 'rick'
        name1_contacts = {ContactFactory(user=self.user,
                                         additional_name=name1,
                                         addresses=None) for _ in range(4)}
        name2_contacts = {ContactFactory(user=self.user,
                                         additional_name=name2,
                                         addresses=None) for _ in range(4)}
        street1 = self.faker.street_address()
        post_code1 = self.faker.postalcode()
        street2 = self.faker.street_address()
        post_code2 = self.faker.postalcode()
        address1_contacts = {ContactFactory(user=self.user,
                                            addresses__street=street1,
                                            addresses__post_code=post_code1)
                             for _ in range(4)}
        address2_contacts = {ContactFactory(user=self.user,
                                            addresses__street=street2,
                                            addresses__post_code=post_code2)
                             for _ in range(4)}

        contacts = set().union(name1_contacts, name2_contacts,
                               address1_contacts, address2_contacts)

        result = remerge.ContactPartitioner\
            .partition_by_addresses_and_middle_names(contacts)
        self.assertEqual(len(result), 4)
        self.assertCountEqual([name1_contacts, name2_contacts,
                               address1_contacts, address2_contacts],
                              result)

    def test_middle_initial_partitioning(self):
        """Test middle initial partitioning edge cases

        Tests the following edge cases:
        1. Contacts with the middle name of rick.
           contacts with the middle initial of r
           contacts with no middle name

           All contacts merged

        2. Contacts with the middle name of rick.
           contacts with the middle initial of b
           contacts with no middle name

           2 groups returned: "rick" group, and group of {middle initial b, no
           middle name}

        3. Contacts with the middle name of rick.
           contacts with the middle initial of r
           contacts with the middle initial of b
           contacts with no middle name

           3 groups returned: one for the rick group + middle initial r
           contacts, one for middle initial b, and one for contacts with no
           middle name.
        """
        rick_contacts = {ContactFactory(user=self.user,
                                        addresses=None,
                                        additional_name='Rick'),
                         ContactFactory(user=self.user,
                                        addresses=None,
                                        additional_name='RICK'),
                         ContactFactory(user=self.user,
                                        addresses=None,
                                        additional_name='rick'),
                         ContactFactory(user=self.user,
                                        addresses=None,
                                        additional_name='rICK')}
        r_contacts = {ContactFactory(user=self.user,
                                     addresses=None,
                                     additional_name='r.'),
                      ContactFactory(user=self.user,
                                     addresses=None,
                                     additional_name='r')}
        b_contacts = {ContactFactory(user=self.user,
                                     addresses=None,
                                     additional_name='b')}
        blank_contacts = {ContactFactory(user=self.user,
                                         addresses=None,
                                         additional_name='')}

        # Edge case 1.
        contacts = set().union(rick_contacts, r_contacts, blank_contacts)
        result = remerge.ContactPartitioner\
            .partition_by_addresses_and_middle_names(contacts)
        self.assertEqual(len(result), 1)
        self.assertCountEqual([contacts], result)

        # Edge case 3.
        contacts.update(b_contacts)
        result = remerge.ContactPartitioner\
            .partition_by_addresses_and_middle_names(contacts)
        self.assertEqual(len(result), 3)
        self.assertCountEqual([rick_contacts.union(r_contacts), blank_contacts,
                               b_contacts], result)

        # Edge case 2.
        contacts.difference_update(r_contacts)
        result = remerge.ContactPartitioner\
            .partition_by_addresses_and_middle_names(contacts)
        self.assertEqual(len(result), 2)
        self.assertCountEqual([rick_contacts,
                               blank_contacts.union(b_contacts)],
                              result)

    def test_middle_name_merges_addresses(self):
        """Verify that a common middle name will merge address partitions
        """
        street1 = self.faker.street_address()
        post_code1 = self.faker.postalcode()
        street2 = self.faker.street_address()
        post_code2 = self.faker.postalcode()
        name1 = 'Jo-bob'
        address1_contacts = {ContactFactory(user=self.user,
                                            additional_name=name1,
                                            addresses__street=street1,
                                            addresses__post_code=post_code1)
                             for _ in range(4)}
        address2_contacts = {ContactFactory(user=self.user,
                                            additional_name=name1,
                                            addresses__street=street2,
                                            addresses__post_code=post_code2)
                             for _ in range(4)}
        contacts = set().union(address1_contacts, address2_contacts)

        # Control test
        result = remerge.ContactPartitioner.group_by_addresses(contacts)
        self.assertEqual(len(result), 2)

        # Test that the contacts are all merged into the same group
        result = remerge.ContactPartitioner\
            .partition_by_addresses_and_middle_names(contacts)
        self.assertEqual(len(result), 1)
        self.assertCountEqual([contacts], result)

    def test_phone_number_merges_addresses(self):
        """Verify that a common phone number will merge address partitions
        """
        street1 = self.faker.street_address()
        post_code1 = self.faker.postalcode()
        street2 = self.faker.street_address()
        post_code2 = self.faker.postalcode()
        phone1 = '2222222222'
        address1_contacts = {ContactFactory(user=self.user,
                                            phone_numbers__number=phone1,
                                            addresses__street=street1,
                                            addresses__post_code=post_code1)
                             for _ in range(4)}
        address2_contacts = {ContactFactory(user=self.user,
                                            phone_numbers__number=phone1,
                                            addresses__street=street2,
                                            addresses__post_code=post_code2)
                             for _ in range(4)}
        contacts = set().union(address1_contacts, address2_contacts)

        # Control test
        result = remerge.ContactPartitioner.group_by_addresses(contacts)
        self.assertEqual(len(result), 2)

        # Test that the contacts are all merged into the same group
        result = remerge.ContactPartitioner\
            .partition_by_addresses_and_middle_names(contacts)
        self.assertEqual(len(result), 1)
        self.assertCountEqual([contacts], result)

    def test_email_merges_addresses(self):
        """Verify that a common email address will merge address partitions
        """
        street1 = self.faker.street_address()
        post_code1 = self.faker.postalcode()
        street2 = self.faker.street_address()
        post_code2 = self.faker.postalcode()
        email1 = self.faker.email()
        address1_contacts = {ContactFactory(user=self.user,
                                            email_addresses__address=email1,
                                            addresses__street=street1,
                                            addresses__post_code=post_code1)
                             for _ in range(4)}
        address2_contacts = {ContactFactory(user=self.user,
                                            email_addresses__address=email1,
                                            addresses__street=street2,
                                            addresses__post_code=post_code2)
                             for _ in range(4)}
        contacts = set().union(address1_contacts, address2_contacts)

        # Control test
        result = remerge.ContactPartitioner.group_by_addresses(contacts)
        self.assertEqual(len(result), 2)

        # Test that the contacts are all merged into the same group
        result = remerge.ContactPartitioner\
            .partition_by_addresses_and_middle_names(contacts)
        self.assertEqual(len(result), 1)
        self.assertCountEqual([contacts], result)

    def test_ignore_single_address(self):
        """Verify that if only a single address is found, that we do not create
        address partitions.

        This is actually a regression test for a weird set of contacts I had.
        """
        address_contacts = {ContactFactory(user=self.user)}
        middle_name_contacts = {ContactFactory(user=self.user,
                                               additional_name='Carl',
                                               addresses=None)}
        contacts = {ContactFactory(user=self.user, addresses=None)}
        contacts.update(address_contacts, middle_name_contacts)

        # Address control test
        result = remerge.ContactPartitioner.group_by_addresses(contacts)
        self.assertEqual(len(result), 1)
        self.assertCountEqual([address_contacts], result)

        # Middle name control test
        result = remerge.ContactPartitioner.partition_by_middle_names(contacts)
        self.assertEqual(len(result), 1)
        self.assertCountEqual([middle_name_contacts], result)

        # Test that the contacts are all merged into the same group
        result = remerge.ContactPartitioner\
            .partition_by_addresses_and_middle_names(contacts)
        self.assertEqual(len(result), 1)
        self.assertCountEqual([contacts], result)

    def test_merge_similar_addresses(self):
        """Verify a series of similar addresses that do have a clear way to
          merge are merged together.
        """
        street = self.faker.street_address()
        post_code1 = self.faker.postalcode()
        post_code2 = self.faker.postalcode()
        state1 = "CA"
        city1 = self.faker.city()
        city2 = self.faker.city()
        city3 = self.faker.city()
        contacts = [ContactFactory.create(user=self.user, addresses=[])
                    for i in range(7)]

        address_dicts = [
            dict(street="", city=city1, region=state1, post_code=post_code1),
            dict(street=street, city=city1, region=state1, post_code=""),
            dict(street=street, post_code=post_code1),

            dict(street="", city=city2, region=state1, post_code=post_code2),
            dict(street="", city=city2, post_code=""),
            dict(street="", post_code=post_code2),

            dict(street="", city=city3, post_code=""),
        ]
        addresses = [AddressFactory.create(contact=contacts[i], **address_dict)
                     for i, address_dict in enumerate(address_dicts)]

        result = remerge.ContactPartitioner.group_by_addresses(contacts)
        self.assertEqual(len(result), 3)
        self.assertCountEqual(
            [set(contacts[0:3]), set(contacts[3:6]), {contacts[6]}],
            result)

    def test_compare_special_cases(self):
        """Verify certain special cases where an address would typical show
           as invalid, but it matches well enough to be considered a match.
        """
        street1 = self.faker.street_address()
        street2 = self.faker.street_address()
        post_code1 = self.faker.postalcode()
        state1 = "CA"
        city1 = self.faker.city()
        city2 = self.faker.city()
        contacts = [ContactFactory.create(user=self.user, addresses=[])
                    for i in range(3)]

        address_dicts = [
            dict(street=street1, city=city1, region=state1, post_code=post_code1),
            dict(street=street1, city=city2, region=state1, post_code=post_code1),
            dict(street=street1, post_code=post_code1),
        ]
        addresses = [AddressFactory.create(contact=contacts[i], **address_dict)
                     for i, address_dict in enumerate(address_dicts)]
        result = remerge.ContactPartitioner.group_by_addresses(contacts)
        self.assertEqual(len(result), 1)

        # Test a contradiction
        addresses[2].street = street2
        addresses[2].save()
        result = remerge.ContactPartitioner.group_by_addresses(contacts)
        self.assertEqual(len(result), 2)
        self.assertCountEqual(
            [{contacts[0], contacts[1]}, {contacts[2]}],
            result)

    def test_split_similar_addresses(self):
        """Verify a series of similar addresses that have no clear way to
           merge are split apart.

        This is basically the 5 addresses on the whiteboard made into a
        test to verify they are split up as we expect
        """
        post_code1 = self.faker.postalcode()
        post_code2 = self.faker.postalcode()
        state1 = "CA"
        city1 = self.faker.city()
        city2 = self.faker.city()
        contacts = [ContactFactory.create(user=self.user, addresses=[])
                    for i in range(5)]

        address_dicts = [
            dict(street="", city=city1, region=state1, post_code=post_code1),
            dict(street="", city=city1, region=state1, post_code=""),
            dict(street="", region=state1, post_code=post_code1),
            dict(street="", city=city1, region=state1, post_code=post_code2),
            dict(street="", city=city2, region=state1, post_code=post_code1),
        ]
        addresses = [AddressFactory.create(contact=contacts[i], **address_dict)
                     for i, address_dict in enumerate(address_dicts)]

        result = remerge.ContactPartitioner.group_by_addresses(contacts)
        self.assertEqual(len(result), 5)
        self.assertCountEqual([{contact} for contact in contacts], result)

        # Second part to this test, verify contacts with multiple addresses
        # are not returned multiple times
        addresses[2].contact = contacts[0]
        addresses[2].save()
        self.assertEqual(contacts[0].addresses.count(), 2)
        result = remerge.ContactPartitioner.group_by_addresses(contacts)
        self.assertEqual(len(result), 4)
        self.assertCountEqual([{contact}
                               for contact in itertools.chain(contacts[0:2],
                                                              contacts[3:5])],
                              result)

    def test_should_pair(self):
        AddressTuple = remerge.AddressTuple
        should_pair = remerge.ContactPartitioner._should_pair
        street1 = self.faker.street_address()
        street2 = self.faker.street_address()
        post_code1 = self.faker.postalcode()
        post_code2 = self.faker.postalcode()
        state1 = "CA"
        city1 = self.faker.city()
        city2 = self.faker.city()

        a1 = AddressTuple(street1, city1, state1, post_code1)
        a2 = AddressTuple(street2, city1, state1, post_code1)
        a3 = AddressTuple(street2, "", "", post_code1)
        a4 = AddressTuple(street2, city1, state1, post_code2)
        a5 = AddressTuple(street1 + "#1", city1, state1, post_code2)
        mid = int(float(len(street1))/2)
        a6 = AddressTuple(street1[:mid] + "some address junk" + street1[mid:],
                          "", state1, "")
        a7 = AddressTuple(street2, city2, state1, post_code2)
        a8 = AddressTuple(street2, city2, state1, post_code1)

        # Verify some simple positive cases
        sp, weight = should_pair(a1, a1)
        self.assertTrue(sp)
        self.assertEqual(weight, 13)
        sp, weight = should_pair(a2, a3)
        self.assertTrue(sp)
        self.assertEqual(weight, 10)

        # Verify simple negative cases
        sp, weight = should_pair(a1, a2)
        self.assertFalse(sp)
        self.assertIsNotNone(sp)
        self.assertEqual(weight, 1)
        sp, weight = should_pair(a2, a7)
        self.assertFalse(sp)
        self.assertIsNotNone(sp)
        self.assertEqual(weight, 1)

        # Verify edit distance
        # This address should be close enough
        self.assertTrue(should_pair(a1, a5)[0])
        # This should be too distant
        self.assertFalse(should_pair(a1, a6)[0])

        # Verify the special case
        sp, weight = should_pair(a2, a4)
        self.assertTrue(sp)
        self.assertEqual(weight, 5)
        sp, weight = should_pair(a2, a8)
        self.assertTrue(sp)
        self.assertEqual(weight, 9)

        # Verify Neutral case
        sp, weight = should_pair(a3, AddressTuple("", city1, state1, ""))
        self.assertIsNone(sp)
        self.assertEqual(weight, 0)

    def test_deep_linking(self):
        """Complex merge case for _merge_groups_with_common_contacts

        This is to test a complex case where old groups are linked through
        friend-of-a-friend-of-a-friend edges. We are verifying the grouping
        code correctly links all edges, and does not result in the wrong number
        of groups returned, or duplicate edges returned.

        Below is a diagram of the links. Each x is a node, the number next to
        the x is the order the node was created. The arrow indicates the
        direction of the relationship discovered.

        1 x < \
          ^    \
          |     \
        2 x      \- x 4
          ^          ^
          |           \__ x 13
        3 x              \
                         v
        5 x              x 12
          ^              |
          |     10       |
        6 x <-- x <--\   v
                     |-- x
        7 x <-- x <--/  11
          ^     9
          |
        8 x

        As you can see from the diagram, node 3 and node 5 are extremely far
        away from each other. This test usually reveals bugs in the grouping
        code. Like it did this time.
        """
        groups = [
            # group 1
            {1},
            {1, 2},
            {2, 3},
            {1, 4},

            # group 2
            {5},
            {5, 6},
            {6, 10},

            # group 3
            {7},
            {7, 8},
            {7, 9},

            # group 4
            {11, 12},

            # link group 2, 3 and 4 together
            {9, 10, 11},

            # link group 1 with group 4, and through group 4 groups 2 and 3.
            {4, 12, 13},
        ]
        all_numbers = set().union(*groups)
        result, _ = union_find_reducer(groups)
        self.assertCountEqual([all_numbers], result)

    def test_force_merging(self):
        """Verify force merge overrides contact partitioning rules
        """
        street1 = self.faker.street_address()
        post_code1 = self.faker.postalcode()
        street2 = self.faker.street_address()
        post_code2 = self.faker.postalcode()
        address1_contact = ContactFactory(user=self.user,
                                          addresses__street=street1,
                                          addresses__post_code=post_code1)
        address2_contact = ContactFactory(user=self.user,
                                          addresses__street=street2,
                                          addresses__post_code=post_code2)
        contacts = [address1_contact, address2_contact]

        # Control test
        result = remerge.ContactPartitioner.process_contacts(contacts)
        result = list(result)
        self.assertEqual(len(result), 2)

        # Create ContactLink objects to force the merge
        dimension = PersonDimension.dimensions.create(
            dimension_type=PersonDimension.PersonDimensionTypes.FORCE_MERGE,
            label="force merge test")
        ContactLink.objects.bulk_create([
            ContactLink(dimension=dimension, contact=address1_contact),
            ContactLink(dimension=dimension, contact=address2_contact)])

        result = list(remerge.ContactPartitioner.process_contacts(contacts))
        self.assertEqual(len(result), 1)

    def test_regression_merged_parent_thrashing(self):
        street1 = self.faker.street_address()
        post_code1 = self.faker.postalcode()
        street2 = self.faker.street_address()
        post_code2 = self.faker.postalcode()

        address1_parent = ContactFactory(user=self.user,
                                         addresses=None,
                                         email_addresses=None,
                                         phone_numbers=None,
                                         contact_type='merged')
        address1_children = [ContactFactory(user=self.user,
                                            parent=address1_parent,
                                            addresses__street=street1,
                                            addresses__post_code=post_code1)
                             for _ in range(2)]
        address2_contact = ContactFactory(user=self.user,
                                          addresses__street=street2,
                                          addresses__post_code=post_code2)

        address1_contacts = [address1_parent] + address1_children
        contacts = address1_contacts + [address2_contact]

        result = remerge.ContactPartitioner\
            .partition_by_addresses_and_middle_names(contacts)
        result = remerge.partitioning_family_therapy(result)
        sorted_result = sorted(result, key=len)

        self.assertEqual(len(result), 2)
        self.assertCountEqual(sorted_result[0], [address2_contact])
        self.assertCountEqual(sorted_result[1], address1_contacts)

    def test_drop_first_initial_contacts_missing_matches(self):
        email = self.faker.email()
        first_initial_contacts = [ContactFactory(first_name='k')
                                  for _ in range(2)]
        first_initial_contacts.extend([ContactFactory(first_name='k', addresses=None)
                                   for _ in range(2)])
        first_initial_email_match_contacts = [
            ContactFactory(first_name='k', email_addresses__address=email)
            for _ in range(2)]
        first_initial_email_match_contacts.extend([
            ContactFactory(first_name='k', email_addresses__address=email, addresses=None)
            for _ in range(2)])
        full_name_contacts = [ContactFactory(email_addresses__address=email)
                              for _ in range(3)]
        full_name_contacts.append(ContactFactory(email_addresses__address=email, addresses=None))

        contacts = (full_name_contacts + first_initial_contacts +
                    first_initial_email_match_contacts)
        result = remerge.ContactPartitioner\
            .partition_by_addresses_and_middle_names(contacts)
        flat_returned_contacts = set().union(*result)
        self.assertCountEqual(flat_returned_contacts,
                              (full_name_contacts +
                               first_initial_email_match_contacts))

    def test_drop_manual_contacts_missing_matches(self):
        email = self.faker.email()
        manual_contacts = [ContactFactory(contact_type='manual')
                                  for _ in range(2)]
        manual_contacts.extend([ContactFactory(contact_type='manual', addresses=None)
                                  for _ in range(2)])
        manual_email_match_contacts = [
            ContactFactory(contact_type='manual',
                           email_addresses__address=email)
            for _ in range(2)]
        manual_email_match_contacts.extend([
            ContactFactory(contact_type='manual',
                           addresses=None,
                           email_addresses__address=email)
            for _ in range(2)])

        normal_contacts = [ContactFactory(email_addresses__address=email)
                           for _ in range(3)]
        normal_contacts.append(ContactFactory(email_addresses__address=email, addresses=None))


        contacts = (normal_contacts + manual_contacts +
                    manual_email_match_contacts)
        result = remerge.ContactPartitioner\
            .partition_by_addresses_and_middle_names(contacts)
        flat_returned_contacts = set().union(*result)
        self.assertCountEqual(flat_returned_contacts,
                              normal_contacts + manual_email_match_contacts)


class AddressGraphTests(TestCase):
    def setUp(self):
        self.graph = networkx.Graph()
        self.graph.add_nodes_from([("A",0), ("B",0), ("C",0),
                                   ("D",0), ("E",0), ("F",0), ("G",0)])
        # Create some default cliques
        self.clique1 = (((("A",0), ("B",0)), 5), ((("A",0), ("C",0)), 3),
                        ((("B",0), ("C",0)), 2))
        self.clique2 = (((("D",0), ("E",0)), 4), ((("D",0), ("F",0)), 2),
                        ((("E",0), ("F",0)), 1))
        remerge.ContactPartitioner._add_weighted_edges(self.graph, self.clique1)
        remerge.ContactPartitioner._add_weighted_edges(self.graph, self.clique2)

    def _sorted_cliques(self, graph):
        return sorted(sorted(c) for c in networkx.clique.find_cliques(graph))

    def test_simple_cliques(self):
        cliques = self._sorted_cliques(self.graph)
        self.assertCountEqual(cliques, [[("A",0),("B",0),("C",0)],
                                        [("D",0),("E",0),("F",0)], [("G",0)]])

        # Verify adding a single edge between cliques does not merge the whole
        self.graph.add_edge(("B",0), ("E",0))
        cliques = self._sorted_cliques(self.graph)
        self.assertCountEqual(cliques,
                              [[("A",0),("B",0),("C",0)], [("B",0),("E",0)],
                               [("D",0),("E",0),("F",0)], [("G",0)]])

        # Verify removing an edge does break a clique
        self.graph.remove_edge(("A",0), ("C",0))
        cliques = self._sorted_cliques(self.graph)
        self.assertCountEqual(
            cliques,
            [[("A",0),("B",0)], [("B",0),("C",0)], [("B",0),("E",0)],
             [("D", 0), ("E", 0), ("F", 0)], [("G", 0)]])

    def test_build_clique_web(self):
        # Verify _build_clique_web will combine two cliques into one.
        cliques = self._sorted_cliques(self.graph)
        self.assertCountEqual(cliques, [[("A", 0), ("B", 0), ("C", 0)],
                                        [("D", 0), ("E", 0), ("F", 0)],
                                        [("G", 0)]])
        new_edges = remerge.ContactPartitioner._build_clique_web(self.graph,
                                                     cliques[0], cliques[1])
        remerge.ContactPartitioner._add_weighted_edges(self.graph, new_edges)
        cliques = self._sorted_cliques(self.graph)
        self.assertCountEqual(cliques, [[("A", 0), ("B", 0), ("C", 0),
                                         ("D", 0), ("E", 0), ("F", 0)],
                                        [("G", 0)]])

    def test_multi_address_edges(self):
        """Verify multi_address_edges function correctly identifies contacts
           that have multiple addresses, and then provides the appropriate
           edges to merge separate cliques.
       """
        # Setup the test
        cliques = self._sorted_cliques(self.graph)
        self.graph.add_node(("A", 1))
        # Add the new node to the second clique
        new_edges = remerge.ContactPartitioner._build_clique_web(
            self.graph, cliques[1], [("A", 1)])
        remerge.ContactPartitioner._add_weighted_edges(self.graph, new_edges)
        # Verify the setup
        cliques = self._sorted_cliques(self.graph)
        self.assertCountEqual(cliques, [[("A", 0), ("B", 0), ("C", 0)],
                                        [("A", 1), ("D", 0), ("E", 0), ("F", 0)],
                                        [("G", 0)]])

        # Verify the correct edges are returns
        edges = remerge.ContactPartitioner._reduce_multi_address_cliques(self.graph)
        expected = [((('A', 0), ('A', 1)), 100), ((('A', 0), ('D', 0)), 1),
                    ((('A', 0), ('E', 0)), 1),   ((('A', 0), ('F', 0)), 1),
                    ((('A', 1), ('B', 0)), 1),   ((('A', 1), ('C', 0)), 1),
                    ((('B', 0), ('D', 0)), 1),   ((('B', 0), ('E', 0)), 1),
                    ((('B', 0), ('F', 0)), 1),   ((('C', 0), ('D', 0)), 1),
                    ((('C', 0), ('E', 0)), 1),   ((('C', 0), ('F', 0)), 1)]
        self.assertCountEqual(edges, expected)

        # Verify this makes the correct cliques
        remerge.ContactPartitioner._add_weighted_edges(self.graph, edges)
        cliques = self._sorted_cliques(self.graph)
        self.assertCountEqual(cliques, [[("A", 0), ("A", 1), ("B", 0),
                                         ("C", 0), ("D", 0), ("E", 0), ("F", 0)],
                                        [("G", 0)]])

    def test_multi_multi_address_edges(self):
        """Same test as test_multi_address_edges except we will have multiple
           multi-address contacts.
        """
        # Setup the test
        CP = remerge.ContactPartitioner
        cliques = self._sorted_cliques(self.graph)
        self.graph.add_nodes_from([("A", 1), ("B", 1)])
        # Add the new node to the second clique
        new_edges = CP._build_clique_web(self.graph, cliques[1], [("A", 1)])
        new_edges.update(CP._build_clique_web(
                                           self.graph, cliques[2], [("B", 1)]))
        CP._add_weighted_edges(self.graph, new_edges)
        # Verify the setup
        cliques = self._sorted_cliques(self.graph)
        self.assertCountEqual(cliques, [[("A", 0), ("B", 0), ("C", 0)],
                                        [("A", 1), ("D", 0), ("E", 0), ("F", 0)],
                                        [("B", 1), ("G", 0)]])

        # Verify the correct edges are returns
        edges = CP._reduce_multi_address_cliques(self.graph)
        expected = [((('A', 0), ('A', 0)), 100), ((('A', 0), ('A', 1)), 100),
                    ((('A', 0), ('B', 1)), 1),   ((('A', 0), ('D', 0)), 1),
                    ((('A', 0), ('E', 0)), 1),   ((('A', 0), ('F', 0)), 1),
                    ((('A', 0), ('G', 0)), 1),   ((('A', 1), ('B', 0)), 1),
                    ((('A', 1), ('B', 1)), 1),   ((('A', 1), ('C', 0)), 1),
                    ((('A', 1), ('G', 0)), 1),   ((('B', 0), ('B', 0)), 100),
                    ((('B', 0), ('B', 1)), 100), ((('B', 0), ('D', 0)), 1),
                    ((('B', 0), ('E', 0)), 1),   ((('B', 0), ('F', 0)), 1),
                    ((('B', 0), ('G', 0)), 1),   ((('B', 1), ('C', 0)), 1),
                    ((('B', 1), ('D', 0)), 1),   ((('B', 1), ('E', 0)), 1),
                    ((('B', 1), ('F', 0)), 1),   ((('C', 0), ('C', 0)), 100),
                    ((('C', 0), ('D', 0)), 1),   ((('C', 0), ('E', 0)), 1),
                    ((('C', 0), ('F', 0)), 1),   ((('C', 0), ('G', 0)), 1),
                    ((('D', 0), ('G', 0)), 1),   ((('E', 0), ('G', 0)), 1),
                    ((('F', 0), ('G', 0)), 1)]
        self.assertCountEqual(edges, expected)

        # Verify this combined everything into one clique
        cliques = self._sorted_cliques(self.graph)
        self.assertCountEqual(cliques, [[("A", 0), ("A", 1), ("B", 0),
                                         ("B", 1), ("C", 0), ("D", 0),
                                         ("E", 0), ("F", 0), ("G", 0)]])


class MoveContactNotesTests(RemoveShortDescription, TestCase):

    def setUp(self):
        self.account1 = AccountFactory()
        self.account2 = AccountFactory()
        self.parent = ContactFactory()
        self.children = [
            ContactFactory(),
            ContactFactory(),
            ContactFactory(),
            ContactFactory(),
        ]

    def test_merge_one_child_note_to_parent_with_no_note(self):
        """Verify that notes on separate accounts are not merged, and that no
        extra content caused by concatenation is present.
        """
        # Control tests, assert no notes exist anywhere
        self.assertFalse(self.parent.notes.exists())
        self.assertFalse(self.children[0].notes.exists())
        self.assertFalse(self.children[2].notes.exists())

        # Create 2 notes, one per account, and assign to separate children.
        acct1_note = ContactNotesFactory(account=self.account1,
                                         contact=self.children[0])
        acct2_note = ContactNotesFactory(account=self.account2,
                                         contact=self.children[2])
        # Control test, verify our method of checking note existence is valid.
        self.assertTrue(self.children[0].notes.exists())
        self.assertTrue(self.children[2].notes.exists())

        remerge.move_contactnotes(self.parent, self.children)

        # Verify that the children no longer have notes, and the parent does
        # have notes now.
        self.assertTrue(self.parent.notes.exists())
        self.assertFalse(self.children[0].notes.exists())
        self.assertFalse(self.children[2].notes.exists())

        # Verify the contents of the acct1 note was moved to the parent without
        # any change in contents.
        new_acct1_note = self.parent.notes.get(account=self.account1)
        self.assertEqual(new_acct1_note.notes1, acct1_note.notes1)
        self.assertEqual(new_acct1_note.notes2, acct1_note.notes2)

        # Verify the contents of the acct2 note was moved to the parent without
        # any change in contents.
        new_acct2_note = self.parent.notes.get(account=self.account2)
        self.assertEqual(new_acct2_note.notes1, acct2_note.notes1)
        self.assertEqual(new_acct2_note.notes2, acct2_note.notes2)

    def test_merge_many_child_note_to_parent_that_has_notes(self):
        """Test merging multiple child notes per account to parent that already
        has notes.

        Test the following edge cases:
        - Deduplication of note contents between children
        - Deduplication of note contents between parent and children
        - Extra line breaks resulting from deduped notes, or blank notes are
          not present.
        - No extra line breaks resulting from a blank parent note field.
        - Parent contents come first, and are not lost if present.
        - No cross-contamination between accounts.
        """
        # Control tests, assert no notes exist anywhere
        self.assertFalse(self.parent.notes.exists())
        self.assertFalse(self.children[0].notes.exists())
        self.assertFalse(self.children[1].notes.exists())
        self.assertFalse(self.children[2].notes.exists())
        self.assertFalse(self.children[3].notes.exists())

        # Create notes on parent, one per account.
        ContactNotesFactory(notes1='blarg', notes2='honk',
                            account=self.account1, contact=self.parent)
        ContactNotesFactory(notes1='rawr', notes2='', account=self.account2,
                            contact=self.parent)

        # Create children notes for acct1. Specify the primary key of the notes
        # to force ordering
        ContactNotesFactory(pk=111, notes1='aaa', notes2='111',
                            account=self.account1, contact=self.children[0])
        # notes2 field on this note should be left out as it is blank
        ContactNotesFactory(pk=112, notes1='bbb', notes2='',
                            account=self.account1, contact=self.children[1])
        # notes1 field on this note should be left out as it is a dupe of the
        # first contact note.
        ContactNotesFactory(pk=113, notes1='aaa', notes2='333',
                            account=self.account1, contact=self.children[2])
        ContactNotesFactory(pk=114, notes1='ccc', notes2='444',
                            account=self.account1, contact=self.children[3])

        # Create children notes for acct2. Specify the primary key of the notes
        # to force ordering
        # notes1 field on this note should be left out as it is a dupe of the
        # parent contact note.
        ContactNotesFactory(pk=121, notes1='rawr', notes2='000',
                            account=self.account2, contact=self.children[2])
        ContactNotesFactory(pk=122, notes1='qqq', notes2='222',
                            account=self.account2, contact=self.children[3])

        # Control test, verify our existence check test works
        self.assertTrue(self.parent.notes.exists())
        self.assertTrue(self.children[0].notes.exists())
        self.assertTrue(self.children[1].notes.exists())
        self.assertTrue(self.children[2].notes.exists())
        self.assertTrue(self.children[3].notes.exists())

        remerge.move_contactnotes(self.parent, self.children)

        # Verify the only notes left belong to the parent.
        self.assertTrue(self.parent.notes.exists())
        self.assertFalse(self.children[0].notes.exists())
        self.assertFalse(self.children[1].notes.exists())
        self.assertFalse(self.children[2].notes.exists())
        self.assertFalse(self.children[3].notes.exists())

        # Verify that the parent note contents are still present, are first,
        # and any duplicates of the parent note data is not included. Also
        # verify that if the parent note is empty, we do not add any extra
        # separators.
        new_acct1_note = self.parent.notes.get(account=self.account1)
        self.assertEqual(new_acct1_note.notes1, 'blarg\n\naaa\n\nbbb\n\nccc')
        self.assertEqual(new_acct1_note.notes2, 'honk\n\n111\n\n333\n\n444')

        new_acct2_note = self.parent.notes.get(account=self.account2)
        self.assertEqual(new_acct2_note.notes1, 'rawr\n\nqqq')
        self.assertEqual(new_acct2_note.notes2, '000\n\n222')

    def test_merge_many_child_note_to_parent_with_no_note(self):
        """Test merging multiple child notes to parent without any notes works
        as expected.

        This combines the other 2 tests into one. Basically this is just to
        verify formatting works as expected when there are multiple children,
        and no parent notes.
        """
        # Control tests
        self.assertFalse(self.parent.notes.exists())
        self.assertFalse(self.children[0].notes.exists())
        self.assertFalse(self.children[1].notes.exists())
        self.assertFalse(self.children[2].notes.exists())
        self.assertFalse(self.children[3].notes.exists())

        # Create child notes for acct1. Specify the primary key of the notes to
        # force ordering
        ContactNotesFactory(pk=111, notes1='aaa', notes2='111',
                            account=self.account1, contact=self.children[0])
        ContactNotesFactory(pk=112, notes1='bbb', notes2='',
                            account=self.account1, contact=self.children[1])
        ContactNotesFactory(pk=113, notes1='aaa', notes2='333',
                            account=self.account1, contact=self.children[2])
        ContactNotesFactory(pk=114, notes1='ccc', notes2='444',
                            account=self.account1, contact=self.children[3])

        # Create child notes for acct2. Specify the primary key of the notes to
        # force ordering
        ContactNotesFactory(pk=121, notes1='aaa', notes2='111',
                            account=self.account2, contact=self.children[2])
        ContactNotesFactory(pk=122, notes1='qqq', notes2='222',
                            account=self.account2, contact=self.children[3])

        self.assertTrue(self.children[0].notes.exists())
        self.assertTrue(self.children[1].notes.exists())
        self.assertTrue(self.children[2].notes.exists())
        self.assertTrue(self.children[3].notes.exists())

        remerge.move_contactnotes(self.parent, self.children)

        self.assertTrue(self.parent.notes.exists())
        self.assertFalse(self.children[0].notes.exists())
        self.assertFalse(self.children[1].notes.exists())
        self.assertFalse(self.children[2].notes.exists())
        self.assertFalse(self.children[3].notes.exists())

        # Verify that the notes from all the children are copied into the new
        # parent note. Additionally verify that notes are deduped, and the
        # order in which notes are concatenated is stable, and based on note
        # creation order.
        new_acct1_note = self.parent.notes.get(account=self.account1)
        self.assertEqual(new_acct1_note.notes1, 'aaa\n\nbbb\n\nccc')
        self.assertEqual(new_acct1_note.notes2, '111\n\n333\n\n444')

        new_acct2_note = self.parent.notes.get(account=self.account2)
        self.assertEqual(new_acct2_note.notes1, 'aaa\n\nqqq')
        self.assertEqual(new_acct2_note.notes2, '111\n\n222')


class MoveProspectCollisionsTests(RemoveShortDescription, TestCase):
    def setUp(self):
        self.event1 = EventFactory()
        self.event2 = EventFactory()
        self.user = RevUpUserFactory()
        self.parent = ContactFactory(user=self.user)
        self.children = [
            ContactFactory(user=self.user),
            ContactFactory(user=self.user),
            ContactFactory(user=self.user),
            ContactFactory(user=self.user),
        ]

    def test_move_oldest_pc_to_parent_if_missing(self):
        """Verify that if there are no prospect collisions on the parent, that
        one of the children prospect collisions for the same event is moved.
        """
        # Create a prospect collision on the parent to verify prospect
        # collisions for seperate events are handled independently.
        ProspectCollisionFactory(user=self.user,
                                 contact=self.parent,
                                 event=self.event2)
        # Create prospect collisions for the children on a different event.
        oldest_child_pc = None
        for i, child in enumerate(self.children):
            pc = ProspectCollisionFactory(pk=(100+i), user=self.user,
                                          contact=child, event=self.event1)
            if oldest_child_pc is None:
                oldest_child_pc = pc

        # Control test
        self.assertFalse(self.parent.prospectcollision_set.filter(
            event=self.event1).exists())
        self.assertTrue(self.children[0].prospectcollision_set.exists())
        self.assertTrue(self.children[1].prospectcollision_set.exists())
        self.assertTrue(self.children[2].prospectcollision_set.exists())
        self.assertTrue(self.children[3].prospectcollision_set.exists())

        orig_count = self.parent.prospectcollision_set.count()
        remerge.move_prospectcollisions(self.parent, self.children)

        post_move_count = self.parent.prospectcollision_set.count()
        self.assertEqual(post_move_count, orig_count+1)
        self.assertTrue(self.parent.prospectcollision_set.filter(
            event=self.event1).exists())

        # Verify that the prospect collision for event1 was previously assigned
        # to one of the children.
        moved_pc = self.parent.prospectcollision_set.get(event=self.event1)
        self.assertEqual(oldest_child_pc.pk, moved_pc.pk)

        # Verify none of the children have any prospect collisions.
        self.assertFalse(self.children[0].prospectcollision_set.exists())
        self.assertFalse(self.children[1].prospectcollision_set.exists())
        self.assertFalse(self.children[2].prospectcollision_set.exists())
        self.assertFalse(self.children[3].prospectcollision_set.exists())

    def test_only_delete_if_parent_has_pc(self):
        """Verify that if the parent already has a prospect collision, that
        nothing is done to the parent prospect collision and the children
        prospect collisions are just deleted.
        """
        parent_pc = ProspectCollisionFactory(user=self.user,
                                             contact=self.parent,
                                             event=self.event1)
        for child in self.children:
            ProspectCollisionFactory(user=self.user, contact=child,
                                     event=self.event1)

        self.assertTrue(self.parent.prospectcollision_set.exists())
        self.assertTrue(self.children[0].prospectcollision_set.exists())
        self.assertTrue(self.children[1].prospectcollision_set.exists())
        self.assertTrue(self.children[2].prospectcollision_set.exists())
        self.assertTrue(self.children[3].prospectcollision_set.exists())

        orig_count = self.parent.prospectcollision_set.count()
        remerge.move_prospectcollisions(self.parent, self.children)

        # Verify none of the parent prospect collisions were changed.
        post_move_count = self.parent.prospectcollision_set.count()
        self.assertEqual(post_move_count, orig_count)
        parent_pc2 = self.parent.prospectcollision_set.get(event=self.event1)
        self.assertEqual(parent_pc.pk, parent_pc2.pk)

        # Verify none of the children have prospect collisions now
        self.assertFalse(self.children[0].prospectcollision_set.exists())
        self.assertFalse(self.children[1].prospectcollision_set.exists())
        self.assertFalse(self.children[2].prospectcollision_set.exists())
        self.assertFalse(self.children[3].prospectcollision_set.exists())


class MoveProspectsTests(RemoveShortDescription, TestCase):
    def setUp(self):
        self.event1 = EventFactory()
        self.event2 = EventFactory()
        self.user = RevUpUserFactory()
        self.parent = ContactFactory(user=self.user)
        self.children = [
            ContactFactory(user=self.user),
            ContactFactory(user=self.user),
            ContactFactory(user=self.user),
            ContactFactory(user=self.user),
        ]

    def test_just_delete_if_parent_has_filled_prospect(self):
        """Verify if the parent has a filled prospect, that we just delete all
        child prospects.
        """
        self.assertFalse(self.parent.prospect_set.exists())
        parent_prospect = ProspectFactory(user=self.user, event=self.event1,
                                          contact=self.parent)
        for child in self.children:
            ProspectFactory(user=self.user, event=self.event1, contact=child)

        # Control test
        self.assertTrue(self.parent.prospect_set.exists())
        self.assertTrue(self.children[0].prospect_set.exists())
        self.assertTrue(self.children[1].prospect_set.exists())
        self.assertTrue(self.children[2].prospect_set.exists())
        self.assertTrue(self.children[3].prospect_set.exists())

        remerge.move_prospects(self.parent, self.children)

        # Verify the parent prospect wasn't deleted, but the children were
        self.assertTrue(self.parent.prospect_set.exists())
        self.assertFalse(self.children[0].prospect_set.exists())
        self.assertFalse(self.children[1].prospect_set.exists())
        self.assertFalse(self.children[2].prospect_set.exists())
        self.assertFalse(self.children[3].prospect_set.exists())

        # Verify the parent prospect has not been touched.
        after_move_prospect = self.parent.prospect_set.get(event=self.event1)
        self.assertEqual(parent_prospect.pk, after_move_prospect.pk)
        self.assertEqual(parent_prospect.soft_amt,
                         after_move_prospect.soft_amt)
        self.assertEqual(parent_prospect.hard_amt,
                         after_move_prospect.hard_amt)
        self.assertEqual(parent_prospect.in_amt, after_move_prospect.in_amt)

    def test_move_empty_prospect_if_no_parent_prospect(self):
        """Verify that if the parent has no prospect for an event, we move one
        from the children to the parent, even if that prospect is empty.
        """
        child_prospects = set()
        for i, child in enumerate(self.children):
            p = ProspectFactory(user=self.user, event=self.event1,
                                contact=child, soft_amt=0, hard_amt=0,
                                in_amt=0)
            child_prospects.add(p)

        # Control test
        self.assertFalse(self.parent.prospect_set.exists())
        self.assertTrue(self.children[0].prospect_set.exists())
        self.assertTrue(self.children[1].prospect_set.exists())
        self.assertTrue(self.children[2].prospect_set.exists())
        self.assertTrue(self.children[3].prospect_set.exists())

        remerge.move_prospects(self.parent, self.children)

        # Verify the parent now has a prospect, and none of the children do.
        self.assertTrue(self.parent.prospect_set.exists())
        self.assertFalse(self.children[0].prospect_set.exists())
        self.assertFalse(self.children[1].prospect_set.exists())
        self.assertFalse(self.children[2].prospect_set.exists())
        self.assertFalse(self.children[3].prospect_set.exists())

        # Verify we move a prospect to the parent when the parent has no
        # prospect even if we cannot find a prospect that has non-zero
        # contents.
        after_move_prospect = self.parent.prospect_set.get(event=self.event1)
        self.assertIn(after_move_prospect, child_prospects)
        self.assertTrue(after_move_prospect.is_empty())

    def test_prefer_filled_prospect_if_no_parent_prospect(self):
        """Verify that if the parent has no prospect for an event, and we have
        children that have non-zero prospects, that we prefer moving a non-zero
        prospect to the parent.
        """
        # Create child prospects
        child_prospects = set()
        for i, child in enumerate(self.children):
            # Make some child prospects empty, and others have data.
            if i % 2:
                contents = dict(soft_amt=10, hard_amt=20, in_amt=30)
            else:
                contents = dict(soft_amt=0, hard_amt=0, in_amt=0)
            p = ProspectFactory(user=self.user, event=self.event1,
                                contact=child, **contents)
            if i % 2:
                child_prospects.add(p)

        # Control test
        self.assertFalse(self.parent.prospect_set.exists())
        self.assertTrue(self.children[0].prospect_set.exists())
        self.assertTrue(self.children[1].prospect_set.exists())
        self.assertTrue(self.children[2].prospect_set.exists())
        self.assertTrue(self.children[3].prospect_set.exists())

        remerge.move_prospects(self.parent, self.children)

        # Verify parent now has a prospect, and the children have none.
        self.assertTrue(self.parent.prospect_set.exists())
        self.assertFalse(self.children[0].prospect_set.exists())
        self.assertFalse(self.children[1].prospect_set.exists())
        self.assertFalse(self.children[2].prospect_set.exists())
        self.assertFalse(self.children[3].prospect_set.exists())

        # Verify one of the prospects with contents was moved to the parent
        # instead of one of the "empty" prospects.
        after_move_prospect = self.parent.prospect_set.get(event=self.event1)
        self.assertIn(after_move_prospect, child_prospects)
        self.assertFalse(after_move_prospect.is_empty())

    def test_delete_child_prospects_if_all_prospects_empty(self):
        """Verify that if the parent prospect is empty, and all child prospects
        are empty, that we do nothing with the parent prospect and delete all
        the children prospects.
        """
        # Control test
        self.assertFalse(self.parent.prospect_set.exists())
        # Create prospect records for parent and children for the same event,
        # and set the amounts to 0.
        parent_prospect = ProspectFactory(user=self.user, event=self.event1,
                                          contact=self.parent, soft_amt=0,
                                          hard_amt=0, in_amt=0)
        for child in self.children:
            ProspectFactory(user=self.user, event=self.event1, contact=child,
                            soft_amt=0, hard_amt=0, in_amt=0)

        # Control test.
        self.assertTrue(self.parent.prospect_set.exists())
        self.assertTrue(self.children[0].prospect_set.exists())
        self.assertTrue(self.children[1].prospect_set.exists())
        self.assertTrue(self.children[2].prospect_set.exists())
        self.assertTrue(self.children[3].prospect_set.exists())

        remerge.move_prospects(self.parent, self.children)

        # Verify the parent still has a prospect, and the children have none.
        self.assertTrue(self.parent.prospect_set.exists())
        self.assertFalse(self.children[0].prospect_set.exists())
        self.assertFalse(self.children[1].prospect_set.exists())
        self.assertFalse(self.children[2].prospect_set.exists())
        self.assertFalse(self.children[3].prospect_set.exists())

        # Verify the parent prospect has not been touched.
        after_move_prospect = self.parent.prospect_set.get(event=self.event1)
        self.assertEqual(parent_prospect.pk, after_move_prospect.pk)

    def test_prefer_child_filled_prospect_if_empty_parent_prospect(self):
        """Verify that if the parent prospect is empty, and there is a prospect
        on a child that does have contents, that we replace the parent prospect
        with one of the filled child prospects.
        """
        self.assertFalse(self.parent.prospect_set.exists())
        # Create empty parent prospect
        parent_prospect = ProspectFactory(user=self.user, event=self.event1,
                                          contact=self.parent, soft_amt=0,
                                          hard_amt=0, in_amt=0)
        # Create a mix of empty and filled child prospects.
        child_prospects = set()
        for i, child in enumerate(self.children):
            if i % 2:
                contents = dict(soft_amt=10, hard_amt=20, in_amt=30)
            else:
                contents = dict(soft_amt=0, hard_amt=0, in_amt=0)
            p = ProspectFactory(user=self.user, event=self.event1,
                                contact=child, **contents)
            if i % 2:
                child_prospects.add(p)

        # Control test
        self.assertTrue(self.parent.prospect_set.exists())
        self.assertTrue(self.children[0].prospect_set.exists())
        self.assertTrue(self.children[1].prospect_set.exists())
        self.assertTrue(self.children[2].prospect_set.exists())
        self.assertTrue(self.children[3].prospect_set.exists())

        pre_move_prospect = self.parent.prospect_set.get(event=self.event1)
        self.assertNotIn(pre_move_prospect, child_prospects)

        remerge.move_prospects(self.parent, self.children)

        # Verify only parent has prospect after move
        self.assertTrue(self.parent.prospect_set.exists())
        self.assertFalse(self.children[0].prospect_set.exists())
        self.assertFalse(self.children[1].prospect_set.exists())
        self.assertFalse(self.children[2].prospect_set.exists())
        self.assertFalse(self.children[3].prospect_set.exists())

        after_move_prospect = self.parent.prospect_set.get(event=self.event1)
        # Verify one of the prospects with contents was moved to the parent
        # instead of one of the "empty" prospects.
        self.assertIn(after_move_prospect, child_prospects)
        self.assertFalse(after_move_prospect.is_empty())

        # Verify the parent's old empty prospect record was deleted.
        with self.assertRaises(Prospect.DoesNotExist):
            Prospect.objects.get(pk=parent_prospect.pk)

    def test_move_prospectcollisions_before_deletion(self):
        """Test that we do not lose any prospect collision records when
        deleting prospect records
        """
        self.assertFalse(self.parent.prospect_set.exists())
        parent_prospect = ProspectFactory(user=self.user, event=self.event1,
                                          contact=self.parent)
        child_prospect = ProspectFactory(user=self.user, event=self.event1,
                                         contact=self.children[0])

        parent_collision = ProspectCollisionFactory(
            external_prospect=parent_prospect)
        child_collision = ProspectCollisionFactory(
            external_prospect=child_prospect)

        # Control tests.
        self.assertTrue(self.parent.prospect_set.exists())
        self.assertTrue(self.children[0].prospect_set.exists())

        self.assertCountEqual(self.parent.prospect_set.all(),
                              [parent_prospect])
        self.assertCountEqual(parent_prospect.prospectcollision_set.all(),
                              [parent_collision])

        remerge.move_prospects(self.parent, self.children)

        # Basic sanity tests
        self.assertTrue(self.parent.prospect_set.exists())
        self.assertFalse(self.children[0].prospect_set.exists())

        # Verify that the parent prospect is still the parent prospect.
        self.assertCountEqual(self.parent.prospect_set.all(),
                              [parent_prospect])

        # Verify the prospect collisions weren't deleted and the external
        # prospect reference is now the parent prospect.
        self.assertCountEqual(parent_prospect.prospectcollision_set.all(),
                              [parent_collision, child_collision])


class MoveCallTimeContactsTests(RemoveShortDescription, TestCase):
    def setUp(self):
        self.user = RevUpUserFactory()
        self.account = AccountFactory(account_user=self.user)
        self.parent = ContactFactory(user=self.user)
        self.children = [
            ContactFactory(user=self.user),
            ContactFactory(user=self.user),
            ContactFactory(user=self.user),
            ContactFactory(user=self.user),
        ]

    def test_simple_merge(self):
        """Verify two contacts merged together, where one has a ct-contact,
        will be merged correctly.
        """
        child = self.children[0]
        phone = PhoneNumberFactory(contact=child)
        ctc1 = CallTimeContactFactory(contact=child, account=self.account,
                                      primary_phone=phone)
        # This is to verify other CallTimeContacts in a group are not
        # disturbed by a merge
        group_test_ctc = CallTimeContactFactory(account=self.account,
                                                primary_phone=phone)

        logs = [CallLogFactory(ct_contact=ctc1) for i in range(3)]
        group = CallTimeContactSetFactory(account=self.account, user=self.user)
        group.add(group_test_ctc, ctc1)
        group2 = CallTimeContactSetFactory(account=self.account,
                                           user=self.user)

        # Verify some assumptions
        self.assertFalse(CallTimeContact.objects.filter(
                                        contact=self.parent).exists())
        self.assertIsNone(ctc1.merged)
        self.assertIn(ctc1, group.contacts.all())
        self.assertIn(group_test_ctc, group.contacts.all())
        self.assertNotIn(ctc1, group2.contacts.all())
        self.assertEqual(group.contact_ordering, [group_test_ctc.id, ctc1.id])

        # Verify the parent receives a ct-contact and log copies
        remerge.move_calltimecontacts(self.parent, self.children)
        parent_ctc = CallTimeContact.objects.get(contact=self.parent)
        # Verify the primary phone was set
        self.assertEqual(parent_ctc.primary_phone, phone)
        self.assertIsNone(parent_ctc.primary_email)
        self.assertEqual(
            sorted(parent_ctc.call_logs.values_list("copied_from_id",
                                                    flat=True)),
            sorted(l.id for l in logs)
        )
        # Verify original ct contact was marked merged
        ctc1.refresh_from_db()
        group.refresh_from_db()
        self.assertIsNotNone(ctc1.merged)
        # Verify original ct contact was replaced in call groups correctly
        self.assertNotIn(ctc1, group.contacts.all())
        self.assertIn(parent_ctc, group.contacts.all())
        self.assertIn(group_test_ctc, group.contacts.all())
        self.assertNotIn(ctc1, group2.contacts.all())
        self.assertNotIn(parent_ctc, group2.contacts.all())
        self.assertNotIn(group_test_ctc, group2.contacts.all())
        self.assertEqual(group.contact_ordering,
                         [group_test_ctc.id, parent_ctc.id])

    def test_mutual_merge(self):
        """Verify two contacts merged together, where both have a ct-contact,
        will be merged correctly.
        """
        ctc1 = CallTimeContactFactory(contact=self.children[0],
                                      account=self.account)
        ctc2 = CallTimeContactFactory(contact=self.children[1],
                                      account=self.account)
        # This is to verify other CallTimeContacts in a group are not
        # disturbed by a merge
        group_test_ctc = CallTimeContactFactory(account=self.account)
        logs1 = [CallLogFactory(ct_contact=ctc1) for i in range(2)]
        logs2 = [CallLogFactory(ct_contact=ctc2) for i in range(2)]
        group = CallTimeContactSetFactory(account=self.account, user=self.user)
        group.add(ctc1, group_test_ctc, ctc2)
        group2 = CallTimeContactSetFactory(account=self.account, user=self.user)
        group2.add(ctc1, group_test_ctc)

        # Verify some assumptions
        self.assertFalse(CallTimeContact.objects.filter(
            contact=self.parent).exists())
        self.assertIsNone(ctc1.merged)
        self.assertIsNone(ctc2.merged)
        self.assertIn(ctc1, group.contacts.all())
        self.assertIn(ctc2, group.contacts.all())
        self.assertIn(group_test_ctc, group.contacts.all())
        self.assertIn(ctc1, group2.contacts.all())
        self.assertIn(group_test_ctc, group2.contacts.all())
        self.assertNotIn(ctc2, group2.contacts.all())
        self.assertEqual(group.contact_ordering,
                         [ctc1.id, group_test_ctc.id, ctc2.id])
        self.assertEqual(group2.contact_ordering, [ctc1.id, group_test_ctc.id])

        remerge.move_calltimecontacts(self.parent, self.children)
        parent_ctc = CallTimeContact.objects.get(contact=self.parent)
        self.assertEqual(
            sorted(parent_ctc.call_logs.values_list("copied_from_id",
                                                    flat=True)),
            sorted(l.id for l in itertools.chain(logs1, logs2))
        )
        # Verify original ct contact was marked merged
        ctc1.refresh_from_db()
        self.assertIsNotNone(ctc1.merged)
        ctc2.refresh_from_db()
        self.assertIsNotNone(ctc2.merged)

        ## Verify calling remerge again does not duplicate logs
        remerge.move_calltimecontacts(self.parent, self.children)
        parent_ctc = CallTimeContact.objects.get(contact=self.parent)
        self.assertEqual(
            sorted(parent_ctc.call_logs.values_list("copied_from_id",
                                                    flat=True)),
            sorted(l.id for l in itertools.chain(logs1, logs2))
        )
        # Verify child ct contact was replaced in call groups correctly
        group.refresh_from_db()
        group2.refresh_from_db()
        self.assertNotIn(ctc1, group.contacts.all())
        self.assertNotIn(ctc2, group.contacts.all())
        self.assertIn(parent_ctc, group.contacts.all())
        self.assertIn(group_test_ctc, group.contacts.all())
        self.assertNotIn(ctc1, group2.contacts.all())
        self.assertNotIn(ctc2, group2.contacts.all())
        self.assertIn(parent_ctc, group2.contacts.all())
        self.assertIn(group_test_ctc, group2.contacts.all())
        self.assertEqual(group.contact_ordering,
                         [parent_ctc.id, group_test_ctc.id])
        self.assertEqual(group2.contact_ordering,
                         [parent_ctc.id, group_test_ctc.id])

    def test_regression__parent_already_in_child_call_group(self):
        """Verify two contacts merged together where the parent is already
          in the call group will not cause an error
        """
        # Setup the test
        ctc1 = CallTimeContactFactory(contact=self.children[0],
                                      account=self.account)
        ctc2 = CallTimeContactFactory(contact=self.children[1],
                                      account=self.account)
        parent_ctc = CallTimeContactFactory(contact=self.parent,
                                            account=self.account)
        # This is to verify other CallTimeContacts in a group are not
        # disturbed by a merge
        group_test_ctc = CallTimeContactFactory(account=self.account)

        group = CallTimeContactSetFactory(account=self.account, user=self.user)
        group.add(ctc1, group_test_ctc, ctc2, parent_ctc)
        self.assertCountEqual([ctc1, group_test_ctc, ctc2, parent_ctc],
                              group.contacts.all())

        # Merge the ct-contacts.
        # This would cause an error before the associated fix.
        remerge.move_calltimecontacts(self.parent, self.children)

        # Verify the merge was done correctly
        expected = [parent_ctc, group_test_ctc]
        group.refresh_from_db()
        self.assertCountEqual(expected, group.contacts.all())
        self.assertEqual(group.contact_ordering, [e.id for e in expected])


@patch.object(remerge.namelib, 'get_nicknames', return_value=[])
@patch('frontend.apps.contact.analysis.remerge._handle_first_name',
       wraps=remerge._handle_first_name)
class HandleFirstNameTests(RemoveShortDescription, TestCase):
    """Test the _handle_first_name function.

    Mock out get_nicknames because there won't be any peacock data, and so we
    can create some interesting edge cases easier.

    Also wraps _handle_first_name with a mock, though it still will call
    _handle_first_name and return the calculated result. Since _handle_first_name
    is recursive, this makes it very easy to test recursive calls.

    Because we have a handle to _handle_first_name from our imports which was
    created before _handle_first_name was patched with the mock, our initial
    call to the function won't be present in the list of mock_calls.
    """
    def test_infinite_recursion_protection(self, mock_handle_first_name,
                                           nickname_mock):
        """Ensure that _handle_first_name only processes each name once"""
        group = remerge.IdHashableSet()
        result = {group: group}
        first_name = 'john'
        other_first_name = 'jerry'
        mega_last_name = {
            first_name: [1, 2],
            other_first_name: [3]
        }
        # Update get_nicknames mock so john expands to jerry, and jerry expands
        # to john, causing what should be infinite recursion.
        nickname_mock.side_effect = [[other_first_name], [first_name]]
        first_names, pks = _handle_first_name(result, group, mega_last_name,
                                             first_name)

        # Verify the group of pks returned is still using the IdHashableSet we
        # gave it in the first place
        self.assertIs(pks, group)
        # Verify that result[group] also hasn't changed.
        self.assertIs(result[group], group)
        # Verify that the values mega_last_name have been updated with group to
        # indicate that the name has already been processed.
        self.assertIs(mega_last_name[first_name], group)
        self.assertIs(mega_last_name[other_first_name], group)
        # Verify that group contains all primary keys found in the test fixture
        self.assertCountEqual(group, [1, 2, 3])
        # Verify that first_names returned from the function contains all first
        # names that were found, and they are only listed once each.
        self.assertCountEqual(first_names, [first_name, other_first_name])

        # Verify that _handle_first_name was only recursively called once, and
        # that that call was to handle the name jerry.
        self.assertEqual(mock_handle_first_name.call_count, 1)
        self.assertEqual(
            mock_handle_first_name.mock_calls,
            [call(result, group, mega_last_name, other_first_name,
                  first_names, name_expansion=ANY)])

    def test_prefix_matching(self, mock_handle_first_name, nickname_mock):
        """Test prefix matching

        Verify that even without the benefit of name expansion or aliases, we
        are able to match names like "ed" and "eduardo".
        """
        group = remerge.IdHashableSet()
        result = {group: group}
        name1 = 'a'
        name2 = 'ab'
        name3 = 'abc'
        mega_last_name = OrderedDict([
            (name1, [1, 2]),
            (name2, [3]),
            (name3, [4])
        ])
        # Run the test using the name 'ab', which will allow us to verify that
        # other names starting with 'ab' are found (ex- 'abc'), and names that
        # 'ab' start with (ex- 'a') are both found.
        first_names, pks = _handle_first_name(result, group, mega_last_name,
                                             name2)

        self.assertIs(pks, group)
        # Verify that all contact ids are included in the result group.
        self.assertCountEqual(group, [1, 2, 3, 4])
        # Verify that first_names returned from the function contains all first
        # names that were found, and they are only listed once each.
        self.assertCountEqual(first_names, [name1, name2, name3])
        # Verify that _handle_first_name was only recursively called twice, once
        # for each name found via prefix matching.
        self.assertEqual(mock_handle_first_name.call_count, 2)
        self.assertCountEqual(
            mock_handle_first_name.mock_calls,
            [call(result, group, mega_last_name, name1, first_names,
                  name_expansion=ANY),
             call(result, group, mega_last_name, name3, first_names,
                  name_expansion=ANY)])

    def test_messy_name(self, mock_handle_first_name, nickname_mock):
        """Ensure that we can link together names that are messy and contain
        middle name, or other issues.
        """
        group = remerge.IdHashableSet()
        result = {group: group}
        messy_first_name = 'l. john'
        first_name = 'john'
        mega_last_name = {
            messy_first_name: [3],
            first_name: [1, 2]
        }
        nickname_mock.side_effect = [[], [first_name], [first_name]]
        first_names, pks = _handle_first_name(result, group, mega_last_name,
                                             messy_first_name)

        self.assertIs(pks, group)
        self.assertIs(result[group], group)
        self.assertIs(mega_last_name[first_name], group)
        self.assertIs(mega_last_name[messy_first_name], group)
        # Verify that all contact ids are included in the result group.
        self.assertCountEqual(group, [1, 2, 3])
        # Verify that first_names returned from the function contains all first
        # names that were found, and they are only listed once each.
        self.assertCountEqual(first_names, [messy_first_name, first_name])
        self.assertEqual(
            mock_handle_first_name.mock_calls,
            [call(result, group, mega_last_name, first_name, first_names,
                  name_expansion=ANY)])

    def test_link_previously_processed_names(self, mock_handle_first_name,
                                             nickname_mock):
        """Ensure that we handle the edge case of discovering a link from the
        name currently being processed to a name that we already finished
        processing.

        We need to make sure we do not end up with the same contact id
        belonging to multiple groups.
        """
        group = remerge.IdHashableSet()
        prev_group1 = remerge.IdHashableSet([11, 12, 13])
        prev_group2 = remerge.IdHashableSet([21, 22, 23])
        result = {group: group,
                  prev_group1: prev_group1,
                  prev_group2: prev_group2}
        first_name = 'john'
        prev_group1_name = 'sandy'
        prev_group2_name = 'scooby'
        mega_last_name = {
            first_name: [1, 2],
            prev_group1_name: prev_group1,
            prev_group2_name: prev_group2,
            "shaggy": [7, 8],
            "velma": [5, 6]
        }
        # Setup test to verify that the short-circuit is used when a link to a
        # previously processed result is found. We should never find "shaggy"
        # and "velma" if the short-circuit code is working correctly.
        nickname_mock.side_effect = [[prev_group1_name, prev_group2_name],
                                     ["shaggy"], ["velma"]]
        first_names, pks = _handle_first_name(result, group, mega_last_name,
                                             first_name)

        # Ensure that the previously processed group records in the result
        # registry are updated with the value of the new group that just linked
        # (replaced) them.
        self.assertIs(result[group], group)
        self.assertIs(result[prev_group1], group)
        self.assertIs(result[prev_group2], group)

        # Ensure the "tombstone" was added to the mega_last_name so we know not
        # to process that name again.
        self.assertIs(mega_last_name[first_name], group)
        # Ensure that all correct contact ids and names are found, and that the
        # contact ids for shaggy and velma are not included.
        self.assertCountEqual(group, [1, 2, 11, 12, 13, 21, 22, 23])
        self.assertCountEqual(first_names, [first_name, prev_group1_name,
                                            prev_group2_name])
        # Verify that we never recurse for shaggy or velma.
        self.assertEqual(
            mock_handle_first_name.mock_calls,
            [call(result, group, mega_last_name, prev_group1_name,
                  first_names, name_expansion=ANY),
             call(result, group, mega_last_name, prev_group2_name,
                  first_names, name_expansion=ANY)])


@patch.object(remerge.namelib, 'get_nicknames', return_value=[])
class GroupifyTests(RemoveShortDescription, TestCase):

    def test_link_case(self, nickname_mock):
        """Ensure we get the same results as
        HandleFirstNameTests.test_link_previously_processed_names

        We're at a higher level than _handle_first_name, so make sure
        interaction between _groupify and _handle_first_name doesn't make
        anything go wonky.
        """
        name1 = 'john'
        name2 = 'sandy'
        name3 = 'scooby'
        mega_last_name = OrderedDict([
            (name1, [1, 2]),
            (name2, [3]),
            (name3, [4])
        ])
        megadict = {
            'smith': mega_last_name
        }
        nickname_mock.side_effect = [[], [], [name1, name2]]
        result = remerge._groupify(megadict)
        result = list(result)
        self.assertEqual(len(result), 1)
        self.assertCountEqual(result[0], [1, 2, 3, 4])

    def _deep_link_setup(self, nickname_mock):
        """Create test fixtures and configure the nickname mock for a really
        complex linking case.

        Note: thar be dragons here!

        This sets up a test case that we may never actually see in the real
        world, or we may see daily. The scenario is we have a complex web of
        names, where names processed later discover names previously processed.
        We use OrderedDicts to ensure that names are iterated in a stable order
        in order to force the creation of the issue.

        Here is the web of discovery we are attempting to create:

            3 -> 2 -> 1
            4 -> 1
            6 -> 5
            8 -> 7
            9 -> 7
            10 -> 6
            11 -> 9
            11 -> 10
            12 -> 11
            13 -> 12
            13 -> 4

        The numbers is the contact process order, and `->` represents a link
        discovered to a previously processed contact. So for example, "4 -> 1"
        translates to: The fourth contact group processed discovered a link to
        the first contact group processed.

        These fixtures are extremely fragile!! The order of the names in
        `names` and the contents of `nickname_mock.side_effect` are intricately
        linked!
        """
        # This is the order the names will be processed in.
        names = ['john', 'sandy', 'scooby', 'blarg', 'honk', 'rawr', 'shaggy',
                 'velma', 'daphne', 'ren', 'stimpy', 'spongebob', 'patrick']
        mega_last_name = OrderedDict([(n, [i])
                                      for i, n in enumerate(names, 1)])
        megadict = OrderedDict([
            ('smith', mega_last_name),
            ('star', {
                'patrick': [78, 88]
            })
        ])

        # Set up the `namelib.get_nicknames` mock. The contents of
        # `nickname_mock.side_effect` are the return values `get_nicknames`
        # will return, and the order of those values correlate the order of the
        # first names in names.
        nickname_mock.side_effect = [
            [], [names[0]], [names[1]], [names[0]], [], [names[4]], [],
            [names[6]], [names[6]], [names[5]], [names[8], names[9]],
            [names[10]], [names[11], names[3]], []]
        return names, megadict

    def test_deep_linking(self, nickname_mock):
        """Verify deep linking works as expected, and that first names in
        separate last name group are separate. Also verifies that no contact
        ids are lost.
        """
        # Create deep-link test fixtures. See comments and docstring in
        # _deep_link_setup method for details.
        names, megadict = self._deep_link_setup(nickname_mock)

        result = remerge._groupify(megadict)
        result = sorted(result, key=len, reverse=True)
        # Verify we got 2 groups, one for each last name
        self.assertEqual(len(result), 2)
        # Verify group with more results contains the contact id of every
        # contact with the "smith" last name.
        self.assertCountEqual(result[0], list(range(1, len(names)+1)))
        # Verify the other group contains all contact ids for contacts with the
        # "star" last name.
        self.assertCountEqual(result[1], [78, 88])

    def test_deep_linking_with_aliases(self, nickname_mock):
        """Using deep linking test fixtures as a base, verify links discovered
        as a result of aliases also work.
        """
        # Create deep-link test fixtures. See comments and docstring in
        # _deep_link_setup method for details.
        names, megadict = self._deep_link_setup(nickname_mock)

        # Create "Alias expansion" records to link "Patrick Star" with
        # "Patrick Smith"
        pd = PersonDimension.dimensions.create(
            label="whatever",
            dimension_type=PersonDimension.PersonDimensionTypes.ALIAS)
        pd.identities.create(given_name="Patrick", family_name="Smith")
        pd.identities.create(given_name="Patrick", family_name="Star")

        result = remerge._groupify(megadict)
        result = list(result)
        # Verify that with the name expansion we now only get one group of
        # contact ids.
        self.assertEqual(len(result), 1)
        self.assertCountEqual(result[0], [78, 88] + list(range(1, len(names)+1)))


@patch('frontend.libs.utils.namelib_utils.namelib.get_nicknames',
       return_value=['eddie', 'eddy'])
class ContactImportBaseTests(TransactionTestCase):
    def setUp(self):
        self.faker = faker = Factory.create()
        last_name1 = 'last_name1'
        last_name2 = 'last_name2'
        street1 = faker.street_address()
        post_code1 = faker.postalcode()
        common_email = faker.email()

        self.other_user = RevUpUserFactory()
        self.user = RevUpUserFactory(import_config=None)
        self.contact = ContactFactory(first_name="Eduardo",
                                      last_name=last_name1,
                                      addresses=None,
                                      email_addresses__address=common_email,
                                      user=self.user)
        self.first_initial_contact = ContactFactory(
            first_name="E",
            last_name=last_name1,
            user=self.user,
            email_addresses__address=common_email,
            addresses__street=street1,
            addresses__post_code=post_code1,
        )
        self.prefix_contacts = [
            self.first_initial_contact,
            ContactFactory(first_name="Edu", last_name=last_name1,
                           addresses=None, user=self.user),
            ContactFactory(first_name="Edu", last_name=last_name1,
                           addresses=None, user=self.user),
            ContactFactory(first_name="Eduardo", last_name=last_name1,
                           addresses=None, user=self.user),
            ContactFactory(first_name="Eduardo", last_name=last_name1,
                           addresses=None, user=self.user),
            ContactFactory(first_name="Eduardo.", last_name=last_name1,
                           addresses=None, user=self.user),
            ContactFactory(first_name="Eduardo.", last_name=last_name1,
                           addresses=None, user=self.user),
        ]

        self.nickname_expansion_contacts = [
            ContactFactory(first_name="Eddie", last_name=last_name1,
                           addresses=None, user=self.user),
            ContactFactory(first_name="Eddy", last_name=last_name1,
                           addresses=None, user=self.user),
            ContactFactory(first_name="Eddie", last_name=last_name1,
                           addresses=None, user=self.user),
            ContactFactory(first_name="Eddy", last_name=last_name1,
                           addresses=None, user=self.user),
        ]

        self.alias_expansion_contacts = [
            ContactFactory(first_name="Edu", last_name=last_name2,
                           addresses=None, user=self.user),
            ContactFactory(first_name="Eduardo", last_name=last_name2,
                           addresses=None, user=self.user),
            ContactFactory(first_name="Eduardo.", last_name=last_name2,
                           addresses=None, user=self.user),
        ]

        pd = PersonDimension.dimensions.create(
            label="whatever",
            dimension_type=PersonDimension.PersonDimensionTypes.ALIAS)
        pd.identities.create(given_name=self.contact.first_name,
                             family_name=self.contact.last_name)
        for contact in self.alias_expansion_contacts:
            pd.identities.create(given_name=contact.first_name,
                                 family_name=contact.last_name)

        self.not_user_contacts = [
            ContactFactory(first_name="Ed", last_name=last_name2,
                           addresses=None, user=self.other_user),
            ContactFactory(first_name="Eduardo", last_name=last_name2,
                           addresses=None, user=self.other_user),
            ContactFactory(first_name="Eduardo.", last_name=last_name2,
                           addresses=None, user=self.other_user),
            ContactFactory(first_name="Eddie", last_name=last_name1,
                           addresses=None, user=self.other_user),
            ContactFactory(first_name="Eddy", last_name=last_name1,
                           addresses=None, user=self.other_user),
            ContactFactory(first_name="Ed", last_name=last_name1,
                           addresses=None, user=self.other_user),
            ContactFactory(first_name="Eduardo", last_name=last_name1,
                           addresses=None, user=self.other_user),
            ContactFactory(first_name="Eduardo.", last_name=last_name1,
                           addresses=None, user=self.other_user),
        ]
        self.all_contacts = {self.contact, self.first_initial_contact}.union(
            self.prefix_contacts, self.nickname_expansion_contacts,
            self.alias_expansion_contacts, self.not_user_contacts)

    def test_find_siblings_prefix_only(self, nicknames_mock):
        result = ContactImportBase._find_siblings(self.contact, False, False)
        self.assertCountEqual(self.prefix_contacts, result)

    def test_find_siblings_prefix_and_nickname(self, nickname_mock):
        result = ContactImportBase._find_siblings(self.contact, True, False)
        self.assertCountEqual(
            self.prefix_contacts + self.nickname_expansion_contacts,
            result)

    def test_find_siblings_prefix_and_alias(self, nickname_mock):
        result = ContactImportBase._find_siblings(self.contact, False, True)
        self.assertCountEqual(
            self.prefix_contacts + self.alias_expansion_contacts,
            result)

    def test_find_siblings_prefix_and_nickname_and_alias(self, nickname_mock):
        result = ContactImportBase._find_siblings(self.contact, True, True)
        self.assertCountEqual(
            (self.prefix_contacts +
             self.nickname_expansion_contacts +
             self.alias_expansion_contacts), result)

    def test_find_siblings_including_force_merge(self, nicknames_mock):
        '''Verify that contacts that a force merge rule exists for are included
        in sibling search results.
        '''
        impossible_contact = ContactFactory(addresses=None, user=self.user)

        # Control test. Verify that without the force-merge rule that
        # impossible_contact won't be found.
        result = ContactImportBase._find_siblings(self.contact, False, False)
        self.assertNotIn(impossible_contact, result)
        self.assertCountEqual(self.prefix_contacts, result)

        # Create ContactLink objects to force the merge
        dimension = PersonDimension.dimensions.create(
            dimension_type=PersonDimension.PersonDimensionTypes.FORCE_MERGE,
            label="force merge test", user=self.user)
        ContactLink.objects.bulk_create([
            ContactLink(dimension=dimension, contact=self.contact),
            ContactLink(dimension=dimension, contact=impossible_contact)])

        result = ContactImportBase._find_siblings(self.contact, False, False)
        self.assertIn(impossible_contact, result)
        self.assertCountEqual(self.prefix_contacts + [impossible_contact], result)

    def test_find_siblings_recursive_search(self, nickname_mock):
        '''Test recursive sibling search to ensure we get all possible siblings
        no matter which sibling we use use as the search start point.
        '''
        last_name1 = self.faker.last_name()
        last_name2 = self.faker.last_name()
        contacts = [
            ContactFactory(first_name="Annabelle", last_name=last_name1,
                           addresses=None, user=self.user),
            ContactFactory(first_name="Annabelle", last_name=last_name2,
                           addresses=None, user=self.user),
            ContactFactory(first_name="Anna", last_name=last_name1,
                           addresses=None, user=self.user),
            ContactFactory(first_name="Anna", last_name=last_name2,
                           addresses=None, user=self.user),
        ]
        pd = PersonDimension.dimensions.create(
            label="family name change",
            dimension_type=PersonDimension.PersonDimensionTypes.ALIAS)
        pd.identities.create(given_name="Annabelle", family_name=last_name1)
        pd.identities.create(given_name="Annabelle", family_name=last_name2)

        # Control test. Prove that the base query can end up returning partial
        # results.
        name_queries = ContactImportBase._build_find_siblings_query(
            [contacts[-1]], False, True)
        result = list(Contact.objects.filter(name_queries, user=self.user))
        self.assertCountEqual(result, [contacts[-1], contacts[1]])

        def _check_contact(contact):
            result = ContactImportBase._find_siblings(contact, False, True)
            # Remove the starting point contact from the list of possible
            # results. By design `_find_siblings` never returns the contact you
            # are searching with.
            contacts_copy = contacts[:]
            contacts_copy.remove(contact)
            self.assertCountEqual(result, contacts_copy)

        # Verify that no matter which contact is used as the starting point,
        # all contacts are discovered.
        _check_contact(contacts[0])
        _check_contact(contacts[1])
        _check_contact(contacts[2])
        _check_contact(contacts[3])

    def test_no_sibling_search_on_first_initial_contact(self, *args):
        source = "test"
        cib = ContactImportBase(self.user, source, feature_flags={
            "Contact Import Name Expansion": False,
            "Contact Import Alias Expansion": False
        })
        cib._find_siblings = search_mock = MagicMock(
            return_value=self.prefix_contacts)

        with patch.object(cib, '_find_alt_base',
                          return_value=None) as alt_base_mock:
            merged = cib.merge_contact(self.first_initial_contact)

        self.first_initial_contact.refresh_from_db()
        alt_base_mock.assert_called_once_with(self.first_initial_contact)
        self.assertFalse(search_mock.called)
        self.assertTrue(self.first_initial_contact.is_primary)
        self.assertIsNone(self.first_initial_contact.parent_id)

        search_mock.reset_mock()

        with patch.object(cib, '_find_alt_base',
                          return_value=self.contact) as alt_base_mock:
            merged = cib.merge_contact(self.first_initial_contact)

        # Ensure that `_find_siblings` is not called with a contact whose first
        # name begins with an initial. Instead it should use the contact
        # discovered via the `_find_alt_base` method.
        search_mock.assert_called_once_with(self.contact,
                                            use_alias_expansion=False,
                                            use_name_expansion=False)
        alt_base_mock.assert_called_once_with(self.first_initial_contact)
        self.first_initial_contact.refresh_from_db()
        self.assertFalse(self.first_initial_contact.is_primary)
        self.assertIsNotNone(self.first_initial_contact.parent_id)

    def test_process_contacts(self, nickname_mock):
        source = "test"
        cib = ContactImportBase(self.user, source, feature_flags={
            "Contact Import Name Expansion": False,
            "Contact Import Alias Expansion": False
        })
        cib._update_if_exists = lambda x,y: (x, True)
        cib.build_contact = lambda x: x
        cib.import_record = MagicMock()

        # Control tests
        control = Contact.objects.get(pk=self.contact.pk)
        self.assertTrue(control.is_primary)
        self.assertIsNone(control.parent)
        self.assertFalse(control.contacts.exists())
        for contact in self.prefix_contacts:
            control = Contact.objects.get(pk=contact.pk)
            self.assertTrue(control.is_primary)
            self.assertIsNone(control.parent)
            self.assertFalse(control.contacts.exists())

        with patch.object(remerge, 'split_parent_children',
                          return_value=(self.contact,
                                        list(self.prefix_contacts))):
            cib.process_contacts([self.contact])
        updated_contact = Contact.objects.get(pk=self.contact.pk)
        self.assertFalse(updated_contact.is_primary)
        self.assertCountEqual(set().union(self.prefix_contacts,
                                          [self.contact]),
                              updated_contact.parent.contacts.all())
