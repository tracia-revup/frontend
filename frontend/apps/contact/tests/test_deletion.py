from itertools import chain

from django.core.exceptions import ObjectDoesNotExist
from unittest.mock import patch, MagicMock, call

from frontend.apps.analysis.factories import (
    ResultFactory, FeatureResultFactory)
from frontend.apps.authorize.factories import RevUpUserFactory
from frontend.apps.campaign.factories import ProspectFactory, AccountFactory
from frontend.apps.call_time.factories import (
    CallTimeContactFactory, CallLogFactory, CallTimeContactSetFactory)
from frontend.apps.contact.deletion import ContactDeleter, OwnerType
from frontend.apps.contact.factories import (
    ContactFactory, PhoneNumberFactory, EmailAddressFactory,
    ContactNotesFactory, AddressFactory, RawContactFactory)
from frontend.apps.contact.models import LimitTracker, Contact
from frontend.apps.locations.factories import LinkedInLocationFactory
from frontend.libs.utils.test_utils import TestCase


class ContactDeleterTests(TestCase):
    def setUp(self):
        self.user = RevUpUserFactory()
        self.user2 = RevUpUserFactory()
        self.account = AccountFactory()
        self.callgroup = CallTimeContactSetFactory(user=self.user)
        self.linkedin_location = LinkedInLocationFactory()
        self.maxDiff = None

    def _prep_contact_deletion_test(self, contact, count=1):
        """Build a bunch of factories"""
        def _gen_factories(f, **kwargs):
            return [f(**kwargs) for i in range(count)]

        contact.locations.add(self.linkedin_location)

        # Create factories for contact
        factories = (PhoneNumberFactory, EmailAddressFactory, AddressFactory)
        objs = []
        for factory in factories:
            objs.extend(_gen_factories(factory, contact=contact))

        # Create Results
        result = ResultFactory(contact=contact, analysis__account=self.account)
        objs.extend(_gen_factories(FeatureResultFactory, result=result))

        # Call Time
        calltime = CallTimeContactFactory(contact=contact)
        objs.extend(_gen_factories(CallLogFactory, ct_contact=calltime))
        self.callgroup.add(calltime)

        # Add singular models
        objs.extend([
            result,
            calltime,
            ContactNotesFactory(contact=contact),
            RawContactFactory(contact=contact)
        ])
        return objs

    def test_dryrun(self):
        contact = ContactFactory(user=self.user, phone_numbers=[],
                                 email_addresses=[], addresses=[])
        self._prep_contact_deletion_test(contact)
        dryrun = ContactDeleter(contact.user).dryrun(contact)
        self.assertCountEqual(list(dryrun.items()),
                              [('Contacts', 1), ('Prospects', 0),
                               ('Call Time Contacts', 1), ('Call Logs', 1),
                               ('Call Groups', 1), ('Notes', 1)])

        contacts = [ContactFactory(user=self.user, phone_numbers=[],
                                   email_addresses=[], addresses=[])
                    for i in range(5)]
        for c in contacts: self._prep_contact_deletion_test(c)
        dryrun = ContactDeleter(contact.user).dryrun(contacts)
        self.assertCountEqual(list(dryrun.items()),
                              [('Contacts', 5), ('Prospects', 0),
                               ('Call Time Contacts', 5), ('Call Logs', 5),
                               ('Call Groups', 5), ('Notes', 5)])

    def test_dryrun_type(self):
        contact = ContactFactory(user=self.user, phone_numbers=[],
                                 email_addresses=[], addresses=[])
        self._prep_contact_deletion_test(contact)

        # Verify account-type dryrun
        dryrun = ContactDeleter(contact.user).dryrun(contact, owner_type=OwnerType.account)
        self.assertCountEqual(list(dryrun.items()),
                              [('Contacts', 1),
                               ('Call Time Contacts', 1), ('Call Groups', 1),
                               ('Call Logs', 1), ('Notes', 1)])
        # Verify user-type dryrun
        dryrun = ContactDeleter(contact.user).dryrun(contact, owner_type=OwnerType.user)
        self.assertCountEqual(list(dryrun.items()),
                              [('Contacts', 1), ('Prospects', 0),
                               ('Notes', 1)])

    def test_merge_dryrun(self):
        parent = ContactFactory(user=self.user, contact_type="merged",
                                phone_numbers=[], email_addresses=[],
                                addresses=[])
        self._prep_contact_deletion_test(parent)
        child_contacts = [ContactFactory(user=self.user, parent=parent,
                                         is_primary=False)
                          for i in range(5)]
        self.assertCountEqual(parent.get_contacts(), child_contacts)

        dryrun = ContactDeleter(parent.user).dryrun(parent)
        self.assertCountEqual(list(dryrun.items()),
                              [('Contacts', 1), ('Prospects', 0),
                               ('Call Time Contacts', 1), ('Call Logs', 1),
                               ('Call Groups', 1), ('Notes', 1)])

    def test_delete_different_users(self):
        contact1 = ContactFactory(user=self.user, phone_numbers=[],
                                  email_addresses=[], addresses=[])
        contact2 = ContactFactory(user=self.user2, phone_numbers=[],
                                  email_addresses=[], addresses=[])
        with self.assertRaises(AssertionError):
            ContactDeleter(contact1.user).delete([contact1, contact2])

    @patch("frontend.apps.contact.deletion.ContactDeleteTransaction")
    def test_delete(self, class_mock_):
        # Setup the test
        mock_ = MagicMock()
        class_mock_.return_value = mock_
        contacts = [ContactFactory(user=self.user, phone_numbers=[],
                                   email_addresses=[], addresses=[])
                    for i in range(5)]
        delete_objs = [self._prep_contact_deletion_test(contact, 3)
                       for contact in contacts]

        # Make some other contacts to verify they are not deleted
        other_contacts = [ContactFactory(user=self.user)
                          for i in range(2)]
        other_delete_objs = [self._prep_contact_deletion_test(contact)
                             for contact in other_contacts]
        # Make some other contacts from a different user to verify they
        # are not deleted
        other_contacts2 = [ContactFactory(user=self.user2)
                           for i in range(2)]
        other_delete_objs2 = [self._prep_contact_deletion_test(contact)
                              for contact in other_contacts2]

        tracker = LimitTracker.get_tracker(self.user, self.account)
        former_count = tracker.deleted_count

        ## Delete the contacts and verify the results
        results = ContactDeleter(self.user).delete(contacts)
        self.assertEqual(results, {
            'actstream.Action': 0,
            'analysis.FeatureResult': 15,
            'analysis.Result': 5,
            'analysis.ResultSort': 5,
            'call_time.CallTimeContact': 5,
            'call_time.CallLog': 15,
            'call_time.CallTimeContactSet_contacts': 5,
            'campaign.ProspectCollision': 0,
            'contact.Address': 15,
            'contact.AlmaMater': 0,
            'contact.Contact': 5,
            'contact.ContactNotes': 5,
            'contact.Contact_locations': 5,
            'contact.DateOfBirth': 5,
            'contact.EmailAddress': 15,
            'contact.ExternalProfile': 0,
            'contact.Gender': 5,
            'contact.ImageUrl': 0,
            'contact.Organization': 0,
            'contact.PhoneNumber': 15,
            'contact.RawContact': 5,
            'contact.ExternalId': 5,
            'contact.Relationship': 0,
            'entities.ContactLink': 0,
            'filtering.ContactIndex': 0,
            'filtering.ResultIndex': 0})

        ## Verify the deleted contacts were tracked
        tracker = LimitTracker.get_tracker(self.user, self.account)
        assert (tracker.deleted_count - former_count) == 5

        ## Verify all the objects were deleted
        for obj in chain(contacts, *delete_objs):
            with self.assertRaises(ObjectDoesNotExist):
                obj.refresh_from_db()

        ## Verify the other objects were not deleted
        for obj in chain(other_contacts, other_contacts2,
                         chain.from_iterable(other_delete_objs),
                         chain.from_iterable(other_delete_objs2)):
            obj.refresh_from_db()
            self.assertIsNotNone(obj.id)

        ## Verify associated objects still exist
        self.callgroup.refresh_from_db()
        self.assertIsNotNone(self.callgroup.id)
        self.linkedin_location.refresh_from_db()
        self.assertIsNotNone(self.linkedin_location.id)
        self.user.refresh_from_db()
        self.assertIsNotNone(self.user.id)

        # Verify contacts will be passed to Kafka
        self.assertCountEqual(mock_.delete.call_args_list,
                              (call(c) for c in contacts))
        self.assertTrue(mock_.commit.called)

    @patch("frontend.apps.contact.deletion.ContactDeleteTransaction")
    def test_merged_delete(self, class_mock):
        mock_ = MagicMock()
        class_mock.return_value = mock_
        parent = ContactFactory(user=self.user, contact_type="merged",
                                phone_numbers=[], email_addresses=[],
                                addresses=[])
        child_contacts = [ContactFactory(user=self.user, parent=parent)
                          for i in range(5)]
        self.assertCountEqual(parent.get_contacts(), child_contacts)

        results = ContactDeleter(self.user).delete(parent)
        self.assertEqual(results, {
            'actstream.Action': 0,
            'analysis.ResultSort': 0,
            # u'analysis.FeatureResult': 0,
            # u'call_time.CallTimeContactSet_contacts': 0,
            'campaign.ProspectCollision': 0,
            'contact.Address': 5,
            'contact.AlmaMater': 0,
            'contact.Contact': 6,
            'contact.ContactNotes': 0,
            'contact.Contact_locations': 0,
            'contact.DateOfBirth': 6,
            'contact.EmailAddress': 5,
            'contact.ExternalProfile': 0,
            'contact.Gender': 6,
            'contact.ImageUrl': 0,
            'contact.Organization': 0,
            'contact.PhoneNumber': 5,
            'contact.ExternalId': 6,
            'contact.RawContact': 0,
            'contact.Relationship': 0,
            'entities.ContactLink': 0,
            'filtering.ContactIndex': 0,})
            # u'filtering.ResultIndex': 0})

        # Verify all the objects were deleted
        for obj in [parent] + child_contacts:
            with self.assertRaises(ObjectDoesNotExist):
                obj.refresh_from_db()

        # Verify associated objects still exist
        self.assertIsNotNone(self.linkedin_location.id)
        self.user.refresh_from_db()
        self.assertIsNotNone(self.user.id)

        # Verify contacts will be passed to Kafka.
        # There should be no 'merged' contacts
        self.assertCountEqual(mock_.delete.call_args_list,
                              (call(c) for c in child_contacts))
        self.assertTrue(mock_.commit.called)

    @patch("frontend.apps.contact.deletion.ContactDeleteTransaction")
    @patch.object(Contact, 'get_preferred_gender', return_value='U')
    def test_delete_clean_parent(self, *_):
        parent = ContactFactory(user=self.user, contact_type="merged",
                                phone_numbers=[], email_addresses=[],
                                addresses=[])
        child1 = ContactFactory(user=self.user, parent=parent)
        child2 = ContactFactory(user=self.user, parent=parent)

        ## Verify deleting only one of the children does not delete the parent
        results = ContactDeleter(self.user, clean_parents=True).delete([child1])

        with self.assertRaises(ObjectDoesNotExist):
            child1.refresh_from_db()
        parent.refresh_from_db()
        self.assertIsNotNone(parent.id)
        child2.refresh_from_db()
        self.assertIsNotNone(child2.id)
        self.assertEqual(results, {
            'actstream.Action': 0,
            'analysis.ResultSort': 0,
            # u'analysis.FeatureResult': 0,
            # u'call_time.CallTimeContactSet_contacts': 0,
            'campaign.ProspectCollision': 0,
            'contact.Address': 1,
            'contact.AlmaMater': 0,
            'contact.Contact': 1,
            'contact.ContactNotes': 0,
            'contact.Contact_locations': 0,
            'contact.DateOfBirth': 1,
            'contact.EmailAddress': 1,
            'contact.ExternalProfile': 0,
            'contact.Gender': 1,
            'contact.ImageUrl': 0,
            'contact.Organization': 0,
            'contact.PhoneNumber': 1,
            'contact.ExternalId': 1,
            'contact.RawContact': 0,
            'contact.Relationship': 0,
            'entities.ContactLink': 0,
            'filtering.ContactIndex': 0,})
            # u'filtering.ResultIndex': 0})

        ## Verify deleting all the children of a parent deletes the parent
        child1 = ContactFactory(user=self.user, parent=parent)
        results = ContactDeleter(self.user,
                                 clean_parents=True).delete([child1, child2])

        for obj in (parent, child1, child2):
            with self.assertRaises(ObjectDoesNotExist):
                obj.refresh_from_db()

        self.assertEqual(results, {
            'actstream.Action': 0,
            'analysis.ResultSort': 0,
            # u'analysis.FeatureResult': 0,
            # u'call_time.CallTimeContactSet_contacts': 0,
            'campaign.ProspectCollision': 0,
            'contact.Address': 2,
            'contact.AlmaMater': 0,
            'contact.Contact': 3,
            'contact.ContactNotes': 0,
            'contact.Contact_locations': 0,
            'contact.DateOfBirth': 3,
            'contact.EmailAddress': 2,
            'contact.ExternalProfile': 0,
            'contact.Gender': 3,
            'contact.ImageUrl': 0,
            'contact.Organization': 0,
            'contact.PhoneNumber': 2,
            'contact.RawContact': 0,
            'contact.ExternalId': 3,
            'contact.Relationship': 0,
            'entities.ContactLink': 0,
            # This index was generated in the previous call to delete
            'filtering.ContactIndex': 1,})
            # u'filtering.ResultIndex': 0})

    @patch("frontend.apps.contact.deletion.ContactDeleteTransaction")
    def test_delete_children_false(self, _):
        parent = ContactFactory(user=self.user, contact_type="merged",
                                phone_numbers=[], email_addresses=[],
                                addresses=[])
        child_contacts = [ContactFactory(user=self.user, parent=parent)
                          for i in range(5)]
        self.assertCountEqual(parent.get_contacts(), child_contacts)

        results = ContactDeleter(self.user, delete_children=False).delete(parent)
        self.assertEqual(results, {
            'actstream.Action': 0,
            'analysis.ResultSort': 0,
            # u'analysis.FeatureResult': 0,
            # u'call_time.CallTimeContactSet_contacts': 0,
            'campaign.ProspectCollision': 0,
            'contact.Address': 0,
            'contact.AlmaMater': 0,
            'contact.Contact': 1,
            'contact.ContactNotes': 0,
            'contact.Contact_locations': 0,
            'contact.DateOfBirth': 1,
            'contact.ExternalProfile': 0,
            'contact.Gender': 1,
            'contact.ImageUrl': 0,
            'contact.Organization': 0,
            'contact.RawContact': 0,
            'contact.ExternalId': 1,
            'contact.Relationship': 0,
            'entities.ContactLink': 0,
            'filtering.ContactIndex': 0,})
            # u'filtering.ResultIndex': 0})

        # Verify parent was deleted
        with self.assertRaises(ObjectDoesNotExist):
            parent.refresh_from_db()

        # Verify the other contacts were not deleted
        for obj in child_contacts:
            obj.refresh_from_db()
            self.assertIsNotNone(obj.id)
            self.assertIsNone(obj.parent_id)

    def test_has_relevant_data(self):
        contact = ContactFactory(user=self.user, phone_numbers=[],
                                 email_addresses=[], addresses=[])
        cd = ContactDeleter(self.user)
        self.assertFalse(cd.has_relevant_data(contact))

        ## Verify adding a contact attribute has no effect
        AddressFactory.create(contact=contact)
        self.assertFalse(cd.has_relevant_data(contact))
        self.assertIsNotNone(contact.addresses.first())

        ## Verify adding a relevant fields does have an effect
        # CallTime
        calltime = CallTimeContactFactory(contact=contact)
        self.assertTrue(cd.has_relevant_data(contact))
        calltime.delete(force=True)
        self.assertFalse(cd.has_relevant_data(contact))

        # Notes
        notes = ContactNotesFactory(contact=contact)
        self.assertTrue(cd.has_relevant_data(contact))
        notes.delete()
        self.assertFalse(cd.has_relevant_data(contact))

        # Prospect
        prospect = ProspectFactory(contact=contact)
        self.assertTrue(cd.has_relevant_data(contact))
        prospect.delete()
        self.assertFalse(cd.has_relevant_data(contact))

        # General test
        self._prep_contact_deletion_test(contact)
        self.assertTrue(cd.has_relevant_data(contact))

