from collections import Iterable
import string

import factory
from factory.fuzzy import FuzzyText, FuzzyChoice, FuzzyInteger

from frontend.apps.contact.models import (
    Contact, ImportRecord, PhoneNumber, ContactNotes, EmailAddress, Address,
    ContactFileUpload, ImportConfig, RawContact, DateOfBirth, Gender,
    ExternalId, Relationship)
from frontend.apps.contact.analysis.contact_import.base import ContactImportBase


@factory.use_strategy(factory.CREATE_STRATEGY)
class ContactFactory(factory.DjangoModelFactory):
    class Meta:
        model = Contact

    user = factory.SubFactory("frontend.apps.authorize.factories"
                              ".RevUpUserFactory")
    contact_id = FuzzyText()
    contact_type = "google-oauth2"
    is_primary = True

    first_name = FuzzyText()
    last_name = FuzzyText()
    composite_id = FuzzyText()
    first_name_lower = factory.LazyAttribute(
        lambda x: ContactImportBase._clean_and_lowercase_name(
            x.first_name))
    last_name_lower = factory.LazyAttribute(
        lambda x: ContactImportBase._clean_and_lowercase_name(
            x.last_name))
    email_addresses = factory.RelatedFactory(
        'frontend.apps.contact.factories.EmailAddressFactory', 'contact')
    phone_numbers = factory.RelatedFactory(
        'frontend.apps.contact.factories.PhoneNumberFactory', 'contact')
    addresses = factory.RelatedFactory(
        'frontend.apps.contact.factories.AddressFactory', 'contact')
    external_ids = factory.RelatedFactory(
        'frontend.apps.contact.factories.ExternalIdFactory', 'contact')
    dob = factory.RelatedFactory(
        'frontend.apps.contact.factories.DateOfBirthFactory', 'contact')
    gender = factory.RelatedFactory(
        'frontend.apps.contact.factories.GenderFactory', 'contact')

    @factory.post_generation
    def contacts(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted is not None:
            if not isinstance(extracted, Iterable):
                extracted = [extracted]
            self.contacts.add(*extracted)

    @factory.post_generation
    def locations(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted is not None:
            if not isinstance(extracted, Iterable):
                extracted = [extracted]
            self.locations.add(*extracted)


@factory.use_strategy(factory.CREATE_STRATEGY)
class RelationshipFactory(factory.DjangoModelFactory):
    class Meta:
        model = Relationship

    contact = factory.SubFactory(ContactFactory)
    relation = FuzzyText()
    name = factory.Faker('name')


@factory.use_strategy(factory.CREATE_STRATEGY)
class ExternalIdFactory(factory.DjangoModelFactory):
    class Meta:
        model = ExternalId

    contact = factory.SubFactory(ContactFactory)
    source = FuzzyText()
    value = FuzzyText()


@factory.use_strategy(factory.CREATE_STRATEGY)
class PhoneNumberFactory(factory.DjangoModelFactory):
    class Meta:
        model = PhoneNumber

    contact = factory.SubFactory(ContactFactory)
    number = FuzzyText(chars=string.digits, prefix='15', length=9)


@factory.use_strategy(factory.CREATE_STRATEGY)
class DateOfBirthFactory(factory.DjangoModelFactory):
    class Meta:
        model = DateOfBirth

    contact = factory.SubFactory(ContactFactory)
    birth_date = factory.Faker('date')


@factory.use_strategy(factory.CREATE_STRATEGY)
class GenderFactory(factory.DjangoModelFactory):
    class Meta:
        model = Gender

    contact = factory.SubFactory(ContactFactory)
    gender_type = FuzzyChoice(['f', 'm', 'u'])


@factory.use_strategy(factory.CREATE_STRATEGY)
class EmailAddressFactory(factory.DjangoModelFactory):
    class Meta:
        model = EmailAddress

    contact = factory.SubFactory(ContactFactory)
    address = factory.Faker('email')
    display_name = factory.LazyAttribute(
        lambda o: "{} {}".format(o.contact.first_name,
                                  o.contact.last_name))
    primary = False


@factory.use_strategy(factory.CREATE_STRATEGY)
class AddressFactory(factory.DjangoModelFactory):
    class Meta:
        model = Address

    contact = factory.SubFactory(ContactFactory)
    street = factory.Faker('street_address')
    post_code = factory.Faker('postalcode')


@factory.use_strategy(factory.CREATE_STRATEGY)
class ImportRecordFactory(factory.DjangoModelFactory):
    class Meta:
        model = ImportRecord

    user = factory.SubFactory("frontend.apps.authorize.factories"
                              ".RevUpUserFactory")
    label = FuzzyText()
    uid = FuzzyText()
    transaction_id = FuzzyText()
    task_id = FuzzyText()
    num_created = FuzzyInteger(low=10)
    num_existing = FuzzyInteger(low=10)


@factory.use_strategy(factory.CREATE_STRATEGY)
class ContactNotesFactory(factory.DjangoModelFactory):
    class Meta:
        model = ContactNotes

    contact = factory.SubFactory(ContactFactory)
    account = factory.SubFactory(
        "frontend.apps.campaign.factories.PoliticalCampaignFactory")
    notes1 = FuzzyText()
    notes2 = FuzzyText()


class ContactFileUploadFactory(factory.DjangoModelFactory):
    class Meta:
        model = ContactFileUpload
    user = factory.SubFactory("frontend.apps.authorize.factories"
                              ".RevUpUserFactory")
    file_name = FuzzyText()
    file_type = 'csv'


class ImportConfigFactory(factory.DjangoModelFactory):
    class Meta:
        model = ImportConfig

    title = FuzzyText()

    @factory.post_generation
    def filters(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted is not None:
            if not isinstance(extracted, Iterable):
                extracted = [extracted]
            self.filters.add(*extracted)
        else:
            from frontend.apps.filtering.factories import FilterFactory
            self.filters.add(FilterFactory(gender=True),
                             FilterFactory(state=True),
                             FilterFactory(city=True))


class RawContactFactory(factory.DjangoModelFactory):
    class Meta:
        model = RawContact

    contact = factory.SubFactory(ContactFactory)
    raw_contact = FuzzyText()
