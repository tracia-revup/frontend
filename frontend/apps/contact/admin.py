from django.contrib import admin

from .models import LimitTracker


class LimitTrackerAdmin(admin.ModelAdmin):
    search_fields = ("user__email", "user__last_name",
                     "account__organization_name")
    list_display = ('user', 'account', "analyzed_count", "deleted_count")


admin.site.register(LimitTracker, LimitTrackerAdmin)
