import logging

from django.db import transaction
from celery.utils import uuid
from frontend.apps.authorize.models import RevUpTask
from frontend.apps.contact.tasks import (
    social_auth_import_task, cs_import_task, nb_import_task, hf_import_task,
    contact_merging)


LOGGER = logging.getLogger(__name__)


def _import_worker(user, account, import_type, uid, task_type,
                   task_method, task_args, task_model_kwargs):
    """Most of the import task setup is identical between social-auth and
    cloudsponge, so this helper is trying to keep everything DRY. Only the
    differences are passed in as arguments.
    """
    if import_type == "social":
        uid_field = "social__uid"
    elif import_type == "cs" or import_type == "nb":
        uid_field = "task_type"
    else:
        raise ValueError("Unknown import type")

    with transaction.atomic():
        task_lock = user.lock()
        CONTACT_IMPORT_TASKS = RevUpTask.TaskType.CONTACT_IMPORT_TASKS
        user.clean_stale_tasks(task_lock=task_lock,
                               task_type=CONTACT_IMPORT_TASKS)

        tasks = task_lock.tasks.filter(task_type__in=CONTACT_IMPORT_TASKS,
                                       **{uid_field: uid})
        for task in tasks:
            if task.pending():
                return

        task_id = uuid()
        RevUpTask.objects.create(user=user, account=account, task_id=task_id,
                                 task_type=task_type, **task_model_kwargs)
        task_method.apply_async(args=task_args, task_id=task_id,
                                countdown=3)


def start_contact_import(user, social, account, feature_flags=None):
    """social-auth import."""
    task_types = {'facebook': RevUpTask.TaskType.FACEBOOK_IMPORT,
                  'google-oauth2': RevUpTask.TaskType.GMAIL_IMPORT,
                  'linkedin-oauth2': RevUpTask.TaskType.LINKEDIN_IMPORT,
                  'twitter': RevUpTask.TaskType.TWITTER_IMPORT}
    task_type = task_types.get(social.provider,
                               RevUpTask.TaskType.OTHER_IMPORT)
    account_id = account.id if account else None
    _import_worker(user, account, "social", social.uid, task_type,
                   social_auth_import_task,
                   [user.id, social.id, account_id, feature_flags],
                   {"social": social})


def start_cs_contact_import(user, source, import_id, account,
                            feature_flags=None):
    """cloudsponge import."""
    source = source.upper()
    task_type = RevUpTask.cs_translations.get(source,
                                              RevUpTask.TaskType.OTHER_IMPORT)
    account_id = account.id if account else None
    # task_type twice is not a typo
    _import_worker(user, account, "cs", task_type, task_type, cs_import_task,
                   [user.id, source, import_id, account_id, feature_flags], {})


def start_nb_contact_import(user, account, source, nation_slug,
                            feature_flags=None):
    """Import contacts from nationbuilder"""
    task_type = RevUpTask.TaskType.NATIONBUILDER_IMPORT
    _import_worker(user, account, 'nb', task_type, task_type,
                   nb_import_task, [user, account, source, nation_slug,
                                    feature_flags], {})


def start_hf_contact_import(user, account, source, collection_name,
                            task_label, feature_flags=None):
    """Import contacts from housefile"""
    import_started = False

    # Create a RevUpTask
    try:
        with transaction.atomic():
            task_id = uuid()
            revup_task_kwargs = dict(
                user=user,
                account=account,
                label=task_label,
                task_id=task_id,
                task_type=RevUpTask.TaskType.HOUSEFILE_IMPORT
            )
            RevUpTask.objects.create(**revup_task_kwargs)
    except Exception as ex:
        LOGGER.exception(f'Housefile import failed on account : {account.id} '
                         f'due to {ex}')
    else:
        # Trigger a Celery task using the RevUpTask created above
        hf_import_task_args = [user, account, source, collection_name,
                               feature_flags]
        hf_import_task.apply_async(
            args=hf_import_task_args,
            task_id=task_id,
            countdown=3
        )
        import_started = True

    return import_started


def start_contact_merging(user, account, import_record, feature_flags):
    """Handy helper to queue contact merging celery task

    This function follows the same pattern as the above
    `start_*_contact_import` helpers, in that it ensures there are no other
    imports or merges already running, creates a RevUpTask, and then queues the
    celery job.

    TODO: add support for passing along a list of contact ids that merging
    should restrict operations to.
    """
    task = None
    with transaction.atomic():
        # Grab lock on user to protect against race condition
        task_lock = user.lock()
        CONTACT_IMPORT_TASKS = RevUpTask.TaskType.CONTACT_IMPORT_TASKS
        # Clean stale tasks
        user.clean_stale_tasks(task_lock=task_lock,
                               task_type=CONTACT_IMPORT_TASKS)
        # Look for import-type tasks (which merging is a type of) owned by the
        # contact owner.
        tasks = task_lock.tasks.filter(revoked=False,
                                       task_type__in=CONTACT_IMPORT_TASKS)
        if import_record.task_id:
            # If there is a celery task id on the import record, assume this
            # function has been called by that import and exclude RevUpTasks
            # with that id from our search (otherwise contact merging would
            # never start when kicked off from an import!).
            tasks = tasks.exclude(task_id=import_record.task_id)
        for task in tasks:
            # If there are any import (or merge) tasks that have not completed
            # yet, don't queue the merge yet. We do this because if there are
            # multiple imports queued up, there is no point in running merge
            # until all the imports finish. We especially don't want to run two
            # merge tasks at the same time as that could cause a merge
            # conflict.
            if task.pending():
                return

        # Create the RevUpTask for the merge.
        task_id = uuid()
        task = RevUpTask.objects.create(
            user=user, account=account, task_id=task_id,
            task_type=RevUpTask.TaskType.CONTACT_MERGE)

    if task:
        # If `task` is not `None`, then our RevUpTask was successfully created
        # and the transaction committed. This ensures that we don't wind up
        # with any merge celery jobs without an associated RevUpTask record.
        try:
            contact_merging.apply_async(
                kwargs={
                    'user_id': user.id,
                    'import_record_id': import_record.id,
                    'feature_flags': feature_flags,
                },
                task_id=task_id,
            )
        except:
            # If the contact merging task fails to queue for any reason, clean
            # up the RevUpTask
            task.revoke()
            raise
