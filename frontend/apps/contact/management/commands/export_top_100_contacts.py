"""
Creates a fixture file used to create contacts in dev environment, as well as
creating an input file for the script `dump_entities`
"""
from itertools import chain
import json
from logging import getLogger
from os.path import join as pjoin
from os import makedirs

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.core.serializers import serialize
from funcy import *

from frontend.apps.authorize.models import RevUpUser
from frontend.apps.contact.models import (
    Address, EmailAddress, Organization, AlmaMater, ImageUrl, ExternalProfile,
    PhoneNumber, RawContact, Contact
)
from frontend.apps.contact_set.models import ContactSet
from frontend.libs.utils.model_utils import ArrayAgg


LOGGER = getLogger(__name__)


def _top_score_contacts(analysis, results_qry, top_count=100):
    partition_set = analysis.partition_set
    part_id = partition_set.partition_configs.order_by(
        '-index').values_list('id', flat=True)[0]
    results = (
        results_qry
        .filter(partition_id=part_id)
        .annotate(children=ArrayAgg('contact__contacts__id'))
        .order_by('-score')[:top_count]
        .values_list('contact', 'children')
    )
    return results


def top_score_contacts(analysis, results_qry, top_count=100):
    return lfilter(None, flatten(_top_score_contacts(
        analysis, results_qry, top_count=top_count)))


def get_all_composite_ids(contact_ids):
    composite_ids = list(
        Contact.objects
        .filter(id__in=contact_ids)
        .exclude(composite_id='')
        .values_list('composite_id', flat=True)
    )
    return composite_ids


class Command(BaseCommand):
    help = '''Creates a fixture file used to create contacts in dev
    environment, as well as creating an input file for the script
    `dump_entities`.

    Contacts selected to populate the file are the top 100 scoring contacts
    from either the user or contact set given.
    '''
    def add_arguments(self, parser):
        parser.add_argument(
            '-o', '--out', default='dump', dest='output_dir',
            type=str)
        parser.add_argument(
            '-s', '--suffix', default=None,
            help='Append a suffix to fixture filenames produced.')
        parser.add_argument(
            '-c', '--contact-set', action='store_true',
            dest='use_contact_set',
            help='Source id is a contact set instead of a user.')
        parser.add_argument(
            'source_id', type=int,
            help=('The id of the contact source. By default '
                  'this is a user id.'))

    def handle(self, source_id, use_contact_set, output_dir, suffix,
               **options):
        try:
            makedirs(output_dir, exist_ok=True)
        except FileExistsError as err:
            raise CommandError(str(err))

        if use_contact_set:
            cs = ContactSet.objects.get(id=source_id)
            analysis = cs.analysis
            results_qry = cs.queryset()
        else:
            user = RevUpUser.objects.get(id=source_id)
            analysis = user.current_analysis
            results_qry = analysis.results

        contact_ids = top_score_contacts(analysis, results_qry)
        composite_ids = get_all_composite_ids(contact_ids)

        # TODO: need to handle missing linkedin locations!
        # ideally also have support for missing cities/zips
        # and maybe copy import records too...
        to_serialize = chain(
            Contact.objects.filter(id__in=contact_ids, parent=None),
            Contact.objects.filter(id__in=contact_ids).exclude(parent=None),
            Address.objects.filter(contact_id__in=contact_ids),
            EmailAddress.objects.filter(contact_id__in=contact_ids),
            Organization.objects.filter(contact_id__in=contact_ids),
            AlmaMater.objects.filter(contact_id__in=contact_ids),
            ImageUrl.objects.filter(contact_id__in=contact_ids),
            ExternalProfile.objects.filter(contact_id__in=contact_ids),
            PhoneNumber.objects.filter(contact_id__in=contact_ids),
            RawContact.objects.filter(contact_id__in=contact_ids),
        )

        fixture_suffix = '-' + suffix.lstrip('-') if suffix else ''

        contact_fixture_path = pjoin(output_dir,
                                     'contact_fixtures{}.json'.format(
                                         fixture_suffix))
        with open(contact_fixture_path, 'w') as fh:
            fh.write(serialize('json', to_serialize))

        composite_id_path = pjoin(output_dir,
                                  'contact_composite_ids{}.json'.format(
                                      fixture_suffix))
        with open(composite_id_path, 'w') as fh:
            json.dump(composite_ids, fh)
