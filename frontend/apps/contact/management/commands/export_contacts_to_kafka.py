
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from frontend.apps.contact.scripts import (
    export_raw_contacts_to_kafka, export_rawless_contacts_to_kafka,
    send_manual_contact_actions_to_kafka)


class Command(BaseCommand):
    help = "Export contacts for specified users to kafka."

    def add_arguments(self, parser):
        parser.add_argument('-r', '--raw-topic',
                            help='Override kafka topic to send contacts')
        parser.add_argument('-u', '--update-topic',
                            help='Override kafka topic to send contact updates')
        parser.add_argument('-f', '--force', action='store_true',
                            help='Enable export of contacts for all users')
        parser.add_argument('user_ids', nargs='*', type=int,
                            help='Users whose contacts will be exported')

    def handle(self, user_ids, raw_topic, update_topic, force, **options):
        if not (user_ids or force):
            raise CommandError("Either specify user ids to run export on, or "
                               "run with --force to export all users contacts")
        if raw_topic:
            settings.BACKEND_CONTACT_TOPIC = raw_topic
        if update_topic:
            settings.BACKEND_CONTACT_UPDATES_TOPIC = update_topic

        if user_ids:
            self.stdout.write(
                "Starting export for user ids: {}".format(user_ids))
        else:
            self.stdout.write("Starting export for all users")
        self.stdout.write("Export manual contact actions")
        send_manual_contact_actions_to_kafka.run(*user_ids)
        self.stdout.write(
            "Export contacts that have no raw contact associated with them")
        export_rawless_contacts_to_kafka.run(*user_ids)
        self.stdout.write("Export contacts with raw contacts")
        # We do this one last since it will cover the most contacts and take
        # the longest.
        export_raw_contacts_to_kafka.run(*user_ids)
