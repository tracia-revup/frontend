import logging
import sys

from django.core.management.base import BaseCommand, CommandError

from frontend.apps.authorize.models import RevUpUser
from frontend.apps.contact.analysis.remerge import remerge_contacts


class Command(BaseCommand):
    help = "Re-run contact merging for users specified."
    args = '<user_id user_id ...>'

    def add_arguments(self, parser):
        parser.add_argument('-n', '--dry-run', dest="dry_run",
                            default=False, action="store_true",
                            help=("Dry run. No changes are made. Shows how "
                                  "contacts would be split and merged."))
        parser.add_argument('-m', '--multi-collect', dest="multi_collect",
                            default=True, action="store_false",
                            help="Enables nickname expansion")
        parser.add_argument('-e', '--name-expansion', dest="name_expansion",
                            default=False, action="store_true",
                            help="Enables nickname expansion")
        parser.add_argument('-p', '--processes', dest="num_processes",
                            default=None, type=int,
                            help=("Number of processes to use to process "
                                  "contact groups."))
        parser.add_argument('user_ids', type=int, nargs='+', metavar='user_id')

    def _config_logging(self, debug=False):
        logger = logging.getLogger('frontend.apps.contact.analysis.remerge')

        logger.setLevel(logging.DEBUG if debug else logging.INFO)
        loggerHandler = logging.StreamHandler(sys.stdout)
        loggerFormatter = logging.Formatter(
            '%(asctime)s [%(process)d:%(thread)d:%(name)s:%(funcName)s] '
            '%(levelname)s: %(message)s')
        loggerHandler.setFormatter(loggerFormatter)
        logger.addHandler(loggerHandler)
        logger.propagate = False

    def handle(self, user_ids, **options):
        name_expansion = options.get('name_expansion')
        dryrun = options.get('dry_run')
        num_processes = options.get('num_processes')
        multi_collect = options.get('multi_collect')
        try:
            verbosity = int(options.get('verbosity', 1))
        except (ValueError, TypeError):
            verbosity = 1
        if verbosity > 0:
            self._config_logging(debug=verbosity > 1)

        users = []
        for user_id in user_ids:
            try:
                user = RevUpUser.objects.get(pk=user_id)
            except RevUpUser.DoesNotExist:
                raise CommandError('User with id %s does not exist' % user_id)
            else:
                users.append(user)

        for user in users:
            self.stdout.write(
                "Performing contact merging for user {} (id: {})".format(
                    user, user.id))
            remerge_contacts(user, dryrun=dryrun,
                             name_expansion=name_expansion,
                             num_processes=num_processes,
                             multi_collect=multi_collect)
            self.stdout.write(
                "Contact merging finished for user {} (id: {})".format(
                    user, user.id))
