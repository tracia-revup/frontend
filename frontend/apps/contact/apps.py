from django.apps import AppConfig



class ContactConfig(AppConfig):
    name = 'frontend.apps.contact'

    def ready(self):
        from actstream import registry
        registry.register(self.get_model('Contact'))
