import json
import logging

from celery.utils import uuid
from django.conf import settings
from django.db import transaction
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, Http404
from rest_condition.permissions import Or, And
from rest_framework import permissions, viewsets, response, status
from rest_framework.decorators import action
from rest_framework.pagination import PageNumberPagination
from rest_framework.views import APIView
from rest_framework_extensions.mixins import NestedViewSetMixin
from rest_framework_extensions.utils import compose_parent_pk_kwarg_name

from frontend.apps.analysis.utils import submit_analysis
from frontend.apps.authorize.api.permissions import UserIsRequester
from frontend.apps.authorize.models import RevUpTask, RevUpUser
from frontend.apps.authorize.utils import create_revuptask_for_contact_source
from frontend.apps.call_time.api.serializers import (
    NeedsAttentionCallTimeContactSerializer)
from frontend.apps.call_time.models import CallTimeContact
from frontend.apps.campaign.api.permissions import IsAccountMember
from frontend.apps.campaign.api.serializers import ProspectSerializer
from frontend.apps.campaign.models import Account, Prospect
from frontend.apps.contact.api.permissions import IsContactOwner
from frontend.apps.contact.api.serializers import (
    ContactEditorSerializer, ContactNotesSerializer, ImportRecordSerializer,
    DetailedContactSerializer)
from frontend.apps.contact.deletion import ContactDeleter
from frontend.apps.contact.models import (
    Contact, ImportRecord, ContactNotes, ContactFileUpload, FileFormatError)
from frontend.apps.contact.analysis.remerge import (
    move_calltimecontacts, move_prospects, move_prospectcollisions,
    move_contactnotes)
from frontend.apps.contact.tasks import upload_mobile_import_task
from frontend.apps.contact.task_utils import start_hf_contact_import
from frontend.apps.contact.utils import check_account_level_import
from frontend.apps.contact_set.models import ContactSet
from frontend.apps.core.product_translations import translate
from frontend.apps.entities.utils import create_force_merge
from frontend.apps.role.api.permissions import (HasPermission)
from frontend.apps.role.permissions import AdminPermissions
from frontend.apps.seat.models import Seat, DelayedAnalysis
from frontend.libs.utils.api_utils import (
    IsOwner, IsAdminUser, RedirectViewSet, AutoinjectUrlParamsIntoDataMixin,
    ViewMethodChecker, InitialMixin, NameSearchMixin, APIException,
    HttpResponseNoContent)
from frontend.libs.utils.string_utils import str_to_bool
from frontend.libs.utils.task_utils import (
    WaffleFlagsMixin, CONTACT_IMPORT_FLAGS, ANALYSIS_FLAGS)
from frontend.libs.utils.tracker_utils import action_send


LOGGER = logging.getLogger(__name__)


class BaseContactViewSet(NameSearchMixin, InitialMixin, WaffleFlagsMixin,
                         NestedViewSetMixin, viewsets.ModelViewSet):
    """This View is only accessible through the users API.

    See authorize/api/urls.py to see how it works.
    """
    serializer_class = ContactEditorSerializer
    flag_names = CONTACT_IMPORT_FLAGS + ANALYSIS_FLAGS

    class pagination_class(PageNumberPagination):
        page_size = 20
        page_size_query_param = 'page_size'
        max_page_size = 100

    def get_serializer_class(self):
        if self.action in ("retrieve", "create", "update"):
            return DetailedContactSerializer
        else:
            return super(BaseContactViewSet, self).get_serializer_class()

    def get_serializer_context(self):
        """Set the serializer to show duplicates for this API because we
        want the user to be able to edit/remove them.
        """
        context = super(BaseContactViewSet, self).get_serializer_context()
        context["show_duplicates"] = True
        context["user"] = self.user
        if self.action in ("create", "update"):
            context["flags"] = self._prepare_flags(self.request)
        return context

    def filter_queryset(self, queryset):
        # Allow name searching
        name_query = self._prepare_name_filter()
        if name_query:
            queryset = queryset.filter(name_query)

        # Sort the contacts
        sort = self.request.query_params.get("sort")
        # Note: Django doesn't support "+" for ascending, it's just left off.
        if sort not in ("+", "-") or sort == "+":
            sort = ""
        queryset = queryset.order_by(sort + "last_name_lower",
                                     "first_name_lower")

        # TODO: this doesn't seem to do anything. Need to figure out a fix.
        queryset.prefetch_related("contacts", "email_addresses",
                                  "contacts__email_addresses")
        return queryset

    def get_queryset(self, is_primary=True):
        # Allow is_primary filter
        qargs_is_primary = self.request.query_params.get('is_primary')
        if is_primary is None and qargs_is_primary:
            is_primary = str_to_bool(qargs_is_primary)
        qs = Contact.objects.filter(user=self.user)

        # If is_primary is specified, use it to filter
        if is_primary is not None:
            qs = qs.filter(is_primary=is_primary)
        return qs

    def get_object(self):
        """Make it possible to query a contact by a child ID"""
        pk = self.kwargs.get('pk')
        if not pk:
            raise Http404

        qs = self.get_queryset(is_primary=None)
        contact = get_object_or_404(qs, id=pk)
        return contact.get_oldest_ancestor()

    def _prepare_name_filter(self):
        def _build_q(first, last, operator=Q.__and__, suffix="__startswith"):
            return operator(Q(**{"first_name_lower{}".format(suffix): first}),
                            Q(**{"last_name_lower{}".format(suffix): last}))

        query = super(BaseContactViewSet, self)._prepare_name_filter()
        first_name = self.request.query_params.get(
            'first_name', '').lower().strip()
        last_name = self.request.query_params.get(
            'last_name', '').lower().strip()
        if first_name or last_name:
            if first_name and last_name:
                if str_to_bool(self.request.query_params.get("exact", False)):
                    kwargs = {"suffix": "__exact"}
                else:
                    kwargs = {}
                query |= _build_q(first_name, last_name, **kwargs)
            elif last_name:
                query |= Q(last_name_lower__startswith=last_name)
            elif first_name:
                query |= Q(first_name_lower__startswith=first_name)
        return query

    def _create_revuptask(self, contact):
        try:
            return create_revuptask_for_contact_source(
                contact, RevUpTask.TaskType.DELETE_CONTACTS)
        except RuntimeError:
            raise APIException(LOGGER, "This task is already running")

    @transaction.atomic
    def destroy(self, request, *args, **kwargs):
        # Importing here because circular import
        from frontend.apps.contact_set.tasks import update_contactsets_count

        dryrun = str_to_bool(request.query_params.get("dryrun", False))
        contact = self.get_object()
        if dryrun:
            cd = ContactDeleter(contact.user, delete_children=True)
            dryrun = cd.dryrun(contact)
            return response.Response(dryrun)

        LOGGER.info("Delete contact: {}. Deleting User: {}".format(
            contact,
            request.impersonator if request.impersonator else request.user))
        contact_id = contact.id
        results = ContactDeleter(contact.user,
                                 delete_children=True,
                                 clean_parents=True,
                                 update_progress=False).delete(contact)

        LOGGER.info("Deleted Contact {}. {}".format(contact_id, results))
        self.invalidate_cache(contact.user)
        # Update the contact-set counts.
        update_contactsets_count.delay(contact.user_id)
        return HttpResponseNoContent()

    def perform_create(self, serializer):
        super(BaseContactViewSet, self).perform_create(serializer)
        self.invalidate_cache(serializer.instance.user)
        self._submit_analysis(serializer.instance,
                              flags=serializer.context.get("flags"),
                              transaction_id=serializer.mct.transaction_id,
                              expected_count=len(serializer.mct.contacts))

    @transaction.atomic
    def perform_update(self, serializer):
        # Lock this contact to limit race conditions in simultaneous requests.
        serializer.instance = serializer.instance.lock()
        super(BaseContactViewSet, self).perform_update(serializer)
        self.invalidate_cache(serializer.instance.user)
        self._submit_analysis(serializer.instance,
                              flags=serializer.context.get("flags"),
                              transaction_id=serializer.mct.transaction_id,
                              expected_count=len(serializer.mct.contacts))

    @action(detail=False)
    def needs_attention_list(self, request, *args, **kwargs):
        """API endpoint for Contacts that need attention from the user.

        Typically this is for handling de-merged contacts
        """
        from frontend.apps.call_time.models import CallTimeContactSet
        call_group_id = request.query_params.get("call_group")

        if call_group_id:
            call_group = get_object_or_404(CallTimeContactSet,
                                           id=call_group_id, user=self.user)
            cg_contacts = call_group.contacts.values_list("contact_id",
                                                          flat=True)
            queryset = Contact.objects.filter(needs_attention=True).filter(
                                              id__in=cg_contacts)
        else:
            queryset = self.get_queryset(is_primary=None).filter(
                                                    needs_attention=True)

        # Filter and sort the queryset
        query = self._prepare_name_filter()
        if query:
            queryset = queryset.filter(query)
        queryset = queryset.order_by("last_name_lower")

        page = self.paginate_queryset(queryset)
        if page:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        else:
            serializer = self.get_serializer(queryset, many=True)
            return response.Response(serializer.data)

    @action(detail=True, methods=["get", "post"])
    def needs_attention(self, request, *args, **kwargs):
        """Build the detailed info about the contact that needs attention"""
        parent_contact = get_object_or_404(Contact, id=kwargs.get("pk"),
                                           user=self.user,
                                           needs_attention=True)

        # If this is a demerge task, query the demerge contacts
        demerge_ids = parent_contact.attention_payload.get("demerge")
        if demerge_ids:
            contacts = Contact.objects.select_related("parent").filter(
                                        id__in=demerge_ids, user=self.user)
        else:
            contacts = []

        # Gather the primary contacts from all of the children. The serializers
        # operate on the primary contacts
        primaries = set()
        for contact in contacts:
            if contact.parent:
                primaries.add(contact.parent)
            else:
                primaries.add(contact)

        # Check if the parent contact has various linked models
        if hasattr(self, "account"):
            call_time_contact = CallTimeContact.objects.filter(
                contact=parent_contact,
                account=self.account).first()
            prospect = None
        else:
            call_time_contact = None
            prospect = Prospect.objects.filter(contact=parent_contact,
                                               user=self.user).first()

        notes = ContactNotes.objects.filter(contact=parent_contact).first()

        if request.method == "POST":
            return self.needs_attention_post(
                request, parent_contact, primaries, call_time_contact,
                prospect, notes)
        else:
            data = dict(
                contacts=DetailedContactSerializer(primaries, many=True).data,
                call_time_contact=NeedsAttentionCallTimeContactSerializer(
                                                    call_time_contact).data,
                prospect=ProspectSerializer(prospect).data,
                notes=ContactNotesSerializer(notes).data
            )
            return response.Response(data)

    @transaction.atomic
    def needs_attention_post(self, request, former_parent, former_children,
                             call_time_contact, prospect, notes):
        # Get the submitted contacts
        submitted_contacts_ids = request.data.get("contacts")
        if submitted_contacts_ids:
            try:
                submitted_contacts_ids = {int(c)
                                          for c in submitted_contacts_ids}
            except TypeError:
                raise APIException(LOGGER, "'contacts' need to be integer IDs",
                                   request)
            submitted_contacts = [c for c in former_children
                                  if c.id in submitted_contacts_ids]
            if len(submitted_contacts_ids) != len(submitted_contacts):
                raise APIException(LOGGER, "Invalid 'contacts' given")
        else:
            raise APIException(LOGGER, "'contacts' are required", request)

        # Gather all child contacts from any submitted contacts
        contacts = set()
        for contact in submitted_contacts:
            if contact.contact_type == "merged":
                contacts.update(contact.get_contacts())
            else:
                contacts.add(contact)

        force_merge = False
        # If there are more than one submitted-contacts, force merge all
        # of the associated children/primary contacts
        if len(submitted_contacts) > 1:
            contacts = create_force_merge(contacts)
            force_merge = True

        # Get the single parent of all the submitted contacts. If there is
        # more than one parent, we have to error. This shouldn't happen.
        parent_struct = {c.parent if c.parent else c for c in contacts}
        if len(parent_struct) > 1:
            LOGGER.error(
                "Needs-Attention cleanup failure; ID: {}. User submitted "
                "contacts: {}. The resulting contacts have more than one "
                "parent: {}".format(
                    former_parent, submitted_contacts, parent_struct))
            new_parent = None
        else:
            new_parent = parent_struct.pop()

        # Migrate the data from the old parent to the new parent
        if new_parent:
            movers = list(contacts) + [former_parent]
            # Move the CallTimeContact
            if call_time_contact:
                move_calltimecontacts(new_parent, movers)
                # If the primary phone and email were in the non-submitted
                # contacts, we have to clear those fields
                if call_time_contact.primary_phone not in \
                        new_parent.all_phones():
                    call_time_contact.primary_phone = None
                if call_time_contact.primary_email not in \
                        new_parent.all_emails():
                    call_time_contact.primary_email = None

            # Move the Prospects
            if prospect:
                move_prospects(new_parent, movers)
                move_prospectcollisions(new_parent, movers)

            if notes:
                move_contactnotes(new_parent, movers)

            # If we merged, we need to run an analysis on the new parent
            if force_merge:
                self._submit_analysis(new_parent)

        # Turn off the needs_attention
        former_parent.needs_attention = False
        former_parent.save()
        return response.Response(status=201)

    @classmethod
    def invalidate_cache(cls, user):
        """Invalidate any caches that may be affected by this view.

        Currently, the only view that caches Contacts is the results.
        """
        from frontend.apps.analysis.api.views import (
            ContactSetAnalysisContactResultsViewSet)
        ContactSetAnalysisContactResultsViewSet.invalidate_cache(user)


class ContactViewSet(BaseContactViewSet):
    permission_classes = (And(permissions.IsAuthenticated,
                              IsOwner),)
    parent_user_lookup = compose_parent_pk_kwarg_name("user_id")

    def _initial(self):
        self.user = get_object_or_404(
            RevUpUser, id=self.kwargs.get(self.parent_user_lookup))

    def _submit_analysis(self, contact, flags=None, **kwargs):
        contact = contact.get_oldest_ancestor()
        flags = flags or self._prepare_flags(self.request)

        delayed = set()
        # Iterate over the user's assigned seats and trigger analyses
        for seat in self.user.seats.assigned().filter(
                account__active=True).select_related("account"):
            # Put the current seat at the front of the queue
            if seat == self.user.current_seat:
                submit_analysis(
                    self.user, seat.account, contact, flags=flags,
                    queue='analysis_single_contact', **kwargs)
            else:
                # Delay the analysis for the user's other seats
                delayed.add(seat)

        # If the user is staff, we don't want to do delayed analysis
        # because it's very annoying when demoing or helping clients
        if not self.user.is_admin:
            DelayedAnalysis.bulk_schedule(delayed, contact)

    def _read_file_data(self, request):
        """Helper method to grab the mobile upload from a file"""
        file_ = request.FILES.get('file', None)
        if not file_:
            raise APIException(LOGGER, 'Upload expects a file.', request)
        file_content = file_.read()
        if not file_content:
            raise APIException(LOGGER, "No contacts were uploaded.", request)
        return json.loads(file_content)

    def _read_post_data(self, request):
        """Helper method to grab the mobile upload from the post data.
        This is to support legacy uploads.
        """
        file_content = request.data.get("contacts")
        if not file_content:
            raise APIException(LOGGER, "No contacts were uploaded.", request)
        return file_content

    @action(detail=False, methods=['post'])
    def mobile_upload(self, request, **kwargs):
        account = check_account_level_import(request)

        task_type_map = dict(
            android=RevUpTask.TaskType.ANDROID_IMPORT,
            iphone=RevUpTask.TaskType.IPHONE_IMPORT)

        mobile_type = request.data.get("type", "").lower()
        if mobile_type not in task_type_map:
            raise APIException(
                LOGGER,
                "Unrecognized mobile type: {}".format(mobile_type), request)

        task_type = task_type_map[mobile_type]
        label = request.data.get("label", "{}".format(mobile_type.title()))
        uid = request.data.get("uid", )

        # Store the file for use by Celery
        try:
            app_version = int(request.data.get("app_version"))
        except (ValueError, TypeError):
            app_version = 1
        if app_version > 1:
            file_content = self._read_file_data(request)
        else:
            file_content = self._read_post_data(request)

        try:
            cfu = ContactFileUpload.create(
                request.user, ContactFileUpload.FileType.MOBILE,
                uid, file_content)
        except FileFormatError as err:
            raise APIException(LOGGER, str(err), request)

        task_id = None
        with transaction.atomic():
            task_lock = request.user.lock()
            request.user.clean_stale_tasks(
                task_type=RevUpTask.TaskType.CONTACT_IMPORT_TASKS,
                task_lock=task_lock)
            tasks = task_lock.tasks.filter(label=label, task_type=task_type)

            import_running = False
            # Prevent the same import file from scheduling twice
            for task in tasks:
                if task.pending():
                    import_running = True
                    break

            if not import_running:
                task_id = uuid()
                RevUpTask.objects.create(
                    label=label,
                    task_id=task_id, user=request.user,
                    account=account,
                    task_type=task_type)

            if task_id is not None:
                # Trigger a Celery task
                upload_mobile_import_task.apply_async(
                    args=(request.user.id, mobile_type, uid, label, cfu.id,
                          account.id if account else None, app_version,
                          self._prepare_flags(request)),
                    task_id=task_id,
                    countdown=3)
        return response.Response()


class AccountContactViewSet(BaseContactViewSet):
    parent_account_lookup = compose_parent_pk_kwarg_name("account_id")
    permission_classes = (
        And(permissions.IsAuthenticated,
            Or(HasPermission(parent_account_lookup,
                             AdminPermissions.VIEW_CONTACTS,
                             methods=['GET']),
               HasPermission(parent_account_lookup,
                             AdminPermissions.MODIFY_CONTACTS,
                             methods=['POST', 'PUT', 'DELETE']))),)

    def _initial(self):
        self.account = get_object_or_404(
            Account, id=self.kwargs.get(self.parent_account_lookup))
        self.user = self.account.account_user

    def _submit_analysis(self, contact, flags=None, **kwargs):
        contact = contact.get_oldest_ancestor()
        flags = flags or self._prepare_flags(self.request)
        submit_analysis(self.user, self.account, contact, flags=flags,
                        queue='analysis_single_contact', **kwargs)


class ImportRecordViewSet(NestedViewSetMixin, viewsets.ReadOnlyModelViewSet):
    model = ImportRecord
    queryset = ImportRecord.objects.all()
    serializer_class = ImportRecordSerializer
    permission_classes = (And(permissions.IsAuthenticated,
                              Or(IsOwner, IsAdminUser)),)

    def get_queryset(self):
        queryset = super(ImportRecordViewSet, self).get_queryset()
        # Only show ImportRecords for the current seat
        queryset = queryset.filter(
            Q(account=self.request.user.current_seat.account) |
            Q(account=None))
        # Order-by in postgres must include all distinct fields.
        # Only 'import_dt' actually matters here.
        queryset = queryset.distinct(
            "import_type", "uid", "user", "account").order_by(
            "import_type", "uid", "user", "account", "-import_dt")
        return queryset


class SeatContactsRedirectViewSet(RedirectViewSet):
    """This ViewSet redirects users from the api endpoint seat_contacts_api to
    user_contacts_api, since they both do the same thing.
    """
    permanent = True
    base_pattern_name = 'user_contacts_api'
    query_string = True
    permission_classes = (And(permissions.IsAuthenticated,
                              Or(IsAccountMember, IsAdminUser)),)

    def get_redirect_url(self, *args, **kwargs):
        """Since the seat_contacts_api url does not include user id, we need to
        get it from the seat since seat_id is included in the url.
        """
        kwargs.pop(compose_parent_pk_kwarg_name('account_id'))
        seat_id = kwargs.pop(compose_parent_pk_kwarg_name('seat_id'))
        seat = get_object_or_404(Seat, pk=seat_id)
        kwargs[compose_parent_pk_kwarg_name('user_id')] = seat.user_id
        return super(SeatContactsRedirectViewSet, self)\
                .get_redirect_url(*args, **kwargs)


class ContactNotesViewSet(AutoinjectUrlParamsIntoDataMixin, NestedViewSetMixin,
                          viewsets.ModelViewSet):
    model = ContactNotes
    queryset = ContactNotes.objects.all()
    serializer_class = ContactNotesSerializer
    permission_classes = (And(permissions.IsAuthenticated,
                              Or(And(IsContactOwner, IsAccountMember),
                                 IsAdminUser)),)
    autoinject_map = {'account': 'account_id',
                      'contact': 'contact_id'}


class ContactSetResultsContactNotesViewSet(AutoinjectUrlParamsIntoDataMixin,
                                           NestedViewSetMixin, InitialMixin,
                                           viewsets.ModelViewSet):
    parent_contact_lookup = compose_parent_pk_kwarg_name('contact_id')
    parent_user_lookup = compose_parent_pk_kwarg_name('user_id')
    parent_seat_lookup = compose_parent_pk_kwarg_name('seat_id')
    parent_contact_set_lookup = compose_parent_pk_kwarg_name('contact_set_id')
    serializer_class = ContactNotesSerializer
    permission_classes = (And(
        permissions.IsAuthenticated, UserIsRequester,
        ViewMethodChecker('verify_user_has_access_to_notes'),
        ViewMethodChecker('verify_contact_is_in_contact_set'),
        ViewMethodChecker('verify_contact_set_has_view_permission')),)
    autoinject_map = {'contact': 'contact_id'}

    def _prepare_injection(self):
        data = super(ContactSetResultsContactNotesViewSet,
                     self)._prepare_injection()
        data["account"] = self.contact_set.account_id
        return data

    def verify_user_has_access_to_notes(self, request):
        # Check the permissions on the ContactSet. The user must have "notes"
        # permissions to view or modify the notes on the contacts in this CS.
        return bool(
            self.contact_set.user_permissions(request.user)[0].get("notes"))

    def verify_contact_set_has_view_permission(self, request):
        # Check if the user has view permission for the contact set.
        return self.contact_set.has_view_permission(request.user)

    def verify_contact_is_in_contact_set(self, request):
        # We need to verify the ContactSet actually contains the
        # requested contact
        q = self.contact_set.build_query(query_on=Contact)
        # For childless merged, we need to query for both
        # primary and not. This is a bit of a hack for limitations in the
        # ContactSet.queryset method
        if self.contact.needs_attention:
            q = Q(is_primary=True) | Q(is_primary=False)
        qs = self.contact_set.queryset(query_on=Contact, query=q,
                                       id=self.contact.id)
        return qs.exists()

    def get_queryset(self):
        return ContactNotes.objects.filter(
            contact_id=self.kwargs.get(self.parent_contact_lookup),
            account=self.contact_set.account)

    def _pre_permission_initial(self):
        # queries needed before checking permission
        self.seat = get_object_or_404(
            Seat.objects.select_related('account', 'user').assigned(),
            id=self.kwargs.get(self.parent_seat_lookup),
            user_id=self.kwargs.get(self.parent_user_lookup))

        contact_set_id = self.kwargs.get(self.parent_contact_set_lookup)
        # This is a special hook that allows the requester to ask for the
        # 'root' contactset without having to lookup the ID first
        if contact_set_id == "root":
            self.contact_set = ContactSet.get_root(
                self.seat.account, self.seat.user,
                select_related='analysis')
        elif contact_set_id == "aroot":
            account = self.seat.account
            self.contact_set = ContactSet.get_root(
                account, account.account_user, select_related='analysis')
        else:
            self.contact_set = ContactSet.objects.select_related(
                'analysis').get(pk=contact_set_id)

        self.contact = get_object_or_404(
            Contact,
            id=self.kwargs.get(self.parent_contact_lookup))


class ContactImportHF(APIView, WaffleFlagsMixin):
    """API for importing housefiles"""
    flag_names = CONTACT_IMPORT_FLAGS + ANALYSIS_FLAGS
    permission_classes = (And(permissions.IsAuthenticated,
                              ViewMethodChecker(
                                  'verify_user_can_manage_housefiles')),)

    def verify_user_can_manage_housefiles(self, request):
        """Verifies whether the user has the permission
        'can_manage_housefiles'. Also returns True when the user is a
        superuser
        """
        return bool(request.user.has_perm('authorize.can_manage_housefiles'))

    def extract_request_params(self, request_params, other_params):
        """Extracts request params from the request"""
        params = request_params.copy()
        params.update(other_params)
        return params

    @classmethod
    def _get_account(cls, account_id):
        """Returns an account or None"""
        try:
            account = Account.objects.get(id=account_id)
        except Account.DoesNotExist:
            account = None
        return account

    @classmethod
    def _get_or_validate_collection_name(cls, collection_name, account):
        """Verifies that a collection with name collection_name exists.
        Returns default collection on the account if no collection was
        provided
        """
        message = ''

        if not collection_name:
            collection_name = account.client_entity_collection_name

        # Verify that collection with that name exists
        collection_info = settings.MONGO_DB.get_collection(collection_name). \
            index_information()
        if not collection_info:
            message = f"No collection exists with name {collection_name}"
            collection_name = None

        return collection_name, message

    @classmethod
    def _get_task_label(cls, account):
        """Returns a label for the RevUpTask based on the type of the
        account
        """
        product_type = account.account_profile.product_type
        task_label = translate("Client Contacts", product_type)
        import_task_label = f"{task_label} Import"
        return import_task_label

    @classmethod
    @transaction.atomic
    def _get_pending_tasks(cls, user, account):
        """Returns any pending Housefile Import tasks on the account"""
        task_lock = user.lock()
        CONTACT_IMPORT_TASKS = RevUpTask.TaskType.CONTACT_IMPORT_TASKS
        user.clean_stale_tasks(task_lock=task_lock,
                               task_type=CONTACT_IMPORT_TASKS)
        task_kwargs = dict(
            label=cls._get_task_label(account),
            user=user,
            account=account,
            task_type=RevUpTask.TaskType.HOUSEFILE_IMPORT)
        tasks = task_lock.tasks.filter(**task_kwargs)
        return tasks

    @classmethod
    def start_housefile_import(cls, request, request_params):
        """Triggers a housefile import celery task and returns the response
        accordingly
        """
        # Validate the account id
        account_id = request_params.get('account_id', None)
        account = cls._get_account(account_id)
        if not account:
            error_message = f'Invalid value for account_id : {account_id}'
            return HttpResponse(error_message, status=400)

        # Validate mongo collection name
        collection_name = request_params.get('collection_name', None)
        collection_name, error_message = cls._get_or_validate_collection_name(
            collection_name, account)
        if not collection_name:
            return HttpResponse(error_message, status=400)

        # Get pending tasks
        # Setting user to account_user instead of request.user as this
        # API is meant to be used by internal staff
        user = account.account_user
        pending_tasks = cls._get_pending_tasks(user, account)
        for task in pending_tasks:
            if task.pending():
                error_message = f'Housefile Import is already running on ' \
                                f'Account: {account_id}'
                return HttpResponse(error_message, status=400)

        # Trigger a housefile import task
        source = "housefile"
        contact_import_kwargs = dict(
            # Setting user to account_user instead of request.user as this
            # API is meant to be used by internal staff
            user=account.account_user,
            account=account,
            source=source,
            collection_name=collection_name,
            task_label=cls._get_task_label(account),
            feature_flags=cls._prepare_flags(request)
        )
        import_started = start_hf_contact_import(**contact_import_kwargs)
        if not import_started:
            error_message = f'Housefile import failed on Account: {account.id}'
            return HttpResponse(error_message, status=500)

        # Log the action
        action_send(request, request.user, verb="Importing housefile",
                    source=source)
        msg = f'Triggered housefile import from the collection ' \
              f'{collection_name} on Account: {account_id}'
        return HttpResponse(msg, status=200)

    def post(self, request, *args, **kwargs):
        request_params = self.extract_request_params(request.POST, kwargs)
        return self.start_housefile_import(request, request_params)

    def get(self, request, *args, **kwargs):
        request_params = self.extract_request_params(request.GET, kwargs)
        return self.start_housefile_import(request, request_params)
