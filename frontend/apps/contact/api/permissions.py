
from frontend.libs.utils.api_utils import IsOwner


class IsContactOwner(IsOwner):
    OBJ_FIELD = 'contact.user_id'
    CONTACT_ID_FIELD = 'contact_id'

    def has_permission(self, request, view, obj=None):
        """Check if the contact this request is operating upon belongs to the
        user making the request.
        """
        api_parent = 'parent_lookup_{}'.format(self.CONTACT_ID_FIELD)
        if api_parent in view.kwargs:
            return request.user.contact_set.filter(
                pk=view.kwargs[api_parent]).exists()

        elif view.lookup_field in view.kwargs:
            # If a specific object is given, we'll let 'has_object_permission'
            # decide if the request has permission to view it.
            return True
        else:
            return False
