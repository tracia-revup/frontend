import logging
from datetime import datetime

from django.db import transaction, models
from rest_framework import serializers, exceptions

from frontend.apps.call_time.models import CallTimeContact
from frontend.apps.contact.analysis.remerge import set_parent_children
from frontend.apps.contact.models import (
    Contact, ImportRecord, PhoneNumber, Organization, Address, AlmaMater,
    EmailAddress, ContactNotes, ImageUrl, ExternalProfile, ImportSources,
    DateOfBirth, Gender, ExternalId, Relationship)
from frontend.apps.contact.utils import KafkaMixin
from frontend.apps.filtering.models import ContactIndex
from frontend.apps.locations.api.serializers import LinkedInLocationSerializer
from frontend.libs.utils.api_utils import WritableMethodField
from frontend.libs.utils.demo_utils import map_name
from frontend.libs.utils.general_utils import instance_cache, deep_hash
from frontend.libs.utils.string_utils import random_string, random_uuid
from ..kafka import ManualContactTransaction

LOGGER = logging.getLogger(__name__)


class ContactSerializer(serializers.ModelSerializer):
    last_name = serializers.SerializerMethodField()
    contact_type = serializers.CharField(required=False)
    deletion_pending = serializers.SerializerMethodField()

    class Meta:
        model = Contact
        fields = ("id", "first_name", "last_name", "additional_name",
                  "needs_attention", "contact_type", "deletion_pending")

    def get_last_name(self, contact):
        name = contact.last_name
        if self.context.get("demo_mode", False):
            name = map_name(name)
            # Attempt to maintain the same text style
            if contact.last_name.isupper():
                name = name.upper()
        return name

    def get_deletion_pending(self, contact):
        return contact.attention_payload.get("deletion_pending", False)

    def _validate_name(self, name):
        if not name.strip():
            raise exceptions.ValidationError({"info": "Name cannot be blank"})
        else:
            return name

    def validate_first_name(self, name):
        return self._validate_name(name)

    def to_internal_value(self, data):
        """Since last_name is a method field, we need to manually write it."""
        obj = super(ContactSerializer, self).to_internal_value(data)
        obj['last_name'] = self._validate_name(data['last_name']).strip()
        return obj

    def create(self, validated_data, is_primary=True):
        validated_data.update(dict(
            first_name_lower=Contact._clean_and_lowercase_name(
                validated_data.get('first_name', '')),
            last_name_lower=Contact._clean_and_lowercase_name(
                validated_data.get('last_name', '')),
            user=self.context["user"],
            is_primary=is_primary,
            contact_type="manual",
            contact_id=random_uuid(),
            modified_by_user=datetime.now()
        ))
        validated_data["composite_id"] = Contact._calc_composite_id(
            validated_data["user"].id, validated_data["contact_id"],
            validated_data["contact_type"])
        return super(ContactSerializer, self).create(validated_data)


class ContactParentSerializer(ContactSerializer):
    def to_representation(self, contact):
        return super(ContactParentSerializer, self).to_representation(
            contact.get_oldest_ancestor())


class ContactNotesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContactNotes
        fields = ("id", "contact", "account", "notes1", "notes2", "modified")


class ImportRecordSerializerBase(serializers.ModelSerializer):
    class Meta:
        model = ImportRecord
        fields = ("id", "import_type", "label", "import_dt")


class ImportRecordSerializer(ImportRecordSerializerBase):
    num_total = serializers.ReadOnlyField()

    class Meta(ImportRecordSerializerBase.Meta):
        fields = ImportRecordSerializerBase.Meta.fields + (
                  "user", "account", "uid", "import_success",
                  "num_created", "num_existing", "num_invalid", "num_merged",
                  "num_total", "num_effective")


class InternalValueIDMixin(object):
    """Simple mixin to force the internal serializer representation to include
    the ID, so we can work on the model instance.
    """
    def to_internal_value(self, data):
        obj = super(InternalValueIDMixin, self).to_internal_value(data)
        obj['id'] = data.get("id")
        return obj


class PhoneNumberSerializer(InternalValueIDMixin, serializers.ModelSerializer):
    number = WritableMethodField(write_method_name="process_number")

    class Meta:
        model = PhoneNumber
        fields = ("id", "label", "number")

    def get_number(self, phone):
        number = phone.number
        if number and self.context.get("demo_mode", False):
            number_hash = str(hash(number))
            number = PhoneNumber(number="1{}555{}".format(
                number_hash[:3].replace("0", "2").replace("1", "2"),
                number_hash[-4:])).rfc3966
        return number

    def process_number(self, data):
        return PhoneNumber(number=data).rfc3966


class AlmaMaterSerializer(InternalValueIDMixin, serializers.ModelSerializer):
    class Meta:
        model = AlmaMater
        fields = ("id", "name", "major", "degree")


class OrganizationSerializer(InternalValueIDMixin, serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = ("id", "name", "department", "title")


class EmailAddressSerializer(InternalValueIDMixin, serializers.ModelSerializer):
    address = WritableMethodField()

    class Meta:
        model = EmailAddress
        fields = ("id", "address", "label")

    def get_address(self, email):
        if self.context.get("demo_mode", False):
            address = ""
            if hasattr(self, "root"):
                username = email.contact.first_name + \
                           map_name(email.contact.last_name)
                if not username:
                    username = random_string(8)
                address = "{}@gmail.com".format(username.lower())
            return address
        else:
            return email.address


class AddressSerializer(InternalValueIDMixin, serializers.ModelSerializer):
    region = WritableMethodField()
    city = WritableMethodField()
    post_code = WritableMethodField()

    class Meta:
        model = Address
        fields = ("id", "street", "city", "region", "post_code",
                  "country", "po_box")

    def get_region(self, address):
        if address.city_key_id:
            return address.city_key.state.abbr
        else:
            return address.region

    def get_city(self, address):
        if address.city_key_id:
            return address.city_key.name
        else:
            return address.city

    def get_post_code(self, address):
        if address.zip_key_id:
            return address.zip_key.zipcode
        else:
            return address.post_code


class ExternalUrlSerializer(InternalValueIDMixin, serializers.ModelSerializer):
    source = serializers.SerializerMethodField()

    class Meta:
        fields = ("id", "url", "source")

    def get_source(self, obj):
        return ImportSources.translate(obj.source, "")


class ExternalProfileSerializer(ExternalUrlSerializer):
    class Meta(ExternalUrlSerializer.Meta):
        model = ExternalProfile


class ExternalIdSerializer(InternalValueIDMixin, serializers.ModelSerializer):
    class Meta:
        model = ExternalId
        fields = ("id", "source", "value")


class RelationshipSerializer(InternalValueIDMixin,
                             serializers.ModelSerializer):
    class Meta:
        model = Relationship
        fields = ("id", "relation", "name")


class ImageUrlSerializer(ExternalUrlSerializer):
    url = serializers.SerializerMethodField()

    class Meta(ExternalUrlSerializer.Meta):
        model = ImageUrl

    def get_url(self, obj):
        if obj.url:
            return obj.url
        else:
            return serializers.ImageField().to_representation(obj.image)


class HideArchivedListSerializer(serializers.ListSerializer):
    def to_representation(self, data):
        """Exclude attributes that are archived."""
        iterable = data.all() if isinstance(data, models.Manager) else data
        return [self.child.to_representation(item) for item in iterable
                                                   if not item.archived]


class ContactEditorSerializerWorker(ContactSerializer):
    email_addresses = HideArchivedListSerializer(
                            child=EmailAddressSerializer(),
                            read_only=True,
                            required=False)

    class Meta:
        model = Contact
        fields = ContactSerializer.Meta.fields + ("email_addresses",)


class ContactEditorSerializer(ContactEditorSerializerWorker):
    """This is a special-case serializer that we need for the Contact Manager.

    The Contact Manager Contact editor shows the contact's email, so we need
    a serializer that does just that.
    """
    def create(self, validated_data):
        raise NotImplementedError

    def to_representation(self, primary_contact):
        # TODO: Unify this method and DetailedContactSerializer.to_native
        primary_contact = primary_contact.get_oldest_ancestor()
        primary = super(ContactEditorSerializer, self).to_representation(
            primary_contact)

        for contact in primary_contact.contacts.all().prefetch_related(
                "email_addresses"):
            contact = ContactEditorSerializerWorker(contact)
            contact = contact.data
            primary['email_addresses'].extend(contact['email_addresses'])

        return primary


class DetailContactSerializerBase(ContactSerializer):
    import_record = ImportRecordSerializerBase(required=False, read_only=True)
    dob = WritableMethodField(_field_model=DateOfBirth, required=False,
                              write_method_name='process_dob')
    gender = WritableMethodField(_field_model=Gender, required=False,
                                 write_method_name='process_gender')

    class Meta(ContactSerializer.Meta):
        fields = ContactSerializer.Meta.fields + ("import_record",
                                                  "dob", "gender")

    def get_dob(self, contact):
        _dob = contact.get_preferred_dob()
        if not _dob:
            return None
        return _dob.birth_date

    def process_dob(self, data):
        _birthdate = datetime.strptime(data, '%Y-%m-%d').date()
        return {'birth_date': _birthdate}

    def get_gender(self, contact):
        return contact.get_preferred_gender()

    def process_gender(self, data):
        if data.lower() not in ['f', 'm', 'u']:
            raise exceptions.ValidationError({"info": "Gender can be only one "
                                                      "of the following: F, M,"
                                                      " U"})
        return {'gender_type': data}


class DetailedContactSerializerWorker(DetailContactSerializerBase):
    addresses = HideArchivedListSerializer(child=AddressSerializer(),
                                           required=False)
    email_addresses = HideArchivedListSerializer(child=EmailAddressSerializer(),
                                                 required=False)
    organizations = HideArchivedListSerializer(child=OrganizationSerializer(),
                                               required=False)
    alma_maters = HideArchivedListSerializer(child=AlmaMaterSerializer(),
                                             required=False)
    phone_numbers = HideArchivedListSerializer(child=PhoneNumberSerializer(),
                                               required=False)
    external_ids = HideArchivedListSerializer(child=ExternalIdSerializer(),
                                              required=False)
    relationships = HideArchivedListSerializer(child=RelationshipSerializer(),
                                               required=False)
    locations = LinkedInLocationSerializer(many=True, required=False)
    image_urls = ImageUrlSerializer(many=True, read_only=True)
    external_profiles = ExternalProfileSerializer(many=True, read_only=True)

    class Meta(DetailContactSerializerBase.Meta):
        fields = DetailContactSerializerBase.Meta.fields + (
                  "addresses", "email_addresses", "organizations",
                  "alma_maters", "phone_numbers", "locations",
                  "image_urls", "external_profiles", "external_ids",
                  "relationships")


class DetailedContactSerializer(DetailedContactSerializerWorker):
    def _build_containers(self):
        # In general, we will want to dedupe any contact attribute values,
        # but some APIs need to show duplicates.
        if self.context.get("show_duplicates", False):
            container_class = ContactAttrContainer
        else:
            container_class = DedupedContactAttrContainer

        # These containers will store the values from the attributes of
        # all of the contacts associated with the contact being serialized.
        # The containers help us dedupe the data, if need be.
        return [
            container_class("addresses"), container_class("email_addresses"),
            container_class("organizations"), container_class("alma_maters"),
            container_class("phone_numbers"), container_class("locations"),
            container_class("image_urls"), container_class("external_profiles"),
            container_class("import_record"), container_class("external_ids"),
            container_class("relationships"),
        ]

    def to_representation(self, primary_contact):
        """Iterate over the primary contact and all its children to collect
        all of the contact attributes. E.g. address, phones, etc.
        """
        field_containers = self._build_containers()
        def _extract(c):
            for fc in field_containers:
                fc.extract(c)

        account = self.context.get("account")
        primary_contact = primary_contact.get_oldest_ancestor()
        primary_phone = None
        primary_email = None
        if account:
            try:
                # Note: We are intentionally leaving off 'active' from
                # this query. We will maintain the primary contact info
                # even if the user removes the contact from call time.
                ct_contact = CallTimeContact.objects.get(
                    contact=primary_contact,
                    account=account)
            except CallTimeContact.DoesNotExist:
                ct_contact = None

            if ct_contact:
                primary_phone = ct_contact.primary_phone_id
                primary_email = ct_contact.primary_email_id

        primary = DetailContactSerializerBase(primary_contact,
                                              context=self.context).data

        children = primary_contact.get_contacts()
        if isinstance(children, models.QuerySet):
            contacts = [primary_contact] + list(children.prefetch_related(
                "addresses__city_key__state", "email_addresses",
                "organizations", "alma_maters", "phone_numbers", "locations",
                "image_urls", "external_profiles", "external_ids",
                "relationships").select_related(
                    "import_record"))
        else:
            contacts = children

        for contact in contacts:
            # Serialize the children and extract all the attribute fields out
            contact = DetailedContactSerializerWorker(
                contact, context=self.context).data
            # Flag the primary email/phones if one exists
            self._flag_primary_attribute(contact.get("email_addresses", []),
                                         primary_email)
            self._flag_primary_attribute(contact.get("phone_numbers", []),
                                         primary_phone)
            _extract(contact)

        # Write the attributes back into the primary contact serializer.
        for field_container in field_containers:
            primary[field_container.field] = field_container.values
        return primary

    def _flag_primary_attribute(self, objs, primary_id):
        """Helper method to flag primary attributes if they match"""
        if objs and primary_id:
            for obj in objs:
                if obj.get("id") == primary_id:
                    obj["primary"] = True

    @transaction.atomic
    def update(self, instance, validated_data):
        self.mct = ManualContactTransaction(self.context.get("flags", {}).get(
                                    "Contact Import Backend Export", False))
        updated_fields = self._get_updated_fields(instance, validated_data)
        ran_create = False
        for data, field, action in updated_fields:
            if action in ['create', 'modify']:
                ran_create = True
            action_method = getattr(self, "_{}_field".format(action))
            action_method(data, field)
        if instance.contact_type != 'merged' and ran_create:
            # If the contact_type is not merged, and a new field was created on
            # a contact, then a manual contact was created, and this contact
            # now has a parent merged contact. We need to ensure that the
            # parent merged contact is whose data we send back. Because of how
            # the parent is set on the contact record, that means that our copy
            # of the record was not updated. Refresh the record before
            # returning it in order to get the parent id.
            instance.refresh_from_db()
        ContactIndex.update_index(instance.user.get_index_filters(),
                                  user=instance.user, contact=instance)
        self.mct.commit()
        return instance

    @transaction.atomic
    def create(self, validated_data):
        self.mct = ManualContactTransaction(self.context.get("flags", {}).get(
                                    "Contact Import Backend Export", False))
        contact_data = dict(
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            additional_name=validated_data.get('additional_name', ""))
        contact = self._create_manual_contact(contact_data, True)

        # Iterate over the given data and create contact attributes
        for field_name, field_data in validated_data.items():
            field = self.fields.get(field_name)
            if field_name == "locations":
                # The user isn't allowed to edit locations
                continue
            elif field_name in ['dob', 'gender']:
                    self._create_field(field_data, field, contact=contact)
            elif isinstance(field_data, list):
                # Create the new attributes
                for item in field_data:
                    self._create_field(item, field, contact=contact)

        ContactIndex.update_index(contact.user.get_index_filters(),
                                  user=contact.user, contact=contact)
        self.mct.commit()
        return contact

    def _update_field(self, data, field):
        """Update an item from a contact field. E.g. a single Address.

        The procedure to update a field is as follows:
         1) Archive the field being modified
         2) Create (or use an existing) manual contact
         3) Add the modified attribute to the manual contact
        """
        instance_id = data.pop('id')
        new = self._create_field(data, field)
        self._delete_field(instance_id, field, replace_with=new)

    def _modify_field(self, data, field):
        """Modifies the existing OneToOneField object"""
        contact, created = self._get_or_create_manual_entry()
        if not created:
            self._update_contact(contact)

        field_model = self._get_field_model(field)
        field_model.objects.update_or_create(contact=contact, defaults=data)

    def _create_field(self, data, field, contact=None):
        """Create an item from a contact field. E.g. a single Address.

        Since we are creating a new record, we need to either use an existing
        manual contact, or generate a new one.
        """
        if not contact:
            contact, created = self._get_or_create_manual_entry()
            if not created:
                self._update_contact(contact)

        field_model = self._get_field_model(field)
        data['contact'] = contact
        if "id" in data:
            # We don't want to try to save any IDs, in case one gets through
            del data["id"]
        if hasattr(field_model, 'build'):
            model_instance = field_model.build(save=True, **data)
        else:
            model_instance = field_model.objects.create(**data)

        self.mct.create(contact, model_instance)
        return model_instance

    def _delete_field(self, instance_id, field, replace_with=None):
        """Delete an item from a contact field. E.g. a single Address."""
        model_instance = self._get_field_model_instance(instance_id, field)
        self._update_contact(model_instance.contact)
        model_instance.delete(replace_with=replace_with)

        # If the model for the record being deleted does not subclass
        # KafkaMixin, then it is not supported by the backend and no further
        # work is required.
        if not isinstance(model_instance, KafkaMixin):
            return
        # Check if the attribute has any siblings in its contact that
        # have duplicate data
        has_duplicates = False
        siblings = model_instance.__class__.objects.filter(
                                contact=model_instance.contact, archived=None)
        instance_data = model_instance.format_for_kafka()
        for sibling in siblings:
            if instance_data == sibling.format_for_kafka():
                has_duplicates = True
                break

        # The data pipeline removes duplicates. If we send a delete signal,
        # there better not be any duplicates remaining on the frontend.
        if not has_duplicates:
            self.mct.archive(model_instance.contact, model_instance)

    def _update_contact(self, contact):
        """Mark the contact as modified."""
        contact.modified_by_user = datetime.now()
        contact.save()

    def _get_field_model(self, field):
        """Get the model associated with a field's serializer. E.g. Address."""
        if hasattr(field, 'child'):
            serializer_class = field.child.__class__
            return serializer_class.Meta.model
        elif hasattr(field, '_field_model'):
            return field._field_model

    def _get_field_model_instance(self, instance_id, field):
        """Get an instance of a field's model."""
        field_model = self._get_field_model(field)
        model_instance = field_model.objects.select_related(
                                "contact__parent").get(id=instance_id)
        # We need to make sure this actually belongs to the contact being
        # referenced in the API.
        try:
            assert (self.instance in (model_instance.contact,
                                      model_instance.contact.parent))
        except AssertionError as e:
            LOGGER.exception("No exception message supplied.  The "
                             "self.instance is {}, the contact is {}, "
                             "contact.parent is {} and "
                             "self.instance contact_type is {},"
                             "model_instance.contact.contact_type is {}"
                             "model_instance.contact.parent.contact_type is {}".
                             format(self.instance, model_instance.contact,
                                    model_instance.contact.parent,
                                    self.instance.contact_type,
                                    model_instance.contact.contact_type,
                                    model_instance.contact.parent.contact_type
                                    if model_instance.contact.parent
                                    else None))
            raise
        else:
            return model_instance

    def _get_updated_fields(self, instance, validated_data):
        """Find any fields (Address, Email, Phone, etc) that were modified.

        This includes any items that were added or deleted, as well.
        """
        # Extract the current contact as it exists before update. We will use
        # it for comparison
        instance_data = self.__class__(instance=instance,
                                       context={"show_duplicates": True}).data
        updated_fields = []

        # Iterate over the given validated data and find edited fields.
        # We will check for changes to update here.
        for field_name, field_data in validated_data.items():
            field = self.fields.get(field_name)
            if field_name == "locations":
                # The user isn't allowed to edit locations
                continue
            elif field_name in ["dob", "gender"]:
                if instance_data[field_name] not in field_data.values():
                    action = "modify"
                    updated_fields.append((field_data, field, action))
            elif isinstance(field_data, list):
                instance_field_data = instance_data[field_name]
                # Check for deletions. We will do that by looking for
                # missing IDs in the submitted data.
                field_data_ids = {int(fd.get("id")) for fd in field_data
                                  if fd.get("id")}
                for item in instance_field_data:
                    item_id = item.get("id")
                    if item_id not in field_data_ids:
                        updated_fields.append((item_id, field, "delete"))

                # Prepare the submitted data and the existing instance data
                # to be compared with each other.
                field_data = self._prepare_field_for_comparison(field_data)
                instance_field_data = self._prepare_field_for_comparison(
                    instance_field_data)

                # Search for new or modified addresses
                for item in field_data:
                    # If the fields differ from any of the contact's existing
                    # fields, we mark it as being
                    if item not in instance_field_data:
                        item = dict(item)
                        action = "update" if item.get('id') else "create"
                        updated_fields.append((item, field, action))
            else:
                # For now, we are not modifying any Contact fields. These
                # typically would be the name fields. They are currently
                # immutable due to limitations with merging.
                continue
        return updated_fields

    def _prepare_field_for_comparison(self, field):
        def generator(item):
            # Clean up IDs so they are not comparing strings to ints
            for key, value in item.items():
                if key == "id" and value:
                    value = int(value)
                yield key, value

        if isinstance(field, list):
            # Extract out the key-value pairs and store them as sorted tuples,
            # so we can effectively compare the two dicts and not worry about
            # ordering issues.
            field = list(sorted(generator(item)) for item in field)
        return field

    @instance_cache
    def _get_or_create_manual_entry(self):
        if not self.instance:
            # Use `_create_manual_entry` for 'create' calls.
            raise AttributeError("This method should only be used for updates")

        manual = None
        if self.instance.contact_type == "manual":
            manual = self.instance
        else:
            for child in self.instance.get_contacts():
                if child.contact_type == "manual":
                    manual = child
                    break

        # If we managed to find an existing manual contact, just return it.
        # Otherwise, we have to create one
        if manual:
            return manual, False
        else:
            # If a contact instance already exists, we'll use that to
            # seed the new "manual" contact
            validated_data = dict(
                first_name=self.instance.first_name,
                last_name=self.instance.last_name,
                additional_name=self.instance.additional_name)
            is_primary = False

            manual = self._create_manual_contact(validated_data, is_primary)
            # We need to create a "merged" contact, and make
            # 'instance' and 'manual' children of it
            set_parent_children(self.instance.parent or self.instance,
                                children=[manual])
            return manual, True

    def _create_manual_contact(self, validated_data, is_primary):
        manual = ContactSerializer(context=self._context).create(
            validated_data=validated_data, is_primary=is_primary)
        self.mct.create(manual, manual)
        return manual


class ContactAttrContainer(object):
    """Helper class for the DetailedContact serializer above.

    This will store all of one attribute into a list. E.g. all 'addresses'
    from every contact associated with the same parent. The attributes
    will not be de-duplicated.
    """
    container_type = list

    def __init__(self, field):
        self.field = field
        self._container = self.container_type()

    def extract(self, contact):
        data = contact[self.field]
        if not data:
            return
        if not isinstance(data, (list, tuple)):
            data = [data]
        self._container.extend(data)

    @property
    def values(self):
        return self._container


class DedupedContactAttrContainer(ContactAttrContainer):
    """Helper class for the DetailedContact serializer above.

    This will store all of one attribute into a list. E.g. all 'addresses'
    from every contact associated with the same parent. This class will
    attempt to de-duplicate the data
    """
    container_type = dict

    def extract(self, contact):
        """Extract the field, hash the value, and store it.
        If the hash has already been seen, that is a duplicate, so we wouldn't
        serialize the duplicate.
        """
        data = contact[self.field]
        if not data:
            return

        if not isinstance(data, (list, tuple)):
            data = [data]
        for value in data:
            temp = value.copy()
            try:
                # IDs give the hash false-negative on duplicates.
                del temp["id"]
            except KeyError: pass
            self._container[deep_hash(temp)] = value

    @property
    def values(self):
        return list(self._container.values())
