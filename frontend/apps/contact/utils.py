from abc import ABCMeta, abstractmethod, abstractproperty
from itertools import chain, repeat
import re

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
import phonenumbers
from rest_framework.exceptions import PermissionDenied

from frontend.libs.utils.string_utils import str_to_bool


class PhoneNumberMixin(object):
    @property
    def national(self):
        """(555) 555-5555"""
        return self.format_number(self.number,
                                  phonenumbers.PhoneNumberFormat.NATIONAL)

    @property
    def e164(self):
        """+15555555555"""
        return self.format_number(self.number,
                                  phonenumbers.PhoneNumberFormat.E164)

    @property
    def rfc3966(self):
        """tel:+1-510-555-5555;ext=1234"""
        return self.format_number(self.number,
                                  phonenumbers.PhoneNumberFormat.RFC3966)

    @classmethod
    def _parse_number(cls, number):
        # Remove unnecessary junk from the number (white spaces, dashes, etc)
        number = re.sub(r"[^\d;x+]", "", number)
        # Make extensions parseable.
        number = re.sub(r"(\d+)[;x]+(\d+)", r"\g<1>;ext=\g<2>", number)
        # If the number is long, add a "+" so the country code can be guessed.
        if len(number.split(";ext=")[0]) > 10 and not number.startswith("+"):
            number = "+" + number
        return phonenumbers.parse(number, "US")

    @classmethod
    def format_number(cls, number, format_):
        try:
            num = cls._parse_number(number)
            return phonenumbers.format_number(num, format_)
        except phonenumbers.NumberParseException:
            return ""


def check_account_level_import(request):
    account_contacts = str_to_bool(request.GET.get('account_contacts')) or \
                       str_to_bool(request.POST.get('account_contacts'))
    seat = request.user.current_seat
    if (account_contacts and
            seat.AdminPermissions.MODIFY_CONTACTS not in seat.permissions):
        raise PermissionDenied(
            "You do not have permission to upload account-level contacts")
    return seat.account if account_contacts else None


def check_account_level_import_social(strategy, user):
    """Checks whether the import task should upload contacts to the account or
    retain them as personal account.
    Return account if the import type is shared otherwise return None.
    """
    account_contacts = str_to_bool(strategy.session_get('account_contacts'))
    if not account_contacts:
        return None
    seat = user.current_seat
    if (account_contacts and
            seat.AdminPermissions.MODIFY_CONTACTS not in seat.permissions):
        raise PermissionDenied(
            "You do not have permission to upload account-level contacts")
    return seat.account


class ContactSourceMixin(models.Model):
    class Meta:
        abstract = True

    # This GenericForeignKey allows to track the contacts that will be
    # analyzed. This field should be occupied by a ContactSet or
    # a single Contact. None is also okay, which will analyze all contacts.
    _csource_content_type = models.ForeignKey(
        ContentType, null=True, blank=True, on_delete=models.CASCADE)
    _csource_id = models.PositiveIntegerField(blank=True, null=True)
    contact_source = GenericForeignKey('_csource_content_type', '_csource_id')


class KafkaMixin(object):
    def format_for_kafka(self):
        """Override in subclass. MUST return unicode"""
        raise NotImplemented

    @classmethod
    def kafka_key(cls):
        return str(cls.__name__)


class CSVExportMixin:
    __metaclass__ = ABCMeta

    @abstractproperty
    def csv_fields(self):
        pass

    @abstractproperty
    def fields_to_iter(self):
        pass

    @classmethod
    def format_for_csv(cls, contact):
        """Iter through all unique values for the given contact
           and format for a CSV
        """
        return list(zip(cls.csv_fields, chain.from_iterable(
            cls.iter_values_for_contact(contact))))

    @classmethod
    def iter_values_for_contact(cls, contact):
        """Iter through all unique values for the given contact."""
        contacts = contact.get_contacts()
        values = cls.objects.filter(
            contact__in=contacts, archived=None
        ).exclude(**dict(zip(cls.fields_to_iter, repeat('')))).values_list(
            *cls.fields_to_iter).distinct(*cls.fields_to_iter)
        for value in values.iterator():
            yield value
