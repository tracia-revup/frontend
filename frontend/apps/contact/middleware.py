from django.shortcuts import render
from social_core import exceptions as social_exceptions
from social_django.middleware import SocialAuthExceptionMiddleware


class CustomSocialAuthExceptionMiddleware(SocialAuthExceptionMiddleware):
    def process_exception(self, request, exception):
        # Gracefully handle when a user selects "Deny" in oauth
        if isinstance(exception, social_exceptions.AuthCanceled):
            return render(request, "core/oauth_finished.html")
