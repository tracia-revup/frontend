from collections import OrderedDict, Counter
from enum import Enum

from django.db import transaction
from django.db.models import QuerySet, Q, Count

from frontend.apps.analysis.utils import ranking_counts_by_account
from frontend.apps.call_time.models import (
    CallTimeContact, CallTimeContactSet, CallLog)
from frontend.apps.campaign.models import Prospect, Account
from frontend.apps.contact.kafka import ContactDeleteTransaction
from frontend.apps.contact.models import ContactNotes, Contact, LimitTracker
from frontend.apps.filtering.models import ContactIndex
from frontend.libs.utils.celery_utils import (
    update_progress as _update_progress, calculate_step_size)


class OwnerType(Enum):
    account = "a"
    user = "u"


class DeleterBase(object):
    def _dryrun_data(self, counts, dryrun_plan):
        data = OrderedDict()
        for i, count in enumerate(counts):
            data[dryrun_plan[i][0]] = count
        return data

    def dryrun(self, objects, dryrun_plan=None):
        if not dryrun_plan:
            dryrun_plan = self.dryrun_plan
        counts = []
        for label, model, filter_ in dryrun_plan:
            counts.append(model.objects.filter(**{filter_: objects}).count())
        return self._dryrun_data(counts, dryrun_plan)

    def delete(self, to_delete):
        # Run the deletion plan
        results = Counter()
        for model, filter_ in self.deletion_plan.items():
            _, result = model.objects.filter(**{filter_: to_delete}).delete()
            results.update(result)
        return results

    def _construct_progress_updater(self, progress_start, progress_end):
        progress_multiplier = calculate_step_size(progress_start, progress_end)

        def updater(progress):
            if not self.update_progress:
                return
            progress = (progress * progress_multiplier) + progress_start
            _update_progress(progress, 100)
        return updater


class ContactDeleter(DeleterBase):
    # The deletion plan is for deletes that Django ORM does not automatically
    # do. Or does incorrectly.
    deletion_plan = OrderedDict((
        (CallLog, "ct_contact__contact__in"),
        (ContactIndex, "contact__in"),
    ))

    dryrun_plan = tuple((
        ("Prospects", Prospect, "contact__in"),
        ("Call Time Contacts", CallTimeContact, "contact__in"),
        ("Call Groups", CallTimeContactSet.contacts.through,
                        "calltimecontact__contact__in"),
        ("Call Logs", CallLog, "ct_contact__contact__in"),
        ("Notes", ContactNotes, "contact__in"),
    ))
    account_dryrun_plan = dryrun_plan[1:]
    user_dryrun_plan = dryrun_plan[0:1] + dryrun_plan[-1:]

    def __init__(self, user, delete_children=True, clean_parents=False,
                 update_progress=False):
        """
        :param delete_children: if the given contacts have children, should
                               the children also be deleted
        :param clean_parents: look for parents of 'contacts' and remove parents
                              that will either be childless or have only
                              'merged'-type children
        """
        self.user = user
        self.delete_children = delete_children
        self.clean_parents = clean_parents
        self.update_progress = update_progress

    def _get_contact_ids(self, contacts):
        if isinstance(contacts, Contact):
            contacts = [contacts.id]
        elif isinstance(contacts, QuerySet):
            contacts = list(contacts.values_list("id", flat=True))
        else:
            contacts = [c.id if isinstance(c, Contact) else c
                        for c in contacts]
        return contacts

    def _get_contacts(self, contacts):
        contacts = self._get_contact_ids(contacts)

        # The contacts should all be from the same user
        assert not len(Contact.objects.filter(id__in=contacts)
                       .values("user").annotate(Count("user"))
                       .values("user__count")) > 1

        q = Q(id__in=contacts)
        # This ensures we capture the children of any merged parents
        if self.delete_children:
            q |= Q(parent_id__in=contacts)
        return Contact.objects.filter(q)

    def dryrun(self, contacts, owner_type=None):
        # Choose the dryrun plan
        plan = None
        if owner_type:
            if owner_type is OwnerType.account:
                plan = self.account_dryrun_plan
            elif owner_type is OwnerType.user:
                plan = self.user_dryrun_plan

        contacts = self._get_contact_ids(contacts)
        data = OrderedDict((("Contacts", 0),))
        # If there are no contacts, let's not waste our time with empty queries
        if not contacts:
            data.update(self._dryrun_data([0] * len(plan), plan))
            return data

        # Only count the primary contacts
        # data["Contacts"] = self._get_contacts(contacts).exclude(
        #     contact_type="merged").count()
        # Although less accurate, it's less confusing to the user if we just
        # return the requested number of deleted contacts
        data["Contacts"] = len(contacts)

        q = Q(id__in=contacts, is_primary=True)
        # This ensures we capture the children of any merged parents
        if self.delete_children:
            q |= Q(parent_id__in=contacts)
        contacts = Contact.objects.filter(q)
        data.update(
            super(ContactDeleter, self).dryrun(contacts, dryrun_plan=plan))
        return data

    def has_relevant_data(self, contacts):
        """Check if deleting these contacts would cause other important data
        to be deleted.
        """
        data = self.dryrun(contacts)
        # We don't want to look at Contact count in this case
        data.pop("Contacts")
        for value in data.values():
            if value > 0:
                return True
        return False

    def _record_counts(self, contacts):
        """Track the number of contacts that are about to be deleted"""
        if not contacts:
            return
        counts = dict(ranking_counts_by_account((c.id for c in contacts)))
        if not counts:
            return
        trackers = LimitTracker.get_trackers(
            self.user, *Account.objects.filter(id__in=counts.keys()),
            select_for_update=True)
        for tracker in trackers:
            tracker.deleted_count += counts[tracker.account_id]
            tracker.save()

    def _delete(self, contacts, progress_updater, pipeline, lock=None):
        """Helper/Worker method that does most of the deletion work.

         This is so we can recursively call the delete functionality below.
        """
        contacts = self._get_contacts(contacts)
        if not contacts:
            return {}, []

        for contact in contacts:
            if contact.contact_type != "merged":
                pipeline.delete(contact)
        progress_updater(30)

        with transaction.atomic():
            ## Prefetch the parents for later cleanup
            # We need to query now because lazy resolution won't work after we
            # delete the given contacts
            parent_ids = list(contacts.filter(contact_type__ne="merged")
                              .exclude(parent=None)
                              .values_list("parent_id", flat=True))

            ## Record import deletion counts
            self._record_counts(contacts)

            ## Run the deletion plan
            results = super(ContactDeleter, self).delete(contacts)
            ## Delete the given contacts
            results.update(contacts.delete()[1])
            progress_updater(50 if self.clean_parents else 80)

            if self.clean_parents:
                # Check for any parent contacts without children
                empty_parents = (Contact.objects.filter(id__in=parent_ids)
                                                .annotate(Count("contacts"))
                                                .filter(contacts__count=0))

                # Lock the user to avoid a race condition with parallel tasks.
                # The race condition occurs in the reindexing code below, where
                # a parent may be deleted in a separate task while the indexes
                # are being generated
                if not lock:
                    lock = self.user.lock()

                ## Delete empty parents
                res, _ = ContactDeleter(
                    self.user, delete_children=False, clean_parents=False,
                    update_progress=False)._delete(
                        empty_parents, progress_updater, pipeline, lock=lock)
                results.update(res)
                progress_updater(65)

                ## Delete parents with only merged-type contacts as children
                # Find the parents that do have children that aren't merged
                normal_parent_ids = (
                    Contact.objects.filter(parent_id__in=parent_ids,
                                           contact_type__ne='merged')
                        .distinct('parent_id')
                        .values_list('parent_id', flat=True))
                # Eliminate the normal parents from all parents to find the
                # parents that only have merged-type children
                merged_only_parents = \
                    set(parent_ids).difference(normal_parent_ids)
                # Delete merge-type children contacts and the parents
                res, _ = ContactDeleter(self.user, delete_children=True,
                                        clean_parents=False,
                                        update_progress=False)._delete(
                    Contact.objects.filter(id__in=merged_only_parents),
                    progress_updater, pipeline, lock=lock)
                results.update(res)
                # Update parent_ids because everything else has been deleted
                parent_ids = normal_parent_ids
                progress_updater(80)
        return dict(results), parent_ids

    def delete(self, contacts, progress_start=0, progress_end=100):
        """Delete the given contacts and any associated objects.

        :param contacts: an iterable or Queryset of contacts to be deleted
        :param progress_start/end: If we are tracking progress, the progress
                            range is configurable.
        :returns: a Django-style report of all deleted objects
        """
        if not contacts:
            return {}

        progress_updater = self._construct_progress_updater(progress_start,
                                                            progress_end)
        progress_updater(1)
        # Prepare the Kafka pipeline
        pipeline = ContactDeleteTransaction()

        # Do the deletion
        results, leftover_parents = self._delete(contacts, progress_updater,
                                                 pipeline)

        ## Reindex any parents that still have children
        with transaction.atomic():
            # Lock the user to avoid a race condition with parallel tasks.
            self.user.lock()
            parents = Contact.objects.filter(id__in=leftover_parents)
            if parents:
                ContactIndex.bulk_update_index(
                    parents, parents[0].user.get_index_filters())
        progress_updater(90)

        # Commit the deletes to the Kafka pipeline
        pipeline.commit()

        progress_updater(100)
        return dict(results)
