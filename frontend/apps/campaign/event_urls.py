from django.conf.urls import url

from .views import EventView, event_default, event_list


urlpatterns = [
    url(r'^/(?P<event_id>[^/]+)/$', EventView.as_view(), name='event_detail'),
    url(r'^$', event_list, name='event_list'),
    url(r'^/$', event_default, name='event_default'),
]
