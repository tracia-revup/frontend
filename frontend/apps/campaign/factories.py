from datetime import datetime

from django.contrib.auth.models import Group
import factory.fuzzy

from frontend.apps.authorize.factories import RevUpUserFactory
from frontend.apps.campaign.models import (Event, Prospect, ProspectCollision,
                                           Product, Account, AccountProfile,
                                           AccountAnalysisConfigLink)
from frontend.apps.role.factories import RoleFactory
from frontend.apps.analysis.factories import (
    AnalysisProfileFactory, AnalysisConfigFactory)


@factory.use_strategy(factory.CREATE_STRATEGY)
class ProductFactory(factory.DjangoModelFactory):
    class Meta:
        model = Product

    title = factory.fuzzy.FuzzyText()
    recurly_plan_code = "test"
    default_seat_count = 10


@factory.use_strategy(factory.CREATE_STRATEGY)
class AccountProfileFactory(factory.DjangoModelFactory):
    class Meta:
        model = AccountProfile

    title = factory.fuzzy.FuzzyText()
    account_type = AccountProfile.AccountType.POLITICAL
    product = factory.SubFactory(ProductFactory)

    @factory.post_generation
    def default_analysis_profiles(self, create, extracted, **kwargs):
        if not create:
            return
        self.default_analysis_profiles.add(AnalysisProfileFactory())


@factory.use_strategy(factory.CREATE_STRATEGY)
class AccountFactory(factory.DjangoModelFactory):
    class Meta:
        model = Account

    account_head = factory.SubFactory(RevUpUserFactory)
    account_user = factory.SubFactory(RevUpUserFactory)
    account_group = factory.LazyAttribute(
        lambda x: Group.objects.create(name=x.organization_name))
    organization_name = factory.fuzzy.FuzzyText()
    start_date = datetime.now().date()
    seat_count_override = -1
    active = True
    account_profile = factory.SubFactory(AccountProfileFactory)

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        account = super(AccountFactory, cls)._create(model_class,
                                                     *args, **kwargs)
        if not account.roles.exists():
            RoleFactory.create(account=account, is_a_seat_default=True)
        else:
            role = account.roles.first()
            role.is_a_seat_default = True
            role.save()
        return account

    @factory.post_generation
    def build_analysis_config(self, create, extracted, **kwargs):
        if not create:
            return
        try:
            profile = self.account_profile.default_analysis_profiles.first()
            config_template = profile.default_config_template
            analysis_config, _ = AnalysisConfigFactory.objects.get_or_create(
                analysis_config_template=config_template, account=self)
            AccountAnalysisConfigLink.objects.get_or_create(
                analysis_config=analysis_config,
                account=self, is_default=True)
        except AttributeError:
            pass


@factory.use_strategy(factory.CREATE_STRATEGY)
class PoliticalCampaignFactory(AccountFactory):
    class Meta:
        model = Account

    account_profile = factory.SubFactory(
        AccountProfileFactory,
        account_type=AccountProfile.AccountType.POLITICAL)


@factory.use_strategy(factory.CREATE_STRATEGY)
class NonprofitCampaignFactory(AccountFactory):
    class Meta:
        model = Account

    account_profile = factory.SubFactory(
        AccountProfileFactory,
        account_type=AccountProfile.AccountType.NONPROFIT)


@factory.use_strategy(factory.CREATE_STRATEGY)
class UniversityFactory(AccountFactory):
    class Meta:
        model = Account

    account_profile = factory.SubFactory(
        AccountProfileFactory,
        account_type=AccountProfile.AccountType.ACADEMIC)


@factory.use_strategy(factory.CREATE_STRATEGY)
class EventFactory(factory.DjangoModelFactory):
    class Meta:
        model = Event

    event_name = "Test Event"
    event_date = factory.LazyAttribute(lambda x: datetime.utcnow())
    goal = 50000
    price_points = [1000, 2600, 5200]
    account = factory.SubFactory(PoliticalCampaignFactory)


@factory.use_strategy(factory.CREATE_STRATEGY)
class ProspectFactory(factory.DjangoModelFactory):
    class Meta:
        model = Prospect

    user = factory.SubFactory("frontend.apps.authorize.factories"
                              ".RevUpUserFactory")
    contact = factory.SubFactory("frontend.apps.contact.factories"
                                 ".ContactFactory",
                                 user=factory.SelfAttribute('..user'))
    event = factory.SubFactory("frontend.apps.campaign.factories.EventFactory")
    soft_amt = factory.fuzzy.FuzzyDecimal(low=0.0, high=100)
    hard_amt = factory.fuzzy.FuzzyDecimal(low=0.0, high=100)
    in_amt = factory.fuzzy.FuzzyDecimal(low=0.0, high=100)
    notes = factory.fuzzy.FuzzyText()


@factory.use_strategy(factory.CREATE_STRATEGY)
class ProspectCollisionFactory(factory.DjangoModelFactory):
    class Meta:
        model = ProspectCollision

    external_prospect = factory.SubFactory(ProspectFactory)
    event = factory.LazyAttribute(lambda x: x.external_prospect.event)
    user = factory.SubFactory("frontend.apps.authorize.factories"
                              ".RevUpUserFactory")
    contact = factory.SubFactory("frontend.apps.contact.factories"
                                 ".ContactFactory",
                                 user=factory.SelfAttribute('..user'))
