import re

from django import template
from django.utils.encoding import force_text

register = template.Library()

@register.filter(is_safe=True)
def fuzzyintcomma(value):
    """Add commas to all numbers in a string, as per humanize.intcomma"""
    orig = force_text(value)
    new = re.sub(r'(^|[^.\d])(\d+)(\d{3})', r'\1\2,\3', orig)
    return new if orig == new else fuzzyintcomma(new)
