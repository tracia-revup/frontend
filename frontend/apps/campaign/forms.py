from django import forms


class ProspectForm(forms.Form):
    attendee = forms.CharField(widget=forms.HiddenInput())
    inviter = forms.CharField(widget=forms.HiddenInput())
    contact_id = forms.CharField(widget=forms.HiddenInput())
    full_name = forms.CharField(widget=forms.HiddenInput())
    soft_amt = forms.DecimalField(
        widget=forms.TextInput(attrs={'class': 'text-right dollarNumField softAmt'}),
        decimal_places=2,
        min_value=0.0,
        required=False)
    hard_amt = forms.DecimalField(
        widget=forms.TextInput(attrs={'class': 'text-right dollarNumField hardAmt'}),
        decimal_places=2,
        min_value=0.0,
        required=False)
    in_amt = forms.DecimalField(
        widget=forms.TextInput(attrs={'class': 'text-right dollarNumField inAmt'}),
        decimal_places=2,
        min_value=0.0,
        required=False)
    num_attending = forms.IntegerField(
        widget=forms.TextInput(attrs={'class': 'text-right intNumField numAttending', 'maxlength': 10}),
        min_value=0,
        required=False)
    notes = forms.CharField(required=False)

    permission_editable_fields = {
        0: tuple(),
        1: ('soft_amt', 'hard_amt', 'num_attending',
            'notes'),
        2: ('in_amt',)
    }

    def __init__(self, *args, **kwargs):
        # The view may want to disable certain Prospects from being editable.
        # Different permission levels will have different fields available to
        # them. E.g. A prospect from another user will have no permissions.
        if "initial" in kwargs:
            permission_level = kwargs['initial'].pop('permission_level', 1)
        else:
            permission_level = 1
        super(ProspectForm, self).__init__(*args, **kwargs)

        # A certain permission level is required to delete this prospect
        self.deletable = True if permission_level >= 1 else False

        # Make all non-editable fields readonly.
        # Note: there are no adverse effect from making hidden fields read only
        editable_fields = self.get_editable_fields(permission_level)
        for field in (set(self.fields.keys()) - editable_fields):
            self.fields[field].widget.attrs['readonly'] = True

    def _clean_number(self, field):
        data = self.cleaned_data.get(field)
        return data if data else 0

    def clean_soft_amt(self):
        return self._clean_number('soft_amt')

    def clean_hard_amt(self):
        return self._clean_number('hard_amt')

    def clean_in_amt(self):
        return self._clean_number('in_amt')

    def clean_num_attending(self):
        return self._clean_number('num_attending')

    @classmethod
    def get_editable_fields(cls, permission_level):
        """Build a set of fields that are editable, based on permission level.
        """
        editable_fields = set()
        while permission_level >= 0:
            editable_fields.update(
                cls.permission_editable_fields[permission_level])
            permission_level -= 1
        return editable_fields
