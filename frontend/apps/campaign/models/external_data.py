"""This module provides models related to campaigns' external data sources
(e.g. NGP, CMDI, etc.) configured for use in analyses.

"""
from django.db import models

from frontend.libs.utils.model_utils import Choices


class ExternalData(models.Model):
    """An external data set of a client campaign.

    Campaigns can configured external data sets for use in analyses. For
    example, a contribution report from NGP or CMDI, or a custom data set
    such as a CSV file containing donation records.

    A static_key can be specified if the external data will use a key that
    does not change. For example, the name of a test data collection in
    MongoDB.

    """
    account = models.ForeignKey('campaign.Account', on_delete=models.CASCADE, null=False,
                                related_name='external_entries')
    config = models.ForeignKey('ExternalDataConfig', on_delete=models.CASCADE, null=False,
                               related_name='+')
    name = models.CharField(max_length=64, blank=True)
    notes = models.TextField()
    static_key = models.CharField(max_length=128, blank=True)
    active = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def external_key(self):
        """Key for use in identifying this instance in an external system.

        Returns:

          If a static_key is defined, the static_key, otherwise a dynamic key:

          A string key comprised of the prefix 'account', the account id,
          and the data set id
        """
        if self.static_key:
            return self.static_key
        else:
            return '_'.join(('account', str(self.account.id), str(self.id)))


class ExternalDataConfig(models.Model):
    """A configuration of an external data set.

    Every data source will have a configuration, containing any details for
    the processing and management of the data. A configuration can belong to
    a specific account, or can be system-wide. Configurations may be re-used
    for multiple data sets. Account-specific configurations may only be
    used by the owning Account.

    """
    account = models.ForeignKey('campaign.Account', on_delete=models.CASCADE, null=True,
                                related_name='data_configs')
    source = models.ForeignKey('ExternalDataSource', on_delete=models.CASCADE, null=False,
                               related_name='+')
    name = models.CharField(max_length=64, blank=True)
    notes = models.TextField()
    active = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


class ExternalDataSource(models.Model):
    """A source of external data (i.e. system from which the data originates.)

    Every external data configuration will have a data source, containing any
    details about the data source as well as its default configuration, if any.

    Data sources may be affiliated with different varieties of the product, or
    with different political parties. This can be used to filter display of
    external data source options for clients.

    """

    name = models.CharField(max_length=64, blank=True)
    index = models.PositiveSmallIntegerField()
    default_config = models.OneToOneField('ExternalDataConfig', null=True,
                                          related_name='default_config',
                                          on_delete=models.CASCADE)

    class Affiliations(Choices):
        """Map of political affiliation choices"""
        choices_map = [
            ("D", "DEMOCRATIC", "Democratic"),
            ("R", "REPUBLICAN", "Republican"),
            ("", "NONE", ""),
        ]

    class Products(Choices):
        """Map of product type choices"""
        choices_map = [
            ("P", "POLITICAL", "Political"),
            ("A", "ACADEMIC", "Academic"),
            ("N", "NONPROFIT", "Non-Profit"),
            ("", "NONE", ""),
        ]

    affiliation = models.CharField(max_length=2,
                                   choices=Affiliations.choices(),
                                   default=Affiliations.NONE,
                                   blank=True)
    product_type = models.CharField(max_length=2,
                                    choices=Products.choices(),
                                    default=Products.NONE,
                                    blank=True)
    active = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


class ExternalDataInstance(models.Model):
    """An instance of one of a client's external data sets.

    A client's configured external data sets may have multiple instances.
    Each instance corresponds to a specific addition of a data set, and records
    details about the processing and status of the added data set instance,
    as well as the original data itself, in the case of uploaded files.

    """
    original_data = models.BinaryField()
    config = models.ForeignKey('ExternalDataConfig', on_delete=models.CASCADE, null=False,
                               related_name='+')
    data_set = models.ForeignKey('ExternalData', on_delete=models.CASCADE, null=False,
                                 related_name='instances')
    external_entry = models.ForeignKey('ExternalData', on_delete=models.CASCADE, null=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    file_name = models.CharField(max_length=128, blank=True)

    class States(Choices):
        """Map of external data instance states"""
        choices_map = [
            ("UPLOAD", "UPLOADED", "Uploaded"),
            ("PROCESS", "IN_PROCESS", "In Process"),
            ("ERROR", "ERROR", "Error"),
            ("READY", "READY", "Ready"),
            ("ARCH", "ARCHIVED", "Archived"),
            ("DEL", "DELETED", "Deleted"),
        ]
    status = models.CharField(max_length=16,
                              choices=States.choices(),
                              default=States.UPLOADED,
                              blank=True)