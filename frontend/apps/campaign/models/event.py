from django.db import models

from frontend.libs.mixins import SimpleUnicodeMixin
from django.contrib.postgres.fields import ArrayField


class Event(models.Model, SimpleUnicodeMixin):
    """Fundraising event class"""
    event_name = models.CharField(max_length=100,
                                  verbose_name='fundraising event name')
    event_date = models.DateTimeField(verbose_name='fundraising event date')
    goal = models.PositiveIntegerField(
        default=0, blank=True, verbose_name='fundraising event goal ($)',)
    price_points = ArrayField(models.PositiveIntegerField(
                              verbose_name='fundraising price point ($)'),
                              default=[], blank=True,
                              verbose_name='fundraising price points ($)')
    account = models.ForeignKey('campaign.Account', on_delete=models.CASCADE)


class ExternalLink(models.Model, SimpleUnicodeMixin):
    SYSTEM_CHOICES = (('NGP', "NGP"),)
    event = models.ForeignKey("Event", on_delete=models.CASCADE,
                              related_name="external_links")
    link_key = models.CharField(max_length=512)
    system_name = models.CharField(max_length=3,
                                   choices=SYSTEM_CHOICES)


class Prospect(models.Model, SimpleUnicodeMixin):
    class Meta:
        unique_together = (("event", "contact"),)

    contact = models.ForeignKey('contact.Contact', on_delete=models.CASCADE)
    user = models.ForeignKey('authorize.RevUpUser', on_delete=models.CASCADE)
    event = models.ForeignKey('Event', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    soft_amt = models.DecimalField(decimal_places=2, default=0,
                                   blank=True, max_digits=12,
                                   verbose_name='soft contribution')
    hard_amt = models.DecimalField(decimal_places=2, default=0,
                                   blank=True, max_digits=12,
                                   verbose_name='hard contribution')
    in_amt = models.DecimalField(decimal_places=2, default=0,
                                 blank=True, max_digits=12,
                                 verbose_name='contributions in',)
    num_attending = models.PositiveIntegerField(
        verbose_name='number attending', default=0, blank=True)
    notes = models.TextField(blank=True,
                             verbose_name='Prospect notes')

    permission_editable_fields = {
        0: tuple(),
        1: ('soft_amt', 'hard_amt', 'num_attending',
            'notes', 'in_amt'),
        2: tuple()
    }

    @classmethod
    def get_editable_fields(cls, permission_level):
        """Build a set of fields that are editable, based on permission level.
        """
        editable_fields = set()
        while permission_level >= 0:
            editable_fields.update(
                cls.permission_editable_fields[permission_level])
            permission_level -= 1
        return editable_fields

    def get_amt_values(self):
        return (self.soft_amt, self.hard_amt, self.in_amt)

    def is_empty(self):
        return self.get_amt_values() == (0, 0, 0)


class ProspectCollision(models.Model, SimpleUnicodeMixin):
    """When a Prospect from one user collides with an Prospect of another
    user, an instance of this should be created.

    Prospect collisions would happen when an event has a host committee, and
    more than one user is attempting to invite the same contact.
    """
    class Meta:
        unique_together = (("event", "contact"),)

    external_prospect = models.ForeignKey(
        Prospect, on_delete=models.CASCADE,
        help_text="The Prospect that already existed when another contact of "
                  "the same person was attempted to be used as an Prospect.")
    contact = models.ForeignKey("contact.Contact", on_delete=models.CASCADE,
                                help_text="The attempted prospect that "
                                          "collided.")
    event = models.ForeignKey("Event", on_delete=models.CASCADE,
                              help_text="The Event these prospect belong to.")
    user = models.ForeignKey('authorize.RevUpUser', on_delete=models.CASCADE,
                             help_text="The user who attempted to add a "
                                       "colliding contact.")
