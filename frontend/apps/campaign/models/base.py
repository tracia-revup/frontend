import collections
from datetime import datetime, date
import logging
import sys

from django.conf import settings
from django.core import exceptions
from django.db import models, transaction, IntegrityError
from django.db.models.signals import post_save
from django.dispatch import receiver
from django_extensions.db.models import TimeStampedModel
from storages.backends.s3boto3 import S3Boto3Storage

from frontend.apps.analysis.models.analysis_configs import (
    AnalysisConfigModel, AnalysisConfig)
from frontend.apps.seat.models import Seat
from frontend.apps.ux.models import SkinSettings
from frontend.libs.utils.general_utils import instance_cache
from frontend.libs.utils.model_utils import (
    Choices, ModelDiffMixin, ChoiceArrayField, TruncatingCharField)
from frontend.libs.utils.date_utils import add_months, next_first_of_month
from frontend.libs.utils.recurly_utils import (create_account,
                                               get_recurly_account,
                                               create_subscription,
                                               get_subscription,
                                               list_subscription_plans)
from frontend.libs.utils.string_utils import (
    random_uuid, scrub_string, string)


LOGGER = logging.getLogger(__name__)
ACCOUNT_KEY_LENGTH = 24


class Product(AnalysisConfigModel):
    class Features(Choices):
        choices_map = [
            # Site Features - Tabs
            ("RNK", "RANKING", "Ranking"),
            ("CAL", "CALL_TIME", "Call Time"),
            ("LMN", "LIST_MANAGER", "List Manager"),
            ("PRS", "PROSPECTS", "Prospects"),
            ("RSR", "RAISERS", "Raisers"),
            ("CMN", "CONTACT_MANAGER", "Contact Manager"),
            ("MOB", "MOBILE_APP", "Mobile Application"),
            ("PSO", "PERSONAS", "Personas"),

            # Site Features - Individual
            # Call Sheet Exports
            ("CSE", "CALL_SHEET_EXPORT_RANKING", "Call Sheet Export - Ranking"),
            ("CEC", "CALL_SHEET_EXPORT_CALL_TIME", "Call Sheet Export - Call Time"),
            # CSV Exports
            ("CSV", "CSV_EXPORT_RANKING", "CSV Export - Ranking"),
            ("CVC", "CSV_EXPORT_CALL_TIME", "CSV Export - Call Time"),
            ("CVP", "CSV_EXPORT_PROSPECTS", "CSV Export - Prospects"),

            ## Contact Source Features
            # Imports
            ("PER", "PERSONAL_CONTACTS", "Personal Contacts"),
            ("ACT", "ACCOUNT_CONTACTS", "Account/Shared Contacts"),
            # Import Types
            ("ILI", "IMPORT_LINKEDIN", "Import: LinkedIn"),
            ("IGM", "IMPORT_GMAIL", "Import: Gmail"),
            ("IOL", "IMPORT_OUTLOOK", "Import: Outlook"),
            ("IVC", "IMPORT_VCARD", "Import: Vcard"),
            ("ICV", "IMPORT_CSV", "Import: CSV"),
            ("IMB", "IMPORT_MOBILE", "Import: Mobile"),
            ("IHF", "IMPORT_HOUSE_FILE", "Import: House File")
        ]

    class ProductType(Choices):
        choices_map = [
            ("G", "GLOBAL", "Global"),
            ("C", "CUSTOM", "Custom")
        ]

    enabled_features = ChoiceArrayField(
        base_field=models.CharField(choices=Features.choices(),
                                    max_length=3, blank=True),
        default=Features.all()
    )
    product_type = models.CharField(
        choices=ProductType.choices(), max_length=2, blank=True,
        default=ProductType.GLOBAL)
    recurly_plan_code = models.CharField(blank=True, max_length=50)

    default_analyze_limit = models.PositiveIntegerField(
        blank=False, default=100_000
    )
    default_create_limit = models.PositiveIntegerField(
        blank=False, default=300_000
    )
    default_seat_count = models.PositiveIntegerField(
        blank=False, default=10, null=True
    )
    contract_term_months = models.PositiveIntegerField(
        blank=False, default=12
    )
    manual_onboarding = models.BooleanField(blank=True, default=False)

    def __str__(self):
        return self.title

    @property
    @instance_cache
    def features(self):
        """Build a dict of all available features and whether they are enabled
        """
        enabled_features = set(self.enabled_features)
        features = {}
        for feature in self.Features.all():
            features[self.Features.attribute_name(feature)] = (
                    feature in enabled_features)
        return features

    @property
    @instance_cache
    def get_price(self):
        plans = list_subscription_plans()
        for p in plans:
            if p['plan_code'] == self.recurly_plan_code:
                return p['unit_amount_in_cents']/100
        return None


class AccountProfile(AnalysisConfigModel):
    class AccountType(Choices):
        """Map of account type choices"""
        choices_map = (
            ("P", "POLITICAL", "Political"),
            ("A", "ACADEMIC", "Academic"),
            ("N", "NONPROFIT", "Nonprofit"),
            ("", "NONE", ""),
        )

    class AccountSubType(Choices):
        """Map of account sub-type choices"""
        choices_map = (
            ("CAN", "CANDIDATE", "Candidate"),
            ("COM", "COMMITTEE", "Committee"),
            ("", "NONE", ""),
        )

    account_type = models.CharField(max_length=2,
                                    choices=AccountType.choices(),
                                    default=AccountType.NONE,
                                    blank=True)
    account_sub_type = models.CharField(max_length=3,
                                        choices=AccountSubType.choices(),
                                        default=AccountSubType.NONE,
                                        blank=True)
    default_analysis_profiles = models.ManyToManyField(
        "analysis.AnalysisProfile",
        blank=True, related_name='+')
    skins = models.ManyToManyField("ux.SkinSettings",
                                   through="AccountSkinSettingsLink",
                                   related_name='+', blank=True)
    product = models.ForeignKey(Product, on_delete=models.deletion.CASCADE)
    quick_filters = models.ManyToManyField('filtering.QuickFilter',
                                           blank=True,
                                           related_name='+')
    roles = models.ManyToManyField('role.Role', related_name='+', blank=True)

    def __str__(self):
        return self.title

    @property
    def product_type(self):
        """Returns the account-type and account sub-type as a single string"""
        full_account_type = "{0} {1}".format(
            self.get_account_type_display(),
            self.get_account_sub_type_display())
        return full_account_type.strip()


@receiver(models.signals.pre_save, sender=AccountProfile)
def verify_account_type(sender, instance, **kwargs):
    """Verifies that the fields `account_type`, `account_sub_type` and `title`
    are a valid combination.
    """
    # Verify that only political accounts have account_sub_type
    if instance.account_sub_type and \
            instance.account_type != AccountProfile.AccountType.POLITICAL:
        LOGGER.exception(f'Invalid account type: {instance.account_type} and '
                         f'account sub-type: {instance.account_sub_type} '
                         f'combination')
        instance.account_sub_type = ''

    # Verify that `account_sub_type` and `title` are a valid combination
    account_title_to_subtype_map = {
        'Political Candidate': AccountProfile.AccountSubType.CANDIDATE,
        'Political Committee': AccountProfile.AccountSubType.COMMITTEE
    }
    if instance.title in account_title_to_subtype_map and \
            instance.account_sub_type != account_title_to_subtype_map.get(
            instance.title):
        LOGGER.exception(f'Invalid account title: {instance.title} and '
                         f'account sub-type: {instance.account_sub_type} '
                         f'combination')
        instance.account_sub_type = account_title_to_subtype_map.get(
            instance.title)


class AccountSkinSettingsLink(models.Model):
    skin = models.ForeignKey("ux.SkinSettings", on_delete=models.CASCADE,
                             related_name='through_reference')
    account_profile = models.ForeignKey("AccountProfile", on_delete=models.CASCADE, related_name='+')
    is_default = models.BooleanField(default=False, blank=True)
    is_active = models.BooleanField(default=True, blank=True)

    class Meta:
        unique_together = (("skin", "account_profile"),)

    def save(self, *args, **kwargs):
        """Override the save method to ensure that if this is marked as
        default, then it is the only one marked as default in the
        account profile.

        These records are rarely updated, so this won't be an expensive
        operation.
        """
        super(AccountSkinSettingsLink, self).save(*args, **kwargs)
        if self.is_default:
            AccountSkinSettingsLink.objects.filter(
                account_profile=self.account_profile).exclude(
                id=self.id).update(is_default=False)


class AccountAnalysisConfigLink(models.Model):
    analysis_config = models.ForeignKey("analysis.AnalysisConfig", on_delete=models.CASCADE,
                                        related_name='+')
    account = models.ForeignKey("Account", on_delete=models.CASCADE, related_name='+')
    is_default = models.BooleanField(default=False, blank=True)
    is_active = models.BooleanField(default=True, blank=True)

    def make_default(self):
        """Make the linked analysis config the default."""
        with transaction.atomic():
            # If not owned, make all non-user-owned configs non-default
            if not self.user:
                self.objects.filter(
                    account=self.account,
                    user__is_null=True).update(is_default=False)
            # If owned, make all user-owned configs non-default
            else:
                self.objects.filter(
                    account=self.account,
                    user=self.user).update(is_default=False)
            # Make this one the default
            self.is_default = True
            self.save()


class DeactivationReason(Choices):
    choices_map = [
        ("FP", "FAILED_PAYMENT"),
        ("SE", "SUBSCRIPTION_EXPIRED"),
        ("CRA", "CLOSED_RECURLY_ACCOUNT"),
        ("", "NONE"),
    ]


class Account(ModelDiffMixin):
    """A representation for  with another organization.

    Could be a range of organizations/entities from political campaign to
    university.
    """

    account_head = models.ForeignKey("authorize.RevUpUser",
                                     on_delete=models.CASCADE, null=True)
    account_user = models.OneToOneField("authorize.RevUpUser",
                                        related_name="account_user",
                                        null=False, on_delete=models.CASCADE)
    account_group = models.OneToOneField("auth.Group",
                                         related_name="account_group",
                                         null=False, on_delete=models.CASCADE)
    active = models.BooleanField(blank=True, default=False)
    onboarding = models.BooleanField(
        blank=True, default=False,
        help_text="If True, the user will be blocked from accessing the site "
                  "until onboarding is finished. I.e. until this field is False")
    organization_name = models.CharField(blank=True, max_length=512)
    start_date = models.DateField()

    recurly_account_code = models.CharField(blank=True, max_length=32)
    recurly_subscription_code = models.CharField(blank=True, max_length=32)
    seat_count_override = models.IntegerField(
        blank=True, default=-1, null=True,
        help_text="This overrides the default set in the Product. The default "
                  "is -1 as a flag because None means 'unlimited'")
    analyze_limit_override = models.PositiveIntegerField(
        blank=True, default=None, null=True,
        help_text="This overrides the default set in the Product")
    create_limit_override = models.PositiveIntegerField(
        blank=True, default=None, null=True,
        help_text="This overrides the default set in the Product")

    preferred_domain = models.CharField(
        blank=True, max_length=256,
        help_text="The client's preferred domain. E.g.: 'purdue.revup.com'. "
                  "Do not include the 'http'")

    account_profile = models.ForeignKey("AccountProfile",
                                        on_delete=models.CASCADE,
                                        related_name='+')
    # analysis_configs are a set of available configs for this account. They
    # come from account_profile by default, but can be edited.
    analysis_configs = models.ManyToManyField(
        "analysis.AnalysisConfig",
        through="AccountAnalysisConfigLink", blank=True, related_name='+')

    current_skin = models.ForeignKey("ux.SkinSettings", blank=True, null=True,
                                     related_name='+',
                                     on_delete=models.SET_NULL)
    deactivation_reason = models.CharField(max_length=3,
                                           choices=DeactivationReason.choices(),
                                           default=DeactivationReason.NONE,
                                           null=True,
                                           blank=True)
    account_key = models.CharField(max_length=256, blank=True, null=True)
    associated_user_guide = "global"
    mdm_watch_fields = {"current_skin"}

    def __eq__(self, other):
        # If other is a subclass of Account, just compare their pks
        if isinstance(other, Account) and self.pk:
            return self.pk == other.pk
        else:
            return super(Account, self).__eq__(other)

    def __str__(self):
        return self.title

    @property
    def display_name(self):
        return self.title

    @property
    def title(self):
        return self.organization_name or ""

    @property
    @instance_cache
    def default_icon_text(self):
        """Generate the text for the client's analysis icon.
        Use the following rules:
        1) If title is one word, use it
        2) If title is multiple words, use the initials
        3) If no title, use default
        4) If any of the above are more than 6 characters, use the default
        """
        # Use the title property as the base for preparing the text icon
        title_tokens = self.title.strip().split(" ")
        if len(title_tokens) == 1:
            icon_text = title_tokens[0]
        elif len(title_tokens) > 1:
            # Get the initials, don't include numbers
            icon_text = "".join(t[0] for t in title_tokens
                                if t and not t.isdigit()).upper()
        else:
            icon_text = ""

        # Icon text must be more than 0 and less than 7 characters
        if 0 < len(icon_text) <= 6:
            return icon_text
        else:
            return "Client"

    def get_skins(self):
        """Query for all skins available to this Account."""
        q = models.Q(account=self)
        if self.account_profile:
            q |= models.Q(
                through_reference__account_profile=self.account_profile)
        return SkinSettings.objects.filter(q)

    @property
    def seat_count(self):
        if self.seat_count_override == -1:
            return self.account_profile.product.default_seat_count
        else:
            return self.seat_count_override

    @property
    def analyze_limit(self):
        if self.analyze_limit_override is not None:
            return self.analyze_limit_override
        else:
            return self.account_profile.product.default_analyze_limit

    @property
    def contract_renews(self):
        """Get a date for when the contract will automatically renew,
        based on the account start-date and term length
        """
        term_months = self.account_profile.product.contract_term_months
        term_renews = self.start_date
        today = date.today()
        # If the start-date + term length is still less than today, that means
        # the account has already renewed at least once before; keep going.
        while term_renews <= today:
            term_renews = add_months(term_renews, term_months)
        return term_renews

    @property
    def create_limit(self):
        if self.create_limit_override is not None:
            return self.create_limit_override
        else:
            return self.account_profile.product.default_create_limit

    def get_default_analysis_config(self):
        """Get the current default analysis config for this account."""
        configs = AccountAnalysisConfigLink.objects.filter(
            account=self, is_active=True).select_related("analysis_config")
        # Use the Accounts configs. Try to find the default, otherwise
        # use the first.
        if configs:
            default_config = configs[0]
            for config in configs:
                if config.is_default:
                    default_config = config
                    break
            return default_config.analysis_config
        else:
            # If this account has no analysis_configs, attempt to gather one
            # from the account profile.
            default_config_templates = [dap.default_config_template_id
               for dap in self.account_profile.default_analysis_profiles.all()]
            return AnalysisConfig.objects.filter(
                account=self,
                analysis_config_template__in=default_config_templates).first()

    def create_seat(self, user, permissions=None, roles=None,
                    inviting_user=None, **kwargs):
        """Create a seat for the user in this Account.

        If the user is the account chair/head, they should receive a
        "Permanent" Seat.
        """
        if user == self.account_head:
            # If the user is the account head, give them access to the
            # account admin and complimentary access to the app.
            seat_type = Seat.Types.PERMANENT
            state = Seat.States.ACTIVE
            permissions = (Seat.FundraiserPermissions.all() +
                           Seat.AdminPermissions.all())
            self.account_group.user_set.add(user)
        else:
            # If no permissions are defined, use the default (if it exists)
            if roles is None and permissions is None:
                roles = self.roles.filter(is_a_seat_default=True)
            seat_type = Seat.Types.TEMPORARY
            state = Seat.States.PENDING

        return Seat.create(user, self, permissions, roles,
                           state=state, seat_type=seat_type,
                           inviting_user=inviting_user, **kwargs)

    def active_seats(self):
        """Returns the number of total assigned seats.
        This includes Permanent seats that don't decrease the available seats.
        """
        return self.seats.assigned().count()

    def available_seats(self):
        """Get the count of available seats.

        Returns either a positiver integer, or 0. Will not return a negative
        number ever. The positive integer will be the number of available
        seats.
        """
        if self.seat_count is None:
            return None
        available_seats = self.seat_count - self.seats.filled().count()
        if available_seats < 0:
            available_seats = 0
        return available_seats

    def get_or_create_recurly_account(self, cc_token=None):
        # If no subscription plan is specified, we wont create an account
        if not self.account_profile.product.recurly_plan_code:
            raise AttributeError("Recurly Subscription plan code is required "
                                 "to create an Account.")

        account = getattr(self, "_account_cache", None)
        if account:
            return account

        # Create an account if there isn't one
        if not self.recurly_account_code:
            self.recurly_account_code = random_uuid()
            account = create_account(self, cc_token, self.account_head)
            self.save()
        else:
            account = get_recurly_account(self.recurly_account_code)
        # Cache the account in the object so we don't keep re-querying it.
        self._account_cache = account
        return account

    def get_or_create_recurly_subscription(self, cc_token=None, prorate=False):
        subscription = getattr(self, "_subscription_cache", None)
        if subscription:
            return subscription

        account = self.get_or_create_recurly_account(cc_token)
        if not self.recurly_subscription_code:
            prorate_date = next_first_of_month() if prorate else None
            subscription = create_subscription(self, account,
                                               prorate_date=prorate_date)
            self.recurly_subscription_code = subscription.uuid
            self.save()
        else:
            subscription = get_subscription(
                self.recurly_subscription_code)

        # Cache the subscription in the object so we don't keep re-querying it.
        self._subscription_cache = subscription
        return subscription

    def update_billing_info(self, cc_token):
        """Update recurly billing info to a new card"""
        assert cc_token
        if not self.recurly_account_code:
            raise AttributeError("'update_billing_info' requires an existing "
                                 "Recurly account")
        account = self.get_or_create_recurly_account()
        billing_info = account.billing_info
        billing_info.token_id = cc_token
        account.update_billing_info(billing_info)

    def is_member(self, user):
        """Returns True if user is member of account"""
        return self.seats.assigned().filter(user=user).exists()

    def save(self, *args, **kwargs):
        # If the 'current_skin' field was updated, let's validate it
        if self.has_changed:
            if self.current_skin and self.current_skin not in self.get_skins():
                raise exceptions.ValidationError(
                    "The selected skin is not available to this account")
        super(Account, self).save(*args, **kwargs)

    @classmethod
    def generate_hash(cls, account_title, account_profile):
        """Generate a hash on the account so that it is in the format
        <product-letter><4-char-uuid><squished-title><optional_chars>"""
        product_letter = account_profile.account_type
        temp_uuid = random_uuid()
        # Scrub every character from the account title except ascii letters
        # and digits
        keep_chars = string.ascii_letters + string.digits
        scrubbed_title = scrub_string(account_title, keep_chars=keep_chars)

        account_hash = product_letter + temp_uuid[:4] + scrubbed_title
        temp_hash_length = len(account_hash)

        # Fix the hash length to ACCOUNT_KEY_LENGTH
        if temp_hash_length >= ACCOUNT_KEY_LENGTH:
            account_hash = account_hash[:ACCOUNT_KEY_LENGTH]
        else:
            num_chars = ACCOUNT_KEY_LENGTH - temp_hash_length
            account_hash = account_hash + temp_uuid[-num_chars:]

        # Convert the account hash to upper case
        return account_hash.upper()

    @classmethod
    def create(cls, account_profile, organization_name, user,
               seat_count=-1, start_date=None, **kwargs):
        from frontend.apps.authorize.models import RevUpUser
        from frontend.apps.contact_set.models import ContactSet
        from django.contrib.auth.models import Group

        username = 'account_user_{}'.format(random_uuid())[:30]
        group_name = "Account Group {}".format(organization_name)[:80]

        def _create_group(group_name, attempts=30):
            name_stub = group_name[:74]
            for _ in range(attempts):
                with transaction.atomic():
                    try:
                        return Group.objects.create(name=group_name)
                    except IntegrityError:
                        group_name = '{}-{}'.format(name_stub,
                                                    random_uuid()[:5])
            raise IntegrityError(
                "Max retries: No unique group name could be found")

        # We don't want to override the seat count if it matches the product
        if seat_count == account_profile.product.default_seat_count:
            seat_count = -1

        # If any of these fail, we want it all to fail. The site requires each.
        with transaction.atomic():
            account_user = RevUpUser.objects.create(
                is_active=False, username=username, email=username)
            account_group = _create_group(group_name)
            account_user.groups.add(account_group)
            account_key = cls.generate_hash(organization_name, account_profile)

            account = cls.objects.create(
                account_head=user,
                account_user=account_user,
                account_group=account_group,
                account_key=account_key,
                organization_name=organization_name,
                start_date=start_date or datetime.now().date(),
                seat_count_override=seat_count,
                account_profile=account_profile,
                current_skin=account_profile.skins.get_default(),
                **kwargs
            )
            # Create the root ContactSet for the account user.
            ContactSet.get_or_create_root(account, account_user)
        return account

    CONTRIB_NAME_TMPL = 'client_contribs__{account_key}'

    @property
    def contrib_collection_name(self):
        return self.CONTRIB_NAME_TMPL.format(account_key=self.account_key)

    CLIENT_ENTITY_NAME_TMPL = 'client_entities__{account_key}'

    @property
    def client_entity_collection_name(self):
        return self.CLIENT_ENTITY_NAME_TMPL.format(
            account_key=self.account_key)

    def deactivate(self, deactivation_reason=DeactivationReason.NONE):
        """Deactivates the account"""
        self.active = False
        self.deactivation_reason = deactivation_reason
        self.save(update_fields=['active', 'deactivation_reason'])

    def reactivate(self):
        """Reactivates the account"""
        if not self.active:
            self.active = True
            self.deactivation_reason = DeactivationReason.NONE
            self.save(update_fields=['active', 'deactivation_reason'])

    def create_contrib_index(self):
        """Creates required index on client contrib collection"""
        if hasattr(sys, "_called_from_test"):
            # If the sys module has this property, we're being run in a unit
            # test and should not make changes to mongodb.
            return
        collection_name = self.contrib_collection_name
        collection = settings.MONGO_DB[collection_name]
        collection.create_index([('analysis_family_name', 1),
                                 ('analysis_given_name', 1)],
                                background=True)


@receiver(post_save, sender=Account)
def create_seat_for_head(sender, instance, created, **kwargs):
    """Create a special seat for the account head."""
    # Since this is inherited, we can't just use sender. We have to
    # call this signal on all saves and only continue if it is a subclass of
    # Account.
    if created and instance.account_head:
        instance.create_seat(instance.account_head)


@receiver(post_save, sender=Account)
def create_analysis_configs(sender, instance, created, **kwargs):
    if created and not instance.analysis_configs.exists():
        # Generate the default analysis configs for the new account.
        for dap in instance.account_profile.default_analysis_profiles.all():
            if not dap.default_config_template:
                continue
            dap.default_config_template.generate_analysis_config(
                instance, title=instance.organization_name,
                add_to_account=True)


@receiver(post_save, sender=Account)
def create_contrib_collection_indexes(sender, instance, created, **kwargs):
    """Create collection and indexes for client contribution collection."""
    if created:
        instance.create_contrib_index()


class AccountTask(models.Model):
    class TaskType(Choices):
        choices_map = [
            ("BFI", "BULK_IMPORT", "Bulk Fundraiser Import"),
        ]

    account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name="tasks")
    task_id = models.CharField(max_length=255)
    user = models.ForeignKey("authorize.RevUpUser", on_delete=models.CASCADE, related_name='+',
                             help_text="The user who started this task")
    task_type = models.CharField(
        max_length=3, db_index=True,
        choices=TaskType.choices(),
        default=TaskType.BULK_IMPORT)

    # TODO: If we make an API for this, extract common methods from
    #       RevUpTask and create abstract model to inherit from.


def _select_storage():
    if settings.ACCOUNT_CONTRIBUTION_S3_STORAGE_ENABLED:
        return S3Boto3Storage(
            bucket=settings.ACCOUNT_CONTRIBUTION_S3_BUCKET)
    else:
        return None


class AccountContributionUpload(TimeStampedModel):
    """Track uploaded client contribution csv files (aka house files)"""
    account = models.ForeignKey(Account, on_delete=models.CASCADE,
                                related_name='contribution_uploads')
    data_hash = models.CharField(max_length=256)
    user = models.ManyToManyField("authorize.RevUpUser")
    file_name = TruncatingCharField(max_length=256, blank=True)
    data_file = models.FileField(storage=_select_storage())


class AccountContract(TimeStampedModel):
    """Store the signed contracts for an Account in this model"""
    account = models.ForeignKey(Account, related_name="contracts",
                                on_delete=models.CASCADE)
    signer = models.ForeignKey("authorize.RevUpUser", related_name='+',
                               on_delete=models.PROTECT,
                               help_text="The user who signed the contract")
    contract = models.TextField(blank=True,
                                help_text="The raw html of a signed contract")
    signature = models.TextField(
        blank=True, help_text="The actual signature on the contract")
