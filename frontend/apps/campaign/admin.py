
from django.contrib import admin, messages

from frontend.apps.contact.api.views import ContactImportHF
from frontend.libs.utils.task_utils import UserAnalysisFlags
from frontend.libs.utils.string_utils import ensure_unicode
from .models import (AccountProfile, AccountAnalysisConfigLink,
                     AccountSkinSettingsLink, Account, Product)


class AccountSkinSettingsLinkInline(admin.TabularInline):
    model = AccountSkinSettingsLink
    extra = 1

    def formfield_for_foreignkey(self, db_field, *args, **kwargs):
        from frontend.apps.ux.models import SkinSettings
        # Limit the queryset to only unowned skins
        if db_field.name == 'skin':
            kwargs["queryset"] = SkinSettings.objects.filter(account=None)
        return super(AccountSkinSettingsLinkInline,
                     self).formfield_for_foreignkey(db_field, *args, **kwargs)


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', "recurly_plan_code", "product_type",)


@admin.register(AccountProfile)
class AccountProfileAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', "account_type", "product")
    inlines = (AccountSkinSettingsLinkInline,)
    list_select_related = ("product",)


class AccountAnalysisConfigLinkInline(admin.TabularInline):
    model = AccountAnalysisConfigLink
    extra = 1


class AccountAdmin(admin.ModelAdmin):
    inlines = (AccountAnalysisConfigLinkInline,)
    list_display = ("id", "organization_name", "account_head",
                    "account_profile", "seat_count", "active", "account_key")
    list_filter = ("active",)
    search_fields = ("organization_name", "account_head__email")
    actions = ("deactivate", "analyze", "trigger_housefile_import")
    list_select_related = ("account_profile__product",)

    def deactivate(self, request, queryset):
        return queryset.update(active=False)

    def analyze(self, request, queryset):
        from frontend.apps.analysis.utils import submit_analysis
        for account in queryset.iterator():
            flags = UserAnalysisFlags(account.account_user).prepare_flags()
            submit_analysis(account.account_user, account, flags=flags)

    def trigger_housefile_import(self, request, queryset):
        """Imports contacts from the default housefile collection"""
        account_ids = queryset.values_list('id', flat=True)
        for _id in account_ids:
            params = {'account_id': _id}
            response = ContactImportHF.start_housefile_import(
                request, params)
            if response.status_code == 200:
                self.message_user(request, ensure_unicode(response.content))
            else:
                self.message_user(request, ensure_unicode(response.content),
                                  messages.ERROR)

    def get_form(self, request, obj=None, **kwargs):
        """Limit the current_skin choices to skins that are available
           to this account.
        """
        form = super(AccountAdmin, self).get_form(request, obj, **kwargs)
        valid_skins = set(obj.get_skins())
        # It's possible an account's current skin is no longer one of the
        # valid skins. If we don't include it, it can't be removed.
        if obj.current_skin:
            valid_skins.add(obj.current_skin)
        form.base_fields['current_skin'].limit_choices_to = dict(
            id__in=(s.id for s in valid_skins))
        return form


admin.site.register(Account, AccountAdmin)
