from operator import attrgetter

from django.shortcuts import get_object_or_404
from rest_framework import permissions

from frontend.apps.campaign.models import Account, Event


class BaseObjectPermission(permissions.BasePermission):
    """This permission class is designed to verify permissions on objects
    with IDs given in the url string.

    Subclasses should always override 'permission_check()'
    """
    DEFAULT_MODEL_CLASS = None

    def __init__(self, url_id_field='pk', model_class=None, *args, **kwargs):
        self.url_id_field = url_id_field
        self.model_class = model_class or self.DEFAULT_MODEL_CLASS
        # noinspection PyArgumentList
        super(BaseObjectPermission, self).__init__(*args, **kwargs)

    def has_permission(self, request, view):
        model_obj = self.get_object_from_url(request, view)
        # If there is no supplied model, there can't be any permissions on it.
        if not model_obj:
            return False
        return self.permission_check(request, view, model_obj)

    def get_object_from_url(self, request, view):
        object_id = view.kwargs.get(self.url_id_field)
        if not object_id or not self.model_class:
            return None
        return get_object_or_404(self.model_class, pk=object_id)

    def permission_check(self, request, view, model_obj):
        """This should be overridden by subclasses"""
        return True


# class IsAccountAdmin(BaseObjectPermission):
#     """Check if the user is a campaign admin of the given campaign.
#
#     The given campaign is the id in the url string.
#     """
#     DEFAULT_MODEL_CLASS = Account
#
#     def permission_check(self, request, view, account):
#         return request.user.is_account_admin(account)


class IsEventFundraiser(BaseObjectPermission):
    """Check if the user is a fundraiser for the given event."""
    DEFAULT_MODEL_CLASS = Event

    def permission_check(self, request, view, event):
        return request.user.events.filter(pk=event.pk).exists()


class SelfAccount(BaseObjectPermission):
    """Allow the user to continue if they own the account."""
    DEFAULT_MODEL_CLASS = Account

    def __init__(self, methods=('GET',), *args, **kwargs):
        self.methods = methods
        super(SelfAccount, self).__init__(*args, **kwargs)

    def permission_check(self, request, view, account):
        if self.methods and request.method.upper() not in self.methods:
            return False

        # Let's attempt to use the current seat because it will be the most
        # common case, and far more performant.
        try:
            current_seat = request.user.current_seat
        except AttributeError:
            current_seat = None

        if current_seat and current_seat.account_id == account.id:
            return True
        else:
            return request.user.seats.assigned().filter(
                        account__pk=account.pk).exists()


class AccountOwnerBase(BaseObjectPermission):
    def __init__(self, account_id_field, *args, **kwargs):
        self.account_id_field = account_id_field
        super(AccountOwnerBase, self).__init__(*args, **kwargs)

    def permission_check(self, request, view, obj):
        return (str(obj.account_id) ==
                view.kwargs.get(self.account_id_field))


class AccountOwnsEvent(AccountOwnerBase):
    """Check if the account ID in the url is the same account as the
     one linked to from the event.
    """
    DEFAULT_MODEL_CLASS = Event


class IsAccountMember(permissions.BasePermission):
    """Object-level permission to only allow members of an account to interact
    with account-level objects.
    """
    OBJ_FIELD = 'account'
    REQUEST_FIELD = 'user'
    SAFE_METHODS = []

    def has_object_permission(self, request, view, obj):
        account = attrgetter(self.OBJ_FIELD)(obj)
        user = attrgetter(self.REQUEST_FIELD)(request)
        return account.is_member(user)

    def has_permission(self, request, view, obj=None):
        """
        Check if the user is member of the account specified in this query.
        Typically, this will be in the event of a sub-API. E.g.
        /accounts/<id>/seats
        Subclasses may want to override this
        """
        api_parent = 'parent_lookup_{}_id'.format(
            self.OBJ_FIELD.replace('_id', ''))
        if api_parent in view.kwargs:
            user = attrgetter(self.REQUEST_FIELD)(request)
            account_id = view.kwargs[api_parent]
            account = Account.objects.get(pk=account_id)
            return account.is_member(user)

        elif view.lookup_field in view.kwargs:
            # If a specific object is given, we'll let 'has_object_permission'
            # decide if the request has permission to view it.
            return True
        else:
            return False
