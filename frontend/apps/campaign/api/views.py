
from django.core.exceptions import ValidationError
from django.http import Http404, HttpResponseForbidden
from django.shortcuts import get_object_or_404
import django_filters
from rest_condition.permissions import Or, And
from rest_framework import permissions, exceptions, viewsets, filters, mixins
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework_extensions.mixins import (NestedViewSetMixin,
                                              DetailSerializerMixin)

from frontend.apps.analysis.api.mixins import CSVExportMixin
from frontend.apps.campaign.api.serializers import (
    AccountSerializer, EventSerializer, ProspectSerializer,
    DetailedProspectSerializer, AccountDetailSerializer)
from frontend.apps.campaign.api.permissions import (
    SelfAccount, IsEventFundraiser, AccountOwnsEvent)
from frontend.apps.campaign.models import (
    Event, Account, Prospect, ProspectCollision, AccountTask)
from frontend.apps.contact.models import Contact
from frontend.apps.contact_set.models import ContactSet
from frontend.apps.role.api.permissions import HasPermission
from frontend.apps.role.permissions import AdminPermissions
from frontend.apps.seat.models import BulkFundraiserImport
from frontend.apps.seat.tasks import bulk_fundraiser_import_task
from frontend.apps.ux.models import SkinSettings
from frontend.libs.utils.api_utils import (
    IsAdminUser, CRDModelViewSet, exception_watcher, PartialUpdateMixin,
    InitialMixin)
from frontend.libs.utils.string_utils import str_to_bool
from frontend.libs.utils.view_utils import build_protocol_host


class AccountViewSet(DetailSerializerMixin, viewsets.ReadOnlyModelViewSet,
                     mixins.UpdateModelMixin):
    serializer_class = AccountSerializer
    serializer_detail_class = AccountDetailSerializer
    permission_classes = (
        And(permissions.IsAuthenticated,
            Or(IsAdminUser,
               And(SelfAccount(methods=("GET", "PUT")),
                   Or(HasPermission("pk",
                                    [AdminPermissions.LOAD_DATA,
                                     AdminPermissions.MOD_FUNDRAISERS],
                                    methods=["GET"]),
                      HasPermission("pk", [AdminPermissions.CONFIG_SKINS,
                                           AdminPermissions.MOD_ACCOUNT],
                                    methods=["PUT"]),
                      )
                   )
            )
        ),
    )
    model = Account
    queryset = Account.objects.all()

    @exception_watcher(ValidationError)
    def update(self, request, *args, **kwargs):
        """This update method only recognizes updates to 'current_skin'."""
        current_skin_id = request.data.get("current_skin")
        cc_token = request.data.get("recurly-token")
        account = self.get_object()

        if current_skin_id:
            if not request.user.has_permissions(
                        [AdminPermissions.CONFIG_SKINS]):
                return HttpResponseForbidden("You do not have permission to "
                                             "perform this action")
            current_skin = get_object_or_404(SkinSettings, pk=current_skin_id)
            account.current_skin = current_skin
            account.save()

        if cc_token:
            if not request.user.has_permissions(
                        [AdminPermissions.MOD_ACCOUNT]):
                return HttpResponseForbidden("You do not have permission to "
                                             "perform this action")
            # Process the payment and attach to the account
            try:
                account.update_billing_info(cc_token)
            except Exception as err:
                raise exceptions.APIException(str(err))
            else:
                # Activate the account
                if not account.active:
                    account.active = True
                    account.save()

        return Response(AccountSerializer(account).data)

    @action(detail=True, methods=['post'])
    def bulk_import(self, request, pk, **kwargs):
        """Bulk import fundraisers to be invited.
        Expected items: file and file_type.
        """
        account = get_object_or_404(Account, pk=pk)
        file_type = request.POST.get('file_type', "").strip().lower()
        file_ = request.FILES.get('file', None)
        if not file_type or not file_:
                raise exceptions.APIException(
                    'Upload external data expects a "file_type" and "file".')

        if file_type not in ('csv',):
            raise exceptions.APIException("Unsupported file type.")

        # Store the file for use by Celery
        try:
            method = getattr(BulkFundraiserImport,
                             "create_{}".format(file_type.lower()))
            bfi = method(account, request.user, file_)
        except ValueError as err:
            raise exceptions.APIException(str(err))

        # Trigger a Celery task
        task_id = bulk_fundraiser_import_task.delay(
            bfi.id, build_protocol_host(request))
        AccountTask.objects.create(account=account, task_id=task_id,
                                   user=request.user,
                                   task_type=AccountTask.TaskType.BULK_IMPORT)
        return Response("Successfully generated import task.")


class EventFilter(django_filters.FilterSet):
    class Meta:
        model = Event
        fields = {'event_date': ['lt', 'gt', 'lte', 'gte']}


class AccountEventsViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    """CRUD Events for an Account."""
    parent_account_lookup = 'parent_lookup_account_id'
    serializer_class = EventSerializer
    permission_classes = (And(permissions.IsAuthenticated,
                              Or(HasPermission(parent_account_lookup,
                                               AdminPermissions.VIEW_EVENTS,
                                               methods=['GET']),
                                 HasPermission(parent_account_lookup,
                                               AdminPermissions.MOD_EVENTS,
                                               methods=['POST', 'PUT',
                                                        'PATCH']),
                                 IsAdminUser)),)
    filter_class = EventFilter

    model = Event
    queryset = Event.objects.all()
    filter_backends = (filters.OrderingFilter,)
    ordering = ('-event_date',)

    def create(self, request, *args, **kwargs):
        account = get_object_or_404(Account,
                                    pk=kwargs.get(self.parent_account_lookup))
        form = EventSerializer(data=request.data)
        if form.is_valid():
            form.save(account=account)
            return Response(form.data)
        else:
            return Response({'errors': form.errors})


class EventProspectsViewSet(NestedViewSetMixin, PartialUpdateMixin,
                            CSVExportMixin, InitialMixin, CRDModelViewSet):
    """Allows for the creation and removal of prospects from an Event."""
    parent_account_lookup = 'parent_lookup_account_id'
    parent_event_lookup = 'parent_lookup_event_id'
    serializer_class = ProspectSerializer
    permission_classes = (
        And(permissions.IsAuthenticated,
            AccountOwnsEvent(parent_account_lookup,
                             parent_event_lookup),
            And(SelfAccount(url_id_field=parent_account_lookup,
                            methods=('GET', 'POST', 'PUT',
                                           'PATCH', 'DELETE')),
                Or(IsEventFundraiser(parent_event_lookup),
                    Or(HasPermission(parent_account_lookup,
                                     AdminPermissions.MOD_PROSPECTS,
                                     methods=['POST', 'PUT',
                                              'PATCH', 'DELETE']),
                       HasPermission(parent_account_lookup,
                                     AdminPermissions.VIEW_PROSPECTS,
                                     methods=['GET']),
                    )
                )
            )
        ),
    )
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('created',)
    ordering = ("-created",)

    def _get_permission_level(self, request, prospect):
        # permission_level determines what fields a user can edit.
        # Account admin gets highest permissions.
        if request.user.current_seat and AdminPermissions.MOD_PROSPECTS in \
                request.user.current_seat.permissions:
            permission_level = 2
        elif request.user.is_lead_fundraiser(prospect.event):
            permission_level = 1
        elif prospect.user_id == request.user.id:
            permission_level = 1
        else:
            permission_level = 0
        return permission_level

    def _initial(self):
        self.event = get_object_or_404(
            Event.objects.select_related("account"),
            pk=self.kwargs.get(self.parent_event_lookup),
            account=self.kwargs.get(self.parent_account_lookup))

    def get_queryset(self):
        return Prospect.objects.filter(event=self.event).select_related(
                                                            "user", "contact")

    def get_object(self):
        """Override this to prevent 'update' from querying the object twice."""
        try:
            return self.object
        except AttributeError:
            self.object = super(EventProspectsViewSet,
                                self).get_object()
            return self.object

    def retrieve(self, request, *args, **kwargs):
        prospect = self.get_object()
        return Response(DetailedProspectSerializer(prospect).data)

    def update(self, request, *args, **kwargs):
        prospect = self.get_object()
        permission_level = self._get_permission_level(request, prospect)
        editable_fields = prospect.get_editable_fields(permission_level)
        # Remove any fields that aren't editable. The user may not update those
        for k in list(request.data.keys()):
            if k not in editable_fields:
                del request.data[k]
        return super(EventProspectsViewSet, self).update(request,
                                                         *args, **kwargs)

    def create(self, request, *args, **kwargs):
        """Create a new Prospect.

        Expected argument is 'contact', which should contain an ID.
        The 'contact' is expected to be a primary contact of request.user.
        """
        contact_id = request.data.get('contact')
        if not contact_id:
            raise exceptions.APIException("A contact ID is required to create "
                                          "a new prospect.")

        # If the contact and user don't match, we respond with a 404
        contact = get_object_or_404(Contact, pk=contact_id,
                                    user=request.user, is_primary=True)



        # Check that this is not already a prospect in this event
        prospects = Prospect.objects.filter(contact=contact, event=self.event)
        collisions = ProspectCollision.objects.filter(contact=contact,
                                                      event=self.event)
        if prospects or collisions:
            raise exceptions.APIException("This contact is already a member "
                                          "of this event")

        # Check if this contact is already a member of the event,
        # added by another fundraiser.
        existing_prospect = self._check_event_membership(contact, self.event)
        if existing_prospect:
            # If this contact is already a member of the event,
            # create an ProspectCollision
            self._create_prospect_collision(contact, existing_prospect,
                                            self.event)
            msg = "Prospect Collision found."
        else:
            new_prospect = Prospect(contact=contact,
                                    user=request.user,
                                    event=self.event, soft_amt=0,
                                    hard_amt=0, in_amt=0,
                                    num_attending=0, notes='')
            new_prospect.save()
            seat = request.user.current_seat
            msg = "Prospect created: {}".format(new_prospect.id)
            # TODO: Uncomment when Prospects cannot be deleted
            # action_send(request, request.user, verb="created prospect",
            #             action_object=new_prospect, target=event,
            #             seat=seat.id if seat else None)

        self.invalidate_cache(request.user)
        return Response({'detail': msg})

    def destroy(self, request, pk=None, *args, **kwargs):
        """Delete the prospect from the given event"""
        if not pk:
            raise Http404

        # Get the prospect, and ensure it's actually part of this event
        prospect = self.get_object()

        permission_level = self._get_permission_level(request, prospect)
        # A certain permission level is required to delete this prospect
        if permission_level < 1:
            return HttpResponseForbidden()

        pid = prospect.id
        prospect.delete()
        self.invalidate_cache(request.user)
        # TODO: Uncomment when Prospects cannot be deleted
        # seat = request.user.current_seat
        # action_send(request, request.user,
        #             verb="prospect deleted", target=prospect,
        #             seat=seat.id if seat else None)
        return Response({'detail': "Prospect deleted: {}".format(pid)})

    def _check_event_membership(self, contact, event):
        """Search the prospects of the Event for any collisions with contact.

        Return the prospect if there is a collision.
        """
        # Query all prospects from the event that are not created by this user.
        prospects = (Prospect.objects.filter(event=event)
                                     .exclude(user=self.request.user))
        for prospect in prospects:
            # Compare the contact from each prospect and the new contact
            if prospect.contact.is_same_person(contact):
                return prospect
        return None

    def _create_prospect_collision(self, contact, existing_prospect, event):
        """Create an ProspectCollision instance."""
        ProspectCollision.objects.get_or_create(
            external_prospect=existing_prospect,
            contact=contact,
            user=self.request.user,
            event=event)

    def _get_export_options(self, request):
        export_options = super(EventProspectsViewSet,
                               self)._get_export_options(request)
        export_options["show_prospect_data"] = str_to_bool(
            request.GET.get("show_prospect_data", True))
        return export_options

    def _get_export_task_type(self):
        return self._export_task_types.PROSPECTS_EXPORT

    def _get_export_contact_set(self):
        """From CSVExportMixin. We need to get the user's root contact set."""
        return ContactSet.get_root(self.event.account, self.request.user,
                                   select_related="analysis")

    def _get_export_label(self, contact_set):
        return "Export Prospects"

    def _get_export_filename(self, contact_set):
        return "Prospects.csv"

    @classmethod
    def invalidate_cache(cls, user):
        """Invalidate any caches that may be affected by this view.

        Currently, the only view that caches prospect results is the results.
        """
        from frontend.apps.analysis.api.views import (
            ContactSetAnalysisContactResultsViewSet)
        ContactSetAnalysisContactResultsViewSet.invalidate_cache(user)


