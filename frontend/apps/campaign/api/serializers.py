
from rest_framework import serializers

from frontend.apps.authorize.models import UserEventLink
from frontend.apps.campaign.models import Event, Prospect, Account
from frontend.apps.contact.api.serializers import (
    DetailedContactSerializer, ContactSerializer)
from frontend.libs.utils.recurly_utils import (get_recurly_account,
                                               get_billing_info)


class BasicAccountSerializer(serializers.ModelSerializer):
    account_type = serializers.SerializerMethodField()
    enabled_features = serializers.SerializerMethodField()

    class Meta:
        model = Account
        fields = ("id", "active", "title", "account_type", "enabled_features")

    def get_account_type(self, account):
        return account.account_profile.account_type

    def get_enabled_features(self, account):
        return account.account_profile.product.enabled_features


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ("id", "account_head", "active", "organization_name",
                  "start_date", "seat_count", "current_skin")


class AccountDetailSerializer(AccountSerializer):
    cc = serializers.SerializerMethodField()

    class Meta(AccountSerializer.Meta):
        fields = AccountSerializer.Meta.fields + ("cc",)

    def get_cc(self, obj):
        """
        Get the Recurly account billing info using the Recurly account
        code from the Account object.
        :param obj:
        :return:
        """
        billing_info = None
        if obj.recurly_account_code:
            account = get_recurly_account(obj.recurly_account_code)

            if account:
                billing_info = get_billing_info(account)
        return billing_info


class EventSerializer(serializers.ModelSerializer):
    price_points = serializers.ListField(
        child=serializers.DecimalField(max_digits=10,
                                       decimal_places=2),
        required=False)
    event_date = serializers.DateTimeField(format='%Y-%m-%d',
                                           input_formats=['%Y-%m-%d'])

    class Meta:
        model = Event
        fields = ('id', 'event_name', 'event_date', 'goal', 'price_points')


class UserEventSerializer(serializers.ModelSerializer):
    """This is the UserEventLink serializer with focus on the event.

    Note: You should use `select_related` on "event" when querying the
          UserEventLinks for this serializer.
    """

    class Meta:
        model = UserEventLink
        fields = ('id', 'event_name', 'event_date', 'goal', 'price_points',
                  "is_lead",)

    id = serializers.ReadOnlyField(source='event.id')
    event_name = serializers.ReadOnlyField(source='event.event_name')
    event_date = serializers.ReadOnlyField(source='event.event_date')
    goal = serializers.ReadOnlyField(source='event.goal')
    price_points = serializers.ReadOnlyField(source='event.price_points')


class ProspectSerializer(serializers.ModelSerializer):
    contact = ContactSerializer(read_only=True)

    class Meta:
        model = Prospect
        fields = ("id", "contact", "user", "soft_amt", "hard_amt",
                  "in_amt", "num_attending")

    def __new__(cls, *args, **kwargs):
        """Less-than-appealing workaround for a circular import issue."""
        if "user" not in cls._declared_fields:
            from frontend.apps.authorize.api.serializers import \
                RevUpUserSubsetSerializer
            cls._declared_fields['user'] = RevUpUserSubsetSerializer(read_only=True)
        return super(ProspectSerializer, cls).__new__(cls, *args, **kwargs)


class DetailedProspectSerializer(ProspectSerializer):
    contact = DetailedContactSerializer(read_only=True)
