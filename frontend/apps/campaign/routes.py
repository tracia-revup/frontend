
from frontend.apps.campaign.models import AccountProfile as AP
from frontend.apps.seat.utils import is_account_type_factory
from frontend.libs.django_view_router import Route


POLCAM_ROUTE = Route(is_account_type_factory(AP.AccountType.POLITICAL),
                     name="polcam", suffix="pc")

POLCOM_ROUTE = Route(is_account_type_factory(AP.AccountType.POLITICAL),
                     name="polcom", suffix="pco")

UNIVERSITY_ROUTE = Route(is_account_type_factory(AP.AccountType.ACADEMIC),
                         name="university", suffix="uni")

NONPROFIT_ROUTE = Route(is_account_type_factory(AP.AccountType.NONPROFIT),
                        name="nonprofit", suffix='np')

ROUTES = [POLCAM_ROUTE, POLCOM_ROUTE, UNIVERSITY_ROUTE, NONPROFIT_ROUTE]
