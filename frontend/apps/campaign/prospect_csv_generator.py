import logging

from django.core.exceptions import ObjectDoesNotExist

from frontend.apps.analysis.csv_generator import CSVGenerator


LOGGER = logging.getLogger(__name__)


class ProspectCSVGenerator(CSVGenerator):
    prospect_fields = ("Attending", "Soft amount", "Hard amount", "In amount")

    def __init__(self, *args, **kwargs):
        self.show_prospect_data = kwargs.pop("show_prospect_data", True)
        super(ProspectCSVGenerator, self).__init__(*args, **kwargs)

    def _format_prospect_fields(self, container, result):
        """Populate the row result dict with the prospect dollar amounts"""
        result[self.prospect_fields[0]] = container.prospect.num_attending
        result[self.prospect_fields[1]] = container.prospect.soft_amt
        result[self.prospect_fields[2]] = container.prospect.hard_amt
        result[self.prospect_fields[3]] = container.prospect.in_amt
        return result

    def _inject_composition_functions(self):
        fns, fields = super(ProspectCSVGenerator,
                            self)._inject_composition_functions()
        if self.show_prospect_data:
            fields.extend(self.prospect_fields)
            fns.append(self._format_prospect_fields)
        return fns, fields

    def _prepare_container(self, obj):
        try:
            result = self.contact_set.queryset().get(
                contact=obj.contact.id, partition=self.partition)
        except ObjectDoesNotExist:
            LOGGER.warn("Result missing for Contact: {} in ContactSet: {}, "
                        "with Partition: {}".format(
                       obj.contact.id, self.contact_set.id, self.partition.id))
            result = None
        return self.Container(analysis_result=result,
                              contact=obj.contact,
                              prospect=obj)
