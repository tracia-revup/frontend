# coding=utf-8

import collections
import datetime
import json
import itertools
from random import randint
import string
from unittest.mock import patch, MagicMock

from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory
from django.urls import reverse
import unittest.mock as mock

from frontend.apps.analysis.factories import AnalysisConfigFactory
from frontend.apps.authorize.factories import RevUpUserFactory
from frontend.apps.authorize.test_utils import AuthorizedUserTestCase
from frontend.apps.campaign.factories import (
    EventFactory, ProspectFactory, PoliticalCampaignFactory,
    ProspectCollisionFactory, AccountFactory, UniversityFactory,
    AccountProfileFactory)
from frontend.apps.campaign.models import (
    ProspectCollision, Prospect, Account, Event, AccountSkinSettingsLink)
from frontend.apps.campaign.models.base import ACCOUNT_KEY_LENGTH
from frontend.apps.campaign.templatetags.contribution import fuzzyintcomma
from frontend.apps.contact.factories import ContactFactory
from frontend.apps.contact_set.models import ContactSet
from frontend.apps.core.views import RecurlyWebhook
from frontend.apps.role.defaults import (Admin, Fundraiser,
                                         Restricted_Fundraiser)
from frontend.apps.role.permissions import AdminPermissions
from frontend.apps.seat.factories import Seat, SeatFactory
from frontend.apps.ux.factories import SkinSettingsFactory
from frontend.libs.utils.date_utils import next_first_of_month
from frontend.libs.utils.string_utils import random_string
from frontend.libs.utils.test_utils import TestCase


class AccountViewSetTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        self.campaign = PoliticalCampaignFactory()
        super(AccountViewSetTests, self).setUp(
            login_now=False, campaigns=[self.campaign])
        self.url = reverse('account_api-list')
        self.url2 = reverse('account_api-detail', args=(self.campaign.id,))
        # Create a second campaign for the list api
        self.campaign2 = PoliticalCampaignFactory()
        self.url3 = reverse('account_api-detail', args=(self.campaign2.id,))

    def test_permissions(self):
        ## Verify any unauthorized user is rejected
        result = self.client.get(self.url)
        self.assertNoCredentials(result)
        result = self.client.get(self.url2)
        self.assertNoCredentials(result)

        # Login the user
        self._login(self.user)
        # Verify user is not an admin, or that would invalidate the test.
        self.assertFalse(self.user.is_admin)

        ## Verify a standard user is rejected from viewing all campaigns
        result = self.client.get(self.url)
        self.assertNoPermission(result)
        ## Verify a standard user is rejected from creating a campaign
        result = self.client.post(self.url, data={'test': '123'})
        self.assertNoPermission(result)
        ## Verify standard user is rejected from updating a campaign
        result = self.client.put(self.url2, data='{"test": "123"}',
                                 content_type='application/json')
        self.assertNoPermission(result)

        ## Verify regular user can't see their own campaign
        result = self.client.get(self.url2)
        self.assertNoPermission(result)

        ## Verify the user cannot edit their own campaign
        result = self.client.put(self.url2, data={'test': '123'})
        self.assertNoPermission(result)

        ## Verify the user cannot see another user's campaign
        result = self.client.get(self.url3)
        self.assertNoPermission(result)

        # Add the Campaign Admin group to the user
        self._add_admin_group(permissions=AdminPermissions.LOAD_DATA)

        ## Verify admin user can see the campaign
        result = self.client.get(self.url2)
        self.assertContains(result, '"id":{}'.format(self.campaign.id))
        ## Verify the list API still rejects the user
        result = self.client.get(self.url)
        self.assertNoPermission(result)
        ## Verify admin user can update the campaign
        self._add_admin_group(permissions=AdminPermissions.CONFIG_SKINS)
        result = self.client.put(self.url2, data='{"test": "123"}',
                                 content_type='application/json')
        self.assertContains(result, '"id":{}'.format(self.campaign.id))
        ## Verify admin user is rejected in update if they don't have permission
        result = self.client.put(self.url2, data='{"recurly-token": "123"}',
                                 content_type='application/json')
        self.assertNoPermission(result)

        # Make the user an admin
        self.user.is_staff = True
        self.user.save()
        self.assertTrue(self.user.is_admin)
        ## Verify the user can now see the list
        result = self.client.get(self.url)
        self.assertContains(result, '"id":{}'.format(self.campaign.id))
        self.assertContains(result, '"id":{}'.format(self.campaign2.id))


class AccountEventViewSetTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        self.campaign = PoliticalCampaignFactory()
        self.event = EventFactory(account=self.campaign)
        super(AccountEventViewSetTests, self).setUp(
            login_now=False, campaigns=[self.campaign], events=[self.event])

        self.url = reverse('events_api-list', args=(self.campaign.id,))
        self.url2 = reverse('events_api-detail',
                            args=(self.campaign.id, self.event.id))
        # Make some more events for list view testing
        self.event2 = EventFactory(account=self.campaign)
        self.event3 = EventFactory()

    def test_permissions(self):
        ## Verify any unauthorized user is rejected
        result = self.client.get(self.url)
        self.assertNoCredentials(result)
        result = self.client.get(self.url2)
        self.assertNoCredentials(result)

        # Login the user
        self._login(self.user)
        # Verify user is not an admin, or that would invalidate the test.
        self.assertFalse(self.user.is_admin)
        self.assertFalse(self.user.is_account_admin())

        ## Verify a standard user is rejected from viewing all campaign events
        result = self.client.get(self.url)
        self.assertNoPermission(result)
        ## Verify a standard user is rejected from creating a campaign
        result = self.client.post(self.url, data={'test': '123'})
        self.assertNoPermission(result)

        ## Verify a standard user is rejected from viewing their own event
        self.assertIn(self.event, self.user.events.all())
        result = self.client.get(self.url2)
        self.assertNoPermission(result)
        ## Verify the user cannot edit their own event
        result = self.client.put(self.url2, data={'test': '123'})
        self.assertNoPermission(result)

        # Add the Campaign Admin group to the user
        self._add_admin_group(self.user,
                              permissions=AdminPermissions.VIEW_EVENTS)

        ## Verify the list API accepts the user
        result = self.client.get(self.url)
        # Verify the two campaign events are in the results, but not the
        # non-campaign event.
        self.assertContains(result, '"id":{}'.format(self.event.id))
        self.assertContains(result, '"id":{}'.format(self.event2.id))
        self.assertNotContains(result, '"id":{}'.format(self.event3.id))

        ## Verify create still doesn't work (read only view)
        result = self.client.post(self.url, data={'test': '123'})
        self.assertNoPermission(result)

        ## Add appropriate permissions and verify creation works
        # Add the Campaign Admin group to the user
        self._add_admin_group(self.user,
                              permissions=AdminPermissions.MOD_EVENTS)
        result = self.client.post(self.url, content_type='application/json',
                                  data="""{"event_name": "eventName",
                                           "event_date": "2016-10-12",
                                           "goal": 50000,
                                           "price_points": [100, 200]}""")
        self.assertContains(result, '"event_name":"eventName"')

    def test_serializer(self):
        ## Verify the correct fields are in the response
        # Login the user and Add the Campaign Admin group to the user
        self._login_and_add_admin_group()

        ## Verify the list API accepts the user
        result = self.client.get(self.url)
        self.verify_serializer(result, (
            'id', 'event_name', 'event_date', 'goal', 'price_points')
        )

    def test_create(self):
        self._login(self.user)
        self._add_admin_group(self.user,
                              permissions=AdminPermissions.MOD_EVENTS)

        event_name = random_string()
        self.assertFalse(Event.objects.filter(event_name=event_name).exists())
        result = self.client.post(
            self.url, content_type='application/json',
            data="""{"event_name": "%s",
                     "event_date": "2016-10-12",
                     "goal": 50000,
                     "price_points": [100, 200]}""" % event_name)
        self.assertContains(result, '"event_name":"%s"' % event_name)

        # Verify the event was created
        event = Event.objects.get(event_name=event_name)
        self.assertEqual(event.event_date, datetime.datetime(
            year=2016, month=10, day=12))
        self.assertEqual(event.goal, 50000)
        self.assertEqual(event.price_points, [100, 200])


class EventProspectsViewSetTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        self.campaign = PoliticalCampaignFactory()
        self.event = EventFactory(account=self.campaign)
        super(EventProspectsViewSetTests, self).setUp(
            login_now=False, campaigns=[self.campaign], events=[self.event])

        self.prospect = ProspectFactory(event=self.event,
                                        user=self.user)

        self.url = reverse('event_prospects_api-list',
                           args=(self.campaign.id, self.event.id))
        self.url2 = reverse('event_prospects_api-detail',
                            args=(self.campaign.id, self.event.id,
                                  self.prospect.id))
        # Make some more prospects for list view testing
        self.prospect2 = ProspectFactory(event=self.event,
                                         user=self.user)
        self.prospect3 = ProspectFactory(user=self.user)

    def test_permissions(self):
        ## Verify any unauthenticated user is rejected
        result = self.client.get(self.url)
        self.assertNoCredentials(result)
        result = self.client.post(self.url, data={"abc": "123"})
        self.assertNoCredentials(result)
        result = self.client.put(self.url2, data={"abc": "123"})
        self.assertNoCredentials(result)
        result = self.client.post(self.url2)
        self.assertNoCredentials(result)

        # Login the user
        self._login(self.user)
        # Verify user is not an admin, or that would invalidate the test.
        self.assertFalse(self.user.is_admin)
        self.assertFalse(self.user.is_account_admin())

        ## Verify positive cases
        check_str = '"id":{},"contact"'
        result = self.client.get(self.url)
        self.assertContains(result, check_str.format(self.prospect.id))
        self.assertContains(result, check_str.format(self.prospect2.id))
        # Not in the event
        self.assertNotContains(result, check_str.format(self.prospect3.id))

        # Create a new prospect
        contact = ContactFactory(user=self.user)
        result = self.client.post(self.url, data={'contact': contact.id})
        self.assertContains(result, "Prospect created")
        # Delete new prospect
        pid = json.loads(result.content)['detail'].split()[2]
        url = reverse('event_prospects_api-detail',
                      args=(self.campaign.id, self.event.id, pid))
        result = self.client.delete(url)
        self.assertContains(result, "Prospect deleted")

        ## Verify negative cases
        # Campaign membership required
        seats = list(self.user.seats.assigned())
        for seat in seats:
            seat.state = Seat.States.REVOKED
            seat.save()
        self.user.reset_current_seat(None)
        result = self.client.get(self.url)
        self.assertNoPermission(result)
        for seat in seats:
            seat.state = Seat.States.ACTIVE
            seat.save()

        # Event membership required
        self.user.event_links.filter(event=self.event).delete()
        result = self.client.get(self.url)
        self.assertNoPermission(result)

        # Add an invalid admin permission and verify access is not granted
        self._add_admin_group(permissions=AdminPermissions.MOD_ACCOUNT)
        result = self.client.get(self.url)
        self.assertNoPermission(result)

        ## Verify admin/superuser cases
        # Making user campaign admin returns permission
        self._add_admin_group(self.user,
                              permissions=AdminPermissions.VIEW_PROSPECTS)
        result = self.client.get(self.url)
        self.assertContains(result, self.prospect.id)

        # Verify view permissions isn't sufficient to write
        result = self.client.delete(self.url2)
        self.assertNoPermission(result)

        # Add proper permissions and delete
        self._add_admin_group(self.user,
                              permissions=AdminPermissions.MOD_PROSPECTS)
        result = self.client.delete(self.url2)
        self.assertContains(result, "Prospect deleted")
        self.prospect = ProspectFactory(event=self.event,
                                        user=self.user)

        # Remove campaign admin
        for seat in self.user.seats.all():
            seat.permissions = []
            seat.save()
        self.assertFalse(self.user.is_account_admin())

        result = self.client.get(self.url)
        self.assertNoPermission(result)

        # Make superuser
        self.user.is_superuser = True
        self.user.save()
        result = self.client.get(self.url)
        self.assertNoPermission(result)

    def test_serializer(self):
        ## Verify the correct fields are in the response
        # Login the user
        self._login(self.user)
        fields = ('id', 'contact', 'user', 'soft_amt', 'hard_amt', 'in_amt',
                  'num_attending')

        ## Verify the list API
        result = self.client.get(self.url)
        self.verify_serializer(result, fields)
        self.verify_serializer(
            result,
            ('id', 'first_name', 'last_name', "additional_name",
             "needs_attention", "contact_type", "deletion_pending"),
            specific_field="contact")
        self.verify_serializer(
            result,
            ('id', 'first_name', 'last_name', 'email', 'is_active'),
            specific_field="user")

        ## Verify the detail API
        result = self.client.get(self.url2)
        self.verify_serializer(result, fields)
        self.verify_serializer(result, (
            'id', 'first_name', 'last_name', "additional_name", "addresses",
            "email_addresses", "organizations", "alma_maters", "phone_numbers",
            "locations", "image_urls", "external_profiles", "needs_attention",
            "contact_type", "import_record", "gender", "dob",
            "deletion_pending", "external_ids", "relationships"),
                               specific_field="contact")
        self.verify_serializer(result, ('id', 'first_name', 'last_name',
                                        'email', 'is_active'),
                               specific_field="user")

    def test_update(self):
        # Login the user
        self._login(self.user)

        pre_in_amt = self.prospect.in_amt
        ## Verify write-able and read-only fields
        self.client.put(self.url2, content_type="application/json",
                        data=json.dumps({"soft_amt": 150, "hard_amt": 155,
                                         "num_attending": 21, "in_amt": 175}))
        prospect = Prospect.objects.get(id=self.prospect.id)
        self.assertEqual(prospect.soft_amt, 150)
        self.assertEqual(prospect.hard_amt, 155)
        self.assertEqual(prospect.num_attending, 21)
        ## As of now, we are allowing regular users to update the in_amt
        # self.assertEqual(prospect.in_amt, pre_in_amt)
        self.assertEqual(prospect.in_amt, 175)

    def test_create(self):
        # Login the user
        self._login(self.user)

        ## Verify a contact ID is required
        result = self.client.post(self.url, data={"abc": "123"})
        self.assertContains(result, "contact ID is required", status_code=500)

        # Create a new contact for the test
        contact = ContactFactory()
        ## Verify create fails if user doesn't own contact
        result = self.client.post(self.url, data={'contact': contact.id})
        self.assertContains(result, "", status_code=404)

        contact.user = self.user
        contact.is_primary = False
        contact.save()

        ## Verify fails if contact isn't primary
        result = self.client.post(self.url, data={'contact': contact.id})
        self.assertContains(result, "", status_code=404)
        contact.is_primary = True
        contact.save()

        ## Verify prospect is created now
        now = datetime.datetime.utcnow()
        result = self.client.post(self.url, data={'contact': contact.id})
        self.assertContains(result, "Prospect created")
        pid = json.loads(result.content)['detail'].split()[2]
        # Verify the content of the new Prospect
        prospect = Prospect.objects.get(id=pid)
        self.assertEqual(prospect.contact, contact)
        self.assertEqual(prospect.user, self.user)
        self.assertEqual(prospect.event, self.event)
        self.assertEqual(prospect.soft_amt, 0)
        self.assertEqual(prospect.hard_amt, 0)
        self.assertEqual(prospect.in_amt, 0)
        self.assertEqual(prospect.notes, "")

        ## Verify contact that is already added is not re-added
        result = self.client.post(self.url, data={'contact': contact.id})
        self.assertContains(result, "contact is already a member",
                            status_code=500)
        ## Verify adding a contact that has an existing collision fails
        ac = ProspectCollisionFactory(event=self.event, user=self.user)
        ac.contact.user = self.user
        ac.contact.save()
        result = self.client.post(self.url, data={'contact': ac.contact.id})
        self.assertContains(result, "contact is already a member",
                            status_code=500)

        ## Verify prospect collision
        contact = ContactFactory(user=self.user,
                                 contact_type="linkedin-oauth2")
        contact2 = ContactFactory(contact_id=contact.contact_id,
                                  contact_type="linkedin-oauth2")
        ProspectFactory(contact=contact2, event=self.event)
        result = self.client.post(self.url, data={'contact': contact.id})
        self.assertContains(result, "Prospect Collision found.")

        ## Verify a user can't create a prospect for a contact they don't own
        contact = ContactFactory(user=self.event.account.account_user,
                                 contact_type="linkedin-oauth2")
        result = self.client.post(self.url, data={'contact': contact.id})
        self.assertContains(result, "Not found.", status_code=404)

    def test_delete(self):
        ## Verify negative case: user without permissions attempts to delete
        user2 = RevUpUserFactory()
        user2.add_events(self.event)
        self._login(user2)
        result = self.client.delete(self.url2)
        self.assertNoPermission(result)
        self._logout()

        ## Verify positive case
        self._login(self.user)
        result = self.client.delete(self.url2)
        self.assertContains(result, "Prospect deleted: {}".format(
            self.prospect.id))
        self.assertRaises(Prospect.DoesNotExist, Prospect.objects.get,
                          pk=self.prospect.id)


class ProspectTests(TestCase):
    def setUp(self):
        self.prospect = ProspectFactory()

    def test_prospect_post_delete(self):
        # Create an ProspectCollision for the attendee
        ac = ProspectCollisionFactory.create(external_prospect=self.prospect)
        self.assertEqual(1, ProspectCollision.objects.filter(
            external_prospect=self.prospect).count())

        ## Verify attendee deletion also deletes collision
        self.prospect.delete()
        self.assertEqual(0, ProspectCollision.objects.filter(
            external_prospect=self.prospect).count())

    def test_get_editable_fields(self):
        ## Verify the returned fields match the expected permission level
        # Verify no permissions means no editable fields
        fields = Prospect.get_editable_fields(0)
        self.assertEqual(fields, set())

        # Verify partial permissions means self-editable fields
        fields = Prospect.get_editable_fields(1)
        self.assertEqual(fields, {'soft_amt', 'hard_amt', 'num_attending',
                                  'notes', 'in_amt'})

        # Verify full permissions means all fields
        # Verify partial permissions means self-editable fields
        fields = Prospect.get_editable_fields(2)
        self.assertEqual(fields, {
            'soft_amt', 'hard_amt', 'num_attending', 'notes', 'in_amt'}
                         )


class AccountProfile(TestCase):
    def setUp(self):
        self.account_profile = AccountProfileFactory()

    def test_features(self):
        """Verify we can toggle features"""
        # Verify all features are enabled by default
        features = self.account_profile.product.features
        assert all(features.values())

        # Disable a feature and verify
        self.account_profile.product.enabled_features.remove("CMN")
        del self.account_profile.product._features
        features = self.account_profile.product.features
        assert not features["CONTACT_MANAGER"]
        # Remove the disabled feature to verify the rest are still enabled
        features.pop("CONTACT_MANAGER")
        assert all(features.values())


class AccountTests(TestCase):
    def setUp(self):
        self.account = AccountFactory()

    def test_get_or_create_recurly_account(self):
        self.account.account_profile.product.recurly_plan_code = ""
        self.assertRaisesRegex(AttributeError, "plan code is required",
                                self.account.get_or_create_recurly_account)
        self.account.account_profile.product.recurly_plan_code = "test"

        with mock.patch("frontend.apps.campaign.models.base."
                        "create_account") as m:
            ## Verify positive case
            self.assertEqual(self.account.recurly_account_code, "")
            self.assertEqual(m.call_count, 0)
            cc_token = "sadfdsfddf"
            self.account.get_or_create_recurly_account(cc_token)
            self.assertNotEqual(self.account.recurly_account_code, "")
            self.assertEqual(len(self.account.recurly_account_code), 32)
            self.assertEqual(m.call_count, 1)
            m.assert_called_with(self.account, cc_token,
                                 self.account.account_head)
            # Verify account_cache was set
            self.assertIsNotNone(self.account._account_cache)

        account_code = self.account.recurly_account_code
        self.account._account_cache = None

        with mock.patch("frontend.apps.campaign.models.base."
                        "get_recurly_account") as m:
            ## Verify new account is not created
            self.assertEqual(m.call_count, 0)
            self.account.get_or_create_recurly_account(cc_token)
            self.assertEqual(account_code, self.account.recurly_account_code)
            self.assertEqual(m.call_count, 1)
            m.assert_called_with(self.account.recurly_account_code)
            # Verify account_cache was set
            self.assertIsNotNone(self.account._account_cache)

            # Verify account_cache is used
            self.account.get_or_create_recurly_account(cc_token)
            self.assertEqual(m.call_count, 1)

    @mock.patch("frontend.apps.campaign.models.Account.get_or_create_"
                "recurly_account", mock.MagicMock(return_value=9))
    def test_get_or_create_recurly_subscription(self):
        with mock.patch("frontend.apps.campaign.models.base."
                        "create_subscription") as m:
            # # Verify positive case
            m.return_value = mock.MagicMock()
            m.return_value.uuid = "abc"
            self.assertEqual(self.account.recurly_subscription_code, "")
            self.assertEqual(m.call_count, 0)
            cc_token = "sadfdsfddf"
            self.account.get_or_create_recurly_subscription(cc_token, True)
            self.assertEqual(self.account.recurly_subscription_code, "abc")

            # Verify call with prorate flag set to True
            next_first = next_first_of_month()
            m.assert_called_with(self.account, 9, prorate_date=next_first)
            self.assertEqual(m.call_count, 1)

            # Verify subscription_cache was set
            self.assertIsNotNone(self.account._subscription_cache)

        subscription_code = self.account.recurly_subscription_code
        self.account._subscription_cache = None

        with mock.patch("frontend.apps.campaign.models.base."
                        "get_subscription") as m:
            # # Verify new subscription is not created
            self.assertEqual(m.call_count, 0)
            self.account.get_or_create_recurly_subscription(cc_token)
            self.assertEqual(subscription_code,
                             self.account.recurly_subscription_code)
            self.assertEqual(m.call_count, 1)
            m.assert_called_with(self.account.recurly_subscription_code)
            # Verify subscription_cache was set
            self.assertIsNotNone(self.account._subscription_cache)

            # Verify subscription_cache is used
            self.account.get_or_create_recurly_subscription(cc_token)
            self.assertEqual(m.call_count, 1)

    def test_create(self):
        profile = AccountProfileFactory()
        name = "TestCase Org"
        user = RevUpUserFactory.create()
        seat_count = 45

        account = Account.create(profile, name, user, seat_count)
        self.assertEqual(account.account_head, user)
        self.assertEqual(account.organization_name, name)
        self.assertIsNotNone(account.account_user)
        self.assertIn("account_user_", account.account_user.username)
        self.assertEqual(account.seat_count, seat_count)
        self.assertEqual(account.account_profile, profile)
        self.assertEqual(account.current_skin, profile.skins.get_default())
        assert account.account_key is not None
        assert len(account.account_key) == ACCOUNT_KEY_LENGTH

        # Verify the ContactSets were created for the Account
        self.assertEqual(ContactSet.objects.filter(
            account=account, user=user, title="All Contacts").count(), 1)
        self.assertEqual(ContactSet.objects.filter(
            account=account, user=account.account_user,
            title="All Contacts").count(), 1)

    def test_create_seat(self):
        with mock.patch("frontend.apps.campaign.models.base.Seat.create") as m:
            # Verify case for regular user
            inviter = RevUpUserFactory()
            user = RevUpUserFactory()
            self.account.create_seat(user, inviting_user=inviter)
            self.assertEqual(repr(mock.call(
                user, self.account, None,
                self.account.roles.filter(is_a_seat_default=True),
                state=Seat.States.PENDING, seat_type=Seat.Types.TEMPORARY,
                inviting_user=inviter)), repr(m.call_args))

            # Verify with account head
            m.reset_mock()
            self.account.create_seat(self.account.account_head)
            m.assert_called_once_with(
                self.account.account_head, self.account,
                Seat.FundraiserPermissions.all() + Seat.AdminPermissions.all(),
                None, inviting_user=None, seat_type=Seat.Types.PERMANENT,
                state=Seat.States.ACTIVE)

            ## Verify with custom group names
            m.reset_mock()
            group_names = ["test1", "test2"]
            self.account.create_seat(user, permissions=group_names)
            m.assert_called_once_with(
                user, self.account, group_names, None,
                inviting_user=None, seat_type=Seat.Types.TEMPORARY,
                state=Seat.States.PENDING)

    def test_eq(self):
        campaign1 = PoliticalCampaignFactory()
        campaign2 = PoliticalCampaignFactory()
        account1 = Account.objects.get(id=campaign1.id)
        account2 = Account.objects.get(id=campaign2.id)

        self.assertEqual(account1, campaign1)
        self.assertEqual(account1, account1)
        self.assertEqual(account2, campaign2)
        self.assertEqual(account2, account2)
        self.assertEqual(campaign1, account1)
        self.assertEqual(campaign2, account2)

        self.assertNotEqual(account1, campaign2)
        self.assertNotEqual(account1, account2)
        self.assertNotEqual(campaign1, account2)
        self.assertNotEqual(account2, account1)

        self.assertNotEqual(account1, account1.id)
        self.assertNotEqual(account1, "abc")
        self.assertNotEqual(campaign1, "abc")

    def test_active_seats(self):
        SeatFactory(state=Seat.States.PENDING, account=self.account)
        SeatFactory(state=Seat.States.ACTIVE, account=self.account)
        SeatFactory(state=Seat.States.REVOKED, account=self.account)
        SeatFactory(state=Seat.States.ACTIVE)
        # 3 seats because one is automatically created for the account head
        self.assertEqual(self.account.active_seats(), 3)
        self.assertEqual(self.account.seats.count(), 4)
        SeatFactory(state=Seat.States.PENDING, account=self.account)
        self.assertEqual(self.account.active_seats(), 4)
        SeatFactory(state=Seat.States.REVOKED, account=self.account)
        self.assertEqual(self.account.active_seats(), 4)

    def test_available_seats(self):
        self.account.seat_count_override = 3
        SeatFactory(state=Seat.States.PENDING, account=self.account)
        SeatFactory(state=Seat.States.ACTIVE, account=self.account)
        SeatFactory(state=Seat.States.REVOKED, account=self.account)
        self.assertEqual(self.account.available_seats(), 1)
        SeatFactory(state=Seat.States.REVOKED, account=self.account)
        self.assertEqual(self.account.available_seats(), 1)
        SeatFactory(state=Seat.States.ACTIVE, account=self.account)
        self.assertEqual(self.account.available_seats(), 0)
        # Go into negative territory, should still be 0
        SeatFactory(state=Seat.States.ACTIVE, account=self.account)
        self.assertEqual(self.account.available_seats(), 0)

    @patch("frontend.apps.campaign.models.base.date")
    def test_contract_renews(self, m1):
        m1.today = mock.MagicMock(return_value=datetime.date(2017, 11, 15))
        product = self.account.account_profile.product
        product.contract_term_months = 6
        product.save()
        start_date = datetime.date(2017, 10, 12)
        self.account.start_date = start_date

        # Verify a simple case of 6 months in the future
        assert self.account.contract_renews == datetime.date(2018, 4, 12)

        # Verify a more advanced case where the contract has been renewed
        m1.today.return_value = datetime.date(2018, 8, 15)
        assert self.account.contract_renews == datetime.date(2018, 10, 12)

    def test_get_default_analysis_config(self):
        account_profile = self.account.account_profile
        default_analysis_profile = \
            account_profile.default_analysis_profiles.first()
        self.account.analysis_configs.clear()
        ac1 = AnalysisConfigFactory(
            account=self.account,
            analysis_profile=default_analysis_profile)
        ac2 = AnalysisConfigFactory(
            account=self.account,
            analysis_profile=default_analysis_profile)
        ac_link1 = self.account.analysis_configs.through.objects.create(
            account=self.account, analysis_config=ac1, is_default=True)
        ac_link2 = self.account.analysis_configs.through.objects.create(
            account=self.account, analysis_config=ac2)

        # ac_link1 was set to default, so ac1 should be returned
        ac = self.account.get_default_analysis_config()
        self.assertEqual(ac, ac1)

        # If there is no explicit default, then the default should be
        # one of ac1 or 2
        ac_link1.is_default = False
        ac_link1.save()
        ac = self.account.get_default_analysis_config()
        self.assertIn(ac, (ac1, ac2))

        # Verify the config from the account profile is selected otherwise
        ac3 = AnalysisConfigFactory(
            account=self.account,
            analysis_config_template= \
                default_analysis_profile.default_config_template,
            analysis_profile=default_analysis_profile)
        self.account.analysis_configs.clear()
        ac = self.account.get_default_analysis_config()
        self.assertEqual(ac, ac3)

        # One more sanity test
        ac_link2.is_default = True
        ac_link2.save()
        # ac_link2 was set to default, so ac2 should be returned
        ac = self.account.get_default_analysis_config()
        self.assertEqual(ac, ac2)

    def test_get_skins(self):
        ## Verify 'get_skins' returns the correct skins
        profile = self.account.account_profile
        skin1 = SkinSettingsFactory(account=self.account)
        skin2 = SkinSettingsFactory()
        skin3 = SkinSettingsFactory()

        # Make skin2 into a shared skin
        AccountSkinSettingsLink.objects.create(
            account_profile=profile,
            skin=skin2)

        skins = list(self.account.get_skins())
        self.assertEqual(len(skins), 2)
        self.assertIn(skin1, skins)
        self.assertIn(skin2, skins)
        self.assertNotIn(skin3, skins)

    def test_default_icon_text(self):
        default = "Client"
        ## Verify simple cases
        # Verify initials case
        self.account.organization_name = "Homer J Simpson"
        self.assertEqual(self.account.default_icon_text, "HJS")
        # Verify small, single title
        del self.account._default_icon_text
        self.account.organization_name = "DGA"
        self.assertEqual(self.account.default_icon_text, "DGA")
        # Verify blank title is default
        del self.account._default_icon_text
        self.account.organization_name = ""
        self.assertEqual(self.account.default_icon_text, default)

        ## Verify more edgy cases
        # Verify caching
        self.account.organization_name = "Al R J Kl Smith"
        self.assertEqual(self.account.default_icon_text, default)
        # Many initials
        del self.account._default_icon_text
        self.assertEqual(self.account.default_icon_text, "ARJKS")
        # Too many initials
        del self.account._default_icon_text
        self.account.organization_name = "Al R J Kl Fn G Smith"
        self.assertEqual(self.account.default_icon_text, default)
        # Too long single word
        del self.account._default_icon_text
        self.account.organization_name = "Testing"
        self.assertEqual(self.account.default_icon_text, default)

    def test_generate_account_key_1(self):
        """Create an account and test that the account key is automatically
        populated
        """
        profile = AccountProfileFactory()
        name = "Test Case 1 Of Name 9$2^%5 Blah Blah Blah"
        user = RevUpUserFactory.create()

        # Create an account and verify that the field `account_key`
        # was auto-populated
        account = Account.create(profile, name, user)
        assert account.account_key is not None
        # Verify that the account key does not have upper case letters
        assert account.account_key.upper() == account.account_key
        # Verify that there are no spaces in the account key
        assert ' ' not in account.account_key
        # Verify that the length of account_key is same as ACCOUNT_KEY_LENGTH
        assert len(account.account_key) == ACCOUNT_KEY_LENGTH
        # Verify that the first letter in the account key indicates the
        # account type
        assert profile.account_type.upper() == account.account_key[0]
        # Verify that all the characters in account key are either upper case
        # ascii or digits
        expected_chars = string.ascii_uppercase + string.digits
        assert all(
            [_char in expected_chars for _char in set(account.account_key)])

    def test_generate_account_key_2(self):
        """Test what the account key looks like when an organization name has
        all special characters and unicode (non-ascii) characters.
        """
        profile = AccountProfileFactory()
        name = "*&^%$#@$%^&*(&^%$#$%^&*&^%$éñ"
        user = RevUpUserFactory.create()

        account = Account.create(profile, name, user)

        # Verify that none of the characters in organization name (where all
        # characters are special characters or unicode characters) are in the
        # account key
        assert any([each_char in account.account_key
                    for each_char in account.organization_name]) is False

        # Verify that the length of account_key is same as ACCOUNT_KEY_LENGTH
        assert len(account.account_key) == ACCOUNT_KEY_LENGTH


class AccountTests2(AuthorizedUserTestCase):
    def setUp(self):
        super().setUp()
        # Setup a request mimicking a recurly webhook
        request_factory = RequestFactory()
        self.recurly_webhook_url = reverse('recurly_webhook')
        self.request = request_factory.post(self.recurly_webhook_url)
        self.request.user = AnonymousUser()
        # Choose a random ip from the list of known ips and assign it to the
        # request
        self.request.META['REMOTE_ADDR'] = settings.RECURLY_NOTIFICATION_IPS[
            randint(0, 5)]
        self.recurly_webhook = RecurlyWebhook.as_view()

    @patch('frontend.apps.core.views.recurly.get_revup_account')
    @patch('frontend.apps.core.views.recurly.objects_for_push_notification')
    def test_account_deactivation_1(self, push_notification_m,
                                    get_revup_account_m):
        # Setup a fake failed_payment_notification and an active account
        failed_payment_notification = {
            'type': 'failed_payment_notification',
            'account': MagicMock()
        }
        push_notification_m.return_value = failed_payment_notification
        account = self.user.current_seat.account
        get_revup_account_m.return_value = account

        # Verify that the account is active before recurly notification
        assert account.active is True

        response = self.recurly_webhook(self.request)
        assert 200 == response.status_code

        # Verify that the account is inactive after receiving recurly
        # notification
        assert account.active is False

    @patch('frontend.apps.core.views.recurly.get_revup_account')
    @patch('frontend.apps.core.views.recurly.objects_for_push_notification')
    def test_account_deactivation_2(self, push_notification_m,
                                    get_revup_account_m):
        # Setup a fake expired_subscription_notification and an active account
        expired_subscription_notification = {
            'type': 'expired_subscription_notification',
            'account': MagicMock()
        }
        push_notification_m.return_value = expired_subscription_notification
        account = self.user.current_seat.account
        get_revup_account_m.return_value = account

        # Verify that the account is active before recurly notification
        assert account.active is True

        response = self.recurly_webhook(self.request)
        assert 200 == response.status_code

        # Verify that the account is inactive after receiving recurly
        # notification
        assert account.active is False

    @patch('frontend.apps.core.views.recurly.get_revup_account')
    @patch('frontend.apps.core.views.recurly.objects_for_push_notification')
    def test_account_deactivation_3(self, push_notification_m,
                                    get_revup_account_m):
        # Setup a fake canceled_account_notification and an active account
        canceled_account_notification = {
            'type': 'canceled_account_notification',
            'account': MagicMock()
        }
        push_notification_m.return_value = canceled_account_notification
        account = self.user.current_seat.account
        get_revup_account_m.return_value = account

        # Verify that the account is active before recurly notification
        assert account.active is True

        response = self.recurly_webhook(self.request)
        assert 200 == response.status_code

        # Verify that the account is inactive after receiving recurly
        # notification
        assert account.active is False

    @patch('frontend.apps.core.views.recurly.get_revup_account')
    @patch('frontend.apps.core.views.recurly.objects_for_push_notification')
    def test_account_reactivation_1(self, push_notification_m,
                                  get_revup_account_m):
        """Tests whether an account gets reactivated when
        reactivated_account_notification is received
        """
        # Setup a fake reactivated_account_notification and a deactivated
        # account
        reactivated_account_notification = {
            'type': 'reactivated_account_notification',
            'account': MagicMock()
        }
        push_notification_m.return_value = reactivated_account_notification

        # Setup a deactivated account
        account = self.user.current_seat.account
        account.active = False
        get_revup_account_m.return_value = account

        # Verify that the account is inactive before recurly notification
        assert account.active is False

        response = self.recurly_webhook(self.request)
        assert 200 == response.status_code

        # Verify that the account is active after receiving recurly
        # notification
        assert account.active is True

    @patch('frontend.apps.core.views.recurly.get_revup_account')
    @patch('frontend.apps.core.views.recurly.objects_for_push_notification')
    def test_account_reactivation_2(self, push_notification_m,
                                  get_revup_account_m):
        """Tests whether an account gets reactivated when
        successful_payment_notification is received and account is inactive
        """
        # Setup a fake reactivated_account_notification and a deactivated
        # account
        reactivated_account_notification = {
            'type': 'successful_payment_notification',
            'account': MagicMock()
        }
        push_notification_m.return_value = reactivated_account_notification

        # Setup a deactivated account
        account = self.user.current_seat.account
        account.active = False
        get_revup_account_m.return_value = account

        # Verify that the account is inactive before recurly notification
        assert account.active is False

        response = self.recurly_webhook(self.request)
        assert 200 == response.status_code

        # Verify that the account is active after receiving recurly
        # notification
        assert account.active is True

    @patch('frontend.apps.core.views.recurly.get_revup_account')
    @patch('frontend.apps.core.views.recurly.objects_for_push_notification')
    def test_account_reactivation_3(self, push_notification_m,
                                    get_revup_account_m):
        """Tests whether an account reactivate() ignores
        successful_payment_notification when the account is already active
        """
        # Setup a fake reactivated_account_notification and an activated
        # account
        reactivated_account_notification = {
            'type': 'successful_payment_notification',
            'account': MagicMock()
        }
        push_notification_m.return_value = reactivated_account_notification
        account = self.user.current_seat.account
        get_revup_account_m.return_value = account

        # Verify that the account is inactive before recurly notification
        assert account.active is True

        response = self.recurly_webhook(self.request)
        assert 200 == response.status_code

        # Verify that the account is active after receiving recurly
        # notification
        assert account.active is True


class ContributionTemplateTagTests(TestCase):
    def test_fuzzyintcomma(self):
        # different lengths
        self.assertEqual(fuzzyintcomma('1'), '1')
        self.assertEqual(fuzzyintcomma('12'), '12')
        self.assertEqual(fuzzyintcomma('123'), '123')
        self.assertEqual(fuzzyintcomma('1234'), '1,234')
        self.assertEqual(fuzzyintcomma('12345'), '12,345')
        self.assertEqual(fuzzyintcomma('123456'), '123,456')
        self.assertEqual(fuzzyintcomma('1234567'), '1,234,567')
        self.assertEqual(fuzzyintcomma('12345678'), '12,345,678')
        self.assertEqual(fuzzyintcomma('123456789'), '123,456,789')
        self.assertEqual(fuzzyintcomma('1234567890'), '1,234,567,890')

        # commas already present
        self.assertEqual(fuzzyintcomma('1,234'), '1,234')
        self.assertEqual(fuzzyintcomma('12,345'), '12,345')
        self.assertEqual(fuzzyintcomma('123,456'), '123,456')
        self.assertEqual(fuzzyintcomma('1,234,567'), '1,234,567')
        self.assertEqual(fuzzyintcomma('12,345,678'), '12,345,678')
        self.assertEqual(fuzzyintcomma('123,456,789'), '123,456,789')
        self.assertEqual(fuzzyintcomma('1,234,567,890'), '1,234,567,890')

        # decimals
        self.assertEqual(fuzzyintcomma('.1234567890'), '.1234567890')
        self.assertEqual(fuzzyintcomma('1.234567890'), '1.234567890')
        self.assertEqual(fuzzyintcomma('12.34567890'), '12.34567890')
        self.assertEqual(fuzzyintcomma('123.4567890'), '123.4567890')
        self.assertEqual(fuzzyintcomma('1234.567890'), '1,234.567890')
        self.assertEqual(fuzzyintcomma('12345.67890'), '12,345.67890')
        self.assertEqual(fuzzyintcomma('123456.7890'), '123,456.7890')
        self.assertEqual(fuzzyintcomma('1234567.890'), '1,234,567.890')
        self.assertEqual(fuzzyintcomma('12345678.90'), '12,345,678.90')
        self.assertEqual(fuzzyintcomma('123456789.0'), '123,456,789.0')
        self.assertEqual(fuzzyintcomma('1234567890.'), '1,234,567,890.')

        # misc values
        self.assertEqual(fuzzyintcomma('$12345678.90'), '$12,345,678.90')
        self.assertEqual(fuzzyintcomma('1.234,567_890'), '1.234,567_890')
        self.assertEqual(fuzzyintcomma('12345   67890'), '12,345   67,890')
        self.assertEqual(fuzzyintcomma('123456,789'), '123,456,789')
        self.assertEqual(fuzzyintcomma('x12345x67890x'), 'x12,345x67,890x')

        # real data
        self.assertEqual(fuzzyintcomma('$1000000+'), '$1,000,000+')
        self.assertEqual(fuzzyintcomma('0'), '0')
        self.assertEqual(fuzzyintcomma('1000 to 2499'), '1,000 to 2,499')
        self.assertEqual(fuzzyintcomma('10000 +'), '10,000 +')
        self.assertEqual(fuzzyintcomma('10000 OR MORE'), '10,000 OR MORE')
        self.assertEqual(fuzzyintcomma('100000 +'), '100,000 +')
        self.assertEqual(fuzzyintcomma('1974'), '1,974')
        self.assertEqual(fuzzyintcomma('1977'), '1,977')
        self.assertEqual(fuzzyintcomma('3M'), '3M')
        self.assertEqual(fuzzyintcomma('500  or  more'), '500  or  more')
        self.assertEqual(fuzzyintcomma('1000 – 2499'), '1,000 – 2,499')
        self.assertEqual(fuzzyintcomma('100000–249999'), '100,000–249,999')
        self.assertEqual(fuzzyintcomma('250 – 499'), '250 – 499')

