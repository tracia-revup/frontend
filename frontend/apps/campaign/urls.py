from django.conf.urls import url

from .views import AccountContributionUploadView


urlpatterns = [
    url(r'^/contrib_upload/?$', AccountContributionUploadView.as_view(),
        name='account_contrib_upload'),
]
