# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0011_university'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='is_collaborative',
        ),
    ]
