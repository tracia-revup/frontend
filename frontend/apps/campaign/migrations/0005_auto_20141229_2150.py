# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0004_politicalcampaign_opponent'),
    ]

    operations = [
        migrations.AddField(
            model_name='politicalcampaign',
            name='banner_name',
            field=models.CharField(max_length=128, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='politicalcampaign',
            name='icon_name',
            field=models.CharField(max_length=128, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='politicalcampaign',
            name='opponent_icon_name',
            field=models.CharField(max_length=128, blank=True),
            preserve_default=True,
        ),
    ]
