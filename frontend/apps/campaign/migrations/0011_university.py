# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion
from localflavor.us.models import USStateField


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0010_remove_event_campaign'),
    ]

    operations = [
        migrations.CreateModel(
            name='University',
            fields=[
                ('account_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='campaign.Account', on_delete=django.db.models.deletion.CASCADE)),
                ('address', models.CharField(max_length=512, blank=True)),
                ('city', models.CharField(max_length=128, blank=True)),
                ('state', USStateField(blank=True, max_length=2, choices=[('AL', 'Alabama'), ('AK', 'Alaska'), ('AS', 'American Samoa'), ('AZ', 'Arizona'), ('AR', 'Arkansas'), ('AA', 'Armed Forces Americas'), ('AE', 'Armed Forces Europe'), ('AP', 'Armed Forces Pacific'), ('CA', 'California'), ('CO', 'Colorado'), ('CT', 'Connecticut'), ('DE', 'Delaware'), ('DC', 'District of Columbia'), ('FL', 'Florida'), ('GA', 'Georgia'), ('GU', 'Guam'), ('HI', 'Hawaii'), ('ID', 'Idaho'), ('IL', 'Illinois'), ('IN', 'Indiana'), ('IA', 'Iowa'), ('KS', 'Kansas'), ('KY', 'Kentucky'), ('LA', 'Louisiana'), ('ME', 'Maine'), ('MD', 'Maryland'), ('MA', 'Massachusetts'), ('MI', 'Michigan'), ('MN', 'Minnesota'), ('MS', 'Mississippi'), ('MO', 'Missouri'), ('MT', 'Montana'), ('NE', 'Nebraska'), ('NV', 'Nevada'), ('NH', 'New Hampshire'), ('NJ', 'New Jersey'), ('NM', 'New Mexico'), ('NY', 'New York'), ('NC', 'North Carolina'), ('ND', 'North Dakota'), ('MP', 'Northern Mariana Islands'), ('OH', 'Ohio'), ('OK', 'Oklahoma'), ('OR', 'Oregon'), ('PA', 'Pennsylvania'), ('PR', 'Puerto Rico'), ('RI', 'Rhode Island'), ('SC', 'South Carolina'), ('SD', 'South Dakota'), ('TN', 'Tennessee'), ('TX', 'Texas'), ('UT', 'Utah'), ('VT', 'Vermont'), ('VI', 'Virgin Islands'), ('VA', 'Virginia'), ('WA', 'Washington'), ('WV', 'West Virginia'), ('WI', 'Wisconsin'), ('WY', 'Wyoming')])),
                ('banner', models.ImageField(upload_to='', blank=True)),
                ('banner_name', models.CharField(max_length=128, blank=True)),
                ('name', models.CharField(max_length=512, blank=True)),
                ('department', models.CharField(help_text='Office of Dean, Finance, etc.', max_length=256, blank=True)),
                ('ncaa_conference', models.CharField(help_text='Big Ten, Pac-12, etc.', max_length=256, verbose_name='NCAA Conference', blank=True)),
            ],
            options={
                'verbose_name': 'University/College',
            },
            bases=('campaign.account', models.Model),
        ),
    ]
