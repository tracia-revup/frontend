# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0021_auto_20160310_1022'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='prospect',
            unique_together=set([('event', 'contact')]),
        ),
        migrations.AlterUniqueTogether(
            name='prospectcollision',
            unique_together=set([('event', 'contact')]),
        ),
    ]
