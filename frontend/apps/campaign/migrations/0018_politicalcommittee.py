# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.contrib.postgres.fields


def create_committee_profile(apps, schema_editor):
    AccountProfile = apps.get_model("campaign", "AccountProfile")
    AccountProfile.objects.get_or_create(account_type="P",
                                         title="Political Committee")


def delete_committee_accounts(apps, schema_editor):
    PoliticalCommittee = apps.get_model("campaign", "PoliticalCommittee")
    for pc in PoliticalCommittee.objects.all():
        pc.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0017_account_profile'),
    ]

    operations = [
        migrations.CreateModel(
            name='PoliticalCommittee',
            fields=[
                ('account_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='campaign.Account', on_delete=django.db.models.deletion.CASCADE)),
                ('banner', models.ImageField(upload_to='', blank=True)),
                ('banner_name', models.CharField(max_length=128, blank=True)),
                ('party', models.CharField(max_length=30, verbose_name='political party', blank=True)),
                ('office_category', models.CharField(max_length=30, verbose_name='type of office', blank=True)),
                ('office', models.CharField(max_length=128, verbose_name='office sought', blank=True)),
                ('issues', django.contrib.postgres.fields.ArrayField(models.CharField(max_length=30, verbose_name='key platform issue'), default=[], size=None, verbose_name='key platform issues', blank=True)),
                ('committee_name', models.CharField(max_length=512, blank=True)),
                ('icon', models.ImageField(upload_to='', blank=True)),
                ('opponent_icon', models.ImageField(upload_to='', blank=True)),
                ('icon_name', models.CharField(max_length=128, blank=True)),
                ('opponent_icon_name', models.CharField(max_length=128, blank=True)),
            ],
            options={
                'verbose_name': 'Political Committee',
            },
            bases=('campaign.account', models.Model),
        ),
        migrations.RunPython(create_committee_profile,
                             reverse_code=delete_committee_accounts),
    ]
