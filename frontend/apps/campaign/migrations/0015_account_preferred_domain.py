# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0014_accounttask'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='preferred_domain',
            field=models.CharField(help_text="The client's preferred domain. E.g.: 'purdue.revup.com'. Do not include the 'http'", max_length=256, blank=True),
            preserve_default=True,
        ),
    ]
