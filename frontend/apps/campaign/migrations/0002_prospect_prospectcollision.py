# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion
import frontend.libs.mixins
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '__first__'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('campaign', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Prospect',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('soft_amt', models.DecimalField(default=0, verbose_name='soft contribution', max_digits=12, decimal_places=2, blank=True)),
                ('hard_amt', models.DecimalField(default=0, verbose_name='hard contribution', max_digits=12, decimal_places=2, blank=True)),
                ('in_amt', models.DecimalField(default=0, verbose_name='contributions in', max_digits=12, decimal_places=2, blank=True)),
                ('num_attending', models.PositiveIntegerField(default=0, verbose_name='number attending', blank=True)),
                ('notes', models.TextField(verbose_name='Prospect notes', blank=True)),
                ('contact', models.ForeignKey(to='contact.Contact', on_delete=django.db.models.deletion.CASCADE)),
                ('event', models.ForeignKey(to='campaign.Event', on_delete=django.db.models.deletion.CASCADE)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model, frontend.libs.mixins.SimpleUnicodeMixin),
        ),
        migrations.CreateModel(
            name='ProspectCollision',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('contact', models.ForeignKey(help_text='The attempted prospect that collided.', to='contact.Contact', on_delete=django.db.models.deletion.CASCADE)),
                ('event', models.ForeignKey(help_text='The Event these prospect belong to.', to='campaign.Event', on_delete=django.db.models.deletion.CASCADE)),
                ('external_prospect', models.ForeignKey(help_text='The Prospect that already existed when another contact of the same person was attempted to be used as an Prospect.', to='campaign.Prospect', on_delete=django.db.models.deletion.CASCADE)),
                ('user', models.ForeignKey(help_text='The user who attempted to add a colliding contact.', to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model, frontend.libs.mixins.SimpleUnicodeMixin),
        ),
    ]
