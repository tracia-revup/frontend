# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.contrib.postgres.fields


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0012_remove_event_is_collaborative'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='seat_count',
            field=models.PositiveIntegerField(default=0, help_text='Total number of seats', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='goal',
            field=models.PositiveIntegerField(default=0, verbose_name='fundraising event goal ($)', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='price_points',
            field=django.contrib.postgres.fields.ArrayField(models.PositiveIntegerField(verbose_name='fundraising price point ($)'), default=[], size=None, verbose_name='fundraising price points ($)', blank=True),
            preserve_default=True,
        ),
    ]
