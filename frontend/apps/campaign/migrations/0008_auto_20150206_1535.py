# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0007_auto_20150210_1212'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='politicalcampaignexternaldata',
            name='campaign',
        ),
        migrations.DeleteModel(
            name='PoliticalCampaignExternalData',
        ),
        migrations.AlterModelOptions(
            name='politicalcampaign',
            options={'verbose_name': 'Political Candidate'},
        ),

        ## Change event.campaign to event.account
        migrations.AddField(
            model_name='event',
            name='account',
            field=models.ForeignKey(to='campaign.Account', null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='campaign',
            field=models.ForeignKey(to='campaign.PoliticalCampaign',
                                    null=True,
                                    on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
    ]
