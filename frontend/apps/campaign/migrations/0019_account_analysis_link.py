# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0003_new_analysis_data'),
        ('campaign', '0018_politicalcommittee'),
    ]

    operations = [
        migrations.CreateModel(
            name='AccountAnalysisConfigLink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_default', models.BooleanField(default=False)),
                ('is_active', models.BooleanField(default=True)),
                ('account', models.ForeignKey(related_name='+', to='campaign.Account', on_delete=django.db.models.deletion.CASCADE)),
                ('analysis_config', models.ForeignKey(related_name='+', to='analysis.AnalysisConfig', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='account',
            name='analysis_configs',
            field=models.ManyToManyField(related_name='+', through='campaign.AccountAnalysisConfigLink', to='analysis.AnalysisConfig', blank=True),
            preserve_default=True,
        ),
    ]
