# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('campaign', '0013_auto_20150323_1431'),
    ]

    operations = [
        migrations.CreateModel(
            name='AccountTask',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('task_id', models.CharField(max_length=255)),
                ('task_type', models.CharField(default='BFI', max_length=3, db_index=True, choices=[('BFI', 'Bulk Fundraiser Import')])),
                ('account', models.ForeignKey(related_name='tasks', to='campaign.Account', on_delete=django.db.models.deletion.CASCADE)),
                ('user', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL, help_text='The user who started this task', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
