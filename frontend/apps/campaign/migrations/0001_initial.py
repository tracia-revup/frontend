# -*- coding: utf-8 -*-


import django.contrib.postgres.fields
from django.db import models, migrations
from localflavor.us.models import USStateField

import frontend.libs.mixins


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('event_name', models.CharField(max_length=100, verbose_name='fundraising event name')),
                ('event_date', models.DateTimeField(verbose_name='fundraising event date')),
                ('goal', models.PositiveIntegerField(default=0, verbose_name='fundraising event goal', blank=True)),
                ('price_points', django.contrib.postgres.fields.ArrayField(models.PositiveIntegerField(verbose_name='fundraising price point'), default=[], size=None, verbose_name='fundraising price points', blank=True)),
                ('is_collaborative', models.BooleanField(default=True, help_text='Are the Attendees of this Event visible and editable by all fundraisers of the Event?')),
            ],
            options={
            },
            bases=(models.Model, frontend.libs.mixins.SimpleUnicodeMixin),
        ),
        migrations.CreateModel(
            name='ExternalLink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('link_key', models.CharField(max_length=512)),
                ('system_name', models.CharField(max_length=3, choices=[('NGP', 'NGP')])),
                ('event', models.ForeignKey(related_name='external_links', to='campaign.Event', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model, frontend.libs.mixins.SimpleUnicodeMixin),
        ),
        migrations.CreateModel(
            name='PoliticalCampaign',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('campaign_name', models.CharField(max_length=128, blank=True)),
                ('first_name', models.CharField(max_length=30, verbose_name='Candidate First Name', blank=True)),
                ('last_name', models.CharField(max_length=30, verbose_name='Candidate Last Name')),
                ('gender', models.CharField(max_length=30, blank=True)),
                ('ethnicity', models.CharField(max_length=30, blank=True)),
                ('birthdate', models.DateField(null=True, blank=True)),
                ('birth_city', models.CharField(max_length=128, blank=True)),
                ('birth_state', USStateField(blank=True, max_length=2, choices=[('AL', 'Alabama'), ('AK', 'Alaska'), ('AS', 'American Samoa'), ('AZ', 'Arizona'), ('AR', 'Arkansas'), ('AA', 'Armed Forces Americas'), ('AE', 'Armed Forces Europe'), ('AP', 'Armed Forces Pacific'), ('CA', 'California'), ('CO', 'Colorado'), ('CT', 'Connecticut'), ('DE', 'Delaware'), ('DC', 'District of Columbia'), ('FL', 'Florida'), ('GA', 'Georgia'), ('GU', 'Guam'), ('HI', 'Hawaii'), ('ID', 'Idaho'), ('IL', 'Illinois'), ('IN', 'Indiana'), ('IA', 'Iowa'), ('KS', 'Kansas'), ('KY', 'Kentucky'), ('LA', 'Louisiana'), ('ME', 'Maine'), ('MD', 'Maryland'), ('MA', 'Massachusetts'), ('MI', 'Michigan'), ('MN', 'Minnesota'), ('MS', 'Mississippi'), ('MO', 'Missouri'), ('MT', 'Montana'), ('NE', 'Nebraska'), ('NV', 'Nevada'), ('NH', 'New Hampshire'), ('NJ', 'New Jersey'), ('NM', 'New Mexico'), ('NY', 'New York'), ('NC', 'North Carolina'), ('ND', 'North Dakota'), ('MP', 'Northern Mariana Islands'), ('OH', 'Ohio'), ('OK', 'Oklahoma'), ('OR', 'Oregon'), ('PA', 'Pennsylvania'), ('PR', 'Puerto Rico'), ('RI', 'Rhode Island'), ('SC', 'South Carolina'), ('SD', 'South Dakota'), ('TN', 'Tennessee'), ('TX', 'Texas'), ('UT', 'Utah'), ('VT', 'Vermont'), ('VI', 'Virgin Islands'), ('VA', 'Virginia'), ('WA', 'Washington'), ('WV', 'West Virginia'), ('WI', 'Wisconsin'), ('WY', 'Wyoming')])),
                ('graduate_schools', django.contrib.postgres.fields.ArrayField(models.CharField(max_length=128), default=[], size=None, blank=True)),
                ('undergraduate_schools', django.contrib.postgres.fields.ArrayField(models.CharField(max_length=128), default=[], size=None, blank=True)),
                ('high_schools', django.contrib.postgres.fields.ArrayField(models.CharField(max_length=128), default=[], size=None, blank=True)),
                ('party', models.CharField(max_length=30, verbose_name='political party', blank=True)),
                ('election', models.CharField(max_length=128, verbose_name='electoral race', blank=True)),
                ('office', models.CharField(max_length=128, verbose_name='office sought', blank=True)),
                ('office_category', models.CharField(max_length=30, verbose_name='type of office', blank=True)),
                ('district', models.CharField(max_length=30, verbose_name='district or town', blank=True)),
                ('district_number', models.CharField(max_length=30, blank=True)),
                ('issues', django.contrib.postgres.fields.ArrayField(models.CharField(max_length=30, verbose_name='key platform issue'), default=[], size=None, verbose_name='key platform issues', blank=True)),
                ('state', USStateField(blank=True, max_length=2, choices=[('AL', 'Alabama'), ('AK', 'Alaska'), ('AS', 'American Samoa'), ('AZ', 'Arizona'), ('AR', 'Arkansas'), ('AA', 'Armed Forces Americas'), ('AE', 'Armed Forces Europe'), ('AP', 'Armed Forces Pacific'), ('CA', 'California'), ('CO', 'Colorado'), ('CT', 'Connecticut'), ('DE', 'Delaware'), ('DC', 'District of Columbia'), ('FL', 'Florida'), ('GA', 'Georgia'), ('GU', 'Guam'), ('HI', 'Hawaii'), ('ID', 'Idaho'), ('IL', 'Illinois'), ('IN', 'Indiana'), ('IA', 'Iowa'), ('KS', 'Kansas'), ('KY', 'Kentucky'), ('LA', 'Louisiana'), ('ME', 'Maine'), ('MD', 'Maryland'), ('MA', 'Massachusetts'), ('MI', 'Michigan'), ('MN', 'Minnesota'), ('MS', 'Mississippi'), ('MO', 'Missouri'), ('MT', 'Montana'), ('NE', 'Nebraska'), ('NV', 'Nevada'), ('NH', 'New Hampshire'), ('NJ', 'New Jersey'), ('NM', 'New Mexico'), ('NY', 'New York'), ('NC', 'North Carolina'), ('ND', 'North Dakota'), ('MP', 'Northern Mariana Islands'), ('OH', 'Ohio'), ('OK', 'Oklahoma'), ('OR', 'Oregon'), ('PA', 'Pennsylvania'), ('PR', 'Puerto Rico'), ('RI', 'Rhode Island'), ('SC', 'South Carolina'), ('SD', 'South Dakota'), ('TN', 'Tennessee'), ('TX', 'Texas'), ('UT', 'Utah'), ('VT', 'Vermont'), ('VI', 'Virgin Islands'), ('VA', 'Virginia'), ('WA', 'Washington'), ('WV', 'West Virginia'), ('WI', 'Wisconsin'), ('WY', 'Wyoming')])),
                ('opponent_committees', django.contrib.postgres.fields.ArrayField(models.CharField(max_length=255), default=[], size=None, blank=True)),
                ('banner', models.ImageField(upload_to='', blank=True)),
                ('icon', models.ImageField(upload_to='', blank=True)),
                ('opponent_icon', models.ImageField(upload_to='', blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model, frontend.libs.mixins.SimpleUnicodeMixin),
        ),
        migrations.CreateModel(
            name='PoliticalCampaignExternalData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('source', models.CharField(max_length=30)),
                ('external_data', models.BinaryField()),
                ('campaign', models.ForeignKey(related_name='external_data', to='campaign.PoliticalCampaign', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model, frontend.libs.mixins.SimpleUnicodeMixin),
        ),
        migrations.AddField(
            model_name='event',
            name='campaign',
            field=models.ForeignKey(to='campaign.PoliticalCampaign', on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
    ]
