# -*- coding: utf-8 -*-


from django.db import migrations


def move_campaign_forward(apps, schema_editor):
    Event = apps.get_model("campaign", "Event")
    for event in Event.objects.all():
        event.account_id = event.campaign_id
        event.save()


def move_campaign_backward(apps, schema_editor):
    Event = apps.get_model("campaign", "Event")
    for event in Event.objects.all():
        if not event.account_id:
            print(event)
            event.delete()
            continue
        event.campaign_id = event.account_id
        event.save()


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0008_auto_20150206_1535'),
    ]
    operations = [
        # Migrate campaign over to account
        migrations.RunPython(move_campaign_forward,
                             reverse_code=move_campaign_backward),

    ]