# -*- coding: utf-8 -*-


from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0015_account_preferred_domain'),
    ]

    operations = [
        migrations.AddField(
            model_name='prospect',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 28, 15, 25, 47, 433968), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='prospect',
            name='updated',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 28, 15, 25, 54, 617921), auto_now=True),
            preserve_default=False,
        ),
    ]
