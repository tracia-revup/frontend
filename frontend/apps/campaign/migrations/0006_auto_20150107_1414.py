# -*- coding: utf-8 -*-

import datetime

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


def is_campaign_admin(user, campaign):
    return user.groups.filter(name__in=[settings.ACCOUNT_ADMIN_GROUP_NAME,
                                        "Campaign Admin"]).exists() and \
           user.campaigns.first().pk == campaign.pk


def create_accounts(apps, schema_editor):
    """Create the account for any existing campaigns"""
    PoliticalCampaign = apps.get_model("campaign", "PoliticalCampaign")
    Account = apps.get_model("campaign", "Account")
    for campaign in PoliticalCampaign.objects.all():
        campaign.account_ptr = Account.objects.create(
            active=True,
            organization_name=" ".join((campaign.first_name,
                                       campaign.last_name, "Campaign")),
            start_date=datetime.datetime.now().date(),
        )
        campaign.save()


def assign_account_heads(apps, schema_editor):
    """Assign the current campaign admins as the account heads."""
    PoliticalCampaign = apps.get_model("campaign", "PoliticalCampaign")
    RevUpUser = apps.get_model('authorize', "RevUpUser")
    for campaign in PoliticalCampaign.objects.all():
        for user in RevUpUser.objects.filter(campaigns__pk=campaign.pk):
            if is_campaign_admin(user, campaign):
                campaign.account_head = user
                campaign.save()
                break


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('campaign', '0005_auto_20141229_2150'),
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('account_head', models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=django.db.models.deletion.CASCADE)),
                ('active', models.BooleanField(default=False)),
                ('organization_name', models.CharField(max_length=512, blank=True)),
                ('start_date', models.DateField()),
                ('recurly_plan_code', models.CharField(max_length=50, blank=True)),
                ('recurly_account_code', models.CharField(max_length=32, blank=True)),
                ('recurly_subscription_code', models.CharField(max_length=32, blank=True)),
                ('seat_price', models.DecimalField(default=0, help_text='Price per seat', max_digits=12, decimal_places=2, blank=True)),
                ('seat_count', models.PositiveIntegerField(default=0, help_text='Total number of seats', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='politicalcampaign',
            name='account_ptr',
            field=models.OneToOneField(null=True, to='campaign.Account', on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.RunPython(create_accounts,
                             reverse_code = lambda x, y: True),
        migrations.RemoveField(
            model_name='politicalcampaign',
            name='id',
        ),
        migrations.AlterField(
            model_name='politicalcampaign',
            name='account_ptr',
            field=models.OneToOneField(parent_link=True, auto_created=True,
                                       primary_key=True, serialize=False,
                                       to='campaign.Account', on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.RunPython(assign_account_heads,
                             reverse_code=lambda x, y: True),
    ]

    def unapply(self, project_state, schema_editor, collect_sql=False):
        """Django couldn't figure out how to do a reverse migration, but
          I think it's important to have one, so I added it here.

          The added Savepoint is necessary because, depending on how far
          forward you have migrated, the drop contraints might fail.
          This savepoint deals with that.
        """
        reverse_operations = [
            'ALTER TABLE "campaign_politicalcampaign" RENAME COLUMN "account_ptr_id" TO "id"',
            'SAVEPOINT one',
            'ALTER TABLE "campaign_politicalcampaignexternaldata" DROP CONSTRAINT "D460bc8f360375edf63b63a61fe0005e"',
            'ALTER TABLE "campaign_event" DROP CONSTRAINT "D75c7c4ef1258593b4ae511341f8d111"',
            'ALTER TABLE "campaign_politicalcampaign" DROP CONSTRAINT campaign_account_ptr_id_5144407b04ac8289_fk_campaign_account_id',
            'ALTER TABLE "campaign_politicalcampaign" DROP CONSTRAINT campaign_politicalcampaign_account_ptr_id_5144407b04ac8289_pk',
            'ALTER TABLE "campaign_politicalcampaign" ADD PRIMARY KEY (id)',
            'ALTER TABLE "campaign_event" ADD CONSTRAINT "c_campaign_id_4340c25ffe6c5b09_fk_campaign_politicalcampaign_id" FOREIGN KEY ("campaign_id") '
            'REFERENCES "campaign_politicalcampaign" ("id") DEFERRABLE INITIALLY DEFERRED;',
            'ALTER TABLE "campaign_politicalcampaignexternaldata" ADD CONSTRAINT "c_campaign_id_2ac810cb2366e1b1_fk_campaign_politicalcampaign_id" '
            'FOREIGN KEY ("campaign_id") REFERENCES "campaign_politicalcampaign" ("id") DEFERRABLE INITIALLY DEFERRED;'
            'DROP TABLE "campaign_account"',
        ]
        for op in reverse_operations:
            print(op)
            try:
                schema_editor.execute(op)
            except Exception as err:
                print(err)
                if "does not exist" in str(err):
                    print("ROLLBACK")
                    schema_editor.execute("ROLLBACK TO SAVEPOINT one")
                else:
                    raise
