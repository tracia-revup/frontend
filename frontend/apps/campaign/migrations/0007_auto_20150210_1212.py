# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion

from frontend.apps.campaign.models.external_data import (
    ExternalDataSource as externalDataSource)

def external_source_setup(apps, schema_editor):
    """Set up default external data source types, if not already created"""

    ExternalDataSource = apps.get_model("campaign", "ExternalDataSource")
    ExternalDataConfig = apps.get_model("campaign", "ExternalDataConfig")
    affiliations = externalDataSource.Affiliations
    types = externalDataSource.Products

    system_sources = [
        {"name": "CMDI",
         "affiliation": affiliations.REPUBLICAN,
         "product_type": types.POLITICAL,
         "index": 1,
         },
        {"name": "RedCurve",
         "affiliation": affiliations.REPUBLICAN,
         "product_type": types.POLITICAL,
         "index": 2,
         },
        {"name": "NGP",
         "affiliation": affiliations.DEMOCRATIC,
         "product_type": types.POLITICAL,
         "index": 1,
         },
        {"name": "ActBlue",
         "affiliation": affiliations.DEMOCRATIC,
         "product_type": types.POLITICAL,
         "index": 2,
         },
        {"name": "Democracy Engine",
         "affiliation": affiliations.DEMOCRATIC,
         "product_type": types.POLITICAL,
         "index": 3,
         },
        {"name": "NationBuilder",
         "affiliation": affiliations.DEMOCRATIC,
         "product_type": types.POLITICAL,
         "index": 4,
         },
        {"name": "Aristotle",
         "affiliation": affiliations.NONE,
         "product_type": types.POLITICAL,
         "index": 1,
         },
        {"name": "Other",
         "affiliation": affiliations.NONE,
         "product_type": types.POLITICAL,
         "index": 2,
         },
    ]

    for system_source in system_sources:
        source, created = ExternalDataSource.objects.get_or_create(
            name=system_source["name"], index=system_source["index"],
            affiliation=system_source["affiliation"],
            product_type=system_source["product_type"],
        )
        if created:
            source.save()
        if not source.default_config and source.name != "Other":
            name = source.name + " default"
            config = ExternalDataConfig.objects.create(
                source=source, name=name,
                notes=name + " configuration"
            )
            config.save()
            source.default_config = config
            source.save()


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0006_auto_20150107_1414'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExternalData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, blank=True)),
                ('notes', models.TextField()),
                ('static_key', models.CharField(max_length=128, blank=True)),
                ('active', models.BooleanField(default=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('account', models.ForeignKey(related_name='external_entries', to='campaign.Account', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ExternalDataConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, blank=True)),
                ('notes', models.TextField()),
                ('active', models.BooleanField(default=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('account', models.ForeignKey(related_name='data_configs', to='campaign.Account', null=True, on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ExternalDataInstance',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('original_data', models.BinaryField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('file_name', models.CharField(max_length=128, blank=True)),
                ('status', models.CharField(default='UPLOAD', max_length=16, blank=True, choices=[('UPLOAD', 'Uploaded'), ('PROCESS', 'In Process'), ('ERROR', 'Error'), ('READY', 'Ready'), ('ARCH', 'Archived'), ('DEL', 'Deleted')])),
                ('config', models.ForeignKey(related_name='+', to='campaign.ExternalDataConfig', on_delete=django.db.models.deletion.CASCADE)),
                ('data_set', models.ForeignKey(related_name='instances', to='campaign.ExternalData', on_delete=django.db.models.deletion.CASCADE)),
                ('external_entry', models.ForeignKey(to='campaign.ExternalData', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ExternalDataSource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, blank=True)),
                ('index', models.PositiveSmallIntegerField()),
                ('affiliation', models.CharField(default='', max_length=2, blank=True, choices=[('D', 'Democratic'), ('R', 'Republican'), ('', '')])),
                ('product_type', models.CharField(default='', max_length=2, blank=True, choices=[('P', 'Political'), ('A', 'Academic'), ('N', 'Non-Profit'), ('', '')])),
                ('active', models.BooleanField(default=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('default_config', models.OneToOneField(related_name='default_config', null=True, to='campaign.ExternalDataConfig', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='externaldataconfig',
            name='source',
            field=models.ForeignKey(related_name='+', to='campaign.ExternalDataSource', on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='externaldata',
            name='config',
            field=models.ForeignKey(related_name='+', to='campaign.ExternalDataConfig', on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.RunPython(external_source_setup,
                             reverse_code=lambda x, y: True),
    ]
