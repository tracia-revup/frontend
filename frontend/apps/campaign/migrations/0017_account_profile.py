# -*- coding: utf-8 -*-


from django.db import models, migrations
import audit_log.models.fields
import django.utils.timezone
from django.conf import settings
import django_extensions.db.fields


def set_account_profile(apps, schema_editor):
    PoliticalCampaign = apps.get_model("campaign", "PoliticalCampaign")
    University = apps.get_model("campaign", "University")
    AccountProfile = apps.get_model("campaign", "AccountProfile")
    AnalysisProfile = apps.get_model("analysis", "AnalysisProfile")

    political, created = AccountProfile.objects.get_or_create(
        account_type="P", title="Political Candidate")
    academic, created = AccountProfile.objects.get_or_create(
        account_type="A", title="University")
    political_legacy = AnalysisProfile.objects.filter(
        title="Political legacy analysis")[0]
    political.default_analysis_profiles.add(political_legacy)
    political.save()
    academic_legacy = AnalysisProfile.objects.filter(
        title="Academic legacy analysis")[0]
    academic.default_analysis_profiles.add(academic_legacy)
    academic.save()

    for pc in PoliticalCampaign.objects.all():
        account = pc.account_ptr
        account.account_profile = political
        account.save()

    for u in University.objects.all():
        account = u.account_ptr
        account.account_profile = academic
        account.save()


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('analysis', '0002_new_analysis_models'),
        ('campaign', '0016_prospect_timestamps'),
    ]

    operations = [
        migrations.CreateModel(
            name='AccountProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('account_type', models.CharField(default='', max_length=2, blank=True, choices=[('P', 'Political'), ('A', 'Academic'), ('N', 'Non-Profit'), ('', '')])),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_campaign_accountprofile_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('default_analysis_profiles', models.ManyToManyField(related_name='+', to='analysis.AnalysisProfile', blank=True)),
                ('modified_by', audit_log.models.fields.LastUserField(related_name='modified_campaign_accountprofile_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by')),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='account',
            name='account_profile',
            field=models.ForeignKey(related_name='+', blank=True, to='campaign.AccountProfile', null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=False,
        ),
        # Populate the account_profile field
        migrations.RunPython(set_account_profile,
                             reverse_code=lambda x, y: True),
        # Make the account profile field non-nullable
        migrations.AlterField(
            model_name='account',
            name='account_profile',
            field=models.ForeignKey(related_name='+', blank=False,
                                    to='campaign.AccountProfile', on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
    ]
