# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0020_auto_20160223_1515'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='current_skin',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='ux.SkinSettings', null=True),
            preserve_default=True,
        ),
    ]
