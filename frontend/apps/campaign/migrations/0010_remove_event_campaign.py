# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0009_data_migration_event_account'),
    ]

    operations = [
        # Remove nullable
        migrations.AlterField(
        model_name='event',
            name='account',
            field=models.ForeignKey(to='campaign.Account', on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.RemoveField(
            model_name='event',
            name='campaign',
        ),
    ]
