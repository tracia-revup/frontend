# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0003_politicalcampaign_is_incumbent'),
    ]

    operations = [
        migrations.AddField(
            model_name='politicalcampaign',
            name='opponent',
            field=models.CharField(max_length=256, null=True, blank=True),
            preserve_default=True,
        ),
    ]
