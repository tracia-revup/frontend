# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ux', '__first__'),
        ('campaign', '0019_account_analysis_link'),
    ]

    operations = [
        migrations.CreateModel(
            name='AccountSkinSettingsLink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_default', models.BooleanField(default=False)),
                ('is_active', models.BooleanField(default=True)),
                ('account_profile', models.ForeignKey(related_name='+', to='campaign.AccountProfile', on_delete=django.db.models.deletion.CASCADE)),
                ('skin', models.ForeignKey(related_name='through_reference', to='ux.SkinSettings', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='account',
            name='current_skin',
            field=models.ForeignKey(related_name='+', blank=True, to='ux.SkinSettings', null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='accountprofile',
            name='skins',
            field=models.ManyToManyField(related_name='+', through='campaign.AccountSkinSettingsLink', to='ux.SkinSettings', blank=True),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='accountskinsettingslink',
            unique_together=set([('skin', 'account_profile')]),
        ),
    ]
