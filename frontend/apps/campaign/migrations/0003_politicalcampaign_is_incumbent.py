# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0002_prospect_prospectcollision'),
    ]

    operations = [
        migrations.AddField(
            model_name='politicalcampaign',
            name='is_incumbent',
            field=models.BooleanField(default=False, help_text='Is this candidate the incumbent?'),
            preserve_default=True,
        ),
    ]
