from django.core.management.base import BaseCommand, CommandError

from frontend.apps.campaign.models.external_data import (
    ExternalDataSource, ExternalDataConfig, ExternalData)
from frontend.apps.campaign.models.base import Account


class Command(BaseCommand):
    """Add an external data source to a campaign

    """
    help = "Add an external data source having a default config to a campaign."

    def add_arguments(self, parser):
        parser.add_argument('-s', '--source', dest="source",
                            help="Name of external data source (i.e. NGP)"),
        parser.add_argument('-a', '--account', dest="account",
                            help="ID of account"),
        parser.add_argument('-n', '--name', dest="name",
                            help="Name (label) for campaign data source"),
        parser.add_argument('-k', '--key', dest="static_key",
                            help="(Optional) static key for campaign data source")

    def handle(self, *args, **options):
        """Create external data source and config and add to campaign

        """
        source_name = options.get('source')
        if source_name:
            source = ExternalDataSource.objects.filter(name=source_name)[0]
            if not source:
                raise CommandError("Source named %s not found. See --help for "
                                   "more info." % source_name)
        else:
            raise CommandError("Source required. See --help for more info.")

        account_id = options.get('account')
        if account_id:
            account = Account.objects.get(pk=account_id)
            if not account:
                raise CommandError("Account id %s not found. See --help for "
                                   "more info." % account_id)
        else:
            raise CommandError("Account required. See --help for more info.")

        config = source.default_config
        if not config:
            raise CommandError("Source named %s has no default config. See "
                               "--help for more info.")
        static_key = options.get('static_key')

        name = options.get('name')
        if not name:
            name = source.name

        data_source = ExternalData.objects.create(account=account,
                                                  config=config,
                                                  name=name,
                                                  static_key=static_key)
        data_source.save()