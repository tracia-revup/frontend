from django.apps import AppConfig



class CampaignConfig(AppConfig):
    name = 'frontend.apps.campaign'

    def ready(self):
        from actstream import registry
        registry.register(self.get_model('Event'))
        registry.register(self.get_model('Prospect'))




