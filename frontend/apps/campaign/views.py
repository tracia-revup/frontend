from base64 import b64encode
from hashlib import md5
import logging
from posixpath import join as pjoin

import boto3
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.files.uploadedfile import UploadedFile
from django.db import transaction
from django.http import Http404, HttpResponse
from django.shortcuts import redirect, render
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import View

from frontend.apps.analysis.analyses.base import ConfigurationWrapper
from frontend.apps.campaign.models import Event, AccountContributionUpload
from frontend.apps.campaign.routes import ROUTES
from frontend.apps.contact_set.models import ContactSet
from frontend.apps.role.decorators import any_group_required, check_permissions
from frontend.apps.role.permissions import (FundraiserPermissions,
                                            AdminPermissions)
from frontend.libs.django_view_router import RoutingView
from frontend.libs.utils.api_utils import APIException
from frontend.libs.utils.kafka_utils import init_producer
from frontend.libs.utils.string_utils import random_uuid, ensure_unicode


LOGGER = logging.getLogger(__name__)


class EventView(RoutingView):
    routes = ROUTES

    @method_decorator(login_required)
    @method_decorator(any_group_required(*FundraiserPermissions.all()))
    def do_dispatch(self, request, handler, event_id=None, *args, **kwargs):
        self.event = Event.objects.get(pk=event_id)

        # Verify this user is authorized to access this event. Account
        # admins are always authorized.
        if (not request.user.events.filter(pk=self.event.id).exists() and not
                AdminPermissions.VIEW_PROSPECTS in
                    request.user.current_seat.permissions):
            LOGGER.error(
                "User: '{}' attempted to access unauthorized Event: '{}'"
                .format(request.user, self.event))
            raise Http404
        return super(EventView, self).do_dispatch(
            request, handler, event_id=event_id, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        request.breadcrumbs([('Home', reverse('index')),
                             ('Prospects', '')])
        account = self.event.account

        # Get partitions
        analysis = request.user.get_default_analysis()
        if analysis and analysis.account == account:
            # Common case, user will have a default analysis
            partition_map = analysis.build_partition_map()
        else:
            # If we don't have an analysis, the partitions aren't useful
            partition_map = []

        # Add the root contact set to the context so we can request
        # contact/result details
        contact_set = ContactSet.get_root(account=self.event.account,
                                          user=request.user)

        return render(
            request=request,
            template_name='campaign/prospects.html',
            context={'event': self.event,
                     'account': account,
                     'contact_set': contact_set,
                     'analysis': analysis,
                     'partition_map': partition_map,
                     'user': request.user,
                     'request': request})


@login_required
@any_group_required(*FundraiserPermissions.all())
def event_default(request):
    event = request.user.get_default_event()
    if event is not None:
        return redirect('event_detail', event.id)
    else:
        return redirect('event_list')


@login_required
@any_group_required(*FundraiserPermissions.all())
def event_list(request):
    events = request.user.events.filter(
        account=request.user.current_seat.account)
    if len(events) == 1:
        return redirect('event_detail', events[0].id)
    else:
        return render(
            request=request,
            template_name='campaign/event_list.html',
            context={'events': events,
                     'user': request.user,
                     'request': request})


class AccountContributionUploadView(View):
    '''Endpoint for uploading Client Contribution CSV files'''
    @method_decorator(login_required)
    @method_decorator(check_permissions(lambda user: user.is_admin))
    def get(self, request, **kwargs):
        '''Displays a form for uploading a client csv file

        Meant for development and debugging purposes.
        '''
        return render(
            request=request,
            template_name='campaign/contrib_upload.html',
        )

    @method_decorator(login_required)
    @method_decorator(any_group_required(AdminPermissions.LOAD_DATA))
    @transaction.atomic
    def post(self, request, **kwargs):
        file_ = request.FILES.get('file', None)
        if not (isinstance(file_, UploadedFile) and file_.size):
            raise APIException(
                LOGGER, "The uploaded file is empty.", request)
        file_name = file_.name
        user = request.user
        account = user.current_seat.account
        # Calculate md5 for uploaded file for deduping purposes, and to ensure
        # data integrity of the file to be uploaded.
        hasher = md5()
        for chunk in file_.chunks():
            hasher.update(chunk)
        file_.seek(0)
        data_hash = hasher.hexdigest()

        acu, created = AccountContributionUpload.objects.get_or_create(
            account=account, data_hash=data_hash, defaults={
                'file_name': file_name,
                'data_file': pjoin(
                    settings.ACCOUNT_CONTRIBUTION_S3_KEY_PREFIX, random_uuid())
            })
        acu.user.add(user)

        bucket = settings.ACCOUNT_CONTRIBUTION_S3_BUCKET
        obj_key = acu.data_file.name
        # Only upload file to s3 if a AccountContributionUpload record didn't
        # already exist for that file's contents.
        if created and settings.ACCOUNT_CONTRIBUTION_S3_STORAGE_ENABLED:
            acct_type_map = {
                'Political Committee': 'CO',
                'Political Candidate': 'CA',
            }
            encoded_data_hash = ensure_unicode(b64encode(hasher.digest()))
            analysis_config = account.get_default_analysis_config()
            cw = ConfigurationWrapper(analysis_config)
            # XXX: uhhh... what if this information within the frontend changes?
            # Attach metadata to the s3 object for the loading process to use.
            metadata = {
                'revup_account_name': account.organization_name,
                'revup_account_type': acct_type_map.get(
                    account.account_profile.title, ''),
                'revup_account_party': cw.get('party', ''),
                'revup_destination': account.contrib_collection_name,
                'md5chksum': encoded_data_hash,
            }
            s3conn = boto3.client('s3')
            s3conn.put_object(Body=file_, Bucket=bucket, Key=obj_key,
                              Metadata=metadata, ContentMD5=encoded_data_hash)
        # Even if we don't upload the file to s3, still queue the file for
        # re-processing.
        if settings.ENABLE_KAFKA_PUBLISHING:
            producer = init_producer()
            contrib_file_record = {
                'bucket': bucket,
                'key': obj_key,
            }
            result_future = producer.send(
                settings.BACKEND_CLIENT_CONTRIB_QUEUE_TOPIC,
                contrib_file_record)
            if result_future.failed():
                LOGGER.error(
                    "Error publishing to kafka. Error: {}".format(
                        result_future.exception),
                    exc_info=result_future.exception)
                raise result_future.exception
            producer.flush()
        if created:
            account.create_contrib_index()
        return HttpResponse('')
