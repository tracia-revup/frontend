
from django.db.models import Count
from django.contrib import admin

from .models import ZipCode, State, County, City, LinkedInLocation, CityAlias


class LocationAdmin(admin.ModelAdmin):
    search_fields = ("name",)


class StateAdmin(LocationAdmin):
    search_fields = LocationAdmin.search_fields + ("abbr",)


class CountyAdmin(LocationAdmin):
    search_fields = LocationAdmin.search_fields + ("state__abbr",)


class ZipInline(admin.StackedInline):
    model = City.zips.through
    raw_id_fields = ("zipcode", "city")
    extra = 0


class CityAdmin(LocationAdmin):
    search_fields = LocationAdmin.search_fields + ("state__abbr",)
    list_filter = ("independent",)
    inlines = (ZipInline,)
    raw_id_fields = ("zips", "county")
    fields = ("name", "state", "county", "independent")


class CityAliasAdmin(admin.ModelAdmin):
    search_fields = ("alias", "city__name")
    list_display = ('id', 'city', 'alias')
    raw_id_fields = ("city", )


@admin.register(LinkedInLocation)
class LinkedInLocationAdmin(admin.ModelAdmin):
    fields = ('name', 'country_code', 'census_cbsas')
    filter_horizontal = ('census_cbsas',)
    list_display = ('id', 'name', 'cbsa_count',)
    readonly_fields = ('name', 'country_code')
    search_fields = ('name',)

    def get_queryset(self, request):
        qs = super(LinkedInLocationAdmin, self).get_queryset(request)
        qs = qs.annotate(Count('census_cbsas'))
        return qs

    def cbsa_count(self, obj):
        return obj.census_cbsas__count
    cbsa_count.admin_order_field = 'census_cbsas__count'


admin.site.register(ZipCode)
admin.site.register(State, StateAdmin)
admin.site.register(County, CountyAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(CityAlias, CityAliasAdmin)

