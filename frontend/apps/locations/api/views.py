from django.db.models import Q
from rest_framework.filters import SearchFilter
from rest_framework.pagination import PageNumberPagination
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework import exceptions

from frontend.apps.locations.api.serializers import (
    StateSerializer, CountySerializer, CitySerializer,
    LinkedInLocationSerializer)
from frontend.apps.locations.models import (
    City, State, County, LinkedInLocation)


# Listings can get expensive, and are only intended for debugging, so limit low
class StandardResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'


class QueryFilter(SearchFilter):
    search_param = 'query'

    def get_search_terms(self, request):
        """Search terms may be separated by commas.

        Separated search terms are treated as ANDs. Spaces do not separate
        search terms. This is so "San Francisco" is one name query.
        """
        params = request.query_params.get(self.search_param, '')
        return [p.strip() for p in params.split(",")]


class LocationAutocompleteViewSet(ReadOnlyModelViewSet):
    pagination_class = StandardResultsSetPagination
    filter_backends = (QueryFilter,)
    search_fields = ('^name',)


class CitiesViewSet(LocationAutocompleteViewSet):
    queryset = City.objects.all().order_by(
        'name').select_related("state", "county")
    serializer_class = CitySerializer
    filter_backends = ()

    def _name_filter(self, queryset, name):
        if name:
            queryset = queryset.filter(
                name__istartswith=name.strip())
        return queryset

    def filter_queryset(self, queryset):
        qstring = self.request.query_params.get('query')
        if not qstring:
            return queryset

        # Build the city query
        tokens = qstring.split(",")
        if len(tokens) == 1:
            return self._name_filter(queryset, qstring)
        elif len(tokens) == 2:
            city, state = (t.strip() for t in tokens)
            # First, build the query for city
            queryset = self._name_filter(queryset, city)
            # Then we have to build the query for state
            return queryset.filter(Q(state__name__istartswith=state) |
                                   Q(state__abbr__istartswith=state))
        else:
            raise exceptions.APIException("Invalid query: {}".format(qstring))

    def get_serializer(self, *args, **kwargs):
        """Iterate over the response Cities and see if any cities share the
        exact same city and state. This is possible in rare occasions.
        If that happens, we will also include the county.

        This was the only place I could reach in and grab the final, paged,
        serialized data before Response.
        """
        serialized = super(CitiesViewSet, self).get_serializer(*args, **kwargs)

        city_labels = {}
        county_cites = {}
        for serialized_city in serialized.data:
            label = serialized_city["label"]
            if label in city_labels:
                county_cites[serialized_city["id"]] = serialized_city

                # Add the first city to the list, if it hasn't been yet.
                city = city_labels[label]
                if city["id"] not in county_cites:
                    county_cites[city["id"]] = city
            else:
                city_labels[label] = serialized_city

        # Add the county name to any cities that are in the same state twice
        for serialized_city in county_cites.values():
            # Independent cities wont have a county
            if serialized_city["county"]:
                serialized_city["label"] = "{} ({} County)".format(
                    serialized_city["label"],
                    serialized_city["county"]["name"])
        return serialized


class StatesViewSet(LocationAutocompleteViewSet):
    queryset = State.objects.all().order_by('name')
    serializer_class = StateSerializer


class CountiesViewSet(LocationAutocompleteViewSet):
    queryset = County.objects.all().order_by('name').select_related("state")
    serializer_class = CountySerializer
    search_fields = ('^name',
                     '=state__name',
                     '=state__abbr',)


class LinkedInLocationViewSet(LocationAutocompleteViewSet):
    queryset = LinkedInLocation.objects.all().order_by('name')
    serializer_class = LinkedInLocationSerializer
    search_fields = ('name',
                     '=zipcodes__city__name',
                     '=zipcodes__city__state__name',
                     '=zipcodes__city__state__abbr',)
