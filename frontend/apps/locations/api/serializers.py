from rest_framework import serializers

from frontend.apps.locations.models import (
    LinkedInLocation, City, State, County)


class LinkedInLocationSerializer(serializers.ModelSerializer):
    label = serializers.ReadOnlyField(source="name")

    class Meta:
        model = LinkedInLocation
        fields = ("id", "name", "country_code", "label")


class StateSerializer(serializers.ModelSerializer):
    class Meta:
        model = State
        fields = '__all__'


class CitySerializer(serializers.ModelSerializer):
    state = StateSerializer()
    label = serializers.SerializerMethodField()
    county = serializers.SerializerMethodField()

    class Meta:
        model = City
        fields = ("id", "state", "name", "county", "label")

    def get_label(self, city):
        return "{}, {}".format(city.name, city.state.abbr)

    def get_county(self, city):
        if city.county:
            return dict(
                id=city.county.id,
                name=city.county.name)
        else:
            return None


class CountySerializer(serializers.ModelSerializer):
    state = StateSerializer()
    label = serializers.SerializerMethodField()

    class Meta:
        model = County
        fields = ("id", "name", "state", "label")

    def get_label(self, county):
        return "{}, {}".format(county.name, county.state.abbr)
