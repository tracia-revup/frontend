
from collections import Iterable

import factory

from frontend.apps.locations.models import (
    LinkedInLocation, ZipCode, City, State, CityAlias, County)


class LinkedInLocationFactory(factory.DjangoModelFactory):
    class Meta:
        model = LinkedInLocation

    name = factory.Faker('state')
    country_code = 'US'


class ZipCodeFactory(factory.DjangoModelFactory):
    class Meta:
        model = ZipCode

    zipcode = factory.Faker('zipcode')
    latitude = factory.Faker('latitude')
    longitude = factory.Faker('longitude')


class StateFactory(factory.DjangoModelFactory):
    class Meta:
        model = State

    name = 'California'
    abbr = 'CA'


class CountyFactory(factory.DjangoModelFactory):
    class Meta:
        model = County

    name = factory.Faker('county')
    state = factory.SubFactory(StateFactory)


class CityFactory(factory.DjangoModelFactory):
    class Meta:
        model = City

    name = factory.Faker('city')
    independent = True
    state = factory.SubFactory(StateFactory)

    @factory.post_generation
    def zips(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted is not None:
            if not isinstance(extracted, Iterable):
                extracted = [extracted]
            self.zips.add(*extracted)
        else:
            self.zips.add(ZipCodeFactory())


class CityAliasFactory(factory.DjangoModelFactory):
    class Meta:
        model = CityAlias

    city = factory.SubFactory(CityFactory)
    alias = factory.Faker('alias')
