"""All zips starting with 0 were somehow left out.
    This script is to add them, and fix the data. This script is being run
    long after migrations 0005 to redo the fixtures.

Running this script generally should be followed by:
    1) locations.scripts.associate_lil_with_zips
    2) contact.scripts.splay_addresses
    3) filtering.management.commands.index_contacts
"""

from collections import defaultdict
import csv
from io import open
import logging

from frontend.apps.locations.models import State, County, ZipCode, City
from frontend.libs.utils.general_utils import FunctionalBuffer
from .geonames_zips import download_file, parse_county


LOGGER = logging.getLogger(__name__)


def run(local_file_path=None):
    if local_file_path:
        csv_file = open(local_file_path, "r")
    else:
        csv_file = download_file()

    dict_reader = csv.DictReader(csv_file)
    if "State" not in dict_reader.fieldnames:
        error = "Bad csv file. If you are accessing the S3 file, make sure " \
                "it is made public during the processing of this script."
        LOGGER.error(error)
        print(error)
        return -1

    states = {s.abbr: s for s in State.objects.all()}
    counties = {}
    cities = {}
    city_zips = defaultdict(list)
    zips = set()
    existing_zips = set(ZipCode.objects.values_list("zipcode", flat=True))

    for i, row in enumerate(dict_reader):
        # Read and set/create the state
        state_name = row['State'].strip()
        # Some records seem to be military related, but we're not interested
        # in those right now
        if not state_name:
            LOGGER.info("Skipping row {}. Reason: "
                        "Missing state for '{}'".format(i, row["City"]))
            continue
        state_abbr = row["State abbr"].strip()
        state = states.get(state_abbr)

        # Build the zip
        code = row["Zip"].strip()
        if len(code) != 5:
            if len(code) == 4:
                LOGGER.warn("POSSIBLE STRIPPED ZIP. Row {}: {}".format(i, code))
                raise Exception("Stripped Code? {}".format(code))
            else:
                LOGGER.info("Skipping row {}. "
                            "Reason: Bad zip ({})".format(i, code))
            continue
        if code in existing_zips:
            # Code has already been entered, nothing to do.
            continue
        else:
            LOGGER.info("New code: {}".format(code))

        zip_code = ZipCode(
            zipcode=code,
            latitude=row["Latitude"].strip(),
            longitude=row["Longitude"].strip())
        zips.add(zip_code)

        # Read and set/create the county
        county_name, independent = parse_county(row['County'])
        if independent:
            county = None
        else:
            county_key = (county_name, state_abbr)
            county = counties.get(county_key)
            if not county:
                county, _ = County.objects.get_or_create(
                    name=county_name,
                    state=state)
                counties[county_key] = county

        # Build the cities
        city_name = row["City"].strip()
        city_key = (city_name, state_abbr, county_name)
        city = cities.get(city_key)
        if not city:
            city, _ = City.objects.get_or_create(
                name=city_name,
                state=state,
                county=county,
                independent=independent)
            cities[city_key] = city
        city_zips[city_key].append(code)

    LOGGER.info("Finished adding states, counties, and cities; starting Zips...")

    ## Bulk create Zips
    ZipCode.objects.bulk_create(zips, batch_size=1000)

    # Add zips to cities. This might be slow...
    LOGGER.info("Adding Zips to Cities...")
    # Grab the through table so we can do a bulk_create across cities
    CityZips = City.zips.through
    zip_buffer = FunctionalBuffer(CityZips.objects.bulk_create,
                                  max_length=1000)
    for city, zip_codes in city_zips.items():
        city = City.objects.get(name=city[0], state__abbr=city[1],
                                county__name=city[2])
        zipcodes = ZipCode.objects.filter(zipcode__in=zip_codes)
        for zipcode in zipcodes:
            zip_buffer.push(CityZips(city=city, zipcode=zipcode))

    # Do a final flush of the buffer
    zip_buffer.flush()
