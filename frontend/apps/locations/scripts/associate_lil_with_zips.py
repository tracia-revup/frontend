import os
import json
from io import open

cbsa_zip_map_path = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                                 'cbsa_zip_map.json'))


def run(apps=None):
    if apps:
        ZipCode = apps.get_model("locations", "ZipCode")
        LinkedInLocation = apps.get_model("locations", "LinkedInLocation")
    else:
        from frontend.apps.locations.models import ZipCode, LinkedInLocation

    # Test to see if ZipCode has been populated yet. Fail if it has not.
    assert ZipCode.objects.exists()
    # Verify LinkedInLocation has a M2M link to ZipCode
    assert hasattr(LinkedInLocation, 'zipcodes')

    cbsa_zip_map = json.load(open(cbsa_zip_map_path, 'r'))

    for lil in LinkedInLocation.objects.all().iterator():
        cbsa_codes = list(map(str, lil.census_cbsas.values_list('cbsa_code',
                                                           flat=True)))
        zips = set()
        for cbsa_code in cbsa_codes:
            zips.update(cbsa_zip_map.get(cbsa_code, []))
        if zips:
            lil.zipcodes.set(ZipCode.objects.filter(zipcode__in=zips))
