import os
import tarfile

from django.core.management import call_command


fixture_dir = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                           '../fixtures'))


def run(apps=None):
    if apps:
        City = apps.get_model("locations", "City")
    else:
        from frontend.apps.locations.models import City

    # Don't load fixtures if there is already data, or we're in test
    if City.objects.exists():
        return

    print("Loading fixtures. This will take a couple minutes...")
    call_command("loaddata", "locations.json")
