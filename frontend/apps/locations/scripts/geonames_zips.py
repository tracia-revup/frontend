"""This script populates the locations models with data from GeoNames"""

from collections import defaultdict
import csv
from io import open
import logging
import re

import requests

from frontend.apps.locations.models import State, County, ZipCode, City
from frontend.libs.utils.general_utils import FunctionalBuffer


LOGGER = logging.getLogger(__name__)
zips_file_url = "https://revup-geonames-data.s3.amazonaws.com/US-zips.csv"


def download_file():
    response = requests.get(zips_file_url, stream=True)
    return response.raw


def run(local_file_path=None):
    if local_file_path:
        csv_file = open(local_file_path, "r")
    else:
        csv_file = download_file()

    states = {}
    counties = {}
    cities = {}
    city_zips = defaultdict(list)
    zips = set()

    dict_reader = csv.DictReader(csv_file)
    if "State" not in dict_reader.fieldnames:
        error = "Bad csv file. If you are accessing the S3 file, make sure " \
                "it is made public during the processing of this script."
        LOGGER.error(error)
        print(error)
        return -1

    for i, row in enumerate(dict_reader):
        # Read and set/create the state
        state_name = row['State'].strip()
        # Some records seem to be military related, but we're not interested
        # in those right now
        if not state_name:
            LOGGER.info("Skipping row {}. Reason: "
                        "Missing state for '{}'".format(i, row["City"]))
            continue
        state_abbr = row["State abbr"].strip()
        state = states.get(state_abbr)
        if not state:
            state = State.objects.create(
                name=state_name,
                abbr=state_abbr)
            states[state_abbr] = state

        # Read and set/create the county
        county_name, independent = parse_county(row['County'])
        if independent:
            county = None
        else:
            county_key = (county_name, state_abbr)
            county = counties.get(county_key)
            if not county:
                county = County.objects.create(
                    name=county_name,
                    state=state)
                counties[county_key] = county

        # Build the zips
        code = row["Zip"].strip()
        if len(code) != 5:
            LOGGER.info("Skipping row {}. "
                        "Reason: Bad zip ({})".format(i, code))
            continue
        zip_code = ZipCode(
            zipcode=code,
            latitude=row["Latitude"].strip(),
            longitude=row["Longitude"].strip())
        zips.add(zip_code)

        # Build the cities
        city_name = row["City"].strip()
        city_key = (city_name, state_abbr, county_name)
        city = cities.get(city_key)
        if not city:
            city = City(
                name=city_name,
                state=state,
                county=county,
                independent=independent)
            cities[city_key] = city
        city_zips[city_key].append(code)

    LOGGER.info("Finished creating states and counties, starting Zips...")

    ## Bulk create anything we can.
    # Start with Zips
    ZipCode.objects.bulk_create(zips, batch_size=1000)

    # Bulk create cities
    LOGGER.info("Finished creating zips, starting Cities...")
    City.objects.bulk_create(iter(cities.values()), batch_size=1000)

    # Add zips to cities. This might be slow...
    LOGGER.info("Finished creating cities, adding Zips to Cities...")
    # Grab the through table so we can do a bulk_create across cities
    CityZips = City.zips.through
    zip_buffer = FunctionalBuffer(CityZips.objects.bulk_create,
                                  max_length=1000)
    for city, zip_codes in city_zips.items():
        city = City.objects.get(name=city[0], state__abbr=city[1],
                                county__name=city[2])
        zipcodes = ZipCode.objects.filter(zipcode__in=zip_codes)
        for zipcode in zipcodes:
            zip_buffer.push(CityZips(city=city, zipcode=zipcode))

    # Do a final flush of the buffer
    zip_buffer.flush()


def parse_county(county):
    # Typos are intentional
    city_strings = ("city of", "(city", "city and boroug", " city")
    #
    replace_strings = (" County", " Parish", " Borough", "Census Area",
                       "\(CA\)")

    # Counties with city strings in their name means they are an independent
    # city. i.e. a city without a county.
    lower_county = county.lower()
    for item in city_strings:
        if item in lower_county:
            return None, True

    clean_county = re.sub("|".join(replace_strings), "", county)
    return clean_county.strip(), False

