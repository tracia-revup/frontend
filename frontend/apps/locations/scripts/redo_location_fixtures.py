import os
import tarfile

from django.core.management import call_command


fixture_dir = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                           '../fixtures'))


def run(apps=None):
    print("Loading fixtures. This will take a couple minutes...")
    call_command("loaddata", "redo_location.json")
