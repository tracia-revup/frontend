import os
import sys

from django.core.management import call_command

from frontend.apps.contact.models import Address
from frontend.apps.contact.scripts import splay_addresses
from ..models import City, State, County, ZipCode


fixture_dir = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                           '../fixtures'))


def _progress_callback(percent):
    sys.stdout.write('Progress: {}%\r'.format(percent))
    sys.stdout.flush()


def run():
    # Blow away all the current locations. They are FUBAR
    print("Delete the dirty locations")
    Address.objects.exclude(city_key=None).update(city_key=None)
    Address.objects.exclude(zip_key=None).update(zip_key=None)
    print(City.objects.all().delete())
    print(State.objects.all().delete())
    print(County.objects.all().delete())
    print(ZipCode.objects.all().delete())

    # Rebuild them with cleaned data
    print("Rebuild the locations with cleaned fixtures")
    call_command("loaddata", "cleaned_locations.json")

    # Rebuild the links to the Address models
    print("Link city locations to addresses. This will take a while...")
    splay_addresses.run()

    print("Reindex the contacts")
    call_command("index_contacts", reindex=True)
