"""
This script is used to download shape files from s3 onto a local machine and
load them into the database.
"""
import argparse
import boto3
import os
from subprocess import call

from django.contrib.gis.utils import LayerMapping, LayerMapError
from django.core.management.base import BaseCommand

from frontend.apps.locations.models import UsBorder

us_border_mapping = {
    'zipcode': 'ZCTA5CE10',
    'geoid': 'AFFGEOID10',
    'zipcode_identifier': 'GEOID10',
    'land_area': 'ALAND10',
    'water_area': 'AWATER10',
    'border': 'MULTIPOLYGON',
}


def load_data(file_path, verbose=True):
    """Maps the fields from shapefiles to UsBorder model"""
    lm = LayerMapping(
        UsBorder, file_path, us_border_mapping,
        transform=True, encoding='iso-8859-1',
    )
    lm.save(strict=True, verbose=verbose)


def valid_shape_file(file_path):
    """This method is used to validate file_path argument. This method checks
    if the file has valid extension and exists on the disk"""
    file_path = file_path.strip("\'\"")
    if not file_path.endswith('.shp'):
        msg = 'File must have .shp extension'
        raise argparse.ArgumentTypeError(msg)
    if not os.path.isfile(file_path):
        msg = '{0} does not exist'.format(file_path)
        raise argparse.ArgumentTypeError(msg)
    return file_path


def valid_directory(directory_path):
    """This method is used to validate directory_path argument. This method
    checks if the given path is a valid directory"""
    directory_path = directory_path.strip("\'\"")
    if not os.path.isdir(directory_path):
        msg = '{0} is not a valid directory'.format(directory_path)
        raise argparse.ArgumentTypeError(msg)
    return directory_path


class Command(BaseCommand):
    help = "Loads shapefiles if path to shapefile is specified. Downloads \
    files from s3 if path to a directory is specified"

    def add_arguments(self, parser):
        parser.add_argument('--file_path', dest="file_path",
                            type=valid_shape_file,
                            help="Specify the path to the shapefile")
        parser.add_argument('--directory_path', dest="directory_path",
                            type=valid_directory,
                            help="Specify the path to store the shapefile")

    def handle(self, *args, **options):
        file_path = options.get('file_path', None)
        directory_path = options.get('directory_path', None)

        # Verify that only one of the arguments : file_path, directory_path is
        # supplied
        if file_path and directory_path:
            print(('Both of the following arguments cannot be specified: '
                   'file_path and directory_path'))
            return
        if not file_path and not directory_path:
            print(('Please specify atleast one of the following arguments: \n'
                   'file_path - Location of shapefiles, or \ndirectory_path - '
                   'Directory to save the shapefiles downloaded from s3'))
            return

        # If directory_path is supplied, download the shapefiles from s3 to
        # the supplied path
        if directory_path:
            shape_file = 'shapefiles-cb-2017.tar.xz'
            destination_path = os.path.join(directory_path, shape_file)

            # Get the service client
            s3 = boto3.client('s3')

            s3.download_file("revup-lat-long", "shapefiles-cb-2017.tar.xz",
                             destination_path)

            # Verify that the file has downloaded successfully
            if not os.path.isfile(destination_path):
                print('Shapefile download from s3 failed')
                return

            # Extract the file
            _cmd = 'tar -xf {0} -C {1}'.format(destination_path, directory_path)
            cmd_ret = call(_cmd, shell=True)
            # Verify that the file extraction was successful
            if cmd_ret != 0:
                print('Could not extract {0}'.format(destination_path))
                return
            file_path = os.path.join(directory_path,
                                     'cb_2017_us_zcta_shapefiles',
                                     'cb_2017_us_zcta510_500k.shp')

        # Load the shapefiles into the database
        try:
            load_data(file_path)
        except LayerMapError:
            print(('LAYERMAPERROR: Given file does not have the expected '
                   'fields'))
            raise

