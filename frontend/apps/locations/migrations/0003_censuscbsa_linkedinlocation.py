# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-25 16:12


import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0002_load_location_data'),
        ('analysis', '0053_move_linkedinlocation_and_census_cbsa_to_locations_app'),
    ]

    state_operations = [
        migrations.CreateModel(
            name='CensusCBSA',
            fields=[
                ('cbsa_code', models.IntegerField(primary_key=True, serialize=False)),
                ('cbsa_title', models.CharField(blank=True, max_length=256, null=True)),
                ('zip_3_codes', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(blank=True, max_length=3, null=True), null=True, size=None)),
            ],
        ),
        migrations.CreateModel(
            name='LinkedInLocation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=256, null=True)),
                ('country_code', models.CharField(blank=True, max_length=3, null=True)),
                ('census_cbsas', models.ManyToManyField(blank=True, related_name='location', to='locations.CensusCBSA')),
            ],
        ),
    ]

    operations = [
        migrations.SeparateDatabaseAndState(state_operations=state_operations)
    ]
