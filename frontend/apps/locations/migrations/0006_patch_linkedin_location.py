# -*- coding: utf-8 -*-


from django.db import models, migrations


def clean_lils(apps, schema_editor):
    """Clean the duplicate LinkedInLocations. There can be only one!"""
    Contact = apps.get_model("contact", "Contact")
    LinkedInLocation = apps.get_model("locations", "LinkedInLocation")

    duplicates = LinkedInLocation.objects.values_list("name").annotate(
        models.Count("name")).filter(name__count__gt=1)

    for name, _ in duplicates:
        # Query for the duplicate LILs, and order-by which has the most
        # defined zipcodes. The typical case is the first one will have zips,
        # and the rest will not have any.
        locs = LinkedInLocation.objects.filter(name=name).annotate(
            models.Count("zipcodes")).order_by("-zipcodes__count")
        primary = locs[0]
        replacing = locs[1:]

        # Update all the Contacts that point at the replaced locations
        Contact.locations.through.objects.filter(
            linkedinlocation__in=replacing).update(linkedinlocation=primary)

        # Delete the replaced locations
        zips = set()
        cbsas = set()
        for replace in replacing:
            zips.update(replace.zipcodes.all())
            cbsas.update(replace.census_cbsas.all())
            replace.delete()

        # Union the zips and cbsas from all duplicates into the primary.
        primary.zipcodes.add(*zips)
        primary.census_cbsas.add(*cbsas)


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0005_redo_location_load'),
    ]

    operations = [
        migrations.RunPython(clean_lils, lambda x, y: True),
    ]

