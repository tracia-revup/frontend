# -*- coding: utf-8 -*-

import os

from django.conf import settings
from django.db import models, migrations

from frontend.apps.locations.scripts import redo_location_fixtures


def load_data(apps, *args):
    # We don't want to run this in the test environment
    if not os.environ.get('CI', False):
        redo_location_fixtures.run(apps=apps)


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0004_linkedinlocation_zipcodes'),
    ]

    operations = [
        migrations.RunPython(load_data,
                             lambda x, y: True)
    ]
