# encoding: utf8
from django.db import migrations
from django.db.models import Q
from django.contrib.gis.db import models


def get_fuzzy_city(cls, city_query):
    """
    Mimicking the model method here since model/class methods are not allowed
    in migrations.
    :param cls:
    :param city_query:
    :return:
    """
    cities = list(cls.objects.filter(city_query))
    if len(cities) == 1:
        return cities[0]
    elif not cities:
        return None
    else:
        # Multiple cities returned. This is the "fuzzy" part.
        # This query counts the number of zip codes each city has and
        # picks the city with the most zips.
        return cls.objects.filter(id__in=(c.id for c in cities)) \
            .annotate(num_zips=models.Count("zips")) \
            .order_by("-num_zips").first()


def load_data(apps, *args):
    CityAlias = apps.get_model("locations", "CityAlias")
    City = apps.get_model("locations", "City")

    city_aliases = [
        ('Los Angeles', ['LA']),
        ('New York City', ['New York', 'NY', 'NYC']),
        ('Las Vegas', ['LV']),
        ('Virginia Beach', ['Va Beach']),
        ('San Diego', ['SD']),
        ('San Francisco', ['SF']),
        ('Colorado Springs', ['Co Springs']),
        ('Oklahoma City', ['Ok City', 'OKC']),
        ('Kansas City', ['Ks City', 'KS']),
        ('Atlantic City', ['AC']),
        ('Providence', ['Prov', 'PVD']),
        ('Austin', ['ATX']),
    ]

    for city_name, aliases in city_aliases:
        city_q = Q(name__iexact=city_name)

        city_obj = get_fuzzy_city(City, city_q)

        if city_obj:
            for alias in aliases:
                CityAlias(city=city_obj, alias=alias).save()


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0009_auto_20180910_1546'),
    ]

    operations = [
        migrations.RunPython(load_data, reverse_code=lambda x, y: True)
    ]

