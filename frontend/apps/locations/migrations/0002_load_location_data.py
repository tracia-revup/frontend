# -*- coding: utf-8 -*-

import os

from django.conf import settings
from django.db import models, migrations

from frontend.apps.locations.scripts import load_location_fixtures


def load_data(apps, *args):
    # We want this to run in development environments only. We will
    # do it manually on the web servers.
    if settings.DEBUG and not os.environ.get('CI', False):
        load_location_fixtures.run(apps=apps)


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(load_data,
                             lambda x, y: True)
    ]
