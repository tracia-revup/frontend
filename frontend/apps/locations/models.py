
from django.contrib.gis.db import models
from django.contrib.gis.geos import Point
from django.contrib.postgres.fields import ArrayField


class ZipCode(models.Model):
    zipcode = models.CharField(max_length=5, unique=True, blank=False)
    latitude = models.FloatField(blank=True)
    longitude = models.FloatField(blank=True)

    def __eq__(self, other):
        if isinstance(other, ZipCode):
            return other.zipcode == self.zipcode
        else:
            # Attempt to cast 'other' as a string and compare
            try:
                return str(other) == self.zipcode
            except TypeError:
                return False

    def __hash__(self):
        return hash(self.zipcode)

    def __str__(self):
        return "{}".format(self.zipcode)

    @classmethod
    def get_us_zipcode(cls, latitude, longitude):
        """Get US zipcode from latitude and longitude. Return empty string if
        the coordinates do not come under US Borders """
        point = Point(float(longitude), float(latitude))
        try:
            zip_area = UsBorder.objects.get(mpoly__intersects=point)
        except UsBorder.DoesNotExist:
            return ''
        zipcode = zip_area.zcta5ce10
        return zipcode


class State(models.Model):
    name = models.CharField(max_length=64, unique=True, blank=False)
    abbr = models.CharField(max_length=2, unique=True, blank=False)

    def __str__(self):
        return "{} ({})".format(self.name, self.abbr)


class County(models.Model):
    name = models.CharField(max_length=64, blank=False)
    state = models.ForeignKey(State, on_delete=models.CASCADE, null=False, blank=False)

    class Meta:
        unique_together = (("name", "state"),)

    def __str__(self):
        return "{}, {}".format(self.name, self.state.abbr)


class City(models.Model):
    name = models.CharField(max_length=64, blank=False)
    state = models.ForeignKey(State, on_delete=models.CASCADE, null=False, blank=False)
    county = models.ForeignKey(County, on_delete=models.CASCADE, null=True, blank=True)
    zips = models.ManyToManyField(ZipCode)
    independent = models.BooleanField(
        default=False, blank=True,
        help_text='Some cities are "independent", which means they are not '
                  'part of a county. If `county` is null because of '
                  'independence, this field should be true')

    class Meta:
        unique_together = (("name", "state", "county", "independent"),)

    def __str__(self):
        return "{}, {}".format(self.name, self.state.abbr)

    @classmethod
    def get_fuzzy_city(cls, city_query):
        cities = list(cls.objects.filter(city_query))
        if len(cities) == 1:
            return cities[0]
        elif not cities:
            return None
        else:
            # Multiple cities returned. This is the "fuzzy" part.
            # This query counts the number of zip codes each city has and
            # picks the city with the most zips.
            return cls.objects.filter(id__in=(c.id for c in cities)) \
                              .annotate(num_zips=models.Count("zips")) \
                              .order_by("-num_zips").first()


class CityAlias(models.Model):
    city = models.ForeignKey(City, on_delete=models.CASCADE, null=False,
                             blank=False, related_name='aliases')
    alias = models.CharField(max_length=64, blank=False, null=False)

    class Meta:
        unique_together = (("city", "alias"),)

    def __str__(self):
        return "City: {} Aliased as: {}".format(self.city.name, self.alias)


class LinkedInLocation(models.Model):
    name = models.CharField(max_length=256, blank=True, null=True)
    country_code = models.CharField(max_length=3, blank=True, null=True)
    census_cbsas = models.ManyToManyField('CensusCBSA', blank=True,
                                          related_name='location')
    zipcodes = models.ManyToManyField('ZipCode', blank=True,
                                      related_name='location')

    def __str__(self):
        return "{}: '{}'".format(self.__class__.__name__, self.name)


class CensusCBSA(models.Model):
    cbsa_code = models.IntegerField(primary_key=True)
    cbsa_title = models.CharField(max_length=256, blank=True, null=True)
    zip_3_codes = ArrayField(models.CharField(
        max_length=3, blank=True, null=True), null=True)

    def __str__(self):
        return self.cbsa_title


class UsBorder(models.Model):
    # 2010 Census 5-digit ZIP Code Tabulation Area code
    zipcode = models.CharField(max_length=5)
    # A full GEOID that is unique across all geography levels
    geoid = models.CharField(max_length=255)
    # 2010 Census 5-digit ZIP Code Tabulation Area identifier
    zipcode_identifier = models.CharField(max_length=5)
    # 2010 Census land area
    land_area = models.BigIntegerField()
    # 2010 Census water area
    water_area = models.BigIntegerField()
    # A polygon field that has the list of co-ordinates on the border
    border = models.MultiPolygonField()

    # Returns the string representation of the model.
    def __str__(self):
        return self.zipcode
