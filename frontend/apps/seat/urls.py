from django.conf.urls import url

from .views import SelectActiveSeat


urlpatterns = [
    url(r'^/select_current/$', SelectActiveSeat.as_view(),
        name="select_current_seat"),
]
