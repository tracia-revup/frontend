import datetime

from ddtrace import tracer


class UpdateLastActiveMiddleware(object):
    """Update the last activity date for a seat.

    This will only happen once per day (per seat).
    """
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    @tracer.wrap()
    def process_view(self, request, view_func, view_args, view_kwargs):
        # Do nothing if the user isn't logged in, or being impersonated
        if not request.user.is_authenticated or request.user.is_impersonate:
            return

        seat = request.user.current_seat
        if seat:
            now = datetime.datetime.utcnow()
            if (not seat.last_activity or
                        seat.last_activity.date() < now.date()):
                seat.last_activity = now
                seat.save()
