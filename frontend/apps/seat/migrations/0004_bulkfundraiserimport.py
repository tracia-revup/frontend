# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        # ('campaign', '0014_accounttask'),
        ('seat', '0003_auto_20150302_1940'),
    ]

    operations = [
        migrations.CreateModel(
            name='BulkFundraiserImport',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file_type', models.CharField(default='CSV', max_length=3, db_index=True, choices=[('CSV', 'CSV File')])),
                ('data_file', models.BinaryField()),
                ('account', models.ForeignKey(related_name='bulk_imports', to='campaign.Account', on_delete=django.db.models.deletion.CASCADE)),
                ('user', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL, help_text='The user who started this task', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
