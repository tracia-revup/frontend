# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion
import datetime
from django.conf import settings


def add_admin_seats(apps, schema_editor):
    """Add a guide manager to each user before making the field non-nullable"""
    from frontend.apps.seat.models import Seat as S

    def is_campaign_admin(user, campaign):
        return user.groups.filter(pk=cag.pk).exists() and \
               user.campaigns.first().pk == campaign.pk

    PoliticalCampaign = apps.get_model('campaign', "PoliticalCampaign")
    RevUpUser = apps.get_model('authorize', "RevUpUser")

    Seat = apps.get_model("seat", "Seat")
    Group = apps.get_model("auth", "Group")

    # Rename the Campaign Group
    Group.objects.filter(name="Campaign Admin").update(
        name=settings.ACCOUNT_ADMIN_GROUP_NAME)

    # Create the permission groups
    cag, created = Group.objects.get_or_create(
        name=settings.ACCOUNT_ADMIN_GROUP_NAME)
    fg, created = Group.objects.get_or_create(
        name=settings.FUNDRAISER_GROUP_NAME)

    for campaign in PoliticalCampaign.objects.all():
        for user in RevUpUser.objects.filter(campaigns__pk=campaign.pk):
            if is_campaign_admin(user, campaign):
                s = Seat()
                s.seat_type = S.Types.PERMANENT
                s.user_id = user.id
                s.account_id = campaign.pk
                s.state = S.States.ACTIVE
                s.save()
                s.permission_groups = [cag, fg]


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('campaign', '0006_auto_20150107_1414'),
    ]

    operations = [
        migrations.CreateModel(
            name='Seat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('seat_type', models.CharField(default='TM', max_length=2, blank=True, choices=[('TM', 'Temporary'), ('PM', 'Permanent')])),
                ('date_assigned', models.DateField(default=datetime.datetime.utcnow, blank=True)),
                ('date_revoked', models.DateField(default=None, null=True, blank=True)),
                ('state', models.CharField(default='PN', max_length=2, blank=True, choices=[('PN', 'Pending'), ('AC', 'Active'), ('RV', 'Revoked')])),
                ('account', models.ForeignKey(related_name='seats', to='campaign.Account', on_delete=django.db.models.deletion.CASCADE)),
                ('permission_groups', models.ManyToManyField(help_text='The permission groups that say where this seat can go inside the app (fundraiser, campaign admin, etc).', to='auth.Group', blank=True)),
                ('user', models.ForeignKey(related_name='seats', to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterIndexTogether(
            name='seat',
            index_together=set([('account', 'state', 'seat_type')]),
        ),
        migrations.RunPython(add_admin_seats,
                             reverse_code=lambda x, y: True),
    ]
