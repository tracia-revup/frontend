# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('seat', '0006_seat_inviting_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='seat',
            name='notes',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
