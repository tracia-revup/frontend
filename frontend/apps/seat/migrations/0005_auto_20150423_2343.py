# -*- coding: utf-8 -*-

import datetime

from django.db import models, migrations


def set_new_dates(apps, schema_editor):
    """Since we won't have the activated date for seats that were activated
    in the past, we'll have to make due with the assigned date.
    We'll also attempt to set last_activity to last_login. This isn't
    perfectly accurate because the user could have been active on another
    seat, but staff should be the only users with multiple seats at this point.
    """
    Seat = apps.get_model("seat", "Seat")
    for seat in Seat.objects.select_related('user').exclude(state="PN"):
        if seat.date_assigned:
            seat.date_activated = datetime.datetime.combine(
                seat.date_assigned, datetime.datetime.min.time())
        if seat.user.last_login:
            seat.last_activity = seat.user.last_login
        seat.save()


class Migration(migrations.Migration):

    dependencies = [
        ('seat', '0004_bulkfundraiserimport'),
    ]

    operations = [
        migrations.AddField(
            model_name='seat',
            name='date_activated',
            field=models.DateTimeField(help_text='When this seat was made Active (the first time the user accessed this seat).', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='seat',
            name='last_activity',
            field=models.DateTimeField(help_text='The last time this seat was used in the site. This will only be updated once per day.', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.RunPython(set_new_dates,
                             reverse_code=lambda x, y: True)
    ]
