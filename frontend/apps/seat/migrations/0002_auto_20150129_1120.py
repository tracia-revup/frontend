# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('seat', '0001_initial'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='seat',
            index_together=set([('user', 'state', 'account'), ('account', 'state', 'seat_type')]),
        ),
    ]
