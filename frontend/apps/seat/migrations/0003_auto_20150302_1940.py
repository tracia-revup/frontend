# -*- coding: utf-8 -*-


from django.conf import settings
from django.db import models, migrations
import django.contrib.postgres.fields


def migrate_permission_groups(apps, schema_editor):
    """Migrate the groups into the arrays"""
    Seat = apps.get_model('seat', 'Seat')
    for seat in Seat.objects.all():
        groups = {pg.name for pg in seat.permission_groups.all()}
        permissions = []
        roles = []
        if settings.FUNDRAISER_GROUP_NAME in groups:
            role = seat.account.roles.get(name="Fundraiser")
            roles.append(role)
            permissions.extend(role.permissions)
        if settings.ACCOUNT_ADMIN_GROUP_NAME in groups:
            role = seat.account.roles.get(name="Admin")
            roles.append(role)
            permissions.extend(role.permissions)

        if permissions:
            seat.permissions = permissions
            seat.save()
            seat._roles.add(*roles)


def reverse_migrate(apps, schema_editor):
    from frontend.apps.seat.models import Seat as S
    Seat = apps.get_model('seat', 'Seat')
    Group = apps.get_model("auth", "Group")

    ag, created = Group.objects.get_or_create(
        name=settings.ACCOUNT_ADMIN_GROUP_NAME)
    fg, created = Group.objects.get_or_create(
        name=settings.FUNDRAISER_GROUP_NAME)

    ap = set(S.AdminPermissions.all())
    fp = set(S.FundraiserPermissions.all())

    for seat in Seat.objects.all():
        permissions = set(seat.permissions)
        if permissions.intersection(ap):
            seat.permission_groups.add(ag)
        if permissions.intersection(fp):
            seat.permission_groups.add(fg)


class Migration(migrations.Migration):

    dependencies = [
        ('role', '0001_initial'),
        ('seat', '0002_auto_20150129_1120'),
    ]

    operations = [
        migrations.AddField(
            model_name='seat',
            name='_roles',
            field=models.ManyToManyField(to='role.Role'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='seat',
            name='permissions',
            field=django.contrib.postgres.fields.ArrayField(
                models.CharField(max_length=3), default=[], size=None,
                blank=True),
            preserve_default=True,
        ),
        migrations.RunPython(
            migrate_permission_groups,
            reverse_code=reverse_migrate
        ),
        migrations.RemoveField(
            model_name='seat',
            name='permission_groups',
        ),
    ]
