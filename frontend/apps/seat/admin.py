from django.contrib import admin

from .models import Seat, DelayedAnalysis
from frontend.libs.utils.admin_utils import UserAccountAdmin


class SeatAdmin(UserAccountAdmin):
    raw_id_fields = UserAccountAdmin.raw_id_fields + ("inviting_user",)
    list_display = ('id', 'user_email', 'state', 'user_name', 'account_title')
    actions = ["revoke_seat"]

    def get_actions(self, request):
        actions = super().get_actions(request)
        del actions['delete_selected']
        return actions

    def revoke_seat(self, request, queryset):
        for seat in queryset.iterator():
            seat.revoke(force=True)

    revoke_seat.short_description = "Revoke the selected Seat(s)"


class DelayedAnalysisAdmin(admin.ModelAdmin):
    raw_id_fields = ("seat",)
    list_display = ("id", "seat", "delayed_analysis", "contact_source")
    search_fields = ("seat__user__email", "seat__account__organization_name")

    def get_queryset(self, request):
        qs = super(DelayedAnalysisAdmin, self).get_queryset(request)
        qs.select_related("seat__user", "seat__account").prefetch_related(
            "contact_source")
        return qs


admin.site.register(Seat, SeatAdmin)
admin.site.register(DelayedAnalysis, DelayedAnalysisAdmin)