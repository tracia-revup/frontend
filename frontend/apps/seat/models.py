from collections import Iterable
import datetime
import itertools
import logging
from io import StringIO

from django.contrib.postgres import fields
from django.db import models, transaction
from django.db.models import Q
from waffle.interface import flag_is_active_for_user

from frontend.apps.contact.utils import ContactSourceMixin
from frontend.apps.contact_set.models import ContactSet
from frontend.apps.role import permissions
from frontend.apps.role.models import Role
from frontend.libs.mixins import SimpleUnicodeMixin
from frontend.libs.utils.general_utils import (DictReaderInsensitive,
                                               instance_cache)
from frontend.libs.utils.model_utils import (
    Choices, GenericForeignKeyQuerySet, LockingModelMixin)
from frontend.libs.utils.task_utils import WaffleFlagsMixin, ANALYSIS_FLAGS


LOGGER = logging.getLogger(__name__)


class SeatQuerySet(GenericForeignKeyQuerySet):
    def temporary(self):
        return self.filter(seat_type=Seat.Types.TEMPORARY)

    def assigned(self):
        return self.filter(state__in=Seat.States.ASSIGNED)

    def revoked(self):
        return self.filter(state=Seat.States.REVOKED)

    def filled(self):
        return self.assigned().temporary()


class DuplicateSeatError(Exception): pass
class SeatRevokeError(Exception): pass


class Seat(models.Model, LockingModelMixin, SimpleUnicodeMixin):
    class Types(Choices):
        choices_map = [
            ("TM", "TEMPORARY", "Temporary"),
            ("PM", "PERMANENT", "Permanent"),
        ]

    class States(Choices):
        choices_map = [
            ("PN", "PENDING", "Pending"),
            ("AC", "ACTIVE", "Active"),
            ("RV", "REVOKED", "Revoked"),
        ]
        ASSIGNED = ("PN", "AC")

    # Adding the permission classes as attributes for easy access
    AdminPermissions = permissions.AdminPermissions
    FundraiserPermissions = permissions.FundraiserPermissions

    user = models.ForeignKey("authorize.RevUpUser", on_delete=models.CASCADE, related_name="seats")
    inviting_user = models.ForeignKey("authorize.RevUpUser", related_name="+",
                                      null=True, blank=True,
                                      on_delete=models.SET_NULL)
    account = models.ForeignKey("campaign.Account", on_delete=models.CASCADE, related_name="seats")
    seat_type = models.CharField(
        choices=Types.choices(),
        default=Types.TEMPORARY,
        max_length=2,
        blank=True)
    date_assigned = models.DateField(
        default=datetime.date.today,
        blank=True,
    )
    date_revoked = models.DateField(
        default=None,
        null=True,
        blank=True
    )
    date_activated = models.DateTimeField(
        blank=True,
        null=True,
        help_text="When this seat was made Active "
                  "(the first time the user accessed this seat).")
    last_activity = models.DateTimeField(
        blank=True,
        null=True,
        help_text="The last time this seat was used in the site. This will "
                  "only be updated once per day.")
    delayed_analysis = models.DateTimeField(
        blank=True,
        null=True,
        help_text="This will be set if this seat needs a new analysis. The "
                  "analysis will be run the next time the seat is active."
    )
    state = models.CharField(
        choices=States.choices(),
        default=States.PENDING,
        max_length=2,
        blank=True
    )

    # Roles and permissions are directly linked. The permissions from the
    # roles are aggregated into the permissions field. The permissions field
    # is the field that has actual power for permissions. The purpose of
    # roles is to allow the users to organize permissions into meaningful
    # groups.
    # Note: Don't access _roles directly. To manage roles, call 'add_role'
    #       and 'remove_role'. This will ensure 'permissions' is correct.
    _roles = models.ManyToManyField("role.Role")
    permissions = fields.ArrayField(
        models.CharField(max_length=3),
        default=[], blank=True
    )
    notes = models.TextField(blank=True, null=True)

    objects = SeatQuerySet.as_manager()

    class Meta:
        index_together = [
            ("account", "state", "seat_type"),
            ("user", "state", "account")
        ]

    def __str__(self):
        return "({}) {};   {}".format(self.id, self.user, self.account)

    def lock(self):
        """Lock this model in the DB.
            This needs to be called from within a transaction.
        """
        return Seat.objects.select_for_update().get(pk=self.pk)

    @property
    @instance_cache
    def account_child(self):
        """Returns the appropriate child class of Account
        (e.g. PoliticalCampaign).
        """
        from frontend.apps.campaign.models import Account
        return Account.objects.get(pk=self.account_id)

    def roles(self, **kwargs):
        """_roles shouldn't be accessed directly, and should be read-only,
        so this method allows access to roles in a read-only fashion.
        """
        return (r for r in self._roles.filter(**kwargs))

    def _update_roles(self, roles, set_method, roles_method):
        """Add/remove one or more roles and update the permissions.

        This method takes a conservative approach and only updates the
        permissions that are in the role. It does not recompute permissions
        from other roles. This allows the user to change permissions and not
        have them overwritten by adding/removing an unrelated role.
        """
        if not roles:
            return
        # Build a set of all permissions in the given roles
        permissions = set(itertools.chain(*(r.permissions for r in roles)))
        # Combine the new permissions with the existing permissions to
        # calculate the correct permissions.
        self.permissions = list(set_method(set(self.permissions), permissions))
        self.save()
        roles_method(*roles)

    @transaction.atomic
    def add_roles(self, *roles):
        """Add one or more roles and update the permissions."""
        role_set = {r.id for r in roles}
        seat = self.lock()
        roles = Role.objects.filter((Q(account=self.account) |
                                     Q(account__isnull=True)) &
                                    Q(id__in=role_set))
        # Verify the given IDs match with Roles owned by this account
        if len(roles) != len(role_set):
            raise ValueError("One or more of the given roles not found.")
        seat._update_roles(roles, set.union, self._roles.add)
        # Update current seat for continuity, but don't save
        self.permissions = seat.permissions

    @transaction.atomic
    def remove_roles(self, *roles):
        """Remove one or more roles and update the permissions."""
        seat = self.lock()
        # Only remove roles this seat actually has
        roles = self._roles.filter(id__in={r.id for r in roles})
        seat._update_roles(roles, set.difference, self._roles.remove)
        # Update current seat for continuity, but don't save
        self.permissions = seat.permissions

    def has_permissions(self, any=None, all=None):
        """Is user active seat membership in any or all of the groups.

        :param any: An iterator of group names that will allow access if the
                    seat is a member of any of them
        :param all: An iterator of group names that will allow access only if
                    the seat is a member of all of them.
        """
        if any and all:
            raise AttributeError("May only specify one of 'any' or 'all'")
        elif not (any or all):
            raise AttributeError("Must specify at least one of 'any' or 'all'")

        # If this seat is not active, then it has no permissions.
        if self.state not in self.States.ASSIGNED:
            return False

        group_names = set(any or all)

        matches = group_names.intersection(self.permissions)
        # If the given group names match the any groups, the all groups,
        # or the user is a superuser, we will return True
        if (any and matches) or (all and len(matches) == len(group_names)):
            return True
        else:
            return False

    def has_admin_access(self):
        """Shortcut method to check if this seat has access to the admin tab.
        Useful for templates.

        Conditions:
           1) This seat has admin-level permissions
        OR 2) This seat is a manager of a management group
        """
        return (self.has_permissions(any=self.AdminPermissions.all()) or
                # TODO: Add a cache layer for this
                self.management_groups_manager.exists())

    def revocable(self):
        # If it's already been revoked, then we return true. This is mostly
        # because the 'revoke' method below doesn't error for revoked seats.
        # Pending seats can also be revoked at any time (while Pending).
        if self.state in (self.States.PENDING, self.States.REVOKED):
            return True

        return bool(
            # Permanent seats can be revoked any time. They are not bound to
            # the account like a Temporary seat is.
            self.seat_type == self.Types.PERMANENT or
            (self.date_assigned and
            # 30 is hardcoded for now, but later we could move it to account.
            (datetime.datetime.utcnow().date() - self.date_assigned).days > 30)
            # TODO: Really shouldn't look at Account in here
            or self.account.seat_count is None
        )

    def revoke(self, save=True, force=False):
        """Revoke this seat.

        If the seat is already revoked, do nothing.
        If 'force' is True, this seat will be revoked regardless of
        revocable. This should only be used by staff.
        """
        if self.state not in Seat.States.ASSIGNED:
            return

        if not self.revocable() and not force:
            raise SeatRevokeError("This seat is not revocable")

        self.state = Seat.States.REVOKED
        self.date_revoked = datetime.datetime.utcnow().date()
        if save:
            self.save()
            self.account.account_group.user_set.remove(self.user)
            # Check for this seat in the Management Groups and remove it
            from frontend.apps.campaign_admin.models import ManagementGroup
            for attr in ('managers', 'members'):
                for mg in ManagementGroup.objects.filter(**{
                        "{}__pk".format(attr): self.pk}):
                    getattr(mg, attr).remove(self)

    def activate(self):
        if self.state == Seat.States.PENDING:
            self.state = Seat.States.ACTIVE
            if not self.date_activated:
                self.date_activated = datetime.datetime.utcnow()
            self.save()
            self.account.account_group.user_set.add(self.user)
            return True
        return False

    def get_default_analysis(self):
        from frontend.apps.analysis.models import Analysis
        return Analysis.objects.exclude(partition_set=None).filter(
            user=self.user, account=self.account,
            status=Analysis.STATUSES.complete).order_by("-modified").first()

    @classmethod
    def create(cls, user, account, permissions_=None, roles=None, **kwargs):
        """Create an instance of Seat with error checking."""
        # Verify this user does not have an active seat with this account
        if user.seats.assigned().filter(account=account).exists():
            raise DuplicateSeatError("User already has an active seat "
                                     "with that account.")

        seat = cls.objects.create(
            user=user, account=account,
            permissions=permissions_ or [],
            **kwargs)
        # Add the roles, if there are any
        seat.add_roles(*(roles or []))

        # If the seat is being created for the account head, make it their
        # current seat
        if user.id == account.account_head_id:
            user.current_seat = seat
            user.save(update_fields=['current_seat'])

        # Schedule a delayed analysis
        DelayedAnalysis.schedule_analysis(seat, None)

        # If the seat doesn't have a root contact set, create it.
        root = ContactSet.get_or_create_root(account=account, user=user)

        # If this user has ContactSets for their import sources, we need to
        # clone them for this Seat too
        for cs in ContactSet.objects.filter(
                user=seat.user,
                source__in=ContactSet.Sources.import_auto_gen_choices).exclude(
                account=seat.account).distinct("source", "source_uid"):
            ContactSet.get_or_create(
                seat.account, seat.user, parents=root,
                filter_kwargs=dict(source=cs.source, source_uid=cs.source_uid),
                create_kwargs=dict(
                    title=cs.title, query_args=cs.query, count=cs.count,
                    filters=cs.filters.all(), source=cs.source,
                    source_uid=cs.source_uid))
        return seat


class DelayedAnalysis(ContactSourceMixin, WaffleFlagsMixin):
    flag_names = ANALYSIS_FLAGS

    seat = models.ForeignKey(Seat, on_delete=models.CASCADE, related_name="delayed_analyses")
    delayed_analysis = models.DateTimeField(
        default=datetime.datetime.now,
        help_text="The earliest time this analysis will be run."
    )

    class Meta:
        verbose_name_plural = "DelayedAnalyses"

    def __str__(self):
        return "{} ({})".format(self.seat, self.contact_source)

    @classmethod
    def _waffle_method(cls):
        return flag_is_active_for_user

    def submit_analysis(self, countdown=0, task_lock=None, delete=True):
        from frontend.apps.analysis.utils import submit_analysis
        flags = self._prepare_flags(self.seat.user)
        submit_analysis(self.seat.user, self.seat.account,
                        contact_source=self.contact_source,
                        countdown=countdown, flags=flags, task_lock=task_lock)
        if delete:
            self.delete()

    @classmethod
    def bulk_submit(cls, seat, task_lock=None):
        """Submit all the delayed analyses for a given seat."""
        with transaction.atomic():
            if isinstance(seat, Seat):
                seat = seat.lock()
            else:
                seat = Seat.objects.select_for_update().get(id=seat)
            delayed_analyses = list(seat.delayed_analyses.all())
            # Add a second per delayed analysis over 1. We do this so we can
            # ensure we get all delayed analyses queued before any execute.
            # This ensures any overlapping analyses will be cancelled out by
            # submit_analysis.
            countdown = len(delayed_analyses) - 1
            for delayed_analysis in delayed_analyses:
                delayed_analysis.submit_analysis(countdown=countdown,
                                                 task_lock=task_lock)

            # Clear the timestamp on the seat
            seat.delayed_analysis = None
            seat.save(update_fields=["delayed_analysis"])

    @classmethod
    def schedule_analysis(cls, seat, source, eta=None, create=True):
        """Schedule a seat to have a delayed analysis.

        :param source: The contacts source.
                       Should be: Contact, ContactSet, or None
        :param eta: datetime if the analysis needs to be delayed until a
                    certain date/time.
        :param create: If the DelayedAnalysis should be saved. In general, you
                    want to leave this True. If you set it False, you'll need
                    to manually update the delayed analysis field in Seat.
        """
        if not eta:
            eta = datetime.datetime.now()
        kwargs = dict(seat=seat, delayed_analysis=eta, contact_source=source)
        if create:
            with transaction.atomic():
                seat = seat.lock()
                obj = DelayedAnalysis.objects.create(**kwargs)
                seat.delayed_analysis = eta
                seat.save(update_fields=["delayed_analysis"])
        else:
            obj = DelayedAnalysis(**kwargs)
        return obj

    @classmethod
    def bulk_schedule(cls, seats, sources, eta=None):
        """Bulk schedule all of the given seats for a delayed analysis."""
        if not isinstance(sources, Iterable):
            sources = [sources] * len(seats)
        assert len(seats) == len(sources)

        with transaction.atomic():
            seat_ids = [s.id for s in seats]
            # Lock the seats from access while we schedule the new analyses
            Seat.objects.select_for_update().filter(id__in=seat_ids)
            # Schedule DelayedAnalysis for each seat-source combo.
            objs = (cls.schedule_analysis(seat, source, eta=eta, create=False)
                    for seat, source in zip(seats, sources))
            cls.objects.bulk_create(objs)
            # Set the timestamp on all of the scheduled seats
            Seat.objects.filter(id__in=seat_ids).update(
                delayed_analysis=datetime.datetime.now())


class BulkFundraiserImport(models.Model):
    class FileType(Choices):
        choices_map = [
            ("CSV", "CSV", "CSV File"),
        ]
    account = models.ForeignKey("campaign.Account", on_delete=models.CASCADE,
                                related_name="bulk_imports")
    user = models.ForeignKey("authorize.RevUpUser", on_delete=models.CASCADE, related_name='+',
                             help_text="The user who started this task")
    file_type = models.CharField(max_length=3, db_index=True,
                                 choices=FileType.choices(),
                                 default=FileType.CSV)
    data_file = models.BinaryField()

    def process_file(self, protocol_host=None):
        """Process this file and create Seats for these fundraisers.

        :param protocol_host: This is necessary if you want to send an email
                  to the user. Without a request, we don't know which site
                  we are sending from. E.g. "https://www.revup.com"
        """
        return getattr(self,
                       "_process_{}".format(
                           self.file_type.lower()))(protocol_host)

    @classmethod
    def create_csv(cls, account, user, data_file):
        """Create an instance with a CSV file. Verify the file is valid."""
        csv_file = DictReaderInsensitive(data_file)
        if 'email' not in csv_file.fieldnames:
            raise ValueError("CSV fundraiser import files are required to "
                             "have an 'email' column")
        data_file.seek(0)
        return cls.objects.create(account=account, user=user,
                                  file_type=cls.FileType.CSV,
                                  data_file=data_file.read())

    def _process_csv(self, protocol_host=None):
        """Process a CSV file.

        Rows that don't fit required formats will be thrown out.

        :param protocol_host: This is necessary if you want to send an email
                  to the user. Without a request, we don't know which site
                  we are sending from. E.g. "https://www.revup.com"
        """
        # Typically, I wouldn't do imports like this, but since this is a
        # model, I didn't want run into circular import issues.
        from django.core.validators import validate_email, ValidationError
        from .helpers import build_seat_and_notify
        from frontend.apps.role.models import Role
        from frontend.apps.campaign_admin.models import ManagementGroup

        errors = []
        def add_error(i, error):
            errors.append("Line {}: {}".format((i+1), error))

        role_cache = {}
        mg_cache = {}

        def get_role(role_name):
            if role_name not in role_cache:
                role = Role.objects.filter(
                    (Q(account=self.account) | Q(account__isnull=True)) &
                    Q(name__iexact=role_name)).order_by("account_id").first()
                if not role:
                    raise Role.DoesNotExist
                else:
                    role_cache[role_name] = role

            return role_cache[role_name]

        def get_mg(mg_name):
            if mg_name not in mg_cache:
                mg_cache[mg_name] = ManagementGroup.objects.get(
                    account=self.account,
                    name__iexact=mg_name)
            return mg_cache[mg_name]

        csv_file = DictReaderInsensitive(StringIO(self.data_file))
        count_success = 0
        count_fail = 0
        for index, row in enumerate(csv_file):
            try:
                email = row.get('email')
                if not email:
                    # Blank lines often creep into csv files
                    continue
                try:
                    validate_email(email)
                except ValidationError:
                    error_string = "Fundraiser creation failed. Reason: " \
                                   "Valid email is required. " \
                                   "Invalid: '{}'".format(email)
                    add_error(index, error_string)
                    raise ValueError(error_string)

                roles_ = row.get('roles', row.get('role'))
                roles_ = roles_.split(",")
                roles = []
                for role in roles_:
                    try:
                        role = role.strip()
                        if role:
                            roles.append(get_role(role))
                    except Role.DoesNotExist:
                        add_error(index,
                                  "Fundraiser created, but failed to add Role."
                                  " Reason: Invalid Role: {}".format(role))
                        raise

                # Create the Seat
                try:
                    # Create a seat and notify the new user
                    user, seat = build_seat_and_notify(email, self.account,
                                                       roles, protocol_host,
                                                       inviting_user=self.user)
                except DuplicateSeatError:
                    # If this seat already exists, we do not process further.
                    continue

                team = row.get('team', "").strip().lower()
                team_position = row.get("team position", "").strip().lower()
                if not team_position:
                    team_position = "member"
                    
                if team_position not in ('manager', 'member'):
                    error_string = "Invalid team position: {}.".format(
                        team_position)
                    add_error(index, error_string +
                                     " Must be 'manager' or 'member'")
                    raise ValueError(error_string)

                if team:
                    try:
                        mg = get_mg(team)
                    except ManagementGroup.DoesNotExist:
                        add_error(index,
                                  "Fundraiser created, but failed to "
                                  "add Team. Invalid Team: {}".format(team))
                        raise

                    # E.g. management_group.add_members(seat)
                    try:
                        getattr(mg, "add_{}s".format(team_position))(seat)
                    except ValueError as err:
                        # This shouldn't happen, so we should log an error
                        LOGGER.error("Seat({}) is already in opposing position"
                                     "to '{}' in MG({})".format(
                                     seat.id, team_position, mg.id))
                        raise

            except Exception as err:
                LOGGER.warn("Failed to bulk import fundraiser for Account {}. "
                            "Error: {}".format(self.account_id, err))
                count_fail += 1
            else:
                count_success += 1

        LOGGER.info("Processed {} total fundraisers for Account {}: "
                    "{} succeeded, {} failed".format(
                    (count_success + count_fail), self.account_id,
                    count_success, count_fail))
        return count_success + count_fail, errors
