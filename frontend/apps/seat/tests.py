import datetime

from django.http import HttpResponse
from django.test import Client
from django.urls import reverse
import unittest.mock as mock
from rest_framework.exceptions import APIException

from frontend.apps.authorize.api.serializers import MINIMAL_FIELDS
from frontend.apps.authorize.models import RevUpUser
from frontend.apps.authorize.factories import RevUpUserFactory
from frontend.apps.authorize.test_utils import (AuthorizedUserTestCase,
                                                TestCase, TransactionTestCase)
from frontend.apps.campaign.factories import (PoliticalCampaignFactory,
                                              EventFactory)
from frontend.apps.campaign_admin.factories import ManagementGroupFactory
from frontend.apps.contact_set.factories import ContactSet, ContactSetFactory
from frontend.apps.role.decorators import any_group_required
from frontend.apps.role.factories import RoleFactory
from frontend.apps.role.permissions import (FundraiserPermissions,
                                            AdminPermissions)
from frontend.apps.seat.factories import SeatFactory
from frontend.apps.seat.models import Seat, SeatRevokeError, DuplicateSeatError
from frontend.libs.utils.string_utils import random_email, random_string


class SeatModelTests(TestCase):
    def setUp(self):
        self.seat = SeatFactory(state=Seat.States.PENDING)

    def test_basic(self):
        self.assertEqual(self.seat.seat_type, Seat.Types.TEMPORARY)
        self.assertEqual(self.seat.state, Seat.States.PENDING)
        self.assertIsNotNone(self.seat.date_assigned)
        self.assertIsNone(self.seat.date_revoked)

    def test_revocable(self):
        self.assertEqual(self.seat.seat_type, Seat.Types.TEMPORARY)
        self.assertEqual(self.seat.state, Seat.States.PENDING)

        # Pending seats should be revocable
        self.assertTrue(self.seat.revocable())

        # Revoked seats should be revocable
        self.seat.state = Seat.States.REVOKED
        self.assertEqual(self.seat.state, Seat.States.REVOKED)
        self.assertTrue(self.seat.revocable())

        # Verify Active seats within the 30 days are not revocable
        self.seat.state = Seat.States.ACTIVE
        self.assertEqual(self.seat.state, Seat.States.ACTIVE)
        self.seat.date_assigned = datetime.datetime.utcnow().date()
        self.assertFalse(self.seat.revocable())

        # Verify Permanent seats are revocable
        self.seat.seat_type = Seat.Types.PERMANENT
        self.assertEqual(self.seat.seat_type, Seat.Types.PERMANENT)
        self.assertTrue(self.seat.revocable())

        # Verify Active seats outside 30 days are revocable
        self.seat.seat_type = Seat.Types.TEMPORARY
        self.assertEqual(self.seat.seat_type, Seat.Types.TEMPORARY)
        self.assertEqual(self.seat.state, Seat.States.ACTIVE)
        self.assertFalse(self.seat.revocable())
        self.seat.date_assigned = (self.seat.date_assigned -
                                   datetime.timedelta(days=31))
        self.assertTrue(self.seat.revocable())

    def test_revoke(self):
        with mock.patch("frontend.apps.seat.models.Seat.revocable") as m:
            m.return_value = False
            self.assertEqual(self.seat.state, Seat.States.PENDING)

            ## Verify non revocable seats raise an error
            self.assertRaisesRegex(SeatRevokeError, "seat is not revocable",
                                    self.seat.revoke)

            ## Verify seats can be force-revoked
            self.seat.revoke(force=True)
            self.assertEqual(self.seat.state, Seat.States.REVOKED)

            ## Verify revoke does nothing with Revoked seats
            date_revoked = self.seat.date_revoked
            self.seat.revoke()
            self.assertEqual(self.seat.state, Seat.States.REVOKED)
            self.assertEqual(self.seat.date_revoked, date_revoked)

            ## Verify positive case
            seat = SeatFactory()
            m.return_value = True
            self.assertIsNone(seat.date_revoked)
            seat.revoke()
            self.assertIsNotNone(seat.date_revoked)
            self.assertEqual(seat.state, Seat.States.REVOKED)

            # Create a management group with a seat in it, and verify
            # seat is removed.
            seat = SeatFactory()
            mg = ManagementGroupFactory(managers=[seat])
            mg2 = ManagementGroupFactory(members=[seat])
            self.assertIn(seat, mg.managers.all())
            self.assertIn(seat, mg2.members.all())
            seat.revoke()
            self.assertNotIn(seat, mg.managers.all())
            self.assertNotIn(seat, mg2.members.all())

    def test_create(self):
        inviter = RevUpUserFactory()
        user = RevUpUserFactory()
        account = PoliticalCampaignFactory()

        ## Verify positive case
        # Ensure no ContactSet exists for this seat
        self.assertFalse(ContactSet.objects.filter(
            user=user, account=account).exists())
        s = Seat.create(user, account, ["PD"],
                        seat_type=Seat.Types.PERMANENT,
                        inviting_user=inviter)
        self.assertEqual(s.user, user)
        self.assertEqual(s.account, account)
        self.assertEqual(s.permissions, ["PD"])
        self.assertEqual(s.seat_type, Seat.Types.PERMANENT)
        self.assertEqual(s.state, Seat.States.PENDING)
        self.assertEqual(s.inviting_user, inviter)
        # Verify a default CS was created for the seat
        self.assertEqual(ContactSet.objects.filter(
            user=user, account=account).count(), 1)
        # Verify the current_seat was not set for a non account head
        user.refresh_from_db()
        self.assertNotEqual(user.current_seat, s)

        ## Verify attempting to create a seat for an active user fails
        self.assertRaisesRegex(DuplicateSeatError,
                                "User already has an active seat",
                                Seat.create, user, account, [])

        ## Verify attempting to create a seat for a revoked user succeeds
        s.revoke(force=True)
        self.assertEqual(s.state, Seat.States.REVOKED)
        s2 = Seat.create(user=user, account=account, permissions_=["PD"],
                         seat_type=Seat.Types.PERMANENT)
        self.assertEqual(s.user, user)
        self.assertEqual(s.account, account)
        self.assertEqual(s2.state, Seat.States.PENDING)

        self.assertEqual(Seat.objects.filter(user=user,
                                             account=account).count(), 2)

    def test_create_account_head(self):
        """Verify the current_seat is set for account heads"""
        user = RevUpUserFactory()
        self.assertFalse(Seat.objects.filter(user=user).exists())
        account = PoliticalCampaignFactory(account_head=user)

        ## Verify positive case
        permissions = list(Seat.FundraiserPermissions.all() +
                                        Seat.AdminPermissions.all())
        s = Seat.objects.get(user=user, account=account)
        self.assertEqual(s.user, user)
        self.assertEqual(s.account, account)
        self.assertEqual(s.permissions, permissions)
        self.assertEqual(s.seat_type, Seat.Types.PERMANENT)
        self.assertEqual(s.state, Seat.States.ACTIVE)
        # Verify the current_seat was set for the account head
        user.refresh_from_db()
        self.assertEqual(user.current_seat, s)

    def test_create_import_cs_cloning(self):
        user = RevUpUserFactory()
        account = PoliticalCampaignFactory()
        account2 = PoliticalCampaignFactory()
        source = "CV"
        source_uid = "test_file.csv"

        ContactSetFactory(user=user, account=account2,
                          source=source, source_uid=source_uid)
        self.assertEqual(ContactSet.objects.filter(user=user).count(), 1)
        s = Seat.create(user, account, ["PD"])
        self.assertEqual(ContactSet.objects.filter(user=user).count(), 3)
        # Verify the import CS was cloned
        self.assertTrue(ContactSet.objects.get(
            user=user, account=account, source=source, source_uid=source_uid))
        # Verify the other created cs was the root
        self.assertTrue(ContactSet.objects.get(
            user=user, account=account, source=""))

    def test_is_group_member(self):
        group1 = "ab"
        group2 = "cd"
        group3 = "ef"
        self.seat.permissions = [group1, group2]
        self.seat.state = Seat.States.ACTIVE

        # Verify any
        self.assertTrue(self.seat.has_permissions(any=(group1,
                                                       group3)))
        self.assertTrue(self.seat.has_permissions(any=(group1,
                                                       group2)))
        self.assertFalse(self.seat.has_permissions(any=(group3,)))

        # Verify all
        self.assertTrue(self.seat.has_permissions(all=(group1,)))
        self.assertTrue(self.seat.has_permissions(all=(group1,
                                                       group2)))
        self.assertFalse(self.seat.has_permissions(all=(
            group1, group2, group3)))
        self.assertFalse(self.seat.has_permissions(all=(group1,
                                                        group3)))

    def test_add_role(self):
        ## Verify a role cannot be added that's not on the account
        role = RoleFactory()
        with self.assertRaises(ValueError) as err:
            self.seat.add_roles(role)
        exception_message = err.exception.args[0]
        self.assertEqual('One or more of the given roles not found.',
                         exception_message)

        ## Verify a role on the account can
        role = RoleFactory(account=self.seat.account, permissions=["abc"])
        expected_permissions = set(self.seat.permissions)
        expected_permissions.add("abc")
        self.seat.add_roles(role)
        self.assertEqual(set(self.seat.permissions), expected_permissions)

    def test_remove_role(self):
        ## Verify a role not on the seat has no affect when removed
        role1 = RoleFactory(account=self.seat.account, permissions=["abc"])
        role2 = RoleFactory(account=self.seat.account, permissions=["def"])
        self.seat.add_roles(role1)
        expected_permissions = set(self.seat.permissions)
        expected_permissions.add("abc")
        self.assertEqual(set(self.seat.permissions), expected_permissions)

        # Remove the other role
        self.seat.remove_roles(role2)
        # Verify nothing changed
        self.assertEqual(set(self.seat.permissions), expected_permissions)

        # Remove the first role and verify it's gone
        self.seat.remove_roles(role1)
        expected_permissions.remove("abc")
        self.assertEqual(set(self.seat.permissions), expected_permissions)


class SelectActiveSeatViewTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        super(SelectActiveSeatViewTests, self).setUp(set_session=False)
        self.seat1 = SeatFactory(user=self.user, state=Seat.States.PENDING)
        self.seat2 = SeatFactory(user=self.user, state=Seat.States.PENDING)
        self.seat3 = SeatFactory(user=self.user, state=Seat.States.PENDING)
        self.url = reverse('select_current_seat')

    def test_get(self):
        # Test basic positive case
        response = self.client.get("{}?next=test".format(self.url))
        self.assertContains(response, 'action="?next=test"')
        self.assertContains(response, 'value="{}"'.format(self.seat1.id))
        self.assertContains(response, self.seat1.account.title)
        self.assertContains(response, 'value="{}"'.format(self.seat2.id))
        self.assertContains(response, self.seat2.account.title)
        self.assertContains(response, 'value="{}"'.format(self.seat3.id))
        self.assertContains(response, self.seat3.account.title)

        # Set one of the seats to inactive and verify its absence
        self.seat3.revoke(force=True)
        response = self.client.get(self.url)
        self.assertContains(response, 'value="{}"'.format(self.seat1.id))
        self.assertContains(response, 'value="{}"'.format(self.seat2.id))
        self.assertNotContains(response, 'value="{}"'.format(self.seat3.id))

        # Set one of the accounts to inactive and verify an account is
        # automatically selected and redirected
        self.assertIsNone(self.user.current_seat)
        self.seat2.account.active = False
        self.seat2.account.save()
        target = reverse('terms_of_service')
        response = self.client.get("{}?next={}".format(self.url, target))
        self.assertRedirects(response, target)
        self.assertTrue(RevUpUser.objects.filter(
            id=self.user.id, current_seat=self.seat1).exists())
        # Set the user to be the account head of seat2 and verify the seat
        # is back because account heads can see their inactive seats
        old_user = self.seat2.account.account_head
        self.seat2.account.account_head = self.user
        self.seat2.account.save()
        self.assertFalse(self.seat2.account.active)
        response = self.client.get("{}?force=1".format(self.url))
        self.assertContains(response, 'value="{}"'.format(self.seat1.id))
        self.assertContains(response, 'value="{}"'.format(self.seat2.id))
        self.assertNotContains(response, 'value="{}"'.format(self.seat3.id))
        self.seat2.account.account_head = old_user
        self.seat2.account.save()

        # Revoke the last seat and verify the message
        self.seat1.revoke(force=True)
        response = self.client.get(self.url)
        self.assertContains(response, "You are not currently a fundraiser")
        # Make the user a superuser and verify they are allowed through
        self.user.is_superuser = True
        self.user.save()
        response = self.client.get(self.url)
        self.assertRedirects(response, reverse('staff_admin_home'))
        self.assertTrue(RevUpUser.objects.filter(
            id=self.user.id, current_seat=None).exists())

    def test_post(self):
        data = {'seat': self.seat1.id}
        target = reverse('terms_of_service')

        ## Verify simple positive case with redirect
        self.assertIsNone(self.user.current_seat)
        self.assertIsNone(self.seat1.date_activated)
        now = datetime.datetime.utcnow()
        response = self.client.post("{}?next={}".format(self.url, target),
                                    data=data)
        self.assertRedirects(response, target)
        self.assertTrue(RevUpUser.objects.filter(
            id=self.user.id, current_seat=self.seat1).exists())
        seat = Seat.objects.get(pk=self.seat1.pk, state=Seat.States.ACTIVE)
        self.assertTrue(
            now <= seat.date_activated <= datetime.datetime.utcnow())
        self.assertEqual(seat.action_object_actions.count(), 1)

        ## Verify case with bad seat
        data['seat'] = "abc"
        response = self.client.post(self.url, data=data)
        self.assertContains(response, 'value="{}"'.format(self.seat1.id))


class AccountSeatViewSetTransactionTests(TransactionTestCase):
    def setUp(self, **kwargs):
        self.campaign = PoliticalCampaignFactory(
            # Not sure why this is necessary, but the test fails without it
            account_profile__modified_by=None)
        self.user = RevUpUserFactory()
        self.seat = SeatFactory(account=self.campaign, user=self.user,
                                seat_type=Seat.Types.TEMPORARY,
                                state=Seat.States.ACTIVE,
                                permissions=Seat.AdminPermissions.all())
        self.url = reverse('account_seats_api-list',
                           args=(self.campaign.id,))

        # Login the user as a campaign admin
        self.client = Client()
        self.client.post(reverse('account_login'),
                         {'email': self.user.email,
                          'password': "this_Is_a_Test_Password1",
                          "login": "true"})

    def test_transaction_failure(self):
        # Mock out the notify function so we don't test that (separate test)
        with mock.patch("frontend.apps.seat.helpers."
                        "send_account_user_added_notification") as m:
            email = random_email()
            # Verify this user does not exist yet
            self.assertFalse(RevUpUser.objects.filter(username=email).exists())

            # First verify the simple case where objects are created
            response = self.client.post(self.url, data={'email': email})
            self.assertTrue(RevUpUser.objects.filter(email=email).exists())
            self.assertTrue(Seat.objects.filter(
                user__email=email, account=self.campaign).exists())

            # Make notification raise an error and verify objects don't exist
            m.side_effect = APIException
            email = random_email()
            response = self.client.post(self.url, data={'email': email})
            self.assertFalse(RevUpUser.objects.filter(email=email).exists())
            self.assertFalse(Seat.objects.filter(
                user__email=email, account=self.campaign).exists())


class SeatMiddlewareTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        super(SeatMiddlewareTests, self).setUp(**kwargs)
        self.seat = self.user.current_seat
        self.url = reverse('index')

    def test_last_activity(self):
        self.assertIsNone(self.seat.last_activity)
        now = datetime.datetime.utcnow()

        ## Verify last_activity is set when it is null
        self.client.get(self.url)
        seat = Seat.objects.get(pk=self.seat.pk)
        self.assertTrue(
            now <= seat.last_activity <= datetime.datetime.utcnow())
        time_cache = seat.last_activity

        ## Verify last_activity is not changed because it's the same day
        self.client.get(self.url)
        seat = Seat.objects.get(pk=self.seat.pk)
        self.assertEqual(time_cache, seat.last_activity)

        ## Verify different day is updated
        time_cache = time_cache - datetime.timedelta(days=1)
        seat.last_activity = time_cache
        seat.save()
        now = datetime.datetime.utcnow()
        self.client.get(self.url)
        seat = Seat.objects.get(pk=self.seat.pk)
        self.assertNotEqual(time_cache, seat.last_activity)
        self.assertTrue(
            now <= seat.last_activity <= datetime.datetime.utcnow())


class AccountSeatViewSetTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        self.campaign = PoliticalCampaignFactory()
        super(AccountSeatViewSetTests, self).setUp(login_now=False)
        self.seat = SeatFactory(account=self.campaign, user=self.user,
                                seat_type=Seat.Types.TEMPORARY,
                                state=Seat.States.ACTIVE)
        self.url = reverse('account_seats_api-list',
                           args=(self.campaign.id,))
        self.url2 = reverse('account_seats_api-detail',
                            args=(self.campaign.id, self.seat.id))

        # Add another user so we can see more than one user in the results
        self.seat2 = SeatFactory(account=self.campaign,
                                 seat_type=Seat.Types.TEMPORARY,
                                 state=Seat.States.ACTIVE)
        # Add another from a different campaign
        self.seat3 = SeatFactory(seat_type=Seat.Types.TEMPORARY,
                                 state=Seat.States.ACTIVE)

    def test_permissions(self):
        ## Verify any unauthenticated user is rejected
        result = self.client.get(self.url)
        self.assertNoCredentials(result)
        result = self.client.post(self.url, data={"abc": "123"})
        self.assertNoCredentials(result)

        # Login the user
        self._login(self.user)
        # Verify user is not an admin, or that would invalidate the test.
        self.assertFalse(self.user.is_admin)
        self.assertFalse(self.user.is_account_admin())

        ## Verify a standard user is rejected from viewing all users
        result = self.client.get(self.url)
        self.assertNoPermission(result)
        ## Verify a standard user is rejected from adding new seats
        result = self.client.post(self.url, data={"abc": "123"})
        self.assertNoPermission(result)
        # # Verify a standard user is rejected from updating seats
        result = self.client.put(self.url2, data={"abc": "123"})
        self.assertNoPermission(result)

        # Add the view permissions to the user
        self._add_admin_group(permissions=[AdminPermissions.VIEW_FUNDRAISERS])

        ## Verify the API now accepts the user
        result = self.client.get(self.url)
        seat_ids = {_['id'] for _ in result.json()['results']}
        first_names = {_['user']['first_name'] for _ in result.json()['results']}
        self.assertIn(self.seat.id, seat_ids)
        self.assertIn(self.user.first_name, first_names)
        self.assertIn(self.seat2.id, seat_ids)
        self.assertIn(self.seat2.user.first_name, first_names)

        ## Verify the user from another campaign is not in the results
        self.assertNotIn(self.seat3.id, seat_ids)

        ## Verify the API does not accept creation/update
        result = self.client.post(self.url, data={"abc": "123"})
        self.assertNoPermission(result)
        result = self.client.put(self.url2, data={"abc": "123"})
        self.assertNoPermission(result)
        result = self.client.patch(self.url2, data={"abc": "123"})
        self.assertNoPermission(result)

        # Add the modify permissions to the user
        self._add_admin_group(permissions=[AdminPermissions.MOD_FUNDRAISERS])

        ## Verify the API now accepts seat creation
        result = self.client.post(self.url, data={"abc": "123"})
        # Although this is an error state, it means the permissions were good
        self.assertContains(result, "'email' is required", status_code=500)
        ## Verify the API now accepts seat updates
        result = self.client.patch(self.url2, data='{"notes": "123"}',
                                   content_type="application/json")
        self.assertContains(result, '"id":{}'.format(self.seat.id))
        result = self.client.put(self.url2, data='{"notes": "123"}',
                                 content_type="application/json")
        self.assertContains(result, '"id":{}'.format(self.seat.id))

        # Remove the group from the user, but make them admin
        for seat in self.user.seats.all():
            seat.permissions = []
            seat.save()
        self.user.is_staff = True
        self.user.save()
        self.assertTrue(self.user.is_admin)
        self.assertFalse(self.user.is_account_admin())
        result = self.client.get(self.url)
        seat_ids = {_['id'] for _ in result.json()['results']}
        self.assertIn(self.seat.id, seat_ids)
        self.assertIn(self.seat2.id, seat_ids)

        result = self.client.post(self.url, data={"abc": "123"})
        # Although this is an error state, it means the permissions were good
        self.assertContains(result, "'email' is required", status_code=500)

    def test_update(self):
        # Login the user
        self._login_and_add_admin_group()

        ## Verify positive case
        # Put and Patch are treated the same in this API
        rs = random_string()
        result = self.client.patch(self.url2,
                                   data='{{"notes": "{}"}}'.format(rs),
                                   content_type="application/json")
        self.assertContains(result, '"id":{}'.format(self.seat.id))
        self.assertContains(result, '"notes":"{}"'.format(rs))
        rs = random_string()
        result = self.client.put(self.url2,
                                 data='{{"notes": "{}"}}'.format(rs),
                                 content_type="application/json")
        self.assertContains(result, '"id":{}'.format(self.seat.id))
        self.assertContains(result, '"notes":"{}"'.format(rs))

        ## Verify non-notes fields are rejected
        result = self.client.patch(self.url2,
                                   data='{"revocable": true}',
                                   content_type="application/json")
        self.assertContains(result, "Invalid fields in request: revocable",
                            status_code=500)

    def test_create(self):
        # Login the user
        self._login_and_add_admin_group()

        # Mock out the function so we don't test that (separate test)
        with mock.patch("frontend.apps.seat.helpers."
                        "send_account_user_added_notification") as m:
            ## Verify the API errs when no email is given
            response = self.client.post(self.url, data={'abc': '123'})
            self.assertContains(response, "'email' is required",
                                status_code=500)

            ## Verify the API errs when a bad email is given
            response = self.client.post(self.url, data={'email': '123'})
            self.assertContains(response, "'email' must be in a valid email "
                                          "format", status_code=500)

            ## Verify the campaign is added to an existing user's seats
            user = RevUpUserFactory(events=[])
            self.assertNotIn(self.campaign,
                             [s.account for s in user.seats.all()])
            response = self.client.post(self.url, data={'email': user.email})
            self.assertIn(self.campaign, [s.account for s in user.seats.all()])
            self.assertContains(response, '"user":{{"id":{}'.format(user.id))
            ## Verify the notification was called correctly
            m.assert_called_once_with(user, self.campaign,
                                      "http://testserver",
                                      inviting_user=self.user)

            seat = user.seats.get(account=self.campaign)
            self.assertEqual(self.user, seat.inviting_user)

            ## Try to add the same user again and verify an error
            response = self.client.post(self.url, data={'email': user.email})
            self.assertContains(response, "The fundraiser: '{}' is already "
                                          "a member".format(user.email),
                                status_code=500)

            # Reset the mock for the next test
            m.reset_mock()
            self.assertEqual(m.call_count, 0)

            ## Verify a new user is created when a new email is given
            rand_email = random_email()
            # Verify this user does not exist yet
            self.assertFalse(
                RevUpUser.objects.filter(username=rand_email).exists())

            # Run a bonus test that verifies notify can be disabled
            # Bonus test: Verify roles can be added
            role = RoleFactory(account=self.campaign)
            now = datetime.datetime.utcnow()
            response = self.client.post(self.url, data={'email': rand_email,
                                                        'notify': 'false',
                                                        'role': [role.id]})
            self.assertContains(response,
                                '"email":"{}"'.format(rand_email))
            ## Verify a user was actually created, and done correctly
            u = RevUpUser.objects.get(username=rand_email)
            self.assertEqual(u.email, rand_email)
            self.assertFalse(u.is_active)
            seat = u.seats.first()
            # Verify the seat
            self.assertEqual(self.campaign, seat.account)
            self.assertEqual(self.user, seat.inviting_user)
            self.assertEqual(seat.seat_type, Seat.Types.TEMPORARY)
            self.assertEqual(seat.state, Seat.States.PENDING)
            self.assertEqual(list(set(Seat.FundraiserPermissions.all())),
                             seat.permissions)
            self.assertEqual(list(seat.roles()), [role])
            self.assertEqual(set(seat.permissions), set(role.permissions))

            # Verify notification wasn't called
            self.assertEqual(m.call_count, 0)

            ## Verify seat count
            # Verify new seat cannot be created when quota exceeded
            self.campaign.seat_count_override = self.campaign.seats.filled().count()
            self.campaign.save()
            response = self.client.post(self.url,
                                        data={'email': random_email()})
            self.assertContains(response, "does not have any available seats",
                                status_code=500)

            # Verify unlimited seats
            self.campaign.seat_count_override = None
            self.campaign.save()
            email = random_email()
            response = self.client.post(self.url, data={'email': email})
            self.assertContains(response, email)

    def test_resend_email(self):
        # Login the user
        self._login_and_add_admin_group()

        # Mock out the function so we don't test that (separate test)
        with mock.patch("frontend.apps.seat.api.views."
                        "send_account_user_added_notification") as m:
            # Resend email
            response = self.client.get(self.url2 + 'resend_email/')

            # Verify email was sent
            m.assert_called_once_with(self.user, self.campaign,
                                      "http://testserver",
                                      inviting_user=self.user)

    def test_destroy(self):
        # Login the user
        self._login_and_add_admin_group()

        # Verify campaign in user
        self.assertTrue(self.user.seats.filter(
            account=self.campaign, state=Seat.States.ACTIVE).exists())

        # Attempt to revoke our own invitation (should fail)
        response = self.client.delete(self.url2)

        # Verify campaign wasn't removed
        self.assertTrue(self.user.seats.filter(
            account=self.campaign, state=Seat.States.ACTIVE).exists())
        self.assertContains(response, "Can't revoke your own access.",
                            status_code=500)

        # Verify campaign in second user
        user2 = self.seat2.user
        self.assertTrue(user2.seats.filter(
            account=self.campaign, state=Seat.States.ACTIVE).exists())

        # Verify user can't be revoked because of time-frame
        revoke_url = reverse('account_seats_api-detail',
                             args=(self.campaign.id, self.seat2.id,))
        response = self.client.delete(revoke_url)
        self.assertContains(response, "This seat is not revocable",
                            status_code=500)

        # Check that there are no revoked seats currently
        self.assertFalse(user2.seats.filter(
            account=self.campaign, state=Seat.States.REVOKED).exists())

        # Show that pending seats can always be revoked
        self.seat2.state = Seat.States.PENDING
        self.seat2.save()
        # Revoke second user
        revoke_url = reverse('account_seats_api-detail',
                             args=(self.campaign.id, self.seat2.id,))
        response = self.client.delete(revoke_url)
        self.assertContains(response, "")

        # Verify seat was revoked
        self.assertTrue(user2.seats.filter(
            account=self.campaign, state=Seat.States.REVOKED).exists())

    def test_serializer(self):
        self._login_and_add_admin_group()

        ## Verify the format of the list response contains only the =standard
        ## fields
        result = self.client.get(self.url)
        top_level = ("id", "user", "account", "seat_type", "date_assigned",
                     "date_revoked", "state", "permissions", "revocable",
                     "notes")
        self.verify_serializer(result, top_level)
        self.verify_serializer(result, MINIMAL_FIELDS, specific_field="user")

        top_level += ("last_activity", "roles")

        ## Verify the verbose view has events also
        result = self.client.get(self.url + '?verbosity=2')
        self.verify_serializer(result, top_level)
        self.verify_serializer(result, MINIMAL_FIELDS + ('events',),
                                specific_field="user")

    def test_retrieve(self):
        ## Verify only the correct events are returned
        self._login_and_add_admin_group()

        event1 = EventFactory(event_name=random_string(),
                              account=self.seat.account)
        event2 = EventFactory(event_name=random_string())
        event3 = EventFactory(event_name=random_string(),
                              account=self.seat.account)
        self.user.events.clear()
        self.user.add_events(event1, event2, event3)

        ## Verify minimum verbosity returns no user details
        response = self.client.get(self.url2 + '?verbosity=0')
        self.assertNotContains(response, "events")
        self.assertNotContains(response, "email")

        ## Verify default verbosity returns no events, but does have user info
        response = self.client.get(self.url2)
        self.assertNotContains(response, "events")
        self.assertContains(response,
                            '"email":"{}"'.format(self.seat.user.email))

        response = self.client.get(self.url2 + '?verbosity=2')
        self.assertContains(response,
                            '"event_name":"{}"'.format(event1.event_name))
        self.assertNotContains(response,
                               '"event_name":"{}"'.format(event2.event_name))
        self.assertContains(response,
                            '"event_name":"{}"'.format(event3.event_name))


class DecoratorsTests(TestCase):
    def setUp(self):
        self.user = RevUpUserFactory()
        self.seat = SeatFactory(user=self.user, permissions=["G3"])
        self.user.current_seat = self.seat

    def test_group_required(self):
        # Mock a request
        mock_request = mock.MagicMock()
        mock_request.user = self.user

        # Build a function and decorate it with the decorator
        @any_group_required('G1', 'G2')
        def function(*args, **kwargs):
            return HttpResponse("passed")

        ## Verify the request is rejected because user doesn't have the group
        # Ensure user is not superuser
        self.assertFalse(self.user.is_superuser)
        response = function(mock_request)
        self.assertEqual(response.status_code, 302)
        self.assertEqual('/?msg=permission_failure', response['Location'])

        ## Verify request is accepted with the user group
        self.seat.permissions = ["G1", "G3"]
        self.seat.save()
        response = function(mock_request)
        self.assertContains(response, 'passed')

        ## Verify request works with superuser
        # Reset the group to a bad one and verify it fails
        self.seat.permissions = []
        self.seat.save()
        response = function(mock_request)
        self.assertEqual(response.status_code, 302)

        # Now set the user to superuser and retest
        self.user.is_superuser = True
        self.assertTrue(self.user.is_superuser)
        response = function(mock_request)
        self.assertEqual(response.status_code, 302)


class SeatRolesViewSetTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        self.campaign = PoliticalCampaignFactory()
        self.campaign.roles.all().delete()
        self.role = RoleFactory(account=self.campaign,
                                permissions=FundraiserPermissions.all())
        super(SeatRolesViewSetTests, self).setUp(
            login_now=False, campaigns=[self.campaign])
        self.seat = SeatFactory(account=self.campaign, state=Seat.States.PENDING)
        self.seat.permissions = []
        self.seat._roles.clear()
        self.seat.save()
        self.seat.add_roles(self.role)
        self.url = reverse('seat_roles_api-list',
                           args=(self.campaign.id, self.seat.id))
        self.url2 = reverse('seat_roles_api-detail',
                            args=(self.campaign.id, self.seat.id,
                                  self.role.id))

    def test_permissions(self):
        self.assertEqual(self.seat._roles.count(), 1)
        # # Verify any unauthenticated user is rejected
        result = self.client.get(self.url)
        self.assertNoCredentials(result)
        result = self.client.post(self.url, data={"abc": "123"})
        self.assertNoCredentials(result)
        result = self.client.get(self.url2)
        self.assertNoCredentials(result)
        result = self.client.delete(self.url2)
        self.assertNoCredentials(result)

        # Login the user
        self._login(self.user)
        # Verify user is not an admin, or that would invalidate the test.
        self.assertFalse(self.user.is_admin)
        self.assertFalse(self.user.is_account_admin())

        # # Verify a standard user is rejected from viewing seat roles
        result = self.client.get(self.url)
        self.assertNoPermission(result)
        ## Verify a standard user is rejected from adding new roles
        result = self.client.post(self.url, data={"abc": "123"})
        self.assertNoPermission(result)
        ## Verify a standard user is rejected from deleting roles
        result = self.client.delete(self.url2)
        self.assertNoPermission(result)

        # Add an invalid admin permission and verify access is not granted
        self._add_admin_group(permissions=AdminPermissions.MOD_ACCOUNT)
        result = self.client.get(self.url)
        self.assertNoPermission(result)

        ## Verify access is granted with correct permission
        self._add_admin_group(permissions=AdminPermissions.MOD_ROLES)
        result = self.client.get(self.url)
        self.assertContains(result, '"id":{}'.format(self.role.id))
        # Verify this permission can delete
        result = self.client.delete(self.url2)
        self.assertContains(result, "Removed role: {}".format(self.role.name))
        self.assertFalse(self.seat._roles.exists())

    def test_create(self):
        self._login()
        self._add_admin_group(permissions=AdminPermissions.MOD_ROLES)
        self.seat = Seat.objects.get(pk=self.seat.pk)

        # # Verify error cases
        response = self.client.post(self.url, data={"abc": 123})
        self.assertContains(response, "Must provide one or more roles",
                            status_code=500)
        response = self.client.post(self.url,
                                    data={"role": [self.role.id, 890]})
        self.assertContains(response,
                            "One or more of the given roles not found.",
                            status_code=500)
        response = self.client.post(
            reverse('seat_roles_api-list', args=(self.campaign.id,
                                                 self.user.seats.first().id)),
            data={"role": [self.role.id]})
        self.assertContains(response,
                            "Users cannot change their own roles.",
                            status_code=500)

        ## Verify positive case where roles are added to the seat
        new_role = RoleFactory(account=self.campaign,
                               permissions=[AdminPermissions.MOD_ACCOUNT])
        self.assertNotIn(new_role, self.seat.roles())
        self.assertIn(self.role, self.seat.roles())
        permissions = self.seat.permissions
        # Include mod roles because we added it above to access the API
        self.assertEqual(set(permissions), set(FundraiserPermissions.all()))
        response = self.client.post(self.url,
                                    data={"role": [self.role.id, new_role.id]})
        self.assertContains(response, "Added roles:")
        self.assertContains(response, self.role.name)
        self.assertContains(response, new_role.name)

        # Verify the permissions were updated correctly
        self.seat = Seat.objects.get(pk=self.seat.pk)
        self.assertEqual(set(self.seat.permissions),
                         set((FundraiserPermissions.all() +
                                [AdminPermissions.MOD_ACCOUNT])))

    def test_destroy(self):
        self._login()
        self._add_admin_group(permissions=AdminPermissions.MOD_ROLES)

        new_role = RoleFactory(account=self.campaign,
                               permissions=[AdminPermissions.MOD_ACCOUNT])
        self.seat.add_roles(new_role)
        self.seat = Seat.objects.get(pk=self.seat.pk)

        self.assertEqual(set(self.seat.permissions),
                         set((FundraiserPermissions.all() +
                              [AdminPermissions.MOD_ACCOUNT])))

        response = self.client.delete(
            reverse('seat_roles_api-detail',
                    args=(self.campaign.id, self.user.seats.first().id,
                          self.role.id)))
        self.assertContains(response,
                            "Users cannot change their own roles.",
                            status_code=500)

        response = self.client.delete(self.url2)
        self.assertContains(response,
                            "Removed role: {}".format(self.role.name))
        self.seat = Seat.objects.get(pk=self.seat.pk)
        self.assertEqual(set(self.seat.permissions),
                         set([AdminPermissions.MOD_ACCOUNT]))

    def test_serializer(self):
        self._login_and_add_admin_group(permissions=AdminPermissions.MOD_ROLES)
        response = self.client.get(self.url)
        self.verify_serializer(response, (
            "id", "name", "permissions", "description", "is_a_seat_default"))


