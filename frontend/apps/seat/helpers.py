"""Functions in this file are used in multiple places within the seat app."""

from django.db import transaction

from frontend.apps.authorize.models import RevUpUser
from frontend.apps.authorize.utils import send_account_user_added_notification


# Use a transaction to roll back any created objects if
# there's a failure.
@transaction.atomic
def build_seat_and_notify(email, account, roles, protocol_host=None,
                          inviting_user=None, first_name='', last_name='',
                          **kwargs):
    # Get an existing user, or create a new one.
    user, created = RevUpUser.get_or_create_inactive(email,
                                                     first_name=first_name,
                                                     last_name=last_name)
    # Use a task lock to try to prevent duplicate seats from being created
    user.lock()

    # Create a seat for the user
    seat = account.create_seat(user, roles=roles or None,
                               inviting_user=inviting_user, **kwargs)

    # If this is a new user, we need to tokenize them to finish signup.
    if not user.is_active:
        user.generate_token()

    # If the user should be notified, send them an email
    if protocol_host:
        send_account_user_added_notification(user, account,
                                             protocol_host,
                                             inviting_user=inviting_user)
    return user, seat
