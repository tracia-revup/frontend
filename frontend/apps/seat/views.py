import itertools
import re

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.utils.decorators import method_decorator

from frontend.apps.seat.models import Seat
from frontend.libs.utils.string_utils import str_to_bool
from frontend.apps.campaign.routes import ROUTES
from frontend.libs.django_view_router import RoutingView
from frontend.libs.utils.tracker_utils import action_send


class SelectActiveSeat(RoutingView):
    routes = ROUTES

    @method_decorator(login_required)
    def do_dispatch(self, request, *args, **kwargs):
        self.next = request.GET.get('next')
        return super(SelectActiveSeat, self).do_dispatch(
            request, *args, **kwargs)

    def _set_seat_and_redirect(self, request, seat):
        request.user.reset_current_seat(seat, request=request)
        if seat:
            # Track the seat selection. We don't track "None" seats
            action_send(request, request.user, verb="seat selected",
                        action_object=seat)

            # If this seat is still Pending, we now mark it as Active,
            # but we don't want to do this for impersonated users
            if not request.user.is_impersonate:
                activated = seat.activate()
                if activated:
                    # Track seat activation
                    action_send(request, seat, verb="seat activated")
        return HttpResponseRedirect(self.next or reverse('index'))

    def get(self, request, *args, **kwargs):
        force = str_to_bool(request.GET.get('force', False))
        seats = request.user.get_valid_seats()

        # If the user only has one available seat, there's no need to prompt
        # them. We can just choose it automatically. Unless this is queried
        # with the 'force' option
        if len(seats) == 1 and not force:
            return self._set_seat_and_redirect(request, seats[0])
        # If this user doesn't have any seats because they are staff,
        # allow them to continue to the staff-admin (which is about all they
        # can do without a seat).
        elif not seats and request.user.is_admin:
            self.next = reverse("staff_admin_home")
            return self._set_seat_and_redirect(request, None)
        else:
            # Steve wanted a certain ordering with his seats, so we have to
            # hack that in right here
            hack_order = ("Ro Khanna", "NRCC", "Purdue")
            # We'll use a regex to speed-up the comparison with titles.
            # Otherwise, we have to loop over the hacks for every seat.
            hack_re_pattern = re.compile("|".join(".*?{}.*?".format(item)
                                                  for item in hack_order))
            hacks = []
            filtered_seats = []
            for seat in seats:
                title = seat.account.title
                # If the seat is from one of the special accounts, set it
                # aside, otherwise add it to the filtered seats.
                if hack_re_pattern.match(title):
                    hacks.append(seat)
                else:
                    filtered_seats.append(seat)

            return render(request=request,
                          template_name='seat/select_current.html',
                          context={'seats': list(itertools.chain(hacks, filtered_seats)),
                                   'next': self.next,
                                   'user': request.user,
                                   'request': request})

    def post(self, request, *args, **kwargs):
        seat_id = request.POST.get('seat', "")
        try:
            # We don't need to do any fancy error checking here because the
            # redirect will cause the middleware to check the validity of the
            # seat instead.
            seat = Seat.objects.get(user=request.user, pk=int(seat_id))
        except (Seat.DoesNotExist, ValueError):
            return self.get(request, *args, **kwargs)
        else:
            return self._set_seat_and_redirect(request, seat)
