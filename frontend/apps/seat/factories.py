import datetime

import factory

from frontend.apps.authorize.factories import RevUpUserFactory
from frontend.apps.campaign.factories import (PoliticalCampaignFactory,
                                              NonprofitCampaignFactory)
from frontend.apps.seat.models import Seat


@factory.use_strategy(factory.CREATE_STRATEGY)
class SeatFactory(factory.DjangoModelFactory):
    class Meta:
        model = Seat

    user = factory.SubFactory(RevUpUserFactory)
    account = factory.SubFactory(PoliticalCampaignFactory)
    permissions = Seat.FundraiserPermissions.all()
    state = Seat.States.ACTIVE

    @factory.lazy_attribute
    def date_activated(self):
        if self.state != Seat.States.PENDING:
            return datetime.datetime.now()


@factory.use_strategy(factory.CREATE_STRATEGY)
class NonprofitSeatFactory(factory.DjangoModelFactory):
    class Meta:
        model = Seat

    user = factory.SubFactory(RevUpUserFactory)
    account = factory.SubFactory(NonprofitCampaignFactory)
    permissions = Seat.FundraiserPermissions.all()
    state = Seat.States.ACTIVE

    @factory.lazy_attribute
    def date_activated(self):
        if self.state != Seat.States.PENDING:
            return datetime.datetime.now()
