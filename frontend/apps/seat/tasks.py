import logging

from celery import task
from django.conf import settings
from django.template.loader import render_to_string

from frontend.apps.seat.models import BulkFundraiserImport
from frontend.libs.utils.email_utils import EmailMessage

LOGGER = logging.getLogger(__name__)


@task(max_retries=10, default_retry_delay=10*60, queue='contact_import')
def bulk_fundraiser_import_task(bulk_data_id, protocol_host=None):
    bfi = BulkFundraiserImport.objects.get(id=bulk_data_id)
    total_processed, errors = bfi.process_file(protocol_host)

    # Send a status email
    subject = "Bulk fundraiser import finished"
    email_template = 'seat/emails/bulk_upload_result.html'
    context = {"total_processed": total_processed, "errors": errors}

    template = render_to_string(email_template, context)
    msg = EmailMessage(subject, template, settings.DEFAULT_FROM_EMAIL,
                       [bfi.user.email])
    msg.content_subtype = "html"  # Main content is now text/html

    # This actually sends the email. Use with caution
    msg.send()

    logging.info("Sent an email from bulk fundraiser import task to the "
                 "address: '{}'".format(bfi.user.email))

