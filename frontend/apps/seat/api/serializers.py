from rest_framework import serializers

from frontend.apps.authorize.api.serializers import (
    RevUpUserSubsetSerializer, CampaignUserSerializer)
from frontend.apps.campaign.api.serializers import BasicAccountSerializer
from frontend.apps.role.api.serializers import RoleSerializerBase
from frontend.apps.seat.models import Seat


class BaseSeatSerializer(serializers.ModelSerializer):
    revocable = serializers.BooleanField(read_only=True)
    permissions = serializers.SerializerMethodField("build_permissions")
    date_assigned = serializers.DateField(format='%Y-%m-%d',
                                              input_formats=['%Y-%m-%d'])
    date_revoked = serializers.DateField(format='%Y-%m-%d',
                                             input_formats=['%Y-%m-%d'])
    state = serializers.SerializerMethodField()
    account = serializers.SerializerMethodField()

    class Meta:
        model = Seat
        fields = ("id", "user", "account", "seat_type", "date_assigned",
                  "date_revoked", "state", "revocable", "permissions", "notes")

    def get_state(self, seat):
        return Seat.States.translate(seat.state)

    def get_account(self, seat):
        if self.context.get("expand_account", False):
            return BasicAccountSerializer(seat.account).data
        else:
            return seat.account_id

    def build_permissions(self, seat):
        return [(p, Seat.FundraiserPermissions.translate(p) or
                    Seat.AdminPermissions.translate(p))
                for p in seat.permissions]


class NameSeatSerializer(serializers.ModelSerializer):
    user = RevUpUserSubsetSerializer(read_only=True)

    class Meta:
        model = Seat
        fields = ("id", "user",)


class SeatSerializer(BaseSeatSerializer):
    user = RevUpUserSubsetSerializer(read_only=True)


class DetailedSeatSerializer(SeatSerializer):
    class Meta:
        model = Seat
        fields = ("id", "user", "account", "seat_type", "date_assigned",
                  "date_revoked", "state", "revocable", "permissions", "notes",
                  'last_activity', 'roles')

    user = serializers.SerializerMethodField("_user")
    roles = RoleSerializerBase(source='_roles', many=True)

    def _user(self, seat):
        return CampaignUserSerializer(seat.user, read_only=True,
                                      context={'account_id':
                                               seat.account_id}).data
