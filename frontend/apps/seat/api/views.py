from functools import reduce
import re

from django.core.validators import validate_email, ValidationError
from django.db.models import Prefetch, Q
from django import db
from django.http import Http404, QueryDict
from django.shortcuts import get_object_or_404
from rest_condition.permissions import Or, And
from rest_framework import permissions, exceptions, viewsets, filters
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework_extensions.mixins import NestedViewSetMixin
from rest_framework.pagination import PageNumberPagination

from frontend.apps.authorize.models import UserEventLink
from frontend.apps.authorize.utils import send_account_user_added_notification
from frontend.apps.seat.api.serializers import (
    BaseSeatSerializer, SeatSerializer, DetailedSeatSerializer)
from frontend.apps.campaign.models import Account
from frontend.apps.role.api.permissions import HasPermission
from frontend.apps.role.api.serializers import RoleSerializer
from frontend.apps.role.models import Role
from frontend.apps.role.permissions import AdminPermissions
from frontend.apps.seat.api.permissions import AccountOwnsSeat
from frontend.apps.seat.helpers import build_seat_and_notify
from frontend.apps.seat.models import Seat, DuplicateSeatError, SeatRevokeError
from frontend.libs.utils.api_utils import (
    IsAdminUser, CRDModelViewSet, PartialUpdateMixin, IsOwner)
from frontend.libs.utils.string_utils import str_to_bool
from frontend.libs.utils.view_utils import build_protocol_host


class RoleMixin(object):
    def get_roles(self, data, account):
        """Query the roles"""
        if isinstance(data, QueryDict):
            role_ids = data.getlist('role', [])
        else:
            role_ids = data.get('role', [])

        if not isinstance(role_ids, (list, tuple)):
            role_ids = [role_ids]

        if role_ids:
            # Look for account specific or global roles
            roles = Role.objects.filter((Q(account=account) |
                                         Q(account__isnull=True)) &
                                        Q(id__in=role_ids))

            if len(roles) != len(set(role_ids)):
                raise exceptions.APIException(
                    "One or more of the given roles not found.")
        else:
            roles = []
        return roles


class AccountSeatViewSet(NestedViewSetMixin, PartialUpdateMixin,
                         CRDModelViewSet, RoleMixin):
    """This API is a derivative of the Accounts API.
    This gives access to an Account's seats.

    This API allows for three levels of verbosity (0-2). The verbosity is
    set by using a GET argument "verbosity". Verbosity has the following
    effect:
        0) All fields in the Seat, including permissions
        1) 0 + basic user information
        2) 1 + Roles and even more detailed user information, including the
               user's events.
    The default is 1
    """
    parent_lookup = 'parent_lookup_account_id'
    serializer_classes = [
        BaseSeatSerializer,
        SeatSerializer,
        DetailedSeatSerializer
    ]
    permission_classes = (
        And(permissions.IsAuthenticated,
            Or(IsAdminUser,
               Or(HasPermission(parent_lookup,
                                AdminPermissions.VIEW_FUNDRAISERS,
                                methods=["GET"]),
                  HasPermission(parent_lookup,
                                AdminPermissions.MOD_FUNDRAISERS,
                                methods=['POST', 'DELETE', "PUT", "PATCH"]))),
            ),
    )
    allowed_state_filters = ("temporary", "assigned", "revoked",
                             "filled", "all")
    valid_update_fields = {'notes'}
    filter_backends = (filters.SearchFilter,)
    search_fields = ('permissions',)

    class pagination_class(PageNumberPagination):
        page_size = 20
        page_size_query_param = 'page_size'
        max_page_size = 100

    @property
    def verbosity(self):
        try:
            verbosity = int(self.request.query_params.get('verbosity', 1))
        except (ValueError, TypeError):
            verbosity = 1
        return verbosity

    def get_serializer_class(self):
        verbosity = self.verbosity
        try:
            return self.serializer_classes[verbosity]
        except IndexError:
            return self.serializer_classes[-1]

    def get_queryset(self):
        state = self.request.query_params.get('state', "assigned").lower()
        if state not in self.allowed_state_filters:
            raise exceptions.APIException(
                "Invalid filter state: {}".format(state))

        account_id = self.kwargs.get(self.parent_lookup)
        queryset = getattr(Seat.objects, state, None)
        queryset = queryset().filter(account=account_id).select_related(
            "user", "account")
        if self.verbosity > 1:
            # If verbosity is > 1, then we're probably showing user events. Add
            # a prefetch here for events because without it we'll end up doing
            # a ton of queries which will really slow down API calls.
            queryset = queryset.prefetch_related(
                '_roles',
                Prefetch('user__event_links',
                         # The filter on event account id is a security
                         # precaution, so we don't accidentally display events
                         # from another campaign.
                         queryset=UserEventLink.objects.filter(
                             event__account_id=account_id).select_related(
                                 'event')))
        return queryset.order_by("user__last_name")

    def filter_queryset(self, queryset):
        queryset = super(AccountSeatViewSet,
                         self).filter_queryset(queryset)
        name_filters = self.request.query_params.get("name")
        email_filters = self.request.query_params.get("email")
        if name_filters:
            queryset = queryset.filter(self._prepare_name_filter(name_filters))
        if email_filters:
            queryset = queryset.filter(
                self._prepare_email_filter(email_filters))
        return queryset

    def _prepare_email_filter(self, filter_value):
        return Q(user__email__istartswith=filter_value)

    def _prepare_name_filter(self, filter_value):
        # Tokenize query on non-word characters
        name_tokens = re.split('\W+', filter_value.lower())
        names = list()
        first_name_key = 'user__first_name__istartswith'
        last_name_key = 'user__last_name__istartswith'
        # If more than one name token, generate combination tuples
        if len(name_tokens) > 1:
            # Assume last token is meant as last name, use remaining as firsts
            for token in name_tokens[:-1]:
                names.append(Q(**{first_name_key: token,
                                  last_name_key: name_tokens[-1]}))
            # Assume first token is last name, use remaining as firsts
            for token in name_tokens[1:]:
                names.append(Q(**{first_name_key: token,
                                  last_name_key: name_tokens[0]}))
        # If only one name token, try it as first or last name
        elif name_tokens:
            names.append(Q(**{first_name_key: name_tokens[0]}))
            names.append(Q(**{last_name_key: name_tokens[0]}))

        query = Q()
        if names:
            query = reduce(lambda a, b: a | b, names)
        return query

    ## Update method is defined in a mixin

    def create(self, request, *args, **kwargs):
        """Add a user to the account.

        This method accepts an email address. The user the email belongs to
        is added to the account. If the user doesn't exist, we create a
        new user.
        """
        # Get the account
        try:
            account = Account.objects.get(
                pk=kwargs.get(self.parent_lookup))
        except Account.DoesNotExist:
            raise Http404

        email = request.data.get('email', '').strip()
        first_name = request.data.get('first_name', '').strip()
        last_name = request.data.get('last_name', '').strip()
        if not email:
            raise exceptions.APIException("'email' is required to add "
                                          "a user to an account.")

        # By default, we notify users they have been added to an account
        send_notification = str_to_bool(request.data.get('notify', True))

        # Let's first check that the given email is in the right format
        try:
            validate_email(email)
        except ValidationError:
            raise exceptions.APIException(
                "'email' must be in a valid email format")

        # The account must have available seats.
        if account.available_seats() == 0:
            raise exceptions.APIException(
                "This account does not have any available seats to assign.")

        # Assign roles, if any are given.
        roles = self.get_roles(request.data, account)

        # If the user should be notified, send them an email
        if send_notification:
            protocol_host = build_protocol_host(request)
        else:
            protocol_host = None

        notes = request.data.get('notes', '').strip()
        try:
            # Create a seat and notify the new user
            user, seat = build_seat_and_notify(email, account, roles,
                                               protocol_host, notes=notes,
                                               first_name=first_name,
                                               last_name=last_name,
                                               inviting_user=request.user)
        except (DuplicateSeatError, db.IntegrityError):
            # Ensure the user isn't already a member of the account
            raise exceptions.APIException(
                "The fundraiser: '{}' is already a member of this account"
                .format(email))
        return Response(SeatSerializer(seat).data)

    @action(detail=True)
    def resend_email(self, request, **kwargs):
        seat = get_object_or_404(Seat.objects.select_related('user'),
                                 pk=kwargs.get(self.lookup_field),
                                 account=kwargs.get(self.parent_lookup))
        seat.user.generate_token()

        try:
            account = Account.objects.get(pk=seat.account_id)
        except Account.DoesNotExist:
            raise Http404

        # Build the protocol and host string here because celery wont have
        # access to the request object
        protocol_host = "http{}://{}".format(
            's' if request.is_secure() else '', request.get_host())
        send_account_user_added_notification(seat.user, account,
                                             protocol_host,
                                             inviting_user=request.user)
        return Response({})

    def destroy(self, request, *args, **kwargs):
        seat = get_object_or_404(Seat.objects.select_related(
            'user', 'account'), pk=kwargs.get(self.lookup_field))

        # Don't let admins revoke their own access
        if seat.user == request.user:
            raise exceptions.APIException("Can't revoke your own access.")
        elif seat.user == seat.account.account_head:
            raise exceptions.APIException("Account head's seat cannot "
                                          "be revoked.")

        # Revoke the seat
        try:
            seat.revoke()
        except SeatRevokeError as err:
            raise exceptions.APIException(err)

        # Delete this campaign's events from user
        for event_link in seat.user.event_links.select_related('event').all():
            if event_link.event.account_id == seat.account_id:
                event_link.delete()
        return Response({})


class SeatRolesViewSet(NestedViewSetMixin, CRDModelViewSet, RoleMixin):
    parent_account_lookup = 'parent_lookup_account_id'
    parent_seat_lookup = 'parent_lookup_seat_id'
    serializer_class = RoleSerializer
    permission_classes = (
        And(permissions.IsAuthenticated,
            Or(IsAdminUser,
               HasPermission(parent_account_lookup,
                             AdminPermissions.MOD_ROLES)),
            AccountOwnsSeat(parent_account_lookup, parent_seat_lookup)
        ),
    )

    def get_queryset(self, **query_args):
        return get_object_or_404(
            Seat, account_id=self.kwargs.get(self.parent_account_lookup),
            pk=self.kwargs.get(self.parent_seat_lookup)
        ).roles(**query_args)

    def retrieve(self, request, pk, *args, **kwargs):
        try:
            data = self.serializer_class(next(self.get_queryset(pk=pk))).data
        except StopIteration:
            data = []
        return Response(data=data)

    def _verify_seat(self, request, seat):
        # Verify this seat can have its role changed
        if seat.user_id == seat.account.account_head_id:
            raise exceptions.APIException("Account head's roles cannot be "
                                          "changed.")
        # User's cannot change their own roles; however, admin users can
        elif seat.user_id == request.user.id and not request.user.is_admin:
            raise exceptions.APIException("Users cannot change their own "
                                          "roles.")

    def create(self, request, *args, **kwargs):
        """Attach the given roles to a seat."""
        seat = get_object_or_404(
            Seat, pk=kwargs.get(self.parent_seat_lookup),
            account=kwargs.get(self.parent_account_lookup))
        self._verify_seat(request, seat)

        roles = self.get_roles(request.data, seat.account_id)
        if not roles:
            raise exceptions.APIException("Must provide one or more roles")

        # Add the roles to the seat
        try:
            seat.add_roles(*roles)
        except ValueError as err:
            raise exceptions.APIException(err)
        return Response("Added roles: {}".format(
            ", ".join(r.name for r in roles)))

    def destroy(self, request, pk, *args, **kwargs):
        seat = get_object_or_404(
            Seat, pk=kwargs.get(self.parent_seat_lookup),
            account=kwargs.get(self.parent_account_lookup))
        self._verify_seat(request, seat)

        try:
            # Retrieve the role and verify it belongs to the seat.
            role = next(seat.roles(pk=pk))
        except StopIteration:
            raise Http404

        seat.remove_roles(role)
        return Response("Removed role: {}".format(role.name))


class UserSeatsViewSet(NestedViewSetMixin, viewsets.ReadOnlyModelViewSet):
    """Simple API that just shows the seats a user owns.

    Only the user themself can see this view.
    """
    serializer_class = BaseSeatSerializer
    queryset = Seat.objects.all()
    permission_classes = (
        And(permissions.IsAuthenticated, IsOwner),
    )

    @action(detail=True)
    def current_skin(self, *args, **kwargs):
        """Get the current skin for the account this Seat belongs to."""
        from frontend.apps.ux.api.serializers import SkinSettingsSerializer
        return Response(SkinSettingsSerializer(
            self.get_object().account.current_skin).data)
