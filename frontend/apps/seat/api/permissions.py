
from rest_framework import permissions

from frontend.apps.campaign.api.permissions import AccountOwnerBase
from frontend.apps.seat.models import Seat
from frontend.libs.utils.api_utils import PerformantPermissionMixin


class AccountOwnsSeat(AccountOwnerBase):
    """Check if the account ID in the url is the same account as the
     one linked to from the Role.
    """
    DEFAULT_MODEL_CLASS = Seat


class SeatPermissionBase(permissions.BasePermission,
                         PerformantPermissionMixin):
    view_instance_attribute = "seat"

    def __init__(self, seat_id_field, *args, **kwargs):
        self.seat_id_field = seat_id_field
        super(SeatPermissionBase, self).__init__(*args, **kwargs)

    def _get_object(self, request, view):
        return Seat.objects.get(pk=view.kwargs.get(self.seat_id_field))


class UserOwnsSeat(SeatPermissionBase):
    """Verify the requesting user owns the requested seat."""
    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view)

    def has_permission(self, request, view):
        seat = self.get_object(request, view)
        return seat.user_id == request.user.id


class SeatHasPermission(SeatPermissionBase):
    """Verify the user has the given permissions.

    This will first look in the user's current seat, then it will look more
    globally across all of the user's seats if the user doesn't have
    a current seat matching the query account.
    """
    view_instance_attribute = "seat"

    def __init__(self, seat_id_field, permissions_, methods=None, all=False,
                 **kwargs):
        """Check if the user has a seat with the given permissions for the
        account in the query.

        If multiple permissions are supplied, and all is False, it is treated
        as an OR relationship. If all is True, then all permissions must be
        present.
        """
        # If methods is supplied, only verify permissions for the given
        # methods. Other methods are automatically rejected (False)
        self.methods = [m.upper() for m in (methods or [])]
        if isinstance(permissions_, str):
            permissions_ = [permissions_]
        self.permissions = permissions_
        self.all = all
        super(SeatHasPermission, self).__init__(seat_id_field, *kwargs)

    def has_object_permission(self, request, view, obj):
        """Use the same rules for objects as for list views.

        The parent class always returns True, which is not correct.
        """
        return self.has_permission(request, view)

    def has_permission(self, request, view):
        if self.methods and request.method.upper() not in self.methods:
            return False

        # Get the Seat object referenced in the url.
        seat = self.get_object(request, view)

        # Verify the requesting user owns the seat specified, and that the seat
        # is active.
        if seat.user_id != request.user.id or \
                        seat.state not in Seat.States.ASSIGNED:
            return False
        else:
            # Look for the permissions in the specified seat.
            return seat.has_permissions(
                **{'all' if self.all else 'any': self.permissions})
