

def is_account_type_factory(*account_types):
    """This is meant to be used with django_view_router as a reusable way
       to specify routes for specific Account types in the current_seat.
    """
    def is_account_type_closure(request, view):
        try:
            account = request.user.current_seat.account
            return account.account_profile.account_type in account_types
        except AttributeError:
            # If the user doesn't have a current_seat
            return False
    return is_account_type_closure
