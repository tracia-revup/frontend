from django.apps import AppConfig



class SeatConfig(AppConfig):
    name = 'frontend.apps.seat'

    def ready(self):
        from actstream import registry
        registry.register(self.get_model('Seat'))


