
import factory
import factory.fuzzy

from frontend.apps.campaign.factories import PoliticalCampaignFactory
from frontend.apps.campaign_admin.models import ManagementGroup
from frontend.libs.utils.factory_utils import M2MMixin


@factory.use_strategy(factory.CREATE_STRATEGY)
class ManagementGroupFactory(factory.DjangoModelFactory, M2MMixin):
    class Meta:
        model = ManagementGroup

    name = factory.fuzzy.FuzzyText()
    account = factory.SubFactory(PoliticalCampaignFactory)
    permissions = [ManagementGroup.Permissions.TRACKER]

    @factory.post_generation
    def managers(self, create, extracted, **kwargs):
        if extracted:
            ManagementGroupFactory._m2m(create, extracted, self.managers)

    @factory.post_generation
    def members(self, create, extracted, **kwargs):
        if extracted:
            ManagementGroupFactory._m2m(create, extracted, self.members)