from django.conf.urls import url

from .views import (
    CampaignAdminHome, ActivationView, ManagementGroupTrackerView,
    FundraisersView, EventsView)

urlpatterns = [
    url(r'^$', CampaignAdminHome.as_view(), name='campaign_admin_home'),

    url(r'^events/?$', EventsView.as_view(),
        name="campaign_admin_events"),
    url(r'^fundraisers/?$', FundraisersView.as_view(),
        name="campaign_admin_fundraisers"),

    url(r'^activate/$', ActivationView.as_view(),
        name="campaign_admin_activate"),

    url(r'^mg_tracker/(?P<pk>[^/]+)/$', ManagementGroupTrackerView.as_view(),
        name="campaign_admin_mg_tracker"),
]
