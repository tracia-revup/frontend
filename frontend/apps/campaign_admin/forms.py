from crispy_forms.helper import FormHelper
from django import forms

from frontend.apps.campaign.models import (
    Event, ExternalLink)
from frontend.libs.utils.string_utils import random_uuid


MEGABYTE = 2**20


class EventForm(forms.ModelForm):
    # system_names = CustomArrayField(forms.CharField(required=False))
    # system_names.widget.input_type = 'hidden'
    # link_keys = CustomArrayField(forms.CharField(required=False))
    # link_keys.widget.input_type = 'hidden'
    event_date = forms.DateField()

    # noinspection PyClassicStyleClass
    class Meta:
        model = Event
        exclude = ["account"]

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_tag = False
        super(EventForm, self).__init__(*args, **kwargs)
        self.initial['system_choices'] = ExternalLink.SYSTEM_CHOICES
        self.fields['event_date'].widget.attrs = {'placeholder': "YYYY-MM-DD"}

        if not self.initial.get('price_points'):
            self.initial['price_points'] = [1000, 2700, 5400]

    # def save(self, commit=True):
    #     event = super(EventForm, self).save(commit)
    #     external_links = zip(self.cleaned_data.get('system_names', []),
    #                          self.cleaned_data.get('link_keys', []))
    #     if commit and external_links:
    #         event.external_links.all().delete()
    #         for system, link in external_links:
    #             if system and link:
    #                 ExternalLink.objects.create(event=event,
    #                                             link_key=link,
    #                                             system_name=system)
    #     return event


class BannerForm(forms.ModelForm):
    banner = forms.ImageField(required=False)

    def __init__(self, *args, **kwargs):
        super(BannerForm, self).__init__(*args, **kwargs)
        # Set default values for file names so blanks don't overwrite them
        self.banner_name = self.initial.get('banner_name', "")

        # Get file names now, they will change if we save self.files
        files = kwargs.get('files', None)
        if files:
            banner = files.getlist('banner', [])
            if banner:
                self.banner_name = banner[0].name

    def _clean_image_file(self, image_file, max_size):
        # Verify the image isn't too big
        check_image_size(image_file, max_size)
        # Rename the file for security. Only rename new images
        committed = getattr(image_file, '_committed', False)
        if image_file and not committed:
            image_file.name = random_uuid()

    def clean_banner(self):
        banner = self.cleaned_data.get('banner')
        # Can't upload images larger than 10MB
        self._clean_image_file(banner, 10)
        return banner

    def clean_banner_name(self):
        self.cleaned_data['banner_name'] = self.banner_name
        return self.banner_name


class AllImagesForm(BannerForm):
    icon = forms.ImageField(required=False)
    opponent_icon = forms.ImageField(required=False)

    def __init__(self, *args, **kwargs):
        super(AllImagesForm, self).__init__(*args, **kwargs)
        # Set default values for file names so blanks don't overwrite them
        self.icon_name = self.initial.get('icon_name', "")
        self.opponent_icon_name = self.initial.get('opponent_icon_name', "")

        # Get file names now, they will change if we save self.files
        files = kwargs.get('files', None)
        if files:
            icon = files.getlist('icon', [])
            if icon:
                self.icon_name = icon[0].name
            opponent_icon = files.getlist('opponent_icon', [])
            if opponent_icon:
                self.opponent_icon_name = opponent_icon[0].name

    def clean_icon(self):
        icon = self.cleaned_data.get("icon")
        self._clean_image_file(icon, 2)
        return icon

    def clean_icon_name(self):
        self.cleaned_data['icon_name'] = self.icon_name
        return self.icon_name

    def clean_opponent_icon(self):
        opponent_icon = self.cleaned_data.get("opponent_icon")
        self._clean_image_file(opponent_icon, 2)
        return opponent_icon

    def clean_opponent_icon_name(self):
        self.cleaned_data['opponent_icon_name'] = self.opponent_icon_name
        return self.opponent_icon_name


def check_image_size(image, max_mb):
    if image:
        committed = getattr(image, '_committed', False)
        # noinspection PyProtectedMember
        if not committed and max_mb * MEGABYTE < image._size:
            raise forms.ValidationError(
                "Images can be no larger than {} Megabytes".format(max_mb))
