# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.contrib.postgres.fields


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0011_university'),
        ('seat', '0002_auto_20150129_1120'),
    ]

    operations = [
        migrations.CreateModel(
            name='ManagementGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=256, blank=True)),
                ('permissions', django.contrib.postgres.fields.ArrayField(models.CharField(max_length=2), default=[], size=None, blank=True)),
                ('account', models.ForeignKey(to='campaign.Account', on_delete=django.db.models.deletion.CASCADE)),
                ('managers', models.ManyToManyField(help_text='The managers of the members of this group. The permissions field gives these users permissions over the members', related_name='management_groups_manager', to='seat.Seat', blank=True)),
                ('members', models.ManyToManyField(help_text='These users are managed by the managers.', related_name='management_groups_member', to='seat.Seat', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='managementgroup',
            unique_together=set([('account', 'name')]),
        ),
    ]
