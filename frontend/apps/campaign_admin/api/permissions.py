
from frontend.apps.campaign.api.permissions import BaseObjectPermission
from frontend.apps.campaign_admin.models import ManagementGroup


class AccountOwnsManagementGroup(BaseObjectPermission):
    """Check if the account ID in the url is the same account as the
     one linked to from the event.
    """
    DEFAULT_MODEL_CLASS = ManagementGroup

    def __init__(self, account_id_field, *args, **kwargs):
        self.account_id_field = account_id_field
        super(AccountOwnsManagementGroup, self).__init__(*args, **kwargs)

    def permission_check(self, request, view, mg):
        return (str(mg.account_id) ==
                view.kwargs.get(self.account_id_field))
