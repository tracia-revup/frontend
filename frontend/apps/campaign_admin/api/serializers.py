
from rest_framework import serializers

from frontend.apps.campaign_admin.models import ManagementGroup


class ManagementGroupSerializer(serializers.ModelSerializer):
    is_manager = serializers.SerializerMethodField('_is_manager')

    def __init__(self, *args, **kwargs):
        self.is_manager = kwargs.pop('is_manager', None)
        super(ManagementGroupSerializer, self).__init__(*args, **kwargs)

    def _is_manager(self, *args, **kwargs):
        return self.is_manager

    class Meta:
        model = ManagementGroup
        fields = ("id", "name", "is_manager")


