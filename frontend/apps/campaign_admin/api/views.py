
from django.db import IntegrityError
from django.http import Http404, HttpResponse
from django.shortcuts import get_object_or_404
from rest_condition.permissions import Or, And
from rest_framework import permissions, exceptions
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework_extensions.mixins import NestedViewSetMixin

from frontend.apps.analysis.models import Analysis
from frontend.apps.campaign.models import Account, Prospect
from frontend.apps.campaign_admin.api.permissions import (
    AccountOwnsManagementGroup)
from frontend.apps.campaign_admin.api.serializers import (
    ManagementGroupSerializer)
from frontend.apps.campaign_admin.models import ManagementGroup
from frontend.apps.seat.api.serializers import SeatSerializer
from frontend.apps.seat.models import Seat
from frontend.apps.role.api.permissions import HasPermission
from frontend.apps.role.permissions import AdminPermissions
from frontend.libs.utils.api_utils import IsAdminUser, CRDModelViewSet
from frontend.libs.utils.string_utils import random_string


class AccountManagementGroupsViewSet(NestedViewSetMixin, CRDModelViewSet):
    """View and create Management Groups."""
    parent_account_lookup = 'parent_lookup_account_id'
    serializer_class = ManagementGroupSerializer
    permission_classes = (And(permissions.IsAuthenticated,
                              Or(HasPermission(parent_account_lookup,
                                               AdminPermissions.MOD_MG),
                                 IsAdminUser)),)
    model = ManagementGroup
    queryset = ManagementGroup.objects.all()

    def create(self, request, *args, **kwargs):
        """Create a new Management Group for the given Account.

        Expected argument is "name". If none is given, a default will be
        generated.
        """
        name = request.data.get('name')
        if not name:
            # Add a single random character to reduce chance of collision.
            # Collision might happen if groups are deleted.
            name = "Management Group ({}{})".format(
                self.get_queryset().count() + 1, random_string(1))

        account = get_object_or_404(Account,
                                    pk=kwargs.get(self.parent_account_lookup))
        try:
            mg = ManagementGroup.objects.create(
                name=name, account=account,
                permissions=[ManagementGroup.Permissions.TRACKER])
        except IntegrityError:
            raise exceptions.APIException("Management Group with name '{}' "
                                          "already exists".format(name))
        return Response(self.serializer_class(mg).data)


class ManagementGroupTrackerViewSet(NestedViewSetMixin, ReadOnlyModelViewSet):
    parent_account_lookup = 'parent_lookup_account_id'
    model = ManagementGroup
    queryset = ManagementGroup.objects.all()
    permission_classes = (permissions.IsAuthenticated,)

    def dispatch(self, request, *args, **kwargs):
        # We need to generate the response first before checking permissions
        # to utilize DRF. If the user doesn't have permission, we wont return
        # this response
        response = super(ManagementGroupTrackerViewSet, self).dispatch(
            request, *args, **kwargs)

        # Check permissions
        # Either this is an admin user
        has_permission = False
        if HasPermission(self.parent_account_lookup,
                         AdminPermissions.FR_TRACKER).has_permission(request,
                                                                     self):
            has_permission = True
        # or they are a manager in a management group with tracker permissions.
        elif request.user.id in (manager.user_id
                                 for manager in self.mg.managers.all()) and \
                ManagementGroup.Permissions.TRACKER in self.mg.permissions:
            has_permission = True

        if has_permission:
            return response
        else:
            return HttpResponse("No Permission", status=403)

    def list(self, request, **kwargs):
        self.mg = self.get_queryset().select_related('user')[0]
        data = []
        for member_seat in self.mg.members.all():
            last_analysis = Analysis.objects.filter(
                user=member_seat.user,
                event__account=member_seat.account).last()

            data.append({
                "seat": member_seat.id,
                "first_name": member_seat.user.first_name,
                "last_name": member_seat.user.last_name,
                "email": member_seat.user.email,
                "first_login": member_seat.date_activated or "",
                "last_login": member_seat.last_activity.date()
                              if member_seat.last_activity else "",
                "last_analyzed": last_analysis.modified.date()
                                 if last_analysis else "",
                "prospect_count": Prospect.objects.filter(
                    user=member_seat.user,
                    event__account=member_seat.account).count()
            })
        return Response(data)


class ManagementGroupsSeatsBase(NestedViewSetMixin, CRDModelViewSet):
    parent_account_lookup = 'parent_lookup_account_id'
    parent_mg_lookup = 'parent_lookup_mg_id'
    serializer_class = SeatSerializer
    permission_classes = (
        And(permissions.IsAuthenticated,
            Or(HasPermission(parent_account_lookup, AdminPermissions.MOD_MG),
               IsAdminUser),
            AccountOwnsManagementGroup(parent_account_lookup,
                                       parent_mg_lookup)),)
    seat_attr = ""

    def get_queryset(self):
        try:
            mg = ManagementGroup.objects.get(
                account=self.kwargs.get(self.parent_account_lookup),
                pk=self.kwargs.get(self.parent_mg_lookup))
            return getattr(mg, self.seat_attr).select_related('user')
        except ManagementGroup.DoesNotExist:
            raise Http404

    def create(self, request, *args, **kwargs):
        """Add a manager to the management group.

        Required argument is 'seat' which is a seat ID
        """
        seat_id = request.data.get('seat')
        if not seat_id:
            raise exceptions.APIException("A seat ID is required to attach "
                                          "{}".format(self.seat_attr))

        seat = get_object_or_404(
            Seat, pk=seat_id,
            account=kwargs.get(self.parent_account_lookup))

        mg = get_object_or_404(
            ManagementGroup, pk=kwargs.get(self.parent_mg_lookup),
            account=kwargs.get(self.parent_account_lookup))

        # E.g. management_group.add_members(seat)
        try:
            getattr(mg, "add_{}".format(self.seat_attr))(seat)
        except ValueError as err:
            raise exceptions.APIException(err)
        return Response({})

    def destroy(self, request, pk, *args, **kwargs):
        """Remove a seat from the management group."""
        seat = get_object_or_404(
            Seat, pk=pk, account=kwargs.get(self.parent_account_lookup))
        mg = get_object_or_404(
            ManagementGroup, pk=kwargs.get(self.parent_mg_lookup),
            account=kwargs.get(self.parent_account_lookup))

        # Remove the seat
        getattr(mg, self.seat_attr).remove(seat)
        return Response({})


class ManagementGroupsManagersViewSet(ManagementGroupsSeatsBase):
    """Add/remove managers to/from Management Group."""
    seat_attr = "managers"


class ManagementGroupsMembersViewSet(ManagementGroupsSeatsBase):
    """Add/remove members to/from Management Group."""
    seat_attr = "members"
