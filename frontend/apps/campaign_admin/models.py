
from django.db import models
from django.contrib.postgres import fields

from frontend.libs.utils.model_utils import Choices


class ManagementGroup(models.Model):
    class Permissions(Choices):
        choices_map = (
            ("TR", "TRACKER", "Tracker"),
        )

    name = models.CharField(max_length=256, blank=True)
    account = models.ForeignKey("campaign.Account", on_delete=models.CASCADE)
    managers = models.ManyToManyField(
        "seat.Seat",
        blank=True,
        related_name="management_groups_manager",
        help_text="The managers of the members of this group. The permissions "
                  "field gives these users permissions over the members"
    )
    members = models.ManyToManyField(
        "seat.Seat",
        blank=True,
        related_name="management_groups_member",
        help_text="These users are managed by the managers."
    )
    permissions = fields.ArrayField(models.CharField(max_length=2,),
                                    default=[], blank=True)

    class Meta:
        unique_together = (("account", "name"),)

    def _add_seat(self, seat_attr, other_seat_attr, seat):
        """Check if the other group (e.g. members) already contains the
        given seats. If not, add them.
        """
        if getattr(self, other_seat_attr).filter(pk=seat.pk).exists():
            raise ValueError("The given seat is already in '{}'. It "
                             "cannot be in both.".format(other_seat_attr))
        else:
            getattr(self, seat_attr).add(seat)

    def add_managers(self, seat):
        self._add_seat('managers', 'members', seat)

    def add_members(self, seat):
        self._add_seat('members', 'managers', seat)

