from django.urls import reverse

from frontend.apps.authorize.test_utils import AuthorizedUserTestCase
from frontend.apps.campaign.factories import (
    PoliticalCampaignFactory)
from frontend.apps.campaign_admin.models import ManagementGroup
from frontend.apps.campaign_admin.factories import ManagementGroupFactory
from frontend.apps.role.permissions import AdminPermissions
from frontend.apps.seat.factories import SeatFactory


class ActivationViewTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        self.campaign = PoliticalCampaignFactory(active=False)
        super(ActivationViewTests, self).setUp(campaigns=[self.campaign])
        self._add_admin_group(self.user)
        self.campaign.account_head = self.user
        self.campaign.save()
        self.url = reverse("campaign_admin_activate")

    def test_redirect(self):
        home = reverse("campaign_admin_home")

        ## Verify user is redirected if campaign is not activated
        response = self.client.get(home)
        self.assertRedirects(response, "{}?next={}".format(self.url, home))

        ## Activate campaign and verify user is not redirected
        self.campaign.active = True
        self.campaign.save()
        response = self.client.get(home)
        self.assertContains(response, "")
        self.assertTemplateUsed(response,
                                "campaign_admin/account_settings.html")


class AccountManagementGroupsViewSetTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        super(AccountManagementGroupsViewSetTests, self).setUp(login_now=False)
        self.account = self.user.current_seat.account
        self.mg = ManagementGroupFactory(account=self.account)
        self.url = reverse("management_groups_api-list",
                           args=(self.account.id,))
        self.url2 = reverse("management_groups_api-detail",
                            args=(self.account.id, self.mg.id))

    def test_permissions(self):
        ## Verify unauthorized users are rejected
        result = self.client.get(self.url)
        self.assertNoCredentials(result)
        result = self.client.get(self.url2)
        self.assertNoCredentials(result)

        # Login the user
        self._login(self.user)
        # Verify user is not an admin, or that would invalidate the test.
        self.assertFalse(self.user.is_admin)

        ## Verify a standard user is rejected from viewing all MGs
        result = self.client.get(self.url)
        self.assertNoPermission(result)
        ## Verify a standard user is rejected from creating a MG
        result = self.client.post(self.url, data={})
        self.assertNoPermission(result)

        # Add the Account Admin group to the user
        self._add_admin_group(permissions=AdminPermissions.MOD_MG)

        ## Verify the user now has access
        result = self.client.get(self.url)
        self.assertContains(result, '"id":{}'.format(self.mg.id))
        self.assertContains(result, '"name":"{}"'.format(self.mg.name))
        self.assertContains(result, '"is_manager":null')
        result = self.client.get(self.url2)
        self.assertContains(result, '"id":{}'.format(self.mg.id))

        ## Verify admin user doesn't have access to a different campaign's MG
        mg = ManagementGroupFactory()
        result = self.client.get(reverse("management_groups_api-list",
                                         args=(mg.account.id,)))
        self.assertNoPermission(result)
        result = self.client.get(reverse("management_groups_api-detail",
                                 args=(mg.account.id, mg.id)))
        self.assertNoPermission(result)

    def test_create(self):
        # Login and make admin
        self._login_and_add_admin_group(permissions=AdminPermissions.MOD_MG)

        # Test default name
        result = self.client.post(self.url, data={})
        self.assertContains(result, '"name":"Management Group ({}'.format(
                            ManagementGroup.objects.count()))

        # Test given name
        name = "test_name"
        result = self.client.post(self.url, data={"name": name})
        self.assertContains(result, '"name":"{}"'.format(name))

        ## Verify query fails if same name is given
        result = self.client.post(self.url, data={"name": name})
        self.assertContains(result, "Management Group with name '{}' already "
                                    "exists".format(name), status_code=500)


class ManagementGroupsSeatsBaseTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        super(ManagementGroupsSeatsBaseTests, self).setUp(login_now=False)
        self.account = self.user.current_seat.account
        self.seat = SeatFactory(account=self.account)
        self.mg = ManagementGroupFactory(account=self.account,
                                         managers=[self.seat])
        self.url = reverse("management_group_managers_api-list",
                           args=(self.account.id, self.mg.id))
        self.url2 = reverse("management_group_managers_api-detail",
                            args=(self.account.id, self.mg.id, self.seat.id))

    def test_permissions(self):
        ## Verify unauthorized users are rejected
        result = self.client.get(self.url)
        self.assertNoCredentials(result)

        # Login the user
        self._login(self.user)
        # Verify user is not an admin, or that would invalidate the test.
        self.assertFalse(self.user.is_admin)

        ## Verify a standard user is rejected from viewing all managers
        result = self.client.get(self.url)
        self.assertNoPermission(result)
        ## Verify a standard user is rejected from creating a manager
        result = self.client.post(self.url, data={})
        self.assertNoPermission(result)

        # Add the Account Admin group to the user
        self._add_admin_group(permissions=AdminPermissions.MOD_MG)

        ## Verify the user now has access
        result = self.client.get(self.url)
        self.assertContains(result, '"id":{}'.format(self.seat.id))
        self.assertContains(result, '"first_name":"{}"'.format(
            self.seat.user.first_name))
        self.assertContains(result, '"account":{}'.format(self.account.id))

        ## Verify admin user doesn't have access to a different campaign's MG
        mg = ManagementGroupFactory()
        result = self.client.get(reverse("management_group_managers_api-list",
                                         args=(mg.account.id, self.mg.id)))
        self.assertNoPermission(result)

    def test_create(self):
        # Login and make admin
        self._login_and_add_admin_group(permissions=AdminPermissions.MOD_MG)

        ## Verify no seat id raises error
        result = self.client.post(self.url, data={})
        self.assertContains(result, "seat ID is required to attach managers",
                            status_code=500)

        ## Verify basic positive case
        seat = SeatFactory(account=self.account)
        self.assertNotIn(seat, self.mg.managers.all())
        result = self.client.post(self.url, data={'seat': seat.id})
        self.assertContains(result, "")
        self.assertIn(seat, self.mg.managers.all())
        self.assertNotIn(seat, self.mg.members.all())

        ## Attempt to add seat to members. Error expected
        result = self.client.post(reverse("management_group_members_api-list",
                                          args=(self.account.id, self.mg.id)),
                                  data={'seat': seat.id})
        self.assertContains(result, "The given seat is already in 'managers'",
                            status_code=500)
        self.assertNotIn(seat, self.mg.members.all())

    def test_destroy(self):
        # Login and make admin
        self._login_and_add_admin_group(permissions=AdminPermissions.MOD_MG)

        ## Verify seat can be removed
        self.assertIn(self.seat, self.mg.managers.all())
        self.assertNotIn(self.seat, self.mg.members.all())
        result = self.client.delete(self.url2)
        self.assertContains(result, "")
        self.assertNotIn(self.seat, self.mg.managers.all())
        self.assertNotIn(self.seat, self.mg.members.all())
