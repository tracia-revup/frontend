from collections import OrderedDict
import logging

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from localflavor.us.us_states import USPS_CHOICES

from frontend.apps.campaign.routes import ROUTES
from frontend.apps.campaign_admin.models import ManagementGroup
from frontend.apps.role.decorators import any_group_required
from frontend.apps.role.permissions import AdminPermissions
from frontend.libs.django_view_router import (RoutingView, RoutingTemplateView,
                                              Route)


LOGGER = logging.getLogger(__name__)


class CampaignAdminHome(RoutingTemplateView):
    routes = ROUTES
    template_name = 'campaign_admin/home/base.html'

    @method_decorator(login_required)
    @method_decorator(any_group_required(*AdminPermissions.all()))
    def get(self, request, *args, **kwargs):
        seat = request.user.current_seat
        user = request.user

        request.breadcrumbs([('Home', reverse('index')),
                             ('Settings', '')])

        # Prepare the analysis config question sets
        account = seat.account
        anal_config = account.get_default_analysis_config()
        # Verify the analysis config belongs to this account
        if anal_config and not \
                account.analysis_configs.filter(id=anal_config.id).exists():
            anal_config = None
        question_sets = OrderedDict()

        if anal_config:
            qs_kwargs = {}
            if not user.is_admin:
                # If user is not an admin, do not show staff only QuestionSets
                qs_kwargs['staff_only'] = False

            # Generate the QuestionSet forms for each of the question sets.
            for qs in anal_config.get_question_sets(**qs_kwargs):
                if qs.id in question_sets or not qs.dynamic_class:
                    continue
                else:
                    question_sets[qs.id] = qs.dynamic_class().get_info(
                        qs, account_id=account.id,
                        analysis_config_id=anal_config.id)

        return render(
            request=request,
            template_name=self.template_name,
            context={'account': account,
                     'analysis_config': anal_config,
                     'question_sets': question_sets,
                     'user': request.user,
                     'request': request}
        )


class ManagementGroupTrackerView(RoutingView):
    routes = ROUTES

    def get_breadcrumbs(self):
        return [('Home', reverse('index')),
                ('Campaign', '')]

    @method_decorator(login_required)
    def get(self, request, pk):
        self.request.breadcrumbs(self.get_breadcrumbs())
        seat = request.user.current_seat
        account = seat.account
        if seat.has_permissions(all=[AdminPermissions.FR_TRACKER]):
            mg = get_object_or_404(ManagementGroup, pk=pk,
                                   account=seat.account)
        else:
            mg = get_object_or_404(seat.management_groups_manager, pk=pk)

        return render(request=request,
                      template_name='campaign_admin/mg_tracker.html',
                      context={'account': account,
                               'seat': seat,
                               'mg': mg,
                               'user': request.user,
                               'request': request})


class FundraisersView(RoutingTemplateView):
    template_name = 'campaign_admin/fundraisers.html'
    routes = ROUTES

    @method_decorator(login_required)
    def do_dispatch(self, request, *args, **kwargs):
        seat = request.user.current_seat
        # User must have admin permission
        if not seat.has_permissions(any=[AdminPermissions.VIEW_FUNDRAISERS]):
            # Before rejecting, check if they are a MG manager
            mg = seat.management_groups_manager.first()
            if mg:
                # Redirect to MG detail page
                return redirect("campaign_admin_mg_tracker", mg.id)
            else:
                return redirect('index')
        return super(FundraisersView, self).do_dispatch(request,
                                                        *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(FundraisersView, self).get_context_data(**kwargs)
        context['account'] = self.request.user.current_seat.account
        return context


class EventsView(RoutingTemplateView):
    template_name = 'campaign_admin/events.html'
    routes = ROUTES

    @method_decorator(login_required)
    @method_decorator(any_group_required(AdminPermissions.EV_TRACKER))
    def do_dispatch(self, *args, **kwargs):
        return super(EventsView, self).do_dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EventsView, self).get_context_data(**kwargs)
        context['account'] = self.request.user.current_seat.account
        return context


@method_decorator(login_required, name='dispatch')
class ActivationView(RoutingTemplateView):
    routes = [
        Route(lambda r, v:
              not r.user.current_seat.account.account_profile.product.recurly_plan_code,
              name="Not Recurly", suffix="nr")
    ]
    template_name = 'campaign_admin/payment.html'
    template_name_nr = 'campaign_admin/pending_activation.html'

    @method_decorator(login_required)
    @method_decorator(any_group_required(*AdminPermissions.all()))
    def do_dispatch(self, request, handler, *args, **kwargs):
        self.next = request.GET.get('next')
        self.account = self.request.user.current_seat.account
        return super(ActivationView, self).do_dispatch(
            request, handler, *args, **kwargs)

    def get_breadcrumbs(self):
        return [('Home', reverse('index')),
                ('Admin', reverse('campaign_admin_home')),
                ('Payment', '')]

    def get_context_data(self, **kwargs):
        self.request.breadcrumbs(self.get_breadcrumbs())

        context = super(ActivationView, self).get_context_data(**kwargs)
        context['account'] = self.account
        context['next'] = self.next
        context['state_choices'] = USPS_CHOICES
        return context

    def get(self, request, *args, **kwargs):
        if self.account.active:
            return HttpResponseRedirect(self.next or
                                        reverse("campaign_admin_home"))
        else:
            return super(ActivationView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        cc_token = request.POST.get("recurly-token")
        if cc_token:
            try:
                self.account.get_or_create_recurly_subscription(cc_token)
            except Exception as err:
                messages.error(request, str(err))
            else:
                if not self.account.active:
                    self.account.active = True
                    self.account.save()
                return HttpResponseRedirect(self.next or
                                            reverse("campaign_admin_home"))

        context = self.get_context_data(**kwargs)
        return self.render(context)
