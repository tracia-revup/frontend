from django.apps import AppConfig



class AuthorizeConfig(AppConfig):
    name = 'frontend.apps.authorize'

    def ready(self):
        from actstream import registry
        registry.register(self.get_model('RevUpUser'))
