from collections import Iterable

from rest_framework import serializers
from rest_framework_jwt.serializers import JSONWebTokenSerializer as JWTS

from frontend.apps.authorize.models import (
    RevUpUser, RevUpTask, UserEventLink, AnalysisViewUserSettings,
    AnalysisViewSettingSet)
from frontend.apps.campaign.api.serializers import (
    UserEventSerializer)
from frontend.libs.utils.api_utils import WritableMethodField
from frontend.libs.utils.form_utils import phone_digits_re

MINIMAL_FIELDS = ('id', 'first_name', 'last_name', 'email', "is_active")
EXPANDED_FIELDS = MINIMAL_FIELDS + ("street_1", "street_2", "city",
                                    "state", "zip", "phone",)


class RevUpUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = RevUpUser
        depth = 4

        fields = EXPANDED_FIELDS + ('current_seat', )
        read_only_fields = ('id', 'current_seat', 'email', 'is_active')

    current_seat = serializers.SerializerMethodField()
    first_name = serializers.CharField(max_length=64, required=True)
    last_name = serializers.CharField(max_length=64, required=True)
    phone = serializers.RegexField(regex=phone_digits_re, required=True)

    def get_current_seat(self, user):
        # Had a circular import issue, so had to serialize in a method
        from frontend.apps.seat.api.serializers import BaseSeatSerializer
        if not user.current_seat:
            # If the user doesn't have a current seat, let's see if they have
            # only one valid seat they could be using
            seats = user.get_valid_seats()
            if len(seats) == 1:
                user.reset_current_seat(seats[0], self.context.get("request"))

        # We want to show some extra information about the account
        # (needed in mobile)
        context = self.context.copy()
        context.update(dict(expand_account=True))
        return BaseSeatSerializer(user.current_seat, context=context).data


class CampaignUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = RevUpUser
        fields = MINIMAL_FIELDS + ('events',)

    events = serializers.SerializerMethodField('_events')

    def _events(self, user):
        """This serializer expects an account. If there is none given, we
        cannot return the events.

        This is a security precaution, so we don't accidentally
        display the wrong events to the wrong campaign.
        """
        account_id = self.context.get('account_id')
        if account_id is None:
            return []
        else:
            try:
                # The below query adds a lot of queries to the page view, so if
                # a prefetch has already been done, use it.
                event_links = user._prefetched_objects_cache['event_links']
                # Ensure all events belong to the given account_id
                event_links = [el for el in event_links
                               if el.event.account_id == account_id]
                return UserEventSerializer(
                    event_links, many=True,
                    read_only=True).data
            except (AttributeError, KeyError):
                events = user.event_links.select_related("event").filter(
                    event__account_id=account_id)
                return UserEventSerializer(events, many=True,
                                           read_only=True).data


class RevUpUserSubsetSerializer(serializers.ModelSerializer):
    """A subset of the RevUpUserSerializer.

    We don't want to show campaign admins all of a user's events and campaigns.
    """
    class Meta:
        model = RevUpUser
        fields = MINIMAL_FIELDS


class RevUpTaskSerializer(serializers.ModelSerializer):
    label = serializers.ReadOnlyField(source='get_label')
    status = serializers.ReadOnlyField(source='get_status_label')
    percent = serializers.ReadOnlyField(source='get_percent')
    verbose_task_type = serializers.SerializerMethodField()
    destination_label = serializers.SerializerMethodField()

    class Meta:
        model = RevUpTask
        fields = ('id', 'label', 'task_type', 'verbose_task_type',
                  'status', 'percent', "account", "user", "destination_label")

    def get_verbose_task_type(self, task):
        return RevUpTask.TaskType.translate(task.task_type, "Unknown")

    def get_destination_label(self, task):
        if task.task_type == RevUpTask.TaskType.CLONE_CONTACTS:
            return "{}'s Contacts".format(task.user.name)
        else:
            return task.label


class EventUserSerializer(serializers.ModelSerializer):
    """A subset of the RevUpUserSerializer.

    We don't want to show campaign admins all of a user's events and campaigns.
    """
    class Meta:
        model = UserEventLink
        fields = MINIMAL_FIELDS + ("is_lead",)

    id = serializers.ReadOnlyField(source='user.id')
    first_name = serializers.ReadOnlyField(source='user.first_name')
    last_name = serializers.ReadOnlyField(source='user.last_name')
    email = serializers.ReadOnlyField(source='user.email')
    is_active = serializers.ReadOnlyField(source='user.is_active')


class AnalysisViewUserSettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnalysisViewUserSettings
        fields = ('id', 'user', 'analysis_profile', 'data')

    data = WritableMethodField("get_data", write_method_name="process_data")

    def __init__(self, *args, **kwargs):
        super(AnalysisViewUserSettingsSerializer, self).__init__(*args, **kwargs)
        # User and AnalysisProfile should never change on a setting.
        # `self.init_data` is data that comes from a form to either create a
        # new record, or update an existing record. Overwrite user and
        # analysis_profile in the form data from the existing record, if it
        # exists, to ensure user and analysis profile do not change. This also
        # means user and analysis_profile do not need to be specified when
        # updating an existing record, but are still present during the
        # validation steps.
        if hasattr(self, 'initial_data') and self.instance:
            for key in ('user', 'analysis_profile'):
                value = getattr(self.instance, '%s_id' % key)
                if value:
                    self.initial_data[key] = value

    def process_data(self, data):
        # Prepare input value for the data field for persistence to the
        # database. In order to prepare the value we need access to the
        # AnalysisViewSettingSet instance for the user in order to get the
        # associated controller class, where the proper `process_data` method
        # lives. In order to get this class we either need to get to the model
        # instance for this record on the root of the serializer, which may not
        # be set, or we need to get the analysis_profile primary key.
        if self.root.instance:
            analysis_view_setting_set = self.root.instance.analysis_profile\
                    .analysis_view_setting_set
        else:
            analysis_profile_id = self.root.initial_data['analysis_profile']
            analysis_view_setting_set = AnalysisViewSettingSet.objects.get(
                analysisprofile=analysis_profile_id)

        controller = analysis_view_setting_set.dynamic_class()
        data = controller.process_data(data)
        return data

    def get_data(self, _):
        if isinstance(self.instance, Iterable):
            return [i.get_deep_data() for i in self.instance]
        else:
            return self.instance.get_deep_data()


class JSONWebTokenSerializer(JWTS):
    """This is subclassed just so I can change the username field to email"""
    @property
    def username_field(self):
        return "email"


def jwt_response_payload_handler(token, user=None, request=None):
    return {
        'token': token,
        'user': RevUpUserSerializer(user, context={'request': request}).data
    }
