
from frontend.libs.utils.api_utils import IsOwner


class IsUserOwner(IsOwner):
    OBJ_FIELD = 'id'


class UserIsRequester(IsOwner):
    """This permission just verifies that the user_id in the URL is the same
    user as request.user.
    """
    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view)
