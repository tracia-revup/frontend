from datetime import timedelta, datetime

from django.conf import settings
from django.db.models import Q
from django.http import Http404
from django.shortcuts import get_object_or_404
from rest_condition.permissions import Or, And
from rest_framework import permissions, exceptions, viewsets, mixins
from rest_framework.response import Response
from rest_framework_extensions.mixins import NestedViewSetMixin
from rest_framework_extensions.utils import compose_parent_pk_kwarg_name
from rest_framework_jwt.views import ObtainJSONWebToken as OJWT

from frontend.apps.analysis.models import AnalysisProfile
from frontend.apps.authorize.api.permissions import IsUserOwner
from frontend.apps.authorize.api.serializers import (
    RevUpUserSerializer, RevUpTaskSerializer, EventUserSerializer,
    AnalysisViewUserSettingsSerializer, JSONWebTokenSerializer)
from frontend.apps.authorize.models import (
    RevUpUser, RevUpTask, UserEventLink, AnalysisViewUserSettings)
from frontend.apps.campaign.api.permissions import AccountOwnsEvent
from frontend.apps.campaign.models import Event
from frontend.apps.role.api.permissions import HasPermission
from frontend.apps.role.permissions import AdminPermissions
from frontend.apps.seat.models import Seat
from frontend.apps.seat.api.permissions import SeatHasPermission, UserOwnsSeat
from frontend.libs.utils.api_utils import (
    IsOwner, IsAdminUser, AutoinjectUrlParamsIntoDataMixin, InitialMixin)
from frontend.libs.utils.string_utils import str_to_bool


class RevUpUserViewSet(viewsets.ReadOnlyModelViewSet,
                       mixins.UpdateModelMixin):
    serializer_class = RevUpUserSerializer
    permission_classes = (And(permissions.IsAuthenticated,
                              Or(IsUserOwner, IsAdminUser)),)
    model = RevUpUser
    queryset = RevUpUser.objects.all()


class RevUpTaskViewSet(NestedViewSetMixin, InitialMixin,
                       viewsets.ReadOnlyModelViewSet,
                       viewsets.mixins.DestroyModelMixin):
    parent_user_lookup = compose_parent_pk_kwarg_name('user_id')
    parent_seat_lookup = compose_parent_pk_kwarg_name('seat_id')
    serializer_class = RevUpTaskSerializer
    admin_contact_permissions = [AdminPermissions.VIEW_CONTACTS]
    permission_classes = (
        And(permissions.IsAuthenticated,
            Or(UserOwnsSeat(parent_seat_lookup),
               SeatHasPermission(parent_seat_lookup,
                                 admin_contact_permissions))),)

    def _pre_permission_initial(self):
        # queries needed before permission checks
        user_id = self.kwargs.get(self.parent_user_lookup)
        seat_id = self.kwargs.get(self.parent_seat_lookup)
        try:
            user_id = int(user_id)
            seat_id = int(seat_id)
        except ValueError:
            raise Http404
        else:
            self.seat = get_object_or_404(
                Seat.objects.select_related("account"),
                id=seat_id,
                user=user_id)

    def get_queryset(self):
        qs = RevUpTask.objects.select_related("account", "user")
        # If the task is an analysis task, we only want to include the analyses
        # for the given seat, not analyses for the user's other seats.
        qs = (qs.filter(task_type=RevUpTask.TaskType.NETWORK_SEARCH,
                        account_id=self.seat.account_id) |
              qs.filter(task_type__in=RevUpTask.TaskType.NON_ANALYSIS_TASKS))
        # Query for all of the user's tasks
        q = Q(user_id=self.seat.user_id)
        # This will return any tasks for the account, if the seat is allowed.
        if self.seat.has_permissions(any=self.admin_contact_permissions):
            q |= Q(user_id=self.seat.account.account_user_id)
        qs = qs.filter(q)

        # Tasks created before alive_cutoff are expired and should not show.
        alive_cutoff = (datetime.utcnow() -
                        timedelta(hours=settings.REVUP_TASK_TTL_HOURS))

        return qs.filter(revoked=False, utc_created__gt=alive_cutoff)


class EventUsersViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    """This API is a derivative of the Events API.

    This gives access to an Event's users.

    The permissions for this API are as follows:
        The user must be logged-in. AND the user must either be an
        Admin user, OR the user must be a campaign admin.
        Also, the given campaign must match the campaign in the event.
    """
    parent_account_lookup = 'parent_lookup_account_id'
    parent_event_lookup = 'parent_lookup_event_id'
    lookup_field = 'user'
    serializer_class = EventUserSerializer
    permission_classes = (
        And(permissions.IsAuthenticated,
            Or(IsAdminUser,
               Or(HasPermission(parent_account_lookup,
                                [AdminPermissions.VIEW_FUNDRAISERS,
                                 AdminPermissions.MOD_FUNDRAISERS],
                                methods=['GET']),
                  HasPermission(parent_account_lookup,
                                AdminPermissions.MOD_FUNDRAISERS,
                                methods=['POST', 'PUT', 'DELETE']))),
            AccountOwnsEvent(parent_account_lookup, parent_event_lookup)),
    )

    def get_queryset(self):
        return UserEventLink.objects.select_related('user').filter(
            event=self.kwargs.get(self.parent_event_lookup),
            user__seats__state__in=Seat.States.ASSIGNED,
            user__seats__account__pk=self.kwargs.get(
                self.parent_account_lookup))

    def create(self, request, *args, **kwargs):
        """Add the given user to the event."""
        user_id = request.data.get('user')
        if not user_id:
            raise exceptions.APIException(
                "'create' expects a 'user' field containing a user ID")

        try:
            user = RevUpUser.objects.get(pk=user_id)
        except RevUpUser.DoesNotExist:
            raise exceptions.APIException("The given user: '{}' does not "
                                          "exist".format(user_id))

        if self.kwargs.get(self.parent_account_lookup) not in \
                (str(s.account_id) for s in user.seats.assigned()):
            raise exceptions.APIException("The given user: '{}' is not a "
                                          "member of the given campaign"
                                          .format(user_id))

        # Check if the user should be a lead fundraiser
        is_lead = str_to_bool(request.data.get('is_lead', False))

        event = get_object_or_404(Event, pk=self.kwargs.get(
            self.parent_event_lookup))

        user.add_events(event, is_lead=is_lead)
        return Response()

    def destroy(self, request, *args, **kwargs):
        """Remove the given user from the event."""
        uel = get_object_or_404(UserEventLink, user=kwargs.get('user'),
                                event=kwargs.get(self.parent_event_lookup))
        uel.delete()
        return Response()

    def update(self, request, *args, **kwargs):
        """Update the event membership.

        The only thing that can be modified is the "is_lead" field.
        """
        uel = get_object_or_404(UserEventLink, user=kwargs.get('user'),
                                event=kwargs.get(self.parent_event_lookup))
        is_lead = str_to_bool(request.data.get('is_lead'))
        if uel.is_lead != is_lead:
            uel.is_lead = is_lead
            uel.save()
        return Response()


class AnalysisViewUserSettingsViewSet(AutoinjectUrlParamsIntoDataMixin,
                                      NestedViewSetMixin,
                                      viewsets.ModelViewSet):
    model = AnalysisViewUserSettings
    queryset = AnalysisViewUserSettings.objects.select_related(
        "analysis_profile__analysis_view_setting_set")
    serializer_class = AnalysisViewUserSettingsSerializer
    permission_classes = (And(permissions.IsAuthenticated,
                              Or(IsOwner, IsAdminUser)),)
    autoinject_map = {'user': 'user_id'}

    def filter_queryset(self, queryset):
        profile = self.request.GET.get("analysis_profile")
        if profile:
            queryset = queryset.filter(analysis_profile=profile)
        return queryset

    def list(self, request, *args, **kwargs):
        """Overriding the default list because we need to generate a default
        record if the user has never saved a settings instance.
        """
        queryset = self.filter_queryset(self.get_queryset())
        if not queryset and "analysis_profile" in request.GET:
            try:
                analysis_profile = AnalysisProfile.objects.get(
                    id=request.GET.get("analysis_profile"))
            except AnalysisProfile.DoesNotExist:
                analysis_profile = None
            # Verify this user has access to this analysis profile
            if analysis_profile and RevUpUser.objects.filter(
                        id=request.user.id,
                        seats__account__analysis_configs__analysis_profile=
                        analysis_profile).exists():
                avus = AnalysisViewUserSettings(
                    analysis_profile=analysis_profile, user=request.user)
                serializer = AnalysisViewUserSettingsSerializer(avus)
                data = [serializer.data]
            else:
                data = []
            return Response(data)
        else:
            return Response(AnalysisViewUserSettingsSerializer(queryset,
                                                               many=True).data)


class ObtainJSONWebToken(OJWT):
    """Subclassing this just so I can modify the serializer class."""
    serializer_class = JSONWebTokenSerializer
