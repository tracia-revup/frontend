from contextlib import ExitStack
import datetime

from django.conf import settings
from django.db import IntegrityError, connection
from django.http import HttpRequest
from django.urls import reverse
import json
import unittest.mock as mock

from rest_framework import status
from social_django.models import UserSocialAuth

from frontend.apps.authorize.api.serializers import MINIMAL_FIELDS
from frontend.apps.authorize.factories import (GroupFactory, RevUpUserFactory,
                                               RevUpUserTokenFactory)
from frontend.apps.authorize.models import (
    RevUpTask, RevUpUser, RevUpUserToken, UserEventLink,
    AnalysisViewUserSettings)
# noinspection PyProtectedMember
from frontend.apps.authorize.utils import (
    _send_user_email, send_account_user_added_notification,
    send_admin_added_notification, create_revuptask_for_contact_source)
from frontend.apps.authorize.test_utils import AuthorizedUserTestCase
from frontend.apps.analysis.factories import (
    AnalysisFactory, AnalysisProfileFactory)
from frontend.apps.campaign.factories import (
    PoliticalCampaignFactory, ProspectFactory, AccountFactory, EventFactory)
from frontend.apps.campaign.api.views import EventProspectsViewSet
from frontend.apps.contact.factories import (
    ImportConfigFactory, ImportConfig, ImportRecordFactory)
from frontend.apps.contact.task_utils import start_contact_import
from frontend.apps.contact_set.factories import ContactSetFactory
from frontend.apps.role.permissions import AdminPermissions
from frontend.apps.seat.factories import SeatFactory, Seat
from frontend.libs.utils.string_utils import random_email
from frontend.libs.utils.test_utils import TestCase


class TasksTests(TestCase):
    def setUp(self):
        self.campaign = PoliticalCampaignFactory()
        self.inviter = RevUpUserFactory(is_active=False)
        self.user = RevUpUserFactory(is_active=False)
        self.seat = SeatFactory(account=self.campaign,
                                user=self.user)
        self.user.current_seat = self.seat
        self.user.save()

    def test_email_includes_inviter(self):
        self.seat.inviting_user = self.inviter
        self.seat.save()
        # Mock out the email sender, just in case we're accidentally sending
        # to real emails.
        with mock.patch("frontend.libs.utils.email_utils.EmailMessage") as m:
            args = (self.user, "email subject",
                    'authorize/emails/campaign_added_email.html',
                    {"protocol_host": "http://testserver",
                     "account": self.campaign}, "test task",
                    '', self.inviter)

            token = self.user.generate_token()
            try:
                _send_user_email(*args)
            finally:
                self.user.clean_tokens()
                self.seat.inviting_user = None
                self.seat.save()

            # Verify the body
            self.assertIn(self.campaign.title, m.call_args[0][1])
            self.assertIn(self.inviter.name, m.call_args[0][1])

            # Verify the sender
            self.assertIn(settings.DEFAULT_FROM_EMAIL, m.call_args[0][2])
            self.assertIn(self.inviter.name, m.call_args[0][2])

    def test_send_user_email(self):
        # Mock out the email sender, just in case we're accidentally sending
        # to real emails.
        with mock.patch("frontend.libs.utils.email_utils.EmailMessage") as m:
            args = (self.user, "email subject",
                    'authorize/emails/admin_added_email.html',
                    {"protocol_host": "http://testserver",
                     "account": self.campaign}, "test task")

            ## Verify a missing registration token raises an exception
            self.assertRaisesRegex(
                Exception, "Cannot send a registration notification without "
                "a registration token", send_account_user_added_notification,
                self.user, self.campaign, "http://testserver")

            # Generate a token for the registration
            token = self.user.generate_token()
            # Re-execute the function for the positive case
            _send_user_email(*args)

            # Verify the subject
            self.assertEqual(m.call_args[0][0], "email subject")
            # Verify the body
            self.assertIn("It's time to accelerate your fundraising",
                          m.call_args[0][1])
            self.assertFalse(self.user.is_active)
            url = "http://testserver{}?next={}".format(
                reverse("finish_account_register", args=(token.token,)),
                reverse("campaign_admin_home"))
            self.assertIn('href="{}"'.format(url), m.call_args[0][1])
            self.assertNotIn('href="http://testserver"', m.call_args[0][1])

            # Verify the sender
            self.assertEqual(m.call_args[0][2], settings.DEFAULT_FROM_EMAIL)
            # Verify the recipient
            self.assertEqual(m.call_args[0][3], [self.user.email])

            ## Verify active user doesn't have a registration link
            m.reset_mock()
            self.user.clean_tokens()
            self.user.is_active = True
            self.user.save()
            _send_user_email(*args)
            self.assertNotIn('href="{}'.format(url), m.call_args[0][1])
            # Double check
            self.assertIn('href="http://testserver{}"'.format(
                reverse("campaign_admin_home")), m.call_args[0][1])
            self.assertIn("Get Started", m.call_args[0][1])
            self.assertNotIn("set up your account", m.call_args[0][1])
            self.assertNotIn("Complete Registration", m.call_args[0][1])

    def test_create_revuptask_util(self):
        """Test the create revup task util method."""
        contact_set = ContactSetFactory()
        task = create_revuptask_for_contact_source(
            contact_set, RevUpTask.TaskType.DELETE_CONTACTS)

        # Check that the task has been create successfully and that the task
        # type is correct
        self.assertTrue(RevUpTask.objects.filter(id=task.id).exists())

        # Check that the created task has the correct values
        self.assertEqual(task.user, contact_set.user)
        self.assertEqual(task.task_type, RevUpTask.TaskType.DELETE_CONTACTS)

        # Check that if the task already exists that it isn't created again
        # and that an error is raised
        self.assertRaises(RuntimeError, create_revuptask_for_contact_source,
                          contact_set, RevUpTask.TaskType.DELETE_CONTACTS)

        # Check that there is only one task created for this contact_set/user
        self.assertEqual(RevUpTask.objects.filter(id=task.id).count(), 1)

        # Create a task of a different task type to verify the task type is
        # correct
        task2 = create_revuptask_for_contact_source(
            contact_set, RevUpTask.TaskType.CLONE_CONTACTS)
        self.assertEqual(task2.task_type, RevUpTask.TaskType.CLONE_CONTACTS)

    def test_send_account_user_added_notification(self):
        # Mock out the email sender, just in case we're accidentally sending
        # to real emails.
        with mock.patch("frontend.apps.authorize.utils._send_user_email") as m:
            protocol_host = "http://testserver"
            send_account_user_added_notification(
                self.user, self.campaign, protocol_host,
                inviting_user=self.inviter)
            m.assert_called_with(
                self.user,
                "Invitation from {}".format(self.campaign.title),
                'authorize/emails/campaign_added_email.html',
                {'account': self.campaign, 'protocol_host': protocol_host},
                "campaign added notification email",
                "for campaign: '{}'".format(self.campaign),
                inviting_user=self.inviter
            )

    def test_send_admin_user_added_notification(self):
        # Mock out the email sender, just in case we're accidentally sending
        # to real emails.
        with mock.patch("frontend.apps.authorize.utils._send_user_email") as m:
            protocol_host = "http://testserver"
            send_admin_added_notification(self.user, self.campaign,
                                          protocol_host,
                                          inviting_user=self.inviter)
            m.assert_called_with(
                self.user, "You are now an Account Administrator",
                'authorize/emails/admin_added_email.html',
                {'protocol_host': protocol_host, "account": self.campaign},
                "client added notification email",
                inviting_user=self.inviter
            )


class RevUpUserTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        with connection.cursor() as cursor:
            cursor.execute("CREATE UNIQUE INDEX IF NOT EXISTS "
                           "authorize_revupuser_email_insensitive_unique "
                           "ON authorize_revupuser(lower(email))")

    def setUp(self):
        self.group1 = GroupFactory(name="Group1")
        self.group2 = GroupFactory(name="Group2")
        self.campaign = PoliticalCampaignFactory()
        self.event = EventFactory(account=self.campaign)
        self.user = RevUpUserFactory.create(groups=[self.group1, self.group2],
                                            events=[self.event])

    def test_default_analysis_removes_invalid_analysis_from_db(self):
        revup_user = RevUpUserFactory()
        self.seat = SeatFactory(account=self.campaign,
                                user=revup_user)
        revup_user.current_seat = self.seat
        revup_user.save()
        contact_set = ContactSetFactory(account=revup_user.current_seat.account,
                                        user=revup_user)
        # add a non-None analysis to the user object
        analysis = AnalysisFactory()
        contact_set.analysis = analysis
        contact_set.save()
        revup_user.current_analysis = analysis
        revup_user.save()
        default_analysis = revup_user.get_default_analysis()
        self.assertEqual(revup_user.current_analysis, analysis)
        self.assertEqual(analysis, default_analysis)

        # Verify deleting analysis unsets the user's current
        analysis.delete()
        revup_user = RevUpUser.objects.get(id=revup_user.id)
        self.assertIsNone(revup_user.get_default_analysis())

    def test_generate_token(self):
        token_type = RevUpUserToken.TokenTypeChoices.REGISTRATION

        def verify_token(t, dt):
            self.assertEqual(t.token_type, token_type)
            self.assertEqual(t.user, self.user)
            self.assertEqual(len(t.token), 32)
            self.assertTrue(
                dt <= token1.utc_created <= datetime.datetime.utcnow())

        ## Verify the case of a clean user
        # Clean the user first, to be sure
        self.user.clean_tokens()
        now = datetime.datetime.utcnow()
        ## Verify the token is generated correctly
        token1 = self.user.generate_token()
        verify_token(token1, now)

        ## Generate another token and verify the first is deleted
        token2 = self.user.generate_token()
        verify_token(token2, now)
        self.assertNotEqual(token1, token2)
        self.assertNotEqual(token1.token, token2.token)
        self.assertEqual(RevUpUserToken.objects.get(
            user=self.user, token_type=token_type), token2)

    def _token_count(self, user=None, tt=None):
        if user is None:
            user = self.user
        query_kwargs = {'user': user}
        if tt:
            query_kwargs['token_type'] = tt
        return RevUpUserToken.objects.filter(**query_kwargs).count()

    def test_clean_tokens(self):
        token_type = RevUpUserToken.TokenTypeChoices.REGISTRATION
        base_tc = self._token_count()

        # Create another user to verify their tokens are not deleted
        user2 = RevUpUserFactory()
        token2 = RevUpUserTokenFactory(user=user2)
        self.assertEqual(user2.get_token(), token2)
        self.assertEqual(self._token_count(user2), 1)

        # Generate a few tokens and verify they are cleaned correctly
        for i in range(3):
            RevUpUserTokenFactory(user=self.user, token_type=token_type)

        self.assertEqual(self._token_count(), base_tc + 3)
        self.user.clean_tokens()
        self.assertEqual(self._token_count(), 0)
        ## Verify the token count of user2 is unchanged
        self.assertEqual(self._token_count(user2), 1)

    def test_get_token(self):
        token_type = RevUpUserToken.TokenTypeChoices.REGISTRATION
        base_tc = self._token_count(tt=token_type)

        # Generate a few tokens and verify they are cleaned correctly.
        dts = [(datetime.datetime.utcnow().replace(microsecond=0) -
                datetime.timedelta(days=i)) for i in range(3)]
        for dt in dts:
            RevUpUserTokenFactory(user=self.user, token_type=token_type,
                                  utc_created=dt)

        self.assertEqual(self._token_count(tt=token_type), base_tc + 3)
        t = self.user.get_token()
        ## Verify extra tokens are cleaned
        self.assertEqual(RevUpUserToken.objects.get(user=self.user,
                                                    token_type=token_type), t)
        # Verify the most recent token is selected
        self.assertGreater(dts[0], dts[1])
        self.assertEqual(t.utc_created, dts[0])

    def test_token_expires(self):
        token_type = RevUpUserToken.TokenTypeChoices.REGISTRATION
        dt = datetime.datetime.utcnow() - datetime.timedelta(
            days=settings.REVUP_INVITE_EXPIRATION_DAYS)
        RevUpUserTokenFactory(user=self.user, token_type=token_type,
                              utc_created=dt)
        self.assertEqual(self.user.get_token(), None)

    def test_get_or_create_inactive(self):
        ## Verify new email creates new user
        email = random_email().lower()
        user, created = RevUpUser.get_or_create_inactive(email)
        self.assertNotEqual(user.id, None)
        self.assertEqual(user.email, email)
        self.assertEqual(user.username, email)
        self.assertFalse(user.is_active)
        self.assertTrue(created)

        ## Verify existing email returns the same user
        user2, created = RevUpUser.get_or_create_inactive(email)
        self.assertEqual(user, user2)
        self.assertFalse(created)

        ## Verify existing email with different case returns the same user
        uemail = email.upper()
        self.assertNotEqual(email, uemail)
        user2, created = RevUpUser.get_or_create_inactive(uemail)
        self.assertEqual(user, user2)
        self.assertFalse(created)

        ## Verify safe=False doesn't save the user
        user, created = RevUpUser.get_or_create_inactive(random_email(),
                                                         save=False)
        self.assertEqual(user.id, None)
        self.assertTrue(created)

        ## Verify the case of long emails
        long_email = "thisemailhasmorethanthirty6haracters@revup.com"
        user, created = RevUpUser.get_or_create_inactive(long_email)
        self.assertIn(long_email[:26], user.username)
        self.assertNotIn(long_email[:27], user.username)
        self.assertEqual(user.email, long_email)
        self.assertTrue(created)
        # Verify the same user is not created again
        user, created = RevUpUser.get_or_create_inactive(long_email)
        self.assertIn(long_email[:26], user.username)
        self.assertNotIn(long_email[:27], user.username)
        self.assertEqual(user.email, long_email)
        self.assertFalse(created)

    def test_lead_fundraiser(self):
        event = self.user.events.first()
        eventView = EventProspectsViewSet()
        prospect = ProspectFactory(event=event)
        request = HttpRequest()
        request.user = self.user

        # Not lead fundraiser, can't edit other attendees' data
        self.assertEqual(False, self.user.is_lead_fundraiser(event))
        request.user.current_seat = SeatFactory.build(permissions=[])
        permission = eventView._get_permission_level(request, prospect)
        self.assertEqual(0, permission)

        # Lead fundraiser, can edit other attendees' data
        self.user.set_lead_fundraiser(event)
        self.assertEqual(True, self.user.is_lead_fundraiser(event))
        permission = eventView._get_permission_level(request, prospect)
        self.assertEqual(1, permission)

        # Not lead fundraiser, can't edit other attendees' data
        self.user.unset_lead_fundraiser(event)
        self.assertEqual(False, self.user.is_lead_fundraiser(event))
        permission = eventView._get_permission_level(request, prospect)
        self.assertEqual(0, permission)

    def test_event_queue(self):
        # Mock celery task
        with mock.patch("frontend.apps.contact.tasks.social_auth_import_task"
                        ".apply_async") as start_task:

            # Start first import
            social = UserSocialAuth()
            social.provider = 'google-oauth2'
            social.uid = 'test@test.com'
            social.user_id = self.user.id
            social.save()
            start_contact_import(self.user, social, None)

            # Verify import starts after a short pause for race condition
            start_task.assert_called_with(args=[self.user.id, social.id,
                                                None, None],
                                          countdown=3,
                                          task_id=mock.ANY)

            # Add pending task to user
            task = RevUpTask()
            task.social = social
            task.pending = lambda: True
            task.task_type = RevUpTask.TaskType.GMAIL_IMPORT
            task.user = self.user
            task.save()

            # Second import with same social auth shouldn't start
            start_task.reset_mock()
            start_contact_import(self.user, social, None)
            self.assertEqual(0, start_task.call_count)

            start_task.reset_mock()
            social2 = UserSocialAuth()
            social2.provider = 'google-oauth2'
            social2.uid = 'other@test.com'
            social2.user_id = self.user.id
            social2.save()
            test_feature_flags = {"test-feature-flags": True}

            with ExitStack() as es:
                es.enter_context(mock.patch("frontend.apps.contact.task_utils.RevUpTask.started",mock.MagicMock(return_value=True)))
                es.enter_context(mock.patch("frontend.apps.contact.task_utils.RevUpTask.running",mock.MagicMock(return_value=True)))
                start_contact_import(self.user, social2, None,
                                     test_feature_flags)
                self.assertEqual(start_task.call_args[1]['args'],
                             [self.user.id, social2.id, None,
                              test_feature_flags])

    def test_register_user(self):
        self.user.is_active = False
        self.user.accepted_tos = None
        self.user.save()

        # Register new user
        user_data = {
            'email': self.user.email,
            'first_name': 'John',
            'last_name': 'Smith',
            'phone': '555-555-5555',
            'street_1': '1 Market St.',
            'street_2': '',
            'city': 'San Francisco',
            'state': 'CA',
            'zip': '94105',
            'password': 'bfgWe934',
            'password2': 'bfgWe934',
        }
        ## Verify open registration no-longer works
        url = reverse('account_register')
        response = self.client.get(url)
        self.assertContains(response, "Self-registration is now closed")
        response = self.client.post(url, user_data)
        self.assertContains(response, "Self-registration is now closed")

        # Create a token for the user and try again
        token = self.user.generate_token()

        ## Verify registration success
        response = self.client.post(
            reverse('finish_account_register', args=(token.token,)),
            data=user_data, follow=True)
        self.assertContains(response,
                            '<title>Home - RevUp - Terms of Service</title>')

        # Verify user fields
        user_data.pop('password')
        user_data.pop('password2')
        user = RevUpUser.objects.get(email=self.user.email)
        for key in user_data:
            self.assertEqual(user_data[key], getattr(user, key))
        self.assertTrue(user.is_active)

    def test_is_account_admin(self):
        # Sanitize the user's seats
        self.user.seats.all().delete()
        seat1 = SeatFactory(user=self.user, permissions=[])
        self.assertEqual(list(self.user.seats.all()), [seat1])

        # Verify simple negative cases
        self.assertEqual(seat1.permissions, [])
        self.assertFalse(self.user.is_account_admin())

        seat1.permissions = Seat.FundraiserPermissions.all()
        self.assertFalse(self.user.is_account_admin())

        # Add another non-admin seat
        seat2 = SeatFactory(user=self.user,
                            permissions=Seat.FundraiserPermissions.all())
        found_seats = list(self.user.seats.all())
        self.assertTrue(seat1 in found_seats)
        self.assertTrue(seat2 in found_seats)
        self.assertEqual(len(found_seats), 2)
        self.assertFalse(self.user.is_account_admin())

        # Make the seat1 an admin and test positive case
        seat1.permissions += Seat.AdminPermissions.all()
        seat1.save()
        self.assertTrue(self.user.is_account_admin())

        ## Verify case with specific accounts
        self.assertTrue(self.user.is_account_admin(account=seat1.account))
        self.assertFalse(self.user.is_account_admin(account=seat2.account))

    def test_case_insensitive_email(self):
        # Verify trying to create user with same email (different case) fails
        email = random_email().lower()
        user = RevUpUserFactory(email=email)
        self.assertIsNotNone(user.id)

        self.assertRaisesRegex(IntegrityError, "duplicate key",
                                RevUpUserFactory, email=email.upper())

    def test_import_config(self):
        ImportConfig.objects.get_or_create(title="Default Import Config")
        # Verify the import config is set by default
        u = RevUpUserFactory.build(import_config=None)
        self.assertIsNone(u.import_config)
        u.save()
        default = ImportConfig.objects.get(title="Default Import Config")
        u = RevUpUser.objects.get(id=u.id)
        self.assertEqual(u.import_config, default)

        # Verify defined import config is not replaced
        config = ImportConfigFactory()
        u = RevUpUserFactory.build(import_config=config)
        self.assertEqual(u.import_config, config)
        u.save()
        u = RevUpUser.objects.get(id=u.id)
        self.assertNotEqual(u.import_config, default)
        self.assertEqual(u.import_config, config)


class RevUpUserViewSetTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        super(RevUpUserViewSetTests, self).setUp(login_now=False)
        self.url = reverse('user_api-list')
        self.url2 = reverse('user_api-detail', args=(self.user.id,))

    def test_permissions(self):
        ## Verify any unauthenticated user is rejected
        result = self.client.get(self.url)
        self.assertNoCredentials(result)
        result = self.client.get(self.url2)
        self.assertNoCredentials(result)

        # Login the user
        self._login(self.user)
        # Verify user is not an admin, or that would invalidate the test.
        self.assertFalse(self.user.is_admin)

        ## Verify a standard user is rejected from viewing all users
        result = self.client.get(self.url)
        self.assertNoPermission(result)

        ## Verify standard user can view themselves
        result = self.client.get(self.url2)
        self.assertContains(result, '"id":{}'.format(self.user.id))
        self.assertContains(result,
                            '"first_name":"{}"'.format(self.user.first_name))

        ## Verify standard user cannot view other users
        user2 = RevUpUserFactory()
        url_u2 = reverse('user_api-detail', args=(user2.id,))
        result = self.client.get(url_u2)
        self.assertNoPermission(result)

        #### Verify admin users have all privileges
        # Make the user an admin and try list again
        self.user.is_staff = True
        self.user.save()
        ## Verify admin user is allowed to see the user list
        result = self.client.get(self.url)
        self.assertContains(result, '"id":{}'.format(self.user.id))
        self.assertContains(result,
                            '"first_name":"{}"'.format(self.user.first_name))

        ## Verify admin user is allowed to see any individual user
        result = self.client.get(url_u2)
        self.assertContains(result, '"id":{}'.format(user2.id))
        self.assertContains(result,
                            '"first_name":"{}"'.format(user2.first_name))

    def test_update_user(self):
        """
        Ensure we can update the user in the API.
        """
        # log in the user
        response = self._login(self.user)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)

        # Sanity check to make sure the value(s) change
        user = RevUpUser.objects.get(id=self.user.id)
        self.assertNotEqual(user.first_name, 'First')
        self.assertNotEqual(user.phone, '111-111-1111')

        # Use the user api to update the user information
        url = reverse('user_api-detail', args=(self.user.id,))
        data = {'first_name': 'First',
                'last_name': 'Last',
                'phone': '111-111-1111',
                'street_1': 'Street 1 Ave',
                'street_2': 'Street 2 Ave',
                'city': 'Portland',
                'state': 'OR',
                'zip': '97209'}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Verify all the user data was updated
        user = RevUpUser.objects.get(id=self.user.id)
        self.assertEqual(user.first_name, 'First')
        self.assertEqual(user.last_name, 'Last')
        self.assertEqual(user.phone, '111-111-1111')
        self.assertEqual(user.street_1, 'Street 1 Ave')
        self.assertEqual(user.street_2, 'Street 2 Ave')
        self.assertEqual(user.city, 'Portland')
        self.assertEqual(user.state, 'OR')
        self.assertEqual(user.zip, '97209')

    def test_cannot_update_password(self):
        """
        Ensure the user cannot update the password.
        """
        # Log in the user.
        response = self._login(self.user)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)

        # Sanity check, just make sure the password is different from what
        # we are trying to update it to.
        self.user.refresh_from_db()
        self.assertNotEqual(self.user.password, 'Update')
        password = self.user.password

        # Use the user api to update the user information
        url = reverse('user_api-detail', args=(self.user.id,))
        data = {'first_name': 'Update',
                'last_name': 'Last',
                'phone': '111-111-1111',
                'password': 'Update'}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Verify that this update to password is not allowed.
        self.user.refresh_from_db()
        self.assertEqual(self.user.password, password)

    def test_cannot_update_is_superuser(self):
        """
        Ensure the user cannot update the is_superuser.
        """
        # Log in the user.
        response = self._login(self.user)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)

        # Sanity check, store the current value for is_superuser.
        self.user.refresh_from_db()
        is_superuser = self.user.is_superuser

        # Use the user api to update the user information
        url = reverse('user_api-detail', args=(self.user.id,))
        data = {'first_name': 'Update',
                'last_name': 'Last',
                'phone': '111-111-1111',
                'is_superuser': not is_superuser}
        response = self.client.put(url, data, format='json')

        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

        # Verify that this update to password is not allowed.
        self.user.refresh_from_db()
        self.assertNotEqual(self.user.is_superuser, not is_superuser)

    def test_cannot_update_is_staff(self):
        """
        Ensure the user cannot update the is_staff.
        """
        # Log in the user.
        response = self._login(self.user)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)

        # Sanity check, store the current value for is_staff.
        self.user.refresh_from_db()
        is_staff = self.user.is_staff

        # Use the user api to update the user information
        url = reverse('user_api-detail', args=(self.user.id,))
        data = {'first_name': 'Update',
                'last_name': 'Last',
                'phone': '111-111-1111',
                'is_staff': not is_staff}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Verify that this update to password is not allowed.
        self.user.refresh_from_db()
        self.assertNotEqual(self.user.is_staff, not is_staff)

    def test_cannot_update_is_active(self):
        """
        Ensure the user cannot update the is_active.
        """
        # Log in the user.
        response = self._login(self.user)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)

        # Sanity check, store the current value for is_active.
        self.user.refresh_from_db()
        is_active = self.user.is_active

        # Use the user api to update the user information
        url = reverse('user_api-detail', args=(self.user.id,))
        data = {'first_name': 'Update',
                'last_name': 'Last',
                'phone': '111-111-1111',
                'is_active': not is_active}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Verify that this update to password is not allowed.
        self.user.refresh_from_db()
        self.assertNotEqual(self.user.is_active, not is_active)

    def test_update_other_user(self):
        """
        Ensure the user cannot update another user in the API.
        """
        # Log in the user.
        response = self._login(self.user)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)

        # Find another user and try to update their information.
        other_user = RevUpUser.objects.filter(id__ne=self.user.id).first()
        self.assertNotEqual(other_user.first_name, 'Update')

        # Use the user api to update the user information
        url = reverse('user_api-detail', args=(other_user.id,))
        data = {'first_name': 'Update',
                'last_name': 'Last',
                'phone': '111-111-1111',
                'street_1': 'Street 1 Ave',
                'street_2': 'Street 2 Ave',
                'city': 'Portland',
                'state': 'OR',
                'zip': '97209'}
        response = self.client.put(url, data, format='json')

        # Verify that this update is not allowed.
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # Verify the update was not successful to this other user.
        user = RevUpUser.objects.get(id=other_user.id)
        self.assertNotEqual(user.first_name, 'Update')

    def test_update_with_invalid_data(self):
        # log in the user
        response = self._login(self.user)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)

        url = reverse('user_api-detail', args=(self.user.id,))

        # Check that a validation error is returned if the phone is not in
        # the correct format
        data = {'phone': '111-'}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code,
                         status.HTTP_400_BAD_REQUEST)

        # Check validation for empty fields
        data = {'first_name': ''}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code,
                         status.HTTP_400_BAD_REQUEST)
        data = {'last_name': ''}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code,
                         status.HTTP_400_BAD_REQUEST)
        data = {'phone': ''}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code,
                         status.HTTP_400_BAD_REQUEST)


class EventUsersViewSetTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        self.campaign = PoliticalCampaignFactory()
        self.event = EventFactory(account=self.campaign)
        super(EventUsersViewSetTests, self).setUp(
            login_now=False, campaigns=[self.campaign], events=[self.event])
        self.url = reverse('event_users_api-list',
                           args=(self.campaign.id, self.event.id))
        self.url2 = reverse('event_users_api-detail',
                            args=(self.campaign.id, self.event.id,
                                  self.user.id))

        # Add another user so we can see more than one user in the results
        self.user2 = RevUpUserFactory(events=[self.event])
        SeatFactory(user=self.user2, account=self.campaign)
        # Add another from a different event
        self.user3 = RevUpUserFactory()
        SeatFactory(user=self.user3, account=self.campaign)
        self.seat = self.user.seats.first()

    def test_permissions(self):
        ## Verify any unauthenticated user is rejected
        result = self.client.get(self.url)
        self.assertNoCredentials(result)
        result = self.client.post(self.url, data={"abc": "123"})
        self.assertNoCredentials(result)

        # Login the user
        self._login(self.user)
        # Verify user is not an admin, or that would invalidate the test.
        self.assertFalse(self.user.is_admin)
        self.assertFalse(bool(self.user.is_account_admin()))

        ## Verify a standard user is rejected from viewing all users
        result = self.client.get(self.url)
        self.assertNoPermission(result)
        ## Verify a standard user is rejected from adding new users
        result = self.client.post(self.url, data={"abc": "123"})
        self.assertNoPermission(result)
        ## Verify a standard user is rejected from deleting event users
        result = self.client.delete(self.url2)
        self.assertNoPermission(result)

        # Add the View Fundraiser permission to the user
        self._add_admin_group(permissions=AdminPermissions.VIEW_FUNDRAISERS)
        ## Verify the API now accepts the user for GET
        result = self.client.get(self.url)
        self.assertContains(result, '"id":{}'.format(self.user.id))
        self.assertContains(result,
                            '"first_name":"{}"'.format(self.user.first_name))
        self.assertContains(result, '"id":{}'.format(self.user2.id))
        self.assertContains(result,
                            '"first_name":"{}"'.format(self.user2.first_name))
        ## Verify the user from another campaign is not in the results
        self.assertNotContains(result, '"id":{}'.format(self.user3.id))

        ## Verify the API still doesn't accepts user creation
        result = self.client.post(self.url, data={"abc": "123"})
        self.assertNoPermission(result)

        ## Add Mod fundraiser permission and verify creation
        self._add_admin_group(permissions=AdminPermissions.MOD_FUNDRAISERS)
        # Although this is an error state, it means the permissions were good
        result = self.client.post(self.url, data={"abc": "123"})
        self.assertContains(result, "'create' expects a 'user' field",
                            status_code=500)

        ## Verify the API now accepts user deletion
        self.assertIn(self.event, self.user.events.all())
        result = self.client.delete(self.url2)
        self.assertNotIn(self.event, self.user.events.all())

        ## Verify query where campaign and event don't match is rejected
        bad_url = reverse('event_users_api-list',
                          args=(self.campaign.id, EventFactory().id))
        result = self.client.get(bad_url)
        self.assertNoPermission(result)

    def test_create(self):
        self._login_and_add_admin_group(
            permissions=AdminPermissions.MOD_FUNDRAISERS)

        ## Test the error cases
        # Verify the api requires a user ID
        result = self.client.post(self.url, data={"abc": "123"})
        self.assertContains(result, "'create' expects a 'user' field",
                            status_code=500)
        # Verify user must be real
        result = self.client.post(
            self.url, data={"user": self.user2.id + 1000})
        self.assertContains(result, "does not exist", status_code=500)
        # Verify user must have campaign
        self.user3.seats.all().delete()
        result = self.client.post(self.url, data={"user": self.user3.id})
        self.assertContains(result, "not a member of the given campaign",
                            status_code=500)

        ## Verify positive case
        self.user3.seats.set([SeatFactory(account=self.seat.account)])
        # Verify event is not already in the user
        self.assertNotIn(self.event, self.user3.events.all())
        now = datetime.datetime.utcnow()
        result = self.client.post(self.url, data={"user": self.user3.id})
        self.assertContains(result, "")  # Verify status code
        self.assertIn(self.event, self.user3.events.all())

    def test_destroy(self):
        self._login_and_add_admin_group(permissions=[
            AdminPermissions.MOD_FUNDRAISERS])

        ## Verify event is removed from user
        self.assertIn(self.event, self.user.events.all())
        result = self.client.delete(self.url2)
        self.assertNotIn(self.event, self.user.events.all())

    def test_update(self):
        self._login_and_add_admin_group(permissions=[
            AdminPermissions.MOD_FUNDRAISERS])

        # # Verify membership is updated correctly
        self.assertIn(self.event, self.user.events.all())
        self.assertFalse(UserEventLink.objects.get(user=self.user,
                                                   event=self.event).is_lead)
        result = self.client.put(self.url2, data='{"is_lead": true}',
                                 content_type='application/json')
        self.assertContains(result, "")
        self.assertTrue(UserEventLink.objects.get(user=self.user,
                                                  event=self.event).is_lead)
        result = self.client.put(self.url2, data='{"is_lead": false}',
                                 content_type='application/json')
        self.assertContains(result, "")
        self.assertFalse(UserEventLink.objects.get(user=self.user,
                                                  event=self.event).is_lead)

    def test_serializer(self):
        self._login_and_add_admin_group(permissions=[
            AdminPermissions.MOD_FUNDRAISERS])

        ## Verify the format of the list response contains only the standard
        ## fields
        fields = MINIMAL_FIELDS + ('is_lead',)
        result = self.client.get(self.url)
        self.verify_serializer(result, fields)

        ## Verify the detail view is the same
        result = self.client.get(self.url2)
        self.verify_serializer(result, fields)


class RegressionTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        super(RegressionTests, self).setUp(login_now=False, **kwargs)
        ImportRecordFactory(user=self.user)

    def test_password(self):
        # Use a bad password and verify login fails
        response = self._login(self.user, "badpassword")
        self.assertContains(response, "email address or password is incorrect",
                            status_code=401)

        # Verify user attempting to view a login required page is
        # redirected back to login
        response = self.client.get(reverse('index'))
        self.assertRedirects(response, "{}?next=/".format(
            reverse("account_login")))

        # Use a good password and verify login is successful
        response = self._login(self.user)
        self.assertRedirects(response, reverse('index'))

    def test_case_insensitive_email(self):
        self.user.email = self.user.email.upper()
        response = self._login(self.user)
        self.assertRedirects(response, reverse('index'))

    def test_login_redirect(self):
        ## Test REVUP-1034
        # Send a user, who is already logged in, to the log in page and
        # verify they are redirected to the correct place
        self._login(self.user)
        self.assertTrue(self.user.is_authenticated)

        # Verify user without "next" argument is sent to "index"
        response = self.client.get(reverse('account_login'))
        self.assertRedirects(response, reverse("index"))
        # Verify user with "next" is sent to "next", not index
        url = reverse("account_edit_user")
        response = self.client.get("{}?next={}".format(
            reverse('account_login'), url))
        self.assertRedirects(response, url)


class AnalysisViewUserSettingsViewSetTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        super(AnalysisViewUserSettingsViewSetTests, self).setUp(**kwargs)
        self.analysis_profile = AnalysisProfileFactory()
        self.url = reverse('analysis_view_user_settings_api-list',
                           args=(self.user.id,))

    def test_create(self):
        test_data = {'display_maxed_donors': True, 'political_colors': False}
        record = {'analysis_profile': self.analysis_profile.id,
                  'data': test_data}

        # Control test, make sure no settings exist yet
        self.assertFalse(self.user.analysis_view_user_settings.filter(
            analysis_profile=self.analysis_profile).exists())

        response = self.client.post(self.url, data=json.dumps(record),
                                    content_type="application/json")

        self.assertEqual(response.status_code, 201)

        self.assertTrue(self.user.analysis_view_user_settings.filter(
            analysis_profile=self.analysis_profile).exists())

        user_settings = self.user.analysis_view_user_settings.get(
            analysis_profile=self.analysis_profile)

        self.assertEqual(user_settings.data, test_data)

        # Ensure creating a duplicate fails
        response = self.client.post(self.url, data=json.dumps(record),
                                    content_type="application/json")

        self.assertContains(
            response,
            'The fields user, analysis_profile must make a unique set.',
            status_code=400)

    def test_update(self):
        old_data = {'display_maxed_donors': True, 'political_colors': False}
        user_settings = AnalysisViewUserSettings.objects.create(
            user=self.user, analysis_profile=self.analysis_profile,
            data=old_data)

        test_data = old_data.copy()
        test_data['political_colors'] = not test_data['political_colors']
        record = {'data': test_data}

        url = reverse('analysis_view_user_settings_api-detail',
                           args=(self.user.id, user_settings.id))

        response = self.client.put(url, data=json.dumps(record),
                                    content_type="application/json")

        self.assertEqual(response.status_code, 200)

        user_settings = self.user.analysis_view_user_settings.get(
            analysis_profile=self.analysis_profile)

        self.assertNotEqual(user_settings.data, old_data)
        self.assertEqual(user_settings.data, test_data)

    def test_permissions(self):
        user2 = RevUpUserFactory()
        analysis_profile2 = AnalysisProfileFactory()

        test_data = {'display_maxed_donors': True, 'political_colors': False}
        record = {'analysis_profile': self.analysis_profile.id,
                  'data': test_data}

        user_settings = AnalysisViewUserSettings.objects.create(
            user=user2, analysis_profile=analysis_profile2,
            data=test_data)

        self.client.logout()

        url2 = reverse('analysis_view_user_settings_api-list',
                           args=(user2.id,))

        detailurl2 = reverse('analysis_view_user_settings_api-detail',
                           args=(user2.id, user_settings.id))

        ## Verify any unauthorized user is rejected
        # Listing setting records should fail
        result = self.client.get(url2)
        self.assertNoCredentials(result)

        # Getting details on a specific setting should fail
        result = self.client.get(detailurl2)
        self.assertNoCredentials(result)

        # Creating a new record should fail
        result = self.client.post(url2, data=json.dumps(record),
                                  content_type="application/json")
        self.assertNoCredentials(result)

        # Login the user
        self._login(self.user)
        # Verify user is not an admin, or that would invalidate the test.
        self.assertFalse(self.user.is_admin)
        self.assertFalse(self.user.is_account_admin())

        # Verify a standard user is rejected from accessing another user's
        # settings
        result = self.client.get(url2)
        self.assertNoPermission(result)
        result = self.client.get(detailurl2)
        self.assertNoPermission(result)

        # Verify one user is not allowed to create settings for another user
        result = self.client.post(url2, data=json.dumps(record),
                                  content_type="application/json")
        self.assertNoPermission(result)

        # Verify one user is not allowed to update settings for another user
        result = self.client.put(detailurl2, data=json.dumps(record),
                                  content_type="application/json")
        self.assertNoPermission(result)
