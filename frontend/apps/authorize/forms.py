from collections import OrderedDict
import re

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

from django import forms
from django.conf import settings
from django.contrib.auth.forms import (PasswordResetForm, SetPasswordForm,
                                       PasswordChangeForm)
from localflavor.us.forms import (USStateField, USStateSelect,
                                         USZipCodeField)
from passwords.fields import PasswordField as PasswordField_
from passwords.validators import (validate_length, common_sequences,
                                  dictionary_words)

from frontend.apps.authorize.models import RevUpUser
from frontend.libs.utils.form_utils import (USPhoneNumberField,
                                            FlexibleComplexityValidator)


class PasswordField(PasswordField_):
    default_validators = [
        validate_length,
        common_sequences,
        dictionary_words,
        FlexibleComplexityValidator(settings.PASSWORD_COMPLEXITY,
                                    settings.PASSWORD_COMPLEXITY_FLEXIBILITY)]


class RevUpUserForm(forms.Form):
    first_name = forms.CharField(
        widget=forms.TextInput(attrs={'required': ''}),
        required=True, label='First Name', max_length=64)
    last_name = forms.CharField(widget=forms.TextInput(attrs={'required': ''}),
                                required=True, label='Last Name', max_length=150)
    phone = USPhoneNumberField(widget=forms.TextInput(attrs={'required': ''}),
                               required=True, label='Phone Number')
    street_1 = forms.CharField(widget=forms.TextInput(attrs={'required': ''}),
                               required=True, label='Street Address 1')
    street_2 = forms.CharField(widget=forms.TextInput(),
                               required=False, label='Street Address 2')
    city = forms.CharField(widget=forms.TextInput(attrs={'required': ''}),
                           required=True, label='City')
    state = USStateField(widget=USStateSelect())
    zip = USZipCodeField(widget=forms.TextInput(attrs={'required': ''}),
                         required=True, label='Zip')


class EmailRegistrationForm(RevUpUserForm):
    control_attributes = {'class': 'form-control'}
    email = forms.EmailField(widget=forms.TextInput(attrs={'required': ''}),
                             required=True, label='Email Address')
    password = PasswordField(required=True, label='Password')
    password2 = forms.CharField(
        widget=forms.PasswordInput(attrs={'required': ''}),
        required=True, label='Re-Enter Password')

    def __init__(self, instance=None, *args, **kwargs):
        super(EmailRegistrationForm, self).__init__(*args, **kwargs)
        for field_name in self.fields:
            field = self.fields.get(field_name)
            if field:
                new_attrs = field.widget.attrs.copy()
                if type(field.widget) in (forms.TextInput, forms.DateInput,
                                          forms.PasswordInput):
                    new_attrs.update({'class': 'form-control',
                                      'placeholder': field.label})
                else:
                    new_attrs.update({'class': 'form-control'})
                field.widget.attrs = new_attrs

        self.instance = instance
        if instance:
            self.fields['first_name'].initial = instance.first_name
            self.fields['last_name'].initial = instance.last_name
            self.fields['email'].initial = instance.email
            self.fields['email'].widget.attrs['readonly'] = True

        # Move the email field to the top of the form
        new_fields = [('email', self.fields['email'])] + [
            (k, v) for k, v in self.fields.items() if k != 'email']
        self.fields = OrderedDict(new_fields)

    def clean_email(self):
        email = self.cleaned_data['email'].strip()
        # Verify the user didn't hack a change in their email
        if self.instance:
            if email != self.instance.email:
                raise forms.ValidationError("This email is already defined. "
                                            "It cannot be changed.")
        else:
            try:
                RevUpUser.objects.get(email__iexact=email)
            except RevUpUser.DoesNotExist:
                return email.lower()
            else:
                raise forms.ValidationError('Someone has already been '
                                            'registered with that email.')

    def clean_password2(self):
        password = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('password2')

        if not password and 'password' in self.errors:
            # Given password is invalid, no need to raise an error here too
            return None
        elif not password2:
            raise forms.ValidationError("You must confirm your password")
        elif password != password2:
            raise forms.ValidationError("Your passwords do not match")
        else:
            return password2


class PasswordResetFormWrapper(PasswordResetForm):
    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'Submit'))
        super(PasswordResetFormWrapper, self).__init__(*args, **kwargs)


class SetPasswordFormWrapper(SetPasswordForm):
    new_password1 = PasswordField(required=True, label="New password")

    def __init__(self, user, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'Submit'))
        self.user = user
        super(SetPasswordFormWrapper, self).__init__(user, *args, **kwargs)

    def save(self, commit=True):
        self.user.password_expiration_date = None
        return super(SetPasswordFormWrapper, self).save(commit=commit)


class PasswordChangeFormWrapper(PasswordChangeForm):
    new_password1 = PasswordField(required=True, label="New password")

    def __init__(self, user, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'Submit'))
        self.user = user
        super(PasswordChangeFormWrapper, self).__init__(user, *args, **kwargs)

    def save(self, commit=True):
        self.user.password_expiration_date = None
        return super(PasswordChangeFormWrapper, self).save(commit=commit)
