# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('authorize', '0016_revupuser_events_temp'),
    ]

    operations = [
        migrations.AlterField(
            model_name='revuptask',
            name='social',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='social_django.UserSocialAuth', null=True),
            preserve_default=True,
        ),
    ]
