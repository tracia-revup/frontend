# -*- coding: utf-8 -*-


from django.db import models, migrations
import audit_log.models.fields
import jsonfield.fields
import django_extensions.db.fields
import django.db.models.deletion
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0026_add_municipal_entities'),
        ('authorize', '0018_make_tasks_revokable'),
    ]

    operations = [
        migrations.CreateModel(
            name='AnalysisViewSettingSet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('module_name', models.CharField(max_length=256, blank=True)),
                ('class_name', models.CharField(max_length=256, blank=True)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_authorize_analysisviewsettingset_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('modified_by', audit_log.models.fields.LastUserField(related_name='modified_authorize_analysisviewsettingset_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by')),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AnalysisViewUserSettings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data', jsonfield.fields.JSONField()),
                ('analysis_profile', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, to='analysis.AnalysisProfile')),
                ('user', models.ForeignKey(related_name='analysis_view_user_settings', to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='analysisviewusersettings',
            unique_together=set([('user', 'analysis_profile')]),
        ),
    ]
