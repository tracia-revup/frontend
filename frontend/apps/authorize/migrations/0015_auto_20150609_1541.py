# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0016_prospect_timestamps'),
        ('authorize', '0014_user_analysis_link'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserEventLink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_lead', models.BooleanField(default=False, help_text='Is this user a lead fundraiser?')),
                ('event', models.ForeignKey(related_name='user_links', to='campaign.Event', on_delete=django.db.models.deletion.CASCADE)),
                ('user', models.ForeignKey(related_name='event_links', to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='usereventlink',
            unique_together=set([('user', 'event')]),
        ),
        migrations.RenameField(
            model_name='revupuser',
            old_name='events',
            new_name='events_temp',
        ),
    ]
