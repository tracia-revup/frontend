# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('seat', '0002_auto_20150129_1120'),
        ('authorize', '0005_auto_20150127_1107'),
    ]

    operations = [
        migrations.AddField(
            model_name='revupuser',
            name='current_seat',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='seat.Seat', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='revupuser',
            name='current_analysis',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='analysis.Analysis', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='revupuser',
            name='current_event',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='campaign.Event', null=True),
            preserve_default=True,
        ),
    ]
