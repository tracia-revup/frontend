# -*- coding: utf-8 -*-


from django.db import models, migrations


def create_political_setting_set(apps, schema_editor):
    AnalysisProfile = apps.get_model("analysis", "AnalysisProfile")
    AnalysisViewSettingSet = apps.get_model("authorize", "AnalysisViewSettingSet")

    from frontend.apps.authorize.settingset_controllers import AcademicSettingSet
    from frontend.libs.utils.model_utils import DynamicClassModelMixin

    ap = AnalysisProfile.objects.get(title="Academic Analysis")

    # Use the Mixin model to parse the class/module name. Models generated
    # through the apps variable don't seem to have all the methods.
    dc = DynamicClassModelMixin()
    dc.dynamic_class = AcademicSettingSet

    ss, _ = AnalysisViewSettingSet.objects.get_or_create(
        title="Academic Analysis User View Settings",
        class_name=dc.class_name, module_name=dc.module_name)

    ap.analysis_view_setting_set = ss
    ap.save()


def cleanout_saved_settings(apps, schema_editor):
    """Delete any saved settings for the AcademicProfile, since they will be
      broken.
    """
    AnalysisProfile = apps.get_model("analysis", "AnalysisProfile")
    AnalysisViewUserSettings = apps.get_model("authorize",
                                              "AnalysisViewUserSettings")

    ap = AnalysisProfile.objects.get(title="Academic Analysis")
    AnalysisViewUserSettings.objects.filter(analysis_profile=ap).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0027_add_view_settings_fk'),
        ('authorize', '0025_revupuser_applied_filters'),
    ]

    operations = [
        migrations.RunPython(
            create_political_setting_set, lambda x, y: True
        ),
        migrations.RunPython(
            cleanout_saved_settings, lambda x, y: True
        ),
    ]
