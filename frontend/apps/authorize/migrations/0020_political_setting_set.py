# -*- coding: utf-8 -*-


from django.db import models, migrations


def create_political_setting_set(apps, schema_editor):
    AnalysisProfile = apps.get_model("analysis", "AnalysisProfile")
    AnalysisViewSettingSet = apps.get_model("authorize", "AnalysisViewSettingSet")

    from frontend.apps.authorize.settingset_controllers import PoliticalSettingSet
    from frontend.libs.utils.model_utils import DynamicClassModelMixin

    aps = AnalysisProfile.objects.filter(title__in=[
        'Political Candidate Analysis', 'Political Committee Analysis'],
        analysis_view_setting_set__isnull=True)

    # Use the Mixin model to parse the class/module name. Models generated
    # through the apps variable don't seem to have all the methods.
    dc = DynamicClassModelMixin()
    dc.dynamic_class = PoliticalSettingSet

    ss, _ = AnalysisViewSettingSet.objects.get_or_create(
        title="Political Analysis User View Settings",
        class_name=dc.class_name, module_name=dc.module_name)

    for ap in aps:
        ap.analysis_view_setting_set = ss
        ap.save()


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0027_add_view_settings_fk'),
        ('authorize', '0019_analysis_view_settings'),
    ]

    operations = [
        migrations.RunPython(
            create_political_setting_set, lambda x, y: True
        ),
    ]
