# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authorize', '0011_userguidemanager_academia_admin'),
    ]

    operations = [
        migrations.AlterField(
            model_name='revuptask',
            name='task_type',
            field=models.CharField(default='OT', max_length=16, db_index=True, choices=[('FB', 'Facebook Import'), ('GM', 'Gmail Import'), ('LI', 'LinkedIn Import'), ('NS', 'Network Search'), ('TW', 'Twitter Import'), ('OU', 'Outlook Import'), ('AB', 'Address Book Import'), ('OT', 'Other Import')]),
            preserve_default=True,
        ),
        # Add a case-insensitive, unique index to email field
        migrations.RunSQL("CREATE UNIQUE INDEX authorize_revupuser_email_insensitive_unique ON authorize_revupuser(lower(email))",
                          reverse_sql="DROP INDEX authorize_revupuser_email_insensitive_unique")
    ]
