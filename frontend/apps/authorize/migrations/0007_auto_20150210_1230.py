# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('authorize', '0006_auto_20150201_1358'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userguidemanager',
            name='political_campaign_admin',
            field=models.OneToOneField(related_name='guide_manager', null=True,
                                       on_delete=django.db.models.deletion.SET_NULL,
                                       blank=True,
                                       to='authorize.PoliticalCampaignAdminGuide'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='userguidemanager',
            name='political_fundraiser',
            field=models.OneToOneField(related_name='guide_manager', null=True,
                                       on_delete=django.db.models.deletion.SET_NULL,
                                       blank=True,
                                       to='authorize.PoliticalFundraiserGuide'),
            preserve_default=True,
        ),
        migrations.CreateModel(
            name='GlobalFundraiserGuide',
            fields=[
                ('userguide_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False,
                                                       to='authorize.UserGuide', on_delete=django.db.models.deletion.CASCADE)),
                ('imported_contacts', models.DateTimeField(null=True, blank=True)),
            ],
            options={
            },
            bases=('authorize.userguide',),
        ),
        migrations.RenameField(
            model_name='userguidemanager',
            old_name='political_fundraiser',
            new_name='political_campaign_fundraiser',
        ),
        migrations.RemoveField(
            model_name='politicalfundraiserguide',
            name='imported_contacts',
        ),
        migrations.AddField(
            model_name='userguidemanager',
            name='global_fundraiser',
            field=models.OneToOneField(related_name='guide_manager', null=True, on_delete=django.db.models.deletion.SET_NULL, blank=True, to='authorize.GlobalFundraiserGuide'),
            preserve_default=True,
        ),
    ]
