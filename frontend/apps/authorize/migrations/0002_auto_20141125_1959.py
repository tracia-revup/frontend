# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0002_prospect_prospectcollision'),
        ('analysis', '0001_initial'),
        ('authorize', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='revupuser',
            name='current_analysis',
            field=models.ForeignKey(blank=True, to='analysis.Analysis', null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='revupuser',
            name='current_event',
            field=models.ForeignKey(blank=True, to='campaign.Event', null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
    ]
