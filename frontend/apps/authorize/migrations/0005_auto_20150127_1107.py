# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('social_django', '__first__'),
        ('authorize', '0004_politicalcampaignadminguide_politicalfundraiserguide_userguide_userguidemanager'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='revuptask',
            name='task_message',
        ),
        migrations.RemoveField(
            model_name='revuptask',
            name='task_name',
        ),
        migrations.AddField(
            model_name='revuptask',
            name='social',
            field=models.ForeignKey(blank=True, to='social_django.UserSocialAuth', null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='revuptask',
            name='task_type',
            field=models.CharField(default='OT', max_length=16, db_index=True, choices=[('FB', 'Facebook Import'), ('GM', 'Gmail Import'), ('LI', 'LinkedIn Import'), ('NS', 'Network Search'), ('OT', 'Other Import'), ('TW', 'Twitter Import')]),
            preserve_default=True,
        ),
    ]
