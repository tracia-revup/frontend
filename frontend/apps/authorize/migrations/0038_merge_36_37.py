from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authorize', '0036_externalsignuptoken'),
        ('authorize', '0037_merge_20180731_1412'),
    ]

    operations = [
    ]
