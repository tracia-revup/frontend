# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


def add_guide_managers(apps, schema_editor):
    """Add a guide manager to each user before making the field non-nullable"""
    RevUpUser = apps.get_model("authorize", "RevUpUser")
    UserGuideManager = apps.get_model("authorize", "UserGuideManager")
    for user in RevUpUser.objects.filter(guide_manager=None):
        UserGuideManager.objects.create(user=user)


class Migration(migrations.Migration):

    dependencies = [
        ('authorize', '0003_auto_20141204_1412'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserGuide',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('enabled', models.BooleanField(default=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PoliticalFundraiserGuide',
            fields=[
                ('userguide_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='authorize.UserGuide', on_delete=django.db.models.deletion.CASCADE)),
                ('imported_contacts', models.DateTimeField(null=True, blank=True)),
                ('analyzed_network', models.DateTimeField(null=True, blank=True)),
                ('created_prospect', models.DateTimeField(null=True, blank=True)),
            ],
            options={
            },
            bases=('authorize.userguide',),
        ),
        migrations.CreateModel(
            name='PoliticalCampaignAdminGuide',
            fields=[
                ('userguide_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='authorize.UserGuide', on_delete=django.db.models.deletion.CASCADE)),
                ('created_campaign', models.DateTimeField(null=True, blank=True)),
                ('created_event', models.DateTimeField(null=True, blank=True)),
                ('invited_users', models.DateTimeField(null=True, blank=True)),
                ('added_user_to_event', models.DateTimeField(null=True, blank=True)),
            ],
            options={
            },
            bases=('authorize.userguide',),
        ),
        migrations.CreateModel(
            name='UserGuideManager',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('political_campaign_admin', models.OneToOneField(related_name='guide_manager', null=True, blank=True, to='authorize.PoliticalCampaignAdminGuide', on_delete=django.db.models.deletion.CASCADE)),
                ('political_fundraiser', models.OneToOneField(related_name='guide_manager', null=True, blank=True, to='authorize.PoliticalFundraiserGuide', on_delete=django.db.models.deletion.CASCADE)),
                ('user', models.OneToOneField(related_name='guide_manager', to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RunPython(add_guide_managers,
                             reverse_code=lambda x, y: True),
    ]
