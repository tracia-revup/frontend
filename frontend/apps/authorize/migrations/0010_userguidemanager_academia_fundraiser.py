# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('authorize', '0009_auto_20150327_1247'),
    ]

    operations = [
        migrations.AddField(
            model_name='userguidemanager',
            name='academia_fundraiser',
            field=models.OneToOneField(related_name='guide_manager', null=True, on_delete=django.db.models.deletion.SET_NULL, blank=True, to='authorize.AcademiaFundraiserGuide'),
            preserve_default=True,
        ),
    ]
