# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-08-08 15:40


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('filtering', '0005_add_result_filters'),
        ('authorize', '0024_revupuser_import_config'),
    ]

    operations = [
        migrations.AddField(
            model_name='revupuser',
            name='applied_filters',
            field=models.ManyToManyField(to='filtering.Filter'),
        ),
    ]
