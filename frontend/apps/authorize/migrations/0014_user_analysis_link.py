# -*- coding: utf-8 -*-


from django.db import models, migrations
from django.conf import settings
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0019_account_analysis_link'),
        ('analysis', '0003_new_analysis_data'),
        ('authorize', '0013_auto_20150512_0037'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserAnalysisConfigLink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_default', models.BooleanField(default=False)),
                ('is_active', models.BooleanField(default=True)),
                ('account', models.ForeignKey(related_name='+', to='campaign.Account', on_delete=django.db.models.deletion.CASCADE)),
                ('analysis_config', models.ForeignKey(related_name='+', to='analysis.AnalysisConfig', on_delete=django.db.models.deletion.CASCADE)),
                ('user', models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='revupuser',
            name='user_analysis_configs',
            field=models.ManyToManyField(related_name='+', through='authorize.UserAnalysisConfigLink', to='analysis.AnalysisConfig', blank=True),
            preserve_default=True,
        ),
    ]
