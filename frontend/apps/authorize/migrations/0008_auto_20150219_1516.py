# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('authorize', '0007_auto_20150210_1230'),
    ]

    operations = [
        migrations.CreateModel(
            name='AcademiaAdminGuide',
            fields=[
                ('userguide_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False,
                                                       to='authorize.UserGuide', on_delete=django.db.models.deletion.CASCADE)),
                ('created_event', models.DateTimeField(null=True, blank=True)),
                ('invited_users', models.DateTimeField(null=True, blank=True)),
                ('added_user_to_event', models.DateTimeField(null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('authorize.userguide', models.Model),
        ),
        migrations.CreateModel(
            name='AcademiaFundraiserGuide',
            fields=[
                ('userguide_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False,
                                                       to='authorize.UserGuide', on_delete=django.db.models.deletion.CASCADE)),
                ('analyzed_network', models.DateTimeField(null=True, blank=True)),
                ('created_prospect', models.DateTimeField(null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('authorize.userguide', models.Model),
        ),
        migrations.RemoveField(
            model_name='politicalcampaignadminguide',
            name='created_campaign',
        ),
    ]
