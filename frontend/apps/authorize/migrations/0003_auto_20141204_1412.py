# -*- coding: utf-8 -*-


from django.db import models, migrations



class Migration(migrations.Migration):

    dependencies = [
        ('authorize', '0002_auto_20141125_1959'),
    ]

    operations = [
        migrations.RunSQL(
            "CREATE UNIQUE INDEX authorize_revupuser_email_key ON authorize_revupuser(email);",
            reverse_sql="DROP INDEX IF EXISTS authorize_revupuser_email_key"
        )
    ]
