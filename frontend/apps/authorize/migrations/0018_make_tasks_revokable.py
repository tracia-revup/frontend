# -*- coding: utf-8 -*-


from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('authorize', '0017_auto_20150730_1659'),
    ]

    operations = [
        migrations.AddField(
            model_name='revuptask',
            name='revoked',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='revuptask',
            name='utc_created',
            field=models.DateTimeField(default=datetime.datetime.utcnow),
            preserve_default=True,
        ),
    ]
