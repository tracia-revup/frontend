# -*- coding: utf-8 -*-


from django.db import models, migrations


def migrate_events(apps, schema_editor):
    RevUpUser = apps.get_model("authorize", "RevUpUser")
    UserEventLink = apps.get_model("authorize", "UserEventLink")

    for user in RevUpUser.objects.all():
        leads = set(e.id for e in user.lead_fundraiser_for.all())
        for event in user.events_temp.all():
            UserEventLink.objects.create(user=user, event=event,
                                         is_lead=event.id in leads)


def reverse_migrate_events(apps, schema_editor):
    RevUpUser = apps.get_model("authorize", "RevUpUser")
    UserEventLink = apps.get_model("authorize", "UserEventLink")

    for user in RevUpUser.objects.all():
        for event_link in UserEventLink.objects.filter(user=user):
            user.events_temp.add(event_link.event)
            if event_link.is_lead:
                user.lead_fundraiser_for.add(event_link.event)


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0016_prospect_timestamps'),
        ('authorize', '0015_auto_20150609_1541'),
    ]

    operations = [
        migrations.AddField(
            model_name='revupuser',
            name='events',
            field=models.ManyToManyField(related_name='+',
                                         through='authorize.UserEventLink',
                                         to='campaign.Event'),
            preserve_default=True,
        ),
        migrations.RunPython(migrate_events,
                             reverse_code=reverse_migrate_events
        ),
        migrations.RemoveField(
            model_name='revupuser',
            name='lead_fundraiser_for',
        ),
        migrations.RemoveField(
            model_name='revupuser',
            name='events_temp',
        ),
    ]

