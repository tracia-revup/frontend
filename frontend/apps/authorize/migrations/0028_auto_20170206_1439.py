# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-02-06 14:39


from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('authorize', '0027_auto_20161118_1623'),
    ]

    operations = [
        migrations.AddField(
            model_name='revuptask',
            name='_csource_content_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType'),
        ),
        migrations.AddField(
            model_name='revuptask',
            name='_csource_id',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]
