# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authorize', '0020_political_setting_set'),
    ]

    operations = [
        migrations.AddField(
            model_name='revupuser',
            name='password_expiration_date',
            field=models.DateField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
