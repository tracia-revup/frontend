# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2018-03-12 08:50


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authorize', '0031_auto_20180118_2013'),
    ]

    operations = [
        migrations.AlterField(
            model_name='revuptask',
            name='task_type',
            field=models.CharField(choices=[('NS', 'Analysis'), ('FB', 'Facebook Import'), ('GM', 'Gmail Import'), ('LI', 'LinkedIn Import'), ('TW', 'Twitter Import'), ('OU', 'Outlook Import'), ('AB', 'Address Book Import'), ('CV', 'CSV Import'), ('IP', 'iPhone Import'), ('AD', 'Android Import'), ('OT', 'Other Import'), ('CC', 'Clone Contacts to Account'), ('RE', 'Ranking Export'), ('PE', 'Prospects Export'), ('CE', 'Call Group Export'), ('GC', 'Generate Call Sheets'), ('GG', 'Generate Call Sheets from Group'), ('GL', 'Generate Call Group from list'), ('DC', 'Delete Contacts')], db_index=True, default='OT', max_length=16),
        ),
    ]
