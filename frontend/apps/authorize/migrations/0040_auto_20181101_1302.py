from django.db import migrations


def add_groups_and_permissions(apps, schema_editor):
    """ Adds the groups and permissions for the staff admin tool. """
    Permission = apps.get_model("auth", "Permission")
    Group = apps.get_model("auth", "Group")
    ContentType = apps.get_model('contenttypes', 'ContentType')
    RevUpUser = apps.get_model('authorize', 'RevUpUser')

    cs_permissions = []
    sales_permissions = []
    permission_types = [('User Management', 'can_manage_users'),
                        ('Database', 'can_manage_database'),
                        ('Site Models', 'can_manage_site_models'),
                        ('Skinning Tools', 'can_manage_skinning_tools'),
                        ('Force Analysis', 'can_force_analysis'),
                        ('Sales', 'can_manage_sales'),
                        ('Customer Success', 'can_manage_customer_success')]

    content_type = ContentType.objects.get_for_model(RevUpUser)

    for name, codename in permission_types:
        permission, _ = Permission.objects.get_or_create(
            name=name, codename=codename, content_type=content_type
        )
        cs_permissions.append(permission)
        if name == 'Sales':
            sales_permissions.append(permission)

    cust_success_group, _ = Group.objects.get_or_create(name='Customer Success')
    cust_success_group.permissions.add(*cs_permissions)

    sales_group, _ = Group.objects.get_or_create(name='Sales')
    sales_group.permissions.add(*sales_permissions)


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('authorize', '0039_merge_20180820_1245'),
        ('auth', '0009_alter_user_last_name_max_length'),
    ]

    operations = [
        migrations.RunPython(
            add_groups_and_permissions, lambda x, y: True
        ),
    ]
