# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authorize', '0008_auto_20150219_1516'),
    ]

    operations = [
        migrations.AlterField(
            model_name='revuptask',
            name='task_type',
            field=models.CharField(default='OT', max_length=16, db_index=True, choices=[('FB', 'Facebook Import'), ('GM', 'Gmail Import'), ('LI', 'LinkedIn Import'), ('NS', 'Network Search'), ('TW', 'Twitter Import'), ('OU', 'Outlook Import'), ('OT', 'Other Import')]),
            preserve_default=True,
        ),
    ]
