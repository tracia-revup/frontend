
from django.core.management.base import BaseCommand, CommandError

from frontend.apps.authorize.models import RevUpUser


class Command(BaseCommand):
    help = "Find user id for user by email address"

    def add_arguments(self, parser):
        parser.add_argument('email_address', type=str,
                            help="Email address of user")

    def handle(self, email_address, **options):
        try:
            user = RevUpUser.objects.get(email=email_address)
        except RevUpUser.DoesNotExist:
            raise CommandError(
                "No user with email address '{}' exists.".format(
                    email_address))
        else:
            print("User id: {:d}".format(user.id))
