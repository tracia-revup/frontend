from functools import wraps
import logging

from django.shortcuts import HttpResponse

from frontend.libs.utils.view_utils import get_client_ip

LOGGER = logging.getLogger(__name__)


def allow_ip(ip_list):
    """Allows the request only if it came from the ips defined in ip_list or
    if the user is an admin"""

    def wrapper(view_func):
        @wraps(view_func)
        def _inner(request, *args, **kwargs):
            # Retrieve request origin ip
            ip = get_client_ip(request)
            # Check if the user is an admin
            user_is_admin = request.user.is_admin if hasattr(request.user,
                                                             'is_admin') else False
            # Check if the request originated from one of the ips in the
            # allowed_ips or if the user is an admin
            if ip in ip_list or user_is_admin:
                return view_func(request, *args, **kwargs)
            LOGGER.error('Received request: {0} from unknown ip: {1}'.format(
                str(request), ip))
            # Return 403 error code as the request does not satisfy any of the
            # conditions
            return HttpResponse('not allowed', status=403)
        return _inner
    return wrapper
