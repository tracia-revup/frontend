from collections import OrderedDict, Mapping
import datetime
from functools import partial
import itertools
import logging
from math import floor
import re

from celery import result, states
from celery.app import app_or_default
from celery.app.control import Inspect
from ddtrace import tracer
from django.conf import settings
from django.db import models, transaction
from django.contrib.auth import hashers
from django.contrib.auth.models import AbstractUser
from django.contrib.contenttypes.models import ContentType
from jsonfield import JSONField
from localflavor.us.models import USStateField
from social_django.models import UserSocialAuth

from frontend.apps.analysis.models.config_base import AnalysisConfigModel
from frontend.apps.analysis.models import Analysis
from frontend.apps.campaign.models import Event
from frontend.apps.contact.models import (
    Contact, ImportConfig, ImportRecord, LimitTracker)
from frontend.apps.contact.utils import ContactSourceMixin
from frontend.apps.contact_set.models import ContactSet
from frontend.apps.role.permissions import AdminPermissions
from frontend.libs.utils.email_utils import send_template_email
from frontend.libs.utils.general_utils import instance_cache
from frontend.libs.utils.model_utils import (
    Choices, DynamicClassModelMixin, GenericForeignKeyQuerySet,
    LockingModelMixin, PhoneNumberField)
from frontend.libs.utils.string_utils import random_uuid, random_string


LOGGER = logging.getLogger(__name__)


class RevUpTask(LockingModelMixin, ContactSourceMixin):
    class TaskType(Choices):
        choices_map = [
            ('NS', "NETWORK_SEARCH", 'Analysis'),
            # Contact Import tasks
            ('FB', "FACEBOOK_IMPORT", 'Facebook Import'),
            ('GM', "GMAIL_IMPORT", 'Gmail Import'),
            ('LI', "LINKEDIN_IMPORT", 'LinkedIn Import'),
            ('TW', "TWITTER_IMPORT", 'Twitter Import'),
            ('OU', "OUTLOOK_IMPORT", "Outlook Import"),
            ('AB', "ADDRESS_BOOK_IMPORT", "Address Book Import"),
            ('CV', "CSV_IMPORT", "CSV Import"),
            ('IP', "IPHONE_IMPORT", "iPhone Import"),
            ('AD', "ANDROID_IMPORT", "Android Import"),
            ('OT', "OTHER_IMPORT", 'Other Import'),
            ('NB', "NATIONBUILDER_IMPORT", 'NationBuilder Import'),
            ('HF', "HOUSEFILE_IMPORT", 'Housefile Import'),
            ('CC', "CLONE_CONTACTS", "Clone Contacts to Account"),
            ('CM', "CONTACT_MERGE", "Contact Merging"),
            # Contact Export tasks
            ('RE', "RANKING_EXPORT", "Ranking Export"),
            ('PE', "PROSPECTS_EXPORT", "Prospects Export"),
            ('CE', "CALL_GROUP_EXPORT", "Call Group Export"),
            # Generate Call Sheet
            ('GC', "GENERATE_CALL_SHEETS", "Generate Call Sheets"),
            ('GG', "GENERATE_CALLGROUP_SHEETS", "Generate Call Sheets from Group"),
            # Background/Batch tasks
            ('GL', "CALL_GROUP_FROM_LIST", "Generate Call Group from list"),
            ('DC', "DELETE_CONTACTS", "Delete Contacts"),
        ]
        CONTACT_IMPORT_TASKS = {'FB', 'GM', 'LI', 'TW', 'OU', 'NB',
                                'AB', 'CV', 'IP', 'AD', 'OT', 'CC', 'HF', 'CM'}
        CONTACT_EXPORT_TASKS = {'RE', 'PE', 'CE'}
        GENERATE_CALL_SHEET_TASKS = {'GC', 'GG'}
        BATCH_TASKS = {'GL', 'DC'}
        NON_ANALYSIS_TASKS = (CONTACT_EXPORT_TASKS | CONTACT_IMPORT_TASKS |
                              GENERATE_CALL_SHEET_TASKS | BATCH_TASKS)
        ANALYSIS_BLOCKING_TASKS = (CONTACT_IMPORT_TASKS | {"DC", "NS"})
        DELETION_BLOCKING_TASKS = ANALYSIS_BLOCKING_TASKS


    # Translate between cloudsponge and TaskType
    cs_translations = {
        "OUTLOOK": TaskType.OUTLOOK_IMPORT,
        "ADDRESSBOOK": TaskType.ADDRESS_BOOK_IMPORT,
    }

    label = models.CharField(max_length=255, default="", blank=True)
    social = models.ForeignKey(UserSocialAuth, blank=True, null=True,
                               on_delete=models.SET_NULL)
    task_id = models.CharField(max_length=255)
    task_type = models.CharField(
        max_length=16, db_index=True, choices=TaskType.choices(),
        default=TaskType.OTHER_IMPORT)
    user = models.ForeignKey("RevUpUser", on_delete=models.CASCADE, related_name='tasks')
    account = models.ForeignKey("campaign.Account", on_delete=models.CASCADE, blank=True, null=True)
    revoked = models.BooleanField(default=False)

    utc_created = models.DateTimeField(default=datetime.datetime.utcnow)

    objects = GenericForeignKeyQuerySet.as_manager()

    def _get_locking_model_class(self):
        return RevUpUser

    def _get_locking_model_id(self):
        return self.user_id

    @property
    def expired(self):
        expire_time = self.utc_created + \
                datetime.timedelta(hours=settings.REVUP_TASK_TTL_HOURS)
        now = datetime.datetime.utcnow()
        return now > expire_time

    @property
    def age(self):
        # How long it has been since task was created as a timedelta
        return datetime.datetime.utcnow() - self.utc_created

    @tracer.wrap()
    def revoke(self, terminate=False):
        _revoke = partial(self._app.control.revoke, terminate=terminate)
        self.revoked = True
        self.save()

        if self.task_type == RevUpTask.TaskType.NETWORK_SEARCH:
            Analysis.objects.filter(task_id=self.task_id).update(
                status=Analysis.STATUSES.error)
        elif self.task_type in RevUpTask.TaskType.CONTACT_IMPORT_TASKS:
            ImportRecord.objects.filter(task_id=self.task_id).update(
                import_success=False)

        _revoke(self.task_id)
        for child_id in self._get_all_child_task_ids():
            _revoke(child_id)

    @tracer.wrap()
    def _get_related_tasks(self, **kwargs):
        # Use RevUpUser as a semaphore. Locking a user's tasks instead could
        # cause a race condition if no tasks exist.
        user = self.lock()
        return user.tasks.filter(revoked=False, **kwargs)

    @tracer.wrap()
    def _analysis_blocked(self):
        """Check if Analysis-type task is blocked"""
        # We'll only look at tasks that can block an analysis. Other tasks
        # don't need to be considered
        tasks = self._get_related_tasks(
            task_type__in=self.TaskType.ANALYSIS_BLOCKING_TASKS)

        if self.account:
            # If this task specifies an account, don't block this task
            # because of other accounts.
            tasks = tasks.filter(models.Q(account=self.account) |
                                 models.Q(account=None))

        for task in tasks:
            # If another task is already running, then this task is blocked
            if task != self and not task.expired and \
                    task.started() and not task.finished():
                return True

            # If this is an Analysis task, and the other task is a blocking
            # task of another type that's queued, we want to let it go first.
            if (task.task_type != self.TaskType.NETWORK_SEARCH and
                    task.queued() and not task.expired):
                return True
        return False

    @tracer.wrap()
    def _import_blocked(self):
        """Check if Import-type task is blocked."""
        # We'll only look at deletion tasks. Other tasks don't need to be
        # considered
        tasks = self._get_related_tasks(
            task_type=self.TaskType.DELETE_CONTACTS)

        # If any deletions are running, we need to hold off on import
        for task in tasks:
            if not task.expired and task.started() and not task.finished():
                return True
        return False

    @tracer.wrap()
    def _deletion_blocked(self):
        """Check if Delete-type task is blocked."""
        # We'll only look at tasks that can block a deletion. Other tasks
        # don't need to be considered
        tasks = self._get_related_tasks(
            task_type__in=self.TaskType.DELETION_BLOCKING_TASKS)

        for task in tasks:
            if task == self:
                continue
            if not task.expired and task.started() and not task.finished():
                return True
        return False

    @tracer.wrap()
    @transaction.atomic
    def blocked(self):
        """Check for various conditions that could block a task from executing
           in Celery.

        Returns True if this task should be blocked from running.
        """
        if self.task_type == RevUpTask.TaskType.NETWORK_SEARCH:
            return self._analysis_blocked()
        elif self.task_type in RevUpTask.TaskType.CONTACT_IMPORT_TASKS:
            return self._import_blocked()
        elif self.task_type == RevUpTask.TaskType.DELETE_CONTACTS:
            return self._deletion_blocked()
        else:
            return False

    @property
    @instance_cache
    def _app(self):
        return app_or_default()

    @property
    @instance_cache
    @tracer.wrap()
    def _task_meta(self):
        return self._app.backend.get_task_meta(self.task_id)

    @property
    @instance_cache
    @tracer.wrap()
    def _task_children_meta(self):
        child_ids = self._get_all_child_task_ids()
        if not child_ids:
            return []
        backend = self._app.backend
        # Mongo backend doesn't have mget, so we can't get the progress with
        # a mongo backend. This should only affect dev environments.
        if not hasattr(backend, "mget"):
            return []
        backend_results = backend.mget([backend.get_key_for_task(k)
                                        for k in child_ids])
        results = list(zip(child_ids,
                      (backend.decode_result(x) if x is not None else {}
                       for x in backend_results)))
        return results

    @property
    @tracer.wrap()
    def _celery_result(self):
        if not hasattr(self, "_celery_result_cache"):
            self._celery_result_cache = result.AsyncResult(self.task_id)
        return self._celery_result_cache

    @classmethod
    @tracer.wrap()
    def _iter_children_ids(cls, children):
        for child in children:
            if isinstance(child, result.GroupResult):
                for child_ in cls._iter_children_ids(child.children):
                    yield child_
            elif isinstance(child, result.AsyncResult):
                yield child.id
            elif isinstance(child, tuple):
                # This code is based off the function result_from_tuple in the
                # celery.result module. For the sake of simplicity and to
                # reduce the number of objects created, we're going to cut out
                # the middle man and not use result_from_tuple, since all we
                # would do is just get the child id anyway.
                #
                # This code is extremely specific to the way we are currently
                # doing task parallelization. Deep nesting (which is possible)
                # is not really supported by this code. If deep nesting is ever
                # required we will need to use the `result_from_tuple`
                # function.
                res, nodes = child
                if nodes:
                    for child_ in cls._iter_children_ids(nodes):
                        yield child_
                else:
                    c_id, parent = \
                        res if isinstance(res, (list, tuple)) else (res, None)
                    yield c_id

    @tracer.wrap()
    def _get_all_child_task_ids(self):
        task_ids = []
        result_ = self._task_meta.get('result')
        children = self._task_meta.get('children')

        if isinstance(result_, result.ResultBase):
            assert isinstance(result_, result.AsyncResult)
            task_ids.append(result_.id)

        if children:
            task_ids.extend(self._iter_children_ids(children))
        return task_ids

    @tracer.wrap()
    def _get_all_task_ids(self):
        task_ids = [self.task_id]
        task_ids.extend(self._get_all_child_task_ids())
        return task_ids

    @tracer.wrap()
    def _all_test_task_result_tree(self, _check):
        """Spider through all subtasks, running predicate `_check` against
        every task to determine state of job.

        Only returns True if every predicate returns True for every subtask.
        """
        return _check(self._task_meta) and all(
            _check(child_meta) for _, child_meta in self._task_children_meta)

    @tracer.wrap()
    def _any_test_task_result_tree(self, _check):
        """Spider through all subtasks, running predicate `_check` against
        every task to determine state of job.

        Returns True if the predicate returns True for even one subtask.
        """
        if _check(self._task_meta):
            return True
        else:
            return any(_check(child_meta)
                       for _, child_meta in self._task_children_meta)

    @tracer.wrap()
    def ready(self):
        return not self.pending()

    @tracer.wrap()
    def queued(self):
        return self.pending() and not self.started()

    @tracer.wrap()
    def running(self):
        """Determine whether the task is actively running on any workers"""
        if self.started():
            i = Inspect(app=self._app)
            result = i.query_task(list(self._get_all_task_ids()))
            if result is not None and any(result.values()):
                return True
        return False

    @tracer.wrap()
    def started(self):
        STARTED = states.state(states.STARTED)
        def _check(task_meta):
            return STARTED <= task_meta.get('status')
        return self._any_test_task_result_tree(_check)

    @tracer.wrap()
    def pending(self):
        def _check(task_meta):
            return task_meta.get('status') not in states.READY_STATES
        return self._any_test_task_result_tree(_check)

    @tracer.wrap()
    def successful(self):
        def _check(task_meta):
            return task_meta.get('status') == states.SUCCESS
        return self._all_test_task_result_tree(_check)

    @tracer.wrap()
    def failed(self):
        def _check(task_meta):
            return task_meta.get('status') == states.FAILURE
        return self._any_test_task_result_tree(_check)

    @tracer.wrap()
    def finished(self):
        return self.successful() or self.failed()

    @tracer.wrap()
    def get_status_label(self):
        if self.revoked:
            status = 'Revoked'
        elif self.failed():
            status = 'Failed'
        elif self.successful():
            status = 'Completed'
        elif self.started() and self.get_percent() != None:
            status = 'Started'
        elif self.expired:
            status = 'Expired'
        else:
            status = 'Queued'
        return status

    @tracer.wrap()
    def get_label(self, regenerate=False):
        """Return a type-appropriate label for the task type."""
        label = None
        if self.label and not regenerate:
            return self.label
        elif self.task_type == self.TaskType.GMAIL_IMPORT:
            if not self.social:
                return label
            label = self.social.uid
        elif self.task_type == self.TaskType.LINKEDIN_IMPORT:
            if not self.social:
                return label
            first = self.social.extra_data.get("first_name", "")
            last = self.social.extra_data.get("last_name", "")
            if first and last:
                label = "{} {}".format(first, last)
        elif self.task_type in (self.TaskType.NETWORK_SEARCH,
                                self.TaskType.DELETE_CONTACTS,
                                self.TaskType.CLONE_CONTACTS):
            if self.contact_source:
                if isinstance(self.contact_source, Contact):
                    label = self.contact_source.name
                else:
                    label = self.contact_source.title
            else:
                if self.account and self.user == self.account.account_user:
                    owner = "Account"
                else:
                    owner = "Personal"
                label = "All {} Contacts".format(owner)
        if not label:
            label = self.TaskType.translate(self.task_type, "")\
                                 .replace(" Import", "")
        return label

    @tracer.wrap()
    def _chord_progress_strategy(self):
        '''Strategy for determining analysis progress

        This strategy is specific to analysis, making assumptions on how
        workers are chained together in order to work.

        The final task which runs the meta features is considered 1% of the
        analysis. The remaining 99% is split evenly between each contact
        processing worker. Since each child will report their progress in the
        metadata, we can just add that progress percentage to our total, and
        that allows us more granular progress tracking.
        '''
        children = self._task_children_meta
        if not children:
            # If there are no children, then the initial task which starts all
            # the sub-tasks for the analysis hasn't completed yet. We'll
            # consider this part of the preparation
            return None
        else:
            worker_count = len(children)
            in_progress = 0.0
            for _, child in children:
                child_state = child.get('status')
                child_result = child.get('result')
                if child_state in states.READY_STATES:
                    in_progress += 100
                elif child_state == states.STARTED and isinstance(child_result,
                                                                  Mapping):
                    in_progress += child_result.get('percent', 0)
            return floor((in_progress / worker_count) * 0.99)

    @instance_cache
    @tracer.wrap()
    def get_percent(self):
        info = self._task_meta.get('result')
        percent = None
        chord_tasks = {RevUpTask.TaskType.NETWORK_SEARCH,
                       RevUpTask.TaskType.DELETE_CONTACTS}.union(
            RevUpTask.TaskType.CONTACT_IMPORT_TASKS).union(
            RevUpTask.TaskType.GENERATE_CALL_SHEET_TASKS)
        # Legacy functionality. If info is a dictionary, then try and get
        # 'percent' out of it. This will cover all cases except for jobs that
        # are: queued, successful, failed, or parallelized.
        if isinstance(info, Mapping):
            percent = info.get('percent')
        elif self.queued():
            percent = None
        elif self.successful():
            percent = 100
        elif self.ready():
            # If we detect the job is ready, but was not successful, the job
            # either failed or was revoked.
            percent = None
        elif self.task_type in chord_tasks:
            percent = self._chord_progress_strategy()

        if isinstance(percent, (int, float)):
            if percent < 0:
                percent = 0
            elif percent > 100:
                percent = 100
        return percent

    @classmethod
    def pre_save(cls, sender, instance, *args, **kwargs):
        # If the label isn't set when this task is created, let's try to set it
        if instance.id is None and not instance.label:
            instance.label = instance.get_label()


models.signals.pre_save.connect(RevUpTask.pre_save, sender=RevUpTask)


class TokenModel(models.Model):
    class Meta:
        abstract = True

    token = models.CharField(max_length=32, blank=True,
                             default=random_uuid)
    utc_created = models.DateTimeField(blank=True,
                                       default=datetime.datetime.utcnow)

    @property
    def is_expired(self):
        return settings.REVUP_INVITE_EXPIRATION_DAYS <= (
                datetime.datetime.utcnow() - self.utc_created).days


class RevUpUserToken(TokenModel):
    """Generates a token for semi-secure user tasks.
    E.g. Finishing registration of a partially-created user
    """
    class TokenTypeChoices(object):
        REGISTRATION = 'CR'

    user = models.ForeignKey("RevUpUser", on_delete=models.CASCADE,
                             related_name="tokens")
    token_type = models.CharField(
        max_length=2, blank=True,
        choices=(
            (TokenTypeChoices.REGISTRATION, "Complete Registration"),
        ),
        default=TokenTypeChoices.REGISTRATION
    )


class ExternalSignupToken(TokenModel):
    first_name = models.CharField(max_length=64, blank=True)
    last_name = models.CharField(max_length=150, blank=True)
    email = models.EmailField(blank=True)
    phone = models.CharField(max_length=32, blank=True)
    organization = models.CharField(max_length=512, blank=True)
    addendum1 = models.CharField(max_length=256, blank=True)
    addendum2 = models.CharField(max_length=256, blank=True)
    redeemed = models.DateTimeField(blank=True, null=True)
    referral_partner = models.CharField(max_length=512, blank=True, null=True)
    skip_payment = models.BooleanField(blank=True, default=False)
    product = models.ForeignKey("campaign.Product",
                                on_delete=models.deletion.CASCADE)
    profile = models.ForeignKey("campaign.AccountProfile",
                                on_delete=models.deletion.CASCADE)
    user = models.ForeignKey("RevUpUser", related_name="signup_tokens",
                             null=True, blank=True,
                             on_delete=models.deletion.SET_NULL)
    account = models.OneToOneField("campaign.Account",
                                   related_name="signup_token",
                                   null=True, blank=True,
                                   on_delete=models.deletion.SET_NULL)

    def clone(self, save=True):
        """This clones an existing token, resetting the utc_created time
        to now and generating a new uuid.
        """
        token = self
        token.id = None
        token.pk = None
        token.utc_created = datetime.datetime.utcnow()
        token.token = random_uuid()

        if save:
            token.save()
        return token

    def send_token_signup_email(self, request):
        """Send the signup email with the token."""
        token = self
        subject = "Welcome to RevUp"
        email_template = 'core/emails/qualifier_signup_email.html'
        context = dict(token=token, request=request)
        send_template_email(subject, email_template, context, token.email,
                            email_type="Onboard Invite")


class UserAnalysisConfigLink(models.Model):
    analysis_config = models.ForeignKey("analysis.AnalysisConfig", on_delete=models.CASCADE,
                                        related_name='+')
    account = models.ForeignKey("campaign.Account", on_delete=models.CASCADE, related_name='+')
    is_default = models.BooleanField(default=False, blank=True)
    is_active = models.BooleanField(default=True, blank=True)
    user = models.ForeignKey('authorize.RevUpUser', on_delete=models.CASCADE, blank=True, null=True,
                             related_name='+', swappable=False)

    def make_default(self):
        """Make the linked analysis config the default."""
        with transaction.atomic():
            # If not owned, make all non-user-owned configs non-default
            # If owned, make all user-owned configs non-default
            if self.user:
                self.objects.filter(
                    account=self.account,
                    user=self.user).update(is_default=False)
                # Make this one the default
                self.is_default = True
                self.save()


class RevUpUser(LockingModelMixin, AbstractUser):
    first_name = models.CharField('first name', max_length=64, blank=True)
    middle_name = models.CharField(max_length=30, blank=True,
                                   verbose_name='middle name')
    events = models.ManyToManyField("campaign.Event", related_name="+",
                                    through="UserEventLink")

    current_event = models.ForeignKey("campaign.Event", blank=True, null=True,
                                      on_delete=models.SET_NULL)

    current_analysis = models.ForeignKey("analysis.Analysis", blank=True,
                                         null=True, on_delete=models.SET_NULL)
    current_seat = models.ForeignKey("seat.Seat", blank=True, null=True,
                                     on_delete=models.SET_NULL)

    street_1 = models.CharField(max_length=256, blank=True)
    street_2 = models.CharField(max_length=256, blank=True)
    city = models.CharField(max_length=64, blank=True)
    state = USStateField(blank=True)
    zip = models.CharField(max_length=10, blank=True)
    phone = PhoneNumberField(blank=True)
    accepted_tos = models.DateTimeField(blank=True, null=True)
    password_expiration_date = models.DateField(blank=True, null=True)

    # analysis_configs are a set of available configs for this account. They
    # come from account_profile by default, but can be edited.
    user_analysis_configs = models.ManyToManyField(
        "analysis.AnalysisConfig",
        through="UserAnalysisConfigLink", blank=True, related_name='+')

    import_config = models.ForeignKey("contact.ImportConfig", blank=True,
                                      null=True, on_delete=models.SET_NULL)
    applied_filters = models.ManyToManyField("filtering.Filter")

    def __str__(self):
        return ' - '.join([self.name, self.email])

    @property
    def name(self):
        # If there is either a first or last name, some name will be returned
        name = " ".join(list(filter(None, (self.first_name, self.last_name))))
        if name:
            return name
        else:
            # Otherwise, return the username portion of an email, or the
            # whole word if username is not an email.
            return self.email.split('@')[0]

    @property
    def is_admin(self):
        return self.is_staff or self.is_superuser

    def has_usable_password(self):
        return hashers.is_password_usable(self.password)

    def has_contacts(self):
        return Contact.objects.filter(user=self).exists()

    def has_permissions(self, permissions, account=None):
        """Check if this user has any seats with the given permissions.

        If an account is provided, check the specific account, otherwise use
        current_seat
        """
        if not account:
            try:
                account = self.current_seat.account
            except AttributeError:
                return False

        account_id = account.id if isinstance(account, models.Model) \
                                else account

        # If the user has a current seat, we can check that first
        if self.current_seat and self.current_seat.account.id == account_id:
            if self.current_seat.has_permissions(any=permissions):
                return True

        # Query for all of this user's assigned seats, optionally for a
        # specific Account.
        seats = self.seats.assigned().filter(account_id=account_id)
        # Iterate over assigned seats and check if any have admin permissions
        for seat in seats.only("id", "user", 'permissions').all():
            if seat.has_permissions(any=permissions):
                return True
        return False

    def is_account_admin(self, account=None):
        """Check if this user is an admin of the given account.

        If no account is supplied, just check for the account admin group

        ****** DEPRECATED *******
        This will be removed soon. Use has_permissions instead.
        """
        # Query for all of this user's assigned seats, optionally for a
        # specific Account.
        seats = self.seats.assigned()
        if account:
            account_id = account.id if isinstance(account, models.Model) \
                else account
            seats = seats.filter(account_id=account_id)

        # Iterate over assigned seats and check if any have admin permissions
        for seat in seats.only('permissions').all():
            if seat.has_permissions(any=AdminPermissions.all()):
                return True
        return False

    def is_account_user(self):
        """Check if this user is the special account_user type

        Although the technique used below is not perfect, it's good enough
        for most purposes. A user couldn't get into the site with none of
        those values set.
        """
        return (self.username.startswith("account_user_") or
                bool(re.match(r"account_\d+_user", self.username))) and not any((
            self.password, self.last_login, self.first_name, self.last_name,
            self.accepted_tos))

    def is_lead_fundraiser(self, event):
        return self.event_links.filter(event=event,
                                       is_lead=True).exists()

    def set_lead_fundraiser(self, event):
        self.event_links.filter(event=event).update(is_lead=True)

    def unset_lead_fundraiser(self, event):
        self.event_links.filter(event=event).update(is_lead=False)

    def reset_current_seat(self, seat, request=None, save=True):
        """Set the current seat to a new value.
        When the seat is changed, the current analysis and event need to be
        cleared also, so we don't mix up different campaigns.
        """
        self.current_event = None
        self.current_seat = seat
        self.current_analysis = seat.get_default_analysis() if seat else None
        if request:
            # Clear the session without logging out.
            for key in list(request.session.keys()):
                # Don't delete private session keys
                if key.startswith("_"):
                    continue
                del request.session[key]
        if save:
            self.save()

    def generate_token(
            self, token_type=RevUpUserToken.TokenTypeChoices.REGISTRATION):
        """Generate and return a semi-secure token.

        A typical use-case is to use in a reasonably unique, unpredictable URL.
        """
        # Clean the existing tokens of this type
        self.clean_tokens(token_type)
        # Generate a new token.
        return RevUpUserToken.objects.create(token_type=token_type, user=self)

    def clean_tokens(self, token_type=None):
        """Clean up (delete) the user's tokens.

        If token_type is None, all tokens for this user will be removed.
        """
        query_kwargs = {'user': self}
        if token_type is not None:
            query_kwargs['token_type'] = token_type
        RevUpUserToken.objects.filter(**query_kwargs).delete()

    def get_token(self,
                  token_type=RevUpUserToken.TokenTypeChoices.REGISTRATION):
        """Retrieve this user's token from the DB, and handle error cases.

        If a user has multiple tokens of the same type, we clean them up.
        """
        tokens = (RevUpUserToken.objects.filter(user=self,
                                                token_type=token_type)
                                        .order_by('-utc_created'))
        token_len = len(tokens)
        if token_len == 0:
            token = None
        elif token_len == 1:
            token = tokens[0]
        else:
            # If multiple objects are returned, invalidate all but the recent.
            token = tokens[0]
            for extra_token in tokens[1:]:
                extra_token.delete()
        if token and token.is_expired:
            token.delete()
            token = None
        return token

    def get_default_analysis(self):
        if hasattr(self, 'account_user'):
            # `account_user` is the reverse link for the OneToOne between
            # `RevUpUser` and `Account`. If this property is set, that means
            # this user is the account-level user, and as such it will never
            # have a seat.
            # This fixes an issue where this method always returned `None` for
            # account users.
            account = self.account_user
            account_id = account.id
        elif self.current_seat:
            account = account_id = self.current_seat.account_id
        else:
            return None

        if self.current_analysis and self.current_analysis.account_id == \
                account_id:
            return self.current_analysis

        root_cs = ContactSet.get_root(account, self,
                                      select_related="analysis")
        if not root_cs:
            return None

        self.current_analysis = root_cs.analysis
        self.save(update_fields=['current_analysis'])
        return self.current_analysis

    def get_default_event(self):
        if not self.current_event:
            # If no current event is set, try to select one automatically.
            if self.current_seat:
                events = self.events.filter(account=self.current_seat.account)
                if not events:
                    event = self._create_default_event()
                else:
                    events = sorted((event for event in events),
                                    key=lambda x: x.event_date, reverse=True)
                    event = events[0]
                self.current_event = event
                if self.current_event:
                    self.save()
        return self.current_event

    def get_default_analysis_config(self, account):
        """Find the default analysis config for this user.
        It could come from one of multiple places.
        """
        analysis_config_link = UserAnalysisConfigLink.objects.filter(
            account=account, user=self, is_default=True,
            is_active=True).select_related("analysis_config").first()
        # If this user has an AnalysisConfig defined, us it. Otherwise, go
        # to the Account to get the config.
        if analysis_config_link:
            return analysis_config_link.analysis_config
        else:
            return account.get_default_analysis_config()

    def get_valid_seats(self):
        """Returns a list of this user's seats that are not revoked and
        not part of an inactive account (except in certain cicumstances,
        see below).
        """
        # Importing here to avoid circular import
        from frontend.apps.seat.models import Seat

        # Query all of this user's assigned seats. We only want active accounts
        seats = Seat.objects.select_related(
            'account__account_profile').assigned().filter(user=self,
                                                          account__active=True)

        # Account heads are allowed to access inactive accounts.
        # Note: Two separate queries is intentional here. This lookup cannot
        #       be done correctly in one query.
        inactive_seats = Seat.objects.select_related(
            'account__account_profile').\
            assigned().filter(user=self,
                              account__account_head=self,
                              account__active=False)
        return list(itertools.chain(seats, inactive_seats))

    def add_events(self, *events, **kwargs):
        """Add an event to this user.

        Since we're using an intermediate table, the standard manager.add()
        does not work. This is a helper method for that.

        Allowed arguments are 1 or more events to link and a kwarg: "is_lead".
        E.g. is_lead=False.
        """
        is_lead = kwargs.pop("is_lead", False)
        links = []
        # Eliminate any events that are already linked
        existing = {uel.event.id: uel
                    for uel in self.event_links.filter(event__in=events)}
        for event in events:
            existing_uel = existing.get(event.id)
            if existing_uel:
                # If the existing UEL is_lead doesn't match, update it.
                if existing_uel.is_lead != is_lead:
                    existing_uel.is_lead = is_lead
                    existing_uel.save()
                    links.append(existing_uel)
                continue
            links.append(UserEventLink.objects.create(user=self, event=event,
                                                      is_lead=is_lead))

    @classmethod
    def _build_user(cls, email, first_name='', last_name=''):
        if len(email) > 30:
            # If an email is longer than 30 characters, we need to truncate
            # it. However, if we truncate, this creates a chance (albeit
            # very small chance) that there could be a collision, so we add
            # a few random characters to reduce the chance even more.
            username = email[:26] + random_string(length=4)
        else:
            username = email
        return RevUpUser(username=username, email=email, first_name=first_name,
                         last_name=last_name, is_active=False)

    @classmethod
    def get_or_create_inactive(cls, email, first_name='', last_name='',
                               save=True, select_for_update=False):
        """Return an existing user, or create a new inactive one.

        *Important*: The email is assumed to be pre-validated

        :return: A tuple of (RevUpUser, boolean if one was created or not)
        """
        # See if the user already exists
        try:
            if select_for_update:
                qs = RevUpUser.objects.select_for_update()
            else:
                qs = RevUpUser.objects
            user = qs.get(email__iexact=email)
            created = False
        except RevUpUser.DoesNotExist:
            # If the user does not exist, we will need to create a new one
            user = cls._build_user(email, first_name=first_name,
                                   last_name=last_name)
            created = True
            if save:
                user.save()
        return user, created

    def _create_default_event(self):
        """Create a dummy event if the user doesn't already have an event

        This is a temporary measure to ensure that a user not yet having an
        event assigned doesn't cause errors in the application. This will be
        replaced/removed once we separate the functionality of prospect lists
        from events.

        """
        if self.current_seat:
            today = datetime.date.today()
            event_date = today + datetime.timedelta(days=365)
            event = Event.objects.create(
                event_name="Test event, " + self.username,
                event_date=event_date, goal=10000,
                price_points=[1000, 2700, 5400],
                account=self.current_seat.account)
            self.add_events(event)
            return event

    @transaction.atomic
    def clean_stale_tasks(self, task_type=None, task_lock=None):
        if task_lock is None:
            task_lock = self.lock()

        if task_type is None:
            tasks = task_lock.tasks.all()
        elif isinstance(task_type, (list, tuple, set)):
            tasks = task_lock.tasks.filter(task_type__in=task_type)
        else:
            tasks = task_lock.tasks.filter(task_type=task_type)

        for task in tasks:
            # TODO: handle case where task is running. need to communicate
            # back to user task is running and get permission to cancel
            # running task.
            if task.queued() and \
                    task.age > settings.REVUP_TASK_REVOKE_MIN_TIMEDELTA:
                # Revoke task if it has been queued for longer than
                # configured time, and has not started yet.
                task.revoke(terminate=True)
            elif task.started() and not task.finished() and not task.running():
                # Revoke task if it has started, but is not running. This
                # indicates the task is in an error state and needs to be
                # restarted.
                task.revoke(terminate=True)

            if task.revoked or task.expired:
                if task.task_type == RevUpTask.TaskType.NETWORK_SEARCH:
                    task_lock.analysis_set.filter(task_id=task.task_id).update(
                        status=Analysis.STATUSES.error)
                task.delete()

    @instance_cache
    def get_index_filters(self):
        try:
            return list(self.import_config.filters.filter(filter_type='CN'))
        except AttributeError:
            return []

    @classmethod
    def add_import_config(cls, instance, created, **kwargs):
        """post_save signal that adds a default import_config."""
        if created and not instance.import_config:
            try:
                instance.import_config = ImportConfig.objects.get(
                    title="Default Import Config")
                instance.save()
            except ImportConfig.DoesNotExist:
                LOGGER.error("Could not set default import config for user: "
                             "{}".format(instance))


models.signals.post_save.connect(RevUpUser.add_import_config, sender=RevUpUser)


class UserEventLink(models.Model):
    user = models.ForeignKey(RevUpUser, on_delete=models.CASCADE, related_name="event_links")
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name="user_links")
    is_lead = models.BooleanField(blank=True, default=False,
                                  help_text="Is this user a lead fundraiser?")

    class Meta:
        unique_together = [("user", "event")]


class AnalysisViewUserSettings(models.Model):
    """Contains a user's settings that are relevant to an AnalysisProfile."""
    class Meta:
        unique_together = (
            ('user', 'analysis_profile'),
        )

    user = models.ForeignKey('authorize.RevUpUser', on_delete=models.CASCADE,
                             related_name='analysis_view_user_settings')
    analysis_profile = models.ForeignKey('analysis.AnalysisProfile',
                                         related_name='+',
                                         on_delete=models.PROTECT)
    data = JSONField(
        load_kwargs={'object_pairs_hook': OrderedDict})

    def get_shallow_data(self):
        return self.analysis_profile.analysis_view_setting_set.dynamic_class()\
                .get_shallow_data(self.data)

    def get_deep_data(self, include_defaults=True):
        data = self.analysis_profile.analysis_view_setting_set.dynamic_class()\
                                    .get_deep_data(self.data or {})
        if include_defaults:
            # Populate user settings with defaults if setting is unset
            fields = self.analysis_profile.analysis_view_setting_set\
                         .dynamic_class().get_fields()
            for field in fields:
                expected = field['expected']
                for ef in expected:
                    field_name = ef['field']
                    if field_name not in data:
                        data[field_name] = ef.get('default')
        return data


class AnalysisViewSettingSet(AnalysisConfigModel, DynamicClassModelMixin):
    """An AnalysisViewSettingSet is the way we prompt the user for information
    about how to display their analysis results. These settings do not affect
    how an analysis is calculated or saved, only displayed.

    Examples of settings:
       - some arbitrary filter to always apply to results.
       - show/hide maxed out donors
       - show/hide political affiliation colors
    """
