
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from social_core.backends.nationbuilder import NationBuilderOAuth2


class CustomModelBackend(ModelBackend):
    """Allows for email authentication.

    Also override the default get_user to include some select_related.
    """
    select_related = ('current_seat',
                      'current_seat__account__account_profile__product',)

    def authenticate(self, email=None, password=None, **kwargs):
        UserModel = get_user_model()
        if not (email and password):
            return None
        try:
            user = UserModel._default_manager.get(email__iexact=email)
            if user.check_password(password):
                return user
        except UserModel.DoesNotExist:
            return None

    def get_user(self, pk, *args, **kwargs):
        """This code is inspired by DjangoUserMixin.get_user, which the parent
        class of this would call. However, it doesn't allow for any injection
        of a select_related.
            social/storage/django_orm.py
        """
        UserModel = get_user_model()
        try:
            return UserModel._default_manager.select_related(
                *self.select_related).get(pk=pk)
        except UserModel.DoesNotExist:
            return None


class MultiNationBuilder(NationBuilderOAuth2):
    EXTRA_DATA = [('nation_slug', 'nation_slug')] + \
            NationBuilderOAuth2.EXTRA_DATA

    @property
    def slug(self):
        return self.strategy.session_get('nation_slug')

    def user_data(self, *args, **kwargs):
        data = super().user_data(*args, **kwargs)
        data['nation_slug'] = self.slug
        return data
