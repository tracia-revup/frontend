from collections import Iterable
import json

from django.test import override_settings
from django.urls import reverse
from rest_framework.test import APIClient as Client

from frontend.apps.authorize.factories import RevUpUserFactory
from frontend.apps.seat.factories import SeatFactory, Seat
from frontend.libs.utils.test_utils import TestCase, TransactionTestCase


class NoPermissionMixin(object):
    # noinspection PyPep8Naming
    def assertNoPermission(self, result):
        """A helper method to keep things a little more DRY."""
        error_str = "You do not have permission to perform this action"
        try:
            self.assertContains(result, error_str, status_code=403)
        except AssertionError:
            # JWT auth returns a 401 instead of 403
            self.assertContains(result, error_str, status_code=401)


class NoCredentialMixin(object):
    # noinspection PyPep8Naming
    def assertNoCredentials(self, result):
        """Another helper method to keep things DRY."""
        err_str = "Authentication credentials were not provided"
        try:
            self.assertContains(result, err_str, status_code=403)
        except AssertionError:
            # JWT auth returns a 401 instead of 403
            self.assertContains(result, err_str, status_code=401)


class NoAuthenticationMixin(object):
    # noinspection PyPep8Naming
    def assertNoAuthentication(self, result):
        """A helper method to keep things a little more DRY.

        This is similar to NoCredentialMixin but this one will occur on APIs
        that support JWT authentication
        """
        self.assertContains(result, "Authentication credentials were not "
                                    "provided.", status_code=401)


class VerifySerializerMixin(object):
    def _parse_response(self, response):
        parsed_response = json.loads(response.content)
        if 'results' in parsed_response:
            data = parsed_response['results']
        else:
            data = [parsed_response]
        return data

    def verify_serializer(self, response, expected, specific_field=None):
        """Verify each object in the response json has the exact fields given
        in 'expected'.

        This method will error if there are more or fewer fields
        in the response than what is expected.

        :param response: An HttpResponse object, with api response content
        :param expected: An iterable of fields that are expected in the output
                         of the API.
        :param specific_field: Some APIs have nested data. This causes the
                        function to look at the contents of the given field
                        instead of the top-level
        """
        self.assertEqual(response.status_code, 200)
        data = self._parse_response(response)

        # Verify the fields in the response match the expected
        sorted_expected = sorted(expected)
        for datum in data:
            if specific_field:
                datum = datum[specific_field]
            self.assertEqual(sorted(datum.keys()), sorted_expected)

    def verify_content(self, response, expected):
        """Verify the fields given in 'expected' are reflected in the
        response.

        Unlike verify_serializer, data points may be omitted (like 'id')
        :param response: An HttpResponse object, with api response content
        :param expected: An dict of fields and values that are expected in
                         the output of the API.
        """
        self.assertEqual(response.status_code, 200)
        data = self._parse_response(response)[0]
        for field, value in expected.items():
            self.assertEqual(data[field], value)


class AuthorizedUserTestCaseBase(NoPermissionMixin, NoCredentialMixin,
                             NoAuthenticationMixin, VerifySerializerMixin):
    """Prepares a user and logs them in.

    This overrides the setUp method to create a RevUpUser and
    log them in. This is useful for view tests that need a
    logged-in user.
    """
    def setUp(self, login_now=True, seat=0, set_session=True, jwt_auth=False, **kwargs):
        # By default, we wont define any campaigns or events for this
        # user. However, they can be passed-in through the kwargs

        # Pull campaigns out of the kwargs so we can create Seats
        accounts = kwargs.pop('campaigns', [])
        if not isinstance(accounts, Iterable):
            accounts = [accounts]

        self.user = RevUpUserFactory(**kwargs)

        if accounts:
            for account in accounts:
                SeatFactory(account=account, user=self.user)

            seat = self.user.seats.first()
            seat.permissions = seat.FundraiserPermissions.all()
            seat.save()
        # seat can be None, so we use a 0 flag instead
        elif seat == 0 and set_session:
            seat = SeatFactory(
                user=self.user,
                permissions=Seat.FundraiserPermissions.all())

        if seat and set_session:
            self.user.current_seat = seat
            self.user.save()

        # Instantiate a client and login
        self.client = Client()
        if login_now:
            self._login(jwt_auth=jwt_auth)

    def _login(self, user=None, password="this_Is_a_Test_Password1",
               client=None, jwt_auth=False):
        if user is None:
            user = self.user
        if client is None:
            client = self.client

        if jwt_auth:
            response = client.post(reverse('api_jwt_auth'),
                                   {'email': user.email,
                                    'password': password})
            if response.status_code == 200:
                token = response.data.get('token', None)
                client.credentials(HTTP_AUTHORIZATION='JWT ' + token)
        else:
            response = client.post(reverse('account_login'),
                                   {'email': user.email,
                                    'password': password})
        return response

    def _logout(self):
        return self.client.post(reverse('account_logout'))

    def _add_admin_group(self, user=None, permissions=None):
        # Factory imported here to avoid circular imports
        if user is None:
            user = self.user

        if isinstance(permissions, str):
            permissions = [permissions]

        # Add the Admin groups to the user
        for seat in user.seats.all():
            seat.permissions += permissions or seat.AdminPermissions.all()
            seat.save()

    def _login_and_add_admin_group(self, user=None, permissions=None,
                                   jwt_auth=False):
        """Helper method for very common operation in test cases.

        Log-in the user and add the campaign admin group to their groups.
        """
        if user is None:
            user = self.user
        # Login the user
        self._login(user, jwt_auth=jwt_auth)
        self._add_admin_group(user, permissions=permissions)



@override_settings(DEFAULT_DOMAIN="", CONTACT_IMPORT_DOMAIN="", CELERY_ALWAYS_EAGER=True)
class AuthorizedUserTestCase(AuthorizedUserTestCaseBase, TestCase):
    pass


@override_settings(DEFAULT_DOMAIN="", CONTACT_IMPORT_DOMAIN="", CELERY_ALWAYS_EAGER=True)
class TransactionAuthorizedUserTestCase(AuthorizedUserTestCaseBase,
                                        TransactionTestCase):
    pass


class PermissionTestMixin(object):
    """A mixin to reduce permission test boiler-plate."""
    permissions = []

    def test_permissions(self):
        # # Verify any unauthorized user is rejected
        result = self.client.get(self.url)
        self.assertNoCredentials(result)
        result = self.client.get(self.url2)
        self.assertNoCredentials(result)

        # Login the user
        self._login(self.user)
        # Verify user is not an admin, or that would invalidate the test.
        self.assertFalse(self.user.is_admin)
        self.assertFalse(self.user.is_account_admin())

        ## Verify a standard user is rejected from accessing
        result = self.client.get(self.url)
        self.assertNoPermission(result)
        ## Verify a standard user is rejected from posting, puting, patching
        for method in (self.client.post, self.client.put, self.client.patch):
            result = method(self.url, data={'test': '123'})
            self.assertNoPermission(result)

        # Add the correct permissions to the seat
        if self.permissions:
            self._add_admin_group(self.user, permissions=self.permissions)
            ## Verify the list API accepts the user
            result = self.client.get(self.url)
            self.assertContains(result, "")
        else:
            print("Warning: No specific permissions set for permissions test")
