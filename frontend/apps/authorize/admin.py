from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import RevUpUser, UserAnalysisConfigLink, RevUpTask


class UserAnalysisConfigLinkInline(admin.TabularInline):
    model = UserAnalysisConfigLink
    extra = 1


class RevUpUserAdmin(UserAdmin):
    inlines = (UserAnalysisConfigLinkInline,)
    list_display = UserAdmin.list_display[:1] + ('id',) + UserAdmin.list_display[1:]

    def __init__(self, *args, **kwargs):
        super(RevUpUserAdmin, self).__init__(*args, **kwargs)
        self.fieldsets[3][1]['fields'] += (
            "password_expiration_date", "accepted_tos")


class RevUpTaskAdmin(admin.ModelAdmin):
    list_display = ("target", "task_type", "label", "age", "get_status_label",
                    "get_percent")
    list_select_related = ("user", "account")
    raw_id_fields = ("user", "account", "social", "_csource_content_type")
    actions = ["revoke"]

    def target(self, task):
        from frontend.apps.campaign.models import Account
        if str(task.user).startswith("account_"):
            account = task.account if task.account else \
                        Account.objects.get(account_user_id=task.user_id)
            return "Account: {}".format(account.organization_name)
        else:
            return "User: {}".format(task.user)

    def revoke(self, request, queryset):
        for task in queryset:
            task.revoke(terminate=True)

admin.site.register(RevUpUser, RevUpUserAdmin)
admin.site.register(RevUpTask, RevUpTaskAdmin)
