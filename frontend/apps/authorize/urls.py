from django.conf.urls import url
from django.contrib.auth.views import logout_then_login

from frontend.apps.authorize.views import (
    RegisterView, LoginView, UserEditView,
    PasswordResetDoneWrapperView, password_reset_wrapper,
    password_reset_confirm_wrapper, password_reset_complete_wrapper,
    password_change, password_change_wrapper, password_change_done_wrapper)


urlpatterns = [
    url(r'^/register/(?P<r_uuid>[^/]+)/$', RegisterView.as_view(),
        name='finish_account_register'),
    url(r'^/register/$', RegisterView.as_view(), name='account_register'),
    url(r'^/login/$', LoginView.as_view(), name='account_login'),
    url(r'^/logout/$', logout_then_login, name='account_logout'),
    url(r'^/edit/$', UserEditView.as_view(), name='account_edit_user'),

    url(r'^/password/reset/$',
        password_reset_wrapper,
        name='password_reset'),
    url(r'^/password/reset/done/$',
        PasswordResetDoneWrapperView.as_view(), name='password_reset_done'),
    url(r'^/password/reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
        password_reset_confirm_wrapper,
        name='password_reset_confirm'),
    url(r'^/password/done/$',
        password_reset_complete_wrapper,
        name='password_reset_complete'),
    url(r'^/password/change/$',
        password_change,
        name='password_change'),
    url(r'^/password/update/$',
        password_change_wrapper,
        name='password_update'),
    url(r'^/password/update/done/$',
        password_change_done_wrapper,
        name='password_change_done'),
]
