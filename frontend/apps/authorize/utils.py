import logging

from celery.utils import uuid

from django.conf import settings
from django.db.models import Q

from frontend.apps.authorize.models import RevUpTask
from frontend.apps.campaign.models.base import AccountProfile
from frontend.libs.utils.email_utils import send_user_email


LOGGER = logging.getLogger(__name__)


def _send_user_email(user, subject, email_template, context,
                     task_type_str, extra_log='', inviting_user=None):
    """Helper method to keep the tasks DRY."""
    # If the user hasn't registered yet, they will need a registration link
    if not user.is_active:
        token = user.get_token()
        if not token:
            raise Exception("Cannot send a registration notification without "
                            "a registration token.")
    else:
        token = None

    if inviting_user:
        sender_address = '{} via {}'.format(inviting_user.name,
                                             settings.DEFAULT_FROM_EMAIL)
    else:
        sender_address = settings.DEFAULT_FROM_EMAIL

    # Generate the content for the email
    context.update({'token': token, 'inviting_user': inviting_user})
    send_user_email(user, subject, sender_address, email_template, context,
                    task_type_str, extra_log)


def send_account_user_added_notification(user, account, protocol_host,
                                         inviting_user=None):
    """Sends an email to a fundraiser notifying them that they have been
     added to an account.

    If the user is new, they are invited to complete their registration.
    """
    # Generate the content for the email
    subject = "Invitation from {}".format(account.title)
    if account.account_profile.account_type == \
            AccountProfile.AccountType.ACADEMIC:
        template = 'authorize/emails/campaign_added_email__uni.html'
    else:
        template = 'authorize/emails/campaign_added_email.html'
    context = {'account': account,
               'protocol_host': protocol_host}

    _send_user_email(user, subject, template, context,
                     "campaign added notification email",
                     "for campaign: '{}'".format(account),
                     inviting_user=inviting_user)


def send_admin_added_notification(user, account, protocol_host,
                                  inviting_user=None):
    """Sends an email to a client notifying them that they have been
     made an account admin

    If the user is new, they are invited to complete their registration.
    """
    # Generate the content for the email
    subject = "You are now an Account Administrator"
    template = 'authorize/emails/admin_added_email.html'
    context = {'protocol_host': protocol_host,
               'account': account}

    _send_user_email(user, subject, template, context,
                     "client added notification email",
                     inviting_user=inviting_user)


def impersonate_user_queryset(request):
    from .models import RevUpUser
    return RevUpUser.objects.exclude(
        Q(username__startswith="account_user_") |
        Q(username__regex=r"account_\d+_user"),
        password='', first_name='', last_name='',
        last_login=None, accepted_tos=None
    )


def create_revuptask_for_contact_source(contact_source, task_type):
    """This method will great a new revup task using either a contact_set or
    a contact. """
    task_kwargs = dict(
        user=contact_source.user,
        task_type=task_type,
        revoked=False)
    try:
        task = RevUpTask.objects.filter_by_generic_obj(
            contact_source).get(**task_kwargs)
        # If this task already exists and isn't finished, we need to reject
        if task.finished():
            task.delete()
        else:
            raise RuntimeError("This task is already running, the task_type"
                               "is {}, the contact_set is {} and the user is "
                               "{}".
                               format(task_type, contact_source,
                                      contact_source.user))
    except RevUpTask.DoesNotExist:
        pass
    return RevUpTask.objects.create(
        task_id=uuid(), contact_source=contact_source, **task_kwargs)
