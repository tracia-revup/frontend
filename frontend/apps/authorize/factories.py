from datetime import datetime
from collections import Iterable

import factory
from factory.fuzzy import FuzzyText, FuzzyChoice
from django.contrib.auth.models import Group
from social_django.models import UserSocialAuth

from frontend.apps.authorize.models import (
    RevUpUser, RevUpUserToken, AnalysisViewUserSettings, ExternalSignupToken,
    AnalysisViewSettingSet, RevUpTask)
from frontend.libs.utils.factory_utils import FuzzyEmail, M2MMixin, FuzzyPhone
from frontend.apps.authorize.settingset_controllers import PoliticalSettingSet
from frontend.apps.analysis.factories import AnalysisConfigModelFactory


@factory.use_strategy(factory.CREATE_STRATEGY)
class RevUpUserFactory(factory.DjangoModelFactory, M2MMixin):
    class Meta:
        model = RevUpUser

    username = FuzzyEmail()
    first_name = FuzzyText()
    last_name = FuzzyText()
    email = factory.LazyAttribute(lambda x: x.username)
    accepted_tos = factory.LazyAttribute(lambda x: datetime.utcnow())
    import_config = factory.SubFactory(
        'frontend.apps.contact.factories.ImportConfigFactory')

    street_1 = "123 Main St"
    city = "Redwood City"
    state = "CA"
    zip = "94061"
    phone = "555-555-5555"

    @factory.post_generation
    def events(self, create, extracted, **kwargs):
        if extracted:
            self.add_events(*extracted)

    @factory.post_generation
    def groups(self, create, extracted, **kwargs):
        if extracted:
            RevUpUserFactory._m2m(create, extracted, self.groups)

    @factory.lazy_attribute
    def password(self):
        from django.contrib.auth.hashers import make_password
        return make_password("this_Is_a_Test_Password1")

    @factory.post_generation
    def applied_filters(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted is not None:
            if not isinstance(extracted, Iterable):
                extracted = [extracted]
            self.applied_filters.add(*extracted)

    @classmethod
    def _prepare(cls, *args, **kwargs):
        # Events were either created or provided, let's set the current_event.
        if 'current_event' not in kwargs:
            try:
                kwargs['current_event'] = kwargs.get('events', [])[0]
            except IndexError:
                kwargs['current_event'] = None
        return super(RevUpUserFactory, cls)._prepare(*args, **kwargs)

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        user = super(RevUpUserFactory, cls)._create(model_class,
                                                    *args, **kwargs)
        # We need to also create a social user
        UserSocialAuth.create_social_auth(user, user.username, "email")

        return user


class ExternalSignupTokenFactory(factory.DjangoModelFactory):
    class Meta:
        model = ExternalSignupToken

    first_name = FuzzyText()
    last_name = FuzzyText()
    email = FuzzyEmail()
    phone = FuzzyPhone()
    organization = FuzzyText()
    product = factory.SubFactory("frontend.apps.campaign.factories."
                                 "ProductFactory")
    profile = factory.SubFactory("frontend.apps.campaign.factories."
                                 "AccountProfileFactory")


class GroupFactory(factory.DjangoModelFactory):
    class Meta:
        model = Group

    name = FuzzyText()

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        # May already exist, fail gracefully
        return model_class.objects.get_or_create(*args, **kwargs)[0]


class RevUpUserTokenFactory(factory.DjangoModelFactory):
    class Meta:
        model = RevUpUserToken

    user = factory.SubFactory(RevUpUserFactory)


class UserSocialAuthFactory(factory.DjangoModelFactory):
    class Meta:
        model = UserSocialAuth

    user = factory.SubFactory(RevUpUserFactory)
    provider = "email"
    uid = FuzzyText()
    extra_data = factory.LazyAttribute(lambda x: {})


class AnalysisViewSettingSetFactory(AnalysisConfigModelFactory):
    class Meta:
        model = AnalysisViewSettingSet

    dynamic_class = PoliticalSettingSet


class AnalysisViewUserSettingsFactory(factory.DjangoModelFactory):
    class Meta:
        model = AnalysisViewUserSettings

    user = factory.SubFactory(RevUpUserFactory)
    analysis_profile = factory.SubFactory(
        'frontend.apps.analysis.factories.AnalysisProfileFactory')


class RevUpTaskFactory(factory.DjangoModelFactory):
    class Meta:
        model = RevUpTask
    user = factory.SubFactory(RevUpUserFactory)
    label = FuzzyText()
    task_id = FuzzyText()
