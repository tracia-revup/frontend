from django.contrib.auth import authenticate, login
from social_core.exceptions import AuthFailed


def _raise_auth_failed(backend, msg='Invalid username/password.'):
    raise AuthFailed(backend, msg)


def verify_login(strategy, is_new=False, auth_path=None, *args, **kwargs):
    """If this is a login, verify the user is real before continuing.

    Since we are using social-auth-pipeline for both registration and
    login, we have an issue where logins with bad users are being saved
    as new users. The purpose of this pipeline component is to ensure
    logins are only for existing users.
    """
    backend = kwargs.get('backend')
    # We're only doing this verification for email right now.
    # Check if this is the login path (vs. register path)
    if backend and backend.name == 'email' and is_new and auth_path == 'login':
        # If we get here, it means this is a login attempt with a bad
        # username/password, so we need to error
        _raise_auth_failed(backend)


def handle_inactive_user(strategy, user, auth_path=None, *args, **kwargs):
    """If a registering user is inactive, we treat them as a new user."""
    backend = kwargs.get('backend')
    if backend and backend.name == 'email' and user and not user.is_active:
        if auth_path == 'register':
            return {'is_new': True}
        else:
            # Inactive users are rejected unless they are registering
            _raise_auth_failed(backend)


def user_password(strategy, user, is_new=False, *args, **kwargs):
    backend = kwargs.get('backend')
    if not backend or backend.name != 'email':
        return
    password = strategy.request_data()['password']
    if is_new:
        user.set_password(password)
        user.save()
        # Log in the new user so they don't have to reenter their password
        user.backend = "{}.{}".format(backend.__module__,
                                      backend.__class__.__name__)
        login(strategy.request, user)
    else:
        if not user.check_password(password):
            _raise_auth_failed(backend)


def user_update(strategy, user, is_new=False, *args, **kwargs):
    backend = kwargs.get('backend')
    if not backend or backend.name != 'email':
        return
    if is_new:
        request_data = strategy.request_data()
        user.first_name = request_data['first_name']
        user.last_name = request_data['last_name']
        user.street_1 = request_data['street_1']
        user.street_2 = request_data['street_2']
        user.city = request_data['city']
        user.state = request_data['state']
        user.zip = request_data['zip']
        user.phone = request_data['phone']
        user.is_active = True
        user.save()

        # Remove any registration tokens, if they exist
        from frontend.apps.authorize.models import RevUpUserToken
        user.clean_tokens(RevUpUserToken.TokenTypeChoices.REGISTRATION)
    return


def revoke_tokens(strategy, entries, *args, **kwargs):
    """Revoke tokens from the external service, if possible."""
    def revoke(backend_, entry_):
        backend_.revoke_token(entry_.extra_data['access_token'], entry_.uid)

    revoke_tokens = strategy.setting('REVOKE_TOKENS_ON_DISCONNECT', False)
    if revoke_tokens:
        for entry in entries:
            if 'access_token' in entry.extra_data:
                backend = entry.get_backend(strategy)(strategy)
                # First we try to revoke the token normally.
                # If that fails, we try to refresh the token and revoke again.
                # If that fails, we give-up and continue. The UserSocialAuth
                # will be deleted without revoking on the app side. This
                # is probably the best we can do.
                try:
                    revoke(backend, entry)
                # We use a catch-all because we don't want this to fail.
                # At a minimum, the UserSocialAuth should be deleted down the
                # pipeline.
                except Exception:
                    try:
                        # Typically, the revoke fails because the access token
                        # has expired. This resolves that.
                        entry.refresh_token(strategy)
                        revoke(backend, entry)
                    except Exception:
                        continue
