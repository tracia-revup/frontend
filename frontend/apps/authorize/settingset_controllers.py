
from frontend.libs.utils.setting_utils import AbstractConfigSetController


class SettingSetController(AbstractConfigSetController):
    """Defines an interface for analysis display configuration questions."""
    def get_info(self, setting_set, analysis_profile,
                 user_settings=None, **kwargs):
        if hasattr(user_settings, 'pk'):
            user_settings = user_settings.pk
        return dict(
            title=setting_set.title,
            analysis_profile_id=analysis_profile.id,
            user_settings_id=user_settings,
            fields=self.get_fields(**kwargs),
        )

    def process_data(self, data):
        """Iterate over the given data and exclude any invalid fields."""
        all_expected = self.get_expected()
        valid_fields = set()
        for expected in all_expected:
            for item in expected:
                field = item.get("field")
                if field:
                    valid_fields.add(field)

        valid_data = {}
        for k, v in data.items():
            if k in valid_fields:
                valid_data[k] = v
        return valid_data


class PoliticalSettingSet(SettingSetController):
    def get_fields(self, **kwargs):
        return [dict(
            label="Show political colors",
            type="checkbox",
            expected=[
                dict(field="political_colors", default=True, type="boolean"),
            ],
        ),dict(
            label="Display maxed out donors",
            type="checkbox",
            expected=[
                dict(field="display_maxed_donors", default=True, type="boolean"),
            ],
        )]


class AcademicSettingSet(SettingSetController):
    def get_fields(self, **kwargs):
        return [dict(
            label="Show political colors",
            type="checkbox",
            expected=[
                dict(field="political_colors", default=True, type="boolean"),
            ],
        )]


class NonProfitSettingSet(SettingSetController):
    def get_fields(self, **kwargs):
        return [dict(
            label="Show political party colors",
            type="checkbox",
            expected=[
                dict(field="political_colors", default=False, type="boolean"),
            ],
        )]
