from datetime import date
import json
import logging

from axes.decorators import axes_dispatch
from django.contrib import messages
from django.contrib.auth import (logout, authenticate, login,
                                 update_session_auth_hash)
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import AnonymousUser
from django.contrib.auth.views import (
    password_reset, password_reset_done, password_reset_confirm,
    password_change_done)
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import send_mail
from django.http import (
    Http404, HttpResponseRedirect, HttpResponsePermanentRedirect, HttpResponse)
from django.shortcuts import redirect, render
from django.template import loader
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic import View
from social_django.views import complete

from frontend.apps.authorize.forms import (
    EmailRegistrationForm, RevUpUserForm, PasswordResetFormWrapper,
    SetPasswordFormWrapper, PasswordChangeFormWrapper)
from frontend.apps.authorize.models import RevUpUserToken, RevUpUser
from frontend.apps.campaign.routes import ROUTES
from frontend.libs.django_view_router import RoutingView, RoutingFormView


LOGGER = logging.getLogger(__name__)


# noinspection PyMethodMayBeStatic
class RegisterView(View):
    def __init__(self, **kwargs):
        super(RegisterView, self).__init__(**kwargs)
        self.object = None

    def _generate_response(self, request, form):
        return render(request, template_name='authorize/register.html',
                                  context={'form': form,
                                           'user': request.user,
                                           'request': request})

    def dispatch(self, request, r_uuid=None, *args, **kwargs):
        # If a uuid is given, that means this user is partially registered
        if r_uuid:
            token_type = RevUpUserToken.TokenTypeChoices.REGISTRATION
            try:
                token = RevUpUserToken.objects.get(token=r_uuid,
                                                    token_type=token_type)
            except RevUpUserToken.DoesNotExist:
                if isinstance(request.user, AnonymousUser):
                    return render(request, template_name='authorize/invalid_invitation.html',
                                              context={'user': request.user,
                                                       'request': request})
                else:
                    return HttpResponsePermanentRedirect(reverse('account_login'))

            if token.is_expired and not token.user.is_active:
                # Registration token has expired. Cancel registration.
                token.delete()
                return render(request, template_name='authorize/expired_invitation.html',
                                          context={'user': request.user,
                                                   'request': request})
            elif token.user.is_active:
                # The user has already registered
                token.delete()
                return HttpResponsePermanentRedirect(reverse('account_login'))
            else:
                self.object = token.user
        else:
            # Open registration is closed
            return self._generate_response(request, None)
        return super(RegisterView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        form = EmailRegistrationForm(self.object)
        return self._generate_response(request, form)

    def post(self, request, *args, **kwargs):
        # Create a form instance and populate it with data from the request:
        form = EmailRegistrationForm(self.object, request.POST)
        # check whether it's valid:
        if form.is_valid():
            # If this user is trying to register as another user while still
            # logged-in, we need to log them out.
            if isinstance(request.user, RevUpUser):
                logout(request)

            request.user = self.object
            kwargs["auth_path"] = "register"
            # We need to "hack" the complete method to redirect to the
            # right place after registration
            if "next" not in request.GET and "next" not in request.POST:
                # Queryset is immutable unless copied
                request.GET = request.GET.copy()
                request.GET['next'] = "/"
            return complete(request, "email", *args, **kwargs)
        else:
            return self._generate_response(request, form)


class UserEditView(RoutingFormView):
    routes = ROUTES
    form_class = RevUpUserForm
    model = RevUpUser
    template_name = "authorize/edit_user.html"

    def get_success_url(self):
        return reverse('index')

    @method_decorator(login_required)
    def do_dispatch(self, request, *args, **kwargs):
        self.object = request.user
        return super(UserEditView, self).do_dispatch(request, *args, **kwargs)

    def get_initial(self):
        """
        Returns the initial data to use for forms on this view.
        """
        if not self.object:
            return super(UserEditView, self).get_initial()

        return {"first_name": self.object.first_name,
                "last_name": self.object.last_name,
                "phone": self.object.phone,
                "street_1": self.object.street_1,
                "street_2": self.object.street_2,
                "city": self.object.city,
                "state": self.object.state,
                "zip": self.object.zip}

    def form_valid(self, form):
        # Assign the form values to the user object
        for field, value in form.cleaned_data.items():
            setattr(self.object, field, value)
        self.object.save()
        return super(UserEditView, self).form_valid(form)


# noinspection PyMethodMayBeStatic
@method_decorator(axes_dispatch, name='dispatch')
class LoginView(View):
    def get(self, request, status=200, *args, **kwargs):
        if request.user.is_authenticated:
            redirect_url = request.GET.get("next", "index")
            return redirect(redirect_url)
        else:
            return render(request=request, template_name='authorize/login.html',
                          context={'next': request.GET.get('next'),
                                   'email': request.POST.get('email', "")},
                          status=status)

    def post(self, request, *args, **kwargs):
        email = request.POST.get('email')
        password = request.POST.get('password')

        if email and password:
            user = authenticate(request=request, email=email, password=password)
            if user is not None:
                # The password verified for the user
                if user.is_active:
                    # Register the login with the session
                    login(request, user)
                    return redirect(request.GET.get('next', reverse('index')))
                else:
                    messages.error(request, "This account has been disabled")
            else:
                # Bad user and/or password
                messages.error(request, "Invalid email/password.")
        return self.get(request, status=401, *args, **kwargs)


@csrf_exempt
def password_reset_wrapper(request, *args, **kwargs):
    template_name = 'authorize/password_reset_form.html'
    email_template_name = 'authorize/password_reset_email.html'
    password_reset_form = PasswordResetFormWrapper

    if request.method == "POST":
        redirect = False
        if request.META.get("CONTENT_TYPE") == "application/json":
            data = json.loads(request.body)
        else:
            data = request.POST
            redirect = True

        form = password_reset_form(data)
        if form.is_valid():
            form.save(
                request=request,
                use_https=request.is_secure(),
                email_template_name=email_template_name,
                domain_override=request.get_host(),
            )
            if redirect:
                return HttpResponseRedirect(reverse('password_reset_done'))
            else:
                return HttpResponse("Email Sent")

    return password_reset(
        request,
        template_name=template_name,
        email_template_name=email_template_name,
        password_reset_form=password_reset_form,
        *args, **kwargs)


class PasswordResetDoneWrapperView(RoutingView):
    routes = ROUTES

    def get(self, request, *args, **kwargs):
        # noinspection PyArgumentList
        return password_reset_done(
            request,
            template_name='authorize/password_reset_done.html',
            *args, **kwargs)


def password_reset_confirm_wrapper(request, *args, **kwargs):
    return password_reset_confirm(
        request,
        template_name='authorize/password_reset_confirm.html',
        set_password_form=SetPasswordFormWrapper,
        *args, **kwargs)


def password_change_done_wrapper(request, *args, **kwargs):
    return password_change_done(
        request,
        template_name='authorize/password_change_done.html',
        *args, **kwargs)


def _password_changed_email(request, use_https=False, domain_override=None,
                            from_email=None):
    user = request.user
    if not domain_override:
        current_site = get_current_site(request)
        site_name = current_site.name
        domain = current_site.domain
    else:
        site_name = domain = domain_override
    c = {
        'email': user.email,
        'domain': domain,
        'site_name': site_name,
        'user': user,
        'protocol': 'https' if use_https else 'http',
    }
    subject = "Password change on {site_name}".format(**c)
    email = loader.render_to_string('authorize/password_change_email.html', c)
    send_mail(subject, email, from_email, [user.email])


@sensitive_post_parameters()
@csrf_protect
@login_required
def password_change_wrapper(request, *args, **kwargs):
    if request.method == "POST":
        form = PasswordChangeFormWrapper(user=request.user, data=request.POST)
        if form.is_valid():
            form.save()
            _password_changed_email(request, use_https=True)
            # TODO: send email
            # Updating the password logs out all other sessions for the user
            # except the current one if
            # django.contrib.auth.middleware.SessionAuthenticationMiddleware
            # is enabled.
            update_session_auth_hash(request, form.user)
            return redirect(reverse('password_change_done'))
    else:
        form = PasswordChangeFormWrapper(user=request.user)
    password_expiration_date = request.user.password_expiration_date
    if password_expiration_date:
        password_expired = date.today() >= password_expiration_date
    else:
        password_expired = False
    context = {
        'form': form,
        'title': 'Password change',
        'password_expired': password_expired,
    }
    if 'extra_context' in kwargs and kwargs['extra_context']:
        context.update(kwargs['extra_context'])
    return render(request=request,
                  template_name='authorize/password_change_form.html',
                  context=context)


def password_reset_complete_wrapper(request, *args, **kwargs):
    return password_reset(
        request,
        template_name='authorize/password_reset_complete.html',
        *args, **kwargs)


def password_change(request, *args, **kwargs):
    # simulate filling in the password reset form and save it
    form = PasswordResetFormWrapper({'email': request.user.email})
    if not form.is_valid():
        LOGGER.error("User: '{}' failed to initiate password change"
                     .format(request.user))
        raise Http404
    form.save(
        email_template_name='authorize/password_reset_email.html',
        request=request,
        use_https=request.is_secure(),
    )
    return redirect(reverse('password_reset_done'))
