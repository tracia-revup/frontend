from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator


from frontend.apps.campaign.routes import ROUTES
from frontend.apps.role.decorators import any_group_required
from frontend.apps.role.permissions import AdminPermissions
from frontend.libs.django_view_router import RoutingTemplateView


class PersonasView(RoutingTemplateView):
    routes = ROUTES
    template_name = 'personas/personas.html'

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        return super(PersonasView, self).get(request, *args, **kwargs)
