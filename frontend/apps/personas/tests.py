from datetime import datetime, timedelta

from django.urls import reverse
from unittest import mock

from frontend.apps.analysis.factories import (
    FeatureFactory, AnalysisFactory, AnalysisConfigFactory)
from frontend.apps.authorize.test_utils import AuthorizedUserTestCase, TestCase
from frontend.apps.campaign.factories import (
    AccountFactory, NonprofitCampaignFactory)
from frontend.apps.contact_set.factories import ContactSetFactory
from frontend.apps.contact_set.models import ContactSet
from frontend.apps.personas.factories import (
    PersonaFactory, PersonaInstanceFactory)
from frontend.apps.personas.dynamic_form_controllers import (
    MajorGiftsPersonaFormController)
from frontend.apps.personas.models import PersonaInstance
from frontend.apps.role.permissions import AdminPermissions
from frontend.apps.seat.factories import SeatFactory


class PersonasViewTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        # Calling super here will create a RevUpUser and log it in.
        super(PersonasViewTests, self).setUp(login_now=False, **kwargs)
        self.account = self.user.current_seat.account
        self.account_user = self.account.account_user
        self.url = reverse("personas")

    def test_permissions(self, *args):
        # Verify the user is redirected because they are not logged in
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

        # Log in
        self._login(self.user)

        # Verify user has permission after login
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)


class PersonaInstanceModelTests(TestCase):
    def setUp(self):
        self.feature = FeatureFactory(title="Major Gifts Persona")
        self.account = AccountFactory()
        self.persona = PersonaFactory(feature=self.feature,
                                      key="MGP")

    def test_identifier(self):
        kwargs = dict(account=self.account, persona=self.persona,
                      title="Test Persona")

        ## Verify the identifier field is filled upon creating a persona
        p = PersonaInstance.objects.create(**kwargs)
        id_ = p.identifier
        assert f"MGP_{p.id}" == id_

        ## Verify saving that persona again does not change the identifier
        p.title = "Some other Title"
        p.save()
        assert p.identifier == id_
        # The Feature title is used in the identifier, but it shouldn't change
        # once created.
        p.feature = FeatureFactory()
        p.save()
        assert p.identifier == id_

        ## Verify when creating a Persona, if the identifier is already set it
        ## won't be changed
        kwargs["identifier"] = "abcdefg"
        p = PersonaInstance.objects.create(**kwargs)
        assert "abcdefg" == p.identifier

    def test_create(self):
        title = "Test PI"
        source_cs = ContactSetFactory(account=self.account,
                                      user=self.account.account_user)

        # Verify PersonaInstance fails to create with inactive persona
        self.persona.active = False
        self.assertRaises(AssertionError, PersonaInstance.create,
                          title, self.persona, self.account, source_cs)

        # Verify PersonaInstance is created
        self.persona.active = True
        pi, cs = PersonaInstance.create(title, self.persona, self.account,
                                        source_cs)
        # Verify the PersonaInstance was created correctly
        assert title == pi.title
        assert self.persona == pi.persona
        assert self.account == pi.account
        assert f"{pi.persona.key}_{pi.id}" == pi.identifier

        # Verify the ContactSet was created correctly
        assert title == cs.title
        assert self.account.account_user == cs.user
        assert self.account == cs.account
        assert "PR" == cs.source
        assert pi.identifier == cs.source_uid
        assert not cs.deletable


class PersonaViewSetTests(AuthorizedUserTestCase):
    def setUp(self):
        super().setUp(login_now=False)
        self.feature = FeatureFactory(title="Major Gifts Persona")
        self.account = AccountFactory()
        self.persona = PersonaFactory(feature=self.feature)
        self.url = reverse("account_personas_api-list", args=[self.account.id])

    def test_permissions(self):
        # Verify logged out user is rejected
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)

        # Logged in user should still be rejected because they are not a
        # member of the account
        self._login()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)

        # Add the user to the account and verify access is granted
        self.user.current_seat = SeatFactory(account=self.account)
        self.user.save()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)


class PersonaInstanceViewSetTests(AuthorizedUserTestCase):
    def setUp(self):
        super().setUp(login_now=False)
        self.feature = FeatureFactory(title="Major Gifts Persona")
        self.account = AccountFactory()
        self.persona = PersonaFactory(feature=self.feature)
        self.url = reverse("account_persona_instance_api-list",
                           args=[self.account.id])

    def test_permissions(self):
        # Verify logged out user is rejected
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)

        # Logged in user should still be rejected because they are not a
        # member of the account
        self._login()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)

        # Add the user to the account, verify they have access to view
        seat = SeatFactory(account=self.account)
        self.user.current_seat = seat
        self.user.save()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

        # Verify user does not have permission to post
        assert AdminPermissions.MODIFY_PERSONAS not in seat.permissions
        response = self.client.post(self.url, data={})
        self.assertEqual(response.status_code, 403)

        # Add admin group. The user should be able to post now
        seat.permissions = [AdminPermissions.MODIFY_PERSONAS]
        seat.save()
        response = self.client.post(self.url, data={})
        self.assertContains(response, "required to save a new persona",
                            status_code=500)

    def test_create(self):
        # Setup the test
        seat = SeatFactory(account=self.account)
        self.user.current_seat = seat
        self.user.save()
        seat.permissions = [AdminPermissions.MODIFY_PERSONAS]
        seat.save()
        self._login()
        contact_set = ContactSetFactory(account=self.account,
                                        user=self.account.account_user)

        with mock.patch("frontend.apps.personas.api.views.submit_analysis") as m:
            assert not PersonaInstance.objects.filter(
                persona=self.persona).exists()

            title = "test persona api"
            response = self.client.post(self.url, data=dict(
                source=contact_set.id, title=title, persona=self.persona.id
            ))

            # Verify response
            self.assertContains(response, title)

            # Verify a persona instance and contactset is created
            persona_instance = PersonaInstance.objects.get(
                title=title, persona=self.persona)
            contact_set = ContactSet.objects.get(
                title=title, source_uid=persona_instance.identifier)

            # Verify the analysis is triggered
            m.assert_called_with(self.account.account_user, self.account,
                                 contact_source=contact_set, countdown=2)

    def test_serializer(self):
        # Setup the test
        seat = SeatFactory(account=self.account)
        self.user.current_seat = seat
        self.user.save()
        self._login()

        ## Verify the simple case of no results
        response = self.client.get(self.url)
        assert response.json()['results'] == []

        ## Verify the correct timestamp and contactset are returned
        persona_instance = PersonaInstanceFactory(persona=self.persona,
                                                  account=self.account)
        assert persona_instance.identifier != ""

        analysis1 = AnalysisFactory(
            account=self.account, user=self.account.account_user,
            last_completed=datetime.now()-timedelta(days=2))
        contact_set = ContactSetFactory(account=self.account,
                                        user=self.account.account_user,
                                        source="PR",
                                        source_uid=persona_instance.identifier,
                                        analysis=analysis1)

        # Verify a basic test with simple setup
        results = self.client.get(self.url).json()['results']
        assert len(results) == 1
        results = results[0]
        assert results['contact_set'] == contact_set.id
        assert results['last_analyzed'] == analysis1.last_completed.isoformat()

        ## Verify the correct values are returned with a more complex setup,
        ## with multiple possible contactsets and analyses

        # This analysis is more recent than analysis1, but is not linked
        # through a persona contactset
        analysis2 = AnalysisFactory(
            account=self.account, user=self.account.account_user,
            last_completed=datetime.now())
        # This analysis is linked through a different persona contactset
        analysis2 = AnalysisFactory(
            account=self.account, user=self.account.account_user,
            last_completed=datetime.now() - timedelta(days=1))
        contact_set2 = ContactSetFactory(account=self.account,
                                         user=self.account.account_user,
                                         source="PR",
                                         source_uid="some-other-identifier",
                                         analysis=analysis2)

        results = self.client.get(self.url).json()['results']
        assert len(results) == 1
        results = results[0]
        assert results['contact_set'] == contact_set.id
        assert results['last_analyzed'] == analysis1.last_completed.isoformat()

        ## Verify adding another PersonaInstance for contact_set2 includes it
        persona_instance2 = PersonaInstanceFactory(persona=self.persona,
                                                   account=self.account)
        contact_set2.source_uid = persona_instance2.identifier
        contact_set2.save()

        response = self.client.get(self.url)
        assert len(response.json()['results']) == 2
        self.assertContains(response, persona_instance.id)
        self.assertContains(response, persona_instance2.id)


class MajorGiftsPersonaFormControllerTests(TestCase):
    def setUp(self):
        self.mgr = MajorGiftsPersonaFormController()
        self.account = NonprofitCampaignFactory()
        self.analysis_config = AnalysisConfigFactory(account=self.account)

    def test_get_fields(self):
        """Tests the return of `get_fields` method in
        'MajorGiftsRangesPersona'
        """
        fields = self.mgr.get_fields(self.account.id, self.analysis_config.id)
        expected = [dict(label="Major Gifts Threshold",
                         type="int",
                         required=False,
                         expected=[
                             dict(field="floor", type="int")
                         ]),
        ]
        self.assertEqual(fields, expected)

    def test_process_data(self):
        """Test `process_data` when ranges were supplied in the data."""
        test_data = {
            "some": "other",
            "junk": "that",
            "shouldn't": "be returned",
            'floor': 1,
            'ceiling': 500
        }
        data = self.mgr.process_data(test_data)
        self.assertEqual(data, {'floor': 1, 'ceiling': 500})

    def test_process_data_2(self):
        """Test `process_data` when no floor was supplied."""
        test_data = {
            "some": "other",
            "junk": "that",
            "shouldn't": "be returned",
            "is_ally": False,
        }
        data = self.mgr.process_data(test_data)
        self.assertEqual(data, {'floor': 0, 'ceiling': float('inf')})
