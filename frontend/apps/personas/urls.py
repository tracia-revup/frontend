from django.conf.urls import url

from frontend.apps.personas.views import PersonasView

urlpatterns = [
    url(r'^$', PersonasView.as_view(), name='personas'),
]
