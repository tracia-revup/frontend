
import factory
from factory.fuzzy import FuzzyText

from .models import PersonaInstance, Persona


@factory.use_strategy(factory.CREATE_STRATEGY)
class PersonaFactory(factory.DjangoModelFactory):
    class Meta:
        model = Persona

    feature = factory.SubFactory("frontend.apps.analysis."
                                 "factories.FeatureFactory")
    key = FuzzyText(length=5)


@factory.use_strategy(factory.CREATE_STRATEGY)
class PersonaInstanceFactory(factory.DjangoModelFactory):
    class Meta:
        model = PersonaInstance

    persona = factory.SubFactory(PersonaFactory)
    account = factory.SubFactory("campaign.Account")
    identifier = FuzzyText(length=5)
