
from django.contrib import admin

from .models import Persona, PersonaInstance


class PersonaAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'group', 'key', 'label')


class PersonaInstanceAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'account', 'identifier')
    list_select_related = ('account', 'persona')
    search_fields = ('account__organization_name',
                     'persona__title', 'persona__key')


admin.site.register(Persona, PersonaAdmin)
admin.site.register(PersonaInstance, PersonaInstanceAdmin)
