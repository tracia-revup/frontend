
from django.db import models, transaction
from django_extensions.db.models import TimeStampedModel, TitleDescriptionModel

from frontend.apps.contact_set.models import ContactSet
from frontend.libs.utils.model_utils import DynamicFormBase, ConfigModelBase


class Persona(TimeStampedModel, TitleDescriptionModel):
    """Defines all available Personas for configuration and processing.

    The associated Feature is the Feature that will do the analysis for this
    Persona. The QuestionSet is an optional form that allows the user to
    provide more configuration. E.g. Major Gifts giving threshold
    """
    active = models.BooleanField(blank=True, default=True)
    feature = models.ForeignKey(
        "analysis.Feature", on_delete=models.PROTECT, null=True,
        help_text="This field should be treated as if it is virtually "
                  "immutable once a Feature has been assigned. The feature is "
                  "used to generate the 'identifier'.")
    key = models.CharField(max_length=8, db_index=True, unique=True)
    group = models.CharField(
        max_length=32, blank=True,
        help_text="This defines the group this Persona is in so the UI can be "
                  "built dynamically")
    label = models.CharField(
        max_length=32, blank=True,
        help_text="This defines the label for this Persona. This is meant "
                  "to be used for the button on the configuration of Personas,"
                  "but we may find other uses for it in the future.")
    config = models.ForeignKey("PersonaDynamicForm", blank=True, null=True,
                               default=None, on_delete=models.SET_NULL)

    def __str__(self):
        return f"({self.id}) {self.title} - {self.key}"


class PersonaInstance(TimeStampedModel):
    """An individual configured Persona.

    Every time a user creates/configures a Persona, a PersonaInstance
    record will be created. It contains the configuration information
    and unique identifier for this specific instance of a Persona.
    """
    title = models.CharField(max_length=256)
    persona = models.ForeignKey('Persona', related_name="persona_configs",
                                on_delete=models.PROTECT)
    account = models.ForeignKey('campaign.Account', on_delete=models.CASCADE,
                                null=False)
    identifier = models.CharField(max_length=32, blank=True,
                                  db_index=True, unique=True)

    def __str__(self):
        return f"({self.id}) {self.title} - {self.identifier}"

    def _generate_identifier(self):
        """Generate the unique identifier for this Persona.
        Once set, it should not be changed. This identifier will be referenced
        in other places. E.g. ContactSet
        """
        self.identifier = f"{self.persona.key}_{self.id}"

    @classmethod
    def generate_identifier(cls, instance, **kwargs):
        """This utility function is meant to be used via the post_save signal.
        Post-save is necessary because _generate_identifier needs an ID,
        which isn't available before create.
        """
        # If the identifier is already set, we don't want to change it. The
        # typical case would be because this is an update of an existing
        # instance, but it's also possible it was given an intentional value
        # during Persona creation.
        if not instance.identifier:
            instance._generate_identifier()
            instance.save()

    @classmethod
    @transaction.atomic
    def create(cls, title, persona, account, source_cs):
        """Create a PersonaInstance and all related objects

        :param title: The title of the PersonaInstance and related ContactSet
        :param persona: The parent Persona to attach
        :param account: The owning Account
        :param source_cs: The source ContactSet that defines the parent of
                          the generated ContactSet
        """
        assert persona.active

        # TODO: replace this with the new dynamicform model when ready
        # # Create a QuestionSetData first. If it fails, everything should.
        # if persona.question_set:
        #     pass

        # Create a PersonaInstance
        persona_instance = PersonaInstance.objects.create(
            title=title, persona=persona, account=account)

        # Create a ContactSet for the specific PersonaInstance
        contact_set = ContactSet.create(
            source_cs, title,
            user=account.account_user,
            account=account,
            source=ContactSet.Sources.PERSONA,
            source_uid=persona_instance.identifier,
            # TODO: Add query args for persona once new Filter exists
            # query_args={persona_instance.identifier: True}
            query_args={},
            deletable=False,
        )
        return persona_instance, contact_set


models.signals.post_save.connect(PersonaInstance.generate_identifier,
                                 sender=PersonaInstance)


class PersonaDynamicForm(ConfigModelBase, DynamicFormBase):
    """Similar to a QuestionSet, this model wraps a dynamic form controller"""
    pass
