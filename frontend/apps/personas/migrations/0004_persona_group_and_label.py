# Generated by Django 2.0.5 on 2019-07-12 15:34

from django.db import migrations, models


def add_group_to_major_gifts(apps, schema_editor):
    """Add the group and label "Major Gifts" to the Major Gifts Persona"""
    Persona = apps.get_model("personas", "Persona")
    Persona.objects.filter(key="mg").update(group="Major Gifts",
                                            label="Major Gifts")


class Migration(migrations.Migration):

    dependencies = [
        ('personas', '0003_create_major_gifts_persona'),
    ]

    operations = [
        migrations.AddField(
            model_name='persona',
            name='group',
            field=models.CharField(blank=True, help_text='This defines the group this Persona is in so the UI can be built dynamically', max_length=32),
        ),
        migrations.AddField(
            model_name='persona',
            name='label',
            field=models.CharField(blank=True,
                                   help_text='This defines the label for this Persona. This is meant to be used for the button on the configuration of Personas,but we may find other uses for it in the future.',
                                   max_length=32),
        ),
        migrations.RunPython(add_group_to_major_gifts, lambda x,y: True)
    ]
