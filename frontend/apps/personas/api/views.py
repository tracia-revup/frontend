
from django.db.models import Subquery, OuterRef
from django.shortcuts import get_object_or_404
from rest_condition.permissions import And, Or
from rest_framework import viewsets, exceptions, permissions, response
from rest_framework_extensions.utils import compose_parent_pk_kwarg_name

from frontend.apps.analysis.utils import submit_analysis
from frontend.apps.campaign.api.permissions import SelfAccount
from frontend.apps.campaign.models import Account
from frontend.apps.contact_set.models import ContactSet
from frontend.apps.personas.models import Persona, PersonaInstance
from frontend.apps.role.api.permissions import HasPermission
from frontend.apps.role.permissions import AdminPermissions
from frontend.libs.utils.api_utils import InitialMixin
from .serializers import PersonaSerializer, PersonaInstanceSerializer



class PersonaViewSet(InitialMixin, viewsets.ReadOnlyModelViewSet):
    parent_account_lookup = compose_parent_pk_kwarg_name("account_id")
    permission_classes = (
        And(permissions.IsAuthenticated,
            SelfAccount(url_id_field=parent_account_lookup)),
        )
    serializer_class = PersonaSerializer
    queryset = Persona.objects.all().order_by("-active")


class PersonaInstanceViewSet(InitialMixin, viewsets.ModelViewSet):
    parent_account_lookup = compose_parent_pk_kwarg_name("account_id")
    permission_classes = (
        And(permissions.IsAuthenticated,
            Or(SelfAccount(url_id_field=parent_account_lookup,
                           methods=('GET',)),
               HasPermission(parent_account_lookup,
                             AdminPermissions.MODIFY_PERSONAS,
                             methods=['POST', 'PUT', 'PATCH' 'DELETE'])
               )),
    )
    serializer_class = PersonaInstanceSerializer

    def get_queryset(self):
        """Annotate the PersonaInstance with the following:
           * The most recent 'last_completed' timestamp connected through the
              persona ContactSet.
           * The ID of the Persona ContactSet that was most recently analyzed
        """
        sub_q_base = ContactSet.objects.filter(
            source_uid=OuterRef("identifier"),
            analysis__last_completed__isnull=False,
            account=self.account).order_by("-analysis__last_completed")
        last_analyzed = sub_q_base.values("analysis__last_completed")
        contact_set_id = sub_q_base.values("id")

        return PersonaInstance.objects\
                .select_related("persona")\
                .filter(account=self.account)\
                .annotate(last_analyzed=Subquery(last_analyzed[0:1]),
                          contact_set_id=Subquery(contact_set_id[0:1]))

    def _initial(self):
        self.account = get_object_or_404(
            Account, pk=self.kwargs.get(self.parent_account_lookup))

    def create(self, request, *args, **kwargs):
        """Create a new Persona Configuration.
        Create a new ContactSet for the new Persona
        Trigger an analysis on the new ContactSet
        """
        # Extract required values from the post data
        temp_vars = []
        for field in ("source", "title", "persona"):
            temp = request.data.get(field)
            if temp:
                temp_vars.append(temp)
            else:
                raise exceptions.APIException(f"A {field} is required to save "
                                              "a new persona configuration.")
        source_id, title, persona_id = temp_vars

        # Get the related models
        source_cs = get_object_or_404(ContactSet, pk=source_id,
                                      account=self.account.id)
        persona = get_object_or_404(Persona, pk=persona_id)
        # Create the PersonaInstance and related models
        persona_instance, contact_set = PersonaInstance.create(
            title, persona, self.account, source_cs)

        # Trigger a new analysis only on the new ContactSet
        submit_analysis(self.account.account_user, self.account,
                        contact_source=contact_set, countdown=2)

        return response.Response(
            PersonaInstanceSerializer(persona_instance).data)
