
from rest_framework import serializers

from frontend.apps.personas.models import (
    PersonaInstance, Persona, PersonaDynamicForm)


class PersonaDynamicFormSerializer(serializers.ModelSerializer):
    form_fields = serializers.SerializerMethodField()

    class Meta:
        model = PersonaDynamicForm
        fields = ('id', 'title', 'description', 'allow_multiple', 'form_fields')

    def get_form_fields(self, dynamic_form):
        return dynamic_form.get_fields()


class PersonaSerializer(serializers.ModelSerializer):
    config = PersonaDynamicFormSerializer(read_only=True)

    class Meta:
        model = Persona
        fields = ('id', 'active', 'title', 'feature', 'key', 'group', 'label',
                  'config')


class PersonaInstanceSerializer(serializers.ModelSerializer):
    persona = PersonaSerializer(read_only=True)
    last_analyzed = serializers.SerializerMethodField()
    contact_set = serializers.SerializerMethodField()

    class Meta:
        model = PersonaInstance
        fields = ('id', 'title', 'persona', 'account', 'identifier',
                  'last_analyzed', 'contact_set')

    def get_last_analyzed(self, persona_instance):
        """Get the last_analyzed timestamp. This relies on an annotated
        query, so it can be missing; hence the exception handler.

        See PersonaInstanceViewSet for the annotated query.
        """
        try:
            return persona_instance.last_analyzed
        except AttributeError:
            return None

    def get_contact_set(self, persona_instance):
        """Get the contact_set_id. This relies on an annotated
        query, so it can be missing; hence the exception handler.

        See PersonaInstanceViewSet for the annotated query.
        """
        try:
            return persona_instance.contact_set_id
        except AttributeError:
            return None
