
from frontend.libs.utils.setting_utils import AbstractConfigSetController


class PersonaController(AbstractConfigSetController):
    """Defines an interface for analysis configuration Questions."""

    def get_info(self, record, **kwargs):
        return dict(
            id=record.id,
            title=record.title,
            label=record.label,
            allow_multiple=record.allow_multiple,
            fields=self.get_fields(**kwargs),
        )


class MajorGiftsPersonaFormController(PersonaController):
    """Controller for Adding Major Gifts Ranges Persona QuestionSet."""
    def get_fields(self, *args, **kwargs):
        return [
            dict(label="Major Gifts Threshold",
                 type="int",
                 required=False,
                 expected=[
                     dict(field="floor", type="int")
                 ]),
        ]

    def process_data(self, data):
        """Process the input data and make it ready for internal storage"""
        floor = int(data.get("floor", 0))

        ceiling = data.get("ceiling")
        # If no upper bound is provided, we assume infinity.
        if not ceiling:
            ceiling = float('inf')

        # Validate the floor is greater or equal to zero
        if floor < 0:
            raise ValueError("The range must be greater than zero")

        # Validate that the floor is less than the ceiling.
        # If not, switch them.
        if floor > ceiling:
            temp = ceiling
            ceiling = floor
            floor = temp

        return dict(floor=floor, ceiling=ceiling)