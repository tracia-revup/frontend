import re

from django.conf import settings
from django.contrib.auth.models import AnonymousUser


def version(request):
    """Extract the product version from the version file and add to the
    context."""
    product_version = settings.PRODUCT_VERSION
    product_release_date = settings.PRODUCT_RELEASE_DATE

    # In production, only show major version
    if not settings.DEBUG:
        product_version = re.sub('-.*', '', product_version)
    return {
        "product_version": product_version,
        "product_release_date": product_release_date,
    }


def permission_constants(request):
    from frontend.apps.role.permissions import (AdminPermissions,
                                                FundraiserPermissions)
    return {"AdminPermissions": AdminPermissions,
            "FundraiserPermissions": FundraiserPermissions}


def account_info(request):
    # Do nothing if user isn't logged in
    if isinstance(request.user, AnonymousUser):
        return {}

    account, account_profile = None, None
    try:
        account = request.user.current_seat.account
        account_profile = account.account_profile
    except Exception:
        # Generally bad to use catch-alls, but the default behavior of
        # django templates is to fail silently
        pass

    return {
        "account": account,
        "account_profile": account_profile
    }


def extract_settings(request):
    """Extract fields from the settings

    To access do {{ settings.<field> }}. This will help avoid collisions with
    other context values
    """
    try:
        fields = {
            export_key: getattr(settings, settings_key)
            for export_key, settings_key in settings.SETTINGS_CONTEXT_EXPORT.items()
        }

        return {
            "settings": fields
        }
    except Exception:
        return {}
