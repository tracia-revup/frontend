
from django.contrib.postgres.fields import JSONField
from django.db import models


class DeleteLog(models.Model):
    request_user = models.ForeignKey("authorize.RevUpUser",
                                     on_delete=models.CASCADE, null=True,
                                     related_name="requested_deletes")
    obj_user = models.ForeignKey("authorize.RevUpUser",
                                 on_delete=models.CASCADE, null=True,
                                 related_name="+")
    account = models.ForeignKey("campaign.Account",
                                on_delete=models.CASCADE, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    data = JSONField(blank=True, default={})

    @classmethod
    def delete_contact_set(cls, contact_set, request_user=None):
        dl = DeleteLog(request_user=request_user,
                       obj_user=contact_set.user,
                       account=contact_set.account)
        dl.data = {
            "model": "ContactSet",
            "label": contact_set.title,
            "count": contact_set.count,
            "source": contact_set.Sources.translate(contact_set.source),
            "source_detail": contact_set.source_uid
        }
        dl.save()
