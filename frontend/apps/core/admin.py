from django.contrib import admin

from .models import DeleteLog


class DeleteLogAdmin(admin.ModelAdmin):
    raw_id_fields = ("request_user", "obj_user", "account")
    list_display = ("id", "account", "info", "request_user", "obj_user")
    search_fields = ("request_user__email", "obj_user__email",
                     "account__organization_name")
    list_select_related = ("request_user", "obj_user", "account")

    def info(self, log):
        model = log.data.get("model", "")
        label = log.data.get("label", "")
        return "{}: {}".format(model, label)


admin.site.register(DeleteLog, DeleteLogAdmin)
