

def get_request_years(request):
    if request.method == 'GET' and 'years' in request.GET:
        try:
            years = int(request.GET.get('years', 2))
        except ValueError:
            years = 2
    elif 'years' in request.session:
        try:
            years = int(request.session['years'])
        except ValueError:
            years = 2
    else:
        years = 2
    request.session['years'] = years
    return years


def create_flags_factory(flags):
    def create_flags(apps, schema_editor):
        Flag = apps.get_model("waffle", "Flag")
        for flag in flags:
            Flag.objects.create(**flag)
    return create_flags


def undo_flags_factory(flags):
    def undo_flags(apps, schema_editor):
        Flag = apps.get_model("waffle", "Flag")
        for flag in flags:
            try:
                flag = Flag.objects.get(name=flag['name'])
                flag.delete()
            except Flag.DoesNotExist:
                pass
    return undo_flags
