# -*- coding: utf-8 -*-


from django.db import models, migrations


def create_splashscreen_flag(apps, schema_editor):
    Flag = apps.get_model("waffle", "Flag")
    RevUpUser = apps.get_model("authorize", "RevUpUser")
    Seat = apps.get_model("seat", "Seat")

    # Get all users with active seats
    users = RevUpUser.objects.filter(id__in=(
        s.user_id for s in Seat.objects.filter(state="AC")))

    flag = Flag.objects.create(
        name="Splash Screen",
        note="Control access to the new ranking page splash screen",
        superusers=False)
    # Add users with active seat to flag, so they will see the splash
    flag.users.add(*users)


def undo_splashscreen(apps, schema_editor):
    Flag = apps.get_model("waffle", "Flag")
    try:
        flag = Flag.objects.get(name="Splash Screen")
        flag.delete()
    except Flag.DoesNotExist:
        pass


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_add_rollout_flag'),
        ('authorize', "0001_initial"),
        ('seat', "0001_initial"),
    ]

    operations = [
        migrations.RunPython(create_splashscreen_flag,
                             undo_splashscreen)
    ]
