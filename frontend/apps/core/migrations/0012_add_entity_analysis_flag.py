


from django.db import migrations

from ..utils import create_flags_factory, undo_flags_factory


flags = [
    {
        'name': 'Entity Analysis Enabled',
        'note': 'Control whether we use the entity analysis runner',
        'everyone': None,
        'superusers': False,
    },
]


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0011_add_import_backend_export_flag'),
    ]

    operations = [
        migrations.RunPython(
            create_flags_factory(flags), undo_flags_factory(flags)
        )
    ]
