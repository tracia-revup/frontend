# -*- coding: utf-8 -*-


from django.conf import settings
from django.db import migrations

from ..utils import create_flags_factory, undo_flags_factory


flags = [
    {
        'name': 'Contact Import Name Expansion',
        'note': 'Control use of name expansion in contact import',
        'everyone': True if settings.DEBUG else None,
    },
    {
        'name': 'Analysis Name Expansion',
        'note': 'Control use of name expansion in contact analysis',
        'everyone': True if settings.DEBUG else None,
    }
]


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_add_campaign_config_flag'),
    ]

    operations = [
        migrations.RunPython(
            create_flags_factory(flags), undo_flags_factory(flags)
        )
    ]
