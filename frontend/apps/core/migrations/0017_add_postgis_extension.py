from django.contrib.postgres.operations import CreateExtension
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0016_featureflag_contrib_schema_check'),
    ]

    operations = [
        CreateExtension('postgis'),
    ]
