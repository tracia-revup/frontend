# -*- coding: utf-8 -*-


from django.conf import settings
from django.db import migrations

from ..utils import create_flags_factory, undo_flags_factory


flags = [
    {
        'name': 'Analysis Parallelization',
        'note': 'Control parallelization in contact analysis',
        'everyone': True if settings.DEBUG else None,
    },
]


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_add_alias_expansion_flags'),
    ]

    operations = [
        migrations.RunPython(
            create_flags_factory(flags), undo_flags_factory(flags)
        )
    ]
