# -*- coding: utf-8 -*-


from django.conf import settings
from django.db import models, migrations


flag_name = "Campaign Config"


def create_flag(apps, schema_editor):
    Flag = apps.get_model("waffle", "Flag")

    # If this on a dev environment, enable the feature automatically
    if settings.DEBUG:
        everyone = True
    else:
        everyone = None

    Flag.objects.create(
        name=flag_name,
        note="Control access to the campaign config in the admin",
        everyone=everyone)


def undo_flag(apps, schema_editor):
    Flag = apps.get_model("waffle", "Flag")
    try:
        flag = Flag.objects.get(name=flag_name)
        flag.delete()
    except Flag.DoesNotExist:
        pass


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20151117_1339'),
    ]

    operations = [
        migrations.RunPython(
            create_flag, undo_flag
        )
    ]
