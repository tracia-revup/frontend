# -*- coding: utf-8 -*-


from django.conf import settings
from django.db import models, migrations


def create_quickfilters_flag(apps, schema_editor):
    Flag = apps.get_model("waffle", "Flag")

    # If this on a dev environment, enable the feature automatically
    if settings.DEBUG:
        everyone = True
    else:
        everyone = None

    Flag.objects.create(
        name="Quick Filters",
        note="Control access to the quick filters on the ranking page",
        everyone=everyone)


def undo_quickfilters_flag(apps, schema_editor):
    Flag = apps.get_model("waffle", "Flag")
    try:
        flag = Flag.objects.get(name="Quick Filters")
        flag.delete()
    except Flag.DoesNotExist:
        pass


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20151030_1530'),
    ]

    operations = [
        migrations.RunPython(
            create_quickfilters_flag, undo_quickfilters_flag
        )
    ]
