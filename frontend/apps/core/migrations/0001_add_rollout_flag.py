# -*- coding: utf-8 -*-


from django.db import models, migrations


def create_analysis_rollout_flag(apps, schema_editor):
    Flag = apps.get_model("waffle", "Flag")
    Group = apps.get_model("auth", "Group")

    group, _ = Group.objects.get_or_create(name="New Ranking")

    flag = Flag.objects.create(
        name="New Ranking",
        note="Control access to the new ranking page")
    flag.groups.add(group)


def undo_analysis_rollout(apps, schema_editor):
    Flag = apps.get_model("waffle", "Flag")
    try:
        flag = Flag.objects.get(name="New Ranking")
        flag.delete()
    except Flag.DoesNotExist:
        pass


class Migration(migrations.Migration):

    dependencies = [
        ("waffle", "0001_initial")
    ]

    operations = [
        migrations.RunPython(create_analysis_rollout_flag,
                             undo_analysis_rollout)
    ]
