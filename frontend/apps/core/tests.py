from datetime import datetime
from random import randint
from unittest.mock import patch, MagicMock

from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from django.test import Client, RequestFactory
from django.test.utils import override_settings
from django.urls import reverse

from frontend.apps.analysis.factories import (
    AnalysisFactory, FeatureConfigFactory)
from frontend.apps.authorize.factories import (
    ExternalSignupTokenFactory, RevUpUserFactory)
from frontend.apps.authorize.models import RevUpUser
from frontend.apps.authorize.test_utils import AuthorizedUserTestCase
from frontend.apps.campaign.factories import PoliticalCampaignFactory
from frontend.apps.core.product_translations import (reverse_translate,
                                                     translate)
from frontend.apps.core.templatetags.committees import parse_committee
from frontend.apps.core.views import RecurlyWebhook
from frontend.apps.campaign.factories import (
    ProductFactory, AccountProfileFactory, AccountFactory)
from frontend.apps.campaign.models import Account, AccountContract
from frontend.apps.campaign.models.base import ACCOUNT_KEY_LENGTH
from frontend.apps.role.permissions import (AdminPermissions)
from frontend.apps.seat.factories import SeatFactory
from frontend.apps.seat.models import Seat
from frontend.libs.utils.test_utils import TestCase


class TermsOfServiceTests(AuthorizedUserTestCase):
    def test_terms_of_service_redirect(self):
        # Test user has not accepted Terms of Service yet
        self.user.accepted_tos = None
        self.user.save()
        self.assertFalse(self.user.accepted_tos)

        url_string = "{}?next={}"
        index = reverse('index')
        tos = reverse('terms_of_service')
        contacts = reverse("contact_manager")
        edit = reverse('account_edit_user')

        # Home page should redirect to Terms of Service
        response = self.client.get(index)
        self.assertRedirects(response, url_string.format(tos, index))

        # Contacts should redirect to Terms of Service
        response = self.client.get(contacts)
        self.assertRedirects(response, url_string.format(tos, contacts))

        # Profile should redirect to Terms of Service
        response = self.client.get(edit)
        self.assertRedirects(response, url_string.format(tos, edit))

        # Accept Terms of Service on test user
        response = self.client.post(url_string.format(tos, contacts),
                                    {'accept': 'I Accept'})
        ## Verify it redirects to contacts
        self.assertRedirects(response, contacts)

        # Home page should now display
        response = self.client.get(index + "?force")
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '<title>Home - RevUp</title>')

        # Contacts should now display
        response = self.client.get(contacts)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '<title>Contact Manager - RevUp</title>')

        # Profile should now display
        response = self.client.get(edit)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '<title>RevUp Software</title>')


class TemplateTagTests(AuthorizedUserTestCase):
    def test_parse_committee(self):
        # Sample ActBlue donation
        donation = {'committee': {'CMTE_NM': 'ACTBLUE'},
                    'record':    {'MEMO_TEXT': 'EARMARKED FOR JOHN SMITH FOR '
                                               'CONGRESS (C00000000)'}}
        result = parse_committee(donation)
        self.assertEqual(result, 'ACTBLUE - FOR JOHN SMITH FOR CONGRESS')

        # Sample JStreetPAC donation
        donation = {'committee': {'CMTE_NM': 'JSTREETPAC'},
                    'record':    {'MEMO_TEXT': 'EARMARKED FOR JANE DOE FOR '
                                               'CONGRESS'}}
        result = parse_committee(donation)
        self.assertEqual(result, 'JSTREETPAC - FOR JANE DOE FOR CONGRESS')

        # Other committees should return their names
        donation = {'committee': {'CMTE_NM': 'OTHER'}}
        result = parse_committee(donation)
        self.assertEqual(result, 'OTHER')

        # Invalid donations should return empty strings
        self.assertEqual(parse_committee({}), '')


class ForcedRedirectMiddlewareTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        super(ForcedRedirectMiddlewareTests, self).setUp(**kwargs)
        AnalysisFactory.create(user=self.user)

    @override_settings(DEFAULT_DOMAIN="default_test")
    def test_preferred_domain_filter(self):
        index = reverse("index")
        # Verify user is redirected to default domain
        result = self.client.get(index)
        self.assertRedirects(result, "http://default_test/", status_code=307,
                             fetch_redirect_response=False)

        # Set a preferred domain and verify user is redirected there
        self.user.current_seat.account.preferred_domain = "preferred"
        self.user.current_seat.account.save()
        result = self.client.get(index)
        self.assertRedirects(result, "http://preferred/", status_code=307,
                             fetch_redirect_response=False)

        # Verify redirects do not occur when PUT or POST is used.
        result = self.client.put(index)
        self.assertEqual(result.status_code, 405)
        result = self.client.post(index)
        self.assertEqual(result.status_code, 405)


@patch("frontend.apps.core.views.recurly.list_subscription_plans", return_value=[])
@patch("frontend.apps.core.views.send_template_email")
@patch("frontend.apps.core.views.verify_recaptcha", return_value=True)
class OnboardingQualifierViewTests(TestCase):
    def setUp(self, **kwargs):
        super().setUp()
        self.url = reverse("onboard_qualifier")
        # Instantiate a client
        self.client = Client()
        self.product = ProductFactory(product_type="G",
                                      manual_onboarding=False)
        self.profile = AccountProfileFactory(product=self.product,
                                             title="Political Candidate",
                                             account_type="P")

    @patch("frontend.apps.core.views.ExternalSignupToken.objects.create")
    def test_process_political(self, m1, m2, m3, m4):
        m1.return_value = dict(token="test_token_123")
        data = dict(
            first_name="test-first",
            last_name="test-last",
            email="test@revup.com",
            phone="555-555-5555",
            title="test title",
            office_type="federal",
            client_type="candidate",
            constituent_count="100000",
            product_id=self.product.id,
            product_type="political",
            recaptcha="1"
        )

        response = self.client.post(self.url, data)
        assert response.status_code == 200
        assert m3.call_args[0][0] == 'Welcome to RevUp'
        assert m3.call_args[0][3] == 'test@revup.com'
        m1.assert_called_with(
            addendum1='100000', addendum2='federal', email='test@revup.com',
            first_name='test-first', last_name='test-last',
            organization='test title', phone='555-555-5555',
            product=self.product, profile=self.profile)

    def test_process_disabled(self, m1, m2, m3):
        data = dict(
            first_name="test-first",
            last_name="test-last",
            email="test@revup.com",
            phone="555-555-5555",
            title="test title",
            product_type="non-profit",
            recaptcha="1"
        )

        response = self.client.post(self.url, data)
        assert response.status_code == 200
        assert m2.call_args[0][0] == "Non-Profit Customer Inquiry"
        assert m2.call_args[0][3] == 'sales@revup.com'


@patch("frontend.apps.core.views.recurly.get_subscription_plan",
       return_value=None)
class OnboardingSignupViewTests(TestCase):
    def setUp(self):
        super().setUp()
        # Instantiate a client
        self.client = Client()
        self.product = ProductFactory(product_type="G",
                                      manual_onboarding=False)
        self.profile = AccountProfileFactory(product=self.product,
                                             title="Political Candidate",
                                             account_type="P")
        self.token = ExternalSignupTokenFactory(product=self.product,
                                                profile=self.profile)
        self.url = reverse("onboard_signup", args=(self.token.token,))

    def test_get_new(self, m1):
        """Test the context when the email is new"""
        response = self.client.get(self.url)
        assert response.context['token'] == self.token
        assert isinstance(response.context['user'], AnonymousUser)
        assert response.context['path'] == "signup"

    def test_get_existing(self, m1):
        existing = RevUpUserFactory(email=self.token.email,
                                    username=self.token.email)
        response = self.client.get(self.url)
        assert response.context['token'] == self.token
        assert isinstance(response.context['user'], AnonymousUser)
        assert response.context['path'] == "login"

    def test_signup_post_invalid_password(self, m1):
        """Verify an invalid password is rejected during signup"""
        data = dict(
            first_name=self.token.first_name,
            last_name=self.token.last_name,
            email=self.token.email,
            password="password"
        )
        response = self.client.post(self.url + "?path=signup", data=data)
        assert response.status_code == 400
        assert b"Invalid Password" in response.content

    def test_signup_post(self, m1):
        """Verify a user is created correctly in the signup post"""
        data = dict(
            first_name=self.token.first_name,
            last_name=self.token.last_name,
            email=self.token.email,
            password="aBcdefg123"
        )
        response = self.client.post(self.url + "?path=signup", data=data)
        assert response.status_code == 200
        assert response.context['user'].is_authenticated
        assert isinstance(response.context['user'], RevUpUser)
        assert response.context['path'] == "contract"
        # Verify all the user fields created correctly
        user = RevUpUser.objects.get(id=response.context['user'].id)
        assert user.check_password(data['password'])
        assert user.first_name == data['first_name']
        assert user.last_name == data['last_name']
        assert user.email == data['email']
        assert user.username == data['email']
        self.token.refresh_from_db()
        assert self.token.user == user
        assert self.token.account is None

    def test_login_post(self, m1):
        """Verify a user is authenticated correctly from the login page"""
        # Create an existing user matching the token
        existing = RevUpUserFactory(email=self.token.email,
                                    username=self.token.email,
                                    password="123rhl23jhrl12")
        data = dict(
            email=existing.email,
            password="aBcdefg123"
        )

        # Verify a bad password is rejected
        response = self.client.post(self.url + "?path=login", data=data)
        assert response.status_code == 401

        # Set the password to the correct value for the positive test
        existing.set_password(data['password'])
        existing.save()
        # Also verify the user is set in the token when it is not
        assert self.token.user is None

        response = self.client.post(self.url + "?path=login", data=data)
        assert response.status_code == 200
        assert response.context['user'].is_authenticated
        assert response.context['user'] == existing
        assert response.context['path'] == "contract"
        self.token.refresh_from_db()
        assert self.token.user == existing


@patch("frontend.apps.core.views.recurly.get_subscription_plan",
       return_value=None)
class AuthenticatedOnboardingSignupViewTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        super().setUp(set_session=False, **kwargs)
        self.product = ProductFactory(product_type="G",
                                      manual_onboarding=False)
        self.profile = AccountProfileFactory(product=self.product,
                                             title="Political Candidate",
                                             account_type="P")
        self.token = ExternalSignupTokenFactory(product=self.product,
                                                profile=self.profile,
                                                email=self.user.email,
                                                user=self.user)
        self.url = reverse("onboard_signup", args=(self.token.token,))

    def test_invalid_user(self, m1):
        """Verify an authenticated user is scrutinized correctly"""
        # Verify the case of no user
        self.token.user = None
        self.token.save()
        response = self.client.get(self.url)
        assert response.status_code == 200
        assert not response.context['user'].is_authenticated
        assert isinstance(response.context['user'], AnonymousUser)
        assert response.context['path'] == "login"

        # Verify the case of a different user
        self.token.user = RevUpUserFactory(username="test12345@revup.com")
        self.token.email = self.token.user.email
        self.token.save()
        response = self.client.get(self.url)
        assert response.status_code == 200
        assert not response.context['user'].is_authenticated
        assert isinstance(response.context['user'], AnonymousUser)
        assert response.context['path'] == "login"

    def test_email_mismatch(self, m1):
        """Verify the user is logged-out if their authorized user email
        doesn't match the token email
        """
        self.token.email = "test12345@revup.com"
        self.token.save()
        assert self.user.email != self.token.email

        response = self.client.get(self.url)
        assert response.status_code == 200
        assert not response.context['user'].is_authenticated
        assert isinstance(response.context['user'], AnonymousUser)
        assert response.context['path'] == "signup"

    def test_no_account(self, m1):
        """Verify authenticated users are taken to contract if they have
        no account
        """
        assert self.user.email == self.token.email
        assert not self.token.account

        response = self.client.get(self.url)
        assert response.status_code == 200
        assert response.context['user'].is_authenticated
        assert isinstance(response.context['user'], RevUpUser)
        assert response.context['path'] == "contract"

    def test_account(self, m1):
        """Verify authenticated users that already have an account are sent
        to payment page
        """
        m2 = MagicMock()
        m3 = MagicMock()
        m2.unit_amount_in_cents = m3
        m3.currencies = dict(USD=5001)
        m1.return_value = m2
        self.token.account = AccountFactory(account_head=self.user,
                                            active=False)
        self.token.save()
        assert self.user.email == self.token.email
        assert self.token.account

        response = self.client.get(self.url)
        assert response.status_code == 200
        assert response.context['user'].is_authenticated
        assert isinstance(response.context['user'], RevUpUser)
        assert response.context['path'] == "payment"
        assert response.context['product']
        assert response.context['product'].price == 50.01

    def test_account_active(self, m1):
        """Verify authenticated users that already have an -active- account
        are redirected to the setup view
        """
        self.token.account = AccountFactory(account_head=self.user,
                                            active=True)
        self.token.save()
        assert self.user.email == self.token.email
        assert self.token.account.active

        response = self.client.get(self.url)
        assert response.status_code == 302
        self.assertRedirects(response, reverse("onboard_setup",
                                               args=(self.token.account.id,)))

    def test_post_contract_no_user(self, m1):
        """Verify a token without a user is redirected appropriately"""
        data = dict(contract="this is a test contract")

        self.token.user = None
        self.token.save()
        response = self.client.post(f"{self.url}?path=contract", data=data)
        assert response.context['path'] == "login"
        assert not response.context['account']
        assert isinstance(response.context['user'], AnonymousUser)

    def test_post_contract(self, m1):
        """Verify contract posting correctly creates account objects"""
        data = dict(contract="this is a test contract",
                    signature="test signature")

        account_count = Account.objects.count()
        contract_count = AccountContract.objects.count()

        response = self.client.post(f"{self.url}?path=contract", data=data)
        assert response.status_code == 200
        # Verify the account was created correctly
        account = response.context['account']
        assert account
        assert not account.active
        assert account.account_head == self.user
        assert account.organization_name == self.token.organization
        assert account.account_user
        assert account.seats.filter(user=self.user).exists()
        assert account.seat_count_override == -1
        assert account.analyze_limit_override is None
        assert account.create_limit_override is None
        assert account.account_profile == self.token.profile
        assert account.account_key is not None
        assert len(account.account_key) == ACCOUNT_KEY_LENGTH
        self.token.refresh_from_db()
        assert self.token.account == account
        assert Account.objects.count() == account_count + 1

        # Verify the contract was saved
        contract = account.contracts.first()
        assert contract
        assert contract.contract == data['contract']
        assert contract.signature == data['signature']
        assert contract.signer == self.user
        assert response.context['contract'] == contract
        assert AccountContract.objects.count() == contract_count + 1

        # Verify posting again does not create another account
        response = self.client.post(f"{self.url}?path=contract", data=data)
        assert response.context['path'] == "payment"
        assert response.status_code == 200
        assert Account.objects.filter(account_head=self.user).count() == 1
        # Active account should redirect to setup
        account.active = True
        account.save()
        response = self.client.post(f"{self.url}?path=contract", data=data)
        assert response.status_code == 302
        self.assertRedirects(response, reverse("onboard_setup",
                                               args=(account.id,)))

    @patch("frontend.apps.core.views.Account.get_or_create_recurly_subscription")
    def test_post_payment(self, m1, m2):
        """Verify payment is processed correctly"""
        data = {'recurly-token': "asdlfhasdljfhasdlfj"}

        # Verify user is redirected if they don't have an account
        assert not self.token.account
        assert self.token.user
        response = self.client.post(f"{self.url}?path=payment", data=data)
        assert response.context['path'] == "contract"
        assert not response.context['account']
        assert response.context['user'] == self.user
        assert not m1.called
        assert not self.token.redeemed

        account = AccountFactory(account_head=self.user,
                                 account_profile=self.token.profile,
                                 active=True)
        self.token.account = account
        self.token.save()

        # Verify case when payment is processed successfully
        response = self.client.post(f"{self.url}?path=payment", data=data)
        m1.assert_called_with(data['recurly-token'])
        m2.assert_called_with('test')
        assert response.status_code == 302
        self.assertRedirects(response, reverse("onboard_setup",
                                               args=(account.id,)))
        account.refresh_from_db()
        assert account.active
        self.token.refresh_from_db()
        assert self.token.redeemed

    @patch("frontend.apps.core.views.Account.get_or_create_recurly_account")
    def test_post_payment_manual_onboarding(self, m1, m2):
        """Verify the combined payment and account creation of the
        manual onboarding flow
        """
        product = ProductFactory(product_type="G",
                                 manual_onboarding=True)
        profile = AccountProfileFactory(product=product,
                                        title="Political Candidate",
                                        account_type="P")
        token = ExternalSignupTokenFactory(product=product,
                                           profile=profile,
                                           email=self.user.email,
                                           user=self.user)
        url = reverse("onboard_signup", args=(token.token,))
        data = {'recurly-token': "asdlfhasdljfhasdlfj"}
        response = self.client.post(f"{url}?path=payment", data=data)
        token.refresh_from_db()
        account = token.account
        assert f"\"redirect\": \"{reverse('onboard_setup', args=(account.id,))}\"" \
               in str(response.content)
        assert response.status_code == 200
        assert Account.objects.filter(account_head=self.user).count() == 1
        assert token.redeemed
        assert account.active is True
        assert account.onboarding is True
        assert account.account_head == self.user
        assert account.organization_name == token.organization
        assert account.account_user
        assert account.seats.filter(user=self.user).exists()
        assert account.seat_count_override == -1
        assert account.analyze_limit_override is None
        assert account.create_limit_override is None
        assert account.account_profile == token.profile
        assert account.account_key is not None
        assert len(account.account_key) == ACCOUNT_KEY_LENGTH


class OnboardingSetupViewTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        super().setUp(**kwargs)
        self._add_admin_group()
        self.product = ProductFactory(product_type="G",
                                      manual_onboarding=False)
        self.profile = AccountProfileFactory(product=self.product,
                                             title="Political Candidate",
                                             account_type="P")

        self.feature_config = FeatureConfigFactory()
        analysis_profile = self.profile.default_analysis_profiles.first()
        analysis_profile.default_config_template.feature_configs.set(
            [self.feature_config])

        self.account = AccountFactory(account_profile=self.profile,
                                      account_head=self.user)
        self.url = reverse("onboard_setup", args=(self.account.id,))

    def test_get_context(self):
        response = self.client.get(self.url)
        assert response.status_code == 200
        assert response.context['question_sets']

        question_set = self.feature_config.question_sets.first()
        context_question_sets = response.context['question_sets']
        assert question_set.id in context_question_sets
        assert context_question_sets[question_set.id]['title'] == \
               question_set.title

    def test_bad_post(self):
        response = self.client.post(f"{self.url}?path=unsupported", data={})
        assert response.status_code == 400
        response = self.client.post(f"{self.url}", data={})
        assert response.status_code == 400

    def test_post_tos(self):
        self.user.accepted_tos = None
        self.user.save()

        now = datetime.now()
        response = self.client.post(f"{self.url}?path=tos", data={})
        assert response.status_code == 200
        self.user.refresh_from_db()
        assert now <= self.user.accepted_tos <= datetime.now()


class RecurlyWebhookTestPermissions(AuthorizedUserTestCase):
    def setUp(self):
        super(RecurlyWebhookTestPermissions, self).setUp(login_now=False)
        self.request_factory = RequestFactory()
        self.recurly_webhook_url = reverse('recurly_webhook')

    def test_as_account_admin_user(self):
        """Test recurly_webhook view when the request comes from an account
        admin and from an unknown ip. Verify that the request is blocked
        """
        self._login_and_add_admin_group()
        # Verify that the user is an account_admin
        self.assertTrue(self.user.is_account_admin())
        # Verify that the user is not an admin
        self.assertFalse(self.user.is_admin)
        # Test the request
        response = self.client.get(self.recurly_webhook_url)
        self.assertEqual(403, response.status_code)

    @patch('frontend.apps.core.views.recurly.objects_for_push_notification')
    # Mock user as an admin
    @patch('frontend.apps.authorize.models.RevUpUser.is_admin', return_value=True)
    def test_as_admin_user(self, is_admin_m,
                           recurly_objects_for_push_notification_m):
        """Test recurly_webhook view when the request comes from admin but
        from an unknown ip. Verify that the request goes through
        """
        recurly_objects_for_push_notification_m.return_value = {
            'type': 'test',
            'account': MagicMock()}
        self._login()
        # Verify that the user is not an account_admin
        self.assertFalse(self.user.is_account_admin())
        # verify that the user is an admin
        self.assertTrue(self.user.is_admin)
        # Test the request
        response = self.client.post(self.recurly_webhook_url)
        self.assertEqual(200, response.status_code)

    def test_as_regular_user(self):
        """Test recurly_webhook view when the request comes from an unknown
        user and unknown ip address"""
        # Create a fake request where the request user is anonymous
        request = self.request_factory.post(self.recurly_webhook_url)
        request.user = AnonymousUser()
        # Test the request
        recurly_webhook = RecurlyWebhook.as_view()
        response = recurly_webhook(request)
        self.assertEqual(403, response.status_code)

    @patch('frontend.apps.core.views.recurly.objects_for_push_notification')
    def test_req_from_known_ip_list_1(self,
                                      recurly_objects_for_push_notification_m):
        """Test recurly_webhook view when the request comes from an unknown
        user and a known ip address and verify that the request goes through
        """
        recurly_objects_for_push_notification_m.return_value = {
            'type': 'test',
            'account': MagicMock()}
        # Create a fake request where the request user is anonymous
        request = self.request_factory.post(self.recurly_webhook_url)
        request.user = AnonymousUser()
        # Choose a random ip from the list of known ips and assign it to the
        # request
        request.META['REMOTE_ADDR'] = settings.RECURLY_NOTIFICATION_IPS[
            randint(0, 5)]
        # Test the request
        recurly_webhook = RecurlyWebhook.as_view()
        response = recurly_webhook(request)
        # Verify that response status code is 200
        self.assertEqual(200, response.status_code)

    @patch('frontend.apps.core.views.recurly.objects_for_push_notification')
    def test_req_from_known_ip_list_2(self,
                                      recurly_objects_for_push_notification_m):
        """Test recurly_webhook view when the request comes from an unknown
        user and a known ip address and verify that the request goes through
        """
        recurly_objects_for_push_notification_m.return_value = {
            'type': 'test',
            'account': MagicMock()}
        # Create a fake request where the request user is anonymous
        request = self.request_factory.post(self.recurly_webhook_url)
        request.user = AnonymousUser()
        # Choose a random ip from the list of known ips and assign it to the
        # request
        request.META['HTTP_X_FORWARDED_FOR'] = '{0},127.0.0.1'.format(
            settings.RECURLY_NOTIFICATION_IPS[randint(0, 5)])
        # Test the request
        recurly_webhook = RecurlyWebhook.as_view()
        response = recurly_webhook(request)
        self.assertEqual(200, response.status_code)


class JwtAuthTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        self.campaign = PoliticalCampaignFactory()
        super(JwtAuthTests, self).setUp(login_now=False, set_session=False)
        self.seat = SeatFactory(account=self.campaign, user=self.user,
                                seat_type=Seat.Types.TEMPORARY,
                                state=Seat.States.ACTIVE)
        self.url = reverse('account_seats_api-list',
                           args=(self.campaign.id,))

    def test_jwt_permissions(self):
        """Tests how various permission levels are assigned to a user with
        the help of  a jwt token
        """
        # Verify that any unauthenticated user is rejected
        result = self.client.get(self.url)
        self.assertNoCredentials(result)

        # Login the user
        self._login(self.user, jwt_auth=True)

        # Verify that the user is neither an admin nor an account admin
        self.assertFalse(self.user.is_admin)
        self.assertFalse(self.user.is_account_admin())

        # Verify that a standard user is rejected from viewing all seats
        result = self.client.get(self.url)
        self.assertNoPermission(result)

        # Add the view permissions to the user
        self._add_admin_group()

        # Verify that the API now accepts the user
        result = self.client.get(self.url, follow=True)
        self.assertEqual(200, result.status_code)

        seat_ids = {_['id'] for _ in result.json()['results']}
        first_names = {_['user']['first_name'] for _ in result.json()['results']}
        self.assertIn(self.seat.id, seat_ids)
        self.assertIn(self.user.first_name, first_names)

    def test_jwt_authentication(self):
        """Tests if JSONWebToken Authentication is successful and if the
        response contains a token
        """
        response = self._login(jwt_auth=True)
        self.assertEqual(200, response.status_code)
        self.assertTrue('token' in response.data)

    def test_serializer(self):
        """Tests the contents of JSONWebTokenSerializer"""
        # Pass credentials to JWT auth endpoint
        response = self.client.post(reverse('api_jwt_auth'),
                                    {'email': self.user.email,
                                     'password': "this_Is_a_Test_Password1"})
        # Verify that the response has the fields: token and user
        self.verify_serializer(response, ('token', 'user'))

        # Verify that the user is in the response is same as the authenticated
        # user and and the user item has the fields: current_seat and email
        response_user = response.data.get('user')
        assert self.user.id == response_user.get('id')
        assert 'current_seat' in response_user
        assert 'email' in response_user

        # Verify that the current_seat in the response is same as the
        # authenticated user and and that the user item has the fields:
        # current_seat and email
        current_seat = response_user.get('current_seat')
        assert self.seat.id == current_seat.get('id')
        assert 'account' in current_seat
        assert 'permissions' in current_seat


class ProductTranslationTests(TestCase):
    def test_translation_political_committee(self):
        """Test the translations for a political committee product"""
        # Test the translation of a string for which political committee
        # translation exists.
        account_profile = AccountProfileFactory(account_type="P",
                                                account_sub_type="COM")
        account_type = account_profile.product_type
        str1 = 'Client Information'
        translated_str1 = translate(str1, account_type)
        assert 'Committee Information' == translated_str1

        # Test the translation of a string for which political committee
        # translation doesn't exist and verify that generic political
        # translation is returned.
        str1 = 'Client Contacts'
        translated_str1 = translate(str1, account_type)
        assert 'House File' == translated_str1

    def test_translation_political(self):
        """Test the translations for a generic political product"""
        # Test the translation of a string for which political translation
        # exists.
        account_profile = AccountProfileFactory(account_type="P")
        account_type = account_profile.product_type
        str1 = 'Key Contributions'
        translated_str1 = translate(str1, account_type)
        assert 'Correlated Campaigns' == translated_str1

        # Test the translation of a string for which generic political
        # translation doesn't exist and verify that default translation is
        # returned.
        str2 = 'Client Information'
        translated_str2 = translate(str2, account_type)
        assert 'Client Information' == translated_str2

    def test_translation_nonprofit(self):
        """Test the translations for a nonprofit product"""
        # Test the translation of a string for which nonprofit translation
        # exists.
        account_profile = AccountProfileFactory(account_type="N")
        account_type = account_profile.product_type
        str1 = 'Contacts'
        translated_str = translate(str1, account_type)
        assert 'Records' == translated_str

    def test_translation_academic(self):
        """Test the translations for an academic product"""
        # Test the translation of a string for which academic translation
        # doesn't exist and verify that default translation is returned.
        account_profile = AccountProfileFactory(account_type="A")
        account_type = account_profile.product_type
        str1 = 'Contact Manager'
        translated_str = translate(str1, account_type)
        assert 'Contact Manager' == translated_str

    def test_translation(self):
        """Test the translation edge cases"""
        # Test the translation for a product that isn't defined in the
        # translations. Verify that original string is returned.
        account_type = 'TestAccountTypeX'
        str1 = 'Contact Manager'
        translated_str = translate(str1, account_type)
        assert 'Contact Manager' == translated_str

        # Test the translation for a string that isn't defined in the
        # translations. Verify that original string is returned.
        account_type = 'Political'
        str1 = 'Test translation for random string'
        translated_str = translate(str1, account_type)
        assert 'Test translation for random string' == translated_str

    def test_reverse_translation(self):
        """Test `reverse_translate` method"""
        # Test reverse translation for a product that isn't defined in the
        # translations. Verify that string is returned as is.
        account_type = 'TestAccountTypeX'
        translated_str = 'Contact Manager'
        reverse_translated_str = reverse_translate(translated_str, account_type)
        assert translated_str == reverse_translated_str

        # Test reverse translation for a string that isn't defined in the
        # translations. Verify that the string is returned as is.
        account_type = 'Political'
        translated_str = 'Test translation for random string'
        reverse_translated_str = reverse_translate(translated_str, account_type)
        assert translated_str == reverse_translated_str

        # Test reverse translation for a string.
        account_type = 'Political Committee'
        translated_str = 'Correlated Campaigns'
        reverse_translated_str = reverse_translate(translated_str, account_type)
        assert 'Key Contributions' == reverse_translated_str
