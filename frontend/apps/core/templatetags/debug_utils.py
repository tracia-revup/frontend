from django import template
from django.conf import settings
from django.utils.safestring import mark_safe

register = template.Library()


@register.simple_tag
def dump_obj(obj):
    """This is a little debug utility that allows front-end devs to see the
    fields and values in an object (typically a model).
    """
    def format_data(data):
        return mark_safe("<br />".join(
            ("<b>{}</b>: &nbsp;&nbsp;&nbsp;{},".format(k, repr(v))
             for k, v in sorted(data.items())
             if not (str(k).startswith('_') or str(k).endswith('_id')))
        ))
    if not settings.DEBUG or not obj:
        return ""
    else:
        if hasattr(obj, "__dict__"):
            return format_data(obj.__dict__)
        elif isinstance(obj, dict):
            return format_data(obj)
        else:
            return repr(obj)
