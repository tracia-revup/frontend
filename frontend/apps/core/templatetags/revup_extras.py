from django import template
from django.utils.safestring import mark_safe
import json

from frontend.apps.core.product_translations import translate
from frontend.libs.utils import view_utils

register = template.Library()

@register.filter
def pk(value):

    # Retrieve _id value
    if '_id' in value:
        value = value['_id']

    # Return value
    return str(value)


@register.filter
def get(d, key_name):
    """Access a dict key by variable in a django template.

    Example:
    <h3>{{ user.id }}</h3>
    <p>{{ user_dict | get:user.id }}</p>

    With built-in Django, you cannot do {{ user_dict[user.id] }} or
    anything equivalent
    """
    try:
        return d[key_name]
    except (KeyError, TypeError):
        from django.conf import settings
        return settings.TEMPLATES[0]['OPTIONS']['string_if_invalid']


@register.filter
def add_class(field, css_class):
    """Add a css class to a form field while rendering it."""
    return field.as_widget(attrs={"class": css_class})


@register.filter
def verbose_name(model):
    """Return the verbose name for a model object/class."""
    try:
        return model._meta.verbose_name
    except AttributeError:
        return ""


@register.filter
def translate_by_product(str_, product_type):
    """
    Translate the given string based on product_type
    :param str_: (string) String to be translated
    :param product_type: (string) Type of the product. If the product type has
    more than one word, the words could be separated by `_` or ` `(space) and
    be in any case.
    Examples: political_committee, Political Candidate, ACADEMIC, nonprofit
    :return: (string) Translated version of the string if the translation
    exists for the specified product_type. Otherwise, returns
    the original string.
    """
    return translate(str_, product_type)


@register.filter
def cast(field, type_):
    """Cast a value to another built-in type.

    E.g.:
        {# Cast to a float #}
        {{ str_value | cast:"float" }}
    """
    try:
        # Somehow __builtins__ is being made into a dict somewhere. This
        # handles that somewhat abnormal behavior.
        if isinstance(__builtins__, dict):
            return __builtins__[type_](field)
        else:
            return getattr(__builtins__, type_)(field)
    except Exception:
        # Fail silently
        return ""


@register.filter
def jsonify(o):
    return mark_safe(json.dumps(o))


@register.simple_tag(takes_context=True)
def protocol(context):
    return _protocol_tag_worker(context, False)


@register.simple_tag(takes_context=True)
def protocol_host(context):
    return _protocol_tag_worker(context, True)


def _protocol_tag_worker(context, include_host=False):
    try:
        request = context['request']
    except KeyError:
        return ""
    else:
        if include_host:
            return view_utils.build_protocol_host(request)
        else:
            return view_utils.build_protocol(request)
