import re

from django.template import Library

register = Library()

@register.simple_tag
def parse_committee(donation):
    try:
        committee_name = donation['committee']['CMTE_NM']
    except KeyError:
        return ""
    pattern = None
    if committee_name == 'ACTBLUE':
        pattern = r'^EARMARK(?:ED)? (?:FOR|TO):? (.*) \(C\d+\)'
    elif committee_name == 'JSTREETPAC':
        pattern = r'^EARMARKED FOR (.*)'
    if pattern:
        match = re.search(pattern, donation['record']['MEMO_TEXT'])
        if match:
            recipient = match.group(1)
            return "{} - FOR {}".format(committee_name, recipient)
    return committee_name
