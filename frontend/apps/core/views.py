import datetime
import json
import logging

from celery.result import AsyncResult
from ddtrace import tracer
from django.conf import settings
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.core.validators import EmailValidator, ValidationError
from django.db import transaction
from django.http import (
    HttpResponseRedirect, JsonResponse, Http404, HttpResponseBadRequest,
    HttpResponse, HttpResponseServerError)
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View, TemplateView
from waffle.interface import flag_is_active
from waffle.models import Flag

from frontend.apps.analysis.models.analysis_configs import QuestionSetData
from frontend.apps.authorize.decorators import allow_ip
from frontend.apps.authorize.forms import PasswordField
from frontend.apps.authorize.models import ExternalSignupToken, RevUpUser
from frontend.apps.campaign.routes import ROUTES
from frontend.apps.campaign.models import (
    Product, Account, AccountProfile, AccountContract, DeactivationReason)
from frontend.apps.role.decorators import any_group_required
from frontend.apps.role.permissions import AdminPermissions
from frontend.libs.django_view_router import RoutingView, RoutingTemplateView
from frontend.libs.utils.email_utils import send_template_email
from frontend.libs.utils.form_utils import verify_recaptcha
import frontend.libs.utils.recurly_utils as recurly
from frontend.libs.utils.view_utils import get_client_ip
from frontend.libs.utils.zendesk_utils import (
    get_zendesk_organization, get_zendesk_user, create_or_update_user,
    get_zendesk_ticket, create_ticket)

LOGGER = logging.getLogger(__name__)


class IndexView(RoutingView):
    routes = ROUTES

    @tracer.wrap()
    def _get_counts(self, request):
        from frontend.apps.contact_set.models import ContactSet

        def worker(account_, user):
            root = ContactSet.get_root(account_, user)
            return root.count if root else 0

        return dict(
            personal_contact_count=worker(
                request.user.current_seat.account, request.user),
            account_contact_count=worker(
                request.user.current_seat.account,
                request.user.current_seat.account.account_user)
        )

    @tracer.wrap()
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        if request.user.current_seat:
            account = request.user.current_seat.account
        else:
            account = None

        # If this user has no imports, send them to the contact manager
        if not (request.user.importrecord_set.exists() or
                    "force" in request.GET):
            response = redirect('contact_manager')
            response['Location'] += "?onboarding"
            return response

        # If the account is not None and this is a nonprofit account redirect
        # to the ranking page.
        if account and account.account_profile.account_type == \
                AccountProfile.AccountType.NONPROFIT:
            response = redirect('analysis_result_list')
            return response

        # Check if we should display the splash screen. The splash screen
        # should only be displayed to a user once.
        if flag_is_active(request, "Splash Screen"):
            show_splash = True
            flag = Flag.objects.get(name="Splash Screen")
            flag.users.remove(request.user)
        else:
            show_splash = False

        context = {'user': request.user,
                   'account': account,
                   'show_splash': show_splash,
                   'message': request.GET.get('msg'),
                   'request':request}
        context.update(self._get_counts(request))

        return render(request, template_name='core/index.html',
                      context=context)


@login_required
def task_info(request):
    tasks = [AsyncResult(task.task_id) for task in request.user.tasks.all()]
    return render(request, template_name='core/tasks.html',
                  context={'tasks': tasks,
                           'user': request.user,
                           'request': request})


@login_required
def task_status(request):
    status_list = []
    for task in request.user.tasks.all():
        result = AsyncResult(task.task_id)
        percent = None
        message = ""
        # Get the progress percent from the running task
        try:
            if getattr(result, "result", None):
                percent = result.result.get("percent")
                message = result.result.get("message")
        except Exception:
            pass
        status_list.append({"id": task.task_id, "type": task.task_type,
                            "state": result.state, "percent": percent,
                            "message": message})
        if result.state in ["SUCCESS", "FAILURE"]:
            task.delete()
    return JsonResponse(status_list, safe=False)


class TermsOfServiceView(View):
    def __init__(self, **kwargs):
        super(TermsOfServiceView, self).__init__(**kwargs)
        self.next = None

    def dispatch(self, request, *args, **kwargs):
        self.next = request.GET.get('next')
        return super(TermsOfServiceView, self).dispatch(
            request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        accepted_tos = (hasattr(request.user, 'accepted_tos') and
                        request.user.accepted_tos)
        return render(request, template_name='core/terms_of_service.html',
                      context={'accepted_tos': accepted_tos,
                               'next': self.next,
                               'user': request.user,
                               'request': request})

    def post(self, request, *args, **kwargs):
        if request.POST.get('accept') == 'I Accept':
            if request.user.is_authenticated and hasattr(request.user,
                                                           'accepted_tos'):
                request.user.accepted_tos = datetime.datetime.utcnow()
                request.user.save()
            return HttpResponseRedirect(self.next or reverse('index'))
        else:
            return self.get(request, *args, **kwargs)


class GettingStartedView(RoutingTemplateView):
    """Route the user to the appropriate getting-started page."""
    routes = ROUTES

    getting_started_path = "getting_started/{}/{}/{}/index.html"
    current_political = "20180312"
    current_academic = "20150424"
    political = "political"
    academic = "academic"
    roles_all = "all"
    roles_admin = "admin"
    roles_raiser = "raiser"

    def get(self, request, forced_type=None, forced_version=None,
            *args, **kwargs):

        def generate_redirect(type_, version, role):
            # Helper method to keep things DRY
            if not version:
                version = getattr(self, "current_{}".format(type_))
            return HttpResponseRedirect(static(
                self.getting_started_path.format(type_, version, role)))

        # Some basic support for forcing a certain getting-started page
        # E.g.: /getting_started/university/
        #       /getting_started/political/
        if forced_type:
            # Some simple sanity/access checking.
            if forced_type not in ("political", "academic"):
                raise Http404
            return generate_redirect(
                forced_type, forced_version, self.roles_all)
        # If the path isn't forced and the user isn't logged in, we can't
        # send them to the right place.
        elif not request.user.is_authenticated:
            raise Http404

        if self.request.route == 'university':
            return generate_redirect(self.academic, self.current_academic,
                                     self.roles_all)
        else:
            # If the user has admin access, show them the admin tutorial
            if request.user.current_seat.has_admin_access():
                return generate_redirect(
                    self.political, self.current_political, self.roles_admin)
            else:
                return generate_redirect(
                    self.political, self.current_political, self.roles_raiser)


class OnboardingQualifierView(TemplateView):
    template_name = "core/onboard_qualifier.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Get all the Global products to present to the user
        products = list(Product.objects.filter(
                                    product_type=Product.ProductType.GLOBAL))

        # Match the products to the recurly plans for pricing info
        recurly_products = {p.recurly_plan_code: p for p in products}

        # Get the Plans from Recurly so we have accurate billing data
        for plan in recurly.list_subscription_plans():
            plan_code = plan['plan_code']
            if plan_code in recurly_products:
                # Add the Plan price to the product for ease of frontend use.
                product = recurly_products[plan_code]
                product.price = plan['unit_amount_in_cents'] / 100
                product.setup_fee = plan['setup_fee_in_cents'] / 100

        context["products"] = {p.title.lower(): p for p in products}
        return context

    def _get_contact_fields(self, request):
        def get_field(field_name):
            return request.POST.get(field_name, "").strip()
        first_name = get_field("first_name")
        last_name = get_field("last_name")
        email = get_field("email")
        # Verify the email is valid
        try:
            EmailValidator()(email)
        except ValidationError:
            email = None
        phone = get_field("phone")
        title = get_field("title")
        return first_name, last_name, email, phone, title

    def _process_disabled(self, request, product_type):
        first_name, last_name, email, phone, title = self._get_contact_fields(request)
        if not all((first_name, last_name, email, phone, title, product_type)):
            return HttpResponseBadRequest("All fields required")

        subject = f"{product_type.title()} Customer Inquiry"
        email_template = 'core/emails/qualifier_inquiry_email.html'
        context = dict(first_name=first_name, last_name=last_name,
                       email=email, phone=phone, title=title,
                       product_type=product_type.title())
        send_template_email(subject, email_template, context, "sales@revup.com")
        return HttpResponse()

    def _process_political(self, request):
        """Process the political product type signup requests"""
        first_name, last_name, email, phone, title = self._get_contact_fields(request)
        office_type = request.POST.get("office_type", "").strip()
        client_type = request.POST.get("client_type", "").strip()
        constituent_count = request.POST.get("constituent_count", "").strip()
        product_id = request.POST.get("product_id", "").strip()

        if not all((first_name, last_name, email, phone, title, client_type,
                    constituent_count, office_type, product_id)):
            return HttpResponseBadRequest("All fields required")

        product = get_object_or_404(
            Product.objects.filter(product_type=Product.ProductType.GLOBAL),
            id=product_id)

        # TODO: We may not want this functionality anymore. Preserving temporarily
        # if product.manual_onboarding:
        #     subject = f"RevUp {product.title} Customer Inquiry"
        #     email_template = 'core/emails/qualifier_inquiry_email.html'
        #     context = dict(first_name=first_name, last_name=last_name,
        #                    email=email, phone=phone, title=title,
        #                    office_type=office_type,
        #                    constituents=constituent_count,
        #                    product=product, product_type="Political")
        #     send_template_email(subject, email_template, context, "sales@revup.com")
        # else:
        if client_type == "candidate":
            profile_title = "Political Candidate"
        elif client_type == "committee":
            profile_title = "Political Committee"
        else:
            return HttpResponseBadRequest("Unrecognized client type")

        # This is a lousy hack to get the AccountProfile. We shouldn't
        # query by name, but alternatives would take a lot more time
        profile = AccountProfile.objects.get(
            title=profile_title, product=product,
            account_type=AccountProfile.AccountType.POLITICAL)

        # Create an External Signup token
        token = ExternalSignupToken.objects.create(
            first_name=first_name,
            last_name=last_name,
            email=email,
            phone=phone,
            organization=title,
            product=product,
            profile=profile,
            addendum1=constituent_count,
            addendum2=office_type)

        # Send an invite email
        subject = "Welcome to RevUp"
        email_template = 'core/emails/qualifier_signup_email.html'
        context = dict(token=token, request=request)
        send_template_email(subject, email_template, context, email,
                            email_type="Onboard Invite")
        return HttpResponse()

    def post(self, request, *args, **kwargs):
        # First check recaptcha. No point wasting time on bots
        recaptcha = request.POST.get("recaptcha")
        client_ip = get_client_ip(request)
        if not (recaptcha and verify_recaptcha(recaptcha, client_ip)):
            return HttpResponseBadRequest("Valid recaptcha required")

        product_type = request.POST.get("product_type", "").lower().strip()
        if product_type == "political":
            return self._process_political(request)
        elif product_type in ('non-profit', 'academic'):
            return self._process_disabled(request, product_type)
        else:
            return HttpResponseBadRequest("Unrecognized product type")


class RefreshTokenView(TemplateView):
    template_name = 'core/refresh_token.html'

    def _get_token(self, token_uuid):
        """Query for the External Token."""
        token = get_object_or_404(ExternalSignupToken.objects.select_related(
            'product', 'profile'), token=token_uuid)
        return token

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['token'] = self._get_token(kwargs.get("token"))
        return context

    def post(self, request, *args, **kwargs):
        """Create a new token with an updated utc_created time and uuid.
        Also resend the welcome onboard email.
        """
        token = self._get_token(kwargs.get("token"))
        new_token = token.clone()

        # Send Onboard Welcome email with token
        new_token.send_token_signup_email(self.request)

        return HttpResponse(status=200)


class OnboardingSignupView(TemplateView):
    template_name = 'core/onboard_signup.html'

    class RefreshToken(Exception):
        def __init__(self, token):
            super().__init__()
            self.token = token
    class IndexRedirect(Exception): pass
    class SplashRedirect(Exception):
        def __init__(self, account_id):
            super().__init__()
            self.account_id = account_id

    def _get_token(self, token_uuid):
        """Query for the External Token."""
        if isinstance(token_uuid, ExternalSignupToken):
            return token_uuid

        token = get_object_or_404(ExternalSignupToken.objects.select_related(
                                  'product', 'profile'), token=token_uuid)

        if token.is_expired and not token.user:
            raise OnboardingSignupView.RefreshToken(token)

        return token

    def _get_accounts(self, user):
        accounts_qs = (
            Account.objects.filter(account_head=user)
                           .select_related('account_profile__product')
                           .prefetch_related('contracts'))
        accounts = []
        for account in accounts_qs:
            accounts.append(dict(
                #TODO: contract=account.contracts.latest(),
                profile=account.account_profile,
                product=account.account_profile.product,
            ))
        return accounts

    def _handle_authenticated(self, token, user):
        """Collect the context for users who are already authenticated"""
        # Retrieve the Recurly plan to display product terms
        plan = recurly.get_subscription_plan(
                                token.product.recurly_plan_code)
        if plan:
            token.product.price = \
                plan.unit_amount_in_cents.currencies['USD'] / 100
            token.product.setup_fee = \
                plan.setup_fee_in_cents.currencies['USD'] / 100
        # Collect the previously-signed contract, if one exists
        contract = token.account.contracts.first() if token.account else None

        context = dict(product=token.product,
                       contract=contract)

        if token.account:
            if token.account.active:
                if token.product.manual_onboarding:
                    raise OnboardingSignupView.IndexRedirect(
                        "Account is already fully setup")
                else:
                    raise OnboardingSignupView.SplashRedirect(token.account_id)
            context['path'] = 'payment'
        elif token.product.manual_onboarding:
            # There is no contract in the manual onboarding flow
            context['path'] = 'payment'
        else:
            context['path'] = 'contract'
        return context

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        token = self._get_token(kwargs.get("token"))
        error_message = kwargs.get("error_message", "")
        user = self.request.user

        # Build the context with common values
        context['token'] = token
        context['user'] = user
        context['error_message'] = error_message
        context['account'] = token.account
        context['account_type'] = token.profile.account_type
        context['office_type'] = token.addendum2
        context['client_type'] = token.profile.title

        if user.is_authenticated:
            # There are a few reasons we would want to log out the user and
            # reprocess:
            # 1) If the token email doesn't match the authenticated user email
            # 2) If the user hasn't been saved in the token (shouldn't happen)
            # 3) The authenticated user doesn't match the token user
            if (user.email != token.email or
                    not token.user or
                    (token.user and token.user != user)):
                logout(self.request)
                return self.get_context_data(**kwargs)
            else:
                context.update(self._handle_authenticated(token, user))
        else:
            # Check if this person already has a RevUpUser
            if RevUpUser.objects.filter(email__iexact=token.email) \
                                .exclude(password="").exists():
                context['path'] = "login"
                context['existing_account'] = Account.objects.filter(
                        account_head__email__iexact=token.email).exists()
            else:
                context['path'] = "signup"
        return context

    def dispatch(self, request, *args, **kwargs):
        try:
            return super(OnboardingSignupView, self).dispatch(
                request, *args, **kwargs)
        except OnboardingSignupView.RefreshToken as exc:
            if not exc.token.product.manual_onboarding:
                return HttpResponseRedirect(reverse('onboard_qualifier'))
            else:
                return HttpResponseRedirect(reverse("refresh_token",
                                                    args=(exc.token.token,)))

    def get(self, request, *args, **kwargs):
        try:
            response = super().get(request, *args, **kwargs)
            if response.status_code == 200 and 'status_code' in kwargs:
                response.status_code = kwargs['status_code']
            return response
        except OnboardingSignupView.IndexRedirect:
            return HttpResponseRedirect(reverse("index"))
        except OnboardingSignupView.SplashRedirect as exc:
            return HttpResponseRedirect(reverse("onboard_setup",
                                                args=(exc.account_id,)))

    def post(self, request, *args, **kwargs):
        path = request.GET.get("path", "").strip().lower()
        # Get the token from the URL. Token is required
        token = self._get_token(kwargs.get("token"))

        if path == "login":
            return self._post_login(request, token)
        elif path == "signup":
            return self._post_signup(request, token)
        elif path == "contract":
            if token.product.manual_onboarding:
                return HttpResponseBadRequest("Invalid Path for this product")
            return self._post_contract(request, token)
        elif path == "payment":
            if token.product.manual_onboarding:
                return self._post_payment_combined(request, token)
            else:
                return self._post_payment(request, token)
        else:
            return HttpResponseBadRequest("Unknown path type")

    def get_field(self, request, field_name, max_length=None):
        field_val = request.POST.get(field_name, "").strip()
        field_len = len(field_val)
        if max_length and field_len > max_length:
            raise ValidationError(f'{field_name} cannot have more than '
                                  f'{max_length} characters. Currently it has '
                                  f'{field_len} characters')
        return field_val

    def _authenticate(self, request, token, email, password, redirect_to=None):
        """Helper function that does the actual authentication and login"""
        user = authenticate(email=email, password=password)
        if user is not None:
            # The password verified for the user
            if user.is_active:
                # Register the login with the session
                login(request, user)

                # Attach the user to the token
                if not token.user:
                    token.user = user
                    token.save()

                if redirect_to:
                    return HttpResponseRedirect(redirect_to=redirect_to)
                else:
                    return self.get(request, token=token)
            else:
                return self.get(request, token=token,
                                error_message="This user has been disabled",
                                status_code=401)
        else:
            # Bad user and/or password
            return self.get(request, token=token,
                            error_message='Invalid email/password',
                            status_code=401)

    def _post_login(self, request, token):
        """Handle POSTs from the 'login' page in the onboarding."""
        email = self.get_field(request, 'email')
        password = self.get_field(request, 'password')
        # If the user requests their existing account, we will log them in
        # and redirect to index
        path = self.get_field(request, "goto")
        redirect_to = reverse("index") if path == "existing" else None

        if email and password:
            return self._authenticate(request, token, email, password,
                                      redirect_to=redirect_to)
        else:
            return self.get(request, token=token,
                            error_message="Email and Password are required",
                            status_code=400)

    def _post_signup(self, request, token):
        """Handle POSTs from the 'signup' page in the onboarding."""
        try:
            first_name = self.get_field(request, "first_name", max_length=64)
            last_name = self.get_field(request, "last_name", max_length=150)
        except ValidationError as err:
            return self.get(request, token=token,
                            error_message=f"{err}",
                            status_code=400)
        email = self.get_field(request, 'email')
        password = self.get_field(request, "password")
        # Verify the password meets requirements
        try:
            PasswordField().run_validators(password)
        except ValidationError as err:
            return self.get(request, token=token,
                            error_message=f"Invalid Password: {err}",
                            status_code=400)

        # A sanity check to make sure the token matches the form
        if token.email.lower() != email.lower():
            return self.get(request, token=token,
                            error_message="Emails don't match",
                            status_code=400)

        with transaction.atomic():
            user, created = RevUpUser.get_or_create_inactive(
                                        email, first_name, last_name,
                                        save=False, select_for_update=True)
            if user.password:
                # If this user already has a password, then it's already
                # been configured. We'll just authenticate them
                return self._authenticate(request, token, email, password)

            user.set_password(password)
            user.is_active = True
            if token.product.manual_onboarding:
                user.accepted_tos = datetime.datetime.now()
            user.save()

        # Login the user after creating/updating it
        user = authenticate(email=user.email, password=password)
        login(request, user)

        # Attach the user to the token
        if not token.user:
            token.user = user
            token.save()
        return self.get(request, token=token)

    def _post_contract(self, request, token):
        contract = self.get_field(request, "contract")
        signature = self.get_field(request, "signature")

        if not token.user or not token.user.is_authenticated:
            # User should be created and authenticated before here
            return self.get(request, token=token)
        # Verify that there is a contract
        elif not all([contract, signature]):
            return HttpResponseBadRequest("Signed contract required")

        with transaction.atomic():
            # Lock the user to prevent multiple accounts from being created by
            # accident
            user = request.user.lock()

            # Check if the account already exists
            token.refresh_from_db()
            # If the account already exists, redirect to the right place
            if token.account:
                if token.account.active:
                    return HttpResponseRedirect(
                            reverse("onboard_setup", args=(token.account_id,)))
                else:
                    return self.get(request, token=token)

            # Create an account
            account = Account.create(token.profile, token.organization,
                                     token.user)
            # Save the contract
            AccountContract.objects.create(account=account, signer=user,
                                           contract=contract,
                                           signature=signature)
            # Attach the account to the token
            token.account = account
            token.save()
        return self.get(request, token=token)

    def _post_payment(self, request, token):
        cc_token = request.POST.get("recurly-token")
        if not cc_token:
            return HttpResponseBadRequest("Credit Card token required")

        if not token.account:
            # This is the wrong place if the user doesn't have an account
            return self.get(request, token=token)

        with transaction.atomic():
            # Lock the user to prevent multiple payments by accident
            request.user.lock()
            account = token.account

            # Process the payment and attach to the account
            try:
                account.get_or_create_recurly_subscription(cc_token)
            except Exception as err:
                return HttpResponseServerError(str(err))
            else:
                # Activate the account
                if not account.active:
                    account.active = True
                    account.save()

            token.redeemed = datetime.datetime.now()
            token.save()

        return HttpResponseRedirect(reverse("onboard_setup",
                                            args=(token.account_id,)))

    def _post_payment_combined(self, request, token):
        """This view combines most of the functionality of post_contract and
        post_payment. This is needed for the premium onboarding flow.

        This will create a RevUp Account and a recurly account
        """
        cc_token = request.POST.get("recurly-token")
        if not (cc_token or token.skip_payment):
            return HttpResponseBadRequest("Credit Card token required")

        if not all([token.user, request.user.is_authenticated,
                    token.user == request.user]):
            # User should be created and authenticated before here
            return self.get(request, token=token)

        with transaction.atomic():
            # Lock the user to prevent multiple accounts from being created by
            # accident
            request.user.lock()

            # Check if the account already exists
            token.refresh_from_db()
            # If the account already exists, redirect to the right place
            if token.account:
                if token.account.active:
                    return HttpResponse(dict(redirect=reverse("index")))
                else:
                    account = token.account
            else:
                # Create an account
                account = Account.create(
                    token.profile, token.organization, token.user,
                    onboarding=True)
                # Attach the account to the token
                token.account = account
                token.save()

        # Using a separate transaction to ensure the Account was created
        # successfully before attempting to charge the card
        with transaction.atomic():
            # Lock the user to prevent multiple payments by accident
            request.user.lock()

            # Process the payment and attach to the account
            if not token.skip_payment:
                try:
                    account.get_or_create_recurly_account(cc_token)
                except Exception as err:
                    return HttpResponseServerError(str(err))

            if not account.active:
                account.active = True
                account.save()

            token.redeemed = datetime.datetime.now()
            token.save()

        return HttpResponse(json.dumps(dict(
            redirect=reverse("onboard_setup", args=(account.id,))
        )))


@method_decorator(login_required, name='dispatch')
@method_decorator(any_group_required(*AdminPermissions.all(),
                                     account_field="account_id"),
                  name='dispatch')
class OnboardingSetupView(TemplateView):
    template_name = 'core/onboard_setup.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        account = get_object_or_404(
            Account.objects.select_related('account_profile__product'),
            id=kwargs.get("account_id")
        )
        user = self.request.user

        # Build the context with common values
        context['user'] = user
        context['account'] = account
        context['client_type'] = account.account_profile.title
        context['product'] = account.account_profile.product
        context['account_type'] = account.account_profile.account_type
        context.update(self._get_settings_context(account))

        if not user.accepted_tos:
            context['path'] = "splash"
        else:
            context['path'] = "settings"
        return context

    def _get_settings_context(self, account):
        """Build the context that's relevant to the settings pages."""
        question_sets = dict()
        anal_config = account.get_default_analysis_config()
        # Verify the analysis config belongs to this account
        if anal_config and not \
                account.analysis_configs.filter(id=anal_config.id).exists():
            anal_config = None

        if anal_config:
            qs_kwargs = {}
            if not self.request.user.is_admin:
                # If user is not an admin, do not show staff only QuestionSets
                qs_kwargs['staff_only'] = False

            # Generate the QuestionSet forms for each of the question sets.
            for qs in anal_config.get_question_sets(**qs_kwargs):
                if qs.id in question_sets or not qs.dynamic_class:
                    continue
                else:
                    question_sets[qs.id] = qs.dynamic_class().get_info(
                        qs, account_id=account.id,
                        analysis_config_id=anal_config.id)
        return dict(
            question_sets=question_sets,
            analysis_config=anal_config
        )

    def post(self, request, *args, **kwargs):
        path = request.GET.get("path", "").strip().lower()
        account = get_object_or_404(
            Account.objects.select_related('account_profile'),
            active=True,
            id=kwargs.get("account_id")
        )

        if path == "tos":
            return self._post_tos(request, account)
        elif path == 'thank_you':
            return self._post_thank_you(request, account)
        else:
            return HttpResponseBadRequest("Unknown path type")

    def _post_tos(self, request, account):
        request.user.accepted_tos = datetime.datetime.now()
        request.user.save()
        return self.get(request, account_id=account.id)

    def _post_thank_you(self, request, account):
        """Send an email to the Customer Success team to notify them
        the client has finished their self-onboarding.  Also create the new
        client ticket in zendesk.
        """
        acc_settings = self._get_settings_context(account)
        question_sets = acc_settings.get('question_sets', [])
        question_sets_ids = [q for q in question_sets]
        analysis_config_ids = [aci.id for aci in account.analysis_configs.all()]
        question_sets_data = QuestionSetData.objects.filter(
            question_set_id__in=question_sets_ids,
            analysis_config_id__in=analysis_config_ids)

        configuration_info = {'allies': [], 'opponents': []}
        for qsd_ in question_sets_data:
            qsd = qsd_.data
            if 'is_ally' in qsd:
                if qsd['is_ally']:
                    configuration_info['allies'].append(qsd['label'])
                else:
                    configuration_info['opponents'].append(qsd['label'])

            else:
                configuration_info.update(qsd)

        # Create organization, user and ticket in zendesk
        self.zendesk_integration(account.organization_name,
                                 account.account_head)

        subject = f"Self-Onboarding Complete - {account.organization_name}"
        email_template = 'core/emails/self_onboarding_complete_email.html'
        # To be able to send a mail to CS even when signup_token doesn't
        # exist on the account
        token = account.signup_token if hasattr(account, 'signup_token') \
            else None
        context = dict(token=token,
                       request=request,
                       account=account,
                       user=request.user,
                       title=account.account_profile.title,
                       product=account.account_profile.product,
                       configuration_info=configuration_info)
        send_template_email(subject, email_template, context,
                            "clients@revup.com")
        return self.get(request, account_id=account.id)

    def zendesk_integration(self, organization_name, account_user):
        """Creates the organization, user and ticket in zendesk for this new
        user and organization.
        """
        # Get the organization id from zendesk and create the new client
        # ticket within zendesk
        organization_id = get_zendesk_organization(organization_name)
        user_id = get_zendesk_user(account_user.email)
        if not user_id:
            user_id = create_or_update_user(
                {'full_name':
                 f'{account_user.first_name} {account_user.last_name}',
                 'email': account_user.email,
                 'organization_name': organization_name})
        if organization_id:
            ticket_data = {
                'organization_id': organization_id,
                'type': 'task',
                'subject': 'New Client',
                'status': 'open',
                'user': user_id,
                'description': 'New client onboarding complete.'
            }
            # only create the ticket if it doesn't already exist
            if not get_zendesk_ticket("New Client", organization_name):
                create_ticket(ticket_data)


@method_decorator(csrf_exempt, 'dispatch')
@method_decorator(allow_ip(settings.RECURLY_NOTIFICATION_IPS), 'dispatch')
class RecurlyWebhook(View):
    # List the deactivate and reactivate account notification types
    deactivate_account_notifications = {
        'failed_payment_notification': DeactivationReason.FAILED_PAYMENT,
        'expired_subscription_notification': DeactivationReason.SUBSCRIPTION_EXPIRED,
        'canceled_account_notification': DeactivationReason.CLOSED_RECURLY_ACCOUNT
    }
    reactivate_account_notifications = ('reactivated_account_notification',
                                        'successful_payment_notification')

    def post(self, request, *args, **kwargs):
        notification = recurly.objects_for_push_notification(request.body)
        notification_type = notification.get('type', None)
        recurly_account = notification.get('account', None)
        recurly_account_code = recurly_account.account_code if \
            hasattr(recurly_account, 'account_code') else None
        revup_account = recurly.get_revup_account(recurly_account_code)

        if not revup_account:
            # This is to enable fixing recurly's account code in the backend.
            # This scenario is possible when billing info is updated directly
            # in recurly instead of going through revup's payment update page
            LOGGER.exception(f'Received recurly notification: '
                             f'{notification_type} for recurly account: '
                             f'{recurly_account_code}. No revup account is '
                             f'associated with this recurly account. '
                             f'Notification payload: {notification.items()}')
            # Acknowledge that the notification was successfully received by
            # sending success status code. If any other status code is sent,
            # recurly tries to resend the notification
            return HttpResponse('success', status=200)

        LOGGER.info(f'Received recurly notification: {notification_type} '
                    f'for revup account title: {revup_account.title} id: '
                    f'{revup_account.id} recurly_account: '
                    f'{recurly_account_code}. Notification payload: '
                    f'{notification.items()}')

        # Deactivate an account if a recurly notification is one of the
        # deactivate_account_notifications
        if notification_type in \
                RecurlyWebhook.deactivate_account_notifications:
            LOGGER.info(('Deactivating revup account id: {0} title: {1} due '
                         'to recurly {2} for recurly_account: {3}').format(
                revup_account.id, revup_account.title, notification_type,
                recurly_account_code))
            # Set the deactivation_reason based on the type of notification
            deactivation_reason = RecurlyWebhook.\
                deactivate_account_notifications.get(notification_type)
            # Deactivate the account
            revup_account.deactivate(deactivation_reason)

        # Reactivate an account if a recurly notification is one of the
        # reactivate_account_notifications
        elif notification_type in \
                RecurlyWebhook.reactivate_account_notifications:
            LOGGER.info(('Reactivating revup account id: {0} title: {1} due '
                         'to recurly {2} for recurly_account: {3}').format(
                revup_account.id, revup_account.title, notification_type,
                recurly_account_code))
            # Reactivate the account
            revup_account.reactivate()

        # Acknowledge that notification was successfully received by sending
        # success status code. If any other status code is sent, recurly tries
        # to resend the notification
        return HttpResponse('success', status=200)


def csrf_failure(request, reason=""):
    """Renders csrf error template when csrf verfication fails on a request"""
    return render(request, template_name='403_csrf.html')
