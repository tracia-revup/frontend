from django.conf.urls import url, include
from django.views.generic import TemplateView

from . import views


urlpatterns = [
    url(r'', include('social_django.urls', namespace='social')),
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^tasks/$', views.task_info, name='task_info'),
    url(r'^task/$', views.task_status, name='task_status'),
    url(r'^terms_of_service/', views.TermsOfServiceView.as_view(),
        name='terms_of_service'),
    url(r'^getting_started/$', views.GettingStartedView.as_view(),
        name='getting_started'),
    url(r'^getting_started/(?P<forced_type>[^/]+)/$',
        views.GettingStartedView.as_view(), name='getting_started_forced'),
    url(r'^getting_started/(?P<forced_type>[^/]+)/(?P<forced_version>[^/]+)/$',
        views.GettingStartedView.as_view(), name='getting_started_forced'),
    url(r'^impersonate/', include('impersonate.urls')),
    url(r'^oauth_finished/$',
        TemplateView.as_view(template_name='core/oauth_finished.html')),
    url(r'^recurly_webhook/$', views.RecurlyWebhook.as_view(), name='recurly_webhook'),
    url(r'^onboard/qualify/$',
        views.OnboardingQualifierView.as_view(), name="onboard_qualifier"),
    url(r'^onboard/refresh_token/(?P<token>[^/]+)/$',
        views.RefreshTokenView.as_view(), name="refresh_token"),
    url(r'^onboard/signup/(?P<token>[^/]+)/$',
        views.OnboardingSignupView.as_view(), name="onboard_signup"),
    url(r'^onboard/setup/(?P<account_id>[^/]+)/$',
        views.OnboardingSetupView.as_view(), name="onboard_setup")
]
