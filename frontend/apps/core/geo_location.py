#!/usr/bin/python
from django.conf import settings

from geopy.geocoders import GeoNames
import logging

LOGGER = logging.getLogger(__name__)


class GeoPyLocator(object):
    COLLECTION = 'locations'
    SCHEMA = 'revup'

    def __init__(self, obj_cache=True):
        self.geo_name_locator = GeoNames(country_bias='USA', timeout=30,
                                         username=settings.GEONAMES_USERNAME)
        self.db_cache = None
        self.obj_cache = {} if obj_cache else None
        self._init()

    def _init(self):
        db_handle = settings.MONGO_DB
        self.db_cache = db_handle[self.COLLECTION]

    def __getstate__(self):
        state = self.__dict__.copy()
        del state['db_cache']
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        self._init()

    def get_geo_names_location(self, loc_input):
        """Get the location information for a given location.

        This method looks into Mongo collection for result otherwise GeoNames.
        """
        try:
            if not isinstance(loc_input, str):
                try:
                    loc_input = loc_input.decode('utf8')
                except UnicodeEncodeError:
                    LOGGER.exception("Unicode encoding error for location %s",
                                      loc_input)
                    return None, None

            # Try to find the location in our caches
            if self.obj_cache is not None:
                try:
                    # First try the local object cache
                    return self.obj_cache[loc_input]
                except KeyError:
                    pass

            # Since we couldn't find the loc in our object cache, let's try the db
            result = self.db_cache.find_one({'loc_key': str(loc_input)})

            if result:
                # DB Cache hit
                obj_id = result['_id']
                location = result.get('location')
            else:
                # No cache hits; make call to GeoNames webservice
                loc_stripped = loc_input.replace("Area", "") \
                                        .replace("Greater", "")
                res = self.geo_name_locator.geocode(loc_stripped,
                                                    exactly_one=True)
                # Update DB cache
                location = res.raw if res else None
                obj_id = self.db_cache.insert({
                    'loc_key': str(loc_input), 'location': location})

            # If we should local cache, set it now
            if self.obj_cache is not None:
                self.obj_cache[loc_input] = (obj_id, location)

            return obj_id, location
        except Exception as err:
            LOGGER.error("Unexpected Error in GeoNames API. "
                          "Error: {}".format(err))
            return None, None
