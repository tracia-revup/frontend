import datetime

from ddtrace import tracer
from django.conf import settings
from django.http import HttpResponseRedirect
from django.urls import reverse

from frontend.apps.seat.models import Seat


_global_excludes = {
    "logout",  # don't intercept logout attempts
    "account_logout",
    "terms_of_service",
    "select_current_seat",
    # Onboarding wizard is outside of the normal site
    "onboard_qualifier",
    "onboard_signup",
    "onboard_setup",
    # Don't prevent password changes
    "password_update",
    "password_change",
    "password_change_done",
    "password_reset",
    "password_reset_done",
    "password_reset_confirm",
    "password_reset_complete",
    # Don't block the staff admin
    "staff_admin_home",
    "staff_admin_create_client",
    "impersonate-stop",
    "impersonate-search",
    "impersonate-start",
}


class FilterBase(object):
    include = []
    exclude = []
    use_global_excludes = True

    ALL_HTTP_METHODS = {'HEAD', 'GET', 'POST', 'PUT', 'DELETE', 'TRACE',
                        'OPTIONS', 'CONNECT', 'PATCH'}
    exclude_methods = []
    include_methods = []

    def __init__(self):
        if self.include and self.exclude:
            raise AttributeError("May only specify one of include or exclude.")

        if self.include_methods and self.exclude_methods:
            raise AttributeError("May only specify one of include_methods or "
                                 "exclude_methods.")

        if self.include_methods:
            self._verify_method_values(self.include_methods)

        if self.exclude_methods:
            self._verify_method_values(self.exclude_methods)

    @tracer.wrap()
    def _verify_method_values(self, methods):
        for method in methods:
            if method not in self.ALL_HTTP_METHODS:
                raise ValueError("Invalid HTTP Method specified: {}".format(
                    method))

    @tracer.wrap()
    def _process_view(self, request, view_func, view_args, view_kwargs):
        url_name = request.resolver_match.url_name
        # If this view is one of the ones we exclude, then we'll do nothing
        if ((self.exclude and url_name in self.exclude) or
                (self.use_global_excludes and url_name in _global_excludes)):
            return

        # If this view is not one of the ones we include, then we do nothing.
        if self.include and url_name not in self.include:
            return

        http_method = request.method

        # If the http method used is one of the ones we exclude, then we'll do
        # nothing.
        if self.exclude_methods and http_method in self.exclude_methods:
            return
        # If the http method used is not one of the ones we include, then we do
        # nothing.
        if self.include_methods and http_method not in self.include_methods:
            return

        # If we get here, either the view: was not in exclude, was in include,
        # or both exclude and include were blank (meaning "all views")
        return self.process_view(request, view_func,
                                 view_args, view_kwargs)

    def _response(self, request, url_name, args=None, include_next=True):
        next = f"?next={request.path}" if include_next else ""
        return HttpResponseRedirect(f"{reverse(url_name, args=args)}{next}")

    def process_view(self, request, view_func, view_args, view_kwargs):
        """Override this in subclasses to indicate whether your filter should
        redirect or not.
        Return an HttpResponseRedirect to force a redirect.
        """
        return


class TermsOfServiceFilter(FilterBase):
    """Redirects (logged-in) user to ToS if they haven't accepted it yet."""
    @tracer.wrap()
    def process_view(self, request, view_func, view_args, view_kwargs):
        if not request.user.is_authenticated:
            # Don't enforce ToS unless logged in
            return
        if hasattr(request.user, 'accepted_tos') and request.user.accepted_tos:
            return
        return self._response(request, 'terms_of_service')


class ActiveSeatFilter(FilterBase):
    """Redirect to a page where they must select their current seat.

    We exclude the activation page so account_heads can activate their accounts
    """
    exclude = {"campaign_admin_activate"}

    @tracer.wrap()
    def process_view(self, request, view_func, view_args, view_kwargs):
        # Don't enforce unless logged in. Also don't enforce on the API.
        if (request.path.startswith('/rest/') or
                request.path.startswith('/staff_admin/') or not
                request.user.is_authenticated):
            return

        seat = request.user.current_seat
        # Let's do some sanity checking:
        # The seat must belong to this user, and it must be active. Also,
        # the seat's account must be active, unless the user is the
        # account_head; they will be redirected by the ActivationFilter
        if (seat and seat.user_id == request.user.id and
                seat.state in Seat.States.ASSIGNED and
                (seat.account.active or
                     seat.account.account_head_id == seat.user_id)):
            return
        # The seat can only be None for staff users who have no other seats
        # elif (not seat and request.user.is_admin and not
        #         request.user.seats.filter(account__active=True)
        #                           .assigned().exists()):
        #     return
        # Anything else and the user needs to select a seat
        else:
            request.user.reset_current_seat(None, request=request)
            return self._response(request, 'select_current_seat')


class AccountActivationFilter(FilterBase):
    """If this campaign admin is trying to access the admin for an inactive
       account, they will be redirected to the account activation page.
    """
    exclude = {"campaign_admin_activate"}

    @tracer.wrap()
    def process_view(self, request, view_func, view_args, view_kwargs):
        if not request.user.is_authenticated:
            return

        current_seat = request.user.current_seat
        if (current_seat and not current_seat.account.active and
                current_seat.account.account_head_id == request.user.id):
            return self._response(request, 'campaign_admin_activate')


class AccountOnboardingFilter(FilterBase):
    """If this Account is currently in the process of being onboarded,
    the Account cannot access the site. The Account head will need to be
    directed to the onboarding setup view.
    If the user is revup staff, they should be allowed to proceed.
    """
    @tracer.wrap()
    def process_view(self, request, view_func, view_args, view_kwargs):
        # Don't enforce unless logged in. Also don't enforce on the APIs or
        # for revup staff members, as they may need to access the account.
        if (request.path.startswith('/rest/') or
                request.path.startswith('/staff_admin/') or
                not request.user.is_authenticated or
                request.user.is_impersonate or
                request.user.is_admin):
            return

        current_seat = request.user.current_seat
        if current_seat and current_seat.account.onboarding:
            if current_seat.account.account_head_id == request.user.id:
                return self._response(request, 'onboard_setup',
                                      args=(current_seat.account.id,),
                                      include_next=False)
            else:
                # If the user isn't the account head, their only option is to
                # go select a different seat. Or log out.
                return self._response(request, 'select_current_seat')



class PreferredDomainFilter(FilterBase):
    """If an account has a preferred domain configured, this will redirect
       them to the correct domain if they are not on their preferred.
    """
    exclude = {"contact_file_upload", "contacts_import_cs"}
    exclude_methods = {'PUT', 'POST'}

    @tracer.wrap()
    def process_view(self, request, view_func, view_args, view_kwargs):
        # There are times when we might want to force the domain.
        if ("force_domain" in request.GET or
                # Any API requests should stay on the current domain, for
                # ajax reasons
                request.path.startswith("/rest/") or
                not request.user.is_authenticated or
                # If the account doesn't have a current seat, there's nothing
                # we can do yet.
                not request.user.current_seat):
            return

        # If the current account has a preferred domain, we need to check
        # if the current domain matches it.
        preferred = request.user.current_seat.account.preferred_domain
        host = request.get_host()
        if preferred:
            # If the current host is not the account's preferred, change it.
            # Otherwise, leave it the same.
            if host != preferred:
                domain = preferred
            else:
                return
        # If the account doesn't have a preferred, but are not on the right
        # domain, redirect them. This might happen if the user is returning
        # to the site from the contact import page.
        elif settings.DEFAULT_DOMAIN and host != settings.DEFAULT_DOMAIN:
            domain = settings.DEFAULT_DOMAIN
        else:
            return

        return HttpResponseRedirect("http{}://{}{}".format(
            's' if request.is_secure() else '',
            domain, request.path), status=307)


class ExpiredPasswordFilter(FilterBase):
    """Redirects (logged-in) user to update password if their password has
    expired.
    """
    @tracer.wrap()
    def process_view(self, request, view_func, view_args, view_kwargs):
        if not request.user.is_authenticated:
            # Don't enforce ToS unless logged in
            return
        if not request.user.password_expiration_date:
            # No expiration date is set for user
            return
        if datetime.date.today() >= request.user.password_expiration_date:
            return self._response(request, 'password_update')


class ForcedRedirectMiddleware(object):
    filters = [TermsOfServiceFilter(),
               ExpiredPasswordFilter(),
               ActiveSeatFilter(),
               AccountOnboardingFilter(),
               AccountActivationFilter(),
               PreferredDomainFilter()]

    def __init__(self, get_response):
        self.get_response = get_response

    @tracer.wrap()
    def __call__(self, request):
        response = self.get_response(request)
        return response

    @tracer.wrap()
    def process_view(self, request, view_func, view_args, view_kwargs):
        """Call the 'process_view' in each of the filters.

        Filters are processed in order. Each filter should define a
        'process_view' method that either returns None or returns
        an HttpResponseRedirect in the event of redirection.
        """
        for filter_ in self.filters:
            redirect = filter_._process_view(request, view_func,
                                             view_args, view_kwargs)
            if redirect:
                return redirect
