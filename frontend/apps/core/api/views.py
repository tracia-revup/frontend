from rest_framework import response, views, permissions

from frontend.apps.analysis.models import Result
from frontend.apps.campaign.routes import UNIVERSITY_ROUTE, NONPROFIT_ROUTE
from frontend.apps.contact_set.models import ContactSet
from frontend.apps.filtering.query_engine import QueryEngine
from frontend.apps.filtering.utils import FilterSet


class MobileHomepageView(views.APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        countable_cs = self._get_countable_contact_sets(request)

        # Get the contact counts
        total_contacts = sum((cs.count for cs in countable_cs))

        nc_filters, ditr_filters = self._get_filters(request)
        # Get the non-contributing contact counts
        nc_contacts = self._get_filter_counts(countable_cs, nc_filters)
        # Get the diamond in the rough contact counts
        ditr_contacts = self._get_filter_counts(countable_cs, ditr_filters)

        data = {"total_contacts": total_contacts,
                "non_contrib_contacts": nc_contacts,
                "ditr_contacts": ditr_contacts}
        return response.Response(data)

    def _get_filter_counts(self, contact_sets, filters):
        """Iterate over the given contact sets, apply the given filters,
        and get a summated count of contacts matching those filters across
        contact sets.
        """
        count = 0
        for cs in contact_sets:
            # We can't count Results if the CS has no analysis
            if not cs.analysis:
                continue

            result_filters = cs.result_filters_cache
            # We only query the result filters because we know we won't
            # be using the user filters in this API
            engine = QueryEngine(result_filters, Result)
            partition = cs.partition_cache
            try:
                qry = engine.filter(
                    filters,
                    analysis=cs.analysis,
                    partition=partition,
                    user=self.request.user)
            except ValueError:
                # This CS/analysis doesn't support this filter
                continue
            count += cs.queryset(partition=partition).filter(qry).count()
        return count

    def _get_filters(self, request):
        """The filters change depending on if we are using an academic
        client or political.
        """
        if UNIVERSITY_ROUTE.should_route(request, self):
            contrib_filters = {'client-giving': ['true'],
                               'client-giving-this-year': ['false']}
            ditr_filters = {'aca-ditr': ['true']}
        elif NONPROFIT_ROUTE.should_route(request, self):
            contrib_filters = {'client-giving': ['true'],
                               'client-giving-this-year': ['false']}
            ditr_filters = {'nfp-ditr': ['true']}
        else:
            contrib_filters = {'client-giving-past': ['true'],
                               'client-giving-current': ['false']}
            ditr_filters = {'ditr': ['true']}
        return contrib_filters, ditr_filters

    def _get_countable_contact_sets(self, request):
        """Return the contact sets that we want to use for counting.
        This would be the User's Root ContactSet, and the Account's
        ContactSet. However, we would not return the Account's Root
        if the user does not have permission.
        """
        if not request.user.current_seat:
            return []

        # Get the contact sets for the requesting users's current seat
        user_owned, account_owned, _ = \
            ContactSet.get_seat_contact_sets(request.user.current_seat)

        # Find the root contact set in each group
        rootf = lambda x: x.source == ContactSet.Sources.ROOT
        user_root = list(filter(rootf, user_owned))
        account_root = list(filter(rootf, account_owned))

        # This is a bit of a hack to reduce the number of queries in the
        # method `_get_filter_counts`
        roots = user_root + account_root
        for cs in roots:
            cs.partition_cache = cs.get_partition_configs(cs.analysis).first()
            cs.result_filters_cache = FilterSet(
                user=self.request.user,
                analysis=cs.analysis).get_result_filters()
        return roots
