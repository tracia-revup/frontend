
PRODUCT_TRANSLATIONS = {
    "All Contacts": {
        "political": "All Contacts",
        "nonprofit": "All Records",
        "default": "All Contacts"
    },
    "Key Contribution": {
        "political": "Correlated Campaign",
        "nonprofit": "Correlated Organization",
        "default": "Key Contribution"
    },
    "Key Contributions": {
        "political": "Correlated Campaigns",
        "nonprofit": "Correlated Organizations",
        "default": "Key Contributions"
    },
    "Client Contacts": {
        "political": "House File",
        "nonprofit": "Source Data File",
        "default": "Client Contacts"
    },
    "Client Information": {
        "political_committee": "Committee Information",
        "political_candidate": "Candidate Information",
        "nonprofit": "Organization Information",
        "default": "Client Information"
    },
    "Contacts": {
        "political": "Contacts",
        "nonprofit": "Records",
        "default": "Contacts"
    },
    "Contact Manager": {
        "political": "Contact Manager",
        "nonprofit": "Source Manager",
        "default": "Contact Manager"
    },
    "Ranking": {
        "political": "Ranking",
        "nonprofit": "Analyzed Records",
        "default": "Ranking"
    }
}


class ProductToTranslationKeyMap:
    """Order of the keys to try for translation per product"""
    POLITICAL_COMMITTEE = ['political_committee', 'political', 'default']
    POLITICAL_CANDIDATE = ['political_candidate', 'political', 'default']
    POLITICAL = ['political', 'default']
    NONPROFIT = ['nonprofit', 'default']
    ACADEMIC = ['academic', 'default']


def build_reverse_map():
    """Builds a map of translations for each string per product"""
    # Get the list of products defined in `ProductToTranslationKeyMap`
    products_list = [p for p in dir(ProductToTranslationKeyMap)
                     if not p.startswith('__')]
    reverse_translation_map = {}

    for product in products_list:
        current_product_translations = {}
        translation_keys = getattr(ProductToTranslationKeyMap, product)
        # For each string defined in the `PRODUCT_TRANSLATIONS` get the
        # current product specific translation and add it to
        # `current_product_translations`
        for each_str in PRODUCT_TRANSLATIONS:
            # Try all the translation keys associated with the product for
            # reverse translation.
            for key in translation_keys:
                translated_str = PRODUCT_TRANSLATIONS.get(
                    each_str).get(key, '')
                if translated_str:
                    current_product_translations.update(
                        {translated_str: each_str})
        # Add the product and the product specific translations to the
        # `reverse_translation_map`
        reverse_translation_map.update({product: current_product_translations})
    return reverse_translation_map


PRODUCT_SPECIFIC_REVERSE_TRANSLATION = build_reverse_map()


def translate(str_, product_type):
    """
    Translate the given string based on product_type
    :param str_: (string) String to be translated
    :param product_type: (string) Type of the product. If the product type has
    more than one word, the words could be separated by `_` or ` `(space) and
    be in any case.
    Examples: political_committee, Political Candidate, ACADEMIC, nonprofit
    :return: (string) Translated version of the string if the translation
    exists for the specified product_type. Otherwise, returns
    the original string.
    """
    # If a translation doesn't exist for the specified string, return the
    # original string
    if str_ not in PRODUCT_TRANSLATIONS:
        return str_

    # If the specified product, isn't defined in the
    # ProductToTranslationKeyMap, return original string.
    # Replace spaces with underscores in the `product_type` string
    product_type = product_type.strip().replace(" ", "_").upper()
    try:
        # Get the list of keys that could be used for translation
        translation_keys = getattr(ProductToTranslationKeyMap, product_type)
    except AttributeError:
        return str_

    # If the translation doesn't exist for the specified product, return
    # default string. If it exists, return the translated string.
    for key in translation_keys:
        translated_str = PRODUCT_TRANSLATIONS.get(str_).get(key, '')
        if not translated_str and key != 'default':
            continue
        return translated_str


def reverse_translate(str_, product_type):
    # If the specified product, isn't defined in the
    # ProductToTranslationKeyMap, return original string.
    # Replace spaces with underscores in the `product_type` string
    product_type = product_type.strip().replace(" ", "_").upper()
    if product_type not in PRODUCT_SPECIFIC_REVERSE_TRANSLATION:
        return str_

    # Get the reverse map of product specific translations
    current_product_translations = PRODUCT_SPECIFIC_REVERSE_TRANSLATION.get(
        product_type)

    # If the specified string exists in `current_product_translations` return
    # the original version of the translated string. Else, return the string
    # as is.
    return current_product_translations.get(str_, str_)
