from importlib import import_module
import logging
import sys

from django.apps import apps
from django.core.management.base import BaseCommand, CommandError
from django.db import connections, DEFAULT_DB_ALIAS
from django.db.migrations.loader import MigrationLoader
from django.utils.module_loading import module_has_submodule


logger = logging.getLogger()


class Command(BaseCommand):
    help = ("Determine if there are any migrations to be run. Exit with "
            "status code 2 if migrations need to be run.")

    def add_arguments(self, parser):
        parser.add_argument('--database', action='store', dest='database',
                            default=DEFAULT_DB_ALIAS,
                            help='Nominates a database to synchronize. '
                            'Defaults to the "default" database.')

    def handle(self, *args, **options):
        # Import the 'management' module within each installed app, to register
        # dispatcher events.
        for app_config in apps.get_app_configs():
            if module_has_submodule(app_config.module, "management"):
                import_module('.management', app_config.name)

        db = options.get('database')
        connection = connections[db]

        # Load migrations from disk/DB
        loader = MigrationLoader(connection, ignore_no_migrations=True)
        graph = loader.graph

        for app_name in loader.migrated_apps:
            for node in graph.leaf_nodes(app_name):
                for plan_node in graph.forwards_plan(node):
                    if plan_node not in loader.applied_migrations:
                        self.stdout.write("There are migrations to be run")
                        # we found a migration that needs to be applied
                        sys.exit(2)

        self.stdout.write("There are *NO* migrations to be run")
