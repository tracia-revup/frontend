#! /usr/bin/python

import os

from django.conf import settings
from django.core.management.base import BaseCommand
from subprocess import call, check_output

class Command(BaseCommand):
    def handle(self, *args, **options):
        os.chdir(settings.SITE_ROOT)
        status = check_output(['git status --porcelain'], shell=True).split(
            '\n')
        created_version = '?? frontend/version'
        modified_version = ' M frontend/version'
        if created_version in status or modified_version in status:
            call('git add version >/dev/null', shell=True)
            call('git commit -C HEAD --amend --no-verify >/dev/null',
                shell=True)
