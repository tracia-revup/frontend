#! /usr/bin/python

from io import open
import os

from django.conf import settings
from django.core.management.base import BaseCommand
from subprocess import check_output

class Command(BaseCommand):
    def handle(self, *args, **options):
        os.chdir(settings.SITE_ROOT)
        with open('version', 'w') as f:
            version = check_output(['git describe --long'], shell=True)
            release_date = check_output('git log -1 --format="%ct"',
                shell=True)
            f.write('{}{}'.format(version, release_date))
