import datetime
from io import open
import json

from django.conf import settings
from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from django.urls import reverse

from frontend.apps.authorize.models import RevUpUser
from frontend.apps.contact.models import (Contact, Address, EmailAddress,
                                          Organization, PhoneNumber)
from frontend.apps.campaign.models.base import (
    AccountProfile, Account, Product)


class Command(BaseCommand):
    help = "Used to setup the dev environment Django"
    contacts_path_default = "../revup_etl/frontend_test_data/contacts.txt"

    def add_arguments(self, parser):
        parser.add_argument('-e', '--email', dest="email",
                            help="Specify your email address for your superuser")
        parser.add_argument('--contacts_path', dest="contacts_path",
                            default=self.contacts_path_default)
        parser.add_argument('--no_superuser', dest="no_superuser", default=False,
                            action="store_true", help="Don't create a superuser")
        parser.add_argument('--no_campaign', dest="no_campaign", default=False,
                            action="store_true", help="Don't create a campaign")
        parser.add_argument('--import-contacts', default=False,
                            action="store_true", help="Whether to automatically import contacts")

    def handle(self, *args, **options):
        """
        Check settings and call the handle() function to set up
        development environments.
        """
        if not settings.DEBUG:
            raise CommandError(
                "This should only be run in dev environments. If this -is- a "
                "dev environment, set DEBUG to True in your settings file.")

        # Extract out the options
        do_import_contacts = options.get('import_contacts')
        create_superuser = not options.get('no_superuser')
        email = options.get('email')
        campaign_email = None
        if not options.get('no_campaign'):
            campaign_email = email

        contacts_path = options.get('contacts_path')
        if not email and create_superuser:
            raise CommandError("Email is required. See --help for more info.")

        with transaction.atomic():
            resp = handle(email, contacts_path, create_superuser,
                          campaign_email, do_import_contacts)

        if 'path' not in resp:
            return

        url = "http://local.revup.com:8000{}".format(resp['path'])
        print("\n***IMPORTANT*** Visit '{}' to "
              "finish setting up your user.\n".format(url))


def handle(email, contacts_path, create_superuser, campaign_email, do_import_contacts):
    """
    This is the entry point when setting up users for development
    or for performance testing. This must not be exposed outside
    those environments.
    """
    # Create the Campaign Admin group if it doesn't exist
    group = Group.objects.get_or_create(name=settings.ACCOUNT_ADMIN_GROUP_NAME)

    resp = {}
    if create_superuser:
        user, created = RevUpUser.get_or_create_inactive(email, save=False)
        user.is_staff = True
        user.is_superuser = True
        user.save()
        user.groups.add(group[0])
        token = user.generate_token()
        resp['path'] = reverse("finish_account_register",
                               args=(token.token,))
    else:
        user = RevUpUser.objects.get(id=1)

    if campaign_email:
        campaign_setup(user, campaign_email)

    if do_import_contacts:
        import_contacts(contacts_path, user)

    return resp


def campaign_setup(user, email):
    from frontend.apps.campaign.models import Event

    if email != user.email:
        # This should only happen for performance tests.
        account_head = RevUpUser.objects.get(email=email)
        campaign = Account.objects.get(
            account_head=account_head)
        campaign.create_seat(user)
    else:
        # Normal path for test users.
        product = Product.objects.get(title="Premium",
                                      product_type=Product.ProductType.GLOBAL)
        political, created = AccountProfile.objects.get_or_create(
                            account_type="P", title="Political Candidate",
                            product=product)
        campaign = Account.create(political, "Ro Khanna for Congress", user,
                                  20, active=True)

    today = datetime.date.today()
    event_date = today + datetime.timedelta(days=365)
    event = Event(event_name="Private event, " + user.last_name + " home",
                  event_date=event_date, goal=50000,
                  price_points=[1000, 2600, 5200], account=campaign)
    event.save()
    user.add_events(event)


def import_contacts(file_path, user):
    with open(file_path, "r") as f:
        contacts = {}
        subcontacts = {}
        for l in f.readlines():
            line = json.loads(l)

            # Exclude certain fields since they are mongo specific
            data = {k: v for k, v in line.items()
                    if k not in ('_id', 'addresses', 'contacts', 'user_id',
                                 'email_addresses', 'organizations',
                                 'phone_numbers', 'parent', 'full_name',
                                 'organization', 'email')}
            data['user'] = user
            data = clean_data(data)
            contact = Contact.objects.create(**data)

            # Create records for all the nested values
            m2o_fields(line.get('addresses'), contact, Address)
            m2o_fields(line.get('email_addresses'), contact, EmailAddress)
            m2o_fields(line.get('organizations'), contact, Organization)
            m2o_fields(line.get('phone_numbers'), contact, PhoneNumber)

            # Save the subcontacts to be applied later
            for c in line.get('contacts'):
                subcontacts[c["$oid"]] = contact
            contacts[line['_id']['$oid']] = contact

        for sid, parent in subcontacts.items():
            subcontact = subcontacts[sid]
            subcontact.parent = parent
            subcontact.save()


def m2o_fields(data, contact, model):
    for d in data:
        d['contact'] = contact
        model.objects.create(**d)


def clean_data(data):
    """Some special handling for dirty data."""
    for k, v in data.items():
        if k in ('additional_name', 'name_prefix', 'name_suffix') and not v:
            data[k] = ""
    return data
