from datetime import datetime
from django.dispatch import receiver, Signal

from frontend.apps.analysis.utils import get_analysis_score_min_max

process_import_meta_signal = Signal(providing_args=['contact_set'])
process_analysis_meta_signal = Signal(
    providing_args=['contact_set', 'analysis_results', 'partition_config_ids'])


@receiver(process_import_meta_signal)
def add_timestamp(sender, contact_set, **kwargs):
    """Adds `imported` info to contact-set meta"""
    contact_set.set_meta('imported', datetime.now())


@receiver(process_analysis_meta_signal)
def calculate_analysis_score_min_max(
        sender, contact_set, analysis_results, partition_config_ids, **kwargs):
    """Adds normalized score info to contact-set meta"""
    score_minmaxes = get_analysis_score_min_max(
        analysis_results,
        partition_config_ids)
    contact_set.set_meta('normalize', score_minmaxes)
