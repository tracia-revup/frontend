from celery.app.task import TaskType
from django.core.management.base import BaseCommand

from frontend.apps.authorize.utils import create_revuptask_for_contact_source
from frontend.apps.contact_set.models import ContactSet
from frontend.apps.contact_set.tasks import delete_contact_set_task


class Command(BaseCommand):
    help = "Find and clean up orphaned contact sets that were not properly " \
           "deleted."
    args = '<account_id account_id ...>'

    def add_arguments(self, parser):
        parser.add_argument('account', type=int)

    def handle(self, *args, **options):
        """This command will find any orphaned contact sets that were not
        properly deleted after being archived."""
        account = options['account']
        orphaned_contact_sets = ContactSet.objects.filter(
            source__in=ContactSet.Sources.auto_gen_choices).\
            exclude(archived=None)

        if account:
            orphaned_contact_sets = orphaned_contact_sets.filter(
                account_id=account)

        for contact_set in orphaned_contact_sets:
            # Create the revup task for deleting contacts
            try:
                create_revuptask_for_contact_source(
                    contact_set, TaskType.DELETE_CONTACTS)
            except RuntimeError:
                pass
            else:
                # Delete the orphaned contact set
                delete_contact_set_task.apply_async(
                        args=(contact_set.id,)
                    )
