from collections import Iterable
from functools import reduce
from funcy import get_in, set_in
import logging

from ddtrace import tracer
from django.contrib.auth.models import AnonymousUser
from django.contrib.postgres import fields
from django.core.exceptions import ValidationError
from django.db import models, transaction
from django.dispatch import receiver
from django_extensions.db.models import TitleDescriptionModel, TimeStampedModel
from jsonfield import JSONField

from frontend.apps.analysis.models.results import Result
from frontend.apps.contact.models import Contact, ImportRecord
from frontend.apps.contact_set.signals import (process_import_meta_signal,
                                               process_analysis_meta_signal)
from frontend.apps.contact_set.utils import ContactSetCategories
from frontend.apps.core.product_translations import translate
from frontend.apps.filtering.query_engine import QueryEngine
from frontend.apps.filtering.utils import FilterSet
from frontend.libs.utils.model_utils import Choices, ArchiveModelMixin


LOGGER = logging.getLogger(__name__)


class ContactSetException(Exception): pass


class ContactSetSeatBase(models.Model):
    seat = models.ForeignKey('seat.Seat', on_delete=models.CASCADE)
    shared_dt = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        abstract = True
        unique_together = (("contact_set", "seat"),)

    def clean(self):
        super(ContactSetSeatBase, self).clean()
        if self.contact_set.account_id != self.seat.account_id:
            raise ValidationError(
                "Cannot share a ContactSet with a Seat that is "
                "on a different account")

    def save(self, *args, **kwargs):
        self.full_clean()
        return super(ContactSetSeatBase, self).save()


class ContactSetsSeats(ContactSetSeatBase):
    """Through table between ContactSet and Seat

    Contains extra permissions array field with permissions seat has on
    ContactSet.
    The VIEW permission is implied by the existence of a record.

    MODIFY:
        The MODIFY privilege means that seat is allowed to modify the query on
        the ContactSet.

    SHARE:
        The SHARE privilege allows a user to grant and revoke the VIEW
        privilege for other users.
    """
    class Permissions(Choices):
        choices_map = [
            ("MOD", "MODIFY", "Modification permission"),
            ("SHR", "SHARE", "Share privileges"),
        ]
    contact_set = models.ForeignKey('contact_set.ContactSet', on_delete=models.CASCADE)
    permissions = fields.ArrayField(
        models.CharField(max_length=3, choices=Permissions.choices()),
        default=[], blank=True)


class ContactSetQuerySet(models.QuerySet):

    def active(self):
        return self.filter(archived=None)

    def standard(self):
        """The standard query for querying ContactSets.
        This just does some basic select_relateds, and excludes contact sets
        that have not completed an analysis.
        """
        return self.active().exclude(analysis=None)


class ContactSetBase(ArchiveModelMixin, TitleDescriptionModel,
                     TimeStampedModel):
    account = models.ForeignKey('campaign.Account', on_delete=models.CASCADE, related_name='+')
    user = models.ForeignKey('authorize.RevUpUser', on_delete=models.CASCADE,
                             related_name="created_%(class)ss")
    analysis = models.ForeignKey('analysis.Analysis', blank=True, null=True,
                                 on_delete=models.SET_NULL)

    count = models.PositiveIntegerField(blank=True, default=0)

    objects = ContactSetQuerySet.as_manager()

    class Meta:
        abstract = True

    @property
    def is_account_owned(self):
        return self.user_id == self.account.account_user_id

    @property
    def is_import_autogen(self):
        return False

    @property
    def is_root(self):
        return False

    @property
    def is_user_generated(self):
        return False

    def update_count(self):
        """Convenience method to update the count without knowing the CS
        internals.
        This should only be used on a CS that has finished being created.
        """
        self._update_count()

    def _update_count(self):
        self.count = self.queryset().count()

    def has_view_permission(self, user):
        return bool(self.user_permissions(user)[0].get("results"))

    def user_permissions(self, user):
        """Get the user's permissions for this ContactSet.

        There are three major factors that determine the permissions
          1) User owns the CS
               * Can do anything
          2) User is an admin and the CS is owned by the account
               * Can view results and notes; no prospects
          3) CS was shared with the user
               * Depends on the permissions set in the sharing through table
                    * Note: For now, we are only supporting "view"
               * No prospects

        :returns: tuple (permissions dict, ContactSet ownership category)
        """
        from frontend.apps.seat.models import Seat

        def _generate_result(v, n, p, c, cat):
            return {"results": v,
                    "notes": n,
                    "prospects": p,
                    "call_time": c}, cat

        try:
            # If the user isn't logged in, raise `Seat.DoesNotExist` exception
            # explicitly. This is required to avoid error in
            # `check_related_objects` method of `query` module
            if isinstance(user, AnonymousUser):
                raise Seat.DoesNotExist
            seat = Seat.objects.assigned().get(user=user, account=self.account)
        except Seat.DoesNotExist:
            # If they have no seat, they have no permission to view this CS
            return _generate_result(False, False, False, None, None)

        has_call_time_access = seat.has_permissions(
                                    [Seat.AdminPermissions.MODIFY_CALLTIME])
        # User owns the CS
        if self.user == user:
            view = True
            notes = True
            prospects = True
            call_time = False if has_call_time_access else None
            cs_category = ContactSetCategories.OWNED
        # Contact set is owned by the account and user has permission
        elif (self.is_account_owned and
              seat.has_permissions([Seat.AdminPermissions.VIEW_CONTACTS,
                                    Seat.AdminPermissions.MODIFY_CONTACTS])):
            view = True
            notes = True
            prospects = False
            cs_category = ContactSetCategories.ADMIN
            # null call_time typically means the user has no permission
            # to access call time at all, so we should not show the column.
            call_time = has_call_time_access or None
        # CS was shared with the user
        elif self.shared_with.filter(user=user).exclude(
                state=Seat.States.REVOKED).exists():
            view = True
            notes = False
            prospects = False
            cs_category = ContactSetCategories.SHARED
            if self.is_account_owned:
                call_time = has_call_time_access or None
            else:
                call_time = False
        else:
            view = False
            notes = False
            prospects = False
            call_time = False if has_call_time_access else None
            cs_category = None

        return _generate_result(view, notes, prospects, call_time, cs_category)

    def queryset(self, query=None, **kwargs):
        raise NotImplementedError

    def build_query(self, query_on=Result):
        raise NotImplementedError

    @classmethod
    def get_user_owned_cs(cls, seat, **kwargs):
        # Get the ContactSets that are owned by this user
        queryset = cls._get_seat_contact_set_qs(seat, **kwargs)
        return queryset.filter(user=seat.user)

    @classmethod
    def get_account_owned_cs(cls, seat, **kwargs):
        # Get the ContactSets that are owned by the account, and this user
        # has permission to view them
        if seat.has_permissions(any=[seat.AdminPermissions.VIEW_CONTACTS]):
            queryset = cls._get_seat_contact_set_qs(seat, **kwargs)
            return list(queryset.filter(user=seat.account.account_user))
        else:
            return cls.objects.none()

    @classmethod
    def get_shared_cs(cls, seat, **kwargs):
        # Get the ContactSets that have been shared with this user/seat
        queryset = cls._get_seat_contact_set_qs(seat, **kwargs)
        return queryset.filter(contactsetsseats__seat=seat).distinct()

    @classmethod
    def get_seat_contact_sets(cls, seat, augment_queryset=None,
                              return_on_first_match=False,
                              require_analysis=True, order_by=None):
        """Universal method to get all of the contact sets for a seat.

        :param seat: The seat to get the contact sets for
        :param augment_queryset: Augment the queryset. This is for things like
                                'order_by', 'annotate', or 'filter'
        :param return_on_first_match: If we only want one ContactSet, we don't
                                      need to query every group. Return as soon
                                      as we find any CS
        :param order_by: An iterator of fields to order by
        :return: Three CS groups: user-owned, account-owned,
                                  and shared with the user/seat
        """
        # Get the ContactSets that are owned by this user
        user_owned = cls.get_user_owned_cs(seat,
                                           augment_queryset=augment_queryset,
                                           require_analysis=require_analysis,
                                           order_by=order_by)
        if user_owned and return_on_first_match:
            return user_owned, [], []

        # Get the ContactSets that are owned by the account
        account_owned = cls.get_account_owned_cs(seat,
                                           augment_queryset=augment_queryset,
                                           require_analysis=require_analysis,
                                           order_by=order_by)
        if account_owned and return_on_first_match:
            return user_owned, account_owned, []

        # Get the ContactSets that have been shared with this user/seat
        shared = cls.get_shared_cs(seat,
                                   augment_queryset=augment_queryset,
                                   require_analysis=require_analysis,
                                   order_by=order_by)

        return user_owned, account_owned, shared

    @classmethod
    def _get_seat_contact_set_qs(cls, seat,
                                 augment_queryset=None, require_analysis=True,
                                 order_by=None):
        """Utility method for the get_*_cs methods below."""
        if require_analysis:
            queryset = cls.objects.standard()
        else:
            queryset = cls.objects.active()
        queryset = queryset.filter(account=seat.account) \
                           .select_related("analysis__analysis_config")

        # Apply the augment queryset to the queryset.
        if augment_queryset is not None:
            queryset &= augment_queryset

        # Order_by should be an iterator
        if order_by:
            queryset = queryset.order_by(*order_by)
        return queryset

    @classmethod
    def get_partition_configs(cls, analysis, as_ids=True):
        if not analysis.partition_set_id:
            return []
        partition_configs = analysis.partition_set.partition_configs.all()

        if as_ids:
            partition_configs = partition_configs.values_list('pk', flat=True)
        return partition_configs

    @classmethod
    def bulk_update_analysis(cls, *args, **kwargs):
        pass  # noop


class ContactSet(ContactSetBase):
    class Sources(Choices):
        choices_map = [
            ('AB', "ADDRESS_BOOK", "VCard"),
            ('AD', "ANDROID", "Android"),
            ('CL', "CLONED", "Cloned"),
            ('CV', "CSV", "CSV"),
            ('FB', "FACEBOOK", 'Facebook'),
            ('GM', "GMAIL", 'Gmail'),
            ('HF', "HOUSEFILE", "HouseFile"),
            ('IP', "IPHONE", "iPhone"),
            ('LI', "LINKEDIN", 'LinkedIn'),
            ('NB', "NATIONBUILDER", "NationBuilder"),
            ('OU', "OUTLOOK", "Outlook"),
            ('PR', "PERSONA", "Persona"),
            ('SV', "SAVED", 'Saved'),
            ('TW', "TWITTER", 'Twitter'),
            ('', "ROOT", ""),
        ]
        import_auto_gen_choices = ('AB', 'AD', 'CL', 'CV', 'FB', 'GM',
                                   'HF', 'IP', 'LI', 'NB', 'OU', 'TW')
        auto_gen_choices = import_auto_gen_choices + ("",)

    meta = JSONField(blank=True, default={})
    query = JSONField(blank=True, default={})
    shared_with = models.ManyToManyField('seat.Seat', blank=True,
                                         related_name="contact_sets",
                                         through=ContactSetsSeats)
    parents = models.ManyToManyField("contact_set.ContactSet", blank=True,
                                     related_name="contact_sets")
    filters = models.ManyToManyField('filtering.Filter', blank=True,
                                     related_name='+')
    source = models.CharField(choices=Sources.choices(),
                              default=Sources.SAVED,
                              max_length=2)
    # Some sources need to keep track of their specific uid. If we
    # allow users to edit the CS title, we need a way to match up the CS
    # with the import source. This should coincide with ImportRecord.uid
    source_uid = models.CharField(blank=True, max_length=256)
    deletable = models.BooleanField(default=True, blank=True)

    def __str__(self):
        source = self.Sources.translate(self.source)
        return "{} ({})".format(self.title, source if source else "root")

    @property
    def is_import_autogen(self):
        return self.source in self.Sources.import_auto_gen_choices

    @property
    def is_root(self):
        return self.source == self.Sources.ROOT

    @property
    def is_user_generated(self):
        return self.source == self.Sources.SAVED

    @classmethod
    @tracer.wrap()
    def _build_parent_query(cls, query_on, parents):
        parent_parts = [parent.build_query(query_on)
                        for parent in parents]
        if not all(parent_parts):
            # If any of the parent parts is empty (which resolves to boolean
            # False), then ignore all the other parents an emulate what the
            # empty parent represents: an all contact query.
            pquery = models.Q()
        else:
            pquery = reduce(lambda a, b: a | b, parent_parts)
        return pquery

    @tracer.wrap()
    def queryset(self, query_on=Result, query=None, inject_analysis=True,
                 **kwargs):
        if query is None:
            query = self.build_query(query_on=query_on)
        if query_on == Result:
            if not self.analysis_id:
                raise ContactSetException(
                    "Cannot view Results on a ContactSet with no Analysis.")
            if inject_analysis:
                # We don't want to blindly add a filter on Result rows for
                # analysis. This prevents the Postgres query planner from
                # using the composite index on ResultSort, and causes poor
                # performance.
                kwargs["analysis"] = self.analysis_id
        else:
            kwargs['user'] = self.user_id
            # If there is no "query" then there won't be any lookup on the
            # ContactIndex. If that's the case, we need to limit the search to
            # only the primary contacts. This is a typical scenario for the
            # Root ContactSet.
            if not query:
                kwargs["is_primary"] = True
        return query_on.objects.filter(query, **kwargs)

    @tracer.wrap()
    def get_contacts(self):
        """Get the contacts that belong to the ContactSet.

        If this is an auto-generated ContactSet for imports, return the actual
         imported Contacts; not the "merged" parents.
        """
        if self.is_import_autogen:
            # Get the import records matching the source_uid of the contactset.
            # ImportRecords are saved using the user who uploaded, whereas
            # ContactSets save the user who owns the contacts
            if self.is_account_owned:
                ir_kwargs = dict(account=self.account)
            else:
                ir_kwargs = dict(user=self.user, account=None)
            import_records = ImportRecord.objects.filter(uid=self.source_uid,
                                                         **ir_kwargs)
            # Query for all contacts matching those import records
            return Contact.objects.filter(import_record__in=import_records)
        else:
            return self.queryset(query_on=Contact)

    @tracer.wrap()
    def build_query(self, query_on=Result):
        qengine = QueryEngine(self.filters.all(), query_on)
        query = qengine.filter(
            self.query, analysis=self.analysis_id, user=self.user_id)

        parents = list(self.parents.all())
        if parents:
            pquery = self._build_parent_query(query_on, parents)
            query = pquery if not query else (query & pquery)
        return query

    @tracer.wrap()
    def _get_query_on(self):
        """This is a utility to help select which model to query on for
        counting. It's possible this could be useful elsewhere.
        """
        if self.is_import_autogen:
            return Contact
        else:
            return Result

    @tracer.wrap()
    def _update_count(self, query=None, partition_config_id=None,
                      query_on=None):
        """Internal worker method that does the actual count updating.

        :arg partition_config_id: A partition config ID to use to generate the
                                  count. Any partition should do.
        :arg query: A "Q" object that contains the filter query for this CS
        """
        query_args = {}
        if not query_on:
            query_on = self._get_query_on()

        if query_on is Result:
            if not self.analysis:
                return

            if not partition_config_id:
                partition_config_ids = self.get_partition_configs(self.analysis)
                partition_config_id = partition_config_ids[0] \
                    if partition_config_ids else None

            if not partition_config_id:
                return
            else:
                query_args["partition_id"] = partition_config_id

        self.count = self.queryset(query_on=query_on, query=query,
                                   **query_args).count()

    @tracer.wrap()
    def get_score_min_max(self, partition_id=None):
        if not self.analysis:
            return {}
        args = ('normalize',)
        if partition_id is not None:
            args += (str(partition_id),)
        return self.get_meta(args)

    @tracer.wrap()
    def _process_analysis_meta(self, partition_config_ids=None, query=None):
        """Updates the meta on analysis results"""
        if not self.analysis:
            return
        if partition_config_ids is None:
            partition_config_ids = self.get_partition_configs(self.analysis)

        if query is None:
            query = self.build_query()

        analysis_results = self.analysis.results.filter(query)
        self.set_meta('analysis', self.analysis_id)

        process_analysis_meta_signal.send(
            sender=self.__class__,
            contact_set=self,
            analysis_results=analysis_results,
            partition_config_ids=partition_config_ids)

    @tracer.wrap()
    def process_import_meta(self):
        process_import_meta_signal.send(
            sender=self.__class__, contact_set=self)

    @tracer.wrap()
    def set_meta(self, path, value):
        """
        Sets the relevant meta info from the meta field. The parameter `path`
        could either be a string or a list of strings.
        Example usages:
        set_meta('imported', datetime.now())
        set_meta(['normalize.17'], {'min_score':0, 'max_score':0})

        NOTE: An explicit save() is required in order to save the meta field.
        """
        if not isinstance(path, (list, tuple)):
            path = [path]
        self.meta = set_in(self.meta, path, value)

    def get_meta(self, path):
        """
        Gets the relevant meta info from the meta field. The parameter `path`
        could either be a string or a list of strings.
        Example usages:
        get_meta('imported')
        get_meta(['normalize.17'])
        """
        if not isinstance(path, (list, tuple)):
            path = [path]
        return get_in(self.meta, path)

    @classmethod
    @tracer.wrap()
    def get_root(cls, account, user, select_related=None):
        try:
            qs = ContactSet.objects
            if select_related:
                qs = qs.select_related(select_related)
            # Only one CS per user & account may be the root
            return qs.get(account=account, user=user, source=cls.Sources.ROOT)
        except ContactSet.DoesNotExist:
            return None
        except ContactSet.MultipleObjectsReturned:
            raise ContactSetException(
                'User: "{}" has multiple root ContactSets on account: {}'
                .format(user, account))

    @classmethod
    @tracer.wrap()
    def get_or_create_root(cls, account, user, analysis=None):
        # We reject old-style analyses
        if analysis and not analysis.partition_set_id:
            analysis = None

        root = cls.get_root(account, user)
        product_type = account.account_profile.product_type
        contact_set_title = translate("All Contacts", product_type)
        if not root:
            root = ContactSet(
                title=contact_set_title,
                account=account, user=user, analysis=analysis,
                source=cls.Sources.ROOT, deletable=False)
            root._update_count(query=models.Q())
            if analysis:
                # Most analyses will have meta results, so we can just copy
                # the results from there without calculation. If not,
                # we'll have to produce them
                if analysis.meta_results:
                    anal_meta = analysis.meta_results.copy()
                    anal_meta.pop("partitions", None)
                    anal_meta["analysis"] = analysis.id
                    root.meta = anal_meta
                else:
                    root._process_analysis_meta(query=models.Q())
            root.save()
        return root

    @classmethod
    @tracer.wrap()
    def create(cls, parents, title, user=None, account=None, description=None,
               query_args=None, filters=None, save=True, **kwargs):
        assert parents
        if not isinstance(parents, Iterable):
            parents = [parents]
        filters = filters or []
        users = {parent.user for parent in parents}
        accounts = {parent.account for parent in parents}
        assert len(users) == 1
        assert len(accounts) == 1
        user_ = users.pop()
        account_ = accounts.pop()
        analysis = parents[0].analysis
        # If this is an old-style analysis, we reject it. The user will just
        # have to run a new analysis.
        if analysis and not analysis.partition_set_id:
            analysis = None

        if user is None:
            user = user_
        else:
            assert user.pk == user_.pk

        if account is None:
            account = account_
        else:
            assert account.pk == account_.pk

        if query_args is None and 'query' in kwargs:
            query_args = kwargs.pop('query') or {}

        contact_set = ContactSet(
            title=title, description=description, account=account, user=user,
            query=query_args, **kwargs
        )
        # Only include the parent's analysis if the ContactSet was not auto
        # generated. If this CS is from an import, it will need a new analysis.
        if not contact_set.is_import_autogen:
            contact_set.analysis = analysis

        # This is a set of switches that we use below to keep things DRY.
        # Most of the time, we will use Result to get a count. If there
        # is no Analysis, then we can't use Result; however, if this is
        # an auto-generated CS, then we can use Contact to get the count.
        query_on = Result
        use_result = True
        use_contact = False
        if not contact_set.analysis:
            use_result = False
            if contact_set._get_query_on() is Contact:
                use_contact = True
                query_on = Contact

        # Get the filters being used for this CS
        if query_args:
            if not filters:
                filters = FilterSet(user, contact_set.analysis).get_all()
            qengine = QueryEngine(filters, query_on)
            # Get the query and filters that are used by this CS
            query, filters = qengine.process(query_args)
        else:
            filters = []
            query = models.Q()

        # Update the CS query with its parent's query
        pquery = cls._build_parent_query(query_on, parents)
        query = pquery if not query else (query & pquery)

        # Calculate the count and normalization, if given analysis.
        if use_result:
            partition_config_ids = list(
                cls.get_partition_configs(contact_set.analysis))
            partition_id = partition_config_ids[0]

            # We use the internal method because the external `update_count`
            # relies on the filters that must be added later in this function.
            contact_set._update_count(query, partition_id, query_on=Result)
            contact_set._process_analysis_meta(partition_config_ids,
                                                      query)
        elif use_contact:
            contact_set._update_count(query=query, query_on=Contact)

        if save:
            contact_set.save()

            if filters:
                contact_set.filters.set(filters)
            contact_set.parents.set(parents)
        return contact_set

    @classmethod
    @tracer.wrap()
    def get_or_create(cls, account, user, parents=None,
                      filter_kwargs=None, create_kwargs=None):
        from frontend.apps.seat.models import Seat

        filter_kwargs = filter_kwargs or {}
        create_kwargs = create_kwargs or {}
        try:
            cs = ContactSet.objects.get(
                user=user, account=account,
                **filter_kwargs)
            created = False
        except ContactSet.DoesNotExist:
            if not parents:
                try:
                    # If we can get an analysis for this user & account,
                    # we should use it
                    seat = Seat.objects.assigned().get(
                        user=user, account=account)
                    analysis = seat.get_default_analysis()
                except Seat.DoesNotExist:
                    analysis = None
                # If no parents are given, use the root.
                parents = ContactSet.get_or_create_root(
                    account=account, user=user,
                    analysis=analysis)
            cs = ContactSet.create(
                parents=parents, user=user, **create_kwargs)
            created = True
        return cs, created

    @classmethod
    @tracer.wrap()
    def bulk_update_analysis(cls, new_analysis):
        # Search for all ContactSets that have the same user as the analysis,
        # and are for the same account.
        if not new_analysis.partition_set_id:
            raise ValueError("Cannot update ContactSets with an old-style "
                             "analysis.")

        cs_to_update = cls.objects.active().filter(account=new_analysis.account,
                                                   user=new_analysis.user)
        # Optimization to get the partition ids only once
        partition_config_ids = list(cls.get_partition_configs(new_analysis))
        partition_id = partition_config_ids[0]

        for cs in cs_to_update.iterator():
            query = cs.build_query(query_on=Result)
            cs.analysis = new_analysis
            if not cs.is_import_autogen:
                cs._update_count(query, partition_id, query_on=Result)
            cs._process_analysis_meta(partition_config_ids, query)
            cs.save()


@receiver(models.signals.pre_save, sender=ContactSet)
def verify_deletable_field_value(sender, instance, **kwargs):
    """Verifies that the field `deletable` is not accidentally set to True on
    non-deletable ContactSets
    """
    non_deletable_sources = [ContactSet.Sources.ROOT,
                             ContactSet.Sources.HOUSEFILE]
    if instance.source in non_deletable_sources and instance.deletable:
        LOGGER.exception(f'ContactSet of type {instance.source} cannot be '
                         f'deletable')
        instance.deletable = False


class JoinedContactSet(ContactSetBase):
    contacts = None

    class Meta:
        abstract = True

    def queryset(self, query_on=Contact, **kwargs):
        query = self.build_query(query_on=query_on)
        return query_on.objects.filter(query, **kwargs)

    def build_query(self, query_on=Contact):
        if query_on == Contact:
            return models.Q(id__in=self.contacts.values("contact_id"))
        elif query_on == Result:
            if not self.analysis_id:
                raise ContactSetException(
                    "Cannot view Results on a ContactSet with no Analysis.")
            return models.Q(contact_id__in=self.contacts.values("contact_id"),
                            analysis=self.analysis_id)
        else:
            raise ValueError("Unrecognized query_on: {}".format(query_on))

    def add(self, *args):
        """Add one or more contact-representations to this contact set, and
          update the count.

        Note: a contact-representation might be a contact, or it might be
            a wrapper around a contact; that is subclass implementation
            specific.
        """
        with transaction.atomic():
            self.contacts.add(*args)
            self.count = self.contacts.count()
            self.save(update_fields=["count"])

    @classmethod
    def get_or_create_root(cls, account, user, analysis=None):
        raise NotImplementedError(
            "ContactSet class must be used for root contacts")

    @classmethod
    def create(cls, **kwargs):
        return cls.objects.create(**kwargs)

    @classmethod
    def get_or_create(cls, **kwargs):
        return cls.objects.get_or_create(**kwargs)

    @classmethod
    def bulk_update_analysis(cls, new_analysis):
        # Search for all ContactSets that have the same user as the analysis,
        # and are for the same account.
        if not new_analysis.partition_set_id:
            raise ValueError("Cannot update ContactSets with an old-style "
                             "analysis.")
        cls.objects.filter(account=new_analysis.account,
                           user=new_analysis.user).update(analysis=new_analysis)
