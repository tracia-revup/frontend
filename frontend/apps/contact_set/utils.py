
from frontend.libs.utils.model_utils import Choices


class ContactSetCategories(Choices):
    choices_map = [
        ("owned", "OWNED"),
        ("admin", "ADMIN"),
        ("shared", "SHARED"),
    ]
