from collections import OrderedDict

from celery import group
from django.conf import settings
from django.db import transaction

from frontend.apps.contact.models import Contact
from frontend.apps.contact.tasks import delete_contacts_task
from frontend.apps.contact.deletion import (
    DeleterBase, ContactDeleter, OwnerType)
from frontend.apps.contact_set.models import ContactSet, ContactSetsSeats
from frontend.libs.utils.general_utils import partition_into_batches


class SourceContactSetDeleter(DeleterBase):
    """Delete the contacts and the contactset that was generated from an
       import source.
    """
    deletion_plan = OrderedDict((
    ))
    dryrun_plan = tuple((
        ("Shares", ContactSetsSeats, "contact_set"),
    ))

    def __init__(self, update_progress=False):
        self.update_progress = update_progress

    def _get_contacts(self, contact_set):
        assert contact_set.is_import_autogen
        return contact_set.get_contacts()

    def dryrun(self, contact_set):
        data = OrderedDict((("Contact Sets", 1),))
        data.update(super(SourceContactSetDeleter, self).dryrun(contact_set))
        if contact_set.is_account_owned:
            owner_type = OwnerType.account
        else:
            owner_type = OwnerType.user

        child_contacts = self._get_contacts(contact_set)
        # Find all the contacts that are actually going to be deleted.
        parent_ids = set(child_contacts.exclude(parent=None).values_list(
                                                "parent_id", flat=True))
        child_ids = set(child_contacts.values_list("id", flat=True))
        # This query finds parents that won't have all their children deleted
        non_del_parents = Contact.objects.filter(parent_id__in=parent_ids,
                                                 contact_type__ne="merged") \
                                         .exclude(id__in=child_ids) \
                                         .values_list("parent_id", flat=True)
        # Subtract the surviving parents from all the parents. The remaining
        # parents are the ones that will be deleted.
        del_parents = parent_ids - set(non_del_parents)
        del_contacts = set(child_ids) | del_parents

        contact_deleter = ContactDeleter(
            contact_set.user,
            delete_children=False, clean_parents=True,
            update_progress=self.update_progress,
        )
        data.update(contact_deleter.dryrun(del_contacts,
                                           owner_type=owner_type))
        # Override the contact count so it is more intuitive for the user
        data["Contacts"] = contact_set.count
        return data

    def parallel_delete(self, contact_set):
        """Delete the ContactSet's contacts by splitting them into subtasks"""
        from frontend.apps.contact_set.tasks import (
            delete_contact_set_complete_task)

        assert contact_set.is_import_autogen
        contacts = self._get_contacts(contact_set).values_list('id', flat=True)

        if contacts.exists():
            batch_size = settings.CONTACT_SET_DELETION_BATCH_SIZE
            tasks = (group(
                delete_contacts_task.s(contact_set.user_id, list(batch),
                                       delete_children=False,
                                       clean_parents=True)
                    for batch in partition_into_batches(contacts, batch_size)) |
                delete_contact_set_complete_task.si(contact_set.id)
                ).apply_async(add_to_parent=True)
        else:
            tasks = delete_contact_set_complete_task.apply_async(
                args=(contact_set.id,),
                add_to_parent=True)
        return tasks

    @transaction.atomic
    def complete_parallel_delete(self, contact_set):
        """Do the ContactSet portion of the delete.
        The contacts should all be deleted by this point.
        """
        update_progress = self._construct_progress_updater(0, 100)

        # Delete the ContactSet
        if contact_set.is_account_owned:
            results = contact_set.delete(force=True)[1]
        # If the ContactSet is user-owned, we need to find every copy
        # of this Import across all of the user's seats
        else:
            results = ContactSet.objects.filter(
                user=contact_set.user,
                source=contact_set.source,
                source_uid=contact_set.source_uid).delete()[1]

        update_progress(100)
        return results
