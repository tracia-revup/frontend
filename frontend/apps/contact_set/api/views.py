from collections import OrderedDict
import logging

from django.core.exceptions import ValidationError as DjangoValidationError
from django.conf import settings
from django.db import transaction
from django.db.models import Max, Case, When
from django.shortcuts import get_object_or_404, Http404
from rest_condition.permissions import Or, And
from rest_framework import permissions, viewsets, status, exceptions
from rest_framework.decorators import action
from rest_framework.mixins import CreateModelMixin
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework_extensions.mixins import DetailSerializerMixin
from rest_framework_extensions.utils import compose_parent_pk_kwarg_name

from frontend.apps.authorize.api.permissions import UserIsRequester
from frontend.apps.authorize.models import RevUpTask
from frontend.apps.authorize.utils import create_revuptask_for_contact_source
from frontend.apps.campaign.api.permissions import SelfAccount
from frontend.apps.campaign.models import Account
from frontend.apps.contact.models import ImportRecord, LimitTracker
from frontend.apps.contact_set.deletion import SourceContactSetDeleter
from frontend.apps.contact_set.api.serializers import (
    ContactSetSerializer, UserContactSetSerializer, BasicContactSetSerializer,
    SourceContactSetSerializer)
from frontend.apps.contact_set.models import ContactSet, ContactSetsSeats
from frontend.apps.contact_set.tasks import (
    notify_seat_of_share, delete_contact_set_task, clone_contactset_task)
from frontend.apps.core.models import DeleteLog
from frontend.apps.role.api.permissions import HasPermission
from frontend.apps.role.permissions import AdminPermissions
from frontend.apps.seat.models import Seat
from frontend.libs.utils.api_utils import (
    HttpResponseLocked, InitialMixin, APIException, HttpResponseNoContent)
from frontend.libs.utils.string_utils import str_to_bool
from frontend.libs.utils.view_utils import build_protocol_host


LOGGER = logging.getLogger(__name__)


class BaseContactSetViewSet(CreateModelMixin, viewsets.ReadOnlyModelViewSet):
    serializer_class = ContactSetSerializer
    queryset = ContactSet.objects.filter(archived=None).annotate(
        last_shared=Max('contactsetsseats__shared_dt'))

    def populate_create_overrides(self, request):
        raise NotImplementedError

    def create(self, request, **kwargs):
        """Override some fields in the request data

        This is to ensure correctness and reduce the number of parameters the
        client needs to send to the API when creating a new ContactSet.
        """
        extra_data = self.populate_create_overrides(request)
        request.data.update(extra_data)
        # The source type will always be "SAVED"
        request.data['source'] = ContactSet.Sources.SAVED
        return super(BaseContactSetViewSet, self).create(request, **kwargs)

    @action(detail=True, methods=['post'])
    def remove_share(self, request, **kwargs):
        # TODO: create permission_classes to verify request.user.seat has SHARE
        # permission
        cs = self.get_object()
        seat_ids = request.data['seats']
        ContactSetsSeats.objects.filter(
            contact_set=cs, seat_id__in=seat_ids).delete()
        return Response({})

    @action(detail=True, methods=['post'])
    def add_share(self, request, **kwargs):
        # TODO: create permission_classes to verify request.user.seat has SHARE
        # permission
        cs = self.get_object()
        seat_ids = request.data['seats']
        shares = []
        for seat_id in seat_ids:
            css = ContactSetsSeats(contact_set=cs,
                                   seat_id=seat_id)
            try:
                # Run clean() to verify the account on the seat matches the
                # account on the ContactSet
                css.clean()
            except DjangoValidationError as err:
                # Translate to DRF's ValidationError class as DRF doesn't like
                # django's ValidationError for whatever reason.
                raise exceptions.ValidationError(err.message)

            shares.append(css)
        ContactSetsSeats.objects.bulk_create(shares)

        # We have to pass the protocol (http[s]) and host (revup.com) to the
        # celery task because only the view has access to that info.
        protocol_host = build_protocol_host(self.request)
        for seat_id in seat_ids:
            notify_seat_of_share.apply_async(
                args=(seat_id, cs.pk, request.user.pk, protocol_host))
        return Response({})

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.source == ContactSet.Sources.SAVED:
            instance.delete()
            return HttpResponseNoContent()
        else:
            return HttpResponseLocked(
                "Only ContactSets with source type of Saved can be deleted.")

    @action(detail=False, methods=['post'])
    def get_count(self, request, **kwargs):
        """
        data:
            {"query": {},
             "parents": [1,2,3]}
        """
        extra_data = self.populate_create_overrides(request)
        extra_data['title'] = 'stub title to make things work'
        request.data.update(extra_data)
        serializer = self.get_serializer(data=request.data)

        # This is handling a special case where the parents kwarg is None or
        # empty.  Cannot perform the subsequent validations so just return
        # zero for the count.
        if not request.data.get('parents'):
            result = {'count': 0}
            status_code = None
        else:
            serializer.is_valid(raise_exception=True)
            try:
                cs = ContactSet.create(save=False, **serializer.validated_data)
            except ValueError as err:
                if "unconfigured filter" in err.message:
                    # Most likely, user hit quick filter without analyzing
                    # TODO: Make quick filters dynamic, so this isn't an issue.
                    raise Http404
                else:
                    raise

            if cs.analysis:
                result = {'count': cs.count}
                status_code = None
            else:
                result = {'error': 'No analysis has been run yet'}
                status_code = status.HTTP_404_NOT_FOUND
        return Response(result, status=status_code)


class ContactSetViewSet(BaseContactSetViewSet):
    parent_account_lookup = compose_parent_pk_kwarg_name('account_id')
    permission_classes = (
        And(permissions.IsAuthenticated,
            Or(HasPermission(parent_account_lookup,
                             AdminPermissions.VIEW_CONTACTS,
                             methods=['GET']),
               HasPermission(parent_account_lookup,
                             AdminPermissions.MODIFY_CONTACTS,
                             methods=['POST', 'PUT', 'DELETE']))),)

    class pagination_class(PageNumberPagination):
        page_size = 20
        page_size_query_param = 'page_size'
        max_page_size = 100

    def get_queryset(self):
        account_id = self.kwargs.get(self.parent_account_lookup)
        account_user_id = Account.objects.values_list(
            'account_user_id', flat=True).get(pk=account_id)

        queryset = super(ContactSetViewSet, self).get_queryset().filter(
            account_id=account_id, user_id=account_user_id).order_by(
            Case(When(source=ContactSet.Sources.ROOT, then=0),
                 When(source=ContactSet.Sources.HOUSEFILE, then=1),
                 default=2,
                 ),
            'title'
           )

        return queryset

    def populate_create_overrides(self, request):
        account_id = self.kwargs.get(self.parent_account_lookup)
        # Get account user id.
        user_id = Account.objects.values_list(
            'account_user_id', flat=True).get(pk=account_id)
        return {'account': account_id,
                'user': user_id}


class SeatContactSetViewSet(DetailSerializerMixin, InitialMixin,
                            BaseContactSetViewSet):
    """This API gives the ContactSets available to a seat.

    Possible ContactSets are:
      1) The Seats own sets. Possibly auto-created
      2) The Account's sets, if the seat has permission
      3) Sets that are shared with the Seat.
    """
    parent_user_lookup = compose_parent_pk_kwarg_name('user_id')
    parent_seat_lookup = compose_parent_pk_kwarg_name('seat_id')
    permission_classes = (And(permissions.IsAuthenticated,
                              UserIsRequester),)
    serializer_class = BasicContactSetSerializer
    serializer_detail_class = UserContactSetSerializer
    queryset_detail = ContactSet.objects.standard().annotate(
        last_shared=Max('contactsetsseats__shared_dt')).select_related(
            "analysis__partition_set",
            "analysis__analysis_config").prefetch_related(
                "analysis__partition_set__partition_configs")

    def _pre_permission_initial(self):
        # Only queries that are allowed before permission checks
        seat_id = self.kwargs.get(self.parent_seat_lookup)
        try:
            seat_id = int(seat_id)
        except ValueError:
            raise Http404
        else:
            self.seat = get_object_or_404(
                Seat.objects.select_related("user", "account__account_user",
                                            "account__account_profile"),
                id=seat_id,
                user=self.kwargs.get(self.parent_user_lookup),
                state__in=Seat.States.ASSIGNED)

    def populate_create_overrides(self, request):
        account_id = self.seat.account_id
        user_id = request.user.id
        return {'account': account_id,
                'user': user_id}

    def create(self, request, **kwargs):
        # XXX: This action actually does already exist. We are doing this to
        # disable it for security reasons.
        raise exceptions.MethodNotAllowed(request.method)

    def destroy(self, request, **kwargs):
        # XXX: This action actually does already exist. We are doing this to
        # disable it for security reasons.
        raise exceptions.MethodNotAllowed(request.method)

    def add_share(self, *args, **kwargs):
        # XXX: This action actually does already exist. We are doing this to
        # disable it for security reasons.
        raise exceptions.NotFound

    def remove_share(self, *args, **kwargs):
        # XXX: This action actually does already exist. We are doing this to
        # disable it for security reasons.
        raise exceptions.NotFound

    def _get_contact_set_groups(self):
        user_owned, account_owned, shared = ContactSet.get_seat_contact_sets(
            self.seat,
            order_by=(Case(When(source=ContactSet.Sources.ROOT, then=0),
                           When(source=ContactSet.Sources.HOUSEFILE, then=1),
                      default=2,),
                      'title'))
        return (("user", user_owned), ("account", account_owned),
                ("shared", shared))

    def _get_serializer_data(self, data):
        return self.get_serializer(data, many=True).data

    def _build_group_result(self, group):
        key, data = group
        return self._get_serializer_data(data) if data else []

    def list(self, request, *args, **kwargs):
        """Return the list of available ContactSets.

        Format:
        {"user": [<user-owned contact sets],
         "account": [<account-owned contact sets],
         "shared": [<shared contact sets>]
        }
        Note: empty data sets will not be included
        """
        groups = self._get_contact_set_groups()

        # Build the results
        results = OrderedDict()
        for group in groups:
            results[group[0]] = self._build_group_result(group)
        return Response(results)

    def get_object(self):
        cs = super(SeatContactSetViewSet, self).get_object()
        if not cs.has_view_permission(self.request.user):
            raise Http404
        else:
            return cs


class SeatSourceContactSetViewSet(SeatContactSetViewSet):
    serializer_class = SourceContactSetSerializer
    queryset_detail = ContactSet.objects.active().annotate(
        last_shared=Max('contactsetsseats__shared_dt')).select_related(
        "analysis__partition_set",
        "analysis__analysis_config").prefetch_related(
        "analysis__partition_set__partition_configs")

    def _associate_import_records_with_cs(self, cs_group, **ir_kwargs):
        source_map = {cs.source_uid: cs for cs in cs_group}

        # Get the most recent ImportRecord for each import_type
        import_records = ImportRecord.objects.filter(
            uid__in=iter(source_map.keys()), **ir_kwargs).distinct(
            "import_type", "uid", "user", "account").order_by(
            # Order-by in postgres must include all distinct fields.
            # Only '-import_dt' actually matters here.
            "import_type", "uid", "user", "account", "-import_dt")

        for import_record in import_records:
            cs = source_map.get(import_record.uid)
            if cs:
                cs.import_record = import_record

    def _pop_root(self, cs_group):
        for i, cs in enumerate(cs_group):
            if cs.source == ContactSet.Sources.ROOT:
                return cs_group.pop(i)

    def _get_contact_set_groups(self):
        """Only return the contact sets that were generated from imports"""
        user_owned, account_owned, _ = ContactSet.get_seat_contact_sets(
            self.seat, augment_queryset=ContactSet.objects.filter(
                    source__in=ContactSet.Sources.auto_gen_choices),
            require_analysis=False,
            order_by=(Case(When(source=ContactSet.Sources.HOUSEFILE, then=0),
                      default=1,
                           ),
                      "title", "source", "created")
        )
        user_owned = list(user_owned)
        account_owned = list(account_owned)

        # If this is a regular fundraiser, check for clones
        if not account_owned and not self.seat.has_permissions(
                any=[Seat.AdminPermissions.VIEW_CONTACTS]):
            account_owned = list(
                ContactSet.objects.filter(account=self.seat.account,
                                          user=self.seat.account.account_user,
                                          source_uid=self.seat.user.username,
                                          source=ContactSet.Sources.CLONED))

        # Extract the root from the cs groups
        user_root = self._pop_root(user_owned)
        account_root = self._pop_root(account_owned)
        user_root_count = user_root.count if user_root else None
        account_root_count = account_root.count if account_root else None

        # Get the contact limits for each group
        user_analyze_limit = settings.PERSONAL_CONTACT_ANALYZE_LIMIT
        user_tracker = LimitTracker.get_tracker(
                            self.seat.user, self.seat.account)
        account_analyze_limit = self.seat.account.analyze_limit
        accont_tracker = LimitTracker.get_tracker(
                            self.seat.account.account_user, self.seat.account)

        # Find the related ImportRecord for each source contactset
        self._associate_import_records_with_cs(
            user_owned, user=self.seat.user, account=None)
        self._associate_import_records_with_cs(
            account_owned, account=self.seat.account)

        return (
            ("user", user_owned,
                dict(total_count=user_root_count,
                     total_analyzed=user_tracker.analyzed_count,
                     total_deleted=user_tracker.deleted_count,
                     analyze_limit=user_analyze_limit)
             ),
            ("account", account_owned,
                dict(total_count=account_root_count,
                     total_analyzed=accont_tracker.analyzed_count,
                     total_deleted=accont_tracker.deleted_count,
                     analyze_limit=account_analyze_limit)
                )
        )

    def _get_serializer_data(self, data):
        return self.get_serializer(data, many=True).data

    def _build_group_result(self, group):
        key, data, result = group
        result["results"] = self._get_serializer_data(data) if data else []
        return result

    def _create_revuptask(self, contact_set, task_type):
        try:
            return create_revuptask_for_contact_source(
                contact_set, task_type)
        except RuntimeError:
            raise APIException(LOGGER, "This task is already running")

    def get_object(self):
        cs = super(SeatSourceContactSetViewSet, self).get_object()
        if not cs.is_import_autogen:
            raise Http404
        else:
            return cs

    def destroy(self, request, **kwargs):
        dryrun = str_to_bool(request.query_params.get("dryrun", False))
        contact_set = self.get_object()

        # There is known bug where real_user is not getting populated for
        # JWT authentication. We need handle that case.
        try:
            user_is_admin = request.real_user.is_admin
        except AttributeError:
            user_is_admin = request.user.is_admin

        if not (contact_set.deletable or user_is_admin):
            return Response('This contact set cannot be deleted', status=403)

        if dryrun:
            cd = SourceContactSetDeleter()
            dryrun = cd.dryrun(contact_set)
            return Response(dryrun)
        else:
            with transaction.atomic():
                task = self._create_revuptask(
                    contact_set, RevUpTask.TaskType.DELETE_CONTACTS)
                DeleteLog.delete_contact_set(
                    contact_set,
                    request_user=request.impersonator if request.impersonator
                                                      else request.user)
                # Archive the ContactSet so it disappears from the app until
                # it's actually deleted
                contact_set.delete()

                # Delay the task so the transaction can commit
                delete_contact_set_task.apply_async(
                    args=(contact_set.id,),
                    task_id=task.task_id,
                    countdown=3
                )
            return HttpResponseNoContent()

    @action(detail=True, methods=['post'])
    def clone(self, request, **kwargs):
        contact_set = self.get_object()
        if contact_set.user != request.user:
            raise APIException(
                LOGGER, "Only Personal ContactSets may be cloned", request,
                "Request user: {}. ContactSet User: {}. ContactSet: {}".format(
                    request.user, contact_set.user, contact_set))
        with transaction.atomic():
            task = self._create_revuptask(
                contact_set, RevUpTask.TaskType.CLONE_CONTACTS)

            # Delay the task so the transaction can commit
            clone_contactset_task.apply_async(
                args=(contact_set.id,),
                task_id=task.task_id,
                countdown=3
            )
        return Response()


class ClientDataContactSetViewSet(InitialMixin, viewsets.ReadOnlyModelViewSet):
    serializer_class = BasicContactSetSerializer
    parent_account_lookup = compose_parent_pk_kwarg_name("account_id")
    permission_classes = (
        And(permissions.IsAuthenticated,
            SelfAccount(url_id_field=parent_account_lookup,
                        methods=('GET',))
            ),)

    def get_queryset(self):
        return ContactSet.objects.filter(account=self.account,
                                         source=ContactSet.Sources.HOUSEFILE)

    def _initial(self):
        self.account = get_object_or_404(
            Account, pk=self.kwargs.get(self.parent_account_lookup))
