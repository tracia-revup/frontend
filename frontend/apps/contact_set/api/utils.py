from django.http import Http404

from frontend.apps.contact_set.models import ContactSet


class QueryContactSetMixin(object):
    def _get_contact_set(self, contact_set_id, user, account,
                         select_related='analysis'):
        # This is a special hook that allows the requester to ask for the
        # 'root' contactset without having to lookup the ID first
        if contact_set_id == "root":
            contact_set = ContactSet.get_root(
                account, user, select_related=select_related)
        elif contact_set_id == "aroot":
            contact_set = ContactSet.get_root(
                account, account.account_user, select_related=select_related)
            if not contact_set.has_view_permission(user):
                raise Http404
        else:
            contact_set = ContactSet.objects.select_related(
                select_related).get(pk=contact_set_id)

        return contact_set
