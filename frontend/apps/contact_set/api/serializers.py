
from rest_framework import serializers

from frontend.apps.analysis.api.serializers import (
    ContactSetAnalysisSerializer, BasicContactSetAnalysisSerializer)
from frontend.apps.contact.api.serializers import ImportRecordSerializer
from frontend.apps.contact_set.models import ContactSet


class BaseContactSetSerializer(serializers.ModelSerializer):
    source = serializers.SerializerMethodField(read_only=True)
    raw_source = serializers.CharField(source='source', read_only=True)
    last_shared = serializers.ReadOnlyField()
    parents = serializers.PrimaryKeyRelatedField(
        queryset=ContactSet.objects.all(),
        many=True, style={'base_template': 'input.html'})
    can_delete = serializers.BooleanField(source='deletable', read_only=True)

    def get_source(self, cs):
        return ContactSet.Sources.translate(cs.source)


class ContactSetSerializer(BaseContactSetSerializer):
    class Meta:
        model = ContactSet
        fields = ('id', 'title', 'description', 'user', 'account',
                  'shared_with', 'parents', 'query', 'analysis', 'source',
                  "source_uid", "count", "created", "modified", 'last_shared',
                  'can_delete', 'raw_source')
        read_only_fields = ('analysis', "count", "created", 'last_shared')

    query = serializers.JSONField()
    last_shared = serializers.ReadOnlyField()
    can_delete = serializers.SerializerMethodField(read_only=True)

    def get_can_delete(self, contact_set):
        return contact_set.source == contact_set.Sources.SAVED

    def create(self, validated_data):
        ModelClass = self.Meta.model
        # Use the custom create method
        return ModelClass.create(**validated_data)

    def validate_parents(self, parents):
        """
        Verify that all parents have the same account and user
        """
        if not parents:
            raise serializers.ValidationError(
                "Parents is a required field and cannot be empty")

        parent_users = {parent.user_id for parent in parents}
        if len(parent_users) != 1:
            raise serializers.ValidationError(
                "All parent ContactSets must belong to the same user")
        parent_accounts = {parent.account_id for parent in parents}
        if len(parent_accounts) != 1:
            raise serializers.ValidationError(
                "All parent ContactSets must belong to the same account")
        return parents

    def validate(self, data):
        """
        Verify that parent user and account fields match the user and account
        fields on this child ContactSet
        """
        parent = data['parents'][0]
        if data['user'].id != parent.user_id:
            # Parents must have the same user_id as the ContactSet we are
            # creating. In the case of account ContactSets, this will be the
            # account user.
            # The user field set by the api framework.
            raise serializers.ValidationError(
                "Parents must have the same user as the new ContactSet")
        if data['account'].id != parent.account_id:
            # Parents must have the same account_id as the ContactSet we are
            # creating. In the case of account ContactSets, this will be the
            # account account.
            # The account field set by the api framework.
            raise serializers.ValidationError(
                "Parents must have the same account as the new ContactSet")
        return data


class BasicContactSetSerializer(BaseContactSetSerializer):
    analysis = BasicContactSetAnalysisSerializer(read_only=True)

    class Meta:
        model = ContactSet
        fields = ('id', 'title', 'description', "analysis", "count",
                  "source", "source_uid", "created", "modified", "can_delete")


class SourceContactSetSerializer(BasicContactSetSerializer):
    import_record = ImportRecordSerializer(read_only=True)
    can_delete = serializers.SerializerMethodField(read_only=True)

    class Meta(BasicContactSetSerializer.Meta):
        fields = BasicContactSetSerializer.Meta.fields + ('import_record',)

    def get_can_delete(self, contact_set):
        """The House File is a source that should always be deletable by Admin
        user."""
        request = self.context.get("request")
        if request:
            # There is known bug where real_user is not getting populated for
            # JWT authentication. We need handle that case.
            try:
                is_admin = request.real_user.is_admin
            except AttributeError:
                is_admin = request.user.is_admin
        else:
            is_admin = False

        return contact_set.deletable or is_admin


class UserContactSetSerializer(BasicContactSetSerializer):
    analysis = ContactSetAnalysisSerializer(read_only=True)
    permissions = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = ContactSet
        fields = ('id', 'title', 'description', "analysis", "source",
                  "permissions", "count", "created", "can_delete")

    def get_permissions(self, cs):
        if "user" in self.context:
            user = self.context["user"]
        elif "request" in self.context:
            user = self.context["request"].user
        else:
            return None
        return cs.user_permissions(user)[0]
