from io import StringIO

from django.core.exceptions import ObjectDoesNotExist
from django.core.management import call_command
from django.db.models import Q
from django.urls import reverse
from unittest.mock import patch, MagicMock

from frontend.apps.analysis.factories import (
    AnalysisConfigFactory, AnalysisFactory, ResultFactory)
from frontend.apps.authorize.factories import RevUpUserFactory
from frontend.apps.authorize.test_utils import (
    NoPermissionMixin, TestCase, AuthorizedUserTestCase)
from frontend.apps.call_time.factories import (
    CallTimeContactFactory, CallTimeContactSetFactory)
from frontend.apps.campaign.factories import AccountFactory
from frontend.apps.contact.tasks import delete_contacts_task
from frontend.apps.contact.factories import (
    ContactFactory, ImportConfigFactory, ImportRecordFactory)
from frontend.apps.contact.models import Contact
from frontend.apps.contact_set.deletion import SourceContactSetDeleter
from frontend.apps.contact_set.factories import ContactSetFactory
from frontend.apps.contact_set.models import ContactSet, ContactSetsSeats
from frontend.apps.contact_set.tasks import update_contactsets_count
from frontend.apps.filtering.factories import (
    ContactIndexFactory, ContactIndex, Filter)
from frontend.apps.seat.factories import SeatFactory
from frontend.apps.seat.models import Seat
from frontend.libs.utils.string_utils import ensure_unicode
from frontend.libs.utils.test_utils import APITestCase


class CleanUpFailedDeletesCommandTestCase(TestCase):
    def test_failed_deletes_command(self):
        """ Test clean up failed deletes management command."""
        out = StringIO()
        err = StringIO()
        account_id = 1
        args = []
        opts = {}

        # Test calling the command with an ID that does exist
        call_command('cleanup_failed_deletes', account_id, stdout=out, *args,
                     **opts)
        self.assertEquals(out.getvalue(), '')
        self.assertEquals(err.getvalue(), '')

        # Test calling the command with an account ID that doesn't exist
        account_id = 34887
        call_command('cleanup_failed_deletes', account_id, stdout=out, *args,
                     **opts)
        self.assertEquals(out.getvalue(), '')
        self.assertEquals(err.getvalue(), '')

        # Test calling the command with an account ID that is None
        with self.assertRaises(BaseException) as cm:
            account_id = None
            call_command('cleanup_failed_deletes', account_id, stdout=out,
                         *args,
                         **opts)
        self.assertEqual(cm.exception.args[0],
                         "Error: argument account: invalid int value: 'None'")

    def test_contact_set_delete(self):
        """ Test delete contact set. """
        contact_set = ContactSetFactory()
        contact_set.source = "FB"
        contact_set.save()
        contact_set.delete(force=True)

        # Check that once the task is run the contact_set.id is None and has
        # been deleted.
        self.assertFalse(ContactSet.objects.filter(id=contact_set.id).exists())


class ContactSetTests(TestCase):
    def setUp(self):
        meta = {
            'analysis': 78,
            'normalize': {'7': {'min_score': 0.0, 'max_score': 1371.7201},
                          '16': {'min_score': 0.0, 'max_score': 1129.076},
                          'latest': {'min_score': 0.0, 'max_score': 1129.076},
                          },
            'imported': '2019-06-13T13:02:23.280'}

        self.contact_set = ContactSetFactory(meta=meta)
        self.user = self.contact_set.user
        self.account = self.contact_set.account
        self.contacts = [ContactFactory(user=self.user) for i in range(5)]
        self.analysis = AnalysisFactory(account=self.account)
        self.contact_set.analysis = self.analysis
        self.contact_set.save()

    def test_meta_field(self):
        """Verifies various operations on the meta property of the contact_set
        model.
        """
        # Verify that nested info in meta field could be accessed
        assert 1371.7201 == self.contact_set.get_meta(
            ['normalize', '7', 'max_score'])
        # Verify that trying to access non-existent keys would return None
        assert self.contact_set.get_meta(['normalize', '13', 'max_score']) \
               is None

    def test_bulk_update_analysis(self):
        """Verify bulk_update_analysis updates the counts on ContactSets.
        Archived ContactSets should be ignored.
        """
        contact_set = ContactSetFactory(user=self.analysis.user,
                                        analysis=self.analysis,
                                        account=self.analysis.account)
        ## create results for the first set of contacts
        for contact in self.contacts:
            ResultFactory(user=self.user,
                          contact=contact,
                          analysis=self.analysis)

        # run the bulk update for these contacts and check count
        ContactSet.bulk_update_analysis(self.analysis)
        contact_set.refresh_from_db()

        assert contact_set.count == 5

        ## create a new set of contacts and a new analysis to check the count
        new_contacts = [ContactFactory(user=self.user) for i in range(3)]
        new_analysis = AnalysisFactory(account=self.account)
        contact_set = ContactSetFactory(user=new_analysis.user,
                                        analysis=new_analysis,
                                        account=new_analysis.account)

        # create results for each new contact
        for new_contact in new_contacts:
            ResultFactory(user=self.user,
                          contact=new_contact,
                          analysis=new_analysis)

        # run the bulk update for the new contacts and check the count again
        ContactSet.bulk_update_analysis(new_analysis)
        contact_set.refresh_from_db()

        assert contact_set.count == 3

        ## test that when a contact_set is already archived the count remains
        # unchanged
        contact_set.count = 333
        contact_set.save()
        contact_set.delete()
        assert contact_set is not None

        # run the bulk update for the new contacts and check the count is
        # unchanged
        ContactSet.bulk_update_analysis(new_analysis)
        contact_set.refresh_from_db()

        assert contact_set.count == 333

    def test_queryset(self):
        """Verify the queryset method"""
        results = [ResultFactory(contact=c, analysis=self.analysis)
                   for c in self.contacts]
        qs = self.contact_set.queryset()
        self.assertCountEqual(qs, results)

        qs = self.contact_set.queryset(query_on=Contact)
        self.assertCountEqual(qs, self.contacts)

        # Merge the contacts under a parent and requery
        parent = ContactFactory(user=self.user, contact_type="merged",
                                phone_numbers=[], email_addresses=[],
                                addresses=[])
        for contact in self.contacts:
            contact.parent = parent
            contact.is_primary = False
            contact.save()
        qs = self.contact_set.queryset(query_on=Contact)
        self.assertCountEqual(qs, [parent])
        # Verify we can override the query. This will get us non-primaries
        query = Q(is_primary=True) | Q(is_primary=False)
        qs = self.contact_set.queryset(query_on=Contact, query=query)
        self.assertCountEqual(qs, [parent]+self.contacts)


class ContactSetFixtureSetupMixin(object):
    @classmethod
    def _create_fixtues_for_user(cls, prefix, filters=None):
        """Method to create fixtures for unit testing ContactSets.

        Meant to be used within the setUpTestData class method.

        Expects the following class attributes to already be set:
            cls.<prefix>_user
            cls.account
            cls.analysis_config

        Creates the following fixtures for that user:
            5 male contacts with indexes split across 2 states (CA & NY)
            5 female contacts with indexes split across 2 states (CA & NY)
            an analysis fixture with Results (no index) for each contact
            a root ContactSet for the user
            a male-only contact set
            a female-only contact set
            a second female-only contact set for use in shares
        """
        user = getattr(cls, prefix + '_user')

        # Create contacts with indexes for user.
        contacts_male = [
            ci.contact for ci in ContactIndexFactory.create_batch(
                5, user=user, gender='M', state=True)]
        contacts_female = [
            ci.contact for ci in ContactIndexFactory.create_batch(
                5, user=user, gender='F', state=True)]
        contacts = (contacts_male + contacts_female)
        setattr(cls, prefix + '_contacts_male', contacts_male)
        setattr(cls, prefix + '_contacts_female', contacts_female)
        setattr(cls, prefix + '_contacts', contacts)

        # Create analysis and analysis results for user.
        analysis = AnalysisFactory(account=cls.account, user=user,
                                   analysis_config=cls.analysis_config,
                                   event=None)
        setattr(cls, prefix + '_analysis', analysis)
        setattr(cls, prefix + '_analysis_results',
                [ResultFactory(analysis=analysis, user=user,
                               contact=contact) for contact in contacts])

        # Create ContactSets (all, male, female) for each user
        cs_root = ContactSet.get_or_create_root(
            cls.account, user, analysis)
        cs_male = ContactSet.create(title="males",
                                    query_args={'gender': 'M'},
                                    parents=[cs_root],
                                    filters=filters)
        cs_female = ContactSet.create(title="females",
                                      query_args={'gender': 'F'},
                                      parents=[cs_root],
                                      filters=filters)
        cs_female_shared = ContactSet.create(title="females shared",
                                             query_args={'gender': 'F'},
                                             parents=[cs_root],
                                             filters=filters)
        setattr(cls, prefix + '_cs_root', cs_root)
        setattr(cls, prefix + '_cs_male', cs_male)
        setattr(cls, prefix + '_cs_female', cs_female)
        setattr(cls, prefix + '_cs_female_shared', cs_female_shared)


class ContactSetViewSetTests(ContactSetFixtureSetupMixin, NoPermissionMixin,
                             APITestCase):
    @classmethod
    def setUpTestData(cls):
        import_config = ImportConfigFactory()
        filters = list(import_config.filters.all())

        # Create users
        cls.admin_seat = SeatFactory(
            permissions=(Seat.AdminPermissions.all() +
                         Seat.FundraiserPermissions.all()),
            account__account_user__applied_filters=filters)
        cls.account = cls.admin_seat.account
        cls.account_user = cls.account.account_user
        cls.admin_user = cls.admin_seat.user
        # The view_admin user only has the VIEW_CONTACTS permission.
        cls.view_admin_seat = SeatFactory(
            account=cls.account,
            permissions=([Seat.AdminPermissions.VIEW_CONTACTS] +
                         Seat.FundraiserPermissions.all()),
            user__import_config=import_config)
        cls.view_admin_user = cls.view_admin_seat.user
        # Create an admin seat on another campaign for fundraiser user in order
        # to verify that admin permissions on one account don't translate into
        # effective admin permissions for contact sets on another account.
        cls.view_admin_seat2 = SeatFactory(
            permissions=(Seat.AdminPermissions.all() +
                         Seat.FundraiserPermissions.all()),
            user=cls.view_admin_user)

        cls.analysis_config = AnalysisConfigFactory(account=cls.account)

        cls._create_fixtues_for_user('account', filters)
        cls._create_fixtues_for_user('admin', filters)
        cls._create_fixtues_for_user('view_admin', filters)

        # Share view_admin CS with admin user
        ContactSetsSeats.objects.create(
            contact_set=cls.view_admin_cs_female_shared,
            seat=cls.admin_seat)
        # Share admin CS with view_admin user
        ContactSetsSeats.objects.create(
            contact_set=cls.admin_cs_female_shared,
            seat=cls.view_admin_seat)
        # Share account CS with view_admin user
        ContactSetsSeats.objects.create(
            contact_set=cls.account_cs_female_shared,
            seat=cls.view_admin_seat)

    def setUp(self):
        # Refresh the values on the seat object from the database before every
        # test so we can manipulate permissions in a test, without worrying
        # about messing up another test that requires the original permissions
        # on the seat.
        self.admin_seat.refresh_from_db()
        self.view_admin_seat.refresh_from_db()

    def test_share_permissions(self):
        fundraiser_seat = SeatFactory(account=self.account)
        url = reverse('contact_set_api-add-share',
                      args=(self.account.id, self.account_cs_female.id))
        self.client.force_authenticate(user=self.view_admin_user)

        # Control tests
        self.assertNotIn(Seat.AdminPermissions.MODIFY_CONTACTS,
                         self.view_admin_seat.permissions)
        self.assertFalse(self.account_cs_female.shared_with.filter(
            pk=fundraiser_seat.id).exists())

        response = self.client.post(url, {'seats': [fundraiser_seat.id]},
                                    format='json')
        self.assertNoPermission(response)
        self.assertFalse(self.account_cs_female.shared_with.filter(
            pk=fundraiser_seat.id).exists())

        self.client.force_authenticate(user=self.admin_user)
        self.assertIn(Seat.AdminPermissions.MODIFY_CONTACTS,
                      self.admin_seat.permissions)
        response = self.client.post(url, {'seats': [fundraiser_seat.id]},
                                    format='json')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.account_cs_female.shared_with.filter(
            pk=fundraiser_seat.id).exists())

        # Now test removal
        url = reverse('contact_set_api-remove-share',
                      args=(self.account.id, self.account_cs_female.id))

        self.client.force_authenticate(user=self.view_admin_user)
        response = self.client.post(url, {'seats': [fundraiser_seat.id]},
                                    format='json')
        self.assertNoPermission(response)
        self.assertTrue(self.account_cs_female.shared_with.filter(
            pk=fundraiser_seat.id).exists())

        self.client.force_authenticate(user=self.admin_user)
        response = self.client.post(url, {'seats': [fundraiser_seat.id]},
                                    format='json')
        self.assertEqual(response.status_code, 200)
        self.assertFalse(self.account_cs_female.shared_with.filter(
            pk=fundraiser_seat.id).exists())

    def test_delete_permissions(self):
        # users without the MODIFY_CONTACTS permission are not allowed to
        # create account contact sets.
        self.client.force_authenticate(user=self.view_admin_user)
        self.assertNotIn(Seat.AdminPermissions.MODIFY_CONTACTS,
                         self.view_admin_seat.permissions)
        url = reverse('contact_set_api-detail',
                      args=(self.account.id, self.account_cs_female.id))
        response = self.client.delete(url)
        self.assertNoPermission(response)
        self.assertTrue(ContactSet.objects.filter(
            pk=self.account_cs_female.id, archived=None).exists())

        # Verify the share permission view does not confer the ability to
        # delete
        url = reverse('contact_set_api-detail',
                      args=(self.account.id, self.account_cs_female_shared.id))
        # Control test to verify we do have the ability to view it
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response = self.client.delete(url)
        self.assertNoPermission(response)
        self.assertTrue(ContactSet.objects.filter(
            pk=self.account_cs_female_shared.id, archived=None).exists())

        # Users with the admin permission are allowed to create account contact
        # sets.
        self.client.force_authenticate(user=self.admin_user)
        self.assertIn(Seat.AdminPermissions.MODIFY_CONTACTS,
                      self.admin_seat.permissions)
        url = reverse('contact_set_api-detail',
                      args=(self.account.id, self.account_cs_female.id))
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 204)
        self.assertFalse(ContactSet.objects.filter(
            pk=self.account_cs_female.id, archived=None).exists())

    def test_create_permissions(self):
        url = reverse('contact_set_api-list', args=(self.account.id,))

        def _get_response():
            return self.client.post(url,
                                    {'title': 'my contact set',
                                     'parents': [self.account_cs_root.id],
                                     'query': {'state': ['CA']}},
                                    format='json')

        # users without the MODIFY_CONTACTS permission are not allowed to
        # create account contact sets.
        self.client.force_authenticate(user=self.view_admin_user)
        self.assertNotIn(Seat.AdminPermissions.MODIFY_CONTACTS,
                         self.view_admin_seat.permissions)
        response = _get_response()
        self.assertNoPermission(response)

        # Users with the admin permission are allowed to create account contact
        # sets.
        self.client.force_authenticate(user=self.admin_user)
        self.assertIn(Seat.AdminPermissions.MODIFY_CONTACTS,
                      self.admin_seat.permissions)
        response = _get_response()
        self.assertEqual(response.status_code, 201)

    def test_get_count_no_parents(self):
        """Tests that if no parents are provided in the request the count
        returned is 0."""
        url = reverse('contact_set_api-get-count',
                      args=(self.account.id,))

        self.client.force_authenticate(user=self.admin_user)
        response = self.client.post(url, {'parents': [],
                                          'query': {}},
                                    format='json')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, {'count': 0})

    def test_get_count_permissions(self):
        url = reverse('contact_set_api-get-count',
                      args=(self.account.id,))

        def _get_response():
            return self.client.post(url,
                                    {'parents': [self.account_cs_root.id],
                                     'query': {'state': ['CA']}},
                                    format='json')

        self.client.force_authenticate(user=self.view_admin_user)
        self.assertNotIn(Seat.AdminPermissions.MODIFY_CONTACTS,
                         self.view_admin_seat.permissions)
        self.assertIn(Seat.AdminPermissions.VIEW_CONTACTS,
                      self.view_admin_seat.permissions)

        # XXX: This test is currently commented out because we do not have a
        # good way to set different permissions on a specific endpoint.
        #response = _get_response()
        #self.assertEqual(response.status_code, 200)

        # Modify view_admin to have the same permissions as a fundraiser so we
        # can check that too.
        self.view_admin_seat.permissions = Seat.FundraiserPermissions.all()
        self.view_admin_seat.save()
        self.assertNotIn(Seat.AdminPermissions.VIEW_CONTACTS,
                         self.view_admin_seat.permissions)
        response = _get_response()
        self.assertNoPermission(response)

        # Users with the admin permission are allowed to create account contact
        # sets.
        self.client.force_authenticate(user=self.admin_user)
        self.assertIn(Seat.AdminPermissions.MODIFY_CONTACTS,
                      self.admin_seat.permissions)
        self.assertIn(Seat.AdminPermissions.VIEW_CONTACTS,
                      self.admin_seat.permissions)
        response = _get_response()
        self.assertEqual(response.status_code, 200)

    def test_list_permissions(self):
        url = reverse('contact_set_api-list', args=(self.account.id,))

        self.client.force_authenticate(user=self.view_admin_user)
        self.assertNotIn(Seat.AdminPermissions.MODIFY_CONTACTS,
                         self.view_admin_seat.permissions)
        self.assertIn(Seat.AdminPermissions.VIEW_CONTACTS,
                      self.view_admin_seat.permissions)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        # Modify view_admin to have the same permissions as a fundraiser so we
        # can verify fundraisers do not have access (even if an account CS has
        # been shared with them.
        self.view_admin_seat.permissions = Seat.FundraiserPermissions.all()
        self.view_admin_seat.save()
        self.assertNotIn(Seat.AdminPermissions.VIEW_CONTACTS,
                         self.view_admin_seat.permissions)
        response = self.client.get(url)
        self.assertNoPermission(response)

        self.client.force_authenticate(user=self.admin_user)
        self.assertIn(Seat.AdminPermissions.MODIFY_CONTACTS,
                      self.admin_seat.permissions)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_detail_view(self):
        url = reverse('contact_set_api-detail',
                      args=(self.account.id, self.account_cs_female_shared.id))

        self.client.force_authenticate(user=self.view_admin_user)
        self.assertNotIn(Seat.AdminPermissions.MODIFY_CONTACTS,
                         self.view_admin_seat.permissions)
        self.assertIn(Seat.AdminPermissions.VIEW_CONTACTS,
                      self.view_admin_seat.permissions)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        # Modify view_admin to have the same permissions as a fundraiser so we
        # can verify fundraisers do not have access even if an account CS has
        # been shared with them.
        self.view_admin_seat.permissions = Seat.FundraiserPermissions.all()
        self.view_admin_seat.save()
        self.assertNotIn(Seat.AdminPermissions.VIEW_CONTACTS,
                         self.view_admin_seat.permissions)
        response = self.client.get(url)
        self.assertNoPermission(response)

        self.client.force_authenticate(user=self.admin_user)
        self.assertIn(Seat.AdminPermissions.MODIFY_CONTACTS,
                      self.admin_seat.permissions)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_share_fails_on_user_cs(self):
        url = reverse('contact_set_api-add-share',
                      args=(self.account.id, self.admin_cs_female.id))
        share_with_seat_id = self.view_admin_seat.id
        self.client.force_authenticate(user=self.admin_user)
        # Control test. Make sure that admin_cs_female contact set does not
        # already have a share to view_admin_user
        self.assertFalse(self.admin_cs_female.shared_with.filter(
            pk=share_with_seat_id).exists())

        response = self.client.post(url, {'seats': [share_with_seat_id]},
                                    format='json')
        # Adding a share to a user-owned CS via the account CS API should fail
        # as it shouldn't even be able to find the CS.
        self.assertEqual(response.status_code, 404)
        # Verify the api response isn't lying, and no share was actually made.
        self.assertFalse(self.admin_cs_female.shared_with.filter(
            pk=share_with_seat_id).exists())

    def test_shared_seat_acct_must_match_cs_seat(self):
        url = reverse('contact_set_api-add-share',
                      args=(self.account.id, self.account_cs_female.id))
        share_with_seat_id = self.view_admin_seat2.id
        self.client.force_authenticate(user=self.admin_user)

        self.assertFalse(self.account_cs_female.shared_with.filter(
            pk=share_with_seat_id).exists())
        response = self.client.post(url, {'seats': [share_with_seat_id]},
                                    format='json')
        # Adding a share on a contact set to a seat that is on a different
        # account should result in a validation error.
        self.assertEqual(response.status_code, 400)
        # Verify the api response isn't lying, and no share was actually made.
        self.assertFalse(self.account_cs_female.shared_with.filter(
            pk=share_with_seat_id).exists())

    def test_delete_fails_on_user_cs(self):
        # The only contact sets that should be deletable via the account CS API
        # are account contact sets
        owned_url = reverse('contact_set_api-detail',
                            args=(self.account.id, self.admin_cs_female.id))
        shared_url = reverse('contact_set_api-detail',
                             args=(self.account.id,
                                   self.view_admin_cs_female_shared.id))

        self.client.force_authenticate(user=self.admin_user)

        # Verify we cannot delete contact sets that have been shared with us.
        response = self.client.delete(shared_url)
        self.assertEqual(response.status_code, 404)

        # Verify we cannot delete contact sets even if we own them
        response = self.client.delete(owned_url)
        self.assertEqual(response.status_code, 404)


class SeatContactSetViewSetTests(ContactSetFixtureSetupMixin,
                                 NoPermissionMixin, APITestCase):
    @classmethod
    def setUpTestData(cls):
        import_config = ImportConfigFactory()
        filters = list(import_config.filters.all())

        # Create users
        cls.admin_seat = SeatFactory(
            permissions=(Seat.AdminPermissions.all() +
                         Seat.FundraiserPermissions.all()),
            account__account_user__applied_filters=filters)
        cls.account = cls.admin_seat.account
        cls.account_user = cls.account.account_user
        cls.admin_user = cls.admin_seat.user
        # The fundraiser user only has the VIEW_CONTACTS permission.
        cls.fundraiser_seat = SeatFactory(
            account=cls.account,
            user__import_config=import_config)
        cls.fundraiser_user = cls.fundraiser_seat.user
        # Create an admin seat on another campaign for fundraiser user in order
        # to verify that admin permissions on one account don't translate into
        # effective admin permissions for contact sets on another account.
        cls.fundraiser_seat2 = SeatFactory(
            permissions=(Seat.AdminPermissions.all() +
                         Seat.FundraiserPermissions.all()),
            user=cls.fundraiser_user)

        cls.analysis_config = AnalysisConfigFactory(account=cls.account)

        cls._create_fixtues_for_user('account', filters)
        cls._create_fixtues_for_user('admin', filters)
        cls._create_fixtues_for_user('fundraiser', filters)

        # Share fundraiser CS with admin user
        ContactSetsSeats.objects.create(
            contact_set=cls.fundraiser_cs_female_shared,
            seat=cls.admin_seat)
        # Share admin CS with fundraiser user
        ContactSetsSeats.objects.create(
            contact_set=cls.admin_cs_female_shared,
            seat=cls.fundraiser_seat)
        # Share account CS with fundraiser user
        ContactSetsSeats.objects.create(
            contact_set=cls.account_cs_female_shared,
            seat=cls.fundraiser_seat)

        # Add clone contactsets to verify their visibility
        # Create cloned sources for 2 different users
        cls.cloned_contactset = ContactSetFactory.create(
            user=cls.account_user,
            account=cls.account,
            source=ContactSet.Sources.CLONED,
            source_uid=cls.fundraiser_user.username,
            analysis=cls.account_cs_female.analysis)
        cls.cloned_contactset_other = ContactSetFactory.create(
            user=cls.account_user,
            account=cls.account,
            source=ContactSet.Sources.CLONED,
            source_uid="testusername-shouldfail",
            analysis=cls.account_cs_female.analysis)

    def test_create_not_allowed(self):
        url = reverse('seat_contact_set_api-list',
                      args=(self.fundraiser_user.id,
                            self.fundraiser_seat.id))

        def _get_response():
            return self.client.post(url,
                                    {'title': 'my contact set',
                                     'parents': [self.fundraiser_cs_female.id],
                                     'query': {'state': ['CA']}},
                                    format='json')

        self.client.force_authenticate(self.fundraiser_user)
        response = _get_response()
        self.assertEqual(response.status_code, 405)

    def test_delete_not_allowed(self):
        url = reverse('seat_contact_set_api-detail',
                      args=(self.fundraiser_user.id, self.fundraiser_seat.id,
                            self.fundraiser_cs_female.id))

        self.client.force_authenticate(self.fundraiser_user)
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 405)

    def test_revoked_seat(self):
        # Ensure contact sets shared with revoked seats cannot be viewed.
        fundraiser_seat_revoked = SeatFactory(
            account=self.account,
            user=self.fundraiser_user,
            state=Seat.States.REVOKED)
        ContactSetsSeats.objects.create(
            contact_set=self.account_cs_female,
            seat=fundraiser_seat_revoked)

        self.client.force_authenticate(self.fundraiser_user)

        # Using an active seat, try to view a contact set that was shared with
        # a revoked seat that belongs to the same user.
        url = reverse('seat_contact_set_api-detail',
                      args=(self.fundraiser_user.id, self.fundraiser_seat.id,
                            self.account_cs_female.id))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

        # Using the revoked seat, try to view a contact set that was shared
        # with it.
        url = reverse('seat_contact_set_api-detail',
                      args=(self.fundraiser_user.id,
                            fundraiser_seat_revoked.id,
                            self.account_cs_female.id))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

        # Using the revoked seat, try to view any contact sets.
        url = reverse('seat_contact_set_api-list',
                      args=(self.fundraiser_user.id,
                            fundraiser_seat_revoked.id))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_fundraiser_get(self):
        self.client.force_authenticate(user=self.fundraiser_user)
        url = reverse('seat_contact_set_api-list',
                      args=(self.fundraiser_user.id,
                            self.fundraiser_seat.id))

        results = self.client.get(url)
        self.assertContains(results, '"id":{}'.format(
            self.fundraiser_cs_female_shared.id))
        self.assertContains(results, '"id":{}'.format(
            self.account_cs_female_shared.id))
        self.assertContains(results, '"id":{}'.format(
            self.admin_cs_female_shared.id))
        self.assertNotContains(results, '"id":{}'.format(
            self.account_cs_female.id))

        # # Verify the clone buckets are invisible for fundraiser
        self.assertNotContains(results, '"id":{}'.format(
            self.cloned_contactset.id))
        self.assertNotContains(results, '"id":{}'.format(
            self.cloned_contactset_other.id))

    def test_admin_get(self):
        self.client.force_authenticate(user=self.admin_user)
        url = reverse('seat_contact_set_api-list',
                      args=(self.admin_user.id, self.admin_seat.id))
        results = self.client.get(url)
        self.assertContains(results, '"id":{}'.format(
            self.fundraiser_cs_female_shared.id))
        self.assertContains(results, '"id":{}'.format(
            self.account_cs_female_shared.id))
        self.assertContains(results, '"id":{}'.format(
            self.admin_cs_female_shared.id))
        self.assertContains(results, '"id":{}'.format(
            self.account_cs_female.id))
        self.assertContains(results, '"id":{}'.format(
            self.account_cs_male.id))

        # Verify the correct clone buckets are visible
        self.assertContains(results, '"id":{}'.format(
            self.cloned_contactset.id))
        self.assertContains(results, '"id":{}'.format(
            self.cloned_contactset_other.id))


class SeatSourceContactSetViewSetTests(AuthorizedUserTestCase,
                                       ContactSetFixtureSetupMixin,
                                       NoPermissionMixin, APITestCase):
    @classmethod
    def setUpTestData(cls):
        import_config = ImportConfigFactory()
        filters = list(import_config.filters.all())

        # Create users
        cls.admin_seat = SeatFactory(
            permissions=(Seat.AdminPermissions.all() +
                         Seat.FundraiserPermissions.all()),
            account__account_user__applied_filters=filters)
        cls.account = cls.admin_seat.account
        cls.account_user = cls.account.account_user
        cls.admin_user = cls.admin_seat.user
        # The fundraiser user only has the VIEW_CONTACTS permission.
        cls.fundraiser_seat = SeatFactory(
            account=cls.account,
            user__import_config=import_config)
        cls.fundraiser_user = cls.fundraiser_seat.user
        # Create an admin seat on another campaign for fundraiser user in order
        # to verify that admin permissions on one account don't translate into
        # effective admin permissions for contact sets on another account.
        cls.fundraiser_seat2 = SeatFactory(
            permissions=(Seat.AdminPermissions.all() +
                         Seat.FundraiserPermissions.all()),
            user=cls.fundraiser_user)

        cls.analysis_config = AnalysisConfigFactory(account=cls.account)

        cls._create_fixtues_for_user('account', filters)
        cls._create_fixtues_for_user('admin', filters)
        cls._create_fixtues_for_user('fundraiser', filters)

        # Share fundraiser CS with admin user
        ContactSetsSeats.objects.create(
            contact_set=cls.fundraiser_cs_female_shared,
            seat=cls.admin_seat)
        # Share admin CS with fundraiser user
        ContactSetsSeats.objects.create(
            contact_set=cls.admin_cs_female_shared,
            seat=cls.fundraiser_seat)
        # Share account CS with fundraiser user
        ContactSetsSeats.objects.create(
            contact_set=cls.account_cs_female_shared,
            seat=cls.fundraiser_seat)

        # Create cloned sources for 2 different users
        cls.cloned_contactset = ContactSetFactory.create(
            user=cls.account_user,
            account=cls.account,
            source=ContactSet.Sources.CLONED,
            source_uid=cls.fundraiser_user.username,
            analysis=cls.account_cs_female.analysis)
        cls.cloned_contactset_other = ContactSetFactory.create(
            user=cls.account_user,
            account=cls.account,
            source=ContactSet.Sources.CLONED,
            source_uid="testusername-shouldfail",
            analysis=cls.account_cs_female.analysis)

        # Add source ContactSets for testing

        # User CS
        cls.gmail_cs_1 = ContactSetFactory(user=cls.fundraiser_user,
                                           account=cls.account,
                                           source="GM",
                                           source_uid="revup@revup.com",
                                           query={"import_source_detail":
                                                      ["revup@revup.com"]})
        # Admin CS
        cls.gmail_cs_2 = ContactSetFactory(user=cls.admin_user,
                                           account=cls.account,
                                           source="GM",
                                           source_uid="revup@revup.com",
                                           query={"import_source_detail":
                                                      ["revup@revup.com"]})
        # Account CS
        cls.gmail_cs_3 = ContactSetFactory(user=cls.account_user,
                                           account=cls.account,
                                           source="GM",
                                           source_uid="revup@revup.com",
                                           query={"import_source_detail":
                                                      ["revup@revup.com"]})
        # Account CS
        cls.csv_cs_1 = ContactSetFactory(user=cls.account_user,
                                         account=cls.account,
                                         source="CV",
                                         source_uid="top_100_contacts.csv",
                                         query={"import_source_detail":
                                                    ["top_100_contacts.csv"]})
        # Account HF
        cls.housefile_cs_1 = ContactSetFactory(user=cls.account_user,
                                               account=cls.account,
                                               source="HF",
                                               source_uid="house_entities",
                                               query={"import_source_detail":
                                                          ["house_entities"]})

    def test_seat_source_contact_set_ordering(self):
        """Test that the ordering of the created contact sets is correct."""
        self.client.force_authenticate(user=self.admin_user)
        url = reverse('seat_source_contact_set_api-list',
                      args=(self.admin_user.id,
                            self.admin_seat.id))

        results = self.client.get(url)
        source_results = [r['source'] for r in results.data.get('account').get('results')]
        self.assertEqual(source_results[0], 'HouseFile')

    def test_seat_contact_set_ordering(self):
        """Test that the ordering of the created contact sets is correct."""
        self.client.force_authenticate(user=self.admin_user)
        url = reverse('seat_contact_set_api-list',
                      args=(self.admin_user.id,
                            self.admin_seat.id))

        results = self.client.get(url)
        source_results = [r['source'] for r in results.data.get('account')]
        self.assertEqual(source_results[0], '')

    def test_fundraiser_get(self):
        self.client.force_authenticate(user=self.fundraiser_user)
        url = reverse('seat_source_contact_set_api-list',
                      args=(self.fundraiser_user.id,
                            self.fundraiser_seat.id))

        results = self.client.get(url)

        self.assertContains(results, '"id":{}'.format(self.gmail_cs_1.id))
        self.assertNotContains(results, '"id":{}'.format(self.gmail_cs_2.id))
        self.assertNotContains(results, '"id":{}'.format(self.gmail_cs_3.id))

        # No non-source CSs in this API
        self.assertNotContains(results, '"id":{}'.format(
            self.fundraiser_cs_female.id))
        self.assertNotContains(results, '"id":{}'.format(
            self.fundraiser_cs_male.id))
        self.assertNotContains(results, '"id":{}'.format(
            self.account_cs_female.id))
        self.assertNotContains(results, '"id":{}'.format(
            self.account_cs_male.id))

        # No shared CSs in this API
        self.assertNotContains(results, '"id":{}'.format(
            self.fundraiser_cs_female_shared.id))
        self.assertNotContains(results, '"id":{}'.format(
            self.account_cs_female_shared.id))
        self.assertNotContains(results, '"id":{}'.format(
            self.admin_cs_female_shared.id))

        # Verify the correct clone buckets are visible
        self.assertContains(results, '"id":{}'.format(
            self.cloned_contactset.id))
        self.assertNotContains(results, '"id":{}'.format(
            self.cloned_contactset_other.id))

    def test_admin_get(self):
        self.client.force_authenticate(user=self.admin_user)
        url = reverse('seat_source_contact_set_api-list',
                      args=(self.admin_user.id, self.admin_seat.id))
        results = self.client.get(url)

        self.assertNotContains(results, '"id":{}'.format(self.gmail_cs_1.id))
        self.assertContains(results, '"id":{}'.format(self.gmail_cs_2.id))
        self.assertContains(results, '"id":{}'.format(self.gmail_cs_3.id))

        # No non-source CSs in this API
        self.assertNotContains(results, '"id":{}'.format(
            self.fundraiser_cs_female.id))
        self.assertNotContains(results, '"id":{}'.format(
            self.fundraiser_cs_male.id))
        self.assertNotContains(results, '"id":{}'.format(
            self.account_cs_female.id))
        self.assertNotContains(results, '"id":{}'.format(
            self.account_cs_male.id))

        # No shared CSs in this API
        self.assertNotContains(results, '"id":{}'.format(
            self.fundraiser_cs_female_shared.id))
        self.assertNotContains(results, '"id":{}'.format(
            self.account_cs_female_shared.id))
        self.assertNotContains(results, '"id":{}'.format(
            self.admin_cs_female_shared.id))

        # Verify the correct clone buckets are visible
        self.assertContains(results, '"id":{}'.format(
            self.cloned_contactset.id))
        self.assertContains(results, '"id":{}'.format(
            self.cloned_contactset_other.id))

    def test_contact_set_deletion_1(self):
        """Tests that a non-deletable contact set cannot be deleted"""
        self.client.force_authenticate(user=self.admin_user)
        url = reverse('seat_source_contact_set_api-detail', args=(
            self.admin_user.id, self.admin_seat.id, self.housefile_cs_1.id))
        # Verify that housefile contact set is not deletable
        assert self.housefile_cs_1.deletable is False

        # Verify that housefile contact set cannot be deleted
        response = self.client.delete(url)
        assert response.status_code == 403
        assert 'This contact set cannot be deleted' in ensure_unicode(
            response.content)

    @patch('frontend.apps.contact_set.api.views.delete_contact_set_task',
           return_value=MagicMock())
    def test_contact_set_deletion_2(self, mock_del_cs_task):
        """Tests that a deletable contact set can be deleted"""
        self.client.force_authenticate(user=self.admin_user)
        url = reverse('seat_source_contact_set_api-detail', args=(
            self.admin_user.id, self.admin_seat.id, self.csv_cs_1.id))
        # Verify that csv contact set is deletable
        assert self.csv_cs_1.deletable is True

        # Verify that csv contact set can be deleted
        response = self.client.delete(url)
        assert response.status_code == 204

    @patch('frontend.apps.contact_set.api.views.delete_contact_set_task',
           return_value=MagicMock())
    def test_admin_can_delete_hf_contact_set(self, mock_del_cs_task):
        """Tests that a non-deletable contact set cannot be deleted."""
        self.admin_user.is_staff = True
        self.admin_user.save()
        self._login(user=self.admin_user)
        # verify that the user is an admin
        self.assertTrue(self.admin_user.is_admin)

        self.client.force_authenticate(user=self.admin_user)
        url = reverse('seat_source_contact_set_api-detail', args=(
            self.admin_user.id, self.admin_seat.id, self.housefile_cs_1.id))

        # Verify that housefile contact set can be deleted by admin user
        response = self.client.delete(url)
        assert response.status_code == 204


class SourceContactSetDeleterTests(TestCase):
    def setUp(self):
        self.account = AccountFactory()
        self.user = self.account.account_user
        self.maxDiff = None
        self.source = "google-oauth2"
        self.source_uid = "revup@revup.com"
        self.import_record = ImportRecordFactory(
            user=self.user, import_type="GM", uid=self.source_uid,
            account=self.account)
        self.import_record2 = ImportRecordFactory(
            user=self.user, import_type="GM", uid=self.source_uid,
            account=self.account)
        self.filter, _ = Filter.objects.get_or_create(
            title="Import Source Detail", filter_type="CN",
            module_name="frontend.apps.filtering.filter_controllers.contacts.imports",
            class_name="ImportSourceDetailFilter")
        self.callgroup = CallTimeContactSetFactory(user=self.user)

    def _setup_contacts(self):
        contacts = [ContactFactory(user=self.user,
                                   import_record=self.import_record,
                                   contact_type=self.source)
                    for i in range(3)]
        contacts += [ContactFactory(user=self.user,
                                    import_record=self.import_record2,
                                    contact_type=self.source)
                     for i in range(3)]

        # Merged with children from the same source, and child merged
        parent = ContactFactory(user=self.user, contact_type="merged",
                                phone_numbers=[], email_addresses=[],
                                addresses=[])
        t1 = ContactFactory(user=self.user, parent=parent,
                            contact_type=self.source,
                            import_record=self.import_record)
        t2 = ContactFactory(user=self.user, parent=parent,
                            contact_type=self.source,
                            import_record=self.import_record2)
        t3 = ContactFactory(user=self.user, contact_type="merged",
                            parent=parent)

        parents = [parent]
        contacts.extend([t1, t2, t3])

        # Merged with children 1 child from different source
        parent2 = ContactFactory(user=self.user, contact_type="merged",
                                 phone_numbers=[], email_addresses=[],
                                 addresses=[])
        t1 = ContactFactory(user=self.user, parent=parent2, first_name="nodelete")
        t2 = ContactFactory(user=self.user, parent=parent2,
                            contact_type=self.source,
                            import_record=self.import_record2)
        parents.append(parent2)
        contacts.extend([t1, t2])
        ContactIndex.bulk_update_index(
            ([c for c in contacts if not c.parent] + parents), [self.filter])

        self.callgroup.add(CallTimeContactFactory(contact=parent))
        CallTimeContactFactory(contact=parent2)
        return contacts, parents

    def _setup_contactsets(self, user=None, account=None):
        if not user:
            user = self.user
        if not account:
            account = self.account
        parent_cs = ContactSetFactory(user=user, account=account)
        contact_set = ContactSetFactory(user=user, account=account,
                                        source="GM",
                                        source_uid=self.source_uid,
                                        query={"import_source_detail":
                                                   [self.source_uid]})
        contact_set.parents.set([parent_cs])
        contact_set.filters.set([self.filter])
        seat = SeatFactory(account=account, user=user)
        self.seat = seat
        ContactSetsSeats.objects.create(contact_set=contact_set, seat=seat)
        contact_set.update_count()
        return contact_set, parent_cs, seat

    def test_dryrun(self):
        contacts, parents = self._setup_contacts()
        contact_set, _, _ = self._setup_contactsets()
        dryrun_results = SourceContactSetDeleter().dryrun(contact_set)
        self.assertEqual(
            dict(dryrun_results),
            dict((('Contact Sets', 1), ('Shares', 1), ('Contacts', 8),
                  ('Call Time Contacts', 1), ('Call Groups', 1),
                  ('Call Logs', 0), ('Notes', 0))))

    @patch("frontend.apps.contact.deletion.ContactDeleteTransaction")
    @patch("frontend.apps.contact_set.deletion.partition_into_batches")
    @patch("frontend.apps.contact_set.deletion.group")
    def test_parallel_delete(self, _, yield_batches, __):
        contacts, parents = self._setup_contacts()
        contact_set, parent_cs, seat = self._setup_contactsets()
        SourceContactSetDeleter().parallel_delete(contact_set)

        # Verify the correct contacts are being sent to deletion
        self.assertCountEqual(
            [contact.id for contact in (contacts[0:8] + [contacts[-1]])],
            list(yield_batches.call_args[0][0]))

    @patch("frontend.apps.contact.deletion.ContactDeleteTransaction")
    def test_delete(self, _):
        contacts, parents = self._setup_contacts()
        contact_set, parent_cs, seat = self._setup_contactsets()
        results = SourceContactSetDeleter().complete_parallel_delete(contact_set)
        expected = {
            'contact_set.ContactSet': 1,
            'contact_set.ContactSet_filters': 1,
            'contact_set.ContactSet_parents': 1,
            'contact_set.ContactSetsSeats': 1,
        }
        self.assertEqual(results, expected)

        with self.assertRaises(ObjectDoesNotExist):
            contact_set.refresh_from_db()

    @patch("frontend.apps.contact.deletion.ContactDeleteTransaction")
    @patch("frontend.apps.contact_set.deletion.group")
    def test_delete_user_cs(self, _, __):
        """Verify a user's contactsets from other seats are also deleted"""
        self.user = RevUpUserFactory()
        self.import_record.user = self.user
        self.import_record.account = None
        self.import_record.save()
        self.import_record2.user = self.user
        self.import_record2.account = None
        self.import_record2.save()

        contacts = self._setup_contacts()
        contact_set, parent_cs, seat = self._setup_contactsets()
        contact_set2, parent_cs2, seat2 = self._setup_contactsets(
            self.user, AccountFactory())

        results = SourceContactSetDeleter().complete_parallel_delete(contact_set)
        expected = {
            'contact_set.ContactSet': 2,
            'contact_set.ContactSet_filters': 2,
            'contact_set.ContactSet_parents': 2,
            'contact_set.ContactSetsSeats': 2,
        }
        self.assertEqual(results, expected)

        for obj in [contact_set, contact_set2]:
            with self.assertRaises(ObjectDoesNotExist):
                obj.refresh_from_db()

    @patch("frontend.apps.contact.deletion.ContactDeleteTransaction")
    @patch("frontend.apps.contact.models.Contact.get_preferred_gender",
           return_value=None)
    def test_contact_deletion(self, *_):
        """Verify the contacts specified by the contactset deleter are
                correctly deleted.
        """
        contacts, parents = self._setup_contacts()
        self.assertEqual(len(contacts), 11)
        self.assertEqual(len(parents), 2)
        contact_set, parent_cs, seat = self._setup_contactsets()
        with patch("frontend.apps.contact_set."
                   "deletion.partition_into_batches") as mock:
            SourceContactSetDeleter().parallel_delete(contact_set)
            contact_ids = list(mock.call_args[0][0])

        results = delete_contacts_task.apply(
            args=(self.user.id, contact_ids,),
            kwargs=dict(delete_children=False, clean_parents=True)).get()
        expected = {
            'actstream.Action': 0,
            'analysis.ResultSort': 0,
            'call_time.CallTimeContact': 1,
            'call_time.CallTimeContactSet_contacts': 1,
            'campaign.ProspectCollision': 0,
            'contact.Address': 10,
            'contact.AlmaMater': 0,
            'contact.Contact': 11,
            'contact.Contact_locations': 0,
            'contact.ContactNotes': 0,
            'contact.DateOfBirth': 11,
            'contact.EmailAddress': 10,
            'contact.ExternalProfile': 0,
            'contact.ExternalId': 11,
            'contact.Gender': 11,
            'contact.ImageUrl': 0,
            'contact.Organization': 0,
            'contact.PhoneNumber': 10,
            'contact.RawContact': 0,
            'contact.Relationship': 0,
            'entities.ContactLink': 0,
            'filtering.ContactIndex': 7}
        self.assertEqual(results, expected)

        for obj in (contacts[0:9] + [contacts[10]] + [parents[0]]):
            with self.assertRaises(ObjectDoesNotExist):
                obj.refresh_from_db()

        for obj in (contacts[9], parents[1], seat, self.filter, parent_cs,
                    self.import_record, self.import_record2):
            obj.refresh_from_db()
            self.assertIsNotNone(obj.id)

        # Verify one child from the correct source was deleted, while the other
        # was not
        children = parents[1].contacts.all()
        self.assertEqual(len(children), 1)
        self.assertEqual(children[0], contacts[9])
        self.assertEqual(children[0].first_name, "nodelete")


class ContactSetTasksTests(TestCase):
    def setUp(self):
        self.user = RevUpUserFactory()
        self.account = AccountFactory()
        self.analysis = AnalysisFactory(account=self.account)
        cs_kwargs = dict(
            user=self.user,
            account=self.account,
            analysis=self.analysis,
        )
        self.root_cs = ContactSetFactory(**cs_kwargs)
        self.contact_sets = [ContactSetFactory(source="GM",
                                               **cs_kwargs)
                             for i in range(5)]

        self.contacts = [ContactFactory(user=self.user) for i in range(5)]

    def test_update_contactsets_count(self):
        """Verify update_contactsets_count updates the counts on ContactSets
        and CallTimeContactSets (CallGroups). Archived ContactSets should be
        ignored.
        """
        # Some more test setup
        other_contact_set = ContactSetFactory(
            account=self.account,
            analysis=AnalysisFactory(account=self.account),
            source="GM")
        other_contacts = [ContactFactory(user=other_contact_set.user)
                          for i in range(5)]

        # Archive one of the ContactSets so it is ignored
        archived_contact_set = self.contact_sets.pop()
        archived_contact_set.delete()
        assert archived_contact_set.archived != None

        ## Verify the counts on active ContactSets are updated
        # Establish a baseline
        for cs in self.contact_sets:
            assert cs.count == 0
        assert other_contact_set.count == 0
        assert self.root_cs.count == 0

        update_contactsets_count.apply(args=(self.user.id,))
        for cs in self.contact_sets:
            cs.refresh_from_db()
            assert cs.count == 5

        ## Verify root wasn't changed since there are no Results yet
        self.root_cs.refresh_from_db()
        assert self.root_cs.count == 0

        ## Verify archived ContactSets are ignored
        archived_contact_set.refresh_from_db()
        assert archived_contact_set.count == 0

        ## Verify ContactSets for a different user are untouched.
        other_contact_set.refresh_from_db()
        assert other_contact_set.count == 0
        # Verify passing the other_contact_set user does give a count
        update_contactsets_count.apply(args=(other_contact_set.user.id,))
        other_contact_set.refresh_from_db()
        assert other_contact_set.count == 5

        ## Verify if we create Results for root, we do get a count
        results = [ResultFactory(contact=c, user=c.user,
                                 analysis=self.root_cs.analysis)
                   for c in self.contacts]
        update_contactsets_count.apply(args=(self.user.id,))
        self.root_cs.refresh_from_db()
        assert self.root_cs.count == 5


class ClientDataContactSetViewSetTests(AuthorizedUserTestCase):
    def setUp(self):
        super().setUp(login_now=False)
        self.account = self.user.current_seat.account
        self.contact_set = ContactSetFactory(
            account=self.account, user=self.account.account_user,
            source=ContactSet.Sources.HOUSEFILE)

        self.url = reverse("contact_sets_client_data_api-list",
                           args=[self.account.id])

    def test_permissions(self):
        # Verify user must be logged in
        response = self.client.get(self.url)
        assert response.status_code == 403

        # Verify positive case
        self._login()
        response = self.client.get(self.url)
        assert response.status_code == 200

        # Verify user must be a member of the account to view personas
        self.user.current_seat.account = AccountFactory()
        self.user.current_seat.save()
        response = self.client.get(self.url)
        assert response.status_code == 403

    def test_get(self):
        # Setup the test
        self._login()
        # Create a ContactSet from another account, verify it isn't returned
        other_cs = ContactSetFactory(
            user=self.account.account_user,
            source=ContactSet.Sources.HOUSEFILE)

        response = self.client.get(self.url)
        assert response.status_code == 200
        results = response.json()['results']

        assert len(results) == 1
        results = results[0]
        assert results['id'] == self.contact_set.id
        self.assertNotContains(response, other_cs.title)
