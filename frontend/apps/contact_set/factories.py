
import factory
from factory.fuzzy import FuzzyText

from .models import ContactSet


@factory.use_strategy(factory.CREATE_STRATEGY)
class ContactSetFactory(factory.DjangoModelFactory):
    class Meta:
        model = ContactSet

    title = FuzzyText()
    source = ""
    user = factory.SubFactory("frontend.apps.authorize.factories"
                              ".RevUpUserFactory")
    account = factory.SubFactory("frontend.apps.campaign.factories"
                                 ".AccountFactory")
