
from django.contrib import admin

from .models import ContactSet
from frontend.libs.utils.admin_utils import UserAccountAdmin
from frontend.libs.utils.task_utils import UserAnalysisFlags


class ContactSetAdmin(UserAccountAdmin):
    list_display = UserAccountAdmin.list_display + (
        "title", "count", 'has_analysis', "source")
    raw_id_fields = UserAccountAdmin.raw_id_fields + ("parents", "analysis")
    search_fields = UserAccountAdmin.search_fields + ("title", "source_uid")
    actions = ["analyze"]

    def has_analysis(self, obj):
        return bool(obj.analysis_id)

    def analyze(self, request, queryset):
        from frontend.apps.analysis.utils import submit_analysis
        for cs in queryset.iterator():
            flags = UserAnalysisFlags(cs.user).prepare_flags()
            submit_analysis(cs.user, cs.account, contact_source=cs,
                            flags=flags)


admin.site.register(ContactSet, ContactSetAdmin)