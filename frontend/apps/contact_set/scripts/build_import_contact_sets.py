
from frontend.apps.contact.models import ImportRecord
from frontend.apps.contact_set.models import ContactSet
from frontend.apps.filtering.filter_controllers.contacts.imports import \
    ImportSourceDetailFilter
from frontend.apps.filtering.models import Filter
from frontend.apps.seat.models import Seat


def run():
    import_source_filter = Filter.objects.get(title="Import Source Detail")

    # Iterate over every seat and create a root CS and a CS for each
    # import source
    seat_count = Seat.objects.count()
    for i, seat in enumerate(Seat.objects.all(), start=1):
        default_analysis = seat.get_default_analysis()

        # Get the root CS for this seat
        root = ContactSet.objects.get(account=seat.account, user=seat.user,
                                      parents=None)

        # The root creation migration didn't update the counts
        # (for performance reasons). Let's do that here.
        if not root.count:
            root.update_count()
            root.save()

        print("Processing seat {} of {}: {}".format(i, seat_count, seat.user))
        import_records = ImportRecord.objects.filter(user=seat.user).distinct(
            "import_type", "uid", "user")
        for import_record in import_records:
            source = import_record.import_type
            # We don't have an analog for the "Other" import type
            if source == "OT":
                continue

            query_args = {
                ImportSourceDetailFilter.filter_arg: [import_record.uid]}

            if not ContactSet.objects.filter(
                    account=seat.account, user=seat.user,
                    title=import_record.label, parents=root).exists():
                ContactSet.create(root, import_record.label,
                                  query_args=query_args,
                                  filters=[import_source_filter],
                                  source=source, source_uid=import_record.uid)
                # ContactSet.create(import_record.label, seat.account, seat.user,
                #                   analysis=default_analysis, source=source,
                #                   parents=root, query_args=query_args,
                #                   filters=[import_source_filter])
