import logging
import random

from celery import task, current_task
from django.conf import settings
from django.db import transaction
from django.template.loader import render_to_string

from frontend.apps.analysis.utils import submit_analysis
from frontend.apps.authorize.models import RevUpUser
from frontend.apps.call_time.models import CallTimeContactSet
from frontend.apps.contact.analysis.contact_import import run_import
from frontend.apps.contact.api.views import BaseContactViewSet
from frontend.apps.contact_set.models import ContactSet
from frontend.apps.seat.models import Seat
from frontend.libs.utils.celery_utils import get_task_record
from frontend.libs.utils.email_utils import EmailMessage
from frontend.libs.utils.task_utils import UserAnalysisFlags, UserFlags
from .deletion import SourceContactSetDeleter


LOGGER = logging.getLogger(__name__)


@task(max_retries=20, default_retry_delay=10*60, queue='email')
def notify_seat_of_share(seat_id, contact_set_id, shared_by_user_id,
                         protocol_host):
    seat = Seat.objects.select_related("user").get(pk=seat_id)
    contact_set = ContactSet.objects.get(pk=contact_set_id)
    shared_by = RevUpUser.objects.get(pk=shared_by_user_id)
    sender_address = '{} via {}'.format(shared_by.name,
                                        settings.DEFAULT_FROM_EMAIL)

    subject = 'A RevUp contact list has been shared with you'
    context = dict(
        shared_by=shared_by,
        contact_set=contact_set,
        seat=seat,
        protocol_host=protocol_host,
    )
    template = render_to_string('contact_set/emails/new_share.html', context)
    msg = EmailMessage(subject, template,
                       sender_address,
                       [seat.user.email])
    msg.content_subtype = "html"  # Main content is now text/html

    # This actually sends the email. Use with caution
    msg.send()

    logging.info("Sent an email to notify seat of share to the "
                 "address: '{}'".format(seat.user.email))


@task(queue="contact_deletion", max_retries=1000)
def delete_contact_set_task(contact_set_id):
    """Delete a contact set and its contacts"""
    task_ = get_task_record()
    if not task_:
        return False
    if task_.blocked():
        current_task.retry(countdown=30 + random.randint(-10, 10))

    if task_.finished():
        LOGGER.info(f"Deletion of {contact_set_id} already finished. "
                    f"Exiting Task now.")
        return

    try:
        contact_set = ContactSet.objects.get(id=contact_set_id)
    except ContactSet.DoesNotExist:
        LOGGER.warning(f"ContactSet {contact_set_id} has already been deleted."
                       f" Exiting Task now.")
        return

    LOGGER.info("Delete ContactSet: {}({}). Account {}".format(
        contact_set.title, contact_set_id, contact_set.account))
    try:
        return SourceContactSetDeleter(update_progress=True).parallel_delete(
            contact_set)
    except Exception:
        # If the deletion fails, we need to unarchive the contactset
        if contact_set.archived and contact_set.id:
            contact_set.archived = None
            contact_set.save(update_fields=["archived"])
        raise


@task(queue="contact_deletion")
def delete_contact_set_complete_task(contact_set_id):
    """Complete the parallel deletion of a contact-set"""
    contact_set = ContactSet.objects.get(id=contact_set_id)
    try:
        results = SourceContactSetDeleter(
            update_progress=True).complete_parallel_delete(contact_set)
        LOGGER.info("Deleted Contact Set {}({}). {}".format(
            contact_set.title, contact_set_id, results))

        # Use the contact delete API to invalidate the cache. It should be
        # all the same caches
        BaseContactViewSet.invalidate_cache(contact_set.user)
        update_contactsets_count.delay(contact_set.user_id)

        # Submit an analysis with a 5 minute delay, in case the user executes
        # more deletes.
        flags = UserAnalysisFlags(contact_set.user).prepare_flags()
        submit_analysis(contact_set.user, contact_set.account,
                        countdown=5 * 60, flags=flags, add_to_parent=False)
    except Exception:
        # If the deletion fails, we need to unarchive the contactset
        if contact_set.archived and contact_set.id:
            contact_set.archived = None
            contact_set.save(update_fields=["archived"])
        raise


@task(queue="celery", max_retries=1000)
@transaction.atomic
def update_contactsets_count(user_id):
    """Update the count of every contactset associate with user-id"""
    user = RevUpUser.objects.get(id=user_id)
    for contact_set in ContactSet.objects.active().filter(user=user)\
                                                  .select_for_update():
        contact_set.update_count()
        contact_set.save(update_fields=["count"])

    for calltime_contactset in CallTimeContactSet.objects.active()\
                                                         .filter(user=user)\
                                                         .select_for_update():
        calltime_contactset.clean_ordering_and_save()


@task(queue="contact_import")
def clone_contactset_task(contact_set_id):
    """Clone the given contactset to the account associated with it"""
    contact_set = ContactSet.objects.select_related("user", "account") \
                                    .get(id=contact_set_id)
    flags = UserFlags(contact_set.user).prepare_flags()

    return run_import(contact_set.user, contact_set.account, "cloned",
                      contact_set, feature_flags=flags)
