
from django.conf.urls import url

from .views import ListManagerView

urlpatterns = [
    url(r'^manager/?$', ListManagerView.as_view(), name="list_manager"),
]

