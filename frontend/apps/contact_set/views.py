
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie

from frontend.apps.campaign.routes import ROUTES
from frontend.libs.django_view_router import RoutingTemplateView


class ListManagerView(RoutingTemplateView):
    routes = ROUTES
    template_name = 'contact_set/list_manager.html'

    @method_decorator(login_required)
    @method_decorator(ensure_csrf_cookie)
    def do_dispatch(self, request, *args, **kwargs):
        return super(ListManagerView, self).do_dispatch(request,
                                                        *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListManagerView, self).get_context_data(**kwargs)
        user = self.request.user
        seat = user.current_seat
        analysis = user.get_default_analysis()
        account = seat.account
        context.update({
            "user": user,
            "analysis": analysis,
            "seat": seat,
            "account": account,
        })
        return context


