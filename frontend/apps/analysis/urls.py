from django.conf.urls import url

from frontend.apps.analysis.views import (
    ContactSetAnalysisResultListView, NetworkSearchView, EntityDiffDetailView)


urlpatterns = [
    url(r'^networks/search/$',
        NetworkSearchView.as_view(), name='network_search'),

    url(r'^$', ContactSetAnalysisResultListView.as_view(),
        name='analysis_result_list'),

    url(r'^entity/(?P<pk>[^/]+)/$', EntityDiffDetailView.as_view(),
        name='entity_detail'),
]
