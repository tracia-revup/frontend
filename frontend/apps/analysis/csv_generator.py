import codecs
import csv
from functools import partial, reduce
from io import BytesIO
import logging
import os

from django.conf import settings
from django.core.files import File
from django.core.files.storage import FileSystemStorage
from django.db.models import QuerySet
from django.urls import reverse
from storages.backends.s3boto3 import S3Boto3Storage

from frontend.apps.authorize.models import RevUpUser
from frontend.apps.batch_jobs.models import BatchFile
from frontend.apps.contact.models import (Address, EmailAddress, PhoneNumber,
                                          Organization, AlmaMater, ExternalId,
                                          _select_storage)
from frontend.libs.utils.celery_utils import update_progress
from frontend.libs.utils.string_utils import random_uuid
from frontend.libs.utils.view_utils import build_protocol_host


LOGGER = logging.getLogger(__name__)


def select_csv_storage():
    """Use revup-contact-upload s3 bucket to store files temporarily if s3
    storage is enabled. Otherwise, use local media storage to store files
    temporarily"""
    storage = _select_storage()
    if not storage:
        storage = FileSystemStorage(location=settings.MEDIA_ROOT)
    return storage


class CSVGenerator(object):
    score_field = 'RevUp Score'
    name_fields = ('Last Name', 'First Name')
    contact_info_fields = (
        EmailAddress.csv_fields + Address.csv_fields + PhoneNumber.csv_fields +
        Organization.csv_fields + AlmaMater.csv_fields + ExternalId.csv_fields)
    total_field_tmpl = '{years}+ Years of Giving'
    detail_field_name = 'Details'
    notes_fields = ("Notes 1", "Notes 2")
    analysis_result_detail_viewname = 'analysis_result_list'
    site_url = build_protocol_host()

    class Container(object):
        """A simple container object that will convert kwargs into fields.

        This is to provide for flexibility in the kinds of objects that can
        be used to generate a CSV.
        """
        def __init__(self, **kwargs):
            for field, value in kwargs.items():
                setattr(self, field, value)

    def __init__(self, contact_set, partition,
                 analysis=None, user_id=None, update_progress=None,
                 show_score=True, show_name=True, show_contact_info=True,
                 show_total=True, show_notes=False, pretty_details=False):
        """
        :param contact_set: The contact set containing the contacts to export
        :param partition: The partition being exported
        :param analysis: The analysis these results were generated from
        :param user_id: The requesting user's ID, for logging purposes
        :param show_score: boolean that determines if the score column
                    is included in the csv.
        :param show_name: boolean that determines if the name columns
                    are included in the csv.
        :param show_contact_info: boolean that determines if the name and
                    contact info columns are included in the csv.
        :param show_notes: boolean that determines if the contact notes are
                    included in the csv
        :param show_total: boolean that determines if the total giving column
                    is included in the csv.
        :param pretty_details: boolean that determines if the details link uses
                    the hyperlink function, or is a bare url.
        """
        self.contact_set = contact_set
        self.partition = partition
        self.analysis = analysis or contact_set.analysis
        self.user_id = user_id or self.contact_set.user_id
        self.update_progress = update_progress
        # Options
        self.show_score = show_score
        self.show_name = show_name
        self.show_contact_info = show_contact_info
        self.show_notes = show_notes
        self.show_total = show_total
        self.pretty_details = pretty_details
        self.partial_file_names = []

    def partial_filenames(self, file_count):
        """Returns a list of names for partial csv files"""
        # Check if the list was already populated
        if self.partial_file_names:
            assert len(self.partial_file_names) == file_count
        else:
            # Generate a list of names for partial csv files if the list
            # wasn't already generated
            random_uid = random_uuid()
            self.partial_file_names = [f"user_{self.user_id}_{random_uid}_" \
                                       f"part{i}.csv" for i in range(file_count)]
        return self.partial_file_names

    def _format_score_field(self, container, result):
        """Populate the row result dict with score

        Run normalization if min and max score are found in meta_results.

        Do this in a separate function so if the user decides they do not want
        this column, we can just leave it out of the functional composition.
        It also makes unit testing much easier.
        """
        if not container.analysis_result:
            return result
        score = None
        # Attempt to normalize using the contact set
        normalize_meta = self.contact_set.get_score_min_max(self.partition.id)
        if normalize_meta:
            min_score = normalize_meta.get('min_score')
            max_score = normalize_meta.get('max_score')
            if not (min_score is None or max_score is None):
                score = container.analysis_result._normalize_score(
                    container.analysis_result.score, min_score, max_score)
        if not score:
            score = container.analysis_result.get_normalized_score(
                                                                self.analysis)
        result[self.score_field] = "{:.2f}".format(score)
        return result

    def _format_contact_info_fields(self, container, result):
        """Populate the row result dict with contact info.

        Do this in a separate function so if the user decides they do not want
        these columns, we can just leave it out of the functional composition.
        This also makes verifying these columns in isolation much easier.
        """
        result.update(container.contact.format_for_csv())
        return result

    def _generate_detail_field_url(self, container):
        postfix = '?contact_set={}&contact_id={}'.format(
            self.contact_set.pk, container.contact.id)
        if self.analysis.id != self.contact_set.analysis_id:
            postfix += '&analysis={}'.format(self.analysis.id)
        return reverse(self.analysis_result_detail_viewname) + postfix

    def _format_detail_field(self, container, result, pretty=True):
        """Populate the row result dict with the detail field.

        If pretty is True, we use a hyperlink function that is understood by
        most spreadsheet applications so that the user sees the text "RevUp
        Details" instead of the full link.
        """
        path = self._generate_detail_field_url(container)
        detail_value = self.site_url + path
        if pretty:
            detail_value = '=HYPERLINK("{}","RevUp Details")'.format(
                detail_value)
        result[self.detail_field_name] = detail_value
        return result

    def _format_name_fields(self, container, result):
        """Populate the row result dict with the contact's name

        Do this in a separate function so if the user decides they do not want
        these columns, we can just leave it out of the functional composition.
        It also makes unit testing much easier.
        """
        result[self.name_fields[0]] = container.contact.last_name
        result[self.name_fields[1]] = container.contact.first_name
        return result

    def _format_notes_fields(self, container, result):
        notes = container.contact.notes.first()
        if notes:
            result[self.notes_fields[0]] = notes.notes1
            result[self.notes_fields[1]] = notes.notes2
        return result

    def _format_total_field(self, container, result, total_field_name):
        """Populate the row result dict with the giving total.

        Do this in a separate function so if the user decides they do not want
        this column, we can just leave it out of the functional composition.
        It also makes unit testing much easier.
        """
        if not container.analysis_result:
            return result
        result[total_field_name] = container.analysis_result.key_signals['giving']
        return result

    @staticmethod
    def _compose(*fns):
        """Functional composition of methods.

        compose(f, g) reads 'f composed with g', or f(g(x))

        Note: this means the first function is executed last.

        This compose function is modified for the specific use-case of this
        class in order to make the methods more pythonic and natural, making
        using the composed functions easier to use in isolation for testing.
        """
        return reduce(lambda f, g: lambda x, y: f(x, g(x, y)), fns)

    def _prepare_container(self, obj):
        """This may be overridden by subclasses if the 'obj' is not a Result"""
        return self.Container(analysis_result=obj, contact=obj.contact)

    def _inject_composition_functions(self):
        return [], []

    def _prepare_composition_functions(self):
        """Prepare the functions that will compile each line of the CSV.
        Each function represents one or more columns in a row of data.
        """
        fns = []
        fields = []

        if self.show_score:
            fields.append(self.score_field)
            fns.append(self._format_score_field)

        if self.show_name:
            fields.extend(self.name_fields)
            fns.append(self._format_name_fields)

        if self.show_contact_info:
            fields.extend(self.contact_info_fields)
            fns.append(self._format_contact_info_fields)

        if self.show_total:
            if self.analysis.partition_set.partition_configs.count() > 1:
                total_field_name = self.total_field_tmpl.format(
                    years=self.partition.years_of_giving)
            else:
                total_field_name = "Giving"

            fields.append(total_field_name)
            fns.append(partial(
                self._format_total_field,
                total_field_name=total_field_name))

        # Inject fields before the details link. It looks less sloppy
        injected_fns, injected_fields = self._inject_composition_functions()
        fns.extend(injected_fns)
        fields.extend(injected_fields)

        if self.show_notes:
            fields.extend(self.notes_fields)
            fns.append(self._format_notes_fields)

        # This is a link to the Result details
        fields.append(self.detail_field_name)
        fns.append(partial(
            self._format_detail_field, pretty=self.pretty_details))
        return fns, fields

    def _prepare_queryset(self, queryset):
        return queryset.select_related("contact").iterator()

    def generate_csv(self, queryset, add_header=True):
        """Returns a generator that iterates over each line of the csv."""
        fns, fields = self._prepare_composition_functions()

        # Use functional composition to run all the `_format_*` methods to
        # populate the row result. This prevents us from having to test whether
        # we need to run each method for each row.
        row_formatter = self._compose(*fns)

        # Use EchoIO so the csv formatted string is returned from writerow.
        csvwriter = csv.DictWriter(EchoIO(), fields)
        # Yield the header row. Yes DictWriter has a `writeheader` method, but
        # it doesn't return the result of the internal writerow method call, so
        # we are unable to capture the output EchoIO returns, making the method
        # useless for our purposes.
        if add_header:
            header = dict(zip(csvwriter.fieldnames, csvwriter.fieldnames))
            yield csvwriter.writerow(header)

        # Maintain count of total records and records yielded for debugging
        if isinstance(queryset, QuerySet):
            record_count = queryset.count()
        else:
            record_count = len(queryset)
        read_count = 0
        finished = 0.0
        iteration_step = 0.97

        try:
            for obj in self._prepare_queryset(queryset):
                container = self._prepare_container(obj)
                row_dict = row_formatter(container, {})
                read_count += 1
                yield csvwriter.writerow(row_dict)

                # Track progress
                if self.update_progress:
                    finished += iteration_step
                    self.update_progress(finished, record_count)
        finally:
            if read_count != record_count:
                LOGGER.warn("Only {} of {} records read by CSV export for "
                            "user id {}".format(read_count, record_count,
                                                self.user_id))

    def parallel_complete(self, filename, batch_count):
        """Merges the partial csv files"""
        partial_files = self.partial_filenames(batch_count)
        storage = select_csv_storage()
        csv_export_success = False
        batch_file_record = None

        try:
            if batch_count > 1:
                temp_full_file = random_uuid() + ".csv"
                temp_f = BytesIO()
                for i in range(batch_count):
                    partial_filename = partial_files[i]
                    csvfile = storage.open(partial_filename, mode='rb')
                    temp_f.write(csvfile.read())
                storage.save(temp_full_file, temp_f)
            else:
                assert len(partial_files) == 1
                temp_full_file = partial_files[0]

            update_progress(99, 100)

            # Save the file as a BatchFile object
            user = RevUpUser.objects.get(id=self.user_id)
            batch_file_record = BatchFile.objects.create(
                user=user,
                file_name=filename or "",
                file_type=BatchFile.FileType.CSV,
                data_file=File(storage.open(temp_full_file, "rb"),
                               name=os.path.basename(temp_full_file)))
            csv_export_success = True
        finally:
            # Clean up the temp files. Files in revup-contact-upload bucket
            # will get deleted automatically in 7 days. Explicit delete
            # operation on S3Boto3Storage is not being performed to avoid
            # giving delete permission
            if not isinstance(storage, S3Boto3Storage):
                storage.delete(temp_full_file)
                for _file in partial_files:
                    storage.delete(_file)
            return csv_export_success, batch_file_record


class EchoIO(object):
    """An object that implements just the write method of the file-like
    interface.
    """
    def write(self, value):
        """Write the value by returning it, instead of storing in a buffer."""
        return value
