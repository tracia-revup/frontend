
from django.core.management.base import BaseCommand, CommandError
from waffle.interface import flag_is_active_for_user

from frontend.apps.analysis.tasks import perform_analysis_task
from frontend.apps.authorize.models import RevUpUser
from frontend.libs.utils.task_utils import WaffleFlagsMixin, ANALYSIS_FLAGS


class Command(BaseCommand, WaffleFlagsMixin):
    help = ('Queue analysis task in celery for user with '
            'specific analysis config')
    flag_names = ANALYSIS_FLAGS

    def add_arguments(self, parser):
        parser.add_argument('account_id', type=int)
        parser.add_argument('user_id', type=int)
        parser.add_argument('analysis_config_id', type=int)

    @classmethod
    def _waffle_method(cls):
        return flag_is_active_for_user

    def handle(self, *args, **options):
        user = RevUpUser.objects.get(id=options['user_id'])
        perform_analysis_task.apply_async(
            args=(options['analysis_config_id'],
                  options['user_id'],
                  options['account_id']),
            kwargs=dict(feature_flags=self._prepare_flags(user)))
