
from argparse import FileType
from contextlib import closing

from django.core.management.base import BaseCommand, CommandError

from frontend.apps.analysis.models import AnalysisConfig
from frontend.apps.analysis.ac_print_utils import ac_to_dict
from frontend.libs.utils.yaml_utils import safe_dump


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-o', '--out', type=FileType('w'), default='-')
        parser.add_argument('analysis_config_id', type=int)

    def handle(self, analysis_config_id, out, **options):
        ac = AnalysisConfig.objects.get(id=analysis_config_id)
        ac_dict = ac_to_dict(ac)
        ac_yaml = safe_dump(ac_dict)
        with closing(out) as fh:
            fh.write(ac_yaml)
