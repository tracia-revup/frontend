from datetime import datetime

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Prefetch
from waffle.interface import flag_is_active_for_user

from frontend.apps.analysis.analyses import AnalysisBase
from frontend.apps.analysis.models import Analysis
from frontend.apps.authorize.models import RevUpUser
from frontend.apps.campaign.models.base import Account
from frontend.libs.utils.task_utils import WaffleFlagsMixin, ANALYSIS_FLAGS


class Command(BaseCommand, WaffleFlagsMixin):
    """
    Run a new analysis (as configured for a user or account) for each
    user-campaign seat combination where:
      + The user has an active seat on the campaign
      + The user has at least one analysis for the campaign
      + The campaign id is not in the (optional) list of ignored campaign ids
      + The user is is not in the (optional) list of users to skip
      + The user's most recent completed analysis for the campaign:
        + Has a config id in the (optional) list of configs being replaced OR
        + Is older than the specified date and time
    """
    help = "Run new analyses for users and accounts needing new analyses."

    flag_names = ANALYSIS_FLAGS

    def add_arguments(self, parser):
        parser.add_argument('-n', '--dry-run', dest="dry_run", action="store_true",
                            help="Dry run. Only list users, accounts, and events, do "
                            "not run analyses."),
        parser.add_argument('-a', '--account-ids', dest="accounts",
                            help="IDs of accounts to be limited to"),
        parser.add_argument('-i', '--ignore-account-ids', dest="ignore",
                            help="IDs of accounts to be ignored"),
        parser.add_argument('-u', '--user-ids', dest="users",
                            help="IDs of users to be limited to"),
        parser.add_argument('-s', '--skip-user-ids', dest="skip",
                            help="IDs of users to skip"),
        parser.add_argument('-b', '--before', default=None,
                            help="Rerun analysis for users who have not run an "
                            "analysis since this date. Format: "
                            "YYYY-MM-DDTHH:MM."),
        parser.add_argument(None, '--include-inactive-users', dest="inactive_users",
                            action="store_true", help="Include inactive users."),
        parser.add_argument(None, '--include-inactive-accounts',
                            dest="inactive_accounts", action="store_true",
                            help="Include inactive accounts."),
        parser.add_argument('-r', '--replace-config-ids', dest="replace",
                            help="IDs of analysis configs being replaced")

    @classmethod
    def _waffle_method(cls):
        return flag_is_active_for_user

    @classmethod
    def _clean_int_list_option(cls, value):
        if not value:
            return False

        try:
            return list(map(int, value.split(',')))
        except:
            raise CommandError("Unable to parse list of ids %s. "
                               "Please provide valid ids in the form "
                               "'1,2,3'." % value)

    @classmethod
    def _clean_timestamp_option(cls, value):
        if not value:
            return False

        try:
            return datetime.strptime(value, '%Y-%m-%dT%H:%M')
        except:
            raise CommandError("Unable to parse datetime %s. "
                               "Please provide datetime in the form "
                               "YYYY-MM-DDTHH:MM." % value)

    def handle(self, *args, **options):

        dry_run = options.get('dry_run')
        account_ids = self._clean_int_list_option(options.get('accounts'))
        ignore_account_ids = self._clean_int_list_option(options.get('ignore'))
        replace_ids = self._clean_int_list_option(options.get('replace'))
        user_ids = self._clean_int_list_option(options.get('users'))
        skip_user_ids = self._clean_int_list_option(options.get('skip'))
        before = self._clean_timestamp_option(options.get('before'))
        inactive_users = options.get('inactive_users')
        inactive_accounts = options.get('inactive_accounts')

        # Filter accounts
        accounts = Account.objects.all()
        if inactive_accounts:
            self.stdout.write("Including inactive accounts.")
        else:
            accounts = accounts.filter(active=True)
        if account_ids:
            self.stdout.write("Limiting to account ids %s" % account_ids)
            accounts = accounts.filter(pk__in=account_ids)
        if ignore_account_ids:
            self.stdout.write("Ignoring account ids %s" % ignore_account_ids)
            accounts = accounts.exclude(pk__in=ignore_account_ids)

        # Filter users
        users = RevUpUser.objects.all()
        if inactive_users:
            self.stdout.write("Including inactive users.")
        else:
            users = users.filter(is_active=True)
        if user_ids:
            self.stdout.write("Limiting to user ids %s" % user_ids)
            users = users.filter(pk__in=user_ids)
        if skip_user_ids:
            self.stdout.write("Skipping user ids %s" % skip_user_ids)
            users = users.exclude(pk__in=skip_user_ids)

        if dry_run:
            self.stdout.write("Dry run--analyses will not be performed.")
            self.stdout.write("Users, accounts, and events for which new "
                              "analyses would be run if --dry-run option not "
                              "specified:")

        for account in accounts:
            # Filter analyses by account, sort by modified descending
            account_analyses_qs = Analysis.objects.filter(
                account=account).order_by('-modified')
            # Process users with active seats on account and prefetch analyses
            for user in users.filter(
                    seats__account=account,
                    seats__state='AC').prefetch_related(
                Prefetch('analysis_set', queryset=account_analyses_qs,
                         to_attr='analyses')):

                run_analysis = False
                # Only process if user has an existing analysis
                if user.analyses:
                    latest = user.analyses[0]
                    # If current event is for account, use that
                    if (user.current_event and
                            user.current_event.account == account):
                        event = user.current_event
                    # Otherwise use event from latest account analysis
                    else:
                        event = latest.event

                    # Run new analysis if latest was prior to 'before' param
                    if before and latest.modified < before:
                        run_analysis = True
                        print(latest.modified)
                    # Run new analysis if latest has config in list to replace
                    elif (replace_ids and
                            latest.analysis_config.pk in replace_ids):
                        run_analysis = True

                if run_analysis:
                    analysis_config = user.get_default_analysis_config(
                        account)
                    # Only run new analysis if we have an analysis config
                    if analysis_config:
                        campaign = account
                        self.stdout.write(
                            'user {}, campaign {}, event {} '
                            'analysis config {}'.format(
                                user, campaign, event, analysis_config))

                        if not dry_run:
                            analysis = AnalysisBase.factory(
                                analysis_config, user, campaign, event,
                                feature_flags=self._prepare_flags(user))
                            analysis.execute()
