import logging

from django.core.management.base import BaseCommand, CommandError
from django.db import connection, models

from frontend.apps.authorize.models import RevUpUser
from frontend.apps.analysis.models import Analysis
from frontend.apps.campaign.models import Account

logger = logging.getLogger()


class Command(BaseCommand):
    """Delete old Analyses, and all of their sub-objects
    (Result, FeatureResult, etc)
    """
    help = 'Cleans up old analysis results'

    def add_arguments(self, parser):
        parser.add_argument('-n', '--dry-run', dest="dry_run",
                            default=False, action="store_true",
                            help=("Dry run. No changes are made. "
                                  "Shows which analysis will be kept."))
        parser.add_argument('-k', '--keep', dest="keep", default=3,
                            help="Number of recent analysis results to keep")
        parser.add_argument('-s', '--skip-user-ids', dest="skip_user_ids",
                            default=None, help="IDs of users to skip")
        parser.add_argument('-a', '--account-ids', dest="accounts",
                            help="IDs of accounts to be limited to")
        parser.add_argument('-i', '--ignore-account-ids', dest="ignore",
                            help="IDs of accounts to be ignored")
        parser.add_argument('-u', '--user-ids', dest="users", default=None,
                            help="IDs of users to be limited to")
        parser.add_argument('--include-inactive-users', dest="inactive_users",
                            action="store_true",
                            help="Include inactive users.")
        parser.add_argument('--include-inactive-accounts',
                            dest="inactive_accounts", action="store_true",
                            help="Include inactive accounts.")
        parser.add_argument('-p', '--protect', dest="protect", default=None,
                            help="IDs of analysis that must not be deleted")
        parser.add_argument('-f', '--skip-failed',
                            dest="skip_failed", action="store_true",
                            help=("Skip deletion of failed analysis "
                                  "(default is false)"))

    def _batch_delete(self, to_delete):
        if not to_delete.exists():
            return
        to_delete_pks = tuple(to_delete.values_list('pk', flat=True))

        # Clear the current analysis field for deleted analyses
        RevUpUser.objects.filter(current_analysis__in=to_delete_pks)\
                         .update(current_analysis=None)

        with connection.cursor() as cursor:
            cursor.execute(
                # Delete Applied Filters
                "DELETE FROM analysis_analysisappliedfilter WHERE "
                "analysis_analysisappliedfilter.analysis_id IN %(analysis_ids)s; ",
                {'analysis_ids': to_delete_pks})

            cursor.execute(
                # Delete Result Indexes
                "DELETE FROM filtering_resultindex WHERE "
                "filtering_resultindex.result_id IN "
                "(SELECT id FROM analysis_result WHERE "
                "analysis_result.analysis_id IN %(analysis_ids)s); ",
                {'analysis_ids': to_delete_pks})

            cursor.execute(
                # Delete FeatureResults
                "DELETE FROM analysis_featureresult WHERE "
                "analysis_featureresult.result_id IN "
                "(SELECT id FROM analysis_result WHERE "
                "analysis_result.analysis_id IN %(analysis_ids)s); ",
                {'analysis_ids': to_delete_pks})

            cursor.execute(
                # Delete ResultSorts
                "DELETE FROM analysis_resultsort WHERE "
                "analysis_resultsort.analysis_id IN %(analysis_ids)s; ",
                {'analysis_ids': to_delete_pks})

            cursor.execute(
                # Delete Results
                "DELETE FROM analysis_result WHERE "
                "analysis_result.analysis_id IN %(analysis_ids)s; ",
                {'analysis_ids': to_delete_pks})

            cursor.execute(
                # Delete Analysis
                "DELETE FROM analysis_analysis WHERE "
                "analysis_analysis.id IN %(analysis_ids)s; ",
                {'analysis_ids': to_delete_pks})

    def _clean_int_list_option(self, value):
        if not value:
            return False

        try:
            return list(map(int, value.split(',')))
        except:
            raise CommandError("Unable to parse list of ids. "
                               "Please provide valid ids in the form "
                               "'1,2,3'")

    def handle(self, *args, **options):
        dry_run = options.get('dry_run')
        keep = options.get('keep')
        account_ids = self._clean_int_list_option(options.get('accounts'))
        ignore_account_ids = self._clean_int_list_option(options.get('ignore'))
        user_ids = self._clean_int_list_option(options.get('users'))
        skip_user_ids = self._clean_int_list_option(options.get(
            'skip_user_ids'))
        inactive_users = options.get('inactive_users')
        inactive_accounts = options.get('inactive_accounts')
        protect_ids = self._clean_int_list_option(options.get('protect'))
        skip_failed = options.get('skip_failed')

        # Filter users
        users = RevUpUser.objects.filter(analysis__isnull=False).distinct()
        if inactive_users:
            self.stdout.write("Including inactive users.")
        else:
            users = users.filter(is_active=True)
        if user_ids:
            self.stdout.write("Limiting to user ids %s" % user_ids)
            users = users.filter(pk__in=user_ids)
        if skip_user_ids:
            self.stdout.write("Skipping user ids %s" % skip_user_ids)
            users = users.exclude(pk__in=skip_user_ids)

        # Filter accounts
        accounts = Account.objects.all()
        if inactive_accounts:
            self.stdout.write("Including inactive accounts.")
        else:
            accounts = accounts.filter(active=True)
        if account_ids:
            self.stdout.write("Limiting to account ids %s" % account_ids)
            accounts = accounts.filter(pk__in=account_ids)
        if ignore_account_ids:
            self.stdout.write("Ignoring account ids %s" % ignore_account_ids)
            accounts = accounts.exclude(pk__in=ignore_account_ids)

        analysis_exclude_kwargs = {}
        if protect_ids:
            self.stdout.write("Will not delete analysis with the ids %s" %
                              protect_ids)
            analysis_exclude_kwargs['pk__in'] = protect_ids

        if not skip_failed:
            # Query for analysis owned by the specified users in a failed state
            # for and delete them first.
            failed_analysis = Analysis.objects.filter(
                status=Analysis.STATUSES.error, contactset=None,
                user__in=users).exclude(**analysis_exclude_kwargs)
            if failed_analysis.exists():
                if dry_run:
                    self.stdout.write("Would delete {} failed analyses for the "
                                      "users specified".format(
                                          failed_analysis.count()))
                else:
                    self.stdout.write("Deleting {} failed analyses for the "
                                      "users specified".format(
                                          failed_analysis.count()))
                    self._batch_delete(failed_analysis)

        for user in users.iterator():
            # Only operate on analysis that are complete. We do not want to
            # run the risk of keeping a failed analysis, or accidentally
            # deleting an analysis that is still running.
            # Additionally only operate on analysis that are not referred to by
            # contact sets.
            analyses = user.analysis_set.filter(
                status=Analysis.STATUSES.complete, contactset=None)
            # Limit to selected accounts, if specified
            if accounts:
                analyses = analyses.filter(account__in=accounts)

            accounts_events = analyses.values_list(
                'account', 'event').order_by('account', 'event').distinct()
            for account_id, event_id in accounts_events:
                to_delete = analyses.order_by('-modified').exclude(
                    **analysis_exclude_kwargs).filter(
                        account_id=account_id, event_id=event_id)
                if dry_run:
                    would_keep = to_delete[:keep].values_list(
                        "pk", "modified")
                    would_keep = "\n".join(
                        "{:<8} {}".format(*v) for v in would_keep)
                    self.stdout.write(
                        "Would keep the following analysis results for user "
                        "%s with account id %s and event id %s:\n%s" %
                        (user.pk, account_id, event_id, would_keep))
                else:
                    self.stdout.write(
                        "Batch deleting analysis results for user %s with "
                        "account id %s and event id %s\n" %
                        (user.pk, account_id, event_id))
                    self._batch_delete(to_delete[keep:])
