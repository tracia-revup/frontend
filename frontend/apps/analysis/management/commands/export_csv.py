from io import open
import logging
import zipfile
import os

from django.core.management.base import BaseCommand, CommandError

from frontend.apps.analysis.csv_generator import CSVGenerator
from frontend.apps.analysis.models import PartitionConfig
from frontend.apps.contact_set.models import ContactSet
from frontend.apps.seat.models import Seat
from frontend.libs.utils.email_utils import EmailMessage

logger = logging.getLogger()


class Command(BaseCommand):
    """Export a CSV and email it

    example usage: manage.py export_csv -c 233 -p 6 -e michael@revup.com
    """
    help = 'Export a CSV and email it'

    def add_arguments(self, parser):
        parser.add_argument('-c', '--contact-set-id', dest="contact_set",
                            default=None, help="ID of the contact set to export")
        parser.add_argument('-s', '--seat-id', dest="seat", default=None,
                            help="ID of the seat to export. "
                                 "Mutually exclusive with contact set")
        parser.add_argument('-e', '--email', dest="email", default=None,
                            help="Email address to mail to")
        parser.add_argument('-p', '--partition-id', dest="partition",
                            default=None,
                            help="Optionally specify the partition")
        parser.add_argument('--preserve-file',
                            dest="preserve_file", action="store_true",
                            help="Don't delete the csv file after emailing")

    def handle(self, *args, **options):
        contact_set_id = options.get("contact_set")
        seat_id = options.get("seat")
        email = options.get("email")
        partition_id = options.get("partition")

        if not (contact_set_id or seat_id):
            raise CommandError("One of contact_set or seat is required")

        contact_set = None
        if contact_set_id:
            contact_set = ContactSet.objects.get(id=contact_set_id)

        if seat_id and not contact_set_id:
            seat = Seat.objects.get(id=seat_id)
            contact_set = ContactSet.get_root(seat.account, seat.user)

        if not contact_set:
            raise CommandError("Somehow contact_set not set.")

        # Prepare the CSV Generator
        if not partition_id:
            partition_id = contact_set.get_partition_configs(
                                                contact_set.analysis)[0]
        partition = PartitionConfig.objects.get(id=partition_id)
        csv_gen = CSVGenerator(contact_set, partition, pretty_details=True)

        # Generate the csv for the contacts in the contactset
        queryset = contact_set.queryset(partition=partition).select_related(
                                        "contact").order_by("-score")
        csv_iter = csv_gen.generate_csv(queryset)

        # Open a file and write to it, zipping the data
        filename = "Revup-Analysis-Results.csv"
        archive_name = "csv.zip"
        with open(filename, "wb") as f:
            f.writelines(csv_iter)

        with zipfile.ZipFile(archive_name, 'w', zipfile.ZIP_DEFLATED) as zipf:
            zipf.write(filename)

        # Email the file
        if email:
            e = EmailMessage("Revup CSV export. Contact Set: {}".format(
                             contact_set.id), "", "revup@revup.com", [email])
            e.attach_file(archive_name)

            # This actually sends the email. Use with caution
            e.send()

            logging.info("Sent an export CSV to email to the "
                         "address: '{}'".format(email))

        # Clean-up
        if email and not options.get("preserve_file"):
            os.remove(filename)
            os.remove(archive_name)


