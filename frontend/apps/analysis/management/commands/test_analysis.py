import logging
import sys

from django.core.management.base import BaseCommand, CommandError

from frontend.apps.analysis.analyses import AnalysisBase
from frontend.apps.analysis.models import AnalysisConfig
from frontend.apps.authorize.models import RevUpUser
from frontend.apps.campaign.models import Account


logger = logging.getLogger()


logger.setLevel(logging.INFO)
loggerHandler = logging.StreamHandler(sys.stdout)
loggerHandler.setLevel(logging.INFO)
loggerFormatter = logging.Formatter('%(asctime)s [%(process)d:%(thread)d:%(name)s:%(funcName)s] %(levelname)s: %(message)s')
loggerHandler.setFormatter(loggerFormatter)
logger.addHandler(loggerHandler)


class Command(BaseCommand):
    """Run one-off analysis for development purposes

    This command simplifies running analysis during development. It allows
    the user to run an analysis without having to run celery or use the web
    ui to force an analysis.

    Features include:
    - specifying the target user
    - specifying the account to run the analysis for
    - specify any arbitrary analysis config
    - able to restrict analysis to a single contact for quick iteration
    """
    help = ('Run one-off analysis for specific user with specified '
            'analysis config')
    def add_arguments(self, parser):
        parser.add_argument('-A', '--analysis-config',
                            dest='analysis_config_id', type=int)
        parser.add_argument('-a', '--account', dest='account_id', type=int,
                            default=1)
        parser.add_argument('-u', '--user',
                            dest='user_id', type=int, default=1)
        parser.add_argument('-c', '--contact',
                            dest='contact_id', type=int)
        parser.add_argument('-U', '--update', action='store_true',
                            help=("The default is to create a new analysis "
                                  "from scratch"))

    def handle(self, analysis_config_id, account_id, user_id, contact_id,
               update, *args, **options):
        u = RevUpUser.objects.get(pk=user_id)
        c = Account.objects.get(pk=account_id)
        if not analysis_config_id:
            ac = c.get_default_analysis_config()
        else:
            ac = AnalysisConfig.objects.get(pk=analysis_config_id)

        if contact_id:
            extra = {'contact_source': 'Contact',
                     'contact_source_id': contact_id}
        else:
            extra = {}
        
        ff = {
            'Analysis Name Expansion': False,
            'Analysis Verify Contribution Schema': False,
            'Analysis Alias Expansion': True,
        }
        ta = AnalysisBase.factory(ac, u, c, force_new_analysis=not update,
                                  feature_flags=ff, **extra)
        ta.execute()
        print("New analysis id: ", ta.analysis.id)
