import sys

from django.core.management.base import BaseCommand

from frontend.apps.analysis.models import AnalysisConfig, FeatureConfig
from frontend.apps.analysis.models.analysis_configs import AnalysisProfile


def alter_analysis_configs(analysis_type=None, act_ids=None,
                           analysis_profile_ids=None, include_account_ids=None,
                           exclude_account_ids=None, fc_add_id=None,
                           fc_remove_id=None, replace=False, dry_run=False):
    """
    Alters the analysis configs by adding and/or removing feature configs

    NOTE: This method is used by the migration
    analysis/migrations/0094_auto_20190208_1645.py

    :param analysis_type: Type of the analysis to filter analysis configs
    :param act_ids: IDs of the Analysis Config Templates to filter the
    AnalysisConfigs
    :param analysis_profile_ids: IDs of the Analysis Profiles to filter the
    AnalysisConfigs
    :param include_account_ids: IDs of the accounts to filter the
    AnalysisConfigs
    :param exclude_account_ids: IDs of the accounts to exclude the
    AnalysisConfigs
    :param fc_add_id: IDs of the FeatureConfigs that will be added to the
    AnalysisConfigs
    :param fc_remove_id: IDs of the FeatureConfigs that will be removed from
    the AnalysisConfigs
    :param replace: A boolean field that indicates whether the fc_remove
    should be replaced by fc_add or fc_add should be added to the
    AnalysisConfig in any case
    :param dry_run: A boolean field that indicates whether this is a dryrun
    """
    analysis_configs = AnalysisConfig.objects.all()

    # Filter the analysis configs by entity analysis type
    if analysis_type:
        analysis_configs = AnalysisConfig.objects.filter(
            analysis_profile__analysis_type=analysis_type)

    # Filter the analysis configs by analysis config templates
    if act_ids:
        analysis_configs = analysis_configs.filter(
            analysis_config_template__id__in=act_ids)

    # Filter the analysis configs by analysis profiles
    if analysis_profile_ids:
        analysis_configs = analysis_configs.filter(
            analysis_profile__id__in=analysis_profile_ids)

    # Filter the analysis configs by specified account ids
    if include_account_ids:
        analysis_configs = analysis_configs.filter(
            account__id__in=include_account_ids)
    if exclude_account_ids:
        analysis_configs = analysis_configs.exclude(
            account__id__in=exclude_account_ids)

    account_ids = list(analysis_configs.values_list('account__id',
                                                    flat=True))

    # Get the FeatureConfigs that should be added or removed
    if fc_add_id:
        fc_new = FeatureConfig.objects.get(id=fc_add_id)
    else:
        fc_new = None

    if fc_remove_id:
        fc_old = FeatureConfig.objects.get(id=fc_remove_id)
    else:
        fc_old = None

    # Stop if this is a dryrun
    if dry_run:
        print(f"This is a dry run. Analysis configs of the following accounts "
              f"are going to be modified: {account_ids}", file=sys.stdout)
        return

    if replace:
        AnalysisConfig.feature_configs.through.objects.filter(
            analysisconfig__in=analysis_configs,
            featureconfig=fc_old
        ).update(featureconfig=fc_new)
    else:
        for ac in analysis_configs:
            if fc_new:
                ac.feature_configs.add(fc_new)

            if fc_old:
                ac.feature_configs.remove(fc_old)

    print(f"Analysis configs of the following accounts were modified: "
          f"{account_ids}", file=sys.stdout)


class Command(BaseCommand):
    help = 'Command to add/remove/replace FeatureConfigs on AnalysisConfigs'

    def add_arguments(self, parser):
        parser.add_argument('--analysis_type',
                            default='ENT_CONTRIB_RANK',
                            choices=AnalysisProfile.AnalysisTypes.all(),
                            help="Type of the Analysis"
                            )
        parser.add_argument('--act', nargs='+', type=int, dest='act_ids',
                            help="IDs of analysis config templates to filter "
                                 "the analysis configs")
        parser.add_argument('--analysis_profile', nargs='+', type=int,
                            dest='analysis_profile_ids',
                            help="IDs of analysis profiles to filter the "
                                 "analysis configs")
        parser.add_argument('--include_accounts', nargs='+', type=int,
                            dest='include_account_ids',
                            help="IDs of accounts for which the analysis "
                                 "configs would be modified")
        parser.add_argument('--exclude_accounts', nargs='+', type=int,
                            dest='exclude_account_ids',
                            help="IDs of accounts for which the analysis "
                                 "config would not be modified")
        parser.add_argument('--fc_add', type=int, dest='fc_add_id',
                            help="An ID of the feature config that would be "
                                 "added")
        parser.add_argument('--fc_remove', type=int, dest='fc_remove_id',
                            help="An ID of the feature config that would be "
                                 "removed")
        parser.add_argument('--replace', action='store_true',
                            help="The script replaces the old FeatureConfig "
                                 "with the new one, only if the old one "
                                 "exists")
        parser.add_argument('--dry_run', action="store_true",
                            help="Prints a list of account ids for which the "
                                 "analysis configs would be modified")

    def handle(self, analysis_type, act_ids, analysis_profile_ids,
               include_account_ids, exclude_account_ids, fc_add_id,
               fc_remove_id, replace, dry_run, **options):
        # Do not run the script if FeatureConfig titles were not specified
        if not fc_add_id and not fc_remove_id:
            print('Please specify at least one of the arguments: '
                  '--fc_add, --fc_remove', file=sys.stderr)
            sys.exit(1)

        # Do not run the script if only one of the FeatureConfigs were
        # specified while the replace argument was also specified
        if replace and not (fc_add_id and fc_remove_id):
            print('Cannot specify --replace argument when only one of the '
                  'following arguments were specified --fc_add, '
                  '--fc_remove', file=sys.stderr)
            sys.exit(1)

        alter_analysis_configs(analysis_type, act_ids, analysis_profile_ids,
                               include_account_ids, exclude_account_ids,
                               fc_add_id, fc_remove_id, replace, dry_run)
