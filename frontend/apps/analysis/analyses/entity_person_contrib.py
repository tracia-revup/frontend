"""Person contribution ranking analysis

This module provides an analysis that ranks individual persons based on past
contributions.

This is a modification of the original, meant to work with entities.

"""
import logging
import time

from ddtrace import tracer
from django.conf import settings

from frontend.apps.analysis.analyses.person_contrib import PersonContribRanking
from frontend.libs.entity_api import transaction_status
from .exceptions import EntityResolutionPendingRetry


LOGGER = logging.getLogger(__name__)


class EntityPersonContribRanking(PersonContribRanking):

    def __init__(self, *args, **kwargs):
        super(EntityPersonContribRanking, self).__init__(*args, **kwargs)
        self.transaction_id = kwargs.get("transaction_id")
        self.expected_count = kwargs.get("expected_count")
        self.seen_count = kwargs.get("seen_count", 0)
        self.done_count = kwargs.get("done_count", 0)
        self.last_count_change = kwargs.get("last_count_change", time.time())

    @tracer.wrap()
    def _check_if_blocked(self):
        super(EntityPersonContribRanking, self)._check_if_blocked()
        # Get the status of the given transaction. If no status can be
        # checked, just run the analysis.
        if not (self.transaction_id and self.expected_count):
            return
        seen, done, done_details = transaction_status(self.transaction_id,
                                                      self.user.id)
        resolved_count = done_details[0]

        # Compare the expected count with the done count. Start the
        # analysis if 100% ready
        if float(done)/self.expected_count >= 1:
            return

        # This checks two cases. In either case, we wait:
        # 1) The contacts are actively being processed
        # 2) The contacts are stalled behind other contacts. We wait for a
        #    minimum of 5% or 100 (which ever is smaller) contacts to be
        #    resolved
        min_percentage = settings.ANALYSIS_ENTITY_MIN_PERCENTAGE
        min_count = settings.ANALYSIS_ENTITY_MIN_COUNT
        now = time.time()
        percent_resolved = resolved_count / float(self.expected_count)
        if ((seen > self.seen_count or done > self.done_count) or
                (percent_resolved < min_percentage and
                 resolved_count < min_count)):
            self.last_count_change = now
            self.seen_count = seen
            self.done_count = done

        # If we've timed out since last update, error and start the analysis
        elapsed = now - self.last_count_change
        if elapsed > settings.ANALYSIS_ENTITY_UPDATE_TIMEOUT:
            LOGGER.warning("Entity analysis timed-out after {} seconds while "
                           "waiting for entity updates. User: {}; Account: "
                           "{}; Seen: {}; Done: {}; Expected: {}".format(
                               elapsed, self.user, self.campaign, seen, done,
                               self.expected_count))
            return

        # Otherwise, retry
        raise EntityResolutionPendingRetry(self.seen_count, self.done_count,
                                           self.last_count_change)

    def start(self):
        should_start = super(EntityPersonContribRanking, self).start()
        if should_start:
            LOGGER.info("Started entity analysis. Expected: {}; Transaction: "
                        "{}".format(self.expected_count, self.transaction_id))
        return should_start
