''' This file merely contains two constant lists representing DEM affiliated
    EIDS and REP affiliated EIDS as a temporary workaround for state cmtes not
    having party affiliations in MISP data
'''

DEM_AFF_EIDS = [
    # Kerrigan Committee
    '20814010',
    # BEAU PAC, INC.
    '22199439',
    # ActBlue
    "22215739",
    "21529569",
    "21266917",
    "20815331",
    "170434",
    "20611217",
    "19909289",
    "22213876",
    "19875745",
    "19456479"
    # Hickenlooper
    "17445330",
    "6506120",
    "18917491",
    # Ted Lieu Ballot Measure Committee
    "16045917",
    # California Democratic Party
    "4807",
    "4874",
    # Betsy Butler for Assembly
    "22215418",
    # Lachman for Assembly
    "13810589",
    # Biden III
    "6472073",
    # Lieu Ted W
    "3480415",
    # Hertzberg for Senate
    "22215415",
    # RESHMA FOR NEW YORK
    "17439929",
    # FRIENDS OF CECLIA TKACZYK
    "16133131",
    # Gipson, Terry W
    "18918390",
    # Dahle, Kevin L
    "6518270",
    # Krage, Jack
    "12745958",
    "12745981", "18918126",
    "13002945", "18918044",
    "18918577",
    "16072405",
    "16072419", "18918041",
    "13012440", "18918121",
    "6515284",
    "4825", '6468707', 
    '6509536', 
    '14005733', 
    '6466806']

REP_AFF_EIDS = ['']
