"""Analysis configuration models.

This module provides an abstract base class and factory for analyses.

"""
from datetime import datetime
from abc import ABCMeta, abstractmethod
from collections import defaultdict, Counter
from contextlib import contextmanager
import itertools
import logging
import random

from celery import current_task
from ddtrace import tracer
from django import template
from django.conf import settings
from django.db import transaction

from frontend.apps.analysis.api.views import (
    ContactSetAnalysisContactResultsViewSet)
from frontend.apps.analysis.exceptions import AnalysisConfigurationError
from frontend.apps.analysis.models import (
    Analysis, AnalysisProfile, Feature, SignalSetsActions, QuestionSetData,
    AnalysisAppliedFilter, QuestionSet)
from frontend.apps.analysis.sources.base import RecordSet
from frontend.apps.filtering.models import Filter
from frontend.libs.utils.celery_utils import get_task_record
from frontend.libs.utils.general_utils import deep_join
from .exceptions import AnalysisBlockedRetry, EntityResolutionPendingRetry

LOGGER = logging.getLogger(__name__)


def _noop_update(finished, total):
    """No-op update function the analysis runner uses when an analysis is
    executed without using celery.

    This function cannot be a method or an inner function because then it will
    not pickle.
    """
    pass


class AnalysisBase(object, metaclass=ABCMeta):
    """Abstract base class for analyses

    Analysis implementations should subclass this abstract base class for
    default initialization, run() method base implementation, complete()
    method, and implementation object factory.

    """

    def __init__(self, analysis_config, user, campaign,
                 event=None, update_progress=None, feature_flags=None,
                 force_new_analysis=False, skip_revup_task=False,
                 *args, **kwargs):
        """Default initialization for analyses

        Initialize implementation and prepare Analysis record.

        :param analysis_config: analysis configuration
        :param user: request user
        :param campaign: campaign for this analysis
        :param event: event for this analysis (optional)
        :param feature_flags: dict of feature flags to enable/disable features
        :param force_new_analysis: boolean to force a new analysis record,
                                   even if one wouldn't normally be created
        """
        self.analysis_config = analysis_config
        self.user = user
        self.campaign = campaign
        self.event = event
        self.skip_revup_task = skip_revup_task
        self.is_task = bool(current_task)
        self.task = None
        self.analysis = None
        self.data_config = self.analysis_config.data_config
        self.record_set_records = {}
        self.signal_sets_results = {}
        self.feature_flags = feature_flags or {}
        self.force_new_analysis = force_new_analysis
        self.is_new_analysis = force_new_analysis
        self.uses_default_analysis_config = (
            self.user.get_default_analysis_config(self.campaign) ==
            self.analysis_config)

        if update_progress and callable(update_progress):
            self.update_progress = update_progress
        else:
            if update_progress:
                LOGGER.warn('The update_progress parameter must be callable')
            self.update_progress = _noop_update

        #TODO: Remove after abstracting older dependencies
        self._db = settings.MONGO_CLIENT.get_default_database()

        self.partition_configs = []
        partition_set_config = self.analysis_config.partition_set_config
        partition_configs = partition_set_config.partition_configs.all()
        for partition_config in partition_configs:
            self.partition_configs.append(partition_config)

        self.record_set_backends = {}
        if self.data_config:
            for data_set_config in (
                    self.analysis_config.data_config.data_set_configs.all()):
                for record_set_config in (
                        data_set_config.record_set_configs.all()):
                    self.record_set_backends[record_set_config.id] = \
                        RecordSet.factory(record_set_config,
                                          self.analysis_config,
                                          self.feature_flags)

        # Ensure that the same Filter is not assigned to multiple
        # FeatureConfigs. We currently do not support this and could lead to a
        # loss of data.
        filter_counts = Counter(filter(None,
                   self.analysis_config.feature_configs.values_list(
                       'filters__pk', flat=True)))
        # Below is a hack to get max working in case filter_counts ends up
        # being empty.
        if max(0, 0, *(filter_counts.values())) > 1:
            raise AnalysisConfigurationError(
                "AnalysisConfig has multiple FeatureConfigs with "
                "the same Filter attached!")

        # Build the config structure for use in the analysis
        self._build_feature_config_struct()

    def __getstate__(self):
        """Customize the pickle process

        The reason why I have to customize the pickle process here is because
        mongodb cannot be pickled. So I'm making a copy of __dict__, removing
        the _db attribute and value from the copy, and returning that. And
        during serialization I'm getting a reference to the db back.
        """
        state = self.__dict__.copy()
        del state['_db']
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        self._db = settings.MONGO_CLIENT.get_default_database()
        # If we're in this method and the analysis attribute is set, we're most
        # likely a celery task about to be run. Refresh the analysis record
        # from the database in case it was updated by other tasks.
        if self.analysis:
            self.analysis.refresh_from_db()

    @tracer.wrap()
    def _build_feature_config_struct(self):
        """Iterate over all the feature configs and build a data structure
            that contains all of the configuration objects we will need to
            run an analysis.

        The resulting structure is formatted as follows:
            {feature_config.id : FeatureConfigsStruct(
                 feature_config=feature_config,
                 record_set_config_map={
                        SignalSetConfig.id: RecordSetConfig.id, ... , },
                 signal_action_configs={
                        SignalSetConfig.id:
                                  {"partitioned": [SignalActionConfig, ...],
                                   "unpartitioned": [SignalActionConfig, ...]
                                   },
                                   ...
                        },
                 child_ssc_map={
                        SignalSetConfig.id: [child-SignalSetConfig.id, ...],
                },
                ...
            }
        """
        self.features = {}
        self.prime_features = []
        self.root_features = []
        self.subset_features = []
        self.summary_features = []
        self.normalization_features = []
        self.meta_features = []

        # This is used below like a switch-statement to assign a feature config
        # to a feature-type list.
        feature_list_map = {
            Feature.FeatureTypes.PRIME: self.prime_features,
            Feature.FeatureTypes.MATCHES: self.root_features,
            Feature.FeatureTypes.MATCHES_SUBSET: self.subset_features,
            Feature.FeatureTypes.SUMMARY: self.summary_features,
            Feature.FeatureTypes.NORMALIZATION: self.normalization_features,
            Feature.FeatureTypes.META: self.meta_features
        }

        self.rsc_feature_map = {}
        self.ssc_fc_lookup_map = {}

        for feature_config in (self.analysis_config.feature_configs
                                   .select_related('feature').all()):
            # Get all the signal_set_configs belonging to this feature
            signal_set_configs = feature_config.signal_set_configs.order_by(
                'featureconfigssignalsets__order')

            # Map of SignalSetConfig id to RecordSetConfig id
            ssc_rsc_map = {ssc.id: ssc.record_set_config_id
                           for ssc in signal_set_configs}
            self.rsc_feature_map.update({rsc_id: feature_config.feature.title
                                         for rsc_id in ssc_rsc_map.values()})
            self.ssc_fc_lookup_map.update({ssc.id: feature_config.id
                                           for ssc in signal_set_configs})

            # Build a structure of child SignalSetConfigs (if any).
            child_ssc_map = {
                ssc.id: list(cs.id for cs in ssc.signal_set_configs.all())
                for ssc in signal_set_configs
            }

            # Query for all signal-set actions that are associated with this
            # FeatureConfig
            signal_set_actions = SignalSetsActions.objects.filter(
                signal_set_config__in=signal_set_configs)\
                    .select_related('signal_action_config')\
                    .only('signal_action_config',
                          'signal_set_config_id',
                          'partitioned')

            # Build a data structure of SignalSetConfigs and their
            # SignalActions. Separate the signal actions by partitioned or not.
            #
            # Example:
            # {SignalSetConfig.id: {"partitioned": [SignalActionConfig, ...],
            #                       "unpartitioned": [SignalActionConfig, ...]}
            # }
            signal_actions_map = {}
            for signal_set_action in signal_set_actions:
                signal_set_config_id = signal_set_action.signal_set_config_id

                # Assign this action's signal_set_config struct to the
                # signal_actions_map, or get it if it is already there
                ssc_dict = signal_actions_map.setdefault(signal_set_config_id,
                                                         {})
                # Select the subset of partitioned or unpartitioned actions
                sac_subset = ssc_dict.setdefault(
                    '{}partitioned'.format(
                    '' if signal_set_action.partitioned else 'un'), [])

                # Initialize the SignalAction with its signal and record sets.
                # RecordSet may be None.
                record_set_config_id = ssc_rsc_map.get(signal_set_config_id)

                # Initialize the SignalAction
                signal_action = SignalAction.factory(
                    signal_action_config=signal_set_action.signal_action_config,
                    # The following items are not preserved in the
                    # SignalAction.__init__, as only a subset of SignalActions
                    # need them. If your SignalAction needs data not here,
                    # add it below as a kwarg.
                    feature_config=feature_config,
                    analysis_config=self.analysis_config,
                    record_set_config_id=record_set_config_id,
                    record_set_backends=self.record_set_backends,
                    partition_configs=self.partition_configs,
                    feature_flags=self.feature_flags,
                    rsc_feature_map=self.rsc_feature_map,
                    upstream_ssc_ids=child_ssc_map[signal_set_config_id],
                    ssc_fc_lookup_map=self.ssc_fc_lookup_map,
                )
                sac_subset.append(signal_action)

            # Initialize FilterControllers associated with feature_config for
            # use in result indexing.
            filters = {
                filter_.pk: filter_.dynamic_class.factory(feature_config,
                                                          self.analysis_config)
                for filter_ in feature_config.filters.filter(
                    filter_type=Filter.FilterTypes.RESULT)}

            # Combine all the generated structures into one group under the
            # FeatureConfig they belong to.
            self.features[feature_config.id] = FeatureConfigsStruct(
                feature_config=feature_config,
                signal_set_configs=signal_set_configs,
                record_set_config_map=ssc_rsc_map,
                signal_actions_map=signal_actions_map,
                child_ssc_map=child_ssc_map,
                filters=filters)

            # Assign the feature to a feature-type-list. Fail silently if we
            # have an unrecognized feature_type (by using a default list).
            try:
                feature_list = feature_list_map.get(
                    feature_config.feature.feature_type, [])
                feature_list.append(feature_config)
            except AttributeError:
                # Some FeatureConfigs may be no-ops with no Feature,
                # and should be ignored.
                pass

    @property
    def _analysis_kwargs(self):
        return dict(
            analysis_config=self.analysis_config,
            user=self.user,
            account=self.campaign,
            partition_set=self.analysis_config.partition_set_config,
        )

    def _get_analysis(self):
        """Query for the most recent analysis, if it exists, or create a new
        one.
        """
        try:
            return Analysis.objects.filter(
                status__in=Analysis.STATUSES.finished,
                **self._analysis_kwargs).latest('modified')
        except Analysis.DoesNotExist:
            return None

    def _new_analysis(self):
        return Analysis(**self._analysis_kwargs)

    def _reuse_analysis(self, analysis):
        """Verify this analysis is not too different to reuse.

        Reasons to create new analysis:
          1) If the analysis did not successfully complete
          2) If the analysis config is different
          3) If the partition set config has changed
          4) If the filters are not the same
        """
        if not analysis:
            return False

        ## 1
        if analysis.status != Analysis.STATUSES.complete:
            return False
        ## 2
        if self.analysis_config != analysis.analysis_config:
            return False
        ## 3
        if self.analysis_config.partition_set_config != analysis.partition_set:
            return False

        # Build a set of all the filters from all of the features
        # in the analysis config
        ac_filters = set(
            itertools.chain(*(iter(fcstruct.filters.keys())
                for fcstruct in self.features.values()))
        )
        # Build a set of all the filters in the analysis' applied filters
        a_filters = set(analysis.applied_filters.values_list("id", flat=True))

        ## 4
        if ac_filters != a_filters:
            return False

        # If we get here, the analysis has passed the test and can be used
        return True

    @classmethod
    def factory(cls, analysis_config, *args, **kwargs):
        """Factory for analysis implementation subclasses

        Create implementation subclass object based on analysis configuration.

        :param analysis_config: analysis configuration
        :param user: request user
        :param campaign: campaign for this analysis
        :param event: event for this analysis (optional)
        :param feature_flags: dict of feature flags to enable/disable features
        :return: Analysis implementation subclass
        """
        types = AnalysisProfile.AnalysisTypes
        analysis_type = analysis_config.analysis_profile.analysis_type
        module_name = ''
        class_name = ''
        if analysis_type == types.PERSON_CONTRIB_RANKING:
            module_name = 'frontend.apps.analysis.analyses'
            class_name = 'PersonContribRanking'
        elif analysis_type == types.ENTITY_CONTRIB_RANKING:
            module_name = 'frontend.apps.analysis.analyses'
            class_name = 'EntityPersonContribRanking'
        elif analysis_type == types.DUMMY:
            module_name = 'frontend.apps.analysis.analyses.dummy'
            class_name = 'DummyRunner'

        module = __import__(module_name, {}, {}, class_name)
        analysis_class = getattr(module, class_name)
        return analysis_class(analysis_config, *args, **kwargs)

    @contextmanager
    def handle_exception(self):
        """Context manager to catch exceptions while performing an analysis

        If an analysis crashes, we want to make sure to set the state of the
        analysis to error. I found myself copy+pasting this code several times,
        so I refactored it into a context manager to keep our code DRY.
        """
        try:
            yield
        except AnalysisBlockedRetry:
            LOGGER.info("Analysis rescheduled: Another analysis or import is "
                        "already running.")
            # Tasks should be able to request a retry. Typically, this is
            # for scheduling conflicts.
            # randint is used to add some variability to the wake up. If tasks
            # wake up at exactly the same time, they can get in the pattern of
            # blocking each other.
            current_task.retry(countdown=30 + random.randint(-10, 10))
        except EntityResolutionPendingRetry as r:
            kwargs = current_task.request.kwargs
            kwargs["seen_count"] = r.seen
            kwargs["done_count"] = r.done
            kwargs["last_count_change"] = r.last_count_change
            LOGGER.info("Analysis rescheduled: Contact resolution is pending. "
                        "Seen: {}, Done: {}".format(r.seen, r.done))
            current_task.retry(kwargs=kwargs,
                               countdown=30 + random.randint(-10, 10))
        except Exception:
            LOGGER.exception("Error performing {} analysis for user {} "
                             "in campaign {}".format(
                              self.__class__.__name__,
                              self.user,
                              self.campaign))
            if self.analysis is not None:
                self.analysis.status = Analysis.STATUSES.error
                self.analysis.save()
            raise

    def _get_run_data(self):
        """Data that is shared by both the parallel and serial algorithms
           should be returned from overridden versions of this method.

        This data can then be used to determine if parallelization is
            appropriate

        :returns: a dict to be used as kwargs for the run methods
        """
        return {}

    def _should_parallelize(self, **kwargs):
        """Determine if we should parallelize the analysis.
        The analysis can't be parallelized if it isn't being executed from a
        celery task.
        We also want to allow for a feature flag to disable parallelization.
        """
        return self.is_task and \
               self.feature_flags.get("Analysis Parallelization", True)

    def _check_if_blocked(self):
        """There are various reasons an analysis might be blocked. Check for
        them here and reschedule the analysis, if need be.
        """
        if self.task.blocked():
            raise AnalysisBlockedRetry("Analysis or Import already running")

    @tracer.wrap()
    def execute(self):
        """Execute the analysis.
        The execution may parallelize if the conditions are met.
        """
        with self.handle_exception():
            if self.start():
                # Start the progress at 0%
                self.update_progress(0, 100)

                run_data = self._get_run_data()
                if self._should_parallelize(**run_data):
                    return self.parallel_run(**run_data)
                else:
                    self.run(**run_data)
                    self.complete()
                    return None

    @tracer.wrap()
    def start(self):
        """Start method for analysis implementations

        If this analysis is being run as a task, checks for existing analysis
        tasks and throws an error if any are already queued up or running. If
        not, marks this analysis task as running.

        Returns True if analysis is clear to run, and False if the analysis was
        canceled and the analysis should not be run.
        """
        if self.is_task:
            if not self.skip_revup_task:
                self.task = get_task_record()
                if not self.task:
                    return False

                # Check for cases where this analysis would be blocked from
                # running and need to be rescheduled
                self._check_if_blocked()

            if current_task.request.get('redelivered', False):
                with transaction.atomic():
                    other_analysis = Analysis.objects.filter(
                        task_id=current_task.request.id)
                    if other_analysis.exists():
                        other_analysis.delete()

        if self.force_new_analysis:
            analysis = None
        else:
            # Get the best fitting, existing analysis (or None)
            analysis = self._get_analysis()

        # Check if this analysis is safe to reuse. If analysis is None, it
        # will always be False
        if self._reuse_analysis(analysis):
            self.analysis = analysis
        else:
            # Create a new analysis if the existing analysis shouldn't be used
            self.analysis = self._new_analysis()
            self.is_new_analysis = True

        LOGGER.info("Using existing analysis: {}".format(
            False if self.is_new_analysis else self.analysis.id))

        if self.is_task:
            self.analysis.task_id = current_task.request.id
        self.analysis.status = Analysis.STATUSES.running
        self.analysis.save()
        root_span = tracer.current_root_span()
        root_span.set_tag('analysis_id', self.analysis.id)
        self.apply_filters_to_analysis(self.analysis, self.features)
        return True

    @classmethod
    @transaction.atomic
    @tracer.wrap()
    def apply_filters_to_analysis(cls, analysis, features):
        """Store the filters that are being applied to the filters.

        If we are reusing an existing analysis, we will delete and save
        the filters. Although we don't allow an analysis to be reused if the
        filters have changed, the filter's configuration values may change.
        We need to preserve those config values (e.g. correlated campaigns)
        """
        # Delete the existing applied filters.
        analysis.analysisappliedfilter_set.all().delete()

        # Now rebuild the applied filters
        applied_filters = defaultdict(list)
        for fcstruct in features.values():
            for filter_id, controller in fcstruct.filters.items():
                # Get filter_config and create AnalysisAppliedFilter
                filter_configs = controller.get_filter_configs()
                # If a filter happens to be a member of multiple features,
                # record all filter configs for later deep merge
                applied_filters[filter_id].append(filter_configs)
        if applied_filters:
            AnalysisAppliedFilter.objects.bulk_create(
                [AnalysisAppliedFilter(analysis=analysis, filter_id=filter_id,
                                       filter_configs=deep_join(filter_configs))
                 for filter_id, filter_configs in applied_filters.items()])

    @abstractmethod
    def run(self, **kwargs):
        pass

    @abstractmethod
    def parallel_run(self, **kwargs):
        pass

    @tracer.wrap()
    def complete(self):
        """Default complete method for analysis implementations

        Updates the Analysis record status as complete, updates the user's
        current analysis to this Analysis result, and updates the user's guide
        to include network analysis.

        """
        self.analysis.status = Analysis.STATUSES.complete
        # Updates the last_completed timestamp since the analysis was run
        # and completed successfully
        self.analysis.last_completed = datetime.now()
        self.analysis.save()
        if self.uses_default_analysis_config:
            self.user.current_analysis = self.analysis
            self.user.save()
        # Invalidate the results cache
        ContactSetAnalysisContactResultsViewSet.invalidate_cache(self.user)


class FeatureConfigsStruct(object):
    def __init__(self, feature_config, signal_set_configs,
                 record_set_config_map, signal_actions_map, child_ssc_map,
                 filters):
        self.feature_config = feature_config
        self.signal_set_configs = signal_set_configs
        self.record_set_config_map = record_set_config_map
        self.signal_actions_map = signal_actions_map
        self.child_ssc_map = child_ssc_map
        self.filters = filters


class SignalAction(object, metaclass=ABCMeta):

    @classmethod
    def trace_name(cls):
        module = cls.__module__
        cname = cls.__name__
        return '%s.%s' % (module, cname)

    def trace_tags(self):
        return {
            'class': self.__class__.__name__,
            'sac_id': self.signal_action_config.id,
            'sac_title': self.signal_action_config.title,
        }

    @staticmethod
    def factory(signal_action_config, *args, **kwargs):
        module_name = signal_action_config.module_name
        class_name = signal_action_config.class_name
        module = __import__(module_name, {}, {}, class_name)
        action_class = getattr(module, class_name)
        # Note: To ensure relevant arguments can be received by subclasses,
        #       always pass items to 'action_class' as kwargs
        return action_class(signal_action_config=signal_action_config,
                            *args, **kwargs)

    def __init__(self, signal_action_config, *args, **kwargs):
        self.signal_action_config = signal_action_config
        self.definition = self.signal_action_config.definition

    def process(self, pipeline, partition_config=None, *args, **kwargs):
        pipeline = self.run(pipeline, partition_config=partition_config,
                            *args, **kwargs)
        return pipeline

    @abstractmethod
    def run(self, pipeline, partition_config=None, *args, **kwargs):
        pass

    @classmethod
    def build_pipeline(cls, target, partitioned=False, **kwargs):
        pipeline = Pipeline(target, partitioned=partitioned, **kwargs)
        return pipeline

    @classmethod
    def build_signal(cls):
        return {
            'partitions': defaultdict(dict),
            'multipliers': defaultdict(dict),
            'score_multipliers': defaultdict(dict),
        }


class Pipeline(object):
    def __init__(self, target, results=None, records_signals=None,
                 partitioned=False, global_signals=None):
        self.target = target
        self.results = results or defaultdict(dict)
        self.records_signals = records_signals or []

        if global_signals is None:
            global_signals = {}
        self.key_signals = global_signals.get("key_signals", {})
        self.tags = global_signals.get("tags", set())
        self.score_multipliers = global_signals.get("score_multipliers", {})

        if partitioned:
            self.results['partitions'] = defaultdict(dict)
        self.global_signals = global_signals


class SummarySignalActionMixin(object):
    """Summary signals use an extra field in the processing called
    'signal_sets'. Instead of doing the exact same override in each of them,
    they should just use a different parent class instead.
    """
    def process(self, pipeline, signal_sets=None, *args, **kwargs):
        self.signal_sets = signal_sets
        return super(SummarySignalActionMixin, self).process(
            pipeline, *args, **kwargs)


# TODO: REVUP-2696 Redo this wrapper once Persona and PersonaInstance models
# are updated with QS and QSD.
class PersonaConfigurationWrapper:
    """Fetches the configurations associated with a FeatureConfig and an
    AnalysisConfig.
    """
    def __init__(self, analysis_config, feature_config):
        # Get the personas associated with a FC
        feature = feature_config.feature
        self.account_id = analysis_config.account_id
        self.personas = feature.persona_set.all()
        self.configs = self.extract_configuration()

    def extract_configuration(self):
        """Extracts the `data` field from the relevant `QuestionSetData` per
        Persona Instance(Configuration)"""
        configs = {}
        for persona in self.personas:
            persona_configs = persona.persona_configs.filter(
                account_id=self.account_id)
            for pc in persona_configs:
                # The try-except block is a temporary hack to bypass the
                # absence of QSD in the PersonaInstance model. As mentioned in
                # the TODO comment above, the entire class may have to be
                # redone once the Persona and PersonaInstance models are
                # updated with QS and QSD.
                try:
                    configs[pc.identifier] = pc.qsd.data
                except AttributeError:
                    pass
        return configs


class ConfigurationWrapper(object):
    """Wraps a QuestionSetData queryset so it can be treated as one."""
    def __init__(self, analysis_config, feature_config=None):
        if feature_config is not None:
            # Get all of the question sets for this SignalAction
            self.question_sets = {qs.id: qs
                                  for qs in feature_config.question_sets.all()}
        else:
            self.question_sets = {}

        # Also include all global QuestionSets that are associated with our
        # AnalysisConfig
        all_feature_configs = analysis_config.feature_configs.all()
        self.question_sets.update({
            qs.id: qs
            for qs in QuestionSet.objects.filter(
                is_global=True, feature_configs__in=all_feature_configs)
        })
        self.question_set_data = {}
        for qsd in QuestionSetData.objects.configuration(
                iter(self.question_sets.values()), analysis_config):
            self.question_set_data.setdefault(
                qsd.question_set_id, []).append(qsd)

    def _get_value(self, qsd, keys):
        """Extract one or more values from a QuestionSetData object."""
        values = []
        if isinstance(keys, str):
            keys = [keys]
        has_val = False
        for key in keys:
            try:
                values.append(qsd[key])
            except template.VariableDoesNotExist:
                values.append(None)
            else:
                has_val = True

        if len(values) == 1:
            values = values[0]
        return values, has_val

    def _null_result(self, keys):
        """Prepare a null result with a tuple of Nones in length len(keys)."""
        num_keys = len(keys)
        return (None,) * num_keys if num_keys > 1 else None

    @tracer.wrap()
    def get(self, key, default=None):
        """Accepts one or more keys to get from a QuestionSetData object.

        This method will return the first matching values it encounters.
        It would be a good idea to not have fields with the same name in
        different QuestionSets.

        This method only operates on QuestionSets set to allow_multiple=False.
        Use get_list for allow_multiple=True.
        """
        for qs_id, qsd_list in self.question_set_data.items():
            # Only search for values in singular question sets
            if not self.question_sets[qs_id].allow_multiple:
                result, has_value = self._get_value(qsd_list[0], key)
                if has_value:
                    return result

        if default == self._null_result:
            return self._null_result(key)
        else:
            return default

    @tracer.wrap()
    def get_single(self, *keys):
        return self.get(keys, default=self._null_result)

    @tracer.wrap()
    def get_list(self, *keys):
        """Accepts one or more keys to get from a group of QuestionSetData
        objects related by the same QuestionSet.

        This method will return the first matching values it encounters.
        It would be a good idea to not have fields with the same name in
        different QuestionSets.

        This method only operates on QuestionSets set to allow_multiple=True.
        Use get() for allow_multiple=False.
        """
        for qs_id, qsd_list in self.question_set_data.items():
            # Only search for values in multiplicative question sets
            if self.question_sets[qs_id].allow_multiple:
                results = []
                for qsd in qsd_list:
                    result, has_value = self._get_value(qsd, keys)
                    if has_value:
                        results.append(result)
                if results:
                    return results

        return [self._null_result(keys)]


class ConfigurableSignalAction(SignalAction):
    """This type of SignalAction is configurable through QuestionSets."""
    def __init__(self, feature_config, analysis_config, *args, **kwargs):
        """Override this __init__ to query for QuestionSetData"""
        super(ConfigurableSignalAction, self).__init__(*args, **kwargs)
        self.feature_config = feature_config
        self.config_wrapper = ConfigurationWrapper(analysis_config,
                                                   feature_config)
        self.persona_config_wrapper = PersonaConfigurationWrapper(
            analysis_config, feature_config)

    def get_setting(self, name, default=None):
        """Helper to try getting a setting value first from SignalActionConfig
        definition, and then from QuestionSetData if not present in the SAC
        definition.

        Assumes the setting key is the same in both data sources.
        """
        if name in self.definition:
            value = self.definition.get(name, default)
        else:
            value = self.config_wrapper.get(name, default)
        return value

    @abstractmethod
    def run(self, pipeline, partition_config=None, *args, **kwargs):
        pass



# These classes have been reorganized into sub-files. In order to preserve
# legacy functionality, these classes need to be imported here, since
# references to these classes are stored in the data base at this location.
#
# Future SignalActions should not be imported here, but rather should be
# referred to at their new location.
from .signal_actions import (
    RecordSearch,
    PartitionSignals,
    AddMultiplier,
    ReduceSignals,
    SubsetFilter,
    FieldCopy
)
