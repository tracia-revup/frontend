from collections import defaultdict
from funcy import get_in
import logging
from itertools import chain, takewhile

from django.conf import settings
from geopy.distance import great_circle

from frontend.apps.analysis.exceptions import SkipContact
from frontend.apps.analysis.analyses.base import (
    SignalAction, ConfigurableSignalAction)
from frontend.apps.campaign.models.base import Account
from frontend.apps.core.geo_location import GeoPyLocator
from frontend.libs.entity_api import EntityResolver
from frontend.libs.utils.general_utils import public


LOGGER = logging.getLogger(__name__)


@public
class RecordSearch(SignalAction):

    def __init__(self, record_set_config_id, record_set_backends,
                 feature_config, *args, **kwargs):
        super(RecordSearch, self).__init__(*args, **kwargs)
        self.target_type = self.definition['target_type']
        self.id_field_name = self.definition['id_field_name']
        self.search_method_name = self.definition.get(
            'search_method_name', 'search')
        record_set = record_set_backends.get(record_set_config_id)
        if not record_set:
            raise AttributeError(
                "A record set is required for the RecordSearch signal action. "
                "Feature: {} ({}). Make sure the record set is defined in "
                "both the SignalSetConfig and the DataSetConfig".format(
                    feature_config, feature_config.id))

        self.record_set_config_id = record_set_config_id
        self.record_set = record_set

    @property
    def search_method(self):
        return getattr(self.record_set, self.search_method_name)

    def run(self, pipeline, *args, **kwargs):

        def search():
            pipeline.key_signals.setdefault('expansion_hit', False)
            for record in self.search_method(pipeline.target):
                signal = self.build_signal()
                if 'given_name' in record:
                    # expansion_hit is used in debugging to determine whether a
                    # contribution record was discovered as a result of name or
                    # alias expansion.
                    expansion_hit = not record['given_name'].upper().startswith(
                        pipeline.target.first_name.upper())
                    pipeline.key_signals['expansion_hit'] |= expansion_hit
                    if expansion_hit:
                        signal["expansion_hit"] = record[self.id_field_name]
                signal["match"] = record[self.id_field_name]
                # Store the record set config for comparison down the pipeline
                signal["record_set_config"] = self.record_set_config_id
                yield record, signal

        pipeline.records_signals = search()
        return pipeline


@public
class PersonEntityResolver(SignalAction):
    """Resolve Person Entities

    Passes person entities to `EntityResolver`, which is then attached to the
    contact wrapper on the property `entity_resolver`. For backwards
    compatibility it is also added as the property
    `entity_contribution_id_mapping`
    """
    def __init__(self, analysis_config, **kwargs):
        super().__init__(**kwargs)
        self.account_key = analysis_config.account.account_key
        self.entity_types = self.definition.get('entity_types', [])
        self.fused_field_mapping = self.definition.get(
            'fused_field_mapping', {})
        self.collection_mapping = self.definition.get('collection_mapping', {})

    def run(self, pipeline, *args, **kwargs):
        pipeline.records_signals = list(pipeline.records_signals)
        person_entities = (record for record, _ in pipeline.records_signals)
        er = EntityResolver.factory(person_entities, settings.MONGO_DB,
                                    account_key=self.account_key)

        contact = pipeline.target
        # Add the entity resolver to the contact wrapper for use in other parts
        # of analysis
        contact.entity_resolver = er
        # This alias is for backwards compatibility
        contact.entity_contribution_id_mapping = er
        return pipeline


@public
class PersonEntityResolveContributionIds(SignalAction):
    """PersonEntityResolveContributionIds resolves contribution ids from Person
    entities. Discovered contribution IDS are stashed on the contact wrapper in
    the attribute `entity_contribution_id_mapping`. The structure of
    `entity_contribution_id_mapping` is:

        {<entity_type>: [<contrib_id>, ...],
         ...}

    Where entity_type is a value in the configuration variable `entity_types`.
    By default we support fec, municipal, misp, non_profit, real_estate and
    client_data. Support for entities for different sources can be added by
    updating the config var `entity_types`.

    The structure of entities currently is:
    Person Entity:
        node_quality:
            <score>: [<fused_entity_id>, ...]

    (score is an integer 1 - 5. higher the value the better the match)

    Fused Entity:
        fec_id: [<fec_entity_id>]
        misp_id: [<misp_entity_id>]
        municipal_id: [<muni_entity_id>]

    Municipal Entity:
        record_links: [
            <muni_contrib_id>,
            ...
        ]

    MISP Entity:
        record_links: [
            <misp_contrib_id>,
            ...
        ]

    FEC Entity:
        record_links: [
            <fec_contrib_id>,
            ...
        ]


    This signal action iterates over all Person entity records discovered in a
    previous step, and picks out all fused entity ids with a score at least as
    high as the configuration variable `min_score` (default 3).

    The configuration setting `fused_field_mapping` can be used to override the
    field we look in the Fused entity for an entity_type's entity id. For
    example the below config would cause us to look at `alt_fec_id` in the
    Fused Entity for the `fec` entity type rather than `fec_id`:

        {'fec': 'alt_fec_id'}

    The configuration setting `collection_mapping` is for overriding the name
    of a collection we look for a specific entity_type's entity record. By
    default we look in a collection named `<entity_type>_entities`.
    """
    def __init__(self, record_set_config_id, record_set_backends,
                 feature_config, *args, **kwargs):
        super(PersonEntityResolveContributionIds,
              self).__init__(*args, **kwargs)
        record_set = record_set_backends.get(record_set_config_id)
        if not record_set:
            raise AttributeError(
                "A record set is required for the {} signal action. "
                "Feature: {} ({}). Make sure the record set is defined in "
                "both the SignalSetConfig and the DataSetConfig".format(
                    self.__class__.__name__, feature_config,
                    feature_config.id))

        self.record_set_config_id = record_set_config_id
        self.record_set = record_set
        self.min_score = self.definition.get('min_score', 0)
        self.max_score = self.definition.get('max_score', 6)
        self.entity_types = self.definition.get(
            'entity_types', ['fec', 'misp', 'municipal', 'non_profit',
                             'real_estate', 'client_data', 'sec', 'opencorp'])

        self.fused_field_mapping = self.definition.get(
            'fused_field_mapping',
            {'fec': 'fec_daily_id', 'client_data': 'client_data'})

        self.account_id = self.record_set.analysis_config.account_id
        self.account = Account.objects.get(id=self.account_id)
        self.client_data_key = self.definition.get(
            'client_data_key', self.account.account_key)
        self.collection_mapping = self.get_collection_mapping()

    def get_collection_mapping(self):
        """Returns a map of entity type and corresponding collection name"""
        collection_mapping = self.definition.get('collection_mapping', {})
        if 'client_data' not in collection_mapping:
            collection_mapping.update(
                {'client_data': self.account.client_entity_collection_name})
        return collection_mapping

    def run(self, pipeline, *args, **kwargs):
        pipeline.records_signals = list(pipeline.records_signals)

        fused_entity_ids = set()

        score_keys = list(map(str, takewhile(lambda x: x >= self.min_score,
                                             reversed(range(self.max_score+1)))))

        # Iterate over each Person entity found, and if there are scored
        # entities found (which are fused_entities), then collect those ids.
        for person, _ in pipeline.records_signals:
            if 'node_quality' in person:
                for score_key in score_keys:
                    fused_entity_ids.update(
                        person['node_quality'].get(score_key, []))

        LOGGER.debug("Fused entity ids found: %s", fused_entity_ids)

        source_specific_entity_ids = {}
        # Query database for all discovered fused entities we found.
        fused_entities = self.record_set.fused_entities(fused_entity_ids)
        for fused_entity in fused_entities:
            # Within each fused_entity will be ids to another entity, one per
            # contribution source. All supported sources are listed in
            # `fused_field_collection_mapping`, which maps the field name
            # containing the id on the fused entity with the name of the
            # collection where the source-specific entity can be found. We
            # collect every source-specific entity id across potentially
            # multiple fused entities so we can get all those source entities
            # in a single query.

            # The ids are nested for client data entities
            # ......
            # 'client_data': {
            #                   "account-key-1" ; {_id:[], num_recs=0}
            #                   "account-key-2" ; {_id:[], num_recs=0}
            #                }
            # .......
            for entity_type in self.entity_types:
                source_id_field = self.fused_field_mapping.get(
                    entity_type, '{}_id'.format(entity_type))

                if entity_type == 'client_data':
                    source_id = get_in(
                        fused_entity,
                        [source_id_field, self.client_data_key, '_id'])
                else:
                    source_id = fused_entity.get(source_id_field)
                if source_id:
                    sse_ids = source_specific_entity_ids.setdefault(
                        entity_type, set())
                    if isinstance(source_id, str):
                        sse_ids.add(source_id)
                    else:
                        sse_ids.update(source_id)

        LOGGER.debug("Source specific entity ids found: %s",
                     source_specific_entity_ids)
        contribution_id_mapping = {}
        # Iterate over each collection of source entities, and query the
        # corresponding mongodb collection for those entity records.
        for entity_type, source_entity_ids in source_specific_entity_ids.items():
            collection_name = self.collection_mapping.get(
                entity_type, '{}_entities'.format(entity_type))

            source_entities = self.record_set.get_source_entities(
                collection_name, source_entity_ids)
            contribution_ids = contribution_id_mapping.setdefault(
                entity_type, set())
            # Iterate over the source-specific entity records we got from the
            # database, and extract the contribution ids found within them.
            for source_entity in source_entities:
                # As mentioned in the class docstring, `record_links` is a list
                # of tuples. Each tuple contains the date of the contribution,
                # the contribution id, and the amount of the contribution.
                # Since we get contrib date and contrib amount from the contrib
                # record itself, we throw those values away.
                for contribution_id in source_entity['record_links']:
                    contribution_ids.add(contribution_id)

        LOGGER.debug("Contribution ids found: %s", contribution_id_mapping)
        contact = pipeline.target
        # Add the contribution ids we discovered from entities to the contact
        # for use in querying later.
        contact.entity_contribution_id_mapping = contribution_id_mapping
        return pipeline


@public
class PurdueLookupPastAddresses(SignalAction):
    """Lookup past addresses from Purdue records for a alumni.

    Stash past addresses in signal for later processing.
    """
    def __init__(self, record_set_config_id, record_set_backends,
                 *args, **kwargs):
        super(PurdueLookupPastAddresses, self).__init__(*args, **kwargs)
        self.record_set = record_set_backends.get(record_set_config_id)

    def run(self, pipeline, *args, **kwargs):
        def _search(records_signals):
            for alum, signal in records_signals:
                signal['past_addresses'] = list(
                    self.record_set.past_addresses(alum))
                yield alum, signal

        pipeline.records_signals = _search(pipeline.records_signals)
        return pipeline


@public
class PurdueLookupLatLong(SignalAction):
    """Lookup latitude and longitude for alumni zip code.

    Stash on signal for further processing later on.
    """
    def __init__(self, *args, **kwargs):
        super(PurdueLookupLatLong, self).__init__(*args, **kwargs)
        self._locator = GeoPyLocator()

    def run(self, pipeline, *args, **kwargs):
        def _search(records_signals):
            for alum, signal in records_signals:
                zip = alum.zip
                if not zip:
                    continue

                # Get the location for the given zip
                zloc_id, zloc = self._locator.get_geo_names_location(zip)
                if not zloc:
                    continue

                # Get the lat and lng for the given zip
                zlat = zloc.get("lat")
                zlng = zloc.get("lng")
                if not (zlat and zlng):
                    continue

                signal['lat_lng'] = zlat, zlng
                signal['lat'] = zlat
                signal['lng'] = zlng

                yield alum, signal

        pipeline.records_signals = _search(pipeline.records_signals)
        return pipeline


@public
class PurdueAlumFilter(SignalAction):
    """Use imported contact information to narrow Purdue alumni records down to
    one record.

    If we are unable to match any imported contact information with any of the
    alumni information, stop all further processing on the contact.

    Note: the logic in this signal action is ripped directly from the legacy
    purdue analysis.
    """
    def __init__(self, record_set_config_id, record_set_backends,
                 *args, **kwargs):
        super(PurdueAlumFilter, self).__init__(*args, **kwargs)
        self.record_set = record_set_backends.get(record_set_config_id)
        self.min_distance_cutoff = self.definition.get('min_distance_cutoff',
                                                       50)
        self.min_zip_overlap_cutoff = self.definition.get(
            'min_zip_overlap_cutoff', 2)

    @classmethod
    def _len_common_zip(cls, s1s, s2s):
        """This method was ripped directly from the legacy purdue analysis.

        Not really sure what it is doing. All I know for sure is that it is used
        to find zip code overlap.
        """
        s2longest = 0
        for s1 in s1s:
            for s2 in s2s:
                m = [0] * (1 + len(s2))
                longest = 0
                length = min(len(s1), len(s2))
                for x in range(1, 1 + length):
                    if s1[x - 1] == s2[x - 1]:
                        m[x] = m[x - 1] + 1
                        if m[x] > longest:
                            longest = m[x]
                    else:
                        m[x] = 0
                        break
                if longest > s2longest:
                    s2longest = longest
        return s2longest

    def run(self, pipeline, *args, **kwargs):
        def _alum_get_zips(alum, signal):
            zips = set([alum.zip])
            for past_address in signal.get('past_addresses', []):
                zips.add(past_address.zip)
            return list(zips)

        def _filter(records_signals):
            found_alum = False
            contact = pipeline.target
            contact_lat_lngs = contact.lat_lngs
            contact_emails = set(e.lower() for e in contact.emails)

            max_zip_overlap_count = 0
            max_zip_overlap_alum = None

            min_distance_alum = None
            min_distance = None

            for alum, signal in records_signals:
                if found_alum:
                    # First alumni that we find a match against contact
                    # information wins!
                    break

                # Check the alumni email address against contact email
                # addresses.
                if alum.email in contact_emails:
                    found_alum = True
                    yield alum, signal
                    break

                # Check alumni phone numbers against contact phone numbers.
                contact_phones = contact.phones
                if contact_phones:
                    alum_phones = chain(
                        alum.phones,
                        (addy.phone for addy in signal['past_addresses']))
                    for alum_phone in alum_phones:
                        if alum_phone in contact_phones:
                            found_alum = True
                            yield alum, signal
                            break

                # Calculate zip code overlap. If there is overlap, remember the
                # alumni with the greatest zip overlap with our contact. This
                # is used after we finish iterating through all alumni records
                # found, provided we don't make a phone number or email match
                # before hand.
                zip_overlap_count = self._len_common_zip(
                    _alum_get_zips(alum, signal), contact.zips)
                if zip_overlap_count > max_zip_overlap_count:
                    max_zip_overlap_count = zip_overlap_count
                    max_zip_overlap_alum = (alum, signal)

                # Get the distance between location and zip. If the distance
                # between any contact location and alumni zip is less than the
                # distance cutoff, remember the alumni with the least distance
                # between them and the contact location.
                if 'lat_lng' in signal and contact_lat_lngs:
                    zlat, zlng = signal['lat_lng']
                    for lat, lng in contact_lat_lngs:
                        distance = great_circle((lat, lng), (zlat, zlng)).miles
                        if distance < self.min_distance_cutoff:
                            if min_distance is None or distance < min_distance:
                                min_distance = distance
                                min_distance_alum = (alum, signal)

            # If we have not yet found an alumni that matches the contact
            # information we have, check to see if the max zip overlap is
            # greater than the zip overlap cutoff (default of 2), use that
            # alumni record.
            if not found_alum and \
               max_zip_overlap_count > self.min_zip_overlap_cutoff:
                found_alum = True
                yield max_zip_overlap_alum

            # If we have not yet found an alumni that matches the contact
            # information we have, and we have a alumni whose zip is less than
            # the min distance cutoff (50 miles) away from a contact location,
            # use that alumni record.
            if not found_alum and min_distance_alum:
                found_alum = True
                yield min_distance_alum

            # If none of the match conditions are met, raise SkipContact to
            # tell the runner to do no further processing on the contact.
            if not found_alum:
                raise SkipContact

        pipeline.records_signals = _filter(pipeline.records_signals)
        return pipeline


@public
class SubsetFilter(SignalAction):

    def __init__(self, record_set_config_id, *args, **kwargs):
        super(SubsetFilter, self).__init__(*args, **kwargs)
        self.record_set_config_id = record_set_config_id
        self.id_field_name = self.definition['id_field_name']
        self.filter_field_name = self.definition['filter_field_name']
        self.values = set(self.definition['values'])

    def run(self, pipeline, cached_record_signals, *args, **kwargs):

        def _filter(record_signals):
            for (record, signal) in record_signals:
                if 'grouped' in signal:
                    for (sub_record, sub_signal) in signal['grouped']:
                        if sub_record[self.filter_field_name] in self.values:
                            yield sub_record, sub_signal.copy()
                else:
                    if record[self.filter_field_name] in self.values:
                        yield record, signal.copy()

        # Only look at this SignalAction's signal set data
        pipeline.records_signals = _filter(
            cached_record_signals.get(self.record_set_config_id, []))
        return pipeline


@public
class FieldUniqueCountFilter(SignalAction):

    def __init__(self, *args, **kwargs):
        super(FieldUniqueCountFilter, self).__init__(*args, **kwargs)
        self.field = self.definition['field']
        self.maximum = self.definition['maximum']
        default_max_length = 5
        try:
            self.max_length = int(self.definition.get('max_length',
                                                      default_max_length))
        except ValueError:
            self.max_length = default_max_length

    def run(self, pipeline, *args, **kwargs):
        records_signals = list(pipeline.records_signals)
        if not (pipeline.target.states or pipeline.target.zip3s):
            values = {record.get(self.field, '')[:self.max_length]
                      for (record, signal) in records_signals}
            count = len(values)
            if count > self.maximum:
                LOGGER.debug("%s unique values found for field %s, "
                             "dropping matches", count, self.field)
                records_signals = []
        pipeline.records_signals = records_signals
        return pipeline


@public
class TargetFieldValueFilter(SignalAction):

    def __init__(self, *args, **kwargs):
        super(TargetFieldValueFilter, self).__init__(*args, **kwargs)
        self.target_field = self.definition['target_field']
        self.record_field = self.definition['record_field']
        self.additional_values = self.definition['additional_values']

    def run(self, pipeline, *args, **kwargs):

        if getattr(pipeline.target, self.target_field):
            target_values = list(getattr(pipeline.target, self.target_field))
            for additional_value in self.additional_values:
                target_values.append(additional_value)
            if target_values:
                pipeline.records_signals = (
                    (record, signal)
                    for (record, signal) in pipeline.records_signals
                    if record[self.record_field] in target_values)
        return pipeline


@public
class Zip3OrStateFilter(SignalAction):
    """
    If the target has 3-digit zip prefixes available, use those to filter
    matching records. If the target does not have 3-digit zip prefixes, then
    filter by state instead.
    """

    def __init__(self, *args, **kwargs):
        super(Zip3OrStateFilter, self).__init__(*args, **kwargs)
        self.target_state_field = self.definition['target_state_field']
        self.target_zip3s_field = self.definition['target_zip3s_field']
        self.record_zip_field = self.definition['record_zip_field']
        self.record_state_field = self.definition['record_state_field']
        self.additional_values = self.definition['additional_values']

    def run(self, pipeline, *args, **kwargs):

        zip3s = getattr(pipeline.target, self.target_zip3s_field)
        target_states = list(getattr(pipeline.target, self.target_state_field))
        if zip3s:
            pipeline.records_signals = (
                (record, signal)
                for (record, signal) in pipeline.records_signals
                if self.record_zip_field in record
                and record[self.record_zip_field][0:3] in zip3s)
        elif target_states:
            for additional_value in self.additional_values:
                target_states.append(additional_value)
            pipeline.records_signals = (
                (record, signal)
                for (record, signal) in pipeline.records_signals
                if record[self.record_state_field] in target_states)
        return pipeline


@public
class ConfigurableEntityFilter(ConfigurableSignalAction):
    def __init__(self, record_set_backends, *args, **kwargs):
        super(ConfigurableEntityFilter, self).__init__(*args, **kwargs)
        self.record_set_backends = record_set_backends
        self.values = []

        for label, is_ally, entities, categories in \
                self.config_wrapper.get_list(
                    "label", "is_ally", "entities", "categories"):
            # Skip if there are no entities or categories associated with a
            # label
            if not (entities or categories):
                continue

            if entities is None:
                entities = []
            if categories is None:
                categories = []

            # Initialize an empty set for entities and categories
            gids = defaultdict(set)
            eids = gids['eids']
            cids = gids['cids']

            for category in categories:
                try:
                    # Construct the NTEE code and add it to cids (a set of
                    # categories)
                    ntee_code = "{0}{1}".format(category['ntee_level_1'],
                                                category['ntee_level_2'])
                    cids.add(ntee_code)
                except KeyError:
                    continue

            for entity in entities:
                try:
                    rsc_id = int(entity["record_set_config"])
                    eid = entity["eid"]
                    # Get the lookup field, which may or may not match eid
                    # depending on the type of record set.
                    record_set = self.record_set_backends[rsc_id]
                    # Get a set of entity IDs that can be used to compare with
                    # contribution records for matches
                    eids.update(record_set.record_resource.get_match_eids(eid))
                except KeyError:
                    continue

            self.values.append((label, is_ally, gids))

    def has_matched_fields(self, record, gids, record_resource):
        """Matches the records either by `match_field` or `qsd_match_field`
        """
        eids = gids['eids']
        cids = gids['cids']

        # Check if the match_field's value in the record exists in eids.
        # If it exists, consider the record as a match.
        match_field_found = record.get(record_resource.match_field) in eids

        # Check if the qsd_match_field's value in the record exists in
        # cids. If it exists, consider the record as a match.
        qsd_match_field_found = False
        if hasattr(record_resource, 'qsd_match_field'):
            qsd_match_field_value = ''.join([
                record.get(k) for k in record_resource.qsd_match_field])
            qsd_match_field_found = qsd_match_field_value in cids

        return match_field_found or qsd_match_field_found

    def run(self, pipeline, cached_record_signals, *args, **kwargs):
        def apply_tag(ally):
            tag = "ally-giving" if ally else "oppo-giving"
            pipeline.tags.add(tag)

        def _filter():
            for label, is_ally, gids in self.values:
                # We need to examine all records for key contrib matches
                for (rsc_id, record_signals) in cached_record_signals.items():
                    record_set = self.record_set_backends[rsc_id]

                    for (record, signal) in record_signals:
                        if 'grouped' in signal:
                            for (sub_record, sub_signal) in signal['grouped']:
                                if self.has_matched_fields(
                                        sub_record, gids,
                                        record_set.record_resource):
                                    signal_copy = sub_signal.copy()
                                    signal_copy["label"] = label
                                    signal_copy["is_ally"] = is_ally
                                    apply_tag(is_ally)
                                    yield sub_record, signal_copy

                        else:
                            if self.has_matched_fields(
                                    record, gids, record_set.record_resource):
                                signal_copy = signal.copy()
                                signal_copy["label"] = label
                                signal_copy["is_ally"] = is_ally
                                apply_tag(is_ally)
                                yield record, signal_copy

        pipeline.records_signals = _filter()
        return pipeline


@public
class ConfigurableAllyTagger(ConfigurableSignalAction):
    """Identify contributions made to correlated organizations

    This SignalAction does much the same thing as `ConfigurableEntityFilter`,
    except it does not discard contributions if no correlated campaign was
    found. It uses `Key Contributions` QSD as well.

    Contributions made to similar organizations have the signal `is_ally` set
    to True. Contributions made to organizations identified as opponents have
    `is_ally` set to False. If the organization id on a contribution is not
    found in our configuration, `is_ally` is also set to False.
    """
    def __init__(self, record_set_config_id, record_set_backends, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.record_set_backends = record_set_backends
        self.record_set_config_id = record_set_config_id
        self.record_resource = self.record_set_backends[record_set_config_id].record_resource
        self.match_field = self.record_resource.match_field
        self.qsd_match_field = self.record_resource.qsd_match_field \
            if hasattr(self.record_resource, 'qsd_match_field') else []
        self.gid_ally_map = {True: defaultdict(set),
                             False: defaultdict(set)}
        self.reverse_gid_ally_map = {}

        for is_ally, entities, categories in self.config_wrapper.get_list(
                "is_ally", "entities", "categories"):
            if not (entities or categories):
                continue

            if entities is None:
                entities = []
            if categories is None:
                categories = []

            gids = self.gid_ally_map[is_ally]
            eids = gids['eids']
            cids = gids['cids']

            for entity in entities:
                rsc_id = int(entity["record_set_config"])
                eid = entity["eid"]
                record_set = self.record_set_backends[rsc_id]
                eids.update(record_set.record_resource.get_match_eids(eid))

            for category in categories:
                try:
                    # Construct the NTEE code and add it to cids (a set of
                    # categories)
                    ntee_code = "{0}{1}".format(category['ntee_level_1'],
                                                category['ntee_level_2'])
                    cids.add(ntee_code)
                except KeyError:
                    continue

        for is_ally in [False, True]:
            for eid in self.gid_ally_map[is_ally]['eids']:
                self.reverse_gid_ally_map[eid] = is_ally

            for cid in self.gid_ally_map[is_ally]['cids']:
                self.reverse_gid_ally_map[cid] = is_ally

    def check_if_ally(self, record):
        """Checks if the record should be considered as an ally either by
        `match_field` or `qsd_match_field`
        """
        # Check if the record's match field value is present in ally map
        match_field_is_ally = self.reverse_gid_ally_map.get(
            record.get(self.match_field), False)

        # Check if the record's qsd match field value is present in ally map
        qsd_match_field_value = "".join([record.get(v)
                                         for v in self.qsd_match_field])
        qsd_field_is_ally = self.reverse_gid_ally_map.get(
            qsd_match_field_value, False)

        return match_field_is_ally or qsd_field_is_ally

    def run(self, pipeline, *args, **kwargs):
        def _tag(records_signals):
            for record, signal in records_signals:
                signal["is_ally"] = self.check_if_ally(record)
                yield record, signal
        pipeline.records_signals = _tag(pipeline.records_signals)
        return pipeline
