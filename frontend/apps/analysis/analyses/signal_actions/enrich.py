import datetime
import logging
from math import ceil

from frontend.apps.analysis.analyses.base import (
    SignalAction, ConfigurableSignalAction)
from frontend.libs.utils import election_utils
from frontend.libs.utils.general_utils import public


LOGGER = logging.getLogger(__name__)


@public
class NonProfitEnrich(SignalAction):
    """Enriches the nonprofit data by adding the following info to the signal.
    - years_passed since a donation was made is added as a key and the amount
    donated is added as its value
    The data will be used in ReduceSignals SignalAction for reduce operations
    such as sum, median etc.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.tomorrow = datetime.date.today() + datetime.timedelta(days=1)

    def run(self, pipeline, *args, **kwargs):
        def _non_profit_enrich(records_signals):
            for (record, signal) in records_signals:
                record_date = record.get('date')
                # Calculate the number of years passed since the contribution.
                # Tomorrow is being used to include contributions made on
                # today's date. This would set years_passed to >= 1
                if record_date:
                    years_passed = ceil((self.tomorrow - record_date).days / 365)
                    key = f'year_{years_passed}'
                else:
                    key = 'year_none'
                # Map the contributions made in a year to the
                # 'year_{years_passed}' key
                signal[key] = record.get('amount', 0)
                yield record, signal

        pipeline.records_signals = _non_profit_enrich(pipeline.records_signals)
        return pipeline

@public
class FECEnrich(SignalAction):

    def __init__(self, record_set_config_id, record_set_backends,
                 *args, **kwargs):
        super(FECEnrich, self).__init__(*args, **kwargs)
        self.apply_overrides = self.definition['apply_overrides']
        self.record_set = record_set_backends.get(record_set_config_id)

    def run(self, pipeline, *args, **kwargs):

        def _fec_enrich(records_signals):

            records = [record for (record, signal) in records_signals]
            committees_candidates = self.record_set.get_committees_candidates(
                records)
            for (record, signal) in records_signals:
                committee_id = record['CMTE_ID']
                (committee, candidate) = committees_candidates.get(
                    committee_id)
                if candidate:
                    signal['office'] = candidate['CAND_OFFICE']
                    signal['status'] = candidate['CAND_ICI']
                signal['party'] = record.get('party')
                if 'override' in record:
                    signal['override'] = record['override']
                    if self.apply_overrides:
                        signal['party'] = signal['override']
                yield record, signal

        pipeline.records_signals = _fec_enrich(list(pipeline.records_signals))
        return pipeline


@public
class MISPEnrich(SignalAction):

    def __init__(self, *args, **kwargs):
        super(MISPEnrich, self).__init__(*args, **kwargs)
        self.apply_overrides = self.definition['apply_overrides']

    def run(self, pipeline, *args, **kwargs):

        def _misp_enrich(records_signals):

            for (record, signal) in records_signals:
                signal['party'] = record.get('party')
                signal['office'] = record.get('General_Office:id')
                if 'override' in record:
                    signal['override'] = record['override']
                    if self.apply_overrides:
                        signal['party'] = signal['override']
                yield record, signal

        pipeline.records_signals = _misp_enrich(pipeline.records_signals)
        return pipeline


@public
class FECMarkSelfFunded(SignalAction):

    def run(self, pipeline, *args, **kwargs):

        def _self_funded(records_signals):
            for (record, signal) in records_signals:
                if record['TRANSACTION_TP'] == "15C":
                    signal['is_self_funded'] = True
                    LOGGER.debug("Will not use this contribution in score \
                            calculation as its self-funded")
                else:
                    signal['is_self_funded'] = False
                yield record, signal

        pipeline.records_signals = _self_funded(pipeline.records_signals)
        return pipeline


@public
class MISPMarkSelfFunded(SignalAction):

    def run(self, pipeline, *args, **kwargs):

        def _self_funded(records_signals):
            for (record, signal) in records_signals:
                # These three errors should(tm) never be possible
                # Critical error should one occur
                # We'll return false as only when they're as in the original
                # logic are they self funded according to FTM
                if 'ElectionEntityEID' not in record:
                    LOGGER.error("missing ElectionEntityEID")

                if 'CFS_EID' not in record:
                    LOGGER.error("missing CFS_EID")

                if 'ElectionEntityEID' in record and 'CFS_EID' in record:
                    if (record['ElectionEntityEID'] == record['CFS_EID'] or
                            record['CatCodeIndustryID'] == "133"):
                        signal['is_self_funded'] = True
                        LOGGER.debug("Will not use this contribution in score \
                                calculation as its self-funded")
                    else:
                        signal['is_self_funded'] = False
                else:
                    signal['is_self_funded'] = False
                yield record, signal

        pipeline.records_signals = _self_funded(pipeline.records_signals)
        return pipeline


@public
class ClientCalculateGivingAvailability(ConfigurableSignalAction):
    """Calculate a contacts giving availability, taking into account how
       much they have already given, the client's position, the year, etc.
    """
    def __init__(self, *args, **kwargs):
        super(ClientCalculateGivingAvailability, self).__init__(*args, **kwargs)
        default_limit = election_utils.contribution_limit(
                datetime.date.today())
        limit = self.get_setting('limit', default_limit)
        if not limit:
            limit = default_limit

        # Cast to an int, but it may also be "unlimited"
        try:
            self.limit = int(limit)
        except ValueError:
            self.limit = limit if limit == "unlimited" else default_limit

    def run(self, pipeline, *args, **kwargs):
        period_giving = pipeline.results.setdefault("giving_this_period", 0)
        past_giving = pipeline.results.setdefault("giving", 0)
        if period_giving and self.limit != 'unlimited':
            # If 'available' is negative, we'll make it 0
            available = max((self.limit - period_giving), 0)
        else:
            available = self.limit

        year_of_last = pipeline.results.get('year_of_last_contribution')
        pipeline.results["giving_availability"] = available
        pipeline.key_signals["giving_availability"] = available
        pipeline.key_signals["giving_this_period"] = period_giving
        pipeline.key_signals['year_of_last_contribution'] = year_of_last

        # Add the relevant tags
        if available != 'unlimited' and available <= 0:
            pipeline.tags.add("client-maxed")
        if period_giving > 0:
            pipeline.tags.add("client-giving-current")
        if (past_giving - period_giving) > 0:
            pipeline.tags.add("client-giving-past")
        return pipeline


@public
class AddFields(SignalAction):
    """Add fields and values specified by definition to analysis signals.
       For example, add a 'party' field and value to signals from a record set
       not having a party field, e.g. client contributions.
    """
    def __init__(self, *args, **kwargs):
        super(AddFields, self).__init__(*args, **kwargs)
        self.fields = self.definition['fields']

    def run(self, pipeline, *args, **kwargs):

        def _copy_fields(records_signals):
            for (record, signal) in records_signals:
                for field in self.fields:
                    field_name = field['field_name']
                    field_value = field['field_value']
                    signal[field_name] = field_value
                yield record, signal

        pipeline.records_signals = _copy_fields(pipeline.records_signals)
        return pipeline


@public
class ContribWithinNYears(SignalAction):
    """Calculates the number of years it has been since a contribution was made
    and adds a signal.

    We are trying to answer the question "which contributions were made within
    the last N years?". The value is rounded up to the nearest int, so a
    contribution made the previous day will have a value of 1.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.today = datetime.date.today()

    def run(self, pipeline, *args, **kwargs):
        def _idempotent_date(date):
            if isinstance(date, datetime.datetime):
                return date.date()
            return date

        def _calc(records_signals):
            for record, signal in records_signals:
                within_n_years = ceil(
                    (self.today - _idempotent_date(record.date)).days / 365)
                signal['within_n_years'] = within_n_years
                yield record, signal
        pipeline.records_signals = _calc(pipeline.records_signals)
        return pipeline
