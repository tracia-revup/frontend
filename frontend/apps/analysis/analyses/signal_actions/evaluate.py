from collections import defaultdict, Mapping, Container
import datetime
from funcy import get_in, lcat
import logging
import math
from os import environ
import re
from statistics import median

if 'REVUP_IN_UWSGI' not in environ:
    from scipy import stats
else:
    stats = None

from frontend.apps.analysis.analyses.base import (
    SignalAction, SummarySignalActionMixin, ConfigurableSignalAction)
from frontend.apps.analysis.utils import get_analysis_score_min_max
from frontend.libs.utils import election_utils, string_utils
from frontend.libs.utils.csv_utils import csv_join, csv_split
from frontend.libs.utils.general_utils import public


LOGGER = logging.getLogger(__name__)


def _decay_amount_before_cutoff(amount, date, cutoff, years):
    if date < cutoff:
        time_delta = float(cutoff.toordinal() - date.toordinal())
        scale = float(int(years) * 365)
        amount *= math.exp(-time_delta / scale)

    return amount


@public
class SimpleCalculateGiving(SignalAction):

    def run(self, pipeline, partition_config=None, *args, **kwargs):

        def _determine_giving(records_signals):
            for (record, signal) in records_signals:
                partition_signal = signal['partitions'][partition_config.id]
                try:
                    amount = int(signal.get('trans_amt', 0))
                except (TypeError, ValueError):
                    LOGGER.error('invalid trans_amt in SimpleCalculateGiving')
                    LOGGER.warning('record: %s', record)
                    LOGGER.warning('signal: %s', signal)
                    amount = 0
                if partition_signal['in_partition']:
                    partition_signal['giving'] = amount
                    # #TODO: generalize
                else:
                    partition_signal['giving'] = int(0)
                yield record, signal

        pipeline.records_signals = _determine_giving(pipeline.records_signals)
        return pipeline


@public
class ClientCalculateGiving(ConfigurableSignalAction):
    """Extract the dollar amounts for each contribution to the client."""

    def __init__(self, *args, **kwargs):
        super(ClientCalculateGiving, self).__init__(*args, **kwargs)
        start = self.get_setting('cycle_start')
        office = self.get_setting("office")
        office_category = self.get_setting("office_category")
        if start:
            try:
                start = datetime.datetime.strptime(start, "%Y-%m-%d").date()
            except ValueError:
                start = None

        if start:
            # manually defined cycle_start is given preference over determining
            # the start of the cycle based on office.
            end = datetime.date.today()
        elif office and office_category == 'Federal':
            state = self.get_setting("state")
            # Use election cycle if defined
            election_cycle = self.get_setting("election")
            if election_cycle:
                try:
                    election_cycle = datetime.datetime.strptime(
                        election_cycle, "%Y-%m-%d").date()
                except ValueError:
                    election_cycle = None

            if not election_cycle:
                election_cycle = datetime.date.today()
            (start, end) = election_utils.election_period_for_office(
                office, election_cycle, state)
        else:
            # if neither office nor cycle start date is defined, go back 2
            # years by default.
            end = datetime.date.today()
            start = election_utils.election_date_for_year(end.year - 2)

        self.start = start
        self.end = end

    def run(self, pipeline, *args, **kwargs):
        start = self.start
        end = self.end

        def _determine_giving(records_signals):
            for (record, signal) in records_signals:
                try:
                    amount = int(record.get('amount', 0))
                except (TypeError, ValueError):
                    LOGGER.error('invalid amount in ClientCalculateGiving')
                    LOGGER.warning('record: %s', record)
                    LOGGER.warning('signal: %s', signal)
                    amount = 0
                signal['giving'] = amount

                # If the record is from this election period, we want to sum
                # up those contributions
                contrib_date = record.get("date")

                if isinstance(contrib_date, str):
                    contrib_date = datetime.datetime.strptime(
                        str(contrib_date), "%Y-%m-%dT%H:%M:%S.000Z")

                signal["year_of_last_contribution"] = contrib_date.year
                if contrib_date and start <= contrib_date.date() <= end:
                    period_amount = amount
                else:
                    period_amount = 0
                signal["giving_this_period"] = period_amount
                yield record, signal

        pipeline.records_signals = _determine_giving(pipeline.records_signals)
        return pipeline


@public
class FECCalculateGiving(SignalAction):

    def run(self, pipeline, partition_config=None, *args, **kwargs):

        def _determine_giving(records_signals):
            for (record, signal) in records_signals:
                partition_signal = signal['partitions'][partition_config.id]
                amount = int(signal['trans_amt'])
                party = signal.get('party', 'OTHER')
                years = int(partition_config.years_before)
                party_giving = None
                if not signal['is_self_funded']:
                    base_giving = _decay_amount_before_cutoff(
                        amount, signal['trans_date'],
                        partition_config.start_date, years)
                    partition_signal['base_giving'] = base_giving
                    # TODO: generalize
                    if party == 'DEM':
                        partition_signal['net_democrat'] = base_giving
                        partition_signal['democrat_count'] = 1
                        party_giving = "democrat_giving"
                    elif party == 'REP':
                        partition_signal['net_republican'] = base_giving
                        partition_signal['republican_count'] = 1
                        party_giving = "republican_giving"
                    else:
                        partition_signal['other_count'] = 1
                        party_giving = "other_giving"

                if partition_signal['in_partition']:
                    partition_signal['giving'] = amount
                    if party_giving:
                        partition_signal[party_giving] = amount
                    # #TODO: generalize
                else:
                    partition_signal['giving'] = 0
                yield record, signal

        pipeline.records_signals = _determine_giving(pipeline.records_signals)
        return pipeline


@public
class MISPCalculateGiving(SignalAction):

    def run(self, pipeline, partition_config=None, *args, **kwargs):

        # Basic scoring
        def _process_giving(records_signals):
            years = int(partition_config.years_before)
            for (record, signal) in records_signals:
                partition_signal = signal['partitions'][partition_config.id]
                if 'grouped' in partition_signal:
                    partition_signal['giving'] = 0
                    partition_signal['base_giving'] = 0
                    partition_signal['net_democrat'] = 0
                    partition_signal['net_republican'] = 0

                    for (sub_record,
                         sub_signal) in partition_signal['grouped']:
                        amount = int(sub_signal['trans_amt'])
                        party = sub_signal.get('party', 'OTHER')
                        if not sub_signal['is_self_funded']:
                            base_giving = _decay_amount_before_cutoff(
                                amount, sub_signal['trans_date'],
                                partition_config.start_date, years)
                            partition_signal['base_giving'] += base_giving
                            # TODO: generalize
                            if party == 'DEM':
                                partition_signal['net_democrat'] += base_giving
                                party_giving = "democrat_giving"
                            elif party == 'REP':
                                partition_signal[
                                    'net_republican'] += base_giving
                                party_giving = "republican_giving"
                            else:
                                party_giving = "other_giving"

                            if sub_signal['in_partition']:
                                existing = partition_signal.setdefault(
                                    party_giving, 0)
                                partition_signal[party_giving] = \
                                    existing + amount

                        if sub_signal['in_partition']:
                            partition_signal['giving'] += amount
                    if partition_signal['net_democrat']:
                        partition_signal['democrat_count'] = 1
                    elif partition_signal['net_republican']:
                        partition_signal['republican_count'] = 1
                else:
                    amount = int(signal['trans_amt'])
                    party = signal.get('party', 'OTHER')
                    party_giving = None
                    if not signal['is_self_funded']:
                        base_giving = _decay_amount_before_cutoff(
                            amount, signal['trans_date'],
                            partition_config.start_date, years)
                        partition_signal['base_giving'] = base_giving
                        # TODO: generalize
                        if party == 'DEM':
                            partition_signal['net_democrat'] = base_giving
                            partition_signal['democrat_count'] = 1
                            party_giving = "democrat_giving"
                        elif party == 'REP':
                            partition_signal['net_republican'] = base_giving
                            partition_signal['republican_count'] = 1
                            party_giving = "republican_giving"
                        else:
                            partition_signal['other_count'] = 1
                            party_giving = "other_giving"

                    if partition_signal['in_partition']:
                        partition_signal['giving'] = amount
                        if party_giving:
                            partition_signal[party_giving] = amount
                    else:
                        partition_signal['giving'] = int(0)
                yield record, signal

        pipeline.records_signals = _process_giving(pipeline.records_signals)
        return pipeline


@public
class IRSCalculateGiving(SignalAction):
    """Calculate contribution analysis signals for IRS political organization
       contributions, e.g. contributions to 527 organizations.
    """
    def run(self, pipeline, partition_config=None, *args, **kwargs):

        def _determine_giving(records_signals):
            for (record, signal) in records_signals:
                partition_signal = signal['partitions'][partition_config.id]
                amount = int(signal['trans_amt'])
                party = signal.get('party', 'OTHER')
                years = int(partition_config.years_before)
                base_giving = _decay_amount_before_cutoff(
                    amount, signal['trans_date'],
                    partition_config.start_date, years)
                partition_signal['base_giving'] = base_giving
                if party == 'DEM':
                    partition_signal['net_democrat'] = base_giving
                    partition_signal['democrat_count'] = 1
                    party_giving = "democrat_giving"
                elif party == 'REP':
                    partition_signal['net_republican'] = base_giving
                    partition_signal['republican_count'] = 1
                    party_giving = "republican_giving"
                else:
                    partition_signal['other_count'] = 1
                    party_giving = "other_giving"

                if partition_signal['in_partition']:
                    partition_signal['giving'] = amount
                    if party_giving:
                        partition_signal[party_giving] = amount
                else:
                    partition_signal['giving'] = 0
                yield record, signal

        pipeline.records_signals = _determine_giving(pipeline.records_signals)
        return pipeline


@public
class ScorePoliticalContribution(SignalAction):

    def run(self, pipeline, partition_config=None, *args, **kwargs):

        def _apply_scale(records_signals):
            for (record, signal) in records_signals:
                partition_signal = signal['partitions'][partition_config.id]
                adjusted_giving = None
                if 'adjusted_giving' in partition_signal:
                    adjusted_giving = partition_signal['adjusted_giving']
                elif 'base_giving' in partition_signal:
                    adjusted_giving = partition_signal['base_giving']
                    partition_signal['adjusted_giving'] = adjusted_giving
                if adjusted_giving:
                    score = _scale_score(adjusted_giving)
                    if adjusted_giving < 0:
                        score *= -1.0
                    partition_signal['score'] = score
                yield record, signal

        # TODO: Revisit results for low amounts
        def _scale_score(amt_input):
            """ This method scales the score transaction by a log transform
            fit to the points (0.0,0.0), (2500.0, 1.0), (50000.0, 15.0),
            (225000.0, 22.0), (1000000.0, 30.0) as per Steve's request of a
            factor of 30 difference between $2500 donation versus a $1,000,000
            donation.

            Work on the derivation of the function below can be found on wiki
            """
            a = 3.43906886e+01
            b = 6.13927389e+00
            c = 7.82396256e-01
            d = 1.91760087e+03
            e = 8.20063383e-01

            if amt_input == 0:
                x = 0.0
            elif amt_input < 0:
                x = abs(amt_input)
            else:
                x = amt_input
            result = math.sqrt(math.pow((a/b)*math.log((c/d)*x+e), 2))
            if result < 0.0:
                result = 0.0

            return result

        pipeline.records_signals = _apply_scale(pipeline.records_signals)
        return pipeline


@public
class SumValues(SummarySignalActionMixin, SignalAction):

    def __init__(self, *args, **kwargs):
        super(SumValues, self).__init__(*args, **kwargs)
        self.absolute_weight = self.definition.get('absolute_weight', False)
        self.use_base_giving = self.definition.get('use_base_giving', False)

    def run(self, pipeline, *args, **kwargs):

        def _grand_mean(pol_spectra):
            """
            This function returns the grand mean of the political spectrum
            tuples
            """
            grand_mean = 0.50
            weighted_sum = 0.0
            sample_size = 0.00000001

            if not pol_spectra:
                return grand_mean
            for pol_spec, weight in pol_spectra:
                if self.absolute_weight:
                    weight = abs(weight)
                weighted_sum += pol_spec * weight
                sample_size += weight
            grand_mean = weighted_sum / sample_size
            return grand_mean

        partitions = self.signal_sets[0]['partitions']
        for signal_partition_id in partitions:
            pol_specs = []
            pol_specs_freq = []
            partition_result = pipeline.results['partitions'][
                signal_partition_id]
            for signal_set in self.signal_sets:
                signal_partition = signal_set['partitions'][
                    signal_partition_id]
                for sum_field in ['adjusted_giving', 'giving', 'score',
                                  'democrat_count', 'republican_count']:
                    partition_result[sum_field] = (
                        partition_result.get(sum_field, 0) +
                        signal_partition.get(sum_field, 0))
                if self.use_base_giving:
                    pol_giving = signal_partition.get('base_giving', 0)
                else:
                    pol_giving = signal_partition.get('adjusted_giving', 0)
                democrat_count = signal_partition.get('democrat_count', 0)
                republican_count = signal_partition.get('republican_count', 0)
                if pol_giving or democrat_count or republican_count:
                    pol_specs.append((
                        signal_partition.get('political_spectrum', 0.5),
                        pol_giving))
                    pol_specs_freq.append((
                        signal_partition.get('political_spectrum_freq', 0.5),
                        (democrat_count + republican_count)))
            partition_result['political_spectrum'] = _grand_mean(
                pol_specs)
            partition_result['political_spectrum_freq'] = _grand_mean(
                pol_specs_freq)
            # Store copy of score and giving in partition key signals with a
            # `political` prefix. This will allow us to quickly get the
            # political values without needing to know the analysis type (nfp,
            # academic, political, etc)
            partition_key_signals = partition_result.setdefault('key_signals',
                                                                {})
            partition_result['political_score'] = partition_result['score']
            partition_result['political_giving'] = partition_result['giving']
            partition_key_signals['political_score'] = partition_result['score']
            partition_key_signals['political_giving'] = partition_result['giving']
        return pipeline


@public
class NormalizeMinMax(SignalAction):
    """Find the min and max scores for the results of each partition
    in this analysis.
    """
    def __init__(self, signal_action_config, partition_configs=None,
                 *args, **kwargs):
        super(NormalizeMinMax, self).__init__(signal_action_config,
                                              *args, **kwargs)
        self.partition_configs = partition_configs or []

    def run(self, pipeline, analysis=None, *args, **kwargs):
        pipeline.results["normalize"] = get_analysis_score_min_max(
            analysis.results.all(),
            (pc.id for pc in self.partition_configs))
        return pipeline


## DEPRECATED
@public
class LegacyNormalize(SignalAction):

    def __init__(self, *args, **kwargs):
        super(LegacyNormalize, self).__init__(*args, **kwargs)
        self.use_offset_log = self.definition.get('use_offset_log', False)

    def run(self, pipeline, *args, **kwargs):

        def _log_or_zero(i):
            if i <= 0:
                return 0
            else:
                return math.log(i)

        def _offset_log(i):
            return math.log(i + 1.0)

        if self.use_offset_log:
            log_func = _offset_log
        else:
            log_func = _log_or_zero

        normalization = defaultdict(dict)
        for result in [result['result'].results
                       for result in pipeline.results]:
            for partition in result['partitions']:
                norm_part = normalization[partition]
                partition_result = result['partitions'][partition]
                max_score = norm_part.get('max_score', 50.0)
                min_score = norm_part.get('min_score', 50.0)
                score = partition_result['score']
                if score < 0:
                    score = 0
                norm_part['max_score'] = max(
                    (score, max_score))
                norm_part['min_score'] = min(
                    (score, min_score))

        for partition in normalization:
            norm_part = normalization[partition]
            mx_score = log_func(float(norm_part['max_score']))
            mn_score = log_func(float(norm_part['min_score']))
            for result in [result['result'].results
                           for result in pipeline.results]:
                partition_result = result['partitions'][partition]
                score = partition_result['score']
                if score < 0:
                    score = 0
                score = log_func(float(score))

                # Normalize
                try:
                    normalized_score = ((score - mn_score) /
                                        (mx_score - mn_score)) * 100.0
                except ZeroDivisionError:
                    normalized_score = 0.0
                    LOGGER.error("This error should never appear. 0 in"
                                 " normalization.")
                if normalized_score < 0.0:
                    normalized_score = 0.0
                elif normalized_score > 100.0:
                    normalized_score = 100.0

                partition_result['normalized_score'] = normalized_score

        return pipeline


@public
class ReduceSignals(SignalAction):

    def __init__(self, *args, **kwargs):
        super(ReduceSignals, self).__init__(*args, **kwargs)
        self.sum_fields = set(self.definition.get('sum_fields', []))
        self.list_fields = set(self.definition.get('list_fields', []))
        self.max_fields = self.definition.get("max_fields", [])
        self.min_fields = self.definition.get("min_fields", [])
        self.median_fields = self.definition.get("median_fields", [])
        combine_fields = self.definition.get("combine_fields", {})
        combine_label = self.definition.get("combine_label")
        if not isinstance(combine_fields, Mapping):
            combine_fields = {combine_label: combine_fields[0]}
        self.combine_fields = combine_fields

    def run(self, pipeline, partition_config=None, *args, **kwargs):

        def _reduce(records_signals):
            for (record, signal) in records_signals:
                if partition_config:
                    input_signal = signal['partitions'][
                        partition_config.id]
                else:
                    input_signal = signal

                for sum_field in self.sum_fields:
                    output_result[sum_field] = (
                        output_result.get(sum_field, 0) +
                        input_signal.get(sum_field, 0))

                for median_field in self.median_fields:
                    if median_field in input_signal:
                        median_values[median_field].append(
                            input_signal[median_field])

                for max_field in self.max_fields:
                    if max_field in input_signal:
                        if max_field in output_result:
                            output_result[max_field] = max(
                                output_result[max_field],
                                input_signal[max_field])
                        else:
                            output_result[max_field] = input_signal[max_field]

                for min_field in self.min_fields:
                    if min_field in input_signal:
                        if min_field in output_result:
                            output_result[min_field] = min(
                                output_result[min_field],
                                input_signal[min_field])
                        else:
                            output_result[min_field] = input_signal[min_field]

                for list_field in self.list_fields:
                    if list_field in input_signal:
                        if list_field in output_result:
                            output_result[list_field].append(
                                input_signal[list_field])
                        else:
                            output_result[list_field] = [
                                input_signal[list_field]]

                for combine_label, combine_field in self.combine_fields.items():
                    for combine_group, group_fields in combine_field.items():
                        combine_group = [item.strip()
                                         for item in combine_group.split(",")]

                        # Collect the values for the items in the group
                        # By using csv_join, we avoid issues with commas in
                        # the label
                        key_values = csv_join(
                            str(input_signal[item])
                            for item in combine_group if item in input_signal)

                        if key_values:
                            # Create a dictionary using the combine label,
                            # so we can group the reductions meaningfully
                            ss_data = output_result.setdefault(
                                combine_label, {})
                            data = ss_data.setdefault(key_values, [])

                            group_values = []
                            try:
                                for value in group_fields:
                                    group_values.append(input_signal[value])
                                data.append(group_values)
                            except KeyError:
                                pass
                yield record, signal

        if partition_config:
            output_result = pipeline.results['partitions'].setdefault(
                partition_config.id, {})
        else:
            output_result = pipeline.results
        median_values = defaultdict(list)

        pipeline.records_signals = list(_reduce(pipeline.records_signals))

        for median_field, values in median_values.items():
            # Calculate the median of all values
            output_result['{}_median'.format(median_field)] = median(values)
        return pipeline


@public
class NonProfitReduceSignals(SignalAction):
    """This would perform reduce operations that are required by
    SimpleNonProfitScore SignalAction
    """

    def __init__(self, *args, **kwargs):
        super(NonProfitReduceSignals, self).__init__(*args, **kwargs)
        self.median_fields = self.definition.get("median_fields", [])

    def run(self, pipeline, partition_config=None, *args, **kwargs):

        def _reduce(records_signals):
            for (record, signal) in records_signals:
                if partition_config:
                    input_signal = signal['partitions'][
                        partition_config.id]
                else:
                    input_signal = signal

                for median_field in self.median_fields:
                    # Get all signal keys that start with median_field. This
                    # is required to handle year_* fields
                    field_keys = filter(lambda x: x.startswith(median_field),
                                        input_signal.keys())
                    for field_key in field_keys:
                        median_values[field_key].append(
                            input_signal[field_key])

                yield record, signal

        # Choose the partition to add the results
        if partition_config:
            output_result = pipeline.results['partitions'].setdefault(
                partition_config.id, {})
        else:
            output_result = pipeline.results

        # Initialize median_values to store median amount per year
        median_values = defaultdict(list)

        # Perform reduce operations
        pipeline.records_signals = list(_reduce(pipeline.records_signals))

        # Calculate the median amount and frequency for each median field.
        # Also calculate the total frequency
        total_frequency = 0
        for median_field, values in median_values.items():
            output_result['{}_frequency'.format(median_field)] = len(values)
            output_result['{}_median'.format(median_field)] = median(values)
            total_frequency += len(values)

        output_result['total_frequency'] = total_frequency

        return pipeline


@public
class SimpleNonProfitScore(SignalAction):
    """Calculates the nonprofit score. Adds the score to the
    non_profit_score and total amount donated to non_profit_giving key on the
    pipeline results.

    This SignalAction looks for the following keys in
    pipeline.results:
        year_{year_num}_median
        year_{year_num}_frequency
    and for the following keys in defintion
        expected_amount_weights
            dictionary mapping year to expected_amount_weights
            example: {'expected_amount_weights':{1: 0.4, 2: 0.2, 3: 0.15,
                        4: 0.15, 5: 0.1}}
            NOTE: sum of all the weights must add up to 1
        frequency_weights
            dictionary mapping year to frequency_weights
            example: {'frequency_weights':{1: 0.56, 2: 0.44}}
            NOTE: sum of all the weights must add up to 1

    Assuming that
    f<num> = pipeline.results['year_<year_num>_frequency']
    m<num> = pipeline.results['year_<year_num>_median']
    wm<num> = expected_amount_weights[num]
    wf<num> = frequency_weights[num]

    the non profit score is calculated as follows:
    expected_amount_calculation = wm1*m1 + wm2*m2 + wm3*m3 + wm4*m4 + wm5*m5
    frequency_calculation = wf1*f1 + wf2*f2

    score = expected_amount_calculation * frequency_calculation * \
                                                            past_donor_modifier

    NOTE: no implementation for past_donor_modifier yet

    SPECIAL CONDITIONS:

    CONDITION 1:
    wm<num> = expected_amount_weights[num] only if a
    median amount (m<num>) exists if not it is set to 0. Since all wm<nums>
    must add up to 1, the remaining weights are scaled accordingly.
    Example:
        lets say we have following values:
            m1 = 2700.0, m2 = None, m3 = 2700.0, m4 = None, m5 = 2600.0
            expected_amount_calculation = m1 * w1 * scale_factor +
                                          m3 * w3 * scale_factor +
                                          m5 * w5 * scale_factor
            where scale_factor = 1/(w1+w3+w5)
            w2 and w4 are out of the equation here (literally :-) )  as
            they are set to zero (because m2 and m4 are None)

    CONDITION 2:
    If there are no contributions made in the years specified in
    expected_amount_weights or frequency_weights then the values are weighed
    differently.
    For expected_amount_calculation, the weight would be
        0.05 / math.log(x)
    For frequency_calculation, the weight would be
        1 / x
    where x is the number of years passed since a contribution was made.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.expected_amount_weights = self.get_expected_amount_weights()
        self.frequency_weights = self.get_frequency_weights()

    def get_expected_amount_weights(self):
        temp_dict = self.definition.get('expected_amount_weights', None)
        if temp_dict:
            weights = {int(key): float(val) for (key, val) in temp_dict.items()}
        else:
            weights = {1: 0.4,
                       2: 0.2,
                       3: 0.15,
                       4: 0.15,
                       5: 0.1}
        return weights

    def get_frequency_weights(self):
        temp_dict = self.definition.get('frequency_weights', None)
        if temp_dict:
            weights = {int(key): float(val) for (key, val) in temp_dict.items()}
        else:
            weights = {1: 0.65,
                       2: 0.35}
        return weights

    def expected_amount_calculation(self, amounts):
        total_weight = 0
        total_score = 0

        # Check if there are any contributions in the years specified in
        # expected_amount_weights
        if not self.expected_amount_weights.keys().isdisjoint(amounts.keys()):
            for year_num, weight in self.expected_amount_weights.items():
                median = amounts.get(year_num, 0)
                # Set the weight to 0 if no contributions were made in that
                # year
                if median == 0:
                    weight = 0

                total_weight += weight
                total_score += weight * median
        # Use different weights if no contributions have been made in the
        # years specified in expected_amount_weights
        else:
            for year_num, median in amounts.items():
                weight = 0.05 / math.log(year_num)
                total_weight += weight
                total_score += weight * median

        try:
            weight_scale_factor = 1 / total_weight
        except ZeroDivisionError:
            weight_scale_factor = 0

        expected_amount = total_score * weight_scale_factor
        return expected_amount

    def frequency_calculation(self, frequencies):
        total_score = 0

        # Check if there are any contributions in the years specified in
        # frequency_weights
        if not self.frequency_weights.keys().isdisjoint(frequencies.keys()):
            for year_num, weight in self.frequency_weights.items():
                frequency = frequencies.get(year_num, 0)
                # Set the weight to 0 if no contributions were made in that
                # year
                if frequency == 0:
                    weight = 0
                total_score += weight * frequency
        # Use different weights if no contributions have been made in the
        # years specified in frequency_weights
        else:
            for year_num, frequency in frequencies.items():
                weight = 1 / year_num
                total_score += weight * frequency

        return total_score

    def run(self, pipeline, partition_config=None, *args, **kwargs):

        if partition_config:
            results = pipeline.results['partitions'].setdefault(
                partition_config.id, {})
        else:
            results = pipeline.results

        def extract_amounts():
            """Inspects all the keys in pipeline.results and extracts the
            median amounts and associated year. Adds the info to a dictionary
            where the key denotes the number of years passed since a
            contribution was made and the value denotes the median amount that
            was contributed.
            """
            pattern = re.compile('year_(\d+)_median')
            amounts = {}

            for key in results.keys():
                matched = pattern.match(key)
                if matched:
                    amount_year = matched.groups()[0]
                    amounts[int(amount_year)] = results.get(key)

            return amounts

        def extract_frequencies():
            """Inspects all the keys in pipeline.results and extracts the
            frequency of contributions and associated year. Adds the info to a
            dictionary where the key denotes the number of years passed since a
            contribution was made and the value denotes the frequency of
            contributions during that period.
            """
            pattern = re.compile('year_(\d+)_frequency')
            frequencies = {}

            for key in results.keys():
                matched = pattern.match(key)
                if matched:
                    frequency_year = matched.groups()[0]
                    frequencies[int(frequency_year)] = results[key]

            return frequencies

        # TODO: get past_donor_modifer value when the infrastructure for
        # boosts on non profit data is setup. For now, set past_donor_modifer=1
        past_donor_modifer = 1

        amounts = extract_amounts()
        frequencies = extract_frequencies()

        amount_calculation = self.expected_amount_calculation(amounts)
        frequency_calculation = self.frequency_calculation(frequencies)

        score = amount_calculation * frequency_calculation * past_donor_modifer
        results['score'] = score

        return pipeline


@public
class GivingBreakdownFilterReducer(SignalAction):
    """Creates summary of contributions breaking them down by target (self, ally,
    all) and year ranges.

    The resulting data structure looks like the following:
    $donation_target:
      $year_or_unbound:
        count: $num_donations
        amount: $amount

    Donation targets:
    - ally
    - all
    - client

    Year partitions are inclusive. Contributions made within the last year are
    also included in larger partitions.

    This SignalAction uses contribution amounts aggregated and partitioned in
    ReduceSignals by `within_n_years, is_ally` to work.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.year_partitions = self.definition.get(
            'year_partitions', [1, 2, 3, 5, 10, 15, None])
        self.client_data_key = self.definition.get(
            'client_data_key', 'giving_breakdown_client_data')
        self.other_data_key = self.definition.get(
            'other_data_key', 'giving_breakdown_nfp_data')

    @classmethod
    def _get_from_list_containing(cls, lst, key, default=None):
        for data in lst:
            if isinstance(data, Container) and key in data:
                return data
        return default

    @classmethod
    def reduce(cls, year_partitions, data, is_client=False, output=None):
        """Iteratively build result data structure

        Schema of result:
        $donation_target:
          $year_or_unbound:
            count: $num_donations
            amount: $amount
        """
        if output is None:
            output = {'ally': {}, 'all': {}, 'client': {}}

        def _update(output_keys, within_n_years, count, total):
            for key in output_keys:
                for partition in year_partitions:
                    if partition is None or partition >= within_n_years:
                        data_ = output[key].setdefault(partition,
                                                       {'count': 0,
                                                        'amount': 0})
                        data_['count'] += count
                        data_['amount'] += total

        for key, amounts in data.items():
            if is_client:
                within_n_years = int(key)
                output_keys = ['all', 'client']
            else:
                within_n_years, is_ally = csv_split(key)
                within_n_years = int(within_n_years)
                if string_utils.str_to_bool(is_ally):
                    output_keys = ['ally', 'all']
                else:
                    output_keys = ['all']

            amounts = lcat(amounts)
            count = len(amounts)
            total = sum(amounts)
            _update(output_keys, within_n_years, count, total)
        return output

    def run(self, pipeline, signal_sets, *args, **kwargs):
        output = {'ally': {}, 'all': {}, 'client': {}}
        pipeline.results['giving_breakdown'] = output

        other_data = self._get_from_list_containing(
            signal_sets, self.other_data_key, {})
        self.reduce(self.year_partitions, other_data, output=output,
                    is_client=False)

        client_data = self._get_from_list_containing(
            signal_sets, self.client_data_key, {})
        self.reduce(self.year_partitions, client_data, output=output,
                    is_client=True)

        return pipeline


@public
class SimplePoliticalSpectrum(SignalAction):

    def run(self, pipeline, partition_config=None, *args, **kwargs):

        def _political_spec_calc(net_democrat, net_republican):
            """ Calculates the political spectrum of this contact
            Negative values represent refunds, if this is true only way we get
            negative values are if we don't have the contribution matching the
            refund in our cutoff group, so abs(value) logic below holds for
            spec calc
            """
            dem = abs(net_democrat)
            rep = abs(net_republican)
            tot = abs(dem + rep)
            if tot - 0.0 <= 0.0000000001:
                spectrum_calculation = 0.5
            else:
                spectrum_calculation = dem / tot
            return spectrum_calculation

        partition_result = pipeline.results['partitions'][partition_config.id]
        if ('net_democrat' in partition_result and
                'net_republican' in partition_result):
            partition_result['political_spectrum'] = _political_spec_calc(
                float(partition_result['net_democrat']),
                float(partition_result['net_republican']))
            partition_result[
                'political_spectrum_freq'] = _political_spec_calc(
                    float(partition_result['democrat_count']),
                    float(partition_result['republican_count']))
        return pipeline


@public
class SpectrumDampen(SummarySignalActionMixin, ConfigurableSignalAction):

    def __init__(self, *args, **kwargs):
        super(SpectrumDampen, self).__init__(*args, **kwargs)
        self.party = self.get_setting('party')
        self.maximum = self.definition.get('maximum', 50)
        self.scale = self.definition.get('scale', 1.0)

    def run(self, pipeline, *args, **kwargs):

        def _dampen(part_result):
            score = part_result['score']
            if score < 0.0:
                score = 0.0
            if self.party not in ['Republican', 'Democrat', 'Democratic']:
                return score
            elif self.party == 'Republican':
                base = 0.0
                count = part_result.get('democrat_count', 0)
            else:
                base = 1.0
                count = part_result.get('republican_count', 0)
            if count > self.maximum:
                count = self.maximum
            spectrum = part_result.get('political_spectrum_freq', base)
            distance = abs(base - spectrum)
            if distance == 1.0:
                distance = 0.9999999999
            prop_distance = self.scale * distance * count / self.maximum
            proportion = 1.0 - prop_distance
            new_score = score * proportion
            return new_score

        partitions = self.signal_sets[0]['partitions']
        for signal_partition_id in partitions:
            partition_result = pipeline.results['partitions'][
                signal_partition_id]
            partition_result['score'] = _dampen(partition_result)
        return pipeline


@public
class KeyContributionsKeySignals(SignalAction):
    """Add the Key Contributions to the Key Signals dict."""
    def __init__(self, *args, **kwargs):
        super(KeyContributionsKeySignals, self).__init__(*args, **kwargs)
        self.combine_label = self.definition.get("combine_label")

    def run(self, pipeline, *args, **kwargs):
        data = []
        for key_contrib, records in pipeline.results.get(
                self.combine_label, {}).items():

            # Look at the records and find the most recent
            most_recent = None
            for record in records:
                # If the record length isn't 3, something isn't configured
                # right, but we don't want to blow up.
                if len(record) == 3:
                    # Pop the date off of the result because we really only
                    # need it for this code right here.
                    trans_date = record.pop()
                    most_recent = max(most_recent, trans_date) \
                                  if most_recent else trans_date

            label, is_ally = csv_split(key_contrib)
            datum = {
                "label": label,
                "is_ally": string_utils.str_to_bool(is_ally)
            }
            if most_recent:
                datum['last_contrib'] = most_recent
            data.append(datum)

        pipeline.key_signals.setdefault(self.combine_label, []).extend(data)
        return pipeline


# TODO: REVUP-2677 This SAC will be redone as a separate task. Also add
# another SAC after `Fetch Subscores` SAC in the `Major Gifts Persona` SSC
# which copies the scores from `affinity_score` field in the signal to
# ['sub_scores', 'AI'] path in the signal.
@public
class CalculateAffinityScore(ConfigurableSignalAction):
    """Calculates an affinity score."""

    def run(self, pipeline, *args, **kwargs):
        def _calculate_affinity(records_signals):
            for record, signal in records_signals:
                signal['affinity_score'] = 0
                yield record, signal

        pipeline.records_signals = _calculate_affinity(pipeline.records_signals)
        return pipeline


@public
class FetchSubscores(ConfigurableSignalAction):
    """Fetches the persona subscores from the fused_entities associated with a
    contact that are specific to the current persona type.
    """
    PERSONA_TYPES = {'Major Gifts Persona': 'major_gifts'}

    def __init__(self, feature_config, analysis_config, *args, **kwargs):
        super().__init__(
            feature_config, analysis_config, *args, **kwargs)
        self.feature = feature_config.feature
        self.persona_types_all = self.definition.get(
            'persona_types', self.PERSONA_TYPES)
        self.persona_type = self.persona_types_all.get(self.feature.title)

    def run(self, pipeline, *args, **kwargs):
        def _add_sub_scores(records_signals, persona_type):
            for record, signal in records_signals:
                all_sub_scores = []
                for each_entity in signal['fused_client_summary']:
                    # Fetch the sub-scores from fused entities that are
                    # specific to the current persona type.
                    sub_scores = get_in(
                        each_entity, ['personas', persona_type],
                        defaultdict(float))
                    all_sub_scores.append(sub_scores)
                signal['sub_scores'] = all_sub_scores

                yield record, signal

        pipeline.records_signals = _add_sub_scores(
            pipeline.records_signals, self.persona_type)
        return pipeline


@public
class CalculateOverallScore(ConfigurableSignalAction):
    """Calculates an overall persona score which is a weighted summation of
    individual sub-scores.
    """
    PERSONA_TYPES = {'Major Gifts Persona': 'major_gifts'}

    def __init__(self, feature_config, *args, **kwargs):
        super().__init__(feature_config, *args, **kwargs)
        self.feature = feature_config.feature
        self.persona_types_all = self.definition.get(
            'persona_types', self.PERSONA_TYPES)
        self.persona_type = self.persona_types_all.get(self.feature.title)
        # TODO: REVUP-2677 Add `AI` in the list of indicators once the SAC for
        # calculating the affinity score is added
        self.indicators = self.definition.get(
            'indicators', ['DG', 'WI', 'PI'])

    def calculate_score(self, scores, weights):
        """Calculates a score which is a weighted summation of all the
        sub-scores of indicators that are defined in `self.indicators`.
        """
        total_score = 0
        for ind in self.indicators:
            total_score += float(scores[ind]) * float(weights[ind])
        return total_score

    def get_max_persona_score(self, sub_scores, weights):
        """Fetches the set of scores that would result in a maximum overall
        score in the event there are multiple client entities associated with a
        contact.
        """
        # Initialize a temporary dictionary
        temp_dict = {ind: 0 for ind in self.indicators}
        temp_dict.update({'score': 0})

        # Iterate through all the set of sub-scores
        for each_sub_score in sub_scores:
            score = self.calculate_score(each_sub_score, weights)
            # If the new overall score is greater than current calculated
            # score, update with new sub-scores.
            if score > temp_dict['score']:
                temp_dict['score'] = score
                temp_dict.update(each_sub_score)
        return temp_dict

    def run(self, pipeline, *args, **kwargs):
        def _calculate_overall_score(records_signals, get_max_persona_score):
            for record, signal in records_signals:
                # Fetch the set of sub-scores that would result in a maximum
                # overall persona score.
                persona_scores = get_max_persona_score(
                     signal['sub_scores'], signal['weights'])
                signal['persona_scores'] = persona_scores
                yield record, signal

        pipeline.records_signals = _calculate_overall_score(
            pipeline.records_signals, self.get_max_persona_score)
        return pipeline


@public
class PopulatePersonaKeySignals(ConfigurableSignalAction):
    """Populates key_signals with `personas` key.

    This SAC is similar to FieldCopy except that the `target_field` is fixed.
    In this case it is pipeline.key_signals with `personas` as a key.

    Stores the persona scores in pipeline.key_signals in the format
    "personas": {
        "<identifier>": {
            "score": float,
            "DG": float,
            "WI": float,
            "PI": float,
            "AI": float
            }
        }
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.mappings = self.definition['mappings']
        self.persona_configs = self.persona_config_wrapper.configs

    def run(self, pipeline, *args, **kwargs):
        pipeline.key_signals.setdefault('personas', defaultdict(dict))

        for (record, signal) in pipeline.records_signals:
            for mapping in self.mappings:
                source_field = mapping['source_field']
                value = signal[source_field]
                # Populate the key_signals for every persona instance.
                for identifier in self.persona_configs.keys():
                    pipeline.key_signals['personas'][identifier].update(value)

        return pipeline


@public
class MajorGiftsTag(ConfigurableSignalAction):
    """Tags a contact based on the `Major Gifts` persona instance
    configurations associated with an account.
    """
    def __init__(self, upstream_ssc_ids, ssc_fc_lookup_map, *args, **kwargs):
        if len(upstream_ssc_ids) != 1:
            # Assuming that the `Nonprofit entity contributions` is set as
            # upstream SSC and is the only upstream SSC
            LOGGER.exception("Inavlid upstream SSC configuration")
        super().__init__(*args, **kwargs)
        self.upstream_ssc_ids = upstream_ssc_ids
        self.ssc_fc_lookup_map = ssc_fc_lookup_map
        self.persona_configs = self.persona_config_wrapper.configs

    def run(self, pipeline, *args, **kwargs):
        # Fetch upstream SSC results to access `giving` of a contact (result
        # of `Nonprofit entity contributions` in the current SAC)
        ssc_id = self.upstream_ssc_ids[0]
        fc_id = self.ssc_fc_lookup_map.get(ssc_id)
        feature_results = pipeline.global_signals['features']
        ssc_results = feature_results[fc_id][ssc_id]
        partitioned_results = ssc_results['partitions']

        # Calculate the max giving across all partitions as the key_signals
        # are NOT per partition
        giving = [v.get('giving', 0) for v in partitioned_results.values()]
        max_giving = max(giving)

        for identifier, data in self.persona_configs.items():
            ranges = data.get('ranges', [])
            # Only one range should be specified per QSD
            if not ranges or len(ranges) > 1:
                LOGGER.error(
                    'Persona: {0} was configured with invalid QSD {1}'.format(
                        identifier, data))
            else:
                range_ = ranges[0]
                if range_['floor'] <= max_giving <= range_['ceiling']:
                    pipeline.tags.add(identifier)
        return pipeline


@public
class PurdueUpdateContact(SignalAction):
    """Update the attributes zip3s and states on the contact we are currently
    processing. Purdue analysis should filter FEC & Misp records based on
    location data from Purdue's database, not the information gleaned from the
    user's imported contacts (which may be incomplete).

    Additionally update key_signals with any information necessary from alumni
    record.
    """
    def run(self, pipeline, *args, **kwargs):
        pipeline.records_signals = list(pipeline.records_signals)
        alum, signal = pipeline.records_signals[0]
        contact = pipeline.target
        contact.bio_id = alum.bio_id
        zip3s = set()
        if alum.zip:
            zip3s.add(alum.zip[:3])
        for past_address in signal['past_addresses']:
            if past_address.zip:
                zip3s.add(past_address.zip[:3])

        contact.zip3s = zip3s

        states = set()
        if alum.state:
            states.add(alum.state)
        for past_address in signal['past_addresses']:
            if past_address.state:
                states.add(past_address.state)
        contact.states = states

        pipeline.key_signals.update({
            # 'major_contributor': alum.major_contributor,
            # 'title': alum.title,
            # 'company': alum.company,
            'graduation_year': alum.graduation_year,
            # 'degree': alum.degree,
            # 'major': alum.major,
            'school': (alum.school or "").replace("College of ", "").title()
        })

        # This is for the Largest Donors quickfilter. See the ClientTopDonor
        # SignalAction and REVUP-1345 for more details.
        if alum.top_donor:
            pipeline.tags.add(ClientTopDonor.tag_name)
        return pipeline


@public
class ResetPipelineResults(SignalAction):
    """Swap the results attribute on a pipeline with a new dict.

    This is necessary for SignalSetConfigs attached to a Prime FeatureConfig
    since the runner does not reinitialize the pipeline object for each
    SignalSetConfig processed per FeatureConfig. This prevents duplication of
    results or (worse), messing up the results for the SSC we just finished
    processing.
    """
    def run(self, pipeline, *args, **kwargs):
        pipeline.results = {}
        return pipeline


@public
class GivingHistory(SignalAction):
    """Keep a running tally of giving history for a source per year for a
    configured number of years.

    Supports determining the years to calculate giving history for using
    relative years. With no configuration `GivingHistory` will determine giving
    history for the last 5 years, using the current year as the endpoint.

    It is also possible to specify concrete start and end dates via the SAC
    definition keys `start_year` and `end_year`. Specifying both these values
    will override dynamic year calculation.

    If only end_year is configured via definition, then giving history is
    calculated for a 5 year span ending with end_year.

    If only start_year is specified in the definition, then the giving history
    will span from the start_year to the current year.
    """
    def __init__(self, *args, **kwargs):
        super(GivingHistory, self).__init__(*args, **kwargs)
        years_history = self.definition.get('years_history', 5)
        start_year = self.definition.get("start_year")
        # set end_year to be 1 year past the actual final year we want, as
        # range is exclusive of the end value.
        end_year = self.definition.get("end_year",
                                       datetime.date.today().year) + 1
        if not start_year:
            start_year = end_year - years_history

        # Build a list of years from start_year to end_year inclusive
        # E.g.: [2011, 2012, 2013, 2014, 2015]
        self.giving_history_years = list(range(start_year, end_year))

    def run(self, pipeline, *args, **kwargs):
        def _calc(records_signals):
            giving_history = defaultdict(int)

            # Iterate over the contribution records and summate the giving
            # amount for each year in giving_history_years
            for record, signal in records_signals:
                year = int(record.date.year)
                if year in self.giving_history_years:
                    giving_history[year] += float(record.amount)
                yield record, signal

            # Convert the giving history data from a dict to a list of
            # tuples, sorted chronologically, and store in the results.
            if giving_history:
                pipeline.results['giving_history'] = sorted(
                    (year, giving_history.get(year, 0))
                    for year in self.giving_history_years
                )

        pipeline.records_signals = _calc(pipeline.records_signals)
        return pipeline


@public
class PurdueSummary(SignalAction):
    """Performs Purdue-specific scoring and summary actions.

    - Replaces the giving total (normally summed across all sources) that is
      stored in key_signals with purdue giving.
    - Uses pre-calculated purdue score in scoring.
    - Boosts purdue score with score from other sources (as was done in legacy).
    - Makes a copy of the political score and political giving in
        pipeline.results for other use later in the pipeline.
    - Copies political score and political giving per partition into a
        per-partition key_signals result object.
    """
    def __init__(self, partition_configs, *args, **kwargs):
        super(PurdueSummary, self).__init__(*args, **kwargs)
        self.partition_configs = partition_configs

    def run(self, pipeline, signal_sets, *args, **kwargs):
        def _log_or_zero(i):
            if i <= 0:
                return 0
            else:
                return math.log(i)

        purdue_result = signal_sets[0]
        try:
            purdue_giving = float(purdue_result['life_total'][0])
        except ValueError:
            purdue_giving = 0
        if 'normal_score_new' in purdue_result:
            purdue_score = int(purdue_result['normal_score_new'][0])
        else:
            purdue_score = int(purdue_result['normal_score'][0])

        for partition in self.partition_configs:
            partition_result = pipeline.results['partitions'].setdefault(
                partition.id, {})
            partition_result['giving'] = purdue_giving
            partition_result['score'] = purdue_score + (
                2 * _log_or_zero(political_score))
            # Store copy of score and giving in partition results and key
            # signals with a `academic` prefix. This will allow us to quickly
            # get the academic values without needing to know the analysis type
            # (nfp, academic, political, etc)
            partition_key_signals = partition_result.setdefault('key_signals',
                                                                {})
            partition_result['academic_score'] = partition_result['score']
            partition_result['academic_giving'] = partition_result['giving']
            partition_key_signals['academic_score'] = partition_result['score']
            partition_key_signals['academic_giving'] = partition_result['giving']
            # Additionally store giving with `client` prefix, since this giving
            # value did come from the client's data.
            partition_result['client_giving'] = partition_result['giving']
            partition_key_signals['client_giving'] = partition_result['giving']
        return pipeline


@public
class NonProfitSummary(SummarySignalActionMixin, SignalAction):
    """Performs NonProfitSummary scoring and summary actions.
    - Uses pre-calculated nonprofit score in scoring.
    - Makes a copy of the political score and political giving in
        pipeline.results for other use later in the pipeline.
    - Copies political score and political giving per partition into a
        per-partition key_signals result object.
    """
    def __init__(self, partition_configs, *args, **kwargs):
        super(NonProfitSummary, self).__init__(*args, **kwargs)
        self.partition_configs = partition_configs

    def run(self, pipeline, *args, **kwargs):
        nfp_signal_set = self.signal_sets[0]
        base_nfp_score = nfp_signal_set.get('score', 0)
        base_nfp_giving = nfp_signal_set.get('giving', 0)
        base_nfp_giving_frequency = nfp_signal_set.get(
            'total_frequency', 0)
        try:
            base_nfp_avg_giving = base_nfp_giving / base_nfp_giving_frequency
        except ZeroDivisionError:
            base_nfp_avg_giving = 0

        for partition in self.partition_configs:
            part_nfp_ss = nfp_signal_set['partitions'].setdefault(
                partition.id, {})
            nfp_score = base_nfp_score or part_nfp_ss.get('score', 0)
            nfp_giving = base_nfp_giving or part_nfp_ss.get('giving', 0)
            nfp_giving_frequency = base_nfp_giving_frequency or part_nfp_ss.get(
                'total_frequency', 0)
            try:
                part_nfp_avg_giving = nfp_giving / nfp_giving_frequency
            except ZeroDivisionError:
                part_nfp_avg_giving = 0
            nfp_avg_giving = base_nfp_avg_giving or part_nfp_avg_giving

            partition_result = pipeline.results['partitions'].setdefault(
                partition.id, {})
            partition_key_signals = partition_result.setdefault('key_signals',
                                                                {})
            partition_result['giving'] = nfp_giving
            partition_result['score'] = nfp_score
            partition_result['avg_giving'] = nfp_avg_giving
            partition_key_signals['avg_giving'] = nfp_avg_giving
            # Store copy of score and giving in partition results and key
            # signals with a `nonprofit` prefix. This will allow us to quickly
            # get the nonprofit values without needing to know the analysis type
            # (nfp, academic, political, etc)
            partition_result['nonprofit_score'] = partition_result['score']
            partition_result['nonprofit_giving'] = partition_result['giving']
            partition_key_signals['nonprofit_score'] = partition_result['score']
            partition_key_signals['nonprofit_giving'] = partition_result['giving']

        return pipeline


@public
class DiamondsInTheRough(ConfigurableSignalAction):
    """
    A "Diamond in the Rough" is a contact who has given to the client
    within this cycle, but gave an amount significantly below the
    average of their normal contributions.

    To gauge significance of contribution, we are using T-statistics, and
    a "Student's t-test". The default maximum p-value is 0.3 (or 30%).
    Essentially, what that means is the amount of money the contact has
    given to the client has a less-than-30% chance of being a realistic giving
    amount. Or, in better words, there is at least a 70% chance the giving
    amount is an outlier (on the low side), and they can/would give more.

    The following items can be configured in the definition json:
      party:  (Required) We only look at one party when building the mean
              for the t-test; typically, the same party as the client.

      max_p_value: (float) The maximum p-value. See the description above
                   for an explanation of p-value

      giving_years: The number of years to look back in order to build the
                    mean for the t-test. E.g. giving_years=5; contributions
                    from 2010 would not be considered, but contributions from
                    2011 would be (today is 2016). Defaults to 4 years.

      min_giving_count: The minimum number of contributions that may be
                        used to build a mean for the t-test. Default is 4.
    """
    def __init__(self, *args, **kwargs):
        super(DiamondsInTheRough, self).__init__(*args, **kwargs)
        self.party = self.get_setting('party', "").strip().upper()[:3]
        if self.party not in ("DEM", "REP"):
            self.party = ""

        self.max_p_value = float(self.definition.get("max_p_value", 0.3))
        self.giving_years = int(self.definition.get("giving_years", 4))
        self.min_giving_count = self.definition.get("min_giving_count", 4)

        self.start_date = (datetime.date.today() -
                           datetime.timedelta(days=self.giving_years*365))

    def run(self, pipeline, cached_record_signals, *args, **kwargs):
        availability = pipeline.key_signals.get('giving_availability')
        client_giving = pipeline.key_signals.get('giving_this_period')

        def _worker():
            givings = []
            # Iterate over the various giving records and extract their
            # giving amounts. The giving amounts will be used to build a mean.
            for (rsc_id, record_signals) in cached_record_signals.items():
                for (record, signal) in record_signals:
                    party = signal.get("party", "").strip().upper()
                    # If party is defined, we will only use same-party
                    # contributions for the t-test
                    if self.party and party != self.party:
                        continue

                    giving = signal.get("trans_amt")
                    giving_date = signal.get(
                        "trans_date", datetime.date.fromtimestamp(0))
                    if giving and giving_date > self.start_date:
                        givings.append(giving)

            if len(givings) > self.min_giving_count:
                # This function will build a mean and compare it to the
                # client's giving. See the class docstring for more
                # info on t-tests.
                t_stat, p_value = stats.ttest_1samp(givings, client_giving)
                # If t_stat is negative, that means this contribution is
                # actually greater than expected. That doesn't help us.
                if t_stat > 0 and p_value < self.max_p_value:
                    if "ditr" not in pipeline.tags:
                        pipeline.tags.add("ditr")

        # Contact has to have given to the client recently, and can't be maxed
        if client_giving and availability:
            _worker()
        return pipeline


@public
class NonprofitDiamondsInTheRough(ConfigurableSignalAction):
    """
    A "Diamond in the Rough" is a contact/donor who has donated over the
    past 12 months and who has given to client NonProfit at a level that is
    statistically and significantly less than what the donor gives to other
    non profits (and/or you can include political in there if easier).

    Algorithm:
    - Only Contacts that have given to the client within 12 months
    - Calculate the Median of the contact's contributions to ALL Nonprofits (m)
    - Summate the contact's contributions to the client for the last 12
    months (s)
    - Compare that summation of client donations (s) to the Median of all
    donations (m)
    - If the summation (s) is less than (median (m) * giving threshold) this is
     a Diamond in the Rough s < (m * giving_threshold)

    All numbers are default values and are configurable.

    The following items can be configured via the definition JSON or via
    QuestionSet:
        giving_days: (int)
            Only consider contributions made within the last given number of
            days when determine whether a contact is a diamond in the rough.
            The default is 365 days.

        giving_threshold: (float)
            The threshold that the summation should be less than the median.
            This is configurable in case the requirements change.

        client_feature_title: (text)
            The Feature Title.

        nonprofit_title: (text)
            The Nonprofit title.
    """
    tag_name = 'nfp-ditr'

    def __init__(self, *args, **kwargs):
        super(NonprofitDiamondsInTheRough, self).__init__(*args, **kwargs)

        self.rsc_feature_map = kwargs.get('rsc_feature_map', {})
        self.client_feature_title = self.get_setting('client_feature_title',
                                                     'Client Matches')
        self.nonprofit_title = self.get_setting('nonprofit_feature_title',
                                                'Nonprofit Matches')
        self.giving_days = int(self.get_setting('giving_days', 365))
        self.giving_threshold = float(self.get_setting('giving_threshold', 0.5))

        self.giving_start_date = (datetime.date.today() -
                                  datetime.timedelta(days=self.giving_days))

    def run(self, pipeline, cached_record_signals, *args, **kwargs):
        giving_median = 0
        median_givings = []
        summate_givings = []

        # Iterate over the various giving records and extract their
        # giving amounts. The giving amounts will be used to find the median.
        for (rsc_id, record_signals) in cached_record_signals.items():
            _giving_total = 0.0
            feature_type = self.rsc_feature_map.get(rsc_id)

            if feature_type:
                for (record, signal) in record_signals:
                    giving = signal.get("giving")
                    giving_date = signal.get("trans_date")
                    if not (giving and giving_date):
                        continue

                    # If the record is client data, calculate the sum.
                    if feature_type == self.client_feature_title:
                        # Only calculate the total sum with givings during the
                        # last 12 months
                        if giving_date > self.giving_start_date:
                            summate_givings.append(giving)
                    # If the record is nonprofit data.
                    # The median is found from ALL non-profit data so add it
                    # here
                    elif feature_type == self.nonprofit_title:
                        median_givings.append(giving)
            else:
                LOGGER.error("NFP DitR RecordSetConfig id is missing or was "
                             "not found in the rsc_feature_map (record set "
                             "config feature map.")

            if median_givings:
                giving_median = median(median_givings)

        # If the summation (client_givings) of givings is less than
        # giving_threshold of the median (giving_median) then this is a
        # nonprofit diamond in the rough
        if self.is_nfp_ditr(summate_givings, giving_median):
            pipeline.tags.add(self.tag_name)

        return pipeline

    def is_nfp_ditr(self, summate, median):
        return sum(summate) < (median * self.giving_threshold) if summate \
            else False


@public
class AcademicDiamondsInTheRough(ConfigurableSignalAction):
    """
    A "Diamond in the Rough" is a contact who has given to the client
    in the last few years, but gave an amount significantly below the
    average of their normal contributions.

    Unlike the political Diamond in the Rough signal action, we use heuristics
    instead of statistics to determine whether the contact is a diamond in the
    rough.

    The conditions to be a diamond in the rough are:
    - Contact has given less than $1K in total to institution over last 3 years
    - Contact has given something (not $0) to institution in last 5 years
    - Contact has given $10K or more in aggregate to all in the last 3 years

    All numbers are default values and are configurable.

    The following items can be configured via the definition JSON or via
    QuestionSet:
        giving_years: (int)
            Only consider contributions made within the last given number of
            years when determine whether a contact is a diamond in the rough.
            The default is 3 years.

        giving_client_threshold: (float)
            The maximum amount a contact can give to the institution during the
            giving_years timeframe and still be considered a diamond in the
            rough. The default value is $1k

        giving_aggregate_threshold: (float)
            The minimum amount a contact must give in aggregate during the
            giving_years timeframe in order to be considered a diamond in the
            rough. The default value is $10k

        check_threshold: (float)
            The minimum amount a contact must give to the institution within
            the check_years timeframe in order to be considered. For example
            there's no point in considering a contact if they haven't given
            anything (the default) to the institution recently.

        check_years: (int)
            Number of years to go back when considering contributions to
            determine if the contact has ever given to the institution.
            Typically check_years is larger than giving_years. The default
            value is 5 years.
    """
    tag_name = 'aca-ditr'

    def __init__(self, *args, **kwargs):
        super(AcademicDiamondsInTheRough, self).__init__(*args, **kwargs)
        self.record_set_config_id = int(self.get_setting(
            'record_set_config_id', -1))
        if self.record_set_config_id == -1:
            # This is not a vital piece of the analysis,
            # so we will only warn about it.
            LOGGER.warn("Academic DitR RecordSetConfig id is missing. "
                        "Configure via QuestionSet")

        self.giving_client_threshold = float(self.get_setting(
            'giving_client_threshold', 1000))
        self.giving_aggregate_threshold = float(self.get_setting(
            'giving_aggregate_threshold', 10000))
        self.check_threshold = float(self.get_setting('check_threshold', 0))

        self.giving_years = int(self.get_setting('giving_years', 3))
        self.check_years = int(self.get_setting('check_years', 5))

        self.giving_start_date = (datetime.date.today() -
                                  datetime.timedelta(
                                      days=self.giving_years*365))
        self.check_start_date = (datetime.date.today() -
                                 datetime.timedelta(days=self.check_years*365))

    def run(self, pipeline, cached_record_signals, *args, **kwargs):
        client_givings = 0.0
        aggregate_givings = 0.0
        check_total = 0.0

        # Iterate over the various giving records and extract their
        # giving amounts. The giving amounts will be used to build a mean.
        for (rsc_id, record_signals) in cached_record_signals.items():
            _giving_total = 0.0
            _check_total = 0.0
            for (record, signal) in record_signals:
                giving = signal.get("trans_amt")
                giving_date = signal.get("trans_date")
                if not (giving and giving_date):
                    continue
                if giving_date > self.giving_start_date:
                    _giving_total += giving
                elif giving_date > self.check_start_date:
                    _check_total += giving

            if rsc_id == self.record_set_config_id:
                # If the records we just iterated over came from the same
                # record set config that is associated with our signal set
                # config, it is client data, and we need to record that
                # separately.
                client_givings = _giving_total
                # If this is client data, then we also need to record the
                # check_total
                check_total = _check_total
            aggregate_givings += _giving_total

        if (check_total > self.check_threshold and
                client_givings <= self.giving_client_threshold and
                aggregate_givings >= self.giving_aggregate_threshold):
            pipeline.tags.add(self.tag_name)

        return pipeline


@public
class HighPotentialDonor(ConfigurableSignalAction):
    """High Potential Donor is similar to Diamond in the Rough, in that it is a
    contact that tends to give a lot. The difference is that we do not track
    donation amounts made to the client.

    There is a short term donation threshold and a long term donation threshold
    that must be met or exceeded in order for a contact to be considered a high
    potential donor.

    The following items can be configured via the definition JSON or via
    QuestionSet:
        long_term_year: (int)
            When determining whether a contact exceeds the long term threshold,
            only consider contributions made since January 1 of this year. If
            this value was 2007, we would consider all contributions made since
            2007/1/1. The default value is 2000.

        long_term_threshold: (float)
            Dollar amount contributions made during the long term period must
            exceed. The default value is $20k

        short_term_years: (int)
            Only consider contributions made within the last given number of
            years when determining whether a contact is an HPD. The default is
            4 years.

        short_term_threshold: (float)
            Dollar amount contributions made during the short term period must
            exceed. The default value is $10k
    """
    tag_name = 'high-potential-donor'

    def __init__(self, *args, **kwargs):
        super(HighPotentialDonor, self).__init__(*args, **kwargs)
        self.long_term_year = int(self.get_setting('long_term_year', 2000))
        self.long_term_threshold = float(self.get_setting(
            'long_term_threshold', 20000))
        self.short_term_years = int(self.get_setting('short_term_years', 4))
        self.short_term_threshold = float(self.get_setting(
            'short_term_threshold', 10000))

        self.short_term_start_date = (datetime.date.today() -
                                      datetime.timedelta(
                                          days=self.short_term_years*365))
        self.long_term_start_date = datetime.date(self.long_term_year, 1, 1)

    def run(self, pipeline, cached_record_signals, *args, **kwargs):
        short_term_givings = 0
        long_term_givings = 0
        for (rsc_id, record_signals) in cached_record_signals.items():
            for (record, signal) in record_signals:
                giving = signal.get("trans_amt")
                giving_date = signal.get("trans_date")
                if not (giving and giving_date):
                    continue
                if giving_date > self.short_term_start_date:
                    short_term_givings += giving
                elif giving_date > self.long_term_start_date:
                    long_term_givings += giving

        long_term_givings += short_term_givings

        if (short_term_givings >= self.short_term_threshold and
                long_term_givings >= self.long_term_threshold):
            pipeline.tags.add(self.tag_name)

        return pipeline


@public
class GivingHistoryTagging(ConfigurableSignalAction):
    class TagNames:
        non_client_giving = 'non-client-giving'
        client_giving = 'client-giving'
        client_giving_this_year = 'client-giving-this-year'

    def __init__(self, *args, **kwargs):
        super(GivingHistoryTagging, self).__init__(*args, **kwargs)
        self.client_record_set_config_id = int(self.get_setting(
            'client_record_set_config_id', -1))
        if self.client_record_set_config_id == -1:
            # This is not a vital piece of the analysis, and is not widely
            # used, so we will only warn about it.
            LOGGER.warn("GivingHistoryTagging RecordSetConfig id is missing. "
                        "Configure via QuestionSet")

        self.start_date = (datetime.date.today() -
                           datetime.timedelta(days=365))

    def run(self, pipeline, cached_record_signals, *args, **kwargs):
        non_client_giving = False
        client_giving = False
        client_giving_this_year = False
        for (rsc_id, record_signals) in cached_record_signals.items():
            is_client_data = rsc_id == self.client_record_set_config_id
            if not is_client_data and non_client_giving:
                # If non_client_giving is true, and the records come from a non
                # client source, then just skip further processing since we
                # already have our answer.
                continue

            for (record, signal) in record_signals:
                giving = signal.get("trans_amt")
                giving_date = signal.get("trans_date")
                if not (giving and giving > 0 and giving_date):
                    continue
                if is_client_data:
                    client_giving = True
                    if giving_date > self.start_date:
                        client_giving_this_year = True
                        break
                else:
                    non_client_giving = True
                    break

            if non_client_giving and client_giving and client_giving_this_year:
                break

        if non_client_giving:
            pipeline.tags.add(self.TagNames.non_client_giving)
        if client_giving:
            pipeline.tags.add(self.TagNames.client_giving)
        if client_giving_this_year:
            pipeline.tags.add(self.TagNames.client_giving_this_year)
        return pipeline


@public
class ClientTopDonor(ConfigurableSignalAction):
    """
    The largest donors are defined as: "Person is one of my top 1000 donors as
    identified on my institution's file". This information will need to be
    provided as a flag in the bio data for the institution.

    Requires the value of that flag in the bio data to have been copied over
    into the signal with the key 'client_largest_donor' in a previous step.

    **NOTE**
    Purdue sets the 'client-largest-donor' tag in the PurdueUpdateContact
    SignalAction. If the tag name for top donor changes here, make sure to
    update it in that class as well.
    """
    tag_name = 'client-largest-donor'

    def __init__(self, *args, **kwargs):
        super(ClientTopDonor, self).__init__(*args, **kwargs)
        self.client_bio_record_set_config_id = int(self.get_setting(
            'client_bio_record_set_config_id', -1))
        if self.client_bio_record_set_config_id == -1:
            # This is not a vital piece of the analysis,
            # so we will only warn about it.
            LOGGER.warn("ClientTopDonor RecordSetConfig id is missing. "
                        "Configure via QuestionSet")

    def run(self, pipeline, cached_record_signals, *args, **kwargs):
        is_top_donor = False
        record_signals = cached_record_signals.get(
            self.client_bio_record_set_config_id)
        if record_signals:
            for (record, signal) in record_signals:
                if signal.get('client_largest_donor'):
                    is_top_donor = True
                    break

        if is_top_donor:
            pipeline.tags.add(self.tag_name)
        return pipeline
