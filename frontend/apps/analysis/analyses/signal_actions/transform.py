import logging
import datetime
import re

from funcy import omit, get_in

from frontend.apps.analysis.analyses.base import SignalAction
from frontend.libs.utils.general_utils import public


LOGGER = logging.getLogger(__name__)


@public
class FECDateTransform(SignalAction):

    def run(self, pipeline, *args, **kwargs):

        """This method creates a generator that adds a cleaned field trans_date
        to the record

        It searches first for the TRANS_DATE and uses the contribution date
        converted into a datetime.  If not found it then derives the year from
        the FEC filename in the record.  If those are not found it uses
        January 1, 1776 as the date of the contribution and logs an error.
        """

        def _fallback_to_filename_fec(record, signal):
            if 'filename' in record:
                file_name = record['filename']
                year_re = re.compile(r'indiv(\d{2})\.txt')
                year = int("20" + year_re.match(file_name).groups()[0])
                signal['trans_date'] = datetime.date(year, 1, 1)
            else:
                logging.error("No valid trans_date could be created from",
                              " record.  Setting date to 1776")
                signal['trans_date'] = datetime.date(1776, 1, 1)
            yield record, signal

        def _transform_date(records_signals):
            for (record, signal) in records_signals:
                if 'TRANS_DATE' in record:
                    try:
                        signal['trans_date'] = (record['TRANS_DATE']).date()
                    except AttributeError:
                        logging.warn(
                            "TRANS_DATE not the right type, falling back")
                        record, signal = _fallback_to_filename_fec(
                            record, signal)
                else:
                    record, signal = _fallback_to_filename_fec(record, signal)
                yield record, signal

        pipeline.records_signals = _transform_date(pipeline.records_signals)
        return pipeline


@public
class MISPDateTransform(SignalAction):

    def run(self, pipeline, *args, **kwargs):

        """This method transforms the st record date into python datetime

        It searches the record for a CSF_Date and uses the contribution date
        as the date of the transaction if it exists.

        Otherwise it uses the year of the
           - FilingYear
           - ElectionYear
        fields and sets trans_date to January 1st of that year.

        It finally falls back to 1776 if none of those exist
        """

        def _fallback_to_1776():
            logging.error("No valid trans_date could be ",
                          "created from record.  Setting date",
                          "to 1776")
            return 1776

        def _fallback_to_election_year(record):
            if 'ElectionYear' in record and \
                    record['ElectionYear'] is not None and \
                    isinstance(record['ElectionYear'], (bytes, str)):
                try:
                    year = int(record['ElectionYear'])
                except ValueError:
                    year = _fallback_to_1776()
            else:
                year = _fallback_to_1776()
            return year

        def _fallback_to_filing_year(record):
            if 'FilingYear' in record and \
                    record['FilingYear'] is not None and \
                    type(record['FilingYear']) in [str, str]:
                try:
                    year = int(record['FilingYear'])
                except ValueError:
                    year = _fallback_to_election_year(record)
            else:
                year = _fallback_to_election_year(record)
            return year

        def _transform_date(records_signals):
            for (record, signal) in records_signals:
                if ('CFS_Date' in record and record['CFS_Date'] is not None and
                        type(record['CFS_Date']) is datetime.datetime):
                    try:
                        signal['trans_date'] = (record['CFS_Date']).date()
                    except (KeyError, ValueError):
                        year = _fallback_to_filing_year(record)
                        signal['trans_date'] = datetime.date(year, 1, 1)
                else:
                    year = _fallback_to_filing_year(record)
                    signal['trans_date'] = datetime.date(year, 1, 1)

                yield record, signal

        pipeline.records_signals = _transform_date(pipeline.records_signals)
        return pipeline


@public
class AmountTransform(SignalAction):

    def __init__(self, *args, **kwargs):
        super(AmountTransform, self).__init__(*args, **kwargs)
        self.field = self.definition['field']

    def run(self, pipeline, *args, **kwargs):

        def _transform_amt(records_signals):
            """ This value transforms the amount into a uniform format """
            for (record, signal) in records_signals:
                signal['trans_amt'] = float(record[self.field])
                yield record, signal

        pipeline.records_signals = _transform_amt(pipeline.records_signals)
        return pipeline


@public
class DateTransform(SignalAction):

    def __init__(self, *args, **kwargs):
        super(DateTransform, self).__init__(*args, **kwargs)
        self.field = self.definition['field']
        self.epoch_time = self.definition.get('epoch_time', False)

    def run(self, pipeline, *args, **kwargs):
        def _idempotent_date(date):
            if isinstance(date, datetime.datetime):
                return date.date()
            return date

        def _transform_date(records_signals):
            """ This value transforms the amount into a uniform format """
            for (record, signal) in records_signals:
                if self.epoch_time:
                    signal['trans_date'] = (datetime.datetime.utcfromtimestamp(
                        record[self.field])).date()
                else:
                    signal['trans_date'] = _idempotent_date(record[self.field])
                yield record, signal

        pipeline.records_signals = _transform_date(pipeline.records_signals)
        return pipeline


@public
class MISPGrouping(SignalAction):

    def __init__(self, *args, **kwargs):
        super(MISPGrouping, self).__init__(*args, **kwargs)
        self.minimum_length = self.definition['minimum_length']
        self.maximum_amount = self.definition['maximum_amount']

    def run(self, pipeline, *args, **kwargs):

        def _group(records_signals):
            prev_entity_id = ''
            prev_amt = 0.0
            group = []
            group_results = []

            def _sorting_key(records_signals):
                (record_item, signal_item) = records_signals
                return (
                    record_item.get('ElectionEntityEID', ''),
                    signal_item['trans_amt'])
            sorted_by_runs = sorted(records_signals, key=_sorting_key)

            for (record, signal) in sorted_by_runs:
                entity_id = record.get('ElectionEntityEID', '')
                if record is None:
                    break
                amount = signal['trans_amt']
                tmp_delta = abs(prev_amt - amount)

                if ((prev_entity_id != entity_id or
                        tmp_delta > 1.0 or amount > self.maximum_amount) and
                        group):

                    # Process record and signal
                    group_results.append(_process_group(group))

                    # start new run
                    group = [(record, signal)]
                    prev_entity_id = entity_id
                    prev_amt = amount
                else:
                    group.append((record, signal))
            if len(group) > 0:
                group_results.append(_process_group(group))
            return [record_signal
                    for group_result in group_results
                    for record_signal in group_result]

        def _process_group(group):
            if len(group) < self.minimum_length:
                return [record_signal for record_signal in group]
            else:
                new_record = []
                new_signal = self.build_signal()
                new_signal['match'] = []
                # expansion_hit is used in debugging to determine whether a
                # contribution record was discovered as a result of name or
                # alias expansion.
                new_signal['expansion_hit'] = []
                new_signal['trans_amt'] = 0
                new_signal['grouped'] = []
                new_dates = []
                for (record, signal) in group:
                    new_signal['match'].append(signal['match'])
                    if 'expansion_hit' in signal:
                        new_signal['expansion_hit'].append(
                            signal['expansion_hit'])
                    new_signal['trans_amt'] += signal['trans_amt']
                    new_signal['multipliers'] = signal['multipliers']
                    new_dates.append(signal['trans_date'])
                    new_signal['grouped'].append((record, signal))

                new_dates.sort()
                new_signal['start_date'] = new_dates[0]
                new_signal['end_date'] = new_dates[-1]
                return [(new_record, new_signal)]

        pipeline.records_signals = _group(pipeline.records_signals)
        return pipeline


@public
class PartitionSignals(SignalAction):

    def run(self, pipeline, partition_config=None, *args, **kwargs):

        def _after_start(records_signals):
            for (record, signal) in records_signals:
                partition_signal = signal['partitions'][partition_config.id]
                if 'grouped' in signal:
                    partition_signal['grouped'] = []
                    # expansion_hit is used in debugging to determine whether a
                    # contribution record was discovered as a result of name or
                    # alias expansion.
                    partition_signal['expansion_hit'] = []
                    partition_signal['match'] = []
                    partition_signal['in_partition'] = False
                    for (r, s) in signal['grouped']:
                        new_signal = s.copy()
                        new_signal['match'] = s['match']
                        if 'expansion_hit' in s:
                            new_signal['expansion_hit'] = s['expansion_hit']
                        if s['trans_date'] > partition_config.start_date:
                            new_signal['in_partition'] = True
                            partition_signal['match'].append(s['match'])
                            if 'expansion_hit' in s:
                                partition_signal['expansion_hit'].append(
                                    s['expansion_hit'])
                            partition_signal['in_partition'] = True
                        else:
                            new_signal['in_partition'] = False
                        partition_signal['grouped'].append((
                            r, new_signal))
                else:
                    if signal['trans_date'] > partition_config.start_date:
                        partition_signal.update(omit(signal, ['partitions']))
                        partition_signal['in_partition'] = True
                    else:
                        partition_signal['in_partition'] = False
                yield record, signal

        pipeline.records_signals = _after_start(pipeline.records_signals)
        return pipeline


@public
class FieldCopy(SignalAction):
    valid_types = {
        "bool": bool,
        "float": float
    }

    def __init__(self, *args, **kwargs):
        super(FieldCopy, self).__init__(*args, **kwargs)
        self.mappings = self.definition['mappings']

    def run(self, pipeline, *args, **kwargs):

        def _copy_fields(records_signals):
            for (record, signal) in records_signals:
                for mapping in self.mappings:
                    source_field = mapping['source_field']
                    target_field = mapping['target_field']
                    try:
                        value = record[source_field]
                        signal[target_field] = value

                        # Cast the value to a specific type. e.g. float, bool
                        typecast_type = mapping.get("typecast")
                        if typecast_type:
                            typecast_type = self.valid_types[typecast_type]
                            try:
                                value = typecast_type(value)
                            except TypeError:
                                continue

                        if mapping.get("key_signal", False):
                            pipeline.key_signals[target_field] = value

                    except KeyError:
                        pass
                yield record, signal

        pipeline.records_signals = _copy_fields(pipeline.records_signals)
        return pipeline


@public
class CopyFromEntity(SignalAction):
    """Copy values from resolved entities

    Configuration options are the same as `FieldCopy` with one exception:
    `source_field` must be a list instead of a string. The reason for this
    change is to enable copying nested values.

    In the future will support generating new records_signals from the copied
    values. Currently only supports copying values to `key_signals`, which is
    disabled by default at the moment (to prevent any surprises when
    records_signals support is added and the default).
    """
    valid_types = {
        "bool": bool,
        "float": float,
        "int": int,
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.mappings = self.definition['mappings']

    def run(self, pipeline, *args, **kwargs):
        if not hasattr(pipeline.target, 'entity_resolver'):
            # Abort if the entity_resolver attribute is missing
            return pipeline
        entity_resolver = pipeline.target.entity_resolver
        record = {}
        signal = {}

        for mapping in self.mappings:
            source_field = mapping['source_field']
            target_field = mapping['target_field']
            if isinstance(source_field, str):
                source_field = [source_field]

            value = get_in(entity_resolver, source_field, KeyError)

            if value is KeyError:
                # XXX: log?
                continue

            # Cast the value to a specific type. e.g. float, bool
            typecast_type = mapping.get("typecast")
            if typecast_type and typecast_type in self.valid_types:
                typecast_type = self.valid_types[typecast_type]
                try:
                    value = typecast_type(value)
                except TypeError:
                    continue

            if mapping.get("key_signal", False):
                pipeline.key_signals[target_field] = value
            # Add all copied fields to a single signal.
            signal[target_field] = value
            # Update record with the latest info in the signal
            record.update(signal)

        pipeline.records_signals.append((record, signal,))

        return pipeline
