import datetime
from itertools import repeat
import logging

from django.conf import settings

from frontend.apps.analysis.analyses.base import (
    SignalAction, SummarySignalActionMixin, ConfigurableSignalAction)
from frontend.libs.utils import election_utils
from frontend.libs.utils.general_utils import public, BlankObject


LOGGER = logging.getLogger(__name__)


@public
class AddMultiplier(SignalAction):

    def __init__(self, *args, **kwargs):
        super(AddMultiplier, self).__init__(*args, **kwargs)
        self.name = self.definition['name']
        self.value = self.definition['value']

    def run(self, pipeline, *args, **kwargs):

        def _apply_multiplier(records_signals):
            for (record, signal) in records_signals:
                signal['multipliers'][self.name] = self.value
                yield record, signal

        pipeline.records_signals = _apply_multiplier(pipeline.records_signals)
        return pipeline


@public
class ConditionalAddMultiplier(AddMultiplier, ConfigurableSignalAction):
    """Conditionally adds an arbitrary multiplier

    Adds an arbitrary multiplier (defined in SAC definition) to every
    contribution seen when enabled.

    Whether the SignalAction is enabled or not is based on whether a
    configurable value pulled from QSD is found in either a preconfigured
    whitelist or blacklist.

    To give an example, if the multiplier should only be active for state
    elections: set `conditional_setting_name` to `office_category`, and add
    `Federal` to the `blacklist_values` and `State` to the `whitelist_values`.

    Here is the conditional logic that is employed (earlier rules take
    precedence over later rules):
    - If the QSD setting looked at is unset, the multiplier is enabled.
    - If the value from QSD is present in the blacklist, the multiplier is
      disabled.
    - If the whitelist is empty, the multiplier is enabled.
    - If the value is present in the whitelist, the multiplier is enabled;
      otherwise it is disabled.
    """
    def __init__(self, *args, **kwargs):
        super(ConditionalAddMultiplier, self).__init__(*args, **kwargs)
        setting_name = self.definition.get('conditional_setting_name')
        if not setting_name:
            self.enabled = False
        else:
            conditional_value = self.get_setting(setting_name,
                                                 default=KeyError)
            whitelist_values = self.definition.get('whitelist_values', [])
            blacklist_values = self.definition.get('blacklist_values', [])
            if conditional_value is KeyError or not any([whitelist_values,
                                                         blacklist_values]):
                enabled = False
            elif conditional_value in blacklist_values:
                enabled = False
            elif not whitelist_values:
                enabled = True
            else:
                enabled = conditional_value in whitelist_values
            self.enabled = enabled

    def run(self, pipeline, *args, **kwargs):
        if self.enabled:
            pipeline = super(ConditionalAddMultiplier, self).run(
                pipeline, *args, **kwargs)
        return pipeline


# TODO: Make more generic/flexible
@public
class PartyMultiplier(ConfigurableSignalAction):

    def __init__(self, *args, **kwargs):
        super(PartyMultiplier, self).__init__(*args, **kwargs)
        self.party = self.get_setting('party', 'OTHER')
        self.ally_multiplier = float(getattr(
            self.definition, 'ally_multiplier', 1.9))
        self.opponent_multiplier = float(getattr(
            self.definition, 'opponent_multiplier', 0.1))
        self.neutral_multiplier = float(getattr(
            self.definition, 'neutral_multiplier', 1.0))
        if self.party == "Democratic":
            self.party_multiplier = {
                'DEM': self.ally_multiplier,
                'OTHER': self.neutral_multiplier,
                'REP': self.opponent_multiplier
            }
        elif self.party == 'Republican':
            self.party_multiplier = {
                'REP': self.ally_multiplier,
                'OTHER': self.neutral_multiplier,
                'DEM': self.opponent_multiplier
            }
        else:
            self.party_multiplier = {
                'REP': self.neutral_multiplier,
                'OTHER': self.neutral_multiplier,
                'DEM': self.neutral_multiplier
            }

    def run(self, pipeline, *args, **kwargs):

        def _apply_multiplier(records_signals):
            for (record, signal) in records_signals:
                party = signal.get('party', 'OTHER')
                multiplier = self.party_multiplier.get(
                    party, self.neutral_multiplier)
                signal['multipliers']['party_multiplier'] = multiplier
                yield record, signal

        pipeline.records_signals = _apply_multiplier(pipeline.records_signals)
        return pipeline


# DEPRECATED
@public
class CommitteeMultipliers(SignalAction):

    def __init__(self, *args, **kwargs):
        super(CommitteeMultipliers, self).__init__(*args, **kwargs)
        self.filters = {}
        for committee_filter in self.definition['filters']:
            for filter_id in committee_filter['ids']:
                self.filters[filter_id] = committee_filter['multiplier']
        self.filter_field = self.definition['filter_field_name']

    def run(self, pipeline, *args, **kwargs):

        def _apply_multiplier(records_signals):
            for (record, signal) in records_signals:
                filter_id = record[self.filter_field]
                if filter_id in self.filters:
                    multiplier = self.filters[filter_id]
                    signal['multipliers']['committee_multiplier'] = multiplier
                yield record, signal

        pipeline.records_signals = _apply_multiplier(pipeline.records_signals)
        return pipeline


# DEPRECATED, obviated by OfficeMultiplier
@public
class FECOfficeMultiplier(SignalAction):

    def __init__(self, *args, **kwargs):
        super(FECOfficeMultiplier, self).__init__(*args, **kwargs)
        self.office = self.definition['office']
        default_office_map = {
            "H": {
                "H": 1.75,
                "S": 1.25,
                "P": 1,
            },
            "S": {
                "H": 1.25,
                "S": 1.5,
                "P": 1,
            },
            "P": {
                "H": 1.25,
                "S": 1.25,
                "P": 1,
            },
        }
        self.office_map = self.definition.get('office_map',
                                              default_office_map)

    def run(self, pipeline, *args, **kwargs):

        def _apply_multiplier(records_signals):
            for (record, signal) in records_signals:
                if 'office' in signal:
                    multiplier = self.office_map[self.office][
                        signal['office']]
                    signal['multipliers']['office_multiplier'] = multiplier
                yield record, signal

        pipeline.records_signals = _apply_multiplier(pipeline.records_signals)
        return pipeline


# DEPRECATED, obviated by OfficeMultiplier
@public
class StateOfficeMultiplier(SignalAction):

    def __init__(self, *args, **kwargs):
        super(StateOfficeMultiplier, self).__init__(*args, **kwargs)
        self.office = self.definition['office']
        default_office_map = {
            "G": {
                "G": 1.5,
            },
        }
        self.office_map = self.definition.get('office_map',
                                              default_office_map)

    def run(self, pipeline, *args, **kwargs):

        def _apply_multiplier(records_signals):
            for (record, signal) in records_signals:
                if 'office' in signal and signal['office']:
                    multiplier = self.office_map[self.office].get(
                        signal['office'], 1.0)
                    signal['multipliers'][
                        'state_office_multiplier'] = multiplier
                yield record, signal

        pipeline.records_signals = _apply_multiplier(pipeline.records_signals)
        return pipeline


@public
class OfficeMultiplier(ConfigurableSignalAction):
    DEFAULT_OFFICE_MAP = {
        'Federal': {
            "House": {
                "H": 1.75,
                "S": 1.25,
                "P": 1,
            },
            "Senate": {
                "H": 1.25,
                "S": 1.5,
                "P": 1,
            },
            "President": {
                "H": 1.25,
                "S": 1.25,
                "P": 1,
            },
        },
        'State': {
            "Governor": {
                "G": 2.0,
            },
            #"H": {
                #"H": 2.0,
            #},
            #"O": {
                #"O": 2.0,
            #},
        },
    }

    def __init__(self, *args, **kwargs):
        super(OfficeMultiplier, self).__init__(*args, **kwargs)
        self.office = self.get_setting('office', 'other')
        self.office_category = self.get_setting('office_category', 'other')
        if self.feature_config.feature.title == '{} Matches'.format(
                self.office_category):
            self.office_map = self.definition.get(
                'office_map', self.DEFAULT_OFFICE_MAP.get(
                    self.office_category, {})).get(self.office, {})
        else:
            self.office_map = {}

    def run(self, pipeline, *args, **kwargs):
        def _apply_multiplier(records_signals):
            for (record, signal) in records_signals:
                multiplier = self.office_map.get(signal.get('office'))
                if multiplier is not None and multiplier != 1.0:
                    signal['multipliers']['office_multiplier'] = multiplier
                yield record, signal

        if self.office_map:
            pipeline.records_signals = _apply_multiplier(
                pipeline.records_signals)
        return pipeline


@public
class ApplyGivingFactors(SignalAction):

    def run(self, pipeline, partition_config=None, *args, **kwargs):

        def _apply_factors(records_signals):
            for (record, signal) in records_signals:
                partition_signal = signal['partitions'][partition_config.id]
                if 'base_giving' in partition_signal:
                    adjusted_giving = partition_signal['base_giving']
                    multipliers = signal['multipliers']
                    for multiplier in [multipliers[i] for i in multipliers]:
                        adjusted_giving *= multiplier
                    partition_signal['adjusted_giving'] = adjusted_giving
                yield record, signal

        pipeline.records_signals = _apply_factors(pipeline.records_signals)
        return pipeline


@public
class ApplyScoreFactors(SignalAction):

    def run(self, pipeline, partition_config=None, *args, **kwargs):

        def _apply_factors(records_signals):
            for (record, signal) in records_signals:
                partition_signal = signal['partitions'][partition_config.id]
                if 'score' in partition_signal:
                    score = partition_signal['score']
                    multipliers = signal['score_multipliers']
                    for multiplier in [multipliers[i] for i in
                                       multipliers]:
                        score *= multiplier
                    partition_signal['score'] = score
                yield record, signal

        pipeline.records_signals = _apply_factors(pipeline.records_signals)
        return pipeline


@public
class EthnicityBoost(SummarySignalActionMixin, ConfigurableSignalAction):
    """Boost score for a contact based on suspected ethnicity

    Boost can be configured in SAC definition, or via QuestionSet. This
    requires the target ethnicity for first and last name to be specified, as
    well as the boost amount.

    Configuration options:

    given_name_target:
        Given name origin language target. If this language is a found on a
        contact, we apply the given_name_boost.

        Valid targets for given name: english, afram, natam, spanish, basque,
        catalan, galacian, french, german, hindu, russian, persian, arabic,
        japanese, chinese, viet, korean, yiddish, hebrew, latin, greek

    given_name_boost:
        How much to boost contact's score if given_name_target is an origin for
        a contact's given name. (The contact's score is multipled by this
        amount).

    family_name_target:
        Given name ethnicity target. This is the ethnicity we are boosting.

        Valid targets for family name: white, black, hispanic, asian, native,
        multirace.

    family_name_boost:
        How much to boost contact's score if a match is made. (The contact's
        score is multiplied by this amount).

    family_name_boost_threshold:
        Family name ethnicity in peacock data is a distribution of ethnicity of
        the people who have that name (e.g. 90% of the people who have the
        family name "Smith" are white). Contact must have a family name that is
        family_name_boost_threshold or higher in order for a match to be made.

    ethnicity_base_increase:
        Base increase boost to give to contact if a match is made against
        family_name_target or given_name_target. (This is added to the
        contact's score)
    """
    def __init__(self, *args, **kwargs):
        super(EthnicityBoost, self).__init__(*args, **kwargs)
        self.given_name_target = self.get_setting('given_name_target', None)
        self.given_name_boost = self.get_setting('given_name_boost', 0.0)
        self.family_name_target = self.get_setting('family_name_target', None)
        self.family_name_boost = self.get_setting('family_name_boost', 0.0)
        self.family_name_threshold = self.get_setting(
            'family_name_boost_threshold', default=70)
        self.base_increase = self.get_setting('ethnicity_base_increase',
                                              default=0)

    def run(self, pipeline, *args, **kwargs):

        def _ethnicity_boost(contact):
            family_name_ethnicity = contact.get_family_name_ethnicity()
            given_name_origin = contact.get_given_name_origin()
            pipeline.key_signals.update(
                {'given_name_origin': given_name_origin,
                 'family_name_ethnicity': family_name_ethnicity})
            boost = 1.0
            if (family_name_ethnicity and family_name_ethnicity.get(
                    self.family_name_target, 0) >= self.family_name_threshold):
                boost += self.family_name_boost

            if given_name_origin and given_name_origin.get(
                    self.given_name_target):
                boost += self.given_name_boost

            if boost > 0 and boost != 1.0:
                pipeline.key_signals['ethnicity_boost_multiplier'] = boost

                def score_boost(x):
                    return x * boost + self.base_increase
            else:
                def score_boost(x):
                    return x
            return score_boost

        partitions = self.signal_sets[0]['partitions']
        boost = _ethnicity_boost(pipeline.target)
        for signal_partition_id in partitions:
            partition_result = pipeline.results['partitions'][
                signal_partition_id]
            partition_result['score'] = boost(partition_result['score'])
        return pipeline


@public
class GenderBoost(SummarySignalActionMixin, ConfigurableSignalAction):
    """Boost score for a contact based on gender.

    Boost can be configured in SAC definition, or via QuestionSet.

    Configuration options:
    male_boost:
        How much to boost contact's score if contact is determined to likely be
        male. (The contact's score is multipled by this amount).

    female_boost:
        How much to boost contact's score if contact is determined to likely be
        female. (The contact's score is multipled by this amount).

    gender_base_increase:
        Base increase boost to give to contact if a gender boost is to be
        applied to score. (This is added to the contact's score)
    """
    def __init__(self, *args, **kwargs):
        super(GenderBoost, self).__init__(*args, **kwargs)
        self.male_boost = self.get_setting('male_boost', default=0.0)
        self.female_boost = self.get_setting('female_boost', default=0.0)
        self.base_increase = self.get_setting('gender_base_increase',
                                              default=0)

    def run(self, pipeline, *args, **kwargs):

        def _gender_boost(contact):
            gender = contact.get_preferred_gender()
            pipeline.key_signals['gender'] = gender
            if gender == 'M':
                boost = self.male_boost
            elif gender == 'F':
                boost = self.female_boost
            else:
                boost = 0

            if boost > 0:
                pipeline.key_signals['gender_boost_multiplier'] = boost

                def score_boost(x):
                    return x * boost + self.base_increase
            else:
                def score_boost(x):
                    return x
            return score_boost

        partitions = self.signal_sets[0]['partitions']
        boost = _gender_boost(pipeline.target)
        for signal_partition_id in partitions:
            partition_result = pipeline.results['partitions'][
                signal_partition_id]
            partition_result['score'] = boost(partition_result['score'])
        return pipeline


@public
class NameBoost(SummarySignalActionMixin, SignalAction):

    def __init__(self, *args, **kwargs):
        super(NameBoost, self).__init__(*args, **kwargs)
        self.ethnicity = self.definition.get('ethnicity', 'south asian')
        self.first_name_boost = self.definition.get('first_name_boost', 0.1)
        self.last_name_boost = self.definition.get('last_name_boost', 0.1)
        self.base_increase = self.definition.get('base_increase', 5.0)

    def run(self, pipeline, *args, **kwargs):

        def _name_boost(contact):
            """
            This function returns a function by which to boost the overall
            giving score based upon a name's suspected ethnicity.

            Currently, only south asian names are supported (from prototype),
            so the boost is only applied for a political campaign with a south
            asian candidate.
            """
            if self.ethnicity.lower() == "south asian":
                boost = _southasian_boost(
                    str(contact.last_name).upper(),
                    str(contact.first_name).upper(),
                    self.last_name_boost,
                    self.first_name_boost)
            else:
                boost = 1.0

            if boost > 1.0:
                def score_boost(x):
                    return x * boost + self.base_increase
            else:
                def score_boost(x):
                    return x
            return score_boost

        def _southasian_boost(last_name, first_name, last_name_boost,
                              first_name_boost):
            """ This temporary functions provides a boost if name
            heuristically appears south asian"""
            # APIslander Surname Boost --------------------------------------
            # Based upon our created document here
            # https://docs.google.com/a/revupsoftware.com/spreadsheets/d/
            # 1VcFc3qGJpJoqUPVvMcof9ozGhU3JjNZelu75kzwD-u8/edit#gid=70886726
            #
            # **** BE SURE TO RUN data/bin/update_southasian_names.sh FIRST!!!
            #
            # We're going to give a (5 pt boost + 100) for a "southasian"
            # forname and another (5pt boost + 100) if they have a "southasian"
            # surname replacing our earlier census percentages

            boost = 1.0
            _db = settings.MONGO_CLIENT.get_default_database()

            match_result = 0
            surname_result = _db.census_surnames.find_one({'_id': last_name})
            if (surname_result is not None and
                    'pctsouthasian' in list(surname_result.keys())):
                LOGGER.debug("Boosting surname: {} because it appears south "
                             "asian".format(last_name))
                boost += last_name_boost
                match_result += 2
            forename_dict = {'_id': first_name}
            forename_result = _db.census_forenames.find_one(forename_dict)
            if (forename_result is not None and
                    'pctsouthasian' in list(forename_result.keys())):
                LOGGER.debug("Boosting forename: {} because it appears south "
                             "asian".format(first_name))
                boost += first_name_boost
                match_result += 1

            # Track whether a south asian name match was made. If the value is
            # 1, then a match was made on the first name only. If the value is
            # 2, then a match was made on the last name only. If the value is
            # 3, then both the first and last name both appear to be south
            # asian.
            if match_result:
                pipeline.key_signals['pctsouthasian'] = match_result
            return boost

        partitions = self.signal_sets[0]['partitions']
        name_boost = _name_boost(pipeline.target)
        for signal_partition_id in partitions:
            partition_result = pipeline.results['partitions'][
                signal_partition_id]
            partition_result['score'] = name_boost(partition_result['score'])
        return pipeline


@public
class StateBoost(SummarySignalActionMixin, ConfigurableSignalAction):

    def __init__(self, *args, **kwargs):
        super(StateBoost, self).__init__(*args, **kwargs)
        self.state = self.get_setting('state', '')
        self.same_state_boost = self.definition.get('same_state_boost', 0.5)
        self.base_increase = self.definition.get('base_increase', 5.0)

    def run(self, pipeline, *args, **kwargs):

        def _proximity_boost(contact):
            """
            This function returns a function by which to boost the overall
            giving score based upon how close the contact's location is to the
            campaign or event.

            For now, we just check to see if the states are the same for
            campaign and contact.

            Todo:  make this more sophisticated by calculating some notion of
            distance.  To do that, we would need a location for the campaign
            and also for the event.
            """
            boost = _same_state_boost(contact.states, self.state,
                                      self.same_state_boost)
            if boost > 1.0:
                def score_boost(x):
                    return x * boost + self.base_increase
            else:
                def score_boost(x):
                    return x
            return score_boost

        def _same_state_boost(contact_states, campaign_state,
                              same_state_boost):
            """
                Returns a boost if the campaign_state matches any of the
                contact_states.
                :param contact_states:
                :param campaign_state:
                :return: 1.0 or some value greater than 1.0
            """
            boost = 1.0
            if campaign_state.lower().strip() in [contact_state.lower().strip()
                                                  for contact_state
                                                  in contact_states]:
                boost += same_state_boost
            return boost

        if self.state:
            partitions = self.signal_sets[0]['partitions']
            proximity_boost = _proximity_boost(pipeline.target)
            for signal_partition_id in partitions:
                partition_result = pipeline.results['partitions'][
                    signal_partition_id]
                partition_result['score'] = proximity_boost(
                    partition_result['score'])
        return pipeline


@public
class ApplyGlobalScoreFactors(SummarySignalActionMixin, SignalAction):
    """Apply global score multipliers to the summary/overall score.

    Multipliers are applied directly to the score. E.g.:
        A multiplier of 0.75 will make the score 75% of what it was.
    """
    def run(self, pipeline, *args, **kwargs):

        for label, multiplier in pipeline.score_multipliers.items():
            partitions = self.signal_sets[0]['partitions']
            for signal_partition_id in partitions:
                partition_result = pipeline.results['partitions'][
                    signal_partition_id]
                partition_result['score'] *= multiplier

        return pipeline


@public
class KeyContribsTagAndApplyMultiplier(ConfigurableSignalAction):
    """Find Key Contributions and apply scoring rules.

    The multipliers works with three tiers: current election, last election,
    and distant past. Each of the three tiers has a different impact on the
    score, with the current election having the most impact.
    The algorithm also takes into account positive and negative contributions.
    I.e. giving to an "allied" campaign vs an "opponent".

    This should be applied -after- the root feature scoring SignalAction.
    It won't work correctly if used before.

    Definition options:
        multipliers: A dict with multiplier values. There are six total
                     options available within multipliers. The following
                     are the options and their defaults:
                pos_current_multiplier: +20
                pos_last_multiplier:    +10
                pos_stale_multiplier:   +5

                neg_current_multiplier: -20
                neg_last_multiplier:    -10
                neg_stale_multiplier:   -5
    """
    mult_defaults = {
        "pos_current_multiplier": +20,
        "pos_last_multiplier": +10,
        "pos_stale_multiplier": +5,

        "neg_current_multiplier": -100,
        "neg_last_multiplier": -50,
        "neg_stale_multiplier": -10,
    }

    def __init__(self, record_set_config_id, record_set_backends,
                 *args, **kwargs):
        super(KeyContribsTagAndApplyMultiplier, self).__init__(*args, **kwargs)

        ## Setup for Key Contrib matching
        self.record_set = record_set_backends.get(record_set_config_id)
        self.match_field = self.record_set.record_resource.match_field
        self.values = []
        for label, is_ally, entities in self.config_wrapper.get_list(
                "label", "is_ally", "entities"):
            eids = set()
            if entities is None:
                entities = []
            for entity in entities:
                try:
                    rsc_id = int(entity["record_set_config"])
                    eid = entity["eid"]
                    # Get the lookup field, which may or may not match eid
                    # depending on the type of record set.
                    record_set = record_set_backends[rsc_id]
                    # Get a set of entity IDs that can be used to compare with
                    # contribution records for matches
                    eids.update(
                        record_set.record_resource.get_match_eids(eid))
                except KeyError:
                    continue
            self.values.append((label, is_ally, eids))

        ## Setup for Key Contrib scoring
        self.multipliers = self.definition.get("multipliers", {})
        # TODO: For now, we will use the default election time. In the near
        # future, we need to hook up QuestionSets for the client config
        #      to this one's parent FeatureConfig
        # "last_term" is the term before the one we are currently in
        self.last_term_end = election_utils.prev_default_election()
        # "prev_term" is the term before the last term
        self.prev_term_end = election_utils.prev_default_election(
            # Give a little padding in the days so we don't end up looking at
            # the same election twice.
            input_date=(self.last_term_end - datetime.timedelta(days=10))
        )

    def run(self, pipeline, *args, **kwargs):
        def _update_signal(signal_, label_, is_ally_):
            # Add the key contrib to the signal
            key_contrib = signal_.setdefault("key_contrib", {})
            key_contrib["label"] = label_
            key_contrib["is_ally"] = is_ally_

            # Apply the tag
            tag = "ally-giving" if is_ally_ else "oppo-giving"
            if tag not in pipeline.tags:
                pipeline.tags.add(tag)

        def _add_multiplier(signal_, is_ally_):
            try:
                date_ = signal_["trans_date"]
            except KeyError:
                # If trans_date isn't set, we can't score this record
                return

            # Get the score multiplier
            prefix = "pos" if is_ally_ else "neg"
            if date_ > self.last_term_end:
                key = "current"
            elif date_ > self.prev_term_end:
                key = "last"
            else:
                key = "stale"
            mult_key = "{}_{}_multiplier".format(prefix, key)
            multiplier = self.multipliers.get(mult_key,
                                              self.mult_defaults.get(mult_key))
            # Apply the multiplier
            try:
                signal_["score_multipliers"]["key_contrib"] = multiplier
            except KeyError:
                pass

        def _filter(records_signals):
            # We need to examine all records for key contrib matches
            for (record, signal) in records_signals:
                for label, is_ally, eids in self.values:
                    if 'grouped' in signal:
                        for (sub_record, sub_signal) in signal['grouped']:
                            if sub_record.get(self.match_field) in eids:
                                # TODO: Rethink this grouping unwind
                                _update_signal(sub_signal, label, is_ally)
                                # _add_multiplier(sub_signal, is_ally)
                                break
                    else:
                        if record.get(self.match_field) in eids:
                            _update_signal(signal, label, is_ally)
                            _add_multiplier(signal, is_ally)
                            break
                yield record, signal

        pipeline.records_signals = _filter(pipeline.records_signals)
        return pipeline


@public
class KeyContribsScoreMultipliers(ConfigurableSignalAction):
    """Analyze the key contributions for a given contact and select a
    multiplier that best fits the key contribution data.

    The multiplier is selected based on various conditionals. The
    mulitipliers for a given condition should be in the
    'available_multipliers' class attribute below.
    """
    neg_current = "neg_current"
    neg_last = "neg_last"
    neg_stale = "neg_stale"
    pos_current = "pos_current"
    pos_last = "pos_last"
    pos_stale = "pos_stale"

    # Available multipliers and their default values
    available_multipliers = (
        ("cur_neg_only", 0.5, "Current cycle, negative contrib only"),
        ("cur_neg_and_pos", 0.65, "Current cycle, negative and positive"),
        ("cur_mostly_neg", 0.6, "Current cycle neg, past cycles positive"),
        ("cur_pos_only", 1.3, "Current cycle, positive contrib only"),
        ("last_neg_only", 0.8, "Last cycle, negative contrib only"),
        ("last_neg_and_pos", 0.9, "Last cycle, negative and positive"),
        ("last_pos_only", 1.2, "Last cycle, positive contrib only"),
        ("other_neg", 0.9, "Any other scenario with negative contribs"),
        ("other_pos", 1.07, "Any other scenario with positive contribs"),
    )

    def __init__(self, *args, **kwargs):
        super(KeyContribsScoreMultipliers, self).__init__(*args, **kwargs)
        ## Setup for Key Contrib scoring
        # TODO: For now, we will use the default election time. In the near
        # future, we need to hook up QuestionSets for the client config
        #      to this one's parent FeatureConfig
        # "last_term" is the term before the one we are currently in
        self.last_term_end = election_utils.prev_default_election()
        # "prev_term" is the term before the last term
        self.prev_term_end = election_utils.prev_default_election(
            # Give a little padding in the days so we don't end up looking at
            # the same election twice.
            input_date=(self.last_term_end - datetime.timedelta(days=10))
        )
        self._init()

    def _init(self):
        self.features = self._prepare_features()
        self.multipliers = self._prepare_multipliers()

    def __getstate__(self):
        state = self.__dict__.copy()
        del state['features']
        del state['multipliers']
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        self._init()

    @classmethod
    def _prepare_features(cls):
        return {
            cls.neg_current: lambda d: (not d.is_ally and d.cycle_current),
            cls.neg_last: lambda d: (not d.is_ally and d.cycle_last),
            cls.neg_stale: lambda d: (not d.is_ally and d.cycle_old),
            cls.pos_current: lambda d: (d.is_ally and d.cycle_current),
            cls.pos_last: lambda d: (d.is_ally and d.cycle_last),
            cls.pos_stale: lambda d: (d.is_ally and d.cycle_old),
        }

    def _prepare_multipliers(self):
        """Define a struct that contains all of the available multipliers
        with either their defined values, or their default values.
        """
        # Builds a dict with key=setting-label and value=setting-value
        def get_settings(*settings):
            for setting, default, _ in settings:
                yield setting, float(self.get_setting(setting, default))

        return BlankObject(**{s: v for s, v in get_settings(
            *self.available_multipliers)})

    def _evaluate_multiplier(self, feature_hits):
        """Evaluate the conditions to find the appropriate multiplier.

        The following code should be treated like a 'switch' statement.
        """
        def comp(condition):
            return condition in feature_hits

        any_negative = bool({self.neg_current, self.neg_last,
                             self.neg_stale}.intersection(feature_hits))
        any_positive = bool({self.pos_current, self.pos_last,
                             self.pos_stale}.intersection(feature_hits))

        if comp(self.neg_current) and not any_positive:
            # Current cycle, negative contrib only
            return self.multipliers.cur_neg_only
        elif comp(self.neg_current) and comp(self.pos_current):
            # Current cycle, negative and positive
            return self.multipliers.cur_neg_and_pos
        elif comp(self.neg_current):
            # Current cycle negative, past cycle positive
            return self.multipliers.cur_mostly_neg
        elif comp(self.pos_current) and not any_negative:
            # Current cycle, positive contrib only
            return self.multipliers.cur_pos_only
        elif comp(self.neg_last) and not any_positive:
            # Last cycle, negative contrib only
            return self.multipliers.last_neg_only
        elif comp(self.neg_last) and comp(self.pos_last):
            # Last cycle, negative and positive
            return self.multipliers.last_neg_and_pos
        elif comp(self.pos_last) and not any_negative:
            # Last cycle, positive contrib only
            return self.multipliers.last_pos_only
        elif any_negative:
            # Any other scenario with negative contribs
            return self.multipliers.other_neg
        elif any_positive:
            # Any other scenario with positive contribs
            return self.multipliers.other_pos

    def _prepare_data(self, record_, signal_):
        """Prepare the data to be used by the conditionals.
        This is mostly to keep things DRY and a slightly more efficient.
        """
        try:
            date_ = signal_["trans_date"]
        except KeyError:
            # If trans_date isn't set, we can't score this record
            return

        # Get the contribution cycle/age
        if date_ > self.last_term_end:
            current = True
            last = False
            stale = False
        elif date_ > self.prev_term_end:
            current = False
            last = True
            stale = False
        else:
            current = False
            last = False
            stale = True

        return BlankObject(
            cycle_current=current,
            cycle_last=last,
            cycle_old=stale,
            is_ally=signal_['is_ally'],
            # TODO: We'll eventually want to add 'maxed-out', among others
        )

    def run(self, pipeline, *args, **kwargs):
        feature_hits = set()

        def check_features(data):
            for label, feature in self.features.items():
                # Check each feature and see if it matches the data.
                # It's more performant to skip the condition if it is
                # already represented
                if label not in feature_hits and feature(data):
                    feature_hits.add(label)

        def _evaluate(records_signals):
            # We need to examine all key contrib matches to find which
            # conditionals are met.
            for record_signal in records_signals:
                record, signal = record_signal
                data = self._prepare_data(record, signal)
                if data:
                    check_features(data)
                yield record_signal

            # Assess the conditions and save the multiplier, if there is one
            multiplier = self._evaluate_multiplier(feature_hits)
            if multiplier is not None:
                pipeline.score_multipliers["key_contribs"] = multiplier

        pipeline.records_signals = _evaluate(pipeline.records_signals)
        return pipeline


@public
class FetchWeights(ConfigurableSignalAction):
    """Fetches the weights from the relevant QSD. If a particular weight is
    missing, the default weight is used instead.

    Following are the detailed names of the indicators:
    DG - Demographics
    WI - Wealth Indicators
    PI - Philanthropic Indicators
    AI - Affinity Indicator
    """
    DEFAULT_WEIGHTS = {'ai_weight': 0,
                       'dg_weight': 0.05,
                       'pi_weight': 0.45,
                       'wi_weight': 0.4,
                       }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.indicators = self.definition.get(
            'indicators', ['AI', 'DG', 'PI', 'WI'])
        self.config_keys = self.fetch_config_keys()
        self.weights = self.fetch_weights()

    def fetch_config_keys(self):
        """Fetches the list of config keys that must be queried from the QSD.
        Examples: 'dg_weight', 'wi_weight', etc.
        """
        return [f'{ind.lower()}_weight' for ind in self.indicators]

    def fetch_weights(self):
        # Build a dictionary for weights where key is the indicator and value
        # is the weight fetched from the QSD or from the default weight in the
        # definition.
        weights = {key: self.get_setting(config_key, self.DEFAULT_WEIGHTS[config_key])
                   for key, config_key in zip(self.indicators, self.config_keys)}
        return weights

    def run(self, pipeline, *args, **kwargs):
        def _store_weights(records_signals, weights):
            for record, signal in records_signals:
                signal['weights'] = weights
                yield record, signal

        pipeline.records_signals = _store_weights(
            pipeline.records_signals, self.weights)
        return pipeline
