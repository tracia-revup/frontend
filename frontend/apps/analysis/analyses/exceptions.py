
from celery import exceptions


class AnalysisBlockedRetry(exceptions.Retry):
    pass


class EntityResolutionPendingRetry(exceptions.Retry):
    def __init__(self, seen, done, last_count_change, *args, **kwargs):
        self.seen = seen
        self.done = done
        self.last_count_change = last_count_change
        super(EntityResolutionPendingRetry, self).__init__(*args, **kwargs)
