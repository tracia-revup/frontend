
from frontend.apps.analysis.analyses.base import AnalysisBase


class DummyRunner(AnalysisBase):
    def run(self):
        pass

    def parallel_run(self):
        pass
