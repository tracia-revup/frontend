"""Person contribution ranking analysis

This module provides an analysis that ranks individual persons based on past
contributions.

"""
import datetime
import itertools
import logging
from math import ceil

from celery import group, current_task
from ddtrace import tracer
from django.conf import settings
from django.core.cache import cache
from django.db import transaction, models
from django.db.models import Exists, OuterRef
from pymongo.errors import CursorNotFound, AutoReconnect
import us

from frontend.apps.analysis.analyses import AnalysisBase
from frontend.apps.analysis.analyses.base import SignalAction
from frontend.apps.analysis.exceptions import SkipContact
from frontend.apps.analysis.models.results import (
    Result, FeatureResult, Analysis, ResultSort)
from frontend.apps.contact.exceptions import NoChildContacts
from frontend.apps.contact.models import Address, Contact, LimitTracker
from frontend.apps.contact_set.models import ContactSet
from frontend.apps.core.geo_location import GeoPyLocator
from frontend.apps.filtering.models import ResultIndex
from frontend.libs.utils.general_utils import (FunctionalBuffer,
                                               instance_cache, retry_on_error, deep_join)
from frontend.libs.utils.string_utils import random_uuid, clean_name


LOGGER = logging.getLogger(__name__)


class PersonContribRanking(AnalysisBase):

    def __init__(self, *args, **kwargs):
        super(PersonContribRanking, self).__init__(*args, **kwargs)
        self.contact_source = kwargs.get("contact_source")
        self.contact_source_id = kwargs.get("contact_source_id")
        self.queue_override = kwargs.get("queue_override")
        self.batch_size_override = kwargs.get("batch_size_override")
        self._locator = GeoPyLocator()
        self.location_zips = {}
        self._runner_cache_key = \
            settings.GLOBAL_CACHE_KEYS.analysis_runner_cache.format(
                analysis_hash=random_uuid())

        if self.user.is_account_user():
            self.analyze_limit = self.campaign.analyze_limit
        else:
            self.analyze_limit = settings.PERSONAL_CONTACT_ANALYZE_LIMIT

        self.use_alias_expansion = self.feature_flags.get(
                                            "Analysis Alias Expansion", False)
        self.use_new_zips_algorithm = self.feature_flags.get(
                                            "Analysis Zips Update", False)

        # Create the result buffers
        self._create_result_buffers()

    def __getstate__(self):
        state = super(PersonContribRanking, self).__getstate__()
        del state['_result_buffer']
        return state

    def __setstate__(self, state):
        super(PersonContribRanking, self).__setstate__(state)
        self._create_result_buffers()

    def _create_result_buffers(self):
        self._result_buffer = FunctionalBuffer(
            self._flush_result_buffer,
            max_length=100)

    @transaction.atomic
    @tracer.wrap()
    def _flush_result_buffer(self, items):
        """Flush the results.

        The format of items must be a tuple:
          (result, FeatureResults for result, ResultIndex for result)
        """
        result_qs = Result.objects.filter(
            user=self.user,
            analysis=self.analysis)

        # Find all the contacts with Results, so we can count the new ones
        results = Result.objects.filter(analysis__account=self.campaign,
                                        contact_id=OuterRef('id'))
        count_contacts_without_results = (
            Contact.objects.filter(id__in=(i[0].contact_id for i in items))
                           .annotate(results_exist=Exists(results))
                           .filter(results_exist=False).count())

        # Get all of the IDs of the contacts inside the Results, including
        # the children of merged contacts. This is so we delete any Results
        # of contacts that were merged and have a new ID.
        all_contact_ids = set(itertools.chain(*(itertools.chain(
            *([i[0].contact_id], (c.id for c in i[0].contact.get_contacts())))
            for i in items)))

        # Delete all of the existing/old Results so they can be replaced.
        # This will also cascade delete the FeatureResults, ResultIndexes, and
        # ResultSorts.
        # Note: this is all in a transaction, so it will not be finalized
        #       until the very end.
        result_qs.filter(contact__in=all_contact_ids).delete()

        # Bulk create the new Results
        Result.objects.bulk_create(i[0] for i in items)

        # Add the result IDs to all of its children models so we can save them
        for result, feature_results, result_index, result_sorts in items:
            result_id = result.id
            if result_index:
                result_index.result_id = result_id
            for rs in result_sorts:
                rs.result_id = result_id
            for fr in feature_results:
                fr.result_id = result_id

        # Bulk create the ResultIndexes
        # It's possible for the Index to be None, so we filter
        ResultIndex.objects.bulk_create(list(filter(None, (i[2] for i in items))))
        # Bulk create the FeatureResults
        FeatureResult.objects.bulk_create(
            # Flatten the FeatureResults into a 1d list, instead of 2d
            itertools.chain.from_iterable(i[1] for i in items))
        # Bulk create the ResultSorts
        ResultSort.objects.bulk_create(
            # Flatten the ResultSorts into a 1d list, instead of 2d
            itertools.chain.from_iterable(i[3] for i in items))

        # If any new results were generated, let's update the tracker
        if count_contacts_without_results:
            tracker = LimitTracker.get_tracker(self.user, self.campaign,
                                               select_for_update=True)
            tracker.analyzed_count += count_contacts_without_results
            tracker.save()

    def _should_parallelize(self, contacts_count, **kwargs):
        """If there is only one contact, we won't parallelize."""
        return (super(PersonContribRanking, self)._should_parallelize(**kwargs)
                and contacts_count > 1)

    def _get_analysis(self):
        if self.contact_source == "ContactSet" and self.contact_source_id:
            contact_set = ContactSet.objects.get(id=self.contact_source_id)
            if contact_set.analysis:
                return contact_set.analysis
        # If we get here, it is because the conditions above failed, and we
        # need to acquire an analysis through different means
        return super(PersonContribRanking, self)._get_analysis()

    def _get_analysis_queue(self, contacts_count):
        """Iterate over the defined partitions and select the appropriate."""
        if self.queue_override:
            return self.queue_override

        partitions = settings.ANALYSIS_QUEUE_PARTITIONS
        for queue, (floor_, ceiling_) in partitions.items():
            if ((floor_ is None or floor_ <= contacts_count) and
                    (ceiling_ is None or ceiling_ >= contacts_count)):
                return queue
        # If the above loop found nothing, we have to default to something
        return "celery"

    def _prefetch_contact_data(self, contacts):
        return contacts.prefetch_related('addresses', 'contacts',
                                         'contacts__addresses')

    @tracer.wrap()
    def _parallel_get_contacts(self, contact_ids):
        """This is used by subtasks to query for their specific contacts."""
        contacts = self.user.contact_set.filter(pk__in=contact_ids)
        contacts_count = len(contact_ids)
        return contacts, contacts_count

    @tracer.wrap()
    def _get_contact_ids(self):
        # This allows us to specify where to get the contacts from, so we
        # are able to run an analysis on a smaller subset of a user's contacts.
        # If this is a fresh analysis, we need to run a complete analysis
        if ((self.contact_source and self.contact_source_id)
                                   and not self.is_new_analysis):
            if self.contact_source == "Contact":
                contacts = Contact.objects.filter(id=self.contact_source_id)
                contact_source_obj = contacts[0]
            elif self.contact_source == "ContactSet":
                contact_set = ContactSet.objects.get(id=self.contact_source_id)
                contacts = contact_set.queryset(query_on=Contact)
                contact_source_obj = contact_set
            else:
                raise ValueError("Unsupported contact source: {}".format(
                    self.contact_source))
            contacts_count = contacts.count()
            LOGGER.info("Contacts Source: {}, Contacts count: {}".format(
                contact_source_obj, contacts_count))

        # If we have no other instruction, we'll analyze all contacts.
        # This could also happen if a complete analysis is required, even if
        # a subset analysis was originally specified. See `is_new_analysis`.
        else:
            contacts = self.user.contact_set.filter(is_primary=True)
            contacts_count = contacts.count()

            # If the task source doesn't reflect the need for a full analysis,
            # update the task so other tasks can queue up more intelligently
            if self.task and self.task.contact_source:
                self.task.contact_source = None
                self.task.label = self.task.get_label(regenerate=True)
                self.task.save()
            LOGGER.info("Contacts Source: User.contact_set, Contacts count: {}"
                        .format(contacts_count))

        ## We now need to restrict the contacts to the analyze limit
        # Any contacts that already have analysis results are allowed
        results = Result.objects.filter(analysis__account=self.campaign,
                                        contact_id=OuterRef('id'))
        base_qs = contacts.annotate(results_exist=Exists(results))
        has_results = base_qs.filter(results_exist=True)\
                             .values_list('id', flat=True)

        has_results_count = has_results.count()
        # Santiy Check if the Limit Tracker has a valid count
        with transaction.atomic():
            tracker = LimitTracker.get_tracker(self.user, self.campaign,
                                               select_for_update=True)
            if has_results_count > tracker.analyzed_count:
                tracker.analyzed_count = has_results_count
                tracker.save()

            # If there is remaining limit, search for contacts that have not
            # been analyzed and analyze them up to the limit.
            remaining_limit = self.analyze_limit - tracker.analyzed_count
            Contact.objects.filter(user=self.user)
            if remaining_limit > 0:
                no_results = (
                    base_qs.filter(results_exist=False)
                           .order_by("created")
                           .values_list('id', flat=True)[:remaining_limit]
                )
            else:
                no_results = []

        return itertools.chain(has_results, no_results), \
               (has_results_count + len(no_results))

    def _get_run_data(self):
        """Data that is shared by both the parallel and serial algorithms
           should be returned from overridden versions of this method.

        This data can then be used to determine if parallelization is
            appropriate
        :returns: a dict with the data from get_contacts
        """
        # Get the contacts
        contact_ids, contacts_count = self._get_contact_ids()
        return dict(contact_ids=contact_ids, contacts_count=contacts_count)

    @tracer.wrap()
    def run(self, contact_ids, contacts_count):
        """Run a person ranking analysis.

        Process contacts serially.
        """
        # Run the analysis
        contact_ids = list(contact_ids)
        self.iter_contacts(contact_ids)

    @tracer.wrap()
    def parallel_run(self, contact_ids, contacts_count):
        """Run a person ranking analysis.

        Process batches of contacts in parallel (celery only).
        """
        from frontend.apps.analysis.tasks import (
            analysis_parallel_complete, new_analysis_parallel_iter_contacts)
        contact_ids = list(contact_ids)

        if self.batch_size_override:
            batch_size = self.batch_size_override
        else:
            batch_size = settings.ANALYSIS_PARALLEL_BATCH_SIZE

        # Select the queue based on contact count
        queue = self._get_analysis_queue(contacts_count)
        LOGGER.info("Analysis using queue: '{}' with count {}"
                    .format(queue, contacts_count))

        task_method = new_analysis_parallel_iter_contacts
        if settings.ANALYSIS_RUNNER_USE_CACHE:
            runner_arg = self._runner_cache_key
            # Add the runner to the cache, so the children can access it
            cache.set(self._runner_cache_key, self,
                      timeout=(24*60*60*2))  # 2 day timeout
        else:
            runner_arg = self

        # Send the batches to subtasks
        batches = ceil(contacts_count / float(batch_size))
        tasks = (group(
            task_method.s(
                self.analysis.task_id,
                runner_arg,
                contact_ids[index * batch_size:(index + 1) * batch_size]).set(
                    queue=queue)
            for index in range(int(batches))) |
            analysis_parallel_complete.si(self.analysis.task_id, runner_arg)
        ).apply_async(add_to_parent=True)
        # We need to return this async result so it gets stored in the root
        # celery result. Without this we have no way to track the final celery
        # task's progress.
        return tasks

    @tracer.wrap()
    def iter_contacts(self, contact_ids):
        # If the state of the analysis is error, then somehow the revoke for
        # this task was lost. Just quit early. This is essentially a backup for
        # celery revoke features.
        if self.analysis.status == Analysis.STATUSES.error:
            LOGGER.info(
                "Skipping processing of contacts, analysis status is error")
            return
        # If this task was redelivered it was shutdown while running and there
        # may be some result records that were persisted. In order to prevent
        # duplicates clear out these old result records.
        if self.is_task and current_task.request.get('redelivered', False):
            self._reset_results(contact_ids)
        contacts, contacts_count = self._parallel_get_contacts(contact_ids)
        self._iter_contacts(contacts, contacts_count)

    @tracer.wrap()
    def complete(self):
        meta_results = self._process_meta_features()
        if meta_results:
            self.analysis.meta_results = meta_results

        # Delete any results that point at non-primary contacts. This might
        # happen if a contact is de-merged.
        Result.objects.filter(
            user=self.user, analysis=self.analysis,
            contact__is_primary=False).delete()

        if self.uses_default_analysis_config:
            # Update all ContactSets to use the new analysis
            ContactSet.bulk_update_analysis(self.analysis)

        if settings.ANALYSIS_RUNNER_USE_CACHE:
            # Delete the cached runner
            cache.delete(self._runner_cache_key)

        # Set the progress to 99%
        self.update_progress(99, 100)
        # Run the base complete() to finalize the analysis
        super(PersonContribRanking, self).complete()

    @tracer.wrap()
    def _iter_contacts(self, contacts, contacts_count):
        finished_contacts = 0.0
        first_iteration_step = 0.95

        for contact in self._prefetch_contact_data(contacts).iterator():
            # This is a safeguard in case we are trying to analyze a child
            contact = contact.get_oldest_ancestor()
            try:
                result = self._process_contact(contact)
            except SkipContact:
                pass
            except NoChildContacts:
                LOGGER.exception(
                    "Skipping merged contact containing no child contacts: {}"
                    .format(contact))
            except Exception:
                LOGGER.error("Uncaught exception raised while processing "
                             "contact: {}".format(contact))
                raise
            else:
                if result:
                    self._persist_result(contact, result)

            finished_contacts += first_iteration_step
            self.update_progress(finished_contacts, contacts_count)

        # Flush any remaining items from the buffer
        self._result_buffer.flush()

    @tracer.wrap()
    @retry_on_error((CursorNotFound, AutoReconnect), 10)
    def _process_contact(self, contact):
        span = tracer.current_span()
        span.set_tag('contact_id', contact.id)
        contact = ContactWrapper(contact, self)
        if not self._good_names(contact):
            LOGGER.debug("Contact {} name too short to search, skipping"
                         .format(contact))
            return

        # Ensure cached results are cleared out incase a retry occurs
        self.signal_sets_results = {}
        self.record_set_records = {}

        # Prepare target data
        result = {'features': {},
                  'key_signals': {},
                  'tags': set(),
                  'score_multipliers': {}}
        self._process_prime_features(contact, result)
        self._process_root_features(contact, result)
        self._process_subset_features(contact, result)
        self._process_summary_features(contact, result)

        # Clear out cached results for contact as we are now done processing
        # for contact.
        self.signal_sets_results = {}
        self.record_set_records = {}
        return result

    @tracer.wrap()
    def _process_prime_features(self, contact, result):
        # Begin with prime features (primary data set searches)
        for feature in self.prime_features:
            self._process_prime_feature(contact, result, feature)

    @tracer.wrap()
    def _process_prime_feature(self, contact, result, feature):
        span = tracer.current_span()
        span.set_tags({
            'feature_id': feature.id,
            'feature_title': feature.title,
        })
        feature_result = {}
        feature_struct = self.features[feature.id]
        signal_actions_map = feature_struct.signal_actions_map

        # Prepare signal pipeline
        pipeline = SignalAction.build_pipeline(contact, global_signals=result)

        # Iterate over SignalSetConfigs for this feature
        for ssc in feature_struct.signal_set_configs:
            # Apply the non-partitioned signal actions
            for signal_action in signal_actions_map[ssc.id].get(
                                                "unpartitioned", []):
                with tracer.trace(signal_action.trace_name()) as span:
                    span.set_tags({
                        'feature_id': feature.id,
                        'ssc_id': ssc.id,
                        **signal_action.trace_tags()
                    })
                    pipeline = signal_action.process(pipeline)

            if pipeline.results:
                self.signal_sets_results[ssc.id] = pipeline.results
                feature_result[ssc.id] = pipeline.results

        if feature_result:
            result['features'][feature.id] = feature_result

    @tracer.wrap()
    def _process_root_features(self, contact, result):
        # Begin with root features (primary data set searches)
        for feature in self.root_features:
            self._process_root_feature(contact, result, feature)

    @tracer.wrap()
    def _process_root_feature(self, contact, result, feature):
        span = tracer.current_span()
        span.set_tags({
            'feature_id': feature.id,
            'feature_title': feature.title,
        })
        feature_result = {}
        feature_struct = self.features[feature.id]
        signal_actions_map = feature_struct.signal_actions_map
        record_set_config_map = feature_struct.record_set_config_map

        # Iterate over SignalSetConfigs for this feature
        for ssc in feature_struct.signal_set_configs:
            # Prepare signal pipeline
            pipeline = SignalAction.build_pipeline(
                contact, feature.partitioned,
                global_signals=result)

            # Apply the non-partitioned signal actions
            for signal_action in signal_actions_map[ssc.id].get(
                                                "unpartitioned", []):
                with tracer.trace(signal_action.trace_name()) as span:
                    span.set_tags({
                        'feature_id': feature.id,
                        'ssc_id': ssc.id,
                        **signal_action.trace_tags()
                    })
                    pipeline = signal_action.process(pipeline)

            # TODO: Any other record-level filters
            ## level of race
            ## fed/state/local
            ## challenger vs incumbent

            # Get the partitioned signal actions
            partitioned_actions = signal_actions_map[ssc.id].get("partitioned",
                                                                 [])

            # Iterate over partitions for partitioned actions
            # TODO: Revisit iteration within actions
            for partition_config in self.partition_configs:
                pipeline = self._process_partition(
                    partition_config, partitioned_actions, pipeline)

            # Cache the records and signals for use by subset features
            # We need to do a tee so we can create a list without exhausting
            # the generator
            pipeline.records_signals, rs_temp = itertools.tee(
                pipeline.records_signals)
            self.record_set_records[record_set_config_map[ssc.id]] = [
                (record, signal)
                for (record, signal) in rs_temp
            ]

            # Cache the signal set results
            self.signal_sets_results[ssc.id] = pipeline.results
            feature_result[ssc.id] = pipeline.results
            result['features'][feature.id] = feature_result

            # Reduced records and signals to data_set dict too?

                    # top level actions - per partition
                    ## spectrum
                    ## giving totals
                    ## scoring

    @tracer.wrap()
    def _process_partition(self, partition_config, partitioned_actions,
                           pipeline):
        # TODO: Consider a helper partition object
        partition_definition = partition_config.definition
        today = datetime.date.today()
        # Calculate dates for partitions based on intervals
        if partition_definition['type'] == 'years_before':
            years = int(partition_definition['years_before'])
            partition_config.years_before = years
            start_date = today - datetime.timedelta(days=365 * years)
            partition_config.start_date = start_date
        else:
            start_date_str = partition_definition.get('start_date')
            start_date = datetime.datetime.strptime(
                start_date_str, "%Y-%m-%d").date()
            partition_config.start_date = start_date
            partition_config.years_before = (
                today.year - start_date.year)

        # Apply the partitioned signal actions to the pipeline
        for signal_action in partitioned_actions:
            pipeline = signal_action.process(
                pipeline, partition_config=partition_config)

            # TODO: Any other signal set-level filters
            ## number of donations to parties?
            ## other spectrum multipliers
        return pipeline

    @tracer.wrap()
    def _process_subset_features(self, contact, result):
        # Process subset features
        for feature in self.subset_features:
            self._process_subset_feature(contact, result, feature)

    @tracer.wrap()
    def _process_subset_feature(self, contact, result, feature):
        span = tracer.current_span()
        span.set_tags({
            'feature_id': feature.id,
            'feature_title': feature.title,
        })
        feature_result = {}
        feature_struct = self.features[feature.id]
        signal_actions_map = feature_struct.signal_actions_map

        # Iterate over SignalSetConfigs for this feature
        for ssc in feature_struct.signal_set_configs:
            # Prepare signal pipeline
            pipeline = SignalAction.build_pipeline(
                contact, global_signals=result)

            # Apply the base signal actions to the pipeline
            base_actions = signal_actions_map[ssc.id].get("unpartitioned", [])
            for signal_action in base_actions:
                with tracer.trace(signal_action.trace_name()) as span:
                    span.set_tags({
                        'feature_id': feature.id,
                        'ssc_id': ssc.id,
                        **signal_action.trace_tags()
                    })
                    pipeline = signal_action.process(
                        pipeline, cached_record_signals=self.record_set_records)

            #TODO: Subset scoring factors

            # Cache the signal set results
            self.signal_sets_results[ssc.id] = pipeline.results
            feature_result[ssc.id] = pipeline.results

            # Cache the records and signals for use in persistence
            feature_result[ssc.id]['records_signals'] = \
                    list(pipeline.records_signals)

        result['features'][feature.id] = feature_result

    @tracer.wrap()
    def _process_summary_features(self, contact, result):
        # Process summary features
        for feature in self.summary_features:
            self._process_summary_feature(contact, result, feature)

    @tracer.wrap()
    def _process_summary_feature(self, contact, result, feature):
        span = tracer.current_span()
        span.set_tags({
            'feature_id': feature.id,
            'feature_title': feature.title,
        })
        pipeline = SignalAction.build_pipeline(
            contact, feature.partitioned,
            global_signals=result)
        feature_struct = self.features[feature.id]
        signal_actions_map = feature_struct.signal_actions_map
        child_ssc_map = feature_struct.child_ssc_map

        for ssc in feature_struct.signal_set_configs:
            child_ids = child_ssc_map[ssc.id]
            signal_sets = [self.signal_sets_results[child_id]
                           for child_id in child_ids]

            # Apply the signal actions for the signal set
            base_actions = signal_actions_map[ssc.id].get("unpartitioned", [])
            for signal_action in base_actions:
                with tracer.trace(signal_action.trace_name()) as span:
                    span.set_tags({
                        'feature_id': feature.id,
                        'ssc_id': ssc.id,
                        **signal_action.trace_tags()
                    })
                    pipeline = signal_action.process(pipeline,
                                                     signal_sets=signal_sets)
        # Add summary fields to result
        result['result'] = pipeline

    @tracer.wrap()
    def _process_meta_features(self):
        """Process meta features.

        Meta features are features that analyze some aspect of the analysis,
        and return some values or augment some data.
        E.g. Normalization.
        """
        meta_results = {}
        for feature in self.meta_features:
            pipeline = SignalAction.build_pipeline(
                None, feature.partitioned)

            feature_struct = self.features[feature.id]
            signal_actions_map = feature_struct.signal_actions_map

            for ssc in feature_struct.signal_set_configs:
                # Apply the signal actions for the signal set
                base_actions = signal_actions_map[ssc.id].get("unpartitioned",
                                                              [])
                for signal_action in base_actions:
                    with tracer.trace(signal_action.trace_name()) as span:
                        span.set_tags({
                            'feature_id': feature.id,
                            'ssc_id': ssc.id,
                            **signal_action.trace_tags()
                        })
                        pipeline = signal_action.process(pipeline,
                                                         analysis=self.analysis)
                    meta_results.update(pipeline.results)
        return meta_results

    ## DEPRECATED
    def _process_normalization_features(self, results):
        # TODO: Simplify/improve understandability of normalization process
        # Process normalization features
        for feature in self.normalization_features:

            # Prepare pipeline of all results
            pipeline = SignalAction.build_pipeline(
                None, feature.partitioned)

            pipeline.results = results

            # Apply normalization
            feature_struct = self.features[feature.id]
            signal_actions_map = feature_struct.signal_actions_map

            for ssc in feature_struct.signal_set_configs:
                # Apply the signal actions for the signal set
                base_actions = signal_actions_map[ssc.id].get("unpartitioned",
                                                              [])
                for signal_action in base_actions:
                    pipeline = signal_action.process(pipeline)

            results = pipeline.results
        return results

    @classmethod
    def _calc_progress_step(cls, result_count):
        """Calculate a progress step for the final 50% that brings us close to
        100%, but leaves a 1-to-5% gap.  We'll make the final jump to 100%
        after doing the last batch commit. The size of the jump scales based on
        the number of analysis results being persisted. The miniumum is a 1%
        jump to completion, and the maximum is a 5% jump for 10,000 analysis
        results or more.
        """
        progress_scaler = 10000.0
        progress_step_max = 0.45 # final jump is from 95%
        progress_step_min = 0.49 # final jump is from 99%

        progress_step = 0.5 - (
            min(float(result_count) / float(progress_scaler), 1) *
            (progress_step_min - progress_step_max + 0.02))

        # Make sure the jump is never less than 1%
        progress_step = min(progress_step_min, progress_step)
        # Make sure the jump is never more than 5%
        progress_step = max(progress_step_max, progress_step)

        return progress_step

    @tracer.wrap()
    @transaction.atomic
    def _reset_results(self, contact_ids):
        FeatureResult.objects.filter(
            result__contact_id__in=contact_ids,
            result__analysis_id=self.analysis.pk).distinct().delete()
        Result.objects.filter(contact_id__in=contact_ids,
                              analysis_id=self.analysis.pk).delete()

    @tracer.wrap()
    def _persist_result(self, contact, analysis_result):
        """Build the persistence objects and add them to the persistence
         buffers to be bulk created.
        """
        results = self._build_persistence(contact, analysis_result)
        self._result_buffer.push(*results)

    @tracer.wrap()
    def _build_result_index(self, result, feature_results):
        return self.build_result_index(self.user, self.features, result,
                                       feature_results)

    @classmethod
    @tracer.wrap()
    def build_result_index(cls, user, features, result, feature_results):
        index_parts = []
        for fcstruct in features.values():
            for controller in fcstruct.filters.values():
                index_parts.append(
                    controller.extract(result, feature_results) or {})
        # Merge nested index data
        index_data = deep_join(index_parts)
        if index_data:
            return ResultIndex(result=result, user=user,
                               analysis=result.analysis,
                               partition=result.partition,
                               attribs=index_data)

    @classmethod
    def _build_resultsort(cls, result):
        def _mk_result_sort(value_type, value, identifier):
            return ResultSort(
                result=result,
                contact_id=result.contact_id,
                analysis_id=result.analysis_id,
                partition_id=result.partition_id,
                identifier=identifier,
                value_type=value_type,
                value=value,
            )
        key_signals = result.key_signals
        for signal_key, value_type in ResultSort.ValueTypes.SIGNAL_MAP.items():
            value = key_signals.get(signal_key)
            if value is not None:
                yield _mk_result_sort(value_type, value, identifier='')
        personas = result.key_signals.get('personas', {})
        for identifier, scores in personas.items():
            for signal_key, value in scores.items():
                value_type = ResultSort.ValueTypes.SIGNAL_MAP.get(signal_key)
                if value_type is not None:
                    yield _mk_result_sort(value_type, value, identifier)

    @tracer.wrap()
    def _build_persistence(self, contact, analysis_result):
        """Build persistence objects for  a single contact.

        There will be one result per partition
        """
        result = analysis_result['result']
        partition_results = result.results['partitions']
        result_records = []

        # Create one Result object per partition configuration
        for partition_config in self.partition_configs:
            partition_result = partition_results[partition_config.id]
            score = partition_result['score']
            giving = partition_result['giving']

            result_model = Result(
                user=self.user,
                contact=contact,
                analysis=self.analysis,
                partition=partition_config,
                score=score,
                giving=giving,
                key_signals={
                    'score': score,
                    'giving': giving,
                    'political_spectrum': partition_result[
                        'political_spectrum'],
                    # Set default value for key_contributions
                    'key_contributions': [],
                },
                tags=list(analysis_result['tags'] or [])
            )
            result_model.key_signals.update(analysis_result['key_signals'])

            # Add partition-specific key_signals if present.
            if 'key_signals' in partition_result:
                result_model.key_signals.update(
                    partition_result['key_signals'])

            # See _prepare_giving_history_chart docstring for explanation.
            if "political_giving" in partition_result:
                chart_giving = partition_result["political_giving"]
            else:
                chart_giving = giving
            giving_chart = self._prepare_giving_history_chart(
                partition_config, chart_giving)
            if giving_chart:
                result_model.key_signals["giving_history_chart"] = giving_chart

            # Create a FeatureResult for every feature that has a SignalSet
            feature_result_models = []
            for feature_id, feature_results in \
                    analysis_result['features'].items():
                feature_config = self.features[feature_id].feature_config
                result_formatter = feature_config.result_formatter(
                    feature_config, self.analysis_config)

                # Build the FeatureResult objects for each partition
                fr_generator = result_formatter.build_feature_results(
                    feature_results, result_model, self.user)
                feature_result_models.extend(fr_generator)

            # We'll only build an index record if we have FeatureResults
            if feature_result_models:
                result_index_model = self._build_result_index(
                    result_model, feature_result_models)
            else:
                result_index_model = None

            # Build the ResultSort records for the Result record
            result_sorts = list(self._build_resultsort(result_model))
            result_records.append((result_model, feature_result_models,
                                   result_index_model, result_sorts))
        # Return the generated records
        return result_records

    def _good_names(self, contact):
        """Checks if this contact has "good" names. If the contact has all
            "bad" names, this will be False.

        An example of a bad name would be a name consisting of only
        punctuation, whitespace, and a single initial. E.g. " H.  "

        `get_known_*_names` excludes "bad" names.
        """
        result = False
        if (list(contact.get_known_first_names()) and
                list(contact.get_known_last_names())):
            result = True
        return result

    def _prepare_giving_history_chart(self, partition_config, giving):
        """If the partition config defines a giving history chart, find the
        giving level and return it.

        The definition should define value ranges for each level.
        """
        definition = partition_config.definition.get("giving_history_chart")
        if definition:
            for level, giving_range in definition.items():
                level = int(level)
                if len(giving_range) == 1:
                    # If the giving range is only one, we assume "to infinity"
                    if giving >= giving_range[0]:
                        return level
                else:
                    if giving_range[0] <= giving <= giving_range[1]:
                        return level
        return None

    @tracer.wrap()
    def _get_contact_zip3s(self, contact):
        """
        Get the 3-digit zip prefixes associated with a contact's LinkedIn
        locations and their addresses.
        """
        contacts = contact.get_contacts()
        if not isinstance(contacts, models.QuerySet):
            contacts = Contact.objects.filter(pk__in=[c.pk for c in contacts])

        # Get all of the explicitly defined zip codes from the contact
        zips = set(filter(None, contact.zips))

        # Waffle flag for a new algorithm for gathering zip3s.
        if self.use_new_zips_algorithm:
            # Get all of the zip codes from every city in the contact's
            # addresses that is listed without a zip code.
            # E.g. Menlo Park, CA vs. Menlo Park, CA 94025 <excluded>
            city_zips = (Address.objects
                                .filter(contact__in=contacts, zip_key=None,
                                        archived=None)
                                .exclude(city_key=None)
                                .values_list("city_key__zips__zipcode",
                                             flat=True))
            zips.update(city_zips)
            if not zips:
                # If the user has no defined zips or cities in their addresses,
                # then use the Metro Area for filtering
                zips.update(
                    contacts.values_list('locations__zipcodes__zipcode',
                                         flat=True).distinct())
        else:
            # Get the zip3s for contact's LinkedIn locations
            zips.update(contacts.values_list('locations__zipcodes__zipcode',
                                             flat=True).distinct())
        return [z[0:3] for z in zips if z]

    # TODO: REPLACE AND REMOVE
    @classmethod
    @tracer.wrap()
    def _get_contact_states(cls, contact):
        def add_state(state_set, new_state):

            # TODO: replace stop-gap measures with proper location entity
            state = us.states.lookup(new_state)
            test_state = None
            if not state:
                # strip punctuation and try again
                test_state = clean_name(new_state)
                state = us.states.lookup(test_state)
            if not state:
                # try the first and last tokens of any multi-token string
                tok = test_state.split()
                if len(tok) > 1:
                    s_check = [us.states.lookup(s) for s in [tok[0], tok[-1]]]
                    state = next((state for state in s_check if state), None)
            if state:
                state_set.add(state.abbr)
            else:
                LOGGER.warn("Could not determine state for region: {}".format(
                    new_state
                ))

        if not contact:
            return []

        states = set()
        contacts = contact.get_contacts()

        # Get states from contact's addresses
        regions = Address.objects.filter(
            contact__in=contacts, archived=None).exclude(
                region='').distinct('region').values_list(
                    'region', flat=True)
        for region in regions.iterator():
            add_state(states, region)
        return list(states)


class ContactWrapper(object):
    """Wraps a contact so we can augment the contact data as it is passed
    through the analysis, without actually touching the contact itself.

    Any fields/methods that aren't overridden here will be accessed from
    the contact itself.
    This also allows for a place to put analysis-specific contact helper
    methods and attributes.
    """
    def __init__(self, contact, analysis_runner):
        # Bypass setattr for these attributes.
        # Otherwise we get an infinite loop
        self.__dict__["_contact"] = contact
        self.__dict__["_analysis_runner"] = analysis_runner

    def __getattr__(self, item):
        return getattr(self._contact, item)

    def __setattr__(self, key, value):
        if hasattr(self._contact, key):
            setattr(self._contact, key, value)
        else:
            super(ContactWrapper, self).__setattr__(key, value)

    @property
    @instance_cache
    @tracer.wrap()
    def zip3s(self):
        """This property replaces the zip3s that were being assigned to the
        contact. Instead, they are managed through this wrapper.

        The zip3s have a default method that may be used to collect them;
        however, the zip3s may also be overridden with custom zip3s by using
        the setter.
        """
        return self._analysis_runner._get_contact_zip3s(self._contact)

    @zip3s.setter
    def zip3s(self, value):
        self._zip3s = value

    @property
    @instance_cache
    @tracer.wrap()
    def states(self):
        """This property replaces the states that were being assigned to the
        contact. Instead, they are managed through this wrapper.

        The states have a default method that may be used to collect them;
        however, the states may also be overridden with custom states by using
        the setter.
        """
        return self._analysis_runner._get_contact_states(self._contact)

    @states.setter
    def states(self, value):
        self._states = value

    @property
    @instance_cache
    @tracer.wrap()
    def lat_lngs(self):
        lat_lngs = []
        location_names = set(self._contact.get_location_names())
        for location_name in location_names:
            _, location = self._analysis_runner._locator.\
                    get_geo_names_location(location_name)
            if location:
                lat = location.get("lat")
                lng = location.get("lng")
                if lat and lng:
                    lat_lngs.append((lat, lng))

        return lat_lngs

    @property
    @instance_cache
    @tracer.wrap()
    def aliases(self):
        if self._analysis_runner.use_alias_expansion:
            aliases = [identity for identity in self._contact.get_aliases()]
        else:
            aliases = []
        return aliases

    @aliases.setter
    def aliases(self, value):
        self._aliases = value

# These classes have been reorganized into sub-files. In order to preserve
# legacy functionality, these classes need to be imported here, since
# references to these classes are stored in the data base at this location.
#
# Future SignalActions should not be imported here, but rather should be
# referred to at their new location.
from .signal_actions import (
    FieldUniqueCountFilter,
    FECEnrich,
    MISPEnrich,
    TargetFieldValueFilter,
    FECMarkSelfFunded,
    MISPMarkSelfFunded,
    FECDateTransform,
    MISPDateTransform,
    AmountTransform,
    DateTransform,
    PartyMultiplier,
    CommitteeMultipliers,
    FECOfficeMultiplier,
    StateOfficeMultiplier,
    MISPGrouping,
    SimpleCalculateGiving,
    ClientCalculateGiving,
    FECCalculateGiving,
    MISPCalculateGiving,
    ClientCalculateGivingAvailability,
    ApplyGivingFactors,
    ScorePoliticalContribution,
    ApplyScoreFactors,
    SimplePoliticalSpectrum,
    SumValues,
    NameBoost,
    StateBoost,
    SpectrumDampen,
    LegacyNormalize
)
