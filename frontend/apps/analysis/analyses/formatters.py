from collections import defaultdict
import logging
from operator import itemgetter
import re

from ddtrace import tracer

from frontend.apps.analysis.models import FeatureResult
from frontend.libs.utils.csv_utils import csv_split
from frontend.libs.utils.demo_utils import map_name
from frontend.libs.utils.string_utils import str_to_bool


LOGGER = logging.getLogger(__name__)


class ResultFormatter(object):
    """This is designed to format the FeatureResult data (signal sets) for
    both storage and display.

    By putting these all in the same class, it does two things:
        1) Keeps it consistent between storage and display, so there are fewer
           maintainability issues.
        2) Allows us to customize the storage and/or display on a per feature
           basis. One example of a use case is Key Contributions.

    Any of the following methods may be overridden
    """
    giving_breakdown = {"democrat_giving", "republican_giving", "other_giving"}
    valid_signals = {'giving', 'score', 'giving_history', 'giving_overview',
                     'giving_availability'} | giving_breakdown

    def __init__(self, feature_config, analysis_config, demo_mode=False,
                 limit=None, **kwargs):
        self.feature_config = feature_config
        self.demo_mode = demo_mode
        self.limit = limit
        self.analysis_config = analysis_config
        self.feature_flags = kwargs.get('feature_flags')

    @tracer.wrap()
    def _build_base_signal_set(self, result):
        signals = {}
        for key in self.valid_signals:
            if key in result:
                # If this key is in the giving-breakdown section, we need
                # to initialize a dict (or retrieve the existing).
                if key in self.giving_breakdown:
                    assign_to = signals.setdefault("giving_breakdown", {})
                else:
                    assign_to = signals
                assign_to[key] = result[key]
        return signals

    @tracer.wrap()
    def _format_signal_set(self, signal_set_id, result):
        """Format a signal set to be stored in a FeatureResult."""
        signals = self._build_base_signal_set(result)

        if 'expansion_hit' in result:
            # expansion_hit is used in debugging to determine whether a
            # contribution record was discovered as a result of name or alias
            # expansion.
            signals['expansion_hit'] = list(map(str, result.get('expansion_hit',
                                                           [])))
        if 'match' in result:
            signals['match'] = list(map(str, result.get('match', [])))
        if signals:
            signals['config_id'] = signal_set_id
        return signals

    @tracer.wrap()
    def build_feature_results(self, feature_results, result, user):
        """Create a FeatureResult for each feature, with partitions as needed.
        """
        # Create a FeatureResult for each Feature. If the data is partitioned,
        # it needs to be handled special.
        if self.feature_config.partitioned:
            signal_sets = []
            part_id = result.partition.id
            for signal_set_id, results in feature_results.items():
                # Format any unpartitioned signals if present
                signals = self._format_signal_set(signal_set_id, results)
                # Format the signals for the specific partition
                part_signals = self._format_signal_set(
                    signal_set_id, results['partitions'][part_id])
                # Merge the partitioned and unpartitioned signals together.
                # partitioned signals take preference.
                signals.update(part_signals)
                if signals:
                    signal_sets.append(signals)
            if signal_sets:
                feature_result = FeatureResult(
                    result=result,
                    user_id=user.id,
                    feature_config_id=self.feature_config.id,
                    signals={'signal_sets': signal_sets})
                yield feature_result

        # Each partition gets a copy of the FeatureResult
        else:
            signal_sets = []
            for signal_set_id, results in feature_results.items():
                signals = self._format_signal_set(signal_set_id, results)
                if signals:
                    signal_sets.append(signals)
            if signal_sets:
                feature_result = FeatureResult(
                    result=result,
                    user_id=user.id,
                    feature_config_id=self.feature_config.id,
                    signals={'signal_sets': signal_sets})
                yield feature_result

    @tracer.wrap()
    def format_result_data(self, signal_sets):
        """Format the FeatureResult data to be displayed to a user."""
        from frontend.apps.analysis.models import SignalSetConfig

        # Get all of the available signalset configs
        config_ids = (ss['config_id'] for ss in signal_sets
                                      if "config_id" in ss)
        signal_configs = {
            ss.id: ss
            for ss in SignalSetConfig.objects.select_related(
                "record_set_config").filter(id__in=config_ids)
        }

        # Format the signal and query the matches
        signals = []
        for signal_set in signal_sets:
            temp = signal_set.copy()
            config = signal_configs[signal_set["config_id"]]
            temp["signal_set_title"] = config.title
            record_set = config.get_record_set(self.analysis_config,
                                               feature_flags=self.feature_flags)

            matches = signal_set.get("match")
            expansion_hits = set(signal_set.get("expansion_hit", []))
            # expansion_hit is used in debugging to determine whether a
            # contribution record was discovered as a result of name or alias
            # expansion.
            if matches:
                temp["match"] = result = []
                for m in record_set.get(matches, limit=self.limit):
                    m = m.get_all()
                    m["expansion_hit"] = str(m['id']) in expansion_hits
                    # This is a hook to allow subclasses to make mods here.
                    m = self._modify_match(m)
                    result.append(m)
            try:
                # 'config_id' shouldn't be displayed
                del temp['config_id']
            except KeyError:
                pass
            if 'expansion_hit' in temp:
                del temp['expansion_hit']

            # This is a hook to allow subclasses to make mods to the signalset
            temp = self._modify_signal_set(temp)

            signals.append(temp)
        return signals

    @tracer.wrap()
    def _modify_match(self, match):
        """If we're in demo mode, we need to replace certain data"""
        if self.demo_mode:
            surname = match.get("family_name")
            if not surname:
                return match

            replacement = map_name(surname)
            if surname.isupper():
                replacement = replacement.upper()
            match["family_name"] = replacement
            if "name" in match:
                match["name"] = re.sub(surname, replacement, match["name"],
                                       flags=re.IGNORECASE)
            if "contributor" in match:
                contributor = match["contributor"]
                contributor[0] = re.sub(surname, replacement, contributor[0],
                                        flags=re.IGNORECASE)
                contributor[2] = re.sub(surname, replacement, contributor[2],
                                        flags=re.IGNORECASE)

        return match

    @tracer.wrap()
    def _modify_signal_set(self, signal_set):
        """This is a noop that is meant to be overridden by subclasses"""
        return signal_set


class ObfuscatingResultFormatter(ResultFormatter):
    def __init__(self, feature_config, analysis_config, show_full_data=False,
                 **kwargs):
        super(ObfuscatingResultFormatter, self).__init__(feature_config,
                                                         analysis_config)
        self.show_full_data = show_full_data

    def _modify_match(self, match):
        # If we are not supposed to show giving, we need to hide the amounts.
        if not self.show_full_data and "amount" in match:
            del match["amount"]
        return match

    def _modify_signal_set(self, signal_set):
        # If we are not supposed to show the giving, we need to delete
        # it from the signal set.
        if not self.show_full_data and "giving" in signal_set:
            del signal_set["giving"]
        return signal_set


class KeyContributionsResultFormatter(ResultFormatter):
    signal_set_key = "key_contribs"

    def _format_signal_set(self, signal_set_id, result):
        """Format the KeyContributions to be stored in a FeatureResult."""
        signals = self._build_base_signal_set(result)
        if 'key_contributions' in result:
            signals[self.signal_set_key] = result['key_contributions']
        return signals

    def format_result_data(self, signal_sets):
        """Format KeyContributions Feature for display in the API."""
        from frontend.apps.analysis.models import RecordSetConfig
        from frontend.apps.analysis.analyses.base import RecordSet

        # Organize the matches into queryable groups
        record_set_groups = defaultdict(list)
        for signal_set in signal_sets:
            # To maintain backward compatibility, we can't assume key_contribs
            # is in the signal set. All new analyses should have it.
            if self.signal_set_key in signal_set:
                signal_set = signal_set[self.signal_set_key]

            for group, signals in signal_set.items():
                for eid, record_set_config_id in signals:
                    record_set_groups[record_set_config_id].append(eid)

        # Query for all of the record set configs
        record_set_configs = RecordSetConfig.objects.filter(
            id__in=iter(record_set_groups.keys())).select_related("record_set")

        # Query each of the record sets for the match data
        matches_dict = {}
        for record_set_config in record_set_configs:
            record_set = RecordSet.factory(record_set_config,
                                           self.analysis_config,
                                           feature_flags=self.feature_flags)
            matches = record_set_groups[record_set_config.id]

            # Create a dict of mappings from match.id to match data. We will
            # need this to reconstruct the original data structure.
            for m in record_set.get(matches):
                m = m.get_all()
                # This is a hook to allow subclasses to make mods here.
                m = self._modify_match(m)
                matches_dict[str(m["id"])] = m

        # Reconstruct the original data structure, augmented with match data
        results = defaultdict(dict)
        for signal_set in signal_sets:
            # To maintain backward compatibility, we can't assume key_contribs
            # is in the signal set. All new analyses should have it.
            if self.signal_set_key in signal_set:
                signal_set = signal_set[self.signal_set_key]

            for group, signals in signal_set.items():
                try:
                    label, is_ally = csv_split(group)
                except ValueError:
                    # If the signal set got corrupted, error gracefully.
                    LOGGER.exception("Ill-formatted Key-Contribution "
                                     "SignalSet: {}".format(signal_set))
                    continue

                data = results[label]
                data["is_ally"] = str_to_bool(is_ally)
                for eid, _ in signals:
                    match = data.setdefault("match", [])
                    try:
                        match.append(matches_dict[eid])
                    except KeyError:
                        # If this KeyError occurs, it is because a record was
                        # deleted from mongo. We want to fail gracefully
                        # TODO: We may want to trigger an analysis
                        continue

        for result in results.values():
            if 'match' in result:
                result['match'].sort(key=itemgetter('date'), reverse=True)
        return [results]
