
from abc import abstractproperty
import pickle

from django.urls import reverse
from intervaltree import IntervalTree
from localflavor.us.us_states import STATE_CHOICES

from frontend.apps.analysis.models import AnalysisConfig
from frontend.apps.analysis.ntee_codes import (
    NTEE_PRIMARY_CATEGORIES, NTEE_SUB_CATEGORIES, NTEE_PRIMARY_CATEGORIES_RAW,
    NTEE_SUB_CATEGORIES_RAW)
from frontend.libs.utils.setting_utils import AbstractConfigSetController


class QuestionSetController(AbstractConfigSetController):
    """Defines an interface for analysis configuration Questions."""

    def get_info(self, question_set, **kwargs):
        return dict(
            id=question_set.id,
            title=question_set.title,
            label=question_set.label,
            icon=question_set.icon,
            allow_multiple=question_set.allow_multiple,
            fields=self.get_fields(**kwargs),
        )


class ClientContributions(QuestionSetController):
    """This QuestionSet is to contain data about the campaign. The current
    class name is probably a bad one. Eventually this should answer the
    questions: Federal Campaign or Federal Committee? If campaign, what office?
    if the the office is senate, what state? contribution limit (optional)?
    cycle start date (required for committees, optional for candidates)?
    """
    def get_fields(self, required=False, **kwargs):
        return [dict(
            label="Contribution Limit",
            type="unlimited-or-integer",
            required=required,
            expected=[
                dict(field="limit", type="int"),
            ],
        ),dict(
            label="Election Cycle Start",
            type="date",
            required=required,
            expected=[
                dict(field="cycle_start", type="date"),
            ],
        )]


class KeyContributions(QuestionSetController):
    def get_fields(self, account_id, analysis_config_id):
        return [dict(
            label="Search",
            type="entity_search",
            query_url=reverse("entity_search_api-list",
                              args=(account_id, analysis_config_id)),
            query_arg="q",
            expected=[
                dict(field="label", type="text"),
                dict(field="is_ally", type="boolean"),
                dict(field="entities", type="list",
                     content=["record_set_config", "eid"])
            ],
        )]

    def process_data(self, data):
        """Process the input data and make it ready for internal storage"""
        label = data.get("label", "").strip()
        is_ally = data.get("is_ally", True)

        entities = []
        for entity in data.get("entities", []):
            record_set_config = entity.get("record_set_config")
            eid = entity.get("eid")
            if not (record_set_config and eid):
                raise AttributeError("Every entity is required to have "
                                     "a record set config and eid.")
            entities.append({
                "record_set_config": record_set_config,
                "eid": eid})

        if not entities:
            raise AttributeError("At least one entity is required submit a "
                                 "key contribution.")
        return dict(
            label=label,
            is_ally=is_ally,
            entities=entities)

    def get_deep_data(self, data):
        """Query the record sets for each of the entities in the data."""
        from frontend.apps.analysis.models import RecordSetConfig

        deep_entities = []
        for entity in data.get("entities", []):
            try:
                config = RecordSetConfig.objects.select_related(
                    "record_set").get(id=entity["record_set_config"])
                record_set = config.record_set
                record_set_worker = record_set.dynamic_class(config, None)
                deep_entity = record_set_worker.get(entity["eid"])

                if deep_entity:
                    deep_entities.extend(
                        d.get_all() if hasattr(d, 'get_all') else d
                        for d in deep_entity)
            except (RecordSetConfig.DoesNotExist, KeyError):
                continue

        data["entities"] = deep_entities
        return data


class CandidateBioController(QuestionSetController):
    def get_fields(self, **kwargs):
        return [
            dict(label="Candidate First Name",
                 type="text",
                 expected=[
                     dict(field="first_name", type="text"),
                 ]),
            dict(label="Candidate Last Name",
                 type="text",
                 expected=[
                     dict(field="last_name", type="text"),
                 ]),
            dict(label="Gender",
                 type="dropdown",
                 add_new_value=True,
                 add_clear=False,
                 sort_choices=False,
                 choices=["Male", "Female", "Decline to state", "Other"],
                 expected=[
                     dict(field="gender", type="text"),
                 ]),
            dict(label="Ethnicity",
                 type="text",
                 expected=[
                     dict(field="ethnicity", type="text"),
                 ]),
            dict(label="Birthdate",
                 type="date",
                 expected=[
                     dict(field="birthdate", type="date"),
                 ]),
            dict(label="Birth City",
                 type="text",
                 required=False,
                 expected=[
                     dict(field="birth_city", type="text"),
                 ]),
            dict(label="Birth State",
                 type="dropdown",
                 add_clear=True,
                 sort_choices=True,
                 required=False,
                 choices=[(x, str(y)) for x, y in STATE_CHOICES],
                 expected=[
                     dict(field="birth_state", type="text"),
                 ]),
        ]


class CampaignInfoController(ClientContributions):
    def get_fields(self, **kwargs):
        contrib_fields = super(CampaignInfoController, self).get_fields(
            **kwargs)
        return [
            dict(label="Political Party",
                 type="dropdown",
                 add_new_value=True,
                 choices=["Democratic", "Republican", "Independent",
                          "Libertarian", "Green"],
                 expected=[
                     dict(field="party", type="text"),
                 ]),
            dict(label="Is the Incumbent",
                 type="boolean",
                 required=False,
                 expected=[
                     dict(field="is_incumbent", type="boolean"),
                 ]),
            dict(label="Election Date",
                 type="date",
                 expected=[
                     dict(field="election", type="date"),
                 ]),
            dict(label="Type of Office",
                 type="dropdown",
                 add_clear=True,
                 choices=['Federal', 'State', 'Local'],
                 expected=[
                     dict(field="office_category", type="text"),
                 ]),
            dict(label="Office Sought",
                 type="dropdown",
                 choice_key="office_category",
                 choices={"Federal": ['President', 'Senate', 'House'],
                          "State": ['Governor', 'Lieutenant Governor',
                                    'Attorney General', 'Secretary of State',
                                    'Controller', 'Treasurer',
                                    'Insurance Commissioner', 'State Senate',
                                    'Assemblyman'],
                          "Local": ['Mayor', 'City Council', 'Superintendent',
                                    "District Attorney", "Sheriff"]},
                 expected=[
                     dict(field="office", type="text"),
                 ]),
            dict(label="District Number",
                 type="text",
                 required=False,
                 expected=[
                     dict(field="district_number", type="text"),
                 ]),
            dict(label="State",
                 type="dropdown",
                 add_clear=True,
                 sort_choices=True,
                 required=False,
                 choices=[(x, str(y)) for x, y in STATE_CHOICES],
                 expected=[
                     dict(field="state", type="text"),
                 ]),
        ] + contrib_fields


class IssuesController(QuestionSetController):
    def get_fields(self, **kwargs):
        return [
            dict(label="Issues",
                 type="list",
                 required=False,
                 expected=[
                     dict(field="issues", type="list"),
                 ]),
        ]


class EducationInfoController(QuestionSetController):
    def get_fields(self, **kwargs):
        return [
            dict(label="Graduate Schools",
                 type="list",
                 required=False,
                 expected=[
                     dict(field="graduate_schools", type="list"),
                 ]),
            dict(label="Undergraduate Schools",
                 type="list",
                 required=False,
                 expected=[
                     dict(field="undergraduate_schools", type="list"),
                 ]),
            dict(label="High Schools",
                 type="list",
                 required=False,
                 expected=[
                     dict(field="high_schools", type="list"),
                 ]),
        ]


class CommitteeInfoController(QuestionSetController):
    def get_fields(self, **kwargs):
        return [
            dict(label="Committee Name",
                 type="text",
                 expected=[
                     dict(field="committee_name", type="text"),
                 ]),
            dict(label="Political Party",
                 type="dropdown",
                 add_new_value=True,
                 choices=["Democratic", "Republican", "Independent",
                          "Libertarian", "Green"],
                 expected=[
                     dict(field="party", type="text"),
                 ]),
            dict(label="State",
                 type="dropdown",
                 add_clear=True,
                 sort_choices=True,
                 required=False,
                 choices=[(x, str(y)) for x, y in STATE_CHOICES],
                 expected=[
                     dict(field="state", type="text"),
                 ]),
        ]


class CommitteeFocusController(ClientContributions):
    def get_fields(self, **kwargs):
        contrib_fields = super(CommitteeFocusController, self)\
                .get_fields(required=True, **kwargs)
        return [
            dict(label="Type of Office",
                 type="dropdown",
                 choices=['Federal', 'State', "Local"],
                 expected=[
                     dict(field="office_category", type="text"),
                 ]),
            dict(label="Office Sought",
                 type="dropdown",
                 choice_key="office_category",
                 choices={"Federal": ['President', 'Senate', 'House'],
                          "State": ['Governor', 'Lieutenant Governor',
                                    'Attorney General', 'Secretary of State',
                                    'Controller', 'Treasurer',
                                    'Insurance Commissioner', 'State Senate',
                                    'Assemblyman'],
                          "Local": ['Mayor', 'City Council', 'Superintendent',
                                    "District Attorney", "Sheriff"]},
                 expected=[
                     dict(field="office", type="text"),
                 ]),
        ] + contrib_fields


class GenderBoostController(QuestionSetController):
    def get_fields(self, **kwargs):
        return [dict(
            label="Male Boost",
            type="text",
            required=False,
            expected=[
                dict(field="male_boost", type="text"),
            ],
        ), dict(
            label="Female Boost",
            type="text",
            required=False,
            expected=[
                dict(field="female_boost", type="text"),
            ],
        ), dict(
            label="Base Increase",
            type="text",
            required=False,
            expected=[
                dict(field="gender_base_increase", type="text"),
            ],
        )]

    def process_data(self, data):
        """Process the input data and make it ready for internal storage"""
        result = {}
        for k, v in data.items():
            try:
                result[k] = float(v)
            except ValueError:
                pass
        return result


class EthnicityBoostController(QuestionSetController):
    def get_fields(self, **kwargs):
        return [
            dict(label="Given Name Target",
                 type="dropdown",
                 choices=('english', 'afram', 'natam', 'spanish', 'basque',
                          'catalan', 'galacian', 'french', 'german', 'hindu',
                          'russian', 'persian', 'arabic', 'japanese',
                          'chinese', 'viet', 'korean', 'yiddish', 'hebrew',
                          'latin', 'greek'),
                 add_clear=True,
                 required=False,
                 expected=[
                     dict(field="given_name_target", type="text"),
                 ]),
            dict(label="Given Name Boost",
                 type="text",
                 required=False,
                 expected=[
                     dict(field="given_name_boost", type="text"),
                 ]),
            dict(label="Family Name Target",
                 type="dropdown",
                 choices=('white', 'black', 'hispanic', 'asian', 'native',
                          'multirace'),
                 add_clear=True,
                 required=False,
                 expected=[
                     dict(field="family_name_target", type="text"),
                 ]),
            dict(label="Family Name Boost",
                 type="text",
                 required=False,
                 expected=[
                     dict(field="family_name_boost", type="text"),
                 ]),
            dict(label="Family Name Boost Threshold",
                 type="text",
                 required=False,
                 expected=[
                     dict(field="family_name_boost_threshold", type="text"),
                 ]),
            dict(label="Base Increase",
                 type="text",
                 required=False,
                 expected=[
                     dict(field="ethnicity_base_increase", type="text"),
                 ]),
        ]

    def process_data(self, data):
        """Process the input data and make it ready for internal storage"""
        result = {}
        for k, v in data.items():
            if 'boost' in k or k == "ethnicity_base_increase":
                try:
                    v = float(v)
                except ValueError:
                    continue
            result[k] = v
        return result


class KeyContribScoringController(QuestionSetController):
    def get_fields(self, **kwargs):
        from frontend.apps.analysis.analyses.signal_actions import (
            KeyContribsScoreMultipliers as KCSM)

        def generate_field(field_, label_, default_):
            return dict(label=label_,
                        type="float",
                        placeholder=default_,
                        required=False,
                        expected=[
                            dict(field=field_, type="float"),
                        ])

        return [generate_field(field, label, default)
                # Options are defined in the SignalAction. Iterate over them
                # to generate the fields
                for field, default, label in KCSM.available_multipliers]

    def process_data(self, data):
        """Process the input data and make it ready for internal storage
        Ignores non-float values.
        """
        result = {}
        for k, v in data.items():
            try:
                v = float(v)
            except ValueError:
                continue
            result[k] = v
        return result


class RecordSetConfigControllerBase(QuestionSetController):
    required = True

    @abstractproperty
    def label(self):
        pass

    @abstractproperty
    def field_name(self):
        pass

    def process_data(self, data):
        """Process the input data and make it ready for internal storage
        Ignores non-float values.
        """
        result = {}
        for k, v in data.items():
            try:
                v = float(v)
            except ValueError:
                continue
            result[k] = v
        return result

    def get_fields(self, analysis_config_id, **kwargs):
        rsc_choices = list(AnalysisConfig.objects.filter(
            pk=analysis_config_id).values_list(
                'data_config__data_set_configs__record_set_configs__pk',
                'data_config__data_set_configs__record_set_configs__title'
            ).distinct())
        return [
            dict(label=self.label,
                 type="dropdown",
                 required=self.required,
                 add_clear=True,
                 sort_choices=True,
                 choices=rsc_choices,
                 expected=[
                     dict(field=self.field_name, type="int"),
                 ]),
        ]


class ClientRecordSetConfigController(RecordSetConfigControllerBase):
    label = "Client Contribution Data RecordSetConfig"
    field_name = "client_record_set_config_id"


class ClientBioRecordSetConfigController(RecordSetConfigControllerBase):
    label = "Client Contributor Bio Data RecordSetConfig"
    field_name = "client_bio_record_set_config_id"


class NonprofitDiamondsInTheRoughController(QuestionSetController):
    def process_data(self, data):
        """Process the input data and make it ready for internal storage
        Ignores non-float values.
        """
        result = {}
        for k, v in data.items():
            try:
                v = float(v)
            except ValueError:
                continue
            result[k] = v
        return result

    def get_fields(self, **kwargs):
        return [
            dict(label="Giving Days",
                 type="float",
                 placeholder=365,
                 required=False,
                 expected=[
                     dict(field="giving_days", type="float"),
                 ]),
            dict(label="Client Feature Title",
                 type="text",
                 required=False,
                 expected=[
                     dict(field="client_feature_title", type="text"),
                 ]),
            dict(label="Nonprofit Feature Title",
                 type="text",
                 required=False,
                 expected=[
                     dict(field="nonprofit_feature_title", type="text"),
                 ]),
            dict(label="Giving Threshold",
                 type="float",
                 placeholder=0.5,
                 required=False,
                 expected=[
                     dict(field="giving_threshold", type="float"),
                 ]),
        ]


class AcademicDiamondsInTheRoughController(QuestionSetController):
    def process_data(self, data):
        """Process the input data and make it ready for internal storage
        Ignores non-float values.
        """
        result = {}
        for k, v in data.items():
            try:
                v = float(v)
            except ValueError:
                continue
            result[k] = v
        return result

    def get_fields(self, analysis_config_id, **kwargs):
        rsc_choices = list(AnalysisConfig.objects.filter(
            pk=analysis_config_id).values_list(
                'data_config__data_set_configs__record_set_configs__pk',
                'data_config__data_set_configs__record_set_configs__title'
            ).distinct())
        return [
            dict(label="record_set_config_id",
                 type="dropdown",
                 required=True,
                 add_clear=True,
                 sort_choices=True,
                 choices=rsc_choices,
                 expected=[
                     dict(field="record_set_config_id", type="int"),
                 ]),
            dict(label="giving_years",
                 type="float",
                 placeholder=3,
                 required=False,
                 expected=[
                     dict(field="giving_years", type="float"),
                 ]),
            dict(label="giving_client_threshold",
                 type="float",
                 placeholder=1000,
                 required=False,
                 expected=[
                     dict(field="giving_client_threshold", type="float"),
                 ]),
            dict(label="giving_aggregate_threshold",
                 type="float",
                 placeholder=10000,
                 required=False,
                 expected=[
                     dict(field="giving_aggregate_threshold", type="float"),
                 ]),
            dict(label="check_years",
                 type="float",
                 placeholder=5,
                 required=False,
                 expected=[
                     dict(field="check_years", type="float"),
                 ]),
            dict(label="check_threshold",
                 type="float",
                 placeholder=0,
                 required=False,
                 expected=[
                     dict(field="check_threshold", type="float"),
                 ]),
        ]


class HighPotentialDonorController(QuestionSetController):
    def process_data(self, data):
        """Process the input data and make it ready for internal storage
        Ignores non-float values.
        """
        result = {}
        for k, v in data.items():
            try:
                v = float(v)
            except ValueError:
                continue
            result[k] = v
        return result

    def get_fields(self, analysis_config_id, **kwargs):
        return [
            dict(label="long_term_year",
                 type="float",
                 placeholder=2000,
                 required=False,
                 expected=[
                     dict(field="long_term_year", type="int"),
                 ]),
            dict(label="long_term_threshold",
                 type="float",
                 placeholder=20000,
                 required=False,
                 expected=[
                     dict(field="long_term_threshold", type="float"),
                 ]),
            dict(label="short_term_years",
                 type="float",
                 placeholder=4,
                 required=False,
                 expected=[
                     dict(field="short_term_years", type="int"),
                 ]),
            dict(label="short_term_threshold",
                 type="float",
                 placeholder=10000,
                 required=False,
                 expected=[
                     dict(field="short_term_threshold", type="float"),
                 ]),
        ]


class ContributionObfuscation(QuestionSetController):

    def get_fields(self, account_id, analysis_config_id):
        return [dict(
            label="Contribution Range",
            type="contrib_range",
            expected=[
                dict(field="ranges", type="struct_list", struct=[
                    dict(field="floor", type="int"),
                    dict(field="ceiling", type="int", required=False),
                    dict(field="display_amount", type="boolean"),
                    dict(field="label", type="text")
                ])

            ],
        )]

    def get_shallow_data(self, data):
        data = data.copy()
        del data["tree_pickle"]
        return data

    def process_data(self, data):
        """Process the input data and make it ready for internal storage"""
        ranges_data = data.get("ranges", [])

        tree = IntervalTree()
        for struct in ranges_data:
            floor = struct.get("floor")
            # Without a lower bound, this range is useless.
            if floor is None:
                continue

            ceiling = struct.get("ceiling")
            # If no upper bound is provided, we assume infinity.
            if ceiling is None:
                ceiling = float('inf')

            display_amount = struct.get("display_amount", False)
            label = struct.get("label")
            if not label and not display_amount:
                raise AttributeError("A label must be provided if the "
                                     "contribution amount is to be hidden.")

            # Add the range to an interval tree.
            tree_args = [floor, ceiling]
            if not display_amount:
                tree_args.append(label)
            tree.addi(*tree_args)

        ### We need to validate the ranges:
        ## First thing is checking the full range should run from 0 to infinity
        if tree.begin() != 0 or tree.end() != float('inf'):
            raise ValueError("The ranges must cover from 0 to Infinity.")

        ## There may not be any overlaps.
        # This checks for the case where upper or lower bounds are equal
        # E.g.: [(1,3), (3,5)]
        overlaps = filter(lambda x: True if x != 1 else False,
                          tree.boundary_table.itervalues())
        if overlaps:
            raise ValueError("The following value(s) should not be equal: "
                             "{}".format(overlaps))
        # This checks for more egregious overlaps
        # E.g.: [(1,5), (3,7)]
        overlaps = tree.list_overlaps()
        if overlaps:
            raise ValueError("The following ranges are overlapping: "
                             "{}".format(overlaps))

        ## There may not be any gaps between ranges.
        gaps = tree.list_gaps()
        if gaps:
            raise ValueError("The ranges include the following gaps: "
                             "{}".format(gaps))

        # If we get here, all is good with the data.
        # Store the tree for faster processing
        data["tree_pickle"] = pickle.dumps(tree)
        return data


class MajorGiftsWeightsPersonaController(QuestionSetController):
    """Controller for Adding Major Gifts Weights Persona QuestionSet."""
    def get_fields(self, account_id, analysis_config_id):
        return [
            dict(label="Demographics Weight",
                 type="float",
                 placeholder=0.0,
                 required=False,
                 expected=[
                     dict(field="dg_weight", type="float")
                 ]),
            dict(label="Wealth Weight",
                 type="float",
                 placeholder=0.0,
                 required=False,
                 expected=[
                     dict(field="wi_weight", type="float")
                 ]),
            dict(label="Philanthropy Weight",
                 type="float",
                 placeholder=0.0,
                 required=False,
                 expected=[
                     dict(field="pi_weight", type="float")
                 ]),
            dict(label="Affinity Weight",
                 type="float",
                 placeholder=0.0,
                 required=False,
                 expected=[
                     dict(field="ai_weight", type="float")
                 ]),
        ]

    def process_data(self, data):
        """Process the input data and make it ready for internal storage"""
        weights = (("dg_weight", "Demographic Weight"),
                   ("wi_weight", "Wealth Weight"),
                   ("pi_weight", "Philanthropy Weight"),
                   ("ai_weight", "Affinity Weight"))
        results = {}
        for field, title in weights:
            w_data = data.get(field, 0.0)
            try:
                result = float(w_data)
            except ValueError:
                continue
            else:
                if result < 0:
                    raise ValueError(f"The {title} must be greater than or"
                                     " equal to 0.")
                results[field] = result
        return dict(results)


class NonProfitOrganizationInfoController(QuestionSetController):
    def get_fields(self, **kwargs):
        return [
            dict(label="Organization Name",
                 type="text",
                 required=True,
                 expected=[
                     dict(field="nfp_organization_name", type="text"),
                 ]),
            dict(label="City",
                 type="text",
                 required=True,
                 expected=[
                     dict(field="city", type="text"),
                 ]),
            dict(label="State",
                 type="dropdown",
                 add_clear=True,
                 sort_choices=True,
                 required=True,
                 choices=[(x, str(y)) for x, y in STATE_CHOICES],
                 expected=[
                     dict(field="state", type="text"),
                 ]),
            dict(label="Zip Code",
                 type="text",
                 required=True,
                 expected=[
                     dict(field="zipcode", type="text"),
                 ]),
            dict(label="Category NTEE Code",
                 type="dropdown",
                 add_clear=True,
                 sort_choices=True,
                 required=True,
                 choices=NTEE_PRIMARY_CATEGORIES,
                 expected=[
                     dict(field="ntee_code", type="text"),
                 ]),
            dict(label="Sub-category NTEE Code",
                 type="dropdown",
                 add_clear=True,
                 sort_choices=True,
                 required=True,
                 choice_key="ntee_code",
                 choices=NTEE_SUB_CATEGORIES,
                 expected=[
                     dict(field="sub_ntee_code", type="text"),
                 ]),
        ]


class NFPKeyContributions(KeyContributions):
    def get_fields(self, account_id, analysis_config_id):
        return [
            dict(label="Is Ally",
                 type="boolean",
                 expected=[
                     dict(field="is_ally", type="boolean"),
                 ]),
            dict(label="Category NTEE Code",
                 type="dropdown",
                 add_clear=True,
                 sort_choices=True,
                 choices=NTEE_PRIMARY_CATEGORIES,
                 expected=[
                     dict(field="ntee_level_1", type="list"),
                 ]),
            dict(label="Sub-category NTEE Code",
                 type="dropdown",
                 add_clear=True,
                 sort_choices=True,
                 choice_key="ntee_level_1",
                 choices=NTEE_SUB_CATEGORIES,
                 expected=[
                     dict(field="ntee_level_2", type="list"),
                 ]),
        ]

    def process_data(self, data):
        """Process the input data and make it ready for internal storage"""
        is_ally = data.get("is_ally", True)

        ntee_level_1_codes = data.get("ntee_level_1", [])
        ntee_level_2_codes = data.get("ntee_level_2", [])
        if len(ntee_level_1_codes) != len(ntee_level_2_codes):
            raise ValueError("ntee_level_1 and ntee_level_2 lists are of "
                             "different lengths")

        # Every category is required to have at-least NTEE level 1 code
        categories = [{'ntee_level_1': l1, 'ntee_level_2': l2} for (l1, l2) in
                      zip(ntee_level_1_codes, ntee_level_2_codes) if l1]

        if not categories:
            raise AttributeError("At least one category is required to submit "
                                 "a key contribution.")

        # Based on the current design, only one ntee code is allowed per
        # label. Yet, the ntee codes are expected to be lists for
        # future-proofing.
        if len(ntee_level_1_codes) != 1:
            raise ValueError("Only one category per label is allowed")
        for (ntee_code_1, ntee_code_2) in zip(
                ntee_level_1_codes, ntee_level_2_codes):
            if not ntee_code_2:
                label = "{0} {1}".format(
                    ntee_code_1,
                    NTEE_PRIMARY_CATEGORIES_RAW.get(ntee_code_1, ''))
            else:
                ntee_sub_categories = NTEE_SUB_CATEGORIES_RAW.get(
                    ntee_code_1, [])
                # Filter NTEE_SUB_CATEGORIES_RAW so that the first item in the
                # tuple is ntee_code_2
                ntee_tuple = list(filter(
                    lambda x: x[0] == ntee_code_2, ntee_sub_categories))
                ntee_name = ntee_tuple[0][1]
                label = "{0}{1} {2}".format(
                    ntee_code_1, ntee_code_2, ntee_name)

        return dict(
            label=label,
            is_ally=is_ally,
            categories=categories
        )
