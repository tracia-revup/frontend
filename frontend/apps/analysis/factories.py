import datetime
from operator import itemgetter
from collections import Iterable

import factory
from factory.fuzzy import FuzzyDate, FuzzyText, FuzzyInteger

from frontend.apps.analysis.models.config_base import OwnershipModel
from frontend.apps.analysis.models import (
    Analysis, AnalysisConfig, AnalysisProfile, PartitionSetConfig, QuestionSet,
    PartitionConfig, FeatureConfig, Feature, AnalysisConfigTemplate, DataSet,
    RecordSet, RecordResource, ResourceInstance, ResourceConfig,
    RecordSetConfig, DataSetConfig, DataConfig, SignalSetConfig,
    SignalActionConfig, SpectrumConfig, SignalSetsActions, QuestionSetData,
    FeatureConfigsSignalSets, Result, FeatureResult, ResultSort)
from frontend.apps.analysis.questionset_controllers import KeyContributions
import frontend.apps.analysis.sources.base as sources
from frontend.apps.contact.factories import ContactFactory


POLCAM_FACTORY = 'frontend.apps.campaign.factories.PoliticalCampaignFactory'

class AnalysisConfigModelFactory(factory.DjangoModelFactory):
    title = FuzzyText()
    description = FuzzyText()


class DynamicClassModelMixinFactory(factory.DjangoModelFactory):
    #TODO: One day populate with real class and module names
    module_name = FuzzyText()
    class_name = FuzzyText()


class OwnershipModelFactory(factory.DjangoModelFactory):
    class Meta:
        model = OwnershipModel

    account = factory.SubFactory(POLCAM_FACTORY)

    @factory.post_generation
    def user(self, create, extracted, **kwargs):
        # Need to do local imports to avoid circular import errors
        from frontend.apps.authorize.factories import RevUpUserFactory
        from frontend.apps.seat.factories import SeatFactory

        _user = extracted if extracted is not None else RevUpUserFactory()
        SeatFactory(account=self.account, user=_user)
        return _user


class DataSetFactory(AnalysisConfigModelFactory):
    class Meta:
        model = DataSet

    dynamic_class = sources.DataSet


class RecordSetFactory(AnalysisConfigModelFactory):
    class Meta:
        model = RecordSet

    dynamic_class = sources.RecordSet
    data_set = factory.SubFactory(DataSetFactory)
    fact_type = factory.Iterator(RecordSet.FactTypes.choices(),
                                 getter=itemgetter(0))


class ContributionRecordSetFactory(RecordSetFactory):
    fact_type = RecordSet.FactTypes.CONTRIB


class EntitiesRecordSetFactory(RecordSetFactory):
    fact_type = RecordSet.FactTypes.POLITICAL_ENTITY


class NonprofitEntitiesRecordSetFactory(RecordSetFactory):
    fact_type = RecordSet.FactTypes.NONPROFIT_ENTITY


class RecordResourceFactory(AnalysisConfigModelFactory):
    class Meta:
        model = RecordResource

    dynamic_class = sources.MongodbRecordResource
    record_set = factory.SubFactory(RecordSetFactory)


class ResourceInstanceFactory(AnalysisConfigModelFactory):
    class Meta:
        model = ResourceInstance

    identifier = FuzzyText()
    record_resource = factory.SubFactory(RecordResourceFactory)


class ResourceConfigFactory(AnalysisConfigModelFactory):
    class Meta:
        model = ResourceConfig

    record_resource = factory.SubFactory(RecordResourceFactory)
    resource_instance = factory.SubFactory(ResourceInstanceFactory)


class RecordSetConfigFactory(AnalysisConfigModelFactory):
    class Meta:
        model = RecordSetConfig

    record_set = factory.SubFactory(RecordSetFactory)
    resource_config = factory.SubFactory(ResourceConfigFactory)


class DataSetConfigFactory(AnalysisConfigModelFactory):
    class Meta:
        model = DataSetConfig

    data_set = factory.SubFactory(DataSetFactory)

    @factory.post_generation
    def record_set_configs(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted is not None:
            if not isinstance(extracted, Iterable):
                extracted = [extracted]
            self.record_set_configs.add(*extracted)
        else:
            self.record_set_configs.add(RecordSetConfigFactory())


class DataConfigFactory(AnalysisConfigModelFactory):
    class Meta:
        model = DataConfig

    @factory.post_generation
    def data_set_configs(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted is not None:
            if not isinstance(extracted, Iterable):
                extracted = [extracted]
            self.data_set_configs.add(*extracted)
        else:
            self.data_set_configs.add(DataSetConfigFactory())


class SpectrumConfigFactory(AnalysisConfigModelFactory):
    class Meta:
        model = SpectrumConfig

    definition = {'a': 'b'}


class SignalActionConfigFactory(AnalysisConfigModelFactory):
    class Meta:
        model = SignalActionConfig

    module_name = 'frontend.apps.analysis.analyses.signal_actions.evaluate'
    class_name = 'SimpleCalculateGiving'
    definition = {}


class SignalSetsActionsFactory(factory.DjangoModelFactory):
    class Meta:
        model = SignalSetsActions

    signal_set_config = factory.SubFactory(
        'frontend.apps.analysis.factories.SignalSetConfigFactory',
        signal_actions=None)
    signal_action_config = factory.SubFactory(SignalActionConfigFactory)
    order = FuzzyInteger(500)


class SignalSetConfigFactory(AnalysisConfigModelFactory):
    class Meta:
        model = SignalSetConfig

    record_set_config = factory.SubFactory(RecordSetConfigFactory)
    spectrum_config = factory.SubFactory(SpectrumConfigFactory)

    @factory.post_generation
    def signal_actions(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted is not None:
            if not isinstance(extracted, Iterable):
                extracted = [extracted]
            for i, extract in enumerate(extracted):
                SignalSetsActionsFactory(signal_set_config=self,
                                         signal_action_config=extract,
                                         order=((i + 1) * 10))
        else:
            SignalSetsActionsFactory(signal_set_config=self,
                                     signal_action_config=SignalActionConfigFactory())


@factory.use_strategy(factory.CREATE_STRATEGY)
class AnalysisFactory(factory.DjangoModelFactory):
    class Meta:
        model = Analysis

    user = factory.SubFactory('frontend.apps.authorize.factories.RevUpUserFactory')
    event = factory.SubFactory('frontend.apps.campaign.factories.EventFactory')
    account = factory.SubFactory(POLCAM_FACTORY)
    created = FuzzyDate(datetime.date(2014, 1, 5))
    modified = created
    analysis_config = factory.SubFactory(
        'frontend.apps.analysis.factories.AnalysisConfigFactory')
    partition_set = factory.SelfAttribute(
        'analysis_config.partition_set_config')
    status = Analysis.STATUSES.complete


class ResultFactory(factory.DjangoModelFactory):
    class Meta:
        model = Result

    score = FuzzyInteger(500)
    key_signals = factory.LazyAttribute(lambda s: {})
    analysis = factory.SubFactory(AnalysisFactory)
    user = factory.SelfAttribute('analysis.user')
    contact = factory.SubFactory(ContactFactory,
                                 user=factory.SelfAttribute('..analysis.user'))
    partition = factory.LazyAttribute(
        lambda self: self.analysis.partition_set.partition_configs.first())
    score_result_sort = factory.RelatedFactory(
        'frontend.apps.analysis.factories.ResultSortFactory', 'result')


class ResultSortFactory(factory.DjangoModelFactory):
    class Meta:
        model = ResultSort
    result = factory.SubFactory(ResultFactory, score_result_sort=None)
    analysis = factory.SelfAttribute('result.analysis')
    partition = factory.SelfAttribute('result.partition')
    contact = factory.SelfAttribute('result.contact')
    identifier = ''
    value_type = ResultSort.ValueTypes.SCORE
    value = factory.SelfAttribute('result.score')


class PartitionConfigFactory(AnalysisConfigModelFactory):
    class Meta:
        model = PartitionConfig

    label = FuzzyText()
    index = factory.fuzzy.FuzzyInteger(low=2000, high=2014, step=2)
    definition = {'type': 'start_date',
                  'start_date':'2012-11-06'}


class PartitionSetConfigFactory(AnalysisConfigModelFactory):
    class Meta:
        model = PartitionSetConfig

    @factory.post_generation
    def partition_configs(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted is not None:
            if not isinstance(extracted, Iterable):
                extracted = [extracted]
            self.partition_configs.add(*extracted)
        else:
            self.partition_configs.add(PartitionConfigFactory())


class AnalysisConfigBaseFactory(AnalysisConfigModelFactory):
    analysis_profile = factory.SubFactory(
        'frontend.apps.analysis.factories.AnalysisProfileFactory',
        default_config_template=None)
    partition_set_config = factory.SubFactory(PartitionSetConfigFactory)
    data_config = factory.SubFactory(DataConfigFactory)


class AnalysisConfigTemplateFactory(AnalysisConfigBaseFactory):
    class Meta:
        model = AnalysisConfigTemplate

    @factory.post_generation
    def ap_set_config(self, create, extracted, **kwargs):
        if not create:
            return
        if not self.analysis_profile.default_config_template:
            self.analysis_profile.default_config_template = self
            self.analysis_profile.save()


class AnalysisConfigFactory(AnalysisConfigBaseFactory, OwnershipModelFactory):
    class Meta:
        model = AnalysisConfig

    @factory.post_generation
    def feature_configs(self, create, extracted, *args, **kwargs):
        if not create:
            return

        if extracted is not None:
            if not isinstance(extracted, Iterable):
                extracted = [extracted]
            self.feature_configs.add(*extracted)
        else:
            self.feature_configs.add(FeatureConfigFactory())


class AnalysisProfileFactory(AnalysisConfigModelFactory):
    class Meta:
        model = AnalysisProfile

    analysis_type = 'DUMMY'
    default_config_template = factory.SubFactory(AnalysisConfigTemplateFactory)
    analysis_view_setting_set = factory.SubFactory(
        'frontend.apps.authorize.factories.AnalysisViewSettingSetFactory')


class FeatureFactory(AnalysisConfigModelFactory):
    class Meta:
        model = Feature

    feature_type = "MATCH"


class FeatureConfigsSignalSetsFactory(factory.DjangoModelFactory):
    class Meta:
        model = FeatureConfigsSignalSets

    feature_config = factory.SubFactory(
        'frontend.apps.analysis.factories.FeatureConfigFactory',
        signal_set_configs=None)
    signal_set_config = factory.SubFactory(SignalSetConfigFactory)
    order = FuzzyInteger(500)


class FeatureConfigFactory(AnalysisConfigModelFactory):
    class Meta:
        model = FeatureConfig

    feature = factory.SubFactory(FeatureFactory)

    @factory.post_generation
    def question_sets(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted is not None:
            if not isinstance(extracted, Iterable):
                extracted = [extracted]
            self.question_sets.add(*extracted)
        else:
            self.question_sets.add(
                QuestionSetFactory(title="Key Contributions"))

    @factory.post_generation
    def signal_set_configs(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted is not None:
            if not isinstance(extracted, Iterable):
                extracted = [extracted]
            for i, extract in enumerate(extracted):
                FeatureConfigsSignalSetsFactory(
                    feature_config=self,
                    signal_set_config=extract,
                    order=((i + 1) * 10))
        else:
            FeatureConfigsSignalSetsFactory(
                feature_config=self,
                signal_set_config=SignalSetConfigFactory())


class FeatureResultFactory(factory.DjangoModelFactory):
    class Meta:
        model = FeatureResult

    result = factory.SubFactory(ResultFactory)
    user = factory.SelfAttribute('result.user')
    feature_config = factory.SubFactory(FeatureConfigFactory)


class QuestionSetFactory(AnalysisConfigModelFactory):
    class Meta:
        model = QuestionSet

    title = "Key Contributions"
    description = None

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        try:
            obj = model_class.objects.get(**kwargs)
            return obj
        except model_class.DoesNotExist:
            # A bit of a hack to make dynamic_class accept a value
            temp = model_class()
            temp.dynamic_class = KeyContributions
            kwargs.setdefault("class_name", temp.class_name)
            kwargs.setdefault("module_name", temp.module_name)
            return super(QuestionSetFactory, cls)._create(
                model_class, *args, **kwargs)


@factory.use_strategy(factory.CREATE_STRATEGY)
class QuestionSetDataFactory(factory.DjangoModelFactory):
    class Meta:
        model = QuestionSetData
