
class AnalysisConfigurationError(Exception):
    """This error indicates an error in AnalysisConfiguration has been
    detected."""


class SkipContact(Exception):
    """This exception is used during analysis to signal that further processing
    for the current contact is not necessary.
    """
