

ranking_view = {
    'contacts': [{
        'id': 0,
        'invited': False,
        'external': False,
        'full_name': "Jim Jones",
        'score': 75,
        'giving': 100000,
        'political_spectrum_class': 'political-spectrum-bar-neutral',
        'score_bar_width': 75,
        'key_contributions': [],
        'notes': 'Coming Soon!'}
    ],
    'ranking_results': [{
        'id': "1067",
        'invited': False,
        "first_name": "John",
        # We don't currently include middle in results, but we should use it
        # when possible in future. May often be blank or initial
        "last_name": "Smith",
        "street_address_1": "123 Fake St",
        "street_address_2": "Apt C",
        "street_address_3": "",
        "city": "Boise",
        "state": "ID",
        "zip": "55555",
        "country": "",
        "phone": "123-456-7890",
        "main_phone": "456-123-0987",
        "email": "email@test.com",
        "company": "Allied Widgets",
        "title": "President",
        "graduation_year": 1964,
        "degree": "BSIM",
        "major": "Industrial Management",
        "school": "School of Management",

        # Open pledge (planned donation in works already) - not sure of format
        "open_pledge": True,
        # is a fundraiser in the account
        "fundraiser": True,
        # suppression file (do not solicit)
        "suppress": False,
        # do not email - separate from suppression - need to see whether we use
        "do_not_email": False,

        # Total giving
        "giving": 1000000.1,
        # Our score
        "score": 99.9,
        # appears in other analysis results
        "in_other_results": True,
        # high-value/sensitive (client data flag)
        "major_contributor": True,

        # NOTE: I think we should make these configurable and not hard-coded
        # (see alternatives below--configuration of analysis will also be
        # available in view). We'll discuss further tomorrow.
        # aggregate donations > $10K
        "total_over_10k": True,
        # has at least one contribution over $5K
        "contrib_over_5k": True,
        # top three contributions (descending by amount, then date)
        "top_3_contributions": [
            {
                "amount": 500000.1,
                "year": 2015,
            },
            {
                "amount": 50000.1,
                "year": 2014,
            },
            {
                "amount": 55555.1,
                "year": 2013,
            },
        ],
        # last contribution over $250
        "last_contribution_over_250":
            {
                "amount": 500000,
                "year": 2014,
            },
        # MORE DYNAMIC ALTERNATIVES BELOW
        # support for multiple aggregate thresholds (in config) - index of max
        "total_over_index": 5,
        # support for multiple aggregate thresholds (in config) - index of max
        "contrib_over_index": 4,
        # top n contributions (descending by amount, then date)
        "top_contributions_for": "Purdue",
        "top_contributions":
            [
                {
                    "amount": 500000,
                    "year": 2014,
                },
                {
                    "amount": 50000,
                    "year": 2014,
                },
                {
                    "amount": 50000,
                    "year": 2013,
                },
                {
                    "amount": 500000,
                    "year": 2014,
                },
                {
                    "amount": 50000,
                    "year": 2014,
                },
                {
                    "amount": 50000,
                    "year": 2013,
                },
            ],
        # last contribution over config minimum
        "last_contribution_over_minimum":
            {
                "amount": 500000,
                "year": 2014,
            },

        # data for a volunteer
        "volunteer_contributions":
            [
                {
                    "ranking": "Major Gifts",
                    "year": 14,
                    "low_amount": 99999,
                    "high_amount": 1000000
                },
                {
                    "ranking": "Mid Level",
                    "year": 13,
                    "low_amount": 7512,
                    "high_amount": 12345
                },
                {
                    "ranking": "Annual Donor",
                    "year": 12,
                    "low_amount": 1,
                    "high_amount": 1234
                }
            ],


        # NOTE: Please ignore the following for now, just for my later ref.
        "contributions": {
            "type": "ClientData",

            "name": "Purdue Giving",

            # not settled how we'll do this internally, but expecting that the
            # view ultimately need not know how to determine this config
            # for instance, max vs no max, opacity, captioning, etc
            # For now, let's just plan on a base URL in data view receives
            "image": "URL",

            # some number of past giving results, descending order by date
            "matches": [
                {
                    "amount": 500,
                    "year": 2014,
                }
            ]
        }
    },
    {
        'id': "1067",
        'invited': True,
        "first_name": "VeryLongFirstName",
        # We don't currently include middle in results, but we should use it
        # when possible in future. May often be blank or initial
        "middle_name": "",
        "last_name": "WithAnEvenLongLastName",
        "street_address_1": "123 Fake St",
        "street_address_2": "Apt C",
        "street_address_3": "",
        "city": "New Orleans",
        "state": "LA",
        "zip": "55555",
        "country": "",
        "mobile_phone": "555-555-5555",
        "work_phone": "555-555-1212",
        "phone": "123-456-7890",
        "main_phone": "456-123-0987",
        "email": "emailemailemailemailemailemailemail@test.com",
        "home_email": "home@test.com",
        "work_email": "work@test.com",
        "company": "Saints",
        "title": "Quarterback Quarterback Quarterback Quarterback Quarterback Quarterback Quarterback",
        "graduation_year": 1964,
        "degree": "BSIM",
        "major": "Industrial Management Industrial Management IndustrialManagementIndustrialManagement IndustrialManagementIndustrialManagement IndustrialManagementIndustrialManagement IndustrialManagementIndustrialManagement Industrial Management Industrial Management",
        "school": "Liberal Arts",

        # Open pledge (planned donation in works already) - not sure of format
        "open_pledge": True,
        # is a fundraiser in the account
        "fundraiser": True,
        # suppression file (do not solicit)
        "suppress": True,
        # do not email - separate from suppression - need to see whether we use
        "do_not_email": True,

        # Total giving
        "giving": 1000000,
        # Our score
        "score": 99.9,
        # appears in other analysis results
        "in_other_results": False,
        # high-value/sensitive (client data flag)
        "major_contributor": True,

        # NOTE: I think we should make these configurable and not hard-coded
        # (see alternatives below--configuration of analysis will also be
        # available in view). We'll discuss further tomorrow.
        # aggregate donations > $10K
        "total_over_10k": True,
        # has at least one contribution over $5K
        "contrib_over_5k": True,
        # top three contributions (descending by amount, then date)
        "top_3_contributions": [
            {
                "amount": 500000,
                "year": 2015,
            },
            {
                "amount": 50000,
                "year": 2014,
            },
            {
                "amount": 55555,
                "year": 2013,
            },
        ],
        # last contribution over $250
        "last_contribution_over_250":
            {
                "amount": 500000,
                "year": 2014,
            },
        # MORE DYNAMIC ALTERNATIVES BELOW
        # support for multiple aggregate thresholds (in config) - index of max
        "total_over_index": 5,
        # support for multiple aggregate thresholds (in config) - index of max
        "contrib_over_index": 4,
        # top n contributions (descending by amount, then date)
        "top_contributions_for": "Purdue",
        "top_contributions":
            [
                {
                    "amount": 500000,
                    "year": 2014,
                },
                {
                    "amount": 50000,
                    "year": 2014,
                },
                {
                    "amount": 50000,
                    "year": 2013,
                },
                {
                    "amount": 500000,
                    "year": 2014,
                },
                {
                    "amount": 50000,
                    "year": 2014,
                },
                {
                    "amount": 50000,
                    "year": 2013,
                },
            ],
        # last contribution over config minimum
        "last_contribution_over_minimum":
            {
                "amount": 500000,
                "year": 2014,
            },

        # data for a volunteer
        "volunteer_contributions":
            [
                {
                    "ranking": "Major Gifts",
                    "year": 14,
                    "low_amount": 99999,
                    "high_amount": 1000000
                },
                {
                    "ranking": "Mid Level",
                    "year": 13,
                    "low_amount": 7512,
                    "high_amount": 12345
                },
                {
                    "ranking": "Annual Donor",
                    "year": 12,
                    "low_amount": 1,
                    "high_amount": 1234
                }
            ],


        # NOTE: Please ignore the following for now, just for my later ref.
        "contributions": {
            "type": "ClientData",

            "name": "Purdue Giving",

            # not settled how we'll do this internally, but expecting that
            # the view ultimately need not know how to determine this
            # config for instance, max vs no max, opacity, captioning, etc
            # For now, let's just plan on a base URL in data view receives
            "image": "URL",

            # some number of past giving results, descending order by date
            "matches": [
                {
                    "amount": 500,
                    "year": 2014,
                }
            ]
        }
    },
    {
        'id': "1067",
        'invited': True,
        "first_name": "Drew",
        # We don't currently include middle in results, but we should use it
        # when possible in future. May often be blank or initial
        "middle_name": "",
        "last_name": "Brees",
        "street_address_1": "123 Fake St",
        "street_address_2": "Apt C",
        "street_address_3": "",
        "city": "New Orleans",
        "state": "LA",
        "zip": "55555",
        "country": "",
        "mobile_phone": "555-555-5555",
        "work_phone": "555-555-1212",
        "phone": "123-456-7890",
        "main_phone": "456-123-0987",
        "email": "email@test.com",
        "home_email": "home@test.com",
        "work_email": "work@test.com",
        "company": "Saints",
        "title": "Quarterback",
        "graduation_year": 1964,
        "degree": "BSIM",
        "major": "Industrial Management",
        "school": "Liberal Arts",

        # Open pledge (planned donation in works already) - not sure of format
        "open_pledge": True,
        # is a fundraiser in the account
        "fundraiser": True,
        # suppression file (do not solicit)
        "suppress": True,
        # do not email - separate from suppression - need to see whether we use
        "do_not_email": True,

        # Total giving
        "giving": {'ranking': 'President Council',
                   'low_amount': 7500,
                   'high_amount': 10000},
        # Our score
        "score": 99.9,
        # appears in other analysis results
        "in_other_results": False,
        # high-value/sensitive (client data flag)
        "major_contributor": True,

        # NOTE: I think we should make these configurable and not hard-coded
        # (see alternatives below--configuration of analysis will also be
        # available in view). We'll discuss further tomorrow.
        # aggregate donations > $10K
        "total_over_10k": True,
        # has at least one contribution over $5K
        "contrib_over_5k": True,
        # top three contributions (descending by amount, then date)
        "top_3_contributions": [
            {'ranking': 'President Council', "year": 2015,
             'low_amount': 7500, 'high_amount': 10000},
            {'ranking': "Annual Donor", "year": 2011,
             'low_amount': 501, 'high_amount': 750},
            {'ranking': 'Annual Donor', "year": 2013,
             'low_amount': 251, 'high_amount': 500},
        ],
        # last contribution over $250
        "last_contribution_over_250":
            {
                "amount": 500000,
                "year": 2014,
            },
        # MORE DYNAMIC ALTERNATIVES BELOW
        # support for multiple aggregate thresholds (in config) - index of max
        "total_over_index": 5,
        # support for multiple aggregate thresholds (in config) - index of max
        "contrib_over_index": 4,
        # top n contributions (descending by amount, then date)
        "top_contributions_for": "Purdue",
        "top_contributions":
            [
                {
                    "amount": 500000,
                    "year": 2014,
                },
                {
                    "amount": 50000,
                    "year": 2014,
                },
                {
                    "amount": 50000,
                    "year": 2013,
                },
                {
                    "amount": 500000,
                    "year": 2014,
                },
                {
                    "amount": 50000,
                    "year": 2014,
                },
                {
                    "amount": 50000,
                    "year": 2013,
                },
            ],
        # last contribution over config minimum
        "last_contribution_over_minimum":
            {
                "amount": 500000,
                "year": 2014,
            },

        # data for a volunteer
        "volunteer_contributions":
            [
                {
                    "ranking": "Major Gifts",
                    "year": 14,
                    "low_amount": 99999,
                    "high_amount": 1000000
                },
                {
                    "ranking": "Mid Level",
                    "year": 13,
                    "low_amount": 7512,
                    "high_amount": 12345
                },
                {
                    "ranking": "Annual Donor",
                    "year": 12,
                    "low_amount": 1,
                    "high_amount": 1234
                }
            ],


        # NOTE: Please ignore the following for now, just for my later ref.
        "contributions": {
            "type": "ClientData",

            "name": "Purdue Giving",

            # not settled how we'll do this internally, but expecting that
            # the view ultimately need not know how to determine this
            # config for instance, max vs no max, opacity, captioning, etc
            # For now, let's just plan on a base URL in data view receives
            "image": "URL",

            # some number of past giving results, descending order by date
            "matches": [
                {
                    "amount": 500,
                    "year": 2014,
                }
            ]
        }
    }]
}

