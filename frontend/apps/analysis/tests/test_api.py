from datetime import datetime, timedelta
from unittest import expectedFailure
import json

from django.urls import reverse

from frontend.apps.analysis.api.views import EntitySearchAPI
from frontend.apps.analysis.factories import (
    AnalysisConfigFactory, FeatureConfigFactory,
    AnalysisFactory, ResultFactory, QuestionSetFactory)
from frontend.apps.analysis.models import QuestionSetData
from frontend.apps.analysis.questionset_controllers import NFPKeyContributions
from frontend.apps.authorize.models import RevUpTask
from frontend.apps.authorize.test_utils import (AuthorizedUserTestCase,
                                                NoPermissionMixin)
from frontend.apps.campaign.factories import (AccountFactory,
                                              AccountProfileFactory)
from frontend.apps.campaign.models import AccountProfile
from frontend.apps.role.permissions import AdminPermissions
from frontend.apps.contact.factories import ContactFactory, ImportConfigFactory
from frontend.apps.contact_set.models import ContactSet, ContactSetsSeats
from frontend.apps.filtering.models import Filter
from frontend.apps.filtering.factories import ContactIndexFactory, FilterFactory
from frontend.apps.filtering.filter_controllers.contacts.gender import GenderFilter
from frontend.apps.seat.factories import SeatFactory
from frontend.apps.seat.models import Seat
from frontend.libs.utils.model_utils import DynamicClassModelMixin
from frontend.libs.utils.test_utils import APITestCase


class EntitySearchAPITests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        super(EntitySearchAPITests, self).setUp(login_now=False, **kwargs)
        self.account = self.user.current_seat.account
        self.analysis_config = AnalysisConfigFactory(account=self.account)
        self.url = reverse('entity_search_api-list',
                           args=(self.account.id, self.analysis_config.id))

    def test_permissions(self):
        # # Verify any unauthorized user is rejected
        result = self.client.get(self.url)
        self.assertNoCredentials(result)

        # Login the user
        self._login(self.user)
        # Verify user is not an admin, or that would invalidate the test.
        self.assertFalse(self.user.is_admin)

        ## Verify a standard user is rejected from querying
        result = self.client.get(self.url)
        self.assertNoPermission(result)

        # Add the correct permissions
        self._add_admin_group(permissions=AdminPermissions.CONFIG_FEATURES)
        result = self.client.get(self.url)
        # Verify access is granted
        self.assertContains(result, "results")

    def test_get_political(self):
        self._login_and_add_admin_group(
            self.user, permissions=AdminPermissions.CONFIG_FEATURES)
        result = self.client.get(self.url, data={'q': 'e'})
        self.verify_serializer(
            result, ("id", "title", "description", "allow_multiple"))

    def test_key_contributions_unexpected_search(self):
        """The search should no break if the users enters some weird or
        unexpected search terms."""
        self._login_and_add_admin_group(
            self.user, permissions=AdminPermissions.CONFIG_FEATURES)

        # Test query with double quotes passes.
        data = {'q': "American"}
        result = self.client.get(self.url, data=data)
        self.assertTrue(result.status_code, 200)
        q_dict = EntitySearchAPI._build_query(data.get('q'))
        self.assertEquals(q_dict.get('target'), data.get('q'))

        # Test query with single and double quotes passes.
        data = {'q': '"American Oversight"'}
        result = self.client.get(self.url, data=data)
        self.assertTrue(result.status_code, 200)
        q_dict = EntitySearchAPI._build_query(data.get('q'))
        self.assertEquals(q_dict.get('target'), data.get('q'))

        # Test a list inside of a string passes.
        data = {'q': '["American Oversight", "aa"]'}
        result = self.client.get(self.url, data=data)
        self.assertTrue(result.status_code, 200)
        q_dict = EntitySearchAPI._build_query(data.get('q'))
        self.assertEquals(q_dict.get('target'), data.get('q'))

        # Test an empty list passes.
        data = {'q': '[]'}
        result = self.client.get(self.url, data=data)
        self.assertTrue(result.status_code, 200)
        q_dict = EntitySearchAPI._build_query(data.get('q'))
        self.assertEquals(q_dict.get('target'), data.get('q'))

        # Test a number and string inside a list passes.
        data = {'q': '[123, "Some string"]'}
        result = self.client.get(self.url, data=data)
        self.assertTrue(result.status_code, 200)
        q_dict = EntitySearchAPI._build_query(data.get('q'))
        self.assertEquals(q_dict.get('target'), data.get('q'))


class ContactSetAnalysisResultsViewSetTests(NoPermissionMixin, APITestCase):
    @classmethod
    def setUpTestData(cls):
        import_config = ImportConfigFactory()
        gender_filter = FilterFactory(dynamic_class=GenderFilter,
                                      filter_type=Filter.FilterTypes.CONTACT,
                                      title='Gender Filter')
        import_config.filters.add(gender_filter)

        # Create users
        cls.admin_seat = SeatFactory(permissions=(Seat.AdminPermissions.all() +
                                                  Seat.FundraiserPermissions.all()),
                                     user__import_config=import_config,
                                     account__account_user__import_config=import_config)
        cls.account = cls.admin_seat.account
        cls.account_user = cls.account.account_user
        cls.admin_user = cls.admin_seat.user
        cls.fundraiser_seat = SeatFactory(account=cls.account,
                                          user__import_config=import_config)
        cls.fundraiser_user = cls.fundraiser_seat.user
        # Create an admin seat on another campaign for fundraiser user in order
        # to verify that admin permissions on one account don't translate into
        # effective admin permissions for contact sets on another account.
        cls.fundraiser_seat2 = SeatFactory(
            permissions=(Seat.AdminPermissions.all() +
                         Seat.FundraiserPermissions.all()),
            user=cls.fundraiser_user)

        # Create contacts for each user
        cls.account_contacts_male = [
            ci.contact for ci in ContactIndexFactory.create_batch(
                5, user=cls.account_user, attribs={'gender': 'M'})]
        cls.account_contacts_female = [
            ci.contact for ci in ContactIndexFactory.create_batch(
                5, user=cls.account_user, attribs={'gender': 'F'})]
        cls.account_contacts = (cls.account_contacts_male +
                                cls.account_contacts_female)
        cls.admin_contacts_male = [
            ci.contact for ci in ContactIndexFactory.create_batch(
                5, user=cls.admin_user, attribs={'gender': 'M'})]
        cls.admin_contacts_female = [
            ci.contact for ci in ContactIndexFactory.create_batch(
                5, user=cls.admin_user, attribs={'gender': 'F'})]
        cls.admin_contacts = (cls.admin_contacts_male +
                              cls.admin_contacts_female)
        cls.fundraiser_contacts_male = [
            ci.contact for ci in ContactIndexFactory.create_batch(
                5, user=cls.fundraiser_user, attribs={'gender': 'M'})]
        cls.fundraiser_contacts_female = [
            ci.contact for ci in ContactIndexFactory.create_batch(
                5, user=cls.fundraiser_user, attribs={'gender': 'F'})]
        cls.fundraiser_contacts = (cls.fundraiser_contacts_male +
                                   cls.fundraiser_contacts_female)

        def _create_results(analysis, contacts):
            user = analysis.user
            return [ResultFactory(analysis=analysis, user=user,
                                  contact=contact)
                    for contact in contacts]

        # Create analysis for each user
        cls.analysis_config = AnalysisConfigFactory(account=cls.account)
        cls.account_analysis = AnalysisFactory(
            account=cls.account, user=cls.account_user,
            analysis_config=cls.analysis_config, event=None)
        cls.account_analysis_results = _create_results(cls.account_analysis,
                                                       cls.account_contacts)

        cls.admin_analysis = AnalysisFactory(
            account=cls.account, user=cls.admin_user,
            analysis_config=cls.analysis_config, event=None)
        cls.admin_analysis_results = _create_results(cls.admin_analysis,
                                                     cls.admin_contacts)

        cls.fundraiser_analysis = AnalysisFactory(
            account=cls.account, user=cls.fundraiser_user,
            analysis_config=cls.analysis_config, event=None)
        cls.fundraiser_analysis_results = _create_results(
            cls.fundraiser_analysis, cls.fundraiser_contacts)

        # Create ContactSets (all, male, female) for each user
        cls.account_cs_root = ContactSet.get_or_create_root(
            cls.account, cls.account_user, cls.account_analysis)
        cls.account_cs_male = ContactSet.create(title="males",
                                                query_args={'gender': 'M'},
                                                parents=[cls.account_cs_root],
                                                filters=[gender_filter])
        cls.account_cs_female = ContactSet.create(title="females",
                                                  query_args={'gender': 'F'},
                                                  parents=[cls.account_cs_root],
                                                  filters=[gender_filter])
        cls.account_cs_female_shared = ContactSet.create(title="females shared",
                                                         query_args={'gender': 'F'},
                                                         parents=[cls.account_cs_root],
                                                         filters=[gender_filter])
        cls.admin_cs_root = ContactSet.get_or_create_root(
            cls.account, cls.admin_user, cls.admin_analysis)
        cls.admin_cs_male = ContactSet.create(title="males",
                                              query_args={'gender': 'M'},
                                              parents=[cls.admin_cs_root],
                                              filters=[gender_filter])
        cls.admin_cs_female = ContactSet.create(title="females",
                                                query_args={'gender': 'F'},
                                                parents=[cls.admin_cs_root],
                                                filters=[gender_filter])
        cls.admin_cs_female_shared = ContactSet.create(title="females shared",
                                                       query_args={'gender': 'F'},
                                                       parents=[cls.admin_cs_root],
                                                       filters=[gender_filter])
        cls.fundraiser_cs_root = ContactSet.get_or_create_root(
            cls.account, cls.fundraiser_user, cls.fundraiser_analysis)
        cls.fundraiser_cs_male = ContactSet.create(title="males",
                                                   query_args={'gender': 'M'},
                                                   parents=[cls.fundraiser_cs_root],
                                                   filters=[gender_filter])
        cls.fundraiser_cs_female = ContactSet.create(title="females",
                                                     query_args={'gender': 'F'},
                                                     parents=[cls.fundraiser_cs_root],
                                                     filters=[gender_filter])
        cls.fundraiser_cs_female_shared = ContactSet.create(title="females shared",
                                                            query_args={'gender': 'F'},
                                                            parents=[cls.fundraiser_cs_root],
                                                            filters=[gender_filter])
        # Share fundraiser CS with admin user
        ContactSetsSeats.objects.create(
            contact_set=cls.fundraiser_cs_female_shared,
            seat=cls.admin_seat)
        # Share admin CS with fundraiser user
        ContactSetsSeats.objects.create(
            contact_set=cls.admin_cs_female_shared,
            seat=cls.fundraiser_seat)
        # Share account CS with fundraiser user
        ContactSetsSeats.objects.create(
            contact_set=cls.account_cs_female_shared,
            seat=cls.fundraiser_seat)

    def test_disabled_seat(self):
        # This test is to verify that users with revoked seats are not able to
        # access contact sets on those accounts.

        def _get_response_factory(seat):
            def inner(postfix, contact_set, contact=None):
                args = (seat.user_id, seat.id, contact_set.id)
                if contact is not None:
                    args += (contact.id,)
                url = reverse(
                    'contact_set_analysis_contact_results_api-{}'.format(
                        postfix), args=args)
                return self.client.get(url)
            return inner

        dead_admin_seat = SeatFactory(
            permissions=(Seat.AdminPermissions.all() +
                         Seat.FundraiserPermissions.all()),
            account=self.account, state=Seat.States.ACTIVE)
        dead_fundraiser_seat = SeatFactory(
            permissions=Seat.FundraiserPermissions.all(),
            account=self.account, state=Seat.States.ACTIVE)
        # Share admin CS with dead fundraiser user
        ContactSetsSeats.objects.create(
            contact_set=self.admin_cs_female_shared,
            seat=dead_fundraiser_seat)
        # Share account CS with dead fundraiser user
        ContactSetsSeats.objects.create(
            contact_set=self.account_cs_female_shared,
            seat=dead_fundraiser_seat)

        _get_response_admin = _get_response_factory(dead_admin_seat)
        _get_response_fundraiser = _get_response_factory(dead_fundraiser_seat)

        self.client.force_authenticate(user=dead_admin_seat.user)

        # Contrl tests to verify that when the admin seat state is active, the
        # seat is able to successfully access the results for account contact
        # sets.
        response = _get_response_admin('list', self.account_cs_female)
        self.assertEqual(response.status_code, 200)

        response = _get_response_admin('detail', self.account_cs_female,
                                       self.account_contacts_female[0])
        self.assertEqual(response.status_code, 200)

        # Revoke admin's seat
        dead_admin_seat.state = Seat.States.REVOKED
        dead_admin_seat.save()

        response = _get_response_admin('list', self.account_cs_female)
        self.assertEqual(response.status_code, 404)

        response = _get_response_admin('detail', self.account_cs_female,
                                       self.account_contacts_female[0])
        self.assertEqual(response.status_code, 404)

        self.client.force_authenticate(user=dead_fundraiser_seat.user)

        # Contrl tests to verify that when the seat state is active, the seat
        # is able to successfully access the results for the contact set.
        response = _get_response_fundraiser('list',
                                            self.admin_cs_female_shared)
        self.assertEqual(response.status_code, 200)
        response = _get_response_fundraiser('list',
                                            self.account_cs_female_shared)
        self.assertEqual(response.status_code, 200)
        response = _get_response_fundraiser('detail',
                                            self.admin_cs_female_shared,
                                            self.admin_contacts_female[0])
        self.assertEqual(response.status_code, 200)
        response = _get_response_fundraiser('detail',
                                            self.account_cs_female_shared,
                                            self.account_contacts_female[0])
        self.assertEqual(response.status_code, 200)

        dead_fundraiser_seat.state = Seat.States.REVOKED
        dead_fundraiser_seat.save()

        response = _get_response_fundraiser('list',
                                            self.admin_cs_female_shared)
        self.assertEqual(response.status_code, 404)
        response = _get_response_fundraiser('list',
                                            self.account_cs_female_shared)
        self.assertEqual(response.status_code, 404)
        response = _get_response_fundraiser('detail',
                                            self.admin_cs_female_shared,
                                            self.admin_contacts_female[0])
        self.assertEqual(response.status_code, 404)
        response = _get_response_fundraiser('detail',
                                            self.account_cs_female_shared,
                                            self.account_contacts_female[0])
        self.assertEqual(response.status_code, 404)

    @expectedFailure
    def test_url_fiddling(self):
        # This test currently fails because the API returning the result
        # contents
        self.client.force_authenticate(user=self.fundraiser_user)

        url = reverse('contact_set_analysis_contact_results_api-list',
                      args=(self.fundraiser_user.id, self.fundraiser_seat2.id,
                            self.account_cs_female.id))
        response = self.client.get(url)
        self.assertNoPermission(response)

        url = reverse('contact_set_analysis_contact_results_api-list',
                      args=(self.fundraiser_user.id, self.fundraiser_seat2.id,
                            self.account_cs_female_shared.id))
        response = self.client.get(url)
        self.assertNoPermission(response)

    def test_fundraiser_permissions(self):
        # Permission tests as fundraiser user.
        #
        self.client.force_authenticate(user=self.fundraiser_user)

        def _get_response(postfix, contact_set, contact=None, data=None):
            args = (self.fundraiser_user.id, self.fundraiser_seat.id,
                    contact_set.id)
            if contact is not None:
                args += (contact.id,)
            url = reverse(
                'contact_set_analysis_contact_results_api-{}'.format(postfix),
                args=args)
            return self.client.get(url, data=data)

        # LIST

        # Action is allowed because user owns the contact set
        response = _get_response('list', self.fundraiser_cs_female)
        self.assertEqual(response.status_code, 200)

        # Action is not allowed because other user's contact set has not been
        # shared with us.
        response = _get_response('list', self.admin_cs_female)
        self.assertNoPermission(response)

        # Action is allowed because other user's contact set has been shared
        # with us.
        response = _get_response('list', self.admin_cs_female_shared)
        self.assertEqual(response.status_code, 200)

        # Action is not allowed because account contact set has not been shared
        # with us, and we do not have the admin permission VIEW_CONTACTS.
        # We are also verifying that the VIEW_CONTACTS permission on a seat
        # attached to another account does not translate to admin permissions
        # for this account.
        response = _get_response('list', self.account_cs_female)
        self.assertNoPermission(response)

        # Action is allowed because account contact set has been shared with
        # us.
        response = _get_response('list', self.account_cs_female_shared)
        self.assertEqual(response.status_code, 200)

        ## Verify access of a specified analysis
        # Contact Set was shared with user, so they have access to the analysis
        response = _get_response('list', self.account_cs_female_shared,
                                 data={"analysis": self.account_analysis.id})
        self.assertEqual(response.status_code, 200)
        # User doesn't have access to admin analysis, should reject
        response = _get_response('list', self.account_cs_female_shared,
                                 data={"analysis": self.admin_analysis.id})
        self.assertEqual(response.status_code, 404)
        # Analysis account doesn't match contact set, should reject
        response = _get_response('list', self.account_cs_female_shared,
                                 data={"analysis": self.fundraiser_analysis.id})
        self.assertEqual(response.status_code, 404)
        # User owns CS and analysis
        response = _get_response('list', self.fundraiser_cs_female,
                                 data={"analysis": self.fundraiser_analysis.id})
        self.assertEqual(response.status_code, 200)

        # DETAIL

        # Action is allowed because user owns the contact set
        response = _get_response('detail', self.fundraiser_cs_female,
                                 self.fundraiser_contacts_female[0])
        self.assertEqual(response.status_code, 200)

        # Action is not allowed because other user's contact set has not been
        # shared with us.
        response = _get_response('detail', self.admin_cs_female,
                                 self.admin_contacts_female[0])
        self.assertNoPermission(response)

        # Action is allowed because other user's contact set has been shared
        # with us.
        response = _get_response('detail', self.admin_cs_female_shared,
                                 self.admin_contacts_female[0])
        self.assertEqual(response.status_code, 200)

        # Action is not allowed because account contact set has not been shared
        # with us, and we do not have the admin permission VIEW_CONTACTS
        response = _get_response('detail', self.account_cs_female,
                                 self.account_contacts_female[0])
        self.assertNoPermission(response)

        # Action is allowed because account contact set has been shared with
        # us.
        response = _get_response('detail', self.account_cs_female_shared,
                                 self.account_contacts_female[0])
        self.assertEqual(response.status_code, 200)

    def test_admin_permissions(self):
        self.client.force_authenticate(user=self.admin_user)

        def _get_response(postfix, contact_set, contact=None, data=None):
            args = (self.admin_user.id, self.admin_seat.id, contact_set.id)
            if contact is not None:
                args += (contact.id,)
            url = reverse(
                'contact_set_analysis_contact_results_api-{}'.format(postfix),
                args=args)
            return self.client.get(url, data=data)

        # List

        # Admin user is allowed to list results for ContactSet admin_cs_female
        # because they own that contact set.
        response = _get_response('list', self.admin_cs_female)
        self.assertEqual(response.status_code, 200)

        # Admin user is allowed to list results for account ContactSet
        # account_cs_female because they have the admin permission
        # VIEW_CONTACTS on their seat.
        response = _get_response('list', self.account_cs_female)
        self.assertEqual(response.status_code, 200)
        # Preconditions and sanity check for previous test
        self.assertEqual(self.admin_seat.user_id, self.admin_user.id)
        self.assertIn(Seat.AdminPermissions.VIEW_CONTACTS,
                      self.admin_seat.permissions)

        # Admin user does not have permission to list results on
        # fundraiser_user's contact set because it has not been shared with
        # them.
        response = _get_response('list', self.fundraiser_cs_female)
        self.assertNoPermission(response)

        # User is allowed to view contact set fundraiser_cs_female_shared
        # because it has been shared with them.
        response = _get_response('list', self.fundraiser_cs_female_shared)
        self.assertEqual(response.status_code, 200)

        ## Verify access of a specified analysis
        # Contact Set was shared with user, so they have access to the analysis
        response = _get_response('list', self.account_cs_female,
                                 data={"analysis": self.account_analysis.id})
        self.assertEqual(response.status_code, 200)
        # User has access to admin
        response = _get_response('list', self.admin_cs_female,
                                 data={"analysis": self.admin_analysis.id})
        self.assertEqual(response.status_code, 200)
        # Analysis account doesn't match contact set, should reject
        response = _get_response('list', self.account_cs_female,
                                 data={"analysis": self.fundraiser_analysis.id})
        self.assertEqual(response.status_code, 404)

        # Detail

        # Action is allowed because user owns the contact set.
        response = _get_response('detail', self.admin_cs_female,
                                 self.admin_contacts_female[0])
        self.assertEqual(response.status_code, 200)

        # Action is allowed because user has VIEW_CONTACTS permission
        response = _get_response('detail', self.account_cs_female,
                                 self.account_contacts_female[0])
        self.assertEqual(response.status_code, 200)

        # Action is not allowed because contact set is owned by another user,
        # and it has not been shared with us.
        response = _get_response('detail', self.fundraiser_cs_female,
                                 self.fundraiser_contacts_female[0])
        self.assertNoPermission(response)

        # Action is allowed because fundraiser_user's contact set
        # fundraiser_cs_female_shared has been shared with admin_user.
        response = _get_response('detail', self.fundraiser_cs_female_shared,
                                 self.fundraiser_contacts_female[0])
        self.assertEqual(response.status_code, 200)

    def test_404_when_contact_not_in_contact_set(self):
        self.client.force_authenticate(user=self.admin_user)
        # If we try to view a result that is present in the analysis, but is
        # excluded by the contact set filters, we should get a 404 error that
        # the result detail does not exist.
        url = reverse(
            'contact_set_analysis_contact_results_api-detail',
            args=(self.admin_user.id, self.admin_seat.id,
                  self.admin_cs_female.id, self.admin_contacts_male[0].id))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)


class QuestionSetViewSetTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        super(QuestionSetViewSetTests, self).setUp(login_now=False,
                                                      **kwargs)
        self.account = self.user.current_seat.account
        self.analysis_config = AnalysisConfigFactory(account=self.account)
        self.feature_config = FeatureConfigFactory(account=self.account)
        self.question_set = self.feature_config.question_sets.first()

        self.analysis_config.feature_configs.add(self.feature_config)
        self.url = reverse('question_set_api-list',
                           args=(self.account.id, self.analysis_config.id))
        self.url2 = reverse('question_set_api-detail',
                            args=(self.account.id, self.analysis_config.id,
                                  self.question_set.id))

    def test_permissions(self):
        # # Verify any unauthorized user is rejected
        result = self.client.get(self.url)
        self.assertNoCredentials(result)
        result = self.client.get(self.url2)
        self.assertNoCredentials(result)

        # Login the user
        self._login(self.user)
        # Verify user is not an admin, or that would invalidate the test.
        self.assertFalse(self.user.is_admin)
        self.assertFalse(self.user.is_account_admin())

        ## Verify a standard user is rejected from accessing feature configs
        result = self.client.get(self.url)
        self.assertNoPermission(result)
        ## Verify a standard user is rejected from configuring a feature config
        result = self.client.post(self.url, data={'test': '123'})
        self.assertNoPermission(result)

        # Add the correct permissions to the seat
        self._add_admin_group(self.user,
                              permissions=AdminPermissions.CONFIG_FEATURES)

        ## Verify the list API accepts the user
        result = self.client.get(self.url)
        self.assertContains(result, "")

        ## Verify detail api redirects to data api
        result = self.client.get(self.url2)
        self.assertRedirects(
            result, reverse('question_set_data_api-list',
                            args=(self.account.id, self.analysis_config.id,
                                  self.question_set.id)))

    def test_serializer(self):
        self._login_and_add_admin_group(
            self.user, permissions=AdminPermissions.CONFIG_FEATURES)
        result = self.client.get(self.url)
        self.verify_serializer(
            result, ("id", "title", "description", "allow_multiple"))


class QuestionSetDataViewSetTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        super(QuestionSetDataViewSetTests, self).setUp(login_now=False,
                                                       **kwargs)
        self.account = self.user.current_seat.account
        self.analysis_config = AnalysisConfigFactory(account=self.account)
        self.feature_config = FeatureConfigFactory(account=self.account)
        self.question_set = self.feature_config.question_sets.first()

        self.analysis_config.feature_configs.add(self.feature_config)
        self.url = reverse('question_set_data_api-list',
                           args=(self.account.id, self.analysis_config.id,
                                 self.question_set.id))
        self.url2 = reverse('question_set_data_api-detail',
                            args=(self.account.id, self.analysis_config.id,
                                  self.question_set.id, "placeholder"))
        self.test_data = {
            "entities": [
                {
                    "record_set_config": 7,
                    "eid": "C00503185"
                },
                {
                    "record_set_config": 6,
                    "eid": "C00503185"
                }
            ],
            "is_ally": True,
            "label": "Foo"
        }
        # Add a couple contacts so the contact count is not zero
        ContactFactory.create(user=self.user)
        ContactFactory.create(user=self.account.account_user)
        # Create a couple seats to verify delayed analysis
        for i in range(2):
            SeatFactory.create(account=self.account)

    def test_permissions(self):
        # # Verify any unauthorized user is rejected
        result = self.client.get(self.url)
        self.assertNoCredentials(result)
        result = self.client.get(self.url2)
        self.assertNoCredentials(result)

        # Login the user
        self._login(self.user)
        # Verify user is not an admin, or that would invalidate the test.
        self.assertFalse(self.user.is_admin)
        self.assertFalse(self.user.is_account_admin())

        ## Verify a standard user is rejected from accessing feature configs
        result = self.client.get(self.url)
        self.assertNoPermission(result)
        ## Verify a standard user is rejected from configuring a feature config
        result = self.client.post(self.url, data={'test': '123'})
        self.assertNoPermission(result)

        # Add the correct permissions to the seat
        self._add_admin_group(self.user,
                              permissions=AdminPermissions.CONFIG_FEATURES)

        ## Verify the list API accepts the user
        result = self.client.get(self.url)
        self.assertContains(result, "")

    def _verify_analysis_submission(self):
        # Verify a task for the user was created
        tasks = RevUpTask.objects.filter_by_generic_obj(None).filter(
            user=self.user, task_type=RevUpTask.TaskType.NETWORK_SEARCH)
        self.assertEqual(tasks.count(), 1)

        # Verify a task for the account was created
        tasks = RevUpTask.objects.filter_by_generic_obj(None).filter(
            user=self.account.account_user,
            task_type=RevUpTask.TaskType.NETWORK_SEARCH)
        self.assertEqual(tasks.count(), 1)

        # Verify the other seats were given delayed analyses
        seat_qs = Seat.objects.filter(account=self.account)
        seat_count = seat_qs.count()
        self.assertEqual(seat_qs.exclude(delayed_analysis=None).count(),
                         # minus 1 because the user isn't included
                         seat_count - 1)
        # Verify the user isn't given a delayed analysis
        self.assertTrue(seat_qs.filter(user=self.user, delayed_analysis=None))

    def test_create(self):
        self._login_and_add_admin_group(
            self.user, permissions=AdminPermissions.CONFIG_FEATURES)

        ## Ensure question_set is configured to allow multiple answers
        self.question_set.allow_multiple = True
        self.question_set.save()

        ## Verify normal save
        response = self.client.post(self.url, data=json.dumps(self.test_data),
                                    content_type="application/json")
        self.assertContains(response, "id")
        self.assertContains(response, '"record_set_config":7')
        self.assertContains(response, '"record_set_config":6')

        ## Verify duplicate save fails
        response = self.client.post(self.url, data=json.dumps(self.test_data),
                                    content_type="application/json")
        self.assertContains(
            response, "Either this is a duplicate, or it has not changed",
            status_code=500)

        ## Verify second save on non-multiple question-set fails
        self.question_set.allow_multiple = False
        self.question_set.save()
        response = self.client.post(self.url, data=json.dumps(self.test_data),
                                    content_type="application/json")
        self.assertContains(
            response, "does not support multiple entries", status_code=500)

        self._verify_analysis_submission()

    def test_update(self):
        self._login_and_add_admin_group(
            self.user, permissions=AdminPermissions.CONFIG_FEATURES)
        qsd = QuestionSetData.create(self.question_set, self.analysis_config,
                                     self.test_data)
        self.test_data["label"] = "Bar"
        url2 = self.url2.replace("placeholder", str(qsd.id))

        # Verify update where last_execution and version match
        question_set_count = QuestionSetData.objects.count()
        response = self.client.put(url2, data=json.dumps(self.test_data),
                                   content_type="application/json")
        self.assertContains(response, '"label":"Bar"')
        # There should not be another QuestionSetData object
        self.assertEqual(QuestionSetData.objects.count(), question_set_count)

        # Update the last_execution, and verify new data instance is created
        self.test_data["label"] = "Baz"
        self.analysis_config.last_execution = datetime.now() + \
                                              timedelta(seconds=5)
        self.analysis_config.save()
        question_set_count = QuestionSetData.objects.count()
        response = self.client.put(url2, data=json.dumps(self.test_data),
                                   content_type="application/json")
        self.assertContains(response, '"label":"Baz"')
        # There should not be another QuestionSetData object
        self.assertEqual(QuestionSetData.objects.count(),
                         question_set_count + 1)
        qsd = QuestionSetData.objects.get(id=qsd.id)
        self.assertFalse(qsd.active)

        self._verify_analysis_submission()

    def test_delete(self):
        # Setup test
        self._login_and_add_admin_group(
            self.user, permissions=AdminPermissions.CONFIG_FEATURES)
        qsd = QuestionSetData.create(self.question_set, self.analysis_config,
                                     self.test_data)
        self.assertTrue(qsd.active)
        url2 = self.url2.replace("placeholder", str(qsd.id))
        response = self.client.delete(url2)
        qsd = QuestionSetData.objects.get(id=qsd.id)
        self.assertFalse(qsd.active)

        self._verify_analysis_submission()


class QuestionSetDataViewSetTests2(AuthorizedUserTestCase):
    """Verifies QuestionSetDataViewSet for NFP Key Contributions QS"""
    def setUp(self, **kwargs):
        self.account_profile = AccountProfileFactory(
            account_type=AccountProfile.AccountType.NONPROFIT)
        self.account = AccountFactory(account_profile=self.account_profile)
        super(QuestionSetDataViewSetTests2, self).setUp(login_now=False,
                                                        campaigns=self.account,
                                                        **kwargs)
        self.analysis_config = AnalysisConfigFactory(account=self.account)
        dc = DynamicClassModelMixin()
        dc.dynamic_class = NFPKeyContributions
        self.question_sets = QuestionSetFactory(title="NFP Key Contributions",
                                                class_name=dc.class_name,
                                                module_name=dc.module_name)
        self.feature_config = FeatureConfigFactory(
            account=self.account, question_sets=self.question_sets)
        self.question_set = self.feature_config.question_sets.first()
        self.analysis_config.feature_configs.add(self.feature_config)
        self.url = reverse('question_set_data_api-list',
                           args=(self.account.id, self.analysis_config.id,
                                 self.question_set.id))
        self.url2 = reverse('question_set_data_api-detail',
                            args=(self.account.id, self.analysis_config.id,
                                  self.question_set.id, "placeholder"))

        self.test_data = {
            "ntee_level_1": ["A"],
            "ntee_level_2": ["65"],
            "is_ally": True
        }
        # Add a couple contacts so the contact count is not zero
        ContactFactory.create(user=self.user)
        ContactFactory.create(user=self.account.account_user)
        # Create a couple seats to verify delayed analysis
        for i in range(2):
            SeatFactory.create(account=self.account)

    def test_create(self):
        """Test creation of QSD for NFP Key Contributions QS. Ensures that
        duplicate data does not get saved.
        """
        self._login_and_add_admin_group(
            self.user, permissions=AdminPermissions.CONFIG_FEATURES)

        # Ensure question_set is configured to allow multiple answers
        self.question_set.allow_multiple = True
        self.question_set.save()

        # Verify normal save
        response = self.client.post(self.url, data=json.dumps(self.test_data),
                                    content_type="application/json")
        self.assertContains(response, "id")
        self.assertContains(response, '"ntee_level_1":"A"')
        self.assertContains(response, '"ntee_level_2":"65"')

        # Verify duplicate save fails
        response = self.client.post(self.url, data=json.dumps(self.test_data),
                                    content_type="application/json")
        self.assertContains(
            response, "Either this is a duplicate, or it has not changed",
            status_code=500)
