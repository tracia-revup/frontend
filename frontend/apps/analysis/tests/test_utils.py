import csv
from functools import partial
import unittest.mock as mock

from django.db.models import QuerySet
from django.test import override_settings


from frontend.apps.analysis.analyses.signal_actions.filter import (
    ConfigurableEntityFilter, ConfigurableAllyTagger)
from frontend.apps.analysis.csv_generator import EchoIO, CSVGenerator
from frontend.apps.analysis.factories import (
     RecordResourceFactory, RecordSetFactory, AnalysisConfigFactory,
     FeatureConfigFactory, AnalysisFactory, ResultFactory, QuestionSetFactory,
     QuestionSetDataFactory, RecordSetConfigFactory, ResourceConfigFactory)
from frontend.apps.analysis.questionset_controllers import NFPKeyContributions
from frontend.apps.analysis.sources.base import RecordSet
from frontend.apps.analysis.sources.non_profit.backends.mongodbv3 import (
    Contributions, Entities)
from frontend.apps.analysis.utils import sort_with_weighted_sum
from frontend.apps.authorize.test_utils import AuthorizedUserTestCase
from frontend.apps.campaign.factories import (AccountFactory,
                                              AccountProfileFactory)
from frontend.apps.campaign.models import AccountProfile
from frontend.apps.contact.factories import ContactFactory
from frontend.apps.contact.models import (
    EmailAddress, Address, PhoneNumber, Organization, AlmaMater)
from frontend.apps.contact_set.factories import ContactSetFactory
from frontend.libs.utils.model_utils import DynamicClassModelMixin
from frontend.libs.utils.test_utils import TestCase


class CSVGeneratorTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        super(CSVGeneratorTests, self).setUp(login_now=False, **kwargs)
        self.account = self.user.current_seat.account
        self.analysis_config = AnalysisConfigFactory(account=self.account)
        self.partition = \
            self.analysis_config.partition_set_config.partition_configs.first()

        CSVGenerator.site_url = "fakeURL://testing/t"
        CSVGenerator._generate_detail_field_url = mock.MagicMock(return_value="")
        self.contact1 = ContactFactory(first_name="Joe", last_name="Horn")
        self.contact2 = ContactFactory(first_name="Bob", last_name="Smith")
        self.analysis1 = AnalysisFactory()
        self.analysis2 = AnalysisFactory()
        self.contact_set = ContactSetFactory(analysis=self.analysis1)
        self.key_signals = {"giving": "99"}
        self.results1 = ResultFactory(analysis=self.analysis1, score=72,
                                      key_signals=self.key_signals,
                                      contact=self.contact1)
        self.results2 = ResultFactory(analysis=self.analysis2, score=9,
                                      key_signals=self.key_signals,
                                      contact=self.contact2)

        self.contact1.format_for_csv = mock.Mock(
            return_value={"Email Address": "joe@horn.com",
                          "Address Street": "55 Bogus"}
        )
        self.contact2.format_for_csv = mock.Mock(
            return_value={"Email Address": "bob@thecure.com",
                          "Address Street": "22 Test"}
        )

    def test_csvfile_nameonly(self):
        expected = 'Last Name,First Name,Details\r\nHorn,Joe,' \
                   '"=HYPERLINK(""fakeURL://testing/t"",""RevUp Details"")"' \
                   '\r\nSmith,Bob,"=HYPERLINK(""fakeURL://testing/t"",' \
                   '""RevUp Details"")"\r\n'

        # Feed generate_csv two fixed and fake contacts
        queryset = mock.Mock(spec=QuerySet)
        queryset.iterator.return_value = [self.results1, self.results2]
        queryset.select_related.return_value = queryset
        csv_gen = CSVGenerator(self.contact_set, self.partition,
                               show_score=False,
                               show_name=True,
                               show_contact_info=False,
                               show_total=False,
                               pretty_details=True)
        result = csv_gen.generate_csv(queryset)

        # Aggregate results and compare
        out = ''.join(result)
        self.assertEqual(expected, out)

    def test_csvfile_namescore(self):
        expected = ('RevUp Score,Last Name,First Name,Details\r\n' \
                   '72.00,Horn,Joe,fakeURL://testing/t\r\n9.00,' \
                   'Smith,Bob,fakeURL://testing/t\r\n')

        # Feed generate_csv two fixed and fake contacts
        queryset = mock.Mock(spec=QuerySet)
        queryset.iterator.return_value = [self.results1, self.results2]
        queryset.select_related.return_value = queryset
        csv_gen = CSVGenerator(self.contact_set, self.partition,
                               show_score=True,
                               show_name=True,
                               show_contact_info=False,
                               show_total=False,
                               pretty_details=False)
        result = csv_gen.generate_csv(queryset)

        # Aggregate results and compare
        out = ''.join(result)
        self.assertEqual(expected, out)

    def test_csvfile_namecontact(self):
        expected = ('Last Name,First Name,Email Address,Email type,'
                    'Email2 Address,Email2 type,Email3 Address,Email3 type,'
                    'Address Street,Address PO Box,Address City,Address State,'
                    'Address Postal Code,Address Country,Address type,'
                    'Address2 Street,Address2 PO Box,Address2 City,'
                    'Address2 State,Address2 Postal Code,Address2 Country,'
                    'Address2 type,Address3 Street,Address3 PO Box,'
                    'Address3 City,Address3 State,Address3 Postal Code,'
                    'Address3 Country,Address3 type,Phone Number,Phone type,'
                    'Phone2 Number,Phone2 type,Phone3 Number,Phone3 type,'
                    'Occupation,Employer,Department,Alma Mater Name,'
                    'Alma Mater Major,Alma Mater Degree,Alma Mater2 Name,'
                    'Alma Mater2 Major,Alma Mater2 Degree,Alma Mater3 Name,'
                    'Alma Mater3 Major,Alma Mater3 Degree,External Id Source,'
                    'External Id Value,External Id2 Source,External Id2 Value,'
                    'External Id3 Source,External Id3 Value,'
                    'External Id4 Source,External Id4 Value,'
                    'External Id5 Source,External Id5 Value,Details\r\n'
                    'Horn,Joe,joe@horn.com,,,,,,55 Bogus,,,,,,,,,,,,,,,,,,,,,,'
                    ',,,,,,,,,,,,,,,,,,,,,,,,,,,fakeURL://testing/t\r\n'
                    'Smith,Bob,bob@thecure.com,,,,,,22 Test,,,,,,,,,,,,,,,,,,,'
                    ',,,,,,,,,,,,,,,,,,,,,,,,,,,,,,fakeURL://testing/t\r\n')

        # Feed generate_csv two fixed and fake contacts
        queryset = mock.Mock(spec=QuerySet)
        queryset.iterator.return_value = [self.results1, self.results2]
        queryset.select_related.return_value = queryset
        csv_gen = CSVGenerator(self.contact_set, self.partition,
                               show_score=False,
                               show_name=True,
                               show_contact_info=True,
                               show_total=False,
                               pretty_details=False)
        result = csv_gen.generate_csv(queryset)

        # Aggregate results and compare
        out = ''.join(result)
        self.assertEqual(expected, out)

    def test_csvfile_allfields(self):
        expected = ('RevUp Score,Last Name,First Name,Email Address,'
                    'Email type,Email2 Address,Email2 type,Email3 Address,'
                    'Email3 type,Address Street,Address PO Box,Address City,'
                    'Address State,Address Postal Code,Address Country,'
                    'Address type,Address2 Street,Address2 PO Box,'
                    'Address2 City,Address2 State,Address2 Postal Code,'
                    'Address2 Country,Address2 type,Address3 Street,'
                    'Address3 PO Box,Address3 City,Address3 State,'
                    'Address3 Postal Code,Address3 Country,Address3 type,'
                    'Phone Number,Phone type,Phone2 Number,Phone2 type,'
                    'Phone3 Number,Phone3 type,Occupation,Employer,Department,'
                    'Alma Mater Name,Alma Mater Major,Alma Mater Degree,'
                    'Alma Mater2 Name,Alma Mater2 Major,Alma Mater2 Degree,'
                    'Alma Mater3 Name,Alma Mater3 Major,Alma Mater3 Degree,'
                    'External Id Source,External Id Value,External Id2 Source,'
                    'External Id2 Value,External Id3 Source,'
                    'External Id3 Value,External Id4 Source,'
                    'External Id4 Value,External Id5 Source,'
                    'External Id5 Value,Giving,Details\r\n'
                    '72.00,Horn,Joe,joe@horn.com,,,,,,55 Bogus,,,,,,,,,,,,,,,,'
                    ',,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,99,fakeURL://testing/t'
                    '\r\n'
                    '9.00,Smith,Bob,bob@thecure.com,,,,,,22 Test,,,,,,,,,,,,,,'
                    ',,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,99,fakeURL://testing/t'
                    '\r\n')

        # Feed generate_csv two fixed and fake contacts
        queryset = mock.Mock(spec=QuerySet)
        queryset.iterator.return_value = [self.results1, self.results2]
        queryset.select_related.return_value = queryset
        csv_gen = CSVGenerator(self.contact_set, self.partition,
                               show_score=True,
                               show_name=True,
                               show_contact_info=True,
                               show_total=True,
                               pretty_details=False)
        result = csv_gen.generate_csv(queryset)

        # Aggregate results and compare
        out = ''.join(result)
        self.assertEqual(expected, out)

    def test_csvfunc_score(self):
        expected = ('72.00\r\n')

        container = mock.Mock()
        container.analysis_result = self.results1
        # Skip header and check single generated segment against fixed string
        fields = ['RevUp Score']
        csv_gen = CSVGenerator(self.contact_set, self.partition,)
        fns = [csv_gen._format_score_field]
        row_formatter = csv_gen._compose(*fns)
        csvwriter = csv.DictWriter(EchoIO(), fields)
        out = csvwriter.writerow(row_formatter(container, {}))
        self.assertEqual(expected, out)

        # Verify csvfunc will still work without a result
        container = mock.Mock()
        container.analysis_result = None
        out = csvwriter.writerow(row_formatter(container, {}))
        assert out in ['""\r\n', '\r\n']

    def test_csvfunc_contact_info(self):
        expected = ('joe@horn.com,,,,,,55 Bogus,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,'
                    ',,,,,,,,,,,,,,,,,\r\n')

        container = mock.Mock()
        container.contact = self.results1.contact
        # Skip header and check single generated segment against fixed string
        fields = CSVGenerator.contact_info_fields
        csv_gen = CSVGenerator(self.contact_set, self.partition)
        fns = [csv_gen._format_contact_info_fields]
        row_formatter = csv_gen._compose(*fns)
        csvwriter = csv.DictWriter(EchoIO(), fields)
        out = csvwriter.writerow(row_formatter(container, {}))
        self.assertEqual(expected, out)

    def test_csvfunc_detail(self):
        expected = ('"=HYPERLINK(""fakeURL://testing/t""' \
                   ',""RevUp Details"")"\r\n')

        container = mock.Mock()
        container.contact = self.results1.contact
        # Skip header and check single generated segment against fixed string
        fields = ['Details']
        csv_gen = CSVGenerator(self.contact_set, self.partition)
        fns = [csv_gen._format_detail_field]
        row_formatter = csv_gen._compose(*fns)
        csvwriter = csv.DictWriter(EchoIO(), fields)
        out = csvwriter.writerow(row_formatter(container, {}))
        self.assertEqual(expected, out)

    def test_csvfunc_name(self):
        expected = ('Horn,Joe\r\n')

        container = mock.Mock()
        container.contact = self.results1.contact
        # Skip header and check single generated segment against fixed string
        fields = ['Last Name', 'First Name']
        csv_gen = CSVGenerator(self.contact_set, self.partition)
        fns = [csv_gen._format_name_fields]
        row_formatter = csv_gen._compose(*fns)
        csvwriter = csv.DictWriter(EchoIO(), fields)
        out = csvwriter.writerow(row_formatter(container, {}))
        self.assertEqual(expected, out)

    def test_csvfunc_total(self):
        expected = ('99\r\n')

        container = mock.Mock()
        container.analysis_result = self.results1
        # Skip header and check single generated segment against fixed string
        total_field_name = "Giving"
        fields = [total_field_name]
        csv_gen = CSVGenerator(self.contact_set, self.partition)
        fns = [partial(csv_gen._format_total_field,
                       total_field_name=total_field_name)]
        row_formatter = csv_gen._compose(*fns)
        csvwriter = csv.DictWriter(EchoIO(), fields)
        out = csvwriter.writerow(row_formatter(container, {}))
        self.assertEqual(expected, out)

        # Verify csvfunc will still work without a result
        container = mock.Mock()
        container.analysis_result = None
        out = csvwriter.writerow(row_formatter(container, {}))
        assert out in ['""\r\n', '\r\n']


class AnalysisUtilTests(TestCase):
    def setUp(self):
        super(AnalysisUtilTests, self).setUp()
        self.mock_mongo_data_1 = [
            (1.416, 'Wilson Harry'),
            (1.375, 'Harris Bill'),
            (1.375, 'Sidhu Harry'),
            (1.333, 'Harris, Kamala D'),
            (1.333, 'Bertram, Harry V'),
            (1.25, 'KAMALA HARRIS FOR DISTRICT ATTORNEY'),
            (1.196, 'Smail Jr, Harry F.'),
            (1.166, 'RE-ELECT DISTRICT ATTORNEY KAMALA HARRIS'),
            (0.75, 'Teague, Harry'),
            (0.75, 'Reid, Harry'),
            (0.75, 'FRIENDS FOR HARRIS'),
            (0.75, 'FOX HARRY'),
            (0.75, 'LEWIS, HARRIS'),
            (0.75, 'Harris, Eric'),
        ]
        self.mock_mongo_data_2 = [
            (3.183, 'Michael Brown 2012'),
            (1.833, 'Brown, Tamara Davis'),
            (1.5, 'Brown, Grady'),
            (1.416, 'Brown, Tim'),
            (1.375, 'Sonnenberg, Jerry'),
            (1.333, 'Brown, Gerald (jerry)'),
            (1.333, 'Brown, Jerry R'),
            (1.333, 'Brown, Jerry L'),
            (1.25, 'brown, Edmund G (jerry)'),
            (1.2, 'brown Jr, Edmund G (jerry)'),
            (0.75, 'Brown, Corrine'),
            (0.75, 'Mcnerny, Jerry'),
            (0.75, 'Browning, Nickey'),
            (0.75, 'Brown, Kay'),
        ]
        self.mock_mongo_data_3 = [
            (1.809, 'Lee, Andrew'),
            (1.630, 'Lee, Michael Vincent'),
            (1.416, 'Neilson, Ed'),
            (1.291, 'Lee, Arthur Shores'),
            (1.266, 'Narain, Edwin (ed)'),
            (1.25, 'ED LEE FOR MAYOR 2015'),
            (1.25, 'ED LEE FOR MAYOR 2011'),
            (1.196, 'Rendell, Edward G (ed)'),
            (1.166, 'MAYOR ED LEE FOR SAN FRANCISCO COMMITTEE'),
            (1.142, 'SF NEIGHBORHOOD ALLIANCE FOR ED LEE MAYOR 2011'),
            (1.142, 'SF NEIGHBOR ALLIANCE FOR ED LEE MAYOR 2011'),
            (1.142, 'SUPPORT DRAFTING ED LEE FOR SF MAYOR 2011'),
            (1.125, 'Givan, Juandalynn (lee Lee)'),
            (1.1, ('SAN FRANCISCANS FOR JOBS AND GOOD GOVERNMENT, SUPPORTING '
                   'ED LEE FOR MAYOR 2011')),
            (1.1, ('COMMITTEE FOR EFFECTIVE CITY MANAGEMENT--A COMMITTEE IN '
                   'SUPPORT OF ED LEE FOR MAYOR 2011')),
            (1.05, ('CITY RESIDENTS OPPOSING ED LEE FOR MAYOR 2011, SPONSORED '
                    'BY AMERICAN FEDERATION OF STATE COUNTY AND MUNICIPAL '
                    'EMPLOYEES AFL-CIO AND AFSCME CA PEOPLE')),
            (1.1, 'Friends Of Ed Vaughn - Ed Vaughn Campaign'),
        ]
        self.mock_mongo_data_4 = [
            (1.416, 'Thomas, Ian'),
            (1.291, 'Calderon, Charles M'),
            (1.25, 'Ian Calderon Campaign Cmte'),
            (1.25, 'Ian Calderon For Assembly 2016'),
            (1.25, 'Ian Calderon For Assembly 2018'),
            (0.75, 'Mcfarland, Ian'),
            (0.666, 'FRIENDS OF NEIL CALDERON'),
            (0.666, 'Randolph, Ian L'),
            (0.666, 'Bender, Michael Ian'),
            (0.625, 'Calderon, Ronald S (ron)'),
            (0.625, 'COMMITTEE TO ELECT IAN BRENSON'),
            (0.6, 'Ron Calderon For State Controller 2014'),
            (0.583, 'THE COMMITTEE TO ELECT JUDGE MICHAEL IAN BENDER'),
            (0.583, 'DARMINIO CALDERONE & PUNZO DEMOCRATS FOR SADDLE BROOK')
        ]

    def _prepare_data(self, _tuple_data):
        """Converts a given list of tuple objects to a list of dictionary
        objects
        """
        new_data = []
        for item in _tuple_data:
            new_data.append({'name': item[1], 'text_score': item[0]})
        return new_data

    def test_sort_with_weighted_sum(self):
        """Test the sort_with_weighted_sum method"""
        query_str = 'Harris Kamala'
        mock_data = self._prepare_data(self.mock_mongo_data_1)
        new_results = sort_with_weighted_sum(mock_data, query_str)
        # Verify that the first result is one of the best matches
        expected_results = ['Harris, Kamala D',
                            'KAMALA HARRIS FOR DISTRICT ATTORNEY',
                            'RE-ELECT DISTRICT ATTORNEY KAMALA HARRIS']
        # Verify that the first result is one of the best matches
        self.assertTrue(new_results[0]['name'] in expected_results)

        query_str = 'Jerry Brown'
        mock_data = self._prepare_data(self.mock_mongo_data_2)
        new_results = sort_with_weighted_sum(mock_data, query_str)
        expected_results = ['Brown, Gerald (jerry)',
                            'Brown, Jerry R',
                            'Brown, Jerry L',
                            'brown, Edmund G (jerry)',
                            'brown Jr, Edmund G (jerry)']
        # Verify that the first result is one of the best matches
        self.assertTrue(new_results[0]['name'] in expected_results)

        query_str = 'Ed Lee'
        mock_data = self._prepare_data(self.mock_mongo_data_3)
        new_results = sort_with_weighted_sum(mock_data, query_str)
        expected_results = ['ED LEE FOR MAYOR 2015', 'ED LEE FOR MAYOR 2011',
                            'MAYOR ED LEE FOR SAN FRANCISCO COMMITTEE',
                            'SF NEIGHBORHOOD ALLIANCE FOR ED LEE MAYOR 2011',
                            'SF NEIGHBOR ALLIANCE FOR ED LEE MAYOR 2011',
                            'SUPPORT DRAFTING ED LEE FOR SF MAYOR 2011',
                            ('SAN FRANCISCANS FOR JOBS AND GOOD GOVERNMENT, '
                             'SUPPORTING ED LEE FOR MAYOR 2011'),
                            ('COMMITTEE FOR EFFECTIVE CITY MANAGEMENT--A '
                             'COMMITTEE IN SUPPORT OF ED LEE FOR MAYOR 2011'),
                            ('CITY RESIDENTS OPPOSING ED LEE FOR MAYOR 2011, '
                             'SPONSORED BY AMERICAN FEDERATION OF STATE '
                             'COUNTY AND MUNICIPAL EMPLOYEES AFL-CIO AND '
                             'AFSCME CA PEOPLE')]
        # Verify that the first result is one of the best matches
        self.assertTrue(new_results[0]['name'] in expected_results)

        query_str = 'Ian Calderon'
        mock_data = self._prepare_data(self.mock_mongo_data_4)
        new_results = sort_with_weighted_sum(mock_data, query_str)
        expected_results = ['Ian Calderon Campaign Cmte',
                            'Ian Calderon For Assembly 2016',
                            'Ian Calderon For Assembly 2018']
        # Verify that the first result is one of the best matches
        self.assertTrue(new_results[0]['name'] in expected_results)


@override_settings(DEBUG=True)
class AllySacUtilsTests(TestCase):
    def setUp(self):
        """Tests ConfigurableEntityFilter SAC"""
        account_profile = AccountProfileFactory(
            account_type=AccountProfile.AccountType.NONPROFIT)
        account = AccountFactory(account_profile=account_profile)
        dc = DynamicClassModelMixin()
        dc.dynamic_class = NFPKeyContributions
        question_sets = QuestionSetFactory(title="NFP Key Contributions",
                                           class_name=dc.class_name,
                                           module_name=dc.module_name,
                                           allow_multiple=True)
        feature_config = FeatureConfigFactory(
            account=account, question_sets=question_sets)
        analysis_config = AnalysisConfigFactory(
            account=account, feature_configs=feature_config)
        qsd_data = {"label": "TEACH FOR AMERICA NTEE",
                    "is_ally": True,
                    "entities": [
                        {
                          "record_set_config": "32",
                          "eid": "581953679"
                        },
                        {
                          "record_set_config": "32",
                          "eid": "610568789"
                        },
                        {
                          "record_set_config": "32",
                          "eid": "135562976"
                        }],
                    "categories": [
                        {"ntee_level_1": "T",
                         "ntee_level_2": "40"},
                        {"ntee_level_1": "B",
                         "ntee_level_2": "63"}]
                    }
        question_set = feature_config.question_sets.first()
        QuestionSetDataFactory(
            question_set=question_set,
            analysis_config=analysis_config,
            data=qsd_data)

        def mock_rsb_get(*args, **kwargs):
            """Mocks the get_match_eids of record resource associated with a
            record set backend
            """
            rsb_selected = mock.MagicMock()
            rsb_selected.record_resource.get_match_eids = lambda x: {x}
            return rsb_selected
        record_set_backends = mock.MagicMock()
        record_set_backends.__getitem__ = mock_rsb_get
        signal_action_config = mock.MagicMock()

        self.test_record_1 = {'EIN': '135562976',
                              'ntee_level_1': 'A',
                              'ntee_level_2': '39'}
        self.test_record_2 = {'EIN': '135527076',
                              'ntee_level_1': 'T',
                              'ntee_level_2': '40'}
        self.test_record_3 = {'EIN': '135103076',
                              'ntee_level_1': 'C',
                              'ntee_level_2': '27'}

        record_resource = RecordResourceFactory(
            dynamic_class=Contributions)
        resource_config = ResourceConfigFactory(
            record_resource=record_resource)
        record_set = RecordSetFactory()
        self.contributions_record_resource = Contributions(
            resource_config=resource_config,
            record_set=record_set)
        self.configurable_entity_filter_args = [
            record_set_backends, feature_config, analysis_config,
            signal_action_config]

        record_set_config = RecordSetConfigFactory()
        record_set = RecordSet(record_set_config, analysis_config)
        self.entities_record_resource = Entities(
            resource_config=resource_config,
            record_set=record_set)

        def mock_rsb_get_2(*args, **kwargs):
            """Mocks the get_match_eids of record resource associated with a
            record set backend
            """
            rsb_selected = mock.MagicMock()
            rsb_selected.record_resource = self.contributions_record_resource
            # rsb_selected.record_resource = self.entities_record_resource
            rsb_selected.record_resource.get_match_eids = lambda x: {x}
            return rsb_selected
        record_set_backends_2 = mock.MagicMock()
        record_set_backends_2.__getitem__ = mock_rsb_get_2

        record_set_config_id = mock.MagicMock()
        self.configurable_ally_tagger_args = [
            record_set_config_id, record_set_backends_2, feature_config,
            analysis_config, signal_action_config]

    def test_check_if_ally(self):
        """Tests the return of `check_if_ally` method"""
        cat_sac = ConfigurableAllyTagger(*self.configurable_ally_tagger_args)
        # Test a record that has matching value in the defined match_field.
        # Verify that the `has_matched_fields` method returns True
        record_matched = cat_sac.check_if_ally(self.test_record_1)
        assert record_matched is True

        # Test a record that has matching value in the defined qsd_match_field.
        # Verify that the `has_matched_fields` method returns True
        record_matched = cat_sac.check_if_ally(self.test_record_2)
        assert record_matched is True

        # Test a record that has matching value in neither of the fields.
        # Verify that the `has_matched_fields` method returns False.
        record_matched = cat_sac.check_if_ally(self.test_record_3)
        assert record_matched is False

    def test_has_matched_fields(self):
        """Tests the return of `has_matched_fields` method"""
        cef_sac = ConfigurableEntityFilter(
            *self.configurable_entity_filter_args)
        _, _, gids = cef_sac.values[0]
        # Test a record that has matching value in the defined match_field.
        # Verify that the `has_matched_fields` method returns True
        record_matched = cef_sac.has_matched_fields(
            self.test_record_1, gids, self.contributions_record_resource)
        assert record_matched is True

        # Test a record that has matching value in the defined qsd_match_field.
        # Verify that the `has_matched_fields` method returns True
        record_matched = cef_sac.has_matched_fields(
            self.test_record_2, gids, self.contributions_record_resource)
        assert record_matched is True

        # Test a record that has matching value in neither of the fields.
        # Verify that the `has_matched_fields` method returns False.
        record_matched = cef_sac.has_matched_fields(
            self.test_record_3, gids, self.contributions_record_resource)
        assert record_matched is False
