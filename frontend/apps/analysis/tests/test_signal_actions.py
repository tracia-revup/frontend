from unittest import mock

from frontend.apps.analysis.analyses.signal_actions.evaluate import (
    CalculateOverallScore, FetchSubscores, PopulatePersonaKeySignals)
from frontend.apps.analysis.analyses.signal_actions.multiplier import FetchWeights
from frontend.apps.analysis.analyses.base import Pipeline
from frontend.apps.analysis.factories import (
    FeatureFactory, FeatureConfigFactory, QuestionSetFactory,
    QuestionSetDataFactory)
from frontend.apps.analysis.questionset_controllers import (
    MajorGiftsWeightsPersonaController)
from frontend.apps.campaign.factories import (
    AccountFactory, AccountProfileFactory, AnalysisConfigFactory)
from frontend.apps.campaign.models.base import AccountProfile
from frontend.libs.utils.model_utils import DynamicClassModelMixin
from frontend.libs.utils.test_utils import TestCase


class FetchWeightsSACTests(TestCase):
    def setUp(self):
        self.account_profile = AccountProfileFactory(
            account_type=AccountProfile.AccountType.NONPROFIT)
        self.account = AccountFactory(account_profile=self.account_profile)
        dc = DynamicClassModelMixin()
        dc.dynamic_class = MajorGiftsWeightsPersonaController
        question_sets = QuestionSetFactory(
            title="Major Gifts Persona Weights",
            class_name=dc.class_name,
            module_name=dc.module_name)
        self.feature_config = FeatureConfigFactory(
            account=self.account, question_sets=question_sets)
        self.question_set = self.feature_config.question_sets.first()
        self.analysis_config = AnalysisConfigFactory(
            account=self.account, feature_configs=self.feature_config)
        self.signal_action_config = mock.MagicMock()
        self.signal_action_config.definition = {}

    def test_fetch_weights_1(self):
        """Test `fetch_weights` method in `FetchWeights` SAC when there is a
        QSD associated with the account.
        """
        qsd_data = {"dg_weight": 0.01,
                    "ai_weight": 0.02,
                    "pi_weight": 0.03,
                    "wi_weight": 0.04}
        expected = {"AI": 0.02,
                    "DG": 0.01,
                    "PI": 0.03,
                    "WI": 0.04}
        QuestionSetDataFactory(
            question_set=self.question_set,
            analysis_config=self.analysis_config,
            data=qsd_data)
        sac = FetchWeights(
            self.feature_config,
            self.analysis_config,
            self.signal_action_config)
        assert expected == sac.fetch_weights()

    def test_fetch_weights_2(self):
        """Test `fetch_weights` method in `FetchWeights` SAC when a particular
        weight is missing in the associated QSD.
        """
        qsd_data = {"dg_weight": 0.01,
                    "pi_weight": 0.03}
        QuestionSetDataFactory(
            question_set=self.question_set,
            analysis_config=self.analysis_config,
            data=qsd_data)
        sac = FetchWeights(
            self.feature_config,
            self.analysis_config,
            self.signal_action_config)
        # verify that the default weights are used when a particular weight is
        # missing
        expected = {'AI': sac.DEFAULT_WEIGHTS.get('ai_weight'),
                    'DG': 0.01,
                    'PI': 0.03,
                    'WI': sac.DEFAULT_WEIGHTS.get('wi_weight')}
        assert expected == sac.fetch_weights()

    def test_fetch_weights_3(self):
        """Test `fetch_weights` method when there are no associated QSDs."""
        sac = FetchWeights(
            self.feature_config,
            self.analysis_config,
            self.signal_action_config)
        expected = {ind: sac.DEFAULT_WEIGHTS.get(f'{ind.lower()}_weight')
                    for ind in sac.indicators}
        assert expected == sac.fetch_weights()


class FetchSubScoreSACTests(TestCase):
    def setUp(self):
        self.account_profile = AccountProfileFactory(
            account_type=AccountProfile.AccountType.NONPROFIT)
        self.account = AccountFactory(account_profile=self.account_profile)
        dc = DynamicClassModelMixin()
        dc.dynamic_class = MajorGiftsWeightsPersonaController
        self.feature = FeatureFactory(title='Major Gifts Persona')
        self.feature_config = FeatureConfigFactory(
            account=self.account,
            feature=self.feature
        )
        self.analysis_config = AnalysisConfigFactory(
            account=self.account, feature_configs=self.feature_config)
        self.signal_action_config = mock.MagicMock()
        self.signal_action_config.definition = {}
        signal = {
            'fused_client_summary': [{'_id': 'bgbfg67g',
                                      'personas': {
                                          'major_gifts': {'DG': 0.1,
                                                          'PI': 0.3,
                                                          'WI': 0.4},
                                          'likelihood_to_give': {'DG': 1.1,
                                                                 'PI': 1.3,
                                                                 'WI': 1.4}}},
                                     {'_id': '63716tyt',
                                      'personas': {
                                          'major_gifts': {'DG': 10,
                                                          'PI': 3,
                                                          'WI': 4},
                                          'likelihood_to_give': {'DG': 2.1,
                                                                 'PI': 2.3,
                                                                 'WI': 2.4}}}],
            'fused_entity_ids': ['bgbfg67g', '63716tyt'],
            'weights': {'AI': 0.02,
                        'WI': 0.04,
                        'PI': 0.03,
                        'DG': 0.01}
        }
        record = signal
        records_signals = [(record, signal)]
        self.pipeline = Pipeline(
            target=mock.MagicMock(),
            records_signals=records_signals)

    def test_fetch_sub_scores_sac(self):
        """Verify that the `FetchSubscores` SAC fetches only the relevant
        persona scores.
        """
        sac = FetchSubscores(
            self.feature_config,
            self.analysis_config,
            self.signal_action_config)
        pipeline_ret = sac.run(self.pipeline)
        (record, signal) = next(pipeline_ret.records_signals)
        # Verify that the relevant sub-scores are fetched
        expected = [{'DG': 0.1, 'PI': 0.3, 'WI': 0.4},
                    {'DG': 10, 'PI': 3, 'WI': 4}]

        assert expected == signal['sub_scores']


class CalculateOverallScoreSACTests(TestCase):
    def setUp(self):
        self.account_profile = AccountProfileFactory(
            account_type=AccountProfile.AccountType.NONPROFIT)
        self.account = AccountFactory(account_profile=self.account_profile)
        dc = DynamicClassModelMixin()
        dc.dynamic_class = MajorGiftsWeightsPersonaController
        self.feature = FeatureFactory(title='Major Gifts Persona')
        self.feature_config = FeatureConfigFactory(
            account=self.account,
            feature=self.feature
        )
        self.analysis_config = AnalysisConfigFactory(
            account=self.account, feature_configs=self.feature_config)
        self.signal_action_config = mock.MagicMock()
        self.signal_action_config.definition = {}
        signal = {
            'fused_client_summary': [{'_id': 'bgbfg67g',
                                      'personas': {
                                          'major_gifts': {'DG': 0.1,
                                                          'PI': 0.3,
                                                          'WI': 0.4},
                                          'likelihood_to_give': {'DG': 1.1,
                                                                 'PI': 1.3,
                                                                 'WI': 1.4}}},
                                     {'_id': '63716tyt',
                                      'personas': {
                                          'major_gifts': {'DG': 10,
                                                          'PI': 3,
                                                          'WI': 4},
                                          'likelihood_to_give': {'DG': 2.1,
                                                                 'PI': 2.3,
                                                                 'WI': 2.4}}}],
            'fused_entity_ids': ['bgbfg67g', '63716tyt'],
            'weights': {'WI': 0.04,
                        'AI': 0.02,
                        'PI': 0.03,
                        'DG': 0.01},
            'sub_scores': [{'DG': 0.1, 'PI': 0.3, 'WI': 0.4},
                           {'DG': 10, 'PI': 3, 'WI': 4}]
        }
        record = signal
        records_signals = [(record, signal)]
        self.pipeline = Pipeline(
            target=mock.MagicMock(),
            records_signals=records_signals)

    def test_calculate_overall_score_sac(self):
        """Verify that the `CalculateOverallScore` SAC uses the sub-scores that
        results in maximum overall score.
        """
        sac = CalculateOverallScore(
            self.feature_config,
            self.analysis_config,
            self.signal_action_config)
        pipeline_ret = sac.run(self.pipeline)
        (record, signal) = next(pipeline_ret.records_signals)
        # Verify that the sub-scores are the expected scores i.e set of scores
        # that would result in maximum overall score.
        expected_persona_scores = {'score': 0.35,
                                   'DG': 10,
                                   'WI': 4,
                                   'PI': 3}
        assert expected_persona_scores == signal['persona_scores']


class PopulatePersonaKeySignalsSACTests(TestCase):
    def setUp(self):
        self.account_profile = AccountProfileFactory(
            account_type=AccountProfile.AccountType.NONPROFIT)
        self.account = AccountFactory(account_profile=self.account_profile)
        dc = DynamicClassModelMixin()
        dc.dynamic_class = MajorGiftsWeightsPersonaController
        self.feature = FeatureFactory(title='Major Gifts Persona')
        self.feature_config = FeatureConfigFactory(
            account=self.account,
            feature=self.feature
        )
        self.analysis_config = AnalysisConfigFactory(
            account=self.account, feature_configs=self.feature_config)
        self.signal_action_config = mock.MagicMock()
        self.signal_action_config.definition = {
                "mappings": [{"source_field": "persona_scores"}]
            }
        signal = {
            'fused_client_summary': [{'_id': 'bgbfg67g',
                                      'personas': {
                                          'major_gifts': {'DG': 0.1,
                                                          'PI': 0.3,
                                                          'WI': 0.4},
                                          'likelihood_to_give': {'DG': 1.1,
                                                                 'PI': 1.3,
                                                                 'WI': 1.4}}},
                                     {'_id': '63716tyt',
                                      'personas': {
                                          'major_gifts': {'DG': 10,
                                                          'PI': 3,
                                                          'WI': 4},
                                          'likelihood_to_give': {'DG': 2.1,
                                                                 'PI': 2.3,
                                                                 'WI': 2.4}}}],
            'fused_entity_ids': ['bgbfg67g', '63716tyt'],
            'weights': {'WI': 0.04,
                        'AI': 0.02,
                        'PI': 0.03,
                        'DG': 0.01},
            'sub_scores': [{'DG': 0.1, 'PI': 0.3, 'WI': 0.4},
                           {'DG': 10, 'PI': 3, 'WI': 4}],
            'persona_scores': {'score': 0.35, 'DG': 10, 'WI': 4, 'PI': 3}
        }
        record = signal
        records_signals = [(record, signal)]

        self.pipeline = Pipeline(
            target=mock.MagicMock(),
            records_signals=records_signals
        )

    def test_sac(self):
        """Verify that the `PopulatePersonaKeySignals` populates the
        key_signals with personas.
        """
        sac = PopulatePersonaKeySignals(
            self.feature_config,
            self.analysis_config,
            self.signal_action_config)
        # Mock out persona_configs
        sac.persona_configs = {'mg_identifier_1': "blah",
                               'mg_identifier_2': "blah blah"}

        pipeline_ret = sac.run(self.pipeline)
        expected = {
            'mg_identifier_1': {
                'score': 0.35,
                'DG': 10,
                'WI': 4,
                'PI': 3},
            'mg_identifier_2': {
                'score': 0.35,
                'DG': 10,
                'WI': 4,
                'PI': 3}}

        # Verify that the `personas` key_signals is populated appropriately.
        assert all([item in pipeline_ret.key_signals['personas']
                    for item in expected])
