import datetime
import string

import unittest.mock as mock

from frontend.apps.analysis.factories import RecordSetConfigFactory
from frontend.apps.analysis.sources.base import (
    RecordAccessWrapper, RecordAccessWrapperField, DefaultValueField,
    StaticValueField, TransformField, ListStrFormatField, StrFormatField,
    MultiTryField, MultipleKeyField)
from frontend.apps.analysis.sources.business_data.backends.mongodb import (
    BusinessData)
from frontend.apps.analysis.sources.fec.backends.mongodb import (
    FECCandidates, FECCommittees)
from frontend.apps.analysis.sources.misp.backends.mongodb import (
    StateEntities)
from frontend.apps.analysis.sources.non_profit.backends.mongodbv3 import (
    ContributionRecordWrapper, NonProfitEntities)
from frontend.apps.analysis.sources.stocks.backends.mongodb import (
    Stocks)
from frontend.libs.utils.general_utils import BlankObject
from frontend.libs.utils.string_utils import swapcase
from frontend.libs.utils.test_utils import TestCase


class TestUtils(object):
    def _common_operations_test_helper(self, record, key, value):
        result = getattr(record, key)
        self.assertEqual(result, value)

        result = record[key]
        self.assertEqual(result, value)

        result = record.get(key)
        self.assertEqual(result, value)

        # This tests the __contains__ magic method
        self.assertIn(key, record)

        # This tests the __iter__ magic method
        self.assertIn(key, list(record))

        self.assertIn(key, list(record.keys()))
        self.assertIn(key, list(record.keys()))

        self.assertIn((key, value), list(record.items()))
        self.assertIn((key, value), list(record.items()))

        self.assertIn(value, list(record.values()))
        self.assertIn(value, list(record.values()))


class RecordAccessWrapperTests(TestCase, TestUtils):
    def shortDescription(self):
        """Hack to prevent nosetests from showing docstrings instead of method
        and class name in test runner.

        See: http://www.saltycrane.com/blog/2012/07/how-prevent-nose-unittest-using-docstring-when-verbosity-2/
        """
        return None



    def test_old_key_continues_to_work(self):
        class test_wrapper(RecordAccessWrapper):
            asd = 'qwe'

        data = {'qwe': 'MY QWE VALUE',
                'zxc': 'MY ZXC VALUE'}
        record = test_wrapper(data)

        key = 'qwe'
        value = 'MY QWE VALUE'

        # Old key is still available via all get methods (e.g.: attribute access,
        # item access, `get()` method)
        result = getattr(record, key)
        self.assertEqual(result, value)

        result = record[key]
        self.assertEqual(result, value)

        result = record.get(key)
        self.assertEqual(result, value)


        # Ensure `key in wrapper` continues to work (which exercises the
        # `__contains__` magic method)
        self.assertIn(key, record)

        # Ensure remapped keys do NOT show up when iterating over keys. They
        # should be replaced with the new renamed key.

        # (this tests the __iter__ magic method)
        self.assertNotIn(key, list(record))

        self.assertNotIn(key, list(record.keys()))
        self.assertNotIn(key, list(record.keys()))

        # Ensure keys in source record that have not been remapped continue to
        # work
        self._common_operations_test_helper(record, 'zxc', 'MY ZXC VALUE')

    def test_simple_rename(self):
        class test_wrapper(RecordAccessWrapper):
            asd = 'qwe'

        data = {'qwe': 'MY QWE VALUE'}
        record = test_wrapper(data)

        # Simple renames should work
        self._common_operations_test_helper(record, 'asd', 'MY QWE VALUE')

        # Renamed fields should show up in result from get_all called with
        # only_remapped=True
        self.assertIn('asd', record.get_all(True))

        # Source fields should not show up in result from get_all called with
        # only_remapped=True
        self.assertNotIn('qwe', record.get_all(True))
        # Source fields should show up in result from get_all called with
        # only_remapped=False
        self.assertIn('qwe', record.get_all(False))

    def test_common_ops_on_rawf(self):
        """Ensure all common operations work on RecordAccessWrapperFields just
        like on renames.
        """
        class test_field(RecordAccessWrapperField):
            def __init__(self, key):
                self.key = key

            def get(self, record_wrapper):
                return record_wrapper.get(self.key)

        class test_wrapper(RecordAccessWrapper):
            asd = test_field('qwe')

        data = {'qwe': 'MY QWE VALUE'}
        record = test_wrapper(data)

        self._common_operations_test_helper(record, 'asd', 'MY QWE VALUE')
        self.assertIn('asd', record.get_all(True))

    def test_noop_renames(self):
        class test_wrapper(RecordAccessWrapper):
            zxc = 'zxc'

        data = {'zxc': 'MY ZXC VALUE'}
        record = test_wrapper(data)

        # No-op renames should work like normal
        self._common_operations_test_helper(record, 'zxc', 'MY ZXC VALUE')

        # No-op renamed fields should show up in result from get_all called with
        # only_remapped=True
        self.assertIn('zxc', record.get_all(True))

    def test_remappings_from_superclass_work(self):
        class parent_wrapper(RecordAccessWrapper):
            asd = 'qwe'
            capwords_asd = TransformField('qwe', string.capwords)

        class test_wrapper(parent_wrapper):
            iop = 'zxc'
            swapped_asd = TransformField('capwords_asd', swapcase)

        data = {'qwe': 'MY QWE VALUE',
                'zxc': 'MY ZXC VALUE'}
        record = test_wrapper(data)

        # Remapped fields from the subclass work as expected
        self._common_operations_test_helper(record, 'iop', 'MY ZXC VALUE')

        # Renamed fields from the parent class also continue to work
        self._common_operations_test_helper(record, 'asd', 'MY QWE VALUE')

        # Remapped fields using a RecordAccessWrapperField from the parent
        # class continue to work
        self._common_operations_test_helper(record, 'capwords_asd', 'My Qwe Value')

        # Remapped RAWF fields in the child referencing a RAWF from the parent
        # class also continue to work, and apply the transformation from the
        # parent class.
        self._common_operations_test_helper(record, 'swapped_asd', 'mY qWE vALUE')

        self.assertDictContainsSubset(
            {'asd': 'MY QWE VALUE',
             'capwords_asd': 'My Qwe Value',
             'swapped_asd': 'mY qWE vALUE',
             'iop': 'MY ZXC VALUE'},
            record.get_all(True))

    def test_rename_to_nested_value(self):
        class test_wrapper(RecordAccessWrapper):
            asd = 'qwe.zxc'

        data = {'qwe': {'zxc': 'MY ZXC VALUE'}}

        record = test_wrapper(data)

        self._common_operations_test_helper(record, 'asd', 'MY ZXC VALUE')
        get_all = record.get_all(True)
        self.assertIn('asd', get_all)
        self.assertEqual(get_all['asd'], 'MY ZXC VALUE')

    def test_missing_rename_target_iter(self):
        class test_wrapper(RecordAccessWrapper):
            asd = 'iop'
        data = {'qwe': 'MY QWE VALUE',
                'zxc': 'MY ZXC VALUE'}
        record = test_wrapper(data)
        self.assertNotIn('asd', list(record))

    def test_missing_rename_target_containment_deep(self):
        class test_wrapper(RecordAccessWrapper):
            asd = 'qwe.zxc'
        # make sure we fail when topmost key is present, but last key is
        # missing
        data1 = {'qwe': {}}
        # make sure we fail if topmost key is missing
        data2 = {}
        # make sure we fail (but don't crash) even the value for the topmost
        # key isn't a mapping
        data3 = {'qwe': 0}
        self.assertNotIn('asd', test_wrapper(data1))
        self.assertNotIn('asd', test_wrapper(data2))
        self.assertNotIn('asd', test_wrapper(data3))

    def test_missing_rename_target_containment(self):
        class test_wrapper(RecordAccessWrapper):
            asd = 'iop'
            qwe = 'qwe'
        data = {}
        record = test_wrapper(data)
        self.assertNotIn('asd', record)
        self.assertNotIn('qwe', record)

    def test_wrapper_mapping_is_recursive(self):
        """Make sure that field renames can reference the value (or a nested
        value) of a RecordAccessWrapperField.

        Same thing applies to RecordAccessWrapperField -- make sure it can
        reference the output of another RecordAccessWrapperField for chained
        transformations.
        """
        def _reduce_campaigns(campaigns):
            best_count = 0
            best = None

            # Attempt to pick the best campaign by the one with the most complete
            # data.
            for campaign in campaigns:
                count = len(list(filter(None, campaign.values())))
                if count > best_count:
                    best_count = count
                    best = campaign
            return best

        class test_wrapper(RecordAccessWrapper):
            iop = TransformField("fgh", str.lower)
            rename_iop = 'iop'
            campaign = TransformField("campaigns", _reduce_campaigns)
            city = 'campaign.city'
            state = 'campaign.state'
            year = 'campaign.cycle'
            lower_state = TransformField('campaign.state', str.lower)

        data = {'fgh': 'MY FGH VALUE',
                "campaigns": [
                    {"city": None,
                     "state": None,
                     "cycle": None,
                     "district": None, },

                    # This is the record that should be selected by the reduce
                    # function. Bury it in the middle to make sure there's no
                    # cheating!
                    {"city": 'Los Angeles',
                     "state": 'CA',
                     "cycle": 2014,
                     "district": None, },

                    {"city": 'Los Angeles',
                     "state": 'CA',
                     "cycle": None,
                     "district": None, },
                    {"city": None,
                     "state": 'CA',
                     "cycle": None,
                     "district": None, }]}

        record = test_wrapper(data)

        campaign_value = {'city': 'Los Angeles',
                          'state': 'CA',
                          'district': None,
                          'cycle': 2014}

        self._common_operations_test_helper(record, 'campaign', campaign_value)
        self._common_operations_test_helper(record, 'iop', 'my fgh value')
        self._common_operations_test_helper(record, 'rename_iop', 'my fgh value')
        self._common_operations_test_helper(record, 'lower_state', 'ca')
        self._common_operations_test_helper(record, 'state', 'CA')
        self._common_operations_test_helper(record, 'city', 'Los Angeles')
        self._common_operations_test_helper(record, 'year', 2014)

        expected_get_all = {
            'campaign': campaign_value,
            'lower_state': 'ca',
            'state': 'CA',
            'city': 'Los Angeles',
            'year': 2014,
            'iop': 'my fgh value',
            'rename_iop': 'my fgh value',
        }
        get_all = record.get_all(True)
        self.assertEqual(get_all, expected_get_all)



    ## Unit tests for individual RecordAccessWrapperFields

    def test_str_format_field(self):
        class test_wrapper(RecordAccessWrapper):
            asd = StrFormatField('>>{qwe}, {zxc}<<')

        data = {'qwe': 'MY QWE VALUE',
                'zxc': 'MY ZXC VALUE'}
        record = test_wrapper(data)

        self._common_operations_test_helper(
            record, 'asd', '>>MY QWE VALUE, MY ZXC VALUE<<')
        self.assertEqual({'asd': '>>MY QWE VALUE, MY ZXC VALUE<<'},
                         record.get_all(True))

    def test_list_str_format_field(self):
        class test_wrapper(RecordAccessWrapper):
            asd = ListStrFormatField('>>>{qwe}, {zxc}<<<', '###{qwe}###',
                                     '!!!{zxc}!!!')

        data = {'qwe': 'aaa',
                'zxc': 'zzz'}
        record = test_wrapper(data)

        expected_value = [
            '>>>aaa, zzz<<<',
            '###aaa###',
            '!!!zzz!!!'
        ]
        self._common_operations_test_helper(record, 'asd', expected_value)
        self.assertEqual({'asd': expected_value}, record.get_all(True))

    def test_multiple_key_field(self):
        """Tests RecordAccessWrapperField MultipleKeyField"""
        class test_wrapper(RecordAccessWrapper):
            key1 = MultipleKeyField('ABC', 'JKL')

        data = {'ABC': 'abc',
                'DEF': 'def',
                'GHI': 'ghi',
                'JKL': 'jkl'}
        record = test_wrapper(data)

        expected_value = {
            'ABC': 'abc',
            'JKL': 'jkl'
        }
        self._common_operations_test_helper(record, 'key1', expected_value)
        self.assertEqual({'key1': expected_value}, record.get_all(True))

    def test_transform_field(self):
        class test_wrapper(RecordAccessWrapper):
            asd = TransformField('qwe', string.capwords)

        data = {'qwe': 'MY QWE VALUE'}
        record = test_wrapper(data)

        self._common_operations_test_helper(record, 'asd', 'My Qwe Value')
        self.assertEqual({'asd': 'My Qwe Value'}, record.get_all(True))

    def test_transform_date_field(self):
        class test_wrapper(RecordAccessWrapper):
            date = TransformField('gift_year',
                                  ContributionRecordWrapper.transform_date)

        data = {'gift_year': 1}
        record = test_wrapper(data)

        self._common_operations_test_helper(
            record, 'date', datetime.date(1, 1, 1))
        self.assertEqual({'date': datetime.date(1, 1, 1)},
                         record.get_all(True))

    def test_default_value_field(self):
        default_value = 'I AM A DEFAULT VALUE'

        class test_wrapper(RecordAccessWrapper):
            asd = DefaultValueField('qwe', default_value)

        # Source key missing from wrapped record
        data = {}
        record = test_wrapper(data)
        self._common_operations_test_helper(record, 'asd', default_value)

        # Source key present in wrapped record, value is anything bool(value) == True
        data = {'qwe': 'I HAVE QWE VALUE!'}
        record = test_wrapper(data)
        self._common_operations_test_helper(record, 'asd', 'I HAVE QWE VALUE!')

        # Source key present in wrapped record, value is anything bool(value) == False
        data = {'qwe': None}
        record = test_wrapper(data)
        self._common_operations_test_helper(record, 'asd', None)

    def test_static_value_field(self):
        static_value = 'I AM A STATIC VALUE!'

        class test_wrapper(RecordAccessWrapper):
            asd = StaticValueField(static_value)

        # Test with blank source record
        data = {}
        record = test_wrapper(data)
        self._common_operations_test_helper(record, 'asd', static_value)


        # Ensure static value is preferred over value in source record
        data = {'asd': 'SOMETHING ELSE'}
        record = test_wrapper(data)
        self._common_operations_test_helper(record, 'asd', static_value)

        get_all = record.get_all(True)
        self.assertIn('asd', get_all)
        self.assertEqual(get_all['asd'], static_value)

    def test_multi_try_field(self):
        default_value = 'I AM A DEFAULT VALUE'
        static_value = 'I AM A STATIC VALUE!'

        class test_wrapper(RecordAccessWrapper):
            blarg = MultiTryField('honk', 'rawr',
                                  DefaultValueField('last_attempt',
                                                    default_value))
            asd = StaticValueField(static_value)
            one = MultiTryField('zxc')
            two = MultiTryField('blarg')
            three = MultiTryField('blarg', 'asd', recursive=False)
            four = MultiTryField('blarg', 'asd', recursive=[False, True])
        data = {}
        record = test_wrapper(data)

        # Test that embedded RecordAccessWrapperFields work, and that the first
        # found value wins.
        self._common_operations_test_helper(record, 'blarg', default_value)
        data['last_attempt'] = 'I AM A LAST ATTEMPT VALUE'
        record = test_wrapper(data)
        self._common_operations_test_helper(record, 'blarg',
                                            'I AM A LAST ATTEMPT VALUE')

        # Verify that keys are attempted in order, and that the first found
        # value wins
        data['rawr'] = 'I AM A RAWR VALUE'
        record = test_wrapper(data)
        self._common_operations_test_helper(record, 'blarg',
                                            'I AM A RAWR VALUE')
        data['honk'] = 'I AM A HONK VALUE'
        record = test_wrapper(data)
        self._common_operations_test_helper(record, 'blarg',
                                            'I AM A HONK VALUE')

        # Verify that nothing breaks even if all attempted fields are missing.
        self._common_operations_test_helper(record, 'one', None)

        # Verify that recursion is supported.
        self._common_operations_test_helper(record, 'two', 'I AM A HONK VALUE')

        # Verify that recursion can be disabled
        self._common_operations_test_helper(record, 'three', None)

        # Verify that recursion can be set per key.
        self._common_operations_test_helper(record, 'four', static_value)


class BusinessDataEntitiesTests(TestCase):
    def setUp(self):
        self.record1 = {
            "_id": "JM_D67200677F3631731BA80EE1C91C8213F8DF49E2_NON_PROF_0",
            "donor_id": "52343b71da659977f451e6adac8a5ffa2bcece64",
            "match_id": "AA_e30024ad9a5715f14dfe58b7fa550fd5f1be4b3f",
            "inits": "AA",
            "name": "AARON AAFEDT",
            "title": "AGENT",
            "company_name": "A TO B CONCRETE PUMPING",
            "company_type": "DOMESTIC CORPORATION",
            "company_address": "10636 COTTONWOOD ROAD BOZEMAN MT 59718-0000",
            "incorporation_date": "11/8/2017",
            "dissolution_date": "11/8/2018",
            "QS": "16.333333333333",
            "canonical_name": "AARON AAFEDT",
            "parsed_given_name": "AARON",
            "parsed_middle_name": "",
            "parsed_last_name": "AAFEDT",
            "n_contact": ['AARON AAFEDT'],
            "n_fore": ['AARON'],
            "n_sur": ['AAFEDT'],
            "n_address": ['10636 COTTONWOOD ROAD BOZEMAN MT 59718'],
            "state": ['MT'],
            "city": ['BOZEMAN'],
            "zip": ['59718'],
            "employer": ['CORNERSTONE CONCRETE INC'],
            "occupation": [],
        }
        self.data = dict(opencorp=["anyIDdoesntmatter"])

    def test_business_data_entities(self):
        """Tests the business data entities to check that the correct methods
        are called and the correct data is returned."""
        rsc = RecordSetConfigFactory()

        # Set the expected_indexes to None so that we aren't going to the DB
        BusinessData.expected_indexes = None

        # Mock the call to _get so that we are not hitting the DB
        BusinessData._get = mock.MagicMock(return_value=[self.record1])

        # Add an extra Id to make sure it does not fail.
        self.data['unicorns'] = ['someotherId']

        resource = BusinessData(rsc.resource_config, rsc.record_set)
        target = BlankObject(entity_contribution_id_mapping=self.data)
        resource_generator = resource.fetch(target)
        result = resource_generator.__next__()

        # Check that _get is called with the correct params
        resource._get.assert_called_with(["anyIDdoesntmatter"], limit=None)

        # Check all the data is returned as expected
        self.assertEqual(result.name, "AARON AAFEDT")
        self.assertEqual(result.business_name, "A TO B CONCRETE PUMPING")
        self.assertEqual(result.company_type, "DOMESTIC CORPORATION")
        self.assertEqual(result.address,
                         "10636 COTTONWOOD ROAD BOZEMAN MT 59718-0000")
        self.assertEqual(result.incorporation_date, "11/8/2017")
        self.assertEqual(result.dissolution_date, "11/8/2018")
        self.assertEqual(result.quality_score, "16.333333333333")


class StockEntitiesTests(TestCase):
    def setUp(self):
        self.record1 = {
            "_id": "AA_5281a212566eb7efd1b8cb7b7a03fbd87f17f8f1",
            "donor_id": "2953e427921c57ba8b46a512aec1fa99c66289eb",
            "match_id": "AA_5281a212566eb7efd1b8cb7b7a03fbd87f17f8f1",
            "inits": "AA",
            "holder_name": "AARON J ALTER",
            "OrgName": "HAWAIIAN HOLDINGS INC",
            "Symbol": "HA",
            "Shares": "7122.0",
            "MarketValue": "288441.0",
            "TransactionDate": "1486425600",
            "sec_address": "3375 KOAPAKA STREET SUITE G 350 HONOLULU HI 96819",
            "sec_city": "HONOLULU",
            "sec_state": "HI",
            "sec_zip": "96819",
            "title": "Exec VP & Chief Legal Officer",
            "company_of_title": "HAWAIIAN HOLDINGS INC",
            "date_of_title": "2017",
            "QS": "22.6",
            "canonical_name": "AARON J ALTER",
            "parsed_give_name": "AARON",
            "parsed_middle_name": "J",
            "parsed_last_name": "ALTER",
            "n_contact": "AARON J ALTER",
            "n_fore": "AARON",
            "n_sur": "ALTER",
            "n_address": "3375 KOAPAKA STREET SUITE G350 HONOLULU HI 96819",
            "state": "HI",
            "city": "HONOLULU",
            "zip": "96819",
            "employer": "",
            "occupation": "",
        }
        self.data = dict(sec=["anyIDdoesntmatter"])

    def test_stock_entities(self):
        """Tests the stock entities to check that the correct methods are
        called and the correct data is returned."""
        rsc = RecordSetConfigFactory()

        # Set the expected_indexes to None so that we are not accessing Mongo
        Stocks.expected_indexes = None

        # Mock the call to _get so that we are not going to the DB
        Stocks._get = mock.MagicMock(return_value=[self.record1])

        # Add another piece of random data to see if it fails
        self.data['acorns'] = ["someOtherId"]

        resource = Stocks(rsc.resource_config, rsc.record_set)
        target = BlankObject(entity_contribution_id_mapping=self.data)
        resource_generator = resource.fetch(target)
        result = resource_generator.__next__()

        # Test that _get is called correctly
        resource._get.assert_called_with(["anyIDdoesntmatter"], limit=None)

        # Check that the data returned matches what is expected
        self.assertEqual(result.holder_name, "AARON J ALTER")
        self.assertEqual(result.OrgName, "HAWAIIAN HOLDINGS INC")
        self.assertEqual(result.Symbol, "HA")
        self.assertEqual(result.Shares, "7122.0")
        self.assertEqual(result.MarketValue, "288441.0")
        self.assertEqual(result.TransactionDate, "1486425600")
        self.assertEqual(result.sec_address,
                         "3375 KOAPAKA STREET SUITE G 350 HONOLULU HI 96819")
        self.assertEqual(result.sec_city, "HONOLULU")
        self.assertEqual(result.sec_state, "HI")
        self.assertEqual(result.sec_zip, "96819")
        self.assertEqual(result.title, "Exec VP & Chief Legal Officer")
        self.assertEqual(result.company_of_title, "HAWAIIAN HOLDINGS INC")
        self.assertEqual(result.date_of_title, "2017")
        self.assertEqual(result.quality_score, "22.6")


class NonprofitEntitiesTests(TestCase):
    def setUp(self):
        self.data = {
            "_id": "JM_D67200677F3631731BA80EE1C91C8213F8DF49E2_NON_PROF_0",
            "donor_id": "JM_D67200677F3631731BA80EE1C91C8213F8DF49E2_NON_PROF_0",
            "donor_name": "JACKIE AND BART MOORE",
            "address1": "",
            "address2": "",
            "donor_city": "",
            "donor_state": "",
            "donor_zip": "",
            "occupation": "",
            "EIN": "741193459",
            "org_category": "HIGHER EDUCATION",
            "org_city": "KERRVILLE",
            "org_home_page": "HTTP://WWW.SCHREINER.EDU/",
            "organization_name": "SCHREINER UNIVERSITY",
            "donation_date": "1970-01-01T00:00:00Z",
            "donor_source": "ANNUAL REPORTS",
            "gift_type": "MEMORIAL",
            "gift_year": 2007,
            "highgift_level": "99",
            "lowgift_level": "1",
            "org_state": "TX",
            "org_zip": "78028",
            "link_to_source": "2007_SCHREINER UNIVERSITY_KERRVILLE_TX_78028.PDF",
            "quality_score": "15.5",
            "ntee_level_1": "B",
            "ntee_level_2": "43",
            "discrete_donation_value": 47,
            "sentiment_affiliation": "",
            "entity_full_name": "JACKIE MOORE",
            "entity_first_name": "JACKIE",
            "entity_middle_name": "",
            "entity_last_name": "MOORE",
            "entity_full_address": "8011 SENDERO RIDGE DRIVE BOERNE TX 78015",
            "entity_city": [
                "BOERNE"
            ],
            "entity_state": [
                "TX"
            ],
            "entity_zip": [
                "78015"
            ],
            "entity_employer": [
                "Revup"
            ],
            "entity_occupation": ["Rocket Man"],
            "n_fore": [
                "JACKIE"
            ],
            "n_sur": [
                "MOORE"
            ],
            "n_contact": [
                "JACKIE MOORE"
            ],
            "filename": "/home/ubuntu/data/donor_search/non-profit/JM.csv",
            "date_loaded": 1551224489
        }

    def test_search_target_plus_ntee_code(self):
        """Tests the search method when both target and ntee code are given
        and found."""
        with mock.patch("frontend.apps.analysis.sources.non_profit."
                        "backends.mongodbv3.NonProfitEntities.__init__") as m:
            m.return_value = None
            rsc = mock.MagicMock()
            rsc.id = 5
            com = NonProfitEntities()
            com.record_set_config = rsc
            com._coll = mock.MagicMock()
            com._coll.find.return_value = [self.data]

            response = com.search(sort=False, target="This is mocked",
                                  ntee_level_1=self.data["ntee_level_1"],
                                  ntee_level_2=self.data["ntee_level_2"])
            self.assertEqual([_.get_all() for _ in response],
                             [{'city': self.data['org_city'],
                               'eid': self.data['donor_id'],
                               'ntee': self.data['ntee_level_1'] +
                               self.data['ntee_level_2'],
                               'record_set_config': 5,
                               'text_score': 0
                               }
                              ]
                             )

    def test_search_target_plus_ntee_code_not_found(self):
        """Tests the search method when both target and ntee code are given
        and not found."""
        with mock.patch("frontend.apps.analysis.sources.non_profit."
                        "backends.mongodbv3.NonProfitEntities.__init__") as m:
            m.return_value = None
            rsc = mock.MagicMock()
            rsc.id = 5
            com = NonProfitEntities()
            com.record_set_config = rsc
            com._coll = mock.MagicMock()
            com._coll.find.return_value = []

            response = com.search(sort=False, target="This is mockec",
                                  ntee_level_1="C", ntee_level_2="42")
            self.assertEqual([_.get_all() for _ in response],
                             []
                             )

    def test_search_target_and_ntee_code_not_found(self):
        """Tests the search method when both target and ntee code are given
        and not found."""
        with mock.patch("frontend.apps.analysis.sources.non_profit."
                        "backends.mongodbv3.NonProfitEntities.__init__") as m:
            m.return_value = None
            rsc = mock.MagicMock()
            rsc.id = 5
            com = NonProfitEntities()
            com.record_set_config = rsc
            com._coll = mock.MagicMock()
            com._coll.find.return_value = []

            response = com.search(sort=False, target="This is mocked",
                                  ntee_level_1=self.data['ntee_level_1'],
                                  ntee_level_2=self.data['ntee_level_2'])
            self.assertEqual([_.get_all() for _ in response],
                             []
                             )

    def test_search_target_one_ntee_code(self):
        """Tests the search method when both target and ntee code are given."""
        with mock.patch("frontend.apps.analysis.sources.non_profit."
                        "backends.mongodbv3.NonProfitEntities.__init__") as m:
            m.return_value = None
            rsc = mock.MagicMock()
            rsc.id = 5
            com = NonProfitEntities()
            com.record_set_config = rsc
            com._coll = mock.MagicMock()
            com._coll.find.return_value = [self.data]

            response = com.search(sort=False, ntee_level_1='B')
            self.assertEqual([_.get_all() for _ in response],
                             [{'city': self.data['org_city'],
                               'eid': self.data['donor_id'],
                               'ntee': self.data['ntee_level_1'] +
                               self.data['ntee_level_2'],
                               'record_set_config': 5,
                               'text_score': 0
                               }
                              ]
                             )

    def test_search_no_target(self):
        """Tests the search method with only ntee code and no target"""
        with mock.patch("frontend.apps.analysis.sources.non_profit."
                        "backends.mongodbv3.NonProfitEntities.__init__") as m:
            m.return_value = None
            rsc = mock.MagicMock()
            rsc.id = 5
            com = NonProfitEntities()
            com.record_set_config = rsc
            com._coll = mock.MagicMock()
            com._coll.find.return_value = [self.data]

            response = com.search(sort=False,
                                  ntee_level_1=self.data['ntee_level_1'],
                                  ntee_level_2=self.data['ntee_level_2'])
            self.assertEqual([_.get_all() for _ in response],
                             [{'city': self.data['org_city'],
                               'eid': self.data['donor_id'],
                               'ntee': self.data['ntee_level_1'] +
                               self.data['ntee_level_2'],
                               'record_set_config': 5,
                               'text_score': 0
                               }
                              ]
                             )

    def test_search_no_search_terms(self):
        """Tests the search method with no search terms"""
        with mock.patch("frontend.apps.analysis.sources.non_profit."
                        "backends.mongodbv3.NonProfitEntities.__init__") as m:
            m.return_value = None
            rsc = mock.MagicMock()
            rsc.id = 5
            com = NonProfitEntities()
            com.record_set_config = rsc
            com._coll = mock.MagicMock()
            com._coll.find.return_value = [self.data]

            response = com.search(sort=False)
            self.assertEqual([_.get_all() for _ in response],
                             [{'city': self.data['org_city'],
                               'eid': self.data['donor_id'],
                               'ntee': self.data['ntee_level_1'] +
                               self.data['ntee_level_2'],
                               'record_set_config': 5,
                               'text_score': 0
                               }
                              ]
                             )


class FECEntitiesTests(TestCase):
    def test_fec_candidates(self):
        ## Verify the formatting works correctly
        # No need to test the data format.
        with mock.patch("frontend.apps.analysis.sources.fec."
                        "backends.mongodb.Entities.__init__") as m:
            m.return_value = None
            rsc = mock.MagicMock()
            rsc.id = 5
            com = FECCandidates()
            com.record_set_config = rsc
            com._coll = mock.MagicMock()
            com._coll.find.return_value = [
                {
                    "_id": "H4CA12055",
                    "CAND_STATUS": "C",
                    "CAND_ST1": "46727 CRAWFORD STREET",
                    "CAND_ST2": "",
                    "filename": "frontend_test_data/2014/cn14.zip",
                    "CAND_NAME": "KHANNA, ROHIT",
                    "CAND_ELECTION_YR": "2014",
                    "CAND_CITY": "FREMONT",
                    "date_loaded": "06-02-2015",
                    "year": None,
                    "CAND_PTY_AFFILIATION": "DEM",
                    "CAND_OFFICE_DISTRICT": "17",
                    "CAND_ST": "CA",
                    "CAND_OFFICE": "H",
                    "CAND_ZIP": "94539",
                    "CAND_OFFICE_ST": "CA",
                    "CAND_PCC": "C00503185",
                    "CAND_ICI": "C",
                    "score": 0.7
                },
                {
                    "_id": "H0AL02087",
                    "CAND_STATUS": "C",
                    "CAND_ST1": "3260 BANKHEAD AVE",
                    "CAND_ST2": "",
                    "filename": "frontend_test_data/2014/cn14.zip",
                    "CAND_NAME": "ROBY, MARTHA",
                    "CAND_ELECTION_YR": "2014",
                    "CAND_CITY": "MONTGOMERY",
                    "date_loaded": "06-02-2015",
                    "year": None,
                    "CAND_PTY_AFFILIATION": "REP",
                    "CAND_OFFICE_DISTRICT": "02",
                    "CAND_ST": "AL",
                    "CAND_OFFICE": "H",
                    "CAND_ZIP": "361062448",
                    "CAND_OFFICE_ST": "AL",
                    "CAND_PCC": "C00462143",
                    "CAND_ICI": "I",
                    "score": 0.5
                }
            ]
            response = com.search(sort=False, target="this is mocked")
            self.assertEqual([_.get_all() for _ in response],
                             [{
                                 "state": "CA",
                                 "city": "Fremont",
                                 "district": "17",
                                 "text_score": 0.7,
                                 "name": "Khanna, Rohit",
                                 "office": "House",
                                 "year": "2014",
                                 "party": "Democrat",
                                 'record_set_config': 5,
                                 "type": "candidate",
                                 "eid": "H4CA12055"
                             },
                             {
                                 "state": "AL",
                                 "city": "Montgomery",
                                 "district": "02",
                                 "text_score": 0.5,
                                 "name": "Roby, Martha",
                                 "office": "House",
                                 "year": "2014",
                                 "party": "Republican",
                                 'record_set_config': 5,
                                 "type": "candidate",
                                 "eid": "H0AL02087"
                             }])

    def test_fec_committees(self):
        ## Verify the formatting works correctly
        with mock.patch("frontend.apps.analysis.sources.fec."
                        "backends.mongodb.Entities.__init__") as m:
            m.return_value = None
            rsc = mock.MagicMock()
            rsc.id = 5
            com = FECCommittees()
            com.record_set_config = rsc
            com._coll = mock.MagicMock()
            com._coll.find.return_value = [
                {
                    "_id": "C00503185",
                    "CMTE_DSGN": "P",
                    "CMTE_ST": "CA",
                    "CMTE_TP": "H",
                    "CMTE_ZIP": "94538",
                    "year": None,
                    "CMTE_ST1": "PO BOX 1398",
                    "TRES_NM": "MOGG, JOHN",
                    "date_loaded": "06-02-2015",
                    "CMTE_FILING_FREQ": "Q",
                    "filename": "frontend_test_data/2014/cm14.zip",
                    "CONNECTED_ORG_NM": "",
                    "ORG_TP": "",
                    "CMTE_CITY": "FREMONT",
                    "CMTE_ST2": "",
                    "CMTE_PTY_AFFILIATION": "DEM",
                    "CAND_ID": "H4CA12055",
                    "CMTE_NM": "RO FOR CONGRESS INC.",
                    "aff": {
                        "dem_for": 1,
                        "rep_for": 0,
                        "oth_against": 0,
                        "dem_against": 0,
                        "rep_against": 0,
                        "oth_for": 0
                    },
                    "score": 0.7
                },
                {
                    "_id": "C00351379",
                    "CMTE_DSGN": "P",
                    "CMTE_ST": "CA",
                    "CMTE_TP": "H",
                    "CMTE_ZIP": "95112",
                    "year": None,
                    "CMTE_ST1": "C/O CONTRIBUTION SOLUTIONS, LLC",
                    "TRES_NM": "DAY, VICKI",
                    "date_loaded": "06-02-2015",
                    "CMTE_FILING_FREQ": "Q",
                    "filename": "frontend_test_data/2014/cm14.zip",
                    "CONNECTED_ORG_NM": "HONDA VICTORY FUND",
                    "ORG_TP": "",
                    "CMTE_CITY": "SAN JOSE",
                    "CMTE_ST2": "123 E. SAN CARLOS ST., #531",
                    "CMTE_PTY_AFFILIATION": "DEM",
                    "CAND_ID": "H0CA15148",
                    "CMTE_NM": "MIKE HONDA FOR CONGRESS",
                    "aff": {
                        "dem_for": 1,
                        "rep_for": 0,
                        "oth_against": 0,
                        "dem_against": 0,
                        "rep_against": 0,
                        "oth_for": 0
                    },
                    "score": 0.5
                }
            ]
            response = com.search(sort=False, target="this is mocked")
            self.assertEqual([_.get_all() for _ in response],
                             [{
                                  "state": "CA",
                                  "city": "Fremont",
                                  "district": None,
                                  "text_score": 0.7,
                                  "name": "Ro For Congress Inc.",
                                  "office": None,
                                  "year": None,
                                  "party": "Democrat",
                                  'record_set_config': 5,
                                  "type": "committee",
                                  "eid": "C00503185"
                             },
                             {
                                 "state": "CA",
                                 "city": "San Jose",
                                 "district": None,
                                 "text_score": 0.5,
                                 "name": "Mike Honda For Congress",
                                 "office": None,
                                 "year": None,
                                 "party": "Democrat",
                                 'record_set_config': 5,
                                 "type": "committee",
                                 "eid": "C00351379"
                             }])


class StateEntitiesTests(TestCase):
    def test_state_entities(self):
        ## Verify the formatting works correctly
        with mock.patch("frontend.apps.analysis.sources.misp."
                        "backends.mongodb.Entities.__init__") as m:
            m.return_value = None
            rsc = mock.MagicMock()
            rsc.id = 5
            com = StateEntities()
            com.record_set_config = rsc
            com._coll = mock.MagicMock()
            com._coll.find.return_value = [
                {
                    "_id": "18967478",
                    "elections": [
                        {
                            "ElectionState": "DC",
                            "FSC_District": "\\N",
                            "FilingYear": "2008",
                            "PartyType": "\\N",
                            "ElectionEntityID": "4427957"
                        }
                    ],
                    "names": [
                        {"ElectionEntityname": "RE-ELECT KWAME R. BROWN"},
                        {"ElectionEntityname": "KWAME R. BROWN"}
                    ],
                    "ElectionEntityEID": "18967478",
                    'score': 0.2
                },
                {
                    "_id": "13008690",
                    "elections": [
                        {
                            "ElectionState": "CA",
                            "FSC_District": "ASSEMBLY DISTRICT 047",
                            "FilingYear": "2012",
                            "PartyType": "Democratic",
                            "ElectionEntityID": "4343889"
                        }
                    ],
                    "names": [
                        {"ElectionEntityname": "BROWN, CHERYL R"}
                    ],
                    "ElectionEntityEID": "13008690",
                    'score': 0.5
                }
            ]
            response = com.search(sort=False, target="this is mocked")
            self.assertEqual([_.get_all() for _ in response],
                             [{
                                 "state": "DC",
                                 "city": None,
                                 "district": "",
                                 "text_score": 0.2,
                                 "name": "Kwame R. Brown",
                                 "office": "",
                                 "year": "2008",
                                 "party": None,
                                 'record_set_config': 5,
                                 "type": "committee",
                                 "eid": "18967478"
                             },
                             {
                                 "state": "CA",
                                 "city": None,
                                 "district": "047",
                                 "text_score": 0.5,
                                 "name": "Brown, Cheryl R",
                                 "office": "Assembly",
                                 "year": "2012",
                                 "party": "Democratic",
                                 'record_set_config': 5,
                                 "type": "candidate",
                                 "eid": "13008690"
                             }])
