from collections import OrderedDict
import datetime
import itertools
import math
import unittest.mock as mock
from statistics import median

from frontend.apps.analysis.analyses import (
    AnalysisBase, PersonContribRanking)
from frontend.apps.analysis.analyses.base import Pipeline
from frontend.apps.analysis.analyses.formatters import (
    ResultFormatter, KeyContributionsResultFormatter)
from frontend.apps.analysis.analyses.person_contrib import ContactWrapper
from frontend.apps.analysis.analyses.signal_actions import (
    DiamondsInTheRough, OfficeMultiplier, NonprofitDiamondsInTheRough
)
from frontend.apps.analysis.factories import (
    AnalysisConfigFactory, FeatureConfigFactory,
    QuestionSetFactory, SignalSetConfigFactory, PartitionConfigFactory)
from frontend.apps.analysis.models import (Analysis, QuestionSetData,
                                           ResultSort, Result)
from frontend.apps.contact.factories import ContactFactory
from frontend.apps.seat.factories import SeatFactory
from frontend.libs.utils.string_utils import random_string
from frontend.libs.utils.test_utils import TestCase, expect


class AnalysisFrameworkTests(TestCase):
    def setUp(self):
        self.seat = SeatFactory()
        self.user = self.seat.user
        self.campaign = self.seat.account

    @mock.patch('frontend.apps.analysis.sources.base.MongodbRecordResource.'
                'expect_indexes', mock.Mock())
    def test_new_persistence(self):
        self.maxDiff = None
        # TODO: create a version of this test where some features/partitions
        # have no matches, or no giving, or neither. ensure that if neither is
        # present, the SignalSet is not created, and if no SignalSets are
        # created for a given FeatureResult, that the FeatureResult is not
        # saved.

        # create a contact for fixture
        contact = ContactFactory(user=self.user)

        # Create an analysis configuration
        ac = AnalysisConfigFactory(
            analysis_profile__analysis_type='PN_CONTRIB_RANK',
            partition_set_config__partition_configs=[
                    PartitionConfigFactory(),
                    PartitionConfigFactory(),
                    PartitionConfigFactory(),
                    PartitionConfigFactory(),
                ]
        )

        # Add partitioned and unpartitioned features with varying number of
        # signal set configs to analysis configuration
        ac.feature_configs.add(
            *[FeatureConfigFactory(
                partitioned=partitioned,
                signal_set_configs=[SignalSetConfigFactory()
                                    for _ in range(count)])
                for partitioned in (True, False) for count in range(4)])

        partitions = list(ac.partition_set_config.partition_configs.all())
        partition_ids = [_.id for _ in partitions]
        partition_count = len(partition_ids)
        features = list(f for f in ac.feature_configs.all()
                        if f.signal_set_configs.exists())
        signals = {f.id: f.signal_set_configs.all() for f in features}

        # Fixture generation
        rrp = {_: {'score': 43, 'giving': 22, 'political_spectrum': 0.8,
                   'key_signals': {'score': 43, 'political_giving': 22,
                                   'nonprofit_giving': 98, 'client_giving': 37,
                                   'giving_capacity': 1000}}
               for _ in partition_ids}

        match_stub = {'giving': 100, 'match': ['123']}
        part_match_stub = {'partitions':
                           {_: match_stub for _ in partition_ids}}
        feature_fixture = OrderedDict((
            f.id, {ssc.id: part_match_stub if f.partitioned else match_stub
                   for ssc in f.signal_set_configs.all()})
            for f in features)

        result = mock.MagicMock()
        result.target = ContactWrapper(contact, mock.MagicMock())
        result.results = {'partitions': rrp}

        fixture = {
            'result': result,
            'features': feature_fixture,
            'key_signals': {},
            'tags': set()
        }

        # create analysis instance
        analysis = AnalysisBase.factory(ac, self.user, self.campaign)
        # make sure analysis record is created
        analysis.start()

        # Verify buffers are empty
        self.assertEqual(len(analysis._result_buffer), 0)

        analysis._persist_result(contact, fixture)

        # Verify we created one Result per Partition
        self.assertEqual(len(analysis._result_buffer), partition_count)
        # Verify we create 1 FeatureResult per FeatureConfig per Result
        frs = list(itertools.chain.from_iterable(
            i[1] for i in analysis._result_buffer._buffer))
        self.assertEqual(len(frs), partition_count * len(features))

        # Ensure the contents of a Result are correct
        results = analysis._result_buffer._buffer
        for i, (result, feature_results, result_index, result_sorts) in enumerate(results):
            self.assertEqual(result.user, self.user)
            self.assertEqual(result.contact, contact)
            self.assertEqual(result.analysis, analysis.analysis)
            self.assertEqual(result.partition, analysis.partition_configs[i])
            self.assertEqual(result.score, 43)
            self.assertEqual(result.key_signals, {
                "score": 43,
                'giving': 22,
                'political_spectrum': 0.8,
                'political_giving': 22,
                'nonprofit_giving': 98,
                'client_giving': 37,
                'giving_capacity': 1000,
                'key_contributions': []
            })
            self.assertEqual(result.tags, [])
            # Verify a ResultSort is created for each key_signal that maps to a
            # ResultSort.ValueTypes
            result_sorts_map = {rs.value_type: rs for rs in result_sorts}
            for key in ('score', 'political_giving', 'nonprofit_giving',
                        'client_giving', 'giving_capacity'):
                value = result.key_signals[key]
                value_type = ResultSort.ValueTypes.SIGNAL_MAP[key]
                result_sort = result_sorts_map[value_type]
                assert result_sort.contact == contact
                assert result_sort.analysis == analysis.analysis
                assert result_sort.result is result
                assert result_sort.partition == analysis.partition_configs[i]
                assert result_sort.value == value
                assert result_sort.identifier == ''

            # Verify the contents of the FeatureResults
            for j, feature in enumerate(features):
                fr = feature_results[j]
                self.assertEqual(fr.user_id, self.user.id)
                self.assertEqual(fr.feature_config_id, feature.id)
                self.assertEqual(fr.result, result)
                # Sort the signals by signalset ID so they can be compared
                self.assertListEqual(
                    sorted(fr.signals['signal_sets'],
                           key=lambda x: x["config_id"]),
                    sorted([{'giving': 100,
                            'config_id': ssc.id,
                            'match': ['123']}
                            for ssc in signals[feature.id]],
                           key=lambda x: x["config_id"])
                )

    def test_build_resultsort(self):
        mock_result = mock.Mock(
            # Specify `spec` and `_state` so we pass isinstance check when
            # `ResultSort` is initialized with this mock standing in for a
            # `Result`
            spec=Result,
            _state=mock.Mock(),
            contact_id=7,
            analysis_id=89,
            partition_id=2,
            key_signals={
                'score': 43,
                'political_giving': 22,
                'nonprofit_giving': 98,
                'client_giving': 37,
                'giving_capacity': 1000,
                'personas': {
                    'mg_15': {
                        'score': 26,
                        'DG': 77,
                        'WI': 21,
                        'PI': 90,
                        'AI': 11
                    },
                    'lg_26': {
                        'score': 6,
                        'DG': 7,
                        'WI': 2,
                        'PI': 9,
                        'AI': 1
                    },
                }
            }
        )
        # Mapping of (identifier, value_type) to result value. There should be
        # one ResultSort created for each entry in this mapping.
        result_key = {
            ('', 'SCORE'): 43,
            ('', 'POL_GIVING'): 22,
            ('', 'NFP_GIVING'): 98,
            ('', 'CLI_GIVING'): 37,
            ('', 'GIV_CAP'): 1000,
            ('mg_15', 'SCORE'): 26,
            ('mg_15', 'DEMO_SCORE'): 77,
            ('mg_15', 'PHIL_SCORE'): 90,
            ('mg_15', 'WEALTH_SCORE'): 21,
            ('mg_15', 'AFF_SCORE'): 11,
            ('lg_26', 'SCORE'): 6,
            ('lg_26', 'DEMO_SCORE'): 7,
            ('lg_26', 'PHIL_SCORE'): 9,
            ('lg_26', 'WEALTH_SCORE'): 2,
            ('lg_26', 'AFF_SCORE'): 1,
        }
        resultsorts = list(PersonContribRanking._build_resultsort(mock_result))
        expect(len(resultsorts)) == len(result_key)
        seen = set()
        for resultsort in resultsorts:
            expect(resultsort.result is mock_result).is_truthy()
            expect(resultsort.contact_id) == 7
            expect(resultsort.analysis_id) == 89
            expect(resultsort.partition_id) == 2
            key = (resultsort.identifier, resultsort.value_type)
            # Verify each (identifier, value_type) combo is unique
            expect(seen).excludes(key)
            seen.add(key)
            # Verify each (identifier, value_type) combo was expected
            expect(result_key).contains(key)
            value = result_key[key]
            # Verify the value persisted matches pre-calcualted value.
            expect(resultsort.value) == value

    @mock.patch('frontend.apps.analysis.analyses.dummy.DummyRunner.run',
                side_effect=RuntimeError)
    @mock.patch('frontend.apps.analysis.analyses.dummy.DummyRunner.parallel_run',
                side_effect=TypeError)
    def test_analysis_task(self, *mocks):
        analysis_config = self.campaign.get_default_analysis_config()
        analysis = AnalysisBase.factory(analysis_config,
                                        self.user, self.campaign)

        with self.assertRaises(RuntimeError):
            analysis.execute()

        # Verify analysis parallelizes
        with mock.patch('frontend.apps.analysis.analyses.dummy.DummyRunner.'
                        '_should_parallelize', return_value=True):
            with self.assertRaises(TypeError):
                analysis.execute()

        self.assertEqual(analysis.analysis.status,
                         Analysis.STATUSES.error)

    @mock.patch('frontend.apps.analysis.analyses.dummy.DummyRunner.run',
                return_value=True)
    def test_analysis_last_completed_success(self, *mocks):
        """Test that when analysis is run successfully the last_completed
        timestamp is updated in the analysis model."""
        analysis_config = self.campaign.get_default_analysis_config()
        analysis = AnalysisBase.factory(analysis_config,
                                        self.user, self.campaign)

        analysis.execute()
        self.assertEqual(analysis.analysis.status,
                         Analysis.STATUSES.complete)
        self.assertIsNotNone(analysis.analysis.last_completed)

    @mock.patch('frontend.apps.analysis.analyses.dummy.DummyRunner.run',
                side_effect=RuntimeError)
    def test_analysis_last_completed_error(self, *mocks):
        """Test that when analysis is run with an error status the
        last_completed timestamp is NOT updated in the analysis model."""
        analysis_config = self.campaign.get_default_analysis_config()
        analysis = AnalysisBase.factory(analysis_config,
                                        self.user, self.campaign)

        with self.assertRaises(RuntimeError):
            analysis.execute()

        self.assertEqual(analysis.analysis.status,
                         Analysis.STATUSES.error)
        self.assertIsNone(analysis.analysis.last_completed)

    def test_calc_progress_step(self):
        for result_count in range(11000):
            result = PersonContribRanking._calc_progress_step(result_count)
            self.assertGreaterEqual(result, 0.45)
            self.assertLessEqual(result, 0.49)

        result_count = 1017
        result = PersonContribRanking._calc_progress_step(result_count)
        stop_point = (result * result_count) + (result_count / 2.0)
        progress_percent = int(stop_point / result_count * 100)
        self.assertEqual(progress_percent, 98)

        result_count = 362
        result = PersonContribRanking._calc_progress_step(result_count)
        stop_point = (result * result_count) + (result_count / 2.0)
        progress_percent = int(stop_point / result_count * 100)
        self.assertEqual(progress_percent, 99)

        result_count = 6471
        result = PersonContribRanking._calc_progress_step(result_count)
        stop_point = (result * result_count) + (result_count / 2.0)
        progress_percent = int(stop_point / result_count * 100)
        self.assertEqual(progress_percent, 96)

    def test_contact_wrapper(self):
        contact = ContactFactory(user=self.user)
        runner = mock.MagicMock()
        wrapper = ContactWrapper(contact, runner)

        ## Verify contact fields are accessible through the wrapper
        self.assertEqual(wrapper.first_name, contact.first_name)
        self.assertEqual(wrapper.get_info(), contact.get_info())

        ## Verify contact fields can be set through the wrapper
        name = random_string()
        wrapper.first_name = name
        self.assertEqual(contact.first_name, name)

        ## Verify the special method
        zip3s = [123, 456, 789]
        runner._get_contact_zip3s.return_value = zip3s
        self.assertEqual(wrapper.zip3s, zip3s)

        new_zip3s = [321, 987]
        wrapper.zip3s = new_zip3s
        self.assertEqual(wrapper.zip3s, new_zip3s)


# NonprofitDiamondsInTheRough
class NFPDitRSignalActionTests(TestCase):
    def test_run(self):
        ac = AnalysisConfigFactory()
        qs = QuestionSetFactory()
        fc = FeatureConfigFactory(question_sets=[qs])
        ac.feature_configs.add(fc)

        # Positive test
        now = datetime.datetime.now().date()
        signals = [{
            "giving": i+100 if i < 2 else 100*i,
            "trans_date": now if i < 2 else datetime.date.today() -
                                            datetime.timedelta(days=500),
            "class": "NonprofitDiamondsInTheRough"} for i in range(1, 6)]

        key_signals = {
            "giving_availability": 5000,
            "giving_this_period": 100,
            "year_of_last_contribution": now.year
        }
        global_signals = {'key_signals': key_signals}

        sac = mock.MagicMock()
        sac.definition = {}

        cached_record_signals = {23: [(mock.MagicMock(), signals[i])
                                      for i in range(len(signals))],
                                 21: [(mock.MagicMock(), signals[i])
                                      for i in range(len(signals))]}

        def _run_tests(present):
            pipeline = Pipeline(mock.MagicMock(),
                                global_signals=global_signals)
            self.assertNotIn("nfp-ditr", pipeline.tags)
            nfp_ditr = NonprofitDiamondsInTheRough(signal_action_config=sac,
                                                   analysis_config=ac,
                                                   feature_config=fc)
            nfp_ditr.rsc_feature_map = {23: 'Nonprofit Matches',
                                        21: 'Client Matches'
                                        }
            nfp_ditr.run(pipeline, cached_record_signals)
            last_assert = self.assertIn if present else self.assertNotIn
            last_assert("nfp-ditr", pipeline.tags)

        def _run_median_test(amounts, median_amount):
            calculated_median = median(amounts)
            self.assertEqual(calculated_median, median_amount)

        # Positive case, test this is a ditr and is added to the pipeline
        sac.definition["class"] = "NonprofitDiamondsInTheRough"
        _run_tests(True)

        # Positive case, test the median is being calculated correctly
        amounts = [s['giving'] for s in signals]
        # get the median from the test data (odd number of in dataset)
        amts_median = amounts[len(amounts)//2]
        _run_median_test(amounts, amts_median)

        # Negative case, test this is NOT a ditr and is not added to the
        # pipeline
        signals = [{
            "giving": i + 100 if i < 2 else 100 * i,
            "trans_date": now,
            "class": "NonprofitDiamondsInTheRough"} for i in range(1, 6)]
        cached_record_signals = {0: [(mock.MagicMock(), signals[i])
                                     for i in range(len(signals))]}
        _run_tests(False)


class DitRSignalActionTests(TestCase):
    def test_run(self):
        ac = AnalysisConfigFactory()
        qs = QuestionSetFactory()
        fc = FeatureConfigFactory(question_sets=[qs])
        ac.feature_configs.add(fc)
        qsd = QuestionSetData.objects.create(
            question_set=qs,
            analysis_config=ac,
            data={},
            uid="1235"
        )

        ## Positive test
        now = datetime.datetime.now().date()
        signals = [{
            "trans_amt": 500 + (30 * (17 % (i or 1))),  # Some mean variance
            "trans_date": now,
            "party": "DEM"} for i in range(-5, 5)]

        key_signals = {
            "giving_availability": 5000,
            "giving_this_period": 100,
            "year_of_last_contribution": now.year
        }
        global_signals = {'key_signals': key_signals}


        sac = mock.MagicMock()
        sac.definition = {}

        cached_record_signals = {0: [(mock.MagicMock(), signals[i])
                                     for i in range(len(signals))]}

        def _run_tests(present):
            pipeline = Pipeline(mock.MagicMock(),
                                global_signals=global_signals)
            self.assertNotIn("ditr", pipeline.tags)
            ditr = DiamondsInTheRough(signal_action_config=sac,
                                      analysis_config=ac, feature_config=fc)
            ditr.run(pipeline, cached_record_signals)
            last_assert = self.assertIn if present else self.assertNotIn
            last_assert("ditr", pipeline.tags)

        # Ensure party values taken from QSD are properly handled
        # Negative test, opposite party
        qsd.data["party"] = "Republican"
        qsd.save()
        _run_tests(False)

        # Positive test, unrecognized party (also verify that valid QS party
        # values don't cause an exception)
        qsd.data["party"] = "Green"
        qsd.save()
        _run_tests(True)

        # Positive test
        qsd.data["party"] = "Democratic"
        qsd.save()
        _run_tests(True)

        # Ensure backwards compatability with definition-provided
        # configuration.
        # Negative test, opposite party
        sac.definition["party"] = "REP"
        _run_tests(False)

        # Positive test
        sac.definition["party"] = "DEM"
        _run_tests(True)

        # Negative case, within acceptable stddev of mean
        key_signals["giving_this_period"] = 475
        _run_tests(False)

        # Negative case, above mean
        key_signals["giving_this_period"] = 800
        _run_tests(False)


class OfficeMultiplierSignalActionTests(TestCase):
    def test_run(self):
        ac = AnalysisConfigFactory()
        qs = QuestionSetFactory(title="Office Info")
        fc_federal = FeatureConfigFactory(feature__title='Federal Matches', question_sets=[qs])
        fc_state = FeatureConfigFactory(feature__title='State Matches', question_sets=[qs])
        ac.feature_configs.add(fc_federal, fc_state)
        qsd = QuestionSetData.objects.create(
            question_set=qs,
            analysis_config=ac,
            data={},
            uid="sadas",
        )
        sac = mock.MagicMock(definition={})

        def _create_records_signals():
            return [
                # Federal "records"
                (None, {'office': 'H', 'multipliers': {}}),
                (None, {'office': 'S', 'multipliers': {}}),
                (None, {'office': 'P', 'multipliers': {}}),
                # State "records"
                (None, {'office': 'G', 'multipliers': {}}),
            ]

        def _run_tests(expected_federal, expected_state):
            pipeline = Pipeline(None, records_signals=_create_records_signals())
            sa = OfficeMultiplier(signal_action_config=sac,
                                  analysis_config=ac, feature_config=fc_federal)
            sa.run(pipeline)
            results_federal = {s['office']: s['multipliers']
                               for r, s in pipeline.records_signals}
            self.assertEqual(results_federal, expected_federal)

            pipeline = Pipeline(None, records_signals=_create_records_signals())
            sa = OfficeMultiplier(signal_action_config=sac,
                                  analysis_config=ac, feature_config=fc_state)
            sa.run(pipeline)
            results_state = {s['office']: s['multipliers']
                             for r, s in pipeline.records_signals}
            self.assertEqual(results_state, expected_state)

        blank_result_stub = {
            "H": {},
            "S": {},
            "P": {},
            "G": {},
        }

        # No crashes and no multipliers when office and office_type are missing
        _run_tests(blank_result_stub, blank_result_stub)

        # No crashes and no multipliers when office is missing
        qsd.data["office_category"] = "Federal"
        qsd.save()
        _run_tests(blank_result_stub, blank_result_stub)

        # Test Federal Senate config
        qsd.data = {"office_category": "Federal", "office": "Senate"}
        qsd.save()
        _run_tests({"H": {'office_multiplier': 1.25},
                    "S": {'office_multiplier': 1.5},
                    "P": {},
                    "G": {}}, blank_result_stub)

        # Test Federal House config
        qsd.data = {"office_category": "Federal", "office": "House"}
        qsd.save()
        _run_tests({"H": {'office_multiplier': 1.75},
                    "S": {'office_multiplier': 1.25},
                    "P": {},
                    "G": {}}, blank_result_stub)

        # Test Federal Presidential config
        qsd.data = {"office_category": "Federal", "office": "President"}
        qsd.save()
        _run_tests({"H": {'office_multiplier': 1.25},
                    "S": {'office_multiplier': 1.25},
                    "P": {},
                    "G": {}}, blank_result_stub)

        # Test State Gubernatorial config
        qsd.data = {"office_category": "State", "office": "Governor"}
        qsd.save()
        _run_tests(blank_result_stub,
                   {"H": {},
                    "S": {},
                    "P": {},
                    "G": {'office_multiplier': 2.0}})

        # for each Federal QSD office
        # for federal fc
        # first three signals should have multipliers
        # last signal should be empty
        # for state fc
        # everything is blank

        # for each State QSD office
        # for federal fc
        # everything is blank
        # for state fc
        # first three signals should be empty
        # last signal should have multiplier


class ConfigurableSignalActionTests(TestCase):
    def test_configuration_wrapper(self):
        from frontend.apps.analysis.analyses.base import ConfigurationWrapper
        # Some setup code to prepare the QuestionSetData
        ac = AnalysisConfigFactory()
        qs1 = QuestionSetFactory(title="Key Contribs", allow_multiple=True)
        qs2 = QuestionSetFactory(title="Not Key Contribs")
        fc1 = FeatureConfigFactory(question_sets=[qs1, qs2])
        fc2 = FeatureConfigFactory(question_sets=[qs2])
        ac.feature_configs.add(fc1, fc2)

        QuestionSetData.objects.create(
            question_set=qs1,
            analysis_config=ac,
            data={"a": "b", "c": "d"},
            uid="123")
        QuestionSetData.objects.create(
            question_set=qs1,
            analysis_config=ac,
            data={"a": "x", "c": "y"},
            uid="1235")
        QuestionSetData.objects.create(
            question_set=qs2,
            analysis_config=ac,
            data={"m": "s"},
            uid="sadas")

        ## Verify a wrapper with multiple QuestionSets
        cw = ConfigurationWrapper(ac, fc1)
        # Verify various positive cases
        self.assertEqual(cw.get_list("a", "c"), [["x", "y"], ["b", "d"]])
        self.assertEqual(cw.get_list("a"), ["x", "b"])
        # "a" and "c" are not part of any singular question sets
        self.assertEqual(cw.get_single("a", "c"), (None, None))
        self.assertEqual(cw.get(["a", "c"]), None)
        self.assertEqual(cw.get(["a", "c"], default=cw._null_result),
                         (None, None))
        self.assertEqual(cw.get_single("a"), None)
        self.assertEqual(cw.get("a"), None)
        self.assertEqual(cw.get("a", default="sadness"), "sadness")

        self.assertEqual(cw.get_single("m"), "s")
        self.assertEqual(cw.get_list("m"), [None])

        # Verify some misses
        self.assertEqual(cw.get_list("a", "d"), [["x", None], ["b", None]])
        self.assertEqual(cw.get_single("f"), None)
        self.assertEqual(cw.get_list("f"), [None])
        self.assertEqual(cw.get_list("f", "g"), [(None, None)])

        ## Verify a wrapper with only one QS
        cw = ConfigurationWrapper(ac, fc2)
        self.assertEqual(cw.get_single("m", "a"), ["s", None])
        # Verify that a wrapper only gets data for QS associated with the
        # provided FC
        self.assertNotEqual(cw.get_list("a"), ["x", "b"])
        self.assertEqual(cw.get_list("a"), [None])

        # Unless the QS has `is_global` set to True
        qs1.is_global = True
        qs1.save()
        cw = ConfigurationWrapper(ac, fc2)
        self.assertEqual(cw.get_list("a"), ["x", "b"])


class ResultFormatterTests(TestCase):
    """Verify the general Result formatter."""
    def test_format_signal_set(self):
        rf = ResultFormatter(mock.MagicMock(), None)
        result = {
            "giving": 10,
            "score": 1500,
            "some-invalid-field": None,
            "match": ["1234567"]
        }
        expected = result.copy()
        del expected['some-invalid-field']
        expected["config_id"] = 11
        signals = rf._format_signal_set(expected["config_id"], result)
        self.assertEqual(signals, expected)


class KeyContributionsResultFormatterTests(TestCase):
    """Verify the Key Contributions Result formatter."""
    def test_format_signal_set(self):
        rf = KeyContributionsResultFormatter(mock.MagicMock(), None)
        result = {
            "giving": 10,
            "score": 1500,
            "some-invalid-field": None,
            "match": ["1234567"],
            "key_contributions": {"Obama,True": [("4061920121157705786", 1)]}
        }
        expected = {
            "giving": 10,
            "score": 1500,
            "key_contribs": {"Obama,True": [("4061920121157705786", 1)]}}
        signals = rf._format_signal_set(11, result)
        self.assertEqual(signals, expected)
