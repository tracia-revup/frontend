from contextlib import ExitStack

from django.core.exceptions import ValidationError
from django.db import transaction
from django.urls import reverse
import pytest
import unittest.mock as mock

from frontend.apps.analysis.factories import (
    AnalysisConfigFactory, FeatureConfigFactory,
    AnalysisConfigTemplateFactory, QuestionSetFactory, DataSetFactory,
    SignalSetConfigFactory, SignalActionConfigFactory,
    PartitionSetConfigFactory, PartitionConfigFactory,
    SignalSetsActionsFactory, FeatureConfigsSignalSetsFactory)
from frontend.apps.analysis.models import QuestionSet, QuestionSetData
from frontend.apps.analysis.ntee_codes import (
    NTEE_PRIMARY_CATEGORIES, NTEE_SUB_CATEGORIES)
from frontend.apps.analysis.questionset_controllers import (
    KeyContributions, ClientContributions, NFPKeyContributions,
    MajorGiftsWeightsPersonaController)
from frontend.apps.authorize.factories import RevUpUserFactory
from frontend.apps.campaign.factories import (
    PoliticalCampaignFactory, NonprofitCampaignFactory)
from frontend.apps.seat.factories import SeatFactory
from frontend.libs.utils.general_utils import deep_hash
from frontend.libs.utils.test_utils import TestCase


class OwnershipValidationTests(TestCase):
    def setUp(self):
        self.seat = SeatFactory()
        self.user = self.seat.user
        self.campaign = self.seat.account

    def test_no_ownership_clean(self):
        # If a ValidationError is not raised, this test passes.
        model = DataSetFactory(user=None, account=None)

        # This should work with no problems.
        model.clean()

    def test_account_and_no_user_success(self):
        # If a ValidationError is not raised, this test passes.
        model = DataSetFactory(user=None, account=None)
        model.account = self.campaign
        model.clean()

    def test_account_and_user_success(self):
        # If a ValidationError is not raised, this test passes.
        model = DataSetFactory(user=None, account=None)
        model.account = self.campaign
        model.user = self.user
        model.clean()

    def test_user_and_no_account_errors(self):
        model = DataSetFactory(user=None, account=None)
        model.user = self.user

        # If user is set, account must also be set otherwise a validation error
        # occurs.
        with self.assertRaises(ValidationError):
            model.clean()

    def test_user_must_have_seat_on_account(self):
        model = DataSetFactory(user=None, account=None)
        model.account = self.campaign
        model.user = RevUpUserFactory()

        # If user and account are set, user must have a seat on account.
        with self.assertRaises(ValidationError):
            model.clean()

    def test_clean_run_on_save(self):
        model = DataSetFactory.build(user=None, account=None)

        with mock.patch('frontend.apps.analysis.models.config_base.'
                        'OwnershipModel.clean') as clean_mock:
            model.save()

        clean_mock.assert_called_once_with()

    def test_successful_ownership_validate(self):
        parent = DataSetFactory.build(user=None, account=None)
        child = DataSetFactory.build(user=None, account=None)

        # Valid configuration:
        # parent.account is unset, parent.user is unset
        # child.account is unset, child.user is unset
        parent.validate(child)

        # Valid configuration:
        # parent.account is set, parent.user is unset
        # child.account is unset, child.user is unset
        parent.account = self.campaign
        parent.validate(child)

        # Valid configuration:
        # parent.account is set, parent.user is unset
        # child.account is set, child.user is unset
        # and parent.account == child.account
        child.account = self.campaign
        parent.validate(child)

        # Valid configuration:
        # parent.account is set, parent.user is set
        # child.account is set, child.user is unset
        # and parent.account == child.account
        parent.user = self.user
        parent.validate(child)

        # Valid configuration:
        # parent.account is set, parent.user is set
        # child.account is unset, child.user is unset
        child.account = None
        parent.validate(child)

        # Valid configuration:
        # parent.account is set, parent.user is set
        # child.account is set, child.user is set
        # and parent.account == child.account
        # and parent.user == child.user
        child.account = self.campaign
        child.user = self.user
        parent.validate(child)

    def test_error_when_child_user_and_no_parent_user(self):
        parent = DataSetFactory.build(user=None, account=self.campaign)
        child = DataSetFactory.build(user=self.user, account=self.campaign)

        # If child.user is set, parent.user must also be set
        with self.assertRaises(ValidationError):
            parent.validate(child)

    def test_error_when_child_user_and_parent_user_do_not_match(self):
        parent = DataSetFactory.build(user=RevUpUserFactory(),
                                      account=self.campaign)
        child = DataSetFactory.build(user=self.user, account=self.campaign)

        # When parent.user and child.user are both set, they must also match.
        with self.assertRaises(ValidationError):
            parent.validate(child)

    def test_error_when_child_account_and_no_parent_account(self):
        parent = DataSetFactory.build(user=None, account=None)
        child = DataSetFactory.build(user=None, account=self.campaign)

        # If child.account is set, parent.account must also be set
        with self.assertRaises(ValidationError):
            parent.validate(child)

    def test_error_when_child_account_and_parent_account_do_not_match(self):
        parent = DataSetFactory.build(user=None, account=self.campaign)
        child = DataSetFactory.build(user=None,
                                     account=PoliticalCampaignFactory())

        # When parent.account and child.account are both set, they must also match.
        with self.assertRaises(ValidationError):
            parent.validate(child)


class AnalysisFrameworkValidationTests(TestCase):
    @mock.patch('frontend.apps.analysis.models.analysis_configs.'
                'AnalysisConfig.validate')
    def test_analysis_config_clean(self, validate_mock):
        ac = AnalysisConfigFactory()

        # Reset mock as it will have been hit during initialization of
        # AnalysisConfig instance
        validate_mock.reset_mock()

        ac.save()

        # Make sure we run validate on all FK fields where the related model is
        # a subclass of OwnershipModel
        self.assertCountEqual(validate_mock.call_args_list, [
            mock.call(ac.data_config),
            mock.call(ac.partition_set_config),
        ])

    @mock.patch('frontend.apps.analysis.models.analysis_configs.'
                'SignalSetConfig.validate')
    def test_signal_set_config_clean(self, validate_mock):
        ssc = SignalSetConfigFactory()
        validate_mock.reset_mock()
        ssc.save()
        # Make sure we run validate on all FK fields where the related model is
        # a subclass of OwnershipModel
        self.assertCountEqual(validate_mock.call_args_list, [
            mock.call(ssc.record_set_config),
            mock.call(ssc.spectrum_config),
        ])

    @mock.patch('frontend.apps.analysis.models.analysis_configs.'
                'SignalSetConfig.validate')
    def test_signal_sets_actions_clean(self, validate_mock):
        signal_action = SignalActionConfigFactory.build()
        signal_set_config = SignalSetConfigFactory.build()

        ssa = SignalSetsActionsFactory.build(
            signal_set_config=signal_set_config,
            signal_action_config=signal_action)

        # Ensure that when SignalSetsActions clean method is called, that
        # validate on SignalSetConfig is called with SignalActionConfig given
        # as the argument.
        ssa.clean()

        validate_mock.assert_called_once_with(signal_action)

    @mock.patch('frontend.apps.analysis.models.analysis_configs.'
                'AnalysisConfig.validate')
    def test_analysis_config_m2m_validate_feature_config(self, validate_mock):
        # Ensure validation is being run when ManyToMany relation is created.
        ac = AnalysisConfigFactory()
        fc = FeatureConfigFactory()

        # Reset mock as it will have been hit during initialization of
        # AnalysisConfig instance
        validate_mock.reset_mock()

        # Add FeatureConfig to AnalysisConfig to trigger validate_m2m, which
        # runs validation
        ac.feature_configs.add(fc)

        validate_mock.assert_called_once_with(fc)

    @mock.patch('frontend.apps.analysis.models.analysis_configs.'
                'SignalSetConfig.validate')
    def test_signal_set_config_m2m_validate_signal_set_config(self, validate_mock):
        # Ensure validation is being run when ManyToMany relation is created.
        parent = SignalSetConfigFactory()
        child = SignalSetConfigFactory()

        # Reset mock as it will have been hit during initialization of
        # SignalSetConfig instances
        validate_mock.reset_mock()

        # Add child SignalSetConfig to parent SignalSetConfig to trigger
        # validate_m2m, which runs validation like so: parent.validate(child)
        parent.signal_set_configs.add(child)

        validate_mock.assert_called_once_with(child)

        # Sanity check to be sure that the framework properly differentiates
        # between parent and child.
        with self.assertRaises(AssertionError):
            validate_mock.assert_called_once_with(parent)

        # Now verify that even when adding the parent to the child via the
        # reverse lookup manager, that we check permissions from parent ->
        # child, and not the other way around.
        validate_mock.reset_mock()
        parent.signal_set_configs.remove(child)

        # Add the parent to the child's reverse related manager. the
        # `validate_m2m` method should properly detect this and run
        # `parent.validate(child)`
        child.dependent_sets.add(parent)

        validate_mock.assert_called_once_with(child)

        # Sanity check to be sure that the framework properly differentiates
        # between parent and child.
        with self.assertRaises(AssertionError):
            validate_mock.assert_called_once_with(parent)

    @mock.patch('frontend.apps.analysis.models.analysis_configs.'
                'PartitionSetConfig.validate')
    def test_partition_set_config_m2m_validate_partition_set(self, validate_mock):
        # Ensure validation is being run when ManyToMany relation is created.
        partition_set_config = PartitionSetConfigFactory.build()
        partition_set_config.save()
        partition_config = PartitionConfigFactory()

        # Add PartitionConfig to PartitionSetConfig to trigger validate_m2m,
        # which runs validation
        partition_set_config.partition_configs.add(partition_config)

        validate_mock.assert_called_once_with(partition_config)

    @mock.patch('frontend.apps.analysis.models.analysis_configs.'
                'FeatureConfig.validate')
    def test_feature_config_m2m_validate_question_sets(self, validate_mock):
        # Ensure validation is being run when ManyToMany relation is created.
        feature_config = FeatureConfigFactory(question_sets=[])
        question_set = QuestionSetFactory()
        validate_mock.reset_mock()

        # Add QuestionSet to FeatureConfig to trigger validate_m2m, which runs
        # validation
        feature_config.question_sets.add(question_set)

        validate_mock.assert_called_once_with(question_set)

    @mock.patch('frontend.apps.analysis.models.analysis_configs.'
                'FeatureConfig.validate')
    def test_feature_config_m2m_validate_signal_set_configs(self, validate_mock):
        # Ensure validation is being run when ManyToMany relation is created.
        feature_config = FeatureConfigFactory.build()
        signal_set_config = SignalSetConfigFactory.build()

        fcss = FeatureConfigsSignalSetsFactory.build(
            feature_config=feature_config,
            signal_set_config=signal_set_config)

        # Ensure that when FeatureConfigsSignalSets clean method is called, that
        # validate on FeatureConfig is called with SignalSetConfig given
        # as the argument.
        fcss.clean()

        validate_mock.assert_called_once_with(signal_set_config)

    def test_m2m_pre_save(self):
        # Verify exception is raised when M2M fails validation, and the
        # relation is not persisted.
        partition_set_config = PartitionSetConfigFactory.build()
        partition_set_config.save()
        pc1 = PartitionConfigFactory()
        pc2 = PartitionConfigFactory(account=PoliticalCampaignFactory())

        # Our control: adding the first PartitionConfig to our
        # PartitionSetConfig should not throw an exception, and we should find
        # that PartitionConfig via the related manager when done.
        self.assertFalse(partition_set_config.partition_configs.filter(
            pk=pc1.pk).exists())
        partition_set_config.partition_configs.add(pc1)
        self.assertTrue(partition_set_config.partition_configs.filter(
            pk=pc1.pk).exists())

        # When we attempt to create a M2M relation between a parent and child
        # that violates one of the validations we set up, an exception should
        # be raised the relation should not be made.
        self.assertFalse(partition_set_config.partition_configs.filter(
            pk=pc2.pk).exists())
        with ExitStack() as es:
            es.enter_context(transaction.atomic())
            es.enter_context(self.assertRaises(ValidationError))
            partition_set_config.partition_configs.add(pc2)
        self.assertFalse(partition_set_config.partition_configs.filter(
            pk=pc2.pk).exists())

    @mock.patch('frontend.apps.analysis.models.analysis_configs.'
                'PartitionSetConfig.validate')
    def test_m2m_multiadd(self, validate_mock):
        # Ensure validation is run when multiple ManyToMany relations are
        # created simultaneously.
        partition_set_config = PartitionSetConfigFactory.build()
        partition_set_config.save()
        pc1 = PartitionConfigFactory()
        pc2 = PartitionConfigFactory()
        pc3 = PartitionConfigFactory()

        # Add PartitionConfig to PartitionSetConfig to trigger validate_m2m,
        # which runs validation
        partition_set_config.partition_configs.add(pc1, pc2, pc3)

        self.assertCountEqual(validate_mock.call_args_list,
                              [mock.call(pc1), mock.call(pc2), mock.call(pc3)])


class QuestionSetDataTests(TestCase):
    def setUp(self):
        self.account = PoliticalCampaignFactory()
        self.analysis_config = AnalysisConfigFactory(account=self.account)
        self.feature_config = FeatureConfigFactory(account=self.account)
        self.question_set = self.feature_config.question_sets.first()

    def test_create_and_update(self):
        self.question_set.dynamic_class = ClientContributions
        self.question_set.save()
        test_data = {"test": "data"}

        ## Verify create
        qsd = QuestionSetData.create(self.question_set, self.analysis_config,
                                     {"test": "data"})
        self.assertTrue(qsd.active)
        self.assertEqual(qsd.question_set, self.question_set)
        self.assertEqual(qsd.analysis_config, self.analysis_config)
        self.assertEqual(qsd.version, self.analysis_config.last_execution)
        self.assertEqual(str(qsd.uid), str(deep_hash(test_data)))
        self.assertEqual(qsd.data, test_data)

        ## Verify update
        old_hash = qsd.uid
        old_version = qsd.version
        new_test_data = {"newtest": "data"}
        qsd.update(new_test_data)
        self.assertTrue(qsd.active)
        self.assertEqual(qsd.question_set, self.question_set)
        self.assertEqual(qsd.analysis_config, self.analysis_config)
        self.assertEqual(qsd.version, old_version)
        self.assertEqual(str(qsd.uid), str(deep_hash(new_test_data)))
        self.assertNotEqual(qsd.uid, old_hash)
        self.assertEqual(qsd.data, new_test_data)

    def test_getitem(self):
        qsd = QuestionSetData(data={"a": {"b": [1, 2]}, "b": 3})
        self.assertEqual(qsd["b"], 3)
        self.assertEqual(qsd["a.b"], [1, 2])
        self.assertEqual(qsd["a.b.0"], 1)
        self.assertEqual(qsd["a.b.1"], 2)


class AnalysisConfigBaseTests(TestCase):
    def setUp(self):
        self.account = PoliticalCampaignFactory()
        self.config_template = AnalysisConfigTemplateFactory()
        self.feature_configs = [FeatureConfigFactory() for i in range(5)]
        self.config_template.feature_configs.set(self.feature_configs)
        self.analysis_config = self.config_template.generate_analysis_config(
            self.account)

    def test_generate_analysis_config(self):
        def verify(source, copy):
            self.assertEqual(source.analysis_profile, copy.analysis_profile)
            self.assertEqual(source.data_config, copy.data_config)
            self.assertEqual([fc.id for fc in source.feature_configs.all()],
                             [fc.id for fc in copy.feature_configs.all()])
            self.assertEqual(source.partition_set_config,
                             copy.partition_set_config)
            self.assertIsNotNone(copy.last_execution)

        ## Verify generating an analysis config from a template
        verify(self.config_template, self.analysis_config)
        self.assertEqual(self.analysis_config.analysis_config_template,
                         self.config_template)
        self.assertEqual(self.analysis_config.account, self.account)
        self.assertIn(self.analysis_config,
                      self.account.analysis_configs.all())

        ## Verify generating an analysis config from another config
        account2 = PoliticalCampaignFactory()
        ac2 = self.analysis_config.generate_analysis_config(account2)
        verify(self.config_template, ac2)
        self.assertEqual(ac2.analysis_config_template, self.config_template)
        self.assertEqual(ac2.account, account2)
        self.assertIn(ac2, account2.analysis_configs.all())

    def test_get_question_sets(self):
        qs1 = QuestionSet.objects.get(title="Key Contributions")
        qs2 = QuestionSetFactory(title="Test QS")
        fc = self.feature_configs[0]
        fc.question_sets.add(qs2)

        # Verify it returns all
        questions = self.analysis_config.get_question_sets()
        self.assertEqual(questions, [qs1, qs2])

        # Verify it can return selected questions
        questions = self.analysis_config.get_question_sets(title="Test QS")
        self.assertEqual(questions, [qs2])


class KeyContributionsControllerTests(TestCase):
    def setUp(self):
        self.kc = KeyContributions()
        self.account = PoliticalCampaignFactory()
        self.analysis_config = AnalysisConfigFactory(account=self.account)

    def test_get_fields(self):
        fields = self.kc.get_fields(self.account.id, self.analysis_config.id)
        expected = [{
            "label": "Search",
            "type": "entity_search",
            "query_url": reverse("entity_search_api-list",
                                 args=(self.account.id,
                                       self.analysis_config.id)),
            "query_arg": "q",
            "expected": [
                dict(field="label", type="text"),
                dict(field="is_ally", type="boolean"),
                dict(field="entities", type="list",
                    content=["record_set_config",
                             "eid"])],
        }]
        self.assertEqual(fields, expected)

    def test_process_data(self):
        test_data = {
            "some": "other",
            "junk": "that",
            "shouldn't": "be returned",
            "entities": [
                {
                    "some more": "junk",
                    "record_set_config": 7,
                    "eid": "C00503185"
                },
                {
                    "record_set_config": 6,
                    "eid": "C00503185"
                }
            ],
            "is_ally": False,
            "label": "Foo"
        }
        data = self.kc.process_data(test_data)
        self.assertEqual(data, {
            "entities": [
                {
                    "record_set_config": 7,
                    "eid": "C00503185"
                },
                {
                    "record_set_config": 6,
                    "eid": "C00503185"
                }
            ],
            "is_ally": False,
            "label": "Foo"
        })


class NfpKeyContributionsControllerTests(TestCase):
    def setUp(self):
        self.kc = NFPKeyContributions()
        self.account = PoliticalCampaignFactory()
        self.analysis_config = AnalysisConfigFactory(account=self.account)

    def test_get_fields(self):
        """Tests the return of `get_fields` method in `NFPKeyContributions`"""
        fields = self.kc.get_fields(self.account.id, self.analysis_config.id)
        expected = [
            {"label": "Is Ally",
             "type": "boolean",
             "expected": [
                     dict(field="is_ally", type="boolean"),
                 ]},
            {"label": "Category NTEE Code",
             "type": "dropdown",
             "add_clear": True,
             "sort_choices": True,
             "choices": NTEE_PRIMARY_CATEGORIES,
             "expected": [
                 dict(field="ntee_level_1", type="list"),
             ]},
            {"label": "Sub-category NTEE Code",
             "type": "dropdown",
             "add_clear": True,
             "sort_choices": True,
             "choice_key": "ntee_level_1",
             "choices": NTEE_SUB_CATEGORIES,
             "expected": [
                 dict(field="ntee_level_2", type="list"),
             ]}
        ]
        self.assertEqual(fields, expected)

    def test_process_data(self):
        """Test `process_data` when ntee codes were supplied in the data."""
        test_data = {
            "some": "other",
            "junk": "that",
            "shouldn't": "be returned",
            "ntee_level_1": ["A"],
            "ntee_level_2": ["63"],
            "is_ally": False,
            "label": "Foo"
        }
        data = self.kc.process_data(test_data)
        self.assertEqual(data, {
            "categories": [{'ntee_level_1': 'A', 'ntee_level_2': '63'}],
            "is_ally": False,
            "label": 'A63 Ballet'
        })

    def test_process_data_2(self):
        """Test `process_data` when ntee_level_1_codes and ntee_level_2 codes
        are of different lengths.
        """
        test_data = {
            "some": "other",
            "junk": "that",
            "shouldn't": "be returned",
            "ntee_level_1": ["A", "B", "C"],
            "ntee_level_2": ["1", "2"],
            "is_ally": False,
            "label": "Foo"
        }

        exception_message = "ntee_level_1 and ntee_level_2 lists are of " \
                            "different lengths"
        # Verify that an exception is raised when ntee_level_1 and
        # ntee_level_2 are of different lengths
        with pytest.raises(ValueError, match=exception_message):
            self.kc.process_data(test_data)

    def test_process_data_3(self):
        """Test `process_data` when no NTEE codes were supplied."""
        test_data = {
            "some": "other",
            "junk": "that",
            "shouldn't": "be returned",
            "is_ally": False,
            "label": "Foo"
        }
        exception_message = "At least one category is required to submit a " \
                            "key contribution."
        # Verify that an exception is raised when no NTEE categories were
        # supplied
        with pytest.raises(AttributeError, match=exception_message):
            self.kc.process_data(test_data)


class MajorGiftsWeightsControllerTests(TestCase):
    def setUp(self):
        self.mgw = MajorGiftsWeightsPersonaController()
        self.account = NonprofitCampaignFactory()
        self.analysis_config = AnalysisConfigFactory(account=self.account)

    def test_get_fields(self):
        """Tests the return of `get_fields` method in
        `MajorGiftsWeightsPersona`
        """
        fields = self.mgw.get_fields(self.account.id, self.analysis_config.id)
        expected = [
            {"label": "Demographics Weight",
             "type": "float",
             "placeholder": 0.0,
             "required": False,
             "expected": [
                     dict(field="dg_weight", type="float"),
                 ]},
            {"label": "Wealth Weight",
             "type": "float",
             "placeholder": 0.0,
             "required": False,
             "expected": [
                 dict(field="wi_weight", type="float"),
             ]},
            {"label": "Philanthropy Weight",
             "type": "float",
             "placeholder": 0.0,
             "required": False,
             "expected": [
                 dict(field="pi_weight", type="float"),
             ]},
            {"label": "Affinity Weight",
             "type": "float",
             "placeholder": 0.0,
             "required": False,
             "expected": [
                 dict(field="ai_weight", type="float"),
             ]}
        ]
        self.assertEqual(fields, expected)

    def test_process_data(self):
        """Test `process_data` when weights and other junk data are supplied in
         the data."""
        test_data = {
            "some": "other",
            "junk": "that",
            "shouldn't": "be returned",
            "pi_weight": "0.5",
            "ai_weight": "0.1",
            "wi_weight": "0.3",
            "dg_weight": "0.2",
            "label": "Foo"
        }
        data = self.mgw.process_data(test_data)
        self.assertEqual(data, {
            'pi_weight': 0.5,
            'ai_weight': 0.1,
            'wi_weight': 0.3,
            'dg_weight': 0.2
            })

    def test_process_data_2(self):
        """Test `process_data` when a weight less than 0 is supplied."""
        test_data = {
            "some": "other",
            "junk": "that",
            "shouldn't": "be returned",
            "wi_weight": -0.3,
            "label": "Foo"
        }
        exception_message = "The Wealth Weight must be greater than or" \
                            " equal to 0."
        # Verify that an exception is raised when weights less than 0 is
        # supplied
        with pytest.raises(ValueError, match=exception_message):
            self.mgw.process_data(test_data)


