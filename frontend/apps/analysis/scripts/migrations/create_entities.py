
from django.conf import settings
from pymongo import TEXT, errors


def run():
    """Create an entities collection from the "st" (misp) collection
    for searching on candidates and committees.
    This creates new collection called "st_entities".

    This script can be ran multiple times; however, 'st_entities'
    WILL BE OVERWRITTEN

    Example output collection format:
    {
    "_id" : "13013232",
    "names": [
        {"ElectionEntityName" : "SPARKS, JOHN"}
    ]
    "elections" : [
        {
            "ElectionEntityID" : "5496285",
            "PartyType" : "Democratic",
            "FSC_District" : "SENATE DISTRICT 016",
            "FilingYear" : "2006",
            "ElectionState" : "OK"
        }
    ],
    "ElectionEntityEID" : "13013232",
    }
    """
    client = settings.MONGO_CLIENT
    db = client[settings.MONGODB_NAME]

    print(db.st.aggregate([
        {"$group": {
          "_id": {"ElectionEntityEID": "$ElectionEntityEID"},
          "names": {"$addToSet": {
              "ElectionEntityname": "$ElectionEntityname"}},
          "elections": {"$addToSet": {
              "ElectionEntityID": "$ElectionEntityID",
              "PartyType": "$PartyType",
              "FSC_District": "$FSC_District",
              "FilingYear": "$FilingYear",
              "ElectionState": "$ElectionState"}}}},
        {"$project": {"_id": "$_id.ElectionEntityEID",
                    "ElectionEntityEID": "$_id.ElectionEntityEID",
                    "names": "$names",
                    "elections": "$elections"}},
        {"$out": 'st_entities'}],
        allowDiskUse=True))
    # Add text index to state entities
    try:
        db.st_entities.drop_index("ElectionEntityName_text")
    except errors.OperationFailure:
        pass
    print(db.st_entities.create_index([("names.ElectionEntityname", TEXT)],
                                      background=True))

    # Add a text index to FEC entities
    print(db.cm.create_index([("CMTE_NM", TEXT)], background=True))
    print(db.cn.create_index([("CAND_NAME", TEXT)], background=True))
