from django.db import models

from frontend.apps.analysis.models import (AnalysisConfig, FeatureConfig,
                                           DataConfig, DataSetConfig)


def run():
    dsc_new = DataSetConfig.objects.get(
        title='Non-profit and Academic Data Configuration')
    fc_new = FeatureConfig.objects.get(
        title='Non-profit and academic contributions')

    dsc_old = DataSetConfig.objects.filter(
        models.Q(title='Academic Data Configuration') |
        models.Q(title='Non-Profit Data Configuration'))
    fc_old = FeatureConfig.objects.filter(
        models.Q(title='Academic contributions') |
        models.Q(title='Nonprofit contributions'))

    dcs_to_update = DataConfig.objects.filter(
        data_set_configs__in=dsc_old).distinct()
    for dc in dcs_to_update.iterator():
        dc.data_set_configs.remove(*dsc_old)
        dc.data_set_configs.add(dsc_new)

    acs_to_update = AnalysisConfig.objects.filter(
        feature_configs__in=fc_old).distinct()
    for ac in acs_to_update.iterator():
        ac.feature_configs.remove(*fc_old)
        ac.feature_configs.add(fc_new)
