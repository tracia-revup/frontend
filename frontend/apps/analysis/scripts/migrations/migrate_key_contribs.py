

def setup_key_contrib_v2(apps=None):
    """Setup the new configuration items needed for key contribs v2.
    If they are already done, this function will just return
    """
    if apps:
        FeatureConfig = apps.get_model("analysis", "FeatureConfig")
        SignalSetConfig = apps.get_model("analysis", "SignalSetConfig")
        SignalActionConfig = apps.get_model("analysis", "SignalActionConfig")
        SignalSetsActions = apps.get_model("analysis", "SignalSetsActions")
    else:
        from frontend.apps.analysis.models import (
            FeatureConfig, SignalSetConfig, SignalActionConfig,
            SignalSetsActions)

    # Create the new FeatureConfig
    try:
        feature_conf = FeatureConfig.objects.get(
            title="Key Contributions v2")
    except FeatureConfig.DoesNotExist:
        # Copy the original Key Contributions feature config
        feature_conf = FeatureConfig.objects.get(
            title="Key Contributions")
        feature_conf.id = None
        feature_conf.pk = None
        feature_conf.title = "Key Contributions v2"
        feature_conf.save()

    # Create the new SignalSetConfig
    kc_sigconfig, created = SignalSetConfig.objects.get_or_create(
        title='Key Contributions v2')

    # Create the score factors signal action
    try:
        SignalActionConfig.objects.get(
            title="Apply Score Factors")
    except SignalActionConfig.DoesNotExist:
        SignalActionConfig.objects.create(
            title="Apply Score Factors",
            definition={
                "class": "ApplyScoreFactors",
                "module": "frontend.apps.analysis.analyses.signal_actions"
            }
        )

    if not created and feature_conf.signal_set_configs.first() == kc_sigconfig:
        # If this is true, we can be fairly sure this is already configured,
        # so there is nothing to do.
        return

    # Add the signal set to the feature config
    feature_conf.signal_set_configs.through.objects.get_or_create(
        feature_config=feature_conf,
        signal_set_config=kc_sigconfig,
        order=10)

    # Create the new SignalActionConfig that will replace one of the
    # SACs in the Key Contributions FeatureConfig.
    try:
        kc_sac = SignalActionConfig.objects.get(
            title="Key Contributions Record Filter")
    except SignalActionConfig.DoesNotExist:
        # 'get_or_create' doesn't work with all of these fields (it creates
        # duplicates), so this is an ad-hoc get_or_create.
        kc_sac = SignalActionConfig.objects.create(
            title="Key Contributions Record Filter",
            definition={
                "class": "KeyContributionRecordFilter",
                "module": "frontend.apps.analysis.analyses.signal_actions"
            }
        )

    # Get the old SignalSet to use as a template to build the new one.
    old_signal_set_config = SignalSetConfig.objects.get(
        title="Key Contributions")
    sig_acts = list((s.signal_action_config, s.order)
                    for s in old_signal_set_config.signalsetsactions_set.all())
    # Replace the first signal action with the new one
    sig_acts[0] = (kc_sac, sig_acts[0][1])
    # Attach the key contrib signal actions to the new signal set config
    for sig_act, order in sig_acts:
        SignalSetsActions.objects.get_or_create(
            signal_set_config=kc_sigconfig,
            signal_action_config=sig_act,
            partitioned=False,
            order=order)

    # Create the new signal action needed in the root features
    try:
        SignalActionConfig.objects.get(
            title="Key Contributions Tag and Apply Multiplier")
    except SignalActionConfig.DoesNotExist:
        # 'get_or_create' doesn't work with all of these fields (it creates
        # duplicates), so this is an ad-hoc get_or_create.
        SignalActionConfig.objects.create(
            title="Key Contributions Tag and Apply Multiplier",
            partitioned=False,
            description="Mark any contributions that match a Key Contribution "
                        "entity, and apply the appropriate multipliers.",
            definition={
                "module": "frontend.apps.analysis.analyses.signal_actions",
                "class": "KeyContribsTagAndApplyMultiplier",
                "multipliers": {}
            }
        )


def configure_root_features(analysis_config, kc_qs, kc_tag_score, apply_mult,
                            new_feature_config, old_feature_config,
                            apps=None):
    if apps:
        FeatureConfig = apps.get_model("analysis", "FeatureConfig")
        SignalSetsActions = apps.get_model("analysis", "SignalSetsActions")
    else:
        from frontend.apps.analysis.models import (
            FeatureConfig, SignalSetsActions)

    # Replace the old key contributions with the new
    analysis_config.feature_configs.remove(old_feature_config)
    analysis_config.feature_configs.add(new_feature_config)

    # Process contribution features
    for root_title, order in (("State individual contributions", (95, 115)),
                              ("FEC individual contributions", (95, 115)),
                              ("Local individual contributions", (55, None))):
        try:
            root_fc = analysis_config.feature_configs.get(
                title__contains=root_title)
            root_fc.question_sets.add(kc_qs)

            # Add the new SignalAction to filter the key contributions
            ssc = root_fc.signal_set_configs.first()
            if not ssc.signalsetsactions_set.filter(
                    signal_action_config__title__contains=
                    "Key Contributions Tag and Apply Multiplier").exists():
                SignalSetsActions.objects.create(
                    signal_set_config=ssc,
                    signal_action_config=kc_tag_score,
                    partitioned=True,
                    order=order[0])

                # Remove CommitteeMultiplier SignalSetsActions because
                # Key Contrib scoring will take-over that role.
                # try:
                # multipliers = ssc.signalsetsactions_set.get(
                # signal_action_config__title__contains=
                # "CommitteeMultipliers")
                #     multipliers.delete()
                # except SignalSetsActions.DoesNotExist:
                #     pass

            if order[1] and not ssc.signalsetsactions_set.filter(
                    signal_action_config__title__contains=
                    "Apply Score Factors").exists():

                # Add score multiplier signal action
                SignalSetsActions.objects.create(
                    signal_set_config=ssc,
                    signal_action_config=apply_mult,
                    partitioned=True,
                    order=order[1])

        except FeatureConfig.DoesNotExist:
            print("{} missing from {}".format(root_title,
                                              analysis_config.title))
            continue


def configure(analysis_configs=None, apps=None):
    """What this migration does:

        1.) Modify the Key Contributions FeatureConfig.
            * The filter signal action in the signal set needs to be reconfigured
              to use a new signal action class. The filtering is done differently
              now.
        2.) For every AnalysisConfig using KeyContributions, we need to do a
            couple things:
            * Add a new Key Contributions SignalAction to each of the root
              features with key contributions.
            * Remove committee multiplier signal actions from those same
              FeatureConfigs
        """
    # If this was called from a migration, we will use the apps object
    if apps:
        SignalActionConfig = apps.get_model("analysis", "SignalActionConfig")
        AnalysisConfig = apps.get_model("analysis", "AnalysisConfig")
        AnalysisConfigTemplate = apps.get_model("analysis",
                                                "AnalysisConfigTemplate")
        QuestionSet = apps.get_model("analysis", "QuestionSet")
        FeatureConfig = apps.get_model("analysis", "FeatureConfig")
    else:
        from frontend.apps.analysis.models import (
            SignalActionConfig, AnalysisConfig,
            AnalysisConfigTemplate, FeatureConfig, QuestionSet
        )

    # Setup the new configuration items needed for key contribs v2.
    # If they are already done, this function will do nothing
    setup_key_contrib_v2()

    # Query some objects we'll need below
    kc_qs = QuestionSet.objects.get(title="Key Contributions")
    kc_tag_score = SignalActionConfig.objects.get(
        title="Key Contributions Tag and Apply Multiplier")
    apply_mult = SignalActionConfig.objects.get(
        title="Apply Score Factors")
    old_feature_config = FeatureConfig.objects.get(title="Key Contributions")
    new_feature_config = FeatureConfig.objects.get(
        title="Key Contributions v2")

    # Configure the analysis_configs. If specific analysis_configs are given,
    # only configure those.
    if analysis_configs:
        if not isinstance(analysis_configs, (list, tuple)):
            analysis_configs = [analysis_configs]

        for analysis_config in analysis_configs:
            if not analysis_config:
                print("Given analysis config is None")
                continue
            configure_root_features(analysis_config, kc_qs,
                                    kc_tag_score, apply_mult,
                                    new_feature_config, old_feature_config,
                                    apps=apps)

    else:
        # Update any configs containing key contributions to the new system
        key_contrib = FeatureConfig.objects.get(title="Key Contributions")

        for model in (AnalysisConfigTemplate, AnalysisConfig):
            # Get any analysis configs that contain a KeyContrib FeatureConfig
            for ac in model.objects.filter(feature_configs=key_contrib):
                configure_root_features(ac, kc_qs, kc_tag_score,
                                        new_feature_config, old_feature_config,
                                        apps=apps)
