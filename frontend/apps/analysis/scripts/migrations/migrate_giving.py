"""Process every analysis result, extract the giving from key_signals,
and update the new 'giving' field.
"""
from frontend.apps.analysis.models import Result
from frontend.apps.contact_set.models import ContactSet


def run():
    # Get all root contact sets for any active account
    for cs in ContactSet.objects.filter(source="", account__active=True) \
                                .exclude(analysis=None) \
                                .select_related("account", "user").iterator():
        print(cs.account, cs.user)
        # Iterate over every result in the root analysis and migrate the giving
        for result in Result.objects.filter(analysis=cs.analysis).only(
                "key_signals", "giving").iterator():
            giving = result.key_signals.get("giving", None)
            # Generally, giving won't be missing, but there's nothing to
            # do if it is.
            if giving is None:
                continue
            result.giving = giving
            result.save(update_fields=["giving"])
