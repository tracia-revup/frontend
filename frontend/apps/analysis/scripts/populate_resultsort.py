"""Script to populate the table for the new ResultSort model for the first time

This script iterates over every `Result` record in the database (that wasn't
created by legacy analysis), and creates `ResultSort` records for them.
Supports political analysis, academic analysis, and NFP analysis.

Speeds up persistence by persisting records in large batches, and running in
multiple processes.

**NOTE** THIS SCRIPT IS NOT SAFE TO RUN IF THE TABLE IS NOT EMPTY
"""

import logging
from multiprocessing import Process, cpu_count, Queue, Condition
from queue import Empty
from threading import Thread
from time import sleep

from django.db import connections
from django.db.models import F, OuterRef, Subquery

from frontend.apps.analysis.models import Result, ResultSort
from frontend.libs.utils.general_utils import FunctionalBuffer
from frontend.libs.utils.multiprocess_utils import multiprocess_joiner

logger = logging.getLogger(__name__)


def iter_query(source_qs, chunk_size):
    """Implements keyset pagination
    https://use-the-index-luke.com/no-offset

    We use this over django's `iterator()` as I discovered the hard way that
    `iterator()` will actually load everything into memory.
    """
    pk = None
    source_qs = source_qs.order_by('pk')
    queryset = source_qs
    while True:
        if pk:
            queryset = source_qs.filter(pk__gt=pk)
        page = queryset[:chunk_size]
        page = list(page)
        nb_items = len(page)

        if nb_items == 0:
            return

        last_item = page[-1]
        pk = last_item.pk
        yield from page
        if nb_items < chunk_size:
            return


def _mk_result_sort(result, value_type, value):
    return ResultSort(
        result=result,
        contact_id=result.contact_id,
        analysis_id=result.analysis_id,
        partition_id=result.partition_id,
        identifier='',
        value_type=value_type,
        value=value
    )


def create_result_sorts(result):
    key_signals = result.key_signals
    if 'political_giving' in key_signals:
        yield _mk_result_sort(result, 'POL_GIVING', key_signals['political_giving'])
        # result is for nfp or academic
        if not key_signals.keys().isdisjoint({'school', 'graduation_year'}):
            # result is academic
            yield _mk_result_sort(result, 'CLI_GIVING', key_signals['giving'])
        else:
            # result is nfp
            yield _mk_result_sort(result, 'NFP_GIVING', key_signals['giving'])
    else:
        # result is for political
        yield _mk_result_sort(result, 'POL_GIVING', key_signals['giving'])

    yield _mk_result_sort(result, 'SCORE', key_signals['score'])


def spawn_mp_worker(queue, queue_ready_cond):
    # Close any open DB connections in this process so we don't share db
    # connections with the parent process.
    connections.close_all()

    # Wait until notified that the queue is ready
    with queue_ready_cond:
        queue_ready_cond.wait()

    # Persist in batches of 10k
    persist_buffer = FunctionalBuffer(ResultSort.objects.bulk_create, 5000)
    try:
        while 1:
            try:
                result = queue.get(timeout=15)
            except Empty:
                logger.info("Worker shutting down as queue is empty")
                break
            try:
                persist_buffer.push(*create_result_sorts(result))
            except Exception as ex:
                logger.exception("Error occurred while processing result: %s",
                                 result)
                raise ex
    finally:
        persist_buffer.flush()


def _topup_queue(queue, results):
    # Continually top-up the queue with result records until we have added
    # every one.
    for result in results:
        queue.put(result, block=True)
    logger.info("Finished loading results into the queue")


def _queue_monitor(queue, feeder):
    """Monitor queue state

    Monitors queue fill state and emits a log message when the fill state
    breaches configured thresholds.

    Won't repeat itself -- once it emits a lot message that fill state drops
    below 80% (for example), it will not log that message again until the fill
    state rises above 80%.

    Additionally, if the fill state drops (or rises) and breaches multiple
    thresholds at once, the last threshold that was breached will be the one
    that is logged.
    """
    max_size = queue._maxsize
    # State-map of threshold and whether value is above it
    thresholds = {
        80: True,
        60: True,
        50: True,
        30: True,
        10: True,
    }
    while feeder.is_alive() or not queue.empty():
        fullness = (queue.qsize() / max_size) * 100
        changed = []
        change_was = None
        # Compare current fill percentage of queue against all configured
        # thresholds.
        for key in thresholds.keys():
            was_above = thresholds[key]
            if fullness >= key:
                if not was_above:
                    thresholds[key] = True
                    changed.append(key)
                    change_was = True
            else:
                if was_above:
                    thresholds[key] = False
                    changed.append(key)
                    change_was = False

        # If a change was detected, emit a log record
        if change_was is False:
            value = min(changed)
            logger.info("Queue fill dropped below %i%%", value)
        elif change_was is True:
            value = max(changed)
            logger.info("Queue fill rose above %i%%", value)

        sleep(5)
    logger.info("Queue is empty!")


def mp_run(results):
    # Initialize queue with a max size so we don't load the entire dataset into
    # memory. We want the queue size to be big enough that the workers don't
    # run out of work to do when we are fetching more results.
    queue = Queue(100000)
    queue_ready_cond = Condition()
    # Spawn workers
    processes = []
    for _ in range(cpu_count()):
        p = Process(target=spawn_mp_worker,
                    daemon=True,
                    args=(queue, queue_ready_cond))
        processes.append(p)
        p.start()

    # Run thread in background to keep queue topped-up with work
    feeder = Thread(name='feeder', target=_topup_queue, daemon=True,
                    args=(queue, results))
    feeder.start()

    quemon = Thread(name='quemon', target=_queue_monitor, daemon=True,
                    args=(queue, feeder))
    quemon.start()

    logger.info("Priming the queue")
    while not queue.full():
        # Give feeder some time to prime the queue
        sleep(5)

    # Wake up the workers
    logger.info("Waking up the workers")
    with queue_ready_cond:
        queue_ready_cond.notify_all()

    # Join worker processes, monitoring them for failure. If any worker dies
    # unexpectedly, fail fast and stop the entire script.
    multiprocess_joiner(processes)
    assert not feeder.is_alive(), "All workers stopped before feeder finished!"
    logger.info("All done")


def sp_run(results, ResultSort=ResultSort):
    persist_buffer = FunctionalBuffer(ResultSort.objects.bulk_create, 10000)
    for result in results:
        persist_buffer.push(*create_result_sorts(result))
    persist_buffer.flush()


def run_from_migration(Result, ResultSort):
    results = Result.objects.exclude(partition=None)
    sp_run(iter_query(results, 10000), ResultSort)


def run():
    results = Result.objects.exclude(partition=None)
    mp_run(iter_query(results, 100000))
