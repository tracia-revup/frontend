"""Analysis tasks

This module provides asynchronous task support for analyses.

"""
from datetime import datetime
from logging import getLogger

from ddtrace import tracer
from django.core.cache import cache
from django.db import transaction
from celery import task, result

from frontend.apps.analysis.analyses import AnalysisBase
from frontend.apps.analysis.models import AnalysisConfig
from frontend.apps.authorize.models import RevUpUser
from frontend.apps.campaign.models import Account
from frontend.apps.contact.models import Contact


LOGGER = getLogger(__name__)


def on_failure(task, exc, task_id, args, kwargs, einfo):
    """Failure handler for analysis_parallel_iter_contacts task

    Find all tasks in group and revoke them so we don't do more work than we
    have to.
    """
    parent_task = result.AsyncResult(args[0])
    callback_task = parent_task.result
    if isinstance(callback_task, result.AsyncResult):
        callback_task.revoke(terminate=True)
    if not parent_task.children:
        LOGGER.error("Child Task died before parent task finished running")
        return
    group = parent_task.children[0]
    for task in group.children:
        if task.task_id == task_id or task.ready():
            continue
        task.revoke(terminate=True)


@task(on_failure=on_failure)
def analysis_parallel_iter_contacts(_, analysis_runner,
                                    contact_query, start, end):
    """Run analysis on a batch of contacts

    This is an internal task. Do not use it directly. Use
    `perform_analysis_task` instead.

    NOTE: The first argument `_` is used by `on_failure`. Do not delete.
    """
    with analysis_runner.handle_exception():
        contact_ids = Contact.objects.all()
        contact_ids.query = contact_query
        analysis_runner.iter_contacts(contact_ids[start:end])


@task(on_failure=on_failure)
def cached_analysis_parallel_iter_contacts(parent_task_id, cache_id,
                                           _, start, end):
    """Run analysis on a batch of contacts with the runner cached instead
       of passed as an argument

    This is an internal task. Do not use it directly. Use
    `perform_analysis_task` instead.
    """
    analysis_runner, contact_query = cache.get(cache_id)
    if not analysis_runner:
        msg = ("Child task of {} cannot run because there is no "
               "cached analysis".format(parent_task_id))
        LOGGER.error(msg)
        raise ValueError(msg)
    with analysis_runner.handle_exception():
        contact_ids = Contact.objects.all()
        contact_ids.query = contact_query
        analysis_runner.parallel_iter_contacts(contact_ids[start:end])


@task(on_failure=on_failure)
def new_analysis_parallel_iter_contacts(parent_task_id, analysis_runner,
                                        contact_ids):
    """Run analysis on a batch of contacts with the runner cached instead
       of passed as an argument

    This is an internal task. Do not use it directly. Use
    `perform_analysis_task` instead.
    """
    span = tracer.current_root_span()
    span.set_tag('celery.parent_id', parent_task_id)
    if not isinstance(analysis_runner, AnalysisBase):
        cache_id = analysis_runner
        cached_value = cache.get(cache_id)
        if isinstance(cached_value, tuple):
            analysis_runner, _ = cached_value
        else:
            analysis_runner = cached_value
        if not analysis_runner:
            msg = ("Child task of {} cannot run because there is no "
                   "cached analysis runner".format(parent_task_id))
            LOGGER.error(msg)
            raise ValueError(msg)

    span.set_tags({
        'user_id': analysis_runner.user.id,
        'account_id': analysis_runner.campaign.id,
        'analysis_id': analysis_runner.analysis.id,
        'analysis_config_id': analysis_runner.analysis_config.id
    })
    with analysis_runner.handle_exception():
        analysis_runner.iter_contacts(contact_ids)


@task(queue='analysis_complete')
def analysis_parallel_complete(parent_task_id, analysis_runner):
    """Run meta features for a parallelized analysis

    This is an internal task. Do not use it directly. Use
    `perform_analysis_task` instead.
    """
    span = tracer.current_root_span()
    span.set_tag('celery.parent_id', parent_task_id)
    if not isinstance(analysis_runner, AnalysisBase):
        cache_id = analysis_runner
        cached_value = cache.get(cache_id)
        if isinstance(cached_value, tuple):
            analysis_runner, _ = cached_value
        else:
            analysis_runner = cached_value
        if not analysis_runner:
            msg = ("Child task of {} cannot run because there is no "
                   "cached analysis runner".format(parent_task_id))
            LOGGER.error(msg)
            raise ValueError(msg)

    span.set_tags({
        'user_id': analysis_runner.user.id,
        'account_id': analysis_runner.campaign.id,
        'analysis_id': analysis_runner.analysis.id,
        'analysis_config_id': analysis_runner.analysis_config.id
    })
    with analysis_runner.handle_exception():
        analysis_runner.analysis.refresh_from_db()
        if (analysis_runner.analysis.status ==
                analysis_runner.analysis.STATUSES.error):
            raise RuntimeError("Analysis already failed. We should not run "
                               "the complete task.")
        analysis_runner.complete()


@task(max_retries=1000, default_retry_delay=5*60, queue='analysis_queue')
def perform_analysis_task(analysis_config, user, campaign,
                          contact_source=None, contact_source_id=None,
                          force_new_analysis=False, update_progress=None,
                          feature_flags=None, transaction_id=None,
                          expected_count=None, **kwargs):
    """Run an analysis as an asynchronous task.

    :param analysis_config: analysis configuration
    :param user: request user
    :param campaign: campaign for this analysis
    :param feature_flags: dict of feature flags to enable/disable features
    :param parallelize: if True, split analysis into subtasks for parallel
    execution.
    """
    span = tracer.current_root_span()
    span.set_tags({
        'user_id': user,
        'account_id': campaign,
        'analysis_config_id': analysis_config
    })
    campaign = Account.objects.get(pk=campaign)
    user = RevUpUser.objects.get(pk=user)

    with transaction.atomic():
        analysis_config = AnalysisConfig.objects.select_for_update()\
                                                .get(pk=analysis_config)
        analysis_config.last_execution = datetime.now()
        analysis_config.save()

    analysis = AnalysisBase.factory(
        analysis_config, user, campaign,
        update_progress=update_progress,
        feature_flags=feature_flags,
        contact_source=contact_source,
        contact_source_id=contact_source_id,
        force_new_analysis=force_new_analysis,
        transaction_id=transaction_id,
        expected_count=expected_count,
        **kwargs
    )
    return analysis.execute()
