from datetime import datetime

from ddtrace import tracer
from django.db import transaction

from frontend.apps.seat.models import DelayedAnalysis


class DelayedAnalysisMiddleware(object):
    """Trigger an analysis for this seat if it needs one."""
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    @tracer.wrap()
    def process_view(self, request, view_func, view_args, view_kwargs):
        # Do nothing if the user isn't logged in
        if not request.user.is_authenticated or not request.user.current_seat:
            return

        now = datetime.now()
        current_seat = request.user.current_seat

        # Check if this seat needs an analysis
        if current_seat.delayed_analysis and \
                        current_seat.delayed_analysis <= now:
            # Trigger the analysis on the root CS
            with transaction.atomic():
                user = current_seat.user.lock()
                DelayedAnalysis.bulk_submit(current_seat, task_lock=user)
            # TODO: Analysis should retry on failure through this mechanism
