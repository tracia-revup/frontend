
from collections import OrderedDict
from funcy import mapcat, select_values

from .models import Feature


def ac_to_dict(analysis_config):
    output = select_values(bool, [
        # TODO: add support to optionally show id
        ('id', analysis_config.id),
        ('title', analysis_config.title),
        ('account', repr(analysis_config.account) if analysis_config.account else None),
        ('user', repr(analysis_config.user) if analysis_config.user else None),
        ('analysis_profile', OrderedDict([
            # TODO: add support to optionally show id
            #('id', analysis_config.analysis_profile.id),
            ('title', analysis_config.analysis_profile.title),
        ])),
        ('data_config', OrderedDict([
            # TODO: add support to optionally show id
            ('id', analysis_config.data_config.id),
            ('title', analysis_config.data_config.title),
            # TODO: add support for showing data source details
            # data_set_configs?
        ])),
        ('partition_set_config', OrderedDict([
            # TODO: add support to optionally show id
            #('id', analysis_config.partition_set_config.id),
            ('title', analysis_config.partition_set_config.title),
            ('label', analysis_config.partition_set_config.label),
            # partition map?
        ])),
        ('feature_configs', get_feature_configs(analysis_config)),
    ])
    return OrderedDict(output)


def get_feature_configs(analysis_config):
    def _get_fc(feature_type):
        return [(fc.title, fc_to_dict(fc))
                for fc in analysis_config.feature_configs.filter(
                    feature__feature_type=feature_type)]

    ft = Feature.FeatureTypes

    return OrderedDict(mapcat(_get_fc, [ft.PRIME, ft.MATCHES,
                                        ft.MATCHES_SUBSET, ft.SUMMARY,
                                        ft.NORMALIZATION, ft.META]))


def fc_to_dict(feature_config):
    output = select_values(bool, [
        # TODO: add support to optionally show id
        ('id', feature_config.id),
        ('title', feature_config.title),
        ('account', repr(feature_config.account) if feature_config.account else None),
        ('user', repr(feature_config.user) if feature_config.user else None),
        ('partitioned', feature_config.partitioned),
        ('class_name', feature_config.class_name or None),
        ('module_name', feature_config.module_name or None),
        ('feature', OrderedDict([
            ('title', feature_config.feature.title),
            ('feature_type', feature_config.feature.feature_type),])),
        # TODO: add support to optionally show QS and filters
        #('question_sets', get_question_sets(feature_config)),
        #('filters', get_filters(feature_config)),
        ('signal_set_configs', get_signal_set_configs(feature_config)),
    ])
    return OrderedDict(output)


def get_question_sets(feature_config):
    return [qs_to_dict(qs)
            for qs in feature_config.question_sets.all()]


def qs_to_dict(qs):
    output = OrderedDict([
        # TODO: add support to optionally show id
        #('id', qs.id),
        ('title', qs.title),
        ('allow_multiple', qs.allow_multiple),
        ('staff_only', qs.staff_only),
        ('class_name', qs.class_name),
        # QSD?
    ])
    return output


def get_filters(feature_config):
    return [filter_to_dict(f)
            for f in feature_config.filters.all()]


def filter_to_dict(filter_):
    output = OrderedDict([
        # TODO: add support to optionally show id
        #('id', filter_.id),
        ('title', filter_.title),
        ('filter_type', filter_.filter_type),
    ])
    return output


def get_signal_set_configs(feature_config, verbose=False):
    if verbose:
        return [OrderedDict([('order', fcssc.order),
                             ('SSC', ssc_to_dict(fcssc.signal_set_config))])
                for fcssc in (feature_config
                              .featureconfigssignalsets_set
                              .order_by('order'))]
    else:
        return OrderedDict([
            (fcssc.signal_set_config.title,
             ssc_to_dict(fcssc.signal_set_config))
            for fcssc in (feature_config
                          .featureconfigssignalsets_set
                          .order_by('order'))])


def ssc_to_dict(ssc):
    output = select_values(bool, [
        # TODO: add support to optionally show id
        ('id', ssc.id),
        ('title', ssc.title),
        ('account', repr(ssc.account) if ssc.account else None),
        ('user', repr(ssc.user) if ssc.user else None),
        ('record_set_config',
         OrderedDict([
             # TODO: add support to optionally show id
             ('id', ssc.record_set_config.id),
             ('title', ssc.record_set_config.title),])
         if ssc.record_set_config else None),
        # TODO: add support to optionally show ids of parent SSCs
        ('signal_set_configs', OrderedDict(
            sorted(ssc.signal_set_configs
                   .values_list('title', 'id')))),

        ('signal_actions', get_signal_actions(ssc)),
    ])
    return OrderedDict(output)


def get_signal_actions(ssc, verbose=False):
    if verbose:
        return [OrderedDict([('order', sacs.order),
                             ('partitioned', sacs.partitioned),
                             ('SAC', sac_to_dict(sacs.signal_action_config))])
                for sacs in ssc.signalsetsactions_set.order_by('order')]
    else:
        return OrderedDict([
            ((sacs.signal_action_config.title or
              sacs.signal_action_config.class_name),
             sac_to_dict(sacs.signal_action_config))
            for sacs in ssc.signalsetsactions_set.order_by('order')])


def sac_to_dict(sac):
    output = select_values(bool, [
        # TODO: add support to optionally show id
        ('id', sac.id),
        ('title', sac.title or None),
        ('account', repr(sac.account) if sac.account else None),
        ('user', repr(sac.user) if sac.user else None),
        ('class_name', sac.class_name),
        ('partitioned', sac.partitioned),
        ('definition', {k: v
                        for k, v in sac.definition.items()
                        if k not in ('class', 'module')}),
    ])
    return OrderedDict(output)
