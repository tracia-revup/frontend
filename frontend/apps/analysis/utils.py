import logging
import random
import textwrap

from celery.utils import uuid
from django.db import transaction, connection
from django.db.models import Min, Max

from frontend.libs.utils.celery_utils import update_progress
from frontend.libs.utils.string_utils import words


LOGGER = logging.getLogger(__name__)


def get_analysis_score_min_max(results_queryset, partition_config_ids):
    score_minmaxes = {}
    for part_id in partition_config_ids:
        score_minmaxes[str(part_id)] = results_queryset.filter(
            partition_id=part_id).aggregate(min_score=Min('score'),
                                            max_score=Max('score'))
    return score_minmaxes


def submit_analysis(user, account, contact_source=None, countdown=0,
                    flags=None, task_lock=None, add_to_parent=True,
                    force_new_analysis=False, transaction_id=None,
                    expected_count=None, queue=None):
    """Submit an analysis task to celery.

    :param countdown: may be a range to randomly select from. Format: (1, 5)
        Returns True if successfully submitted a new task.

    :param add_to_parent: is to tell celery whether or not to make this new
        task a child of the task that's calling this function. This should
        only be False if this function is being called from another task.

    :param task_lock: This function will use the user as a semaphore. If the
        user has already been locked, it may be passed-in here.

    :param transaction_id: The ID of an associated transaction in the Kafka
        pipeline. Typically, this is relevant to contact imports.
        `expected_count` below is required also.
    :param expected_count: The expected count of contacts in the transaction.
        This is REQUIRED to accompany transaction_id. Both will be ignored
        if either is missing.

    :param queue: The queue in which to send the analysis
    """
    from frontend.apps.analysis.tasks import perform_analysis_task
    from frontend.apps.authorize.models import RevUpTask
    from frontend.apps.contact.models import Contact
    from frontend.apps.contact_set.models import ContactSet

    analysis_config = user.get_default_analysis_config(account)
    # If we don't have a config, there's nothing we can do
    if not analysis_config:
        return None

    if contact_source is None:
        has_contacts = user.contact_set.filter(is_primary=True).exists()
        contact_source_cls = None
        contact_source_id = None
    elif isinstance(contact_source, Contact):
        has_contacts = True
        contact_source_cls = "Contact"
        contact_source_id = contact_source.id
    elif isinstance(contact_source, ContactSet):
        has_contacts = bool(contact_source.count)
        # This is a fail safe to make sure the count isn't falsely 0,
        # which would lock us out of analyzing this list.
        if not has_contacts:
            contact_source.update_count()
            has_contacts = bool(contact_source.count)
            if has_contacts:
                contact_source.save(update_fields=['count'])

        contact_source_cls = "ContactSet"
        contact_source_id = contact_source.id
    else:
        raise TypeError("Unknown 'contact_source' type: {}".format(
            type(contact_source)))

    if not has_contacts:
        return None

    with transaction.atomic():
        # Use RevUpUser as a semaphore. Locking a user's tasks instead
        # could cause a race condition if no tasks exist.
        if not task_lock:
            task_lock = user.lock()
        NETWORK_SEARCH = RevUpTask.TaskType.NETWORK_SEARCH
        user.clean_stale_tasks(task_type=NETWORK_SEARCH,
                               task_lock=task_lock)

        task_kwargs = dict(
            account=account, user=user, task_type=NETWORK_SEARCH)

        # Check for any tasks pending on the same contacts. If there are,
        # we revoke it and schedule a new one. This is so we can get the
        # correct 'countdown'.
        if not contact_source:
            tasks = RevUpTask.objects.filter(**task_kwargs)
        else:
            tasks = (RevUpTask.objects.filter_by_generic_obj(contact_source) |
                     RevUpTask.objects.filter_by_generic_obj(None)
                     ).filter(**task_kwargs)
        for task in tasks:
            if task.queued():
                # If the queued task is an all-contact analysis, we need to
                # make sure the new Task is also all-contact
                if task.contact_source is None:
                    contact_source = None
                    contact_source_cls = None
                    contact_source_id = None
                task.revoke()
                task.delete()

        # We store the contact_source so we can check if the same task is
        # already running
        task_rec = RevUpTask.objects.create(
            task_id=uuid(), contact_source=contact_source, **task_kwargs)

    # countdown may be a range to randomly select from
    if isinstance(countdown, (tuple, list)):
        countdown = random.randint(countdown[0], countdown[1])

    # This code needs to be outside the transaction, so we know a
    # RevUpTask instance exists before the celery task is called.
    try:
        perform_analysis_task.apply_async(
            args=(analysis_config.id, user.id, account.id),
            kwargs=dict(
                contact_source=contact_source_cls,
                contact_source_id=contact_source_id,
                force_new_analysis=force_new_analysis,
                update_progress=update_progress,
                feature_flags=flags,
                transaction_id=transaction_id,
                expected_count=expected_count),
            task_id=task_rec.task_id,
            countdown=countdown,
            queue=queue,
            add_to_parent=add_to_parent)

        log_message = "Queued analysis for user: '{} ({})' " \
                      "on account: '{} ({})' for contact-source: '{}'. " \
                      "Celery task: {}".format(
                       user, user.id, account, account.id,
                       contact_source, task_rec.task_id)
        LOGGER.info(log_message)
    except Exception:
        task_rec.delete()
        raise
    return task_rec


def ranking_counts_by_account(contact_ids):
    """This is a fancy and probably inefficient query to count the number of
    unique Results for a set of contacts, across multiple accounts.

    This is primarily meant to track deletions of Results
    """
    contact_array = f"({','.join(map(str, contact_ids))})"
    if not contact_array:
        return []

    query = textwrap.dedent(f"""
        WITH cte as (
            SELECT DISTINCT ON ("analysis_result"."contact_id", "analysis_analysis"."account_id") contact_id,
                "analysis_analysis"."account_id"
            FROM "analysis_result"
            INNER JOIN "analysis_analysis"
              ON ("analysis_result"."analysis_id" = "analysis_analysis"."id")
            WHERE "analysis_result"."contact_id" IN (SELECT U0."id"
                                                     FROM "contact_contact" U0
                                                     WHERE U0."id" IN {contact_array} AND U0."is_primary"=TRUE) 
        )
        SELECT "account_id", COUNT("contact_id") AS "ranking__count"
        FROM cte
        GROUP BY "account_id";
        """)
    with connection.cursor() as cursor:
        cursor.execute(query)
        groups = cursor.fetchall()
    return groups


def sort_with_weighted_sum(results, _query_str):
    """Sorts the items in results by calculating a new score. New score is
    the weighted sum of mongo score and a value that identifies the number of
    words matched from the _query_str in mongo record
    """
    results2 = []
    max_text_score = -1
    max_new_score = -1
    query_name = set(words(_query_str))
    # calculate new_score for every record. new_score is the number of
    # words common among the query and record
    for r in results:
        result_name = set(words(r['name']))
        text_score = r['text_score']
        new_score = len(query_name.intersection(result_name))
        results2.append((new_score, text_score, r))
        max_text_score = max(text_score, max_text_score)
        max_new_score = max(new_score, max_new_score)

    def weighted_sum(_new_score, _text_score):
        """This method normalizes new_score and text_score so that they
        are in the range [0,1]. After normalization, it calculates the
        weighted sum of new_score and text_score where new_score is given
        a higher weightage of 0.6 while the old text_score is given a
        lower weightage of 0.4
        """
        try:
            # normalized new score
            score1 = _new_score / max_new_score
        except ZeroDivisionError:
            score1 = 0
        try:
            # normalized text score
            score2 = _text_score / max_text_score
        except ZeroDivisionError:
            score2 = 0
        # give higher weightage to new score over text score
        return 0.6 * score1 + 0.4 * score2

    # Sort all of the results across collections by weighted sum of new
    # score and the text score so that the best matches from all the
    # collections are on top
    results2.sort(key=lambda x: weighted_sum(x[0], x[1]),
                  reverse=True)
    return [r[2] for r in results2]
