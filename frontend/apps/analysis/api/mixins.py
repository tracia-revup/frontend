import logging

from celery.utils import uuid
from django.db import transaction
from django.db.models import QuerySet
import funcy as f
from rest_framework.decorators import action
from rest_framework.response import Response

from frontend.apps.authorize.models import RevUpTask
from frontend.apps.batch_jobs.tasks import generate_csv_task
from frontend.libs.utils.string_utils import str_to_bool
from frontend.libs.utils.api_utils import APIException


LOGGER = logging.getLogger(__name__)


class QuerySetPickleWrapper:
    def __init__(self, queryset):
        self._queryset = queryset

    @property
    def model(self):
        qs = self._queryset
        if isinstance(qs, QuerySet):
            return qs.model
        return type(qs) # XXX: NO

    def __getstate__(self):
        qs = self._queryset
        if isinstance(qs, QuerySet):
            return {'model': qs.model,
                    'query': qs.query}
        return {'queryset': qs}

    def __setstate__(self, state):
        if f.all(state.keys(), ['model', 'query']):
            qs = state['model'].objects.all()
            qs.query = state['query']
        else:
            qs = state['queryset']
        self._queryset = qs

    def __getitem__(self, *args, **kwargs):
        return self._queryset.__getitem__(*args, **kwargs)

    def __getattr__(self, name):
        if name in ('_queryset',):
            raise RuntimeError(
                "Essential attribute '{0}' missing!".format(name))

        r = getattr(self._queryset, name, default=AttributeError)
        if r is AttributeError:
            raise AttributeError(
                "'{0}' object has no attribute '{1}'".format(
                    self.__class__.__name__, name))
        return r


class CSVExportMixin(object):
    _export_task_types = RevUpTask.TaskType

    @classmethod
    def _get_export_options(cls, request):
        show_score = str_to_bool(request.GET.get("show_score", True))
        show_name = str_to_bool(request.GET.get("show_name", True))
        show_contact_info = str_to_bool(request.GET.get("show_contact_info",
                                                        True))
        show_total = str_to_bool(request.GET.get("show_total", True))
        show_notes = str_to_bool(request.GET.get("show_notes", False))
        pretty_details = str_to_bool(request.GET.get("pretty_details", True))
        return dict(
            show_score=show_score,
            show_name=show_name,
            show_contact_info=show_contact_info,
            show_total=show_total,
            show_notes=show_notes,
            pretty_details=pretty_details)

    def _get_export_contact_set(self):
        return self.contact_set

    @classmethod
    def _get_export_partition_id(cls, contact_set):
        return contact_set.get_partition_configs(contact_set.analysis).first()

    @classmethod
    def _get_export_analysis(cls, contact_set):
        return contact_set.analysis

    def _get_export_count(self, contact_set):
        """Returns the count of the objects in the final queryset that will be
        used in the csv export
        """
        count = self._get_export_queryset(contact_set).count
        if callable(count):
            count = count()
        return count

    def _get_export_queryset(self, contact_set):
        return self.filter_queryset(self.get_queryset())

    @classmethod
    def _get_export_filename(cls, contact_set):
        # Set the file name, and ensure it's not so long that '.csv' is
        # truncated off the end.
        if contact_set.title.endswith(".csv"):
            return contact_set.title
        else:
            return contact_set.title + ".csv"

    @classmethod
    def _get_export_label(cls, contact_set):
        return "Export \"{}\"".format(contact_set.title[:118])

    @classmethod
    def _get_export_task_type(cls):
        return None

    def _get_export_revuptask(self, contact_set, task_user):
        task_type = self._get_export_task_type()
        label = self._get_export_label(contact_set)[:128]
        task_kwargs = dict(
            user=task_user,
            task_type=task_type,
            label=label,
            revoked=False,
        )
        try:
            task = RevUpTask.objects.get(**task_kwargs)
            # If this task already exists and isn't finished, we need to reject
            if task.finished():
                task.delete()
            else:
                raise APIException(LOGGER, "This task is already running")
        except RevUpTask.DoesNotExist:
            pass
        return RevUpTask.objects.create(task_id=uuid(), **task_kwargs)

    @classmethod
    def _generate_csv_task(cls, task, queryset, partition_id, contact_set,
                           user, analysis, email_to, filename, contact_count,
                           export_options):
        return generate_csv_task.apply_async(
            args=(QuerySetPickleWrapper(queryset),
                  contact_set.id, partition_id, user.id, contact_count,),
            kwargs=dict(
                analysis_id=analysis.id,
                email_to=email_to,
                filename=filename,
                **export_options),
            task_id=task.task_id,
            add_to_parent=True
        )

    @action(detail=False)
    def export(self, request, *args, **kwargs):
        email_to = [request.user.email]
        if request.impersonator:
            email_to.append(request.impersonator.email)

        contact_set = self._get_export_contact_set()
        partition_id = self._get_export_partition_id(contact_set)
        analysis = self._get_export_analysis(contact_set)
        contact_count = self._get_export_count(contact_set)
        queryset = self._get_export_queryset(contact_set)
        filename = self._get_export_filename(contact_set)

        with transaction.atomic():
            task_user = contact_set.user.lock()
            revuptask = self._get_export_revuptask(contact_set, task_user)
            self._generate_csv_task(
                revuptask,
                queryset,
                partition_id,
                contact_set,
                request.user,
                analysis,
                email_to,
                filename,
                contact_count,
                self._get_export_options(request))
        return Response()
