
import time

from django.conf import settings
from django.core.cache import cache
from rest_framework_extensions.key_constructor import bits
from rest_framework_extensions.key_constructor.constructors import (
    DefaultObjectKeyConstructor, DefaultListKeyConstructor)


class UserAnalysisResultsUpdatedAtKeyBit(bits.KeyBitBase):
    """Cache KeyBit that adds a timestamp to the key for invalidations
    """
    key_tmpl = 'user_analysis_results_updated_at_timestamp:{user_id}'

    @classmethod
    def get_key(cls, user):
        if not isinstance(user, int):
            user = user.pk
        return cls.key_tmpl.format(user_id=user)

    @classmethod
    def update_timestamp(cls, user):
        value = time.time()
        key = cls.get_key(user)
        cache.set(key, value, timeout=60*60, version=settings.PRODUCT_VERSION)
        return value

    def get_data(self, params, view_instance, view_method, request, args,
                 kwargs):
        user = view_instance.analysis.user_id
        key = self.get_key(user)
        value = cache.get(key, None, version=settings.PRODUCT_VERSION)
        if not value:
            value = self.update_timestamp(user)
        return value


class SeatPermissionsKeyBit(bits.KeyBitBase):
    """Cache KeyBit that adds seat permissions of requesting user to cache key
    """
    def get_data(self, params, view_instance, view_method, request, args,
                 kwargs):
        seat = request.user.current_seat
        if seat:
            permissions = seat.permissions
        else:
            permissions = ""
        return permissions


class IsDebugEnabledKeyBit(bits.KeyBitBase):
    """Cache KeyBit that adds whether debug mode is enabled or not to cache key
    """
    def get_data(self, params, view_instance, view_method, request, args,
                 kwargs):
        return view_instance.is_debug_enabled()


class IsDemoModeKeyBit(bits.KeyBitBase):
    """Cache KeyBit that adds whether demo mode is enabled or not to cache key
    """
    def get_data(self, params, view_instance, view_method, request, args,
                 kwargs):
        return view_instance.is_demo_mode()


class IsMobileRequestKeyBit(bits.KeyBitBase):
    """Cache KeyBit that adds whether mobile mode is enabled to cache key
    """
    def get_data(self, params, view_instance, view_method, request, args,
                 kwargs):
        return view_instance.is_mobile_request()


class ProductVersionKeyBit(bits.KeyBitBase):
    """Cache KeyBit that adds product version to cache key
    """
    def get_data(self, params, view_instance, view_method, request, args,
                 kwargs):
        return settings.PRODUCT_VERSION


class ResultKeyConstructorMixin(object):
    """Mixin with all the extra pieces of data cache key should include

    Includes:

    seat_permissions:
        If the seat's permissions change, we want to make sure the user still
        has permissions to view the cached data.

    debug_enabled:
        If debug mode is enabled, we need to invalidate the cache so we can add
        debug data. Ditto when cache is disabled.

    demo_mode:
        If demo mode is enabled, we need to invalidate the cache so we can use
        the demo data

    product_version:
        Because we're now using the stock CacheResponse class that comes with
        drf-extensions, we now need to add product version to the cache key
        since it won't be added to the key namespace.

    user_analysis_results_updated_at:
        This is used for cache invalidation. When filter indexes for contact or
        result are updated, new contacts are imported, or a prospect record is
        created for a user we need to invalidate the cache in order to reflect
        that in the data we return.
    """
    user_id = bits.UserKeyBit()
    seat_permissions = SeatPermissionsKeyBit()
    debug_enabled = IsDebugEnabledKeyBit()
    demo_mode = IsDemoModeKeyBit()
    mobile_mode = IsMobileRequestKeyBit()
    product_version = ProductVersionKeyBit()
    user_analysis_results_updated_at = UserAnalysisResultsUpdatedAtKeyBit()


class ResultObjectKeyConstructor(ResultKeyConstructorMixin,
                                 DefaultObjectKeyConstructor):
    """
    Generates cache keys for specific Results.

    The default object key constructor includes the following key bits:

    retrieve_sql_query:
        The SQL used to retrieve the object. This will include the analysis id
        and contact id/result id as well as any filters being applied
        automagically so no invalidation is required when a new analysis is
        created.

    unique_method_id:
        The class and method name used to generate the output we're caching.

    format:
        The format of the output (json by default) in case we ever support
        multiple serialization formats.
    """
    pass


class ResultListKeyConstructor(ResultKeyConstructorMixin,
                               DefaultListKeyConstructor):
    """
    Generates cache keys for lists of Results.

    The default list key constructor includes the following key bits:

    retrieve_sql_query:
        The SQL used to retrieve the result list. This will include the
        analysis id as well as any filters being applied automagically so no
        invalidation is required when a new analysis is created.

    unique_method_id:
        The class and method name used to generate the output we're caching.

    format:
        The format of the output (json by default) in case we ever support
        multiple serialization formats.

    pagination:
        The pagination parameters (page size, page number, etc).
    """
    pass
