import json
from collections import defaultdict
from datetime import datetime, timedelta
from json import loads as json_loads, JSONDecodeError
import logging
import pickle
import re

from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.db import transaction, IntegrityError
from django.db.models import Q
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect
from rest_condition.permissions import And, Or
from rest_framework import permissions, exceptions, viewsets
from rest_framework.response import Response
from rest_framework.serializers import Serializer
from rest_framework.viewsets import ReadOnlyModelViewSet, GenericViewSet
from rest_framework_extensions.cache.mixins import CacheResponseMixin
from rest_framework_extensions.mixins import (NestedViewSetMixin,
                                              DetailSerializerMixin)
from rest_framework_extensions.utils import compose_parent_pk_kwarg_name
from rest_framework.pagination import PageNumberPagination
from waffle.interface import flag_is_active_for_user

from frontend.apps.analysis.api.cache import (
    ResultObjectKeyConstructor, ResultListKeyConstructor,
    UserAnalysisResultsUpdatedAtKeyBit)
from frontend.apps.analysis.api.mixins import CSVExportMixin
from frontend.apps.analysis.api.serializers import (
    AnalysisConfigSerializer, DetailedResultSerializer, ResultSerializer,
    PartitionConfigSerializer, QuestionSetSerializer,
    QuestionSetDataSerializer, QuestionSetDataDetailedSerializer,
    DebugResultSerializer, DebugDetailedResultSerializer,
    MobileDetailedResultSerializer)
from frontend.apps.analysis.models import (
    QuestionSet, QuestionSetData, RecordSet,
    AnalysisConfig, Result, Analysis, ResultSort)
from frontend.apps.analysis.utils import (submit_analysis,
                                          sort_with_weighted_sum)
from frontend.apps.authorize.api.permissions import UserIsRequester
from frontend.apps.authorize.models import RevUpUser
from frontend.apps.call_time.models import CallTimeContact
from frontend.apps.call_time.call_sheet import CallSheet
from frontend.apps.campaign.models import (
    Prospect, ProspectCollision, Account, AccountProfile)
from frontend.apps.contact.models import Contact
from frontend.apps.contact_set.api.utils import QueryContactSetMixin
from frontend.apps.contact_set.models import ContactSet
from frontend.apps.filtering.query_engine import QueryEngine
from frontend.apps.filtering.utils import FilterSet
from frontend.apps.role.permissions import (
    FundraiserPermissions, AdminPermissions)
from frontend.apps.role.api.permissions import HasPermission
from frontend.apps.seat.api.permissions import SeatHasPermission
from frontend.apps.seat.models import Seat, DelayedAnalysis
from frontend.libs.utils.api_utils import (
    IsAdminUser, exception_watcher, ViewMethodChecker, InitialMixin,
    NameSearchMixin, PrintTemplateRenderer)
from frontend.libs.utils.string_utils import str_to_bool, ensure_bytes
from frontend.libs.utils.task_utils import WaffleFlagsMixin, ANALYSIS_FLAGS


LOGGER = logging.getLogger(__name__)


class AccountAnalysisConfigsViewSet(NestedViewSetMixin, ReadOnlyModelViewSet):
    parent_account_lookup = 'parent_lookup_account_id'
    serializer_class = AnalysisConfigSerializer
    permission_classes = (And(permissions.IsAuthenticated, IsAdminUser),)

    def get_queryset(self):
        return AnalysisConfig.objects.filter(
            account=self.kwargs.get(self.parent_account_lookup))


class QuestionSetAPIBase(NestedViewSetMixin):
    parent_account_lookup = 'parent_lookup_account_id'
    parent_analysis_config_lookup = 'parent_lookup_analysis_config_id'
    serializer_class = Serializer
    permission_classes = (
        And(permissions.IsAuthenticated,
            Or(IsAdminUser,
               HasPermission(parent_account_lookup,
                             AdminPermissions.CONFIG_FEATURES)
            )
        ),
    )

    def get_analysis_config(self):
        return get_object_or_404(
            AnalysisConfig,
            account=self.kwargs.get(self.parent_account_lookup),
            id=self.kwargs.get(self.parent_analysis_config_lookup)
        )


class QuestionSetViewSet(QuestionSetAPIBase, viewsets.ReadOnlyModelViewSet):
    def get_queryset(self):
        analysis_config = self.get_analysis_config()
        question_sets = analysis_config.get_question_sets()
        return question_sets

    def list(self, request, **kwargs):
        """Return the set of available QuestionSets for this AnalysisConfig."""
        question_sets = self.get_queryset()
        return Response(QuestionSetSerializer(question_sets, many=True).data)

    def retrieve(self, request, **kwargs):
        return redirect(
            "question_set_data_api-list",
            kwargs[self.parent_account_lookup],
            kwargs[self.parent_analysis_config_lookup], kwargs['pk'])


class QuestionSetDataViewSet(QuestionSetAPIBase, viewsets.ModelViewSet,
                             WaffleFlagsMixin):
    parent_question_set_lookup = 'parent_lookup_question_set_id'
    serializer_class = QuestionSetDataSerializer
    flag_names = ANALYSIS_FLAGS

    def get_queryset(self):
        return QuestionSetData.objects.configuration(
            question_set=self.kwargs.get(self.parent_question_set_lookup),
            analysis_config=self.kwargs.get(self.parent_analysis_config_lookup)
        ).select_related("question_set")

    def get_object(self, queryset=None):
        if queryset is None:
            return super(QuestionSetDataViewSet, self).get_object()

        # Perform the lookup filtering.
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

        assert lookup_url_kwarg in self.kwargs, (
            'Expected view %s to be called with a URL keyword argument '
            'named "%s". Fix your URL conf, or set the `.lookup_field` '
            'attribute on the view correctly.' %
            (self.__class__.__name__, lookup_url_kwarg)
        )

        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        try:
            obj = get_object_or_404(queryset, **filter_kwargs)
        except (TypeError, ValueError):
            raise Http404
        # May raise a permission denied
        self.check_object_permissions(self.request, obj)
        return obj

    def retrieve(self, request, **kwargs):
        data = self.get_object()
        return Response(QuestionSetDataDetailedSerializer(data).data)

    def list(self, request, **kwargs):
        """Return the data for a given QuestionSet."""
        data = self.get_queryset()
        return Response(QuestionSetDataSerializer(data, many=True).data)

    @exception_watcher(AttributeError, QuestionSetData.DuplicateException,
                       ValidationError, IntegrityError, ValueError)
    def create(self, request, **kwargs):
        question_set_id = self.kwargs.get(self.parent_question_set_lookup)
        question_set = get_object_or_404(QuestionSet, id=question_set_id)

        analysis_config = self.get_analysis_config()
        # Check that this question set is a member of the analysis config
        if question_set not in analysis_config.get_question_sets():
            raise Http404

        if self.get_queryset().exists() and not question_set.allow_multiple:
            raise exceptions.APIException(
                "The given question set ({}) already has data, and does "
                "not support multiple entries".format(question_set_id))

        with transaction.atomic():
            qsd = QuestionSetData.create(question_set, analysis_config,
                                         request.data)

        self._submit_account_analysis()
        return Response(QuestionSetDataSerializer(qsd).data)

    @exception_watcher(AttributeError, QuestionSetData.DuplicateException,
                       ValidationError, IntegrityError, ValueError)
    def update(self, request, *args, **kwargs):
        # We make this atomic for two reasons:
        #    1) In case of two submissions near simultaneously
        #    2) If there is an error, we don't want to deactivate the old data
        with transaction.atomic():
            queryset = self.get_queryset().select_related(
                "analysis_config").select_for_update()
            data = self.get_object(queryset=queryset)
            analysis_config = data.analysis_config

            if analysis_config.last_execution > data.version:
                qsd = QuestionSetData.create(
                    data.question_set, analysis_config, request.data)
                # Deactivate the old data so the new may take over
                self._destroy(data)
            else:
                data.update(request.data)
                qsd = data

        self._submit_account_analysis()
        return Response(QuestionSetDataSerializer(qsd).data)

    def _destroy(self, data):
        if QuestionSetData.objects.filter(
                question_set_id=data.question_set_id,
                analysis_config_id=data.analysis_config_id,
                uid=data.uid, active=False).exists():
            # A unique constraint on the fields question_set,
            # analysis_config, uid, and active means that if a QSD is
            # created, deleted (which is really just marking it as
            # inactive), and then recreated with the same configuration, an
            # attempt to "delete" that QSD (which again is just marking it
            # inactive) will fail. If we detect this condition, delete the
            # QSD for real.
            data.delete()
        else:
            data.active = False
            data.save()

    def destroy(self, request, *args, **kwargs):
        with transaction.atomic():
            queryset = self.get_queryset().select_for_update()
            data = self.get_object(queryset)
            self._destroy(data)
        self._submit_account_analysis()
        return Response({})

    @classmethod
    def _waffle_method(cls):
        return flag_is_active_for_user

    def _submit_account_analysis(self):
        """Submit an analysis for the account.

        We need to do the following:
            1) Submit analysis for current user
            2) Submit analysis for the account user
            3) Submit delayed analyses for every seat in the account
        """
        user = self.request.user
        account = Account.objects.get(
            id=self.kwargs.get(self.parent_account_lookup))
        # Don't start the analysis for 5 minutes, in case the user is making
        # more config changes.
        countdown = 60 * 5

        # Submit an analysis for this user
        flags = self._prepare_flags(user)
        submit_analysis(user, account, countdown=countdown, flags=flags)

        # Submit an analysis for the account
        user = account.account_user
        flags = self._prepare_flags(user)
        submit_analysis(user, account, countdown=countdown, flags=flags)

        # Submit delayed analyses
        eta = datetime.now() + timedelta(seconds=countdown)
        seats = list(Seat.objects.assigned().exclude(
            id=self.request.user.current_seat_id).filter(account=account))
        DelayedAnalysis.bulk_schedule(seats, None, eta=eta)


class EntitySearchAPI(NestedViewSetMixin, GenericViewSet):
    serializer_class = Serializer
    parent_account_lookup = 'parent_lookup_account_id'
    parent_analysis_config_lookup = 'parent_lookup_analysis_config_id'
    permission_classes = (
        And(permissions.IsAuthenticated,
            Or(IsAdminUser,
               HasPermission(parent_account_lookup,
                             AdminPermissions.CONFIG_FEATURES)
            )
        ),
    )
    default_fact_type = RecordSet.FactTypes.POLITICAL_ENTITY
    fact_type_map = {
        AccountProfile.AccountType.POLITICAL: RecordSet.FactTypes.POLITICAL_ENTITY,
        AccountProfile.AccountType.NONPROFIT: RecordSet.FactTypes.NONPROFIT_ENTITY,
    }

    def _get_record_sets_confs(self, anal_config):
        cache_key = "entity_search_record_sets_{}".format(anal_config.id)
        cached = cache.get(cache_key)
        if cached is not None:
            return cached
        else:
            data_conf = anal_config.data_config
            data_set_confs = data_conf.data_set_configs.all() \
                             if data_conf else []

            # Find all RecordSets of the corresponding Entity type, so we can
            # search them
            fact_type = self.fact_type_map.get(
                anal_config.account.account_profile.account_type,
                self.default_fact_type)
            record_sets_confs = []
            for data_set_conf in data_set_confs:
                record_sets_confs.extend(
                    data_set_conf.record_set_configs.select_related(
                        'record_set', 'resource_config').filter(
                        record_set__fact_type=fact_type))

            cache.set(cache_key, record_sets_confs, timeout=600)  # 10 minutes
            return record_sets_confs

    @classmethod
    def _build_query(cls, q):
        """ build the entity query from the search param."""
        try:
            q_dict = json.loads(q)
            if not isinstance(q_dict, dict):
                raise ValueError
        except (JSONDecodeError, ValueError):
            q_dict = dict(target=str(q))
        return q_dict

    def get(self, request, **kwargs):
        """Search for Entities using query parameters.

        Data Sources are provided by the DataConfig in the given
        AnalysisConfig.
        """
        q = request.GET.get("q")
        if not q:
            return Response()

        anal_config = get_object_or_404(
            AnalysisConfig.objects.select_related('account__account_profile',
                                                  'data_config'),
            pk=kwargs.get(self.parent_analysis_config_lookup),
            account=kwargs.get(self.parent_account_lookup))
        record_sets_confs = self._get_record_sets_confs(anal_config)

        results = []
        q_dict = dict()
        # Query each RecordSet with the search query.
        for record_sets_conf in record_sets_confs:
            record_set = record_sets_conf.record_set
            record_set_worker = record_set.dynamic_class(record_sets_conf,
                                                         anal_config)

            # get the query dict from the search param provided by the user
            q_dict = self._build_query(q)

            # Get all the entities matching the query from this record set
            results.extend(record_set_worker.search(**q_dict))

        results = sort_with_weighted_sum(results, q_dict.get('target', ''))
        return Response([r.get_all() for r in results])

    def create(self, request, **kwargs):
        """This method is required, but does nothing"""
        return self.get(request, **kwargs)


class AnalysisViewSet(NestedViewSetMixin, viewsets.ReadOnlyModelViewSet):
    parent_account_lookup = 'parent_lookup_account_id'
    queryset = Analysis.objects.none()


class ContactSetAnalysisResultsViewSet(
        DetailSerializerMixin, CacheResponseMixin, CSVExportMixin,
        NestedViewSetMixin, InitialMixin, NameSearchMixin,
        QueryContactSetMixin, viewsets.ReadOnlyModelViewSet, WaffleFlagsMixin):

    parent_user_lookup = compose_parent_pk_kwarg_name('user_id')
    parent_seat_lookup = compose_parent_pk_kwarg_name('seat_id')
    parent_contact_set_lookup = compose_parent_pk_kwarg_name('contact_set_id')
    serializer_class = ResultSerializer
    debug_serializer_class = DebugResultSerializer
    serializer_detail_class = DetailedResultSerializer
    mobile_serializer_detail_class = MobileDetailedResultSerializer
    debug_serializer_detail_class = DebugDetailedResultSerializer
    debug_permissions = IsAdminUser()
    object_cache_key_func = ResultObjectKeyConstructor()
    list_cache_key_func = ResultListKeyConstructor()
    name_search_prefix = "contact"
    flag_names = ANALYSIS_FLAGS

    permission_classes = (
        And(permissions.IsAuthenticated,
            UserIsRequester,
            And(ViewMethodChecker('verify_user_has_access_to_contact_set'),
                SeatHasPermission(parent_seat_lookup,
                                  FundraiserPermissions.all()))
            ),)

    class pagination_class(PageNumberPagination):
        page_size = 25
        page_size_query_param = 'page_size'
        max_page_size = 100

    basic_filters = ("location", "name", "quick")

    @classmethod
    def _waffle_method(cls):
        return flag_is_active_for_user

    def is_debug_enabled(self):
        debug_queryparam = str_to_bool(self.request.query_params.get('debug',
                                                                     False))
        debug_cookie = str_to_bool(self.request.COOKIES.get('debug', False))
        return ((debug_queryparam or debug_cookie) and
                self.debug_permissions.has_permission(self.request, self))

    def is_demo_mode(self):
        return str_to_bool(self.request.COOKIES.get('demo', False))

    def is_mobile_request(self):
        return str_to_bool(self.request.query_params.get("mobile"))

    def get_serializer_class(self):
        if self._is_request_to_detail_endpoint():
            if self.is_debug_enabled():
                klass = self.debug_serializer_detail_class
            elif self.is_mobile_request():
                klass = self.mobile_serializer_detail_class
            else:
                klass = self.serializer_detail_class
        else:
            if self.is_debug_enabled():
                klass = self.debug_serializer_class
            else:
                klass = self.serializer_class
        return klass

    def get_renderers(self):
        renderers_ = super(ContactSetAnalysisResultsViewSet,
                           self).get_renderers()
        # This is a bit of a hack to make the API able to produce an HTML
        # callsheet. The API will raise a 404 if we attempt to specify
        # a "print" format without actually having a "print" Renderer.
        if self.action == "retrieve":
            renderers_ += [PrintTemplateRenderer()]
        return renderers_

    def _get_order_by(self):
        """Process order_by queryarg value

        Supports legacy ordering format (score, -score, giving, -giving), and
        new format necessary for sorting using ResultSort table.

        The new format: `[-][<Persona.identifier>.]<ResultSort.value_type>`
        Like the legacy ordering format, the presence of a minus sign (`-`)
        indicates descending order, while its absence indicates ascending
        order.

        The persona identifier can be left out if viewing the ranking results
        for a persona contact set. In order to force sorting on a persona
        score, the persona identifier can be set.

        The last part of the order format is the value type. Valid value types
        are the members of the ResultSort.ValueTypes class. Ex: SCORE,
        NFP_GIVING, GIV_CAP, etc. (Must be all caps)

        The persona identifier and value type are separated by a period.

        If order_by is missing or an invalid value, we default to ordering by
        descending score.

        This method returns 3 arguments:
            is_desc: a bool to indicate desc or asc order
            identifier: Persona identifier or empty string
            value_type: The ResultSort.ValueTypes to order by.
        """
        order_by = self.request.GET.get("order_by", "")
        is_desc = order_by.startswith('-') or not order_by
        order_by_parts = order_by[1 if is_desc else 0:].split('.')
        if len(order_by_parts) == 2:
            identifier, value_type = order_by_parts
        else:
            value_type = order_by_parts[0]
            cs = self.contact_set
            if cs.source == ContactSet.Sources.PERSONA:
                # If the contact set being viewed is a Persona CS, grab the
                # persona identifier from that.
                identifier = cs.source_uid
            else:
                # Otherwise fall back to an empty identifier.
                identifier = ''
        # We only accept a couple kinds of ordering
        if value_type == 'giving':
            # legacy functionality
            # need to determine what kind of giving
            account_type = self.analysis.account.account_profile.account_type
            if account_type == AccountProfile.AccountType.NONPROFIT:
                value_type = ResultSort.ValueTypes.NFP_GIVING
            elif account_type == AccountProfile.AccountType.ACADEMIC:
                value_type = ResultSort.ValueTypes.CLI_GIVING
            else:
                # If it isn't NFP or academic, it is either political or new.
                # in either case we should have political, so do that for now.
                value_type = ResultSort.ValueTypes.POL_GIVING
        elif value_type == 'score' or not hasattr(ResultSort.ValueTypes,
                                                  value_type):
            value_type = 'SCORE'
        if value_type in ResultSort.ValueTypes.SINGLETON:
            # If value_type is a deduped type, then unset identifier if set.
            identifier = ''
        return is_desc, identifier, value_type

    def get_queryset(self):
        # We are going to cheat a little and replace the contact set's
        # analysis with the analysis defined in this API. It may be the same
        # analysis, or it may be a replacement.
        self.contact_set.analysis = self.analysis
        self.contact_set.analysis_id = self.analysis.id
        is_desc, identifier, value_type = self._get_order_by()
        queryset = self.contact_set.queryset(
            # We do not want the queryset method to automatically add a filter
            # by analysis on Result rows. This prevents the Postgres query
            # planner from using the composite index on ResultSort, and causes
            # poor performance.
            inject_analysis=False,
            resultsort__analysis=self.analysis,
            resultsort__partition=self.partition_id,
            resultsort__identifier=identifier,
            resultsort__value_type=value_type,
        ).order_by('{}resultsort__value'.format('-' if is_desc else ''))
        return queryset.select_related("contact")

    def filter_queryset(self, queryset):
        queryset = super(ContactSetAnalysisResultsViewSet,
                         self).filter_queryset(queryset)

        # Run basic and advanced filters
        basic_filters = self.request.query_params.getlist("basic_filter")
        adv_filters = self.request.query_params.get('q')
        if adv_filters:
            adv_filters = json_loads(adv_filters)
        else:
            # Filter format: ?filter=<filter-type>:<filter-value>
            # E.g.: ?filter=given_name:john&filter=state:CA
            adv_filters = self.request.query_params.getlist("filter")
            adv_filters = self._extract_filters(adv_filters)

        if basic_filters:
            queryset = self._prepare_basic_filters(basic_filters, queryset)
        if adv_filters:
            queryset = self._prepare_advanced_filters(adv_filters, queryset)
        return queryset

    def _get_filter_set(self):
        return FilterSet(user=self.contact_set.user,
                         analysis=self.analysis)

    def _prepare_advanced_filters(self, filters, queryset):
        """Prepare the advanced filters for use in a query.

        Filters are prepared by parsing them into filter-type and filter-value
        components. Those items are then passed off to a QueryEngine that
        generates the appropriate queries for the given filters
        """
        # Get the available filters
        filter_set = self._get_filter_set()

        engine = QueryEngine(filter_set.get_all(), Result)
        qry = engine.filter(filters, analysis=self.analysis,
                            partition=self.partition_id,
                            user=self.analysis.user)
        return queryset.filter(qry)

    def _prepare_basic_filters(self, filter_params, queryset):
        """Prepare the basic filters for use in a query.

        Filters are prepared by parsing them into filter-type and filter-value
        components. Then `Q` objects are generated from those filters.

        Valid format options should be listed in the class variable,
        `basic_filters`. For every item in 'basic_filters, it needs to have
        a corresponding method that generates a Q object. The naming
        signature for the method is _prepare_<filter-name>_filter(self, value)
        E.g. method call:
            self._prepare_location_filter("CA")

        Filter format: ?basic_filter=<filter-type>:<filter-value>
        E.g.: ?basic_filter=name:john+doe&filter=location:CA
        """
        filters = self._extract_filters(filter_params,
                                        "|".join(self.basic_filters))

        # Build the query. Every filter is expected to have a prepare
        # method that returns a Q object. E.g. "prepare_location_filter"
        query = Q()
        for filter_, values in filters.items():
            for value in values:
                method = getattr(self, "_prepare_{}_filter".format(filter_))
                query &= method(value)

        if query:
            queryset = queryset.filter(query).distinct()
        return queryset

    def _extract_filters(self, filter_params, valid_filter_pattern=".*"):
        regex = r"^({})?:(.*)?".format(valid_filter_pattern)
        filters = defaultdict(list)
        for filter_param in filter_params:
            match = re.match(regex, filter_param)
            try:
                filter_, value = match.groups()
                value = value.strip()
                filters[filter_].append(value)
            except (ValueError, AttributeError):
                raise exceptions.APIException(
                    "Ill-formed filter: {}".format(filter_param))
        return filters

    def get_serializer_context(self):
        """Add the normalize metadata to the Serializer, if it is available."""
        context = super(ContactSetAnalysisResultsViewSet,
                        self).get_serializer_context()
        # Check for demo mode
        context["demo_mode"] = self.is_demo_mode()
        context["account"] = self.seat.account
        context["feature_flags"] = self._prepare_flags(self.user)

        # If the user's permissions limit their view, check if their results
        # should be obfuscated.
        if (not self.seat.has_permissions(
                    [FundraiserPermissions.INTERNAL_DATA]) and
                self.seat.has_permissions(
                    [FundraiserPermissions.SUMMARY_DATA])):
            # We know this user's data should be obfuscated.
            context["show_full_data"] = False

            # Get the obfuscation configuration
            try:
                qs = QuestionSet.objects.get(
                    title="Information Display Options")
                analysis_config = self.analysis.analysis_config
                qsd = QuestionSetData.objects.configuration(
                    qs, analysis_config).first()
            except QuestionSet.DoesNotExist:
                qsd = None

            # Store the interval tree in the serializer. The tree contains
            # the obfuscation information. i.e. which ranges to label
            if qsd:
                tree_pickle = qsd.data.get("tree_pickle")
                if tree_pickle:
                    try:
                        interval_tree = pickle.loads(ensure_bytes(tree_pickle))
                        context["interval_tree"] = interval_tree
                    except (pickle.UnpicklingError, ValueError) as err:
                        LOGGER.error("Could not unpickle the interval tree. "
                                     "Error: {}".format(err))
        elif self.seat.has_permissions([FundraiserPermissions.INTERNAL_DATA]):
            # From now on, we will err on the side of caution in the
            # serializers. If "show_full_data" isn't true, we will always
            # obfuscate. This will ensure that we never show data to people
            # who should not see it
            context["show_full_data"] = True

        # Add the normalize metadata to the Serializer, if it is available.
        partition_id = str(self.partition_id)
        if self.specified_analysis:
            # If the user specifies an analysis, we'll try to get the correct
            # normalization information
            normalize_meta = self.analysis.meta_results.get("normalize")
            if normalize_meta:
                partition = normalize_meta.get(partition_id)
                if partition:
                    context["normalize"] = partition
        else:
            # Typically normalization comes from the contact set
            normalize_meta = self.contact_set.get_score_min_max(partition_id)
            if normalize_meta:
                context["normalize"] = normalize_meta
        return context

    def paginate_queryset(self, queryset):
        """Extend this method to include queries for Prospect collisions
        for the contacts on this page. There is no need to go beyond this
        page because this lookup is only for display purposes.

        Doing it this way means there are only 2 queries instead of '2n'
        """
        page = super(ContactSetAnalysisResultsViewSet, self).paginate_queryset(
            queryset)
        if not page:
            return page

        # Only query for things the user has permission to view
        permissions, _ = self.contact_set.user_permissions(self.user)
        # Build a data-structure we can use to check for existing Prospects
        results = {result.contact_id: result for result in page}

        if permissions.get("prospects"):
            # Get all of the prospect collisions for the results in the page
            for m, external in ((Prospect, False), (ProspectCollision, True)):
                contacts = m.objects.filter(contact__in=iter(results.keys()),
                                            user=self.user,
                                            event=self.user.get_default_event())\
                                    .values_list("id", "contact_id")

                # Store the prospect information directly in the Result
                # object it is relevant to. The serializer will pick it up.
                for prospect_id, contact_id in contacts:
                    result = results.get(contact_id)
                    if result:
                        result.prospect = {
                            "invited": True,
                            "external": external,
                            "prospect_id": prospect_id}

        if permissions.get("call_time"):
            for ct_contact_id, contact_id in \
                    CallTimeContact.objects.active().filter(
                        contact__in=iter(results.keys()),
                        account=self.seat.account_id).values_list(
                        "id", "contact_id"):
                result = results.get(contact_id)
                if result:
                    result.call_time_contact = ct_contact_id
        return page

    def _prepare_location_filter(self, filter_value):
        loc_tokens = filter_value.split(",")
        if len(loc_tokens) == 1:
            fv = filter_value
            if len(fv) == 2:
                # If there are just two characters, we assume state
                # abbreviation.
                fv = fv.upper()
                q = (Q(contact__addresses__city_key__state__abbr=fv) |
                     Q(contact__contacts__addresses__city_key__state__abbr=fv))
            else:
                # If only one word is given, we'll treat it like it could be
                # either a city or a state name
                q = (
                    (Q(contact__addresses__city_key__name__istartswith=fv) |
                     Q(contact__addresses__city_key__state__name__istartswith=fv)) |
                    (Q(contact__contacts__addresses__city_key__name__istartswith=fv) |
                     Q(contact__contacts__addresses__city_key__state__name__istartswith=fv)))
        else:
            # If there are more than one comma, we'll treat the first as the
            # city and the last as the state
            city = loc_tokens[0].strip()
            state = loc_tokens[-1].strip()
            if len(state) == 2:
                state = state.upper()
                q = (
                    (Q(contact__addresses__city_key__name__istartswith=city) &
                     Q(contact__addresses__city_key__state__abbr=state)) |
                    (Q(contact__contacts__addresses__city_key__name__istartswith=city) &
                     Q(contact__contacts__addresses__city_key__state__abbr=state)))
            else:
                q = (
                    (Q(contact__addresses__city_key__name__istartswith=city) &
                     Q(contact__addresses__city_key__state__name__istartswith=state)) |
                    (Q(contact__contacts__addresses__city_key__name__istartswith=city) &
                     Q(contact__contacts__addresses__city_key__state__name__istartswith=state)))
        return q

    def _prepare_quick_filter(self, filter_value):
        """Filter query by tags.

        This also handles simple NOT logic by prefixing a '!' to the front
        of the tag.
        """
        if filter_value.startswith("!"):
            q = ~Q(tags__contains=[filter_value.replace("!", "")])
        else:
            q = Q(tags__contains=[filter_value])
        return q

    def verify_user_has_access_to_contact_set(self, request):
        return self.contact_set.has_view_permission(request.user)

    def _get_export_task_type(self):
        return self._export_task_types.RANKING_EXPORT

    def _get_export_partition(self, contact_set):
        return get_object_or_404(
            self.analysis.partition_set.partition_configs,
            pk=self.partition_id)

    def _get_export_analysis(self, contact_set):
        return self.analysis

    def _pre_permission_initial(self):
        # queries needed before permission checks
        self.user = get_object_or_404(
            RevUpUser,
            id=self.kwargs.get(self.parent_user_lookup))

        self.seat = get_object_or_404(
            Seat.objects.assigned().select_related("account"),
            id=self.kwargs.get(self.parent_seat_lookup),
            user=self.user)

        contact_set_id = self.kwargs.get(self.parent_contact_set_lookup)
        self.contact_set = self._get_contact_set(
            contact_set_id, self.user, self.seat.account,
            # pre-fetch analysis and analysis_profile to reduce the number of
            # queries made. We need analysis_profile to figure out sorting
            # order for legacy order_by values.
            select_related='analysis__analysis_config__analysis_profile')

    def _initial(self):
        # queries to perform after permission checks
        if 'analysis' in self.request.query_params:
            analysis_id = self.request.query_params['analysis']
            self.analysis = get_object_or_404(
                Analysis, id=analysis_id,
                # The given analysis must be from the same account and user
                user_id=self.contact_set.user_id,
                account_id=self.contact_set.account_id)
            self.specified_analysis = True
        else:
            self.analysis = self.contact_set.analysis
            self.specified_analysis = False

        # Check if the analysis hasn't run yet
        if self.analysis is None:
            raise Http404
        elif self.analysis.status == Analysis.STATUSES.error:
            # TODO: Not sure what to do here. Should probably trigger an
            # analysis. I want to leave this here as a flag that we should do
            # something...
            LOGGER.warn("User: {} is attempting to view a ContactSet '{}' "
                        "with a failed analysis ({})".format(
                         self.user, self.contact_set, self.analysis.id))

        partition_id = self.request.GET.get('partition', None)
        if partition_id is None:
            partition_id = self.contact_set.get_partition_configs(
                self.analysis).order_by('-index').first()
        self.partition_id = partition_id

    def retrieve(self, request, *args, **kwargs):
        # Check for "print" requests. Print requests will return a callsheet
        renderer, _ = self.perform_content_negotiation(request, )
        if isinstance(renderer, PrintTemplateRenderer):
            result = self.get_object()
            call_sheet = CallSheet(result.contact, result, template="web")
            return Response(call_sheet.build_context(),
                            template_name=call_sheet.template)
        else:
            return super(ContactSetAnalysisResultsViewSet, self).retrieve(
                                            request, *args, **kwargs)

    @classmethod
    def invalidate_cache(cls, user):
        """Clear the list and detail view caches in the relevant namespace"""
        UserAnalysisResultsUpdatedAtKeyBit.update_timestamp(user)


class ContactSetAnalysisContactResultsViewSet(
        ContactSetAnalysisResultsViewSet):
    def get_object(self, queryset=None):
        """Allow the user to query a result with a contact ID.

        If a contact has been merged, the analysis result may actually
        point to a merge-child. We need to be able to query for both.
        """
        if not queryset:
            queryset = self.get_queryset()

        contact_id = self.kwargs.get("pk")
        try:
            contact_id = int(contact_id)
        except ValueError:
            raise Http404
        else:
            contact = get_object_or_404(Contact.objects.select_related("parent"),
                                        id=contact_id)
            return get_object_or_404(queryset,
                                     contact=contact.get_oldest_ancestor())


class PartitionConfigViewSet(NestedViewSetMixin, viewsets.ReadOnlyModelViewSet):
    parent_account_lookup = compose_parent_pk_kwarg_name('account_id')
    serializer_class = PartitionConfigSerializer
    permission_classes = (And(permissions.IsAuthenticated,
                              Or(IsAdminUser,
                                 HasPermission(parent_account_lookup,
                                               FundraiserPermissions.all()))
                              ),)

    def get_queryset(self):
        parent_kwargs = self.get_parents_query_dict()
        account_id = parent_kwargs.get('account_id')
        analysis_id = parent_kwargs.get('analysis_id')
        analysis = get_object_or_404(Analysis, account_id=account_id,
                                     pk=analysis_id)
        return analysis.partition_set.partition_configs.order_by('-index').all()
