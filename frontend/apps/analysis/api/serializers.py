import random

from django.conf import settings
from rest_framework import serializers

from frontend.apps.analysis.models import (
    Analysis, AnalysisConfig, Result, PartitionConfig, PartitionSetConfig,
    QuestionSet, QuestionSetData)
from frontend.apps.contact.api.serializers import (
    DetailedContactSerializer, ContactParentSerializer)


class AnalysisSerializer(serializers.ModelSerializer):
    class Meta:
        model = Analysis


class AnalysisConfigSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnalysisConfig
        fields = ('id', 'title', "last_execution")


class ResultSerializerBase(serializers.ModelSerializer):
    tags = serializers.ReadOnlyField()
    score = serializers.SerializerMethodField()
    key_signals = serializers.SerializerMethodField()

    class Meta:
        model = Result
        fields = ("id", "contact", "key_signals", "tags", "score", "giving")
        key_signals_exclude = ('gender', 'given_name_origin',
                               'family_name_ethnicity',
                               'gender_boost_multiplier',
                               'ethnicity_boost_multiplier', 'pctsouthasian')

    def __init__(self, *args, **kwargs):
        super(ResultSerializerBase, self).__init__(*args, **kwargs)
        self.obfuscated = False

    def to_representation(self, instance):
        data = super(ResultSerializerBase, self).to_representation(instance)
        # In some cases, we need to obfuscate giving data. We need to prepare
        # that here
        if instance.giving is not None:
            label, self.obfuscated = self._obfuscate_giving(instance.giving)
            data["key_signals"]["giving"] = label
            data["giving"] = label
        return data

    def get_score(self, result):
        score = result.score
        normalize_meta = self.context.get('normalize')
        if normalize_meta:
            min_score = normalize_meta.get('min_score')
            max_score = normalize_meta.get('max_score')
            if not (min_score is None or max_score is None):
                score = result._normalize_score(score, min_score, max_score)
        return score

    def get_key_signals(self, result):
        key_signals = result.key_signals
        for key in self.Meta.key_signals_exclude:
            if key in key_signals:
                del key_signals[key]

        if settings.INCLUDE_MOCK_DATA:
            persona_dict = dict(score=random.random()*100)
            for key in ("DG", "WI", "PI", "AI"):
                persona_dict[key] = dict(
                    score=random.random()*100,
                    weight=random.random()*100,
                )
            key_signals["persona"] = persona_dict

        return key_signals

    def _obfuscate_giving(self, giving):
        """Replace the giving with a label in certain cases."""
        # If the context specifically tells us to return the unobfuscated
        # giving, then we will do so. Otherwise, we will always obfuscate,
        # for security/privacy reasons.
        show_full_data = self.context.get("show_full_data", False)
        if show_full_data:
            return giving, False

        label = ""
        obfuscated = True
        # If the interval tree is in the context, we can search for the
        # range matching the giving. Otherwise, we return an empty string.
        # The empty string is to err on the side of caution. We will always
        # obfuscate unless told otherwise by the view.
        if "interval_tree" in self.context:
            interval_tree = self.context["interval_tree"]
            intervals = interval_tree.search(giving)
            if intervals:
                # If we get more than one interval, we'll always use the first.
                # It should be impossible to have more than one, unless there
                # is an issue somewhere else
                interval = intervals.pop()
                label = interval[2]
                # 'label' will be None if an interval is configured to show
                # the real values. In that case, we will show the real value
                if label is None:
                    label = giving
                    obfuscated = False
        return label, obfuscated


class ResultSerializer(ResultSerializerBase):
    contact = ContactParentSerializer()
    prospect = serializers.SerializerMethodField()
    call_time_contact = serializers.SerializerMethodField()

    class Meta(ResultSerializerBase.Meta):
        fields = ResultSerializerBase.Meta.fields + (
            "prospect", "call_time_contact")

    def get_prospect(self, result):
        try:
            return result.prospect
        except AttributeError:
            return {"invited": False}

    def get_call_time_contact(self, result):
        try:
            return result.call_time_contact
        except AttributeError:
            return None


class DebugResultSerializer(ResultSerializer):
    class Meta(ResultSerializer.Meta):
        key_signals_exclude = ()


class DetailedResultSerializer(ResultSerializerBase):
    contact = DetailedContactSerializer()
    features = serializers.SerializerMethodField()

    class Meta(ResultSerializerBase.Meta):
        fields = ResultSerializerBase.Meta.fields + ("features",)

    def get_user_agent(self):
        request = self.context.get("request")
        if request is None:
            return None
        return request.META.get('HTTP_USER_AGENT')

    def get_features(self, result, include_features=None, group_by_feature=True):
        show_full_data = not self.obfuscated
        demo_mode = self.context.get("demo_mode", False)
        feature_flags = self.context.get("feature_flags", None)
        features = result.get_features(
            include_features=include_features, show_full_data=show_full_data,
            demo_mode=demo_mode, feature_flags=feature_flags)

        # This is a terrible hack to temporarily fix mobile
        # Mobile sends this user agent: RevUp/295 CFNetwork/902.2 Darwin/17.7.0
        user_agent = self.get_user_agent()
        user_agent = user_agent.lower() if user_agent else None
        if user_agent and \
                all((i in user_agent for i in ("revup", "cfnetwork", "darwin"))):
            group_by_feature = False

        # Groups by feature config
        if not group_by_feature:
            return {title: signals for title, _, signals in features}

        # Groups by feature title
        else:
            return {feature_title: {title: signals}
                    for title, feature_title, signals in features
                    if feature_title}


class MobileDetailedResultSerializer(DetailedResultSerializer):
    def get_features(self, result):
        include_features = ["Client Matches", "Key Contributions"]

        try:
            app_version = int(self.context.get(
                'request').query_params.get("app_version"))
        except (ValueError, TypeError):
            app_version = 1

        # Newer app_versions group by feature title
        if app_version >= 4:
            return super(MobileDetailedResultSerializer, self).get_features(
                result,
                include_features=include_features,
                group_by_feature=True
            )

        #  Older app_versions group by feature configs
        else:
            return super(MobileDetailedResultSerializer, self).get_features(
                result,
                include_features=include_features,
                group_by_feature=False
            )


class DebugDetailedResultSerializer(DetailedResultSerializer):
    feature_result_map = serializers.SerializerMethodField()

    class Meta(DetailedResultSerializer.Meta):
        fields = ("id", "contact", "key_signals", "features", "tags", "score",
                  "feature_result_map")
        key_signals_exclude = ()

    def get_feature_result_map(self, result, include_features_regex=None):
        return dict(result.features.values_list('feature_config__title', 'pk'))


class PartitionConfigSerializer(serializers.ModelSerializer):
    years_of_giving = serializers.IntegerField()

    class Meta:
        model = PartitionConfig
        fields = ('id', 'index', 'label', 'years_of_giving')


class PartitionSetConfigSerializer(serializers.ModelSerializer):
    partition_configs = PartitionConfigSerializer(many=True, read_only=True)

    class Meta:
        model = PartitionSetConfig
        fields = ("id", "label", "partition_configs")


class QuestionSetSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuestionSet
        fields = ('id', 'title', 'description', 'allow_multiple')


class QuestionSetDataSerializer(serializers.ModelSerializer):
    data = serializers.SerializerMethodField()

    class Meta:
        model = QuestionSetData
        fields = ('id', 'question_set', 'version', 'data')

    def get_data(self, instance):
        return instance.get_shallow_data()


class QuestionSetDataDetailedSerializer(QuestionSetDataSerializer):
    def get_data(self, instance):
        return instance.get_deep_data()


class BasicContactSetAnalysisSerializer(serializers.ModelSerializer):
    analysis_profile = serializers.SerializerMethodField()

    class Meta:
        model = Analysis
        fields = ("id", "modified", "analysis_profile")

    def get_analysis_profile(self, analysis):
        return analysis.analysis_config.analysis_profile_id


class ContactSetAnalysisSerializer(BasicContactSetAnalysisSerializer):
    partition_set = PartitionSetConfigSerializer(read_only=True)

    class Meta:
        model = Analysis
        fields = ("id", "modified", "partition_set", "analysis_profile")
