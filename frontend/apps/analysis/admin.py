from django import forms
from django.contrib import admin, messages
from django.contrib.admin import helpers
from django.contrib.admin.utils import model_ngettext, get_deleted_objects
from django.core.exceptions import PermissionDenied
from django.db import router
from django.template.response import TemplateResponse
from django.utils.encoding import force_text
from django.utils.translation import ugettext_lazy
from frontend.apps.authorize.models import RevUpUser
from frontend.apps.campaign.models import Account
from frontend.libs.utils.admin_utils import (ChoiceLimitAdminForm,
                                             ChoiceLimitAdminInlineFormSet,
                                             generate_ownership_qs_filter)

from .models import (ResourceInstance, ResourceConfig, RecordSetConfig,
                     DataSetConfig, DataConfig, SignalActionConfig,
                     SignalSetsActions, SignalSetConfig, FeatureConfig,
                     PartitionSetConfig, PartitionConfig, AnalysisConfig,
                     Feature, AnalysisProfile, QuestionSet, QuestionSetData,
                     AnalysisConfigTemplate, FeatureConfigsSignalSets,
                     RecordResource, RecordSet, DataSet)


class AnalysisAdmin(admin.ModelAdmin):
    date_hierarchy = 'modified'
    readonly_fields = ('created', 'modified', 'created_by',
                       'created_with_session_key', 'modified_by',
                       'modified_with_session_key')
    copy_selected_confirmation_template = None
    delete_selected_template = None

    search_fields = ["title"]

    pre_list_display = ['id', 'name']
    post_list_display = ['created', 'modified',
                         'created_by', 'modified_by']
    pre_fieldsets = [
        (None, {
            'fields': ('title', 'description')
        }),
    ]
    post_fieldsets = [
        ('Time/Auth Stamps', {
            'classes': ('collapse',),
            'fields': ('created', 'modified', 'created_by',
                       'created_with_session_key', 'modified_by',
                       'modified_with_session_key')
        }),
    ]
    pre_list_display_links = ['name']
    pre_list_filter = []

    def __new__(cls, *args, **kwargs):
        cls.fieldsets = cls.pre_fieldsets[:]
        cls.fieldsets.extend(getattr(cls, 'additional_fieldsets', []))
        cls.fieldsets.extend(getattr(cls, 'owner_fieldsets', []))
        cls.fieldsets.extend(cls.post_fieldsets)

        cls.list_display = cls.pre_list_display[:]
        cls.list_display.extend(getattr(cls,
                                        'additional_list_display', []))
        cls.list_display.extend(getattr(cls,
                                        'owner_list_display', []))
        cls.list_display.extend(cls.post_list_display)

        cls.actions = cls.base_actions[:]
        cls.actions.extend(getattr(cls, 'additional_actions', []))
        if getattr(cls, 'additional_display_links', []):
            cls.list_display_links = getattr(cls, 'additional_display_links')

        cls.list_display_links = cls.pre_list_display_links[:]

        cls.list_filter = cls.pre_list_filter[:]
        cls.list_filter.extend(getattr(cls, 'additional_list_filter', []))
        cls.list_filter.extend(getattr(cls, 'owner_list_filter', []))
        cls.search_fields.extend(getattr(cls, "additional_search_fields", []))

        return super(AnalysisAdmin, cls).__new__(cls)

    class CopyForm(forms.Form):

        new_label = forms.CharField(
            required=False,
            label='Label to append to copied items',
            widget=forms.TextInput(
                attrs={'placeholder': '"copy" is the default',}))
        new_account = forms.ModelChoiceField(
            required=False,
            label='Owning account (if any) for copied items',
            queryset=Account.objects.all())
        new_user = forms.ModelChoiceField(
            required=False,
            label='Owning user (if any) for copied items',
            queryset=RevUpUser.objects.all())

    def copy_selected(self, request, queryset, deep=False):
        opts = self.model._meta
        app_label = opts.app_label
        form = None

        if 'copy' in request.POST:
            form = self.CopyForm(request.POST)

            if form.is_valid():
                cleaned_data = form.cleaned_data

                n = queryset.count()
                if n:
                    for item in queryset.all():
                        if deep:
                            item.deep_copy(**cleaned_data)
                        else:
                            item.copy(**cleaned_data)
                    self.message_user(
                        request, "Successfully copied %(count)d %(items)s." % {
                            "count": n, "items":
                            model_ngettext(self.opts, n)
                        }, messages.SUCCESS)
                # Return None to display the change list page again.
                return None

        if not form:
            form = self.CopyForm()

        if len(queryset) == 1:
            objects_name = force_text(opts.verbose_name)
        else:
            objects_name = force_text(opts.verbose_name_plural)

        title = 'Copy items?'

        context = dict(
            self.admin_site.each_context(request),
            title=title,
            objects_name=objects_name,
            queryset=queryset,
            opts=opts,
            copy_form=form,
            action_checkbox_name=helpers.ACTION_CHECKBOX_NAME,
            deep=deep,
        )

        # Display the confirmation page
        return TemplateResponse(
            request=request, template=self.copy_selected_confirmation_template or [
                "admin/%s/%s/copy_selected.html" %
                (app_label, opts.model_name),
                "admin/%s/copy_selected.html" % app_label,
                "admin/copy_selected.html"
            ], context=context)
    copy_selected.short_description = ugettext_lazy(
        "Shallow copy selected %(verbose_name_plural)s")

    def deep_copy_selected(self, request, queryset):
        return self.copy_selected(request, queryset, deep=True)
    deep_copy_selected.short_description = ugettext_lazy(
        "Deep copy selected %(verbose_name_plural)s and dependent items")

    def delete_selected_including_orphans(self, request, queryset):
        opts = self.model._meta
        app_label = opts.app_label

        # Check that the user has delete permission for the actual model
        if not self.has_delete_permission(request):
            raise PermissionDenied

        using = router.db_for_write(self.model)

        # Populate deletable_objects, a data structure of all related objects
        # that will also be deleted.
        deletable_objects, perms_needed, protected = get_deleted_objects(
            queryset, opts, request.user, self.admin_site, using)

        # The user has already confirmed the deletion.
        # Do the deletion and return a None to display the change list view
        # again.
        if request.POST.get('post'):
            if perms_needed:
                raise PermissionDenied
            n = queryset.count()
            if n:
                for obj in queryset:
                    obj_display = force_text(obj)
                    self.log_deletion(request, obj, obj_display)
                for item in queryset.all():
                    item.delete_including_orphans()
                self.message_user(
                    request, "Successfully deleted %(count)d %(items)s." % {
                        "count": n, "items": model_ngettext(self.opts, n)
                    }, messages.SUCCESS)
            # Return None to display the change list page again.
            return None

        if len(queryset) == 1:
            objects_name = force_text(opts.verbose_name)
        else:
            objects_name = force_text(opts.verbose_name_plural)

        if perms_needed or protected:
            title = "Cannot delete %(name)s" % {"name": objects_name}
        else:
            title = "Are you sure?"

        context = dict(
            self.admin_site.each_context(request),
            title=title,
            objects_name=objects_name,
            deletable_objects=[deletable_objects],
            queryset=queryset,
            perms_lacking=perms_needed,
            protected=protected,
            opts=opts,
            action_checkbox_name=helpers.ACTION_CHECKBOX_NAME,
        )

        # Display the confirmation page
        return TemplateResponse(
            request=request, template=self.delete_selected_template or [
                "admin/%s/%s/delete_selected.html" %
                (app_label, opts.model_name),
                "admin/%s/delete_selected.html" % app_label,
                "admin/delete_selected.html"
            ], context=context)

    base_actions = [copy_selected]


class AccountListFilter(admin.SimpleListFilter):
    title = 'account'
    parameter_name = 'account_id__exact'

    def lookups(self, request, model_admin):
        qs = model_admin.get_queryset(request)
        account_ids = qs.filter(account__isnull=False).values_list(
            'account_id', flat=True)
        accounts = Account.objects.filter(id__in=account_ids)
        accounts = [
            (account.id, account.__str__()) for account in accounts]
        accounts.append(('NONE', '(None)'))
        return accounts

    def queryset(self, request, queryset):
        if self.value():
            if self.value() == 'NONE':
                return queryset.filter(account__isnull=True)
            else:
                return queryset.filter(account=self.value())
        else:
            return queryset


class UserListFilter(admin.SimpleListFilter):
    title = 'user'
    parameter_name = 'user_id__exact'

    def lookups(self, request, model_admin):
        qs = model_admin.get_queryset(request)
        user_ids = qs.filter(user__isnull=False).values_list(
            'user_id', flat=True)
        users = RevUpUser.objects.filter(id__in=user_ids)
        users = [
            (user.id, user.__str__()) for user in users]
        users.append(('NONE', '(None)'))
        return users

    def queryset(self, request, queryset):
        if self.value():
            if self.value() == 'NONE':
                return queryset.filter(user__isnull=True)
            else:
                return queryset.filter(user=self.value())
        else:
            return queryset


class AnalysisAdminOwned(AnalysisAdmin):

    owner_list_display = ['account', 'user', ]
    owner_fieldsets = [
        (None, {
            'fields': ('account', 'user')
        }),
    ]
    owner_list_filter = [AccountListFilter, UserListFilter]
    raw_id_fields = ("user",)


@admin.register(ResourceInstance)
class ResourceInstanceAdmin(AnalysisAdminOwned):
    additional_fieldsets = [
        (None, {
            'fields': (
                'record_resource', 'identifier')
        }),
    ]
    additional_list_display = ['identifier']

    class form(ChoiceLimitAdminForm):
        choice_limit_filter_map = {
            'record_resource': generate_ownership_qs_filter,
        }


@admin.register(ResourceConfig)
class ResourceConfigAdmin(AnalysisAdminOwned):
    additional_fieldsets = [
        (None, {
            'fields': (
                'record_resource', 'resource_instance')
        }),
    ]
    additional_actions = [
        AnalysisAdmin.deep_copy_selected,
        AnalysisAdmin.delete_selected_including_orphans,
    ]

    class form(ChoiceLimitAdminForm):
        choice_limit_filter_map = {
            'record_resource': generate_ownership_qs_filter,
            'resource_instance': generate_ownership_qs_filter,
        }


@admin.register(RecordResource)
class RecordResourceAdmin(AnalysisAdminOwned):
    additional_fieldsets = [
        (None, {
            'fields': (
                'module_name',
                'class_name',
                'record_set',
            )
        }),
    ]
    additional_actions = [
        AnalysisAdmin.deep_copy_selected,
        AnalysisAdmin.delete_selected_including_orphans,
    ]

    class form(ChoiceLimitAdminForm):
        choice_limit_filter_map = {
            'record_set': generate_ownership_qs_filter,
        }


@admin.register(RecordSet)
class RecordSetAdmin(AnalysisAdminOwned):
    additional_fieldsets = [
        (None, {
            'fields': (
                'module_name',
                'class_name',
                'fact_type',
                'data_set',
            )
        }),
    ]
    additional_actions = [
        AnalysisAdmin.deep_copy_selected,
        AnalysisAdmin.delete_selected_including_orphans,
    ]

    class form(ChoiceLimitAdminForm):
        choice_limit_filter_map = {
            'data_set': generate_ownership_qs_filter,
        }


@admin.register(RecordSetConfig)
class RecordSetConfigAdmin(AnalysisAdminOwned):
    additional_fieldsets = [
        (None, {
            'fields': (
                'record_set', 'resource_config')
        }),
    ]
    additional_actions = [
        AnalysisAdmin.deep_copy_selected,
        AnalysisAdmin.delete_selected_including_orphans,
    ]

    class form(ChoiceLimitAdminForm):
        choice_limit_filter_map = {
            'record_set': generate_ownership_qs_filter,
            'resource_config': generate_ownership_qs_filter,
        }


@admin.register(DataSet)
class DataSetAdmin(AnalysisAdminOwned):
    additional_fieldsets = [
        (None, {
            'fields': (
                'module_name',
                'class_name',
            )
        }),
    ]


@admin.register(DataSetConfig)
class DataSetConfigAdmin(AnalysisAdminOwned):
    additional_fieldsets = [
        (None, {
            'fields': (
                'data_set', 'record_set_configs')
        }),
    ]
    additional_actions = [
        AnalysisAdmin.deep_copy_selected,
        AnalysisAdmin.delete_selected_including_orphans,
    ]

    class form(ChoiceLimitAdminForm):
        choice_limit_filter_map = {
            'data_set': generate_ownership_qs_filter,
            'record_set_configs': generate_ownership_qs_filter,
        }


@admin.register(DataConfig)
class DataConfigAdmin(AnalysisAdminOwned):
    additional_fieldsets = [
        (None, {
            'fields': (
                'data_set_configs',
            )
        }),
    ]
    additional_actions = [
        AnalysisAdmin.delete_selected_including_orphans,
    ]

    class form(ChoiceLimitAdminForm):
        choice_limit_filter_map = {
            'data_set_configs': generate_ownership_qs_filter,
        }


@admin.register(SignalActionConfig)
class SignalActionConfigAdmin(AnalysisAdminOwned):
    additional_fieldsets = [
        (None, {
            'fields': (
                'module_name',
                'class_name',
                'definition',
                'partitioned',
            )
        }),
    ]
    additional_list_filter = ['signalsetconfig']

    def signalsetsactions(self, obj):
        return obj.signalsetsactions_set.all()

    def signalsetconfig(self, obj):
        return obj.signalsetconfig__set.all()


class SignalSetsActionsInLine(admin.TabularInline):
    model = SignalSetsActions
    extra = 1
    formset = ChoiceLimitAdminInlineFormSet

    class form(ChoiceLimitAdminForm):
        choice_limit_filter_map = {
            'signal_action_config': generate_ownership_qs_filter,
        }

        def get_parent_instance(self):
            if self.instance.signal_set_config_id:
                return self.instance.signal_set_config


@admin.register(SignalSetConfig)
class SignalSetConfigAdmin(AnalysisAdminOwned):
    inlines = (SignalSetsActionsInLine,)
    additional_fieldsets = [
        (None, {
            'fields': (
                'record_set_config',
                'signal_set_configs',
                'spectrum_config',
            )
        }),
    ]

    class form(ChoiceLimitAdminForm):
        choice_limit_filter_map = {
            'record_set_config': generate_ownership_qs_filter,
            'signal_set_configs': generate_ownership_qs_filter,
            'spectrum_config': generate_ownership_qs_filter,
        }


class FeatureConfigsSignalSetsInLine(admin.TabularInline):
    model = FeatureConfigsSignalSets
    extra = 1
    formset = ChoiceLimitAdminInlineFormSet

    class form(ChoiceLimitAdminForm):
        choice_limit_filter_map = {
            'signal_set_config': generate_ownership_qs_filter,
        }

        def get_parent_instance(self):
            if self.instance.feature_config_id:
                return self.instance.feature_config


@admin.register(FeatureConfig)
class FeatureConfigAdmin(AnalysisAdminOwned):
    inlines = (FeatureConfigsSignalSetsInLine,)
    additional_fieldsets = [
        (None, {
            'fields': (
                'feature',
                'question_sets',
                'filters',
                'partitioned',
                'icon',
                'icon_name',
                'text',
                'hidden',
                'legacy_alias',
                'module_name',
                'class_name',
            )
        }),
    ]
    additional_actions = [
        AnalysisAdmin.deep_copy_selected,
    ]

    class form(ChoiceLimitAdminForm):
        choice_limit_filter_map = {
            'question_sets': generate_ownership_qs_filter,
        }


@admin.register(PartitionSetConfig)
class PartitionSetConfigAdmin(AnalysisAdminOwned):
    additional_fieldsets = [
        (None, {
            'fields': (
                'label',
                'partition_configs',
            )
        }),
    ]

    class form(ChoiceLimitAdminForm):
        choice_limit_filter_map = {
            'partition_configs': generate_ownership_qs_filter,
        }


@admin.register(PartitionConfig)
class PartitionConfigAdmin(AnalysisAdminOwned):
    post_list_display = ["index"]
    additional_fieldsets = [
        (None, {
            'fields': (
                'index',
                'label',
                'definition',
            )
        }),
    ]


@admin.register(AnalysisConfig)
class AnalysisConfigAdmin(AnalysisAdminOwned):
    post_list_display = ['last_execution', 'created', 'modified']
    additional_list_display = ['analysis_config_template']
    additional_list_filter = ['analysis_config_template']
    additional_fieldsets = [
        (None, {
            'fields': (
                'analysis_profile',
                'data_config',
                'partition_set_config',
                'feature_configs',
            )
        }),
    ]
    class CopyForm(AnalysisAdmin.CopyForm):
        clone_question_set_data = forms.BooleanField(
            label='Clone Question Set Data to copy?',
        )

    class form(ChoiceLimitAdminForm):
        choice_limit_filter_map = {
            'data_config': generate_ownership_qs_filter,
            'partition_set_config': generate_ownership_qs_filter,
            'feature_configs': generate_ownership_qs_filter,
        }


@admin.register(AnalysisConfigTemplate)
class AnalysisConfigTemplateAdmin(AnalysisAdmin):
    additional_fieldsets = [
        (None, {
            'fields': (
                'analysis_profile',
                'data_config',
                'partition_set_config',
                'feature_configs',
            )
        }),
    ]

    class GenerateConfigForm(forms.Form):

        new_label = forms.CharField(
            required=False,
            label='Analysis Config Title',
            help_text="If left empty, template title will be used",
            widget=forms.TextInput(
                attrs={'style': "width: 250px;"}))
        new_account = forms.ModelChoiceField(
            required=True,
            label='Owning account for generated config',
            queryset=Account.objects.all())

    def generate_analysis_config(self, request, queryset):
        opts = self.model._meta
        app_label = opts.app_label
        form = None

        if 'copy' in request.POST:
            form = self.GenerateConfigForm(request.POST)

            if form.is_valid():
                title = form.cleaned_data['new_label']
                account = form.cleaned_data['new_account']

                n = queryset.count()
                if n:
                    for item in queryset.all():
                        item.generate_analysis_config(
                            account=account, title=title or item.title)
                    self.message_user(
                        request, "Successfully copied {count} {items}.".format(
                            count=n, items=model_ngettext(self.opts, n)),
                        messages.SUCCESS)
                # Return None to display the change list page again.
                return None

        if not form:
            form = self.GenerateConfigForm()

        if len(queryset) == 1:
            objects_name = force_text(opts.verbose_name)
        else:
            objects_name = force_text(opts.verbose_name_plural)

        title = 'Generate configs?'

        context = dict(
            self.admin_site.each_context(request),
            title=title,
            objects_name=objects_name,
            queryset=queryset,
            opts=opts,
            copy_form=form,
            action_checkbox_name=helpers.ACTION_CHECKBOX_NAME,
        )

        # Display the confirmation page
        return TemplateResponse(
            request=request, template=self.copy_selected_confirmation_template or [
                "admin/%s/%s/copy_selected.html" %
                (app_label, opts.model_name),
                "admin/%s/copy_selected.html" % app_label,
                "admin/copy_selected.html"
            ], context=context)

    additional_actions = [generate_analysis_config]


@admin.register(Feature)
class FeatureAdmin(AnalysisAdmin):
    additional_list_display = ['feature_type',]
    additional_fieldsets = [
        (None, {
            'fields': (
                'feature_type',
            )
        }),
    ]


@admin.register(AnalysisProfile)
class AnalysisProfileAdmin(AnalysisAdmin):
    additional_fieldsets = [
        (None, {
            'fields': (
                'analysis_type',
                'default_config_template',
                'features',
                'analysis_view_setting_set',
            )
        }),
    ]


@admin.register(QuestionSet)
class QuestionSetAdmin(AnalysisAdminOwned):
    additional_list_display = ["display_order", "label",]
    additional_fieldsets = [
        (None, {
            'fields': (
                'display_order',
                'label',
                'module_name',
                'class_name',
                'allow_multiple',
                'staff_only',
                'is_global',
                'icon'
            )
        }),
    ]


@admin.register(QuestionSetData)
class QuestionSetDataAdmin(admin.ModelAdmin):
    list_display = ('id', "question_set", "analysis_config")
    readonly_fields = ('created', 'modified', 'version')
