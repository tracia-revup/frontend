# -*- coding: utf-8 -*-


from django.conf import settings
from django.db import models, migrations


# def configure(apps, schema_editor):
#     from frontend.apps.analysis.scripts.migrations import migrate_key_contribs
#     if settings.DEBUG:
#         migrate_key_contribs.configure(apps=apps)


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0035_add_trans_date_to_key_contrib'),
    ]

    operations = [
        # This data migration was intended to setup the algorithm for scoring
        # key contributions. However, during testing on staging, we found
        # this implementation to be inadequate. Since there are several
        # migrations now dependent on this one, I can't just remove it.
        # Instead, we are making it a noop migration for anyone who has not
        # run these migration yet (like production)
        # migrations.RunPython(configure, lambda x, y: True)
    ]
