# -*- coding: utf-8 -*-


from django.db import models, migrations


def update_client_reducesignals(apps, schema_editor):
    SignalActionConfig = apps.get_model("analysis", "SignalActionConfig")

    # Update Client ReduceSignals to determine year of last contribution
    crs = SignalActionConfig.objects.get(title="Client ReduceSignals")
    max_fields = set(crs.definition.get("max_fields", []))
    max_fields.add("year_of_last_contribution")
    crs.definition["max_fields"] = max_fields
    crs.save()


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0020_add_key_contributions_config'),
    ]

    operations = [
        migrations.RunPython(update_client_reducesignals,
                             lambda x, y: True)
    ]
