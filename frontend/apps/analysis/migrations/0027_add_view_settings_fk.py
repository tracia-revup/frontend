# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0026_add_municipal_entities'),
        ('authorize', '0019_analysis_view_settings'),
    ]

    operations = [
        migrations.AddField(
            model_name='analysisprofile',
            name='analysis_view_setting_set',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, blank=True, to='authorize.AnalysisViewSettingSet', null=True),
            preserve_default=True,
        ),
    ]
