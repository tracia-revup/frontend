# -*- coding: utf-8 -*-


from django.db import models, migrations
import datetime


def setup(apps, schema_editor):
    PartitionConfig = apps.get_model("analysis", "PartitionConfig")
    PartitionSetConfig = apps.get_model("analysis", "PartitionSetConfig")
    partition_set = PartitionSetConfig.objects.create(
        title='New Base Partition Set',
        description='Partitions including dates since start of 2000 through 2014 election cycles',
        label='Giving History Since {} Election Cycle')

    ensure_schema(apps)

    cycles = [(2000, datetime.date(1998, 11, 3)),
              (2002, datetime.date(2000, 11, 7)),
              (2004, datetime.date(2002, 11, 5)),
              (2006, datetime.date(2004, 11, 2)),
              (2008, datetime.date(2006, 11, 7)),
              (2010, datetime.date(2008, 11, 4)),
              (2012, datetime.date(2010, 11, 2)),
              (2014, datetime.date(2012, 11, 6))]

    for i, (year,start_date) in enumerate(reversed(cycles)):
        index = i + 1
        title = "%s Election Cycle" % year
        description = "Dates since start of %s election cycle." % year
        definition = {
            "type": "start_date",
            "start_date": start_date
        }
        pc = PartitionConfig.objects.create(
            title=title,
            description=description,
            index=index, label=str(year),
            definition=definition)
        partition_set.partition_configs.add(pc)


def ensure_schema(apps):
    try:
        NetworkResult = apps.get_model("analysis", "NetworkResult")
        NetworkResult.ensure_indexes()
    except LookupError as e:
        if str(e).lower() == "app 'analysis' doesn't have a 'networkresult' model.":
            # NetworkResult model has probably been completely deprecated, so
            # do not fail.
            pass
        else:
            raise


def cleanup(apps, schema_editor):
    PartitionConfig = apps.get_model("analysis", "PartitionConfig")
    PartitionSetConfig = apps.get_model("analysis", "PartitionSetConfig")
    partition_set = PartitionSetConfig.objects.filter(
        title='New Base Partition Set').first()

    for partconfig in partition_set.partition_configs.all():
        partition_set.partition_configs.remove(partconfig)
        partconfig.delete()

    partition_set.delete()



class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0006_add_municipal_data'),
    ]

    operations = [
        migrations.RunPython(setup, cleanup),
    ]
