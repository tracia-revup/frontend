# Generated by Django 2.0.5 on 2019-02-22 17:16

from django.db import migrations


def forward(apps, schema_editor):
    DataSetConfig = apps.get_model('analysis', 'DataSetConfig')
    RecordSet = apps.get_model('analysis', 'RecordSet')
    RecordResource = apps.get_model('analysis', 'RecordResource')
    ResourceInstance = apps.get_model('analysis', 'ResourceInstance')
    ResourceConfig = apps.get_model('analysis', 'ResourceConfig')
    RecordSetConfig = apps.get_model('analysis', 'RecordSetConfig')

    dsc = DataSetConfig.objects.get(
        title='Non-profit Entity Data Configuration')
    ds = dsc.data_set

    rs, _ = RecordSet.objects.get_or_create(
        title='NonProfit Organization Entities',
        module_name='frontend.apps.analysis.sources.base',
        class_name='RecordSet',
        fact_type="NPE",
        data_set=ds)
    rr, _ = RecordResource.objects.get_or_create(
        title='MongoDB NonProfit Organization Entities',
        module_name='frontend.apps.analysis.sources.non_profit.backends.mongodbv3',
        class_name='NonProfitEntities',
        record_set=rs)
    ri, _ = ResourceInstance.objects.get_or_create(
        title='MongoDB NonProfit Organization Entities "non_profit_orgs" collection',
        identifier='non_profit_orgs',
        record_resource=rr)
    rc, _ = ResourceConfig.objects.get_or_create(
        title='NonProfit Organization Entities - MongoDB Configuration',
        record_resource=rr,
        resource_instance=ri)
    rsc, _ = RecordSetConfig.objects.get_or_create(
        title='NonProfit Organization Entities Configuration',
        record_set=rs,
        resource_config=rc)
    dsc.record_set_configs.add(rsc)


def backward(apps, schema_editor):
    DataSetConfig = apps.get_model('analysis', 'DataSetConfig')
    RecordSetConfig = apps.get_model('analysis', 'RecordSetConfig')

    dsc = DataSetConfig.objects.get(
        title='Non-profit Entity Data Configuration')
    rsc = RecordSetConfig.objects.get(
        title='NonProfit Organization Entities Configuration')
    rc = rsc.resource_config
    ri = rc.resource_instance
    rr = rc.record_resource
    rs = rsc.record_set

    dsc.record_set_configs.remove(rsc)

    rsc.delete()
    rc.delete()
    ri.delete()
    rr.delete()
    rs.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0095_more_fact_types'),
    ]

    operations = [
        migrations.RunPython(forward, backward)
    ]
