# -*- coding: utf-8 -*-


from django.db import models, migrations


def change_legacy_profiles_to_dummy(apps, schema_editor):
    AnalysisProfile = apps.get_model('analysis', 'AnalysisProfile')
    analysis_profiles = AnalysisProfile.objects.filter(analysis_type='LEGACY')
    for ap in analysis_profiles.iterator():
        ap.title = ap.title.replace('legacy', 'dummy')
        ap.analysis_type = 'DUMMY'
        ap.save()


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0049_new_nonprofit_config'),
    ]

    operations = [
        migrations.AlterField(
            model_name='analysisprofile',
            name='analysis_type',
            field=models.CharField(blank=True, max_length=16, choices=[('LEGACY', 'Legacy Individual Contribution Ranking'), ('PN_CONTRIB_RANK', 'Individual Contribution Ranking'), ('DUMMY', 'No-op dummy runner')]),
            preserve_default=True,
        ),
        migrations.RunPython(change_legacy_profiles_to_dummy,
                             lambda x, y: True),
        migrations.AlterField(
            model_name='analysisprofile',
            name='analysis_type',
            field=models.CharField(blank=True, max_length=16, choices=[('PN_CONTRIB_RANK', 'Individual Contribution Ranking'), ('DUMMY', 'No-op dummy runner')]),
            preserve_default=True,
        ),
    ]
