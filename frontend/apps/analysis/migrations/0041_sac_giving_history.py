# -*- coding: utf-8 -*-


from django.db import models, migrations


def setup(apps, schema_editor):
    from frontend.apps.analysis.analyses import signal_actions
    from frontend.libs.utils.model_utils import DynamicClassModelMixin
    SignalSetConfig = apps.get_model('analysis', 'SignalSetConfig')
    SignalActionConfig = apps.get_model('analysis', 'SignalActionConfig')
    SignalSetsActions = apps.get_model('analysis', 'SignalSetsActions')

    dc = DynamicClassModelMixin()
    dc.dynamic_class = signal_actions.GivingHistory
    try:
        sac_giving_history = SignalActionConfig.objects.get(
            title='Giving History')
    except SignalActionConfig.DoesNotExist:
        sac_giving_history = SignalActionConfig.objects.create(
            title='Giving History',
            definition={
                # Start year is inclusive (i.e. 2011 is returned)
                'start_year': 2011,
                'end_year': 2015,
                'class': dc.class_name,
                'module': dc.module_name
            }
        )

    ssc_purdue_giving = SignalSetConfig.objects.get(title='Purdue Giving')

    # The Giving History SignalAction should run before PartitionSignals
    SignalSetsActions.objects.get_or_create(
        signal_set_config=ssc_purdue_giving,
        signal_action_config=sac_giving_history,
        order=25
    )


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0040_academic_analysis_profile'),
    ]

    operations = [
        migrations.RunPython(setup,
                             lambda x, y: True),
    ]
