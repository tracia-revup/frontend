# -*- coding: utf-8 -*-

import datetime

from django.db import models, migrations
import django.utils.timezone
import audit_log.models.fields
import jsonfield.fields
import django.db.models.deletion
from django.conf import settings
import django_extensions.db.fields


def setup_questionset(apps, schema_config):
    from frontend.apps.analysis.questionset_controllers import \
        KeyContributions
    from frontend.libs.utils.model_utils import DynamicClassModelMixin
    QuestionSet = apps.get_model("analysis", "QuestionSet")
    Feature = apps.get_model("analysis", "Feature")
    FeatureConfig = apps.get_model("analysis", "FeatureConfig")
    AnalysisConfig = apps.get_model("analysis", "AnalysisConfig")

    # Use the Mixin model to parse the class/module name. Models generated
    # through the apps variable don't seem to have all the methods.
    dc = DynamicClassModelMixin()
    dc.dynamic_class = KeyContributions

    # Create the new QuestionSet
    entity_dmrc, _ = QuestionSet.objects.get_or_create(
        title="Key Contributions", class_name=dc.class_name,
        module_name=dc.module_name, allow_multiple=True,
        icon="tracker")

    # Create the new Feature Config
    feature = Feature.objects.get(title="Matching signal subsets")
    feature_conf, _ = FeatureConfig.objects.get_or_create(
        title="Key Contributions", feature=feature, partitioned=False)
    # Add the question set
    feature_conf.question_sets.add(entity_dmrc)

    # Add the Key Contributions feature to the relevant analysis configs
    candidate = AnalysisConfig.objects.get(
        title="Default analysis config for political candidate campaign")
    comittee = AnalysisConfig.objects.get(
        title="Default analysis config for political committee campaign")
    candidate.feature_configs.add(feature_conf)
    comittee.feature_configs.add(feature_conf)


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('campaign', '0019_account_analysis_link'),
        ('analysis', '0013_copy_result_featureresult_m2m_relation_to_fk'),
    ]

    operations = [
        migrations.CreateModel(
            name='QuestionSet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('icon', models.CharField(max_length=255, blank=True)),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('module_name', models.CharField(max_length=256, blank=True)),
                ('class_name', models.CharField(max_length=256, blank=True)),
                ('account', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='campaign.Account', null=True)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_analysis_questionset_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('modified_by', audit_log.models.fields.LastUserField(related_name='modified_analysis_questionset_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by')),
                ('user', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('allow_multiple', models.BooleanField(default=False)),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='QuestionSetData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('active', models.BooleanField(default=True)),
                ('version', models.DateTimeField()),
                ('data', jsonfield.fields.JSONField(default={}, blank=True)),
                ('uid', models.CharField(max_length=256, blank=True)),
                ('analysis_config', models.ForeignKey(related_name='+', to='analysis.AnalysisConfig', on_delete=django.db.models.deletion.CASCADE)),
                ('question_set', models.ForeignKey(related_name='+', to='analysis.QuestionSet', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='questionsetdata',
            unique_together=set(
                [('question_set', 'analysis_config', 'active', 'uid')]),
        ),
        migrations.AddField(
            model_name='analysisconfig',
            name='last_execution',
            field=models.DateTimeField(blank=True, default=datetime.datetime.now),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='featureconfig',
            name='question_sets',
            field=models.ManyToManyField(related_name='feature_configs', null=True, to='analysis.QuestionSet', blank=True),
            preserve_default=True,
        ),
        migrations.RunPython(
            setup_questionset, lambda x, y: True
        ),






    ]
