# -*- coding: utf-8 -*-


from django.db import models, migrations


def add_purdue_sources(apps, schema_editor):
    DataSet = apps.get_model('analysis', 'DataSet')
    RecordSet = apps.get_model('analysis', 'RecordSet')
    RecordResource = apps.get_model('analysis', 'RecordResource')
    ResourceInstance = apps.get_model('analysis', 'ResourceInstance')
    ResourceConfig = apps.get_model('analysis', 'ResourceConfig')
    RecordSetConfig = apps.get_model('analysis', 'RecordSetConfig')
    DataSetConfig = apps.get_model('analysis', 'DataSetConfig')

    def create_source(name, collection_name, purdue_ds):
        purdue_rs, _ = RecordSet.objects.get_or_create(
            title='Purdue {} RecordSet'.format(name),
            module_name='frontend.apps.analysis.sources',
            class_name='RecordSet',
            fact_type='CNT',
            data_set=purdue_ds)
        purdue_rr, _ = RecordResource.objects.get_or_create(
            title='MongoDB Purdue {} RecordResource'.format(name),
            module_name='frontend.apps.analysis.sources.purdue.backends.mongodb',
            class_name='Purdue{}RecordResource'.format(name),
            record_set=purdue_rs)
        purdue_ri, _ = ResourceInstance.objects.get_or_create(
            title='MongoDB Purdue {} ResourceInstance "{}" collection'.format(
                name, collection_name),
            identifier=collection_name,
            record_resource=purdue_rr)
        purdue_rc, _ = ResourceConfig.objects.get_or_create(
            title='Purdue {} ResourceConfig - MongoDB Configuration'.format(
                name),
            record_resource=purdue_rr,
            resource_instance=purdue_ri)
        purdue_rsc, _ = RecordSetConfig.objects.get_or_create(
            title='Purdue {} RecordSetConfig'.format(name),
            record_set=purdue_rs,
            resource_config=purdue_rc)
        return purdue_rsc


    purdue_ds, _ = DataSet.objects.get_or_create(
        title='Purdue Data',
        description='Parent data set for Purdue record sets',
        module_name='frontend.apps.analysis.sources.purdue.purdue',
        class_name='Purdue')

    purdue_dsc, _ = DataSetConfig.objects.get_or_create(
        title='Purdue Data Configuration',
        data_set=purdue_ds)

    purdue_dsc.record_set_configs.add(
        create_source('Bio', 'purdue_bio', purdue_ds))
    # We need a special class for the Bio RecordSet
    from frontend.libs.utils.model_utils import DynamicClassModelMixin
    from frontend.apps.analysis.sources.purdue.purdue import PurdueBioRecordSet
    dc = DynamicClassModelMixin()
    dc.dynamic_class = PurdueBioRecordSet
    purdue_bio_rs = RecordSet.objects.get(title='Purdue Bio RecordSet')
    purdue_bio_rs.class_name = dc.class_name
    purdue_bio_rs.module_name = dc.module_name
    purdue_bio_rs.save()

    purdue_dsc.record_set_configs.add(
        create_source('Edu', 'purdue_edu', purdue_ds))

    purdue_dsc.record_set_configs.add(
        create_source('Activity', 'purdue_activity', purdue_ds))

    purdue_dsc.record_set_configs.add(
        create_source('Giving', 'purdue_giving', purdue_ds))

    purdue_dsc.record_set_configs.add(
        create_source('PastAddress', 'purdue_past_address', purdue_ds))


def add_purdue_features(apps, schema_editor):
    from frontend.apps.analysis.analyses import signal_actions
    from frontend.libs.utils.model_utils import DynamicClassModelMixin
    SignalSetConfig = apps.get_model('analysis', 'SignalSetConfig')
    SignalActionConfig = apps.get_model('analysis', 'SignalActionConfig')
    SignalSetsActions = apps.get_model('analysis', 'SignalSetsActions')
    Feature = apps.get_model('analysis', 'Feature')
    FeatureConfig = apps.get_model('analysis', 'FeatureConfig')
    FeatureConfigsSignalSets = apps.get_model('analysis', 'FeatureConfigsSignalSets')
    RecordSetConfig = apps.get_model('analysis', 'RecordSetConfig')

    prime_feature = Feature.objects.get(feature_type='PRIME')
    root_feature = Feature.objects.get(feature_type='MATCH')
    fc_purdue_giving = FeatureConfig.objects.create(
        title='Purdue Giving',
        partitioned=True, feature=root_feature)
    fc_purdue_alum = FeatureConfig.objects.create(
        title='Purdue Alum Lookup',
        partitioned=False, feature=prime_feature)

    rsc_purdue_bio = RecordSetConfig.objects.get(
        title='Purdue Bio RecordSetConfig')
    rsc_purdue_activity = RecordSetConfig.objects.get(
        title='Purdue Activity RecordSetConfig')
    rsc_purdue_giving = RecordSetConfig.objects.get(
        title='Purdue Giving RecordSetConfig')

    sac_record_search = SignalActionConfig.objects.get(title='RecordSearch')
    sac_partition_signals = SignalActionConfig.objects.get(title='PartitionSignals')

    dc = DynamicClassModelMixin()

    dc.dynamic_class = signal_actions.DateTransform
    sac_purdue_datetransform = SignalActionConfig.objects.create(
        title='Purdue DateTransform',
        definition={
            'field': 'date',
            'epoch_time': False,
            'class': dc.class_name,
            'module': dc.module_name
        }
    )

    dc.dynamic_class = signal_actions.ResetPipelineResults
    sac_reset_pipeline = SignalActionConfig.objects.create(
        title='Reset Pipeline Result',
        definition={
            'class': dc.class_name,
            'module': dc.module_name
        }
    )

    dc.dynamic_class = signal_actions.PurdueUpdateContact
    sac_purdue_contact_update = SignalActionConfig.objects.create(
        title='Purdue Contact Update',
        definition={
            'class': dc.class_name,
            'module': dc.module_name
        }
    )

    dc.dynamic_class = signal_actions.PurdueAlumFilter
    sac_purdue_alum_filter = SignalActionConfig.objects.create(
        title='Purdue Alum Filter',
        definition={
            'class': dc.class_name,
            'module': dc.module_name
        }
    )

    dc.dynamic_class = signal_actions.PurdueLookupLatLong
    sac_purdue_lookup_latlong = SignalActionConfig.objects.create(
        title='Purdue Lookup LatLong',
        definition={
            'class': dc.class_name,
            'module': dc.module_name
        }
    )

    dc.dynamic_class = signal_actions.PurdueLookupPastAddresses
    sac_purdue_lookup_past_addresses = SignalActionConfig.objects.create(
        title='Purdue Lookup Past Addresses',
        definition={
            'class': dc.class_name,
            'module': dc.module_name
        }
    )

    dc.dynamic_class = signal_actions.ReduceSignals
    sac_purdue_reduce_signals = SignalActionConfig.objects.create(
        title='Purdue ReduceSignals',
        definition={
            'list_fields': ['match'],
            'class': dc.class_name,
            'module': dc.module_name
        }
    )

    ssc_purdue_bio = SignalSetConfig.objects.create(
        title='Purdue Bio', record_set_config=rsc_purdue_bio)
    FeatureConfigsSignalSets.objects.create(
        feature_config=fc_purdue_alum,
        signal_set_config=ssc_purdue_bio,
        order=10
    )
    SignalSetsActions.objects.create(
        signal_set_config=ssc_purdue_bio,
        signal_action_config=sac_record_search,
        order=10
    )
    SignalSetsActions.objects.create(
        signal_set_config=ssc_purdue_bio,
        signal_action_config=sac_purdue_lookup_past_addresses,
        order=20
    )
    SignalSetsActions.objects.create(
        signal_set_config=ssc_purdue_bio,
        signal_action_config=sac_purdue_lookup_latlong,
        order=30
    )
    SignalSetsActions.objects.create(
        signal_set_config=ssc_purdue_bio,
        signal_action_config=sac_purdue_alum_filter,
        order=40
    )
    SignalSetsActions.objects.create(
        signal_set_config=ssc_purdue_bio,
        signal_action_config=sac_purdue_reduce_signals,
        order=50
    )
    SignalSetsActions.objects.create(
        signal_set_config=ssc_purdue_bio,
        signal_action_config=sac_purdue_contact_update,
        order=60
    )

    ssc_purdue_activities = SignalSetConfig.objects.create(
        title='Purdue Activities', record_set_config=rsc_purdue_activity)
    FeatureConfigsSignalSets.objects.create(
        feature_config=fc_purdue_alum,
        signal_set_config=ssc_purdue_activities,
        order=20
    )
    SignalSetsActions.objects.create(
        signal_set_config=ssc_purdue_activities,
        signal_action_config=sac_reset_pipeline,
        order=1
    )
    SignalSetsActions.objects.create(
        signal_set_config=ssc_purdue_activities,
        signal_action_config=sac_record_search,
        order=10
    )
    SignalSetsActions.objects.create(
        signal_set_config=ssc_purdue_activities,
        signal_action_config=sac_purdue_reduce_signals,
        order=20
    )

    ssc_purdue_giving = SignalSetConfig.objects.create(
        title='Purdue Giving', record_set_config=rsc_purdue_giving)
    FeatureConfigsSignalSets.objects.create(
        feature_config=fc_purdue_giving,
        signal_set_config=ssc_purdue_giving,
        order=10
    )
    SignalSetsActions.objects.create(
        signal_set_config=ssc_purdue_giving,
        signal_action_config=sac_record_search,
        order=10
    )
    SignalSetsActions.objects.create(
        signal_set_config=ssc_purdue_giving,
        signal_action_config=sac_purdue_datetransform,
        order=20
    )
    SignalSetsActions.objects.create(
        signal_set_config=ssc_purdue_giving,
        signal_action_config=sac_partition_signals,
        partitioned=True,
        order=30
    )
    SignalSetsActions.objects.create(
        signal_set_config=ssc_purdue_giving,
        signal_action_config=sac_purdue_reduce_signals,
        partitioned=True,
        order=40
    )


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0037_order_signal_set_configs'),
    ]

    operations = [
        migrations.RunPython(add_purdue_sources,
                             lambda x, y: True),
        migrations.RunPython(add_purdue_features,
                             lambda x, y: True),
    ]
