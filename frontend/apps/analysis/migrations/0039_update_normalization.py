# -*- coding: utf-8 -*-


from django.db import models, migrations
import jsonfield.fields


def setup(apps, schema_config):
    Feature = apps.get_model('analysis', 'Feature')
    meta, _ = Feature.objects.get_or_create(
        title='Meta features', feature_type='META',
        description="Meta analysis features. I.e. Features that"
                    " analyze the analysis.")

    # Convert the Normalize FeatureConfig to the "meta" Feature type
    FeatureConfig = apps.get_model('analysis', 'FeatureConfig')
    normalize_fc = FeatureConfig.objects.get(title="Normalization signals")
    normalize_fc.feature = meta
    normalize_fc.save()

    SignalSetConfig = apps.get_model('analysis', 'SignalSetConfig')
    SignalActionConfig = apps.get_model('analysis', 'SignalActionConfig')
    normalize_ssc = SignalSetConfig.objects.get(title="Normalization signals")

    # Get or create the new Normalize SignalActionConfig
    try:
        normalize_sac = SignalActionConfig.objects.get(
            title="Normalize Min & Max")
    except SignalActionConfig.DoesNotExist:
        normalize_sac = SignalActionConfig.objects.create(
            title="Normalize Min & Max",
            definition={
                "class": "NormalizeMinMax",
                "module": "frontend.apps.analysis.analyses.signal_actions"
            })
    # Replace the normalize signal set action with the new one.
    assert normalize_ssc.signalsetsactions_set.count() == 1
    ssa = normalize_ssc.signalsetsactions_set.first()
    ssa.signal_action_config = normalize_sac
    ssa.save()


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0038_purdue_analysis_config'),
    ]

    operations = [
        migrations.AddField(
            model_name='analysis',
            name='meta_results',
            field=jsonfield.fields.JSONField(default={}, help_text='Contains the results from the meta analysis features. A meta analysis is an analysis on the rest of the analysis', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='feature',
            name='feature_type',
            field=models.CharField(blank=True, max_length=16, choices=[('LOOKUP', 'Field lookups'), ('SPECTRUM', 'Spectrum values'), ('MATCH', 'Matching facts from data sets'), ('MATCH_SUB', 'Subsets of matching facts'), ('SUM', 'Summary features'), ('NORM', 'Feature normalization'), ('PRIME', 'Features that need to be run first'), ('META', 'Meta analysis')]),
            preserve_default=True,
        ),
        migrations.RunPython(setup, lambda x, y: True)
    ]
