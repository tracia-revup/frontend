# -*- coding: utf-8 -*-


import os
from django.core.management import call_command
from django.db import models, migrations
import django.contrib.postgres.fields


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0030_create_account_config_questionsets'),
    ]

    operations = [
        migrations.CreateModel(
            name='CensusCBSA',
            fields=[
                ('cbsa_code', models.IntegerField(serialize=False, primary_key=True)),
                ('cbsa_title', models.CharField(max_length=256, null=True, blank=True)),
                ('zip_3_codes', django.contrib.postgres.fields.ArrayField(models.CharField(max_length=3, null=True, blank=True), null=True, size=None)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LinkedInLocation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=256, null=True, blank=True)),
                ('country_code', models.CharField(max_length=3, null=True, blank=True)),
                ('census_cbsa', models.ForeignKey(blank=True, to='analysis.CensusCBSA', null=True, on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
