# -*- coding: utf-8 -*-


from django.db import models, migrations


def set_fact_type(apps, schema_config):
    RecordSet = apps.get_model("analysis", "RecordSet")
    RecordSet.objects.filter(
        class_name__icontains="contrib").update(fact_type="CNT")
    RecordSet.objects.exclude(fact_type="CNT").update(fact_type="ENT")


def create_record_sets(apps, schema_config):
    DataSet = apps.get_model('analysis', 'DataSet')
    RecordSet = apps.get_model('analysis', 'RecordSet')
    RecordResource = apps.get_model('analysis', 'RecordResource')
    ResourceInstance = apps.get_model('analysis', 'ResourceInstance')
    ResourceConfig = apps.get_model('analysis', 'ResourceConfig')
    RecordSetConfig = apps.get_model('analysis', 'RecordSetConfig')
    DataSetConfig = apps.get_model('analysis', 'DataSetConfig')

    def fec(fec_ds, fec_ds_conf):
        # Create FEC Committee configs
        if not RecordSet.objects.filter(title='FEC Committees').exists():
            fec_com = RecordSet.objects.create(
                title='FEC Committees',
                module_name='frontend.apps.analysis.sources.base',
                class_name='RecordSet',
                fact_type="ENT",
                data_set=fec_ds)
            fec_com_mongo = RecordResource.objects.create(
                title='MongoDB FEC Committees',
                module_name='frontend.apps.analysis.sources.fec.backends.mongodb',
                class_name='FECCommittees',
                record_set=fec_com)
            fec_com_mongo_instance = ResourceInstance.objects.create(
                title='MongoDB FEC Committees "cm" collection',
                identifier='cm',
                record_resource=fec_com_mongo)
            fec_com_mongo_conf = ResourceConfig.objects.create(
                title='FEC Committees - MongoDB Configuration',
                record_resource=fec_com_mongo,
                resource_instance=fec_com_mongo_instance)
            fec_com_conf = RecordSetConfig.objects.create(
                title='FEC Committees Configuration',
                record_set=fec_com,
                resource_config=fec_com_mongo_conf)
            fec_ds_conf.record_set_configs.add(fec_com_conf)

        # Create FEC Candidate configs
        if not RecordSet.objects.filter(title='FEC Candidates').exists():
            fec_can = RecordSet.objects.create(
                title='FEC Candidates',
                module_name='frontend.apps.analysis.sources.base',
                class_name='RecordSet',
                fact_type="ENT",
                data_set=fec_ds)
            fec_can_mongo = RecordResource.objects.create(
                title='MongoDB FEC Candidates',
                module_name='frontend.apps.analysis.sources.fec.backends.mongodb',
                class_name='FECCandidates',
                record_set=fec_can)
            fec_can_mongo_instance = ResourceInstance.objects.create(
                title='MongoDB FEC Candidates "cn" collection',
                identifier='cn',
                record_resource=fec_can_mongo)
            fec_can_mongo_conf = ResourceConfig.objects.create(
                title='FEC Candidates - MongoDB Configuration',
                record_resource=fec_can_mongo,
                resource_instance=fec_can_mongo_instance)
            fec_can_conf = RecordSetConfig.objects.create(
                title='FEC Candidates Configuration',
                record_set=fec_can,
                resource_config=fec_can_mongo_conf)
            fec_ds_conf.record_set_configs.add(fec_can_conf)

    try:
        fec_ds = DataSet.objects.get(title='FEC Data')
        fec_ds_conf = DataSetConfig.objects.get(
            title='FEC Configuration', data_set=fec_ds)
    except Exception:
        pass
    else:
        fec(fec_ds, fec_ds_conf)

    # Create MISP Entity Configs
    def misp(misp_ds, misp_ds_conf):
        if not RecordSet.objects.filter(title='State Election Entities').exists():
            misp_ent = RecordSet.objects.create(
                title='State Election Entities',
                module_name='frontend.apps.analysis.sources.base',
                class_name='RecordSet',
                fact_type="ENT",
                data_set=misp_ds)
            misp_ent_mongo = RecordResource.objects.create(
                title='MongoDB State Election Entities',
                module_name='frontend.apps.analysis.sources.misp.backends.mongodb',
                class_name='StateEntities',
                record_set=misp_ent)
            misp_ent_mongo_instance = ResourceInstance.objects.create(
                title='MongoDB State Election Entities "st_entities" collection',
                identifier='st_entities',
                record_resource=misp_ent_mongo)
            misp_ent_mongo_conf = ResourceConfig.objects.create(
                title='State Election Entities - MongoDB Configuration',
                record_resource=misp_ent_mongo,
                resource_instance=misp_ent_mongo_instance)
            misp_ent_conf = RecordSetConfig.objects.create(
                title='State Election Entities Configuration',
                record_set=misp_ent,
                resource_config=misp_ent_mongo_conf)
            misp_ds_conf.record_set_configs.add(misp_ent_conf)

    try:
        misp_ds = DataSet.objects.get(title='State Election Data')
        misp_ds_conf = DataSetConfig.objects.get(
            title='State Election Data Configuration', data_set=misp_ds)
    except Exception:
        pass
    else:
        misp(misp_ds, misp_ds_conf)



class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0014_auto_20150818_1048'),
    ]

    operations = [
        migrations.AddField(
            model_name='recordset',
            name='fact_type',
            field=models.CharField(default=None, max_length=3, null=True, choices=[('CNT', 'Contributions'), ('ENT', 'Entities')]),
            preserve_default=True,
        ),
        migrations.RunPython(
            set_fact_type, lambda x, y: True
        ),
        migrations.RunPython(
            create_record_sets, lambda x, y: True
        )
    ]
