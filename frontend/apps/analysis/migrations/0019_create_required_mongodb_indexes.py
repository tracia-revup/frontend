# -*- coding: utf-8 -*-


from os import environ
from django.db import models, migrations

from frontend.apps.analysis.scripts.migrations.create_entities import run


def create_entity_indexes(apps, schema_editor):
    if not environ.get('CI', False):
        run()


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0018_analysisconfigtemplate_pt3'),
    ]

    operations = [
        migrations.RunPython(create_entity_indexes,
                             reverse_code=lambda x, y: True)
    ]
