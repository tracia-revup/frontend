# -*- coding: utf-8 -*-


from django.db import models, migrations


def create_signal_actions(apps, schema_editor):
    SignalActionConfig = apps.get_model("analysis", "SignalActionConfig")

    giving, _ = SignalActionConfig.objects.get_or_create(
        title="Client Calculate Giving",
        partitioned=False,
        definition={
            "class": "ClientCalculateGiving",
            "module": "frontend.apps.analysis.analyses.person_contrib",
            # Default to House
            "office": "H",
            "state": None}
    )
    availability, _ = SignalActionConfig.objects.get_or_create(
        title="Client Calculate Giving Availability",
        partitioned=False,
        definition={
            "class": "ClientCalculateGivingAvailability",
            "module": "frontend.apps.analysis.analyses.person_contrib"}
    )

    SignalSetConfig = apps.get_model("analysis", "SignalSetConfig")
    SignalSetsActions = apps.get_model("analysis", "SignalSetsActions")
    try:
        client_contributions = SignalSetConfig.objects.get(
            title="Client contributions template")
        SignalSetsActions.objects.get_or_create(
            signal_set_config=client_contributions,
            signal_action_config=giving,
            partitioned=False,
            order=50
        )
        SignalSetsActions.objects.get_or_create(
            signal_set_config=client_contributions,
            signal_action_config=availability,
            partitioned=False,
            order=300
        )
    except SignalSetConfig.DoesNotExist:
        pass

    # Update Client ReduceSignals to process giving values
    try:
        crs = SignalActionConfig.objects.get(title="Client ReduceSignals")
        sum_fields = set(crs.definition.get("sum_fields", []))
        sum_fields.update(("giving", "giving_this_period"))
        crs.definition["sum_fields"] = sum_fields
        crs.save()
    except SignalActionConfig.DoesNotExist:
        pass


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0008_result_score'),
    ]

    operations = [
        migrations.RunPython(create_signal_actions,
                             lambda x, y: True)
    ]
