from django.db import migrations


def setup(apps, schema_config):
    DataSet = apps.get_model("analysis", "DataSet")
    RecordSet = apps.get_model("analysis", "RecordSet")
    RecordResource = apps.get_model("analysis", "RecordResource")
    ResourceInstance = apps.get_model("analysis", "ResourceInstance")
    ResourceConfig = apps.get_model("analysis", "ResourceConfig")
    RecordSetConfig = apps.get_model("analysis", "RecordSetConfig")
    DataSetConfig = apps.get_model("analysis", "DataSetConfig")

    municipal_ds = DataSet.objects.create(
        title='Municipal Contribution Data',
        description='Default system-wide municipal contribution data (initial'
                    ' configuration)',
        module_name='frontend.apps.analysis.sources.municipal.municipal',
        class_name='Municipal')
    municipal_contrib = RecordSet.objects.create(
        title='Municipal Individual Contributions',
        module_name='frontend.apps.analysis.sources.municipal.municipal',
        class_name='Contributions',
        data_set=municipal_ds)
    municipal_contrib_mongo = RecordResource.objects.create(
        title='MongoDB Municipal Individual Contributions',
        module_name='frontend.apps.analysis.sources.municipal.backends.mongodb',
        class_name='Contributions',
        record_set=municipal_contrib)
    municipal_contrib_mongo_instance = ResourceInstance.objects.create(
        title='MongoDB Municipal Individual Contributions '
              '"municipal" collection',
        identifier='municipal_contributions',
        record_resource=municipal_contrib_mongo)
    municipal_contrib_mongo_conf = ResourceConfig.objects.create(
        title='Municipal Individual Contributions - MongoDB '
              'Configuration',
        record_resource=municipal_contrib_mongo,
        resource_instance=municipal_contrib_mongo_instance)
    municipal_contrib_conf = RecordSetConfig.objects.create(
        title='Municipal Individual Contributions Configuration',
        record_set=municipal_contrib,
        resource_config=municipal_contrib_mongo_conf)
    municipal_ds_conf = DataSetConfig.objects.create(
        title='Municipal Data Configuration',
        data_set=municipal_ds)
    municipal_ds_conf.record_set_configs.add(municipal_contrib_conf)
    municipal_ds.save()


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0005_drop_old_collections'),
    ]

    operations = [
        migrations.RunPython(setup),
    ]
