# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0011_add_signalsetsactions_unique_constraint'),
    ]

    operations = [
        migrations.AlterField(
            model_name='result',
            name='features',
            field=models.ManyToManyField(related_name='m2mresults', db_table='analysis_result_features', to='analysis.FeatureResult'),
            preserve_default=True,
        ),
        migrations.RenameField(
            model_name='result',
            old_name='features',
            new_name='m2mfeatures',
        ),
        migrations.AddField(
            model_name='featureresult',
            name='result',
            field=models.ForeignKey(related_name='features', blank=True, to='analysis.Result', null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
    ]
