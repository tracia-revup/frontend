# -*- coding: utf-8 -*-


from django.db import models, migrations


def set_ordering_values(apps, schema_editor):
    """Set the ordering of existing QuestionSets."""
    QuestionSet = apps.get_model("analysis", "QuestionSet")
    orderings = [
        ("Personal Information", 10),
        ("Campaign Information", 30),
        ("Committee Information", 30),
        ("Committee Focus", 40),
        ("Education", 50),
        ("Key Contributions", 70),
        ("Issues", 90),
    ]

    for title, order in orderings:
        try:
            qs = QuestionSet.objects.get(title=title)
            qs.display_order = order
            qs.save()
        except QuestionSet.DoesNotExist:
            continue


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0030_create_account_config_questionsets'),
    ]

    operations = [
        migrations.AddField(
            model_name='questionset',
            name='display_order',
            field=models.IntegerField(default=100, help_text='Ordered from lowest to highest'),
            preserve_default=True,
        ),
        migrations.RunPython(
            set_ordering_values, lambda x, y: True
        )
    ]
