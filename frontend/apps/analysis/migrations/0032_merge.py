# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0031_censuscbsa_linkedinlocation'),
        ('analysis', '0031_questionset_display_order'),
    ]

    operations = [
    ]
