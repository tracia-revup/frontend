# -*- coding: utf-8 -*-


from django.db import models, migrations


def add_municiple_entities(apps, schema_editor):
    DataSet = apps.get_model('analysis', 'DataSet')
    DataSetConfig = apps.get_model('analysis', 'DataSetConfig')
    RecordSet = apps.get_model('analysis', 'RecordSet')
    RecordResource = apps.get_model('analysis', 'RecordResource')
    ResourceInstance = apps.get_model('analysis', 'ResourceInstance')
    ResourceConfig = apps.get_model('analysis', 'ResourceConfig')
    RecordSetConfig = apps.get_model('analysis', 'RecordSetConfig')

    muni_ds = DataSet.objects.get(title='Municipal Contribution Data')
    muni_ds_conf = DataSetConfig.objects.get(
        title='Municipal Data Configuration', data_set=muni_ds)

    muni_rs, _ = RecordSet.objects.get_or_create(
        title='Municipal Election Entities',
        module_name='frontend.apps.analysis.sources.base',
        class_name='RecordSet',
        fact_type="ENT",
        data_set=muni_ds)
    muni_rr, _ = RecordResource.objects.get_or_create(
        title='MongoDB Municipal Election Entities',
        module_name='frontend.apps.analysis.sources.municipal.backends.mongodb',
        class_name='MunicipalEntities',
        record_set=muni_rs)
    muni_ent_mongo_instance, _ = ResourceInstance.objects.get_or_create(
        title='MongoDB Municipal Election Entities "municipal_recipients" collection',
        identifier='municipal_recipients',
        record_resource=muni_rr)
    muni_ent_mongo_conf, _ = ResourceConfig.objects.get_or_create(
        title='Municipal Election Entities - MongoDB Configuration',
        record_resource=muni_rr,
        resource_instance=muni_ent_mongo_instance)
    misp_ent_conf, _ = RecordSetConfig.objects.get_or_create(
        title='Municipal Election Entities Configuration',
        record_set=muni_rs,
        resource_config=muni_ent_mongo_conf)
    muni_ds_conf.record_set_configs.add(misp_ent_conf)


def remove_municiple_entities(apps, schema_editor):
    DataSetConfig = apps.get_model('analysis', 'DataSetConfig')
    RecordSetConfig = apps.get_model('analysis', 'RecordSetConfig')

    muni_ds_conf = DataSetConfig.objects.get(
        title='Municipal Data Configuration')
    misp_ent_conf = RecordSetConfig.objects.get(
        title='Municipal Election Entities Configuration')

    muni_ds_conf.record_set_configs.remove(misp_ent_conf)


def create_municiple_feature(apps, schema_editor):
    Feature = apps.get_model("analysis", "Feature")
    FeatureConfig = apps.get_model("analysis", "FeatureConfig")
    SignalSetConfig = apps.get_model("analysis", "SignalSetConfig")
    AnalysisConfigTemplate = apps.get_model("analysis",
                                            "AnalysisConfigTemplate")
    RecordSetConfig = apps.get_model('analysis', 'RecordSetConfig')
    SignalActionConfig = apps.get_model('analysis', 'SignalActionConfig')
    SignalSetsActions = apps.get_model('analysis', 'SignalSetsActions')
    DataConfig = apps.get_model('analysis', 'DataConfig')
    DataSetConfig = apps.get_model('analysis', 'DataSetConfig')

    match_feature = Feature.objects.get(title='Matching signals')
    municipal_contrib_conf = RecordSetConfig.objects.get(
        title='Municipal Individual Contributions Configuration')

    # Add Municipal Data to the base data config
    dsc = DataSetConfig.objects.get(title="Municipal Data Configuration")
    dc = DataConfig.objects.get(title="Base Data Configuration")
    dc.data_set_configs.add(dsc)

    muni_feature_conf, _ = FeatureConfig.objects.get_or_create(
        title='Local individual contributions',
        feature=match_feature,
        partitioned=True,
        legacy_alias='municipal_matches')
    muni_feature_signals, _ = SignalSetConfig.objects.get_or_create(
        title='Municipal contributions',
        record_set_config=municipal_contrib_conf)

    muni_feature_conf.signal_set_configs.add(muni_feature_signals)


    # Prepare the Signal Actions
    search_signal = SignalActionConfig.objects.get(title="RecordSearch")
    SignalSetsActions.objects.get_or_create(
        signal_set_config=muni_feature_signals,
        signal_action_config=search_signal,
        order=10)

    date_trans_signal, created = SignalActionConfig.objects.get_or_create(
        title="Municipal DateTransform")
    if created:
        date_trans_signal.definition = {
            "class": "DateTransform",
            "module": "frontend.apps.analysis.analyses.signal_actions",
            "field": "analysis_date_received",
            "epoch_time": True
        }
        date_trans_signal.save()
    SignalSetsActions.objects.get_or_create(
        signal_set_config=muni_feature_signals,
        signal_action_config=date_trans_signal,
        order=20)

    amount_trans_signal, created = SignalActionConfig.objects.get_or_create(
        title="Municipal AmountTransform")
    if created:
        amount_trans_signal.definition = {
            "field": "analysis_amount",
            "class": "AmountTransform",
            "module": "frontend.apps.analysis.analyses.signal_actions"
        }
        amount_trans_signal.save()
    SignalSetsActions.objects.get_or_create(
        signal_set_config=muni_feature_signals,
        signal_action_config=amount_trans_signal,
        order=30)

    partition_signal = SignalActionConfig.objects.get(title="PartitionSignals")
    SignalSetsActions.objects.get_or_create(
        signal_set_config=muni_feature_signals,
        signal_action_config=partition_signal,
        partitioned=True,
        order=40)

    try:
        simple_calc_signal = SignalActionConfig.objects.get(
            title="Municipal SimpleCalculateGiving")
        simple_calc_signal.title = "SimpleCalculateGiving"
        simple_calc_signal.save()
    except SignalActionConfig.DoesNotExist:
        simple_calc_signal, created = SignalActionConfig.objects.get_or_create(
            title="SimpleCalculateGiving")
        if created:
            simple_calc_signal.definition = {
                "class": "SimpleCalculateGiving",
                "module": "frontend.apps.analysis.analyses.person_contrib"
            }
            simple_calc_signal.save()
    SignalSetsActions.objects.get_or_create(
        signal_set_config=muni_feature_signals,
        signal_action_config=simple_calc_signal,
        partitioned=True,
        order=50)

    reduce_signal, created = SignalActionConfig.objects.get_or_create(
        title="Municipal ReduceSignals")
    if created:
        reduce_signal.definition = {
            "sum_fields": ["giving"],
            "class": "ReduceSignals",
            "module": "frontend.apps.analysis.analyses.signal_actions",
            "list_fields": ["match"]
        }
        reduce_signal.save()
    SignalSetsActions.objects.get_or_create(
        signal_set_config=muni_feature_signals,
        signal_action_config=reduce_signal,
        partitioned=True,
        order=60)

    # Add the Feature to the Config Templates
    candidate = AnalysisConfigTemplate.objects.get(
        title="Default analysis config for political candidate campaign")
    committee = AnalysisConfigTemplate.objects.get(
        title="Default analysis config for political committee campaign")
    candidate.feature_configs.add(muni_feature_conf)
    committee.feature_configs.add(muni_feature_conf)


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0025_client_contributions_questionset'),
    ]

    operations = [
        migrations.RunPython(create_municiple_feature,
                             lambda x, y: True),
        migrations.RunPython(add_municiple_entities,
                             remove_municiple_entities)
    ]

