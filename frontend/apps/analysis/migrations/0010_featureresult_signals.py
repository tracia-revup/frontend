# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion
import jsonfield.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('contact', '0006_address_location_index'),
        ('analysis', '0009_add_client_signal_actions'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='featureresult',
            name='feature_config',
        ),
        migrations.RemoveField(
            model_name='featureresult',
            name='user',
        ),
        migrations.RemoveField(
            model_name='result',
            name='analysis',
        ),
        migrations.RemoveField(
            model_name='result',
            name='contact',
        ),
        migrations.RemoveField(
            model_name='result',
            name='features',
        ),
        migrations.RemoveField(
            model_name='result',
            name='partition',
        ),
        migrations.RemoveField(
            model_name='result',
            name='user',
        ),
        migrations.DeleteModel(
            name='Result',
        ),
        migrations.RemoveField(
            model_name='signalset',
            name='feature_result',
        ),
        migrations.DeleteModel(
            name='FeatureResult',
        ),
        migrations.RemoveField(
            model_name='signalset',
            name='signal_config',
        ),
        migrations.RemoveField(
            model_name='signalset',
            name='user',
        ),
        migrations.DeleteModel(
            name='SignalSet',
        ),
        migrations.CreateModel(
            name='FeatureResult',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('signals', jsonfield.fields.JSONField(default='', blank=True)),
                ('feature_config', models.ForeignKey(related_name='+', to='analysis.FeatureConfig', on_delete=django.db.models.deletion.CASCADE)),
                ('user', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Result',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key_signals', jsonfield.fields.JSONField()),
                ('score', models.FloatField(default=0.0, db_index=True)),
                ('analysis', models.ForeignKey(related_name='results', to='analysis.Analysis', on_delete=django.db.models.deletion.CASCADE)),
                ('contact', models.ForeignKey(related_name='+', blank=True, to='contact.Contact', null=True, on_delete=django.db.models.deletion.CASCADE)),
                ('features', models.ManyToManyField(related_name='+', to='analysis.FeatureResult')),
                ('partition', models.ForeignKey(related_name='+', blank=True, to='analysis.PartitionConfig', null=True, on_delete=django.db.models.deletion.CASCADE)),
                ('user', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]

