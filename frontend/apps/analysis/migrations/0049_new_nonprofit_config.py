# -*- coding: utf-8 -*-


from django.db import models, migrations


def add_nonprofit_sources(apps, schema_editor):
    DataSet = apps.get_model('analysis', 'DataSet')
    RecordSet = apps.get_model('analysis', 'RecordSet')
    RecordResource = apps.get_model('analysis', 'RecordResource')
    ResourceInstance = apps.get_model('analysis', 'ResourceInstance')
    ResourceConfig = apps.get_model('analysis', 'ResourceConfig')
    RecordSetConfig = apps.get_model('analysis', 'RecordSetConfig')
    DataSetConfig = apps.get_model('analysis', 'DataSetConfig')
    DataConfig = apps.get_model('analysis', 'DataConfig')

    ds_nonprofit, _ = DataSet.objects.get_or_create(
        title='Nonprofit and Academic Data',
        description='Data set for non-profit and academic record set',
        module_name='frontend.apps.analysis.sources.non_profit.non_profit',
        class_name='NonProfit')

    dsc_nonprofit, _ = DataSetConfig.objects.get_or_create(
        title='Non-profit and Academic Data Configuration',
        data_set=ds_nonprofit)

    nonprofit_rs, _ = RecordSet.objects.get_or_create(
        title='Non-profit RecordSet',
        module_name='frontend.apps.analysis.sources',
        class_name='RecordSet',
        fact_type='CNT',
        data_set=ds_nonprofit)
    nonprofit_rr, _ = RecordResource.objects.get_or_create(
        title='MongoDB Non-profit RecordResource',
        module_name='frontend.apps.analysis.sources.non_profit.backends.mongodbv2',
        class_name='Contributions',
        record_set=nonprofit_rs)
    nonprofit_ri, _ = ResourceInstance.objects.get_or_create(
        title='MongoDB Non-profit ResourceInstance "{}" collection'.format(
            'academic_nonprofit_master_v2'),
        identifier='academic_nonprofit_master_v2',
        record_resource=nonprofit_rr)
    nonprofit_rc, _ = ResourceConfig.objects.get_or_create(
        title='Non-profit ResourceConfig - MongoDB Configuration',
        record_resource=nonprofit_rr,
        resource_instance=nonprofit_ri)
    nonprofit_rsc, _ = RecordSetConfig.objects.get_or_create(
        title='Non-profit RecordSetConfig',
        record_set=nonprofit_rs,
        resource_config=nonprofit_rc)

    dsc_nonprofit.record_set_configs.add(nonprofit_rsc)

    dc_base = DataConfig.objects.get(title='Base Data Configuration')
    dc_base.data_set_configs.add(dsc_nonprofit)
    dsc_old = DataSetConfig.objects.filter(
        models.Q(title='Academic Data Configuration') |
        models.Q(title='Non-Profit Data Configuration'))
    dc_base.data_set_configs.remove(*dsc_old)


def add_nonprofit_features(apps, schema_editor):
    AnalysisConfigTemplate = apps.get_model('analysis', 'AnalysisConfigTemplate')
    SignalSetConfig = apps.get_model('analysis', 'SignalSetConfig')
    SignalActionConfig = apps.get_model('analysis', 'SignalActionConfig')
    SignalSetsActions = apps.get_model('analysis', 'SignalSetsActions')
    Feature = apps.get_model('analysis', 'Feature')
    FeatureConfig = apps.get_model('analysis', 'FeatureConfig')
    FeatureConfigsSignalSets = apps.get_model('analysis', 'FeatureConfigsSignalSets')
    RecordSetConfig = apps.get_model('analysis', 'RecordSetConfig')

    root_feature = Feature.objects.get(feature_type='MATCH')
    fc_nonprofit = FeatureConfig.objects.create(
        title='Non-profit and academic contributions',
        partitioned=False, feature=root_feature)

    rsc_nonprofit = RecordSetConfig.objects.get(
        title='Non-profit RecordSetConfig')
    ssc_nonprofit, _ = SignalSetConfig.objects.get_or_create(
        title='Non-profit and academic contributions',
        record_set_config=rsc_nonprofit)
    sac_search = SignalActionConfig.objects.get(title='RecordSearch')
    sac_reducer = SignalActionConfig.objects.get(title='Nonprofit ReduceSignals')
    SignalSetsActions.objects.create(
        signal_set_config=ssc_nonprofit,
        signal_action_config=sac_search,
        order=10
    )
    SignalSetsActions.objects.create(
        signal_set_config=ssc_nonprofit,
        signal_action_config=sac_reducer,
        order=200
    )

    FeatureConfigsSignalSets.objects.create(
        feature_config=fc_nonprofit,
        signal_set_config=ssc_nonprofit,
        order=10
    )

    fc_old = FeatureConfig.objects.filter(
        models.Q(title='Academic contributions') |
        models.Q(title='Nonprofit contributions'))

    # Update AnalysisConfigTemplates removing old academic and nonprofit
    # FeatureConfigs and replace with the new combined FeatureConfig.
    acts_to_update = AnalysisConfigTemplate.objects.filter(
        feature_configs__in=fc_old).distinct()
    for act in acts_to_update.iterator():
        act.feature_configs.remove(*fc_old)
        act.feature_configs.add(fc_nonprofit)


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0048_key_contrib_scoring'),
    ]

    operations = [
        migrations.RunPython(add_nonprofit_sources,
                             lambda x, y: True),
        migrations.RunPython(add_nonprofit_features,
                             lambda x, y: True),
    ]
