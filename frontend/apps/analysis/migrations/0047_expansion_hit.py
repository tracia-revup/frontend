# -*- coding: utf-8 -*-


from django.db import models, migrations


def update_reducesignals_definition(apps, schema_editor):
    SignalActionConfig = apps.get_model('analysis', 'SignalActionConfig')

    reducers = SignalActionConfig.objects.filter(
        definition__contains='ReduceSignals')

    for sac in reducers:
        list_fields = sac.definition.setdefault('list_fields', [])
        if 'expansion_hit' not in list_fields:
            list_fields.append('expansion_hit')
            sac.save()


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0046_gender_and_ethnicity_config'),
    ]

    operations = [
        migrations.RunPython(
            update_reducesignals_definition, lambda x, y: True
        ),
    ]
