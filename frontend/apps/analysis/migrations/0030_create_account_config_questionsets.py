# -*- coding: utf-8 -*-


from django.db import models, migrations


def setup_questionset(apps, schema_config):
    from frontend.apps.analysis.questionset_controllers import (
        CandidateBioController, CampaignInfoController,
        EducationInfoController, CommitteeInfoController,
        CommitteeFocusController, IssuesController)
    from frontend.libs.utils.model_utils import DynamicClassModelMixin
    QuestionSet = apps.get_model("analysis", "QuestionSet")
    FeatureConfig = apps.get_model("analysis", "FeatureConfig")
    AnalysisConfigTemplate = apps.get_model("analysis", "AnalysisConfigTemplate")

    # format of QS def is: (QuestionSetController subclass, title, icon name)
    candidate_qs_defs = [
        (CandidateBioController, "Personal Information", "bio"),
        (CampaignInfoController, "Campaign Information", "campaign-info"),
        (EducationInfoController, "Education", "education"),
    ]

    committee_qs_defs = [
        (CommitteeInfoController, "Committee Information", "bio"),
        (CommitteeFocusController, "Committee Focus", "campaign-info"),
    ]

    both_qs_defs = [
        (IssuesController, "Issues", "issues"),
    ]


    def _create_qs(klass, title, icon):
        # Use the Mixin model to parse the class/module name. Models generated
        # through the apps variable don't seem to have all the methods.
        dc = DynamicClassModelMixin()
        dc.dynamic_class = klass

        # Create the new QuestionSet
        qs, _ = QuestionSet.objects.get_or_create(
            title=title, class_name=dc.class_name,
            module_name=dc.module_name, allow_multiple=False,
            icon=icon)
        return qs

    # Get or create candidate-specific QuestionSet records
    candidate_qs = []
    for qs_def in candidate_qs_defs:
        qs = _create_qs(*qs_def)
        candidate_qs.append(qs)

    # Get or create committee-specific QuestionSet records
    committee_qs = []
    for qs_def in committee_qs_defs:
        qs = _create_qs(*qs_def)
        committee_qs.append(qs)

    # Get or create QuestionSet records that apply to both
    both_qs = []
    for qs_def in both_qs_defs:
        qs = _create_qs(*qs_def)
        both_qs.append(qs)

    # Get or create the committee FeatureConfig
    committee_fc, _ = FeatureConfig.objects.get_or_create(
        title="Committee Account Config")

    # Get or create the candidate FeatureConfig
    candidate_fc, _ = FeatureConfig.objects.get_or_create(
        title="Candidate Account Config")

    # Add committee specific QuestionSets and generic QS to committee
    # FeatureConfig
    committee_fc.question_sets.add(*(committee_qs + both_qs))

    # Add candidate specific QuestionSets and generic QS to candidate
    # FeatureConfig
    candidate_fc.question_sets.add(*(candidate_qs + both_qs))

    # Get the candidate AanalysisConfigTemplate and add the candidate
    # FeatureConfig to it
    candidate_act = AnalysisConfigTemplate.objects.get(
        title="Default analysis config for political candidate campaign")

    candidate_act.feature_configs.add(candidate_fc)

    # Get the committee AanalysisConfigTemplate and add the committee
    # FeatureConfig to it
    committee_act = AnalysisConfigTemplate.objects.get(
        title="Default analysis config for political committee campaign")

    committee_act.feature_configs.add(committee_fc)


    ### Remove ClientContributions because it has been incorporated into
    ### other QuestionSets
    try:
        contrib_limit_qs = QuestionSet.objects.get(
            title="Client Contributions")
        # Get the client contributions template FeatureConfig
        feature_conf = FeatureConfig.objects.get(
            title="Client contributions template")

        # Remove the question set
        feature_conf.question_sets.remove(contrib_limit_qs)
        contrib_limit_qs.delete()
    except QuestionSet.DoesNotExist:
        pass


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0029_remove_result_m2mfeatures'),
    ]

    operations = [
        migrations.RunPython(
            setup_questionset, lambda x, y: True
        ),
    ]
