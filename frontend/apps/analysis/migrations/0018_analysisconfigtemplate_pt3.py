# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0017_analysisconfigtemplate_pt2'),
    ]

    operations = [
        migrations.AlterField(
            model_name='analysisconfig',
            name='account',
            field=models.ForeignKey(related_name='+',
                                    on_delete=models.deletion.PROTECT,
                                    to='campaign.Account'),
            preserve_default=True,
        ),

        migrations.RemoveField(
            model_name='analysisprofile',
            name='default_config',
        ),
    ]
