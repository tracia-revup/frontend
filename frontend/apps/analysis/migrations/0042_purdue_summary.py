# -*- coding: utf-8 -*-


from django.db import models, migrations


def setup(apps, schema_editor):
    from frontend.apps.analysis.analyses import signal_actions
    from frontend.libs.utils.model_utils import DynamicClassModelMixin
    SignalSetConfig = apps.get_model('analysis', 'SignalSetConfig')
    SignalActionConfig = apps.get_model('analysis', 'SignalActionConfig')
    SignalSetsActions = apps.get_model('analysis', 'SignalSetsActions')
    Feature = apps.get_model('analysis', 'Feature')
    FeatureConfig = apps.get_model('analysis', 'FeatureConfig')
    FeatureConfigsSignalSets = apps.get_model('analysis', 'FeatureConfigsSignalSets')

    dc = DynamicClassModelMixin()

    # Use the FieldCopy SignalAction to copy the fields life_total,
    # normal_score, and normal_score_new from the record into the signal for
    # later use by Purdue ReduceSignals
    dc.dynamic_class = signal_actions.FieldCopy
    sac_purdue_fieldcopy, _ = SignalActionConfig.objects.get_or_create(
        title='Purdue FieldCopy')
    sac_purdue_fieldcopy.definition = {
        'class': dc.class_name,
        'module': dc.module_name,
        'mappings': [
            {"source_field": "life_total",
             "target_field": "life_total"},
            {"source_field": "normal_score_new",
             "target_field": "normal_score_new"},
            {"source_field": "normal_score",
             "target_field": "normal_score"}
        ]}
    sac_purdue_fieldcopy.save()

    ssc_purdue_bio = SignalSetConfig.objects.get(title='Purdue Bio')
    SignalSetsActions.objects.get_or_create(
        signal_set_config=ssc_purdue_bio,
        signal_action_config=sac_purdue_fieldcopy,
        order=45)


    # Use Purdue ReduceSignals to copy the fields life_total, normal_score, and
    # normal_score_new from the purdue_bio signal into the signal_set_result
    # object. We need these for later processing during summary.
    sac_purdue_reduce_signals = SignalActionConfig.objects.get(
        title='Purdue ReduceSignals')
    sac_purdue_reduce_signals.definition['list_fields'].extend(
        ['life_total', 'normal_score', 'normal_score_new'])
    sac_purdue_reduce_signals.save()


    # Create Purdue Summary SAC and SSC to do academic scoring
    dc.dynamic_class = signal_actions.PurdueSummary
    sac_purdue_summary, _ = SignalActionConfig.objects.get_or_create(
        title='Purdue Summary')
    sac_purdue_summary.definition = {
        'class': dc.class_name,
        'module': dc.module_name}
    sac_purdue_summary.save()

    ssc_purdue_summary, _ = SignalSetConfig.objects.get_or_create(
        title='Purdue Summary signals')

    ssc_purdue_summary.signal_set_configs.add(ssc_purdue_bio)

    SignalSetsActions.objects.get_or_create(
        signal_set_config=ssc_purdue_summary,
        signal_action_config=sac_purdue_summary,
        order=10)


    # Create summary FeatureConfig for Purdue
    feature_summary = Feature.objects.get(feature_type='SUM')
    fc_purdue_summary, _ = FeatureConfig.objects.get_or_create(
        title='Purdue Summary',
        partitioned=True,
        feature=feature_summary)

    # Run Summary Signals signal set config first to calculate political score.
    # We use a separate SignalSetConfig instead of adding the SumValues SAC to
    # the Purdue summary SSC because we need to be able to differentiate
    # between the signal_set_result records we get to find the purdue_bio alum
    # record.
    ssc_summary = SignalSetConfig.objects.get(title='Summary signals')
    FeatureConfigsSignalSets.objects.get_or_create(
        feature_config=fc_purdue_summary,
        signal_set_config=ssc_summary,
        order=10)

    # Run Purdue Summary signals SSC second.
    FeatureConfigsSignalSets.objects.get_or_create(
        feature_config=fc_purdue_summary,
        signal_set_config=ssc_purdue_summary,
        order=20)


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0041_sac_giving_history'),
    ]

    operations = [
        migrations.RunPython(setup,
                             lambda x, y: True),
    ]
