# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0007_partition_configs_to_2000'),
    ]

    operations = [
        migrations.AddField(
            model_name='result',
            name='score',
            field=models.FloatField(default=0.0, db_index=True),
            preserve_default=True,
        ),
    ]
