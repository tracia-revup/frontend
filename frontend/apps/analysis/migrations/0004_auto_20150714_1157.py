# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0003_new_analysis_data'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='signalsetsactions',
            options={'ordering': ['order'], 'verbose_name_plural': 'Signal sets actions'},
        ),
        migrations.RemoveField(
            model_name='dataset',
            name='default_config',
        ),
        migrations.RemoveField(
            model_name='feature',
            name='default_config',
        ),
        migrations.RemoveField(
            model_name='recordresource',
            name='default_config',
        ),
        migrations.RemoveField(
            model_name='recordset',
            name='default_config',
        ),
        migrations.AlterField(
            model_name='analysisconfig',
            name='account',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='campaign.Account', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='analysisconfig',
            name='analysis_profile',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, to='analysis.AnalysisProfile'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='analysisconfig',
            name='data_config',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='analysis.DataConfig', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='analysisconfig',
            name='partition_set_config',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='analysis.PartitionSetConfig', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='analysisconfig',
            name='user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='analysisprofile',
            name='default_config',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='analysis.AnalysisConfig', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='dataconfig',
            name='account',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='campaign.Account', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='dataconfig',
            name='user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='dataset',
            name='account',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='campaign.Account', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='dataset',
            name='user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='datasetconfig',
            name='account',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='campaign.Account', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='datasetconfig',
            name='data_set',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, to='analysis.DataSet'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='datasetconfig',
            name='user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='featureconfig',
            name='account',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='campaign.Account', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='featureconfig',
            name='feature',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='analysis.Feature', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='featureconfig',
            name='user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='partitionconfig',
            name='account',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='campaign.Account', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='partitionconfig',
            name='user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='partitionsetconfig',
            name='account',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='campaign.Account', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='partitionsetconfig',
            name='user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='recordresource',
            name='account',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='campaign.Account', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='recordresource',
            name='record_set',
            field=models.ForeignKey(related_name='record_resources', on_delete=django.db.models.deletion.PROTECT, to='analysis.RecordSet'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='recordresource',
            name='user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='recordset',
            name='account',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='campaign.Account', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='recordset',
            name='data_set',
            field=models.ForeignKey(related_name='record_sets', on_delete=django.db.models.deletion.PROTECT, to='analysis.DataSet'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='recordset',
            name='user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='recordsetconfig',
            name='account',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='campaign.Account', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='recordsetconfig',
            name='record_set',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, to='analysis.RecordSet'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='recordsetconfig',
            name='resource_config',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, to='analysis.ResourceConfig'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='recordsetconfig',
            name='user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='resourceconfig',
            name='account',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='campaign.Account', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='resourceconfig',
            name='record_resource',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, to='analysis.RecordResource'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='resourceconfig',
            name='resource_instance',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, to='analysis.ResourceInstance'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='resourceconfig',
            name='user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='resourceinstance',
            name='account',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='campaign.Account', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='resourceinstance',
            name='record_resource',
            field=models.ForeignKey(related_name='resource_instances', on_delete=django.db.models.deletion.PROTECT, to='analysis.RecordResource'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='resourceinstance',
            name='user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='signalactionconfig',
            name='account',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='campaign.Account', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='signalactionconfig',
            name='user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='signalsetconfig',
            name='account',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='campaign.Account', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='signalsetconfig',
            name='record_set_config',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='analysis.RecordSetConfig', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='signalsetconfig',
            name='signal_actions',
            field=models.ManyToManyField(to='analysis.SignalActionConfig', null=True, through='analysis.SignalSetsActions', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='signalsetconfig',
            name='spectrum_config',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='analysis.SpectrumConfig', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='signalsetconfig',
            name='user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='signalsetsactions',
            name='signal_action_config',
            field=models.ForeignKey(to='analysis.SignalActionConfig', on_delete=django.db.models.deletion.PROTECT),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='signalsetsactions',
            name='signal_set_config',
            field=models.ForeignKey(to='analysis.SignalSetConfig', on_delete=django.db.models.deletion.PROTECT),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='spectrumconfig',
            name='account',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='campaign.Account', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='spectrumconfig',
            name='user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
    ]
