# Generated by Django 2.0.5 on 2019-04-22 13:17

from django.db import migrations


def forwards(apps, schema_editor):
    from frontend.libs.utils.model_utils import get_or_clone

    AnalysisConfigTemplate = apps.get_model('analysis',
                                            'AnalysisConfigTemplate')
    DataConfig = apps.get_model('analysis', 'DataConfig')
    DataSetConfig = apps.get_model('analysis', 'DataSetConfig')
    RecordSetConfig = apps.get_model('analysis', 'RecordSetConfig')

    # Create a DataSetConfig for the 'Client Contributions Configuration -
    # ENTITY' RecordSetConfig
    rsc_title = 'Client Contributions Configuration - ENTITY'
    rsc = RecordSetConfig.objects.get(title=rsc_title)
    dsc, _ = DataSetConfig.objects.get_or_create(
        title='Client Data Set Configuration - ENTITY',
        data_set_id=rsc.record_set.data_set_id)
    dsc.record_set_configs.add(rsc)

    # Clone a DataConfig from the 'Base Entity Data Configuration' DataConfig
    entity_title = 'Base Entity Data Configuration - Client Entities'
    description = 'This data config is similar to DataConfig: Base Entity ' \
                  'Data Configuration. The two data configs differ by ' \
                  'client data set configs'
    dc = get_or_clone(
        DataConfig,
        locate_kwargs=dict(title=entity_title),
        base_locate_kwargs=dict(title='Base Entity Data Configuration'),
        title=entity_title,
        description=description
    )
    # Add the new client DSC to the new entity DataConfig and remove the
    # old DSC
    dc.data_set_configs.add(dsc)
    dsc_old = DataSetConfig.objects.get(
        title='Client Data Set Configuration - BLANK')
    dc.data_set_configs.remove(dsc_old)

    # Add the new DataConfig to the Nonprofit ACT
    act_non_profit = AnalysisConfigTemplate.objects.get(
        title='NonProfit Entity Analysis Config Template')
    act_non_profit.data_config = dc
    act_non_profit.save()


def backwards(apps, schema_editor):
    AnalysisConfigTemplate = apps.get_model('analysis',
                                            'AnalysisConfigTemplate')
    DataConfig = apps.get_model('analysis', 'DataConfig')
    DataSetConfig = apps.get_model('analysis', 'DataSetConfig')
    RecordSetConfig = apps.get_model('analysis', 'RecordSetConfig')

    # Delete the new DataSetConfig created for the 'Client Contributions
    # Configuration - ENTITY' RecordSetConfig
    rsc_title = 'Client Contributions Configuration - ENTITY'
    rsc = RecordSetConfig.objects.get(title=rsc_title)
    try:
        dsc = DataSetConfig.objects.get(
            title='Client Data Set Configuration - ENTITY',
            data_set_id=rsc.record_set.data_set_id)
    except DataSetConfig.DoesNotExist:
        pass
    else:
        dsc.delete()

    # Add back the old DataConfig to the nonprofit ACT
    act_non_profit = AnalysisConfigTemplate.objects.get(
        title='NonProfit Entity Analysis Config Template')
    dc_old = DataConfig.objects.get(title='Base Entity Data Configuration')
    act_non_profit.data_config = dc_old
    act_non_profit.save()

    # Delete the new DataConfig cloned from the 'Base Entity Data
    # Configuration' DataConfig
    try:
        dc = DataConfig.objects.get(
            title='Base Entity Data Configuration - Client Entities')
    except DataConfig.DoesNotExist:
        pass
    else:
        dc.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0104_auto_20190321_1456'),
    ]

    operations = [
        migrations.RunPython(forwards, backwards)
    ]
