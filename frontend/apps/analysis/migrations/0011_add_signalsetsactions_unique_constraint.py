# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0010_featureresult_signals'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='signalsetsactions',
            unique_together=set([('signal_set_config', 'order')]),
        ),
    ]
