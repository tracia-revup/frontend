# -*- coding: utf-8 -*-


from django.db import models, migrations


def add_key_contributions(apps, schema_editor):
    SignalSetConfig = apps.get_model('analysis', 'SignalSetConfig')
    SignalActionConfig = apps.get_model('analysis', 'SignalActionConfig')
    SignalSetsActions = apps.get_model('analysis', 'SignalSetsActions')
    FeatureConfig = apps.get_model("analysis", "FeatureConfig")

    key_contrib = FeatureConfig.objects.get(title="Key Contributions")

    key_contributions_signals = SignalSetConfig.objects.create(
        title='Key Contributions',)
    key_contrib.signal_set_configs.add(key_contributions_signals)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.signal_actions',
        'class': 'ConfigurableEntityFilter',
    }
    filter_action = SignalActionConfig.objects.create(
        title='Key Contributions Entity Filter',
        definition=action_def)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.base',
        'class': 'ReduceSignals',
        'list_fields': [],
        'sum_fields': [],
        'combine_fields': [{"label, is_ally": ("match", "record_set_config")}],
        'combine_label': 'key_contributions'
    }
    reduce_action = SignalActionConfig.objects.create(
        title='Key Contributions ReduceSignals',
        definition=action_def)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.signal_actions',
        'class': 'KeyContributionsKeySignals',
        'combine_label': 'key_contributions'
    }
    key_sig_action = SignalActionConfig.objects.create(
        title='Key Contributions Key Signals',
        definition=action_def)

    SignalSetsActions.objects.create(
        signal_set_config=key_contributions_signals,
        signal_action_config=filter_action,
        order=20)
    SignalSetsActions.objects.create(
        signal_set_config=key_contributions_signals,
        signal_action_config=reduce_action,
        order=30)
    SignalSetsActions.objects.create(
        signal_set_config=key_contributions_signals,
        signal_action_config=key_sig_action,
        order=40)


def remove_key_contributions(apps, schema_editor):
    SignalSetConfig = apps.get_model('analysis', 'SignalSetConfig')
    SignalActionConfig = apps.get_model('analysis', 'SignalActionConfig')
    SignalSetsActions = apps.get_model('analysis', 'SignalSetsActions')
    FeatureConfig = apps.get_model("analysis", "FeatureConfig")

    key_contrib = FeatureConfig.objects.get(title="Key Contributions")

    key_contributions_signals = SignalSetConfig.objects.get(
        title='Key Contributions')
    key_contrib.signal_set_configs.remove(key_contributions_signals)

    filter_action = SignalActionConfig.objects.get(
        title='Key Contributions Entity Filter')
    reduce_action = SignalActionConfig.objects.get(
        title='Key Contributions ReduceSignals')
    key_sig_action = SignalActionConfig.objects.get(
        title='Key Contributions Key Signals')

    SignalSetsActions.objects.filter(
        signal_set_config=key_contributions_signals,
        signal_action_config=filter_action).delete()
    SignalSetsActions.objects.filter(
        signal_set_config=key_contributions_signals,
        signal_action_config=reduce_action).delete()
    SignalSetsActions.objects.filter(
        signal_set_config=key_contributions_signals,
        signal_action_config=key_sig_action).delete()

    filter_action.delete()
    reduce_action.delete()
    key_sig_action.delete()
    key_contributions_signals.delete()


def add_result_formatter(apps, schema_editor):
    from frontend.apps.analysis.analyses.formatters import \
        KeyContributionsResultFormatter
    from frontend.libs.utils.model_utils import DynamicClassModelMixin

    FeatureConfig = apps.get_model("analysis", "FeatureConfig")
    key_contrib = FeatureConfig.objects.get(title="Key Contributions")
    dc = DynamicClassModelMixin()
    dc.dynamic_class = KeyContributionsResultFormatter

    key_contrib.class_name = dc.class_name
    key_contrib.module_name = dc.module_name
    key_contrib.save()


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0019_create_required_mongodb_indexes'),
    ]

    operations = [
        migrations.RunPython(add_key_contributions,
                             remove_key_contributions),
        migrations.AddField(
            model_name='featureconfig',
            name='class_name',
            field=models.CharField(max_length=256, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='featureconfig',
            name='module_name',
            field=models.CharField(max_length=256, blank=True),
            preserve_default=True,
        ),
        migrations.RunPython(add_result_formatter,
                             reverse_code=lambda x, y: True)
    ]
