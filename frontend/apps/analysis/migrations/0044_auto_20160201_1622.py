# -*- coding: utf-8 -*-


from django.db import models, migrations


def setup_ditr(apps, schema_editor):
    from frontend.apps.analysis.analyses import signal_actions
    from frontend.libs.utils.model_utils import DynamicClassModelMixin
    SignalSetConfig = apps.get_model('analysis', 'SignalSetConfig')
    SignalActionConfig = apps.get_model('analysis', 'SignalActionConfig')
    SignalSetsActions = apps.get_model('analysis', 'SignalSetsActions')
    FeatureConfig = apps.get_model("analysis", "FeatureConfig")
    Feature = apps.get_model("analysis", "Feature")
    FeatureConfigsSignalSets = apps.get_model('analysis', 'FeatureConfigsSignalSets')

    subset_feature = Feature.objects.get(feature_type='MATCH_SUB')

    def factory(party):
        ditr_fc, _ = FeatureConfig.objects.get_or_create(
            title="Diamonds in the Rough ({})".format(party),
            feature=subset_feature)
        ditr_ssc, _ = SignalSetConfig.objects.get_or_create(
            title="Diamonds in the Rough ({})".format(party))

        # Add the signal set to the feature config
        FeatureConfigsSignalSets.objects.get_or_create(
            feature_config=ditr_fc,
            signal_set_config=ditr_ssc,
            order=10)

        dc = DynamicClassModelMixin()
        dc.dynamic_class = signal_actions.DiamondsInTheRough
        try:
            ditr_sac = SignalActionConfig.objects.get(
                title='Diamonds In The Rough ({})'.format(party))
        except SignalActionConfig.DoesNotExist:
            ditr_sac = SignalActionConfig.objects.create(
                title='Diamonds In The Rough ({})'.format(party),
                description='A "Diamond in the Rough" is a contact who has '
                            'given to our client within the last two cycles, '
                            'but gave an amount significantly below the '
                            'average of their normal contributions.',
                definition={
                    "module": "frontend.apps.analysis.analyses.signal_actions",
                    "class": "DiamondsInTheRough",
                    "party": "{}".format(party)
                })

        SignalSetsActions.objects.get_or_create(
            signal_set_config=ditr_ssc,
            signal_action_config=ditr_sac,
            order=10)

    for party in ("REP", "DEM"):
        factory(party)


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0043_merge'),
    ]

    operations = [
        migrations.RunPython(setup_ditr, lambda x, y: True)
    ]
