
from django.db import migrations


def forwards(apps, schema_editor):
    from frontend.libs.utils.model_utils import get_or_clone
    AnalysisConfigTemplate = apps.get_model("analysis",
                                            "AnalysisConfigTemplate")
    FeatureConfig = apps.get_model("analysis", "FeatureConfig")
    DataConfig = apps.get_model("analysis", "DataConfig")
    DataSetConfig = apps.get_model("analysis", "DataSetConfig")

    committee_entity_act_title = 'Default entity analysis config for ' \
                                 'political committee campaign'

    fc_old = FeatureConfig.objects.get(title__in=
                                 ['Non-profit and academic contributions',
                                  'Nonprofit and academic contributions'])

    fc_new = FeatureConfig.objects.get(
        title='Non-profit entity contributions')

    dc_entities = DataConfig.objects.get(
        title='Base Entity Data Configuration')
    dsc_non_profit_entities = DataSetConfig.objects.get(
        title='Non-profit Entity Data Configuration')
    dc_entities.data_set_configs.add(dsc_non_profit_entities)

    act_non_profit_title = 'NonProfit Entity Analysis Config Template'
    act_non_profit = get_or_clone(
        AnalysisConfigTemplate,
        locate_kwargs=dict(title=act_non_profit_title),
        base_locate_kwargs=dict(title=committee_entity_act_title),
        title=act_non_profit_title
    )

    fc_existing = FeatureConfig.objects.get(title='Summary signals for Entity')
    act_non_profit.feature_configs.remove(fc_existing, fc_old)

    fc_non_profit_title = 'Non-profit Entity Summary'
    fc_non_profit = FeatureConfig.objects.get(
        title=fc_non_profit_title)
    act_non_profit.feature_configs.add(fc_non_profit, fc_new)


def backwards(apps, schema_editor):
    AnalysisConfigTemplate = apps.get_model("analysis",
                                            "AnalysisConfigTemplate")
    DataConfig = apps.get_model("analysis", "DataConfig")
    DataSetConfig = apps.get_model("analysis", "DataSetConfig")

    dc_entities = DataConfig.objects.get(
        title='Base Entity Data Configuration')
    dsc_non_profit_entities = DataSetConfig.objects.get(
        title='Non-profit Entity Data Configuration')
    dc_entities.data_set_configs.remove(dsc_non_profit_entities)

    act_non_profit_title = 'NonProfit Entity Analysis Config Template'
    AnalysisConfigTemplate.objects.filter(title=act_non_profit_title).delete()


class Migration(migrations.Migration):
    dependencies = [
        ('analysis', '0089_merge_20181120_0951'),
    ]

    operations = [
        migrations.RunPython(forwards, backwards),
    ]
