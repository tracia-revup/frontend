# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0028_auto_20151112_1323'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='result',
            name='m2mfeatures',
        ),
    ]
