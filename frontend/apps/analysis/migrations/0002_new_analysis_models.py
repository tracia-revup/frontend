# -*- coding: utf-8 -*-

import datetime

from django.db import models, migrations
import audit_log.models.fields
import django_extensions.db.fields
import jsonfield.fields
import django.utils.timezone
from django.conf import settings


def set_analyses_accounts(apps, schema_editor):
    Analysis = apps.get_model("analysis", "Analysis")

    analyses = Analysis.objects.all()

    for analysis in analyses:
        account = analysis.event.account
        analysis.account = account
        analysis.save()


def create_base_partitions(apps, schema_editor):
    PartitionConfig = apps.get_model("analysis", "PartitionConfig")

    title = "2010 Election Cycle"
    description = "Dates since start of 2010 election cycle."
    definition = {
        "type": "start_date",
        "start_date": datetime.date(2008, 11, 4)
    }
    partitions = [
        PartitionConfig.objects.create(
            title=title,
            description=description,
            index=1, label="2010",
            definition=definition)
    ]
    title = "2012 Election Cycle"
    description = "Dates since start of 2012 election cycle."
    definition = {
        "type": "start_date",
        "start_date": datetime.date(2010, 11, 2)
    }
    partitions.append(
        PartitionConfig.objects.create(
            title=title,
            description=description,
            index=2,
            label="2012",
            definition=definition))
    title = "2014 Election Cycle"
    description = "Dates since start of 2014 election cycle."
    definition = {
        "type": "start_date",
        "start_date": datetime.date(2012, 11, 6)
    }
    partitions.append(
        PartitionConfig.objects.create(
            title=title,
            description=description,
            index=3,
            label="2014",
            definition=definition))
    return partitions


def create_base_partionset(apps, schema_editor):
    PartitionSetConfig = apps.get_model("analysis", "PartitionSetConfig")
    title = "Base Partition Set"
    description = ("Partitions including dates since start of 2008, "
                   "2010 and 2014 election cycles")
    label = "Giving History Since {} Election Cycle"
    configs = create_base_partitions(apps, schema_editor)
    partition_set = PartitionSetConfig.objects.create(
        title=title, description=description, label=label)
    for config in configs:
        partition_set.partition_configs.add(config)
    partition_set.save()
    return partition_set


def create_legacy_analysis_configs(apps, schema_editor):
    AnalysisConfig = apps.get_model("analysis", "AnalysisConfig")
    title = "Political Legacy Analysis"
    description = ("Analysis configuration for political campaign use of "
                   "legacy analysis. TO BE DEPRECATED. Use only for legacy "
                   "analysis compatibility.")
    analysis_profile = create_political_legacy_analysis_profile(
        apps, schema_editor)
    partitions = create_legacy_partionset(apps, schema_editor)
    partitions = create_base_partionset(apps, schema_editor)
    config = AnalysisConfig.objects.create(title=title,
                                           description=description,
                                           analysis_profile=analysis_profile,
                                           partition_set_config=partitions)
    analysis_profile.default_config = config
    analysis_profile.save()
    title = "Academic Legacy Analysis"
    description = ("Analysis configuration for academic campaign use of "
                   "legacy analysis. TO BE DEPRECATED. Use only for legacy "
                   "analysis compatibility.")
    analysis_profile = create_academic_legacy_analysis_profile(
        apps, schema_editor)
    config = AnalysisConfig.objects.create(title=title,
                                           description=description,
                                           analysis_profile=analysis_profile)
    analysis_profile.default_config = config
    analysis_profile.save()


def create_legacy_partitions(apps, schema_editor):
    PartitionConfig = apps.get_model("analysis", "PartitionConfig")

    title = "( DEPRECATED) 6 years prior"
    description = ("Default partition for dates since 6 years prior to "
                   "analysis date. TO BE DEPRECATED. Use only for legacy "
                   "analysis compatibility.")
    definition = {
        "type": "years_before",
        "years_before": "6"
    }
    partitions = [PartitionConfig.objects.create(title=title,
                                                 description=description,
                                                 index=1, label="2010",
                                                 definition=definition)]
    title = "( DEPRECATED) 4 years prior"
    description = ("Default partition for dates since 4 years prior to "
                   "analysis date. TO BE DEPRECATED. Use only for legacy "
                   "analysis compatibility.")
    definition = {
        "type": "years_before",
        "years_before": "4"
    }
    partitions.append(PartitionConfig.objects.create(
        title=title, description=description, index=2, label="2012",
        definition=definition))
    title = "( DEPRECATED) 2 years prior"
    description = ("Default partition for dates since 2 years prior to "
                   "analysis date. TO BE DEPRECATED. Use only for legacy "
                   "analysis compatibility.")
    definition = {
        "type": "years_before",
        "years_before": "2"
    }
    partitions.append(PartitionConfig.objects.create(
        title=title, description=description, index=3, label="2014",
        definition=definition))
    return partitions


def create_legacy_partionset(apps, schema_editor):
    PartitionSetConfig = apps.get_model("analysis", "PartitionSetConfig")
    title = "Legacy Partition Set"
    description = ("Partitions include dates 6, 4, and 2 years prior to "
                   "analysis date. TO BE DEPRECATED. Use only for legacy "
                   "analysis compatibility.")
    label = "Giving History Since {} Election Cycle"
    configs = create_legacy_partitions(apps, schema_editor)
    partition_set = PartitionSetConfig.objects.create(
        title=title, description=description, label=label)
    for config in configs:
        partition_set.partition_configs.add(config)
    partition_set.save()
    return partition_set


def create_political_legacy_analysis_profile(apps, schema_editor):
    AnalysisProfile = apps.get_model("analysis", "AnalysisProfile")
    title = "Political legacy analysis"
    description = ("Analysis profile for political campaigns using legacy "
                   "analysis. TO BE DEPRECATED. Use only for legacy analysis "
                   "compatibility.")
    analysis_type = "LEGACY"
    return AnalysisProfile.objects.create(title=title, description=description,
                                   analysis_type=analysis_type)

def create_academic_legacy_analysis_profile(apps, schema_editor):
    AnalysisProfile = apps.get_model("analysis", "AnalysisProfile")
    title = "Academic legacy analysis"
    description = ("Analysis profile for academic campaigns using legacy "
                   "analysis. TO BE DEPRECATED. Use only for legacy analysis "
                   "compatibility.")
    analysis_type = "LEGACY"
    return AnalysisProfile.objects.create(title=title, description=description,
                                   analysis_type=analysis_type)


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('campaign', '0016_prospect_timestamps'),
        ('contact', '0004_auto_20150512_0037'),
        ('analysis', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AnalysisConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('account', models.ForeignKey(related_name='+', blank=True, to='campaign.Account', null=True, on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AnalysisProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('analysis_type', models.CharField(blank=True, max_length=16, choices=[('LEGACY', 'Legacy Individual Contribution Ranking'), ('PN_CONTRIB_RANK', 'Individual Contribution Ranking')])),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_analysis_analysisprofile_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('default_config', models.OneToOneField(related_name='+', null=True, blank=True, to='analysis.AnalysisConfig', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DataConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('account', models.ForeignKey(related_name='+', blank=True, to='campaign.Account', null=True, on_delete=django.db.models.deletion.CASCADE)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_analysis_dataconfig_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DataSet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('class_name', models.CharField(max_length=256, blank=True)),
                ('module_name', models.CharField(max_length=256, blank=True)),
                ('account', models.ForeignKey(related_name='+', blank=True, to='campaign.Account', null=True, on_delete=django.db.models.deletion.CASCADE)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_analysis_dataset_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DataSetConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('account', models.ForeignKey(related_name='+', blank=True, to='campaign.Account', null=True, on_delete=django.db.models.deletion.CASCADE)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_analysis_datasetconfig_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('data_set', models.ForeignKey(related_name='+', to='analysis.DataSet', on_delete=django.db.models.deletion.CASCADE)),
                ('modified_by', audit_log.models.fields.LastUserField(related_name='modified_analysis_datasetconfig_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by')),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Feature',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('feature_type', models.CharField(blank=True, max_length=16, choices=[('LOOKUP', 'Field lookups'), ('SPECTRUM', 'Spectrum values'), ('MATCH', 'Matching facts from data sets'), ('MATCH_SUB', 'Subsets of matching facts'), ('SUM', 'Summary features'), ('NORM', 'Feature normalization')])),
                ('allow_multiple', models.BooleanField(default=False)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_analysis_feature_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FeatureConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('partitioned', models.BooleanField(default=False)),
                ('icon', models.ImageField(null=True, upload_to='', blank=True)),
                ('icon_name', models.CharField(max_length=128, blank=True)),
                ('text', models.TextField(blank=True)),
                ('hidden', models.BooleanField(default=False)),
                ('legacy_alias', models.CharField(max_length=32, blank=True)),
                ('account', models.ForeignKey(related_name='+', blank=True, to='campaign.Account', null=True, on_delete=django.db.models.deletion.CASCADE)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_analysis_featureconfig_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('feature', models.ForeignKey(related_name='+', blank=True, to='analysis.Feature', null=True, on_delete=django.db.models.deletion.CASCADE)),
                ('modified_by', audit_log.models.fields.LastUserField(related_name='modified_analysis_featureconfig_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by')),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FeatureResult',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('feature_config', models.ForeignKey(related_name='+', to='analysis.FeatureConfig', on_delete=django.db.models.deletion.CASCADE)),
                ('user', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PartitionConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('index', models.IntegerField(null=True, blank=True)),
                ('label', models.CharField(max_length=64)),
                ('definition', jsonfield.fields.JSONField()),
                ('account', models.ForeignKey(related_name='+', blank=True, to='campaign.Account', null=True, on_delete=django.db.models.deletion.CASCADE)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_analysis_partitionconfig_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('modified_by', audit_log.models.fields.LastUserField(related_name='modified_analysis_partitionconfig_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by')),
                ('user', models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
                'ordering': ['index'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PartitionSetConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('label', models.CharField(max_length=64, blank=True)),
                ('account', models.ForeignKey(related_name='+', blank=True, to='campaign.Account', null=True, on_delete=django.db.models.deletion.CASCADE)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_analysis_partitionsetconfig_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('modified_by', audit_log.models.fields.LastUserField(related_name='modified_analysis_partitionsetconfig_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by')),
                ('partition_configs', models.ManyToManyField(related_name='+', null=True, to='analysis.PartitionConfig', blank=True)),
                ('user', models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RecordResource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('class_name', models.CharField(max_length=256, blank=True)),
                ('module_name', models.CharField(max_length=256, blank=True)),
                ('account', models.ForeignKey(related_name='+', blank=True, to='campaign.Account', null=True, on_delete=django.db.models.deletion.CASCADE)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_analysis_recordresource_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RecordSet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('class_name', models.CharField(max_length=256, blank=True)),
                ('module_name', models.CharField(max_length=256, blank=True)),
                ('account', models.ForeignKey(related_name='+', blank=True, to='campaign.Account', null=True, on_delete=django.db.models.deletion.CASCADE)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_analysis_recordset_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('data_set', models.ForeignKey(related_name='record_sets', to='analysis.DataSet', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RecordSetConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('account', models.ForeignKey(related_name='+', blank=True, to='campaign.Account', null=True, on_delete=django.db.models.deletion.CASCADE)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_analysis_recordsetconfig_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('modified_by', audit_log.models.fields.LastUserField(related_name='modified_analysis_recordsetconfig_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by')),
                ('record_set', models.ForeignKey(related_name='+', to='analysis.RecordSet', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ResourceConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('account', models.ForeignKey(related_name='+', blank=True, to='campaign.Account', null=True, on_delete=django.db.models.deletion.CASCADE)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_analysis_resourceconfig_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('modified_by', audit_log.models.fields.LastUserField(related_name='modified_analysis_resourceconfig_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by')),
                ('record_resource', models.ForeignKey(related_name='+', to='analysis.RecordResource', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ResourceInstance',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('identifier', models.CharField(max_length=64, blank=True)),
                ('account', models.ForeignKey(related_name='+', blank=True, to='campaign.Account', null=True, on_delete=django.db.models.deletion.CASCADE)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_analysis_resourceinstance_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('modified_by', audit_log.models.fields.LastUserField(related_name='modified_analysis_resourceinstance_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by')),
                ('record_resource', models.ForeignKey(related_name='resource_instances', to='analysis.RecordResource', on_delete=django.db.models.deletion.CASCADE)),
                ('user', models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Result',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key_signals', jsonfield.fields.JSONField()),
                ('analysis', models.ForeignKey(related_name='results', to='analysis.Analysis', on_delete=django.db.models.deletion.CASCADE)),
                ('contact', models.ForeignKey(related_name='+', blank=True, to='contact.Contact', null=True, on_delete=django.db.models.deletion.CASCADE)),
                ('features', models.ManyToManyField(related_name='+', to='analysis.FeatureResult')),
                ('partition', models.ForeignKey(related_name='+', blank=True, to='analysis.PartitionConfig', null=True, on_delete=django.db.models.deletion.CASCADE)),
                ('user', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SignalActionConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('definition', jsonfield.fields.JSONField()),
                ('partitioned', models.BooleanField(default=False)),
                ('account', models.ForeignKey(related_name='+', blank=True, to='campaign.Account', null=True, on_delete=django.db.models.deletion.CASCADE)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_analysis_signalactionconfig_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('modified_by', audit_log.models.fields.LastUserField(related_name='modified_analysis_signalactionconfig_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by')),
                ('user', models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SignalSet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('signals', jsonfield.fields.JSONField()),
                ('feature_result', models.ForeignKey(related_name='signal_sets', to='analysis.FeatureResult', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SignalSetConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('account', models.ForeignKey(related_name='+', blank=True, to='campaign.Account', null=True, on_delete=django.db.models.deletion.CASCADE)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_analysis_signalsetconfig_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('modified_by', audit_log.models.fields.LastUserField(related_name='modified_analysis_signalsetconfig_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by')),
                ('record_set_config', models.ForeignKey(related_name='+', blank=True, to='analysis.RecordSetConfig', null=True, on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SignalSetsActions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('partitioned', models.BooleanField(default=False)),
                ('order', models.IntegerField()),
                ('signal_action_config', models.ForeignKey(to='analysis.SignalActionConfig', on_delete=django.db.models.deletion.CASCADE)),
                ('signal_set_config', models.ForeignKey(to='analysis.SignalSetConfig', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
                'ordering': ['order'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SpectrumConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('definition', jsonfield.fields.JSONField()),
                ('account', models.ForeignKey(related_name='+', blank=True, to='campaign.Account', null=True, on_delete=django.db.models.deletion.CASCADE)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_analysis_spectrumconfig_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('modified_by', audit_log.models.fields.LastUserField(related_name='modified_analysis_spectrumconfig_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by')),
                ('user', models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='signalsetconfig',
            name='signal_actions',
            field=models.ManyToManyField(related_name='+', null=True, through='analysis.SignalSetsActions', to='analysis.SignalActionConfig', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='signalsetconfig',
            name='signal_set_configs',
            field=models.ManyToManyField(related_name='dependent_sets', null=True, to='analysis.SignalSetConfig', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='signalsetconfig',
            name='spectrum_config',
            field=models.ForeignKey(related_name='+', blank=True, to='analysis.SpectrumConfig', null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='signalsetconfig',
            name='user',
            field=models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='signalset',
            name='signal_config',
            field=models.ForeignKey(related_name='+', to='analysis.SignalSetConfig', on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='signalset',
            name='user',
            field=models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='resourceconfig',
            name='resource_instance',
            field=models.ForeignKey(related_name='+', to='analysis.ResourceInstance', on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='resourceconfig',
            name='user',
            field=models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='recordsetconfig',
            name='resource_config',
            field=models.ForeignKey(related_name='+', to='analysis.ResourceConfig', on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='recordsetconfig',
            name='user',
            field=models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='recordset',
            name='default_config',
            field=models.OneToOneField(related_name='+', null=True, blank=True, to='analysis.RecordSetConfig', on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='recordset',
            name='modified_by',
            field=audit_log.models.fields.LastUserField(related_name='modified_analysis_recordset_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='recordset',
            name='user',
            field=models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='recordresource',
            name='default_config',
            field=models.OneToOneField(related_name='+', null=True, blank=True, to='analysis.ResourceConfig', on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='recordresource',
            name='modified_by',
            field=audit_log.models.fields.LastUserField(related_name='modified_analysis_recordresource_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='recordresource',
            name='record_set',
            field=models.ForeignKey(related_name='record_resources', to='analysis.RecordSet', on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='recordresource',
            name='user',
            field=models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='featureconfig',
            name='signal_set_configs',
            field=models.ManyToManyField(related_name='+', null=True, to='analysis.SignalSetConfig', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='featureconfig',
            name='user',
            field=models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='feature',
            name='default_config',
            field=models.OneToOneField(related_name='+', null=True, blank=True, to='analysis.FeatureConfig', on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='feature',
            name='modified_by',
            field=audit_log.models.fields.LastUserField(related_name='modified_analysis_feature_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='datasetconfig',
            name='record_set_configs',
            field=models.ManyToManyField(related_name='+', to='analysis.RecordSetConfig'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='datasetconfig',
            name='user',
            field=models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dataset',
            name='default_config',
            field=models.OneToOneField(related_name='+', null=True, blank=True, to='analysis.DataSetConfig', on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dataset',
            name='modified_by',
            field=audit_log.models.fields.LastUserField(related_name='modified_analysis_dataset_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dataset',
            name='user',
            field=models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dataconfig',
            name='data_set_configs',
            field=models.ManyToManyField(related_name='+', to='analysis.DataSetConfig'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dataconfig',
            name='modified_by',
            field=audit_log.models.fields.LastUserField(related_name='modified_analysis_dataconfig_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dataconfig',
            name='user',
            field=models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='analysisprofile',
            name='features',
            field=models.ManyToManyField(to='analysis.Feature', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='analysisprofile',
            name='modified_by',
            field=audit_log.models.fields.LastUserField(related_name='modified_analysis_analysisprofile_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='analysisconfig',
            name='analysis_profile',
            field=models.ForeignKey(related_name='+', to='analysis.AnalysisProfile', on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='analysisconfig',
            name='created_by',
            field=audit_log.models.fields.CreatingUserField(related_name='created_analysis_analysisconfig_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='analysisconfig',
            name='data_config',
            field=models.ForeignKey(related_name='+', blank=True, to='analysis.DataConfig', null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='analysisconfig',
            name='feature_configs',
            field=models.ManyToManyField(related_name='+', null=True, to='analysis.FeatureConfig', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='analysisconfig',
            name='modified_by',
            field=audit_log.models.fields.LastUserField(related_name='modified_analysis_analysisconfig_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='analysisconfig',
            name='partition_set_config',
            field=models.ForeignKey(related_name='+', blank=True, to='analysis.PartitionSetConfig', null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='analysisconfig',
            name='user',
            field=models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='analysis',
            name='account',
            field=models.ForeignKey(to='campaign.Account', null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='analysis',
            name='analysis_config',
            field=models.ForeignKey(related_name='+', to='analysis.AnalysisConfig', null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='analysis',
            name='partition_set',
            field=models.ForeignKey(related_name='+', to='analysis.PartitionSetConfig', null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='analysis',
            name='event',
            field=models.ForeignKey(related_name='+', to='campaign.Event', null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='analysis',
            name='timestamp',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, null=True, editable=False, db_index=True, blank=True),
            preserve_default=True,
        ),
        # Create all configuration objects for legacy analyses
        migrations.RunPython(create_legacy_analysis_configs,
                             reverse_code=lambda x, y: True),
        # Populate the account field
        migrations.RunPython(set_analyses_accounts,
                             reverse_code=lambda x, y: True),
        # Make the account field non-nullable
        migrations.AlterField(
            model_name='analysis',
            name='account',
            field=models.ForeignKey(to='campaign.Account', on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
    ]
