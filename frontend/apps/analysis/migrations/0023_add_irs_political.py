# -*- coding: utf-8 -*-


from django.db import migrations


def add_irs_sources(apps, schema_editor):
    DataSet = apps.get_model('analysis', 'DataSet')
    RecordSet = apps.get_model('analysis', 'RecordSet')
    RecordResource = apps.get_model('analysis', 'RecordResource')
    ResourceInstance = apps.get_model('analysis', 'ResourceInstance')
    ResourceConfig = apps.get_model('analysis', 'ResourceConfig')
    RecordSetConfig = apps.get_model('analysis', 'RecordSetConfig')
    DataSetConfig = apps.get_model('analysis', 'DataSetConfig')

    ds_title = 'IRS Data'
    if not DataSet.objects.filter(title=ds_title).exists():
        irs_ds = DataSet.objects.create(
            title=ds_title,
            description='Parent data set for IRS record sets',
            module_name='frontend.apps.analysis.sources.irs.irs',
            class_name='IRS')
        irs_rs = RecordSet.objects.create(
            title='IRS Political Contributions',
            module_name='frontend.apps.analysis.sources.irs.irs',
            class_name='PoliticalContributions',
            data_set=irs_ds)
        irs_rr = RecordResource.objects.create(
            title='MongoDB IRS Political Contributions',
            module_name='frontend.apps.analysis.sources.irs.backends.mongodb',
            class_name='PoliticalContributions',
            record_set=irs_rs)
        irs_ri = ResourceInstance.objects.create(
            title='MongoDB IRS Political Contributions "irs_political" '
                  'collection',
            identifier='irs_political',
            record_resource=irs_rr)
        irs_rc = ResourceConfig.objects.create(
            title='IRS Political Contributions - MongoDB Configuration',
            record_resource=irs_rr,
            resource_instance=irs_ri)
        irs_rsc = RecordSetConfig.objects.create(
            title='IRS Political Contributions Configuration',
            record_set=irs_rs,
            resource_config=irs_rc)
        irs_org_rs = RecordSet.objects.create(
            title='IRS Political Organizations',
            module_name='frontend.apps.analysis.sources.base',
            class_name='RecordSet',
            fact_type='ENT',
            data_set=irs_ds)
        irs_org_rr = RecordResource.objects.create(
            title='MongoDB IRS Political Organizations',
            module_name='frontend.apps.analysis.sources.irs.backends.mongodb',
            class_name='PoliticalOrganizations',
            record_set=irs_org_rs)
        irs_org_ri = ResourceInstance.objects.create(
            title='MongoDB IRS Political Organizations '
                  '"irs_political_organizations" collection',
            identifier='irs_political_organizations',
            record_resource=irs_org_rr)
        irs_org_rc = ResourceConfig.objects.create(
            title='IRS Political Organizations - MongoDB Configuration',
            record_resource=irs_org_rr,
            resource_instance=irs_org_ri)
        irs_org_rsc = RecordSetConfig.objects.create(
            title='IRS Political Organizations Configuration',
            record_set=irs_org_rs,
            resource_config=irs_org_rc)
        irs_dsc = DataSetConfig.objects.create(
            title='IRS Data Configuration',
            data_set=irs_ds)
        irs_dsc.record_set_configs.add(irs_rsc)
        irs_dsc.record_set_configs.add(irs_org_rsc)
        irs_dsc.save()


def add_irs_features(apps, schema_editor):
    SignalSetConfig = apps.get_model('analysis', 'SignalSetConfig')
    SignalActionConfig = apps.get_model('analysis', 'SignalActionConfig')
    SignalSetsActions = apps.get_model('analysis', 'SignalSetsActions')
    Feature = apps.get_model('analysis', 'Feature')
    FeatureConfig = apps.get_model('analysis', 'FeatureConfig')
    RecordSetConfig = apps.get_model('analysis', 'RecordSetConfig')

    match_feature = Feature.objects.get(title='Matching signals')
    irs_rsc = RecordSetConfig.objects.filter(
        title='IRS Political Contributions Configuration')[0]
    client_rsc = RecordSetConfig.objects.filter(
        title='Test Client Individual Contributions Configuration')[0]

    # --- Common SignalActions ---
    # Find common SignalActionConfigs, adding titles for later convenience
    search_sac = SignalActionConfig.objects.raw('''
      SELECT * FROM analysis_signalactionconfig
      WHERE definition LIKE '{"module":"frontend.apps.analysis.analyses.base","class":"RecordSearch","target_type":"CONTACT","id_field_name":"_id"}'
      ''')[0]
    search_sac.title = 'RecordSearch'
    search_sac.save()
    party_multiplier_sac = SignalActionConfig.objects.filter(
        title='PartyMultiplier Template')[0]
    partition_sac = SignalActionConfig.objects.raw('''
      SELECT * FROM analysis_signalactionconfig
      WHERE definition LIKE '{"module":"frontend.apps.analysis.analyses.base","class":"PartitionSignals"}'
      ''')[0]
    partition_sac.title = 'PartitionSignals'
    partition_sac.save()
    apply_giving_sac = SignalActionConfig.objects.raw('''
      SELECT * FROM analysis_signalactionconfig
      WHERE definition LIKE '{"module":"frontend.apps.analysis.analyses.person_contrib","class":"ApplyGivingFactors"}'
      ''')[0]
    apply_giving_sac.title = 'Apply Giving Factors'
    apply_giving_sac.save()
    score_political_sac = SignalActionConfig.objects.raw('''
      SELECT * FROM analysis_signalactionconfig
      WHERE definition LIKE '{"module":"frontend.apps.analysis.analyses.person_contrib","class":"ScorePoliticalContribution"}'
      ''')[0]
    score_political_sac.title = 'ScorePoliticalContribution'
    score_political_sac.save()
    simple_spectrum_sac = SignalActionConfig.objects.raw('''
      SELECT * FROM analysis_signalactionconfig
      WHERE definition LIKE '{"module":"frontend.apps.analysis.analyses.person_contrib","class":"SimplePoliticalSpectrum"}'
      ''')[0]
    simple_spectrum_sac.title = 'SimplePoliticalSpectrum'
    simple_spectrum_sac.save()
    # --- Common SignalActions ---

    irs_client_title = 'IRS Client Contributions'
    irs_fc_title = 'IRS Political Contributions'
    if not (FeatureConfig.objects.filter(title=irs_fc_title).exists()
        and FeatureConfig.objects.filter(title=irs_client_title).exists()):

        # --- IRS Contributions Feature ---

        # --- IRS Contributions Signal Actions ---
        irs_tfv_sac = SignalActionConfig.objects.create(
            title='IRS TargetFieldValueFilter',
            definition={
                "target_field":"states",
                "record_field":"PrimaryState",
                "class":"TargetFieldValueFilter",
                "module":"frontend.apps.analysis.analyses.signal_actions",
                "additional_values": [
                    ""
                ]
            })
        irs_date_sac = SignalActionConfig.objects.create(
            title='IRS DateTransform',
            definition={
                "class":"DateTransform",
                "module":"frontend.apps.analysis.analyses.signal_actions",
                "field":"ReceivedOn_Cleansed"
            })
        irs_field_copy_sac = SignalActionConfig.objects.create(
            title='IRS FieldCopy',
            definition={
                "class":"FieldCopy",
                "module":"frontend.apps.analysis.analyses.signal_actions",
                "mappings":[
                    {
                        "source_field":"Amount_Cleansed",
                        "target_field":"trans_amt"
                    },
                    {
                        "source_field":"ReceivedOn_Cleansed",
                        "target_field":"date"
                    },
                    {
                        "source_field":"analysis_party",
                        "target_field":"party"
                    }
                ]
            })
        irs_calculate_sac = SignalActionConfig.objects.create(
            title='IRS Calculate Giving',
            definition={
                "class":"IRSCalculateGiving",
                "module":"frontend.apps.analysis.analyses.signal_actions"
            })
        irs_reduce_sac = SignalActionConfig.objects.create(
            title='IRS Reduce Signals',
            definition={
                "sum_fields":
                    [
                        "adjusted_giving",
                        "democrat_count",
                        "democrat_giving",
                        "giving",
                        "net_democrat",
                        "net_republican",
                        "other_giving",
                        "republican_count",
                        "republican_giving",
                        "score"
                    ],
                "class":"ReduceSignals",
                "module":"frontend.apps.analysis.analyses.signal_actions",
                "list_fields":
                    [
                        "match"
                    ]
            })
        # --- IRS Contributions Signal Actions ---

        irs_ssc = SignalSetConfig.objects.create(
            title='IRS Political Contributions',
            record_set_config=irs_rsc)
        irs_signal_actions = [
            search_sac, irs_tfv_sac, irs_date_sac, irs_field_copy_sac,
            party_multiplier_sac, partition_sac, irs_calculate_sac,
            apply_giving_sac, score_political_sac, irs_reduce_sac,
            simple_spectrum_sac]
        partitioned = False
        for i, signal_action in enumerate(irs_signal_actions):
            if signal_action == partition_sac:
                partitioned = True
            SignalSetsActions.objects.create(
                signal_set_config=irs_ssc,
                signal_action_config=signal_action,
                partitioned=partitioned,
                order=(i + 1) * 10)
        irs_fsc = FeatureConfig.objects.create(
            title=irs_fc_title,
            feature=match_feature, partitioned=True)
        irs_fsc.signal_set_configs.add(irs_ssc)
        # --- IRS Contributions Feature ---

        # --- IRS Client Contributions Feature ---

        # --- IRS Client Contributions Signal Actions ---
        irs_client_field_copy_sac = SignalActionConfig.objects.create(
            title='IRS Client Contribution FieldCopy',
            definition={
                "class":"FieldCopy",
                "module":"frontend.apps.analysis.analyses.signal_actions",
                "mappings":[
                    {
                        "source_field":"Amount_Cleansed",
                        "target_field":"trans_amt"
                    },
                    {
                        "source_field":"ReceivedOn_Cleansed",
                        "target_field":"date"
                    }
                ]
            })
        irs_add_fields_sac = SignalActionConfig.objects.create(
            title='IRS AddFields Example',
            definition={
                "class":"AddFields",
                "module":"frontend.apps.analysis.analyses.signal_actions",
                "fields":[
                    {
                        "field_name": "party",
                        "field_value": "REP"
                    }
                ]
            })
        irs_client_multiplier = SignalActionConfig.objects.create(
            title='IRS Client Contribution Multiplier',
            definition={
                "class":"AddMultiplier",
                "module":"frontend.apps.analysis.analyses.signal_actions",
                "name": "IRS Client Contribution Multiplier",
                "value": 5
            })
        # --- IRS Client Contributions Signal Actions ---

        irs_client_ssc = SignalSetConfig.objects.create(
            title='IRS Client Contributions',
            record_set_config=client_rsc)
        irs_client_signal_actions = [
            search_sac, irs_tfv_sac, irs_date_sac, irs_client_field_copy_sac,
            irs_add_fields_sac, party_multiplier_sac, irs_client_multiplier,
            partition_sac, irs_calculate_sac, apply_giving_sac,
            score_political_sac, irs_reduce_sac, simple_spectrum_sac]
        partitioned = False
        for i, signal_action in enumerate(irs_client_signal_actions):
            if signal_action == partition_sac:
                partitioned = True
            SignalSetsActions.objects.create(
                signal_set_config=irs_client_ssc,
                signal_action_config=signal_action,
                partitioned=partitioned,
                order=(i + 1) * 10)
        irs_client_fc = FeatureConfig.objects.create(
            title=irs_client_title,
            feature=match_feature, partitioned=True)
        irs_client_fc.signal_set_configs.add(irs_client_ssc)
    # --- IRS Client Contributions Feature ---

class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0022_add_contribution_spread'),
    ]

    operations = [
        migrations.RunPython(add_irs_sources,
                             lambda x, y: True),
        migrations.RunPython(add_irs_features,
                             lambda x, y: True),
    ]
