# -*- coding: utf-8 -*-


from django.db import models, migrations

def forwards_func(apps, schema_editor):
    FeatureResult = apps.get_model("analysis", "FeatureResult")
    base_feature_query = FeatureResult.objects.filter(m2mresults__isnull=False)
    feature_count = base_feature_query.count()
    chunk_size = 10000
    chunks = int(feature_count / chunk_size)
    if feature_count % chunk_size:
        chunks += 1

    for step in range(chunks):
        features = base_feature_query\
                [step * chunk_size:(step + 1) * chunk_size]\
                .prefetch_related('m2mresults')

        for feature in features:
            results = feature.m2mresults.all()
            feature.result = results[0]
            feature.save()

            if results.count() > 1:
                for result in results[1:]:
                    feature.pk = None
                    feature.result_id = result.id
                    feature.save(force_insert=True, force_update=False)


def backwards_func(apps, schema_editor):
    Result = apps.get_model("analysis", "Result")

    results = Result.objects\
            .filter(m2mfeatures__isnull=True, features__isnull=False)\
            .prefetch_related('features')

    for result in results:
        result.m2mfeatures.add(*list(result.features.all()))


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0012_add_featureresult_result_fk'),
    ]

    operations = [
        migrations.RunPython(
            forwards_func,
            reverse_code=backwards_func
        ),
    ]
