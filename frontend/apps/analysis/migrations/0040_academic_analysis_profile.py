# -*- coding: utf-8 -*-


from django.db import models, migrations



def setup(apps, schema_editor):
    AnalysisProfile = apps.get_model('analysis', 'AnalysisProfile')

    ap = AnalysisProfile.objects.get(title='Political Candidate Analysis')
    act = ap.default_config_template
    ap.default_config_template_id = None

    act_old_fc = list(act.feature_configs.all())
    act.pk = None
    act.title = 'default analysis config for academia'
    act.save()
    act.feature_configs.set(act_old_fc)

    ap_old_features = list(ap.features.all())
    ap.pk = None
    ap.default_config_template_id = act.pk
    ap.title = 'Academic Analysis'
    ap.save()
    ap.features.set(ap_old_features)

    act.analysis_profile = ap
    act.save()


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0039_update_normalization'),
    ]

    operations = [
        migrations.RunPython(setup,
                             lambda x, y: True),
    ]
