
from django.db import migrations


def forwards(apps, schema_editor):
    from frontend.apps.analysis.analyses import base, signal_actions
    from frontend.libs.utils.model_utils import DynamicClassModelMixin, \
        get_or_clone

    SignalActionConfig = apps.get_model("analysis", "SignalActionConfig")
    SignalSetConfig = apps.get_model("analysis", "SignalSetConfig")
    SignalSetsActions = apps.get_model("analysis", "SignalSetsActions")
    FeatureConfig = apps.get_model("analysis", "FeatureConfig")
    FeatureConfigsSignalSets = apps.get_model("analysis", "FeatureConfigsSignalSets")

    dc = DynamicClassModelMixin()

    # Create FieldCopy SignalActionConfig to copy the fields amount and giving
    #  from the record into the signal
    sac_non_profit_fieldcopy_definition = {
        'mappings': [
            {"source_field": "amount",
             "target_field": "giving"}
        ]}
    dc.dynamic_class = signal_actions.FieldCopy
    sac_non_profit_fieldcopy, created = SignalActionConfig.objects.update_or_create(
        title='Non-Profit FieldCopy', class_name=dc.class_name,
        module_name=dc.module_name, defaults=dict(
            definition=sac_non_profit_fieldcopy_definition))

    # Create NonProfitEnrich SignalActionConfig to partition the contributions
    dc.dynamic_class = signal_actions.NonProfitEnrich
    sac_non_profit_enrich, created = SignalActionConfig.objects.update_or_create(
        title='NonProfit Enrich', class_name=dc.class_name,
        module_name=dc.module_name, defaults=dict(definition={}))

    # Create Nonprofit ReduceSignals SignalActionConfig to perform reduce
    # operations like, sum, median etc.
    action_def = {
        'list_fields': ['match', 'expansion_hit'],
        'sum_fields': ['giving'],
        'median_fields': ['year_0', 'year_1', 'year_2', 'year_3', 'year_4',
                          'year_5']
    }
    dc.dynamic_class = base.ReduceSignals
    sac_non_profit_reducer, created = SignalActionConfig.objects.update_or_create(
        title='Nonprofit ReduceSignals', class_name=dc.class_name,
        module_name=dc.module_name, defaults=dict(definition=action_def))

    # Create SimpleNonProfitScore SignalActionConfig to calculate the ranking
    # score
    dc.dynamic_class = signal_actions.SimpleNonProfitScore
    sac_simple_non_profit_score, created = SignalActionConfig.objects.update_or_create(
        title='SimpleNonProfitScore', class_name=dc.class_name,
        module_name=dc.module_name, defaults=dict(definition={}))

    # Create a SignalSetConfig for the above SignalActionConfigs
    ssc_nonprofit_title = 'Non-profit entity contributions'
    ssc_nonprofit = SignalSetConfig.objects.get(title=ssc_nonprofit_title)

    # Map all the SignalActionConfigs to the newly created SignalSetConfig
    # using SignalSetsActions
    SignalSetsActions.objects.get_or_create(
        signal_set_config=ssc_nonprofit,
        signal_action_config=sac_non_profit_fieldcopy,
        order=20)

    SignalSetsActions.objects.get_or_create(
        signal_set_config=ssc_nonprofit,
        signal_action_config=sac_non_profit_enrich,
        order=30)

    # A SignalSetsActions for sac_non_profit_reducer already exists and its
    # order is 200. Create the next SignalSetsActions with order greater than
    # 200

    SignalSetsActions.objects.get_or_create(
        signal_set_config=ssc_nonprofit,
        signal_action_config=sac_simple_non_profit_score,
        order=210)

    # Create NonProfitSummary SignalActionConfig for calculating Summary
    dc.dynamic_class = signal_actions.NonProfitSummary
    sac_non_profit_summary, created = SignalActionConfig.objects.update_or_create(
        title='NonProfitSummary', class_name=dc.class_name,
        module_name=dc.module_name, defaults=dict(definition={}))

    # Create a SignalSetConfig for the above SignalActionConfig
    ssc_non_profit_summary, created = SignalSetConfig.objects.get_or_create(
        title='Non-profit Summary')

    ssc_non_profit_summary.signal_set_configs.add(ssc_nonprofit)

    # Map the SignalActionConfigs to the SignalSetConfig using
    # SignalSetsActions
    ssa_non_profit, created = SignalSetsActions.objects.get_or_create(
        signal_set_config=ssc_non_profit_summary,
        signal_action_config=sac_non_profit_summary,
        order=100)

    # Create a FeatureConfig for the Nonprofit Summary SignalSetConfig
    fc_non_profit_title = 'Non-profit Entity Summary'
    fc_non_profit = get_or_clone(
        FeatureConfig,
        locate_kwargs=dict(title=fc_non_profit_title),
        base_locate_kwargs=dict(title='Summary signals for Entity'),
        title=fc_non_profit_title)

    # Map the FeatureConfig to the SignalSetConfig using
    # FeatureConfigsSignalSets
    fcss_non_profit, created = FeatureConfigsSignalSets.objects.get_or_create(
        feature_config=fc_non_profit,
        signal_set_config=ssc_non_profit_summary, order=20)


class Migration(migrations.Migration):
    dependencies = [
        ('analysis', '0087_merge_20181114_1353'),
    ]

    operations = [
        migrations.RunPython(forwards, lambda x, y: True),
    ]
