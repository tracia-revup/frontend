# Generated by Django 2.0.5 on 2018-10-01 17:57

from django.conf import settings
from django.db import migrations
from django.db.models import Q


def forwards(apps, schema_editor):
    remove_entity_stuff_stuff_from_normal_act(apps, schema_editor)
    create_purdue_act(apps, schema_editor)
    fix_misp_and_fec_legacy_ssc(apps, schema_editor)
    # ENSURE DEVELOPMENT ENVIRONMENT
    if settings.ENV == 'dev':
        create_purdue_analysis_config_automatically(apps, schema_editor)


def fix_misp_and_fec_legacy_ssc(apps, schema_editor):
    """Fix misp & fec SSC to use RecordSearch instead of RecordFetch"""
    SignalActionConfig = apps.get_model('analysis', 'SignalActionConfig')
    SignalSetsActions = apps.get_model('analysis', 'SignalSetsActions')
    SignalSetConfig = apps.get_model('analysis', 'SignalSetConfig')

    legacy_ssc_titles = [
        "FEC individual contributions",
        "FEC individual contributions :: Legacy Backup",
        "State individual contributions",
        "State individual contributions :: Legacy Backup",
        "Municipal contributions",
    ]
    sac_fetch = SignalActionConfig.objects.get(title='Record Fetch')
    sac_search = SignalActionConfig.objects.get(title='RecordSearch')
    SignalSetsActions.objects.filter(
        signal_action_config=sac_fetch,
        signal_set_config__title__in=legacy_ssc_titles
    ).update(signal_action_config=sac_search)

    # Remove entity version of Muni contribs from Summary signals
    summary_ssc_titles = [
        "Summary signals :: Legacy Backup",
        "Summary signals",
    ]
    old_target_title = "Municipal entity contributions"
    ssc_muni = SignalSetConfig.objects.get(title="Municipal contributions")
    ssc_through_ssc = SignalSetConfig.signal_set_configs.through
    ssc_through_ssc.objects.filter(
        from_signalsetconfig__title__in=summary_ssc_titles,
        to_signalsetconfig__title=old_target_title
    ).update(to_signalsetconfig=ssc_muni)


def remove_entity_stuff_stuff_from_normal_act(apps, schema_editor):
    """Remove Entity FeatureConfigs from all non-entity ACT"""
    AnalysisConfigTemplate = apps.get_model('analysis', 'AnalysisConfigTemplate')
    FeatureConfig = apps.get_model('analysis', 'FeatureConfig')

    fc_entity = list(FeatureConfig.objects.filter(
        title__in=('Person Entity',
                   'Local individual entity contributions')))
    fc_muni = FeatureConfig.objects.get(title='Local individual contributions')

    acts = AnalysisConfigTemplate.objects.exclude(
        Q(title__icontains='entity') |
        Q(title__icontains='legacy'))
    for act in acts:
        act.feature_configs.add(fc_muni)
        act.feature_configs.remove(*fc_entity)


def create_purdue_act(apps, schema_editor):
    """Create a Purdue-specific AnalysisConfigTemplate

    We do this instead of modifying the Academic ACT that already exists
    because there is a bunch of Purdue-specific steps and data that:
    1) We don't know will apply to other university clients
    2) We don't want to accidentally leak Purdue data
    """
    from frontend.libs.utils.model_utils import clone_record, get_or_clone
    DataSetConfig = apps.get_model('analysis', 'DataSetConfig')
    DataConfig = apps.get_model('analysis', 'DataConfig')
    AnalysisConfigTemplate = apps.get_model('analysis', 'AnalysisConfigTemplate')
    FeatureConfig = apps.get_model('analysis', 'FeatureConfig')

    # Create DataConfig for Purdue AC
    dc_purdue_title = 'Purdue Data Configuration'
    dc_purdue = get_or_clone(
        DataConfig,
        locate_kwargs=dict(title=dc_purdue_title),
        base_locate_kwargs=dict(title='Base Data Configuration'),
        title=dc_purdue_title
    )
    dc_purdue.data_set_configs.add(
        DataSetConfig.objects.get(title='Purdue Data Configuration'))

    # Clone Academic ACT to create Purdue ACT
    act_purdue_title = 'Analysis Config Template for Purdue'
    act_purdue = get_or_clone(
        AnalysisConfigTemplate,
        locate_kwargs=dict(title=act_purdue_title),
        base_locate_kwargs=dict(title='default analysis config for academia'),
        title=act_purdue_title,
        data_config=dc_purdue
    )
    # Add Purdue-specific FC to ACT
    act_purdue.feature_configs.add(
        *FeatureConfig.objects.filter(
            title__in=('Purdue Giving',
                       'Purdue Alum Lookup',
                       'Purdue Summary')))
    # Remove 'Summary Signals' FC from Purdue ACT
    try:
        fc_summary = act_purdue.feature_configs.get(title='Summary signals')
    except FeatureConfig.DoesNotExist:
        pass
    else:
        act_purdue.feature_configs.remove(fc_summary)


def create_purdue_analysis_config_automatically(apps, schema_editor):
    """Update the Academic AnalysisProfile to use the Purdue ACT when we detect
    we're in a dev environment"""
    AnalysisProfile = apps.get_model('analysis', 'AnalysisProfile')
    AnalysisConfigTemplate = apps.get_model('analysis', 'AnalysisConfigTemplate')
    act_purdue_title = 'Analysis Config Template for Purdue'
    act_purdue = AnalysisConfigTemplate.objects.get(title=act_purdue_title)
    AnalysisProfile.objects.filter(
        title="Academic Analysis").update(default_config_template=act_purdue)


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0084_dev_env_fixes'),
        ('campaign', '0040_academic-profile-fix'),
    ]

    operations = [
        migrations.RunPython(forwards, lambda x, y: True),
    ]
