# -*- coding: utf-8 -*-


import os
from django.core.management import call_command
from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0032_merge'),
    ]

    operations = [
        migrations.RenameField(
            model_name='linkedinlocation',
            old_name='census_cbsa',
            new_name='cbsa',
        ),
        migrations.AddField(
            model_name='linkedinlocation',
            name='census_cbsas',
            field=models.ManyToManyField(related_name='location', to='analysis.CensusCBSA', null=True, blank=True),
            preserve_default=True,
        )
    ]
