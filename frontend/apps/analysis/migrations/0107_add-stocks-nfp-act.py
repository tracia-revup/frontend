# Generated by Django 2.0.5 on 2018-12-13 12:04

from django.db import migrations


def forwards(apps, schema_editor):
    AnalysisConfigTemplate = apps.get_model("analysis",
                                            "AnalysisConfigTemplate")
    FeatureConfig = apps.get_model("analysis", "FeatureConfig")
    DataSetConfig = apps.get_model("analysis", "DataSetConfig")

    act_non_profit_title = 'NonProfit Entity Analysis Config Template'
    act = AnalysisConfigTemplate.objects.get(title=act_non_profit_title)
    dsc = DataSetConfig.objects.get(title='Stocks Data Configuration')
    fc = FeatureConfig.objects.get(title='Stocks entities')
    act.feature_configs.add(fc)
    act.data_config.data_set_configs.add(dsc)


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0106_stocks-entity'),
    ]

    operations = [
        migrations.RunPython(forwards, lambda x, y: True),
    ]
