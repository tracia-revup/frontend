# coding: utf-8


from django.db import migrations
from django.conf import settings


def forwards(apps, schema_editor):
    for c in ['analysis', 'attendee', 'campaign', 'contact', 'django_session',
              'external_data_cache', 'event', 'group', 'rev_up_user_token',
              'user', 'user_social_auth']:
        settings.MONGO_CLIENT.revup.drop_collection(c)


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0004_auto_20150714_1157'),
    ]

    operations = [
        migrations.RunPython(forwards, reverse_code=lambda x, y: None)
    ]
