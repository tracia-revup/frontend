# -*- coding: utf-8 -*-


from django.db import migrations
from frontend.apps.authorize.models import RevUpUser


def setup(apps, schema_config):

    AnalysisConfig = apps.get_model("analysis", "AnalysisConfig")
    PartitionSetConfig = apps.get_model("analysis", "PartitionSetConfig")
    base_partition_set = PartitionSetConfig.objects.filter(
        title='Base Partition Set').first()

    (data_conf, features) = setup_data_conf_and_features(apps)

    candidate_profile = create_political_candidate_analysis_profile(apps)
    candidate_config = AnalysisConfig.objects.create(
        title='Default analysis config for political candidate campaign',
        analysis_profile=candidate_profile,
        data_config=data_conf,
        partition_set_config=base_partition_set)
    committee_profile = create_political_committee_analysis_profile(apps)
    committee_config = AnalysisConfig.objects.create(
        title='Default analysis config for political committee campaign',
        analysis_profile=committee_profile,
        data_config=data_conf,
        partition_set_config=base_partition_set)

    for (config, profile) in [(candidate_config, candidate_profile),
                              (committee_config, committee_profile)]:
        for feature_conf in features:
            config.feature_configs.add(feature_conf)
        config.save()
        profile.default_config = config
        profile.save()


def setup_data_conf_and_features(apps):

    DataSet = apps.get_model('analysis', 'DataSet')
    RecordSet = apps.get_model('analysis', 'RecordSet')
    RecordResource = apps.get_model('analysis', 'RecordResource')
    ResourceInstance = apps.get_model('analysis', 'ResourceInstance')
    ResourceConfig = apps.get_model('analysis', 'ResourceConfig')
    RecordSetConfig = apps.get_model('analysis', 'RecordSetConfig')
    DataSetConfig = apps.get_model('analysis', 'DataSetConfig')
    DataConfig = apps.get_model('analysis', 'DataConfig')

    ##### FEC DATA #####
    fec_ds = DataSet.objects.create(
        title='FEC Data',
        description='Default system-wide FEC data (initial configuration)',
        module_name='frontend.apps.analysis.sources.fec.fec',
        class_name='FEC')
    fec_indiv = RecordSet.objects.create(
        title='FEC Individual Contributions',
        module_name='frontend.apps.analysis.sources.fec.fec',
        class_name='IndividualContributions',
        data_set=fec_ds)
    fec_indiv_mongo = RecordResource.objects.create(
        title='MongoDB FEC Individual Contributions',
        module_name='frontend.apps.analysis.sources.fec.backends.mongodb',
        class_name='IndividualContributions',
        record_set=fec_indiv)
    fec_indiv_mongo_instance = ResourceInstance.objects.create(
        title='MongoDB FEC Individual Contributions "records" collection',
        identifier='records',
        record_resource=fec_indiv_mongo)
    fec_indiv_mongo_conf = ResourceConfig.objects.create(
        title='FEC Individual Contributions - MongoDB Configuration',
        record_resource=fec_indiv_mongo,
        resource_instance=fec_indiv_mongo_instance)
    fec_indiv_conf = RecordSetConfig.objects.create(
        title='FEC Individual Contributions Configuration',
        record_set=fec_indiv,
        resource_config=fec_indiv_mongo_conf)
    fec_ds_conf = DataSetConfig.objects.create(
        title='FEC Configuration',
        data_set=fec_ds)
    fec_ds_conf.record_set_configs.add(fec_indiv_conf)
    fec_ds_conf.save()
    ##### FEC DATA #####

    ##### MISP DATA #####
    misp_ds = DataSet.objects.create(
        title='State Election Data',
        description='Default system-wide state election data (initial '
                    'configuration)',
        module_name='frontend.apps.analysis.sources.misp.misp',
        class_name='MISP')
    misp_indiv = RecordSet.objects.create(
        title='State Election Individual Contributions',
        module_name='frontend.apps.analysis.sources.misp.misp',
        class_name='Contributions',
        data_set=misp_ds)
    misp_indiv_mongo = RecordResource.objects.create(
        title='MongoDB State Election Individual Contributions',
        module_name='frontend.apps.analysis.sources.misp.backends.mongodb',
        class_name='Contributions',
        record_set=misp_indiv)
    misp_indiv_mongo_instance = ResourceInstance.objects.create(
        title='MongoDB State Election Individual Contributions "st" '
              'collection',
        identifier='st',
        record_resource=misp_indiv_mongo)
    misp_indiv_mongo_conf = ResourceConfig.objects.create(
        title='State Election Individual Contributions - MongoDB '
              'Configuration',
        record_resource=misp_indiv_mongo,
        resource_instance=misp_indiv_mongo_instance)
    misp_indiv_conf = RecordSetConfig.objects.create(
        title='State Election Individual Contributions Configuration',
        record_set=misp_indiv,
        resource_config=misp_indiv_mongo_conf)
    misp_ds_conf = DataSetConfig.objects.create(
        title='State Election Data Configuration',
        data_set=misp_ds)
    misp_ds_conf.record_set_configs.add(misp_indiv_conf)
    misp_ds.save()
    ##### MISP DATA #####

    ##### NONPROFIT DATA #####
    non_prof_ds = DataSet.objects.create(
        title='Non-Profit Contribution Data',
        description='Default system-wide non-profit contribution data (initial'
                    ' configuration)',
        module_name='frontend.apps.analysis.sources.non_profit.non_profit',
        class_name='NonProfit')
    non_prof_contrib = RecordSet.objects.create(
        title='Non-Profit Individual Contributions',
        module_name='frontend.apps.analysis.sources.non_profit.non_profit',
        class_name='Contributions',
        data_set=non_prof_ds)
    non_prof_contrib_mongo = RecordResource.objects.create(
        title='MongoDB Non-Profit Individual Contributions',
        module_name=
        'frontend.apps.analysis.sources.non_profit.backends.mongodb',
        class_name='Contributions',
        record_set=non_prof_contrib)
    non_prof_contrib_mongo_instance = ResourceInstance.objects.create(
        title='MongoDB Non-Profit Individual Contributions '
              '"nonprofit_contributions" collection',
        identifier='nonprofit_contributions',
        record_resource=non_prof_contrib_mongo)
    non_prof_contrib_mongo_conf = ResourceConfig.objects.create(
        title='Non-Profit Individual Contributions - MongoDB '
              'Configuration',
        record_resource=non_prof_contrib_mongo,
        resource_instance=non_prof_contrib_mongo_instance)
    non_prof_contrib_conf = RecordSetConfig.objects.create(
        title='Non-Profit Individual Contributions Configuration',
        record_set=non_prof_contrib,
        resource_config=non_prof_contrib_mongo_conf)
    non_prof_ds_conf = DataSetConfig.objects.create(
        title='Non-Profit Data Configuration',
        data_set=non_prof_ds)
    non_prof_ds_conf.record_set_configs.add(non_prof_contrib_conf)
    non_prof_ds.save()
    ##### NONPROFIT DATA #####

    ##### ACADEMIC DATA #####
    academic_ds = DataSet.objects.create(
        title='Academic Contribution Data',
        description='Default system-wide academic contribution data (initial'
                    ' configuration)',
        module_name='frontend.apps.analysis.sources.academic.academic',
        class_name='Academic')
    academic_contrib = RecordSet.objects.create(
        title='Academic Individual Contributions',
        module_name='frontend.apps.analysis.sources.academic.academic',
        class_name='Contributions',
        data_set=academic_ds)
    academic_contrib_mongo = RecordResource.objects.create(
        title='MongoDB Academic Individual Contributions',
        module_name='frontend.apps.analysis.sources.academic.backends.mongodb',
        class_name='Contributions',
        record_set=academic_contrib)
    academic_contrib_mongo_instance = ResourceInstance.objects.create(
        title='MongoDB Academic Individual Contributions '
              '"academic_contributions" collection',
        identifier='academic_contributions',
        record_resource=academic_contrib_mongo)
    academic_contrib_mongo_conf = ResourceConfig.objects.create(
        title='Academic Individual Contributions - MongoDB '
              'Configuration',
        record_resource=academic_contrib_mongo,
        resource_instance=academic_contrib_mongo_instance)
    academic_contrib_conf = RecordSetConfig.objects.create(
        title='Academic Individual Contributions Configuration',
        record_set=academic_contrib,
        resource_config=academic_contrib_mongo_conf)
    academic_ds_conf = DataSetConfig.objects.create(
        title='Academic Data Configuration',
        data_set=academic_ds)
    academic_ds_conf.record_set_configs.add(academic_contrib_conf)
    academic_ds.save()
    ##### ACADEMIC DATA #####

    ##### TEST CLIENT DATA #####
    client_ds = DataSet.objects.create(
        title='Client Contribution Data',
        description='Client contribution data (initial'
                    ' configuration)',
        module_name='frontend.apps.analysis.sources.client.client',
        class_name='Client')
    client_contrib = RecordSet.objects.create(
        title='Client Individual Contributions',
        module_name='frontend.apps.analysis.sources.client.client',
        class_name='Contributions',
        data_set=client_ds)
    client_contrib_mongo = RecordResource.objects.create(
        title='MongoDB Client Individual Contributions',
        module_name='frontend.apps.analysis.sources.client.backends.mongodb',
        class_name='Contributions',
        record_set=client_contrib)
    client_contrib_mongo_instance = ResourceInstance.objects.create(
        title='MongoDB Test Client Individual Contributions "ngptest" '
              'collection',
        identifier='ngptest',
        record_resource=client_contrib_mongo)
    client_contrib_mongo_conf = ResourceConfig.objects.create(
        title='Test Client Individual Contributions - MongoDB '
              'Configuration',
        record_resource=client_contrib_mongo,
        resource_instance=client_contrib_mongo_instance)
    client_contrib_conf = RecordSetConfig.objects.create(
        title='Test Client Individual Contributions Configuration',
        record_set=client_contrib,
        resource_config=client_contrib_mongo_conf)
    client_ds_conf = DataSetConfig.objects.create(
        title='Test Client Data Set Configuration',
        data_set=client_ds)
    client_ds_conf.record_set_configs.add(client_contrib_conf)
    client_ds.save()
    ##### CLIENT DATA #####

    ##### DATA CONFIG #####
    data_conf = DataConfig.objects.create(
        title='Base Data Configuration',
        description='Default system-wide data sets (initial configuration)',
    )
    data_conf.data_set_configs.add(fec_ds_conf)
    data_conf.data_set_configs.add(misp_ds_conf)
    data_conf.data_set_configs.add(non_prof_ds_conf)
    data_conf.data_set_configs.add(academic_ds_conf)
    data_conf.save()
    ##### DATA CONFIG #####

    Feature = apps.get_model('analysis', 'Feature')
    FeatureConfig = apps.get_model('analysis', 'FeatureConfig')
    SignalSetConfig = apps.get_model('analysis', 'SignalSetConfig')
    SignalActionConfig = apps.get_model('analysis', 'SignalActionConfig')
    SignalSetsActions = apps.get_model('analysis', 'SignalSetsActions')

    ##### FEATURES #####
    match_feature = Feature.objects.create(
        title='Matching signals', feature_type='MATCH',
        description='Sets of matching facts from data sets, e.g. federal '
                    'election contributions matching a person.')
    subset_feature = Feature.objects.create(
        title='Matching signal subsets', feature_type='MATCH_SUB',
        allow_multiple=True,
        description='Subsets of matching facts from data sets, e.g. election '
                    'contributions to specific candidates or committees.')
    summary_feature = Feature.objects.create(
        title='Summary features', feature_type='SUM',
        description='Summarizations of or across other features, e.g. giving '
                    'totals, scoring totals and boosts, etc.')
    normalization_feature = Feature.objects.create(
        title='Normalization', feature_type='NORM',
        description='Feature normalizations, e.g. normalizing scores.')
    ##### FEATURES #####

    ##### FEC FEATURES #####
    fec_feature_conf = FeatureConfig.objects.create(
        title='FEC individual contributions',
        feature=match_feature,
        partitioned=True,
        legacy_alias='fec_matches')
    fec_feature_signals = SignalSetConfig.objects.create(
        title='FEC individual contributions',
        record_set_config=fec_indiv_conf)

    ##### FEC SIGNAL ACTIONS #####
    search_def = {
        'module': 'frontend.apps.analysis.analyses.base',
        'class': 'RecordSearch',
        'target_type': 'CONTACT',
        'id_field_name': '_id',
    }
    search_action = SignalActionConfig.objects.create(
        definition=search_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=fec_feature_signals,
        signal_action_config=search_action,
        order=10)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'FieldUniqueCountFilter',
        'field': 'ZIP_CODE',
        'maximum': 15,
    }
    signal_action = SignalActionConfig.objects.create(
        title='FEC FieldUniqueCountFilter',
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=fec_feature_signals,
        signal_action_config=signal_action,
        order=20)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'FECEnrich',
        'apply_overrides': True,
    }
    signal_action = SignalActionConfig.objects.create(
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=fec_feature_signals,
        signal_action_config=signal_action,
        order=25)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'TargetFieldValueFilter',
        'target_field': 'states',
        'record_field': 'STATE',
        'additional_values': [],
    }
    signal_action = SignalActionConfig.objects.create(
        title='FEC TargetFieldValueFilter',
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=fec_feature_signals,
        signal_action_config=signal_action,
        order=30)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'FECMarkSelfFunded',
    }
    signal_action = SignalActionConfig.objects.create(
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=fec_feature_signals,
        signal_action_config=signal_action,
        order=40)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'FECDateTransform',
    }
    signal_action = SignalActionConfig.objects.create(
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=fec_feature_signals,
        signal_action_config=signal_action,
        order=50)

    # Setting no party as a default
    party_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'PartyMultiplier',
        'party': 'Other'
    }
    party_action = SignalActionConfig.objects.create(
        title='PartyMultiplier Template',
        definition=party_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=fec_feature_signals,
        signal_action_config=party_action,
        order=60)

    # client-specific signal action config to be added but not assigned
    action_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'CommitteeMultipliers',
        'id_field_name': '_id',
        'filter_field_name': 'CMTE_ID',
        'filters': [
            {
                'label': 'DNC',
                'ids': [
                    'C00010603',
                ],
                'multiplier': -20,
            },
            {
                'label': 'Priorities USA',
                'ids': [
                    'C00495861',
                ],
                'multiplier': -20,
            },
            {
                'label': 'RNC',
                'ids': [
                    'C00003418',
                ],
                'multiplier': 1.5,
            },
            {
                'label': 'DCCC',
                'ids': [
                    'C00000935',
                ],
                'multiplier': -15,
            },
            {
                'label': 'House Majority PAC',
                'ids': [
                    'C00495028',
                ],
                'multiplier': -15,
            },
            {
                'label': 'NRCC',
                'ids': [
                    'C00075820',
                ],
                'multiplier': 1.2,
            },
            {
                'label': 'DSCC',
                'ids': [
                    'C00042366',
                ],
                'multiplier': -10,
            },
            {
                'label': 'Senate Majority PAC',
                'ids': [
                    'C00484642',
                ],
                'multiplier': -10,
            },
            {
                'label': 'NRSC',
                'ids': [
                    'C00027466',
                ],
                'multiplier': 1.2,
            },
        ],
    }
    signal_action = SignalActionConfig.objects.create(
        definition=action_def)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'FECOfficeMultiplier',
        'office': 'H'
    }
    signal_action = SignalActionConfig.objects.create(
        title='FECOfficeMultiplier Template',
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=fec_feature_signals,
        signal_action_config=signal_action,
        order=66)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'AmountTransform',
        'field': 'TRANSACTION_AMT',
    }
    signal_action = SignalActionConfig.objects.create(
        title='FEC AmountTransform',
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=fec_feature_signals,
        signal_action_config=signal_action,
        order=70)

    partition_def = {
        'module': 'frontend.apps.analysis.analyses.base',
        'class': 'PartitionSignals',
    }
    partition_action = SignalActionConfig.objects.create(
        definition=partition_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=fec_feature_signals,
        signal_action_config=partition_action,
        order=80,
        partitioned=True)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'FECCalculateGiving',
    }
    signal_action = SignalActionConfig.objects.create(
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=fec_feature_signals,
        signal_action_config=signal_action,
        order=90,
        partitioned=True)

    factors_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'ApplyGivingFactors',
    }
    factors_action = SignalActionConfig.objects.create(
        definition=factors_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=fec_feature_signals,
        signal_action_config=factors_action,
        order=100,
        partitioned=True)

    score_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'ScorePoliticalContribution',
    }
    score_action = SignalActionConfig.objects.create(
        definition=score_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=fec_feature_signals,
        signal_action_config=score_action,
        order=110,
        partitioned=True)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.base',
        'class': 'ReduceSignals',
        'list_fields': ['match'],
        'sum_fields': ['score', 'giving', 'adjusted_giving',
                       'net_democrat', 'net_republican', 'democrat_count',
                       'republican_count']
    }
    signal_action = SignalActionConfig.objects.create(
        title='FEC ReduceSignals',
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=fec_feature_signals,
        signal_action_config=signal_action,
        order=120,
        partitioned=True)

    spectrum_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'SimplePoliticalSpectrum',
    }
    spectrum_action = SignalActionConfig.objects.create(
        definition=spectrum_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=fec_feature_signals,
        signal_action_config=spectrum_action,
        order=130,
        partitioned=True)
    ##### FEC SIGNAL ACTIONS #####

    fec_feature_conf.signal_set_configs.add(fec_feature_signals)
    ##### FEC FEATURES #####

    ##### MISP FEATURES #####
    state_feature_conf = FeatureConfig.objects.create(
        title='State individual contributions',
        feature=match_feature,
        partitioned=True,
        legacy_alias='state_matches')
    state_feature_signals = SignalSetConfig.objects.create(
        title='State individual contributions',
        record_set_config=misp_indiv_conf)

    ##### MISP SIGNAL ACTIONS #####
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=state_feature_signals,
        signal_action_config=search_action, order=10)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'FieldUniqueCountFilter',
        'field': 'SAT_Zip',
        'maximum': 15,
    }
    signal_action = SignalActionConfig.objects.create(
        title='State FieldUniqueCountFilter',
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=state_feature_signals,
        signal_action_config=signal_action,
        order=20)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'MISPEnrich',
        'apply_overrides': True,
    }
    signal_action = SignalActionConfig.objects.create(
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=state_feature_signals,
        signal_action_config=signal_action,
        order=25)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'TargetFieldValueFilter',
        'target_field': 'states',
        'record_field': 'SAT_State',
        'additional_values': [],
    }
    signal_action = SignalActionConfig.objects.create(
        title='State TargetFieldValueFilter',
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=state_feature_signals,
        signal_action_config=signal_action,
        order=30)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'MISPMarkSelfFunded',
    }
    signal_action = SignalActionConfig.objects.create(
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=state_feature_signals,
        signal_action_config=signal_action,
        order=40)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'MISPDateTransform',
    }
    signal_action = SignalActionConfig.objects.create(
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=state_feature_signals,
        signal_action_config=signal_action,
        order=50)

    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=state_feature_signals,
        signal_action_config=party_action,
        order=60)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'StateOfficeMultiplier',
        'office': 'G',
        'office_map': {
            "G": {
                "G": 1.5,
            },
        }
    }
    signal_action = SignalActionConfig.objects.create(
        title='StateOfficeMultiplier Template',
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=state_feature_signals,
        signal_action_config=signal_action,
        order=65)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'AmountTransform',
        'field': 'CFS_Amount',
    }
    signal_action = SignalActionConfig.objects.create(
        title='State AmountTransform',
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=state_feature_signals,
        signal_action_config=signal_action,
        order=70)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'MISPGrouping',
        'minimum_length': 3,
        'maximum_amount': 250,
    }
    signal_action = SignalActionConfig.objects.create(
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=state_feature_signals,
        signal_action_config=signal_action,
        order=75)

    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=state_feature_signals,
        signal_action_config=partition_action,
        order=80,
        partitioned=True)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'MISPCalculateGiving',
    }
    signal_action = SignalActionConfig.objects.create(
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=state_feature_signals,
        signal_action_config=signal_action,
        order=90,
        partitioned=True)

    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=state_feature_signals,
        signal_action_config=factors_action,
        order=100,
        partitioned=True)

    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=state_feature_signals,
        signal_action_config=score_action,
        order=110,
        partitioned=True)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.base',
        'class': 'ReduceSignals',
        'list_fields': ['match'],
        'sum_fields': ['score', 'giving', 'adjusted_giving',
                       'net_democrat', 'net_republican', 'democrat_count',
                       'republican_count']
    }
    signal_action = SignalActionConfig.objects.create(
        title='State ReduceSignals',
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=state_feature_signals,
        signal_action_config=signal_action,
        order=120,
        partitioned=True)

    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=state_feature_signals,
        signal_action_config=spectrum_action,
        order=130,
        partitioned=True)
    ##### MISP SIGNAL ACTIONS #####

    state_feature_conf.signal_set_configs.add(state_feature_signals)
    ##### MISP FEATURES #####

    ##### NONPROFIT FEATURES #####
    nonprofit_feature_conf = FeatureConfig.objects.create(
        title='Nonprofit contributions',
        feature=match_feature,
        partitioned=True,
        legacy_alias='nonprofit_contribs')
    nonprofit_feature_signals = SignalSetConfig.objects.create(
        title='Nonprofit contributions',
        record_set_config=non_prof_contrib_conf)

    ##### NONPROFIT SIGNAL ACTIONS #####
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=nonprofit_feature_signals,
        signal_action_config=search_action,
        order=10)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.base',
        'class': 'ReduceSignals',
        'list_fields': ['match'],
        'sum_fields': []
    }
    signal_action = SignalActionConfig.objects.create(
        title='Nonprofit ReduceSignals',
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=nonprofit_feature_signals,
        signal_action_config=signal_action,
        order=200)
    ##### NONPROFIT SIGNAL ACTIONS #####

    nonprofit_feature_conf.signal_set_configs.add(nonprofit_feature_signals)
    ##### NONPROFIT FEATURES #####

    ##### ACADEMIC FEATURES #####
    academic_feature_conf = FeatureConfig.objects.create(
        title='Academic contributions',
        feature=match_feature,
        partitioned=True,
        legacy_alias='academic_contribs')
    academic_feature_signals = SignalSetConfig.objects.create(
        title='Academic contributions',
        record_set_config=academic_contrib_conf)

    ##### ACADEMIC SIGNAL ACTIONS #####
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=academic_feature_signals,
        signal_action_config=search_action,
        order=10)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.base',
        'class': 'ReduceSignals',
        'list_fields': ['match'],
        'sum_fields': []
    }
    signal_action = SignalActionConfig.objects.create(
        title='Academic ReduceSignals',
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=academic_feature_signals,
        signal_action_config=signal_action,
        order=200)
    ##### ACADEMIC SIGNAL ACTIONS #####

    academic_feature_conf.signal_set_configs.add(academic_feature_signals)
    ##### ACADEMIC FEATURES #####

    ##### TEST CLIENT FEATURES #####
    # These signal actions created but not automatically assigned to a config
    client_feature_conf = FeatureConfig.objects.create(
        title='Client contributions template',
        feature=match_feature,
        legacy_alias='contrib')
    client_feature_signals = SignalSetConfig.objects.create(
        title='Client contributions template',
        record_set_config=client_contrib_conf)

    ##### TEST CLIENT SIGNAL ACTIONS #####
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=client_feature_signals,
        signal_action_config=search_action,
        order=10)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'TargetFieldValueFilter',
        'target_field': 'states',
        'record_field': 'PrimaryState',
        'additional_values': [''],
    }
    signal_action = SignalActionConfig.objects.create(
        title='Client TargetFieldValueFilter',
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=client_feature_signals,
        signal_action_config=signal_action,
        order=30)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.base',
        'class': 'ReduceSignals',
        'list_fields': ['match'],
        'sum_fields': []
    }
    signal_action = SignalActionConfig.objects.create(
        title='Client ReduceSignals',
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=client_feature_signals,
        signal_action_config=signal_action,
        order=200)
    ##### TEST CLIENT SIGNAL ACTIONS #####

    client_feature_conf.signal_set_configs.add(client_feature_signals)
    # ##### TEST CLIENT FEATURES #####

    ##### OPPONENT FEATURES #####
    # These signal actions created but not automatically assigned to a config
    opponent_feature_conf = FeatureConfig.objects.create(
        title='Opponent contributions template',
        feature=subset_feature,
        legacy_alias='opponent_contrib')
    opponent_feature_signals = SignalSetConfig.objects.create(
        title='Opponent FEC contributions template',
        record_set_config=fec_indiv_conf)

    ##### OPPONENT SIGNAL ACTIONS #####
    # These signal actions created but not automatically assigned to a config
    action_def = {
        'module': 'frontend.apps.analysis.analyses.base',
        'class': 'SubsetFilter',
        'id_field_name': '_id',
        'filter_field_name': 'CMTE_ID',
        'values': [
            'C00462192',
            'C00351379',
            'C00559484',
        ],
    }
    signal_action = SignalActionConfig.objects.create(
        title='Example congressional opponent subset filter',
        definition=action_def)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.base',
        'class': 'SubsetFilter',
        'id_field_name': '_id',
        'filter_field_name': 'CMTE_ID',
        'values': [
            'C00000935',
        ],
    }
    signal_action = SignalActionConfig.objects.create(
        title='Example congressional committee opponent subset filter',
        definition=action_def)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.base',
        'class': 'ReduceSignals',
        'list_fields': ['match'],
        'sum_fields': []
    }
    signal_action = SignalActionConfig.objects.create(
        title='Opponent ReduceSignals',
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=opponent_feature_signals,
        signal_action_config=signal_action,
        order=20)
    ##### OPPONENT SIGNAL ACTIONS #####

    opponent_feature_conf.signal_set_configs.add(opponent_feature_signals)
    ##### OPPONENT FEATURES #####

    ##### SUMMARY FEATURES #####
    # These signal actions created but not automatically assigned to a config
    summary_feature_conf = FeatureConfig.objects.create(
        title='Summary signals',
        feature=summary_feature,
        partitioned=True)
    summary_signals = SignalSetConfig.objects.create(
        title='Summary signals'
    )

    ##### SUMMARY SIGNAL ACTIONS #####
    action_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'SumValues',
    }
    signal_action = SignalActionConfig.objects.create(
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=summary_signals,
        signal_action_config=signal_action,
        order=10)
    summary_signals.signal_set_configs.add(fec_feature_signals)
    summary_signals.signal_set_configs.add(state_feature_signals)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'NameBoost',
        'ethnicity': 'south asian',
        'first_name_boost': 0.05,
        'last_name_boost': 0.05,
        'base_increase': 5.0,
    }
    signal_action = SignalActionConfig.objects.create(
        title='NameBoost Template',
        definition=action_def)

    action_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'StateBoost',
        'state': 'CA',
        'same_state_boost': 0.33,
        'base_increase': 5.0,
    }
    signal_action = SignalActionConfig.objects.create(
        title='StateBoost Template',
        definition=action_def)
    action_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'SpectrumDampen',
        'party': 'Republican',
        'maximum': 50,
    }
    signal_action = SignalActionConfig.objects.create(
        definition=action_def)
    ##### SUMMARY SIGNAL ACTIONS #####

    summary_feature_conf.signal_set_configs.add(summary_signals)
    ##### SUMMARY FEATURES #####

    ##### NORMALIZATION #####
    normalization_feature_conf = FeatureConfig.objects.create(
        title='Normalization signals',
        feature=normalization_feature,
        partitioned=True)
    normalization_signals = SignalSetConfig.objects.create(
        title='Normalization signals'
    )

    action_def = {
        'module': 'frontend.apps.analysis.analyses.person_contrib',
        'class': 'LegacyNormalize',
    }
    signal_action = SignalActionConfig.objects.create(
        definition=action_def)
    signal_actions = SignalSetsActions.objects.create(
        signal_set_config=normalization_signals,
        signal_action_config=signal_action,
        order=10)
    normalization_feature_conf.signal_set_configs.add(normalization_signals)
    ##### NORMALIZATION #####

    return data_conf, [
        fec_feature_conf, state_feature_conf, nonprofit_feature_conf,
        academic_feature_conf, summary_feature_conf,
        normalization_feature_conf
    ]


def create_political_candidate_analysis_profile(apps):
    AnalysisProfile = apps.get_model('analysis', 'AnalysisProfile')
    title = 'Political Candidate Analysis'
    description = ("Analysis profile for political candidates' campaigns.")
    analysis_type = 'PN_CONTRIB_RANK'
    return AnalysisProfile.objects.create(
        title=title,
        description=description,
        analysis_type=analysis_type)


def create_political_committee_analysis_profile(apps):
    AnalysisProfile = apps.get_model('analysis', 'AnalysisProfile')
    title = 'Political Committee Analysis'
    description = ("Analysis profile for political committees' campaigns.")
    analysis_type = 'PN_CONTRIB_RANK'
    return AnalysisProfile.objects.create(
        title=title,
        description=description,
        analysis_type=analysis_type)


def cleanup(apps, schema_editor):
    new_analysis_cleanup(apps)
    new_data_cleanup(apps)
    new_analysis_config_cleanup(apps)


def new_analysis_cleanup(apps):
    Analysis = apps.get_model('analysis', 'Analysis')
    new_analyses = Analysis.objects.filter(analysis_config__isnull=False)
    count = new_analyses.count()
    if count:
        ok_to_delete = input('''

There are %s analyses using the new framework.

Do you want to delete them? If you're unsure, answer 'no'.

    Type 'yes' to continue, or 'no' to cancel: ''' % count)

        if ok_to_delete == 'yes':
            for analysis in new_analyses:
                users = RevUpUser.objects.filter(current_analysis=analysis.id)
                for user in users:
                    user.current_analysis = None
                    user.save()
                analysis_result_cleanup(apps, analysis)
                analysis.delete()


def analysis_result_cleanup(apps, analysis):

    from frontend.apps.analysis.models import NetworkResult
    NetworkResult.objects.filter(_analysis=analysis.id).delete()


def new_data_cleanup(apps):
    DataSet = apps.get_model('analysis', 'DataSet')
    DataConfig = apps.get_model('analysis', 'DataConfig')
    RecordSet = apps.get_model('analysis', 'RecordSet')
    RecordResource = apps.get_model('analysis', 'RecordResource')
    ResourceInstance = apps.get_model('analysis', 'ResourceInstance')
    ResourceConfig = apps.get_model('analysis', 'ResourceConfig')
    RecordSetConfig = apps.get_model('analysis', 'RecordSetConfig')
    DataSetConfig = apps.get_model('analysis', 'DataSetConfig')
    DataSet.objects.all().delete()
    DataConfig.objects.all().delete()
    RecordSet.objects.all().delete()
    RecordResource.objects.all().delete()
    ResourceInstance.objects.all().delete()
    ResourceConfig.objects.all().delete()
    RecordSetConfig.objects.all().delete()
    DataSetConfig.objects.all().delete()


def new_analysis_config_cleanup(apps):
    AnalysisProfile = apps.get_model('analysis', 'AnalysisProfile')
    Feature = apps.get_model('analysis', 'Feature')
    FeatureConfig = apps.get_model('analysis', 'FeatureConfig')
    SignalSetConfig = apps.get_model('analysis', 'SignalSetConfig')
    SignalActionConfig = apps.get_model('analysis', 'SignalActionConfig')
    SignalSetsActions = apps.get_model('analysis', 'SignalSetsActions')
    AnalysisProfile.objects.filter(analysis_type='PN_CONTRIB_RANK').delete()
    Feature.objects.all().delete()
    FeatureConfig.objects.all().delete()
    SignalSetConfig.objects.all().delete()
    SignalActionConfig.objects.all().delete()
    SignalSetsActions.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0002_new_analysis_models'),
        ('campaign', '0017_account_profile'),
    ]

    operations = [
        migrations.RunPython(setup, cleanup),
    ]
