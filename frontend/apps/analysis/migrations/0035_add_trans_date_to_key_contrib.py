# -*- coding: utf-8 -*-


from django.db import models, migrations


def add_trans_date(apps, schema_editor):
    """Add `trans_date` to key contribs signal action config

    Example:
    "combine_fields":[
        {
          "label, is_ally":[
            "match",
            "record_set_config",
            "trans_date"
          ]
        }
    ],

    """
    SignalActionConfig = apps.get_model("analysis", "SignalActionConfig")

    key_contrib = SignalActionConfig.objects.get(
        title="Key Contributions ReduceSignals")

    for cf in key_contrib.definition.get("combine_fields", []):
        try:
            fields = cf["label, is_ally"]
            if len(fields) == 2:
                fields.append("trans_date")
        except KeyError:
            pass

    key_contrib.save()


def remove_trans_date(apps, schema_editor):
    """Remove `trans_date` to key contribs signal action config

    Example:
    "combine_fields":[
        {
          "label, is_ally":[
            "match",
            "record_set_config"
          ]
        }
    ],

    """
    SignalActionConfig = apps.get_model("analysis", "SignalActionConfig")

    key_contrib = SignalActionConfig.objects.get(
        title="Key Contributions ReduceSignals")

    for cf in key_contrib.definition.get("combine_fields", []):
        try:
            fields = cf["label, is_ally"]
            if len(fields) == 3:
                fields.pop()
        except KeyError:
            pass

    key_contrib.save()


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0034_linkedin_cbsa_migrate'),
    ]

    operations = [
        migrations.RunPython(
            add_trans_date, remove_trans_date
        )

    ]
