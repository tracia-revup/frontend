# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0044_auto_20160201_1622'),
    ]

    operations = [
        migrations.AddField(
            model_name='questionset',
            name='staff_only',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
