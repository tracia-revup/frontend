# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion
import frontend.libs.mixins
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('campaign', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Analysis',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(default='RUNNING', max_length=10, blank=True, choices=[('RUNNING', 'RUNNING'), ('COMPLETE', 'COMPLETE')])),
                ('timestamp', models.DateTimeField(default=datetime.datetime.utcnow, db_index=True, blank=True)),
                ('event', models.ForeignKey(to='campaign.Event', on_delete=django.db.models.deletion.CASCADE)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
            },
            bases=(models.Model, frontend.libs.mixins.SimpleUnicodeMixin),
        ),
    ]
