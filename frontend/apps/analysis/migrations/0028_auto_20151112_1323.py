# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.contrib.postgres.fields


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0027_add_view_settings_fk'),
    ]

    operations = [
        migrations.AddField(
            model_name='result',
            name='tags',
            field=django.contrib.postgres.fields.ArrayField(models.CharField(max_length=64), default=[], size=None),
            preserve_default=True,
        ),
        migrations.RunSQL(
            "create index analysis_result_tags_gin on analysis_result using gin(tags)",
            "drop index analysis_result_tags_gin"
        )
    ]
