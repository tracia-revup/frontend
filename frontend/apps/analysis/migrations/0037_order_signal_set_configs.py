# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion


def forwards(apps, schema_editor):
    FeatureConfig = apps.get_model("analysis", "FeatureConfig")
    FeatureConfigsSignalSets = apps.get_model("analysis", "FeatureConfigsSignalSets")

    fc_m2m_ssc_recs = FeatureConfig.signal_set_configs.through.objects.order_by('featureconfig_id').iterator()

    prev_fc = None
    count = None
    for m2mrec in fc_m2m_ssc_recs:
        if m2mrec.featureconfig_id != prev_fc:
            count = 0
        count += 1
        prev_fc = m2mrec.featureconfig_id
        order = count * 10
        FeatureConfigsSignalSets.objects.create(
            feature_config_id=m2mrec.featureconfig_id,
            signal_set_config_id=m2mrec.signalsetconfig_id,
            order=order)


def backwards(apps, schema_editor):
    FeatureConfig = apps.get_model("analysis", "FeatureConfig")
    FeatureConfigsSignalSets = apps.get_model("analysis", "FeatureConfigsSignalSets")

    fc_m2m_ssc = FeatureConfig.signal_set_configs.through
    for rec in FeatureConfigsSignalSets.objects.iterator():
        fc_m2m_ssc.objects.get_or_create(
            featureconfig_id=rec.feature_config_id,
            signalsetconfig_id=rec.signal_set_config_id
        )


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0036_add_prime_feature_type'),
    ]

    operations = [
        migrations.CreateModel(
            name='FeatureConfigsSignalSets',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.IntegerField()),
                ('feature_config', models.ForeignKey(to='analysis.FeatureConfig', on_delete=django.db.models.deletion.PROTECT)),
                ('signal_set_config', models.ForeignKey(to='analysis.SignalSetConfig', on_delete=django.db.models.deletion.PROTECT)),
            ],
            options={
                'ordering': ['order'],
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='featureconfigssignalsets',
            unique_together=set([('feature_config', 'order'), ('feature_config', 'signal_set_config')]),
        ),

        migrations.RunPython(forwards, backwards),

        migrations.RemoveField(
            model_name='featureconfig',
            name='signal_set_configs',
        ),
        migrations.AddField(
            model_name='featureconfig',
            name='signal_set_configs',
            field=models.ManyToManyField(related_name='+', null=True, through='analysis.FeatureConfigsSignalSets', to='analysis.SignalSetConfig', blank=True),
            preserve_default=True,
        ),

    ]
