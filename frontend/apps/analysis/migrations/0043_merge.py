# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0036_reconfigure_key_contribs'),
        ('analysis', '0042_purdue_summary'),
    ]

    operations = [
    ]
