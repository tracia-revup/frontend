# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.utils.timezone
import audit_log.models.fields
import django.db.models.deletion
from django.conf import settings
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('analysis', '0015_recordset_fact_type'),
    ]

    operations = [
        migrations.CreateModel(
            name='AnalysisConfigTemplate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('analysis_profile', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, to='analysis.AnalysisProfile')),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_analysis_analysisconfigtemplate_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('data_config', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='analysis.DataConfig', null=True)),
                ('feature_configs', models.ManyToManyField(related_name='analysis_analysisconfigtemplate_+', null=True, to='analysis.FeatureConfig', blank=True)),
                ('modified_by', audit_log.models.fields.LastUserField(related_name='modified_analysis_analysisconfigtemplate_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by')),
                ('partition_set_config', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='analysis.PartitionSetConfig', null=True)),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='analysisconfig',
            name='analysis_config_template',
            field=models.ForeignKey(blank=True, editable=False, to='analysis.AnalysisConfigTemplate', null=True,
                                    help_text='The template that was used to generate this config',
                                    on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='analysisprofile',
            name='default_config_template',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='analysis.AnalysisConfigTemplate', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='analysisconfig',
            name='feature_configs',
            field=models.ManyToManyField(
                related_name='analysis_analysisconfig_+', null=True,
                to='analysis.FeatureConfig', blank=True),
            preserve_default=True,
        ),
        migrations.RemoveField(
            model_name='feature',
            name='allow_multiple',
        ),
    ]
