# -*- coding: utf-8 -*-


from django.db import models, migrations


def add_breakdown_fields(apps, schema_editor):
    SignalActionConfig = apps.get_model("analysis", "SignalActionConfig")

    # Update FEC and State ReduceSignals to include giving breakdown fields
    for sac_name in ("State ReduceSignals", "FEC ReduceSignals"):
        sac = SignalActionConfig.objects.get(title=sac_name)
        sum_fields = set(sac.definition.get("sum_fields", []))
        sum_fields |= {"democrat_giving", "republican_giving", "other_giving"}
        sac.definition["sum_fields"] = sorted(sum_fields)
        sac.save()


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0021_client_contrib_default_config_find_last_year_giving'),
    ]

    operations = [
        migrations.RunPython(add_breakdown_fields,
                             lambda x, y: True)
    ]
