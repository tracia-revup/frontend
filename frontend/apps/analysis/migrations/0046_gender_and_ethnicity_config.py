# -*- coding: utf-8 -*-


from django.db import models, migrations


def setup_questionset(apps, schema_config):
    from frontend.apps.analysis.questionset_controllers import \
        GenderBoostController, EthnicityBoostController
    from frontend.libs.utils.model_utils import DynamicClassModelMixin
    QuestionSet = apps.get_model("analysis", "QuestionSet")

    # Use the Mixin model to parse the class/module name. Models generated
    # through the apps variable don't seem to have all the methods.
    dc = DynamicClassModelMixin()

    # Create the new QuestionSets
    dc.dynamic_class = GenderBoostController
    QuestionSet.objects.get_or_create(
        title="Gender Boost Config", class_name=dc.class_name,
        module_name=dc.module_name, allow_multiple=False,
        staff_only=True, icon="gender")

    dc.dynamic_class = EthnicityBoostController
    QuestionSet.objects.get_or_create(
        title="Ethnicity Boost Config", class_name=dc.class_name,
        module_name=dc.module_name, allow_multiple=False,
        staff_only=True, icon="ethnicity")


def setup_signal_actions(apps, schema_editor):
    from frontend.apps.analysis.analyses import signal_actions
    from frontend.libs.utils.model_utils import DynamicClassModelMixin
    SignalActionConfig = apps.get_model('analysis', 'SignalActionConfig')

    dc = DynamicClassModelMixin()

    dc.dynamic_class = signal_actions.GenderBoost
    sac_gender_boost, created = SignalActionConfig.objects.get_or_create(
        title='Gender Boost')
    if created:
        sac_gender_boost.definition = {
            'class': dc.class_name,
            'module': dc.module_name}
        sac_gender_boost.save()

    dc.dynamic_class = signal_actions.EthnicityBoost
    sac_ethnicity_boost, created = SignalActionConfig.objects.get_or_create(
        title='Ethnicity Boost')
    if created:
        sac_ethnicity_boost.definition = {
            'class': dc.class_name,
            'module': dc.module_name}
        sac_ethnicity_boost.save()


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0045_questionset_staff_only'),
    ]

    operations = [
        migrations.RunPython(
            setup_questionset, lambda x, y: True
        ),
        migrations.RunPython(
            setup_signal_actions, lambda x, y: True
        ),
    ]
