# -*- coding: utf-8 -*-


from django.db import models, migrations


def cbsa_fk_m2m(apps, schema_editor):
    cbsa_additions = {
        'San Francisco Bay Area': [41860, 41940, 46700],
        'Greater Los Angeles Area': [31100, 40140],
        'Hawaiian Islands': [25900, 26180, 27980, 28180],
        'Raleigh-Durham, North Carolina Area': [20500, 39580],
        'Cleveland/Akron, Ohio Area': [17460, 10420],
        'Odessa/Midland, Texas Area': [36220, 33260],
        'Greensboro/Winston-Salem, North Carolina Area': [24660, 49180],
        'Greater New York City Area': [35620, 14860]
    }
    additions = list(cbsa_additions.keys())
    LinkedInLocation = apps.get_model("analysis", "LinkedInLocation")
    CensusCBSA = apps.get_model("analysis", "CensusCBSA")
    for linkedin_location in LinkedInLocation.objects.all():
        if linkedin_location.cbsa:
            linkedin_location.census_cbsas.add(linkedin_location.cbsa)
            linkedin_location.cbsa = None
        if linkedin_location.name in additions:
            for census_cbsa_id in cbsa_additions[linkedin_location.name]:
                linkedin_location.census_cbsas.add(
                    CensusCBSA.objects.get(pk=census_cbsa_id))
        linkedin_location.save()

def cbsa_m2m_fk(apps, schema_editor):
    LinkedInLocation = apps.get_model("analysis", "LinkedInLocation")
    for linkedin_location in LinkedInLocation.objects.all():
        census_cbsas = linkedin_location.census_cbsas.all()
        if census_cbsas:
            linkedin_location.cbsa = census_cbsas.first()
            linkedin_location.census_cbsas.clear()
            linkedin_location.save()


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0033_linkedin_multi_cbsa'),
    ]

    operations = [
        migrations.RunPython(cbsa_fk_m2m, cbsa_m2m_fk),
    ]
