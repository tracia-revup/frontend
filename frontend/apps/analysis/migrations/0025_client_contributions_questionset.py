# -*- coding: utf-8 -*-


from django.db import models, migrations

def setup_questionset(apps, schema_config):
    from frontend.apps.analysis.questionset_controllers import \
        ClientContributions
    from frontend.libs.utils.model_utils import DynamicClassModelMixin
    QuestionSet = apps.get_model("analysis", "QuestionSet")
    FeatureConfig = apps.get_model("analysis", "FeatureConfig")

    # Use the Mixin model to parse the class/module name. Models generated
    # through the apps variable don't seem to have all the methods.
    dc = DynamicClassModelMixin()
    dc.dynamic_class = ClientContributions

    # Create the new QuestionSet
    entity_dmrc, _ = QuestionSet.objects.get_or_create(
        title="Contribution Limits", class_name=dc.class_name,
        module_name=dc.module_name, allow_multiple=False,
        icon="limit")

    # Get the client contributions template FeatureConfig
    feature_conf = FeatureConfig.objects.get(
        title="Client contributions template")

    # Add the question set
    feature_conf.question_sets.add(entity_dmrc)


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0024_add_celery_task_id_to_analysis_result'),
    ]

    operations = [
        migrations.RunPython(
            setup_questionset, lambda x, y: True
        ),
    ]
