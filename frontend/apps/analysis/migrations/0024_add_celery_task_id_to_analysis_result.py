# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0023_add_irs_political'),
    ]

    operations = [
        migrations.AddField(
            model_name='analysis',
            name='task_id',
            field=models.CharField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='analysis',
            name='status',
            field=models.CharField(default='RUNNING', max_length=10, blank=True, choices=[('RUNNING', 'RUNNING'), ('COMPLETE', 'COMPLETE'), ('ERROR', 'ERROR')]),
            preserve_default=True,
        ),
    ]
