# -*- coding: utf-8 -*-


from django.db import models, migrations

def setup(apps, schema_config):
    Feature = apps.get_model('analysis', 'Feature')
    Feature.objects.get_or_create(
        title='Prime features', feature_type='PRIME',
        description='Prime features are features that should be run first. '
                    'They are run before MATCH features.')


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0035_add_trans_date_to_key_contrib'),
    ]

    operations = [
        migrations.AlterField(
            model_name='feature',
            name='feature_type',
            field=models.CharField(blank=True, max_length=16, choices=[('LOOKUP', 'Field lookups'), ('SPECTRUM', 'Spectrum values'), ('MATCH', 'Matching facts from data sets'), ('MATCH_SUB', 'Subsets of matching facts'), ('SUM', 'Summary features'), ('NORM', 'Feature normalization'), ('PRIME', 'Features that need to be run first')]),
            preserve_default=True,
        ),
        migrations.RunPython(setup, lambda x,y: True),
    ]
