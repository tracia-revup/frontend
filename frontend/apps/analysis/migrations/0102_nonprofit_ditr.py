# Generated by Django 2.0.5 on 2019-03-04 10:48
from django.db import migrations, models


def forwards(apps, schema_editor):
    from frontend.apps.analysis.questionset_controllers import (
        NonprofitDiamondsInTheRoughController)
    from frontend.apps.analysis.analyses import signal_actions
    from frontend.libs.utils.model_utils import DynamicClassModelMixin
    AnalysisConfigTemplate = apps.get_model('analysis',
                                            'AnalysisConfigTemplate')
    SignalSetConfig = apps.get_model('analysis', 'SignalSetConfig')
    SignalActionConfig = apps.get_model('analysis', 'SignalActionConfig')
    SignalSetsActions = apps.get_model('analysis', 'SignalSetsActions')
    Feature = apps.get_model('analysis', 'Feature')
    FeatureConfig = apps.get_model('analysis', 'FeatureConfig')
    FeatureConfigsSignalSets = apps.get_model('analysis',
                                              'FeatureConfigsSignalSets')
    QuestionSet = apps.get_model("analysis", "QuestionSet")

    dc = DynamicClassModelMixin()

    # Create FeatureConfig for Nonprofit DitR
    subset_feature = Feature.objects.get(title='Matching signal subsets',
                                         feature_type='MATCH_SUB')
    fc_nonprofit_ditr, _ = FeatureConfig.objects.get_or_create(
        title='Nonprofit Diamonds in the Rough',
        partitioned=False, feature=subset_feature)

    # Create QuestionSet for Nonprofit DitR, and add it to Nonprofit DitR
    # FeatureConfig
    dc.dynamic_class = NonprofitDiamondsInTheRoughController
    qs_nonprofit_ditr, _ = QuestionSet.objects.get_or_create(
        title="Nonprofit Diamonds in the Rough",
        label="Diamonds in the Rough",
        class_name=dc.class_name,
        module_name=dc.module_name,
        allow_multiple=False,
        staff_only=True, icon="ditr")
    fc_nonprofit_ditr.question_sets.add(qs_nonprofit_ditr)

    # Create SignalActionConfig for Nonprofit DitR
    dc.dynamic_class = signal_actions.NonprofitDiamondsInTheRough
    sac_nonprofit_ditr, _ = SignalActionConfig.objects.update_or_create(
        title='Nonprofit Diamonds in the Rough',
        defaults=dict(class_name=dc.class_name, module_name=dc.module_name),
    )

    # Create SignalSetConfig for Nonprofit DitR
    ssc_nonprofit_ditr, _ = SignalSetConfig.objects.get_or_create(
        title='Nonprofit Diamonds in the Rough')

    # Create record in through table to link FeatureConfig and SignalSetConfig.
    # Set the order value as 100 so it is easy to add other SSC before
    # Nonprofit DitR.
    FeatureConfigsSignalSets.objects.update_or_create(
        feature_config=fc_nonprofit_ditr,
        signal_set_config=ssc_nonprofit_ditr,
        defaults=dict(order=100),
    )

    # Create record in through table to link SignalSetConfig and
    # SignalActionConfig. Set the order value as 100 so it is easy to add
    # other SAC before Nonprofit DitR.
    SignalSetsActions.objects.update_or_create(
        signal_set_config=ssc_nonprofit_ditr,
        signal_action_config=sac_nonprofit_ditr,
        defaults=dict(order=100),
    )

    # Add the Nonprofit DitR to nonprofit analysis config template.
    act = AnalysisConfigTemplate.objects.get(
        title='NonProfit Entity Analysis Config Template')
    act.feature_configs.remove(
        FeatureConfig.objects.get(title='Diamonds in the Rough'))
    act.feature_configs.add(fc_nonprofit_ditr)


def backwards(apps, schema_editor):
    SignalSetConfig = apps.get_model('analysis', 'SignalSetConfig')
    SignalActionConfig = apps.get_model('analysis', 'SignalActionConfig')
    SignalSetsActions = apps.get_model('analysis', 'SignalSetsActions')
    FeatureConfig = apps.get_model('analysis', 'FeatureConfig')
    FeatureConfigsSignalSets = apps.get_model('analysis',
                                              'FeatureConfigsSignalSets')
    QuestionSet = apps.get_model("analysis", "QuestionSet")

    nonprofit_ditr_title = "Nonprofit Diamonds in the Rough"
    QuestionSet.objects.filter(title=nonprofit_ditr_title).delete()
    FeatureConfigsSignalSets.objects.filter(
        models.Q(feature_config__title=nonprofit_ditr_title) |
        models.Q(signal_set_config__title=nonprofit_ditr_title)).delete()
    FeatureConfig.objects.filter(title=nonprofit_ditr_title).delete()
    SignalSetsActions.objects.filter(
        models.Q(signal_set_config__title=nonprofit_ditr_title) |
        models.Q(signal_action_config__title=nonprofit_ditr_title)).delete()
    SignalActionConfig.objects.filter(title=nonprofit_ditr_title).delete()
    SignalSetConfig.objects.filter(title=nonprofit_ditr_title).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0101_generic-date-transform'),
    ]

    operations = [
        migrations.RunPython(forwards, backwards),
    ]
