# -*- coding: utf-8 -*-


from django.db import models, migrations


def setup_questionset(apps, schema_config):
    from frontend.apps.analysis.questionset_controllers import \
        KeyContribScoringController
    from frontend.libs.utils.model_utils import DynamicClassModelMixin
    QuestionSet = apps.get_model("analysis", "QuestionSet")
    FeatureConfig = apps.get_model("analysis", "FeatureConfig")

    # Use the Mixin model to parse the class/module name. Models generated
    # through the apps variable don't seem to have all the methods.
    dc = DynamicClassModelMixin()

    # Create the new QuestionSets
    dc.dynamic_class = KeyContribScoringController
    kc_qs, _ = QuestionSet.objects.get_or_create(
        title="Key Contribution Multipliers", class_name=dc.class_name,
        module_name=dc.module_name, allow_multiple=False,
        staff_only=True,)

    key_contrib = FeatureConfig.objects.get(title="Key Contributions")
    key_contrib.question_sets.add(kc_qs)


def setup_signal_actions(apps, schema_editor):
    from frontend.apps.analysis.analyses import signal_actions
    from frontend.libs.utils.model_utils import DynamicClassModelMixin
    SignalActionConfig = apps.get_model('analysis', 'SignalActionConfig')
    SignalSetConfig = apps.get_model('analysis', 'SignalSetConfig')
    SignalSetsActions = apps.get_model('analysis', 'SignalSetsActions')

    dc = DynamicClassModelMixin()

    # Key contribs
    dc.dynamic_class = signal_actions.KeyContribsScoreMultipliers
    kc_mult, created = SignalActionConfig.objects.get_or_create(
        title='Key Contributions Score Multipliers')
    if created:
        kc_mult.definition = {
            'class': dc.class_name,
            'module': dc.module_name}
        kc_mult.save()

    # Summary multiplier
    dc.dynamic_class = signal_actions.ApplyGlobalScoreFactors
    agsf_sac, created = SignalActionConfig.objects.get_or_create(
        title='Apply Global Score Factors')
    if created:
        agsf_sac.definition = {
            'class': dc.class_name,
            'module': dc.module_name}
        agsf_sac.save()

    # Add the multiplier to the summary signalsetconfig
    summary_signals = SignalSetConfig.objects.get(title="Summary signals")
    if not SignalSetsActions.objects.filter(signal_set_config=summary_signals,
                                            signal_action_config=agsf_sac)\
                                    .exists():
        # Attach the SignalAction to Summary Signals at the end.
        action = SignalSetsActions.objects.filter(
            signal_set_config=summary_signals).order_by("-order").first()
        SignalSetsActions.objects.create(
            signal_set_config=summary_signals,
            signal_action_config=agsf_sac,
            order=action.order + 10
        )



class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0047_expansion_hit'),
    ]

    operations = [
        migrations.RunPython(
            setup_questionset, lambda x, y: True
        ),
        migrations.RunPython(
            setup_signal_actions, lambda x, y: True
        ),
    ]
