# Generated by Django 2.0.5 on 2019-06-04 10:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0049_auto_20190409_1553'),
        ('contact_set', '0013_auto_20190520_2002'),
        ('analysis', '0109_auto_20190529_2325'),
    ]

    operations = [
        migrations.CreateModel(
            name='ResultSort',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('value_type', models.CharField(choices=[('SCORE', 'Ranking/Persona Score'), ('NFP_GIVING', 'Nonprofit Giving'), ('POL_GIVING', 'Political Giving'), ('CLI_GIVING', 'Client Giving'), ('GIV_CAP', 'Giving Capacity'), ('DEMO_SCORE', 'Demographic Score'), ('PHIL_SCORE', 'Philanthropic Score'), ('WEALTH_SCORE', 'Wealth Indicators Score'), ('AFF_SCORE', 'Affiliation Score')], max_length=16)),
                ('value', models.FloatField(default=0.0)),
                ('analysis', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='analysis.Analysis')),
                ('contact', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='contact.Contact')),
                ('contact_set', models.ForeignKey(blank=False, null=False, on_delete=django.db.models.deletion.CASCADE, to='contact_set.ContactSet')),
                ('partition', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='analysis.PartitionConfig')),
                ('result', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='analysis.Result')),
            ],
        ),
        migrations.AddIndex(
            model_name='resultsort',
            index=models.Index(fields=['analysis', 'partition', 'contact_set', 'value_type', 'value'], name='analysis_re_analysi_212f64_idx'),
        ),
    ]
