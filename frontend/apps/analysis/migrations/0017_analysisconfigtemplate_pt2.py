# -*- coding: utf-8 -*-


from django.db import models, migrations


def create_analysis_templates(apps, schema_editor):
    AnalysisConfig = apps.get_model("analysis", "AnalysisConfig")
    AnalysisConfigTemplate = apps.get_model("analysis", "AnalysisConfigTemplate")
    AnalysisProfile = apps.get_model("analysis", "AnalysisProfile")
    RevUpUser = apps.get_model("authorize", "RevUpUser")

    # Get analysis configs that are owner-less and build config templates
    for ac in AnalysisConfig.objects.filter(account=None):
        def get_data(fields):
            return {f.name: getattr(ac, f.name)
                    for f in fields
                    if hasattr(ac, f.name) and f.name != "id" and
                    not isinstance(f, models.ManyToManyField)}

        # Copy the fields from the config
        act = AnalysisConfigTemplate(**get_data(
            AnalysisConfigTemplate._meta.get_fields()))
        act.save()
        # Copy and save the feature configs.
        fcs = ac.feature_configs.through.objects.filter(
            analysisconfig=ac).select_related("featureconfig")
        for fc in fcs:
            act.feature_configs.through.objects.create(
                analysisconfigtemplate=act, featureconfig=fc.featureconfig)

        # Find this config in any analysis profiles and replace
        for ap in AnalysisProfile.objects.filter(default_config=ac):
            ap.default_config_template = act
            ap.default_config = None
            ap.save()

            # Update any AnalysisConfigs with the template that belongs to
            # their AnalysisProfile
            AnalysisConfig.objects.filter(analysis_profile=ap) \
                                  .update(analysis_config_template=act)

        # Delete the owner-less config
        RevUpUser.user_analysis_configs.through.objects.filter(analysis_config=ac).delete()
        #RevUpUser.objects.filter(current_analysis=ac).update(current_analysis=None)
        ac.delete()

    # Do a little sanity checking while we have the advantage of a transaction
    if AnalysisConfig.objects.filter(account=None).exists():
        raise Exception("AnalysisConfig without an owner still exists")
    if AnalysisProfile.objects.filter(default_config_template=None)\
                              .exclude(default_config=None).exists():
        raise Exception("AnalysisProfile exists with a default template with "
                        "an account owner.")


def create_analysis_templates_reversed(apps, schema_editor):
    AnalysisConfig = apps.get_model("analysis", "AnalysisConfig")
    AnalysisConfigTemplate = apps.get_model("analysis", "AnalysisConfigTemplate")
    AnalysisProfile = apps.get_model("analysis", "AnalysisProfile")

    # Rebuild the owner-less AnalysisConfigs from the template
    for act in AnalysisConfigTemplate.objects.all():
        def get_data(fields):
            return {f.name: getattr(act, f.name)
                    for f in fields
                    if hasattr(act, f.name) and f.name != "id" and
                    not isinstance(f, models.ManyToManyField)}

        # Copy the fields from the config
        ac = AnalysisConfig(**get_data(
                AnalysisConfig._meta.get_fields()))
        ac.save()
        # Copy and save the m2m references.
        fcs = act.feature_configs.through.objects.filter(
            analysisconfigtemplate=act).select_related("featureconfig")
        for fc in fcs:
            ac.feature_configs.through.objects.create(
                analysisconfig=ac, featureconfig=fc.featureconfig)

        # Find this config in any analysis profiles and replace
        for ap in AnalysisProfile.objects.filter(default_config_template=act):
            ap.default_config = ac
            ap.default_config_template = None
            ap.save()


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0016_analysisconfigtemplate_pt1'),
        ('authorize', '0015_auto_20150609_1541'),
    ]

    operations = [
        # Before altering analysisConfig account, we need to create templates
        # from all of the non-account configs and delete them.
        migrations.RunPython(
            create_analysis_templates,
            create_analysis_templates_reversed
        ),


    ]
