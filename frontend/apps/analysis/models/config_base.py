import datetime

from django.core.exceptions import ValidationError
from django.db import models, transaction

from frontend.libs.utils.model_utils import (
    OwnershipModel as OwnershipBase, clone_record, ValidatingModel,
    ConfigModelBase)


class AnalysisConfigModel(ConfigModelBase):
    """Model abstract base class providing common fields and functionality for
    analysis and data source configuration models.
    """
    class Meta(ConfigModelBase.Meta):
        ordering = ('-modified', '-created',)
        abstract = True

    @property
    def name(self):
        if self.title:
            return self.title
        return self.__str__()

    def _translate_copy_kwargs(self, new_label=None, new_account=None,
                                new_user=None, modify_kwargs=False, **kwargs):
        result = kwargs if modify_kwargs else {}
        if not new_label:
            new_label = 'copy'
        result['append_suffix'] = {'title': ' ' + new_label.lstrip()}
        if new_account:
            result['account'] = new_account
        if new_user:
            result['user'] = new_user
        return result

    def _copy_title(self, **kwargs):
        new_label = kwargs.get('new_label')
        if not new_label:
            new_label = 'copy'
        return ' '.join([self.name, new_label])

    def _copy_prepare(self, **kwargs):
        self.pk = None
        self.id = None
        new_account = kwargs.get('new_account')
        new_user = kwargs.get('new_user')
        if new_account or new_user:
            try:
                self.update_account_user(new_account, new_user)
            except AttributeError:
                # Subclasses may not inherit from OwnershipModel
                pass
        self.created = datetime.datetime.now()
        self.title = self._copy_title(**kwargs)

    @transaction.atomic
    def copy(self, **kwargs):
        kwargs = self._translate_copy_kwargs(modify_kwargs=True, **kwargs)
        new_record = clone_record(self, **kwargs)
        return new_record

    def old_copy(self, **kwargs):
        self._copy_prepare(**kwargs)
        self.save()
        return self


class AnalysisConfigBase(AnalysisConfigModel):
    """Configuration for an analysis.

    An analysis has a root configuration that specifies the type of analysis,
    the data configuration used for the analysis, the partitioning (if any) of
    the analysis, and the feature configurations used in the analysis. These
    configurations are used both for executing an analysis, as well as for
    later rendering the results of the analysis.

    Attributes:
      title (str): Title
      description (str): Longer description
      analysis_profile (AnalysisProfile): Analysis profile of this analysis
        configuration
      data_config (DataConfig): Source data configuration for this analysis
        configuration
      partition_set_config (PartitionConfig): Partitioning configuration for
        this analysis configuration
      feature_configs (FeatureConfigs): Feature configurations for this
        analysis configuration
    """
    analysis_profile = models.ForeignKey('analysis.AnalysisProfile',
                                         related_name='+',
                                         on_delete=models.PROTECT)
    data_config = models.ForeignKey('analysis.DataConfig', related_name='+',
                                    blank=True, null=True,
                                    on_delete=models.PROTECT)
    partition_set_config = models.ForeignKey('analysis.PartitionSetConfig',
                                             related_name='+', blank=True,
                                             null=True,
                                             on_delete=models.PROTECT)
    feature_configs = models.ManyToManyField(
        'analysis.FeatureConfig', blank=True,
        # This related_name is necessary as a result of the following bug:
        # https://code.djangoproject.com/ticket/24505. Work-around detailed in
        # https://code.djangoproject.com/ticket/18595#comment:1
        related_name='%(app_label)s_%(class)s_+')

    class Meta(AnalysisConfigModel.Meta):
        abstract = True

    def get_question_sets(self, **kwargs):
        # Query for all question sets
        through_table = self.feature_configs.model.question_sets.through

        query_args = dict(featureconfig__in=self.feature_configs.all())
        if kwargs:
            # We want this to behave like a m2m, so filter queries should go
            # straight through to question_set.
            query_args.update({"questionset__{}".format(k): v
                               for k, v in kwargs.items()})

        # This reduces the number of queries down to 1 instead of 1 per feature
        qss = [through_item.questionset
               for through_item in through_table.objects.filter(**query_args)
                                                .select_related("questionset")
                                                .distinct("questionset")]
        # We have to do the sorting in the code, and not in the database,
        # because we use a "Distinct" query, which doesn't play nicely
        # with `order_by`. There is no real performance difference.
        return sorted(qss, key=lambda qs: qs.display_order)

    def generate_analysis_config(self, account, title=None,
                                 add_to_account=True):
        """Generate an AnalysisConfig from either an AnalysisConfigTemplate
            or another AnalysisConfig.
        """
        # Need to import here or we will have a circular import
        from frontend.apps.analysis.models import AnalysisConfig
        from frontend.apps.campaign.models import AccountAnalysisConfigLink

        # Fields that should not be copied. Many of these are auto-defined
        # and we don't want to override that.
        exclude_fields = {
            "id", "created", "modified", "created_by", "modified_by",
            "created_with_session_key", "modified_with_session_key",
            "analysis_config_template", "last_execution", "account", "user"}

        def get_data(predicate, fields):
            return {f.name: getattr(self, f.name)
                    for f in fields
                    if (hasattr(self, f.name) and
                        f.name not in exclude_fields and
                        predicate(f))}

        try:
            template = self.analysis_config_template
        except AttributeError:
            template = self

        # Copy this model's data and create a config from it.
        def not_m2m(field):
            return not isinstance(field, models.ManyToManyField)

        data = get_data(not_m2m, AnalysisConfig._meta.get_fields())
        if title is not None:
            data['title'] = title
        ac = AnalysisConfig.objects.create(
            account=account, analysis_config_template=template, **data)

        # Migrate any m2m values over.
        def is_m2m(field):
            return isinstance(field, models.ManyToManyField)

        for field, value in get_data(
                is_m2m, AnalysisConfig._meta.get_fields()).items():
            # Generates a list from the values in the m2m of the copied config,
            # and assigns the list to the new AnalysisConfig
            getattr(ac, field).set(list(value.all()))

        if add_to_account:
            AccountAnalysisConfigLink.objects.create(
                analysis_config=ac,
                account=account
            )
        return ac


class OwnershipModel(ValidatingModel, OwnershipBase):
    """Model abstract base class providing user and account ownership fields
    """
    class Meta:
        abstract = True

    def validate(self, child):
        """Helper for models to validate ownership rules with their relations.
        """

        if self.user_id is None and child.user_id is not None:
            raise ValidationError(
                "Leaky permissions, a generic parent config object must not "
                "use a config object with a specific user attached.")

        if self.account_id is None and child.account_id is not None:
            raise ValidationError(
                "Leaky permissions, a generic parent config object must not "
                "use a config object with a specific account attached.")

        if self.user_id is not None and child.user_id is not None \
           and self.user_id != child.user_id:
            raise ValidationError("User must match on parent and child")

        if self.account_id is not None and child.account_id is not None \
           and self.account_id != child.account_id:
            raise ValidationError("Account must match on parent and child")

    def clean(self):
        super(OwnershipModel, self).clean()

        if self.user_id is not None and self.account_id is None:
            raise ValidationError(
                "If user is set, account must be set as well")

        if self.user_id is not None and self.account_id is not None and not self.account.is_member(self.user):
            raise ValidationError(
                "User {} does not have an active seat on account {}".format(
                    self.user, self.account))

    def update_account_user(self, new_account=None, new_user=None):
        if new_account:
            self.account = new_account
        if new_user:
            self.user = new_user

    @staticmethod
    def validate_m2m(sender, instance, action, model, pk_set, reverse, **kwargs):
        """Helper to run validation on ManyToMany relationships.

        Runs validate on the "parent" instance against the child model. The
        parent is assumed to be the model where the ManyToMany relationship was
        defined.
        """
        if action == "pre_add":
            for pk in pk_set:
                other_instance = model.objects.get(pk=pk)
                if not reverse:
                    # If reverse is false, then the modification is coming from
                    # the PartitionSetConfig end, and instance is a
                    # PartitionSetConfig
                    instance.validate(other_instance)
                else:
                    # If reverse is true, then the modification is coming from
                    # the PartitionConfig end, and instance is a
                    # PartitionConfig
                    other_instance.validate(instance)

