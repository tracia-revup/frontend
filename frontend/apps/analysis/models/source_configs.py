"""
Models providing specification and configuration of analysis data sources.
"""
import logging
from django.db import models, transaction
from django.db.models import ProtectedError

from frontend.apps.analysis.models.analysis_configs import (
    AnalysisConfigModel, OwnershipModel)
from frontend.libs.utils.model_utils import Choices, DynamicClassModelMixin


LOGGER = logging.getLogger(__name__)


class DataSet(AnalysisConfigModel, OwnershipModel, DynamicClassModelMixin):
    """Specifications for a data set to be used in analyses.

    A data set has one or more record sets. Examples include FEC, MISP, NGP,
    a group of a client's proprietary record sets, etc.

    Attributes:
      title (str): Title
      description (str): Longer description
      class_name (str): Name of implementing class
      module_name (str): Name of module containing implementing class
      account (Account): Owning Account (optional)
      user (RevUpUser): Owning User (optional)

    """
    pass


class RecordSet(AnalysisConfigModel, OwnershipModel, DynamicClassModelMixin):
    """Specifications for a record set to be used in analyses.

    A record set provides a set of one kind of facts. Examples include FEC
    candidates, committees, MISP contributions, NGP records, a client's
    proprietary record set, etc.

    Attributes:
      title (str): Title
      description (str): Longer description
      data_set (DataSet): Data set this record set is part of
      class_name (str): Name of implementing class
      module_name (str): Name of module containing implementing class
      account (Account): Owning Account (optional)
      user (RevUpUser): Owning User (optional)

    """
    class FactTypes(Choices):
        choices_map = [
            ("CNT", "CONTRIB", "Contributions"),
            ("PLE", "POLITICAL_ENTITY", "Political Entities"),
            ("NPE", "NONPROFIT_ENTITY", "NonProfit Entities"),
        ]

    data_set = models.ForeignKey('DataSet', related_name='record_sets',
                                 on_delete=models.PROTECT)
    fact_type = models.CharField(max_length=3, choices=FactTypes.choices(),
                                 default=None, null=True)

    def clean(self):
        super(RecordSet, self).clean()
        self.validate(self.data_set)


class RecordResource(AnalysisConfigModel, OwnershipModel,
                     DynamicClassModelMixin):
    """Specifications for a record resource to be used in analyses.

    A record resource provides the actual storage, indexing, or other resource
    for a given record set. Examples might include specific databases,
    search engines, etc.

    Attributes:
      title (str): Title
      description (str): Longer description
      record_set (RecordSet): Record set this resource stores and/or indexes
      class_name (str): Name of implementing class
      module_name (str): Name of module containing implementing class
      account (Account): Owning Account (optional)
      user (RevUpUser): Owning User (optional)

    """
    record_set = models.ForeignKey('RecordSet', on_delete=models.PROTECT,
                                   related_name='record_resources')

    def clean(self):
        super(RecordResource, self).clean()
        self.validate(self.record_set)


class ResourceInstance(AnalysisConfigModel, OwnershipModel):
    """Specifications for a resource instance to be used in analyses.

    A resource instance identifies a specific instance of a record resource.
    Examples might include a mongodb collection name, a search engine index
    name, etc.

    Attributes:
      title (str): Title
      description (str): Longer description
      record_resource (RecordSet): Record resource this is an instance of
      identifier (str): Identifier for this instance (e.g. a collection name)
      account (Account): Owning Account (optional)
      user (RevUpUser): Owning User (optional)

    """
    record_resource = models.ForeignKey('RecordResource',
                                        related_name='resource_instances',
                                        on_delete=models.PROTECT)
    identifier = models.CharField(max_length=64, blank=True)

    def clean(self):
        super(ResourceInstance, self).clean()
        self.validate(self.record_resource)


class DataConfig(AnalysisConfigModel, OwnershipModel):
    """A specific configuration of data sets for use in analyses.

    Attributes:
      title (str): Title
      description (str): Longer description
      data_set_configs (DataSetConfigs): Data set configs for this config
      account (Account): Owning Account (optional)
      user (RevUpUser): Owning User (optional)

    """
    data_set_configs = models.ManyToManyField('DataSetConfig',
                                              related_name='+')

    @transaction.atomic
    def copy(self, **kwargs):
        old_data_set_configs = list(self.data_set_configs.all())
        self._copy_prepare(**kwargs)
        self.save()
        self.data_set_configs.set(old_data_set_configs)

    @transaction.atomic
    def delete_including_orphans(self):
        old_data_set_configs = list(self.data_set_configs.all())
        self.delete()
        for old_data_set_config in old_data_set_configs:
            try:
                old_data_set_config.delete_including_orphans()
            except ProtectedError:
                LOGGER.warning('Unable to delete %s - still in use',
                               old_data_set_config)

models.signals.m2m_changed.connect(
    DataConfig.validate_m2m,
    sender=DataConfig.data_set_configs.through)


class DataSetConfig(AnalysisConfigModel, OwnershipModel):
    """A specific configuration of a data set for use in analyses.

    Attributes:
      title (str): Title
      description (str): Longer description
      data_set (DataSet): Data set with which this config is associated
      record_set_configs (RecordSetConfigs): Record set configs for this config
      account (Account): Owning account (optional)
      user (RevUpUser): Owning user (optional)

    """
    data_set = models.ForeignKey('DataSet',
                                 related_name='+', on_delete=models.PROTECT)
    record_set_configs = models.ManyToManyField('RecordSetConfig',
                                                related_name='+')

    @transaction.atomic
    def deep_copy(self, **kwargs):
        new_record_set_configs = [record_set_config.deep_copy(**kwargs)
                                  for record_set_config
                                  in self.record_set_configs.all()]
        self._copy_prepare(**kwargs)
        self.save()
        self.record_set_configs.set(new_record_set_configs)
        return self

    @transaction.atomic
    def copy(self, **kwargs):
        old_record_set_configs = list(self.record_set_configs.all())
        self._copy_prepare(**kwargs)
        self.save()
        self.record_set_configs.set(old_record_set_configs)

    @transaction.atomic
    def delete_including_orphans(self):
        old_record_set_configs = list(self.record_set_configs.all())
        self.delete()
        for old_record_set_config in old_record_set_configs:
            try:
                old_record_set_config.delete_including_orphans()
            except ProtectedError:
                LOGGER.warning('Unable to delete %s - still in use',
                               old_record_set_config)

    def clean(self):
        super(DataSetConfig, self).clean()
        self.validate(self.data_set)

models.signals.m2m_changed.connect(
    DataSetConfig.validate_m2m,
    sender=DataSetConfig.record_set_configs.through)


class RecordSetConfig(AnalysisConfigModel, OwnershipModel):
    """A specific configuration of a record set for use in analyses.

    Attributes:
      title (str): Title
      description (str): Longer description
      record_set (RecordSet): Record set with which this config is associated
      resource_config (ResourceConfig): Resource config for this config
      account (Account): Owning account (optional)
      user (RevUpUser): Owning user (optional)

    """
    record_set = models.ForeignKey('RecordSet',
                                   related_name='+', on_delete=models.PROTECT)
    resource_config = models.ForeignKey('ResourceConfig',
                                        related_name='+',
                                        on_delete=models.PROTECT)

    @transaction.atomic
    def deep_copy(self, **kwargs):
        new_resource_config = self.resource_config.deep_copy(**kwargs)
        self._copy_prepare(**kwargs)
        self.resource_config = new_resource_config
        self.save()
        return self

    @transaction.atomic
    def delete_including_orphans(self):
        old_resource_config = self.resource_config
        self.delete()
        if old_resource_config:
            try:
                old_resource_config.delete_including_orphans()
            except ProtectedError:
                LOGGER.warning('Unable to delete %s - still in use',
                               old_resource_config)

    def clean(self):
        super(RecordSetConfig, self).clean()
        self.validate(self.record_set)
        self.validate(self.resource_config)


class ResourceConfig(AnalysisConfigModel, OwnershipModel):
    """A specific configuration of a record resource for use in analyses.

    Attributes:
      title (str): Title
      description (str): Longer description
      record_resource (RecordResource): Record resource with which this config
        is associated
      resource_instance (ResourceInstance): Resource instance for this config
      account (Account): Owning account (optional)
      user (RevUpUser): Owning user (optional)

    """
    record_resource = models.ForeignKey('RecordResource',
                                        related_name='+',
                                        on_delete=models.PROTECT)
    resource_instance = models.ForeignKey('ResourceInstance',
                                          related_name='+',
                                          on_delete=models.PROTECT)

    @transaction.atomic
    def deep_copy(self, **kwargs):
        new_resource_instance = self.resource_instance.copy(**kwargs)
        self._copy_prepare(**kwargs)
        self.resource_instance = new_resource_instance
        self.save()
        return self

    @transaction.atomic
    def delete_including_orphans(self):
        old_resource_instance = self.resource_instance
        self.delete()
        if old_resource_instance:
            try:
                old_resource_instance.delete()
            except ProtectedError:
                LOGGER.warning('Unable to delete %s - still in use',
                               old_resource_instance)

    def clean(self):
        super(ResourceConfig, self).clean()
        self.validate(self.record_resource)
        self.validate(self.resource_instance)

