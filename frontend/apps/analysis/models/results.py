"""Analysis result models

This module provides models for the production and storage of analysis results.
"""
import collections
import math
import time

from django.conf import settings
from django.contrib.postgres import fields
from django.core.cache import cache
from django.db import models
from django_extensions.db.models import TimeStampedModel
from jsonfield import JSONField

from frontend.libs.mixins import SimpleUnicodeMixin
from frontend.libs.utils.model_utils import (
    JSONEncoder, VirtualForeignObject, Choices)


class Analysis(TimeStampedModel, SimpleUnicodeMixin):
    """The result of an analysis for a specific user

    The results of an analysis are organized into associated Result records. An
    analysis may be organized by one or more partitions (i.e. date ranges).

    Attributes:
      user (RevUpUser): Owning user
      event (Event): Event for which this analysis was run (if any)
      account (Account): Campaign for which this analysis was run
      analysis_config (AnalysisConfig): Analysis configuration used to produce
        this analysis
      partition_set (PartitionSetConfig): Partition set configuration used to
        produce this analysis
      status (str): Status of this analysis
      timestamp (datetime): Timestamp for creation of this analysis

    """
    class STATUSES:
        running = "RUNNING"
        complete = "COMPLETE"
        error = "ERROR"
        finished = (complete, error)

    user = models.ForeignKey('authorize.RevUpUser', on_delete=models.CASCADE)
    event = models.ForeignKey('campaign.Event', on_delete=models.CASCADE, null=True,
                              related_name='+')
    account = models.ForeignKey('campaign.Account', on_delete=models.CASCADE)
    analysis_config = models.ForeignKey('analysis.AnalysisConfig', on_delete=models.CASCADE, null=True,
                                        related_name='+')
    partition_set = models.ForeignKey('analysis.PartitionSetConfig', on_delete=models.CASCADE, null=True,
                                      related_name='+')
    applied_filters = models.ManyToManyField("filtering.Filter",
                                             through="AnalysisAppliedFilter",
                                             related_name="+")
    last_completed = models.DateTimeField(
        blank=True,
        null=True,
        help_text="The last time the Analysis was completed successfully."
    )

    meta_results = JSONField(
        blank=True, default={},
        help_text="Contains the results from the meta analysis features. A "
                  "meta analysis is an analysis on the rest of the analysis")

    status = models.CharField(
        max_length=10,
        choices=(
            (STATUSES.running, STATUSES.running),
            (STATUSES.complete, STATUSES.complete),
            (STATUSES.error, STATUSES.error),
        ),
        default=STATUSES.running,
        blank=True,
    )
    task_id = models.CharField(max_length=255, null=True, blank=True)

    def is_running(self):
        return self.status == self.STATUSES.running

    def is_complete(self):
        return self.status == self.STATUSES.complete

    def get_result_count(self):
        # If this hasn't been saved, it can't have a result count
        if not self.id:
            return 0

        # Check the cache for results
        key = settings.GLOBAL_CACHE_KEYS.analysis_results_count_cache.format(
            analysis_id=self.id,
            modified=int(time.mktime(self.modified.timetuple())))
        result_count = cache.get(key, version=settings.PRODUCT_VERSION)
        if result_count:
            return result_count

        # Cache miss
        if self.is_complete() and self.results.exists():
            partition = self.partition_set.partition_configs.first()
            result_count = self.results.filter(partition=partition).count()
            cache.set(key, result_count, version=settings.PRODUCT_VERSION)
            return result_count
        else:
            return 0

    def build_partition_map(self):
        """Build a map of this analysis' partitions. These are used in views"""
        partition_set = self.partition_set
        if partition_set is None:
            # old analysis may not necessarily have partition_set populated
            partition_set = self.analysis_config.partition_set_config
        return partition_set.build_partition_map()


class AnalysisAppliedFilter(models.Model):
    class Meta:
        unique_together = ('analysis', 'filter')
    analysis = models.ForeignKey("Analysis", on_delete=models.CASCADE)
    filter = models.ForeignKey("filtering.Filter", on_delete=models.CASCADE)
    filter_configs = fields.JSONField(blank=True)


class Result(models.Model):
    """The result of an analysis for a specific target

    The results of an analysis are organized into result records. Each result
    will have a target (e.g. a person) and one or more feature results for that
    target. A result may also be associated with a partition of the analysis
    (i.e. a date range).

    Initially, only Contacts will be used as targets, until we transition to
    a more generic target Entity.

    Attributes:
      user (RevUpUser): Owning user
      contact (Contact): Contact corresponding to this result
      analysis (Analysis): Analysis for which this result was produced
      partition (PartitionConfig): Configuration for the partition this
        result is in
      features (FeatureResults): Feature results for this result
      key_signals (OrderedDict): Portions of signal data from signal sets of
        a result's features for use in result-level summaries

    """
    user = models.ForeignKey('authorize.RevUpUser', on_delete=models.CASCADE, related_name='+')
    contact = models.ForeignKey('contact.Contact', on_delete=models.CASCADE, related_name='+',
                                blank=True, null=True)
    contact_index = VirtualForeignObject(
        'filtering.ContactIndex',
        related_name='+',
        from_fields=['contact_id'],
        to_fields=['contact'],
        on_delete=models.DO_NOTHING,
        db_index=False, editable=False, blank=True, null=True)
    analysis = models.ForeignKey('Analysis', on_delete=models.CASCADE, related_name='results')
    partition = models.ForeignKey('analysis.PartitionConfig', on_delete=models.CASCADE, related_name='+',
                                  blank=True, null=True)
    key_signals = JSONField(
        # Use customized json encoder with support for ObjectIds
        dump_kwargs={'cls': JSONEncoder, 'separators': (',', ':')},
        load_kwargs={'object_pairs_hook': collections.OrderedDict})
    score = models.FloatField(default=0.0)
    giving = models.DecimalField(blank=True, decimal_places=2,
                                 max_digits=12, default=0)
    tags = fields.ArrayField(models.CharField(max_length=64),
                             default=[])

    class Meta:
        index_together = (
            ("analysis", "partition", "score"),
            ("analysis", "partition", "giving"),
        )

    def get_features(self, include_features=None, show_full_data=True,
                     demo_mode=False, limit=None, feature_flags=None):
        """Generate a list of all FeatureResults and their values."""
        feature_results = []
        has_results = set()
        for feature in self.features.select_related("feature_config__feature"):
            # If this feature is not included in the results, we skip it.
            if include_features and feature.feature_config.feature.title \
                    not in include_features:
                continue

            signal_sets = feature.formatted_data(
                show_full_data=show_full_data, demo_mode=demo_mode,
                limit=limit, analysis_config=self.analysis.analysis_config,
                feature_flags=feature_flags)

            feature_results.append((
                feature.feature_config.title,
                feature.feature_config.feature.title, signal_sets))
            has_results.add(feature.feature_config.id)

        # Add blank placeholders for any features that don't have results.
        # We don't need to do this if only certain features are requested.
        if not include_features:
            all_features = [
                (fc.title, fc.feature.title if fc.feature else None, [])
                for fc in self.analysis.analysis_config.feature_configs
                                       .select_related("feature")
                if fc.id not in has_results and not fc.hidden]
            feature_results.extend(all_features)
        return feature_results

    @classmethod
    def _log_or_zero(cls, i):
        if i <= 0:
            return 0
        else:
            return math.log(i)

    @classmethod
    def _normalize_score(cls, score, min_score, max_score):
        """Normalize the value score given a min of min_score and a max of max_score.
        """
        min_score = cls._log_or_zero(float(min_score))
        max_score = cls._log_or_zero(float(max_score))
        norm_score = cls._log_or_zero(float(score))
        try:
            norm_score = ((norm_score - min_score) /
                          float(max_score - min_score)) * 100.0
        except ZeroDivisionError:
            norm_score = score

        if norm_score < 0:
            norm_score = 0.0
        elif norm_score > 100:
            norm_score = 100.0
        score = norm_score
        return score

    @property
    def normalized_score(self):
        return self.get_normalized_score(self.analysis)

    def get_normalized_score(self, analysis):
        """Normalized score

        Normalize the score only if min and max score are found in
        meta_results. Otherwise the score is assumed to already be normalized.
        """
        score = self.score
        partition_id = str(self.partition_id)
        try:
            normalize_meta = analysis.meta_results.get(
                'normalize')
        except AttributeError:
            normalize_meta = None
        if normalize_meta and partition_id in normalize_meta:
            normalize_meta = normalize_meta[partition_id]
            min_score = normalize_meta.get('min_score')
            max_score = normalize_meta.get('max_score')
            if not (min_score is None or max_score is None):
                score = self._normalize_score(score, min_score, max_score)
        return score

    @classmethod
    def deduplicate_tags(cls, instance, *args, **kwargs):
        # No need to waste time deduping lists of 1 or 0
        if len(instance.tags) > 1:
            # Note: this does kill the original ordering, but that shouldn't
            #       matter for tags.
            instance.tags = list(set(instance.tags))
models.signals.pre_save.connect(Result.deduplicate_tags, sender=Result)


class FeatureResult(models.Model):
    """An ordered set of analysis signals

    Analyses may have multiple features, such as additional data about a
    target (e.g. additional biographical fields for a person), sets of signals
    from different data sets (e.g. electoral, academic, or nonprofit
    contributions, spectra (e.g. Dem vs Republican), subsets of contributions
    that may contribute positively or negatively to scores (e.g. to a
    candidate, to similar candidates, or to dissimilar candidates or
    opponents), score and contribution total values, etc.

    A FeatureResult is the result for a particular Feature based on analysis
    input and configuration of the feature, and can have multiple associated
    SignalSets.

    Attributes:
      user (RevUpUser): Owning user
      feature_config (FeatureConfig): Configuration for this feature result
      signals (OrderedDict): Ordered dict of sets of signals

      Example signals:
        {
          'signal_sets': [
            {
              'config_id': 1,
              'giving': 1000,
              'score': 80,
              'match': [1, 2, 3, 4, 5],
              'giving_availability': 5400
            }
          ]
        }

    """
    user = models.ForeignKey('authorize.RevUpUser', on_delete=models.CASCADE, related_name='+')
    feature_config = models.ForeignKey('analysis.FeatureConfig', on_delete=models.CASCADE,
                                       related_name='+')
    result = models.ForeignKey('Result', on_delete=models.CASCADE, related_name='features',
                               blank=True, null=True)
    signals = JSONField(
        # Use customized json encoder with support for ObjectIds
        dump_kwargs={'cls': JSONEncoder, 'separators': (',', ':')},
        load_kwargs={'object_pairs_hook': collections.OrderedDict},
        blank=True, default='')

    def formatted_data(self, **kwargs):
        """Format the SignalSet data into an API usable format."""
        # Since this data should never change, let's cache it.
        cache_key = "feature_result__{}".format(self.id)
        cached = cache.get(cache_key, version=2)
        if cached:
            return cached
        else:
            result_formatter = self.feature_config.result_formatter(
                self.feature_config, **kwargs)
            signal_sets = result_formatter.format_result_data(
                self.signals.get("signal_sets", []))
            # Cache the results so we don't keep making the same queries.
            # Cache for one day.
            cache.set(cache_key, signal_sets, 60*60*24, version=2)
        return signal_sets


class ResultSort(models.Model):
    """ResultSort is a helper model to enable fast sorting on any result field

    The purpose of ResultSort is to allow us to use b-tree indexes to speed up
    pagination of results. We want to be able to sort on more than just giving
    and score. In-memory sort doesn't scale for customers with thousands of
    contacts as it would require reading all records into memory first.

    Previously `Result` enabled index sorting on `giving` and `score` fields by
    creating two multi-column indexes, one on `(analysis, partition, score)`,
    and one on `(analysis, partition, giving)`. Unfortunately this also doesn't
    scale as we don't want to have to create a new index every time a new sort
    axis is added.

    To support index sorting on any field we use a multi-column index like so:
    `(analysis, partition, value_type, value)`, where `value_type` indicates
    the type of value contained in `value` (i.e. score, giving, or any other
    axis necessary).

    This isn't enough though as we must also support personas, and each persona
    has a different score. To support this we add `contact_set`:
    `(analysis, partition, contact_set, value_type, value)`. We use
    `contact_set` to identify whether we are viewing a persona or not. If we
    are not viewing a persona, we use the root contact set id to view the
    normal values, otherwise we use the persona contact set id.
    """
    class Meta:
        indexes = (
            models.Index(fields=['analysis', 'partition', 'identifier',
                                 'value_type', 'value']),
        )

    class ValueTypes(Choices):
        choices_map = [
            ('SCORE', 'SCORE', 'Ranking/Persona Score'),
            ('NFP_GIVING', 'NFP_GIVING', 'Nonprofit Giving'),
            ('POL_GIVING', 'POL_GIVING', 'Political Giving'),
            ('CLI_GIVING', 'CLI_GIVING', 'Client Giving'),
            ('GIV_CAP', 'GIV_CAP', 'Giving Capacity'),
            ('DEMO_SCORE', 'DEMO_SCORE', 'Demographic Score'),
            ('PHIL_SCORE', 'PHIL_SCORE', 'Philanthropic Score'),
            ('WEALTH_SCORE', 'WEALTH_SCORE', 'Wealth Indicators Score'),
            ('AFF_SCORE', 'AFF_SCORE', 'Affiliation Score'),
        ]
        SIGNAL_MAP = {
            'score': 'SCORE',
            'political_giving': 'POL_GIVING',
            'nonprofit_giving': 'NFP_GIVING',
            'client_giving': 'CLI_GIVING',
            'giving_capacity': 'GIV_CAP',
            'DG': 'DEMO_SCORE',
            'WI': 'WEALTH_SCORE',
            'PI': 'PHIL_SCORE',
            'AI': 'AFF_SCORE',
        }
        PERSONA_SPECIFIC = {
            'SCORE',
            'DEMO_SCORE',
            'PHIL_SCORE',
            'WEALTH_SCORE',
            'AFF_SCORE',
        }
        # The following value types are all deduped. To lookup a score on these
        # values `identifier` must be empty.
        SINGLETON = {
            'NFP_GIVING',
            'POL_GIVING',
            'CLI_GIVING',
            'GIV_CAP',
        }

    id = models.BigAutoField(primary_key=True)
    result = models.ForeignKey(Result, on_delete=models.CASCADE, blank=False,
                               null=False)
    analysis = models.ForeignKey(Analysis, on_delete=models.CASCADE,
                                 blank=False, null=False)
    partition = models.ForeignKey('analysis.PartitionConfig',
                                  on_delete=models.CASCADE, blank=False,
                                  null=False)
    identifier = models.CharField(max_length=256, blank=True, null=False)
    value_type = models.CharField(max_length=16, blank=False, null=False,
                                  choices=ValueTypes.choices())
    value = models.FloatField(default=0.0)

    contact = models.ForeignKey('contact.Contact', on_delete=models.CASCADE,
                                blank=False, null=False)
    contact_index = VirtualForeignObject(
        'filtering.ContactIndex',
        related_name='+',
        from_fields=['contact_id'],
        to_fields=['contact'],
        on_delete=models.DO_NOTHING,
        db_index=False, editable=False, blank=True, null=True)
    result_index = VirtualForeignObject(
        'filtering.ResultIndex',
        related_name='+',
        from_fields=['result_id'],
        to_fields=['result'],
        on_delete=models.DO_NOTHING,
        db_index=False, editable=False, blank=True, null=True)
