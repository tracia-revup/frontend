"""
Models supporting analyses and their configurations.
"""
from collections import Iterable, OrderedDict
import logging
from datetime import datetime

from django import template
from django.core.exceptions import ValidationError
from django.db import models, transaction, IntegrityError
from django_extensions.db.models import TimeStampedModel
from jsonfield import JSONField

from frontend.apps.analysis.models.config_base import (
    AnalysisConfigModel, OwnershipModel, AnalysisConfigBase, ValidatingModel)
from frontend.apps.analysis.sources.base import RecordSet
from frontend.libs.utils.general_utils import deep_hash, instance_cache
from frontend.libs.utils.election_utils import previous_election_date_for_office
from frontend.libs.utils.model_utils import (
    Choices, DynamicClassModelMixin, DynamicFormBase)


LOGGER = logging.getLogger(__name__)


class AnalysisProfile(AnalysisConfigModel):
    """Definition of a type and profile of analysis.

    Analyses have a basic type, for example, a ranking of individuals based on
    likelihood to make a contribution. Analyses may also be associated with
    analysis profiles that allow further specification of analysis features and
    configurations.

    For example, there may be a variation of individual contribution rankings
    for use with political action committee campaigns, as opposed to political
    candidate campaigns, or university campaigns.

    Analysis profiles may be associated with a default analysis configuration,
    and may use multiple features.

    Attributes:
      title (str): Title
      description (str): Longer description
      analysis_type (str): Choice from AnalysisTypes
      default_config (AnalysisConfig): Default configuration for profile
      features (Features): Feature types available to Accounts using this
                           type of Profile
                           # TODO: Actually use this.
    """
    class AnalysisTypes(Choices):
        """
        Types of analyses.
        """
        choices_map = [
            ('PN_CONTRIB_RANK', 'PERSON_CONTRIB_RANKING',
             'Individual Contribution Ranking'),
            ('ENT_CONTRIB_RANK', 'ENTITY_CONTRIB_RANKING',
             'Individual Contribution Ranking using Entities'),
            ('DUMMY', 'DUMMY', 'No-op dummy runner'),
        ]

    analysis_type = models.CharField(choices=AnalysisTypes.choices(),
                                     max_length=16, blank=True)
    default_config_template = models.ForeignKey(
        'AnalysisConfigTemplate', null=True, blank=True,
        related_name='+', on_delete=models.PROTECT)
    features = models.ManyToManyField('Feature', blank=True)
    analysis_view_setting_set = models.ForeignKey(
        'authorize.AnalysisViewSettingSet',
        null=True, blank=True, on_delete=models.PROTECT)


class Feature(AnalysisConfigModel):
    """Definition of a feature used in analyses.

    Analyses may have multiple features, such as additional data about a
    target (e.g. additional biographical fields for a person), sets of signals
    from different data sets (e.g. electoral, academic, or nonprofit
    contributions, spectra (e.g. Dem vs Republican), subsets of contributions
    that may contribute positively or negatively to scores (e.g. to a
    candidate, to similar candidates, or to dissimilar candidates or
    opponents), score and contribution total values, etc.

    A feature is associated with a basic feature type, may allow multiple
    configuration instances of the feature in an analysis, and may be
    associated with a default feature configuration.

    Attributes:
      title (str): Title
      description (str): Longer description
      feature_type (str): Choice from FeatureTypes
    """
    class FeatureTypes(Choices):
        """Types of features."""
        choices_map = [
            ('LOOKUP', 'LOOKUP', 'Field lookups'),
            ('SPECTRUM', 'SPECTRUM', 'Spectrum values'),
            ('MATCH', 'MATCHES', 'Matching facts from data sets'),
            ('MATCH_SUB', 'MATCHES_SUBSET', 'Subsets of matching facts'),
            ('SUM', 'SUMMARY', 'Summary features'),
            ('NORM', 'NORMALIZATION', 'Feature normalization'),
            ('PRIME', 'PRIME', 'Features that need to be run first'),
            ('META', 'META', "Meta analysis")
        ]

    feature_type = models.CharField(choices=FeatureTypes.choices(), blank=True,
                                    max_length=16)


class AnalysisConfigTemplate(AnalysisConfigBase):
    """Defines a common template for an AnalysisConfig that can be
    shared across many accounts. This model is almost the same as an
    AnalysisConfig, but is separated for organizational purposes.

    See AnalysisConfigBase for more information on the model fields.
    """
    pass


class AnalysisConfig(AnalysisConfigBase, OwnershipModel):
    """Configuration for an analysis.

    See AnalysisConfigBase for more.
    """
    analysis_config_template = models.ForeignKey(
        "AnalysisConfigTemplate", on_delete=models.CASCADE, blank=True, null=True, editable=False,
        help_text="The template that was used to generate this config")
    # Keeps track of when this analysis config is used to generate an
    # analysis. This timestamp is used to version other things that
    # are dependent on whether we have run an analysis or not
    # (e.g. QuestionSetData). We default it on creation to simplify the
    # logic elsewhere in the app.
    last_execution = models.DateTimeField(blank=True, default=datetime.now)

    @transaction.atomic
    def copy(self, **kwargs):
        clone_question_set_data = kwargs.pop('clone_question_set_data', False)
        new_record = super(AnalysisConfig, self).copy(**kwargs)
        if clone_question_set_data is True:
            old_question_set_data = QuestionSetData.objects.filter(
                active=True, analysis_config=self)
            for qsd in old_question_set_data:
                qsd.id = None
                qsd.analysis_config = new_record
                qsd.save()
        return new_record

    def clean(self):
        super(AnalysisConfig, self).clean()
        # We do not validate the analysis_profile or analysis_config_template
        # relations because neither of those models have ownership set.
        if self.data_config is not None:
            self.validate(self.data_config)
        if self.partition_set_config is not None:
            self.validate(self.partition_set_config)

models.signals.m2m_changed.connect(
    AnalysisConfig.validate_m2m,
    sender=AnalysisConfig.feature_configs.through)

# This overrides OwnershipModel. The field in that model is not required.
# Note: Doing it this way because Django doesn't allow model field overrides.
_account_field = AnalysisConfig._meta.get_field('account')
_account_field.required = True
_account_field.null = False
_account_field.blank = False


class PartitionSetConfig(AnalysisConfigModel, OwnershipModel):
    """Configuration for partitioning of an analysis.

    Analyses may organize results into one or more partitions. For example,
    contributions and their impact on scores and totals may be partitioned by
    date ranges. A partition set may have a label for use in rendering, such
    as "Giving History Since {} Election Cycle".

    Attributes:
      title (str): Title
      description (str): Longer description
      label (str): Text for use in rendering this partition set
      partition_configs (PartitionConfigs): Partition configurations using in
        this partition set configuration
      account (Account): Owning account (optional)
      user (RevUpUser): Owning user (optional)

    """
    label = models.CharField(max_length=64, blank=True)
    partition_configs = models.ManyToManyField('PartitionConfig',
                                               related_name='+',
                                               blank=True)

    def build_partition_map(self):
        """Build a map of the partitions. These are used in views"""
        return [(p.id, p.label, p.years_of_giving)
                for p in self.partition_configs.all()]

models.signals.m2m_changed.connect(
    PartitionSetConfig.validate_m2m,
    sender=PartitionSetConfig.partition_configs.through)


class PartitionConfig(AnalysisConfigModel, OwnershipModel):
    """Configuration for a partition of an analysis.

    The attributes of a partition. All partitions must have a label for use in
    rendering.

    Attributes:
      title (str): Title
      description (str): Longer description
      index (int): Index of the partition (for use in ordering in a
        partition set)
      label (str): Text for use in rendering this partition
      definition (dict): Definition of this partition
      account (Account): Owning account (optional)
      user (RevUpUser): Owning user (optional)
    """
    index = models.IntegerField(blank=True, null=True)
    label = models.CharField(max_length=64)
    definition = JSONField(default={},
                           load_kwargs={'object_pairs_hook': OrderedDict})

    class Meta:
        ordering = ['index']

    @property
    @instance_cache
    def years_of_giving(self):
        start_date = self.definition.get('start_date')
        if start_date:
            if isinstance(start_date, str):
                start_date = datetime.strptime(start_date, '%Y-%m-%d')
            # TODO: support alternative election cycles
            previous_election = previous_election_date_for_office('H')
            yog = previous_election.year - start_date.year
        else:
            years_before = self.definition.get('years_before')
            if years_before:
                if isinstance(years_before, str):
                    years_before = int(years_before)
                yog = years_before
            else:
                errmsg = ("Unrecognized PartitionConfig definition."
                          " Unable to determine start of election"
                          " cycle for partition")
                LOGGER.error(errmsg)
                yog = None

        return yog


class FeatureConfig(AnalysisConfigModel, OwnershipModel,
                    DynamicClassModelMixin):
    """Configuration of an analysis feature.

    Analysis results may be configured to generate multiple features, such as
    additional data about the target (e.g. additional bio for a person),
    sets of signals from different data sets (e.g. electoral, academic, or
    nonprofit contributions, spectra (e.g. Dem vs Republican), positive or
    negative contributions (e.g. to a candidate or opponents), etc.

    A feature may be configured to be about a specific kind of record (e.g.
    individual FEC contributions). A feature may also be configured to contain
    sets of signals from one or more record sources (e.g. references to
    contributions to an opponent from past federal, state, and/or local
    elections.

    A feature may be associated with a specific spectrum configuration (e.g.
    Dem vs. Republican).

    Attributes:
      title (str): Title
      description (str): Longer description
      feature (Feature): Feature for which this is a configuration
      signal_set_configs (SignalSetConfigs): Signal set configs for this
        feature config
      partitioned (bool): Whether this feature is partitioned or not (if not,
        feature will appear in all partitions of an analysis
      icon (File): Image file (optional) for use in rendering this feature
      icon_name (str): Original name of icon (if any)
      text (str): Text for use in rendering this feature
      hidden (bool): Whether this feature config should be hidden from users
        other than staff
      legacy_alias (str): Alias for use in saving results in legacy form
      account (Account): Owning account (optional)
      user (RevUpUser): Owning user (optional)

    """
    feature = models.ForeignKey('Feature', related_name='+', null=True,
                                blank=True, on_delete=models.PROTECT)
    signal_set_configs = models.ManyToManyField(
        'SignalSetConfig',
        related_name='+',
        blank=True,
        through='analysis.FeatureConfigsSignalSets')
    partitioned = models.BooleanField(default=False)
    icon = models.ImageField(blank=True, null=True)
    icon_name = models.CharField(max_length=128, blank=True)
    text = models.TextField(blank=True)
    hidden = models.BooleanField(default=False)
    legacy_alias = models.CharField(max_length=32, blank=True)

    question_sets = models.ManyToManyField("QuestionSet",
                                           related_name="feature_configs",
                                           blank=True)
    filters = models.ManyToManyField("filtering.Filter",
                                     related_name="feature_configs",
                                     blank=True)

    @property
    def result_formatter(self):
        """Get the result formatter for this FeatureConfig.

        If no result formatter is specified, we use a default.
        """
        from frontend.apps.analysis.analyses import ResultFormatter
        dc = self.dynamic_class
        if dc:
            return dc
        else:
            return ResultFormatter

    def deep_copy(self, **kwargs):
        dc_kwargs = self._translate_copy_kwargs(**kwargs)
        return self.copy(deep_copy_fields={'signal_set_configs':
                                           {'deep_copy_fields':
                                            {'signal_set_config': dc_kwargs}}},
                         **kwargs)

    @transaction.atomic
    def delete_including_orphans(self):
        old_signal_set_configs = list(self.signal_set_configs.all())
        self.delete()
        for old_signal_set_config in old_signal_set_configs:
            try:
                old_signal_set_config.delete()
            except models.ProtectedError:
                LOGGER.warning('Unable to delete %s - still in use',
                               old_signal_set_config)

models.signals.m2m_changed.connect(
    FeatureConfig.validate_m2m,
    sender=FeatureConfig.question_sets.through)


class FeatureConfigsSignalSets(ValidatingModel):
    feature_config = models.ForeignKey('FeatureConfig',
                                       on_delete=models.PROTECT)
    signal_set_config = models.ForeignKey('SignalSetConfig',
                                          on_delete=models.PROTECT)
    order = models.IntegerField()

    class Meta:
        ordering = ['order']
        unique_together = (("feature_config", "order"),
                           ("feature_config", "signal_set_config"))

    def clean(self):
        super(FeatureConfigsSignalSets, self).clean()
        self.feature_config.validate(self.signal_set_config)


class QuestionSet(AnalysisConfigModel, DynamicFormBase, OwnershipModel):
    """A QuestionSet is the way we prompt the user for information about
    a Feature's configuration. Any Feature that has configuration options
    should also have one or more QuestionSets.

    If one QuestionSet's data could serve multiple Feature's configuration
    (e.g. political party), just link the same QuestionSet to all of the
    Features that benefit from the same user input.

    allow_multiple (bool): Whether multiple instances of this QuestionSet are
        allowed in an analysis config
    """
    icon = models.CharField(max_length=255, blank=True)
    staff_only = models.BooleanField(default=False)
    is_global = models.BooleanField(default=False)
    display_order = models.IntegerField(
        default=100,  # 100 is arbitrary
        help_text="Ordered from lowest to highest")


class QuestionSetDataManager(models.Manager):
    def configuration(self, question_set, analysis_config, active=True):
        kwargs = {
            'active': active,
            'analysis_config': analysis_config
        }
        if (isinstance(question_set, Iterable) and not
              isinstance(question_set, str)):
            kwargs["question_set__in"] = question_set
        else:
            kwargs["question_set"] = question_set
        return self.get_queryset().filter(**kwargs)


class QuestionSetData(ValidatingModel, TimeStampedModel):
    """Associated with a QuestionSet. This holds data provided by the user
    to setup the Feature. The data is used in the setup and execution of an
    Analysis.

    data: The data provided by the user to the FeatureConfig setup. This data
         should be in key:value format, as provided by the
         Question.form_clean method.
    """
    active = models.BooleanField(blank=True, default=True)
    question_set = models.ForeignKey('QuestionSet', on_delete=models.CASCADE, related_name="+")
    analysis_config = models.ForeignKey('AnalysisConfig', on_delete=models.CASCADE, related_name="+")
    version = models.DateTimeField()
    data = JSONField(default={}, blank=True)
    uid = models.CharField(max_length=256, blank=True)

    objects = QuestionSetDataManager()

    class Meta(TimeStampedModel.Meta):
        unique_together = (
            ("question_set", "analysis_config", "active", "uid"),)

    class DuplicateException(Exception): pass

    def __getitem__(self, key):
        """This allows the data to be accessed like a django template variable.

        This uses the django built-in Variable tag to resolve the variable,
        which means you can do all the same things here that you can in a
        template.

        E.g.:
        >>> q = QuestionSetData(data={"a": {"b": [1, 2]}})
        >>> q["a.b.1"]
        2
        """
        v = template.Variable(key)
        return v.resolve(self.data)

    def get(self, key, default=None):
        try:
            return self[key]
        except template.VariableDoesNotExist:
            return default

    def get_shallow_data(self):
        return self.question_set.dynamic_class().get_shallow_data(self.data)

    def get_deep_data(self):
        return self.question_set.dynamic_class().get_deep_data(self.data)

    def update(self, data, save=True):
        """Update this object with new data.
        This should typically only be done if the analysis config this data is
        associated with has not been executed since this object was created.
        """
        controller = self.question_set.dynamic_class()
        data = controller.process_data(data)
        uid = deep_hash(data)

        self.data = data
        self.uid = uid
        if save:
            try:
                self.save()
            except IntegrityError as e:
                if e.message.startswith(
                    'duplicate key value violates unique constraint'):

                    raise self.DuplicateException(
                        "Cannot save configuration. Either this is a duplicate, "
                        "or it has not changed.")
                else:
                    raise

            except ValidationError as e:
                if '__all__' not in e.message_dict:
                    raise
                elif any(_.endswith(' already exists.')
                         for _ in e.message_dict['__all__']):
                    if len(e.message_dict) > 1 or \
                       len(e.message_dict['__all__']) > 1:
                        LOGGER.exception("Validation errors in addition to "
                                         "Duplication error detected")

                    raise self.DuplicateException(
                        "Cannot save configuration. Either this is a duplicate, "
                        "or it has not changed.")
                else:
                    raise

    @classmethod
    def create(cls, question_set, analysis_config, data):
        qs_data = cls(question_set=question_set,
                      analysis_config=analysis_config)
        qs_data.update(data)
        return qs_data

    @classmethod
    def question_set_data_version(cls, instance, *args, **kwargs):
        """Ensure the version is set to the version of the analysis config when
        the QuestionSetData instance is created.
        """
        if not instance.version:
            instance.version = instance.analysis_config.last_execution

    def save(self, *args, **kwargs):
        self.question_set_data_version(self)
        super(QuestionSetData, self).save(*args, **kwargs)


class SignalSetConfig(AnalysisConfigModel, OwnershipModel):
    """Configuration of a set of analysis signals.

    Analysis features may be configured to contain multiple sets of signals,
    which are potential attributes (with some measure of confidence) of the
    target of an analysis result. Signal sets may include matching facts,
    (e.g. political, academic, and charitable contributions), specific values
    (e.g. distance of a contact from an event, predicted ethnicity), etc.

    Matching facts may only be from one specific record set. A signal set
    may also be associated with a spectrum (e.g. political spectrum within
    a specific electorate). A signal set may be configured to use one or more
    signal filters (e.g. references to committees associated with a particular
    opponent in a given electorate).

    Attributes:
      title (str): Title
      description (str): Longer description
      record_set_config (RecordSetConfig): Record set configuration (optional)
        for this signal set configuration
      signal_set_configs (SignalSetConfig): References to other signal sets (if
        any) on which this signal set depends
      spectrum_config (SpectrumConfig): Spectrum configuration for this signal
        set configuration
      signal_actions (SignalActionConfigs): Signal action configurations for
        this signal set configuration
      account (Account): Owning account (optional)
      user (RevUpUser): Owning user (optional)

    """
    record_set_config = models.ForeignKey('analysis.RecordSetConfig',
                                          related_name='+', null=True,
                                          blank=True,
                                          on_delete=models.PROTECT)
    signal_set_configs = models.ManyToManyField('analysis.SignalSetConfig',
                                                related_name='dependent_sets',
                                                blank=True)
    spectrum_config = models.ForeignKey('SpectrumConfig', related_name='+',
                                        blank=True, null=True,
                                        on_delete=models.PROTECT)
    signal_actions = models.ManyToManyField('SignalActionConfig',
                                            blank=True,
                                            through='SignalSetsActions')

    def get_record_set(self, analysis_config, feature_flags=None):
        if not self.record_set_config:
            return
        return RecordSet.factory(self.record_set_config, analysis_config,
                                 feature_flags=feature_flags)


    def clean(self):
        super(SignalSetConfig, self).clean()
        # Note: signal_action verification is handled in SignalSetsActions
        if self.record_set_config is not None:
            self.validate(self.record_set_config)
        if self.spectrum_config is not None:
            self.validate(self.spectrum_config)

models.signals.m2m_changed.connect(
    SignalSetConfig.validate_m2m,
    sender=SignalSetConfig.signal_set_configs.through)


class SignalSetsActions(ValidatingModel):
    """
    Through table to impose ordering in many-to-many relationships between
    signal set configs and signal action configs.

    Attributes:
      signal_set_config (SignalSetConfig): Signal set config of relation
      signal_action_config (SignalActionConfig): Signal action config of
        relation
      partitioned (bool): Whether a related signal action config respects
        analysis partitions in relation to this signal set config
      order (int): Ordinal position of a signal action config within a set of
       signal action configs related to a signal set config

    """
    signal_set_config = models.ForeignKey('SignalSetConfig',
                                          on_delete=models.PROTECT)
    signal_action_config = models.ForeignKey('SignalActionConfig',
                                             on_delete=models.PROTECT)
    partitioned = models.BooleanField(default=False)
    order = models.IntegerField()

    class Meta:
        ordering = ['order']
        verbose_name_plural = 'Signal sets actions'
        unique_together = (("signal_set_config", "order"),)

    def clean(self):
        super(SignalSetsActions, self).clean()
        self.signal_set_config.validate(self.signal_action_config)


class SignalActionConfig(AnalysisConfigModel, DynamicClassModelMixin,
                         OwnershipModel):
    """
    Configuration of an action for processing analysis signals.

    When executing an analysis, we may need to find signals and define
    their use in one or more features.

    Attributes:
      title (str): Title
      description (str): Longer description
      definition (dict): Definition of this signal action
      account (Account): Owning account (optional)
      user (RevUpUser): Owning user (optional)

    """
    definition = JSONField(default={}, blank=True,
                           load_kwargs={'object_pairs_hook': OrderedDict})
    partitioned = models.BooleanField(default=False)

    def __str__(self):
        if self.title:
            return self.title
        else:
            return self.definition.get('class')


class SpectrumConfig(AnalysisConfigModel, OwnershipModel):
    """
    Configuration of a spectrum for use in analyses.

    Analysis signals may be organized into one or more spectra, such as Dem vs.
    Republican in political giving.

    Attributes:
      title (str): Title
      description (str): Longer description
      definition (dict): Definition of this spectrum
      account (Account): Owning account (optional)
      user (RevUpUser): Owning user (optional)

    """
    definition = JSONField(default={},
                           load_kwargs={'object_pairs_hook': OrderedDict})
