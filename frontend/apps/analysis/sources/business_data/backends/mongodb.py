
from frontend.apps.analysis.sources.base import (
    MongodbRecordResource, RecordAccessWrapper)


class BusinessDataRecordWrapper(RecordAccessWrapper):
    id = '_id'
    owner_name = 'name'
    business_name = 'company_name'
    company_type = 'company_type'
    address = 'company_address'
    incorporation_date = 'incorporation_date'
    dissolution_date = 'dissolution_date'
    quality_score = 'QS'


class BusinessData(MongodbRecordResource):
    record_wrapper = BusinessDataRecordWrapper
    label = 'opencorp'
    expected_indexes = ['_id']
    uppercase_names = True
