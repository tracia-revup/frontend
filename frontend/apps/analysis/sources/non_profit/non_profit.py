from frontend.apps.analysis.sources import DataSet, RecordSet


class NonProfit(DataSet):
    """Parent data set for all nonprofit record sets."""


class Contributions(RecordSet):
    """Record set for nonprofit contributions."""
