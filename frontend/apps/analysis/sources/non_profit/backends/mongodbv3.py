import datetime

from frontend.apps.analysis.sources.base import (
    MongodbRecordResource, RecordAccessWrapper, TransformField, StrFormatField,
    DefaultValueField, Entities, MultipleKeyField)


class ContributionRecordWrapper(RecordAccessWrapper):
    id = '_id'
    given_name = 'entity_first_name'
    family_name = 'entity_last_name'
    amount = 'discrete_donation_value'
    date = TransformField('gift_year', 'transform_date')
    recipient = 'organization_name'
    ntee_category = StrFormatField("{ntee_level_1}{ntee_level_2}")
    contributor = TransformField(MultipleKeyField('entity_full_name',
                                                  'entity_occupation',
                                                  'entity_employer',
                                                  'entity_city',
                                                  'entity_state',
                                                  'entity_zip'),
                                 'stringify_data')

    @classmethod
    def stringify_data(cls, values):
        """Returns the data in string format rather than a list format."""
        # 'values' come with lists. We want to extract the first item from
        # each of the lists. The lists can be empty or missing.
        extracted_values = {}
        for k, v in values.items():
            if isinstance(v, (list, tuple)):
                try:
                    v = v[0]
                except IndexError:
                    v = ""
            extracted_values[k] = v or ""

        results = list(extracted_values.values())[0:3]
        results.append(
            "{entity_city}, {entity_state} {entity_zip}".format(
                **extracted_values))

        return results

    @classmethod
    def transform_date(cls, value):
        """Returns python date: it uses gift_year and returns date as January
        1st of gift_year. If gift_year doesn't exist, it returns None
        """
        if value:
            return datetime.date(value, 1, 1)


class Contributions(MongodbRecordResource):
    record_wrapper = ContributionRecordWrapper
    label = 'non_profit'
    expected_indexes = ['_id']
    uppercase_names = True
    sort_field = 'gift_year'
    match_field = 'EIN'
    qsd_match_field = ('ntee_level_1', 'ntee_level_2')

    def _fetch(self, contrib_ids):
        records = super()._fetch(contrib_ids)
        for record in records:
            # If a record has no date, discard it.
            if not record.date:
                continue
            yield record


class NonProfitEntitiesWrapper(RecordAccessWrapper):
    name = 'name'
    city = 'org_city'
    ntee = StrFormatField('{ntee_level_1}{ntee_level_2}')
    text_score = DefaultValueField("score", 0)
    eid = 'eid'
    record_set_config = 'record_set_config'


class NonProfitEntities(Entities):
    record_wrapper = NonProfitEntitiesWrapper

    def _prepare_projection(self, target=None, ntee_level_1=None,
                            ntee_level_2=None, **kwargs):
        """This method is expected to return a mongo projection,
        which is a list of query items.
        """
        projection = (super(NonProfitEntities, self)
                      ._prepare_projection(target, **kwargs))

        if not projection:
            projection = [dict()]

        if ntee_level_1:
            projection[0]['ntee_level_1'] = ntee_level_1

        if ntee_level_2:
            projection[0]["ntee_level_2"] = ntee_level_2

        return projection
