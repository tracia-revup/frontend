import logging

from bson import ObjectId

from frontend.apps.analysis.sources.base import (
    MongodbRecordResource, RecordAccessWrapper, ListStrFormatField)


LOGGER = logging.getLogger(__name__)


class NonProfitRecordWrapper(RecordAccessWrapper):
    id = '_id'
    name = 'name'
    given_name = 'forename'
    family_name = 'surname'
    amount = 'amount_input'
    # date = 'retrieval_list_date'
    recipient = 'organization'
    contributor = ListStrFormatField("{name}", "{amount_input}", "{source}")


class Contributions(MongodbRecordResource):
    record_wrapper = NonProfitRecordWrapper
    expected_indexes = ['_id', 'forename', 'surname']

    def _search(self, target, **kwargs):
        query = self.name_query(target)
        nonprofit_results = self._coll.find(query)

        return (result
                for result in nonprofit_results
                if not isinstance(result['_id'], ObjectId))
