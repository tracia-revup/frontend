
import logging

from frontend.apps.analysis.sources.base import (
    MongodbRecordResource, RecordAccessWrapper, ListStrFormatField)


LOGGER = logging.getLogger(__name__)


class ContributionRecordWrapper(RecordAccessWrapper):
    id = '_id'
    given_name = 'presentation_donor_first_name'
    family_name = 'presentation_donor_last_name'
    amount = 'presentation_contribution_level'
    recipient = 'presentation_organization'
    contributor = ListStrFormatField("{presentation_donor_names}",
                                     "{amount}", "{presentation_state}")


class Contributions(MongodbRecordResource):
    record_wrapper = ContributionRecordWrapper
    label = 'academic_nonprofit'
    expected_indexes = ['_id', ('analysis_donor_last_name',
                                'analysis_donor_first_name')]
    given_name_field = 'analysis_donor_first_name'
    family_name_field = 'analysis_donor_last_name'
    uppercase_names = True

    def _search(self, target, **kwargs):
        query = self.name_query(target)
        return self._coll.find(query)
