from datetime import datetime
import logging
import string

from schema import And, Or, Use

from frontend.apps.analysis.sources.base import (
    RecordAccessWrapper, MongodbRecordResource, ListStrFormatField, Entities,
    DefaultValueField, TransformField)


LOGGER = logging.getLogger(__name__)


class MunicipalRecordWrapper(RecordAccessWrapper):
    id = '_id'
    given_name = 'analysis_given_name'
    family_name = 'analysis_family_name'
    state = 'analysis_campaign_state'
    amount = 'analysis_amount'
    date = TransformField('analysis_date_received', '_transform_date')
    recipient = 'presentation_recipient'
    contributor = ListStrFormatField(
        "{presentation_donor_name}", "{presentation_occupation}",
        "{presentation_employer}",
        "{analysis_donor_city}, {analysis_donor_state} {analysis_donor_zip}")
    analysis_donor_state = DefaultValueField('analysis_donor_state', '',
                                             recursive=False)

    @classmethod
    def _transform_date(cls, value):
        return datetime.fromtimestamp(value)


class Contributions(MongodbRecordResource):
    record_wrapper = MunicipalRecordWrapper
    label = 'municipal'
    uppercase_names = True
    sort_field = "analysis_date_received"
    match_field = "analysis_municipal_recipient_id"
    expected_indexes = ['_id', ('analysis_family_name', 'analysis_given_name'),
                        "analysis_date_received"]
    record_schema = {
        'analysis_donor_state': And(str, len),
        'analysis_donor_zip': And(str, len),
        'analysis_date_received': int,
        'analysis_amount': Or(int, float, Use(float)),
    }

    def _search(self, target, **kwargs):
        query = self.name_query(target)
        return self._coll.find(query)


def reduce_campaigns(campaigns):
    best_count = 0
    best = (None,)*5

    # Attempt to pick the best campaign by the one with the most complete
    # data.
    for campaign in campaigns:
        office = None # campaign.get("analysis_campaign_office")
        city = campaign.get("analysis_campaign_city")
        state = campaign.get("analysis_campaign_state")
        cycle = campaign.get("analysis_campaign_campaign_election_cycle")
        district = campaign.get("analysis_campaign_district")
        count = len(list(filter(None, (office, city, state, district, cycle))))

        if count > best_count:
            best_count = count
            best = (office, city, state, district, cycle)

    # Only store the fields that are actually populated. This is necessary
    # so the default value can be selected correctly by DefaultValueField
    fields = ['office', 'city', 'state', 'district', 'cycle']
    result = {}
    for i in range(len(fields)):
        value = best[i]
        if value:
            result[fields[i]] = value

    # Clean up district
    if "district" in result:
        district = result["district"]
        result["district"] = district.lower().replace("district", "").strip()
    return result


class MunicipalEntitiesWrapper(RecordAccessWrapper):
    type = DefaultValueField("analysis_recipient_type", "")
    party = TransformField("analysis_party_type", Entities.format_party)
    campaign = TransformField("campaigns", reduce_campaigns)
    name = DefaultValueField("analysis_recipient_name", "")
    year = 'campaign.cycle'
    office = 'campaign.office'
    district = 'campaign.district'
    state = 'campaign.state'
    city = TransformField(DefaultValueField("campaign.city", ""),
                          string.capwords)
    text_score = DefaultValueField("score", 0)
    eid = 'eid'
    record_set_config = 'record_set_config'


class MunicipalEntities(Entities):
    """A RecordResource for the Municipal Entities collection, which includes
    city-and-county-level committees and candidates.
    """
    record_wrapper = MunicipalEntitiesWrapper

    def _prepare_projection(self, target=None, **kwargs):
        """This method is expected to return a mongo projection,
        which is a list of query items.
        """
        query = {"$text": {"$search": target}}
        # Exclude the contributions ids (it's a ton of IDs we don't need)
        text_score = {'score': {'$meta': "textScore"}, 'contributions_ids': 0}
        return [query, text_score]
