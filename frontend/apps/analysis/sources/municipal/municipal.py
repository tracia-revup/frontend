from frontend.apps.analysis.sources import DataSet, RecordSet


class Municipal(DataSet):
    """Parent data set for all municipal giving record sets."""


class Contributions(RecordSet):
    """Record set for municipal individual political campaign contributions."""
