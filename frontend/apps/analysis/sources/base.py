from abc import ABCMeta, abstractmethod
from collections import Mapping, Iterable
from itertools import chain, repeat
import logging
import pymongo
import re
import string
from types import FunctionType, MethodType

from bson.objectid import ObjectId, InvalidId
from django.conf import settings
from django import template
from schema import Schema, SchemaError

from frontend.libs.utils.string_utils import strip_punctuation
from frontend.libs.utils.celery_utils import log_and_dismiss_exception


LOGGER = logging.getLogger(__name__)


class DataSet(object, metaclass=ABCMeta):
    """A set of one or more record sets for use in analyses.

    A data set has one or more record sets. Examples include FEC, MISP, NGP,
    a group of a client's proprietary record sets, etc.

    """

    @classmethod
    def factory(cls, data_set_config, **rs_kwargs):
        data_set = data_set_config.data_set
        module = __import__(data_set.module_name, {}, {},
                            data_set.class_name)
        data_set_class = getattr(module, data_set.class_name)
        return data_set_class(data_set_config, **rs_kwargs)

    def __init__(self, data_set_config, **rs_kwargs):
        self.individuals = None
        self.data_set_config = data_set_config
        self.record_set_configs = self.data_set_config.record_set_configs.all()
        for record_set_config in self.record_set_configs:
            setattr(self, record_set_config.attribute_name,
                    RecordSet.factory(record_set_config, **rs_kwargs))

    # _cache = {}
    #
    # def __new__(cls, *args, **kwargs):
    #     config_id = kwargs['config'].id
    #     if config_id in DataSet._cache:
    #         instance = DataSet._cache[config_id]
    #     else:
    #         instance = object.__new__(cls)
    #         DataSet._cache[config_id] = instance
    #     return instance


class RecordSet(object, metaclass=ABCMeta):
    """A set of one kind of facts for use in analyses.

    A record set provides a set of one kind of facts. Examples include FEC
    candidates, committees, MISP contributions, NGP records, a client's
    proprietary record set, etc.

    """

    def __init__(self, record_set_config, analysis_config, feature_flags=None):
        self.analysis_config = analysis_config
        self.record_set_config = record_set_config
        self.record_resource = RecordResource.factory(
            self.record_set_config.resource_config, self, feature_flags)

    def fetch(self, target):
        return self.record_resource.fetch(target)

    def search(self, target, **kwargs):
        return self.record_resource.search(target=target, **kwargs)

    @classmethod
    def factory(cls, record_set_config, analysis_config, feature_flags=None):
        record_set = record_set_config.record_set
        return record_set.dynamic_class(record_set_config, analysis_config,
                                        feature_flags)

    def get(self, record_ids, limit=None):
        return self.record_resource.get(record_ids, limit=limit)


class RecordResource(object, metaclass=ABCMeta):
    """A resource that stores or indexes a record set.

    A record resource provides the actual storage, indexing, or other resource
    for a given record set. Examples might include specific databases,
    search engines, etc.

    """

    match_field = "_id"
    id_field = "_id"
    record_wrapper = None
    # Required property that is used to identify source type
    label = None
    # XXX: default record_schema to something that will pass if unset by
    # subclasses, or something that will fail if unset?
    record_schema = object

    @classmethod
    def factory(cls, resource_config, record_set, feature_flags=None):
        record_resource = resource_config.record_resource
        return record_resource.dynamic_class(resource_config, record_set,
                                             feature_flags)

    def __init__(self, resource_config, record_set, feature_flags=None):
        self.resource_config = resource_config
        self.parent_record_set = record_set
        self.resource_instance = self.resource_config.resource_instance

        self.feature_flags = feature_flags or {}
        self.use_alias_expansion = (feature_flags or {}).get(
            'Analysis Alias Expansion')
        self.use_name_expansion = (feature_flags or {}).get(
            "Analysis Name Expansion")
        self.verify_contrib_schema = (feature_flags or {}).get(
            "Analysis Verify Contribution Schema")

    def fetch(self, target):
        """Fetch is meant to be used by the RecordSearch signal action
        Queries database for contribution records using ids discovered by
        alternate means.
        """
        if self.label is None:
            raise NotImplementedError("Label attribute is not configured")

        contrib_ids = target.entity_contribution_id_mapping.get(
            self.label, [])
        LOGGER.debug("Fetch on %s for contrib ids %s", self.label, contrib_ids)
        return self._fetch(contrib_ids)

    def _fetch(self, contrib_ids):
        return self.get(contrib_ids)

    def get(self, record_ids, limit=None):
        if not isinstance(record_ids, Iterable) or \
           isinstance(record_ids, str):
            record_ids = [record_ids]

        for record in self._get(record_ids, limit=limit):
            if not isinstance(record, RecordAccessWrapper):
                record = self.record_wrapper(record)
            if not self.verify_contrib_schema or self.validate_record(
                    record._record):
                yield record

    def _get(self, record_ids, limit=None):
        raise NotImplementedError

    def search(self, target, **kwargs):
        for record in self._search(target, **kwargs):
            if not self.verify_contrib_schema or self.validate_record(record):
                yield self.record_wrapper(record)

    def _search(self, target, **kwargs):
        raise NotImplementedError

    def validate_record(self, record):
        """Test record to see if it matches the defined record schema

        Tests record against schema defined on attribute `record_schema` to
        determine whether record matches the schema described.

        Note: We do not care if there are fields on the record that are not
        described in the schema.

        Returns True if record matches schema, False otherwise.
        """
        record_schema = self.record_schema
        if isinstance(record_schema, (FunctionType, MethodType)):
            record_schema = record_schema()
        try:
            Schema(record_schema, ignore_extra_keys=True).validate(record)
        except SchemaError as err:
            schema_err = ' '.join(filter(None, err.autos))
            LOGGER.error(
                "SchemaError while validating record id: {record_id}. "
                "RecordSetConfig: ({rsc_id}){rsc_title}. "
                "Errors: {errors}".format(
                    record_id=record.get(self.id_field, 'missing id'),
                    rsc_id=self.parent_record_set.record_set_config.id,
                    rsc_title=self.parent_record_set.record_set_config.title,
                    errors=schema_err))
            return False
        else:
            return True


class IndexNotFound(Exception):
    pass


class MongodbRecordResource(RecordResource):
    """MongoDB-based record resource implementation base class."""
    sort_field = ""
    expected_indexes = None
    uppercase_names = False

    def __init__(self, *args, **kwargs):
        super(MongodbRecordResource, self).__init__(*args, **kwargs)
        self._set_db_handle()
        if self.expected_indexes:
            self.expect_indexes(self._coll, self.expected_indexes)
        if self.uppercase_names:
            self.case_method = 'upper'
        else:
            self.case_method = 'lower'
        if self.record_wrapper is not None:
            rename_map = self.record_wrapper._rename_map
            if (not hasattr(self, 'given_name_field') or
                    not self.given_name_field):
                self.given_name_field = rename_map.get('given_name')
            if (not hasattr(self, 'family_name_field') or
                    not self.family_name_field):
                self.family_name_field = rename_map.get('family_name')

    def _set_db_handle(self):
        self._db = settings.MONGO_CLIENT.get_default_database()
        self._coll = self._db[self.resource_instance.identifier] \
            if self.resource_instance.identifier else None

    def __getstate__(self):
        state = self.__dict__.copy()
        del state['_db']
        del state['_coll']
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        self._set_db_handle()

    def _get(self, record_ids, limit=None):
        # I'd prefer to make this a generator, but PyMongo wont accept it
        r_ids = []
        for r in record_ids:
            try:
                r_ids.append(ObjectId(r))
            except InvalidId:
                r_ids.append(r)
        record_ids = r_ids

        coll = self._coll.find({self.id_field: {"$in": record_ids}})
        if self.sort_field:
            # Set the default value for sort direction to descending. If the
            # sort_field is a list/tuple, direction should be set to None.
            # This is because pymongo expects each item in the list/tuple to
            # have direction as well.
            direction = pymongo.DESCENDING
            if isinstance(self.sort_field, (list, tuple)):
                direction = None
            coll = coll.sort(self.sort_field, direction)
        if limit is not None:
            coll = coll.limit(limit)
        return coll

    @classmethod
    def expect_indexes(cls, coll, expected):
        info = coll.index_information()
        # extract tuples of field names that build index
        indexes = [tuple(t[0] for t in vals.get('key', []))
                   for vals in info.values()]
        for index in expected:
            if isinstance(index, str):
                index = (index,)
            if index in indexes:
                continue
            if settings.DEBUG:
                LOGGER.warn('Missing index %s on collection %s', index,
                            coll)
            else:
                raise IndexNotFound({'coll': getattr(coll, 'name', str(coll)),
                                     'missing_index': index})

    def prep_name(self, name):
        return getattr(str(name), self.case_method)()

    def name_query(self, target):
        if self.record_wrapper is None:
            raise NotImplementedError

        remove_table = {ord(char): None
                        for char in string.punctuation
                        if char not in "-'"}
        given_names = target.get_known_first_names()
        given_names.update([strip_punctuation(name, remove_table)
                            for name in given_names])

        # Collection of given names to search over with meta characters
        # escaped. For given names discovered directly from contacts, we use
        # prefix matching.
        given_name_query_part = [
            re.compile("^" + re.escape(self.prep_name(name)))
            for name in given_names
        ]

        if self.use_name_expansion:
            names = target.get_nicknames()
            if names:
                # For any names discovered via name expansion, we use exact
                # matching.
                given_name_query_part.extend(
                    self.prep_name(name) for name in names)
        name_query = {
                self.given_name_field: {
                    "$in": given_name_query_part
                },
                self.family_name_field: {
                    "$in": [self.prep_name(name)
                            for name in target.get_known_last_names()]
                }
        }

        if self.use_alias_expansion:
            aliases = []
            for identity in target.aliases:
                aliases.append({
                    self.family_name_field: self.prep_name(
                        identity.family_name),
                    self.given_name_field: self.prep_name(identity.given_name)
                })
            if aliases:
                name_query = {"$or": aliases + [name_query]}
        return name_query

    def _search(self, target, **kwargs):
        raise NotImplementedError


class RecordAccessWrapperField(object, metaclass=ABCMeta):
    @abstractmethod
    def get(self, record_wrapper):
        pass


class RecordAccessWrapperMeta(ABCMeta):
    def __new__(mcs, name, bases, attrs):
        rename_map = {}
        special_fields = {}
        for k, v in attrs.items():
            if isinstance(v, str) and not (k.startswith('__') or
                                                  k.startswith('_abc_')):
                rename_map[k] = v
            elif isinstance(v, RecordAccessWrapperField):
                special_fields[k] = v

        # Remove the mapping definitions from the wrapper instance so attribute
        # access works
        for k in chain(rename_map, special_fields):
            attrs.pop(k)

        # Get the rename/special dicts from the subclasses and override
        for base in bases:
            if hasattr(base, "_rename_map"):
                temp = rename_map
                # noinspection PyProtectedMember
                rename_map = base._rename_map.copy()
                rename_map.update(temp)
            if hasattr(base, "_special_fields"):
                temp = special_fields
                # noinspection PyProtectedMember
                special_fields = base._special_fields.copy()
                special_fields.update(temp)

        attrs.update({
            "_rename_map": rename_map,
            "_special_fields": special_fields,
            "_inverse_map": {v: k for k, v in rename_map.items()}})
        return super(RecordAccessWrapperMeta, mcs).__new__(
            mcs, name, bases, attrs)


class RecordAccessWrapper(Mapping, metaclass=RecordAccessWrapperMeta):
    def __init__(self, record):
        super(RecordAccessWrapper, self).__init__()
        self._record = record
        self._special_fields_cache = {}

    def __len__(self):
        return len(self._record) + len(self._special_fields)

    def __contains__(self, key):
        if key in self._record:
            return True
        else:
            # Because a key could refer to a rename mapping, whose target
            # doesn't exist, or a special field (the value of which also might
            # not exist), there are no shortcuts to determining whether a key
            # actually exists.
            try:
                self[key]
            except KeyError:
                return False
            else:
                return True

    def __getitem__(self, name):
        r = self.get(name, default=KeyError)
        if r is KeyError:
            raise KeyError("'{0}'".format(name))
        return r

    def __getattr__(self, name):
        if name in ('_rename_map', '_record'):
            raise RuntimeError(
                "Essential attribute '{0}' missing!".format(name))
        r = self.get(name, default=AttributeError)
        if r is AttributeError:
            raise AttributeError(
                "'{0}' object has no attribute '{1}'".format(
                    self.__class__.__name__, name))
        return r

    def __iter__(self):
        renamed = (self._inverse_map.get(k, k) for k in self._record)
        seen = set()
        for key in chain(renamed, self._rename_map, self._special_fields):
            if key not in seen:
                seen.add(key)
                if key in self:
                    yield key

    @log_and_dismiss_exception((ValueError, TypeError,), LOGGER)
    def get(self, name, default=None, resolve=True):
        if not resolve:
            return self._record.get(name, default)

        if name in self._rename_map:
            name = self._rename_map[name]

        if name in self._special_fields:
            # Cache special field values, so we don't keep reprocessing them
            if name not in self._special_fields_cache:
                special_field = self._special_fields[name]
                self._special_fields_cache[name] = special_field.get(self)
            return self._special_fields_cache[name]

        if '.' in name:
            # Support accessing data like a django template variable.
            v = template.Variable(name)
            try:
                return v.resolve(self)
            except template.VariableDoesNotExist:
                pass
        return self._record.get(name, default)

    def get_all(self, only_remapped=True):
        """Return all of the values formatted into a dict.

        If only_remapped is True, then only the values that have a remapping
        are added to the dict.
        """
        if only_remapped:
            result = {}
        else:
            result = self._record.copy()

        # Add the remapped fields and the special fields to the results
        for key in set(chain(self._rename_map, self._special_fields)):
            try:
                result[key] = self[key]
            except KeyError:
                pass
        return result


class StrFormatField(RecordAccessWrapperField):
    """This is a special field that allows specification of multiple values
       into a field following a string format.

    For example:
    StrFormatField("{first_name} {last_name}")
    """
    class Formatter(string.Formatter):
        def get_value(self, key, args, kwargs):
            return kwargs.get(key, "")
    formatter = Formatter()

    def __init__(self, format_str):
        self.format_str = format_str

    def get(self, record_wrapper):
        return self.format(record_wrapper)

    def format(self, data):
        return self.formatter.vformat(self.format_str, [], data)


class ListStrFormatField(RecordAccessWrapperField):
    """This allows (and returns) a list of formatted fields.

    For example:
    ListStrFormatField("{first_name} {last_name}", "{street}",
                       "{city}, {state} {zip}")
    """
    def __init__(self, *args, **kwargs):
        self.filter_func = kwargs.get("filter_func")
        self.format_strings = [StrFormatField(item) for item in args]

    def get(self, record_wrapper):
        return self.format(record_wrapper)

    def format(self, data):
        if self.filter_func:
            return [val
                    for val in (fs.format(data) for fs in self.format_strings)
                    if self.filter_func(val)]
        else:
            return [fs.format(data) for fs in self.format_strings]


class MultipleKeyField(RecordAccessWrapperField):
    """MutipleKeyField wrapper returns a dictionary of values for the fields"""
    def __init__(self, *args, **kwargs):
        self.keys = args

    def get(self, record_wrapper):
        values = {}
        for key in self.keys:
            value = record_wrapper.get(key, None)
            values[key] = value
        return values


class StaticValueField(RecordAccessWrapperField):
    """StaticValueField wrapper is for setting a static value for a field."""
    def __init__(self, value):
        self.value = value

    def get(self, record_wrapper):
        return self.value


class TransformField(RecordAccessWrapperField):
    """TransformField wrapper is for running an arbitrary function to transform
    the value of the configured key.

    `key` may be a another a RecordAccessWrapperField, or an actual field name
    in the record.
    `func` may be a function reference, or it may be a string. If it is a
        string, it is expected to be a method of the RecordWrapper class.
    `recursive` is a boolean and is used to indicate whether `key` should be
    resolved on the parent record, or if no more resolution should be done.
    This is primarily meant to allow transformations on a value in the source
    record, where the key does not change. Example:

        date = TransformField('date', '_transform_date', recursive=False)

    Without the `recursive=False`, we'd get in an infinite loop.
    """
    def __init__(self, key, func, recursive=True):
        self.key = key
        self.recursive = recursive
        # Build the appropriate function to handle method name vs function ref
        if isinstance(func, str):
            self.func = lambda rw, value: getattr(rw, func)(value)
        else:
            self.func = lambda _, value: func(value)

    def get(self, record_wrapper):
        if isinstance(self.key, RecordAccessWrapperField):
            value = self.key.get(record_wrapper)
        else:
            value = record_wrapper.get(self.key,
                                       resolve=self.recursive)
        return self.func(record_wrapper, value)


class DefaultValueField(RecordAccessWrapperField):
    """DefaultValueField wrapper is for specifying a default value for a field.
    This has the advantage of not requiring the client code to figure out
    default values for itself.

    This is also nominally for the wrapper so `get_all` always includes the
    same fields.
    """
    def __init__(self, key, default, recursive=True):
        self.key = key
        self.default = default
        self.recursive = recursive

    def get(self, record_wrapper):
        return record_wrapper.get(self.key, self.default,
                                  resolve=self.recursive)


class MultiTryField(RecordAccessWrapperField):
    """MultiTryField wrapper is for checking multiple keys for the desired
    data.

    The primary purpose of this wrapper is to facilitate contribution schema
    changes by checking both the new and old key for the desired data.

    `keys` is a list of keys or RecordAccessWrapperField to attempt to get the
    data with. The keys are tried in order. If a key either doesn't exist or
    has a falsey value, the next key is tried.

    `recursive` toggles whether a string key is resolved when looked up, or if
    we go straight to the source record. This enables looking up the a key with
    the same name as the mapped field without entering an infinite loop.
    Valid values for `recursive` are a boolean, or a list of booleans. If a
    list of booleans are given, the number of bool values must match the number
    of keys. A list of booleans enables toggling resolution on specific keys.
    """
    def __init__(self, *keys, recursive=True):
        self.keys = keys
        self.recursive = recursive

    def get(self, record_wrapper):
        recursive = self.recursive
        if isinstance(recursive, Iterable) and \
                len(recursive) == len(self.keys):
            iargs = zip(self.keys, recursive)
        else:
            iargs = zip(self.keys, repeat(bool(recursive)))
        for args in iargs:
            value = self._try_get(record_wrapper, *args)
            if value is KeyError or not value:
                continue
            else:
                break
        return value if value is not KeyError else None

    def _try_get(self, record_wrapper, key, recursive):
        if isinstance(key, RecordAccessWrapperField):
            value = key.get(record_wrapper)
        else:
            value = record_wrapper.get(key, default=KeyError,
                                       resolve=recursive)
        return value


class Entities(MongodbRecordResource):
    def __init__(self, *args, **kwargs):
        super(Entities, self).__init__(*args, **kwargs)
        self.record_set_config = self.parent_record_set.record_set_config

    def pre_process(self, result):
        if "eid" not in result:
            eid = result.get(self.id_field)
            result["eid"] = eid
        if "record_set_config" not in result:
            result["record_set_config"] = self.record_set_config.id
        return result

    def _get(self, *args, **kwargs):
        results = super(Entities, self)._get(*args, **kwargs)
        for result in results:
            yield self.pre_process(result)

    def _prepare_projection(self, target=None, **kwargs):
        """This method is expected to return a mongo projection,
        which is a list of query items.
        """
        if target:
            query = {"$text": {"$search": target}}
            text_score = {'score': {'$meta': "textScore"}}
            return [query, text_score]
        else:
            return []

    def _sort(self, target=None, **kwargs):
        """Get the sort args for this search."""
        if target:
            return [('score', {'$meta': "textScore"})]
        else:
            return []

    def search(self, sort=True, **kwargs):
        """Search for FEC/State Candidates/Committees.
        The lookup is the same, only the collection differs.

        We optionally sort by mongo's text score, so we only get (at most)
        'limit' of the closest matches to the given target.
        """
        projection = self._prepare_projection(**kwargs)
        if projection:
            results = self._coll.find(*projection)
            if sort:
                sort_args = self._sort(**kwargs)
                if sort_args:
                    results = results.sort(sort_args)
            for result in results:
                result = self.pre_process(result)
                yield self.record_wrapper(result)

    def get_match_eids(self, eid):
        """Get the EIDs for the entity represented by this record resource
           to compare a contribution record to.

        Some records identify the entity by a different ID than is used in
        this record resource for identification.
        E.g. A FEC candidate is represented with a candidate ID, but an FEC
          record uses a committee ID.

        This method may be overridden by subclasses to convert the IDs to
        the correct type.
        E.g. FEC candidate ID to FEC committees for the candidate.
        """
        return {eid}

    @classmethod
    def format_party(cls, party):
        party = party.upper() if party else ""
        if party == "DEM":
            return "Democrat"
        elif party == "REP":
            return "Republican"
        elif party == "IND":
            return "Independent"
        elif party in ("UNK", "NNE"):
            return None
        else:
            return party or None
