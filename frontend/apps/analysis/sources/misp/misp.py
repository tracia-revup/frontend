from frontend.apps.analysis.sources import DataSet, RecordSet


class MISP(DataSet):
    """Parent data set for all MISP state political giving record sets."""


class Contributions(RecordSet):
    """Record set for MISP state individual political campaign contributions."""
