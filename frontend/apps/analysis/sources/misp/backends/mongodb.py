import collections
from datetime import date
import logging
import string

from schema import And, Or, Use

from frontend.apps.analysis.sources.base import (
    MongodbRecordResource, RecordAccessWrapper, ListStrFormatField, Entities)


LOGGER = logging.getLogger(__name__)


class MispRecordWrapper(RecordAccessWrapper):
    id = '_id'
    name = 'Name'
    given_name = 'GIVENNAME'
    family_name = 'LASTNAME'
    date = 'CFS_Date'
    amount = 'CFS_Amount'
    state = 'SAT_State'
    recipient = 'ElectionEntityname'
    party = 'display_party'
    contributor = ListStrFormatField(
        "{name}", "{Occupation}", "{EmployerName}",
        "{SAT_City}, {state} {SAT_Zip}")


class Contributions(MongodbRecordResource):
    record_wrapper = MispRecordWrapper
    label = 'misp'
    uppercase_names = False
    sort_field = "CFS_Date"
    match_field = "ElectionEntityEID"
    expected_indexes = [('LASTNAME', 'GIVENNAME'), '_id', 'CFS_Date',
                        'ElectionEntityEID']
    record_schema = {
        'SAT_State': And(str, len),
        'SAT_Zip': And(str, len),
        'CFS_Date': date,
        'CFS_Amount': Or(int, float, Use(float)),
    }

    def __init__(self, *args, **kwargs):
        super(Contributions, self).__init__(*args, **kwargs)
        self.election_entities = MISPElectionEntities(*args, **kwargs)

    def _get(self, record_ids, limit=None):
        records = super(Contributions, self)._get(record_ids, limit=limit)
        return self._add_party_and_override(records)

    def _search(self, target, **kwargs):
        query = self.name_query(target)
        records = self._coll.find(query)
        return self._add_party_and_override(records)

    def _add_party_and_override(self, records):
        """Add party and override (if any) to record"""
        election_entity_eids = set()
        contributions = []
        for record in records:
            election_entity_eid = record.get('ElectionEntityEID')
            if election_entity_eid:
                election_entity_eids.add(election_entity_eid)
            contributions.append(record)
        overrides = self.election_entities.get_entity_overrides(
            election_entity_eids)

        for contribution in contributions:
            party = self._lookup_party(contribution)
            contribution['party'] = party
            contribution['display_party'] = party
            election_entity_eid = contribution.get('ElectionEntityEID')
            if election_entity_eid and election_entity_eid in overrides:
                override = overrides[election_entity_eid]
                contribution['override'] = override
                contribution['display_party'] = override
            yield contribution

    def _lookup_party(self, record):

        # TODO: better handle variations like DFL, Independent Republicans
        # Party precedence: 1. New schema party; 2. Old schema party;
        # 3. Old schema label
        for key in ['General_Party:General_Party', 'General_Party',
                    'PartyType', 'FSC_Party']:
            party = record.get(key, None)
            if party:
                party = party.lower()
                # Handle name changes between old MISP dump and new API
                # TODO: remove once old dump retired and values confirmed
                if party == 'democrat':
                    party = 'democratic'
                if party == MISPParty.DEMOCRATIC.name:
                    return MISPParty.DEMOCRATIC.label
                elif party == MISPParty.REPUBLICAN.name:
                    return MISPParty.REPUBLICAN.label
        else:
            return MISPParty.OTHER.label


class MISPElectionEntities(MongodbRecordResource):

    Affiliation = collections.namedtuple('Affiliation', 'party score')

    def __init__(self, *args, **kwargs):
        super(MISPElectionEntities, self).__init__(*args, **kwargs)
        self.expect_indexes(self._db.st_affiliation_override,
                            ['_id', 'filer_ids', 'EntityEID'])
        self.override_cache = {}

    # TODO: limits?
    def get_entity_overrides(self, election_entity_eids):
        override_choices = {
            'd': MISPParty.DEMOCRATIC.label,
            'r': MISPParty.REPUBLICAN.label
        }
        results = {}
        search_entity_eids = {}

        for election_entity_eid in election_entity_eids:
            # Add previously cached overrides to results
            if election_entity_eid in self.override_cache:
                # Don't add cached None results
                if self.override_cache[election_entity_eid]:
                    results[election_entity_eid] = \
                        self.override_cache[election_entity_eid]
            # Add cache misses to search dict
            else:
                search_entity_eids[election_entity_eid] = election_entity_eid

        # Search for overrides
        override_query = {
            "$or": [
                {"filer_ids": {"$in": list(search_entity_eids.keys())}},
                {"EntityEID": {"$in": list(search_entity_eids.keys())}}
            ]
        }
        overrides = self._db.st_affiliation_override.find(override_query)

        # Add any found overrides to cached overrides and to results
        for override in overrides:
            election_entity_eids = set()
            election_entity_eids.add(override['EntityEID'])
            for filer_id in override.get('filer_ids', []):
                election_entity_eids.add(filer_id)
            party = override_choices.get(override['PartyAffiliation'],
                                         MISPParty.OTHER.label)
            for election_entity_eid in election_entity_eids:
                self.override_cache[election_entity_eid] = party
                results[election_entity_eid] = party
                # Remove entities with found overrides from search dict
                search_entity_eids.pop(election_entity_eid, None)

        # Cache empty result for any entities not having overrides
        for election_entity_eid in search_entity_eids:
            self.override_cache[election_entity_eid] = None

        return results


class MISPParty(object):

    Party = collections.namedtuple('Party', 'label name')

    DEMOCRATIC = Party('DEM', "democratic")
    REPUBLICAN = Party('REP', "republican")
    OTHER = Party('OTHER', "other")
    NONE = Party('', "none provided")

    @staticmethod
    def get_multiplier(campaign_party):
        # Multipliers
        if campaign_party == MISPParty.DEMOCRATIC:
            party_multiplier = {
                MISPParty.DEMOCRATIC.label: 1.90,
                MISPParty.OTHER.label: 1.0,
                MISPParty.REPUBLICAN.label: 0.10
            }
        elif campaign_party == MISPParty.REPUBLICAN:
            party_multiplier = {
                MISPParty.REPUBLICAN.label: 1.90,
                MISPParty.OTHER.label: 1.0,
                MISPParty.DEMOCRATIC.label: 0.10
            }
        else:
            party_multiplier = {
                MISPParty.DEMOCRATIC.label: 1.0,
                MISPParty.OTHER.label: 1.0,
                MISPParty.REPUBLICAN.label: 1.0
            }
        return party_multiplier


class StateEntitiesWrapper(RecordAccessWrapper):
    type = 'type'
    party = 'party'
    state = 'state'
    name = 'name'
    year = 'year'
    office = 'office'
    district = 'district'
    city = 'city'
    text_score = 'text_score'
    eid = 'eid'
    record_set_config = 'record_set_config'


class StateEntities(Entities):
    """A RecordResource for the State Entities collection, which includes
    state-level committees and candidates.
    """
    record_wrapper = StateEntitiesWrapper

    def pre_process(self, result):
        result = super(StateEntities, self).pre_process(result)
        elections = [(election.get("FilingYear", ""), election)
                     for election in result.get('elections', [])]
        # Sort by year so we get the most recent
        sorted(elections, key=lambda x: (x[0], x[1].get('ElectionState', '')), reverse=True)

        if elections:
            election = elections[0][1]
            year = elections[0][0]
            state = election.get("ElectionState")
            office_dist = election.get(
                "FSC_District", "").replace("\\N", "").title()

            dist_index = office_dist.find("District")
            if dist_index:
                office = office_dist[:dist_index - 1]
                district = office_dist[dist_index:]
            else:
                office = office_dist
                district = None

            party = election.get("PartyType",
                                 "").replace("\\N", "").title() or None
        else:
            year, state, office, party, district = (None,) * 5

        # The state entities collection stores names in a list of all known
        # entity names. This attempts to choose the best name by using
        # a heuristic. The idea is that the shortest name is probably the best
        # because it will have the least extra stuff in it.
        names = [n["ElectionEntityname"] for n in result.get("names", [])]
        try:
            name = names[0]
            for n in names:
                if len(n) < len(name):
                    name = n
        except IndexError:
            name = ""

        # Clean up district
        if district:
            district = district.lower().replace("district", "").strip()

        result.update({
            "type": "candidate" if office else "committee",
            "party": party,
            "state": state,
            "city": None,
            "name": string.capwords(name or ""),
            "year": year,
            "office": office,
            "district": district,
            "text_score": result.get("score", 0)
        })
        return result
