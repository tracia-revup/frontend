import logging

from frontend.apps.analysis.sources.misp.backends import mongodb

LOGGER = logging.getLogger(__name__)


class MispSwapRecordWrapper(mongodb.MispRecordWrapper):
    given_name = 'LASTNAME'
    family_name = 'GIVENNAME'


class Contributions(mongodb.Contributions):
    record_wrapper = MispSwapRecordWrapper
