from datetime import date
import logging

from django.conf import settings
from pymongo.errors import OperationFailure
from schema import And, Or, Use

from frontend.apps.analysis.sources.base import (
    MongodbRecordResource, RecordAccessWrapper, ListStrFormatField,
    MultiTryField)


LOGGER = logging.getLogger(__name__)


class ClientRecordWrapper(RecordAccessWrapper):
    id = '_id'
    name = 'ContactName'
    given_name = 'FirstName'
    family_name = 'LastName'
    state = 'PrimaryState'
    amount = 'Amount_Cleansed'
    date = 'ReceivedOn_Cleansed'
    recipient = 'organization_name'
    party = 'analysis_party'
    contributor = ListStrFormatField("{name}", "{Occupation}", "{Employer}",
                                     "{PrimaryCity}, {state} {PrimaryZip}")


class DynamicClientRecordWrapper(RecordAccessWrapper):
    id = '_id'
    name = MultiTryField('presentation_donor_name', 'ContactName')
    given_name = MultiTryField('analysis_given_name', 'FirstName')
    family_name = MultiTryField('analysis_family_name', 'LastName')
    state = MultiTryField('analysis_donor_state', 'PrimaryState')
    city = MultiTryField('analysis_donor_city', 'PrimaryCity')
    zip = MultiTryField('analysis_donor_zip', 'PrimaryZip')
    amount = MultiTryField('analysis_amount', 'Amount_Cleansed')
    date = MultiTryField('analysis_date_received', 'ReceivedOn_Cleansed')
    recipient = MultiTryField('presentation_recipient', 'organization_name')
    party = MultiTryField('analysis_party_type', 'analysis_party')
    occupation = MultiTryField('presentation_occupation', 'Occupation')
    employer = MultiTryField('presentation_employer', 'Employer')
    contributor = ListStrFormatField(
        "{name}", "{occupation}", "{employer}",
        "{city}, {state} {zip}")


class Contributions(MongodbRecordResource):
    record_wrapper = ClientRecordWrapper
    label = 'client'
    sort_field = "ReceivedOn_Cleansed"
    expected_indexes = [('LastName', 'FirstName'), '_id']
    record_schema = {
        'PrimaryState': And(str, len),
        'PrimaryZip': And(str, len),
        'ReceivedOn_Cleansed': date,
        'Amount_Cleansed': Or(int, float, Use(float)),
    }

    def _search(self, target, **kwargs):
        query = self.name_query(target)
        return self._coll.find(query)


class DynamicContributionsBase(Contributions):
    """Record Resource that calculates collection name to use

    This class queries mongodb for client contributions from a collection name
    that is generated using the account id and environment. This eliminates the
    need to create a separate data source config, FeatureConfig, and
    SignalActionConfig for every client. Instead we can use the same default
    client config for every account.

    The collection name is calculated by methods on the Account model, which we
    get a reference to from the AnalysisConfig via the reference we hold to our
    parent RecordSet.

    If there is no account on the AnalysisConfig, we fall back to the
    collection name found in the ResourceInstance.

    To account for clients who have no client data, we skip the
    expected_indexes check if the collection does not exist.
    """

    def _set_db_handle(self):
        self._db = settings.MONGO_CLIENT.get_default_database()
        coll_name = self._calc_collection_name()
        self._coll = self._db[coll_name] if coll_name else None

    def _calc_collection_name(self):
        """Calculate collection name for Account that owns the AnalysisConfig

        If the analysis config has no account associated with it, fall back to
        the collection name found in the ResourceIdentifier.
        """
        # Cache the calculated collection name on the instance to prevent an
        # AttributeError during deserialization caused by out of order loading
        # of the objects.
        if not hasattr(self, '_cached_collection_name'):
            acct = self.parent_record_set.analysis_config.account
            if acct:
                collection_name = acct.contrib_collection_name
            else:
                collection_name = self.resource_instance.identifier
            self._cached_collection_name = collection_name
        return self._cached_collection_name

    @classmethod
    def expect_indexes(cls, coll, expected):
        """Skip index check if collection doesn't exist"""
        db = coll.database
        name = coll.name
        try:
            db.command('collstats', name)
        except OperationFailure as err:
            if str(err).endswith(' not found.'):
                LOGGER.warn("Client collection '%s' does not exist yet.", name)
            else:
                raise
        else:
            return super().expect_indexes(coll, expected)


class DynamicContributions(DynamicContributionsBase):
    record_wrapper = DynamicClientRecordWrapper
    sort_field = "analysis_date_received"
    expected_indexes = [('analysis_family_name', 'analysis_given_name'), '_id']
    given_name_field = 'analysis_given_name'
    family_name_field = 'analysis_family_name'
    record_schema = {
        'analysis_donor_state': And(str, len),
        'analysis_donor_zip': And(str, len),
        'analysis_date_received': date,
        'analysis_amount': Or(int, float, Use(float)),
    }


class DynamicContributionsCompat(DynamicContributionsBase):
    expected_indexes = ['_id']
    label = 'client_data'
