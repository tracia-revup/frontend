from frontend.apps.analysis.sources import DataSet, RecordSet


class Client(DataSet):
    """Parent data set for all client record sets."""


class Contributions(RecordSet):
    """Record set for client individual political campaign contributions."""
