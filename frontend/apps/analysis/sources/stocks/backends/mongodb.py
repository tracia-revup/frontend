
from frontend.apps.analysis.sources.base import (
    MongodbRecordResource, RecordAccessWrapper)


class StocksRecordWrapper(RecordAccessWrapper):
    id = '_id'
    name = 'holder_name'
    org_name = 'OrgName'
    symbol = 'Symbol'
    shares = 'Shares'
    amount = 'MarketValue'
    transaction_date = 'TransactionDate'
    sec_address = 'sec_address'
    sec_city = 'sec_city'
    sec_state = 'sec_state'
    sec_zip = 'sec_zip'
    title = 'title'
    company_of_title = 'company_of_title'
    date_of_title = 'date_of_title'
    quality_score = 'QS'


class Stocks(MongodbRecordResource):
    record_wrapper = StocksRecordWrapper
    label = 'sec'
    expected_indexes = ['_id']
    uppercase_names = True
