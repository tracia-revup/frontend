from frontend.apps.analysis.sources import DataSet, RecordSet


class Purdue(DataSet):
    """Parent data set for all academic record sets."""


class PurdueBioRecordSet(RecordSet):
    def past_addresses(self, target):
        return self.record_resource.past_addresses(target)
