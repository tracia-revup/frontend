from abc import abstractproperty
from datetime import datetime
import logging

import phonenumbers

from frontend.apps.analysis.sources.base import (
    RecordAccessWrapper, MongodbRecordResource, ListStrFormatField,
    StrFormatField, TransformField)
from frontend.apps.contact.utils import PhoneNumberMixin
from frontend.libs.utils.string_utils import str_to_bool

LOGGER = logging.getLogger(__name__)


class PurdueBioRecordWrapper(RecordAccessWrapper):
    id = '_id'
    bio_id = '_id'
    given_name = 'forename'
    family_name = 'surname'
    title = 'title'
    company = 'company'
    school = 'school'
    graduation_year = 'gradyr'
    major = 'major'
    degree = 'degree'
    top_donor = TransformField('largest_donor', str_to_bool)
    major_contributor = TransformField('life_agg_10K', lambda x: x == '1')
    email = TransformField('email', '_strip_lower', recursive=False)
    zip = TransformField('zip', str.strip, recursive=False)
    home_phone = TransformField('hphone', "_format_phone")
    mobile_phone = TransformField('mobphn', "_format_phone")
    business_phone = TransformField('bphone', "_format_phone")
    phones = ListStrFormatField("{home_phone}", "{business_phone}",
                                "{mobile_phone}",
                                filter_func=lambda v: bool(v))

    @classmethod
    def _format_phone(cls, value):
        pnm = PhoneNumberMixin()
        pnm.number = value
        try:
            return pnm.rfc3966
        except phonenumbers.NumberParseException:
            return value

    @classmethod
    def _strip_lower(cls, value):
        if not value:
            return
        return value.strip().lower()


class PurdueBioRecordResource(MongodbRecordResource):
    record_wrapper = PurdueBioRecordWrapper
    expected_indexes = ['_id', 'forename', 'surname']

    def _search(self, target, **kwargs):
        query = self.name_query(target)
        results = self._coll.find(query)
        return results

    def past_addresses(self, target):
        query = {"ID": target.bio_id}
        results = self._db.purdue_past_address.find(query)

        for result in results:
            yield PurduePastAddressRecordWrapper(result)


class PurdueIdMatch(MongodbRecordResource):
    sort_field = "gdat"

    @abstractproperty
    def record_wrapper(self):
        pass

    def _search(self, target, **kwargs):
        query = {"ID": target.bio_id}
        results = self._coll.find(query)
        return results


class PurdueGivingRecordWrapper(RecordAccessWrapper):
    id = '_id'
    bio_id = 'ID'
    department = 'department'
    amount = TransformField('gamt', '_transform_amount')
    fiscal_year = 'fiscalyr'
    date = TransformField('gdat', '_transform_date')
    school = 'school'

    @classmethod
    def _transform_amount(cls, value):
        try:
            return float(value)
        except ValueError:
            return 0.0

    @classmethod
    def _transform_date(cls, value):
        return datetime.strptime(value, '%Y%m%d')


class PurdueGivingRecordResource(PurdueIdMatch):
    record_wrapper = PurdueGivingRecordWrapper


class PurdueEduRecordWrapper(RecordAccessWrapper):
    id = '_id'
    bio_id = 'ID'
    degree = 'degree'
    graduation_year = 'gradyr'
    majors = ListStrFormatField("{major}", "{major2}")
    school = 'school'


class PurdueEduRecordResource(PurdueIdMatch):
    record_wrapper = PurdueEduRecordWrapper


class PurdueActivityRecordWrapper(RecordAccessWrapper):
    id = '_id'
    bio_id = 'ID'
    start_date = TransformField('start_dt', '_transform_date')
    stop_date = TransformField('stop_dt', '_transform_date')
    activity = 'student_activity'
    participation = 'student_participation'

    @classmethod
    def _transform_date(cls, value):
        if not value:
            return
        try:
            return datetime.strptime(value, '%Y%m%d.0')
        except ValueError:
            try:
                return datetime.strptime(value, '%Y0000.0')
            except ValueError:
                pass


class PurdueActivityRecordResource(PurdueIdMatch):
    record_wrapper = PurdueActivityRecordWrapper


class PurduePastAddressRecordWrapper(RecordAccessWrapper):
    id = '_id'
    bio_id = 'ID'
    state = 'state_code'
    zip = TransformField('zipcode', str.strip)
    phone = StrFormatField("{phone_area_code}{phone_number}")


class PurduePastAddressRecordResource(PurdueIdMatch):
    record_wrapper = PurduePastAddressRecordWrapper
