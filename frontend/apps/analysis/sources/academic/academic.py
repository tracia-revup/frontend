from frontend.apps.analysis.sources import DataSet, RecordSet


class Academic(DataSet):
    """Parent data set for all academic record sets."""


class Contributions(RecordSet):
    """Record set for academic contributions."""
