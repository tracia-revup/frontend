import logging

from bson import ObjectId

from frontend.apps.analysis.sources.base import (
    MongodbRecordResource, RecordAccessWrapper, ListStrFormatField)


LOGGER = logging.getLogger(__name__)


class AcademicRecordWrapper(RecordAccessWrapper):
    id = '_id'
    name = 'name'
    given_name = 'forename'
    family_name = 'surname'
    amount = 'amount_input'
    date = 'date_source_accessed'
    recipient = 'organization'
    source = "source"
    contributor = ListStrFormatField("{name}", "{amount}", "{source}",
                                     "{date}")


class Contributions(MongodbRecordResource):
    record_wrapper = AcademicRecordWrapper
    label = 'academic'
    expected_indexes = ['_id', 'forename', 'surname']

    def _search(self, target, **kwargs):
        query = self.name_query(target)
        academic_results = self._coll.find(query)

        return (result
                for result in academic_results
                if not isinstance(result['_id'], ObjectId))
