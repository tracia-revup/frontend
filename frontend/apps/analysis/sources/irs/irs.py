from frontend.apps.analysis.sources import DataSet, RecordSet


class IRS(DataSet):
    """Parent data set for IRS record sets."""


class PoliticalContributions(RecordSet):
    """Record set for political organization contributions disclosed to IRS."""
