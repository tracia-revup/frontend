"""
MongoDB implementation classes for contributions disclosed to IRS
"""
from datetime import date
import logging
import string

from schema import And, Or, Use

from frontend.apps.analysis.sources.base import (
    MongodbRecordResource, RecordAccessWrapper, ListStrFormatField, Entities,
    StaticValueField, DefaultValueField, TransformField)


LOGGER = logging.getLogger(__name__)


class PoliticalContributionRecordWrapper(RecordAccessWrapper):
    """Wrapper for MongoDB IRS political organization contributions.
    """
    id = '_id'
    name = 'ContactName'
    given_name = 'FirstName'
    family_name = 'LastName'
    state = 'PrimaryState'
    amount = 'Amount_Cleansed'
    date = 'ReceivedOn_Cleansed'
    recipient = 'organization_name'
    party = 'analysis_party'
    contributor = ListStrFormatField("{name}", "{Occupation}", "{Employer}",
                                     "{PrimaryCity}, {state} {PrimaryZip}")


class PoliticalContributions(MongodbRecordResource):
    """MongoDB IRS political organization contributions.
    """
    record_wrapper = PoliticalContributionRecordWrapper
    label = 'irs'
    sort_field = "ReceivedOn_Cleansed"
    match_field = "analysis_recipient_id"
    expected_indexes = [('LastName', 'FirstName'), '_id']
    record_schema = {
        'PrimaryState': And(str, len),
        'PrimaryZip': And(str, len),
        'ReceivedOn_Cleansed': date,
        'Amount_Cleansed': Or(int, float, Use(float)),
    }

    def _search(self, target, **kwargs):
        query = self.name_query(target)
        return self._coll.find(query)


def format_office(office):
    office = office.upper() if office else ""
    if office == "G":
        return "Governor"
    else:
        return office or None


class PoliticalOrganizationsWrapper(RecordAccessWrapper):
    type = DefaultValueField("organization_type", "")
    party = DefaultValueField("analysis_party", "")
    state = DefaultValueField("organization_state", "")
    city = TransformField(DefaultValueField("organization_city", ""),
                          string.capwords)
    name = DefaultValueField("organization_name", "")
    year = StaticValueField(None)
    office = TransformField("organization_office", format_office)
    district = StaticValueField(None)
    text_score = DefaultValueField("score", 0)
    eid = 'eid'
    record_set_config = 'record_set_config'


class PoliticalOrganizations(Entities):
    """A RecordSet for the IRS political organizations collection.
    """
    record_wrapper = PoliticalOrganizationsWrapper
