
from frontend.apps.analysis.sources.base import (
    RecordAccessWrapper, MongodbRecordResource, ListStrFormatField,
    StrFormatField, TransformField)


class PersonRecordWrapper(RecordAccessWrapper):
    id = '_id'
    composite_ids = 'composite_id'


class PersonRecordResource(MongodbRecordResource):
    record_wrapper = PersonRecordWrapper
    label = 'person'
    expected_indexes = ['_id', 'composite_id']

    def _search(self, target, **kwargs):
        composite_ids = list(target.get_composite_ids())
        query = {"composite_id": {"$elemMatch": {"$in": composite_ids}}}
        results = self._coll.find(query)
        return results

    def fused_entities(self, fused_entity_ids):
        """Used by PersonEntityResolveContributionIds to fetch fused entity
        records.
        """
        return self.get_source_entities('fused_entities', fused_entity_ids)

    def get_source_entities(self, collection_name, entity_ids):
        """Used by PersonEntityResolveContributionIds to fetch source specific
        entities (fec, misp, etc) from the given collection, excluding those
        with no such entities (which will have '' in entity_ids).
        """
        query = {'_id': {'$in': [id for id in entity_ids if id]}}
        results = self._db[collection_name].find(query)
        for result in results:
            yield result
