from frontend.apps.analysis.sources import RecordSet


class PersonRecordSet(RecordSet):
    def fused_entities(self, target):
        return self.record_resource.fused_entities(target)

    def get_source_entities(self, collection_name, target):
        return self.record_resource.get_source_entities(
            collection_name, target)
