from frontend.apps.analysis.sources import DataSet, RecordSet


class FEC(DataSet):
    """Parent data set for all FEC record sets."""


class IndividualContributions(RecordSet):
    """Record set for FEC individual political campaign contributions."""
    def get_committees_candidates(self, records):
        return self.record_resource.get_committees_candidates(records)
