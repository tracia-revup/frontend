import collections
import datetime
import itertools
import logging
import string
import re

from schema import And, Or, Use

from frontend.apps.analysis.sources.base import (
    MongodbRecordResource, RecordAccessWrapper, Entities, ListStrFormatField,
    StaticValueField, DefaultValueField, TransformField, StrFormatField)


LOGGER = logging.getLogger(__name__)


class IndividualContributionsRecord(RecordAccessWrapper):
    id = '_id'
    name = DefaultValueField('NAME',
                             StrFormatField("{given_name} {family_name}"))
    given_name = 'GIVENNAME'
    family_name = 'LASTNAME'
    date = 'TRANS_DATE'
    amount = 'TRANSACTION_AMT'
    recipient = 'recipient'
    party = 'display_party'
    contributor = ListStrFormatField("{name}", "{OCCUPATION}", "{EMPLOYER}",
                                     "{CITY}, {STATE} {ZIP_CODE}")


class IndividualContributionsDaily(MongodbRecordResource):
    record_wrapper = IndividualContributionsRecord
    label = 'fec'
    uppercase_names = True
    sort_field = "TRANS_DATE"
    match_field = "CMTE_ID"
    expected_indexes = ['_id', 'TRANS_DATE']
    committee_earmark_patterns = {
        'ACTBLUE': re.compile(
            r'^EARMARK(?:ED)? (?:FOR|TO):? (.*) \((?:C\d+)?\)'),
        'JSTREETPAC': re.compile(r'^EARMARKED FOR (.*)'),
    }
    record_schema = {
        'STATE': And(str, len),
        'ZIP_CODE': And(str, len),
        'CMTE_ID': And(str, len),
        'TRANS_DATE': datetime.date,
        'TRANSACTION_AMT': Or(int, float, Use(float)),
    }

    def __init__(self, *args, **kwargs):
        super(IndividualContributionsDaily, self).__init__(*args, **kwargs)
        self.expect_indexes(self._db.cm, ['_id'])
        self.expect_indexes(self._db.cn, ['_id', 'CAND_NAME'])
        self.expect_indexes(self._db.ccl, ['_id', 'CAND_ID', 'CMTE_ID'])
        self.fec_committees = Committees(*args, **kwargs)
        self.affiliations = {}

    @classmethod
    def _is_jstreet_earmarked_record(cls, record):
        """Test record to see if it is a Jstreet earmarked record."""
        # C00441949 is the committee id for JStreetPac
        return (record['CMTE_ID'] == 'C00441949' and
                'EARMARK' in record.get('MEMO_TEXT', '').upper())

    def _fetch(self, contrib_ids):
        """Drop JStreet earmarked contributions as they are dupes. This is a
        temporary measure until something more permanent can be put in place in
        the FEC entity generation side.

        We put the filtering code in this method in order to prevent it from
        affecting the display of previously generated results, and so that it
        only is used in entity analysis.
        """
        records = super(IndividualContributionsDaily, self)._fetch(contrib_ids)
        for record in records:
            if self._is_jstreet_earmarked_record(record):
                continue
            yield record

    def _parse_earmarked_memo(self, committee_name, record):
        pattern = self.committee_earmark_patterns.get(committee_name, None)
        memo_text = record['MEMO_TEXT']
        if pattern and memo_text:
            match = re.search(pattern, memo_text)
            if match:
                recipient = match.group(1)
                return "{} - FOR {}".format(committee_name, recipient)
        return committee_name

    def _inject_affiliation_data(self, records):
        # Add the party affiliations for the records to the affiliations cache
        results = list(records)
        committees_candidates = self.get_committees_candidates(results)
        self._add_affiliations(results)

        for result in results:
            committee_id = result[self.match_field]
            if committee_id in self.affiliations:
                committee = self.affiliations[committee_id]
                result['party'] = committee['party']
                if 'override' in committee:
                    result['override'] = committee['override']
                    result['display_party'] = committee['override']
            else:
                result['party'] = 'UNK'

            if "display_party" not in result:
                result['display_party'] = result['party']

            committee, candidate = committees_candidates[committee_id]
            if candidate:
                result['recipient'] = candidate['CAND_NAME']
            elif committee:
                result['recipient'] = self._parse_earmarked_memo(
                    committee['CMTE_NM'], result)
            else:
                LOGGER.warn("No candidate or committee record found for FEC "
                            "record: %s", result['_id'])
                result['recipient'] = ''
        return results

    def _get(self, *args, **kwargs):
        records = super(IndividualContributionsDaily,
                        self)._get(*args, **kwargs)
        results = self._inject_affiliation_data(records)
        return results

    def get_committees_candidates(self, records):

        committees = {}
        candidates = {}
        committees_candidates = {}
        committee_ids = set()
        candidate_ids = set()

        for record in records:
            if 'CAND_ID' in record:
                candidate_ids.add(record['CAND_ID'])
            committee_ids.add(record[self.match_field])

        committee_query = {
            "_id": {"$in": [cm_id for cm_id in committee_ids]}}
        committee_records = self._db.cm.find(committee_query)

        for record in committee_records:
            if 'CAND_ID' in record:
                candidate_ids.add(record['CAND_ID'])
            committees[record['_id']] = record

        candidate_query = {
            "_id": {"$in": [cn_id for cn_id in candidate_ids]}}
        candidate_records = self._db.cn.find(candidate_query)

        for record in candidate_records:
            candidates[record['_id']] = record

        for record in records:
            committee_id = record[self.match_field]
            committee = committees.get(committee_id)

            candidate_id = record.get('CAND_ID')
            if (not candidate_id) and committee:
                candidate_id = committee.get('CAND_ID')
            if candidate_id:
                candidate = candidates.get(candidate_id)
            else:
                candidate = None
            committees_candidates[committee_id] = (committee, candidate)

        return committees_candidates

    def _search(self, target, **kwargs):
        query = self.name_query(target)
        query.update(
            {"ENTITY_TP": "IND",
             "TRANSACTION_TP": {"$not": {"$in": ["15T", "24T"]}}})

        records = self._coll.find(query)
        records = self._filter_current_records(records)
        results = []
        committee_ids = set()

        for record in records:
            committee_ids.add(record[self.match_field])
            results.append(record)

        # Add the party affiliations for the records to the affiliations cache
        self._add_affiliations(results)

        for result in results:
            committee_id = result[self.match_field]
            if committee_id in self.affiliations:
                committee = self.affiliations[committee_id]
                result['party'] = committee['party']
                if 'override' in committee:
                    result['override'] = committee['override']
                    result['display_party'] = committee['override']
            else:
                result['party'] = 'UNK'

            if "display_party" not in result:
                result['display_party'] = result['party']
        return results

    # TODO: Move  resolution work to data set to allow resource independence
    def _add_affiliations(self, records):

        # Get the committees not already cached
        committee_ids = [record[self.match_field] for record in records
                         if record[self.match_field] not in self.affiliations]
        affiliations = self.fec_committees.get_affiliations(
            committee_ids)

        for affiliation in affiliations:
            self.affiliations[affiliation] = affiliations[affiliation]

    @classmethod
    def _filter_current_records(cls, records):
        """This method is used to filter the fec records of this class
        according to its status as a termination record.

        While it doesn't typically happen, we'll use this logic in filtering
        the records.

        Take the most recent record (ordered by date), if there's still a
        disparity (there shouldn't be), we'll take the record in this
        precedence {T, A, N, <blank>}"""
        results = []

        def fec_rec_key(record):
            trans_id = record.get('TRAN_ID')
            trans_date = record.get('TRANS_DATE',
                                    datetime.datetime(1901, 1, 1))
            amend_ind = record.get('AMNDT_IND')
            try:
                file_num = int(record.get('FILE_NUM', 0))
            except ValueError:
                # On ValueError in above cast we'll just set to 0
                file_num = 0
            return trans_id, trans_date, amend_ind, file_num

        def tran_key(record):
            _, trans_date, amend_ind, file_num = fec_rec_key(record)
            return trans_date, amend_ind, file_num

        # This sort works as our key for dates and amndt_ind because
        # 'T' > 'N' > 'A'
        # We also assume as before, that FILE_NUMs are monotonically increasing
        # using this as tiebreaker if everything else in key is the same
        records = sorted(records, key=fec_rec_key, reverse=True)

        # Then we group by TRAN_ID
        for k, g in itertools.groupby(records, lambda x: x.get('TRAN_ID')):
            # If TRAN_ID == '' we add to results because we can't group by ''
            if k == '':
                results.extend(list(g))
            else:  # Otherwise we take the largest key
                results.append(max(g, key=tran_key))
        return results


class IndividualContributionsWeekly(IndividualContributionsDaily):
    expected_indexes = [('LASTNAME', 'GIVENNAME'), '_id', 'TRANS_DATE']

    def __init__(self, *args, **kwargs):
        super(IndividualContributionsWeekly, self).__init__(*args, **kwargs)
        self.use_fec_daily_ids = self.feature_flags.get('FEC Use Daily Ids')

    def _fetch(self, contrib_ids):
        if not contrib_ids:
            return []
        if not self.use_fec_daily_ids:
            return super(IndividualContributionsWeekly, self)._fetch(
                contrib_ids)
        else:
            # If `self.use_fec_daily_ids` is True, that means we should expect
            # `contrib_ids` to be FEC daily ids, which are calculated from 3
            # fields, rather than be SUB_IDs. This being the case, we need to
            # parse out the field values and construct a different database
            # query.
            query_parts = []
            bad_fec_daily_ids = []
            for fec_daily_id in contrib_ids:
                id_parts = list(filter(None, fec_daily_id.split('_', 2)))
                if len(id_parts) != 3:
                    bad_fec_daily_ids.append(fec_daily_id)
                    continue
                cmte_id, file_num, tran_id = id_parts
                query_parts.append({
                    'CMTE_ID': cmte_id,
                    'FILE_NUM': file_num,
                    'TRAN_ID': tran_id
                })
            if bad_fec_daily_ids:
                LOGGER.warn("Bad FEC Daily IDs found: %s",
                            ", ".join(bad_fec_daily_ids))

            query = {"$or": query_parts}
            records = self._coll.find(query)
            if self.sort_field:
                records = records.sort(self.sort_field, -1)
            records = self._inject_affiliation_data(records)
            return list(map(self.record_wrapper, records))


IndividualContributions = IndividualContributionsWeekly


class FECEntities(Entities):
    @classmethod
    def format_office(cls, office):
        office = office.upper() if office else ""
        if office == "H":
            return "House"
        elif office == "S":
            return "Senate"
        elif office == "P":
            return "President"
        else:
            return office or None


class FECCommitteesWrapper(RecordAccessWrapper):
    type = StaticValueField("committee")
    party = TransformField("CMTE_PTY_AFFILIATION", Entities.format_party)
    state = "CMTE_ST"
    city = TransformField(DefaultValueField("CMTE_CITY", ""), string.capwords)
    name = TransformField(DefaultValueField("CMTE_NM", ""), string.capwords)
    year = StaticValueField(None)
    office = StaticValueField(None)
    district = StaticValueField(None)
    text_score = DefaultValueField("score", 0)
    eid = 'eid'
    record_set_config = 'record_set_config'


# Not to be confused with Committees below, this is actually a RecordResource
class FECCommittees(FECEntities):
    """A RecordSet for the FEC Committees collection."""
    record_wrapper = FECCommitteesWrapper


class FECCandidatesWrapper(RecordAccessWrapper):
    type = StaticValueField("candidate")
    party = TransformField("CAND_PTY_AFFILIATION", Entities.format_party)
    state = "CAND_OFFICE_ST"
    city = TransformField("CAND_CITY", "format_city")
    name = TransformField(DefaultValueField("CAND_NAME", ""), string.capwords)
    year = "CAND_ELECTION_YR"
    office = TransformField("CAND_OFFICE", FECEntities.format_office)
    district = TransformField(DefaultValueField("CAND_OFFICE_DISTRICT", ""),
                              "format_district")
    text_score = DefaultValueField("score", 0)
    eid = 'eid'
    record_set_config = 'record_set_config'

    def format_district(self, value):
        district = value.lower().replace("district", "").strip()
        # If 00, it is a Presidential
        if district == "00":
            district = None
        return district

    def format_city(self, value):
        # Only show the city for House candidates (otherwise it doesn't make
        # much sense).
        if self.office == "House":
            if isinstance(value, str):
                value = string.capwords(value)
            return value
        else:
            return None


class FECCandidates(FECEntities):
    """A RecordSet for the FEC Candidates collection."""
    record_wrapper = FECCandidatesWrapper

    def get_match_eids(self, eid):
        """Override the default because Candidate EIDs need to be converted
        to committee EIDs.
        :param eid: candidate id
        """
        # TODO: Fix this. Ideally, we should know the committee resource.
        #  I really hate doing this, but I don't have a clear alternative
        return {cm["_id"] for cm in self._db.cm.find({"CAND_ID": eid})}


# TODO: Move this resolution work up to data set to allow resource independence
# TODO: Need better debugging of various degenerate cases
class Committees(MongodbRecordResource):

    Affiliation = collections.namedtuple('Affiliation', 'party score')

    def __init__(self, *args, **kwargs):
        super(Committees, self).__init__(*args, **kwargs)
        self.expect_indexes(self._db.cm, ['_id'])
        self.expect_indexes(self._db.cn, ['_id', 'CAND_NAME'])
        self.expect_indexes(self._db.fec_affiliation_override,
                            ['_id', 'cmte_id'])
        self.committee_cache = {}

    # TODO: limits?
    def get_affiliations(self, committee_ids):
        affiliations = {}
        search_committee_ids = {}

        for committee_id in committee_ids:
            if committee_id in self.committee_cache:
                committee = self.committee_cache[committee_id]
                affiliations[committee_id] = committee
            else:
                search_committee_ids[committee_id] = committee_id

        overrides = self.get_committee_overrides(search_committee_ids)
        search_committees = self.search_committee_affiliations(
            search_committee_ids)
        for committee_id in committee_ids:
            if committee_id in search_committees:
                party = search_committees[committee_id]
            else:
                party = FECParty.NONE.label
            committee = {'party': party}
            if committee_id in overrides:
                committee['override'] = overrides[committee_id]
            self.committee_cache[committee_id] = committee
            affiliations[committee_id] = committee
        return affiliations

    def get_committee_overrides(self, committee_ids):
        override_choices = {
            'd': FECParty.DEMOCRATIC.label,
            'r': FECParty.REPUBLICAN.label
        }
        results = {}
        override_query = {
            "cmte_id": {"$in": [cm_id for cm_id in committee_ids]}}
        overrides = self._db.fec_affiliation_override.find(override_query)

        for override in overrides:
            committee_id = override['cmte_id']
            party = override_choices.get(override['PartyAffiliation'],
                                         FECParty.OTHER.label)
            results[committee_id] = party
        return results

    def search_committee_affiliations(self, committee_ids):
        affiliations = {}
        candidates_committees = collections.defaultdict(list)
        committee_query = {
            "_id": {"$in": [cm_id for cm_id in committee_ids]}}
        committees = self._db.cm.find(committee_query)

        for committee in committees:
            committee_id = committee['_id']

            party = self.get_computed_affiliation(committee)
            affiliations[committee_id] = party
            if ('CMTE_PTY_AFFILIATION' in committee and
                    committee['CMTE_PTY_AFFILIATION']):
                party = committee['CMTE_PTY_AFFILIATION']
                affiliations[committee_id] = party
            elif 'CAND_ID' in committee and committee['CAND_ID']:
                candidate_id = committee['CAND_ID']
                candidates_committees[candidate_id].append(committee_id)

        for committee_id, party in self.get_candidate_committee_affiliations(
                candidates_committees):
            affiliations[committee_id] = party
        return affiliations

    def get_candidate_committee_affiliations(self, candidates_committees):
            candidate_query = {
                "_id": {"$in": list(candidates_committees.keys())}}
            candidates = self._db.cn.find(candidate_query)

            for candidate in candidates:
                for committee_id in candidates_committees[candidate['_id']]:
                    party = candidate['CAND_PTY_AFFILIATION']
                    yield committee_id, party

    def get_computed_affiliation(self, committee):
        if 'aff' in committee:
            computed = committee['aff']
            democratic = self.Affiliation(
                FECParty.DEMOCRATIC, computed['dem_for'] -
                computed['dem_against'])
            republican = self.Affiliation(
                FECParty.REPUBLICAN, computed['rep_for'] -
                computed['rep_against'])
            other = self.Affiliation(
                FECParty.OTHER, computed['oth_for'] -
                computed['oth_against'])
            return max(democratic, republican, other,
                       key=lambda a: a.score).party.label
        else:
            return FECParty.NONE.label


class FECParty(object):

    Party = collections.namedtuple('Party', 'label name')

    DEMOCRATIC = Party('DEM', "Democratic")
    REPUBLICAN = Party('REP', "Republican")
    OTHER = Party('OTHER', "Other")
    NONE = Party('', "None Provided")
