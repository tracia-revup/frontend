
from frontend.apps.analysis.sources.base import (
    MongodbRecordResource, RecordAccessWrapper, ListStrFormatField,
    TransformField)

from frontend.libs.utils.string_utils import str_to_bool


class PropertyRecordWrapper(RecordAccessWrapper):
    id = '_id'
    given_name = 'entity_first_name'
    family_name = 'entity_last_name'
    amount = 'total_assessed_value'
    quality_score = 'QS'
    primary_residence = TransformField('primary_residence', str_to_bool,
                                       recursive=False)
    address = TransformField(key=ListStrFormatField('{property_full_street}',
                                                    '{property_city_name}',
                                                    '{property_state}'),
                             func=lambda v: ', '.join(filter(None, v)))


class Properties(MongodbRecordResource):
    record_wrapper = PropertyRecordWrapper
    label = 'real_estate'
    expected_indexes = ['_id']
    uppercase_names = True
