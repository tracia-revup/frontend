import itertools
import json
import logging

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.views.generic import View, TemplateView
from django.shortcuts import HttpResponse, get_object_or_404
from django.urls import reverse
from django.utils.decorators import method_decorator

from frontend.apps.analysis.models import FeatureResult, RecordSetConfig
from frontend.apps.analysis.sources.base import RecordSet as RecordSetSource
from frontend.apps.analysis.utils import submit_analysis
from frontend.apps.authorize.api.serializers import \
    AnalysisViewUserSettingsSerializer
from frontend.apps.authorize.models import AnalysisViewUserSettings
from frontend.apps.campaign.routes import ROUTES
from frontend.apps.contact_set.api.serializers import UserContactSetSerializer
from frontend.apps.contact_set.models import ContactSet, ContactSetException
from frontend.apps.role.decorators import any_group_required
from frontend.apps.role.permissions import (FundraiserPermissions,
                                            AdminPermissions)
from frontend.libs.django_view_router import RoutingTemplateView
from frontend.libs.utils.string_utils import str_to_bool
from frontend.libs.utils.task_utils import WaffleFlagsMixin, ANALYSIS_FLAGS
from frontend.libs.utils.tracker_utils import action_send
from frontend.libs.utils.general_utils import deep_hash


LOGGER = logging.getLogger(__name__)


class ContactSetAnalysisResultListView(RoutingTemplateView):
    routes = ROUTES
    template_name = 'analysis/ranking.html'

    @method_decorator(login_required)
    @method_decorator(any_group_required(*FundraiserPermissions.all()))
    def do_dispatch(self, request, *args, **kwargs):
        return super(ContactSetAnalysisResultListView, self).do_dispatch(
            request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        user = self.request.user
        self.request.breadcrumbs([('Home', reverse('index')), ('Ranking', '')])
        self.page = self.request.GET.get("page", 1)
        self.partition = self.request.GET.get('partition', "-1").strip()

        contact_set = self._get_contact_set()

        if contact_set:
            # Get the user settings for this contact set's analysis_profile.
            profile = contact_set.analysis.analysis_config.analysis_profile
            try:
                user_settings = AnalysisViewUserSettings.objects.select_related(
                    "analysis_profile__analysis_view_setting_set").get(
                    analysis_profile=profile, user=user)
            except (AnalysisViewUserSettings.DoesNotExist, AttributeError):
                # If the user hasn't saved any settings yet, they won't have one,
                # so we need to return a default.
                user_settings = AnalysisViewUserSettings(
                    analysis_profile=profile, user=user)

            # Convert the objects into serialized json
            user_settings = AnalysisViewUserSettingsSerializer(
                instance=user_settings).data
            contact_set = UserContactSetSerializer(
                instance=contact_set, context={"request": self.request}).data
        else:
            user_settings = None

        return dict(
            contact_set=contact_set,
            user_settings=user_settings,
            account=user.current_seat.account,
            event=user.get_default_event(),
            page=self.page,
        )

    def _get_contact_set(self):
        # If the contact set is provided, try to use it
        cs_id = self.request.GET.get("contact_set")
        cs = None
        if cs_id:
            try:
                cs = ContactSet.objects.standard().select_related(
                    "account").get(id=cs_id)
                # Verify the user has permission to view the given CS
                if not cs.has_view_permission(self.request.user):
                    raise ContactSetException
            except (ContactSet.DoesNotExist, ContactSetException):
                cs = None

        # If no CS was provided, find one to use
        if not cs:
            seat = self.request.user.current_seat
            contact_set_groups = ContactSet.get_seat_contact_sets(
                seat, augment_queryset=ContactSet.objects.order_by("source"),
                return_on_first_match=True)
            try:
                # Grab the first CS from the groups. It will typically be root.
                cs = next(iter(itertools.chain(*contact_set_groups)))
            except StopIteration:
                cs = None
        return cs


class NetworkSearchBase(View, WaffleFlagsMixin):
    flag_names = ANALYSIS_FLAGS

    def submit_analysis(self, request, user, account,
                        force_new_analysis=False):
        flags = self._prepare_flags(request)
        if submit_analysis(user, account, flags=flags,
                           force_new_analysis=force_new_analysis):
            # Record the analysis being run
            action_send(request, user.current_seat or user,
                        verb="ran analysis")


class NetworkSearchView(NetworkSearchBase):
    @method_decorator(login_required)
    def get(self, request):
        user = request.user
        seat = user.current_seat
        account = seat.account
        account_user = account.account_user
        user_analysis, account_analysis = False, False

        user_only = str_to_bool(request.GET.get("user_only"))
        account_only = str_to_bool(request.GET.get("account_only"))
        force_new_analysis = str_to_bool(request.GET.get("force_new_analysis"))

        # If this seat can view rankings, check if they need an analysis
        if seat.has_permissions(any=FundraiserPermissions.all()):
            if not account_only:
                # Queue an analysis for the user
                self.submit_analysis(request, user, account,
                                     force_new_analysis=force_new_analysis)
                user_analysis = True

        # If this seat is an admin, check if account contacts need analysis
        if seat.has_permissions(all=[AdminPermissions.MODIFY_CONTACTS]):
            if not user_only:
                # Queue an analysis for the account
                self.submit_analysis(request, account_user, account,
                                     force_new_analysis=force_new_analysis)
                account_analysis = True

        return HttpResponse(json.dumps({
            "analysis_queued": any((user_analysis, account_analysis)),
            "user_analysis": user_analysis,
            "account_analysis": account_analysis,
            "force_new_analysis": force_new_analysis,
        }))


@method_decorator(login_required, name='dispatch')
class EntityDiffDetailView(TemplateView):
    template_name = 'analysis/entity_diff_detail.html'

    @classmethod
    def _calc_contact_composite_ids(cls, contacts):
        for contact in contacts:
            if contact.contact_type in ['manual', 'merged']:
                continue
            if not contact.contact_id:
                LOGGER.warn("No contact_id for contact %r", contact)
                continue
            data = {
                'contact_id': contact.contact_id,
                'user_id': contact.user_id,
                'contact_type': str(contact.contact_type)
            }
            yield deep_hash(data), contact

    def get_context_data(self, **kwargs):
        rsc = RecordSetConfig.objects.get(
            title="FEC Individual Contributions Configuration")
        rss = RecordSetSource.factory(rsc, None)

        context = super(EntityDiffDetailView,
                        self).get_context_data(**kwargs)
        feature_result = get_object_or_404(FeatureResult, pk=kwargs['pk'])
        contact = feature_result.result.contact
        contact_id_map = dict(
            self._calc_contact_composite_ids(contact.get_contacts()))
        contact_ids = list(contact_id_map.keys())
        context['frontend_contact_ids'] = contact_ids
        db = settings.MONGO_DB
        entity_col = db.entities_for_export
        entities = list(entity_col.find(
            {"contact_ids": {"$elemMatch": {"$in": contact_ids}}}))

        entity_contact_ids = set().union(
            *(entity.get('contact_ids', []) for entity in entities))
        context['entity_contact_ids'] = sorted(entity_contact_ids)
        context['shared_contact_ids'] = sorted(
            entity_contact_ids.intersection(contact_ids))
        context['shared_contacts'] = [
            contact_id_map[c_id] for c_id in context['shared_contact_ids']]
        context['entity_only_contact_ids'] = sorted(
            entity_contact_ids.difference(contact_ids))
        context['entity_only_contacts'] = [
            contact_id_map[c_id] for c_id in context['entity_only_contact_ids']]
        context['frontend_only_contact_ids'] = sorted(
            set(contact_ids).difference(entity_contact_ids))
        context['frontend_only_contacts'] = [
            contact_id_map[c_id] for c_id in context['frontend_only_contact_ids']]

        context['feature_result'] = feature_result
        context['entities'] = entities
        legacy_matches = set().union(
            *(sigset.get('match', [])
              for sigset in feature_result.signals.get('signal_sets', [])))
        entity_matches = set().union(
            *(entity.get('fec_contrib_ids', []) for entity in entities))
        context['legacy_matches'] = legacy_matches
        context['entity_matches'] = entity_matches
        context['shared_matches'] = sorted(
            legacy_matches.intersection(entity_matches))
        context['legacy_only_matches'] = sorted(legacy_matches.difference(
            entity_matches))
        context['entity_only_matches'] = sorted(entity_matches.difference(
            legacy_matches))

        context['shared_contribs'] = list(rss.get(context['shared_matches']))
        context['legacy_only_contribs'] = list(
            rss.get(context['legacy_only_matches']))
        context['entity_only_contribs'] = list(
            rss.get(context['entity_only_matches']))
        return context
