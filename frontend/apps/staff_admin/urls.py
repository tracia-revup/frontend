from django.conf.urls import url, include
from django.contrib import admin

from frontend.apps.authorize.views import LoginView
from .views import (ClientAdminView, StaffAdminHomeView, AdminSalesView,
                    AdminClientActivationView)


urlpatterns = [
    url(r'^$', StaffAdminHomeView.as_view(), name='staff_admin_home'),
    url(r'^client/$', ClientAdminView.as_view(),
        name='staff_admin_create_client'),
    url(r'^client/add_account/$', AdminSalesView.as_view(),
        name='staff_admin_add_account_to_token'),
    url(r'^client/client_activation/$', AdminClientActivationView.as_view(),
        name='staff_admin_client_activation'),
    url(r'^admin/login/', LoginView.as_view(), name='staff_login'),
    url(r'^admin/', include((admin.site.urls[0], 'admin'))),
    # url(r'^list_analysis/', ListAnalysisView.as_view(),
    #     name='staff_admin_list_analysis'),
    # url(r'^run_analysis/$', RunAnalysisView.as_view(),
    #     name='staff_admin_run_analysis_for_account'),
]
