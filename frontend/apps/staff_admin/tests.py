from django.conf import settings
from django.test import Client
from django.urls import reverse
import unittest.mock as mock

from frontend.apps.authorize.factories import GroupFactory, RevUpUserFactory
from frontend.apps.authorize.models import RevUpUser
from frontend.apps.authorize.test_utils import AuthorizedUserTestCase
from frontend.apps.campaign.factories import (
    ProductFactory, AccountProfileFactory, AccountFactory)
from frontend.apps.campaign.models import (AccountProfile,
                                           AccountSkinSettingsLink)
from frontend.apps.campaign.models.base import ACCOUNT_KEY_LENGTH
from frontend.apps.seat.models import Seat
from frontend.apps.ux.factories import SkinSettingsFactory
from frontend.libs.utils.string_utils import random_email


class AdminSalesViewTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        super(AdminSalesViewTests, self).setUp(
            login_now=False, groups=[], **kwargs)
        self.url = reverse("staff_admin_add_account_to_token")
        GroupFactory(name=settings.ACCOUNT_ADMIN_GROUP_NAME)
        GroupFactory(name=settings.FUNDRAISER_GROUP_NAME)

        # Instantiate a client and account profiles
        self.client = Client()
        self.product = ProductFactory(product_type="G",
                                      manual_onboarding=True)
        self.profile = AccountProfileFactory(product=self.product,
                                             title="Political Candidate",
                                             account_type="P")
        self.profile2 = AccountProfileFactory(product=self.product,
                                              title="Political Committee",
                                              account_type="P")
        self.profiles = [self.profile, self.profile2]

    @mock.patch("frontend.apps.campaign.models.base.list_subscription_plans")
    def test_get_context_data(self, m1):
        m1.return_value = [dict(
            plan_code=self.product.recurly_plan_code,
            unit_amount_in_cents=39500)]

        # Login the user
        self._login(self.user)

        # Make the user a staff member
        self.user.is_staff = True
        self.user.save()

        response = self.client.get(self.url)
        assert response.status_code == 200
        assert response.context['account_profile_options']

        account_profiles = AccountProfile.objects.filter(
            product__product_type='G', product__manual_onboarding=True).\
            order_by("title", "product__default_analyze_limit")
        ap_options = [(ap.id, f'{ap.name} - ${ap.product.get_price:.2f}')
                      for ap in account_profiles]
        context_ap_options = response.context['account_profile_options']
        assert context_ap_options == ap_options

    @mock.patch("frontend.apps.staff_admin.views.send_template_email")
    @mock.patch("frontend.apps.staff_admin.views.ExternalSignupToken."
                "objects.create")
    def test_process_sales(self, m1, m2):
        # Login the user
        self._login(self.user)

        # Make the user a staff member
        self.user.is_staff = True
        self.user.save()

        m1.return_value = mock.MagicMock(token="test_token_123",
                                         organization="AA",
                                         profile=self.profile,
                                         product=self.product)
        data = dict(
            first_name="First",
            last_name="Last",
            phone="555-555-4321",
            email="test@revup.com",
            organization_name='AA',
            account_profile=self.profile.id
        )

        response = self.client.post(self.url, data)
        assert response.status_code == 302

        # second email
        assert m2.call_args_list[0][0][0] == \
            f'{self.product.title} Customer Onboarding - AA'
        assert len(m2.call_args_list[0][0][3]) == 3
        assert 'clients@revup.com' in m2.call_args_list[0][0][3]

        m1.assert_called_with(
            first_name="First",
            last_name="Last",
            phone="555-555-4321",
            email='test@revup.com',
            organization='AA',
            product=self.product,
            profile=self.profile,
            referral_partner='',
            skip_payment=False
        )

    @mock.patch("frontend.apps.staff_admin.views.send_template_email")
    @mock.patch("frontend.apps.staff_admin.views.ExternalSignupToken."
                "objects.create")
    def test_process_sales_2(self, m1, m2):
        # Login the user
        self._login(self.user)

        # Make the user a staff member
        self.user.is_staff = True
        self.user.save()

        m1.return_value = mock.MagicMock(token="test_token_123",
                                         organization="AA",
                                         profile=self.profile,
                                         product=self.product)
        data = dict(
            first_name="First",
            last_name="Last",
            phone="555-555-4321",
            email="test@revup.com",
            organization_name='AA',
            account_profile=self.profile.id,
            referral_partner='test_partner',
            skip_payment=True
        )

        response = self.client.post(self.url, data)
        assert response.status_code == 302

        # second email
        assert m2.call_args_list[0][0][0] == \
            f'{self.product.title} Customer Onboarding - AA'
        assert len(m2.call_args_list[0][0][3]) == 3
        assert 'clients@revup.com' in m2.call_args_list[0][0][3]

        m1.assert_called_with(
            first_name="First",
            last_name="Last",
            phone="555-555-4321",
            email='test@revup.com',
            organization='AA',
            product=self.product, profile=self.profile,
            referral_partner='test_partner',
            skip_payment=True
        )


class ClientAdminViewTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        super(ClientAdminViewTests, self).setUp(
            login_now=False, groups=[], **kwargs)
        self.url = reverse("staff_admin_create_client")
        GroupFactory(name=settings.ACCOUNT_ADMIN_GROUP_NAME)
        GroupFactory(name=settings.FUNDRAISER_GROUP_NAME)

    @mock.patch('frontend.apps.staff_admin.views.list_subscription_plans',
                mock.MagicMock())
    def test_permissions(self):
        # Verify logged-out user is redirected to login
        result = self.client.get(self.url)
        self.assertRedirects(result, "{}?next={}".format(
            reverse('account_login'), self.url))

        # Login the user
        self._login(self.user)

        # Verify non staff admin user is redirected away
        result = self.client.get(self.url, follow=True)
        self.assertRedirects(result, reverse('contact_manager')+"?onboarding")

        # Make the user a staff member
        self.user.is_staff = True
        self.user.save()

        # Verify the form page is returned
        result = self.client.get(self.url)
        self.assertContains(result, "Client&#39;s email")
        self.assertContains(result, "Create New Client")
        self.assertContains(result, "Organization name")
        self.assertContains(result, "Organization type")

    @mock.patch('frontend.apps.staff_admin.views.list_subscription_plans',
                mock.MagicMock())
    def test_post(self):
        # Login the user
        self._login(self.user)

        # Add the Campaign Admin group to the user
        self.user.is_staff = True
        self.user.save()
        ap = AccountProfile.objects.first()
        sk = SkinSettingsFactory()

        data = {"email": '',
                "organization_name": "name",
                "account_profile": ap.id,
                "start_date": "12/04/2014",
                "seat_count": 10,
                "payment_type": "recurly",
                "activate_now": False}

        def cr(**kwargs):
            new_d = data.copy()
            new_d.update(kwargs)
            return new_d

        def verify_account(account, email, organization_name,
                           account_profile, start_date, seat_count,
                           activate_now, skin=sk, **kwargs):
            self.assertEqual(account.account_head.email, email)
            self.assertEqual(account.organization_name, organization_name)
            self.assertEqual(account.start_date.strftime("%m/%d/%Y"),
                             start_date)
            self.assertEqual(account.account_profile_id, account_profile)
            self.assertEqual(account.seat_count, seat_count)
            self.assertTrue(account.active == activate_now)
            self.assertEqual(account.current_skin, skin)
            assert account.account_key is not None
            assert len(account.account_key) == ACCOUNT_KEY_LENGTH

            for ac in account.analysis_configs.all():
                self.assertEqual(ac.account, account)
                self.assertEqual(ac.title, account.organization_name)

        with mock.patch('frontend.apps.staff_admin.views.'
                        'send_admin_added_notification') as m:
            result = self.client.post(self.url, data=data)
            self.assertContains(result, 'This field is required')
            result = self.client.post(self.url, data=cr(email='sdfsdf'))
            self.assertContains(result, 'Enter a valid email address')

            email = random_email()
            ## Verify a campaign admin is created for a new user
            cdata = cr(email=email)
            result = self.client.post(self.url, data=cdata, follow=True)
            user = RevUpUser.objects.get(email=email)

            # Verify a new account was created with the correct values
            account = user.seats.first().account
            # Since we haven't defined a default skin yet, the current skin
            # should be None
            verify_account(account, skin=None, **cdata)
            # Verify a permanent seat was added for this user
            s = Seat.objects.get(user=user, account=account)
            self.assertEqual(s.state, Seat.States.ACTIVE)
            self.assertEqual(s.seat_type, Seat.Types.PERMANENT)
            self.assertEqual(s.permissions, Seat.FundraiserPermissions.all() +
                                            Seat.AdminPermissions.all())
            self.assertContains(result,
                                "Created new campaign admin: {}".format(email))
            m.assert_called_with(user, account, "http://testserver",
                                 inviting_user=self.user)

            # Add the Skin to the account profile as the default
            AccountSkinSettingsLink.objects.create(
                account_profile=ap, skin=sk, is_default=True)

            ## Verify an existing user without admin status receives it.
            m.delay.reset_mock()
            for seat in user.seats.all():
                seat.permissions = []
                seat.save()
            self.assertFalse(user.is_account_admin())
            cdata = cr(email=email)
            # This verifies the page works even without an active seat
            self.user.seats.all().delete()
            result = self.client.post(self.url, data=cdata, follow=True)

            # Verify another new account was created with the correct values
            account2 = user.seats.last().account
            self.assertNotEqual(account, account2)
            verify_account(account2, **cdata)

            self.assertContains(
                result, "Gave existing user admin status: {}".format(email))
            m.assert_called_with(user, account2, "http://testserver",
                                 inviting_user=self.user)

            ## Verify form issues a warning for existing accounts
            m.reset_mock()
            result = self.client.post(self.url, data=cr(email=email),
                                      follow=True)
            self.assertContains(
                result, "The user &#39;{}&#39; is an admin "
                        "for multiple accounts".format(email))
            self.assertTrue(m.called)

            ## Verify an account can be created with an empty seat count
            email = random_email()
            cdata = cr(email=email, seat_count="")
            result = self.client.post(self.url, follow=True, data=cdata)
            user = RevUpUser.objects.get(email=email)
            account = user.seats.first().account
            # Verify empty seat count becomes None
            cdata['seat_count'] = None
            verify_account(account, **cdata)

            ## Verify activate_now
            # Verify it works for recurly
            email = random_email()
            cdata = cr(email=email, activate_now=True, payment_type="recurly")
            result = self.client.post(self.url, follow=True, data=cdata)
            user = RevUpUser.objects.get(email=email)
            account = user.seats.first().account
            # Verify account is not activated
            cdata['activate_now'] = True
            verify_account(account, **cdata)

            # Verify it does work for purchase order
            email = random_email()
            cdata = cr(email=email, activate_now=True, payment_type="po")
            result = self.client.post(self.url, follow=True, data=cdata)
            user = RevUpUser.objects.get(email=email)
            account = user.seats.first().account
            # Verify account is not activated
            verify_account(account, **cdata)


class ImpersonatorViewTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        super(ImpersonatorViewTests, self).setUp()
        self.user2 = RevUpUserFactory()
        self.list = reverse("impersonate-list")
        self.search = reverse("impersonate-search")
        self.start = reverse("impersonate-start", args=(self.user2.id,))
        self.stop = reverse("impersonate-stop")

    def test_permissions(self):
        ## Verify non-staff user cannot access impersonator
        self.assertFalse(self.user.is_admin)
        response = self.client.get(self.list)
        self.assertRedirects(response, "/", fetch_redirect_response=False)
        response = self.client.get(self.search)
        self.assertRedirects(response, "/", fetch_redirect_response=False)
        response = self.client.get(self.start)
        self.assertRedirects(response, "/", fetch_redirect_response=False)

        ## Verify staff user can access impersonator
        self.user.is_staff = True
        self.user.save()
        response = self.client.get(self.list)
        self.assertContains(response, "")
        response = self.client.get(self.search)
        self.assertContains(response, "")
        response = self.client.get(self.start)
        # Redirects to impersonated user
        self.assertRedirects(response, "/", fetch_redirect_response=False)

    def test_tracker(self):
        # Verfiy impersonation is tracked
        self.user.is_staff = True
        self.user.save()
        pre_count = self.user.actor_actions.filter(
            verb="impersonated user").count()
        response = self.client.get(self.start)
        self.assertEqual(pre_count + 1, self.user.actor_actions.filter(
            verb="impersonated user").count()
        )


class AdminClientActivationViewTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        super(AdminClientActivationViewTests, self).setUp(
            login_now=False, groups=[], **kwargs)
        self.url = reverse("staff_admin_client_activation")

        # Instantiate a client and account profiles
        self.client = Client()
        self.account1 = AccountFactory(onboarding=True)
        self.account2 = AccountFactory(onboarding=True)
        self.account3 = AccountFactory(onboarding=False)

    def test_permissions(self):
        # Verify logged-out user is redirected to login
        result = self.client.get(self.url)
        assert result.status_code == 302

        # Login the user
        self._login(self.user)

        # Verify non staff admin user is redirected away
        result = self.client.get(self.url)
        assert result.status_code == 302

        # Make the user a staff member
        self.user.is_staff = True
        self.user.save()
        # Verify the form page is returned
        result = self.client.get(self.url)
        assert result.status_code == 200

    def test_list(self):
        self._login()
        self.user.is_staff = True
        self.user.save()

        # Verify the correct accounts in displayed in the list
        result = self.client.get(self.url)
        content = str(result.content)
        assert self.account1.title in content
        assert self.account2.title in content
        assert self.account3.title not in content
        assert self.account3.title not in content

    def test_post(self):
        self._login()
        self.user.is_staff = True
        self.user.save()

        # Make sure there is no recurly account
        assert self.account1.recurly_account_code == ""
        response = self.client.post(self.url,
                                    data=dict(activate=[self.account1.id]),
                                    follow=True)
        assert "a subscription was not created" in str(response.content)

        for account, onboarding in ((self.account1, False),
                                    (self.account2, True),
                                    (self.account3, False)):
            account.refresh_from_db()
            assert account.onboarding == onboarding
