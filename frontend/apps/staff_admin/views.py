import datetime
import logging

from actstream import action
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import FormView
from django.utils.decorators import method_decorator
from impersonate.signals import session_begin, session_end

from frontend.apps.authorize.models import RevUpUser, ExternalSignupToken
from frontend.apps.authorize.utils import send_admin_added_notification
from frontend.apps.campaign.models import AccountProfile, Account, Product
from frontend.apps.campaign.routes import ROUTES
from frontend.apps.contact.utils import PhoneNumberMixin
from frontend.libs.django_view_router import RoutingTemplateView
from frontend.libs.utils.email_utils import send_template_email
from frontend.libs.utils.recurly_utils import list_subscription_plans
from frontend.libs.utils.view_utils import user_passes_test
from frontend.libs.utils.zendesk_utils import (
    get_zendesk_organization, create_or_update_organization,
    create_or_update_user)
from .forms import ClientForm, AdminSalesForm, AdminClientActivationForm


LOGGER = logging.getLogger(__name__)


class StaffAdminHomeView(RoutingTemplateView):
    routes = ROUTES
    template_name = "staff_admin/home.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(lambda u: u.is_admin))
    def dispatch(self, request, *args, **kwargs):
        return super(StaffAdminHomeView, self).dispatch(
            request, *args, **kwargs)


class AdminClientActivationView(FormView):
    """Activate the client account and start billing. """
    template_name = "staff_admin/activate_client.html"
    success_url = "#"
    form_class = AdminClientActivationForm

    @method_decorator(login_required)
    @method_decorator(user_passes_test(lambda u: u.is_admin))
    def dispatch(self, request, *args, **kwargs):
        return super(AdminClientActivationView, self).dispatch(request, *args, **kwargs)

    def get_form(self, form_class=None):
        """Returns an instance of the form to be used in this view."""
        if form_class is None:
            form_class = self.form_class
        return form_class(**self.get_form_kwargs())

    def get_context_data(self, **kwargs):
        """Get all the Accounts that are not active."""
        context = super(AdminClientActivationView, self).get_context_data(**kwargs)
        # Get all accounts that need to be activated
        context['accounts'] = Account.objects.filter(active=True,
                                                     onboarding=True)
        return context

    def post(self, request, *args, **kwargs):
        account_ids = [int(account_id)
                       for account_id in request.POST.getlist('activate')]
        self._process_activation(account_ids)

        msg = f"Client activation complete and Billing start date " \
              f"set for selected accounts. "
        messages.success(self.request, msg)

        return HttpResponseRedirect(reverse('staff_admin_client_activation'))

    def _process_activation(self, account_ids):
        for account in Account.objects.filter(id__in=account_ids):
            with transaction.atomic():
                # Lock the user to prevent multiple payments by accident
                account.account_user.lock()
                # activate the account
                account.onboarding = False
                # Set the account start date
                account.start_date = datetime.datetime.today()
                account.save()

                # Accounts can be onboarded without setting up payment, so
                # we need to allow for that
                if account.recurly_account_code:
                    # Create the recurly subscription
                    account.get_or_create_recurly_subscription(prorate=True)
                else:
                    msg = f"{account.title} was not configured with a " \
                          "recurly account, so a subscription was not created."
                    messages.warning(self.request, msg)


class AdminSalesView(FormView):
    """Add the Account info into the token."""
    template_name = "staff_admin/admin_sales.html"
    success_url = "#"
    form_class = AdminSalesForm

    @method_decorator(login_required)
    @method_decorator(user_passes_test(lambda u: u.is_admin))
    def dispatch(self, request, *args, **kwargs):
        self.profiles = AccountProfile.objects.filter(
                            product__product_type=Product.ProductType.GLOBAL,
                            product__manual_onboarding=True) \
                .order_by("title", "product__default_analyze_limit")
        return super(AdminSalesView, self).dispatch(request, *args, **kwargs)

    def get_form(self, form_class=None):
        """Returns an instance of the form to be used in this view."""
        if form_class is None:
            form_class = self.form_class
        return form_class(self.profiles, **self.get_form_kwargs())

    def get_context_data(self, **kwargs):
        context = super(AdminSalesView, self).get_context_data(**kwargs)

        # Build the list for account profile: product
        ap_options = [(ap.id, f'{ap.name} - ${ap.product.get_price:.2f}')
                      for ap in self.profiles if ap.product.get_price is not None]

        context['account_profile_options'] = ap_options
        return context

    def form_valid(self, form):
        email = form.cleaned_data['email']

        # Check to see if there is an existing user with this email
        user_exists = RevUpUser.objects.filter(email=email).exists()

        # If zendesk is enabled create the organization and user in zendesk.
        if settings.ZENDESK_ENABLED:
            org_data = dict(
                organization_name=form.cleaned_data['organization_name'],
                referral_party=form.cleaned_data['referral_partner'],
                revup_sales_person=self.request.user.name,
                product_type=form.cleaned_data['account_profile'].name
            )
            organization_name = org_data.get('organization_name')
            organization_id = get_zendesk_organization(organization_name)
            if organization_id:
                org_data['id'] = organization_id
                messages.warning(self.request, f"An Organization with name "
                                               f"'{organization_name}' "
                                               f"already exists. All the "
                                               f"information has been stored "
                                               f"in Zendesk")

            organization_id = create_or_update_organization(org_data)

            # Handle the case/alert the user if the organization was not
            # created or updated in zendesk
            if not organization_id:
                messages.warning(self.request, "We encountered an error when "
                                               "trying to create the "
                                               "organization in the zendesk "
                                               "API")

            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            user_data = dict(
                full_name=f'{first_name} {last_name}',
                email=form.cleaned_data['email'],
                phone=form.cleaned_data['phone'],
                organization_name=organization_name
            )
            user_id = create_or_update_user(user_data)

            # Handle the case/alert the user if the user was not created or
            # updated in zendesk
            if not user_id:
                messages.warning(self.request, "We encountered an error when "
                                               "trying to create the "
                                               "user in the zendesk API")

        # If there is already a user with this email alert the staff to let
        # them know
        if user_exists:
            messages.warning(self.request, f" A user with email '{email}' "
                                           f"already exists.")
        msg = f"Sent an onboarding email to '{email}'."

        messages.success(self.request, msg)
        self._process_sales(form)
        return HttpResponseRedirect(reverse("staff_admin_add_account_to_token"))

    def _process_sales(self, form):
        """Add the account info into the token."""
        first_name = form.cleaned_data['first_name']
        last_name = form.cleaned_data['last_name']
        phone = form.cleaned_data['phone']
        email = form.cleaned_data["email"]
        title = form.cleaned_data["organization_name"]
        account_profile = form.cleaned_data["account_profile"]
        referral_partner = form.cleaned_data["referral_partner"]
        skip_payment = form.cleaned_data["skip_payment"]
        product = account_profile.product
        onboarded_by = self.request.user

        # Create an External Signup token
        token = ExternalSignupToken.objects.create(
            first_name=first_name,
            last_name=last_name,
            phone=phone,
            email=email,
            organization=title,
            profile=account_profile,
            skip_payment=skip_payment,
            product=product,
            referral_partner=referral_partner,
        )

        # Send Onboard Welcome email with token
        token.send_token_signup_email(self.request)

        # Send an email to Customer Success with information about the client
        subject = f"{product.title} Customer Onboarding - " \
                  f"{token.organization}"
        email_template = 'core/emails/onboard_new_client_email.html'

        # Beautify the phone number
        phone_formatter = PhoneNumberMixin()
        phone_formatter.number = phone
        phone = phone_formatter.national

        context = dict(first_name=first_name, last_name=last_name, phone=phone,
                       email=email, title=title, product=product,
                       pay_in_flow="Yes" if not skip_payment else "No",
                       account_profile=account_profile,
                       onboarded_by=onboarded_by,
                       referral_partner=referral_partner)
        email_to = ['clients@revup.com', onboarded_by.email, 'dale@revup.com']
        send_template_email(subject, email_template, context,
                            email_to)


class ClientAdminView(FormView):
    """Give a new client Campaign Admin status.

    If the client doesn't exist yet, create a new user and send them through
    the registration process.
    """
    template_name = "staff_admin/create_client.html"
    success_url = "#"
    form_class = ClientForm

    @method_decorator(login_required)
    @method_decorator(user_passes_test(lambda u: u.is_admin))
    def dispatch(self, request, *args, **kwargs):
        self.profiles = AccountProfile.objects.all()
        return super(ClientAdminView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ClientAdminView, self).get_context_data(**kwargs)
        # Query Recurly for a list of subscription plans
        try:
            plans = list_subscription_plans()
        except Exception:
            # If recurly fails, we should still be able to load the page
            plans = []

        # Combine AccountProfiles with their recurly plan details
        plans = {plan['plan_code']: plan for plan in plans}
        profile_plans = {}
        for profile in self.profiles:
            plan = plans.get(profile.product.recurly_plan_code)
            profile_plans[profile.id] = dict(
                name=plan['name'],
                setupCost=plan['setup_fee_in_cents']/100.0,
                seatCost=plan['unit_amount_in_cents']/100.0,
                # TODO: add default seat count here
            ) if plan else None

        context['profile_plans'] = profile_plans

        # Build the list for account profile: product
        ap_options = [(ap.id, f'{ap.name}: {ap.product.name}')
                      for ap in self.profiles]
        context['account_profile_options'] = ap_options
        return context

    def get_form(self, form_class=None):
        """
        Returns an instance of the form to be used in this view.
        """
        if form_class is None:
            form_class = self.form_class
        return form_class(self.profiles, **self.get_form_kwargs())

    def _create_account(self, user, data):
        account_profile = data['account_profile']
        seat_count = data["seat_count"]
        organization_name = data['organization_name']
        start_date = data.get("start_date")

        kwargs = {}
        if data["activate_now"]:
            kwargs['active'] = True
        account = Account.create(
            account_profile, organization_name, user, seat_count,
            start_date=start_date, **kwargs)
        return account

    def form_valid(self, form):
        email = form.cleaned_data['email']

        # Use a transaction to roll back any created objects if
        # there's a failure.
        with transaction.atomic():
            # Get an existing user, or create a new one.
            user, created = RevUpUser.get_or_create_inactive(email)

            # If the user is already an admin, we error to let the staff know
            if user.is_account_admin():
                messages.warning(self.request, "The user '{}' is an admin "
                                 "for multiple accounts".format(email))
                # return super(ClientAdminView, self).form_valid(form)

            # Create the account/campaign
            account = self._create_account(user, form.cleaned_data)

            if created:
                msg = "Created new campaign admin: {}"
            else:
                msg = "Gave existing user admin status: {}"

            # If the user hasn't activated yet, we'll need to generate a token
            if not user.is_active:
                user.generate_token()

            # Send the notification
            # Build the protocol and host string here because celery wont have
            # access to the request object
            protocol_host = "http{}://{}".format(
                's' if self.request.is_secure() else '',
                self.request.get_host())
            send_admin_added_notification(user, account, protocol_host,
                                          inviting_user=self.request.user)

        messages.success(self.request, msg.format(email))
        return super(ClientAdminView, self).form_valid(form)


# TODO: These are super outdated. It would be nice to bring them back eventually
# class RunAnalysisView(FormView):
#     template_name = "staff_admin/run_analysis.html"
#     success_url = "/staff_admin/list_analysis/"
#     form_class = RunAnalysisForm
#
#     @method_decorator(login_required)
#     @method_decorator(user_passes_test(lambda u: u.is_admin))
#     def dispatch(self, request, *args, **kwargs):
#         return super(RunAnalysisView, self).dispatch(
#             request, *args, **kwargs)
#
#     def form_valid(self, form):
#         account = form.cleaned_data['account'].get_subclass()
#         analysis_config = form.cleaned_data['analysis_config']
#
#         NETWORK_SEARCH = RevUpTask.TaskType.NETWORK_SEARCH
#         task_id = perform_analysis_task.apply_async(
#             args=(analysis_config.id, self.request.user.id, account.id),
#             kwargs=dict(update_progress=update_progress),
#             queue='analysis_large')
#         RevUpTask.objects.create(user=self.request.user, task_id=str(task_id),
#                                  task_type=NETWORK_SEARCH)
#
#         return super(RunAnalysisView, self).form_valid(form)
#
#
# class ListAnalysisView(SortableListView):
#     model = Analysis
#     template_name = 'staff_admin/list_analysis.html'
#     paginate_by = 50
#     default_sort_field = 'modified'
#
#     allowed_sort_fields = {'account': {'default_direction': '',
#                                        'verbose_name': 'Account'},
#                            'analysis_config': {'default_direction': '',
#                                                'verbose_name': 'Analysis Config'},
#                            'modified': {'default_direction': '',
#                                          'verbose_name': 'Created'},
#                           }
#
#     def get_queryset(self):
#         qs = super(ListAnalysisView, self).get_queryset()
#         return qs.filter(user=self.request.user)


def start_impersonate_handler(impersonator, impersonating, **kwargs):
    # Log the impersonation
    action.send(impersonator, verb="impersonated user",
                action_object=impersonating, public=False)
session_begin.connect(start_impersonate_handler)


def stop_impersonate_handler(impersonator, request, **kwargs):
    # Log the impersonation end
    action.send(impersonator, verb="stopped impersonating user",
                action_object=request.user, public=False)
session_end.connect(stop_impersonate_handler)
