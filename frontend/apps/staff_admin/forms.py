from django import forms

from frontend.apps.campaign.models import AccountProfile, Account, Product
from frontend.apps.analysis.models import AnalysisConfig
from frontend.libs.utils.form_utils import USPhoneNumberField

PAYMENT_TYPES = {
    "recurly": "Recurly Credit Card",
    "po": "Purchase Order"
}


class AdminClientActivationForm(forms.Form):
    activate = forms.BooleanField(label="Activate account for:")

    def __init__(self, **kwargs):
        super(AdminClientActivationForm, self).__init__(**kwargs)


class AdminSalesForm(forms.Form):
    first_name = forms.CharField(max_length=64, label="First Name")
    last_name = forms.CharField(max_length=150, label="Last Name")
    phone = USPhoneNumberField(max_length=32, label="Phone Number")
    email = forms.EmailField(label="Email")
    organization_name = forms.CharField(max_length=512, label="Organization Name")
    referral_partner = forms.CharField(max_length=512,
                                       label="Referral Partner",
                                       required=False)
    skip_payment = forms.BooleanField(label="Activate Without Payment",
                                      required=False)
    account_profile = forms.ModelChoiceField(
        queryset=AccountProfile.objects.filter(
            product__product_type=Product.ProductType.GLOBAL),
        label="Product")

    def __init__(self, profile_qs, **kwargs):
        super(AdminSalesForm, self).__init__(**kwargs)
        self.fields['account_profile'].queryset = profile_qs


class ClientForm(forms.Form):
    email = forms.EmailField(label="Client's email")
    organization_name = forms.CharField(max_length=512)
    account_profile = forms.ModelChoiceField(
        queryset=AccountProfile.objects.none(),
        label="Account Profile")
    preferred_domain = forms.CharField(required=False)
    start_date = forms.DateField(required=False)
    seat_count = forms.IntegerField(required=False)
    activate_now = forms.BooleanField(
        required=False,
        help_text="Mark to activate this account immediately.")

    def __init__(self, profile_qs, **kwargs):
        super(ClientForm, self).__init__(**kwargs)
        self.fields['account_profile'].queryset = profile_qs


class RunAnalysisForm(forms.Form):
    account = forms.ModelChoiceField(
        queryset=Account.objects.all(),
        label="Account",
        widget=forms.Select(attrs={'onchange':'UpdateAnalysisConfigOptions(this);'}))

    analysis_config = forms.ModelChoiceField(
        queryset=AnalysisConfig.objects.all(),
        label="Analysis Config",
        widget=forms.Select(attrs={'disabled':'true'}))

    def __init__(self, *args, **kwargs):
        super(RunAnalysisForm, self).__init__(*args, **kwargs)
        self.fields['analysis_config'].choices = (("", "Select account"),)

    def clean(self):
        cleaned_data = super(RunAnalysisForm, self).clean()
        account = cleaned_data.get('account')
        analysis_config = cleaned_data.get('analysis_config')
        if not (account or analysis_config):
            raise forms.ValidationError(
                "Account or AnalysisConfig field unset")
        if analysis_config.account.id != account.id:
            raise forms.ValidationError(
                "AnalysisConfig does not belong to that account")

        return cleaned_data

