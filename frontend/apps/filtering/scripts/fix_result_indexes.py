
from frontend.apps.analysis.models import Analysis
from frontend.apps.filtering.models import ResultIndex


def script():
    for analysis_id in ResultIndex.objects.values_list("result__analysis", flat=True).distinct():
        analysis = Analysis.objects.get(id=analysis_id)
        # Update the indexes to point at an analysis
        print("Update indexes for analysis: ", analysis)
        ResultIndex.objects.filter(result__analysis=analysis).update(
            analysis=analysis)

        # Update the indexes to have partitions
        for partition in analysis.partition_set.partition_configs.all():
            ResultIndex.objects.filter(
                result__analysis=analysis, result__partition=partition).update(
                partition=partition)
