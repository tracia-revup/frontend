
from django.contrib import admin

from .models import Filter, QuickFilter


@admin.register(Filter)
class FilterAdmin(admin.ModelAdmin):
    list_display = ("title", "filter_type")


@admin.register(QuickFilter)
class QuickFilterAdmin(admin.ModelAdmin):
    list_display = ("title", "query", "order")
