
from unittest.mock import patch

from django.db import IntegrityError
from frontend.apps.contact.factories import ContactFactory
from frontend.apps.contact.models import ImportConfig
from frontend.apps.filtering.models import ContactIndex, Filter
from frontend.libs.utils.test_utils import TestCase


@patch("frontend.apps.contact.models.Contact.get_preferred_gender", return_value=None)
class ContactIndexTests(TestCase):
    def setUp(self):
        # Create a simple primary contact
        self.contact1 = ContactFactory()
        self.user = self.contact1.user

        # Sometimes the filters are not created in the test. Not sure why
        from frontend.apps.filtering.migrations._utils import generate_filters
        generate_filters(ImportConfig, Filter)

        self.filters = Filter.objects.filter(filter_type='CN')
        self.assertEqual(len(self.filters), 15)

        # Create a merge group
        self.contact2 = ContactFactory(user=self.user, contact_type="merged",
                                       phone_numbers=[], email_addresses=[],
                                       addresses=[])
        self.child_contacts = [
            ContactFactory(user=self.user, parent=self.contact2)
            for i in range(5)]
        self.child_contacts[0].contact_type = "linkedin-oauth2"
        self.child_contacts[0].save()

    def test_single_contact_index(self, *_):
        kwargs = dict(user=self.user, contact=self.contact1)
        index = ContactIndex.update_index(self.filters, **kwargs)
        self.assertIsNotNone(index.id)
        expected = {'given_name': self.contact1.first_name_lower,
                    'family_name': self.contact1.last_name_lower,
                    'import_source': ['google-oauth2'],
                    'gender': None,
                    'area_code': [
                        self.contact1.phone_numbers.first().rfc3966[7:10]]}
        self.assertEqual(index.attribs, expected)

        # Verify we retrieve the same index when calling on the same contact
        index_id = index.id
        index = ContactIndex.update_index(self.filters, **kwargs)
        self.assertEqual(index.id, index_id)

        # Verify we can create indexes without saving
        index.delete()
        index = ContactIndex.update_index(self.filters, save=False, **kwargs)
        self.assertIsNone(index.id)

        # Verify attempting to create the same index errs
        index = ContactIndex.update_index(self.filters, **kwargs)
        with self.assertRaises(IntegrityError):
            ContactIndex.update_index(self.filters, get_existing=False,
                                      **kwargs)

    def test_merged_contact_index(self, *_):
        kwargs = dict(user=self.user, contact=self.contact2)

        index = ContactIndex.update_index(self.filters, **kwargs)
        expected = {'given_name': self.contact2.first_name_lower,
                    'family_name': self.contact2.last_name_lower,
                    'gender': None}
        self.assertCountEqual(index.attribs.pop('import_source'),
                              ['google-oauth2', "linkedin-oauth2"])
        self.assertCountEqual(
            index.attribs.pop('area_code'),
            {p.rfc3966[7:10] for p in self.contact2.all_phones()})
        self.assertEqual(index.attribs, expected)

    def test_bulk_update_index(self, *_):
        indexes = list(ContactIndex.bulk_update_index(
            [self.contact1, self.contact2], self.filters, save=False))
        self.assertEqual(indexes[0].attribs,
                         {'given_name': self.contact1.first_name_lower,
                          'family_name': self.contact1.last_name_lower,
                          'import_source': ['google-oauth2'],
                          'gender': None,
                          'area_code': [
                              self.contact1.phone_numbers.first().rfc3966[
                              7:10]]}
                         )

        self.assertCountEqual(indexes[1].attribs.pop('import_source'),
                              ['google-oauth2', "linkedin-oauth2"])
        self.assertCountEqual(
            indexes[1].attribs.pop('area_code'),
            {p.rfc3966[7:10] for p in self.contact2.all_phones()})
        self.assertEqual(indexes[1].attribs,
                         {'given_name': self.contact2.first_name_lower,
                          'family_name': self.contact2.last_name_lower,
                          'gender': None})
