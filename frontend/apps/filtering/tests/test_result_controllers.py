
from pytest import fixture, mark
from pytest_describe import behaves_like
from unittest.mock import Mock

from frontend.apps.analysis.factories import FeatureConfigFactory, AnalysisConfigFactory
from frontend.apps.filtering.filter_controllers import results
from frontend.apps.personas.factories import PersonaInstanceFactory
from frontend.libs.utils.test_utils import expect


def result_tag_filter_controller():
    @fixture(scope='module')
    def tag_name(controller):
        return controller.index_arg

    @fixture(scope='module')
    def index_key(controller):
        return controller.index_arg

    def describe_extract():
        def is_true_when_tag_is_present(controller, tag_name, index_key,
                                        mocker):
            mock_result = mocker.Mock(tags=[tag_name])
            output = controller.extract(mock_result, [])
            expect(output) == {index_key: True}

        def is_false_when_tag_is_present(controller, tag_name, index_key,
                                         mocker):
            mock_result = mocker.Mock(tags=[])
            output = controller.extract(mock_result, [])
            expect(output) == {index_key: False}

    def describe_filter():
        def handles_bool_true(controller, index_key):
            output = controller.filter(True)
            expect(output) == {index_key: True}

        def handles_bool_false(controller, index_key):
            output = controller.filter(False)
            expect(output) == {index_key: False}

        def handles_wrapped_bool_true(controller, index_key):
            output = controller.filter([True])
            expect(output) == {index_key: True}

        def handles_wrapped_bool_false(controller, index_key):
            output = controller.filter([False])
            expect(output) == {index_key: False}

        def handles_str_true(controller, index_key):
            output = controller.filter("True")
            expect(output) == {index_key: True}

        def handles_str_false(controller, index_key):
            output = controller.filter("False")
            expect(output) == {index_key: False}

        def handles_wrapped_str_true(controller, index_key):
            output = controller.filter(["True"])
            expect(output) == {index_key: True}

        def handles_wrapped_str_false(controller, index_key):
            output = controller.filter(["False"])
            expect(output) == {index_key: False}


@behaves_like(result_tag_filter_controller)
def describe_ditr():
    @fixture(scope='module')
    def controller():
        return results.DiamondsInTheRoughFilter()


@behaves_like(result_tag_filter_controller)
def describe_academic_ditr():
    @fixture(scope='module')
    def controller():
        return results.AcademicDiamondsInTheRoughFilter()


@behaves_like(result_tag_filter_controller)
def describe_nonprofit_ditr():
    @fixture(scope='module')
    def controller():
        return results.NonprofitDiamondsInTheRoughFilter()


@behaves_like(result_tag_filter_controller)
def describe_high_potential_donor():
    @fixture(scope='module')
    def controller():
        return results.HighPotentialDonorFilter()


@behaves_like(result_tag_filter_controller)
def describe_ally_giving():
    @fixture(scope='module')
    def controller():
        return results.AllyGivingFilter()


@behaves_like(result_tag_filter_controller)
def describe_opponent_giving():
    @fixture(scope='module')
    def controller():
        return results.OpponentGivingFilter()


@behaves_like(result_tag_filter_controller)
def describe_client_maxed():
    @fixture(scope='module')
    def controller():
        return results.ClientMaxedFilter()


@behaves_like(result_tag_filter_controller)
def describe_client_giving_current():
    @fixture(scope='module')
    def controller():
        return results.ClientGivingCurrentFilter()


@behaves_like(result_tag_filter_controller)
def describe_client_giving_past():
    @fixture(scope='module')
    def controller():
        return results.ClientGivingPastFilter()


@behaves_like(result_tag_filter_controller)
def describe_client_giving_ever():
    @fixture(scope='module')
    def controller():
        return results.ClientGivingFilter()


@behaves_like(result_tag_filter_controller)
def describe_client_giving_this_year():
    @fixture(scope='module')
    def controller():
        return results.ClientGivingThisYearFilter()


@behaves_like(result_tag_filter_controller)
def describe_non_client_giving():
    @fixture(scope='module')
    def controller():
        return results.NonClientGivingFilter()


@behaves_like(result_tag_filter_controller)
def describe_client_largest_donors():
    @fixture(scope='module')
    def controller():
        return results.ClientLargestDonorFilter()


def describe_giving_breakdown():
    @fixture(scope='module')
    def controller():
        return results.GivingBreakdownFilter()

    @fixture(scope='module')
    def index_key(controller):
        return controller.index_arg

    def describe_filter():
        def missing_giving_period_means_unbound(controller, index_key):
            result = controller.filter([{"donation_target": "target",
                                         "amount_partition": 10}])
            data = result[index_key]
            expect(data['target']).contains(None)
            expect(data) == {'target': {None: {'amount': {10: True}}}}

        def it_handles_num_donations_over_time_query(controller, index_key):
            result = controller.filter([{"donation_target": "client",
                                         "giving_period": 7,
                                         "count_partition": 9}])
            expect(result[index_key]) == {"client": {7: {"count": {9: True}}}}

        def it_handles_amt_donations_over_time_query(controller, index_key):
            result = controller.filter([{"donation_target": "ally",
                                         "giving_period": 3,
                                         "amount_partition": 100}])
            expect(result[index_key]) == {"ally": {3: {"amount": {100: True}}}}

        def it_combines_amounts(controller, index_key):
            result = controller.filter([{"donation_target": "ally",
                                         "giving_period": 3,
                                         "amount_partition": amt}
                                        for amt in [2, 1001, 7832]])
            expect(result[index_key]) == {"ally": {3: {"amount": {2: True,
                                                                  1001: True,
                                                                  7832: True}}}}

        def it_combines_amount_and_count(controller, index_key):
            result = controller.filter([{"donation_target": "ally",
                                         "giving_period": 3,
                                         "amount_partition": 100},
                                        {"donation_target": "ally",
                                         "giving_period": 3,
                                         "count_partition": 70}])
            expect(result[index_key]) == {"ally": {3: {"amount": {100: True},
                                                       "count": {70: True}}}}

        def it_handles_multiple_giving_periods(controller, index_key):
            result = controller.filter([{"donation_target": "ally",
                                         "giving_period": 3,
                                         "amount_partition": 100},
                                        {"donation_target": "ally",
                                         "giving_period": 8,
                                         "count_partition": 70}])
            expect(result[index_key]) == {"ally": {3: {"amount": {100: True}},
                                                   8: {"count": {70: True}}}}

        def it_handles_multiple_targets(controller, index_key):
            result = controller.filter([{"donation_target": "ally",
                                         "giving_period": 3,
                                         "amount_partition": 100},
                                        {"donation_target": "client",
                                         "giving_period": 8,
                                         "count_partition": 70}])
            expect(result[index_key]) == {"ally": {3: {"amount": {100: True}}},
                                          "client": {8: {"count": {70: True}}}}

        def it_handles_complex_case(controller, index_key):
            result = controller.filter([
                {"donation_target": "ally",
                 "giving_period": 3,
                 "amount_partition": 100},
                {"donation_target": "ally",
                 "count_partition": 100},
                {"donation_target": "client",
                 "giving_period": 8,
                 "count_partition": 70},
                {"donation_target": "client",
                 "giving_period": 8,
                 "amount_partition": 6},
                {"donation_target": "client",
                 "giving_period": 2,
                 "amount_partition": 6},
                {"donation_target": "all",
                 "amount_partition": 10000},
            ])
            expect(result[index_key]) == {"ally": {3: {"amount": {100: True}},
                                                   None: {"count": {100: True}}},
                                          "client": {8: {"count": {70: True},
                                                         "amount": {6: True}},
                                                     2: {"amount": {6: True}}},
                                          "all": {None: {"amount": {10000: True}}}}

    def describe_extract():
        def _create_feature_result(is_client=True):
            data = {}
            fr = Mock(**{
                'feature_config.feature.title': 'Client Matches' if is_client else 'X Matches',
                'signals': {
                    'signal_sets': [
                        {'giving_overview': data}
                    ]
                }
            })
            return data, fr

        def _client_data():
            return {'3': [[2700]], '5': [[2600]], '1': [[2700]]}

        def _nfp_data():
            return {
                '3,True': [[25733.0], [35397.0], [100000.0], [10.0], [5000000.0], [250000.0]],
                '3,False': [[10.0],
                            [100000.0],
                            [10782.0],
                            [25733.0],
                            [1000000.0],
                            [1000000.0],
                            [258537.0],
                            [10782.0],
                            [104708.0],
                            [51045.0]],
                '4,False': [[50000.0],
                            [100000.0],
                            [1000000.0],
                            [1000.0],
                            [5298874.0],
                            [51045.0],
                            [1000000.0],
                            [10782.0],
                            [1000000.0],
                            [25000.0],
                            [51045.0],
                            [1000000.0],
                            [10782.0],
                            [10.0],
                            [258537.0],
                            [500000.0],
                            [25733.0],
                            [13000000.0],
                            [1000000.0],
                            [250000.0],
                            [104708.0]],
                '4,True': [[100000.0], [10.0], [5000000.0], [250000.0]],
                '5,False': [[1000000.0],
                            [100000.0],
                            [10000.0],
                            [10.0],
                            [25000.0],
                            [1000.0],
                            [1000000.0],
                            [25000.0],
                            [500.0],
                            [10782.0],
                            [10782.0],
                            [10000.0],
                            [15601.0],
                            [1000000.0],
                            [500000.0],
                            [1000000.0],
                            [25733.0],
                            [10782.0],
                            [51045.0],
                            [10.0],
                            [10.0],
                            [10.0],
                            [10782.0],
                            [25733.0],
                            [12000000.0],
                            [1300000.0],
                            [250000.0],
                            [12000000.0],
                            [1300000.0],
                            [250000.0],
                            [12000000.0],
                            [1300000.0],
                            [250000.0],
                            [12000000.0],
                            [1300000.0],
                            [250000.0],
                            [12000000.0],
                            [1300000.0],
                            [250000.0],
                            [12000000.0],
                            [1300000.0],
                            [250000.0],
                            [12000000.0],
                            [1300000.0],
                            [250000.0],
                            [12000000.0],
                            [1300000.0],
                            [250000.0],
                            [12000000.0],
                            [1300000.0],
                            [250000.0],
                            [12000000.0],
                            [1300000.0],
                            [250000.0],
                            [12000000.0],
                            [1300000.0],
                            [250000.0],
                            [10782.0],
                            [10.0],
                            [10.0]],
                '5,True': [[10.0], [10.0], [5000000.0], [104708.0]],
                '6,False': [[10.0],
                            [25000.0],
                            [10782.0],
                            [48502.0],
                            [5298874.0],
                            [1000000.0],
                            [10782.0],
                            [50000.0],
                            [258537.0],
                            [25733.0],
                            [500000.0],
                            [100000.0],
                            [10000.0],
                            [100000.0],
                            [50000.0],
                            [104708.0],
                            [25733.0],
                            [10.0],
                            [11000000.0],
                            [1300000.0],
                            [250000.0],
                            [10782.0],
                            [25733.0],
                            [10.0],
                            [100000.0],
                            [10.0]],
                '6,True': [[25733.0], [23358.0]],
                '7,False': [[48502.0],
                            [258537.0],
                            [100000.0],
                            [104708.0],
                            [1000000.0],
                            [25000.0],
                            [5138.0],
                            [1000000.0],
                            [0.0],
                            [1000.0],
                            [10.0],
                            [10.0],
                            [10000.0],
                            [10.0],
                            [10.0],
                            [10000000.0],
                            [10.0],
                            [250000.0],
                            [1000000.0],
                            [10.0],
                            [1000000.0],
                            [104708.0],
                            [100000.0],
                            [104708.0],
                            [100000.0],
                            [144742.0]],
                '7,True': [[100000.0], [10.0], [50000.0]],
                '8,False': [[250000.0],
                            [10782.0],
                            [1000000.0],
                            [1000302.0],
                            [25733.0],
                            [10.0],
                            [10.0],
                            [104708.0],
                            [104708.0],
                            [10.0],
                            [10.0],
                            [10000.0],
                            [1000000.0],
                            [1000000.0],
                            [25733.0],
                            [10.0],
                            [250000.0],
                            [50000.0],
                            [10.0],
                            [9000000.0],
                            [425267.0],
                            [250000.0],
                            [10782.0],
                            [100000.0]],
                '8,True': [[100000.0]],
                '9,False': [[51045.0],
                            [10.0],
                            [10.0],
                            [10.0],
                            [10782.0],
                            [10.0],
                            [1000000.0],
                            [250000.0],
                            [1000302.0],
                            [250000.0],
                            [104708.0],
                            [1000000.0],
                            [1137133.0],
                            [51045.0],
                            [250000.0],
                            [250000.0],
                            [100000.0],
                            [1000000.0],
                            [10.0],
                            [1000000.0],
                            [50000.0],
                            [250000.0],
                            [50000.0],
                            [10.0],
                            [1000000.0],
                            [10.0],
                            [50000.0],
                            [10.0],
                            [144742.0]],
                '9,True': [[100000.0], [104708.0]],
                '10,False': [[1000000.0],
                             [25000.0],
                             [2939393.0],
                             [500000.0],
                             [10.0],
                             [10.0],
                             [5138.0],
                             [500000.0],
                             [425267.0],
                             [425267.0],
                             [2650.0],
                             [250000.0],
                             [1000000.0],
                             [1137133.0],
                             [25000.0],
                             [500000.0],
                             [104708.0],
                             [1000000.0],
                             [10.0],
                             [7000000.0],
                             [1000000.0],
                             [10.0],
                             [250000.0],
                             [10.0],
                             [10.0],
                             [5000000.0],
                             [10.0],
                             [322206.0],
                             [104708.0],
                             [104708.0],
                             [322206.0],
                             [10.0],
                             [1000000.0]],
                '10,True': [[100000.0]],
                '11,False': [[1000000.0],
                             [258537.0],
                             [250000.0],
                             [10.0],
                             [250000.0],
                             [25000.0],
                             [258537.0],
                             [8000000.0],
                             [10.0],
                             [250000.0],
                             [1000000.0],
                             [322206.0],
                             [100000.0],
                             [500000.0],
                             [1000000.0],
                             [322206.0],
                             [126620.0],
                             [1000000.0],
                             [10.0],
                             [1000000.0],
                             [5000000.0]],
                '11,True': [[10.0], [250000.0], [10.0]],
                '12,False': [[50062.0],
                             [10000.0],
                             [25000.0],
                             [25000.0],
                             [250000.0],
                             [250000.0],
                             [250000.0],
                             [10.0],
                             [10.0],
                             [10.0],
                             [100000.0],
                             [250000.0],
                             [1000000.0],
                             [5138.0],
                             [25000.0],
                             [5138.0],
                             [10.0],
                             [10.0],
                             [50000.0],
                             [250000.0],
                             [144742.0],
                             [800000.0],
                             [25000.0],
                             [25000.0],
                             [5000000.0],
                             [100000.0],
                             [99922.0],
                             [500000.0],
                             [10.0],
                             [5000000.0],
                             [200000.0],
                             [1000000.0],
                             [25000.0]],
                '12,True': [[4667556.0], [10.0], [104708.0]],
                '13,False': [[1000000.0],
                             [2500.0],
                             [250000.0],
                             [250000.0],
                             [5000.0],
                             [500000.0],
                             [250000.0],
                             [1000000.0],
                             [2500.0],
                             [60467.0],
                             [5138.0],
                             [10.0],
                             [104708.0],
                             [1000000.0],
                             [1000000.0],
                             [100000.0],
                             [15000.0],
                             [100000.0]],
                '13,True': [[2238095.0]],
                '14,False': [[200000.0],
                             [1387.0],
                             [100000.0],
                             [100000.0],
                             [2500.0],
                             [126620.0],
                             [200000.0],
                             [1000000.0],
                             [100000.0],
                             [10.0],
                             [1000000.0],
                             [170.0]],
                '14,True': [[2238095.0], [1000000.0], [100000.0]],
                '15,False': [[51045.0],
                             [10.0],
                             [10.0],
                             [258537.0],
                             [5138.0],
                             [1000000.0],
                             [1387.0],
                             [1000000.0],
                             [25733.0],
                             [1000000.0]],
                '15,True': [[10.0]],
                '16,False': [[10782.0],
                             [10782.0],
                             [10000.0],
                             [5138.0],
                             [10782.0],
                             [500000.0],
                             [500000.0],
                             [51045.0],
                             [250000.0],
                             [508.0],
                             [1000000.0],
                             [1000000.0],
                             [100000.0],
                             [100000.0],
                             [50000.0]],
                '16,True': [[51045.0]],
                '17,False': [[10782.0],
                             [5138.0],
                             [100000.0],
                             [10000.0],
                             [500000.0],
                             [5138.0],
                             [10782.0],
                             [100000.0],
                             [126620.0],
                             [5000.0],
                             [250000.0],
                             [250000.0],
                             [25733.0]],
                '17,True': [[51045.0]],
                '18,False': [[15601.0],
                             [2650.0],
                             [258537.0],
                             [1000000.0],
                             [5138.0],
                             [1000000.0],
                             [250000.0],
                             [47636.0],
                             [100000.0],
                             [50000.0],
                             [1000000.0],
                             [10.0],
                             [10.0],
                             [100000.0],
                             [99470.0],
                             [100000.0]],
                '19,False': [[50000.0], [104708.0], [100000.0], [5000.0]],
                '20,False': [[5138.0], [10000.0], [10.0], [10000.0]],
                '21,False': [[5000.0], [2000.0], [500.0], [50000.0]],
                '24,False': [[5138.0], [10.0], [1110.0]],
                '31,False': [[1110.0]]
            }

        @fixture
        def client_feature_result():
            data, fr = _create_feature_result(True)
            data.update(_client_data())
            return fr

        @fixture
        def nfp_feature_result():
            data, fr = _create_feature_result(False)
            data.update(_nfp_data())
            return fr

        def it_works(controller, client_feature_result, nfp_feature_result):
            feature_results = [client_feature_result, nfp_feature_result]
            output = controller.extract(result=None,
                                        feature_results=feature_results)
            expected = {
                'giving_breakdown': {
                    'all': {
                        1: {'count': {1: True, 2: False, 5: False, 10: False, 20: False, 30: False, 50: False, 100: False},
                            'amount': { 500: True, 1000: True, 2000: True, 5000: False, 7500: False, 10000: False, 20000: False}},
                        2: {'count': { 1: True, 2: False, 5: False, 10: False, 20: False, 30: False, 50: False, 100: False},
                            'amount': { 500: True, 1000: True, 2000: True, 5000: False, 7500: False, 10000: False, 20000: False}},
                        3: {'count': { 1: True, 2: True, 5: True, 10: True, 20: False, 30: False, 50: False, 100: False},
                            'amount': { 500: True, 1000: True, 2000: True, 5000: True, 7500: True, 10000: True, 20000: True}},
                        5: {'count': { 1: True, 2: True, 5: True, 10: True, 20: True, 30: True, 50: True, 100: True},
                            'amount': { 500: True, 1000: True, 2000: True, 5000: True, 7500: True, 10000: True, 20000: True}},
                        10: {'count': { 1: True, 2: True, 5: True, 10: True, 20: True, 30: True, 50: True, 100: True},
                             'amount': { 500: True, 1000: True, 2000: True, 5000: True, 7500: True, 10000: True, 20000: True}},
                        15: {'count': { 1: True, 2: True, 5: True, 10: True, 20: True, 30: True, 50: True, 100: True},
                             'amount': { 500: True, 1000: True, 2000: True, 5000: True, 7500: True, 10000: True, 20000: True}},
                        None: {'count': { 1: True, 2: True, 5: True, 10: True, 20: True, 30: True, 50: True, 100: True},
                               'amount': { 500: True, 1000: True, 2000: True, 5000: True, 7500: True, 10000: True, 20000: True}}},
                    'ally': {
                        3: {'count': { 1: True, 2: True, 5: True, 10: False, 20: False, 30: False, 50: False, 100: False},
                            'amount': { 500: True, 1000: True, 2000: True, 5000: True, 7500: True, 10000: True, 20000: True}},
                        5: {'count': { 1: True, 2: True, 5: True, 10: True, 20: False, 30: False, 50: False, 100: False},
                            'amount': { 500: True, 1000: True, 2000: True, 5000: True, 7500: True, 10000: True, 20000: True}},
                        10: {'count': { 1: True, 2: True, 5: True, 10: True, 20: True, 30: False, 50: False, 100: False},
                             'amount': { 500: True, 1000: True, 2000: True, 5000: True, 7500: True, 10000: True, 20000: True}},
                        15: {'count': { 1: True, 2: True, 5: True, 10: True, 20: True, 30: True, 50: False, 100: False},
                             'amount': { 500: True, 1000: True, 2000: True, 5000: True, 7500: True, 10000: True, 20000: True}},
                        None: {'count': { 1: True, 2: True, 5: True, 10: True, 20: True, 30: True, 50: False, 100: False},
                               'amount': { 500: True, 1000: True, 2000: True, 5000: True, 7500: True, 10000: True, 20000: True}}},
                    'client': {
                        1: {'count': { 1: True, 2: False, 5: False, 10: False, 20: False, 30: False, 50: False, 100: False},
                            'amount': { 500: True, 1000: True, 2000: True, 5000: False, 7500: False, 10000: False, 20000: False}},
                        2: {'count': { 1: True, 2: False, 5: False, 10: False, 20: False, 30: False, 50: False, 100: False},
                            'amount': { 500: True, 1000: True, 2000: True, 5000: False, 7500: False, 10000: False, 20000: False}},
                        3: {'count': { 1: True, 2: True, 5: False, 10: False, 20: False, 30: False, 50: False, 100: False},
                            'amount': { 500: True, 1000: True, 2000: True, 5000: True, 7500: False, 10000: False, 20000: False}},
                        5: {'count': { 1: True, 2: True, 5: False, 10: False, 20: False, 30: False, 50: False, 100: False},
                            'amount': { 500: True, 1000: True, 2000: True, 5000: True, 7500: True, 10000: False, 20000: False}},
                        10: {'count': { 1: True, 2: True, 5: False, 10: False, 20: False, 30: False, 50: False, 100: False},
                             'amount': { 500: True, 1000: True, 2000: True, 5000: True, 7500: True, 10000: False, 20000: False}},
                        15: {'count': { 1: True, 2: True, 5: False, 10: False, 20: False, 30: False, 50: False, 100: False},
                             'amount': { 500: True, 1000: True, 2000: True, 5000: True, 7500: True, 10000: False, 20000: False}},
                        None: {'count': { 1: True, 2: True, 5: False, 10: False, 20: False, 30: False, 50: False, 100: False},
                               'amount': { 500: True, 1000: True, 2000: True, 5000: True, 7500: True, 10000: False, 20000: False}}}}
            }
            expect(output) == expected


def describe_persona():
    @fixture(scope='module')
    def controller():
        return results.PersonaFilter()

    @fixture(scope='module')
    def index_key(controller):
        return controller.index_arg

    def describe_filter():
        def it_supports_str_input(controller, index_key):
            result = controller.filter('personatag')
            expect(result[index_key]) == {'personatag': True}

        def it_supports_list_of_tags(controller, index_key):
            result = controller.filter(['personatag', 'othertag'])
            expect(result[index_key]) == {'personatag': True, 'othertag': True}

        def it_supports_mapping_input(controller, index_key):
            # This also allows us to support negation
            result = controller.filter({'personatag': True, 'othertag': False})
            expect(result[index_key]) == {'personatag': True, 'othertag': False}

    def describe_extract():
        def it_creates_index_entries_for_all_identifiers(controller, index_key,
                                                         mocker):
            controller.identifiers = ['a', 'b', 'c', 'd']
            mock_result = mocker.Mock(tags=['b', 'c'])
            output = controller.extract(mock_result, [])
            expect(output) == {index_key: {'a': False, 'b': True, 'c': True,
                                           'd': False}}

    def it_stores_identifiers_in_analysisapplied_filters(controller):
        controller.identifiers = ['a', 'b', 'c', 'd']
        output = controller.get_filter_configs()
        expect(output) == {'identifiers': ['a', 'b', 'c', 'd']}

    @mark.django_db
    def factory_populates_relevant_identifiers_on_instance():
        feature_config = FeatureConfigFactory()
        analysis_config = AnalysisConfigFactory(feature_configs=[feature_config])
        persona_instances = [PersonaInstanceFactory(account=analysis_config.account,
                                                    persona__feature=feature_config.feature)
                             for _ in range(3)]
        controller = results.PersonaFilter.factory(feature_config, analysis_config)
        identifiers = [pi.identifier for pi in persona_instances]
        expect(controller.identifiers).items_equal(identifiers)
