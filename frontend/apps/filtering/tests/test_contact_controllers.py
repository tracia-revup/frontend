from unittest.mock import patch, MagicMock

from faker import Factory

from frontend.apps.authorize.factories import RevUpUserFactory
from frontend.apps.campaign.factories import (AccountFactory,
                                              AccountProfileFactory)
from frontend.apps.campaign.models.base import AccountProfile
from frontend.apps.contact.factories import (ContactFactory, AddressFactory)
from frontend.apps.filtering.filter_controllers.contacts.gender \
    import GenderFilter
from frontend.apps.filtering.filter_controllers.contacts.imports import \
    (ImportSourceFilter, ImportSourceDetailFilter, ImportDateFilter)
from frontend.apps.filtering.filter_controllers.contacts.location import \
    (CityFilter, StateFilter, ZipCodeFilter, AreaCodeFilter, CountyFilter,
     MetroAreaFilter)
from frontend.apps.filtering.filter_controllers.contacts.name import \
    (GivenNameFilter, FamilyNameFilter)
from frontend.apps.filtering.filter_controllers.results.correlated_campaign \
    import CorrelatedCampaignFilter
from frontend.apps.locations.factories import (CityFactory, ZipCodeFactory,
    LinkedInLocationFactory, CountyFactory, StateFactory)
from frontend.libs.utils.test_utils import TestCase


class FilteringTests(TestCase):
    def setUp(self):
        self.faker = Factory.create()
        self.state = StateFactory(name='Oregon')
        self.county = CountyFactory(name='Multnomah', state=self.state)
        self.city = CityFactory(name='Portland', county=self.county,
                                state=self.state)
        self.lil = LinkedInLocationFactory(name='Florida')
        self.zip = ZipCodeFactory(zipcode=90210)
        self.contact1 = ContactFactory(first_name="Jon", last_name="Doe",
                                       addresses__city_key=self.city)
        self.contact2 = ContactFactory(first_name="Steve", last_name="Holt",
                                       addresses__zip_key=self.zip)
        self.contact3 = ContactFactory(first_name='Harvey', last_name='Spektor',
                                       addresses__city_key=self.city)
        self.addr = AddressFactory(contact=self.contact1, street="1 Main St",
                                   post_code="90418")
        self.contact1.addresses.set([self.addr])
        self.contact1.locations.set([self.lil])
        self.user = RevUpUserFactory()
        self.account_profile_2 = AccountProfileFactory(
            account_type=AccountProfile.AccountType.NONPROFIT)
        self.account_1 = AccountFactory()
        self.account_2 = AccountFactory(account_profile=self.account_profile_2)

    @patch("frontend.apps.contact.models.Contact.get_preferred_gender",
           return_value=None)
    def test_gender_filter(self, *_):
        expect = {'gender': None}   # In test env peacock not expected to be configured
        filt = GenderFilter()
        ret = filt.extract(self.contact1)
        self.assertEqual(expect, ret)

    def test_family_name_filter(self):
        expect = {'family_name': 'doe'}
        filt = FamilyNameFilter()
        ret = filt.extract(self.contact1)
        self.assertEqual(expect, ret)

    def test_given_name_filter(self):
        expect = {'given_name': 'jon'}
        filt = GivenNameFilter()
        ret = filt.extract(self.contact1)
        self.assertEqual(expect, ret)

    def test_import_source_filter(self):
        expect = {'import_source': ['google-oauth2']}
        filt = ImportSourceFilter()
        ret = filt.extract(self.contact1)
        self.assertEqual(expect, ret)

    def test_import_source_detail_filter(self):
        expect = None
        filt = ImportSourceDetailFilter()
        ret = filt.extract(self.contact1)
        self.assertEqual(expect, ret)

    def test_import_date_filter(self):
        expect = None
        filt = ImportDateFilter()
        ret = filt.extract(self.contact1)
        self.assertEqual(expect, ret)

    def test_city_filter(self):
        expect = {'city': [self.city.id]}
        filt = CityFilter()
        ret = filt.extract(self.contact1)
        self.assertEqual(expect, ret)

    def test_state_filter(self):
        expect = {'state': [self.city.state.abbr]}
        filt = StateFilter()
        ret = filt.extract(self.contact1)
        self.assertEqual(expect, ret)

    def test_zip_code_filter(self):
        expect = {'zip_code': [addr.zip_key.zipcode
                               for addr in self.contact2.addresses.all()]}
        filt = ZipCodeFilter()
        ret = filt.extract(self.contact2)
        self.assertEqual(expect, ret)

    def test_area_code_filter(self):
        expect = {'area_code':
                  [phone.number[7:10] for phone in
                                      self.contact1.phone_numbers.all()]}
        filt = AreaCodeFilter()
        ret = filt.extract(self.contact1)
        self.assertEqual(expect, ret)

    def test_county_filter(self):
        expect = {'county': [addr.city_key.county.id
                             for addr in self.contact3.addresses.all()]}
        filt = CountyFilter()
        ret = filt.extract(self.contact3)
        self.assertEqual(expect, ret)

    def test_metro_area_filter(self):
        expect = {'metro_area': [loc.pk
                                 for loc in self.contact1.locations.all()]}
        filt = MetroAreaFilter()
        ret = filt.extract(self.contact1)
        self.assertEqual(expect, ret)

    def test_correlated_campaign_filter(self):
        """Tests the get_field method of CorrelatedCampaignFilter"""
        filt = CorrelatedCampaignFilter()
        filt.applied_filter_configs = MagicMock()
        # Verify that the filter label is `Correlated Campaign` for political
        # accounts.
        field_1 = filt.get_field(
            user=self.user, filter_rec=MagicMock(), account=self.account_1)
        assert 'Correlated Campaign' == field_1['label']

        # Verify that the filter label is `Correlated Organization` for
        # nonprofit accounts.
        field_2 = filt.get_field(
            user=self.user, filter_rec=MagicMock(), account=self.account_2)
        assert 'Correlated Organization' == field_2['label']
