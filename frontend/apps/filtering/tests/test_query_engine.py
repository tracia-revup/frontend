from django.db.models import Q
from unittest.mock import patch, Mock, DEFAULT

from frontend.apps.analysis.models import Result
from frontend.apps.filtering.factories import FilterFactory
from frontend.apps.filtering.models import Filter
from frontend.apps.filtering.query_engine import QueryEngine
from frontend.libs.utils.test_utils import TestCase


FT = Filter.FilterTypes


class QueryEngineTests(TestCase):
    def assert_q_equality(self, this, that):
        self.assertEqual(this.negated, that.negated)
        self.assertEqual(this.connector, that.connector)

        def _separate(children):
            tuples = []
            qobjs = []
            for child in children:
                if isinstance(child, Q):
                    qobjs.append(child)
                else:
                    tuples.append(child)
            return tuples, qobjs

        this_tuples, this_qobjs = _separate(this.children)
        that_tuples, that_qobjs = _separate(that.children)

        self.assertCountEqual(this_tuples, that_tuples)
        self.assertEqual(len(this_qobjs), len(that_qobjs))

        for this_qobj in this_qobjs:
            equal_qobj_found = False
            for that_qobj in iter(that_qobjs):
                try:
                    self.assert_q_equality(this_qobj, that_qobj)
                except AssertionError:
                    pass
                else:
                    equal_qobj_found = True
                    that_qobjs.remove(that_qobj)
                    break
            msgtmpl = 'Q object equal to %r not found in in %r'
            self.assertTrue(equal_qobj_found,
                            msg=msgtmpl % (this_qobj, that_qobjs))

    @classmethod
    def setUpTestData(cls):
        cls.gender_filter = FilterFactory(gender=True)
        cls.state_filter = FilterFactory(state=True)
        cls.city_filter = FilterFactory(city=True)
        cls.filters = [cls.gender_filter, cls.state_filter, cls.city_filter]

    def setUp(self):
        self.qengine = QueryEngine(self.filters, Result)

    def test_assert_q_equality(self):
        self.assertRaises(AssertionError,
                          self.assert_q_equality, Q(a=1), Q(a=2))
        self.assertRaises(AssertionError,
                          self.assert_q_equality, Q(a=1), Q(b=1))
        self.assertRaises(AssertionError,
                          self.assert_q_equality, Q(a=1), ~Q(a=1))
        self.assertRaises(AssertionError,
                          self.assert_q_equality,
                          Q(a=1) | Q(b=1), Q(a=1) & Q(b=1))

    @patch.object(QueryEngine, 'process', side_effect=lambda x: (x, []))
    def test_not_handler(self, mock_process):
        value = Q(a=1) | Q(a=2) | Q(a=3, b=1)
        result, _ = self.qengine._handle_not(value)
        self.assert_q_equality(result, ~value)
        self.assertTrue(result.negated)

    @patch.object(QueryEngine, 'process', side_effect=lambda x: (x, []))
    def test_and_handler(self, mock_process):
        value = {}
        result, _ = self.qengine._handle_and(value)
        self.assertIs(result, value)
        mock_process.assert_called_once_with(value)

        values = [Q(a=1)]
        result, _ = self.qengine._handle_and(values)
        self.assert_q_equality(result, Q(a=1))

        values = [Q(a=1), Q(a=2)]
        result, _ = self.qengine._handle_and(values)
        self.assert_q_equality(result, Q(a=1) & Q(a=2))
        self.assertEqual(result.connector, Q.AND)
        self.assertCountEqual(result.children, [('a', 1), ('a', 2)])

        values = [Q(a=1), Q(a=2), Q(a=3, b=1)]
        result, _ = self.qengine._handle_and(values)
        self.assert_q_equality(result, Q(a=1) & Q(a=2) & Q(a=3, b=1))
        self.assertEqual(result.connector, Q.AND)
        self.assertCountEqual(result.children,
                              [('a', 1), ('a', 2), ('a', 3), ('b', 1)])

    @patch.object(QueryEngine, 'process', side_effect=lambda x: (x, []))
    def test_or_handler(self, mock_process):
        values = [Q(a=1)]
        result, _ = self.qengine._handle_or(values)
        self.assert_q_equality(result, Q(a=1))

        values = [Q(a=1), Q(a=2)]
        result, _ = self.qengine._handle_or(values)
        self.assert_q_equality(result, Q(a=1) | Q(a=2))
        self.assertEqual(result.connector, Q.OR)

        values = [Q(a=1), Q(a=2), Q(a=3, b=1)]
        result, _ = self.qengine._handle_or(values)
        self.assert_q_equality(result, Q(a=1) | Q(a=2) | Q(a=3, b=1))
        self.assertEqual(result.connector, Q.OR)

    @patch.object(QueryEngine, 'process')
    def test_filter_method(self, mock_process):
        contact_filter = Mock(filter_type=FT.CONTACT)
        result_filter = Mock(filter_type=FT.RESULT)
        base_value = Q(a=1)
        user_value = 'somebody'
        partition_value = 'my partition id'
        analysis_value = 'my analysis id'

        filters_used = []
        mock_process.side_effect = lambda x: (x, filters_used)

        result = self.qengine.filter(base_value)
        self.assert_q_equality(result, base_value)

        # Verify that user, partition and analysis do not get added to query
        # returned by process method if no contact or result filters are used.
        result = self.qengine.filter(base_value,
                                     user=user_value,
                                     partition=partition_value,
                                     analysis=analysis_value)
        self.assert_q_equality(result, base_value)

        # Update mock to pretend that a contact filter was used, but result
        # filters were not. The result should be that the query is updated to
        # use the user value if provided.
        filters_used = [contact_filter]
        result = self.qengine.filter(base_value)
        self.assert_q_equality(result, base_value)

        result = self.qengine.filter(base_value,
                                     user=user_value,
                                     partition=partition_value,
                                     analysis=analysis_value)
        self.assert_q_equality(
            result, Q(base_value, contact_index__user=user_value))

        # Update mock to pretend only result filters were used in query.
        filters_used = [result_filter]
        # Since only result filters were used, do not add the supplied user
        # value to the query; only the analysis and partition values should be
        # added.
        result = self.qengine.filter(base_value,
                                     user=user_value,
                                     partition=partition_value,
                                     analysis=analysis_value)
        self.assert_q_equality(result, Q(base_value,
                                         index__analysis=analysis_value,
                                         index__partition=partition_value))

        # If only analysis is provided as an argument to filter, then the query
        # should only be updated with the provided analysis value.
        result = self.qengine.filter(base_value, analysis=analysis_value)
        self.assert_q_equality(
            result, Q(base_value, index__analysis=analysis_value))

        # If only partition is provided as an argument to filter, then the
        # query should only be updated with the provided partition value.
        result = self.qengine.filter(base_value, partition=partition_value)
        self.assert_q_equality(
            result, Q(base_value, index__partition=partition_value))

        # Update mock to pretend contact and result filters were both used in
        # the query for the following tests.
        filters_used = [contact_filter, result_filter]
        # Verify that if no values are provided for user, partition, and
        # analysis, then the query returned by the process method is not
        # modified.
        result = self.qengine.filter(base_value)
        self.assert_q_equality(result, base_value)

        # Since both result and contact filters were present in the query, and
        # user, analysis, and partition were all present as arguments to
        # filter, the query should be updated to contain all three values.
        result = self.qengine.filter(base_value,
                                     user=user_value,
                                     partition=partition_value,
                                     analysis=analysis_value)
        self.assert_q_equality(result, Q(base_value,
                                         contact_index__user=user_value,
                                         index__analysis=analysis_value,
                                         index__partition=partition_value))

        # If only analysis is provided as an argument to filter, then only
        # update the query with the analysis value.
        result = self.qengine.filter(base_value, analysis=analysis_value)
        self.assert_q_equality(
            result, Q(base_value, index__analysis=analysis_value))

        # If only partition is provided as an argument to filter, then only
        # update the query with the partition value.
        result = self.qengine.filter(base_value, partition=partition_value)
        self.assert_q_equality(
            result, Q(base_value, index__partition=partition_value))

        # If only user and partition are provided as arguments to the filter
        # method, then only update the query with the partition and user
        # values.
        result = self.qengine.filter(base_value, user=user_value,
                                     partition=partition_value)
        self.assert_q_equality(result, Q(base_value,
                                         contact_index__user=user_value,
                                         index__partition=partition_value))

        # If only user and analysis are provided as arguments to the filter
        # method, then only update the query with the analysis and user
        # values.
        result = self.qengine.filter(base_value, user=user_value,
                                     analysis=analysis_value)
        self.assert_q_equality(result, Q(base_value,
                                         contact_index__user=user_value,
                                         index__analysis=analysis_value))

    @patch.multiple(QueryEngine, _handle_or=DEFAULT, _handle_and=DEFAULT,
                    _handle_not=DEFAULT)
    def test_process_schema_router(self, _handle_or, _handle_and, _handle_not):
        def _reset_mocks():
            _handle_not.reset_mock()
            _handle_and.reset_mock()
            _handle_or.reset_mock()

        def _side_effect(val):
            return val, []

        _handle_or.side_effect = _side_effect
        _handle_and.side_effect = _side_effect
        _handle_not.side_effect = _side_effect

        test_val = Q(a=1)

        result, _ = self.qengine.process({'$AND': test_val})
        _handle_and.assert_called_once_with(test_val)
        self.assertFalse(_handle_or.called)
        self.assertFalse(_handle_not.called)
        _reset_mocks()

        result, _ = self.qengine.process({'$aNd': test_val})
        _handle_and.assert_called_once_with(test_val)
        _reset_mocks()

        result, _ = self.qengine.process({'$OR': test_val})
        _handle_or.assert_called_once_with(test_val)
        self.assertFalse(_handle_and.called)
        self.assertFalse(_handle_not.called)
        _reset_mocks()

        result, _ = self.qengine.process({'$oR': test_val})
        _handle_or.assert_called_once_with(test_val)
        _reset_mocks()

        result, _ = self.qengine.process({'$NOT': test_val})
        _handle_not.assert_called_once_with(test_val)
        self.assertFalse(_handle_and.called)
        self.assertFalse(_handle_or.called)
        _reset_mocks()

        result, _ = self.qengine.process({'$noT': test_val})
        _handle_not.assert_called_once_with(test_val)

    def test_AND_schema(self):
        # The following queries are all valid, and all produce the same
        # results.
        query1 = {'$AND': [{'gender': 'M'}, {'state': 'CA'}]}
        query2 = {'$AND': {'gender': 'M', 'state': 'CA'}}
        query3 = {'gender': 'M', 'state': 'CA'}

        result1, filters1 = self.qengine.process(query1)
        self.assertCountEqual(
            filters1, [self.gender_filter, self.state_filter])
        self.assert_q_equality(
            result1,
            Q(Q(contact_index__attribs__contains={'gender': 'M'}) &
              Q(contact_index__attribs__contains={'state': ['CA']})))

        result2, filters2 = self.qengine.process(query2)
        self.assertCountEqual(
            filters2, [self.gender_filter, self.state_filter])

        result3, filters3 = self.qengine.process(query3)
        self.assertCountEqual(
            filters3, [self.gender_filter, self.state_filter])

        self.assert_q_equality(result2, Q(result3))
        self.assertEqual(str(Result.objects.filter(result2).query),
                         str(Result.objects.filter(result3).query))
