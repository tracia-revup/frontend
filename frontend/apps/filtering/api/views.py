from django.shortcuts import get_object_or_404
from rest_condition.permissions import And
from rest_framework import viewsets, exceptions, permissions, response
from rest_framework_extensions.utils import compose_parent_pk_kwarg_name

from frontend.apps.authorize.api.permissions import UserIsRequester
from frontend.apps.campaign.models import Account
from frontend.apps.contact_set.models import ContactSet
from frontend.apps.contact_set.utils import ContactSetCategories
from frontend.apps.filtering.utils import FilterSet
from frontend.apps.role.api.permissions import HasPermission
from frontend.apps.role.permissions import AdminPermissions
from frontend.libs.utils.api_utils import ViewMethodChecker, InitialMixin


class FiltersViewSetBase(viewsets.ReadOnlyModelViewSet):

    def _get_filterset_args(self):
        return self.request.user, None

    def _get_user(self):
        raise NotImplementedError

    def _get_account(self):
        raise NotImplementedError

    def _prepare_import_filters(self, import_filters):
        filters = []
        for filter_ in import_filters:
            # cs_category is defined in the subclasses
            if self.cs_category not in filter_.contact_set_categories:
                continue

            user = self._get_user()
            account = self._get_account()

            field = filter_.dynamic_instance.get_field(user=user,
                                                       account=account,
                                                       filter_rec=filter_)
            if field:
                if filter_.dynamic_class.multiquestion:
                    # If the FilterController has multiquestion set to True,
                    # that means `get_field` is returning more than form
                    filters.extend(field)
                else:
                    filters.append(field)
        return filters

    def _prepare_quick_filters(self, quick_filters):
        """Extracts the necessary fields from each QuickFilter object and
        returns a list of dictionaries
        """
        filters = []
        for f in quick_filters:
            filters.append({'id': f.id,
                            'order': f.order,
                            'query': f.query,
                            'title': f.title,
                            'short_title': f.short_title}
                           )
        return filters

    def get_quick_filters(self):
        user, analysis = self._get_filterset_args()
        filter_set = FilterSet(user=user, analysis=analysis)
        return filter_set.get_quick_filters()

    def get_queryset(self):
        user, analysis = self._get_filterset_args()
        filter_set = FilterSet(user=user, analysis=analysis)
        return filter_set.get_all()

    def retrieve(self, request, *args, **kwargs):
        raise exceptions.MethodNotAllowed

    def list(self, request, *args, **kwargs):
        import_filters = self.get_queryset()
        filters = self._prepare_import_filters(import_filters)

        # Return individual and pinned filters if version number in request is
        # greater or equal to 2
        if not request.version:
            request_version = 1
        else:
            try:
                request_version = int(request.version)
            except (ValueError, TypeError):
                request_version = 1
        if request_version >= 2:
            quick_filters = self._prepare_quick_filters(
                self.get_quick_filters())

            filters = dict(
                individual=filters,
                pinned=quick_filters
            )
        return response.Response(filters)


class ContactSetFiltersViewSet(InitialMixin, FiltersViewSetBase):
    """Retrieve the filters available to the requested Seat while viewing
      the requested ContactSet.
    """
    parent_contactset_lookup = compose_parent_pk_kwarg_name("contact_set_id")
    permission_classes = (And(permissions.IsAuthenticated,
                              UserIsRequester,
                              ViewMethodChecker(
                                  'verify_user_has_access_to_contact_set')),)

    def verify_user_has_access_to_contact_set(self, request):
        return bool(self.permissions.get("results"))

    def _get_filterset_args(self):
        return self.contact_set.user, self.contact_set.analysis

    def _get_user(self):
        if self.cs_category == ContactSetCategories.OWNED:
            return self.request.user
        else:
            return self.contact_set.user

    def _get_account(self):
        return self.contact_set.account

    def _pre_permission_initial(self):
        self.contact_set = get_object_or_404(
            ContactSet.objects.select_related('user', 'analysis', 'account'),
            id=self.kwargs.get(self.parent_contactset_lookup))
        self.permissions, self.cs_category = self.contact_set.user_permissions(
            self.request.user)


class AccountFiltersViewSet(FiltersViewSetBase):
    """Retrieve the Filters available to the Account Contacts.
      I.e List Manager
    """
    parent_account_lookup = compose_parent_pk_kwarg_name("account_id")
    permission_classes = (And(permissions.IsAuthenticated,
                              HasPermission(parent_account_lookup,
                                            AdminPermissions.VIEW_CONTACTS)),)

    def _get_filterset_args(self):
        return self.account.account_user, \
               self.account.account_user.get_default_analysis()

    def _get_user(self):
        return self.account.account_user

    def _get_account(self):
        return self.account

    def dispatch(self, *args, **kwargs):
        self.account = get_object_or_404(
            Account.objects.select_related("account_user"),
            id=kwargs.get(self.parent_account_lookup))
        self.cs_category = ContactSetCategories.ADMIN
        return super(AccountFiltersViewSet, self).dispatch(*args, **kwargs)
