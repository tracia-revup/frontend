from functools import reduce

from django.db.models import Q

from frontend.apps.analysis.models.results import Result
from frontend.apps.contact.models import Contact
from frontend.apps.filtering.models import Filter
from frontend.libs.utils.general_utils import deep_merge

FT = Filter.FilterTypes


class QueryEngine(object):
    query_key_prefix_map = {Result:
                            {FT.CONTACT: "contact_index",
                             FT.RESULT: "index"},
                            Contact:
                            {FT.CONTACT: "index",
                             # TODO: reduce the number of joins from Contact to ResultIndex
                             FT.RESULT: "user__current_analysis__results__index"}}

    def __init__(self, filters, query_on):
        self._filters = filters
        self.filters = self._build_filter_map(filters)
        self.query_on = query_on
        assert query_on in self.query_key_prefix_map
        self.query_prefix_map = self.query_key_prefix_map[query_on]
        self._query_key_cache = {}

    @classmethod
    def _build_filter_map(cls, filters):
        filter_map = {}
        for filter_ in filters:
            controller = filter_.dynamic_class()
            filter_map[controller.filter_arg.lower()] = (filter_, controller)
        return filter_map

    def _handle_or(self, values):
        filters_used = set()
        qry_parts = []
        for value in values:
            part, used = self.process(value)
            qry_parts.append(part)
            filters_used.update(used)
        qry = reduce(lambda a, b: a | b, qry_parts)
        return qry, filters_used

    def _handle_and(self, values):
        if isinstance(values, dict):
            return self.process(values)
        qry_parts = []
        filters_used = set()
        for value in values:
            part, used = self.process(value)
            qry_parts.append(part)
            filters_used.update(used)
        qry = reduce(lambda a, b: a & b, qry_parts)
        return qry, filters_used

    def _handle_not(self, values):
        part, used = self.process(values)
        return ~part, used

    def _get_query_key(self, kind, append='__attribs__contains'):
        key = (kind, append or None)
        if key not in self._query_key_cache:
            try:
                qkey = self.query_prefix_map[kind]
            except KeyError:
                raise ValueError("Unknown filter type: {}".format(kind))

            if append:
                qkey += append
            self._query_key_cache[key] = qkey
        return self._query_key_cache[key]

    def filter(self, query_data, analysis=None, partition=None, user=None):
        query, filters_used = self.process(query_data)

        # Get the kinds of filters used
        filter_kinds_used = {f.filter_type for f in filters_used}

        # This section is to reduce the size of the query. By adding these
        # extra values, it reduces the possible index records that need to be
        # joined with the Result set.
        extra_params = {}
        if FT.CONTACT in filter_kinds_used and user:
            key = self._get_query_key(FT.CONTACT, append='__user')
            extra_params[key] = user
        if FT.RESULT in filter_kinds_used and (analysis or partition):
            if analysis:
                key = self._get_query_key(FT.RESULT, "__analysis")
                extra_params[key] = analysis
            if partition:
                key = self._get_query_key(FT.RESULT, "__partition")
                extra_params[key] = partition
        if extra_params:
            query = Q(query, **extra_params)
        return query

    def process(self, query_args):
        if isinstance(query_args, Q):
            return query_args
        assert isinstance(query_args, dict)

        filters_used = set()
        query = {}
        parts = []
        for filter_arg, val in query_args.items():
            filter_arg = filter_arg.lower()
            if filter_arg in self.filters:
                filter_, controller = self.filters[filter_arg]
                filters_used.add(filter_)

                # The filter values should be wrapped in a list at this point.
                if not isinstance(val, (list, tuple)):
                    val = [val]

                key = self._get_query_key(filter_.filter_type)
                filter_data = controller.filter(val)
                current = query.get(key)
                if current is None:
                    query[key] = filter_data
                elif isinstance(current, dict):
                    # Merge nested dicts. Will turn:
                    # `{"correlated_campaign": {"one": true}}`
                    # and
                    # `{"correlated_campaign": {"two": true}}`
                    # into:
                    # `{"correlated_campaign": {"two": true, "one": true}}`
                    current = deep_merge(current, filter_data)
                elif isinstance(current, list):
                    current.extend(filter_data)

            elif filter_arg in ("$and", "$or", "$not"):
                method = "_handle_{}".format(filter_arg.replace("$", ""))
                part, used = getattr(self, method)(val)
                parts.append(part)
                filters_used.update(used)

            else:
                raise ValueError(
                    "Unknown or unconfigured filter: {}".format(filter_arg))
        return Q(*parts, **query), filters_used
