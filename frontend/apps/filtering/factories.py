
import factory
from factory.fuzzy import FuzzyText

from .models import Filter, ContactIndex, ResultIndex
from frontend.apps.filtering.filter_controllers.contacts.gender import GenderFilter
from frontend.apps.filtering.filter_controllers.contacts import location


@factory.use_strategy(factory.CREATE_STRATEGY)
class FilterFactory(factory.DjangoModelFactory):
    class Meta:
        model = Filter

    class Params:
        contact = factory.Trait(
            filter_type=Filter.FilterTypes.CONTACT,
        )
        result = factory.Trait(
            filter_type=Filter.FilterTypes.RESULT,
        )
        gender = factory.Trait(
            title='Gender Filter',
            contact=True,
            dynamic_class=GenderFilter,
        )
        state = factory.Trait(
            title='State Filter',
            contact=True,
            dynamic_class=location.StateFilter,
        )
        city = factory.Trait(
            title='City Filter',
            contact=True,
            dynamic_class=location.CityFilter,
        )


class ContactIndexFactory(factory.DjangoModelFactory):
    class Meta:
        model = ContactIndex
        exclude = ('state_val',)
    user = factory.SubFactory("frontend.apps.authorize.factories"
                              ".RevUpUserFactory")
    contact = factory.SubFactory(
        "frontend.apps.contact.factories.ContactFactory",
        user=factory.SelfAttribute('..user'))

    class Params:
        gender = None
        state = None

    # This value is used to populate state in the index when the
    # ContactIndexFactory is initialized with the parameter state=True.
    # Otherwise it is unused.
    state_val = factory.Iterator(['CA', 'NY'])

    @factory.lazy_attribute
    def attribs(self):
        attribs = {}
        if self.gender:
            attribs['gender'] = self.gender.upper()
        if self.state:
            state = self.state
            if isinstance(state, bool):
                state = self.state_val
            if isinstance(state, str):
                state = [state]
            attribs['state'] = list(map(str.upper, state))
        return attribs


class ResultIndexFactory(factory.DjangoModelFactory):
    class Meta:
        model = ResultIndex
    user = factory.SubFactory("frontend.apps.authorize.factories"
                              ".RevUpUserFactory")
    analysis = factory.SubFactory(
        "frontend.apps.analysis.factories.AnalysisFactory",
        event=None,
        user=factory.SelfAttribute('..user'),
        account=factory.SelfAttribute('..user.current_seat.account'))
    result = factory.SubFactory(
        "frontend.apps.analysis.factories.ResultFactory",
        analysis=factory.SelfAttribute('..analysis'))
    partition = factory.SelfAttribute('result.partition')
