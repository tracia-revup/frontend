import itertools

from frontend.libs.utils.general_utils import instance_cache


class FilterSet(object):
    """Build a set of the filters available to the user"""
    def __init__(self, user, analysis=None):
        self.user = user
        self.analysis = analysis

    @instance_cache
    def get_result_filters(self):
        if not self.analysis:
            user_analysis = self.user.get_default_analysis()
            if user_analysis:
                self.analysis = user_analysis
            else:
                return []

        applied_filters = \
            self.analysis.analysisappliedfilter_set.select_related("filter")
        filters = []
        for applied_filter in applied_filters:
            filter_ = applied_filter.filter
            # Prepare the filter controller by passing it the filter_configs
            # from the through table (if there are any).
            filter_.dynamic_instance.applied_filter_configs = \
                applied_filter.filter_configs
            filters.append(filter_)
        return filters

    @instance_cache
    def get_contact_filters(self):
        return self.user.applied_filters.all()

    @instance_cache
    def get_quick_filters(self):
        """Build a set of quick filters available to the user
        """
        if not self.analysis:
            return []

        return self.analysis.account.account_profile.quick_filters.order_by(
            'order')

    # @instance_cache
    # def get_pinned_filters(self):
    #     # TODO: actually return pinned/saved filters
    #     return []

    def get_all(self):
        return itertools.chain(self.get_contact_filters(),
                               self.get_result_filters())
