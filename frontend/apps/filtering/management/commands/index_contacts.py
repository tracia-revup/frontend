
import sys

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q
from django.db.transaction import atomic

from frontend.apps.analysis.api.views import (
    ContactSetAnalysisContactResultsViewSet)
from frontend.apps.authorize.models import RevUpUser
from frontend.apps.campaign.models.base import Account
from frontend.apps.filtering.models import ContactIndex
from frontend.libs.utils.general_utils import update_progress


class Command(BaseCommand):
    help = 'Re-index contacts'

    def add_arguments(self, parser):
        parser.add_argument('-a', '--account-ids', dest="accounts",
                            action='append', type=int,
                            help="IDs of accounts to be limited to")
        parser.add_argument('-i', '--ignore-account-ids', dest="ignore",
                            action='append', type=int,
                            help="IDs of accounts to be ignored")
        parser.add_argument('-u', '--user-ids', dest="users", action='append',
                            type=int, help="IDs of users to be limited to")
        parser.add_argument('-s', '--skip-user-ids', dest="skip",
                            action='append', type=int,
                            help="IDs of users to skip")
        parser.add_argument('--include-inactive-users', dest="inactive_users",
                            action="store_true",
                            help="Include inactive users.")
        parser.add_argument('--include-inactive-accounts',
                            dest="inactive_accounts", action="store_true",
                            help="Include inactive accounts.")
        parser.add_argument('--include-account-users',
                            dest="account_users", action="store_true",
                            help="Include inactive accounts.")
        parser.add_argument('-r', '--reindex', dest="reindex",
                            action="store_true",
                            help="Recreate index? (Default is to skip "
                                 "processing a contact if it already "
                                 "has an index)")
        parser.add_argument('-p', '--progress', dest="show_progress",
                            action="store_true",
                            help="Show progress percent")

    def _progress_callback(self, percent):
        sys.stdout.write('Progress: {}%\r'.format(percent))
        sys.stdout.flush()

    def handle(self, *args, **options):
        account_ids = options.get('accounts')
        ignore_account_ids = options.get('ignore')
        user_ids = options.get('users')
        skip_user_ids = options.get('skip')
        inactive_users = options.get('inactive_users')
        inactive_accounts = options.get('inactive_accounts')
        account_users = options.get("account_users")
        reindex = options.get('reindex')
        show_progress = options.get("show_progress")

        # Filter accounts
        accounts = Account.objects.all()
        if inactive_accounts:
            self.stdout.write("Including inactive accounts.")
        else:
            accounts = accounts.filter(active=True)
        if account_ids:
            self.stdout.write("Limiting to account ids %s" % account_ids)
            accounts = accounts.filter(pk__in=account_ids)
        if ignore_account_ids:
            self.stdout.write("Ignoring account ids %s" % ignore_account_ids)
            accounts = accounts.exclude(pk__in=ignore_account_ids)

        # Filter users
        if user_ids:
            self.stdout.write("Limiting to user ids %s" % user_ids)
            users = RevUpUser.objects.filter(pk__in=user_ids)
        else:
            users_qry = Q(seats__account__in=accounts)
            if inactive_users:
                self.stdout.write("Including inactive users.")
            else:
                users_qry &= Q(is_active=True)

            if account_users:
                self.stdout.write("Including account_users.")
                users_qry |= Q(account_user__in=accounts)
            else:
                self.stdout.write("Not including account users")

            users = RevUpUser.objects.filter(users_qry).distinct()
            if skip_user_ids:
                self.stdout.write("Skipping user ids %s" % skip_user_ids)
                users = users.exclude(pk__in=skip_user_ids)

        user_count = users.count()
        users = users.order_by("-last_login")
        for user_idx, user in enumerate(users.iterator()):
            self.stdout.write(
                "Processing user {user_idx} of {user_count}: {name}".format(
                    name=user.name, user_count=user_count, user_idx=user_idx))
            index_filters = user.get_index_filters()

            with atomic():
                if reindex:
                    self.stdout.write(
                        "Deleting ContactIndex for any child contacts")
                    ContactIndex.objects.filter(contact__user=user).delete()
                contacts = user.contact_set.filter(is_primary=True, index=None)

                # Counting contacts is an expensive operation and adds time
                # that we may not want to waste if nobody will be watching
                contact_count = contacts.count() if show_progress else None
                self.stdout.write("Indexing {} contacts".format(
                    contact_count if show_progress else "user's"))

                indexes = []
                for i, contact in enumerate(contacts.iterator()):
                    index = ContactIndex.update_index(
                        index_filters, save=False, user=user, contact=contact)
                    if index:
                        indexes.append(index)
                    if show_progress:
                        update_progress(self._progress_callback,
                                        contact_count, i)

                ContactIndex.objects.bulk_create(indexes, batch_size=10000)

            # Save the applied filters to the user
            user.applied_filters.set(index_filters)
            # Invalidate the results cache
            ContactSetAnalysisContactResultsViewSet.invalidate_cache(user)

