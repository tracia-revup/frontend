import sys

from django.core.management.base import BaseCommand
from django.db.models import Q

from frontend.apps.analysis.analyses.person_contrib import PersonContribRanking
from frontend.apps.analysis.models import Analysis, AnalysisAppliedFilter
from frontend.apps.analysis.api.views import (
    ContactSetAnalysisContactResultsViewSet)
from frontend.apps.authorize.models import RevUpUser
from frontend.apps.campaign.models.base import Account
from frontend.apps.filtering.models import ResultIndex, Filter
from frontend.libs.utils.general_utils import (
    BlankObject, FunctionalBuffer, update_progress)


class Command(BaseCommand):
    help = 'Index analysis results'

    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)
        self._analysis_config_cache = {}

    def add_arguments(self, parser):
        parser.add_argument('-a', '--account-ids', dest="accounts",
                            action='append', type=int,
                            help="IDs of accounts to be limited to")
        parser.add_argument('-n', '--analysis-ids', dest="analyses",
                            action='append', type=int,
                            help="IDs of analyses to be limited to")
        parser.add_argument('-i', '--ignore-account-ids', dest="ignore",
                            action='append', type=int,
                            help="IDs of accounts to be ignored")
        parser.add_argument('-u', '--user-ids', dest="users", action='append',
                            type=int, help="IDs of users to be limited to")
        parser.add_argument('-s', '--skip-user-ids', dest="skip",
                            action='append', type=int,
                            help="IDs of users to skip")
        parser.add_argument('--include-inactive-users', dest="inactive_users",
                            action="store_true",
                            help="Include inactive users.")
        parser.add_argument('--include-inactive-accounts',
                            dest="inactive_accounts", action="store_true",
                            help="Include inactive accounts.")
        parser.add_argument('--include-account-users',
                            dest="account_users", action="store_true",
                            help="Include inactive accounts.")
        parser.add_argument('-r', '--reindex', dest="reindex",
                            action="store_true",
                            help=("Recreate index? (Default is to skip "
                                  "processing an analysis if it already "
                                  "has an index)"))

    def _progress_callback(self, percent):
        sys.stdout.write('Progress: {}%\r'.format(percent))
        sys.stdout.flush()

    def _get_filter_config(self, analysis_config):
        if analysis_config.pk in self._analysis_config_cache:
            return self._analysis_config_cache[analysis_config.pk]

        feature_configs = analysis_config.feature_configs.filter(
            filters__isnull=False)
        top_config = {}
        for feature_config in feature_configs.iterator():
            filters = {
                filter_.pk: filter_.dynamic_class.factory(feature_config,
                                                          analysis_config)
                for filter_ in feature_config.filters.filter(
                    filter_type=Filter.FilterTypes.RESULT)}
            top_config[feature_config.pk] = BlankObject(
                feature_config=feature_config, filters=filters)

        self._analysis_config_cache[analysis_config.pk] = top_config
        return top_config

    def _process_analysis(self, analysis):
        analysis_config = analysis.analysis_config
        features = self._get_filter_config(analysis_config)
        PersonContribRanking.apply_filters_to_analysis(analysis, features)
        results = analysis.results.prefetch_related('features')
        result_count = results.count()
        for i, result in enumerate(results.iterator()):
            result_index_model = PersonContribRanking.build_result_index(
                analysis.user, features, result, result.features.all())
            if result_index_model:
                self._result_index_buffer.push(result_index_model)

            update_progress(self._progress_callback, result_count, i)

    def handle(self, *args, **options):
        account_ids = options.get('accounts')
        analysis_ids = options.get('analyses')
        ignore_account_ids = options.get('ignore')
        user_ids = options.get('users')
        skip_user_ids = options.get('skip')
        inactive_users = options.get('inactive_users')
        inactive_accounts = options.get('inactive_accounts')
        account_users = options.get("account_users")
        reindex = options.get('reindex')

        self._result_index_buffer = FunctionalBuffer(
            ResultIndex.objects.bulk_create,
            max_length=200)

        # Filter accounts
        accounts = Account.objects.all()
        if inactive_accounts:
            self.stdout.write("Including inactive accounts.")
        else:
            accounts = accounts.filter(active=True)
        if account_ids:
            self.stdout.write("Limiting to account ids %s" % account_ids)
            accounts = accounts.filter(pk__in=account_ids)
        if ignore_account_ids:
            self.stdout.write("Ignoring account ids %s" % ignore_account_ids)
            accounts = accounts.exclude(pk__in=ignore_account_ids)

        # Filter users
        if user_ids:
            self.stdout.write("Limiting to user ids %s" % user_ids)
            users = RevUpUser.objects.filter(pk__in=user_ids)
        else:
            users_qry = Q(seats__account__in=accounts)
            if inactive_users:
                self.stdout.write("Including inactive users.")
            else:
                users_qry &= Q(is_active=True)

            if account_users:
                self.stdout.write("Including account_users.")
                users_qry |= Q(account_user__in=accounts)
            else:
                self.stdout.write("Not including account users")

            users = RevUpUser.objects.filter(users_qry).distinct()
            if skip_user_ids:
                self.stdout.write("Skipping user ids %s" % skip_user_ids)
                users = users.exclude(pk__in=skip_user_ids)

        user_count = users.count()
        # Prioritize users by recent activity
        users = users.order_by("-last_login")
        for user_idx, user in enumerate(users.iterator()):
            # Only operate on analysis that are complete. We do not want to
            # run the risk of creating indexes on a failed or running analysis
            analyses = user.analysis_set.filter(
                status=Analysis.STATUSES.complete).exclude(
                analysis_config=None)
            # Limit to selected accounts, if specified
            if accounts:
                analyses = analyses.filter(account__in=accounts)

            # Limit to selected analyses, if specified
            if analysis_ids:
                analyses = analyses.filter(id__in=analysis_ids)
            # Otherwise use only the latest analysis per account
            else:
                analyses = analyses.order_by(
                    'account', '-modified').distinct('account')
            for analysis in analyses.iterator():
                if not analysis.analysis_config.feature_configs.filter(
                        filters__isnull=False).exists():
                    # Skip processing analysis if it does not have any
                    # FeatureConfigs that have filters.
                    continue
                if (analysis.applied_filters.exists() or
                        analysis.results.filter(index__isnull=False).exists()):
                    if reindex:
                        ResultIndex.objects.filter(
                            result__analysis=analysis).delete()
                        AnalysisAppliedFilter.objects.filter(
                            analysis=analysis).delete()
                    else:
                        # Skip processing analysis if is already indexed.
                        continue

                self.stdout.write(
                    "Creating analysis result indexes for user {user_idx} of "
                    "{user_count}: {name}({user_id}) for analysis "
                    "{analysis}".format(
                        name=user.name, user_count=user_count, user_id=user.id,
                        user_idx=user_idx, analysis=analysis))
                self._process_analysis(analysis)

            # Invalidate the results cache
            ContactSetAnalysisContactResultsViewSet.invalidate_cache(user)

        self._result_index_buffer.flush()
