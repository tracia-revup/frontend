
from datetime import datetime

from django.db.models import QuerySet

from ..base import FilterController
from frontend.apps.contact.models import ImportRecord
from frontend.apps.contact_set.models import ContactSet


class ImportSourceFilter(FilterController):
    filter_arg = "import_source"
    index_arg = filter_arg

    def extract(self, contact, **kwargs):
        contacts = contact.get_contacts()
        if isinstance(contacts, QuerySet):
            sources = contacts.distinct('contact_type')\
                              .values_list('contact_type', flat=True)
        else:
            sources = {contact.contact_type for contact in contacts}
        sources = list(filter(None, sources))
        return {self.index_arg: sources} if sources else None

    def filter(self, filter_data, **kwargs):
        if isinstance(filter_data, str):
            filter_data = [filter_data]
        return {self.index_arg: filter_data}

    def get_field(self, **kwargs):
        source_map = {v: k
                      for k, v in ImportRecord.TaskType.source_map.items()}
        source_choices = [(source_map[value], label)
                          for value, label in ImportRecord.TaskType.choices()
                          if value in source_map]
        return dict(label="Upload Provider",
                    group="imports",
                    type="choices",
                    display_type="contact_src_icon",
                    desc="Select the provider of the contact import.",
                    choices=source_choices,
                    expected=[
                        dict(field=self.filter_arg, type="text")
                    ])


class ImportSourceDetailFilter(FilterController):
    filter_arg = "import_source_detail"
    index_arg = filter_arg

    def extract(self, contact, **kwargs):
        contacts = contact.get_contacts()
        if isinstance(contacts, QuerySet):
            sources = contacts.distinct('import_record__uid')\
                              .values_list('import_record__uid', flat=True)
        else:
            sources = {contact.import_record.uid for contact in contacts
                       if contact.import_record_id and
                       contact.import_record.uid}
        return self._prepare_index(sources)

    def _prepare_index(self, sources):
        sources = list(filter(None, sources))
        return {self.index_arg: sources} if sources else None

    def filter(self, filter_data, **kwargs):
        if isinstance(filter_data, str):
            filter_data = [filter_data]
        return {self.index_arg: filter_data}

    def get_field(self, user, account, **kwargs):
        # Query to get the available import sources for this user
        sources = ContactSet.objects.active().filter(
            user=user, account=account).exclude(source_uid="").values_list(
            "source", "source_uid", "title").distinct()

        # Build the choices from the results of the query
        Sources = ContactSet.Sources
        source_choices = []
        for source, source_uid, title in sources:
            source = Sources.translate(source, default="Other")
            # For some sources, the translated source and the label are the
            # same. We don't want to say "Outlook - Outlook"
            choice_label = "{} - {}".format(source, title) \
                           if source != title else title
            source_choices.append((source_uid, choice_label))

        return dict(label="Upload Source",
                    group="imports",
                    type="dropdown",
                    choices=source_choices,
                    display_type="contact_icon_dropdown",
                    desc="Select the specific contact import source.",
                    expected=[
                        dict(field=self.filter_arg, type="text")
                    ])


class ImportDateFilter(FilterController):
    filter_arg = "import_date"
    index_arg = filter_arg

    def extract(self, contact, **kwargs):
        contacts = contact.get_contacts()
        date_tuples = set()
        if isinstance(contacts, QuerySet):
            import_dates = contacts.filter(import_record__isnull=False)\
                    .distinct('import_record')\
                    .values_list('import_record__import_dt', flat=True)\
                    .iterator()
        else:
            import_dates = (contact.import_record.import_dt
                            for contact in contacts
                            if (contact.import_record_id and
                                contact.import_record.import_dt))

        for import_date in import_dates:
            try:
                date_tuples.add(
                    (import_date.year, import_date.month, import_date.day))
            except AttributeError:
                pass

        dates = [dict(zip(('year', 'month', 'day'), date_tuple))
                 for date_tuple in date_tuples]

        return {self.index_arg: dates} if dates else None

    def filter(self, filter_data, **kwargs):
        def _parse(dt):
            try:
                date = datetime.strptime(dt, "%Y-%m-%d")
            except ValueError:
                return None
            else:
                return {'year': date.year,
                        'month': date.month,
                        'day': date.day}

        if isinstance(filter_data, str):
            filter_data = [filter_data]

        filter_data = list(filter(None, map(_parse, filter_data)))
        # TODO: add support for "partial" dates
        # TODO: add support for date ranges
        return {self.index_arg: filter_data}

    def get_field(self, user, **kwargs):
        # Provide a list of import dates for use in selection.
        # We have to search from the contacts to ensure we are querying the
        # correct user, since the import_record stores the importing user,
        # not the owning user (in the case of Account Contacts)
        choices = set(user.contact_set.values_list(
            "import_record__import_dt", flat=True).distinct())
        choices = (dt.strftime('%Y-%m-%d')
                   for dt in sorted(list(filter(None, choices)), reverse=True))
        return dict(label="Import Date",
                    group="imports",
                    type="dropdown",
                    display_type="date",
                    sortOrder="descending",
                    desc="Select the date of the contact import.",
                    choices=[(choice, choice) for choice in choices],
                    expected=[
                        dict(field=self.filter_arg, type="text"),
                    ])
