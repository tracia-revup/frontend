
from ..base import FilterController


class GivenNameFilter(FilterController):
    filter_arg = "given_name"
    index_arg = filter_arg

    def extract(self, contact, **kwargs):
        return {self.index_arg: contact.first_name_lower}

    def filter(self, filter_data, **kwargs):
        if not isinstance(filter_data, str):
            filter_data = filter_data[0].strip().lower()
        return {self.index_arg: filter_data}

    def get_field(self, **kwargs):
        return dict(label="First Name",
                    group="biography",
                    type="text",
                    desc="The first name of the contact. Exact matches only.",
                    expected=[
                        dict(field=self.filter_arg, type="text")
                    ])


class FamilyNameFilter(FilterController):
    filter_arg = "family_name"
    index_arg = filter_arg

    def extract(self, contact, **kwargs):
        return {self.index_arg: contact.last_name_lower}

    def filter(self, filter_data, **kwargs):
        if not isinstance(filter_data, str):
            filter_data = filter_data[0].strip().lower()
        return {self.index_arg: filter_data}

    def get_field(self, **kwargs):
        return dict(label="Last Name",
                    group="biography",
                    type="text",
                    desc="The last name of the contact. Exact matches only.",
                    expected=[
                        dict(field=self.filter_arg, type="text")
                    ])
