
from ..base import FilterController


class GenderFilter(FilterController):
    filter_arg = "gender"
    index_arg = filter_arg

    def extract(self, contact, **kwargs):
        return {self.index_arg: contact.get_preferred_gender()}

    def filter(self, filter_data, **kwargs):
        if not isinstance(filter_data, str):
            filter_data = filter_data[0].upper()
        return {self.index_arg: filter_data}

    def get_field(self, **kwargs):
        return dict(label="Predicted Gender",
                    group="demographics",
                    type="choices",
                    desc='The predicted gender of the contact. This is only '
                         'a prediction, and is not guaranteed to be perfectly '
                         'accurate.',
                    choices=[("M", "Male"),
                             ("F", "Female")],
                    expected=[
                        dict(field=self.filter_arg, type="text")
                    ])
