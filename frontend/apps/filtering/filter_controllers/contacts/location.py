from django.db.models import QuerySet
from django.db.models.functions import Substr
from django.urls import reverse
from localflavor.us.us_states import US_STATES

from ..base import FilterController
from frontend.apps.contact.models import Contact


class CityFilter(FilterController):
    filter_arg = "city"
    index_arg = filter_arg

    def extract(self, contact, **kwargs):
        contacts = contact.get_contacts()
        if isinstance(contacts, QuerySet):
            city_ids = contacts.values_list(
                'addresses__city_key_id', flat=True).distinct()
        else:
            city_ids = {city_id
                        for contact in contacts
                        for city_id in contact.addresses.values_list(
                            'city_key_id', flat=True).distinct()}

        city_ids = list(filter(None, city_ids))
        return {self.index_arg: city_ids} if city_ids else None

    def filter(self, filter_data, **kwargs):
        if isinstance(filter_data, str):
            filter_data = [filter_data]
        filter_data = list(map(int, filter_data))
        return {self.index_arg: filter_data}

    def get_field(self, **kwargs):
        return dict(label="City",
                    group="location",
                    type="autocomplete",
                    desc="Filter by City. To find the city, start typing its "
                         "name. You MUST select it from the list. City "
                         "searches may be narrowed by state. E.g.: "
                         "Concord, MA",
                    url=reverse('locations_cities_api-list'),
                    expected=[
                        dict(field=self.filter_arg, type="int")
                    ])


class StateFilter(FilterController):
    filter_arg = "state"
    index_arg = filter_arg

    @classmethod
    def filter_state_abbr(cls, abbr):
        if abbr and isinstance(abbr, str):
            abbr = abbr.strip()
            if len(abbr) == 2:
                return True
        return False

    def extract(self, contact, **kwargs):
        contacts = contact.get_contacts()
        if not isinstance(contacts, QuerySet):
            contacts = Contact.objects.filter(
                pk__in=(c.pk for c in contacts))

        states = contacts.values_list(
            'addresses__city_key__state__abbr', flat=True).distinct()
        states = list(filter(None, states))

        # If we couldn't get state from a city key, try for an abbreviation
        if not states:
            states = contacts.values_list('addresses__region',
                                          flat=True).distinct()
            states = list(filter(self.filter_state_abbr, states))

        # If we still can't find a state, try to get it from the zipcodes
        if not states:
            states = contacts.values_list(
                "addresses__zip_key__city__state__abbr", flat=True).distinct()
            states = list(filter(None, states))

        # If we still couldn't find a state, we can try to get states from the
        # metro area
        if not states:
            states = contacts.values_list(
                'locations__zipcodes__city__state__abbr', flat=True).distinct()
            states = list(filter(None, states))

        return {self.index_arg: states} if states else None

    def filter(self, filter_data, **kwargs):
        if isinstance(filter_data, str):
            filter_data = [filter_data]
        filter_data = [s.upper() for s in filter_data]
        return {self.index_arg: filter_data}

    def get_field(self, **kwargs):
        return dict(label="State",
                    group="location",
                    type="dropdown",
                    dropdown_data='usStates',
                    choices=list(US_STATES),
                    desc="Select the state to search in from the list below.",
                    expected=[
                        dict(field=self.filter_arg, type="text")
                    ])


class ZipCodeFilter(FilterController):
    filter_arg = "zip_code"
    index_arg = filter_arg

    def extract(self, contact, **kwargs):
        contacts = contact.get_contacts()
        if isinstance(contacts, QuerySet):
            zip_ids = contacts.values_list(
                'addresses__zip_key__zipcode', flat=True).distinct()
        else:
            zip_ids = {zip_id
                       for contact in contacts
                       for zip_id in contact.addresses.values_list(
                           'zip_key__zipcode', flat=True).distinct()}

        zip_ids = list(filter(None, zip_ids))
        return {self.index_arg: zip_ids} if zip_ids else None

    def get_field(self, **kwargs):
        return dict(label="Zip Code",
                    group="location",
                    type="text",
                    desc="Enter the 5 digit zip code you wish to search in.",
                    display_type='integer',
                    display_length=5,
                    expected=[
                        dict(field=self.filter_arg, type="text")
                    ])


class AreaCodeFilter(FilterController):
    filter_arg = "area_code"
    index_arg = filter_arg

    def extract(self, contact, **kwargs):
        contacts = contact.get_contacts()
        if not isinstance(contacts, QuerySet):
            contacts = Contact.objects.filter(
                pk__in=(c.pk for c in contacts))
        phone_pattern = r'^tel:\+1-\d{3}-\d{3}-\d{4}'
        areacodes = contacts.filter(
            # Only look at phone numbers that have area codes
            phone_numbers__number__regex=phone_pattern).annotate(
                # Use substring function to extract area code from phone
                # number.
                areacode=Substr('phone_numbers__number', 8,
                                3)).values_list('areacode',
                                                flat=True).distinct()
        areacodes = list(filter(None, areacodes))
        return {self.index_arg: areacodes} if areacodes else None

    def get_field(self, **kwargs):
        return dict(label="Area Code",
                    group="location",
                    type="text",
                    desc="Enter the 3 digit area code you wish to search in.",
                    display_type='integer',
                    display_length=3,
                    expected=[
                        dict(field=self.filter_arg, type="text")
                    ])


class CountyFilter(FilterController):
    filter_arg = "county"
    index_arg = filter_arg

    def extract(self, contact, **kwargs):
        contacts = contact.get_contacts()
        if not isinstance(contacts, QuerySet):
            contacts = Contact.objects.filter(
                pk__in=(c.pk for c in contacts))

        county_ids = contacts.values_list(
            'addresses__city_key__county_id',
            flat=True).distinct()

        county_ids = list(filter(None, county_ids))
        return {self.index_arg: county_ids} if county_ids else None

    def filter(self, filter_data, **kwargs):
        if isinstance(filter_data, str):
            filter_data = [filter_data]
        filter_data = list(map(int, filter_data))
        return {self.index_arg: filter_data}

    def get_field(self, **kwargs):
        return dict(label="County",
                    group="location",
                    type="autocomplete",
                    url=reverse('locations_counties_api-list'),
                    desc="Filter by County. To find the county, start typing "
                         "its name. You MUST select it from the list. "
                         "County searches may be narrowed by state. E.g.: "
                         "Washington, OR",
                    expected=[
                        dict(field=self.filter_arg, type="int")
                    ])


class MetroAreaFilter(FilterController):
    filter_arg = "metro_area"
    index_arg = filter_arg

    def extract(self, contact, **kwargs):
        contacts = contact.get_contacts()
        if not isinstance(contacts, QuerySet):
            contacts = Contact.objects.filter(
                pk__in=(c.pk for c in contacts))

        # Get the metro area from the Contact's LinkedInLocations
        lil_ids = contacts.values_list(
            'locations__pk', flat=True).distinct()
        lil_ids = set(filter(None, lil_ids))

        # Get the metro area from the Contact's zipcodes, corresponding to a
        # LinkedInLocation
        zip_lil_ids = contacts.values_list(
            'addresses__zip_key__location__pk', flat=True).distinct()
        lil_ids.update(list(filter(None, zip_lil_ids)))

        # Get the metro area from the Contact's city, corresponding to a
        # LinkedInLocation
        city_lil_ids = contacts.values_list(
            'addresses__city_key__zips__location__pk', flat=True).distinct()
        lil_ids.update(list(filter(None, city_lil_ids)))

        return {self.index_arg: list(lil_ids)} if lil_ids else None

    def filter(self, filter_data, **kwargs):
        if isinstance(filter_data, str):
            filter_data = [filter_data]
        filter_data = list(map(int, filter_data))
        return {self.index_arg: filter_data}

    def get_field(self, **kwargs):
        return dict(label="Metro Area",
                    group="location",
                    type="autocomplete",
                    url=reverse('locations_metro_area_api-list'),
                    desc="Filter by Metro-area. To find the Metro-area, start "
                         "typing its name. You MUST select it from the list. "
                         "Metro-area may also be searched by a city in the "
                         "Metro-area, or by a state containing the Metro-area",
                    expected=[
                        dict(field=self.filter_arg, type="int")
                    ])
