
from ..base import ResultTagFilterController, ResultFilterController
from frontend.apps.analysis.models import AnalysisAppliedFilter
from frontend.apps.core.product_translations import translate
from frontend.libs.utils.general_utils import public


@public
class CorrelatedCampaignFilter(ResultFilterController):
    filter_arg = "correlated_campaign"
    index_arg = filter_arg

    @classmethod
    def factory(cls, feature_config, analysis_config):
        controller = super(CorrelatedCampaignFilter, cls).factory(
            feature_config, analysis_config)
        controller.campaigns = set(
            filter(None, controller.config_wrapper.get_list('label')))
        return controller

    def get_filter_configs(self):
        if self.campaigns:
            return {'labels': list(self.campaigns)}
        else:
            return super(CorrelatedCampaignFilter, self).get_filter_configs()

    def extract(self, result, feature_configs):
        if not self.campaigns:
            return
        key_contribs = result.key_signals['key_contributions']
        found_campaigns = {key_contrib['label']
                           for key_contrib in key_contribs}
        campaigns = {campaign: campaign in found_campaigns
                     for campaign in self.campaigns}
        return {self.index_arg: campaigns}

    def filter(self, filter_data, **kwargs):
        if isinstance(filter_data, str):
            filter_data = [filter_data]
        filter_data = {tag: True for tag in filter_data}
        # TODO: add support for NOT
        return {self.index_arg: filter_data}

    def get_field(self, user, filter_rec, account=None, **kwargs):
        filter_configs = getattr(self, 'applied_filter_configs', None)
        if not filter_configs:
            user_analysis = user.get_default_analysis()
            if not user_analysis:
                return None
            try:
                filter_configs = user_analysis.analysisappliedfilter_set.get(
                    filter=filter_rec).filter_configs
            except (AnalysisAppliedFilter.DoesNotExist,
                    AnalysisAppliedFilter.MultipleObjectsReturned):
                return None
        choices = filter_configs.get('labels')
        if not choices:
            return None

        if not account:
            account = user.current_seat.account
        product_type = account.account_profile.product_type

        key_contribution_type = translate("Key Contribution", product_type)

        # TODO: add support for NOT queries, and multiselect.
        return dict(label=key_contribution_type,
                    group="key_contribs",
                    type="choices",
                    desc="The specific {0} the contact contributed to.".format(
                        key_contribution_type),
                    choices=[(choice, choice) for choice in sorted(choices)] ,
                    expected=[
                        dict(field=self.filter_arg, type="text")
                    ])


@public
class AllyGivingFilter(ResultTagFilterController):
    filter_arg = "ally-giving"
    index_arg = filter_arg
    label = "Gave to allies?"


@public
class OpponentGivingFilter(ResultTagFilterController):
    filter_arg = "oppo-giving"
    index_arg = filter_arg
    label = "Gave to opponents?"
