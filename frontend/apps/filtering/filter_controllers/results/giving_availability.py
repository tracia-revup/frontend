
from ..base import ResultTagFilterController
from frontend.libs.utils.general_utils import public


@public
class ClientMaxedFilter(ResultTagFilterController):
    filter_arg = "client-maxed"
    index_arg = filter_arg
    label = "Contact maxed giving availability?"


@public
class ClientGivingCurrentFilter(ResultTagFilterController):
    filter_arg = "client-giving-current"
    index_arg = filter_arg
    label = "Gave in the current cycle"


@public
class ClientGivingPastFilter(ResultTagFilterController):
    filter_arg = "client-giving-past"
    index_arg = filter_arg
    label = "Gave in a previous cycle"
