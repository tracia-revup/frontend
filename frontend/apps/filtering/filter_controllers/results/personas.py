
from collections import Mapping

from frontend.apps.analysis.models import AnalysisAppliedFilter
from frontend.apps.personas.models import PersonaInstance
from frontend.libs.utils.general_utils import public
from ..base import ResultTagFilterController, ResultFilterController


@public
class PersonaFilter(ResultFilterController):
    filter_arg = 'personas'
    index_arg = filter_arg

    @classmethod
    def factory(cls, feature_config, analysis_config):
        controller = super().factory(feature_config, analysis_config)
        # Cache all Persona identifiers for the specific feature we are
        # attached to, for the account being run on to the controller instance
        # we return.
        controller.identifiers = list(PersonaInstance.objects.filter(
            account=analysis_config.account,
            persona__feature=feature_config.feature).values_list(
                'identifier', flat=True))
        return controller

    def get_filter_configs(self):
        """Store identifiers of analyzed Personas in AnalysisAppliedFilters"""
        if self.identifiers:
            return {'identifiers': self.identifiers}
        else:
            return super().get_filter_configs()

    def extract(self, result, feature_results):
        return {self.index_arg: {identifier: identifier in result.tags
                                 for identifier in self.identifiers}}

    def filter(self, filter_data, **kwargs):
        if isinstance(filter_data, str):
            filter_data = [filter_data]
        if not isinstance(filter_data, Mapping):
            filter_data = {tag: True for tag in filter_data}
        return {self.index_arg: filter_data}

    def get_field(self, user, filter_rec, account=None, **kwargs):
        filter_configs = getattr(self, 'applied_filter_configs', None)
        if not filter_configs:
            user_analysis = user.get_default_analysis()
            if not user_analysis:
                return None
            try:
                filter_configs = user_analysis.analysisappliedfilter_set.get(
                    filter=filter_rec).filter_configs
            except (AnalysisAppliedFilter.DoesNotExist,
                    AnalysisAppliedFilter.MultipleObjectsReturned):
                return None
        choices = filter_configs.get('identifiers')
        if not choices:
            return None
        return dict(label="Personas",
                    group="personas",
                    type="dropdown",
                    # TODO: need human-readable choices
                    choices=[(choice, choice) for choice in sorted(choices)],
                    expected=[
                        dict(field=self.filter_arg, type="text")
                    ])
