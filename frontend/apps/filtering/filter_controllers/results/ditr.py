
from ..base import ResultTagFilterController
from frontend.apps.analysis.analyses import signal_actions
from frontend.libs.utils.general_utils import public


@public
class DiamondsInTheRoughFilter(ResultTagFilterController):
    filter_arg = "ditr"
    index_arg = filter_arg
    label = "Diamonds In The Rough"


@public
class AcademicDiamondsInTheRoughFilter(ResultTagFilterController):
    filter_arg = signal_actions.AcademicDiamondsInTheRough.tag_name
    index_arg = filter_arg
    label = "Academic Diamonds In The Rough"


@public
class NonprofitDiamondsInTheRoughFilter(ResultTagFilterController):
    filter_arg = signal_actions.NonprofitDiamondsInTheRough.tag_name
    index_arg = filter_arg
    label = "Diamonds In The Rough"


@public
class HighPotentialDonorFilter(ResultTagFilterController):
    filter_arg = signal_actions.HighPotentialDonor.tag_name
    index_arg = filter_arg
    label = "High Potential Donor"
