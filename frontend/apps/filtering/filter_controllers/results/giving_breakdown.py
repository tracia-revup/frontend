
from django.contrib.humanize.templatetags.humanize import intcomma
from funcy import set_in

from frontend.apps.analysis.analyses.signal_actions import GivingBreakdownFilterReducer
from ..base import ResultTagFilterController, ResultFilterController
from frontend.libs.utils.general_utils import public


@public
class GivingBreakdownFilter(ResultFilterController):
    """Questions answerable by this filter:

    Number of donations over time
      - donation target
      - within last Y years
      - number of donations gte Z

    $ amount of donations over time
      - donation target
      - within last Y years
      - amount gte X

    total $ amount of donations
      - donation target
      - amount gte X
      - all time


    donation targets
    - my organization
    - all organizations
    - organizations similar to mine (correlated campaign/key contribs)

    year Y partitions
    - 1
    - 2
    - 3
    - 5
    - 10
    - 15
    - None

    dollar amount partitions
    - 500
    - 1k
    - 2k
    - 5k
    - 7.5k
    - 10k
    - 20k

    donation count partitions
    - 1
    - 2
    - 5
    - 10
    - 20
    - 30
    - 50
    - 100
    """
    filter_arg = "giving_breakdown"
    index_arg = filter_arg
    multiquestion = True

    SIGNAL_KEY = 'giving_overview'
    CLIENT_FEATURE_TYPE = 'Client Matches'
    TARGETS = {'client': 'my organization',
               'ally': 'organizations similar to mine',
               'all': 'all organizations'}
    YEAR_PARTITIONS = (1, 2, 3, 5, 10, 15, None)
    COUNT_PARTITIONS = (1, 2, 5, 10, 20, 30, 50, 100)
    DOLLAR_PARTITIONS = (500, 1000, 2000, 5000, 7500, 10000, 20000)

    def _extract_reduce(self, feature_results):
        """Searches feature results for giving overview data and creates
        intermediate data structure necessary for indexing.

        output data structure:
        $donation_target:
          $year_or_unbound:
            count: $num_donations
            amount: $amount
        """
        output = {target: {} for target in self.TARGETS}
        for feature_result in feature_results:
            if (not feature_result.signals or
                    'signal_sets' not in feature_result.signals):
                continue
            feature_title = feature_result.feature_config.feature.title
            # Determine whether the feature result contain's client data
            is_client = feature_title == self.CLIENT_FEATURE_TYPE
            for signal_set in feature_result.signals['signal_sets']:
                if self.SIGNAL_KEY in signal_set:
                    data = signal_set[self.SIGNAL_KEY]
                    GivingBreakdownFilterReducer.reduce(self.YEAR_PARTITIONS,
                                                        data, output=output,
                                                        is_client=is_client)
        return output

    def _extract_prep(self, data):
        """Take intermediate data structure and prepare it for indexing

        This transformation is necessary because jsonb indexes do not support
        comparison or range operations. So we have to reduce everything to a
        boolean question.

        incoming data structure:
        $donation_target:
          $year_or_unbound:
            count: $num_donations
            amount: $amount

        result data structure
        $donation_target:
          $year_or_unbound:
            count:
              $count_partition: bool
            amount:
              $amount_partition: bool
        """
        output = {}
        for target, year_data in data.items():
            output_year_data = output.setdefault(target, {})
            for year, details in year_data.items():
                count = details['count']
                amount = details['amount']
                counts = {count_part: count >= count_part
                          for count_part in self.COUNT_PARTITIONS}
                dollars = {dollar_part: amount >= dollar_part
                           for dollar_part in self.DOLLAR_PARTITIONS}
                output_year_data[year] = {'count': counts, 'amount': dollars}
        return output

    def extract(self, result, feature_results):
        data = self._extract_reduce(feature_results)
        output = self._extract_prep(data)
        return {self.index_arg: output}

    def filter(self, filter_data, **kwargs):
        """Convert incoming query to data struct matching indexed format

        Example of incoming query format:
        [
            {"donation_target": X, "giving_period": Y, "amount_partition": Z},
            {"donation_target": X, "giving_period": Y, "count_partition": Z},
            {"donation_target": X, "amount_partition": Z}
        ]

        We convert that into something that looks like:
        $donation_target:
          $year_or_unbound:
            count:
              $count_partition: bool
            amount:
              $amount_partition: bool

        Note: if `giving_period` is not present in the incoming query, we
        interpret that to mean perform an unbound search, which is represented
        by `None`.
        """
        qry = {}
        for filter_arg in filter_data:
            if 'amount_partition' in filter_arg:
                qry = set_in(qry,
                             [filter_arg['donation_target'],
                              filter_arg.get('giving_period'),
                              'amount',
                              filter_arg['amount_partition']],
                             True)
            if 'count_partition' in filter_arg:
                qry = set_in(qry,
                             [filter_arg['donation_target'],
                              filter_arg.get('giving_period'),
                              'count',
                              filter_arg['count_partition']],
                             True)
        return {self.index_arg: qry}

    def get_field(self, **kwargs):
        return [
            {
                "group": "giving_breakdown",
                "label": "Number of donations over time",
                "type": "multifield",
                "layout": "inline",
                "fields": [
                    {
                        "label": "Contacts who have made more than",
                        "id": "count_partition",
                        "type": "dropdown",
                        "choices": [(c, f'{str(c)} donation' if c == 1 else
                                    f'{str(c)} donations')
                                    for c in self.COUNT_PARTITIONS]
                    },
                    {
                        "label": "over the last",
                        "id": "giving_period",
                        "type": "dropdown",
                        "choices": [(year, f'{year} year' if year == 1 else
                                    f'{year} years')
                                    for year in self.YEAR_PARTITIONS if year]
                    },
                    {
                        "label": "to",
                        "id": "donation_target",
                        "type": "dropdown",
                        "choices": list(self.TARGETS.items())
                    }
                ],
                "expected": [
                    {
                        "field": "giving_breakdown",
                        "type": "object",
                        "fields": [
                            {"field": "donation_target", "type": "text"},
                            {"field": "giving_period", "type": "int"},
                            {"field": "count_partition", "type": "int"}
                        ]
                    }
                ]
            },
            {
                "group": "giving_breakdown",
                "label": "$ amount of donations over time",
                "type": "multifield",
                "layout": "inline",
                "fields": [
                    {
                        "label": "Contacts who have made more than",
                        "id": "amount_partition",
                        "type": "dropdown",
                        "choices": [(d, '$' + intcomma(d, False))
                                    for d in self.DOLLAR_PARTITIONS]
                    },
                    {
                        "label": "in total donations over the last",
                        "id": "giving_period",
                        "type": "dropdown",
                        "choices": [(year, f'{year} year' if year == 1 else
                                    f'{year} years')
                                    for year in self.YEAR_PARTITIONS
                                    if year]
                    },
                    {
                        "label": "to",
                        "id": "donation_target",
                        "type": "dropdown",
                        "choices": list(self.TARGETS.items())
                    }
                ],
                "expected": [
                    {
                        "field": "giving_breakdown",
                        "type": "object",
                        "fields": [
                            {"field": "donation_target", "type": "text"},
                            {"field": "giving_period", "type": "int"},
                            {"field": "amount_partition", "type": "int"}
                        ]
                    }
                ]
            },
            {
                "group": "giving_breakdown",
                "label": "Total $ amount of donations",
                "type": "multifield",
                "layout": "inline",
                "fields": [
                    {
                        "label": "Contacts whose total donations made to",
                        "id": "donation_target",
                        "type": "dropdown",
                        "choices": list(self.TARGETS.items())
                    },
                    {
                        "label": "is greater than or equal to",
                        "id": "amount_partition",
                        "type": "dropdown",
                        "choices": [(d, '$' + intcomma(d, False))
                                    for d in self.DOLLAR_PARTITIONS]
                    },
                ],
                "expected": [
                    {
                        "field": "giving_breakdown",
                        "type": "object",
                        "fields": [
                            {"field": "donation_target", "type": "text"},
                            {"field": "amount_partition", "type": "int"}
                        ]
                    }
                ]
            },
        ]
