
from ..base import ResultTagFilterController
from frontend.apps.analysis.analyses import signal_actions
from frontend.libs.utils.general_utils import public

GHTagNames = signal_actions.GivingHistoryTagging.TagNames


@public
class ClientGivingFilter(ResultTagFilterController):
    filter_arg = GHTagNames.client_giving
    index_arg = filter_arg
    label = "Has ever given to client"


@public
class ClientGivingThisYearFilter(ResultTagFilterController):
    filter_arg = GHTagNames.client_giving_this_year
    index_arg = filter_arg
    label = "Has given to client this year"


@public
class NonClientGivingFilter(ResultTagFilterController):
    filter_arg = GHTagNames.non_client_giving
    index_arg = filter_arg
    label = "Has ever given to other causes"


@public
class ClientLargestDonorFilter(ResultTagFilterController):
    filter_arg = signal_actions.ClientTopDonor.tag_name
    index_arg = filter_arg
    label = "Largest client donors"
