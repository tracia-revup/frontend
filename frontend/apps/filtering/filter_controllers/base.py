
from abc import ABCMeta, abstractmethod, abstractproperty
from collections import Sequence

from frontend.apps.analysis.analyses.base import ConfigurationWrapper
from frontend.libs.utils.string_utils import str_to_bool


class FilterControllerBase(object, metaclass=ABCMeta):
    multiquestion = False

    @abstractproperty
    def filter_arg(self):
        pass

    @abstractproperty
    def index_arg(self):
        pass

    @abstractmethod
    def extract(self, **kwargs):
        pass

    @abstractmethod
    def filter(self, filter_data, **kwargs):
        pass

    @abstractmethod
    def get_field(self, **kwargs):
        pass


# TODO: Tried to pre-optimize. Ended up not being what I needed.
# TODO: Possibly revisit after more filters are built
class FilterController(FilterControllerBase):
    def extract(self, **kwargs):
        try:
            index_item = self._extract(**kwargs)
        except AttributeError:
            return
        else:
            return {self.index_arg: index_item}

    def _extract(self, **kwargs):
        pass

    def filter(self, filter_data, **kwargs):
        return {self.index_arg: filter_data}

    @abstractmethod
    def get_field(self, **kwargs):
        pass


class ResultFilterController(FilterController):
    def __init__(self):
        super(ResultFilterController, self).__init__()
        self.applied_filter_configs = None

    @classmethod
    def factory(cls, feature_config, analysis_config):
        controller = cls()
        controller.feature_config = feature_config
        controller.config_wrapper = ConfigurationWrapper(
            analysis_config, feature_config)
        return controller

    def get_filter_configs(self):
        return {}


class ResultTagFilterController(ResultFilterController):
    @abstractproperty
    def label(self):
        pass

    def extract(self, result, feature_results):
        def _has_tag():
            try:
                return self.index_arg in result.tags
            except (TypeError, KeyError):
                return False
        return {self.index_arg: _has_tag()}

    def filter(self, filter_data, **kwargs):
        if not isinstance(filter_data, bool):
            if not isinstance(filter_data, str):
                if isinstance(filter_data, Sequence) and len(filter_data) == 1:
                    filter_data = filter_data[0]
                else:
                    # TODO: throw some kind of an error here.
                    pass
            filter_data = str_to_bool(filter_data)
        return {self.index_arg: filter_data}

    def get_field(self, **kwargs):
        return dict(label=self.label,
                    group="tags",
                    type="checkbox",
                    expected=[
                        dict(field=self.filter_arg, type="boolean"),
                    ])
