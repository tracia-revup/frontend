# Generated by Django 2.0.5 on 2019-02-26 15:49
from django.db import migrations


def migrate_np_filters(apps, schema_editor):
    Filter = apps.get_model("filtering", "Filter")
    QuickFilter = apps.get_model("filtering", "QuickFilter")
    AccountProfile = apps.get_model("campaign", "AccountProfile")

    nonprofit_q_filters = []

    # (filter_name(s), quick_filter.title, quick_filter.query)
    nonprofit_filter_data = (
        (('Client Giving',),
         'Contacts who gave to my organization',
         {'client-giving': True}
         ),
        (('Client Giving', 'Client Giving This Year',),
         'Contacts who gave to my organization previously, but not in the last'
         ' 12 months',
         {'client-giving': True, 'client-giving-this-year': False}
         ),
        (('Client Giving',),
         'Contacts who never gave to my organization',
         {'client-giving': False}
         ),
        (('Ally Giving',),
         'Contacts who gave to organizations similar to mine',
         {'ally-giving': True}
         ),
    )

    # Add the nonprofit quick filters
    for filter_names, q_filter_title, q_filter_query in nonprofit_filter_data:
        filters = []

        for filter_name in filter_names:
            f = Filter.objects.get(title=filter_name)
            filters.append(f)

        q, _ = QuickFilter.objects.get_or_create(
            title=q_filter_title,
            query=q_filter_query
        )

        # Clear any existing filters
        q.filters.clear()

        for f in filters:
            q.filters.add(f)
        nonprofit_q_filters.append(q)

    nonprofit_account_profile = AccountProfile.objects.filter(account_type='N')

    # Add the new quick filters to Nonprofit Account Profiles
    for ap in nonprofit_account_profile:
        ap.quick_filters.clear()
        ap.quick_filters.add(*nonprofit_q_filters)


class Migration(migrations.Migration):

    dependencies = [
        ('filtering', '0018_auto_20190206_0947'),
    ]

    operations = [
        migrations.RunPython(migrate_np_filters, lambda x, y: True)
    ]
