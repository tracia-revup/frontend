
from frontend.libs.utils.model_utils import DynamicClassModelMixin


def generate_filters(ImportConfig, Filter):
    default_config, _ = ImportConfig.objects.get_or_create(
        title="Default Import Config", account=None, user=None)

    dc = DynamicClassModelMixin()
    from frontend.apps.filtering.models import Filter as Fil
    FilterTypes = Fil.FilterTypes

    def _create_filter(klass, title, filter_type):
        dc.dynamic_class = klass
        # Use update_or_create so if class or module names are changed during
        # refactoring, the database records are kept in sync and no manual work
        # is required.
        filter_, _ = Filter.objects.update_or_create(
            title=title,
            defaults={
                'class_name': dc.class_name,
                'module_name': dc.module_name,
            },
            filter_type=filter_type
        )
        default_config.filters.add(filter_)

    # Create Import Source filter
    from frontend.apps.filtering.filter_controllers.contacts import imports

    _create_filter(imports.ImportSourceFilter, "Import Source",
                   FilterTypes.CONTACT)
    _create_filter(imports.ImportSourceDetailFilter, "Import Source Detail",
                   FilterTypes.CONTACT)
    _create_filter(imports.ImportDateFilter, "Import Date",
                   FilterTypes.CONTACT)

    # Create Gender filter
    from frontend.apps.filtering.filter_controllers.contacts import gender
    _create_filter(gender.GenderFilter, "Gender", FilterTypes.CONTACT)

    # Create Location filters
    from frontend.apps.filtering.filter_controllers.contacts import location
    _create_filter(location.CityFilter, "City", FilterTypes.CONTACT)
    _create_filter(location.StateFilter, "State", FilterTypes.CONTACT)
    _create_filter(location.ZipCodeFilter, "Zip Code", FilterTypes.CONTACT)
    _create_filter(location.AreaCodeFilter, "Area Code", FilterTypes.CONTACT)
    _create_filter(location.CountyFilter, "County", FilterTypes.CONTACT)
    _create_filter(location.MetroAreaFilter, "Metro Area", FilterTypes.CONTACT)

    # Create Name filters
    from frontend.apps.filtering.filter_controllers.contacts import name
    _create_filter(name.GivenNameFilter, "Given Name", FilterTypes.CONTACT)
    _create_filter(name.FamilyNameFilter, "Family Name", FilterTypes.CONTACT)

