
from django.contrib.postgres.fields import JSONField
from django.core import exceptions
from django.db import models, transaction
from django_extensions.db.models import TitleDescriptionModel

from frontend.apps.analysis.models import Result
from frontend.apps.contact_set.utils import ContactSetCategories
from frontend.libs.utils.general_utils import deep_join
from frontend.libs.utils.model_utils import (
    DynamicClassModelMixin, OwnershipModel, EntityModel, Choices)
from frontend.libs.third_party.choice_array_field import ChoiceArrayField


class IndexBase(models.Model):
    user = models.ForeignKey('authorize.RevUpUser',
                             on_delete=models.CASCADE,
                             related_name="+")
    attribs = JSONField()

    class Meta:
        abstract = True

    @classmethod
    def get_index(cls, **kwargs):
        try:
            index = cls.objects.get(**kwargs)
        except cls.DoesNotExist:
            index = None
        return index

    @classmethod
    def get_controllers(cls, filters):
        return (filter_.dynamic_class() for filter_ in filters)

    @classmethod
    def update_index(cls, filters, save=True, get_existing=True, **kwargs):
        index_parts = []
        for controller in cls.get_controllers(filters):
            index_parts.append(controller.extract(**kwargs) or {})
        # Merge nested index data
        index_data = deep_join(index_parts)

        index = cls.get_index(**kwargs) if get_existing else None

        # If an index already exists, and needs updating, we'll update it
        # with the new data
        if index and index_data != index.attribs:
            index.attribs = index_data
            if save:
                index.save(update_fields=['attribs'])
        # If an index doesn't exist, we'll only create one if there is data.
        # Empty data is a waste of a write.
        elif not index and index_data:
            kwargs["attribs"] = index_data
            if save:
                index = cls.objects.create(**kwargs)
            else:
                index = cls(**kwargs)
        # No data and no index, means nothing to do.
        else:
            index = index or None
        return index


class ContactIndex(IndexBase):
    contact = models.OneToOneField('contact.Contact',
                                   on_delete=models.CASCADE,
                                   related_name="index")

    @classmethod
    @transaction.atomic
    def bulk_update_index(cls, contacts, filters, save=True, **kwargs):
        # Create the indexes
        indexes = (
            cls.update_index(filters, save=False, get_existing=False,
                             contact=contact, user_id=contact.user_id,
                             **kwargs)
            for contact in contacts
        )
        if save:
            # It's more efficient to delete the indexes and recreate them in bulk
            cls.objects.filter(contact__in=contacts).delete()
            return ContactIndex.objects.bulk_create(indexes, batch_size=10000)
        else:
            return indexes


class ResultIndex(IndexBase):
    result = models.OneToOneField('analysis.Result',
                                  on_delete=models.CASCADE,
                                  related_name="index")

    # 'analysis' and 'partition' are here to help shrink the query size.
    # Otherwise, queries on the indexes take -forever-. There are not meant to
    # be used as real foreign keys. See migration 0006 for their index.
    analysis = models.ForeignKey('analysis.Analysis',
                                 on_delete=models.CASCADE,
                                 related_name="+",
                                 null=True,
                                 blank=True,
                                 db_index=False)
    partition = models.ForeignKey('analysis.PartitionConfig',
                                  on_delete=models.CASCADE,
                                  related_name="+",
                                  null=True,
                                  blank=True,
                                  db_index=False)


class Filter(EntityModel, TitleDescriptionModel, OwnershipModel,
             DynamicClassModelMixin):

    class FilterTypes(Choices):
        choices_map = [
            ("RS", "RESULT"),
            ("CN", "CONTACT"),
        ]

    filter_type = models.CharField(choices=FilterTypes.choices(),
                                   max_length=2, blank=False)
    contact_set_categories = ChoiceArrayField(models.CharField(
            choices=ContactSetCategories.choices(),
            max_length=10,
            blank=True,
        ),
        default=ContactSetCategories.all(),
        help_text="Decided which contexts this filter is visible for."
    )

    def delete(self, *args, **kwargs):
        """
        Block deletion of filter if the filter is being used by a quickfilter
        :return:
        """
        if QuickFilter.objects.filter(filter=self).exists():
            raise exceptions.ValidationError(
                "Cannot delete this filter, it is being used by other "
                "Quickfilters")
        else:
            super(Filter, self).delete(*args, **kwargs)


class QuickFilter(models.Model):
    """Allows users to build their own quick filters with the ability to save
    and delete
    """
    owner = models.ForeignKey('authorize.RevUpUser',
                              on_delete=models.CASCADE,
                              related_name="quick_filters",
                              null=True, blank=True)
    title = models.CharField(max_length=255)
    short_title = models.CharField(max_length=255)
    filters = models.ManyToManyField('filtering.Filter',
                                     blank=True,
                                     related_name='quick_filters')
    query = JSONField(blank=True, default={})
    order = models.IntegerField(null=True, blank=True)

    class Meta:
        ordering = ['order', ]

    def __str__(self):
        return "{} ({})".format(self.title, ' '.join(
            [f.title for f in self.filters.all()]
        ))

    @classmethod
    @transaction.atomic
    def create(cls, title, owner=None, filters=None):
        """ Adds the query for this filter and also adds the quick filter to
        the associated account profile
        """
        from frontend.apps.campaign.models import Account
        account = Account.objects.get(account_user=owner)

        q_filter = QuickFilter.objects.create(
            owner=owner, title=title, filters=filters)

        # Build the query for this quick_filter
        q_filter.query = q_filter.build_query()
        q_filter.save()

        # Get the account profile to save this filter to
        account_profile = account.account_profile

        # Set the quick_filter in the account profile
        account_profile.quick_filters.add(q_filter)

        return q_filter

    def build_query(self, query_on=Result):
        """
        Build the query for the quick filter.
        :param query_on:
        :return: query
        """
        from frontend.apps.filtering.query_engine import QueryEngine
        qengine = QueryEngine(self.filters.all(), query_on)
        query = qengine.filter(
            self.query, user=self.owner.id)

        return query
