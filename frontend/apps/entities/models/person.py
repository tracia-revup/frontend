"""
Models supporting person entities.
"""
from functools import reduce
import logging

from django.db import models

from frontend.libs.utils.model_utils import (Choices, TruncatingCharField,
                                             EntityModel, OwnershipModel)

LOGGER = logging.getLogger(__name__)


class PersonDimensionQuerySet(models.QuerySet):
    """Reusable filters for various types of person dimensions."""

    def aliases(self, names=None):
        """Filter personal alias dimensions. Optionally, filter only personal
        aliases with an identity matching one of a sequence of name tuples.

        Args:
            names: A sequence of (given_name, family_name) tuples

        Returns:
            A QuerySet of personal alias dimensions matching names, if provided
            or all alias dimensions if not.
        """
        q_list = []
        if names:
            for name in names:
                try:
                    given_name, family_name = name
                except TypeError:
                    LOGGER.error('"%s" could not be parsed for alias search',
                                 name)
                    continue
                q_list.append(models.Q(given_name__iexact=given_name,
                                       family_name__iexact=family_name))
            q_list = reduce(lambda a, b: a | b, q_list)
        if q_list:
            identities = Identity.objects.filter(
                q_list, dimension__dimension_type=PersonDimension.
                PersonDimensionTypes.ALIAS
            ).values('pk')
            return self.filter(identities__pk__in=identities
                               ).distinct().prefetch_related('identities')
        else:
            return self.filter(dimension_type=PersonDimension.
                               PersonDimensionTypes.ALIAS)


class IdentityQuerySet(models.QuerySet):
    """Reusable filters for various types of person dimensions."""

    def aliases(self, *names):
        """Filter personal alias dimensions. Optionally, filter only personal
        aliases with an identity matching one of a sequence of name tuples.

        Args:
            names:
                (family_name, given_name), ...
                (family_name, (given_names, ...)), ...

        Returns:
            A QuerySet of personal alias dimensions matching names, if provided
            or all alias dimensions if not.
        """
        assert names
        q_list = []
        # XXX: automagic lower-case?
        for family_name, given_names in names:
            if isinstance(given_names, str):
                given_names = [given_names]
            q_list.append(models.Q(
                dimension__identities__given_name__lower__in=given_names,
                dimension__identities__family_name__lower=family_name
            ))
        q_list = reduce(lambda a, b: a | b, q_list)
        return self.filter(
            q_list, dimension__dimension_type=PersonDimension.
            PersonDimensionTypes.ALIAS).distinct()


class PersonDimension(EntityModel, OwnershipModel):
    """Dimension describing a person and the person's identities.

    A person may have various dimensions, which describe or define a person as
    their identities are represented in various contexts. For example,
    identities indicated by contact records, user or system-maintained personal
    aliases, transactions from public records, etc.

    A PersonDimension may collect one or more identities representing a person
    in a particular context.
    """
    class PersonDimensionTypes(Choices):
        """Types of person dimensions."""
        choices_map = [
            ('ALIAS', 'ALIAS', 'Personal alias'),
            ('FMERGE', 'FORCE_MERGE', 'Force Merge')
        ]

    dimension_type = models.CharField(choices=PersonDimensionTypes.choices(),
                                      blank=True, max_length=16)
    label = models.CharField(max_length=256)

    @property
    def name(self):
        return self.label

    dimensions = PersonDimensionQuerySet.as_manager()


class Identity(EntityModel):
    """Identity representing a person.

    A person may have one or more identities representing them in various
    contexts.
    """

    objects = IdentityQuerySet.as_manager()

    dimension = models.ForeignKey(
        "PersonDimension", on_delete=models.CASCADE, blank=True, null=True, related_name='identities')
    given_name = TruncatingCharField(max_length=128)
    additional_name = TruncatingCharField(max_length=128, blank=True)
    family_name = TruncatingCharField(max_length=128)
    name_prefix = TruncatingCharField(max_length=128, blank=True)
    name_suffix = TruncatingCharField(max_length=128, blank=True)

    @property
    def name(self):
        if self.given_name or self.family_name:
            return "{} {}".format(self.given_name, self.family_name)
        else:
            return "{}: '{}'".format(self.__class__.__name__, self.pk)

    @classmethod
    def build_based_on_contact(cls, contact, dimension=None,
                               get_or_create=False):
        kwargs = {}
        if dimension is not None:
            kwargs['dimension'] = dimension
        if get_or_create:
            action = cls.objects.get_or_create
        else:
            action = cls
        return action(
            given_name=contact.first_name,
            additional_name=contact.additional_name,
            family_name=contact.last_name,
            name_prefix=contact.name_prefix,
            name_suffix=contact.name_suffix,
            **kwargs
        )


class ContactLink(EntityModel):
    class Meta:
        unique_together = (("contact", "dimension"),)

    dimension = models.ForeignKey(
        "PersonDimension", on_delete=models.CASCADE, blank=False, null=False, related_name='contact_links')
    contact = models.ForeignKey(
        "contact.Contact", on_delete=models.CASCADE, blank=False, null=False, related_name='contact_links')
