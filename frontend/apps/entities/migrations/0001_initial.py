# -*- coding: utf-8 -*-


from django.db import models, migrations
import audit_log.models.fields
import frontend.libs.utils.model_utils
import django_extensions.db.fields
import django.db.models.deletion
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('campaign', '0019_account_analysis_link'),
    ]

    operations = [
        migrations.CreateModel(
            name='Identity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('given_name', frontend.libs.utils.model_utils.TruncatingCharField(max_length=128)),
                ('additional_name', frontend.libs.utils.model_utils.TruncatingCharField(max_length=128, blank=True)),
                ('family_name', frontend.libs.utils.model_utils.TruncatingCharField(max_length=128)),
                ('name_prefix', frontend.libs.utils.model_utils.TruncatingCharField(max_length=128, blank=True)),
                ('name_suffix', frontend.libs.utils.model_utils.TruncatingCharField(max_length=128, blank=True)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_entities_identity_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PersonDimension',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('dimension_type', models.CharField(blank=True, max_length=16, choices=[('ALIAS', 'Personal alias')])),
                ('label', models.CharField(max_length=256)),
                ('account', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to='campaign.Account', null=True)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_entities_persondimension_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('modified_by', audit_log.models.fields.LastUserField(related_name='modified_entities_persondimension_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by')),
                ('user', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.PROTECT, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='identity',
            name='dimension',
            field=models.ForeignKey(related_name='identities', blank=True, to='entities.PersonDimension', null=True, on_delete=django.db.models.deletion.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='identity',
            name='modified_by',
            field=audit_log.models.fields.LastUserField(related_name='modified_entities_identity_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by'),
            preserve_default=True,
        ),
    ]
