
from itertools import dropwhile
from logging import getLogger

from frontend.apps.contact.analysis.remerge import merge_retry_wrapper
from frontend.apps.contact.models import Contact
from frontend.apps.entities.models import PersonDimension, ContactLink
from frontend.apps.contact.analysis import remerge
from frontend.apps.contact.analysis.contact_import.base import ContactImportBase

LOGGER = getLogger(__name__)


@merge_retry_wrapper(raise_on_failure=True)
def create_force_merge(contacts, use_name_expansion=False,
                       use_alias_expansion=True, index_filters=None):
    """Helper utility to create a force merge on a set of contacts

    **NOTE** This method works best when it is working on child contacts.
    Creating a force merge between merge parents is still possible, and
    sometimes can even be useful, but it isn't that strong of a rule, and can
    be broken unintentionally.

    Creates all necessary database records to force the merge
    (`PersonDimension` & `ContactLink`). If some of the contacts we are
    creating the force merge on already have a `ContactLink`, we reuse the
    `PersonDimension` and `ContactLink` records. If the contacts are spread
    across multiple `PersonDimension` records, we move all `ContactLink`
    children into the oldest `PersonDimension` record, and delete extra
    `PersonDimension` records.

    Once the force merge records have been created we run merging on that
    contact.
    """
    assert len({c.user_id for c in contacts}) == 1
    assert len(contacts) > 1
    contacts = list(contacts)
    for contact in contacts:
        contact.refresh_from_db()

    user = contacts[0].user
    needs_link = contacts
    dimensions = list(PersonDimension.dimensions.filter(
        dimension_type=PersonDimension.PersonDimensionTypes.FORCE_MERGE,
        user=user, contact_links__contact__in=contacts).distinct(
            'id').order_by('id'))
    if not dimensions:
        # NOTE: it is important that the dimension_type and user are both set
        # on force merge PersonDimensions. We rely upon this in several queries
        # when looking for force merges for a user.
        dimension = PersonDimension.dimensions.create(
            user=user,
            dimension_type=PersonDimension.PersonDimensionTypes.FORCE_MERGE,
            label="new force merge user_id: {}, contact ids: {}".format(
                user.id, [c.id for c in contacts])[:255])
    else:
        dimension = dimensions[0]
        if len(dimensions) > 1:
            dimensions = dimensions[1:]
            ContactLink.objects.filter(dimension__in=dimensions).update(
                dimension=dimension)
            PersonDimension.dimensions.filter(
                id__in=[pd.id for pd in dimensions]).delete()
        existing_links = set(dimension.contact_links.values_list(
            'contact', flat=True))
        needs_link = [c for c in contacts if c.id not in existing_links]
    if needs_link:
        ContactLink.objects.bulk_create(
            [ContactLink(contact=contact, dimension=dimension)
             for contact in needs_link])
    try:
        merge_base = next(dropwhile(lambda c: len(c.first_name_lower) < 2,
                               contacts))
    except StopIteration:
        merge_base = dimension.contact_links.filter(
            first_name_lower__length__gte=2).first()
        if merge_base is None:
            # Seems unlikely that a user would create a force merge between
            # only contacts with a first initial, but whatever.
            merge_base = ContactImportBase._find_alt_base(contacts[0])
    if merge_base is None:
        # If we didn't find a contact with a full name to use for locating
        # merge siblings, then we decline to do anything else. We will not
        # create a merge of first-initial only contacts, or create a merge
        # contact that only has a first initial for a name.
        LOGGER.warn("Not performing any merges for the PersonDimension {}. "
                    "We were unable to locate a full-name contact".format(
                        dimension))
        return
    merge_candidates = ContactImportBase._find_siblings(
        merge_base, use_name_expansion=use_name_expansion,
        use_alias_expansion=use_alias_expansion)
    contact_ids = remerge.process_contacts(
        [merge_base] + list(merge_candidates), index_filters=index_filters)
    return Contact.objects.filter(id__in=contact_ids, needs_attention=False)
