from django.contrib import admin
from django.db.models import Q

from frontend.apps.entities.models import Identity, PersonDimension, ContactLink
from frontend.libs.utils.admin_utils import (ChoiceLimitAdminForm,
                                             ChoiceLimitAdminInlineFormSet)


class ContactLinkAdminInline(admin.TabularInline):
    model = ContactLink
    extra = 1
    raw_id_fields = ("contact",)
    formset = ChoiceLimitAdminInlineFormSet

    class form(ChoiceLimitAdminForm):
        choice_limit_filter_map = {
            'contact': lambda dimension: Q(user_id=dimension.user_id),
        }

        def get_parent_instance(self):
            if self.instance.dimension_id:
                return self.instance.dimension


class IdentityAdminInline(admin.TabularInline):
    model = Identity
    extra = 1
    verbose_name_plural = "Identities"


class PersonDimensionAdmin(admin.ModelAdmin):
    inlines = (IdentityAdminInline, ContactLinkAdminInline)
    search_fields = ('label',)


admin.site.register(PersonDimension, PersonDimensionAdmin)
