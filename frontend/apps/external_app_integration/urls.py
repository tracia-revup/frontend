from django.conf.urls import url
from .views import ContactImportNB, nb_access_token

urlpatterns = [
    url(r'^nationbuilder/access_token/$', nb_access_token, name='nb-access-token'),
    url(r'^nationbuilder/contact_import/$', ContactImportNB.as_view(), name='nb-contact-import'),
]
