import requests
from urllib.error import HTTPError
from urllib.parse import urlencode
from django.contrib.auth.decorators import login_required
from django.http import (HttpResponse, HttpResponseForbidden,
                         HttpResponseRedirect)
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.http import require_http_methods
from django.views.generic import View
from rest_framework import exceptions
from frontend.apps.external_app_integration.models import ExternalApp
from frontend.apps.contact.task_utils import start_nb_contact_import
from frontend.apps.contact.utils import check_account_level_import
from frontend.libs.utils.string_utils import str_to_bool
from frontend.libs.utils.task_utils import (
    WaffleFlagsMixin, CONTACT_IMPORT_FLAGS, ANALYSIS_FLAGS)
from frontend.libs.utils.tracker_utils import action_send


def valid_token(nation):
    """This function checks if the oauth_token is valid
    :param nation: an instance of ExternalApp
    """
    if nation and nation.oauth_token:
        people_count_url = ('https://{nation_slug}.nationbuilder.com/api/v1/'
                            'people/count?access_token={access_token}').format(
            nation_slug=nation.external_id, access_token=nation.oauth_token)
        try:
            response = requests.get(url=people_count_url)
        except HTTPError:
            response = None
        if response and response.status_code == 200:
            return True
    return False


@require_http_methods(["GET"])
@login_required()
def nb_access_token(request, *args, **kwargs):
    """
    Triggers python_social_auth pipeline if a token doesn't already exist
    for the user
    Returns a HttpResponse or triggers a contact import oif token already
    exists for the user
    """
    request_params = request.GET

    # request must have nation_slug parameter
    nation_slug = request_params.get('nation_slug', None)
    if not nation_slug:
        return HttpResponse('Please pass the parameter "nation_slug"',
                            status=400)

    account_contacts = str_to_bool(request_params.get('account_contacts',
                                                      None))
    if account_contacts:
        user = request.user.current_seat.account.account_user
    else:
        user = request.user

    import_contacts = str_to_bool(request_params.get('import_contacts', None))

    # Get the oauth token from revup db if it exists
    try:
        nation = user.external_apps.filter(
            app=ExternalApp.App.NATIONBUILDER,
            external_id=nation_slug).latest('updated')
    except ExternalApp.DoesNotExist:
        nation = None

    if valid_token(nation):
        # oauth token exists in revup db. Import contacts or notify the user
        # that authorization was already successful
        if import_contacts:
            request_params = request_params.dict()
            request_query = urlencode(request_params)
            return HttpResponseRedirect(reverse('nb-contact-import') + '?' + \
                                        request_query)
        return HttpResponse('{0} already authorized'.format(nation_slug),
                            status=200)
    else:
        # Token does not exist. Redirect the request to python_social_auth
        # Build the url
        url_ = reverse('social:begin', kwargs={'backend': 'nationbuilder'}) \
               + '?nation_slug={nation_slug}&' \
                 'account_contacts={account_contacts}&' \
                 'import_contacts={import_contacts}'.format(
            nation_slug=nation_slug,
            account_contacts=account_contacts, import_contacts=import_contacts)
        return HttpResponseRedirect(url_)


class ContactImportNB(View, WaffleFlagsMixin):
    """This view triggers a nationbuilder contact import task if a valid
    token exists for the user.
    """
    flag_names = CONTACT_IMPORT_FLAGS + ANALYSIS_FLAGS

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        request_params = request.POST
        # Request must have nation_slug parameter
        nation_slug = request_params.get('nation_slug', None)
        if not nation_slug:
            return HttpResponse('"nation_slug" parameter was not provided',
                                status=400)
        source = 'nationbuilder'

        # Is this an account-level import
        try:
            account = check_account_level_import(request)
        except exceptions.PermissionDenied as err:
            return HttpResponseForbidden(str(err))

        if account:
            user = account.account_user
        else:
            user = request.user

        # Retrieve token from revup database
        try:
            nation = user.external_apps.filter(
                app=ExternalApp.App.NATIONBUILDER,
                external_id=nation_slug).latest('updated')
        except ExternalApp.DoesNotExist:
            nation = None

        # If a token doesn't already exist or the existing token is invalid,
        # generate a new token
        if not valid_token(nation):
            request_params = request_params.dict()
            request_params['import_contacts'] = True
            request_query = urlencode(request_params)
            return HttpResponseRedirect(reverse('nb-access-token') + '?' +
                                        request_query)

        # Parameter request.user is not a typo. Import task handles the
        # assignment of contacts to the account_user instead of request.user
        # for account level imports
        start_nb_contact_import(request.user, account, source, nation_slug,
                                self._prepare_flags(request))
        action_send(request, request.user, verb="imported contacts",
                    source=source)
        return HttpResponse('Started import contacts from {0}'.format(source))

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        request_params = request.GET
        # Request must have nation_slug parameter
        nation_slug = request_params.get('nation_slug', None)
        if not nation_slug:
            return HttpResponse('"nation_slug" parameter was not provided',
                                status=400)
        source = 'nationbuilder'

        # Is this an account-level import
        try:
            account = check_account_level_import(request)
        except exceptions.PermissionDenied as err:
            return HttpResponseForbidden(str(err))

        if account:
            user = account.account_user
        else:
            user = request.user

        # Retrieve token from revup database
        try:
            nation = user.external_apps.filter(
                app=ExternalApp.App.NATIONBUILDER,
                external_id=nation_slug).latest('updated')
        except ExternalApp.DoesNotExist:
            nation = None

        # If a token doesn't already exist or the existing token is invalid,
        # generate a new token
        if not valid_token(nation):
            request_params = request_params.dict()
            request_params['import_contacts'] = True
            request_query = urlencode(request_params)
            return HttpResponseRedirect(reverse('nb-access-token') + '?' +
                                        request_query)

        # Parameter request.user is not a typo. Import task handles the
        # assignment of contacts to the account_user instead of request.user
        # for account level imports
        start_nb_contact_import(request.user, account, source, nation_slug,
                                self._prepare_flags(request))
        action_send(request, request.user, verb="imported contacts",
                    source=source)
        return HttpResponse('Started import contacts from {0}'.format(source))
