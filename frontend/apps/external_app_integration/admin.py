from django.contrib import admin
from .models import ExternalApp


class ExternalAppAdmin(admin.ModelAdmin):
    readonly_fields = ('id', 'account_title', 'revup_user', 'app',
                       'external_id', 'oauth_token', 'created', 'updated')
    list_display = ('id', 'account_title', 'app', 'external_id', 'updated')
    list_display_links = ('id', 'external_id',)
    list_filter = ('app',)
    list_select_related = ('revup_user',)
    ordering = ('-updated',)
    search_fields = ['revup_user__account_user__organization_name',
                     'external_id']

    def account_title(self, obj):
        return obj.revup_user.account_user


admin.site.register(ExternalApp, ExternalAppAdmin)