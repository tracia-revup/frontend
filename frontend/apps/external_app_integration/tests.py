from unittest import mock
from urllib.parse import urlencode

from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse

from frontend.apps.authorize.test_utils import AuthorizedUserTestCase
from frontend.apps.external_app_integration.models import ExternalApp


def patch_python_social_redirect(*args, **kwargs):
    """Patches the call to python_social_auth. Returns custom response if
    the request is a redirect to python_social_auth. Else redirect the request
    as is
    """
    if args[0].startswith('/login/nationbuilder'):
        return HttpResponse('starting python_social_auth pipeline', status=200)
    return HttpResponseRedirect(*args, **kwargs)


class ExternalAppViewTests(AuthorizedUserTestCase):
    def setUp(self):
        super(ExternalAppViewTests, self).setUp()
        self.account_user = self.user.current_seat.account.account_user
        self.app1, _ = ExternalApp.objects.get_or_create(
            external_id='test_slug_1', revup_user=self.user,
            app=ExternalApp.App.NATIONBUILDER, oauth_token='test_token_1')
        self.app2, _ = ExternalApp.objects.get_or_create(
            external_id='test_slug_2', revup_user=self.account_user,
            app=ExternalApp.App.NATIONBUILDER, oauth_token='test_token_2')
        self.contact_import_url = reverse('nb-contact-import')
        self.access_token_url = reverse('nb-access-token')

    @mock.patch('frontend.apps.external_app_integration.views.valid_token')
    @mock.patch(('frontend.apps.external_app_integration.views.'
                 'start_nb_contact_import'), side_effect=mock.MagicMock())
    @mock.patch('frontend.apps.external_app_integration.views.action_send',
                side_effect=mock.MagicMock())
    def test_contact_import_1(self, action_send_m, start_nb_contact_import_m,
                              valid_token_m):
        """Test nb-contact-import view when token already exists in
        db. Verify that start_nb_contact_import task was called
        """
        self._login()
        nation_slug = self.app1.external_id
        data = {'nation_slug': nation_slug}
        # Patch valid_token method's return value to True
        valid_token_m.return_value = True

        # DO a POST on nb-contact-import
        response = self.client.post(self.contact_import_url, data=data)

        # Verify that the methods: valid_token_m, start_nb_contact_import task
        # and action_send_m were called once
        valid_token_m.assert_called_once_with(self.app1)
        start_nb_contact_import_m.assert_called_once()
        action_send_m.assert_called_once()
        # Verify that the response status code is 200
        self.assertEqual(200, response.status_code)

    @mock.patch('frontend.apps.external_app_integration.views.'
                'HttpResponseRedirect',
                side_effect=patch_python_social_redirect)
    @mock.patch('frontend.apps.external_app_integration.views.valid_token')
    @mock.patch(('frontend.apps.external_app_integration.views.'
                 'start_nb_contact_import'), side_effect=mock.MagicMock())
    @mock.patch('frontend.apps.external_app_integration.views.action_send',
                side_effect=mock.MagicMock())
    def test_contact_import_2(self, action_send_m, start_nb_contact_import_m,
                              valid_token_m, python_social_redirect_m):
        """Test contact import when the existing token is not valid. Verify
        that the nb-contact-import view redirects to the
        nb-access-token view
        """
        self._login()
        nation_slug = self.app1.external_id
        data = {'nation_slug': nation_slug}
        query_params = '?nation_slug={0}&import_contacts=True'.format(
            nation_slug)
        # Patch valid_token method's return value to False
        valid_token_m.return_value = False

        # DO a POST on nb-contact-import
        response = self.client.post(self.contact_import_url, data=data,
                                    follow=True)

        # Verify that the request was redirected to nb-access-token view which
        # redirected the request to trigger python_social_auth pipeline
        self.assertRedirects(response, self.access_token_url+query_params,
                             target_status_code=200)
        # Verify that valid_token was called twice and that the methods:
        # start_nb_contact_import and action_send were not called
        self.assertEqual(2, valid_token_m.call_count)
        valid_token_m.assert_called_with(self.app1)
        start_nb_contact_import_m.assert_not_called()
        action_send_m.assert_not_called()
        # Verify that the response code is 200
        self.assertEqual(200, response.status_code)
        self.assertEqual(b'starting python_social_auth pipeline',
                         response.content)

    @mock.patch('frontend.apps.external_app_integration.views.'
                'HttpResponseRedirect', side_effect=patch_python_social_redirect)
    @mock.patch(('frontend.apps.external_app_integration.views.'
                 'start_nb_contact_import'), side_effect=mock.MagicMock())
    @mock.patch('frontend.apps.external_app_integration.views.action_send',
                side_effect=mock.MagicMock())
    def test_contact_import_3(self, action_send_m, start_nb_contact_import_m,
                              http_response_redirect_m):
        """Test contact import when no token exists in db. Verify that the
        request got redirected to nb-access-token view
        """
        self._login()
        nation_slug = 'test_slug_new'
        data = {'nation_slug': nation_slug}
        query_params = '?nation_slug={0}&import_contacts=True'.format(
            nation_slug)

        # DO a POST on nb-contact-import
        response = self.client.post(self.contact_import_url, data=data,
                                    follow=True)
        # Verify that the request was redirected to nb-access-token view which
        # redirected the request to trigger python_social_auth pipeline
        self.assertRedirects(response, self.access_token_url+query_params)
        # Verify that valid_token was called twice and that the methods:
        # start_nb_contact_import and action_send were not called
        start_nb_contact_import_m.assert_not_called()
        action_send_m.assert_not_called()
        # Verify that the response code is 200
        self.assertEqual(200, response.status_code)
        self.assertEqual(b'starting python_social_auth pipeline',
                         response.content)

    @mock.patch('frontend.apps.external_app_integration.views.valid_token')
    @mock.patch(('frontend.apps.external_app_integration.views.'
                 'start_nb_contact_import'), side_effect=mock.MagicMock())
    @mock.patch('frontend.apps.external_app_integration.views.action_send',
                side_effect=mock.MagicMock())
    def test_contact_import_4(self, action_send_m, start_nb_contact_import_m,
                              valid_token_m):
        """Test account-level contact import for regular user"""
        self._login()
        nation_slug = self.app1.external_id
        data = {'nation_slug': nation_slug}
        # Verify that the user is a regular user
        self.assertFalse(self.user.is_account_admin() or self.user.is_admin)
        # Patch valid_token method's return value to True
        valid_token_m.return_value = True
        request_params = '?account_contacts=True'

        # DO a POST on nb-contact-import
        response = self.client.post(self.contact_import_url + request_params,
                                    data=data)
        # Verify that the response status code is 403 as the user is a regular
        # user with no admin permisssions
        self.assertEqual(403, response.status_code)
        # Verify that the methods: valid_token, start_nb_contact_import and
        # action_send were not called
        valid_token_m.assert_not_called()
        start_nb_contact_import_m.assert_not_called()
        action_send_m.assert_not_called()

    @mock.patch('frontend.apps.external_app_integration.views.valid_token')
    @mock.patch(('frontend.apps.external_app_integration.views.'
                 'start_nb_contact_import'), side_effect=mock.MagicMock())
    @mock.patch('frontend.apps.external_app_integration.views.action_send',
                side_effect=mock.MagicMock())
    def test_contact_import_5(self, action_send_m, start_nb_contact_import_m,
                              valid_token_m):
        """Test account-level contact import for account admin"""
        self._login_and_add_admin_group()
        nation_slug = self.app1.external_id
        data = {'nation_slug': nation_slug}
        # Verify that the user is an account admin
        self.assertFalse(self.user.is_admin)
        self.assertTrue(self.user.is_account_admin())
        # Patch valid_token method's return value to True
        valid_token_m.return_value = True
        request_params = '?account_contacts=True'

        # DO a POST on nb-contact-import
        response = self.client.post(self.contact_import_url + request_params,
                                    data=data)
        # Verify that the response status code is 403 as the user is a regular
        # user with no admin permisssions
        self.assertEqual(200, response.status_code)
        self.assertEqual(b'Started import contacts from nationbuilder',
                         response.content)
        # Verify that the methods: valid_token, start_nb_contact_import and
        # action_send were called once
        valid_token_m.assert_called_once()
        start_nb_contact_import_m.assert_called_once()
        action_send_m.assert_called_once()

    @mock.patch('frontend.apps.external_app_integration.views.valid_token',
                return_value=True)
    def test_get_access_token_1(self, valid_token_m):
        """Test the case where oauth token already exists and contacts need
        not be imported
        """
        self._login()
        nation_slug = self.app1.external_id
        data = {'nation_slug': nation_slug}

        # Do a GET on nb-access-token
        response = self.client.get(self.access_token_url, data=data)

        # Verify that valid_token was called once
        valid_token_m.assert_called_once_with(self.app1)
        # Verify that the response status code is 200
        self.assertEqual(200, response.status_code)
        self.assertEqual('{0} already authorized'.format(nation_slug),
                         response.content.decode('utf-8'))

    @mock.patch('frontend.apps.external_app_integration.views.valid_token')
    @mock.patch(('frontend.apps.external_app_integration.views.'
                 'start_nb_contact_import'), side_effect=mock.MagicMock())
    @mock.patch('frontend.apps.external_app_integration.views.action_send',
                side_effect=mock.MagicMock())
    def test_get_access_token_2(self, action_send_m,
                                start_nb_contact_import_m, valid_token_m):
        """Test the case where oauth token already exists and contacts are to
        be imported
        """
        self._login()
        nation_slug = self.app1.external_id
        data = {'nation_slug': nation_slug, 'import_contacts': True}
        query_params = '?' + urlencode(data)
        # Patch valid_token method's return value to True
        valid_token_m.return_value = True

        # Do a GET on nb-access-token
        response = self.client.get(self.access_token_url, data=data,
                                   follow=True)

        # Verify that the request was redirected to
        # nb-contact-import view and that the methods:
        # start_nb_contact_import, action_send and valid_token were called
        self.assertRedirects(response, self.contact_import_url+query_params,
                             target_status_code=200)
        # Verify that valid_token was called twice
        self.assertEqual(2, valid_token_m.call_count)
        valid_token_m.assert_called_with(self.app1)
        start_nb_contact_import_m.assert_called_once()
        action_send_m.assert_called_once()
        # Verify that the response status code is 200
        self.assertEqual(200, response.status_code)
