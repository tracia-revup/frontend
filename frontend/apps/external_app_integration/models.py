from django.db import models
from frontend.libs.utils.model_utils import Choices


class ExternalApp(models.Model):
    class App(Choices):
        choices_map = [
            ("NB", "NATIONBUILDER"),
        ]
    external_id = models.CharField(max_length=255)
    revup_user = models.ForeignKey('authorize.RevUpUser',
                                   on_delete=models.CASCADE,
                                   related_name='external_apps')
    oauth_token = models.CharField(max_length=255)
    app = models.CharField(max_length=2, choices=App.choices())
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.app + '_' + self.external_id
