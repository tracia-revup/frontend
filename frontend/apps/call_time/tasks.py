
from celery import task
from django.core.files.base import ContentFile
from django.db import transaction

from frontend.apps.authorize.models import RevUpUser
from frontend.apps.batch_jobs.models import BatchFile
from frontend.apps.batch_jobs.utils import select_storage, send_success_email
from frontend.apps.call_time.call_sheet import BatchCallSheet
from frontend.apps.call_time.models import CallTimeContactSet, CallTimeContact
from frontend.apps.contact.models import Contact
from frontend.apps.contact_set.models import ContactSet
from frontend.libs.utils.celery_utils import update_progress



@task(max_retries=20, default_retry_delay=60, queue='batch_call_sheet')
def generate_batch_call_sheet_task(query, model, user_id, email_to=None,
                                   filename=None):
    user = RevUpUser.objects.get(id=user_id)
    queryset = model.objects.all()
    queryset.query = query
    batch_generator = BatchCallSheet.factory(queryset, user,
                                             # TODO: Remove with email function
                                             email_to)
    return batch_generator.execute(filename=filename)


@task(max_retries=20, default_retry_delay=60, queue='batch_call_sheet')
def _parallel_generate_batch_call_sheet(batch_generator, partial_filename,
                                        slice_begin, slice_end):
    """Generate a PDF file from a slice of the original queryset.
     Store each partial PDF in S3, to be reconstructed in 'complete'.

     These are measures to prevent both OOM and timeout errors.
    """
    with batch_generator.handle_exception():
        partial_pdf_file = batch_generator.parallel_run(slice_begin, slice_end)
        # Write the pdf to a string buffer and save it
        storage = select_storage()
        storage.save(partial_filename, partial_pdf_file)


@task(max_retries=20, default_retry_delay=60, queue='batch_call_sheet')
def _parallel_complete_batch_call_sheet(batch_generator, batch_count,
                                        filename):
    with batch_generator.handle_exception():
        storage = select_storage()
        partials = []
        # Read all of the partial pdfs from file to be reconstituted
        for i in range(batch_count):
            partial_filename = batch_generator.generate_partial_filename(i)
            partial_pdf = storage.open(partial_filename)
            partials.append(partial_pdf)

        # Recombine the partials into one document and save it
        pdf_file = batch_generator.parallel_complete(partials)
        cf = ContentFile(pdf_file.getvalue(), name="{}_{}.pdf".format(
                         batch_generator.user.id, batch_generator.batch_key))
        batch_file_record = BatchFile.objects.create(
            user=batch_generator.user,
            file_name=filename or "Revup_call_sheets.pdf",
            file_type=BatchFile.FileType.BATCH_CALL_SHEET,
            data_file=cf)

        # Send the email
        if batch_generator.email_to:
            send_success_email(batch_file_record, batch_generator.email_to,
                               "Call Sheets")

        # Clean up the partials
        for partial in partials:
            storage.delete(partial.name)


@task(max_retries=20, default_retry_delay=60)
def add_contact_set_to_call_group(contact_set_id, call_group_id):
    update_progress(1, 100)
    contact_set = ContactSet.objects.active().get(id=contact_set_id)
    with transaction.atomic():
        # Create CallTimeContacts from each contact in the ContactSet
        # We'll need to ensure the CTC doesn't already exist though
        contact_ids = contact_set.queryset().distinct(
            "contact").values_list("contact_id", flat=True)
        # Build a set of contact IDs that already have a CTC.
        ctc_cids = set(CallTimeContact.objects.filter(
                contact__in=contact_ids).values_list("contact_id", flat=True))
        update_progress(20, 100)

        # Iterate over each contact and make a CallTimeContact for it
        new_ctcs = [
            CallTimeContact(contact_id=c, account_id=contact_set.account_id)
            for c in contact_ids if c not in ctc_cids
        ]
        update_progress(40, 100)
        CallTimeContact.objects.bulk_create(new_ctcs)
        update_progress(60, 100)

        # Add the CTCs to the CallGroup
        call_group = CallTimeContactSet.objects.active().select_for_update() \
                                                        .get(id=call_group_id)
        # We need to requery the CallTimeContacts so we have their IDs, then
        # add them to the group
        call_group.add_queryset(
            CallTimeContact.objects.filter(contact_id__in=contact_ids))
        update_progress(100, 100)
