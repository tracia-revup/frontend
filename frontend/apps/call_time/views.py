
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from frontend.apps.campaign.routes import ROUTES
from frontend.apps.role.decorators import any_group_required
from frontend.apps.role.permissions import AdminPermissions
from frontend.libs.django_view_router import RoutingTemplateView


class CallTimeManagerView(RoutingTemplateView):
    routes = ROUTES
    template_name = 'call_time/manager.html'

    def get_context_data(self, **kwargs):
        context = super(CallTimeManagerView, self).get_context_data(**kwargs)
        context['account'] = self.request.user.current_seat.account
        return context

    @method_decorator(login_required)
    @method_decorator(any_group_required(AdminPermissions.MODIFY_CALLTIME,
                                         AdminPermissions.MODIFY_CALLGROUPS))
    def get(self, request, *args, **kwargs):
        return super(CallTimeManagerView, self).get(request, *args, **kwargs)