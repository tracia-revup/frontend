import logging

from celery.utils import uuid
from django.conf import settings
from django.core.paginator import InvalidPage
from django.db import IntegrityError, models, transaction
from django.shortcuts import get_object_or_404, get_list_or_404, Http404
from rest_condition.permissions import And
from rest_framework import permissions, viewsets, exceptions, response, filters
from rest_framework.decorators import action
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework_extensions.mixins import (
    NestedViewSetMixin, DetailSerializerMixin)
from rest_framework_extensions.utils import compose_parent_pk_kwarg_name

from frontend.apps.analysis.api.mixins import CSVExportMixin
from frontend.apps.authorize.api.permissions import UserIsRequester
from frontend.apps.authorize.models import RevUpUser, RevUpTask
from frontend.apps.call_time.call_sheet import CallSheet
from frontend.apps.call_time.models import (
    CallTimeContact, CallTimeContactSet, CallLog, CallTimeSeat,
    CallGroupQuotaExceededError)
from frontend.apps.call_time.tasks import (
    generate_batch_call_sheet_task, add_contact_set_to_call_group)
from frontend.apps.campaign.models import Account
from frontend.apps.contact.models import (
    Contact, PhoneNumber, EmailAddress, ContactNotes)
from frontend.apps.contact.api.serializers import ContactNotesSerializer
from frontend.apps.contact_set.models import ContactSet
from frontend.apps.role.api.permissions import HasPermission
from frontend.apps.role.permissions import AdminPermissions, PermissionError
from frontend.apps.seat.models import Seat
from frontend.apps.seat.api.permissions import UserOwnsSeat
from frontend.libs.utils.api_utils import (
    AutoinjectUrlParamsIntoDataMixin, InitialMixin, exception_watcher,
    NameSearchMixin, ViewMethodChecker, APIException, PrintTemplateRenderer,
    HttpResponseNoContent)
from frontend.libs.utils.string_utils import str_to_bool
from .serializers import (
    CallTimeContactSerializer, CallTimeCallGroupSerializer, CallLogSerializer,
    DetailedCallTimeContactSerializer, CallGroupContactSerializer,
    CallGroupSerializer)


LOGGER = logging.getLogger(__name__)


class CallTimeContactViewSet(NestedViewSetMixin, DetailSerializerMixin,
                             InitialMixin, NameSearchMixin,
                             viewsets.ModelViewSet):
    serializer_class = CallTimeContactSerializer
    serializer_detail_class = DetailedCallTimeContactSerializer
    parent_account_lookup = compose_parent_pk_kwarg_name('account_id')
    name_search_prefix = "contact"
    permission_classes = (
        And(permissions.IsAuthenticated,
            HasPermission(parent_account_lookup,
                          AdminPermissions.MODIFY_CALLTIME)
            ),)
    queryset = CallTimeContact.objects.select_related("contact").active() \
                              .prefetch_related(models.Prefetch(
                                "call_time_contact_sets",
                                queryset=CallTimeContactSet.objects.active()))\
                              .order_by("-created")

    class pagination_class(PageNumberPagination):
        page_size = 20
        page_size_query_param = 'page_size'
        max_page_size = 100

    def filter_queryset(self, queryset):
        query = self._prepare_name_filter()
        return queryset.filter(query)

    def get_serializer_context(self):
        context = super(CallTimeContactViewSet, self).get_serializer_context()
        context['account'] = self.account
        context.update(
            DetailedCallTimeContactSerializer.prepare_view_context(self))
        return context

    def get_renderers(self):
        renderers_ = super(CallTimeContactViewSet,
                           self).get_renderers()
        # This is a bit of a hack to make the API able to produce an HTML
        # callsheet. The API will raise a 404 if we attempt to specify
        # a "print" format without actually having a "print" Renderer.
        if self.action == "retrieve":
            renderers_ += [PrintTemplateRenderer()]
        return renderers_

    def _initial(self):
        account_id = self.kwargs.get(self.parent_account_lookup)
        self.account = get_object_or_404(
            Account.objects.select_related("account_user"), pk=account_id)

    def retrieve(self, request, *args, **kwargs):
        # Check for "print" requests. Print requests will return a callsheet
        renderer, _ = self.perform_content_negotiation(request)
        if isinstance(renderer, PrintTemplateRenderer):
            obj = self.get_object()
            root_cs = ContactSet.get_root(self.account,
                                          self.account.account_user)
            result = root_cs.queryset().filter(contact_id=obj.contact_id)\
                                       .select_related("partition")\
                                       .order_by("-partition__index").first() \
                     if root_cs else None

            if not result:
                LOGGER.warn("Missing Result for CallTimeContact: "
                            "({}){}".format(obj.id, obj.contact))

            call_sheet = CallSheet(obj.contact, result,
                                   account=self.account, template="web")
            return Response(call_sheet.build_context(),
                            template_name=call_sheet.template)
        else:
            return super(CallTimeContactViewSet, self).retrieve(
                request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        primary_phone_id = request.data.get("primary_phone")
        primary_email_id = request.data.get("primary_email")
        if not (primary_phone_id or primary_email_id):
            raise exceptions.APIException("'primary_phone' or 'primary_email' "
                                          "IDs are required")
        ct_contact = self.get_object()

        if primary_phone_id:
            primary_phone = get_object_or_404(PhoneNumber,
                                              id=int(primary_phone_id))
            # Verify this contact actually owns this phone number
            if primary_phone not in ct_contact.contact.all_phones():
                raise Http404
            # If everything checks-out above, assign the primary phone
            ct_contact.primary_phone = primary_phone

        if primary_email_id:
            primary_email = get_object_or_404(EmailAddress,
                                              id=int(primary_email_id))
            # Verify this contact actually owns this email
            if primary_email not in ct_contact.contact.all_emails():
                raise Http404
            # If everything checks-out above, assign the primary phone
            ct_contact.primary_email = primary_email

        ct_contact.save()
        self.invalidate_cache(self.account.account_user)
        return Response({})

    def create(self, request, **kwargs):
        contact_id = request.data.get("contact")
        contact = get_object_or_404(Contact, pk=contact_id,
                                    # We only allow account contacts
                                    user=self.account.account_user)
        contact = contact.get_oldest_ancestor()
        try:
            ct_contact = CallTimeContact.objects.create(contact=contact,
                                                        account=self.account)
        except IntegrityError:
            try:
                # Check if the ct-contact was deleted. If so, un-delete
                ct_contact = CallTimeContact.objects.exclude(
                    archived=None).get(contact=contact, account=self.account)
                ct_contact.archived = None
                ct_contact.save(update_fields=["archived"])
            except CallTimeContact.DoesNotExist:
                raise exceptions.APIException(
                    "CallTime record already exists for contact: "
                    "{}".format(contact))

        self.invalidate_cache(self.account.account_user)
        return Response(CallTimeContactSerializer(instance=ct_contact).data)

    def destroy(self, request, *args, **kwargs):
        response = super(CallTimeContactViewSet, self).destroy(
            request, *args, **kwargs)
        self.invalidate_cache(self.account.account_user)
        return response

    @action(detail=False, methods=["post"])
    def call_sheets(self, request, *args, **kwargs):
        ctc_ids = request.data.get("ids", [])
        if not ctc_ids:
            raise APIException(LOGGER,
                               "Call Time Contact IDs 'ids' are required")
        elif not isinstance(ctc_ids, (list, tuple)):
            ctc_ids = [ctc_ids]

        queryset = self.filter_queryset(self.get_queryset()).filter(
            id__in=ctc_ids)

        email_to = [request.user.email]
        if request.impersonator:
            email_to.append(request.impersonator.email)

        # Generate a RevUpTask for tracking, then send this to celery
        task_type = RevUpTask.TaskType.GENERATE_CALL_SHEETS
        task = _get_revuptask(task_type,
                              RevUpTask.TaskType.translate(task_type),
                              request.user)

        generate_batch_call_sheet_task.apply_async(
            args=(queryset.query, queryset.model, request.user.id,
                  email_to),
            task_id=task.task_id,
            add_to_parent=True
        )
        return Response()

    @classmethod
    def invalidate_cache(cls, user):
        """Invalidate any caches that may be affected by this view.

        Currently, the only view that caches call time info is the results.
        """
        from frontend.apps.analysis.api.views import (
            ContactSetAnalysisContactResultsViewSet)
        ContactSetAnalysisContactResultsViewSet.invalidate_cache(user)


class CallGroupViewSetBase(AutoinjectUrlParamsIntoDataMixin, InitialMixin,
                           NestedViewSetMixin, viewsets.ModelViewSet):
    filter_backends = (filters.SearchFilter,)
    search_fields = ('title',)
    autoinject_map = {'account': 'get_account',
                      'user': "get_user"}

    class pagination_class(PageNumberPagination):
        page_size = 20
        page_size_query_param = 'page_size'
        max_page_size = 100

    def get_user(self):
        return self.user.id

    def get_account(self):
        return self.account.id

    @action(detail=True, methods=["get", "post"])
    def call_sheets(self, request, *args, **kwargs):
        # Get the group just to verify the given ID is good
        group = self.get_object()
        queryset = CallTimeContactSet.objects.filter(id=group.id)
        email_to = [request.user.email]
        if request.impersonator:
            email_to.append(request.impersonator.email)

        # Generate a RevUpTask for tracking, then send this to celery
        task_type = RevUpTask.TaskType.GENERATE_CALLGROUP_SHEETS
        label = "Generate Call Sheets for \"{}\"".format(group.title)
        task = _get_revuptask(task_type, label, request.user)

        generate_batch_call_sheet_task.apply_async(
            args=(queryset.query, CallTimeContactSet, request.user.id,
                  email_to),
            kwargs=dict(filename="Revup_call_sheets-{}.pdf".
                        format(group.title)),
            task_id=task.task_id,
            add_to_parent=True
        )
        return Response()


class CallGroupViewSet(CSVExportMixin, CallGroupViewSetBase):
    parent_account_lookup = compose_parent_pk_kwarg_name('account_id')
    serializer_class = CallTimeCallGroupSerializer
    permission_classes = (
        And(permissions.IsAuthenticated,
            HasPermission(parent_account_lookup,
                          AdminPermissions.MODIFY_CALLGROUPS),
            ),)
    queryset = CallTimeContactSet.objects.active().prefetch_related(
                        "calltimeseat_set__seat__user").order_by("-created")

    def _share_helper(self, request, **kwargs):
        call_group = self.get_object()
        # Accept one 'seat' or a list of 'seats'
        seat_ids = request.data.get("seat")
        if seat_ids:
            if not isinstance(seat_ids, list):
                seat_ids = [seat_ids]
        else:
            seat_ids = request.data.get("seats")
        seats = get_list_or_404(
            Seat.objects.assigned(), id__in=seat_ids,
            account=kwargs.get(self.parent_account_lookup))
        return call_group, seats

    def _initial(self):
        self.account = get_object_or_404(
            Account, id=self.kwargs.get(self.parent_account_lookup))
        self.user = self.account.account_user

    def _get_export_options(self, request):
        export_options = super(CallGroupViewSet,
                               self)._get_export_options(request)
        export_options["show_call_log"] = str_to_bool(
            request.GET.get("show_call_log", True))
        return export_options

    def _get_export_contact_set(self):
        """From CSVExportMixin. We need to get the user's root contact set."""
        return ContactSet.get_root(self.account, self.user,
                                   select_related="analysis")

    def _get_export_task_type(self):
        return self._export_task_types.CALL_GROUP_EXPORT

    def _get_export_queryset(self, contact_set):
        return self.get_queryset().get(id=self.kwargs.get(self.lookup_field))

    def _get_export_label(self, contact_set):
        return "Export Call Group \"{}\"".format(self.group.title[:105])

    def _get_export_filename(self, contact_set):
        return self.group.title[:251] + ".csv"

    @action(detail=True)
    def export(self, request, *args, **kwargs):
        """Override this method to be detail-route instead of list."""
        self.group = self.get_object()
        return super(CallGroupViewSet, self).export(request, *args, **kwargs)

    @action(detail=True, methods=["post"])
    @exception_watcher(PermissionError)
    def add_share(self, request, **kwargs):
        call_group, seats = self._share_helper(request, **kwargs)
        shared = call_group.share(*seats)
        return Response({"shared_seats": [s.seat.id for s in shared]})

    @action(detail=True, methods=["post"])
    def remove_share(self, request, **kwargs):
        call_group, seats = self._share_helper(request, **kwargs)
        CallTimeSeat.objects.filter(
            contact_set=call_group, seat__in=seats).delete()
        return Response({})


class SeatCallGroupViewSet(CallGroupViewSetBase):
    parent_user_lookup = compose_parent_pk_kwarg_name('user_id')
    parent_seat_lookup = compose_parent_pk_kwarg_name('seat_id')
    serializer_class = CallGroupSerializer
    permission_classes = (
        And(permissions.IsAuthenticated,
            UserIsRequester,
            UserOwnsSeat(parent_seat_lookup)),)

    def get_queryset(self):
        return CallTimeContactSet.objects.active().filter(
            account=self.account, shared_with=self.seat)

    def _initial(self):
        seat_id = self.kwargs.get(self.parent_seat_lookup)
        user_id = self.kwargs.get(self.parent_user_lookup)
        try:
            seat_id = int(seat_id)
            user_id = int(user_id)
        except ValueError:
            raise Http404
        else:
            self.seat = get_object_or_404(
                Seat.objects.assigned(),
                id=seat_id,
                user_id=user_id)
            self.account = self.seat.account
            self.user = self.account.account_user

    def create(self, request, **kwargs):
        raise exceptions.MethodNotAllowed

    def destroy(self, request, *args, **kwargs):
        raise exceptions.MethodNotAllowed


class CallGroupContactsViewSetBase(InitialMixin, DetailSerializerMixin,
                                   NestedViewSetMixin, viewsets.ModelViewSet):
    parent_call_group_lookup = compose_parent_pk_kwarg_name('call_group_id')
    serializer_class = CallGroupContactSerializer
    serializer_detail_class = CallGroupContactSerializer


    class pagination_class(PageNumberPagination):
        page_size = 20
        page_size_query_param = 'page_size'
        max_page_size = 100

    def is_mobile_request(self):
        return str_to_bool(self.request.query_params.get("mobile"))

    def get_serializer_context(self):
        context = super(CallGroupContactsViewSetBase,
                        self).get_serializer_context()
        context["mobile_mode"] = self.is_mobile_request()
        return context

    def get_queryset(self):
        return self.call_group.queryset().prefetch_related("call_logs")

    def paginate_queryset(self, queryset):
        """Paginate a queryset if required, either returning a
         page object, or `None` if pagination is not configured for this view.

        Much of the following code is taken from DjangoPaginator or the
        PageNumberPagination classes. We couldn't use those because we
        need to call the custom "slice" method below.
        """
        pagination = self.pagination_class()
        page_size = pagination.get_page_size(self.request)
        if not page_size:
            return None

        paginator = pagination.django_paginator_class(queryset, page_size)
        # We'll provide the count so we don't need the extra query
        paginator._count = self.call_group.count if self.call_group else 0
        page_number = self.request.query_params.get(
            pagination.page_query_param, 1)
        if page_number in pagination.last_page_strings:
            page_number = paginator.num_pages
        # Validate the given page number
        try:
            page_number = paginator.validate_number(page_number)
        except InvalidPage as err:
            raise exceptions.NotFound(err)

        # Slice and order the contact set page
        bottom = (page_number - 1) * page_size
        top = bottom + page_size
        slice_ = self.call_group.slice(bottom, top, queryset)

        # Setup the paginator to work with rest framework
        pagination.page = paginator._get_page(slice_, page_number, paginator)
        pagination.request = self.request
        self._paginator = pagination
        return list(self._paginator.page)

    def update(self, request, **kwargs):
        return self.create(request, **kwargs)

    def _create_or_destroy(self, contact_ids, create=True):
        if not isinstance(contact_ids, list):
            contact_ids = [contact_ids]
        ct_contacts = get_list_or_404(
            CallTimeContact.objects.active(),
            id__in=contact_ids, account=self.call_group.account)
        if create:
            try:
                self.call_group.add(*ct_contacts)
            except CallGroupQuotaExceededError:
                raise APIException(LOGGER, "This Call Group is too large to "
                                           "contain any more Call Sheets.")
        else:
            self.call_group.remove(*ct_contacts)

    def create(self, request, **kwargs):
        """Add a call-time contact to this contact set."""
        contact_ids = request.data.get("id", [])
        if not contact_ids:
            raise exceptions.APIException(
                "Create requires a call time contact 'id'")
        self._create_or_destroy(contact_ids, True)
        return response.Response()

    def destroy(self, request, *args, **kwargs):
        # Remove call-time contact
        contact_id = self.kwargs.get("pk")
        self._create_or_destroy(contact_id, False)
        return HttpResponseNoContent()


    @exception_watcher(ValueError)
    @action(detail=True, methods=["put"])
    def ordering(self, request, **kwargs):
        """Change a call-time contacts location in the ordering.

        This endpoint will accept 2 modes of operation: "shift" and "relative".

        mode: "shift"
            Accepts a `direction` and `places` arguments to determine how
            many places a call-time contact should move.

            Examples:
                Move up 2 places.
                {"mode": "shift",
                 "direction": "up",
                 "places": 2}

                Move down 1 position
                {"mode": "shift",
                 "direction": "down",
                 "places": 1}

        mode: "relative"
            Accepts a `position` and `id` arguments to set
            the call-time contact's position relative to another in the set.

            Examples:
                Place it before ct-contact with id 10
                {"mode": "relative",
                 "position": "before",  # "b" is also acceptable
                 "id": 10}

                Place if at the front of the list
                {"mode": "relative",
                 "position": "after",  # "a" is also acceptable
                 "id": null}

                Place if at the end of the list
                {"mode": "relative",
                 "position": "before",
                 "id": null}
        """
        pk = kwargs.get("pk")
        ct_contact = get_object_or_404(CallTimeContact.objects.active(),
                                       id=pk, account=self.call_group.account)

        mode = request.data.get("mode", "").lower()
        if mode == "shift":
            direction = request.data.get("direction", "").lower()
            try:
                places = request.data.get("places")
                places = int(places)
            except (ValueError, TypeError):
                raise exceptions.APIException(
                    "Invalid value for 'places': {}".format(places))
            self.call_group.order_shift(ct_contact, direction, places)
        elif mode == "relative":
            position = request.data.get("position", "").lower()
            try:
                other_id = request.data.get("id")
                other_id = int(other_id)
            except (ValueError, TypeError):
                raise exceptions.APIException(
                    "Invalid value for 'id': {}".format(other_id))
            self.call_group.order_relative(ct_contact, position, other_id)
        else:
            raise exceptions.APIException(
                "'mode' must be specified as either 'relative' or 'shift'.")
        return response.Response()

    @action(detail=True, methods=["get"])
    def get_next(self, request, **kwargs):
        """Get the next contact in the call group from the current pk"""
        ct_contact = self.get_object()
        next_ctc = self.call_group.get_next(ct_contact)
        data = (self.get_serializer_class()(
                                instance=next_ctc,
                                context=self.get_serializer_context()).data
                if next_ctc else {})
        return response.Response(data=data)


class CallGroupContactsViewSet(CallGroupContactsViewSetBase):
    parent_account_lookup = compose_parent_pk_kwarg_name('account_id')
    permission_classes = (
        And(permissions.IsAuthenticated,
            HasPermission(parent_account_lookup,
                          AdminPermissions.MODIFY_CALLGROUPS),
            ),)

    def _initial(self):
        self.call_group = get_object_or_404(
            CallTimeContactSet.objects.active(),
            id=self.kwargs.get(self.parent_call_group_lookup),
            account=self.kwargs.get(self.parent_account_lookup))

    @action(detail=False, methods=["post"])
    def from_contact_set(self, request, *args, **kwargs):
        try:
            contact_set_id = request.data.get("contact_set")
        except (ValueError, TypeError):
            raise APIException(LOGGER, "A 'contact_set' is required")

        contact_set = get_object_or_404(ContactSet.objects.active(),
                                        id=contact_set_id,
                                        account_id=self.call_group.account_id)

        # This query will calculate the number of contacts the call group will
        # have after this merge.
        new_group_count = \
            contact_set.queryset().exclude(
                contact_id__in=self.call_group.contacts.values_list(
                        "contact_id", flat=True)).distinct("contact").count()

        # Verify the added contacts will not exceed the size limit of the group
        if new_group_count > settings.MAXIMUM_CALLGROUP_SIZE:
            raise APIException(
                LOGGER,
                'Cannot add this list ("{}") to the Call Group: '
                '"{}" because it will exceed the size limit of "{}"'.format(
                    contact_set.title,
                    self.call_group.title,
                    settings.MAXIMUM_CALLGROUP_SIZE))

        # Generate a RevUpTask for tracking, then send this to celery
        task_type = RevUpTask.TaskType.CALL_GROUP_FROM_LIST
        task = _get_revuptask(task_type,
                              RevUpTask.TaskType.translate(task_type),
                              contact_set.user)
        add_contact_set_to_call_group.apply_async(
            args=(contact_set.id, self.call_group.id),
            task_id=task.task_id,
            add_to_parent=True)
        return Response()


class SeatCallGroupContactsViewSet(CallGroupContactsViewSetBase):
    parent_user_lookup = compose_parent_pk_kwarg_name('user_id')
    parent_seat_lookup = compose_parent_pk_kwarg_name('seat_id')
    serializer_detail_class = DetailedCallTimeContactSerializer
    permission_classes = (
        And(permissions.IsAuthenticated,
            UserIsRequester,
            UserOwnsSeat(parent_seat_lookup),
            ViewMethodChecker('verify_user_has_access_to_call_group')
            ),)

    def get_serializer_context(self):
        context = super(SeatCallGroupContactsViewSet,
                        self).get_serializer_context()
        context['account'] = self.account
        context.update(
            DetailedCallTimeContactSerializer.prepare_view_context(self))
        return context

    def verify_user_has_access_to_call_group(self, request):
        # The requested seat may only view this call group if the call group
        # has been shared with it
        return self.call_group.shared_with.filter(id=self.seat.id).exists()

    def _pre_permission_initial(self):
        # queries needed before permission checks
        self.user = get_object_or_404(
            RevUpUser,
            id=self.kwargs.get(self.parent_user_lookup))

        self.seat = get_object_or_404(
            Seat.objects.assigned().select_related("account"),
            id=self.kwargs.get(self.parent_seat_lookup),
            user=self.user)

        self.account = self.seat.account

        self.call_group = get_object_or_404(
            CallTimeContactSet.objects.active(),
            id=self.kwargs.get(self.parent_call_group_lookup),
            account=self.account)


class CallLogViewSetBase(NestedViewSetMixin, InitialMixin,
                         viewsets.ModelViewSet):
    serializer_class = CallLogSerializer
    parent_ct_contact_lookup = compose_parent_pk_kwarg_name('ct_contact_id')
    parent_call_group_lookup = compose_parent_pk_kwarg_name('call_group_id')

    def get_queryset(self):
        qs = CallLog.objects
        if self.call_group:
            qs = qs.filter(call_group=self.call_group)
        return qs.filter(ct_contact=self.ct_contact)

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        data = request.data
        if not data.get("call_group") and self.call_group:
            data["call_group"] = self.call_group.id
        data["ct_contact"] = self.ct_contact.id
        notes = data.pop("notes", None)

        response = super(CallLogViewSetBase, self).create(request,
                                                          *args, **kwargs)
        if notes:
            notes["account"] = self.ct_contact.account_id
            notes["contact"] = self.ct_contact.contact_id
            if "id" in notes:
                notes_instance = get_object_or_404(
                    ContactNotes, id=notes.pop("id"), contact=notes["contact"])
            else:
                notes_instance = None
            serializer = ContactNotesSerializer(instance=notes_instance,
                                                data=notes)
            serializer.is_valid(raise_exception=True)
            serializer.save()
        return response

    def destroy(self, request, *args, **kwargs):
        raise exceptions.MethodNotAllowed


class CallLogViewSet(CallLogViewSetBase):
    serializer_class = CallLogSerializer
    parent_account_lookup = compose_parent_pk_kwarg_name('account_id')

    permission_classes = (
        And(permissions.IsAuthenticated,
            HasPermission(parent_account_lookup,
                          AdminPermissions.MODIFY_CALLTIME)
            ),)

    def _initial(self):
        self.ct_contact = get_object_or_404(
            CallTimeContact.objects.active(),
            id=self.kwargs.get(self.parent_ct_contact_lookup),
            account=self.kwargs.get(self.parent_account_lookup))

        if self.parent_call_group_lookup in self.kwargs:
            self.call_group = get_object_or_404(
                CallTimeContactSet.objects.active(),
                id=self.kwargs.get(self.parent_call_group_lookup),
                account=self.kwargs.get(self.parent_account_lookup))
        else:
            self.call_group = None


class SeatCallLogViewSet(CallLogViewSetBase):
    parent_user_lookup = compose_parent_pk_kwarg_name('user_id')
    parent_seat_lookup = compose_parent_pk_kwarg_name('seat_id')
    permission_classes = (
        And(permissions.IsAuthenticated,
            UserIsRequester,
            UserOwnsSeat(parent_seat_lookup),
            ViewMethodChecker('verify_user_has_access_to_call_group')
            ),)

    def verify_user_has_access_to_call_group(self, request):
        # The requested seat may only view this call group if the call group
        # has been shared with it
        return self.call_group.shared_with.filter(id=self.seat.id).exists()

    def _pre_permission_initial(self):
        # queries needed before permission checks
        self.user = get_object_or_404(
            RevUpUser,
            id=self.kwargs.get(self.parent_user_lookup))

        self.seat = get_object_or_404(
            Seat.objects.assigned(),
            id=self.kwargs.get(self.parent_seat_lookup),
            user=self.user)

        self.ct_contact = get_object_or_404(
            CallTimeContact.objects.active(),
            id=self.kwargs.get(self.parent_ct_contact_lookup),
            account=self.seat.account)

        self.call_group = get_object_or_404(
            CallTimeContactSet.objects.active(),
            id=self.kwargs.get(self.parent_call_group_lookup),
            account=self.seat.account,)


def _get_revuptask(task_type, label, task_user):
    """Build a RevUpTask. Reject if task already exists"""
    task_kwargs = dict(
        user=task_user,
        task_type=task_type,
        label=label,
        revoked=False,
    )
    try:
        task = RevUpTask.objects.get(**task_kwargs)
        # If this task already exists and isn't finished, we need to reject
        if not task.finished():
            raise APIException(LOGGER, "This task is already running")
    except RevUpTask.DoesNotExist:
        pass

    return RevUpTask.objects.create(task_id=uuid(), **task_kwargs)

