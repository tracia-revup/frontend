from datetime import timedelta, datetime

from rest_framework import serializers

from frontend.apps.analysis.api.serializers import (
    MobileDetailedResultSerializer, DetailedResultSerializer)
from frontend.apps.call_time.models import (
    CallTimeContact, CallTimeContactSet, CallLog, CallTimeSeat)
from frontend.apps.contact.api.serializers import (
    ContactSerializer, ContactNotesSerializer, DetailedContactSerializer)
from frontend.apps.contact_set.models import ContactSet
from frontend.apps.seat.api.serializers import NameSeatSerializer
from frontend.libs.utils.api_utils import WritableMethodField
from frontend.libs.utils.string_utils import str_to_bool


class MinimalCallGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = CallTimeContactSet
        fields = ("id", "title", "description", "user", "created", "account")
        read_only_fields = ("id", "created")


class CallTimeContactSerializerBase(serializers.ModelSerializer):
    contact = ContactSerializer(read_only=True)

    class Meta:
        model = CallTimeContact
        fields = ("id", "contact", "account", "primary_phone", "primary_email",
                  "created")


class CallTimeContactSerializer(CallTimeContactSerializerBase):
    call_groups = MinimalCallGroupSerializer(
        many=True, source="call_time_contact_sets")

    class Meta(CallTimeContactSerializerBase.Meta):
        fields = CallTimeContactSerializerBase.Meta.fields + ("call_groups",)


class DetailedCallTimeContactSerializer(CallTimeContactSerializer):
    contact = DetailedContactSerializer(read_only=True)
    notes = serializers.SerializerMethodField()
    result = serializers.SerializerMethodField()

    class Meta(CallTimeContactSerializer.Meta):
        fields = CallTimeContactSerializer.Meta.fields + ("result", "notes")

    def get_result(self, obj):
        root_cs = self.context.get("root_cs")
        result = root_cs.queryset().filter(contact_id=obj.contact_id).first() \
                 if root_cs else None

        if self.context.get("mobile_mode"):
            result_serializer_class = MobileDetailedResultSerializer
        else:
            result_serializer_class = DetailedResultSerializer
        return result_serializer_class(result, context=self.context).data \
                if result else None

    def get_notes(self, obj):
        return ContactNotesSerializer(obj.contact.notes.all(),
                                      many=True, context=self.context).data

    @classmethod
    def prepare_view_context(cls, view):
        context = {}
        if view.action in ("retrieve", "get_next"):
            # If this is the detail view, pass the root-cs to the serializer.
            root_cs = ContactSet.get_root(view.account,
                                          view.account.account_user)
            context["root_cs"] = root_cs
            if root_cs and root_cs.analysis:
                # Get the normalization information
                partition_id = root_cs.get_partition_configs(
                    root_cs.analysis).first()
                normalize_meta = root_cs.analysis.meta_results.get("normalize")
                if normalize_meta:
                    partition = normalize_meta.get(str(partition_id))
                    if partition:
                        context["normalize"] = partition
        return context


class CallTimeSharedWithSerializer(serializers.ModelSerializer):
    seat = NameSeatSerializer(read_only=True)

    class Meta:
        model = CallTimeSeat
        fields = ('seat', 'shared_dt')


class CallGroupSerializer(MinimalCallGroupSerializer):
    progress = serializers.SerializerMethodField()

    class Meta(MinimalCallGroupSerializer.Meta):
        fields = MinimalCallGroupSerializer.Meta.fields + (
            "count", "progress",)
        read_only_fields = MinimalCallGroupSerializer.Meta.read_only_fields + (
            "count", "progress",)

    def get_progress(self, call_group):
        return None


class CallTimeCallGroupSerializer(CallGroupSerializer):
    shared_with = CallTimeSharedWithSerializer(many=True, read_only=True,
                                               source="calltimeseat_set")

    class Meta(CallGroupSerializer.Meta):
        fields = CallGroupSerializer.Meta.fields + ("shared_with",)


class CallLogSerializer(serializers.ModelSerializer):
    call_result = WritableMethodField()
    log_type = WritableMethodField()
    call_time = WritableMethodField(required=False)

    class Meta:
        model = CallLog
        fields = ("id", "ct_contact", "log_type", "call_back", "call_result",
                  "call_group", "pledged", "call_time", "created", "see_notes")
        read_only_fields = ("id", "created")

    def get_log_type(self, call):
        return CallLog.LogTypes.translate(call.log_type, "")

    def get_call_result(self, call):
        return CallLog.CallResults.translate(call.call_result, "")

    def get_call_time(self, call):
        # Will give HH:MM:SS format
        return str(timedelta(seconds=call.call_time))

    def to_internal_value(self, data):
        # If call result is not specified, we have two options: either pledged
        # or unknown
        if "call_result" not in data:
            if data.get("pledged"):
                result = CallLog.CallResults.PLEDGED
            else:
                result = CallLog.CallResults.UNKNOWN
            data["call_result"] = result

        # We allow the frontend to specify a boolean value instead of a
        # datetime for call_back. If a boolean is given, we'll just choose now
        if str_to_bool(data.get("call_back")):
            data["call_back"] = datetime.now()
        return super(CallLogSerializer, self).to_internal_value(data)


class CallGroupContactSerializer(CallTimeContactSerializerBase):
    logs = CallLogSerializer(many=True, source="call_logs")

    class Meta(CallTimeContactSerializerBase.Meta):
        fields = CallTimeContactSerializerBase.Meta.fields + ("logs",)


class NeedsAttentionCallTimeContactSerializer(serializers.ModelSerializer):
    logs = CallLogSerializer(many=True, source="call_logs")

    class Meta:
        model = CallTimeContact
        fields = ("id", "contact", "account", "primary_phone", "primary_email",
                  "created", "logs")