from collections import Iterable
from contextlib import contextmanager
import logging
from math import ceil
from os import environ
from io import BytesIO

from celery import group, current_task
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.template import loader
from PyPDF2 import PdfFileReader, PdfFileMerger
if 'REVUP_IN_UWSGI' not in environ:
    import weasyprint
else:
    weasyprint = None

from frontend.apps.contact.api.serializers import DetailedContactSerializer
from frontend.apps.contact.models import ContactNotes
from frontend.apps.contact_set.models import ContactSet
from frontend.libs.utils.celery_utils import update_progress
from frontend.libs.utils.string_utils import random_uuid


LOGGER = logging.getLogger(__name__)


class CallSheet(object):
    templates = dict(
        web="call_time/call_sheet-web.html",
        pdf="call_time/call_sheet-pdf.html")
    limits = dict(
        client_matches=5,
        key_contributions=3,
        federal_matches=10,
        state_matches=5,
        local_matches=5,
        nonprofit_matches=5,)

    def __init__(self, contact, result=None, account=None, partition=None,
                 template="pdf", *args, **kwargs):
        self.contact = contact
        self.result = result
        self.template = self.templates[template]

        self.account = account
        if not self.account and self.result:
            self.account = result.analysis.account
        self.partition = partition
        if not self.partition and self.result:
            self.partition = result.partition

    def _limit_match(self, signal_set, limit):
        """Limit the count of matches based on feature type."""
        if not isinstance(signal_set, (list, tuple)):
            signal_set = [signal_set]

        for signals in signal_set:
            if not limit or "match" not in signals:
                return
            signals["match"] = signals["match"][:limit]

    def reformat_key_contribs(self, key_contribs):
        """Change the format of key contribs to be better for the template"""
        allies, oppos = [], []
        limit = self.limits["key_contributions"]
        for key_contrib in key_contribs:
            if not isinstance(key_contrib, dict):
                LOGGER.warn("Key Contrib is not a dict: {}".format(key_contrib))
                continue
            for label, data in key_contrib.items():
                self._limit_match(data, limit)
                data["label"] = label
                if data.pop("is_ally", None):
                    allies.append(data)
                else:
                    oppos.append(data)
        return allies, oppos

    def build_context(self):
        def key_signal(key, default=None):
            if not self.result:
                return default
            return self.result.key_signals.get(key, default)

        # Collect the data
        contact_serializer = DetailedContactSerializer(instance=self.contact)
        contact = dict(contact_serializer.data)
        features = self.result.get_features(limit=10) if self.result else []

        # Limit the phone numbers to 5
        contact["phone_numbers"] = contact["phone_numbers"][:5]

        total_giving = key_signal("giving", 0)
        giving_availability = key_signal("giving_availability")

        try:
            notes = ContactNotes.objects.get(account=self.account,
                                             contact=self.contact)
        except ContactNotes.DoesNotExist:
            notes = None

        # Build the context
        context = dict(contact=contact,
                       account=self.account,
                       total_giving=total_giving,
                       giving_availability=giving_availability,
                       giving_since=self.partition.label,
                       notes=notes)

        # Add all of the FeatureResults, grouped by their feature type
        for title, feature_type, signals in features:
            if not feature_type:
                continue

            feature_type = feature_type.strip().lower().replace(" ", "_")
            # Key contribs need special treatment
            if feature_type == "key_contributions" and signals:
                allies, oppos = self.reformat_key_contribs(signals)
                signals = dict(
                    allies=allies,
                    opponents=oppos)
            else:
                self._limit_match(signals, self.limits.get(feature_type))

            if feature_type in context:
                context[feature_type].extend(signals)
            else:
                context[feature_type] = signals
        return context

    def render_html(self):
        # Build the context
        context = self.build_context()
        # Render the template
        return loader.render_to_string(self.template, context)

    def render_pdf(self):
        """Use weasyprint to generate a PDF from HTML"""
        html = self.render_html()
        return weasyprint.HTML(string=html).render(presentational_hints=True)

    def write_pdf(self, target, **kwargs):
        """Write the PDF

        :param target: A filename or file-like object
        """
        self.render_pdf().write_pdf(target=target, **kwargs)


class BatchBase(object):
    def __init__(self, queryset, user, email_to):
        self.queryset = queryset
        self.user = user
        self.email_to = email_to
        # This hash will be used to construct the filenames
        self.batch_key = random_uuid()
        if current_task:
            self.update_progress = update_progress
        else:
            self.update_progress = BatchBase.noop

    @classmethod
    def noop(cls, _, __):
        return

    def execute(self, filename=None):
        from frontend.apps.call_time.tasks import (
            _parallel_generate_batch_call_sheet,
            _parallel_complete_batch_call_sheet)
        with self.handle_exception():
            batch_size = settings.CALL_SHEET_PARALLEL_BATCH_SIZE
            batches_count = int(ceil(self.total_count / float(batch_size)))
            # Start the progress at 0%
            self.update_progress(0, 100)

            # Deconstruct the queryset to passed through celery
            self.query = self.queryset.query
            self.model = self.queryset.model
            self.queryset = None

            # Build the subtasks
            tasks = (group(
                _parallel_generate_batch_call_sheet.s(
                    self,
                    self.generate_partial_filename(index),
                    index * batch_size,
                    (index + 1) * batch_size)
                for index in range(batches_count)) |
                _parallel_complete_batch_call_sheet.si(
                    self, batches_count, filename)
            ).apply_async(add_to_parent=True)
            return tasks

    def parallel_run(self, slice_begin, slice_end):
        """Generate a partial PDF from a slice of the original queryset."""
        # Reconstruct the queryset
        self.queryset = self.model.objects.all()
        self.queryset.query = self.query

        # Render and write the slice to stringio
        pdf_file = BytesIO()
        self.write_pdf(pdf_file, slice_tuple=(slice_begin, slice_end))
        return pdf_file

    def parallel_complete(self, partial_pdfs):
        """Build the whole document from the partials"""
        output = PdfFileMerger(strict=False)
        # Read each of the partial pdfs and concatenate them
        for pdf in partial_pdfs:
            doc = PdfFileReader(pdf)
            output.append(doc)
        # Write the whole, reconstituted pdf file to stringio and return
        pdf_file = BytesIO()
        output.write(pdf_file)
        return pdf_file

    @contextmanager
    def handle_exception(self):
        try:
            yield
        except Exception:
            if self.email_to:
                from frontend.apps.batch_jobs.utils import send_error_email
                send_error_email(self.user, self.email_to, "Call Sheets")
            # TODO: Do stuff here with task tracking
            raise

    def generate_partial_filename(self, index):
        return "{}_{}_{}.part.pdf".format(self.user.id, self.batch_key, index)

    def contact_results(self, slice_tuple=None):
        qs = self.queryset
        if slice_tuple:
            qs = self.queryset[slice_tuple[0]: slice_tuple[1]]
        return ((r.contact, r) for r in qs)

    def render_pdf(self, slice_tuple=None):
        # Setup progress tracking
        iteration_step = 0.97
        finished = 0.0
        # This is a rough count, as there may not be as many items as the
        # slice is requesting, but this is good enough for progress tracking.
        count = slice_tuple[1] - slice_tuple[0] \
                if slice_tuple else self.total_count

        # Generate the pages for each Result
        pages = []
        for contact, result in self.contact_results(slice_tuple=slice_tuple):
            document = BytesIO()
            cs = CallSheet(contact, result, account=self.account,
                           partition=self.partition)
            cs.write_pdf(document)
            pages.append(document)
            # Track progress
            finished += iteration_step
            self.update_progress(finished, count)

        # Build the whole document
        output = PdfFileMerger(strict=False)
        if pages:
            for page in pages:
                doc = PdfFileReader(page)
                output.append(doc)
            return output
        else:
            return None

    def write_pdf(self, target, slice_tuple=None, **kwargs):
        pdf = self.render_pdf(slice_tuple=slice_tuple)
        if pdf:
            pdf.write(target)
        else:
            raise ValueError("No PDF was generated")

    @classmethod
    def factory(cls, queryset, user, email_to):
        from frontend.apps.analysis.models import Result
        from frontend.apps.call_time.models import (
            CallTimeContact, CallTimeContactSet)

        if queryset.model is Result:
            batch_generator = BatchCallSheet(queryset,
                                             user, email_to)
        elif queryset.model is CallTimeContact:
            batch_generator = CallTimeContactBatchCallSheet(queryset,
                                                            user, email_to)
        elif queryset.model is CallTimeContactSet:
            batch_generator = CallGroupBatchCallSheet(queryset, user, email_to)
        else:
            raise TypeError("Unrecognized model for call sheets: {}".format(
                queryset.model.__name__))
        return batch_generator


class BatchCallSheet(BatchBase):
    def __init__(self, queryset, *args):
        super(BatchCallSheet, self).__init__(queryset, *args)
        results = queryset.select_related("contact")
        r = results[0]
        # Prefetch the account and partition to reduce needless queries
        self.account = r.analysis.account
        self.partition = r.partition
        self.queryset = results
        self.total_count = results.count()


class CallTimeContactBatchCallSheet(BatchBase):
    def __init__(self, queryset, user, email_to, partition=None):
        super(CallTimeContactBatchCallSheet, self).__init__(
                                                queryset, user, email_to)
        self.user = user
        self.email_to = email_to
        ctc = queryset.select_related("account", "contact__user").first()
        if not ctc:
            return

        # Get the root contact set so we can get the Results
        self.account = ctc.account
        user = ctc.contact.user
        root_cs = ContactSet.get_root(self.account, user,
                                      select_related="analysis")
        if not root_cs.analysis:
            return

        if partition:
            self.partition = partition
        else:
            # Use the oldest partition
            self.partition = root_cs.get_partition_configs(
                root_cs.analysis, as_ids=False).order_by('-index').first()

        # Use the root contactset to query the Results
        contact_qs = queryset.values_list("contact_id")
        self.queryset = root_cs.queryset(partition=self.partition) \
                               .filter(contact__in=contact_qs) \
                               .select_related("contact")
        self.total_count = self.queryset.count()


class CallGroupBatchCallSheet(BatchBase):
    """Generate call sheets for a Call Group.
        This will ensure the ordering of the call group is maintained in the
        call sheets.
    """
    def __init__(self, queryset, user, email_to, partition=None):
        super(CallGroupBatchCallSheet, self).__init__(queryset, user, email_to)

        if isinstance(queryset, Iterable):
            call_group = queryset[0]
        else:
            call_group = queryset

        # Make sure the call group is all clean before we start
        call_group.clean_ordering()
        call_group.save()

        self.call_group = call_group
        self.account = call_group.account
        self.total_count = self.call_group.count if self.call_group else 0
        self.root_cs = ContactSet.get_root(self.account, call_group.user,
                                           select_related="analysis")
        if partition:
            self.partition = partition
        elif self.root_cs.analysis:
            # Use the oldest partition
            self.partition = self.root_cs.get_partition_configs(
                                    self.root_cs.analysis,
                                    as_ids=False).order_by('-index').first()
        else:
            self.partition = None

    def contact_results(self, slice_tuple=None):
        """We need this to keep the call group's contacts ordered correctly."""
        if self.partition:
            root_qs = self.root_cs.queryset(
                        partition=self.partition).select_related("contact")
        else:
            root_qs = None

        def generator():
            iterator = self.call_group.slice(*slice_tuple) \
                       if slice_tuple else self.call_group
            for ctc in iterator:
                try:
                    if root_qs is None:
                        raise ObjectDoesNotExist
                    result = root_qs.get(contact_id=ctc.contact_id)
                    yield result.contact, result
                except ObjectDoesNotExist:
                    LOGGER.error("Missing Result for CTC: {}".format(ctc.id))
                    yield ctc.contact, None
        return generator()
