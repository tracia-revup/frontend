import logging

from django.core.exceptions import ObjectDoesNotExist

from frontend.apps.analysis.csv_generator import CSVGenerator


LOGGER = logging.getLogger(__name__)


class CallGroupCSVGenerator(CSVGenerator):
    call_log_field = "Last Call Result"

    def __init__(self, *args, **kwargs):
        self.show_call_log = kwargs.pop("show_call_log", True)
        super(CallGroupCSVGenerator, self).__init__(*args, **kwargs)

    def _format_log_field(self, container, result):
        """Populate the row result dict with the contact's name

        Do this in a separate function so if the user decides they do not want
        these columns, we can just leave it out of the functional composition.
        It also makes unit testing much easier.
        """
        last_call_log = container.call_time_contact.call_logs.first()
        if last_call_log:
            result[self.call_log_field] = last_call_log.call_result_label
        return result

    def _inject_composition_functions(self):
        fns, fields = super(CallGroupCSVGenerator,
                            self)._inject_composition_functions()
        if self.show_call_log:
            fields.append(self.call_log_field)
            fns.append(self._format_log_field)
        return fns, fields

    def _prepare_container(self, obj):
        try:
            result = self.contact_set.queryset().get(
                contact=obj.contact.id, partition=self.partition)
        except ObjectDoesNotExist:
            LOGGER.warn("Result missing for Contact: {} in ContactSet: {}, "
                        "with Partition: {}".format(
                       obj.contact.id, self.contact_set.id, self.partition.id))
            result = None
        return self.Container(analysis_result=result,
                              contact=obj.contact,
                              call_time_contact=obj)

    def _prepare_queryset(self, queryset):
        return queryset
