import logging

from django.conf import settings
from django.contrib.postgres import fields
from django.db import models, transaction
from django_extensions.db.models import TimeStampedModel

from frontend.apps.contact_set.models import JoinedContactSet
from frontend.libs.utils.model_utils import (
    Choices, ArchiveModelMixin, LockingModelMixin)


LOGGER = logging.getLogger(__name__)


class CallGroupQuotaExceededError(Exception): pass


class CallTimeContactQuerySet(models.QuerySet):
    def active(self):
        return self.filter(archived=None, merged=None)


class CallTimeContact(ArchiveModelMixin, TimeStampedModel):
    contact = models.ForeignKey("contact.Contact", on_delete=models.CASCADE,
                                related_name="call_time_contacts")
    account = models.ForeignKey("campaign.Account", on_delete=models.CASCADE,
                                related_name="call_time_contacts")

    primary_phone = models.ForeignKey("contact.PhoneNumber",
                                      blank=True, null=True, related_name="+",
                                      on_delete=models.SET_NULL)
    primary_email = models.ForeignKey("contact.EmailAddress",
                                      blank=True, null=True, related_name="+",
                                      on_delete=models.SET_NULL)
    merged = models.DateTimeField(blank=True, null=True)

    objects = CallTimeContactQuerySet.as_manager()

    class Meta:
        unique_together = (("contact", "account"),)
        index_together = (("account", "archived", "merged", "contact"),)

    def delete(self, using=None, keep_parents=False, force=False):
        """Archive or delete this calltimecontact."""
        with transaction.atomic():
            # We need to cleanly remove this ct_contact from any groups
            for ct_group in self.call_time_contact_sets.all():
                ct_group.remove(self)

            return super(CallTimeContact, self).delete(
                using=using, keep_parents=keep_parents, force=force)


class CallLog(TimeStampedModel):
    class LogTypes(Choices):
        choices_map = [
            ("CAL", "CALL"),
            ("SMS", "SMS"),
            ("EML", "EMAIL"),
            ("UNK", "UNKNOWN")
        ]

    class CallResults(Choices):
        choices_map = [
            ("PLD", "PLEDGED"),
            ("NOA", "NO_ANSWER"),
            ("MSG", "VOICE_MESSAGE"),
            ("REF", "REFUSED"),
            ("BAD", "BAD_NUMBER"),
            ("UNK", "UNKNOWN")
        ]

    call_back = models.DateTimeField(
        blank=True, null=True, default=None,
        help_text="If set, the user is indicating they should reconnect with "
                  "this contact at a later time.")
    ct_contact = models.ForeignKey("call_time.CallTimeContact",
                                   related_name="call_logs", blank=False,
                                   null=False, on_delete=models.PROTECT)
    log_type = models.CharField(max_length=3, blank=True,
                                default=LogTypes.UNKNOWN,
                                choices=LogTypes.choices())
    call_result = models.CharField(max_length=3, blank=True,
                                   default=CallResults.UNKNOWN,
                                   choices=CallResults.choices())
    see_notes = models.BooleanField(blank=True, default=False)
    call_group = models.ForeignKey("call_time.CallTimeContactSet",
                                   related_name="call_logs",
                                   blank=True, null=True,
                                   on_delete=models.SET_NULL)
    pledged = models.PositiveIntegerField(blank=True, default=0)
    call_time = models.PositiveIntegerField(
                        blank=True, default=0,
                        help_text="Elapsed seconds on the call")
    # If we copied this Log from another Log, typically during a contact merge,
    # we will keep record of that in case the merge needs to be undone.
    copied_from = models.ForeignKey("self", on_delete=models.CASCADE, db_index=False, related_name="+",
                                    blank=True, null=True, default=None)

    class Meta:
        ordering = ("-created",)

    @property
    def call_result_label(self):
        # Reformat the call result to be more human readable
        t_call_result = self.CallResults.translate(
            self.call_result, "").replace("_", " ").title()

        # If this is a pleded log, add the pledged amount
        if self.call_result == self.CallResults.PLEDGED and self.pledged:
            t_call_result = "{} - ${:,}".format(t_call_result, self.pledged)
        return t_call_result


class CallTimeSeat(models.Model):
    """Through table between CallTimeContactSet and Seat"""
    contact_set = models.ForeignKey('call_time.CallTimeContactSet', on_delete=models.CASCADE)
    seat = models.ForeignKey('seat.Seat', on_delete=models.CASCADE)
    shared_dt = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ("-shared_dt",)


class CallTimeContactSet(JoinedContactSet, LockingModelMixin):
    contacts = models.ManyToManyField("call_time.CallTimeContact",
                                      related_name="call_time_contact_sets")
    shared_with = models.ManyToManyField('seat.Seat', blank=True,
                                         related_name="call_time_contact_sets",
                                         through=CallTimeSeat)
    contact_ordering = fields.ArrayField(
        models.PositiveIntegerField(), default=[], blank=True)

    def __getitem__(self, index):
        """Get the contact from the 'index' position in the ordering
        :returns: CallTimeContact, a slice of a queryset on CallTimeContact or
        raises IndexError
        """
        if isinstance(index, slice):
            return self.slice(index.start, index.stop,
                              self.queryset().select_related("contact"))
        def get_item():
            ctc_id = self.contact_ordering[index]
            return self.queryset().get(id=ctc_id)
        try:
            return get_item()
        except CallTimeContact.DoesNotExist:
            # Attempt to recover from a bad access. It's possible the
            # ordering array was corrupted somehow
            self.clean_ordering_and_save()
            return get_item()

    def __iter__(self):
        """Iterator for iterating over all contacts in order."""
        def generator():
            slice_size = 40
            start = 0
            end = slice_size
            while True:
                # Keep slicing the group until we run out of slices
                queryset = self.queryset().select_related("contact")
                slice_ = self.slice(start, end, queryset=queryset)
                start = end
                end += slice_size
                if not slice_:
                    raise StopIteration

                for ctc in slice_:
                    yield ctc
        return generator()

    def queryset(self, query_on=CallTimeContact, **kwargs):
        if query_on is not CallTimeContact:
            return super(CallTimeContactSet, self).queryset(
                query_on=query_on, **kwargs)
        else:
            return self.contacts.filter(archived=None)

    def slice(self, bottom, top, queryset=None):
        """Slice the queryset and order it based on contact_ordering"""
        def _get_slice():
            # Slice the contact_ordering to get the exact IDs in the page
            slice_ids_ = self.contact_ordering[bottom:top]
            if not slice_ids_:
                return [], {}
            # Drop them into a dict so we can get O(1) lookup for sorting
            slice_dict_ = {c.id: c for c in
                           queryset.filter(id__in=slice_ids_).iterator()}
            return slice_ids_, slice_dict_

        assert 0 <= bottom <= top
        if not queryset:
            queryset = self.queryset()

        slice_ids, slice_dict = _get_slice()
        # If the slice IDs don't match the queried contacts, the contact data
        # may be corrupted. We should clean it and try again.
        if len(slice_ids) != len(slice_dict):
            LOGGER.warn("Requested Call Group ({}) slice returned fewer "
                        "contacts than IDs: {}; {}".format(
                         self.id, slice_ids, slice_dict))
            self.clean_ordering_and_save()
            slice_ids, slice_dict = _get_slice()

        # Sort the queryset slice by the contact_ordering
        return [slice_dict[id_] for id_ in slice_ids if id_ in slice_dict]

    def clean_ordering_and_save(self):
        with transaction.atomic():
            self.lock()
            self.clean_ordering()
            self.save()

    def clean_ordering(self):
        """Do a sanity check on the ordering array, and repair issues."""
        # Get a list all of the call time contacts for this set
        all_contact_ids = {c_id for c_id in
                           self.contacts.filter(archived=None).values_list(
                               "id", flat=True)}
        ordering_set = set(self.contact_ordering)

        # If ordering_set is longer than all_contacts, we might have
        # archived contacts still hanging around. We want to remove those
        if len(ordering_set) > len(all_contact_ids):
            LOGGER.warn("Call Group ({}) may have archived contacts. "
                        "Cleaning them now".format(self.id))
            self.contacts.remove(*self.contacts.exclude(archived=None))

        # If the list is longer than the set, that means we have duplicates
        if len(self.contact_ordering) > len(ordering_set):
            tracker = set()
            temp = []
            # Iterate over contact_ordering and exclude duplicates.
            for c_id in self.contact_ordering:
                if c_id in tracker:
                    continue
                temp.append(c_id)
                tracker.add(c_id)
            self.contact_ordering = temp

        # Find any contacts missing from the ordering
        missing_contacts = all_contact_ids - ordering_set
        # Remove any extra IDs from the ordering
        self.contact_ordering = [c for c in self.contact_ordering
                                   if c in all_contact_ids]
        # Add the missing contacts to the end
        self.contact_ordering.extend(missing_contacts)
        # Update the count
        self.count = len(self.contact_ordering)

    def _update_contacts(self, ct_contacts, add=True):
        """Worker method to add or remove contacts from this ContactSet.

        :param ct_contacts: the call-time contacts to add or remove
        :param add: True to add, False to remove
        """
        if not ct_contacts:
            return

        with transaction.atomic():
            # Lock this model in the DB
            self.lock()

            if add:
                # Add the call time contact to this set.
                # Ignore archived call time contacts.
                self.contacts.add(*(ctc for ctc in ct_contacts
                                    if not ctc.archived))
                # Append the new contact to the ordering so it goes to the end
                self.contact_ordering.extend(c.id for c in ct_contacts)
            else:
                # Remove the call time contacts from this set
                # Add the call time contact to this set
                self.contacts.remove(*ct_contacts)
                # The clean_ordering() call below will remove the ct_contacts
                # from the ordering list.

            # Clean the ordering array. This will remove deleted ct_contacts.
            # It will also remove duplicates potentially created by add.
            self.clean_ordering()
            self.save()

    def add(self, *ct_contacts):
        """Add call time contacts to this contact set."""
        # Verify the added contacts will not exceed the size limit of the group
        if (len(ct_contacts) + self.count) > settings.MAXIMUM_CALLGROUP_SIZE:
            raise CallGroupQuotaExceededError
        self._update_contacts(ct_contacts, True)

    def add_queryset(self, ct_contact_qs):
        """Add call time contacts to this contact set with contacts provided
           by a queryset. This allows us to do a little more accurate
           count checking.
       """
        new_group_count = \
            ct_contact_qs.exclude(id__in=self.contacts.all()).count()
        if new_group_count > settings.MAXIMUM_CALLGROUP_SIZE:
            raise CallGroupQuotaExceededError
        self._update_contacts(ct_contact_qs, True)

    def remove(self, *ct_contacts):
        """Remove call time contacts from this contact set."""
        self._update_contacts(ct_contacts, False)

    def get_next(self, ct_contact):
        """Get the next contact in the ordering after the given.

        :returns: CallTimeContact or None if `ct_contact` is the end
        """
        # If given contact is None, we assume that means the front
        if ct_contact is None:
            next_index = 0
        else:
            try:
                next_index = self.contact_ordering.index(ct_contact.id) + 1
            except ValueError:
                raise ValueError("Invalid contact {}".format(ct_contact.id))
        try:
            return self[next_index]
        except IndexError:
            return None

    def order_shift(self, ct_contact, direction, places):
        """Shift the order of a contact in the ordering.

        Accepts a `direction` and `places` arguments to determine how
        many places a call-time contact should move.

        Examples:
            Move up 2 places.
             direction="up", places=2

            Move down 1 position
             direction="down", places=1
        """
        assert isinstance(places, int)
        if direction not in ("up", "down"):
            raise ValueError("Invalid direction {}".format(direction))

        with transaction.atomic():
            # Lock this model in the DB
            self.lock()
            # Clean up any issues with the ordering list before shifting.
            self.clean_ordering()

            try:
                ctc_index = self.contact_ordering.index(ct_contact.id)
            except ValueError:
                raise ValueError("Invalid contact {}".format(ct_contact.id))
            self.contact_ordering.pop(ctc_index)

            # Adjust the index to the new location
            if direction == "up":
                ctc_index -= places
                if ctc_index < 0:
                    ctc_index = 0
            else:
                ctc_index += places
            self.contact_ordering.insert(ctc_index, ct_contact.id)
            self.save()

    def order_relative(self, ct_contact, position, other_id):
        """Change the location of a contact relative to another.

        Accepts a `position` and `id` arguments to set
        the call-time contact's position relative to another in the set.

            Examples:
                Place it before ct-contact with id 10
                 position="before",  # "b" is also acceptable
                 other_id=10

                Place if at the front of the list
                 position="after",  # "a" is also acceptable
                 other_id=None

                Place if at the end of the list
                 position="before",
                 other_id=None
        """
        if position not in ("before", "b", "after", "a"):
            raise ValueError("Invalid direction {}".format(position))

        with transaction.atomic():
            # Lock this model in the DB
            self.lock()
            # Clean up any issues with the ordering list before shifting.
            self.clean_ordering()

            try:
                ctc_index = self.contact_ordering.index(ct_contact.id)
                # Remove the contact from its current spot before we find
                # the index of the other, otherwise it will be wrong.
                self.contact_ordering.pop(ctc_index)
                if other_id is not None:
                    o_index = self.contact_ordering.index(other_id)
                else:
                    o_index = None
            except ValueError:
                raise ValueError(
                    "One or both are invalid contacts: {}, {}".format(
                        ct_contact.id, other_id))

            if position in ("before", "b"):
                # If we are before None, that means we should append to the end
                if o_index is None:
                    new_index = len(self.contact_ordering)
                else:
                    new_index = o_index
                    if new_index < 0:
                        new_index = 0
            else:
                # If we are after None, that means we should insert in front
                if o_index is None:
                    new_index = 0
                else:
                    new_index = o_index + 1

            # Move the contact to its new spot
            self.contact_ordering.insert(new_index, ct_contact.id)
            self.save()

    @transaction.atomic
    def share(self, *seats):
        """Verify the CS may be shared with the seat, then share it."""

        # Get the IDs of any seats that are already share-ees.
        existing = set(CallTimeSeat.objects.filter(
            seat_id__in=(s.id for s in seats), contact_set=self).values_list(
            "seat_id", flat=True))

        ct_seats = []
        for seat in seats:
            if self.account != seat.account:
                raise ValueError(
                    "Given seat {} is not a member of this "
                    "account {}.".format(seat.id, self.account.id))

            # We don't error if a seat has already been shared with. Instead,
            # we ignore it.
            if seat.id not in existing:
                ct_seats.append(CallTimeSeat(seat=seat, contact_set=self))

        return CallTimeSeat.objects.bulk_create(ct_seats) if ct_seats else []
