
import factory

from frontend.apps.contact.factories import ContactFactory
from frontend.apps.campaign.factories import PoliticalCampaignFactory
from frontend.apps.call_time.models import (
    CallTimeContact, CallTimeContactSet, CallLog)


class CallTimeContactFactory(factory.DjangoModelFactory):
    account = factory.SubFactory(PoliticalCampaignFactory)
    contact = factory.SubFactory(
        ContactFactory, user=factory.SelfAttribute('..account.account_user'))

    class Meta:
        model = CallTimeContact


class CallTimeContactSetFactory(factory.DjangoModelFactory):
    account = factory.SubFactory(PoliticalCampaignFactory)
    user = factory.SelfAttribute("account.account_user")

    class Meta:
        model = CallTimeContactSet


class CallLogFactory(factory.DjangoModelFactory):
    ct_contact = factory.SubFactory(CallTimeContactFactory)
    log_type = CallLog.LogTypes.CALL

    class Meta:
        model = CallLog