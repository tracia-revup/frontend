
from django.conf.urls import url

from .views import CallTimeManagerView


urlpatterns = [
    url(r'^manager/?$', CallTimeManagerView.as_view(),
        name='call_time_manager'),
]