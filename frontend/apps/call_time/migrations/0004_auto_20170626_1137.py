# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-06-26 11:37


from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('call_time', '0003_auto_20170612_1849'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='calllog',
            options={'ordering': ('-created',)},
        ),
        migrations.AlterModelOptions(
            name='calltimeseat',
            options={'ordering': ('-shared_dt',)},
        ),
    ]
