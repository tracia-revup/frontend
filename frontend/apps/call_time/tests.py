from datetime import datetime
import json

from django.urls import reverse

from frontend.apps.authorize.factories import RevUpUserFactory
from frontend.apps.authorize.test_utils import (
    AuthorizedUserTestCase, TransactionAuthorizedUserTestCase, TestCase)
from frontend.apps.call_time.factories import (
    CallTimeContactFactory, CallTimeContactSetFactory)
from frontend.apps.call_time.models import (
    CallTimeContact, CallLog, CallTimeContactSet)
from frontend.apps.contact.factories import ContactFactory
from frontend.apps.contact.models import ContactNotes
from frontend.apps.role.permissions import AdminPermissions
from frontend.apps.seat.factories import SeatFactory


class CallTimeContactTests(TestCase):
    def test_delete_and_active_qs(self):
        ctc1 = CallTimeContactFactory()
        ctc2 = CallTimeContactFactory(account=ctc1.account)

        # Verify there are two call-time contacts in this account
        ct_contacts = CallTimeContact.objects.filter(account=ctc1.account)
        self.assertEqual(len(ct_contacts), 2)

        # Verify delete does not actually delete the contact
        ctc2.delete()
        ct_contacts = CallTimeContact.objects.filter(account=ctc1.account)
        self.assertEqual(len(ct_contacts), 2)

        # Verify un-archived and active return the same contact
        self.assertEqual(ct_contacts.filter(archived=None).get(),
                         CallTimeContact.objects.active().get())

    def test_delete_removes_from_cs(self):
        ctc = CallTimeContactFactory()
        ct_cs = CallTimeContactSetFactory(account=ctc.account)
        ct_cs.add(ctc)
        self.assertEqual(set(ct_cs.contacts.all()), {ctc})
        ct_cs.refresh_from_db()
        self.assertEqual(ct_cs.contact_ordering, [ctc.id])

        # Delete the call-time contact and verify it is removed from ct_cs
        ctc.delete()
        self.assertFalse(ct_cs.contacts.exists())
        ct_cs.refresh_from_db()
        self.assertEqual(ct_cs.contact_ordering, [])


class CallTimeContactSetTests(TestCase):
    def setUp(self):
        self.ct_contact_set = CallTimeContactSetFactory()
        self.ct_contact1 = CallTimeContactFactory(
            account=self.ct_contact_set.account)
        self.ct_contact2 = CallTimeContactFactory(
            account=self.ct_contact_set.account)
        self.ct_contact3 = CallTimeContactFactory(
            account=self.ct_contact_set.account)

    def test_clean_ordering(self):
        # Setup the test
        self.ct_contact_set.contact_ordering = []
        self.assertEqual(list(self.ct_contact_set.contacts.all()), [])
        self.assertEqual(self.ct_contact_set.contact_ordering, [])

        # Setup the contact set for testing the ordering
        self.ct_contact_set.contacts.add(
            self.ct_contact1, self.ct_contact2, self.ct_contact3)
        ordering = [self.ct_contact2.id, self.ct_contact3.id,
                    self.ct_contact1.id]
        self.ct_contact_set.contact_ordering = ordering

        ## Verify a good ordering is untouched
        self.ct_contact_set.clean_ordering()
        self.assertEqual(self.ct_contact_set.contact_ordering, ordering)

        ## Verify extra IDs are removed
        self.ct_contact_set.contact_ordering = \
            ordering + [(self.ct_contact3.id + 10), (self.ct_contact3.id + 20)]
        self.assertNotEqual(self.ct_contact_set.contact_ordering, ordering)
        self.ct_contact_set.clean_ordering()
        self.assertEqual(self.ct_contact_set.contact_ordering, ordering)

        ## Verify missing IDs are added
        self.ct_contact_set.contact_ordering = ordering[1:]
        self.assertNotEqual(self.ct_contact_set.contact_ordering, ordering)
        self.ct_contact_set.clean_ordering()
        # The missing ID should be on the end of the ordering
        self.assertEqual(self.ct_contact_set.contact_ordering,
                         ordering[1:3] + ordering[0:1])

        ## Verify duplicates are removed
        self.ct_contact_set.contact_ordering = ordering + [self.ct_contact3.id]
        self.assertNotEqual(self.ct_contact_set.contact_ordering, ordering)
        self.ct_contact_set.clean_ordering()
        self.assertEqual(self.ct_contact_set.contact_ordering, ordering)

        ## Verify a combination of the above
        self.ct_contact_set.contact_ordering = [
            self.ct_contact2.id, self.ct_contact3.id,
            (self.ct_contact3.id + 10), self.ct_contact2.id]
        self.assertNotEqual(self.ct_contact_set.contact_ordering, ordering)
        self.ct_contact_set.clean_ordering()
        self.assertEqual(self.ct_contact_set.contact_ordering, ordering)

    def test_archive_regression(self):
        """Verify archived ct-contacts that failed to cleanup can be
           appropriately cleaned.
       """
        # Setup the test
        self.ct_contact_set.contact_ordering = []
        self.assertEqual(list(self.ct_contact_set.contacts.all()), [])
        self.assertEqual(self.ct_contact_set.contact_ordering, [])

        contacts = [self.ct_contact1, self.ct_contact2, self.ct_contact3]
        ordering = [self.ct_contact2.id, self.ct_contact3.id,
                    self.ct_contact1.id]
        # Setup the contact set for testing the ordering
        def reset_test():
            for contact in contacts:
                contact.archived = None
                contact.save()
            self.ct_contact_set.contacts.add(*contacts)
            self.ct_contact_set.contact_ordering = ordering
            self.ct_contact3.archived = datetime.now()
            self.ct_contact3.save()
            self.assertEqual(list(self.ct_contact_set.contacts.all()),
                             contacts)
            self.assertEqual(self.ct_contact_set.contact_ordering, ordering)

        expected_contacts = [self.ct_contact1, self.ct_contact2]
        expected_ordering = [self.ct_contact2.id, self.ct_contact1.id]

        # Verify archived contact is removed during clean_ordering
        reset_test()
        self.ct_contact_set.clean_ordering()
        self.assertEqual(list(self.ct_contact_set.contacts.all()),
                         expected_contacts)
        self.assertEqual(self.ct_contact_set.contact_ordering,
                         expected_ordering)

        # Verify queryset does not return archived
        reset_test()
        self.assertEqual(list(self.ct_contact_set.queryset()),
                         expected_contacts)
        # Note the ordering has not been updated
        self.assertEqual(self.ct_contact_set.contact_ordering, ordering)

        # Verify slice cleans up
        reset_test()
        # Note, slice puts the contacts in order
        self.assertEqual(list(self.ct_contact_set.slice(0, 5)),
                         [self.ct_contact2, self.ct_contact1])
        self.assertEqual(self.ct_contact_set.contact_ordering,
                         expected_ordering)

        # Verify attempting to access a specific archived contact will clean
        # up and return a better contact
        reset_test()
        ct_contact = self.ct_contact_set[1]
        self.assertEqual(ct_contact, self.ct_contact1)
        self.assertEqual(list(self.ct_contact_set.contacts.all()),
                         expected_contacts)
        self.assertEqual(self.ct_contact_set.contact_ordering,
                         expected_ordering)

        # Verify get_next skips and cleans the archived
        reset_test()
        ct_contact = self.ct_contact_set.get_next(self.ct_contact2)
        self.assertEqual(ct_contact, self.ct_contact1)
        self.assertEqual(list(self.ct_contact_set.contacts.all()),
                         expected_contacts)
        self.assertEqual(self.ct_contact_set.contact_ordering,
                         expected_ordering)

        # Verify add ignores archived
        self.ct_contact_set.add(self.ct_contact3)
        self.assertEqual(list(self.ct_contact_set.contacts.all()),
                         expected_contacts)
        self.assertEqual(self.ct_contact_set.contact_ordering,
                         expected_ordering)

    def test_add(self):
        # Setup the test
        self.ct_contact_set.contact_ordering = []
        self.assertEqual(list(self.ct_contact_set.contacts.all()), [])
        self.assertEqual(self.ct_contact_set.contact_ordering, [])

        # Verify a simple add
        self.ct_contact_set.add(self.ct_contact1)
        self.assertEqual(list(self.ct_contact_set.contacts.all()),
                         [self.ct_contact1])
        self.assertEqual(self.ct_contact_set.contact_ordering,
                         [self.ct_contact1.id])

        # Verify multiple add
        self.ct_contact_set.add(self.ct_contact2, self.ct_contact3)
        self.assertEqual(list(self.ct_contact_set.contacts.all()),
                         [self.ct_contact1, self.ct_contact2, self.ct_contact3])
        self.assertEqual(self.ct_contact_set.contact_ordering,
                         [self.ct_contact1.id, self.ct_contact2.id,
                          self.ct_contact3.id])

        # Verify adding contact already in set
        self.ct_contact_set.add(self.ct_contact1)
        self.assertEqual(list(self.ct_contact_set.contacts.all()),
                         [self.ct_contact1, self.ct_contact2,
                          self.ct_contact3])
        self.assertEqual(self.ct_contact_set.contact_ordering,
                         [self.ct_contact1.id, self.ct_contact2.id,
                          self.ct_contact3.id])

    def test_remove(self):
        # Setup the test
        self.ct_contact_set.contact_ordering = []
        self.assertEqual(list(self.ct_contact_set.contacts.all()), [])
        self.assertEqual(self.ct_contact_set.contact_ordering, [])

        # Setup the contact set for the test
        self.ct_contact_set.add(self.ct_contact1, self.ct_contact2,
                                self.ct_contact3)
        self.assertEqual(list(self.ct_contact_set.contacts.all()),
                         [self.ct_contact1, self.ct_contact2,
                          self.ct_contact3])
        self.assertEqual(self.ct_contact_set.contact_ordering,
                         [self.ct_contact1.id, self.ct_contact2.id,
                          self.ct_contact3.id])

        # Remove one
        self.ct_contact_set.remove(self.ct_contact2)
        self.assertEqual(list(self.ct_contact_set.contacts.all()),
                         [self.ct_contact1, self.ct_contact3])
        self.assertEqual(self.ct_contact_set.contact_ordering,
                         [self.ct_contact1.id, self.ct_contact3.id])

        # Remove multiple
        self.ct_contact_set.remove(self.ct_contact1, self.ct_contact3)
        self.assertEqual(list(self.ct_contact_set.contacts.all()), [])
        self.assertEqual(self.ct_contact_set.contact_ordering, [])

    def test_order_shift(self):
        # Setup the test
        self.ct_contact_set.contact_ordering = []
        self.assertEqual(list(self.ct_contact_set.contacts.all()), [])
        self.assertEqual(self.ct_contact_set.contact_ordering, [])

        standard_order = [self.ct_contact1.id, self.ct_contact2.id,
                          self.ct_contact3.id]

        # Setup the contact set for the test
        self.ct_contact_set.add(self.ct_contact1, self.ct_contact2,
                                self.ct_contact3)
        self.assertEqual(list(self.ct_contact_set.contacts.all()),
                         [self.ct_contact1, self.ct_contact2,
                          self.ct_contact3])
        self.assertEqual(self.ct_contact_set.contact_ordering, standard_order)

        # Verify a middle shift up
        self.ct_contact_set.order_shift(self.ct_contact2, "up", 1)
        self.assertEqual(self.ct_contact_set.contact_ordering,
                         [self.ct_contact2.id, self.ct_contact1.id,
                          self.ct_contact3.id])
        # Verify a middle shift down
        self.ct_contact_set.order_shift(self.ct_contact1, "down", 1)
        self.assertEqual(self.ct_contact_set.contact_ordering,
                         [self.ct_contact2.id, self.ct_contact3.id,
                          self.ct_contact1.id])

        # Verify front shift up does nothing
        self.ct_contact_set.order_shift(self.ct_contact2, "up", 1)
        self.assertEqual(self.ct_contact_set.contact_ordering,
                         [self.ct_contact2.id, self.ct_contact3.id,
                          self.ct_contact1.id])
        # Verify end shift down does nothing
        self.ct_contact_set.order_shift(self.ct_contact1, "down", 1)
        self.assertEqual(self.ct_contact_set.contact_ordering,
                         [self.ct_contact2.id, self.ct_contact3.id,
                          self.ct_contact1.id])

          # Verify multi-place shift
        self.ct_contact_set.order_shift(self.ct_contact1, "up", 2)
        self.assertEqual(self.ct_contact_set.contact_ordering, standard_order)
        # Verify zero shift
        self.ct_contact_set.order_shift(self.ct_contact1, "up", 0)
        self.assertEqual(self.ct_contact_set.contact_ordering, standard_order)

    def test_order_relative(self):
        # Setup the test
        self.ct_contact_set.contact_ordering = []
        self.assertEqual(list(self.ct_contact_set.contacts.all()), [])
        self.assertEqual(self.ct_contact_set.contact_ordering, [])

        standard_order = [self.ct_contact1.id, self.ct_contact2.id,
                          self.ct_contact3.id]

        # Setup the contact set for the test
        self.ct_contact_set.add(self.ct_contact1, self.ct_contact2,
                                self.ct_contact3)
        self.assertEqual(list(self.ct_contact_set.contacts.all()),
                         [self.ct_contact1, self.ct_contact2,
                          self.ct_contact3])
        self.assertEqual(self.ct_contact_set.contact_ordering, standard_order)

        # Verify after other
        self.ct_contact_set.order_relative(self.ct_contact1, "after",
                                           self.ct_contact2.id)
        self.assertEqual(self.ct_contact_set.contact_ordering,
                         [self.ct_contact2.id, self.ct_contact1.id,
                          self.ct_contact3.id])

        # Verify before other
        self.ct_contact_set.order_relative(self.ct_contact1, "before",
                                           self.ct_contact2.id)
        self.assertEqual(self.ct_contact_set.contact_ordering, standard_order)

        # Verify after None
        self.ct_contact_set.order_relative(self.ct_contact2, "after", None)
        self.assertEqual(self.ct_contact_set.contact_ordering,
                         [self.ct_contact2.id, self.ct_contact1.id,
                          self.ct_contact3.id])
        # Verify before None
        self.ct_contact_set.order_relative(self.ct_contact2, "before", None)
        self.assertEqual(self.ct_contact_set.contact_ordering,
                         [self.ct_contact1.id, self.ct_contact3.id,
                          self.ct_contact2.id])

        # Verify moving multiple spots
        ct_contact4 = CallTimeContactFactory(
            account=self.ct_contact_set.account)
        self.ct_contact_set.contact_ordering = standard_order
        self.ct_contact_set.add(ct_contact4)
        self.assertEqual(self.ct_contact_set.contact_ordering,
                         [self.ct_contact1.id, self.ct_contact2.id,
                          self.ct_contact3.id] + [ct_contact4.id])
        self.ct_contact_set.order_relative(ct_contact4, "before",
                                           self.ct_contact2.id)
        self.assertEqual(self.ct_contact_set.contact_ordering,
                         [self.ct_contact1.id, ct_contact4.id,
                          self.ct_contact2.id, self.ct_contact3.id])
        self.ct_contact_set.order_relative(self.ct_contact1, "before",
                                           self.ct_contact3.id)
        self.assertEqual(self.ct_contact_set.contact_ordering,
                         [ct_contact4.id, self.ct_contact2.id,
                          self.ct_contact1.id, self.ct_contact3.id])

    def test_share(self):
        bad_seat1 = SeatFactory()
        bad_seat2 = SeatFactory(account=self.ct_contact_set.account,
                               user=self.ct_contact_set.user,
                               permissions=[])
        good_seat = SeatFactory(
                        account=self.ct_contact_set.account,
                        user=self.ct_contact_set.user,
                        permissions=[AdminPermissions.MODIFY_CALLGROUPS])

        # Verify non-account members may not be added
        with self.assertRaisesMessage(ValueError,
                                      "not a member of this account"):
            self.ct_contact_set.share(bad_seat1)

        # Verify adding a seat
        ct_seats = self.ct_contact_set.share(good_seat)
        self.assertEqual(len(ct_seats), 1)
        ct_seat = ct_seats[0]
        self.assertEqual(ct_seat.seat, good_seat)
        self.assertEqual(ct_seat.contact_set, self.ct_contact_set)
        self.assertIsNotNone(ct_seat.shared_dt)
        self.assertEqual(self.ct_contact_set.calltimeseat_set.count(), 1)

        # Verify adding the same seat doesn't change anything
        ct_seats = self.ct_contact_set.share(good_seat)
        self.assertEqual(len(ct_seats), 0)
        self.assertEqual(self.ct_contact_set.calltimeseat_set.count(), 1)

    def test_get_next(self):
        # Setup the contact set
        ordering = [self.ct_contact1, self.ct_contact2, self.ct_contact3]
        self.ct_contact_set.add(*ordering)
        self.assertEqual(self.ct_contact_set.slice(0, 3), ordering)

        # Verify all of the `get_next`s
        ctc = self.ct_contact_set.get_next(None)
        self.assertEqual(ctc, self.ct_contact1)
        ctc = self.ct_contact_set.get_next(self.ct_contact1)
        self.assertEqual(ctc, self.ct_contact2)
        ctc = self.ct_contact_set.get_next(self.ct_contact2)
        self.assertEqual(ctc, self.ct_contact3)
        ctc = self.ct_contact_set.get_next(self.ct_contact3)
        self.assertEqual(ctc, None)


class CallTimeContactViewSetTests(TransactionAuthorizedUserTestCase):
    def setUp(self, **kwargs):
        super(CallTimeContactViewSetTests, self).setUp(login_now=False,
                                                       **kwargs)
        self.account = self.user.current_seat.account
        self.contact1 = ContactFactory(user=self.account.account_user)
        self.url = reverse("call_time_contacts_api-list",
                           args=(self.account.id,))

    def test_permissions(self):
        ## Verify any unauthorized user is rejected
        result = self.client.get(self.url)
        self.assertNoCredentials(result)

        # Login the user
        self._login(self.user)
        # Verify user is not an admin, or that would invalidate the test.
        self.assertFalse(self.user.is_admin)

        ## Verify a standard user is rejected from querying
        result = self.client.get(self.url)
        self.assertNoPermission(result)

        ## Add incorrect permissions and verify
        self._add_admin_group(permissions=AdminPermissions.CONFIG_FEATURES)
        result = self.client.get(self.url)
        self.assertNoPermission(result)

        # Add the correct permissions
        self._add_admin_group(permissions=AdminPermissions.MODIFY_CALLTIME)
        result = self.client.get(self.url)
        # Verify access is granted
        self.assertContains(result, "results")

    def test_create(self):
        self._login_and_add_admin_group(self.user,
                                        AdminPermissions.MODIFY_CALLTIME)

        # Verify we can create a new call-time contact
        self.assertEqual(
            list(CallTimeContact.objects.filter(contact=self.contact1)), [])
        response = self.client.post(self.url,
                                    data={"contact": self.contact1.id})
        ct_contact1 = CallTimeContact.objects.get(contact=self.contact1)
        self.assertContains(response, '"id":{}'.format(ct_contact1.id))
        self.assertContains(response, '"id":{}'.format(self.contact1.id))
        self.assertEqual(ct_contact1.contact, self.contact1)

        # Verify the same contact cannot be created again
        response = self.client.post(self.url,
                                    data={"contact": self.contact1.id})
        self.assertContains(response,
                            "CallTime record already exists for contact",
                            status_code=500)

        # Verify a deleted ct contact can be brought back
        self.assertFalse(ct_contact1.archived)
        ct_contact1.delete()
        ct_contact1.refresh_from_db()
        self.assertTrue(ct_contact1.archived)

        # Verify the deleted contact is brought back
        old_id = ct_contact1.id
        response = self.client.post(self.url,
                                    data={"contact": self.contact1.id})
        ct_contact1 = CallTimeContact.objects.get(contact=self.contact1)
        self.assertContains(response, '"id":{}'.format(ct_contact1.id))
        self.assertContains(response, '"id":{}'.format(self.contact1.id))
        self.assertEqual(old_id, ct_contact1.id)
        self.assertFalse(ct_contact1.archived)

    def test_destroy(self):
        self._login_and_add_admin_group(self.user,
                                        AdminPermissions.MODIFY_CALLTIME)
        ct_contact = CallTimeContactFactory(contact=self.contact1,
                                            account=self.account)
        # Verify ct contact is in the results to validate the test
        response = self.client.get(self.url)
        self.assertContains(response, '"id":{}'.format(ct_contact.id))

        # Verify ct contact is archived when deleted
        url = reverse("call_time_contacts_api-detail",
                      args=(self.account.id, ct_contact.id))
        response = self.client.delete(url)
        self.assertContains(response, "", status_code=204)
        ct_contact.refresh_from_db()
        self.assertTrue(ct_contact.archived)

        # Verify ct contact is not included in results
        response = self.client.get(self.url)
        self.assertNotContains(response, '"id":{}'.format(ct_contact.id))

    def test_update(self):
        self._login_and_add_admin_group(self.user,
                                        AdminPermissions.MODIFY_CALLTIME)
        ct_contact = CallTimeContactFactory(contact=self.contact1,
                                            account=self.account)
        phone = ct_contact.contact.phone_numbers.first()
        url = reverse("call_time_contacts_api-detail",
                      args=(self.account.id, ct_contact.id))

        # Verify API errs correctly
        response = self.client.put(url, content_type='application/json',
                                   data=json.dumps({"contact": 10}))
        self.assertContains(response, "'primary_phone' or 'primary_email' "
                                      "IDs are required", status_code=500)

        # Verify ct contact is in the results to validate the test
        response = self.client.get(url)
        self.assertContains(response, '"id":{}'.format(ct_contact.id))
        self.assertContains(response, '"primary_phone":null')
        self.assertContains(response, '"primary_email":null')

        response = self.client.put(url, content_type='application/json',
                                   data=json.dumps({"primary_phone": phone.id}))
        self.assertContains(response, "")

        # Verify primary phone is set
        response = self.client.get(url)
        self.assertContains(response, '"id":{}'.format(ct_contact.id))
        self.assertContains(response, '"primary_phone":{}'.format(phone.id))

        # Parse the json and verify the correct phone is flagged as primary
        r_json = json.loads(response.content)
        for pn in r_json['contact']['phone_numbers']:
            if pn["id"] == phone.id:
                self.assertTrue(pn["primary"])
            else:
                self.assertFalse(pn.get("primary"))


class CallGroupViewSetTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        super(CallGroupViewSetTests, self).setUp(login_now=False, **kwargs)
        self.account = self.user.current_seat.account
        self.url = reverse("call_time_groups_api-list",
                           args=(self.account.id,))

    def test_permissions(self):
        ## Verify any unauthorized user is rejected
        result = self.client.get(self.url)
        self.assertNoCredentials(result)

        # Login the user
        self._login(self.user)
        # Verify user is not an admin, or that would invalidate the test.
        self.assertFalse(self.user.is_admin)

        ## Verify a standard user is rejected from querying
        result = self.client.get(self.url)
        self.assertNoPermission(result)

        ## Add incorrect permissions and verify
        self._add_admin_group(permissions=AdminPermissions.MODIFY_CALLTIME)
        result = self.client.get(self.url)
        self.assertNoPermission(result)

        # Add the correct permissions
        self._add_admin_group(permissions=AdminPermissions.MODIFY_CALLGROUPS)
        result = self.client.get(self.url)
        # Verify access is granted
        self.assertContains(result, "results")

    def test_create(self):
        # Setup the test
        self._login_and_add_admin_group(self.user,
                                        AdminPermissions.MODIFY_CALLGROUPS)

        self.assertFalse(CallTimeContactSet.objects.filter(
                            account=self.account).exists())
        response = self.client.post(self.url, data={"title": "test-title"})
        self.assertContains(response, '"title":"test-title"', status_code=201)
        ctcs = CallTimeContactSet.objects.get(account=self.account)
        self.assertEqual(ctcs.account, self.account)
        self.assertEqual(ctcs.user, self.account.account_user)

    def test_destroy(self):
        # Setup the test
        self._login_and_add_admin_group(self.user,
                                        AdminPermissions.MODIFY_CALLGROUPS)
        ct_cs = CallTimeContactSetFactory(account=self.account)
        detail_url = reverse("call_time_groups_api-detail",
                             args=(self.account.id, ct_cs.id))
        self.assertEqual(ct_cs.archived, None)

        # Verify call group is in results before delete
        response = self.client.get(self.url)
        self.assertContains(response, '"id":{}'.format(ct_cs.id))

        # Verify we can delete a call time contact set
        response = self.client.delete(detail_url)
        self.assertContains(response, "", status_code=204)

        # Verify it is not actually deleted, but archived
        ct_cs.refresh_from_db()
        self.assertNotEqual(ct_cs.archived, None)

        # Verify it is gone
        response = self.client.get(detail_url)
        self.assertContains(response, "", status_code=404)
        response = self.client.get(self.url)
        self.assertNotContains(response, '"id":{}'.format(ct_cs.id))


class SeatCallGroupViewSetTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        super(SeatCallGroupViewSetTests, self).setUp(login_now=False,
                                                          **kwargs)
        self.seat = self.user.current_seat
        self.url = reverse("seat_call_time_groups_api-list",
                           args=(self.user.id, self.seat.id))

    def test_permissions(self):
        ## Verify any unauthorized user is rejected
        result = self.client.get(self.url)
        self.assertNoCredentials(result)

        # Login the user
        self._login(self.user)
        # Verify user is not an admin.
        self.assertFalse(self.user.is_admin)

        ## Verify a different user is rejected from querying
        user2 = RevUpUserFactory(import_config=None)
        url2 = reverse("seat_call_time_groups_api-list",
                       args=(user2.id, self.seat.id))
        response = self.client.get(url2)
        self.assertEqual(response.status_code, 403)

        ## Verify the user can access their own call groups
        result = self.client.get(self.url)
        # Verify access is granted
        self.assertContains(result, '"results":[]')

    def test_get(self):
        # Login the user
        self._login(self.user)
        # Verify results are empty.
        result = self.client.get(self.url)
        self.assertContains(result, '"results":[]')

        # Share a call time group with this user and verify it is in the api
        call_group = CallTimeContactSetFactory(account=self.seat.account)
        call_group.share(self.seat)

        ## Verify the call group is in response
        result = self.client.get(self.url)
        # Verify access is granted
        self.assertContains(result, '"id":{}'.format(call_group.id))


class SeatCallGroupContactsViewSetTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        super(SeatCallGroupContactsViewSetTests, self).setUp(login_now=False,
                                                             **kwargs)
        self.seat = self.user.current_seat
        self.call_group = CallTimeContactSetFactory(account=self.seat.account)
        self.call_group.share(self.seat)
        self.url = reverse("seat_call_time_groups_contacts_api-list",
                           args=(self.user.id, self.seat.id,
                                 self.call_group.id))

    def test_permissions(self):
        ## Verify any unauthorized user is rejected
        result = self.client.get(self.url)
        self.assertNoCredentials(result)

        # Login the user
        self._login(self.user)
        # Verify user is not an admin.
        self.assertFalse(self.user.is_admin)

        ## Verify a different user is rejected from querying
        seat2 = SeatFactory(account=self.seat.account)
        url2 = reverse("seat_call_time_groups_contacts_api-list",
                       args=(seat2.user.id, self.seat.id, self.call_group.id))
        response = self.client.get(url2)
        self.assertEqual(response.status_code, 404)

        ## Verify a user that hasn't been shared with is rejected
        seat2 = SeatFactory(account=self.seat.account)
        url2 = reverse("seat_call_time_groups_contacts_api-list",
                       args=(seat2.user.id, seat2.id, self.call_group.id))
        response = self.client.get(url2)
        self.assertNoPermission(response)

        ## Verify even admins don't have access
        # Add the Admin permissions and verify access is still not granted
        self._add_admin_group(user=seat2.user,
                              permissions=AdminPermissions.MODIFY_CALLGROUPS)
        result = self.client.get(url2)
        # Verify access is granted
        self.assertNoPermission(result)

        ## Verify the user can access their own call group's contacts
        result = self.client.get(self.url)
        # Verify access is granted
        self.assertContains(result, '"results":[]')

    def test_get(self):
        # Login the user
        self._login(self.user)
        # Verify results are empty.
        response = self.client.get(self.url)
        self.assertContains(response, '"results":[]')

        # Add contacts and verify their presence
        ctc1 = CallTimeContactFactory(account=self.seat.account)
        ctc2 = CallTimeContactFactory(account=self.seat.account)
        self.call_group.add(ctc1)

        response = self.client.get(self.url)
        self.assertContains(response, '"id":{}'.format(ctc1.id))
        self.assertNotContains(response, '"id":{}'.format(ctc2.id))


class CallGroupContactsViewSetTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        super(CallGroupContactsViewSetTests, self).setUp(
            login_now=False, **kwargs)
        self.account = self.user.current_seat.account
        self.ct_contact1 = CallTimeContactFactory(account=self.account)
        self.ct_contact2 = CallTimeContactFactory(account=self.account)
        self.ct_contact3 = CallTimeContactFactory(account=self.account)
        self.ct_cs = CallTimeContactSetFactory(account=self.account)
        self.url = reverse("call_time_groups_contacts_api-list",
                           args=(self.account.id, self.ct_cs.id))
        self.detail_url = reverse("call_time_groups_contacts_api-detail",
                                  args=(self.account.id, self.ct_cs.id,
                                        self.ct_contact1.id))

    def test_permissions(self):
        ## Verify any unauthorized user is rejected
        result = self.client.get(self.url)
        self.assertNoCredentials(result)

        # Login the user
        self._login(self.user)
        # Verify user is not an admin, or that would invalidate the test.
        self.assertFalse(self.user.is_admin)

        self.ct_cs.add(self.ct_contact1)
        next_url = self.detail_url + "get_next/"

        ## Verify a standard user is rejected from querying
        result = self.client.get(self.url)
        self.assertNoPermission(result)
        result = self.client.get(next_url)
        self.assertNoPermission(result)

        ## Add incorrect permissions and verify
        self._add_admin_group(permissions=AdminPermissions.MODIFY_CALLTIME)
        result = self.client.get(self.url)
        self.assertNoPermission(result)
        result = self.client.get(next_url)
        self.assertNoPermission(result)

        # Add the correct permissions
        self._add_admin_group(permissions=AdminPermissions.MODIFY_CALLGROUPS)
        result = self.client.get(self.url)
        # Verify access is granted
        self.assertContains(result, "results")
        result = self.client.get(next_url)
        self.assertEqual(result.status_code, 200)

    def test_create(self):
        self._login_and_add_admin_group(self.user,
                                        AdminPermissions.MODIFY_CALLGROUPS)

        # Verify we can create a new call-time contact
        self.assertEqual(
            self.ct_cs.contacts.filter(id=self.ct_contact1.id).first(), None)
        response = self.client.post(self.url, data={"id": self.ct_contact1.id})
        self.assertContains(response, "")
        self.assertEqual(self.ct_cs.contacts.get(id=self.ct_contact1.id),
                         self.ct_contact1)
        self.ct_cs.refresh_from_db()
        self.assertEqual(self.ct_cs.contact_ordering, [self.ct_contact1.id])

        ## Verify adding another contact appends to the end of the ordering
        response = self.client.post(self.url, data={"id": self.ct_contact2.id})
        self.assertContains(response, "")
        self.assertEqual(self.ct_cs.contacts.get(id=self.ct_contact2.id),
                         self.ct_contact2)
        self.ct_cs.refresh_from_db()
        self.assertEqual(self.ct_cs.contact_ordering,
                         [self.ct_contact1.id, self.ct_contact2.id])

        ## Verify adding the same contact does not change anything
        response = self.client.post(self.url, data={"id": self.ct_contact1.id})
        self.assertContains(response, "")
        self.assertEqual(set(self.ct_cs.contacts.all()),
                         {self.ct_contact1, self.ct_contact2})
        self.ct_cs.refresh_from_db()
        self.assertEqual(self.ct_cs.contact_ordering,
                         [self.ct_contact1.id, self.ct_contact2.id])

        ## Verify create can create multiple
        ct_contact4 = CallTimeContactFactory(account=self.account)
        response = self.client.post(self.url, content_type='application/json',
                                    data=json.dumps(
                                        {"id": [self.ct_contact3.id,
                                                ct_contact4.id]}))
        self.assertContains(response, "")
        self.assertEqual(set(self.ct_cs.contacts.all()),
                         {self.ct_contact1, self.ct_contact2,
                          self.ct_contact3, ct_contact4})
        self.ct_cs.refresh_from_db()
        self.assertEqual(self.ct_cs.contact_ordering,
                         [self.ct_contact1.id, self.ct_contact2.id,
                          self.ct_contact3.id, ct_contact4.id])

    def test_destroy(self):
        # Setup the test
        self._login_and_add_admin_group(self.user,
                                        AdminPermissions.MODIFY_CALLGROUPS)
        self.ct_cs.add(self.ct_contact1, self.ct_contact2)
        self.assertEqual(set(self.ct_cs.contacts.all()),
                         {self.ct_contact1, self.ct_contact2})
        self.ct_cs.refresh_from_db()
        self.assertEqual(self.ct_cs.contact_ordering,
                         [self.ct_contact1.id, self.ct_contact2.id])

        # Verify delete
        response = self.client.delete(self.detail_url)
        self.assertContains(response, "", status_code=204)

        self.assertEqual(list(self.ct_cs.contacts.all()), [self.ct_contact2])
        self.ct_cs.refresh_from_db()
        self.assertEqual(self.ct_cs.contact_ordering, [self.ct_contact2.id])

    def test_ordering(self):
        # Setup the test
        self._login_and_add_admin_group(self.user,
                                        AdminPermissions.MODIFY_CALLGROUPS)
        self.ct_cs.add(self.ct_contact1, self.ct_contact2, self.ct_contact3)
        self.assertEqual(set(self.ct_cs.contacts.all()),
                         {self.ct_contact1, self.ct_contact2, self.ct_contact3})
        self.ct_cs.refresh_from_db()
        self.assertEqual(self.ct_cs.contact_ordering,
                         [self.ct_contact1.id, self.ct_contact2.id,
                          self.ct_contact3.id])

        # Verify we can change ordering using "shift" mode
        response = self.client.put(
            self.detail_url + "ordering/", content_type='application/json',
            data=json.dumps({"mode": "shift", "direction": "down",
                             "places": 1}))
        self.assertContains(response, "")
        self.ct_cs.refresh_from_db()
        self.assertEqual(set(self.ct_cs.contacts.all()),
                         {self.ct_contact1, self.ct_contact2,
                          self.ct_contact3})
        self.assertEqual(self.ct_cs.contact_ordering,
                         [self.ct_contact2.id, self.ct_contact1.id,
                          self.ct_contact3.id])

        # Verify we can change ordering using "relative" mode
        response = self.client.put(
            self.detail_url + "ordering/", content_type='application/json',
            data=json.dumps(
                {"mode": "relative", "position": "before",
                 "id": self.ct_contact2.id}))
        self.assertContains(response, "")
        self.ct_cs.refresh_from_db()
        self.assertEqual(set(self.ct_cs.contacts.all()),
                         {self.ct_contact1, self.ct_contact2,
                          self.ct_contact3})
        self.assertEqual(self.ct_cs.contact_ordering,
                         [self.ct_contact1.id, self.ct_contact2.id,
                          self.ct_contact3.id])

    def test_pagination(self):
        # Setup the test
        self._login_and_add_admin_group(self.user,
                                        AdminPermissions.MODIFY_CALLGROUPS)
        self.ct_cs.add(self.ct_contact1, self.ct_contact2, self.ct_contact3)
        self.assertEqual(set(self.ct_cs.contacts.all()),
                         {self.ct_contact1, self.ct_contact2,
                          self.ct_contact3})
        self.ct_cs.contact_ordering = [
            self.ct_contact3.id, self.ct_contact1.id, self.ct_contact2.id]
        self.ct_cs.save()

        ## Verify various paging scenarios
        response = self.client.get(self.url, data={"page": 3, "page_size": 1})
        self.assertContains(response, '"id":{}'.format(self.ct_contact2.id))
        self.assertNotContains(response, '"id":{}'.format(self.ct_contact3.id))
        self.assertNotContains(response, '"id":{}'.format(self.ct_contact1.id))

        response = self.client.get(self.url, data={"page": 1, "page_size": 1})
        self.assertContains(response, '"id":{}'.format(self.ct_contact3.id))
        self.assertNotContains(response, '"id":{}'.format(self.ct_contact2.id))
        self.assertNotContains(response, '"id":{}'.format(self.ct_contact1.id))

        response = self.client.get(self.url, data={"page": 1, "page_size": 2})
        self.assertNotContains(response, '"id":{}'.format(self.ct_contact2.id))
        self.assertContains(response, '"id":{}'.format(self.ct_contact3.id))
        self.assertContains(response, '"id":{}'.format(self.ct_contact1.id))

        response = self.client.get(self.url, data={"page": 2, "page_size": 2})
        self.assertContains(response, '"id":{}'.format(self.ct_contact2.id))
        self.assertNotContains(response, '"id":{}'.format(self.ct_contact3.id))
        self.assertNotContains(response, '"id":{}'.format(self.ct_contact1.id))

        response = self.client.get(self.url, data={"page": 3, "page_size": 2})
        self.assertContains(response, '"That page contains no results"',
                            status_code=404)


class SeatCallLogViewSetTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        super(SeatCallLogViewSetTests, self).setUp(login_now=False, **kwargs)
        self.seat = self.user.current_seat
        self.call_group = CallTimeContactSetFactory(account=self.seat.account)
        self.call_group.share(self.seat)
        self.ct_contact = CallTimeContactFactory(account=self.seat.account)
        self.call_group.add(self.ct_contact)
        self.url = reverse("call_time_groups_call_log_api-list",
                           args=(self.user.id, self.seat.id,
                                 self.call_group.id, self.ct_contact.id))

    def test_permissions(self):
        ## Verify any unauthorized user is rejected
        result = self.client.get(self.url)
        self.assertNoCredentials(result)

        # Login the user
        self._login(self.user)
        # Verify user is not an admin.
        self.assertFalse(self.user.is_admin)

        ## Verify a different user is rejected from querying
        seat2 = SeatFactory(account=self.seat.account)
        url2 = reverse("seat_call_time_groups_contacts_api-list",
                       args=(seat2.user.id, self.seat.id, self.call_group.id))
        response = self.client.get(url2)
        self.assertEqual(response.status_code, 404)

        ## Verify a user that hasn't been shared with is rejected
        seat2 = SeatFactory(account=self.seat.account)
        url2 = reverse("seat_call_time_groups_contacts_api-list",
                       args=(seat2.user.id, seat2.id, self.call_group.id))
        response = self.client.get(url2)
        self.assertNoPermission(response)

        ## Verify even admins don't have access
        # Add the Admin permissions and verify access is still not granted
        self._add_admin_group(user=seat2.user,
                              permissions=AdminPermissions.MODIFY_CALLGROUPS)
        result = self.client.get(url2)
        # Verify access is granted
        self.assertNoPermission(result)

        ## Verify the user can access their own call group's contacts
        result = self.client.get(self.url)
        # Verify access is granted
        self.assertContains(result, '"results":[]')

    def test_create(self):
        # Login the user
        self._login(self.user)

        self.assertFalse(CallLog.objects.filter(
                                ct_contact=self.ct_contact).exists())
        data = {
            "log_type": "CAL",
            "call_back": True,
            "call_result": "REF",
            "see_notes": True,
            "notes": {"notes1": "testnotes"}
        }
        response = self.client.post(self.url, data=json.dumps(data),
                                    content_type="application/json")
        self.assertContains(response,
                            '"ct_contact":{}'.format(self.ct_contact.id),
                            status_code=201)
        log = CallLog.objects.get(ct_contact=self.ct_contact)
        self.assertEqual(log.ct_contact, self.ct_contact)
        self.assertEqual(log.log_type, "CAL")
        self.assertEqual(log.call_result, "REF")
        self.assertEqual(log.call_group, self.call_group)
        self.assertEqual(log.pledged, 0)
        self.assertEqual(log.call_time, 0)
        self.assertTrue(log.see_notes)
        self.assertIsNotNone(log.call_back)
        # Verify notes were set
        notes = ContactNotes.objects.get(contact=self.ct_contact.contact)
        self.assertEqual(notes.notes1, "testnotes")

        # Verify attempting to save the notes again without an ID fails
        response = self.client.post(self.url, data=json.dumps(data),
                                    content_type="application/json")
        self.assertContains(response,
                            "fields contact, account must make a unique set.",
                            status_code=400)
        # Add the ID and try again
        data["notes"] = {"id": notes.id, "notes1": "testnotes2"}
        response = self.client.post(self.url, data=json.dumps(data),
                                    content_type="application/json")
        self.assertEqual(response.status_code, 201)
        notes.refresh_from_db()
        self.assertEqual(notes.notes1, "testnotes2")
