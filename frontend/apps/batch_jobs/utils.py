
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from storages.backends.s3boto3 import S3Boto3Storage

from frontend.libs.utils.email_utils import send_user_email
from frontend.libs.utils.view_utils import build_protocol_host


def send_success_email(batch_file, email_to, email_type):
    context = dict(
        batch_file=batch_file,
        protocol_host=build_protocol_host()
    )
    lower = email_type.lower().replace(" ", "_")
    send_user_email(
        batch_file.user,
        "RevUp {} ready for download".format(email_type),
        settings.DEFAULT_FROM_EMAIL,
        "batch_jobs/emails/{}_ready_email.html".format(lower),
        context, "{} download notification email".format(lower),
        email_to=email_to)


def send_error_email(user, email_to, email_type):
    lower = email_type.lower().replace(" ", "_")
    send_user_email(user, "RevUp {} export failed".format(email_type),
                    settings.DEFAULT_FROM_EMAIL,
                    "batch_jobs/emails/{}_gen_failed.html".format(lower),
                    {}, "{} export failure notification email".format(lower),
                    email_to=email_to)


def select_storage():
    if settings.ENV in ("staging", "prod"):
        return S3Boto3Storage(bucket='revup-batch-jobs')
    else:
        return FileSystemStorage()
