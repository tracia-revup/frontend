from math import ceil

from celery import task, group, current_task
from django.conf import settings
from django.core.cache import cache
from io import BytesIO

from frontend.apps.analysis.csv_generator import CSVGenerator, select_csv_storage
from frontend.apps.analysis.models import PartitionConfig, Analysis, Result
from frontend.apps.authorize.models import RevUpUser
from frontend.apps.batch_jobs.utils import send_error_email, send_success_email
from frontend.apps.call_time.call_time_csv_generator import (
    CallGroupCSVGenerator)
from frontend.apps.call_time.models import CallTimeContactSet
from frontend.apps.campaign.models import Prospect
from frontend.apps.campaign.prospect_csv_generator import ProspectCSVGenerator
from frontend.apps.contact_set.models import ContactSet
from frontend.libs.utils.celery_utils import update_progress

# Map models to CSV generators so the appropriate generator is used
_generator_map = {
    Result: CSVGenerator,
    Prospect: ProspectCSVGenerator,
    CallTimeContactSet: CallGroupCSVGenerator,
}


@task(max_retries=20, default_retry_delay=60, queue='batch_csv')
def generate_csv_task(queryset, contact_set_id,
                      partition_id, user_id, contact_count, analysis_id=None,
                      email_to=None, filename=None, **export_options):
    """Generates a CSV file and saves it in a BatchFile record."""
    contact_set = ContactSet.objects.active().get(id=contact_set_id)
    partition = PartitionConfig.objects.get(id=partition_id)
    user = RevUpUser.objects.get(id=user_id)
    analysis = Analysis.objects.get(id=analysis_id) if analysis_id else None

    try:
        update_progress(0, 100)

        try:
            Generator = _generator_map[queryset.model]
        except KeyError:
            raise TypeError("Unknown query model: {}".format(queryset.model))

        csv_generator = Generator(contact_set, partition,
                                  analysis=analysis,
                                  user_id=user.id,
                                  update_progress=update_progress,
                                  **export_options)

        batch_size = settings.EXPORT_CSV_PARALLEL_BATCH_SIZE
        batches_count = int(ceil(contact_count / float(batch_size)))
        partial_filenames = csv_generator.partial_filenames(batches_count)

        task_arg = (csv_generator, queryset)
        if settings.BATCH_JOBS_USE_CACHE:
            cache_key = 'generate_csv_task_args__{}'.format(
                current_task.request.id)
            cache.set(cache_key, task_arg,
                      timeout=(24*60*60*2))  # 2 day timeout
            task_arg = cache_key

        (group(_parallel_generate_csv.s(task_arg,
                                        partial_filenames[index],
                                        index*batch_size,
                                        (index + 1) * batch_size)
               for index in range(batches_count)) |
         _parallel_complete_csv.si(task_arg, batches_count, filename,
                                   user, email_to)).apply_async(
            add_to_parent=True)
    except Exception as ex:
        if email_to:
            send_error_email(user, email_to, "CSV")
        raise ex


@task(max_retries=20, default_retry_delay=60, queue='batch_call_sheet')
def _parallel_generate_csv(generator, partial_filename,
                           slice_begin, slice_end):
    """Generate a csv file from a slice of the original queryset.
    Reconstruct the partial files in 'complete'.
    These are measures to prevent both OOM and timeout errors.
    """
    if isinstance(generator, tuple):
        csv_generator, queryset = generator
    else:
        # if generator isn't a tuple of csv generator and queryset, it is the
        # cache id for them.
        csv_generator, queryset = cache.get(generator)
    _queryset = queryset[slice_begin:slice_end]
    add_header = slice_begin == 0
    iter_csv = csv_generator.generate_csv(_queryset, add_header=add_header)

    storage = select_csv_storage()

    temp_file = BytesIO()
    for line in iter_csv:
        temp_file.write(line.encode('utf-8'))
    storage.save(partial_filename, temp_file)


@task(max_retries=20, default_retry_delay=60, queue='batch_call_sheet')
def _parallel_complete_csv(generator, batch_count, filename, user,
                           email_to):
    """
    Reconstruct the partial csv files and send email to user
    """
    do_delete = False
    if isinstance(generator, tuple):
        csv_generator, _ = generator
    else:
        # if generator isn't a tuple of csv generator and queryset, it is the
        # cache id for them.
        csv_generator, _ = cache.get(generator)
        do_delete = True
    (csv_export_success, batch_file_record) = csv_generator.parallel_complete(
        filename, batch_count)

    # Send the email
    if email_to:
        if csv_export_success:
            send_success_email(batch_file_record, email_to, "CSV")
        else:
            send_error_email(user, email_to, "CSV")
    if do_delete:
        cache.delete(generator)
