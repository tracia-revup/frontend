# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-08-03 23:01


from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django_extensions.db.fields
import frontend.apps.batch_jobs.models
import frontend.libs.utils.model_utils


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='BatchFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('file_name', frontend.libs.utils.model_utils.TruncatingCharField(blank=True, max_length=256)),
                ('file_type', models.CharField(choices=[('CSV', 'CSV File'), ('BCS', 'BATCH_CALL_SHEET')], default='CSV', max_length=3)),
                ('details', frontend.libs.utils.model_utils.TruncatingCharField(blank=True, max_length=512)),
                ('data_file', models.FileField(upload_to='')),
                ('expiration', models.DateField(blank=True, default=frontend.apps.batch_jobs.models._set_expiration_date, help_text='Auto-set expiration date to 30 days in the future')),
                ('downloaded', models.DateTimeField(blank=True, null=True)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='batch_files', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
        ),
    ]
