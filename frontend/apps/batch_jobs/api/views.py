from datetime import datetime
import logging

from django.shortcuts import get_object_or_404, HttpResponseRedirect
from rest_condition.permissions import And
from rest_framework import permissions, viewsets
from rest_framework.decorators import action
from rest_framework.pagination import PageNumberPagination
from rest_framework_extensions.mixins import (
    NestedViewSetMixin, DetailSerializerMixin)
from rest_framework_extensions.utils import compose_parent_pk_kwarg_name

from frontend.apps.authorize.models import RevUpUser
from frontend.apps.batch_jobs.api.serializers import (
    BatchFileSerializer, DetailedBatchFileSerializer)
from frontend.apps.batch_jobs.models import BatchFile
from frontend.libs.utils.api_utils import IsOwner, InitialMixin, APIException


LOGGER = logging.getLogger(__name__)


class BatchFileViewSet(NestedViewSetMixin, InitialMixin, DetailSerializerMixin,
                       viewsets.ReadOnlyModelViewSet):
    serializer_class = BatchFileSerializer
    serializer_detail_class = DetailedBatchFileSerializer
    permission_classes = (And(permissions.IsAuthenticated,
                              IsOwner),)
    parent_user_lookup = compose_parent_pk_kwarg_name("user_id")

    class pagination_class(PageNumberPagination):
        page_size = 20
        page_size_query_param = 'page_size'
        max_page_size = 100

    def get_queryset(self):
        return BatchFile.objects.filter(user=self.user).order_by("-created")

    def _initial(self):
        self.user = self.user = get_object_or_404(
            RevUpUser,
            id=self.kwargs.get(self.parent_user_lookup))

    @action(detail=True, methods=['get'])
    def download(self, request, **kwargs):
        batch_file = self.get_object()
        url = batch_file.url
        if not url:
            raise APIException(LOGGER,
                               "This batch file no longer exists. "
                               "A new file will need to be generated.",
                               request)
        # If this is the first time this file is downloaded, save the datetime
        if not batch_file.downloaded:
            batch_file.downloaded = datetime.now()
            batch_file.save(update_fields=["downloaded"])
        return HttpResponseRedirect(url)
