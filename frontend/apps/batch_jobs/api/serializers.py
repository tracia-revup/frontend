
from rest_framework import serializers

from frontend.apps.batch_jobs.models import BatchFile


class BatchFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = BatchFile
        fields = ("id", "user", "file_name", "file_type", "details",
                  "expiration", "downloaded", "downloadable")
        read_only_fields = fields


class DetailedBatchFileSerializer(BatchFileSerializer):
    class Meta(BatchFileSerializer.Meta):
        fields = BatchFileSerializer.Meta.fields + ("url",)
        read_only_fields = fields