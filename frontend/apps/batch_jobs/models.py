from datetime import date, timedelta
import logging

from django.conf import settings
from django.db import models
from django_extensions.db.models import TimeStampedModel
from storages.backends.s3boto3 import S3Boto3Storage

from frontend.libs.utils.model_utils import TruncatingCharField, Choices
from frontend.libs.utils.string_utils import ensure_unicode


LOGGER = logging.getLogger(__name__)


def _select_storage():
    if settings.BATCH_JOB_S3_STORAGE_ENABLED:
        return S3Boto3Storage(bucket='revup-batch-jobs')
    else:
        return None


def _set_expiration_date():
    return date.today() + timedelta(days=30)


class BatchFile(TimeStampedModel):
    class FileType(Choices):
        choices_map = [
            ("CSV", "CSV", "CSV File"),
            ("BCS", "BATCH_CALL_SHEET"),
        ]

    user = models.ForeignKey("authorize.RevUpUser", on_delete=models.CASCADE, related_name='batch_files')
    file_name = TruncatingCharField(max_length=256, blank=True)
    file_type = models.CharField(max_length=3,
                                 choices=FileType.choices(),
                                 default=FileType.CSV)
    details = TruncatingCharField(max_length=512, blank=True)
    data_file = models.FileField(storage=_select_storage())

    expiration = models.DateField(blank=True, default=_set_expiration_date,
                                  help_text="Auto-set expiration date to 30 "
                                            "days in the future")
    downloaded = models.DateTimeField(blank=True,
                                      null=True)

    @property
    def expired(self):
        return date.today() >= self.expiration

    @property
    def downloadable(self):
        return bool(self.data_file) and not self.expired

    @property
    def url(self):
        if self.downloadable:
            if self.file_name and isinstance(self.data_file.storage,
                                             S3Boto3Storage):
                # Replace unicode quotes (common in apple) with normal
                # apostrophes, remove quotes, then escape out any unicode
                try:
                    filename = "\"{}\"".format(
                        ensure_unicode(self.file_name.replace("\u2019", "'")
                                     .replace('"', "")))
                except Exception as err:
                    LOGGER.warn("Failed to generate filename for batch file "
                                "({}). Reason: {}".format(self.id, err))
                    # If anything goes wrong here, just use the datafile name
                    filename = self.data_file.name

                # The following argument tells S3 to set the name to file_name
                # instead of returning the uuid
                url = self.data_file.storage.url(
                    self.data_file.name,
                    parameters={
                        "ResponseContentDisposition":
                            "attachment; filename={}".format(filename)}
                )
            else:
                url = self.data_file.url
            return url
        else:
            return None
