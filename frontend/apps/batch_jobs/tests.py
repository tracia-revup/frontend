from datetime import date, timedelta

from django.urls import reverse

from frontend.apps.authorize.factories import RevUpUserFactory
from frontend.apps.authorize.test_utils import AuthorizedUserTestCase
from .factories import BatchFileFactory


class BatchFileViewSetTests(AuthorizedUserTestCase):
    def setUp(self, **kwargs):
        # Calling super here will create a RevUpUser and log it in.
        super(BatchFileViewSetTests, self).setUp(login_now=False)
        self.url = reverse("batch_files_api-list", args=(self.user.id,))

    def test_permissions(self):
        ## Verify the API rejects a user that is not logged in
        response = self.client.get(self.url)
        self.assertNoCredentials(response)

        # Log in
        self._login(self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

        # Verify other user cannot access
        other = RevUpUserFactory()
        url = reverse("batch_files_api-list", args=(other.id,))
        response = self.client.get(url)
        self.assertNoPermission(response)

    def test_get(self):
        # Log in
        self._login(self.user)
        batch_file = BatchFileFactory(user=self.user)
        url = reverse("batch_files_api-detail",
                      args=(self.user.id, batch_file.id))

        response = self.client.get(url)
        json = response.json()
        self.assertEqual(
            json,
            {'file_type': 'CSV', 'file_name': batch_file.file_name,
             'user': self.user.id, 'downloaded': None,
             'downloadable': False, 'details': '',
             'url': None, 'id': batch_file.id,
             'expiration': (date.today() + timedelta(days=30)).strftime(
                                                            "%Y-%m-%d")
             })
