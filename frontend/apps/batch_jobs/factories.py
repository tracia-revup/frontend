
import factory
from factory.fuzzy import FuzzyText

from frontend.apps.batch_jobs.models import BatchFile


@factory.use_strategy(factory.CREATE_STRATEGY)
class BatchFileFactory(factory.DjangoModelFactory):
    class Meta:
        model = BatchFile

    user = factory.SubFactory("frontend.apps.authorize.factories"
                              ".RevUpUserFactory")
    file_name = FuzzyText()
