from django.conf.urls import url
from rest_framework_extensions.routers import ExtendedDefaultRouter
from rest_framework_jwt.views import RefreshJSONWebToken

from frontend.apps.analysis.api.views import (
    AnalysisViewSet, PartitionConfigViewSet, AccountAnalysisConfigsViewSet,
    QuestionSetViewSet, EntitySearchAPI, QuestionSetDataViewSet,
    ContactSetAnalysisContactResultsViewSet)
from frontend.apps.authorize.api.views import (
    RevUpUserViewSet, EventUsersViewSet, RevUpTaskViewSet,
    AnalysisViewUserSettingsViewSet, ObtainJSONWebToken)
from frontend.apps.batch_jobs.api.views import BatchFileViewSet
from frontend.apps.call_time.api.views import (
    CallTimeContactViewSet, CallGroupViewSet, CallGroupContactsViewSet,
    SeatCallGroupViewSet, SeatCallGroupContactsViewSet, CallLogViewSet,
    SeatCallLogViewSet)
from frontend.apps.campaign.api.views import (
    AccountViewSet, AccountEventsViewSet, EventProspectsViewSet)
from frontend.apps.campaign_admin.api.views import (
    AccountManagementGroupsViewSet, ManagementGroupsManagersViewSet,
    ManagementGroupsMembersViewSet, ManagementGroupTrackerViewSet)
from frontend.apps.contact.api.views import (
    ContactViewSet, AccountContactViewSet, ImportRecordViewSet,
    ContactNotesViewSet, ContactSetResultsContactNotesViewSet,
    SeatContactsRedirectViewSet)
from frontend.apps.contact_set.api.views import (
    ContactSetViewSet, SeatContactSetViewSet, SeatSourceContactSetViewSet,
    ClientDataContactSetViewSet)
from frontend.apps.core.api.views import MobileHomepageView
from frontend.apps.filtering.api.views import (
    ContactSetFiltersViewSet, AccountFiltersViewSet)
from frontend.apps.locations.api.views import (
    CitiesViewSet, StatesViewSet, CountiesViewSet, LinkedInLocationViewSet)
from frontend.apps.personas.api.views import (
    PersonaViewSet, PersonaInstanceViewSet)
from frontend.apps.role.api.views import AccountRolesViewSet
from frontend.apps.seat.api.views import (
    AccountSeatViewSet, SeatRolesViewSet, UserSeatsViewSet)
from frontend.apps.ux.api.views import (SkinSettingsViewSet,
                                        StaffSkinSettingsViewSet)


base_router = ExtendedDefaultRouter()

# Build the Users API
users_router = base_router.register(r'users', RevUpUserViewSet,
                                    base_name='user_api')
## users/<id>/contacts/[<id>]
users_router.register(r'contacts', ContactViewSet,
                      base_name='user_contacts_api',
                      parents_query_lookups=['user_id'])
## users/<id>/import_records/[<id>]
users_router.register(r'import_records', ImportRecordViewSet,
                      base_name='user_import_records_api',
                      parents_query_lookups=['user_id'])
## users/<id>/analysis_view_settings/[<id>]
users_router.register(r'analysis_view_settings',
                      AnalysisViewUserSettingsViewSet,
                      base_name='analysis_view_user_settings_api',
                      parents_query_lookups=['user_id'])
## users/<id>/batch_files/[<id>]
users_router.register(r'batch_files', BatchFileViewSet,
                      base_name='batch_files_api',
                      parents_query_lookups=['user_id'])
## users/<id>/seats/[<id>]
user_seats_router = users_router.register(r'seats',
                                          UserSeatsViewSet,
                                          base_name='user_seats_api',
                                          parents_query_lookups=['user_id'])
## users/<id>/seats/<id>/tasks/[<id>]
user_seats_router.register(r'tasks', RevUpTaskViewSet,
                           base_name='seat_tasks_api',
                           parents_query_lookups=['user_id', 'seat_id'])

# Locations app, currently only used for autocomplete data
base_router.register(r'locations_cities', CitiesViewSet,
                     base_name='locations_cities_api')
base_router.register(r'locations_states', StatesViewSet,
                     base_name='locations_states_api')
base_router.register(r'locations_counties', CountiesViewSet,
                     base_name='locations_counties_api')
base_router.register(r'locations_metro_area', LinkedInLocationViewSet,
                     base_name='locations_metro_area_api')

# Build the Accounts API
accounts_router = base_router.register(r'accounts',
                                       AccountViewSet,
                                       base_name='account_api')

# accounts/<id>/contacts/[<id>]
accounts_router.register(r'contacts', AccountContactViewSet,
                         base_name='account_contacts_api',
                         parents_query_lookups=['account_id'])

## accounts/<id>/roles/[<id>]
roles_router = accounts_router.register(
    r'roles', AccountRolesViewSet, base_name='roles_api',
    parents_query_lookups=['account_id'])

## /accounts/<id>/filters/
accounts_router.register(
    r'filters', AccountFiltersViewSet, base_name='account_filters_api',
    parents_query_lookups=["account_id"])

## accounts/<id>/seats/[<id>]
seats_router = accounts_router.register(
    r'seats', AccountSeatViewSet, base_name='account_seats_api',
    parents_query_lookups=['account_id'])
## accounts/<id>/seats/<id>/roles/[<id>]
seats_router.register(
    r'roles', SeatRolesViewSet, base_name='seat_roles_api',
    parents_query_lookups=['account_id', 'seat_id']
)

## accounts/<id>/seats/<id>/contacts/[<id>]
contacts_router = seats_router.register(
    r'contacts', SeatContactsRedirectViewSet, base_name='seat_contacts_api',
    parents_query_lookups=['account_id', 'seat_id']
)
## accounts/<id>/seats/<id>/contacts/[<id>]/notes/[<id>]
contacts_router.register(
    r'notes', ContactNotesViewSet, base_name='contact_notes_api',
    parents_query_lookups=['account_id', 'contact__user__seats__id',
                           'contact_id']
)


## accounts/<id>/events/[<id>]
events_router = accounts_router.register(
    r'events', AccountEventsViewSet, base_name='events_api',
    parents_query_lookups=['account_id'])
## accounts/<id>/events/<id>/users/[<id>]
events_router.register(r'users', EventUsersViewSet,
                       base_name='event_users_api',
                       parents_query_lookups=['account_id', 'event_id'])
## accounts/<id>/events/<id>/prospects/[<id>]
events_router.register(r'prospects', EventProspectsViewSet,
                       base_name='event_prospects_api',
                       parents_query_lookups=['account_id', 'event_id'])


## accounts/<id>/management_groups/[<id>]
mg_router = accounts_router.register(
    r'management_groups', AccountManagementGroupsViewSet,
    base_name="management_groups_api", parents_query_lookups=['account_id']
)
## accounts/<id>/management_groups/<id>/tracker/
mg_router.register(
    r'tracker', ManagementGroupTrackerViewSet,
    base_name="management_group_tracker_api",
    parents_query_lookups=['account_id', 'pk'])
## accounts/<id>/management_groups/<id>/managers/[<id>]
mg_router.register(
    r'managers', ManagementGroupsManagersViewSet,
    base_name="management_group_managers_api",
    parents_query_lookups=['account_id', 'mg_id'])
## accounts/<id>/management_groups/<id>/members/[<id>]
mg_router.register(
    r'members', ManagementGroupsMembersViewSet,
    base_name="management_group_members_api",
    parents_query_lookups=['account_id', 'mg_id'])


## accounts/<id>/analysis/[<id>]
analysis_router = accounts_router.register(
    r"analysis", AnalysisViewSet, base_name="analysis_api",
    parents_query_lookups=['account_id'])
## accounts/<id>/analysis/<id>/partitions/[<id>]
partitioned_router = analysis_router.register(
    r"partitions", PartitionConfigViewSet, base_name="analysis_partition_api",
    parents_query_lookups=['account_id', 'analysis_id'])


## accounts/<id>/analysis_configs/[<id>]
ac_router = accounts_router.register(
    r'analysis_configs', AccountAnalysisConfigsViewSet,
    base_name='analysis_configs_api', parents_query_lookups=['account_id']
)
## accounts/<id>/analysis_configs/<id>/question_sets/[<id>]
qs_router = ac_router.register(
    r'question_sets', QuestionSetViewSet,
    base_name='question_set_api',
    parents_query_lookups=['account_id', 'analysis_config_id']
)
## accounts/<id>/analysis_configs/<id>/question_sets/<id>/data/[<id>]
qs_router.register(
    r'data', QuestionSetDataViewSet,
    base_name='question_set_data_api',
    parents_query_lookups=['account_id', 'analysis_config_id',
                           'question_set_id']
)

## accounts/<id>/analysis_configs/<id>/entity_search/
ac_router.register(
    r'entity_search', EntitySearchAPI,
    base_name="entity_search_api",
    parents_query_lookups=['account_id', 'analysis_config_id']
)

## accounts/<id>/contact_sets/<id>/
accounts_router.register(
    r'contact_sets', ContactSetViewSet,
    base_name="contact_set_api",
    parents_query_lookups=['account_id']
)
## accounts/<id>/contact_sets_client_data/[<id>/]
accounts_router.register(
    r'contact_sets_client_data', ClientDataContactSetViewSet,
    base_name="contact_sets_client_data_api",
    parents_query_lookups=['account_id']
)

## /users/<id>/seats/<id>/contact_sets/[<id>]/
contact_set_router = user_seats_router.register(
    r'contact_sets', SeatContactSetViewSet,
    base_name="seat_contact_set_api",
    parents_query_lookups=['user_id', 'seat_id']
)

## /users/<id>/seats/<id>/source_contact_sets/[<id>]/
user_seats_router.register(
    r'source_contact_sets', SeatSourceContactSetViewSet,
    base_name="seat_source_contact_set_api",
    parents_query_lookups=['user_id', 'seat_id']
)

## /users/<id>/seats/<id>/contact_sets/<id>/filters/
contact_set_router.register(
    r'filters', ContactSetFiltersViewSet, base_name='contact_set_filters_api',
    parents_query_lookups=['user_id', 'seat_id', "contact_set_id"]
)

# Equivalent API to the results api, just uses contact IDs instead
## users/<id>/seats/<id>/contact_sets/<id>/contact_results/[<id>]
contact_set_results_router = contact_set_router.register(
    r"contact_results", ContactSetAnalysisContactResultsViewSet,
    base_name="contact_set_analysis_contact_results_api",
    parents_query_lookups=['user_id', 'seat_id', 'contact_set_id'])

## users/<id>/seats/<id>/contact_sets/<id>/contact_results/[<id>]/notes/[<id>]
contact_set_results_router.register(
    r'notes', ContactSetResultsContactNotesViewSet,
    base_name='contact_set_results_notes_api',
    parents_query_lookups=['user_id', 'seat_id',
                           'contact_set_id', 'contact_id'])


# Build the staff skins API: skins/[<id>]
base_router.register(r'skins', StaffSkinSettingsViewSet,
                     base_name='staff_skins_api')
## accounts/<id>/skins/[<id>]
accounts_router.register(
    r'skins', SkinSettingsViewSet,
    base_name="ux_skins_api", parents_query_lookups=['account_id']
)


# Call Time APIs
## /accounts/<id>/call_time_contacts/[<id>]
calltime_contact_router = accounts_router.register(
    r'call_time_contacts', CallTimeContactViewSet,
    base_name="call_time_contacts_api", parents_query_lookups=["account_id"]
)
## /accounts/<id>/call_time_groups/[<id>]
calltime_cs_router = accounts_router.register(
    r'call_time_groups', CallGroupViewSet,
    base_name="call_time_groups_api",
    parents_query_lookups=["account_id"]
)
## /accounts/<id>/call_time_groups/<id>/call_time_contacts/[<id>]
ct_cs_contacts_router = calltime_cs_router.register(
    r'call_time_contacts', CallGroupContactsViewSet,
    base_name="call_time_groups_contacts_api",
    parents_query_lookups=["account_id", "call_group_id"]
)
## /accounts/<id>/call_time_groups/<id>/call_time_contacts/<id>/logs/[<id>]
ct_cs_contacts_router.register(
    r'logs', CallLogViewSet, base_name="call_time_groups_call_log_api",
    parents_query_lookups=["account_id", "call_group_id", "ct_contact_id"]
)
## /accounts/<id>/call_time_contacts/<id>/logs/[<id>]
calltime_contact_router.register(
    r'logs', CallLogViewSet, base_name="call_time_contact_call_log_api",
    parents_query_lookups=["account_id", "ct_contact_id"]
)

## /users/<id>/seats/<id>/call_time_groups/[<id>]/
seat_call_groups_router = user_seats_router.register(
    r'call_time_groups', SeatCallGroupViewSet,
    base_name="seat_call_time_groups_api",
    parents_query_lookups=['user_id', 'seat_id']
)
## /users/<id>/seats/<id>/call_time_groups/<id>/call_time_contacts/[<id>]/
seat_call_groups_contacts_router = seat_call_groups_router.register(
    r'call_time_contacts', SeatCallGroupContactsViewSet,
    base_name="seat_call_time_groups_contacts_api",
    parents_query_lookups=['user_id', 'seat_id', "call_group_id"]
)

## /users/<id>/seats/<id>/call_time_groups/<id>/call_time_contacts/<id>/logs/[<id>]
seat_call_groups_contacts_router.register(
    r'logs', SeatCallLogViewSet, base_name="call_time_groups_call_log_api",
    parents_query_lookups=['user_id', 'seat_id', "call_group_id", "ct_contact_id"]
)


# Persona APIs
## /accounts/<id>/personas/[<id>/]
personas_router = accounts_router.register(
    r'personas', PersonaViewSet, base_name="account_personas_api",
    parents_query_lookups=["account_id"]
)
## /accounts/<id>/persona_instances/[<id>/]
accounts_router.register(
    r'persona_instances', PersonaInstanceViewSet,
    base_name="account_persona_instance_api",
    parents_query_lookups=["account_id"]
)


urlpatterns = base_router.urls

urlpatterns += (
    # Add the JWT login api
    url(r'^api-auth/$', ObtainJSONWebToken.as_view(), name='api_jwt_auth'),
    url(r'^api-auth-refresh/$', RefreshJSONWebToken.as_view(),
        name='api_jwt_auth_refresh'),
    # Build the Homepage API
    url(r"^home/m/$", MobileHomepageView.as_view()),
)
