from os import environ

from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib.staticfiles.storage import staticfiles_storage
from django.views.generic.base import RedirectView

from frontend.apps.authorize.views import LoginView

media = static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# Frontend apps urls
app_urls = [
    url(r'^favicon.ico$',
        RedirectView.as_view(
            url=staticfiles_storage.url('img/favicon.ico'),
            permanent=False),
        name="favicon"),
    url(r'^accounts', include('frontend.apps.authorize.urls')),
    url(r'^rankings/', include('frontend.apps.analysis.urls')),
    url(r'^contacts', include('frontend.apps.contact.urls')),
    url(r'^personas/', include('frontend.apps.personas.urls')),
    url(r'^campaign', include('frontend.apps.campaign.urls')),
    url(r'^events', include('frontend.apps.campaign.event_urls')),
    url(r'^contact_sets/', include('frontend.apps.contact_set.urls')),
    url(r'^seats', include('frontend.apps.seat.urls')),
    url(r'^staff_admin/', include('frontend.apps.staff_admin.urls')),
    url(r'^campaign_admin/', include('frontend.apps.campaign_admin.urls')),
    url(r'^call_time/', include('frontend.apps.call_time.urls')),
    url(r'^external_apps/', include('frontend.apps.external_app_integration.urls')),
    url(r'^', include('frontend.apps.core.urls')),
]

# API urls
# This looks complicated, but all it does is prepend '^rest/' to all the urls.
api_urls = [
    url(r'^rest/',
        include([
            # Redirects rest framework login to revup login
            url(r'^api-auth/login/', LoginView.as_view(), name='rest_api_login'),
            url(r'^api-auth/',
                include('rest_framework.urls',
                        namespace='rest_framework')),
            # URLs to the various internal APIs
            url(r'^', include("frontend.api_urls")),
        ])),
]

urlpatterns = api_urls + media + app_urls


if settings.DEBUG and not environ.get('CI'):
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
