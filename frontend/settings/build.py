"""Production settings and globals."""
from .common import *
from os.path import join, normpath

SECRET_KEY = 'lkajsdlkjqwelkj'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3'
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

# maybe?
CELERY_RESULT_BACKEND = "disabled"
CELERY_BROKER_URL = CELERY_RESULT_BACKEND


INSTALLED_APPS += (
    'storages',
)

# Gzips static assets and adds md5sum to name for versioning purposes.
STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'

STATIC_ROOT = normpath(join(DJANGO_ROOT, 'staticfiles'))
STATICFILES_DIRS = (
    normpath(join(DJANGO_ROOT, 'assets')),
    normpath(join(DJANGO_ROOT, 'static'))
)

