"""Production settings and globals."""
from celery.apps import worker
from celery.exceptions import WorkerTerminate
from .common import *
import dj_database_url
from functools import partial
import os
from os.path import join, normpath
import socket

# GENERAL CONFIGURATION
if not ENV:
    ENV = 'prod'

MIDDLEWARE = \
        ('sslify.middleware.SSLifyMiddleware',
         'hirefire.contrib.django.middleware.HireFireMiddleware',) +\
        MIDDLEWARE

# FORCE SSL REDIRECT
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

INCLUDE_MOCK_DATA = str_to_bool(environ.get("INCLUDE_MOCK_DATA", False))
# END GENERAL CONFIGURATION

# EMAIL CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-host
EMAIL_HOST = environ.get('EMAIL_HOST', 'smtp.gmail.com')

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-host-password
EMAIL_HOST_PASSWORD = environ.get('EMAIL_HOST_PASSWORD', 'GDS8NBXzDVvlJ@')

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-host-user
EMAIL_HOST_USER = environ.get('EMAIL_HOST_USER', 'smtp@revup.com')

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-port
EMAIL_PORT = environ.get('EMAIL_PORT', 587)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-subject-prefix
# EMAIL_SUBJECT_PREFIX = '[%s] ' % SITE_NAME

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-use-tls
EMAIL_USE_TLS = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#default-from-email
DEFAULT_FROM_EMAIL = 'RevUp <revup@revup.com>'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#server-email
SERVER_EMAIL = DEFAULT_FROM_EMAIL

# See https://docs.djangoproject.com/en/dev/ref/settings/#email-timeout
EMAIL_TIMEOUT = 10
# END EMAIL CONFIGURATION


########## CACHE CONFIGURATION
CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': environ.get('REDISGREEN_URL'),
        'OPTIONS': {
            'PARSER_CLASS': 'redis.connection.HiredisParser',
            'PICKLE_VERSION': 2,
            'IGNORE_EXCEPTIONS': True,
            'SOCKET_TIMEOUT': 5,
            'SOCKET_CONNECT_TIMEOUT': 5,
            'CONNECTION_POOL_CLASS': 'redis.BlockingConnectionPool',
            'CONNECTION_POOL_KWARGS': {
                'max_connections': 20 if not IN_CELERY_PROCESS else 2,
                'timeout': 20,
                'retry_on_timeout': True,
                'socket_keepalive': True,
                'socket_keepalive_options': {
                    socket.TCP_KEEPIDLE: 10,  # start keepalive after X seconds idle
                    socket.TCP_KEEPINTVL: 10,  # send keepalive every X seconds
                    socket.TCP_KEEPCNT: 5  # close connection after X failed ping
                }
            },
            'COMPRESSOR': 'django_redis.compressors.zlib.ZlibCompressor',
        }
    },
}

######### end CACHE CONFIGURATION


# DATABASE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
# https://docs.djangoproject.com/en/1.7/ref/settings/#std:setting-CONN_MAX_AGE
DATABASES = {
    # If DB_ENABLE_POOLING is true, try configuring the default database with
    # the url of the heroku connection pool. If DB_ENABLE_POOLING instead is
    # false or if the connection pool url is missing from the environment, fall
    # back to the default DATABASE_URL.
    'default': (
        DB_ENABLE_POOLING and
        dj_database_url.config('DATABASE_CONNECTION_POOL_URL') or
        dj_database_url.config('DATABASE_URL')),
    'peacock': dj_database_url.config('PEACOCK_URL'),
    'contactdb': dj_database_url.config('CONTACTDB_URL'),
}
# postgis configuration
DATABASES['default']['ENGINE'] = 'django.contrib.gis.db.backends.postgis'
GDAL_LIBRARY_PATH = os.getenv('GDAL_LIBRARY_PATH')
GEOS_LIBRARY_PATH = os.getenv('GEOS_LIBRARY_PATH')
# Reuse each connection for up to 60 seconds.
DATABASES['default']['CONN_MAX_AGE'] = 60
# See: https://docs.djangoproject.com/en/2.0/ref/settings/#disable-server-side-cursors
# If pooling is enabled, we need to disable server-side cursors in order to
# work around this bug: https://code.djangoproject.com/ticket/28062
if DB_ENABLE_POOLING:
    DATABASES['default']['DISABLE_SERVER_SIDE_CURSORS'] = True
# END DATABASE CONFIGURATION


# CELERY CONFIGURATION
# See: http://docs.celeryproject.org/en/3.1/configuration.html#std:setting-CELERY_MESSAGE_COMPRESSION
CELERY_RESULT_COMPRESSION = 'gzip'
CELERY_TASK_COMPRESSION = 'gzip'

# Store the analysis runner in a cache instead of pickling into every subtask
ANALYSIS_RUNNER_USE_CACHE = str_to_bool(
    environ.get("ANALYSIS_RUNNER_USE_CACHE", True))
BATCH_JOBS_USE_CACHE = str_to_bool(
    environ.get("BATCH_JOBS_USE_CACHE", True))

# Enable zendesk
ZENDESK_ENABLED = str_to_bool(environ.get('ZENDESK_ENABLED', True))

# See: http://docs.celeryproject.org/en/latest/configuration.html#celeryd-max-tasks-per-child
# We want to restart the celery worker process after each job in order to
# release the memory it consumed.
CELERY_WORKER_MAX_TASKS_PER_CHILD = 100

CELERY_RESULT_BACKEND = environ.get('REDISGREEN_URL')
CELERY_BROKER_URL = CELERY_RESULT_BACKEND
CELERY_REDIS_MAX_CONNECTIONS = 5
CELERY_BROKER_POOL_LIMIT = 0
CELERY_BROKER_TRANSPORT_OPTIONS.update({
    'socket_keepalive': True,
    'socket_keepalive_options': {
        socket.TCP_KEEPIDLE: 10,  # start keepalive after X seconds idle
        socket.TCP_KEEPINTVL: 10,  # send keepalive every X seconds
        socket.TCP_KEEPCNT: 5  # close connection after X failed ping
    }
})
CELERY_RESULT_BACKEND_TRANSPORT_OPTIONS = {
    'CONNECTION_POOL_KWARGS': {
        'retry_on_timeout': True,
        'socket_keepalive': True,
        'socket_keepalive_options': {
            socket.TCP_KEEPIDLE: 10,  # start keepalive after X seconds idle
            socket.TCP_KEEPINTVL: 10,  # send keepalive every X seconds
            socket.TCP_KEEPCNT: 5  # close connection after X failed ping
        }
    }
}

# Install SIGTERM handler into celery workers that forces a quick shutdown
# without waiting for jobs to finish.
worker.install_worker_term_handler = partial(
    worker._shutdown_handler, sig='SIGTERM', how='Cold', exc=WorkerTerminate,
)
# HEROKU AUTOSCALE CONFIGURATION
HIREFIRE_PROCS = ['frontend.procs.WorkerProc',
                  'frontend.procs.HeavyWorkerProc',
                  'frontend.procs.BackgroundWorkerProc']
HIREFIRE_TOKEN = environ.get('HIREFIRE_TOKEN')
# END HEROKU AUTOSCALE CONFIGURATION


# STORAGE CONFIGURATION
# See: http://django-storages.readthedocs.org/en/latest/index.html
INSTALLED_APPS += (
    'storages',
)

BATCH_JOB_S3_STORAGE_ENABLED = True

CONTACT_UPLOAD_S3_STORAGE_ENABLED = True

# Gzips static assets and adds md5sum to name for versioning purposes.
STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'

STATIC_ROOT = normpath(join(DJANGO_ROOT, 'staticfiles'))
STATICFILES_DIRS = (
    normpath(join(DJANGO_ROOT, 'assets')),
    normpath(join(DJANGO_ROOT, 'static'))
)

# See: http://django-storages.readthedocs.org/en/latest/backends/amazon-S3.html#settings
# STATICFILES_STORAGE = DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'

# See: http://django-storages.readthedocs.org/en/latest/backends/amazon-S3.html#settings
# AWS_CALLING_FORMAT = CallingFormat.SUBDOMAIN

# See: http://django-storages.readthedocs.org/en/latest/backends/amazon-S3.html#settings
AWS_ACCESS_KEY_ID = environ.get('AWS_ACCESS_KEY_ID', '')
AWS_SECRET_ACCESS_KEY = environ.get('AWS_SECRET_ACCESS_KEY', '')

AWS_DEFAULT_ACL = None
AWS_BUCKET_ACL = None

AWS_STORAGE_BUCKET_NAME = environ.get('AWS_STORAGE_BUCKET_NAME', 'revup-media')
AWS_LOCATION = environ.get('AWS_LOCATION', 'prod/media')
# AWS_AUTO_CREATE_BUCKET = True
# AWS_QUERYSTRING_AUTH = False

# AWS cache settings, don't change unless you know what you're doing:
# AWS_EXPIRY = 60 * 60 * 24 * 7
# AWS_HEADERS = {
#     'Cache-Control': 'max-age=%d, s-maxage=%d, must-revalidate' % (AWS_EXPIRY,
#         AWS_EXPIRY)
# }

# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
# STATIC_URL = 'https://s3.amazonaws.com/%s/' % AWS_STORAGE_BUCKET_NAME
CDN_DOMAIN = environ.get('CDN_DOMAIN', 'd1yxqk9cswzpwq.cloudfront.net')
STATICFILES_LOCATION = 'static'
MEDIAFILES_LOCATION = 'media'

STATIC_URL = "https://%s/%s/" % (CDN_DOMAIN, STATICFILES_LOCATION)
MEDIA_URL = "https://%s/%s/" % (CDN_DOMAIN, MEDIAFILES_LOCATION)
# END STORAGE CONFIGURATION


# COMPRESSION CONFIGURATION
# See: http://django_compressor.readthedocs.org/en/latest/settings/#django.conf.settings.COMPRESS_OFFLINE
# COMPRESS_OFFLINE = True

# See: http://django_compressor.readthedocs.org/en/latest/settings/#django.conf.settings.COMPRESS_STORAGE
# COMPRESS_STORAGE = DEFAULT_FILE_STORAGE

# See: http://django_compressor.readthedocs.org/en/latest/settings/#django.conf.settings.COMPRESS_CSS_FILTERS
# COMPRESS_CSS_FILTERS += [
#     'compressor.filters.cssmin.CSSMinFilter',
# ]

# See: http://django_compressor.readthedocs.org/en/latest/settings/#django.conf.settings.COMPRESS_JS_FILTERS
# COMPRESS_JS_FILTERS += [
#     'compressor.filters.jsmin.JSMinFilter',
# ]
# END COMPRESSION CONFIGURATION

# SECRET CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
SECRET_KEY = environ.get('DJANGO_SECRET_KEY', SECRET_KEY)
# END SECRET CONFIGURATION

# ALLOWED HOSTS CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#allowed-hosts
ALLOWED_HOSTS = [
    '.revup.com',
    '.revup.com.',
]
# END ALLOWED HOST CONFIGURATION

if not DEFAULT_DOMAIN:
    DEFAULT_DOMAIN = "app.revup.com"

# COOKIE CONFIG
# See: https://docs.djangoproject.com/en/1.4/ref/settings/#session-cookie-domain
SESSION_COOKIE_DOMAIN = ".revup.com"
# end COOKIE CONFIG

# LOGGING CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] [dd.trace_id:%(dd.trace_id)s;dd.span_id:%(dd.span_id)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
    },
    'filters': {
        'require_debug_false': {
                 '()': 'django.utils.log.RequireDebugFalse',
        }
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console', 'mail_admins'],
            'propagate': True,
            'level': 'WARN',
        },
        'django.db.backends': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'bmemcached': {
            'handlers': ['null'],
            'propagate': False,
        },
        '': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
        'kafka': {
            'level': 'INFO',
        },
        'ddtrace': {
            'level': 'INFO',
        },
    }
}
if IN_CELERY_PROCESS:
    patch_celery_logging(LOGGING)
# END LOGGING CONFIGURATION

# ENVIRONMENT-SPECIFIC PYTHON-SOCIAL-AUTH RELATED CONFIGURATION

SOCIAL_AUTH_FACEBOOK_KEY = '299102280281965'
SOCIAL_AUTH_FACEBOOK_SECRET = '3ff33d8e8e855e6ef7a78dbd9cd25bb2'
SOCIAL_AUTH_FACEBOOK_SCOPE = [
    'public_profile', 'user_friends'
]

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '246377779235-93jogs9q9p9dtjl8hu40p67iqa7lff5t.apps.googleusercontent.com'
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'cusjwBFa3psWpJAHi9I6xhS-'
SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = [
    'https://www.google.com/m8/feeds',
]
SOCIAL_AUTH_GOOGLE_OAUTH2_AUTH_EXTRA_ARGUMENTS = {'access_type': 'offline'}

SOCIAL_AUTH_LINKEDIN_OAUTH2_KEY = '7578upp7wah714'
SOCIAL_AUTH_LINKEDIN_OAUTH2_SECRET = 'DkiQyssjAdf7Dn11'
SOCIAL_AUTH_LINKEDIN_OAUTH2_SCOPE = [
    'r_network', 'r_fullprofile', 'r_emailaddress'
]

SOCIAL_AUTH_TWITTER_KEY = '3S2S9rFOiOy5NXycHQHrRcNmS'
SOCIAL_AUTH_TWITTER_SECRET = 'ZoaQcEL96lzxjtYBM8M5EIZfCL6jdlaq5FkdLxYDDreC83ud4e'

SOCIAL_AUTH_NATIONBUILDER_KEY = 'a4e2e66ebdc49a043743d990c17a1a1e1bd4a2d772e101284b7bd2411fdc8a2d'
SOCIAL_AUTH_NATIONBUILDER_SECRET = 'c97705d3ab4a70de4a1057c551aca296138a1e3ec8b159b0d5acbd91b83e9623'

# END ENVIRONMENT-SPECIFIC PYTHON-SOCIAL-AUTH RELATED CONFIGURATION

########## RECURLY CONFIG
# Public key should be used by js functions
RECURLY_PUBLIC_API_KEY = "ewr1-U0oXjPtSuXTq3imROunuLj"
# Private key should be used by Python API. DO NOT EXPOSE TO FRONTEND.
RECURLY_PRIVATE_API_KEY = "340841cdf57e4e70a7e61c6b24047ae8"
RECURLY_SUBDOMAIN = "revup"
########## end RECURLY CONFIG


########## DJANGO AXES CONFIG
# If True, it will look for the IP address from the header defined at
# AXES_REVERSE_PROXY_HEADER. Please make sure if you enable this setting to
# configure your proxy to set the correct value for the header, otherwise you
# could be attacked by setting this header directly in every request. Default:
# False
AXES_BEHIND_REVERSE_PROXY = True
########## end DJANGO AXES CONFIG

########## Backend Contact Kafka CONFIG
BACKEND_KAFKA_SECURITY_ENABLED = True
BACKEND_CONTACT_TOPIC = 'contacts-raw-prod'
BACKEND_CONTACT_UPDATES_TOPIC = 'contacts-update-prod'
BACKEND_CONTACT_DELETE_TOPIC = 'contacts-delete-prod'
BACKEND_KAFKA_BOOTSTRAP_SERVERS = [
    'kafka00-data.revup.com:9093',
    'kafka01-data.revup.com:9093',
    'kafka02-data.revup.com:9093',
]
########## end Backend Contact Kafka CONFIG

########## Account Contribution CSV Upload CONFIG
ACCOUNT_CONTRIBUTION_S3_STORAGE_ENABLED = True
########## end Account Contribution CSV Upload CONFIG

# CloudSponge CONFIG
CLOUDSPONGE_DOMAIN_KEY = '8T3EP6KYE85G7WUJLU6M'
CLOUDSPONGE_DOMAIN_PASSWORD = 'GXUls731RgvvIZN'

CONTACT_IMPORT_DOMAIN = "apps.revup.com"
# END CloudSponge CONFIG


# GEOPY CONFIG
GEONAMES_USERNAME = "revup"
# END GEOPY CONFIG
