"""Development settings and globals."""
from .common import *
import os

########## DEBUG CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = True

# This is not secure, but for dev it does not need to be.
SECRET_KEY = 'bAXZ65PSPx7sr481OwWZf1ax0x4JyuXboMFzFVKSB28LRwnk3l'

if DEBUG:
    import mimetypes
    mimetypes.add_type("font/opentype", ".otf", True)
########## END DEBUG CONFIGURATION


# GENERAL CONFIGURATION
if not ENV:
    ENV = 'test'

# END GENERAL CONFIGURATION

########## EMAIL CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
########## END EMAIL CONFIGURATION


########## DATABASE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'revup',
        'USER': 'ubuntu' if environ.get('CI') else 'engine',  # Rev Up your Engine!
        'PASSWORD': None if environ.get('CI') else 'password',
        'HOST': 'localhost',
        'PORT': '5432',
    },
}
# postgis configuration
GDAL_LIBRARY_PATH = os.getenv('GDAL_LIBRARY_PATH')
GEOS_LIBRARY_PATH = os.getenv('GEOS_LIBRARY_PATH')
########## END DATABASE CONFIGURATION


########## CELERY CONFIGURATION
if environ.get('CI'):
    CELERY_RESULT_BACKEND = 'redis://localhost/'
    CELERY_BROKER_URL = CELERY_RESULT_BACKEND
else:
    CELERY_RESULT_BACKEND = 'mongodb://localhost/'
    CELERY_MONGODB_BACKEND_SETTINGS = {
        'database': 'celery',
        'taskmeta_collection': 'my_taskmeta_collection',
    }
    CELERY_BROKER_URL = 'mongodb://localhost/celery'
    CELERY_BROKER_TRANSPORT_OPTIONS = {}
########## END CELERY CONFIGURATION


########## ENVIRONMENT-SPECIFIC PYTHON-SOCIAL-AUTH RELATED CONFIGURATION

SOCIAL_AUTH_FACEBOOK_KEY = 'TODO'
SOCIAL_AUTH_FACEBOOK_SECRET = 'TODO'

SOCIAL_AUTH_FACEBOOK_KEY = '299103923615134'
SOCIAL_AUTH_FACEBOOK_SECRET = '05dff31a407b11fa0acd0008585c0ef0'
SOCIAL_AUTH_FACEBOOK_SCOPE = [
    'public_profile', 'user_friends'
]

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '21679637810-ig1r2buo7kgjig2on5653fflhm0t94v0.apps.googleusercontent.com'
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'KUHW_8_OHtjgvwhOMq1G-8o3'
SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = [
    'https://www.google.com/m8/feeds',
]
SOCIAL_AUTH_GOOGLE_OAUTH2_AUTH_EXTRA_ARGUMENTS = {'access_type': 'offline'}

SOCIAL_AUTH_LINKEDIN_OAUTH2_KEY = '75yws5f305h5th'
SOCIAL_AUTH_LINKEDIN_OAUTH2_SECRET = 'ZGZuJqGDHauZbpQp'
SOCIAL_AUTH_LINKEDIN_OAUTH2_SCOPE = [
    'r_network', 'r_fullprofile', 'r_emailaddress'
]

SOCIAL_AUTH_TWITTER_KEY = 'fTO1viOC5gvKWUJzeksKtypgJ'
SOCIAL_AUTH_TWITTER_SECRET = 'ovxtCbfD0Xbjh7n9iJZQl08VeZv210tY4gnsVQnu5rSzYgQneU'

########## END ENVIRONMENT-SPECIFIC PYTHON-SOCIAL-AUTH RELATED CONFIGURATION

######## TEST CONFIGURATION
REPORT_DIR = environ.get('REPORT_DIR',
                         environ.get('CIRCLE_TEST_REPORTS', '.'))

TEST_RUNNER = "frontend.libs.utils.test_utils.PytestTestRunner"
######## END TEST CONFIGURATION

########## DATADOG CONFIGURATION
DATADOG_TRACE['ENABLED'] = False
DATADOG_TRACE['AUTO_INSTRUMENT'] = False
########## END DATADOG CONFIGURATION


## Import optional local settings
try:
    # A developer might want to override some settings locally.
    # To do so, just add a file: 'settings/local.py' and change any settings
    # you wish to override. Settings are overridden completely using this
    # technique; you won't be able to do "+=" like above.
    from .local import *
except ImportError:
    pass
