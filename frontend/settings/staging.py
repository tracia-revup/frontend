"""Production settings and globals."""
import os
from os.path import join, normpath

from celery.apps import worker
from celery.exceptions import WorkerTerminate
import dj_database_url
from functools import partial
import socket

from .common import *

# GENERAL CONFIGURATION
if not ENV:
    ENV = 'staging'

MIDDLEWARE = \
        ('sslify.middleware.SSLifyMiddleware',
         'hirefire.contrib.django.middleware.HireFireMiddleware',) +\
        MIDDLEWARE

# FORCE SSL REDIRECT
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
# END GENERAL CONFIGURATION

# EMAIL CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-host
EMAIL_HOST = environ.get('EMAIL_HOST', 'smtp.gmail.com')

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-host-password
EMAIL_HOST_PASSWORD = environ.get('EMAIL_HOST_PASSWORD', 'GDS8NBXzDVvlJ@')

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-host-user
EMAIL_HOST_USER = environ.get('EMAIL_HOST_USER', 'smtp@revup.com')

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-port
EMAIL_PORT = environ.get('EMAIL_PORT', 587)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-subject-prefix
# EMAIL_SUBJECT_PREFIX = '[%s] ' % SITE_NAME

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-use-tls
EMAIL_USE_TLS = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#default-from-email
DEFAULT_FROM_EMAIL = 'RevUp Staging <revup-staging@revup.com>'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#server-email
SERVER_EMAIL = DEFAULT_FROM_EMAIL

# See https://docs.djangoproject.com/en/dev/ref/settings/#email-timeout
EMAIL_TIMEOUT = 10
# END EMAIL CONFIGURATION

PG_APPNAME_TMPL = '{dyno_type}-{conn_name}'

DYNO_TYPE = environ.get('DYNO', 'UNKNOWN')


# DATABASE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    # If DB_ENABLE_POOLING is true, try configuring the default database with
    # the url of the heroku connection pool. If DB_ENABLE_POOLING instead is
    # false or if the connection pool url is missing from the environment, fall
    # back to the default DATABASE_URL.
    'default': (
        DB_ENABLE_POOLING and
        dj_database_url.config('DATABASE_CONNECTION_POOL_URL') or
        dj_database_url.config('DATABASE_URL')),
    'peacock': dj_database_url.config('PEACOCK_URL'),
    'contactdb': dj_database_url.config('CONTACTDB_URL'),
}
# Reuse each connection for up to 60 seconds.
DATABASES['default']['CONN_MAX_AGE'] = 60
# See: https://docs.djangoproject.com/en/2.0/ref/settings/#disable-server-side-cursors
# If pooling is enabled, we need to disable server-side cursors in order to
# work around this bug: https://code.djangoproject.com/ticket/28062
if DB_ENABLE_POOLING:
    DATABASES['default']['DISABLE_SERVER_SIDE_CURSORS'] = True

# postgis configuration
DATABASES['default']['ENGINE'] = 'django.contrib.gis.db.backends.postgis'
GDAL_LIBRARY_PATH = os.getenv('GDAL_LIBRARY_PATH')
GEOS_LIBRARY_PATH = os.getenv('GEOS_LIBRARY_PATH')

for conn_name, value in DATABASES.items():
    value.setdefault('OPTIONS', {})['application_name'] = PG_APPNAME_TMPL.format(
        dyno_type=DYNO_TYPE, conn_name=conn_name)

# END DATABASE CONFIGURATION

########## CACHE CONFIGURATION
CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': environ.get('REDISGREEN_URL'),
        'OPTIONS': {
            'PARSER_CLASS': 'redis.connection.HiredisParser',
            'PICKLE_VERSION': 2,
            'IGNORE_EXCEPTIONS': True,
            'SOCKET_TIMEOUT': 5,
            'SOCKET_CONNECT_TIMEOUT': 5,
            'CONNECTION_POOL_CLASS': 'redis.BlockingConnectionPool',
            'CONNECTION_POOL_KWARGS': {
                'max_connections': 20 if not IN_CELERY_PROCESS else 2,
                'timeout': 20,
                'retry_on_timeout': True,
                'socket_keepalive': True,
                'socket_keepalive_options': {
                    socket.TCP_KEEPIDLE: 10,  # start keepalive after X seconds idle
                    socket.TCP_KEEPINTVL: 10,  # send keepalive every X seconds
                    socket.TCP_KEEPCNT: 5  # close connection after X failed ping
                }
            },
            'COMPRESSOR': 'django_redis.compressors.zlib.ZlibCompressor',
        }
    },
}

######### end CACHE CONFIGURATION


# CELERY CONFIGURATION
# See: http://docs.celeryproject.org/en/latest/configuration.html#celeryd-max-tasks-per-child
# We want to restart the celery worker process after each job in order to
# release the memory it consumed.
CELERY_WORKER_MAX_TASKS_PER_CHILD = 100

# See: http://docs.celeryproject.org/en/3.1/configuration.html#std:setting-CELERY_MESSAGE_COMPRESSION
CELERY_RESULT_COMPRESSION = 'gzip'
CELERY_TASK_COMPRESSION = 'gzip'

# Store the analysis runner in a cache instead of pickling into every subtask
ANALYSIS_RUNNER_USE_CACHE = str_to_bool(
    environ.get("ANALYSIS_RUNNER_USE_CACHE", True))
BATCH_JOBS_USE_CACHE = str_to_bool(
    environ.get("BATCH_JOBS_USE_CACHE", True))

# Celery logging config
#Defaults
#CELERYD_LOG_FORMAT = '[%(asctime)s: %(levelname)s/%(processName)s] %(message)s'
#CELERYD_TASK_LOG_FORMAT = '[%(asctime)s: %(levelname)s/%(processName)s] [%(task_name)s(%(task_id)s)] %(message)s'

CELERY_WORKER_LOG_FORMAT = '[%(asctime)s: %(levelname)s/%(processName)s:%(name)s:%(funcName)s:%(lineno)s] %(message)s'
CELERY_WORKER_TASK_LOG_FORMAT = '[%(asctime)s: %(levelname)s/%(processName)s:%(name)s:%(funcName)s:%(lineno)s] [%(task_name)s(%(task_id)s)] %(message)s'

CELERY_RESULT_BACKEND = environ.get('REDISGREEN_URL')
CELERY_BROKER_URL = CELERY_RESULT_BACKEND
CELERY_REDIS_MAX_CONNECTIONS = 5
CELERY_BROKER_POOL_LIMIT = 0
CELERY_BROKER_TRANSPORT_OPTIONS.update({
    'socket_keepalive': True,
    'socket_keepalive_options': {
        socket.TCP_KEEPIDLE: 10,  # start keepalive after X seconds idle
        socket.TCP_KEEPINTVL: 10,  # send keepalive every X seconds
        socket.TCP_KEEPCNT: 5  # close connection after X failed ping
    }
})
CELERY_RESULT_BACKEND_TRANSPORT_OPTIONS = {
    'CONNECTION_POOL_KWARGS': {
        'retry_on_timeout': True,
        'socket_keepalive': True,
        'socket_keepalive_options': {
            socket.TCP_KEEPIDLE: 10,  # start keepalive after X seconds idle
            socket.TCP_KEEPINTVL: 10,  # send keepalive every X seconds
            socket.TCP_KEEPCNT: 5  # close connection after X failed ping
        }
    }
}

# Install SIGTERM handler into celery workers that forces a quick shutdown
# without waiting for jobs to finish.
worker.install_worker_term_handler = partial(
    worker._shutdown_handler, sig='SIGTERM', how='Cold', exc=WorkerTerminate,
)
# END CELERY CONFIGURATION

# HEROKU AUTOSCALE CONFIGURATION
HIREFIRE_PROCS = ['frontend.procs.WorkerProc',
                  'frontend.procs.HeavyWorkerProc',
                  'frontend.procs.BackgroundWorkerProc']
HIREFIRE_TOKEN = environ.get('HIREFIRE_TOKEN')
# END HEROKU AUTOSCALE CONFIGURATION

# STORAGE CONFIGURATION
# See: http://django-storages.readthedocs.org/en/latest/index.html
INSTALLED_APPS += (
    'storages',
)

BATCH_JOB_S3_STORAGE_ENABLED = True

CONTACT_UPLOAD_S3_STORAGE_ENABLED = True

# Gzips static assets and adds md5sum to name for versioning purposes.
STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'

STATIC_ROOT = normpath(join(DJANGO_ROOT, 'staticfiles'))
STATICFILES_DIRS = (
    normpath(join(DJANGO_ROOT, 'assets')),
    normpath(join(DJANGO_ROOT, 'static'))
)

# See: http://django-storages.readthedocs.org/en/latest/backends/amazon-S3.html#settings
# STATICFILES_STORAGE = DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'

# See: http://django-storages.readthedocs.org/en/latest/backends/amazon-S3.html#settings
# AWS_CALLING_FORMAT = CallingFormat.SUBDOMAIN

# See: http://django-storages.readthedocs.org/en/latest/backends/amazon-S3.html#settings
AWS_ACCESS_KEY_ID = environ.get('AWS_ACCESS_KEY_ID', '')
AWS_SECRET_ACCESS_KEY = environ.get('AWS_SECRET_ACCESS_KEY', '')
AWS_DEFAULT_ACL = None
AWS_BUCKET_ACL = None

AWS_STORAGE_BUCKET_NAME = environ.get('AWS_STORAGE_BUCKET_NAME', 'revup-media')
AWS_LOCATION = environ.get('AWS_LOCATION', 'stage/media')
# AWS_AUTO_CREATE_BUCKET = True
# AWS_QUERYSTRING_AUTH = False

# AWS cache settings, don't change unless you know what you're doing:
# AWS_EXPIRY = 60 * 60 * 24 * 7
# AWS_HEADERS = {
#     'Cache-Control': 'max-age=%d, s-maxage=%d, must-revalidate' % (AWS_EXPIRY,
#         AWS_EXPIRY)
# }

# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
# STATIC_URL = 'https://s3.amazonaws.com/%s/' % AWS_STORAGE_BUCKET_NAME
CDN_DOMAIN = environ.get('CDN_DOMAIN', 'd1wo3hghzifscm.cloudfront.net')
STATICFILES_LOCATION = 'static'
MEDIAFILES_LOCATION = 'media'

STATIC_URL = "https://%s/%s/" % (CDN_DOMAIN, STATICFILES_LOCATION)
MEDIA_URL = "https://%s/%s/" % (CDN_DOMAIN, MEDIAFILES_LOCATION)
# END STORAGE CONFIGURATION


# COMPRESSION CONFIGURATION
# See: http://django_compressor.readthedocs.org/en/latest/settings/#django.conf.settings.COMPRESS_OFFLINE
# COMPRESS_OFFLINE = True

# See: http://django_compressor.readthedocs.org/en/latest/settings/#django.conf.settings.COMPRESS_STORAGE
# COMPRESS_STORAGE = DEFAULT_FILE_STORAGE

# See: http://django_compressor.readthedocs.org/en/latest/settings/#django.conf.settings.COMPRESS_CSS_FILTERS
# COMPRESS_CSS_FILTERS += [
#     'compressor.filters.cssmin.CSSMinFilter',
# ]

# See: http://django_compressor.readthedocs.org/en/latest/settings/#django.conf.settings.COMPRESS_JS_FILTERS
# COMPRESS_JS_FILTERS += [
#     'compressor.filters.jsmin.JSMinFilter',
# ]
# END COMPRESSION CONFIGURATION

# SECRET CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
SECRET_KEY = environ.get('DJANGO_SECRET_KEY', SECRET_KEY)
# END SECRET CONFIGURATION

# ALLOWED HOSTS CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#allowed-hosts
ALLOWED_HOSTS = [
    '.revup.com',
    '.revup.com.',
]
# END ALLOWED HOST CONFIGURATION

if not DEFAULT_DOMAIN:
    DEFAULT_DOMAIN = "staging.revup.com"

# LOGGING CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] [dd.trace_id:%(dd.trace_id)s;dd.span_id:%(dd.span_id)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
            'level': 'WARN',
        },
        'django.db.backends': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'bmemcached': {
            'handlers': ['null'],
            'propagate': False,
        },
        '': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
        'kafka': {
            'level': 'INFO',
        },
        'ddtrace': {
            'level': 'INFO',
        },
    }
}
if IN_CELERY_PROCESS:
    patch_celery_logging(LOGGING)
# END LOGGING CONFIGURATION

# ENVIRONMENT-SPECIFIC PYTHON-SOCIAL-AUTH RELATED CONFIGURATION

SOCIAL_AUTH_FACEBOOK_KEY = '299103580281835'
SOCIAL_AUTH_FACEBOOK_SECRET = '9d7ceafa6b643dfe3df696d8dfb7422c'
SOCIAL_AUTH_FACEBOOK_SCOPE = [
    'public_profile', 'user_friends'
]

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '474016672394-prokdcb4n4nchcaapp599sstt0a1n1jm.apps.googleusercontent.com'
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'jXi4__050C61fXXbLgAL2vHS'
SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = [
    'https://www.google.com/m8/feeds',
]
SOCIAL_AUTH_GOOGLE_OAUTH2_AUTH_EXTRA_ARGUMENTS = {'access_type': 'offline'}

SOCIAL_AUTH_LINKEDIN_OAUTH2_KEY = '75rj5sx26wy9le'
SOCIAL_AUTH_LINKEDIN_OAUTH2_SECRET = 'OCteUXB5y7xDpO8I'
SOCIAL_AUTH_LINKEDIN_OAUTH2_SCOPE = [
    'r_network', 'r_fullprofile', 'r_emailaddress'
]

SOCIAL_AUTH_TWITTER_KEY = 'Z0SznGwYZQIg14NMvMLbti9PW'
SOCIAL_AUTH_TWITTER_SECRET = 'iZBRljxn3PluyyCpxq10t9ju6isZ9F6ZsIZv5rzk6Eom0QNx27'

SOCIAL_AUTH_NATIONBUILDER_KEY = 'ed2e18ba3d3ddc7cb5252dd5ec3a05686d95fcde9f10d134c15dca7843c23386'
SOCIAL_AUTH_NATIONBUILDER_SECRET = 'edba2ab6973925ac10ecb07d24fce2a551ad22e07e56fe6ff727eb08c4af6215'

# END ENVIRONMENT-SPECIFIC PYTHON-SOCIAL-AUTH RELATED CONFIGURATION


# CLOUDSPONGE CONFIG
CLOUDSPONGE_DOMAIN_KEY = 'SXPM3E84NVZGUPG8PN3U'
CLOUDSPONGE_DOMAIN_PASSWORD = 'tTRhDmdHarVVrt'
# END CLOUDSPONGE CONFIG


########## DJANGO AXES CONFIG
# If True, it will look for the IP address from the header defined at
# AXES_REVERSE_PROXY_HEADER. Please make sure if you enable this setting to
# configure your proxy to set the correct value for the header, otherwise you
# could be attacked by setting this header directly in every request. Default:
# False
AXES_BEHIND_REVERSE_PROXY = True
########## end DJANGO AXES CONFIG

########## Backend Contact Kafka CONFIG
BACKEND_KAFKA_SECURITY_ENABLED = True
BACKEND_CONTACT_TOPIC = environ.get('BACKEND_CONTACT_TOPIC', 'contacts-raw-stage')
BACKEND_CONTACT_UPDATES_TOPIC = environ.get('BACKEND_CONTACT_UPDATES_TOPIC',
                                            'contacts-update-stage')
BACKEND_CONTACT_DELETE_TOPIC = environ.get('BACKEND_CONTACT_DELETE_TOPIC',
                                            'contacts-delete-stage')
backend_servers = environ.get("BACKEND_KAFKA_BOOTSTRAP_SERVERS")
if backend_servers:
    BACKEND_KAFKA_BOOTSTRAP_SERVERS = backend_servers.split(',')
else:
    BACKEND_KAFKA_BOOTSTRAP_SERVERS = [
        'kafka00-data.revup.com:9093',
        'kafka01-data.revup.com:9093',
        'kafka02-data.revup.com:9093',
    ]
########## end Backend Contact Kafka CONFIG

########## Account Contribution CSV Upload CONFIG
ACCOUNT_CONTRIBUTION_S3_STORAGE_ENABLED = True
########## end Account Contribution CSV Upload CONFIG
