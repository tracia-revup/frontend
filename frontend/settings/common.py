"""Common settings and globals."""
from datetime import timedelta, datetime
from io import open
import os
from os import environ
from os.path import abspath, basename, dirname, join, normpath
from urllib.parse import quote_plus
import sys

from ddtrace import patch_all, tracer
from kombu import Queue
import pymongo
# noinspection PyUnresolvedReferences
#import mongoengine

from frontend.libs.utils.string_utils import str_to_bool

patch_all(logging=True)


########## PATH CONFIGURATION
# Absolute filesystem path to the Django project directory:
BASE_DIR = DJANGO_ROOT = dirname(dirname(abspath(__file__)))

# Absolute filesystem path to the top-level project folder:
SITE_ROOT = dirname(DJANGO_ROOT)

# Site name:
SITE_NAME = basename(DJANGO_ROOT)

# Add our project to our pythonpath, this way we don't need to type our project
# name in our dotted import paths:
sys.path.append(DJANGO_ROOT)
########## END PATH CONFIGURATION


########## DEBUG CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = False
########## END DEBUG CONFIGURATION


########## ENVIRONMENT CONFIGURATION
ENV = environ.get("ENV")
########## END ENVIRONMENT CONFIGURATION


########## MANAGER CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = (
    ('RevUp Support', 'support@revup.com'),
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS
########## END MANAGER CONFIGURATION


########## DATABASE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DB_PASSWORD = environ.get('DJANGO_DB_PASSWORD', 'password')
DB_HOST = environ.get('DJANGO_DB_HOST', 'localhost')
DB_ENABLE_POOLING = str_to_bool(os.getenv('DB_ENABLE_POOLING', False))
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.dummy',
    }
}

MONGODB_USER = environ.get('DJANGO_MONGODB_USER')
MONGODB_PASSWORD = environ.get('DJANGO_MONGODB_PASSWORD')
MONGODB_HOST = environ.get('DJANGO_MONGODB_HOST', 'localhost')
MONGODB_NAME = environ.get('DJANGO_MONGODB_DBNAME', 'revup')
MONGODB_SSL = environ.get('DJANGO_MONGODB_SSL', False)
MONGODB_QUERYARGS = environ.get('DJANGO_MONGODB_QUERYARGS')

# if MONGODB_SSL has a truthy value and is a string, then make sure the string
# doesn't indicate false.
if MONGODB_SSL and isinstance(MONGODB_SSL, str):
    if MONGODB_SSL.lower() in ('0', 'false'):
        MONGODB_SSL = False
    else:
        MONGODB_SSL = True

if MONGODB_USER and MONGODB_PASSWORD:
    MONGODB_AUTH_STR = '{}:{}@'.format(MONGODB_USER,
                                        quote_plus(MONGODB_PASSWORD))
else:
    MONGODB_AUTH_STR = ''
MONGODB_DATABASE_HOST = 'mongodb://{}{}/{}'.format(
    MONGODB_AUTH_STR, MONGODB_HOST, MONGODB_NAME)

if MONGODB_QUERYARGS:
    MONGODB_DATABASE_HOST += '?' + MONGODB_QUERYARGS

DYNO_VAL = environ.get('DYNO')
# If DYNO_VAL is None, then we're not running on a heroku dyno and should
# initialize the mongodb client. If DYNO_VAL starts with `web`, then we're
# running django and should initialize the mongodb client. Otherwise, we're
# running under celery and we do not want pymongo's background threads starting
# up in the fork host.
if ENV != 'build' and (DYNO_VAL is None or DYNO_VAL.startswith('web.')):
    MONGO_CLIENT = pymongo.MongoClient(MONGODB_DATABASE_HOST)
    MONGO_DB = MONGO_CLIENT.get_default_database()

    #mongoengine.connect(MONGODB_NAME, host=MONGODB_DATABASE_HOST,
                        #ssl=MONGODB_SSL)
########## END DATABASE CONFIGURATION


########## CACHE CONFIGURATION
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': '/var/tmp/django_cache',
    },
    'template_fragments': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': '/var/tmp/django_cache',
    }
}

######### end CACHE CONFIGURATION


########## GENERAL CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#time-zone
TIME_ZONE = 'America/Los_Angeles'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = 'en_US'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = False

# See: https://docs.djangoproject.com/en/dev/releases/1.6/#new-test-runner
TEST_RUNNER = "django.test.runner.DiscoverRunner"

# See: https://docs.djangoproject.com/en/1.9/ref/settings/#silenced-system-checks
SILENCED_SYSTEM_CHECKS = (
    # Silence system check that alerts if url patterns are prefixed with
    # forward slashes. Django assumes higher level path components will have
    # the slash appended to the end.
    "urls.W002"
)

DEFAULT_DOMAIN = environ.get("DEFAULT_DOMAIN")

INCLUDE_MOCK_DATA = str_to_bool(environ.get("INCLUDE_MOCK_DATA", True))
########## END GENERAL CONFIGURATION


########## PERMISSION GROUPS CONFIGURATION
ACCOUNT_ADMIN_GROUP_NAME = "Account Admin"
FUNDRAISER_GROUP_NAME = "Fundraiser"
########## end PERMISSION GROUPS CONFIGURATION


########## MEDIA CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = normpath(join(DJANGO_ROOT, 'media'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = '/media/'
########## END MEDIA CONFIGURATION


########## STATIC FILE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = normpath(join(DJANGO_ROOT, 'static'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = '/static/'

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = (
    normpath(join(DJANGO_ROOT, 'assets')),
)

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

WEBPACK_SERVER = False
########## END STATIC FILE CONFIGURATION


########## SECRET CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
SECRET_KEY = environ.get('DJANGO_SECRET_KEY')
########## END SECRET CONFIGURATION


########## FIXTURE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-FIXTURE_DIRS
FIXTURE_DIRS = (
    normpath(join(DJANGO_ROOT, 'fixtures')),
)
########## END FIXTURE CONFIGURATION


########## TEMPLATE CONFIGURATION
# See: https://docs.djangoproject.com/en/1.9/ref/settings/#templates
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'DIRS': [
            normpath(join(DJANGO_ROOT, 'templates')),
        ],
        'OPTIONS': {
            #'loaders': [
                #'django.template.loaders.filesystem.Loader',
                #'django.template.loaders.app_directories.Loader',
            #],
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
                # 'social_django.context_processors.backends',
                'social_django.context_processors.login_redirect',
                'frontend.apps.core.context_processors.version',
                'frontend.apps.core.context_processors.permission_constants',
                'frontend.apps.core.context_processors.account_info',
                'frontend.apps.core.context_processors.extract_settings',
                'frontend.apps.ux.context_processors.current_skin',
                'frontend.libs.django_view_router.context_processors.route',
            ]
        }
    },
]

SETTINGS_CONTEXT_EXPORT = {
    'environment': 'ENV',
    'recurly_api_key': 'RECURLY_PUBLIC_API_KEY',
    'WEBPACK_SERVER': 'WEBPACK_SERVER',
}
########## END TEMPLATE CONFIGURATION


########## MIDDLEWARE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#middleware-classes
MIDDLEWARE = (
    # Use GZip compression to reduce bandwidth.
    'django.middleware.gzip.GZipMiddleware',

    # Default Django middleware.
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',

    # Handles social auth exceptions (for contact import)
    'frontend.apps.contact.middleware.CustomSocialAuthExceptionMiddleware',

    # Allow for user impersonation
    # Note: This should always come before seat.UpdateLastActiveMiddleware
    'impersonate.middleware.ImpersonateMiddleware',

    # Enforce certain forced redirects, like for ToS
    'frontend.apps.core.middleware.ForcedRedirectMiddleware',
    # Update seat activity
    'frontend.apps.seat.middleware.UpdateLastActiveMiddleware',
    # Trigger delayed analysis, if needed
    'frontend.apps.analysis.middleware.DelayedAnalysisMiddleware',
    # breadcrumbs
    'breadcrumbs.middleware.BreadcrumbsMiddleware',
    # Audit logs
    'audit_log.middleware.UserLoggingMiddleware',
    'waffle.middleware.WaffleMiddleware',
)
########## END MIDDLEWARE CONFIGURATION


########## URL CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#root-urlconf
ROOT_URLCONF = '%s.urls' % SITE_NAME
########## END URL CONFIGURATION


########## APP CONFIGURATION
DJANGO_APPS = (
    # Default Django apps:
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.flatpages',

    # Useful template tags:
    'django.contrib.humanize',
)

THIRD_PARTY_APPS = (
    # Django REST framework
    'rest_framework',
    'rest_framework_jwt',
    'django_filters',
    # python-social-auth ORM
    'social_django',
    # integrate bootstrap and django forms
    'crispy_forms',
    'breadcrumbs',
    # Allow staff impersonation of other users
    "impersonate",
    # Add view routing
    'frontend.libs.django_view_router',
    # Django Activity Stream
    'actstream',
    # Django extensions
    'django_extensions',
    'sortable_listview',
    'waffle',
    # Failed login lockout application
    'axes',
)

LOCAL_APPS = (
    'frontend.apps.authorize',
    'frontend.apps.analysis',
    'frontend.apps.batch_jobs',
    'frontend.apps.call_time',
    'frontend.apps.campaign',
    'frontend.apps.campaign_admin',
    'frontend.apps.contact',
    'frontend.apps.contact_set',
    'frontend.apps.core',
    'frontend.apps.entities',
    'frontend.apps.filtering',
    'frontend.apps.locations',
    'frontend.apps.personas',
    'frontend.apps.role',
    'frontend.apps.seat',
    'frontend.apps.staff_admin',
    'frontend.apps.ux',
    'frontend.apps.external_app_integration',
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS
########## END APP CONFIGURATION


########## LOGGING CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] [dd.trace_id:%(dd.trace_id)s;dd.span_id:%(dd.span_id)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        }
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
        'kafka': {
            'level': 'INFO',
        },
        'factory': {
            'level': 'ERROR',
        },
        'django.request': {
            'handlers': ['mail_admins', 'console'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

def patch_celery_logging(LOGGING):
    # Patch the LOGGING config object when running under celery to add
    # additional context to logs to make tracking events to the original celery
    # task easier.
    LOGGING.setdefault('filters', {})['current_task_context'] = {
        '()': 'frontend.libs.utils.celery_utils.CurrentTaskContextFilter'
    }
    LOGGING.setdefault('formatters')['celery'] = {
        'format': '[%(asctime)s] %(levelname)s/%(processName)s [%(name)s:%(funcName)s:%(lineno)s] [dd.trace_id:%(dd.trace_id)s;dd.span_id:%(dd.span_id)s] [%(task_name)s(%(task_id)s);parent_id:%(parent_id)s;root_id:%(root_id)s] %(message)s'
    }
    LOGGING.setdefault('handlers', {})['console'].update(
        formatter='celery', filters=['current_task_context'])
########## END LOGGING CONFIGURATION


########## CELERY CONFIGURATION
# Detect whether we are in a celery worker
IN_CELERY_PROCESS = 'celery' in sys.modules['__main__'].__name__

CELERY_ACCEPT_CONTENT = ['pickle']
CELERY_TASK_SERIALIZER = 'pickle'
CELERY_RESULT_SERIALIZER = 'pickle'

# See: http://docs.celeryq.org/en/latest/configuration.html#celery-always-eager
CELERY_TASK_ALWAYS_EAGER = False

# See: http://celery.readthedocs.org/en/latest/configuration.html#celery-track-started
CELERY_TASK_TRACK_STARTED = True
CELERY_WORKER_SEND_TASK_EVENTS = True

# The secret sauce to requeueing incomplete/failed tasks is using ACKS_LATE and
# disabling the PREFETCH_MULTIPLIER.
CELERY_TASK_ACKS_LATE = True
CELERY_WORKER_PREFETCH_MULTIPLIER = 1
CELERY_TASK_REJECT_ON_WORKER_LOST = True
CELERY_RESULT_EXTENDED = True

# End celery's death-grip on logging!
CELERY_WORKER_HIJACK_ROOT_LOGGER = False
CELERY_WORKER_REDIRECT_STDOUTS = False

# Common redis broker transport options
CELERY_BROKER_TRANSPORT_OPTIONS = {
    # fanout_prefix: prefix broadcast messages so that they will only be
    # received by the active virtual host. This will be the default in the
    # future, so might as well start setting it now.
    'fanout_prefix': True,
    # fanout_patterns: to avoid workers receiving all task related events by
    # default, set to true so that the workers may only subscribe to worker
    # related events. This will be the default in the future.
    'fanout_patterns': True,
    # visibility_timeout: defines the number of seconds to wait for the worker
    # to acknowledge the task before the message is redelivered to another
    # worker
    'visibility_timeout': 3600
}

# Queues
CELERY_TASK_QUEUES = (
    Queue('_heartbeat', routing_key='_donotuse'),
    Queue('celery', routing_key='celery'),
    Queue('contact_import', routing_key='contact_import'),
    Queue('contact_deletion', routing_key='contact_deletion'),
    Queue('contact_merging', routing_key='contact_merging'),
    Queue('analysis_single_contact', routing_key='analysis_single_contact'),
    Queue('analysis_queue', routing_key='analysis_queue'),
    Queue('analysis_light', routing_key='analysis_light'),
    Queue('analysis_medium', routing_key='analysis_medium'),
    Queue('analysis_heavy', routing_key='analysis_heavy'),
    Queue('analysis_complete', routing_key='analysis_complete'),
    Queue('batch_csv', routing_key="batch_csv"),
    Queue('batch_call_sheet', routing_key="batch_call_sheet"),
    Queue('email', routing_key='email'),
    Queue('background', routing_key='background'),
)

# Batch size of parallelized tasks
ANALYSIS_PARALLEL_BATCH_SIZE = int(environ.get('ANALYSIS_PARALLEL_BATCH_SIZE', 75))
CALL_SHEET_PARALLEL_BATCH_SIZE = int(environ.get('CALL_SHEET_PARALLEL_BATCH_SIZE', 50))
EXPORT_CSV_PARALLEL_BATCH_SIZE = int(environ.get('EXPORT_CSV_PARALLEL_BATCH_SIZE', 1000))
# Used in EntityPersonContrib analysis. This timeout will cause the analysis to
# execute if no new contacts are resolved to entities after the time period
ANALYSIS_ENTITY_UPDATE_TIMEOUT = int(environ.get("ANALYSIS_ENTITY_UPDATE_TIMEOUT", 300))
ANALYSIS_ENTITY_MIN_PERCENTAGE = float(environ.get("ANALYSIS_ENTITY_MIN_PERCENTAGE", 0.05))
ANALYSIS_ENTITY_MIN_COUNT = int(environ.get("ANALYSIS_ENTITY_MIN_COUNT", 100))
# Partition the analysis to run in separate queues depending on the number
# of Contacts. Big analyses clog up everything else
# Format: dict( <Celery Queue name>: (<Partition min>, <Partition max>) )
ANALYSIS_QUEUE_PARTITIONS = dict(
    analysis_light=(None, 500),
    analysis_medium=(501, 5000),
    analysis_heavy=(5001, None)
)
CONTACT_IMPORT_PARALLEL_BATCH_SIZE = int(environ.get('CONTACT_IMPORT_PARALLEL_BATCH_SIZE', 100))
CONTACT_SET_DELETION_BATCH_SIZE = int(environ.get('CONTACT_SET_DELETION_BATCH_SIZE', 100))
REVUP_TASK_TTL_HOURS = 12
REVUP_TASK_REVOKE_MIN_TIMEDELTA = timedelta(seconds=86400)  # 1 day

# See: http://celery.readthedocs.org/en/latest/configuration.html#celery-task-result-expires
CELERY_RESULT_EXPIRES = timedelta(hours=REVUP_TASK_TTL_HOURS+1)

# Store the analysis runner in a cache instead of pickling into every subtask
ANALYSIS_RUNNER_USE_CACHE = False
BATCH_JOBS_USE_CACHE = False
########## END CELERY CONFIGURATION


########## WSGI CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'wsgi.application'
########## END WSGI CONFIGURATION


########## PYTHON-SOCIAL-AUTH CONFIGURATION
AUTH_USER_MODEL = 'authorize.RevUpUser'

# See: http://docs.mongoengine.org/en/latest/django.html
AUTHENTICATION_BACKENDS = (
    'frontend.apps.authorize.backends.CustomModelBackend',
    'social_core.backends.email.EmailAuth',
    # 'social_core.backends.facebook.FacebookOAuth2',
    'social_core.backends.google.GoogleOAuth2',
    'social_core.backends.linkedin.LinkedinOAuth2',
    'frontend.apps.authorize.backends.MultiNationBuilder',
    # 'social_core.backends.twitter.TwitterOAuth',
    # 'social_core.backends.username.UsernameAuth',
)

SOCIAL_AUTH_DISCONNECT_REDIRECT_URL = '/accounts/profile/'

SOCIAL_AUTH_EMAIL_FORM_HTML = 'authorize/register.html'
SOCIAL_AUTH_EMAIL_FORM_URL = '/accounts/login/'
SOCIAL_AUTH_EMAIL_VALIDATION_FUNCTION = 'example.app.mail.send_validation'
SOCIAL_AUTH_EMAIL_VALIDATION_URL = '/email-sent/'

SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']
FACEBOOK_EXTENDED_PERMISSIONS = ['email']

SOCIAL_AUTH_NEW_ASSOCIATION_REDIRECT_URL = '/oauth_finished/'
SOCIAL_AUTH_NEW_USER_REDIRECT_URL = '/contacts/manager/'
SOCIAL_AUTH_SANITIZE_REDIRECTS = True
SOCIAL_AUTH_STORAGE = 'social_django.models.DjangoStorage'
SOCIAL_AUTH_USER_MODEL = 'authorize.RevUpUser'
SOCIAL_AUTH_USERNAME_IS_FULL_EMAIL = True
SOCIAL_AUTH_REVOKE_TOKENS_ON_DISCONNECT = True
SOCIAL_AUTH_FIELDS_STORED_IN_SESSION = ['account_contacts',
                                        'nation_slug',
                                        'import_contacts']

SOCIAL_AUTH_PIPELINE = (
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.auth_allowed',
    'social_core.pipeline.social_auth.social_user',
    'social_core.pipeline.user.get_username',
    # 'frontend.apps.authorize.pipeline.verify_login',
    #'social_core.pipeline.mail.mail_validation',
    'social_core.pipeline.user.create_user',
    'frontend.apps.authorize.pipeline.handle_inactive_user',
    'frontend.apps.authorize.pipeline.user_password',
    'social_core.pipeline.social_auth.associate_user',
    'social_core.pipeline.social_auth.load_extra_data',
    #'social_core.pipeline.user.user_details',
    'frontend.apps.authorize.pipeline.user_update',
    'frontend.apps.contact.pipeline.import_contacts',
)
SOCIAL_AUTH_DISCONNECT_PIPELINE = (
    'social_core.pipeline.disconnect.allowed_to_disconnect',
    # Collects the social associations to disconnect.
    'social_core.pipeline.disconnect.get_entries',
    # Revoke any access_token when possible.
    'frontend.apps.authorize.pipeline.revoke_tokens',
    # Removes the social associations.
    'social_core.pipeline.disconnect.disconnect'
)
########## END PYTHON-SOCIAL-AUTH CONFIGURATION
# Not part of python-social-auth proper but rather revup configuration of
SOCIAL_AUTH_CONN_TIMEGAP = 3  # seconds
SOCIAL_AUTH_CONN_RETRIES = 2  # not including initial attempt

########## DJANGO CRISPY FORMS
CRISPY_TEMPLATE_PACK = 'bootstrap3'
########## END DJANGO CRISPY FORMS


########## REST FRAMEWORK GLOBAL CONFIG
REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
        'frontend.libs.api_renderers.CustomJSONRenderer',
        'frontend.libs.api_renderers.NoHTMLFormBrowsableAPIRenderer',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
    ),
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend',
    ),
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework.parsers.JSONParser',
        'rest_framework.parsers.FormParser',
        'rest_framework.parsers.MultiPartParser'
    ),
    'DEFAULT_VERSIONING_CLASS':
        'rest_framework.versioning.AcceptHeaderVersioning'
}

REST_FRAMEWORK_EXTENSIONS = {
    'DEFAULT_CACHE_ERRORS': False,
    'DEFAULT_CACHE_RESPONSE_TIMEOUT': 60*60,
}

JWT_AUTH = {
    'JWT_EXPIRATION_DELTA': timedelta(seconds=604_800),  # 7 days
    'JWT_ALLOW_REFRESH': True,
    # This is the maximum time a token can be refreshed since the original login
    'JWT_REFRESH_EXPIRATION_DELTA': timedelta(seconds=2_592_000),  # 30 days
    # This allows us to modify the API response from login.
    'JWT_RESPONSE_PAYLOAD_HANDLER': "frontend.apps.authorize.api.serializers.jwt_response_payload_handler",
}
########## END REST FRAMEWORK GLOBAL CONFIG


########## RECURLY CONFIG
# Public key should be used by js functions
RECURLY_PUBLIC_API_KEY = environ.get('RECURLY_PUBLIC_API_KEY', "ewr1-YxuwjruLxXBE2SckNXzBTQ")
# Private key should be used by Python API. DO NOT EXPOSE TO FRONTEND.
RECURLY_PRIVATE_API_KEY = environ.get('RECURLY_PRIVATE_API_KEY', "e46d62693aa144619439c1f4a398ef0c")
RECURLY_SUBDOMAIN = environ.get('RECURLY_SUBDOMAIN', "revup-test")

RECURLY_NOTIFICATION_IPS = [
    '50.18.192.88',
    '52.8.32.100',
    '52.9.209.233',
    '50.0.172.150',
    '52.203.102.94',
    '52.203.192.184',
]
########## end RECURLY CONFIG


########## ZENDESK API CONFIG
ZENDESK_ENABLED = str_to_bool(environ.get('ZENDESK_ENABLED', False))
ZENDESK_EMAIL = 'engineering@revup.com'
ZENDESK_SUBDOMAIN = 'revupsupport'
ZENDESK_PASSWORD = environ.get('ZENDESK_PASSWORD', '')
########## end ZENDESK CONFIG


########## CloudSponge CONFIG
CLOUDSPONGE_DOMAIN_KEY = 'DQVKG6LVFFVLZUJUWCML'
CLOUDSPONGE_DOMAIN_PASSWORD = 'wYsrMKuSPAURm8xT'
CLOUDSPONGE_CACHE_QUERY = False

# If a domain is supplied, contact imports will be redirected to
# the given domain. E.g. "apps.revup.com"
CONTACT_IMPORT_DOMAIN = ""
########## end CloudSponge CONFIG


########## MISCELLANEOUS REVUP CONFIG
REVUP_INVITE_EXPIRATION_DAYS = 14

# Personal contacts aren't limited to a single account, so we can't really
# configure them based on their account
PERSONAL_CONTACT_ANALYZE_LIMIT = int(environ.get("PERSONAL_CONTACT_ANALYZE_LIMIT", 10_000))
PERSONAL_CONTACT_CREATE_LIMIT = int(environ.get("PERSONAL_CONTACT_CREATE_LIMIT", 30_000))

MERGING_ADDRESS_SIMILARITY_THRESHOLD = float(environ.get('MERGING_ADDRESS_SIMILARITY_THRESHOLD', "0.85"))
MAXIMUM_CALLGROUP_SIZE = int(environ.get("MAXIMUM_CALLGROUP_SIZE", 5000))
########## END MISCELLANEOUS REVUP CONFIG

########## DJANGO AXES CONFIG
# The number of login attempts allowed before a record is created for the
# failed logins.
AXES_LOGIN_FAILURE_LIMIT = 5
# the name of the form field that contains your users usernames.
AXES_USERNAME_FORM_FIELD = "email"
# After the number of allowed login attempts are exceeded, should we lock out
# this IP (and optional user agent)?
AXES_LOCK_OUT_AT_FAILURE = True
# If set, defines a period of inactivity after which old failed login attempts
# will be forgotten. Can be set to a python timedelta object or an integer. If
# an integer, will be interpreted as a number of hours.
AXES_COOLOFF_TIME = 1

# If True, it will look for the IP address from the header defined at
# AXES_REVERSE_PROXY_HEADER. Please make sure if you enable this setting to
# configure your proxy to set the correct value for the header, otherwise you
# could be attacked by setting this header directly in every request. Default:
# False
AXES_BEHIND_REVERSE_PROXY = False
# If AXES_BEHIND_REVERSE_PROXY is True, it will look for the IP address from
# this header. Default: HTTP_X_FORWARDED_FOR
AXES_REVERSE_PROXY_HEADER = 'HTTP_X_FORWARDED_FOR'

# If set, specifies a template to render when a user is locked out. Template
# receives cooloff_time and failure_limit as context variables. Default: None
AXES_LOCKOUT_TEMPLATE = 'authorize/login.html'

# If set, specifies a URL to redirect to on lockout.
# AXES_LOCKOUT_URL = None
########## end DJANGO AXES CONFIG


########## DJANGO PASSWORDS CONFIG
PASSWORD_MIN_LENGTH = 8
# The name of this option is a little misleading. The match threshold
# it represents is how close the password is to a list of common
# sequences (e.g. "abcedfghijk"). If the password is too close to matching
# part of that sequence, it is rejected.
PASSWORD_MATCH_THRESHOLD = 0.95
PASSWORD_COMPLEXITY = {
    # You can omit any or all of these for no limit for that particular set
    "UPPER": 1,        # Uppercase
    "LOWER": 1,        # Lowercase
    "DIGITS": 1,         # Digits
    "SPECIAL": 1,      # Characters that are not alphanumeric or spaces. e.g. punctuation
    # "WORDS": 1         # Words (alphanumeric sequences separated by a whitespace or punctuation character)
}
# How many of the complexity values must be met in the password.
PASSWORD_COMPLEXITY_FLEXIBILITY = 3
########## end DJANGO PASSWORDS CONFIG


########## DJANGO IMPERSONATE CONFIG
### For options, See: https://bitbucket.org/petersanchez/django-impersonate
IMPERSONATE = {
    'REDIRECT_URL': '/',
    'ALLOW_SUPERUSER': True,
    'CUSTOM_USER_QUERYSET': 'frontend.apps.authorize.utils.impersonate_user_queryset',
}
########## end DJANGO IMPERSONATE CONFIG


########## GEOPY CONFIG
GEONAMES_USERNAME = "charles_revup"
########## end GEOPY CONFIG


########## Activity Stream CONFIG
ACTSTREAM_SETTINGS = {
    'USE_JSONFIELD': True,
}
########## end Activity Stream CONFIG


########## Backend Contact Kafka CONFIG
PERSON_ENTITIES_COLLECTION = environ.get("PERSON_ENTITIES_COLLECTION",
                                         "person_entities")
BACKEND_CONTACT_TOPIC = environ.get("BACKEND_CONTACT_TOPIC", "contacts-raw")
BACKEND_CONTACT_UPDATES_TOPIC = environ.get("BACKEND_CONTACT_UPDATES_TOPIC",
                                            "contacts-update")
BACKEND_CONTACT_DELETE_TOPIC = environ.get("BACKEND_CONTACT_DELETE_TOPIC",
                                            "contacts-delete")
BACKEND_CLIENT_CONTRIB_QUEUE_TOPIC = environ.get(
    "BACKEND_CLIENT_CONTRIB_QUEUE_TOPIC", "producer-client-queue-s3")
BACKEND_KAFKA_USERNAME = environ.get("BACKEND_KAFKA_USERNAME")
BACKEND_KAFKA_PASSWORD = environ.get("BACKEND_KAFKA_PASSWORD")
BACKEND_KAFKA_SECURITY_ENABLED = str_to_bool(
    environ.get("BACKEND_KAFKA_SECURITY_ENABLED", False))
BACKEND_KAFKA_BOOTSTRAP_SERVERS = (
    environ.get("BACKEND_KAFKA_BOOTSTRAP_SERVERS", "") or
    "localhost:9092").split(',')
BACKEND_KAFKA_API_VERSION_AUTO_TIMEOUT = int(
    environ.get("BACKEND_KAFKA_API_VERSION_AUTO_TIMEOUT", 10000))
ENABLE_KAFKA_PUBLISHING = str_to_bool(
    environ.get("ENABLE_KAFKA_PUBLISHING", True))
########## end Backend Contact Kafka CONFIG


########## Account Contribution CSV Upload CONFIG
ACCOUNT_CONTRIBUTION_S3_STORAGE_ENABLED = environ.get(
    "ACCOUNT_CONTRIBUTION_S3_STORAGE_ENABLED", False)
ACCOUNT_CONTRIBUTION_S3_BUCKET = environ.get(
    'ACCOUNT_CONTRIBUTION_S3_BUCKET', 'revup-client-upload')
ACCOUNT_CONTRIBUTION_S3_KEY_PREFIX = environ.get(
    'ACCOUNT_CONTRIBUTION_S3_KEY_PREFIX', 'client-csv-upload')
########## end Account Contribution CSV Upload CONFIG


########## Storage CONFIG
BATCH_JOB_S3_STORAGE_ENABLED = str_to_bool(
    environ.get("BATCH_JOB_S3_STORAGE_ENABLED", False))

CONTACT_UPLOAD_S3_STORAGE_ENABLED = str_to_bool(
    environ.get("CONTACT_UPLOAD_S3_STORAGE_ENABLED", False))
########## end Storage CONFIG


class _GlobalCacheKeys(object):
    """In order to avoid cache collisions, use this class to store them
        in a common place.
    """
    analysis_results_count_cache = "analysis_results_count_cache_{analysis_id}_{modified}"
    analysis_runner_cache = "analysis_runner_cache_{analysis_hash}"
    recurly_subscription_plans_cache = "recurly_subscription_plans_cache"
GLOBAL_CACHE_KEYS = _GlobalCacheKeys()


########## Read and set the Product Version
try:
    with open('version', 'r') as f:
        PRODUCT_VERSION = f.readline().strip()

        product_release_date = int(f.readline().strip())
        PRODUCT_RELEASE_DATE = datetime.fromtimestamp(
            product_release_date).strftime('%m/%d/%Y')
except IOError:
    PRODUCT_VERSION, PRODUCT_RELEASE_DATE = "", ""
########## end Version code


########## NATIONBUILDER CONFIG
NATION_BUILDER_AUTH_URI = 'nationbuilder/oauth2/'
########## NATIONBUILDER CONFIG


########## SENTRY CONFIGURATION
if environ.get('SENTRY_DSN'):
    INSTALLED_APPS += (
        'raven.contrib.django.raven_compat',
    )
    RAVEN_CONFIG = {
        'dsn': environ.get('SENTRY_DSN'),
        'release': environ.get('HEROKU_SLUG_COMMIT'),
        'environment': environ.get('HEROKU_APP_NAME'),
        'name': (environ.get('DD_HOSTNAME') or
                 (environ.get("HEROKU_APP_NAME") and
                  '{}.{}'.format(environ.get('HEROKU_APP_NAME'),
                                 environ.get('DYNO'))))
    }
########## END SENTRY CONFIGURATION

########## DATADOG CONFIGURATION
from ddtrace.filters import FilterRequestsOnUrl
from frontend.libs.utils.datadog_utils import RateSampleRequestsOnUrl
INSTALLED_APPS = (
    # DataDog tracing
    'ddtrace.contrib.django',
) + INSTALLED_APPS
DATADOG_TRACE = {
    'DEFAULT_SERVICE': 'frontend',
    'TAGS': {'env': environ.get('HEROKU_APP_NAME')},
}
LOGGING['loggers']['ddtrace'] = {
    'handlers': ['console'],
    'level': 'INFO',
}
tracer.configure(
    settings={
        'FILTERS': [
            # Drop traces of requests that hit the hirefire endpoint
            FilterRequestsOnUrl(r'.*/hirefire/[-\d\w]+/info$'),
            # Only send 20% of traces that hit the task poll requests
            RateSampleRequestsOnUrl(r'.*/rest/users/\d+/seats/\d+/tasks/', 0.2),
        ],
    }
)
########## END DATADOG CONFIGURATION

GDAL_LIBRARY_PATH = os.getenv('GDAL_LIBRARY_PATH')

CSRF_FAILURE_VIEW = 'frontend.apps.core.views.csrf_failure'
