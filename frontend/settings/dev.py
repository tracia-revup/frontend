"""Development settings and globals."""
from .common import *
import psycopg2

########## DEBUG CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = True

# This is not secure, but for dev it does not need to be.
SECRET_KEY = 'bAXZ65PSPx7sr481OwWZf1ax0x4JyuXboMFzFVKSB28LRwnk3l'

if not DEFAULT_DOMAIN:
    DEFAULT_DOMAIN = "local.revup.com:8000"

if DEBUG:
    import mimetypes
    mimetypes.add_type("font/opentype", ".otf", True)
########## END DEBUG CONFIGURATION


# GENERAL CONFIGURATION
if not ENV:
    ENV = 'dev'

ALLOWED_HOSTS = ['local.revup.com']
# END GENERAL CONFIGURATION

########## EMAIL CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
########## END EMAIL CONFIGURATION


########## DATABASE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'revup',
        'USER': 'engine',  # Rev Up your Engine!
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'PORT': '5432',
    },
    'contactdb': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'contacts',
        'USER': 'engine',  # Rev Up your Engine!
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'PORT': '5432',
    },
}
########## END DATABASE CONFIGURATION


########## CELERY CONFIGURATION
CELERY_RESULT_BACKEND = 'mongodb://localhost/'
CELERY_MONGODB_BACKEND_SETTINGS = {
    'database': 'celery',
    'taskmeta_collection': 'my_taskmeta_collection',
}
CELERY_BROKER_TRANSPORT_OPTIONS = {}
CELERY_BROKER_URL = 'mongodb://localhost/celery'

if IN_CELERY_PROCESS:
    patch_celery_logging(LOGGING)
########## END CELERY CONFIGURATION


########## TOOLBAR CONFIGURATION
# See: https://github.com/django-debug-toolbar/django-debug-toolbar#installation
DEBUG_TOOLBAR_PATCH_SETTINGS = False

DEBUG_TOOLBAR_CONFIG = dict(
    JQUERY_URL="",
    SHOW_COLLAPSED=True,
    SHOW_TOOLBAR_CALLBACK='frontend.settings.dev.custom_show_debug_toolbar',
)

INSTALLED_APPS += (
    'debug_toolbar',
)
DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.profiling.ProfilingPanel',
    # 'debug_toolbar_mongo.panel.MongoDebugPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
]
INTERNAL_IPS = ('127.0.0.1',)

MIDDLEWARE += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

########## END TOOLBAR CONFIGURATION

########## ENVIRONMENT-SPECIFIC PYTHON-SOCIAL-AUTH RELATED CONFIGURATION

SOCIAL_AUTH_FACEBOOK_KEY = 'TODO'
SOCIAL_AUTH_FACEBOOK_SECRET = 'TODO'

SOCIAL_AUTH_FACEBOOK_KEY = '299103923615134'
SOCIAL_AUTH_FACEBOOK_SECRET = '05dff31a407b11fa0acd0008585c0ef0'
SOCIAL_AUTH_FACEBOOK_SCOPE = [
    'public_profile', 'user_friends'
]

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '21679637810-ig1r2buo7kgjig2on5653fflhm0t94v0.apps.googleusercontent.com'
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'KUHW_8_OHtjgvwhOMq1G-8o3'
SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = [
    'https://www.google.com/m8/feeds',
]
SOCIAL_AUTH_GOOGLE_OAUTH2_AUTH_EXTRA_ARGUMENTS = {'access_type': 'offline'}

SOCIAL_AUTH_LINKEDIN_OAUTH2_KEY = '75yws5f305h5th'
SOCIAL_AUTH_LINKEDIN_OAUTH2_SECRET = 'ZGZuJqGDHauZbpQp'
SOCIAL_AUTH_LINKEDIN_OAUTH2_SCOPE = [
    'r_network', 'r_fullprofile', 'r_emailaddress'
]

SOCIAL_AUTH_TWITTER_KEY = 'fTO1viOC5gvKWUJzeksKtypgJ'
SOCIAL_AUTH_TWITTER_SECRET = 'ovxtCbfD0Xbjh7n9iJZQl08VeZv210tY4gnsVQnu5rSzYgQneU'

SOCIAL_AUTH_NATIONBUILDER_KEY = '025a0e522b737f8715699887696baba5248fa1fbee37a0f7ab0942e8d562790e'
SOCIAL_AUTH_NATIONBUILDER_SECRET = '459f83e0937fcd9e420479b4263e11a9e8ae11895283297f57c437486a3a382e'

########## END ENVIRONMENT-SPECIFIC PYTHON-SOCIAL-AUTH RELATED CONFIGURATION

######## TEST CONFIGURATION
TEST_RUNNER = "frontend.libs.utils.test_utils.PytestTestRunner"
######## END TEST CONFIGURATION


def custom_show_debug_toolbar(request):
    """This can be overridden in local.py to hide the toolbar.
    Just return False to disable the toolbar.
    """
    from debug_toolbar.middleware import show_toolbar
    return show_toolbar(request)


## Import optional local settings
try:
    # A developer might want to override some settings locally.
    # To do so, just add a file: 'settings/local.py' and change any settings
    # you wish to override. Settings are overridden completely using this
    # technique; you won't be able to do "+=" like above.
    from .local import *
except ImportError:
    pass
