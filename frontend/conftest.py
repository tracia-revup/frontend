
def pytest_configure(config):
    '''Hack to determine if being run by a test

    Source: https://docs.pytest.org/en/latest/example/simple.html#detect-if-running-from-within-a-pytest-run
    '''
    import sys

    sys._called_from_test = True


def pytest_unconfigure(config):
    import sys

    del sys._called_from_test
