/****************************/
/*                          */
/*     jQuery Plug-ins      */
/*                          */
/****************************/
var revupListView = (function (jQuery, window, undefined) {
    var viewDefinition = null;
    var debugMode = !!Cookies.get("debug"); // debug mode state

    function isViewDefinitionDefined() {
        if (viewDefinition == null) {
            console.error("revupListView - viewDefinition not loaded");

            return false;
        }

        return true;
    }; // isViewDefinitionDefined

    /*
     *
     * Helpers for list entry type "rankingField"
     *
     */
    function scoreFormat(rawValue)
    {
        // if undefined rawValue return -1
        if (!rawValue) {
            rawValue = 0;
        }

        // get the score, if the fraction part is 0 don't show it
        score = rawValue.toFixed(1);

        // remove the fraction if fraction value is 0
        var tmp = score.toString();
        tmp = tmp.split('.');
        if ((tmp.length == 2) && (Number(tmp[1]) == 0)) {
            score = Number(tmp[0]);
        }
        else {
            score = Number(score);
        }

        if (score == 0) {
            return "&nbsp;";
        }

        return score;
    }; // scoreFormat

    function scoreColorClass(rawValue, formatedScore)
    {
        var politicalSpectrumClass = "";
        if (rawValue == undefined || !formatedScore) {
            return politicalSpectrumClass;
        }
        var polSpectrum = rawValue.toFixed(1);
        if ((polSpectrum >= 0) && (formatedScore > 0)) {
            if (polSpectrum == 0) {
                politicalSpectrumClass = 'political-spectrum-bar-republican'
            }
            else if ((polSpectrum > 0) && (polSpectrum < 0.4)) {
                politicalSpectrumClass = 'political-spectrum-bar-republican-lite'
            }
            else if ((polSpectrum >= 0.4) && (polSpectrum <= 0.6)) {
                politicalSpectrumClass = 'political-spectrum-bar-neutral'
            }
            else if ((polSpectrum > 0.6) && (polSpectrum < 1)) {
                politicalSpectrumClass = 'political-spectrum-bar-democrat-lite'
            }
            else {
                politicalSpectrumClass = 'political-spectrum-bar-democrat'
            }
        }

        return politicalSpectrumClass;
    }; // scoreColorClass

    function scoreColor(rawValue)
    {
        var color = "";
        if (!rawValue){
            rawValue = -1
        }
        var val = rawValue.toFixed(1);

        if (val == 0) {
            color = '#fc3a3a';
        }
        else if ((val > 0) && (val < 0.4)) {
            color = '#fc9090';
        }
        else if ((val >= 0.4) && (val <= 0.6)) {
            color = '#d6c0f0';
        }
        else if ((val > 0.6) && (val < 1)) {
            color = '#90c3f0';
        }
        else {
            color =  '#298fe6';
        }

        return color
    } // scoreColor

    /*
     *
     * format/display entry givingIcon
     *
     */
    function formatGivingIcon(k, candidateImage)
    {
        var bCandidateGaveThisCycle = false;
        if  (k && k.giving_this_period && k.giving_this_period > 0) {
            bCandidateGaveThisCycle = true;
        }
        var bMaxedForCycle = false;
        if (k && "giving_availability" in k && k.giving_availability ==  0) {
            bMaxedForCycle = true;
        }
        var lastYearOfGiving = "";
        if (k && k.year_of_last_contribution && k.year_of_last_contribution != 0) {
            lastYearOfGiving = k.year_of_last_contribution;
        }
        var bOpponents = false;
        var bKeyContrib = false;
        var oppoContribYear = 0;
        if (k && k.key_contributions && k.key_contributions.length != 0) {
            var keyLst = k.key_contributions;
            for (var kIndex = 0; kIndex < keyLst.length; kIndex++) {
                if (keyLst[kIndex].is_ally) {
                    bKeyContrib = true;
                }
                else {
                    bOpponents = true;

                    var lastContrib = keyLst[kIndex].last_contrib;
                    if (lastContrib != undefined) {
                        lastContrib = lastContrib.split('-');

                        var year = Number(lastContrib[0]);
                        if (year > oppoContribYear) {
                            oppoContribYear = year;
                        }
                    }
                }
            }
        }

        var sTmp = [];
        if (lastYearOfGiving != "" || bOpponents || bKeyContrib) {
            sTmp.push('<div class="iconSection">');
                sTmp.push('<div class="iconContainer">')
                    // key contributor
                    sTmp.push('<div class="iconDiv">');
                        if (bKeyContrib) {
                            sTmp.push('<div class="keyContribIcon"></div>');
                        }
                        else {
                            sTmp.push('<div class="noKeyContrib"></div>')
                        }
                    sTmp.push('</div>');

                    if (lastYearOfGiving != "") {
                        // candidate icon
                        sTmp.push('<div class="iconDiv">');
                            var extraClass = "";
                            if ((bMaxedForCycle) || ((lastYearOfGiving != 0) && (!bCandidateGaveThisCycle))) {
                                extraClass = " withMsg";
                            }

                            // giving to icon
                            // which icon to display
                            // 1) If there is skinning info in the skin template use it
                            // 2) If there is there is a candidate icon use it
                            // 3) If none of the above the use default color and the system provided default icon text
                            if ((candidateImage != '') && (candidateImage != '/static/img/sample/icon.png') && (currentSkin.candidateIconName == '')) {
                                sTmp.push('<img class="icon' + extraClass + '" src="' + candidateImage + '">');
                            }
                            else {
                                var iconDivColor = "";
                                if (currentSkin.candidateIconColor) {
                                    iconDivColor = ' style="background-color:' + currentSkin.candidateIconColor +';"';
                                }
                                var candidateName = currentSkin.defaultAccountIconText;
                                if (currentSkin.candidateIconName) {
                                    candidateName = currentSkin.candidateIconName;
                                }
                                sTmp.push('<div class="candidateIconDiv"' + iconDivColor + '>');
                                    sTmp.push('<div class="text">' + candidateName + '</div>');
                                sTmp.push('</div>')
                            }

                            // messageing under icon
                            if (bMaxedForCycle) {
                                sTmp.push('<div class="msg">Maxed</div>');
                            }
                            else if ((lastYearOfGiving != 0) && (!bCandidateGaveThisCycle)) {
                                sTmp.push('<div class="msg">' + lastYearOfGiving + '</div>');
                            }
                        sTmp.push('</div>');
                    }

                    if (bOpponents) {
                        // opponent icon
                        var extraClass = "";
                        if (oppoContribYear != 0) {
                            extraClass = " withMsg";
                        }
                        sTmp.push('<div class="iconDiv">');
                            sTmp.push('<div class="oppo' + extraClass + '">oppo</div>');
                            if (oppoContribYear != 0) {
                                sTmp.push('<div class="msg">' + oppoContribYear + '</div>');
                            }
                        sTmp.push('</div>');
                    }
                sTmp.push('</div>');
            sTmp.push('</div>');
        }

        return sTmp.join('');
    }; // formatGivingIcon

    function buildSliderBar(bBlank, extraClass)
    {
        var sTmp = [];

        var blank = "";
        if (bBlank) {
            blank = ' blank';
        }
        sTmp.push('<div class="slider' + blank + extraClass + '">');
        if (!bBlank) {
            sTmp.push('<div class="sOuter"></div>');
                sTmp.push('<div class="sInner"></div>');
                    sTmp.push('<div class="sCenter"></div>')
                sTmp.push('<div class="sInner"></div>');
            sTmp.push('<div class="sOuter"></div>');
        }
        sTmp.push('</div>');

        return sTmp.join('')
    } // buildSliderBar

    return {
        loadDefinition: function(def)
        {
            viewDefinition = def;
        }, // loadDefinition

        buildListHeader: function(seatId, permission, joinPermission)
        {
            // make sure the viewDefinition is defined
            if (!isViewDefinitionDefined()) {
                console.error(" revupListView --> buildLstHeader");

                return "";
            }

            // get the stored column sizes
            var colWidth = localStorage.getItem('rankingColWidth-v3-' + seatId);
            if (colWidth) {
                colWidth = JSON.parse(colWidth);
            }

            var sTmp = [];
            sTmp.push('<div class="' + viewDefinition.headerSectionClass + '">');
                for (var i = 0; i < viewDefinition.headerFields.length; i++) {
                    var sort = viewDefinition.headerFields[i].sortable;
                    var field = viewDefinition.headerFields[i];
                    var fieldCSS = field.css.replace(' sortable', '');
                    var style = "";

                    // see if there is permission to display this column only if permission is passed in and
                    // the field has what permission defined
                    if ((joinPermission) && (field.permission) && (field.permission != joinPermission)) {
                        continue;
                    }
                    else if ((!joinPermission) && (permission) && (field.permission) && (!permission[field.permission])) {
                        continue;
                    }

                    // build the style for the column
                    if ((field.position != undefined) && (field.position != "")) {
                        style += "text-align:" + field.position + ";";
                    }
                    if ((colWidth) && (colWidth[fieldCSS])) {
                        style += "width:" + colWidth[fieldCSS] + "px;";
                    }
                    else if (field.width) {
                        let bNoPerm = false;
                        if (joinPermission && (field.noPerm) && (field.noPermWidth)) {
                            for (let i = 0; i < field.noPerm.length; i++) {
                                if (field.noPerm[i] == joinPermission) {
                                    style += "width:" + field.noPermWidth[i] + ";";
                                    bNoPerm = true

                                    break;
                                }
                            }
                        }
                        else if ((!joinPermission) && (permission) && (field.noPerm) && (field.noPermWidth)) {
                            if (!permission[field.noPerm]) {
                                if ($.isArray(field.noPerm)) {
                                    for (let i = 0; i < field.noPerm.length; i++) {
                                        if (permission[field.noPerm[i]]) {
                                            style += "width:" + field.noPermWidth[i] + ";";
                                            bNoPerm = true

                                            break;
                                        }
                                    }

                                }
                                else {
                                    style += "width:" + field.noPermWidth + ";";
                                }
                            }
                        }

                        if (!bNoPerm) {
                            style += "width:" + field.width + ";";
                        }
                    }
                    if ((field.style != undefined) && (field.style != "")) {
                        style += field.style + ';';
                    }
                    if (style != "") {
                        style = ' style=" ' + style + '"';
                    }

                    // see if there is a min column width
                    var extraAttr = ""
                    if (field.minColWidth) {
                        extraAttr += ' minColWidth="' + field.minColWidth + '"';
                    }

                    // add the sort key
                    if (field.sortkey) {
                        extraAttr += ' sortkey="' + field.sortkey + '"';
                    }

                    // add key of column name based on css
                    if (field.css != "") {
                        extraAttr += ' columnName="' + field.css + '"';
                    }


                    // build the css
                    var css = "column";
                    if (field.css != "") {
                        css += " " + field.css;
                    }
                    if (field.sortable) {
                        css += " sortable";
                    }

                    if (field.enablePermission && permission && !permission[field.enablePermission]) {
                        css += ' disabled';
                    }

                    var classCss = (viewDefinition.headerFields[i].css);
                    if ((sort == true) && (field.sortFirst)) {
                        css += ' activeSort';
                        extraAttr += ' sortDir="desc"';
                        sTmp.push('<div class="' + css + '"' + style + extraAttr + '>');
                            sTmp.push('<div class="' + viewDefinition.headerLabelClass + '" style= "color: ' + revupConstants.color.primaryGreen2 + '">' + field.text + '</div>');
                            sTmp.push('<div class="activeSort ' + 'icon icon-triangle-dn "' + 'style="display: inline-block; vertical-align: top; color: ' + revupConstants.color.primaryGray4 + '"</div>');
                        sTmp.push('</div></div>');

                    }
                    else if (sort == true) {
                        sTmp.push('<div class="' + css + '"' + style + extraAttr + '>');
                            sTmp.push('<div class="' + viewDefinition.headerLabelClass + '" style= ""' + '">' + field.text + '</div>');
                            sTmp.push('<div class="' + 'icon icon-triangle-up "' + 'style="position: relative;display: inline-block; vertical-align: top; color: ' +  revupConstants.color.primaryGray4 + '; display: none"</div>');
                        sTmp.push('</div></div>');
                    }
                    else {
                        sTmp.push('<div class="' + css + '"' + style + extraAttr + '>');
                        sTmp.push('<div class="' + viewDefinition.headerLabelClass + '">' + field.text + '</div></div>');
                    }

                    // see if there is a slider
                    if (permission && field.noPermSlider && !(permission[field.noPermSlider])) {
                        continue;
                    }

                    if (field.sliderAfter) {
                        let bShowSlider = true;
                        if (field.sliderPermissions) {
                            bShowSlider = false;
                            for (let i = 0; i < field.sliderPermissions.length; i++) {
                                if ((joinPermission && field.sliderPermissions[i] == joinPermission) ||
                                    (permission && permission[field.sliderPermissions[i]])) {
                                    bShowSlider = true;
                                    break;
                                }
                            }

                        }
                        if (bShowSlider) {
                                var extraClass = '';
                                if (viewDefinition.headerFields[i + 1].sortable){
                                    extraClass += ' sortRight';
                                }
                                if(viewDefinition.headerFields[i].sortable){
                                    extraClass += ' sortLeft';
                                }
                            sTmp.push(buildSliderBar(false, extraClass));
                        }
                    }
                }
            sTmp.push('</div>');

            return sTmp.join('');
        }, // buildListHeader

        buildListEntry: function(entryClass, entryStyle, entryAttr, data, seatId, permission, joinPermission)
        {
            // make sure the viewDefinition is defined
            if (!isViewDefinitionDefined()) {
                console.error(" revupListView --> buildLstHeader");

                return "";
            }

            // get the stored column sizes
            var colWidth = localStorage.getItem('rankingColWidth-v3-' + seatId);
            if (colWidth) {
                colWidth = JSON.parse(colWidth);
            }

            // build the entry class
            var eClass = "entry";
            if ((viewDefinition.entryClass != undefined) && (viewDefinition.entryClass != "")) {
                eClass += " " + viewDefinition.entryClass;
            }
            if ((entryClass != undefined) && (entryClass != "")) {
                eClass += " " + entryClass;
            }

            // build the entry style
            var eStyle = "";
            if ((viewDefinition.entryStyle != undefined) && (viewDefinition.entryStyle != "")) {
                eStyle += " " + viewDefinition.entryStyle + ';';
            }
            if ((entryStyle != undefined) && (entryStyle != "")) {
                eStyle += " " + entryStyle + ";";
            }
            if (eStyle != "") {
                eStyle = ' style="' + eStyle + '"';
            }

            // build the entry attr
            var eAttr = "";
            if ((viewDefinition.entryAttr != undefined) && (viewDefinition.entryAttr != "")) {
                eAttr += " " + viewDefinition.entryAttr + ' ';
            }
            if ((entryAttr != undefined) && (entryAttr != "")) {
                eAttr += " " + entryAttr + " ";
            }
            if (eAttr != "") {
                eAttr = ' ' + eAttr;
            }

            var sTmp = [];
            sTmp.push('<div class="' + eClass + '"' + eStyle + eAttr + '>');
                for (var i = 0; i < viewDefinition.entryFields.length; i++) {
                    var field = viewDefinition.entryFields[i];
                    var fieldCSS = field.css.replace(' sortable', '');
                    var style = "";

                    // see if there is permission to display this column only if permission is passed in and
                    // the field has what permission defined
                    if ((joinPermission) && (field.permission) && (field.permission != joinPermission)) {
                        continue;
                    }
                    else if ((!joinPermission) && (permission) && (field.permission) && (!permission[field.permission])) {
                        continue;
                    }

                    // build the style for the column
                    if ((field.position != undefined) && (field.position != "")) {
                        style += "text-align:" + field.position + ";";
                    }

                    if ((colWidth) && (colWidth[fieldCSS])) {
                        style += "width:" + colWidth[fieldCSS] + "px;";
                    }
                    else if (field.width) {
                        let bNoPerm = false;
                        if (joinPermission && (field.noPerm) && (field.noPermWidth)) {
                            for (let i = 0; i < field.noPerm.length; i++) {
                                if (field.noPerm[i] == joinPermission) {
                                    style += "width:" + field.noPermWidth[i] + ";";
                                    bNoPerm = true

                                    break;
                                }
                            }
                        }
                        else if ((!joinPermission) && (permission) && (field.noPerm) && (field.noPermWidth)) {
                            if (!permission[field.noPerm]) {
                                if ($.isArray(field.noPerm)) {
                                    for (let i = 0; i < field.noPerm.length; i++) {
                                        if (permission[field.noPerm[i]]) {
                                            style += "width:" + field.noPermWidth[i] + ";";
                                            bNoPerm = true

                                            break;
                                        }
                                    }
                                }
                                else {
                                    style += "width:" + field.noPermWidth + ";";
                                }
                            }
                        }

                        if (!bNoPerm) {
                            style += "width:" + field.width + ";";
                        }
                    }
                    if ((field.style != undefined) && (field.style != "")) {
                        style += field.style + ";";
                    }
                    if (style != "") {
                        style = ' style="' + style + '"';
                    }

                    // build the css
                    var css = "column";
                    if (field.css != "") {
                        css += " " + field.css;
                    }

                    if (field.enablePermission && permission && !permission[field.enablePermission]) {
                        css += ' disabled';
                    }

                    // get data for this control
                    var d = data;
                    if ((field.dataSection != undefined) && (field.dataSection != "")) {
                        d = data[field.dataSection];
                    }

                    // attributes for the column div
                    var attr = "";
                    if (field.colDivAttr) {
                        var attrLst = field.colDivAttr;
                        for(var a = 0; a < attrLst.length; a++) {
                            var attrDef = attrLst[a];
                            var attrData = data;
                            if (attrDef.section) {
                                attrData = attrData[attrDef.section];
                            }

                            var v = "";
                            if (attrDef.field) {
                                v = attrData[attrDef.field];
                            }
                            else if (typeof attrData != 'object') {
                                v = attrData;
                            }

                            if ((attrDef.name) && (v != "")) {
                                if (attr != "") {
                                    attr += " ";
                                }
                                attr += attrDef.name + '="' + v + '"';
                            }
                        }

                        attr = " " + attr;
                    }

                    // see if a title tag needs to be added
                    var title = '';
                    switch (field.type) {
                        case 'dollar':
                            var amt = d[field.dataField];
                            if (!$.isNumeric(amt)) {
                                title = 'title="' + amt + '"';
                            }
                            break;
                    } //end switch -- field.type -> for title tag

                    sTmp.push('<div class="' + css + '"' + style + attr + title + '>');
                        switch (field.type) {
                            case "rankingField":
                                // get and format the score
                                var formatedScore = "";
                                if (field.scoreNumField) {
                                    // get the section holding the score
                                    var d = data;
                                    if ((field.scoreNumSection != undefined) && (field.scoreNumSection != "")) {
                                        d = data[field.scoreNumSection];
                                    }

                                    formatedScore = scoreFormat(d[field.scoreNumField]);
                                }

                                // see if in the definition of the field
                                var style = "";
                                if ((field.scoreBackground) && (formatedScore != -1)) {
                                    style = ' style="background:' + field.scoreBackground + ';"';
                                }

                                // skin color takes precedence
                                if ((currentSkin.colors) && currentSkin.colors['scoreHighlightColor']) {
                                    // look for a background color rule
                                    colorRules = currentSkin.colors['scoreHighlightColor'];
                                    for (var cr = 0; cr < colorRules.length; cr++) {
                                        if (colorRules[cr].colorType == "background") {
                                            style = ' style="background:' + colorRules[cr].colorVal + ';"';
                                        }
                                    }
                                }

                                // see of the score color is via css
                                var css2 = "";
                                if (field.scoreColorField) {
                                    // get the section holding the score
                                    var d = data;
                                    if ((field.scoreColorSection != undefined) && (field.scoreColorSection != "")) {
                                        d = data[field.scoreColorSection];
                                    }

                                    css2 = ' ' + scoreColorClass(d[field.scoreColorField], formatedScore);
                                }

                                sTmp.push('<div class="rankingBox' + css2 + '"' + style + '>');
                                    sTmp.push('<div class="text">' + formatedScore + '</div>');
                                sTmp.push('</div>');

                                break;
                            case "name":
                            case "text":
                                var name = "";
                                if (Array.isArray(field.dataField)) {
                                    var tmp = [];
                                    for (var ii = 0; ii < field.dataField.length; ii++) {
                                        tmp.push(d[field.dataField[ii]]);
                                    }
                                    name = tmp.join(" ");
                                }
                                else {
                                    name = d[field.dataField];
                                }
                                if (debugMode) {
                                  if (data.key_signals['expansion_hit']) {
                                    name += '&nbsp;|&nbsp;N';
                                  }
                                  if ('gender_boost_multiplier' in data.key_signals) {
                                    name += '&nbsp;|&nbsp;G';
                                  }
                                  if ('ethnicity_boost_multiplier' in data.key_signals) {
                                    name += '&nbsp;|&nbsp;E';
                                  }
                                }

                                if (field.dataDivClass) {
                                    sTmp.push('<div class="' + field.dataDivClass + '">' + name + '</div>')
                                }
                                else {
                                    sTmp.push(name)
                                }
                                break;
                            case "givingIcon":
                                sTmp.push(formatGivingIcon(d, field.extraData.candidateImage));
                                break;
                            case "dollar":
                                var amt = d[field.dataField];
                                if ($.isNumeric(amt)) {
                                    amt = "$" + revupUtils.commify(amt);
                                }
                                sTmp.push(amt);
                                break;
                            case "propectSwitch":
                                var bInvited = d[field.dataField];
                                if (bInvited) {
                                    sTmp.push('<div class="invited">');
                                        sTmp.push('<div class="icon icon-check"></div>');
                                    sTmp.push('</div>');
                                }
                                else {
                                    sTmp.push('<div class="addToBtn">');
                                        sTmp.push('<div class="icon icon-plus"></div>');
                                    sTmp.push('</div>');
                                }
                                break;
                            case "callTimeSwitch":
                                if (field.dataSection && typeof d != 'object') {
                                    sTmp.push('<div class="invited">');
                                        sTmp.push('<div class="icon icon-check"></div>');
                                    sTmp.push('</div>');
                                }
                                else {
                                    sTmp.push('<div class="addToBtn">');
                                        sTmp.push('<div class="icon icon-plus"></div>');
                                    sTmp.push('</div>');
                                }
                                break;
                            case "tickMarks":
                                var spectrum = d[field.spectrum];
                                var numTicks = d[field.numTicks];
                                if (!numTicks){
                                    numTicks = 0;
                                }
                                var color = scoreColor(spectrum);

                                sTmp.push('<div class="ticks" style="color:' + color +';">');
                                    sTmp.push('<div class="icon icon-tick-' + numTicks + '"></div>');
                                sTmp.push('</div>');
                                break;
                            default:
                                console.error("Display Field \"" + field.type + "\" not defined");
                                break;
                        }
                        //sTmp.push('<div class="' + viewDefinition.headerLabelClass + '">' + field.text + '</div>');
                    sTmp.push('</div>')

                    // see if there is a slider
                    if (field.sliderAfter) {
                        let bShowSlider = true;
                        if (field.sliderPermissions) {
                            bShowSlider = false;
                            for (let i = 0; i < field.sliderPermissions.length; i++) {
                                if ((joinPermission && field.sliderPermissions[i] == joinPermission) ||
                                 (permission && permission[field.sliderPermissions[i]])) {
                                    bShowSlider = true;
                                    break;
                                 }
                            }

                        }
                        if (bShowSlider)
                            sTmp.push(buildSliderBar(true));
                    }

                }
            sTmp.push('</div>')

            return sTmp.join('');
        }, // buildListEntry
    }
}(jQuery, window));
