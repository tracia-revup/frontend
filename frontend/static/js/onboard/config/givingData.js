var givingData = function ($configWizard, templateData) {
    let userName = '';
    let ptemplateData = templateData;
    let $validateCSVBtn = $();

    return {
        pageName: function() {
            //required
            var $givingDataAddBtn = $('.wizardBtnDiv .rightSide .nextBtn');
            $givingDataAddBtn.addClass('givingHistoryAddBtn');
            return 'givingDataPage';
        },

        display: function($wizardBody) {
            // required
            let sTmp = [];
            sTmp.push('<h1>Historical Giving Data</h1>');
                sTmp.push('<div class="givingDataDiv entry">');

                    sTmp.push('<div class="givingDataDes">');
                        sTmp.push('Do you have a file containing past contribution data for ' + templateData.title + '</b>');

                        sTmp.push(' that you would like RevUp to utilize during analysis?');
                    sTmp.push('</div>');

                    sTmp.push('<div class="givingDataDetails">');
                        sTmp.push('We gladly accept files in a CSV format only, and have a specific data layout that needs to </b>');
                        sTmp.push('be followed. Please see our <button class="formatInstruct">format instructions</button> and download a ');
                        sTmp.push('<button class="sampleCSV">sample file</button> for help. We highly recommend that you utiltize our CSV validation tool to insure maximum compatability with our software. </b>');

                    sTmp.push('</div>');
                sTmp.push('</div>');

            sTmp.push('</div>');


            // no arguments - just return the html
            // can add the following arguments to the class
            //      htmlClass: extra class (optional blank)
            //      disableNext: true/false (optional false)

            return {html: sTmp.join(''), disableNext: false, htmlClass: 'givingData'};
        }, // display

        getData: function($wizardBody) {

            $('.wizard', $configWizard).revupWizard('displayMiddleBtn').revupWizard('enableMiddle', true).revupWizard('changeBtnLabel', 'middleBtn', 'Upload');;

            $('.wizardBtn.middleBtn').off('click').on('click', function (e) {
                csvValidator.load(false, templateData);
            });


            $('.sampleCSV', $wizardBody).on('click', function(e) {
                var w = window.open(templateData.csvSample,"_blank");
                w.focus();
            });

            $('.formatInstruct', $wizardBody).on('click', function(e) {
                 window.open(templateData.csvHelpUrl, "_blank");
            })

        }, // getData

/*
        getData: function($wizardBody) {
            // optional
            // once the page is displayed go fetch data for the page (optional, default - don't do anything)
            // Return Formats
            //  1) true/false enable prev/next buttons
            //  2) object with the following fields
            //      a) showWaitSpinner - show the wait spinner, the prev/next buttons will be disabled
        }, // getData

        footerMsg: function() {
            // optional
            // return the markup to display in the message area in the button area, left side  (optional, default - '')
        }, // footerMsg

        okToFinish: function($wizardBody) {
            // when the finish button is press, on last page, called for each page to see if ok to finish

            // see if the next button should be enabled or not (optional, default - true)
        }, // okToFinish

        okToMove: function($wizardBody) {
            // see if ok to move to the next page, varify data on page (option, default - true)


            // Return Formats
            //  1) true/false about advance to the new pages
            //  2) object with the following fields
            //      a) okToGo - true/false go to the next page (must have)
            //      b) historyShowError - should the history dot be done; green or blue or red in error (optonal - ok dot)
        }, // okToMove

        save: function($wizardBody) {
            // save - time for the page to save the data on the page (optional - default do nothing)

            // Return Formats
            //  1) true/false save done and can go to the next page (defult - true going to next page)
            //  2) object with the following fields
            //      a) updateAndLoad - true/false if can go to the next page (optional - if missing of to go to next page)
            //      b) showWaitSpinner - tre/false should the wait spinner be displayed, if true the next and prev buttons are disabled (default, do not display wait spinner)
        }, // save
*/
    }
}; // givingData
