var campaignInfo = function ($configWizard, templateData, questionSet) {
    // results
    let qsBioResults = {};
    let qsCampaignInfoResults = {};

    // question set this page is interested in
    let qsBio = {};
    let qsCampaignInfo = {};

    // load once stuff in the dom
    let iUtils;

    // find the question sets by looking about the icon
    let numFound = 0
    for (let i = 0; i < questionSet.length; i++) {
        if (questionSet[i] && questionSet[i].icon == 'bio') {
            qsBio = questionSet[i];

            numFound++;
        }

        if (questionSet[i] && questionSet[i].icon == 'campaign-info') {
            qsCampaignInfo = questionSet[i];

            numFound++;
        }

        if (numFound == 2) {
            break;
        }
    }

    // build list of bio choice fields
    let bioChoiceFields = [];
    if (!$.isEmptyObject(qsBio)) {
        for (let i = 0; i < qsBio.fields.fields.length; i++) {
            if (qsBio.fields.fields[i].choice_key && qsBio.fields.fields[i].choice_key) {
                bioChoiceFields.push(qsBio.fields.fields[i])
            }
        }
    }

    // build list of choice fields
    let campaignChoiceFields = [];
    if (!$.isEmptyObject(qsCampaignInfo)) {
        for (let i = 0; i < qsCampaignInfo.fields.fields.length; i++) {
            if (qsCampaignInfo.fields.fields[i].choice_key && qsCampaignInfo.fields.fields[i].choice_key) {
                campaignChoiceFields.push(qsCampaignInfo.fields.fields[i])
            }
        }
    }

    // console.log('personal info question set: ', qsBio);
    // console.log('Campaing Info question set: ', qsCampaignInfo);

    return {
        pageName: function() {
            //required
            return 'campaignInfoPage';
        },

        display: function($wizardBody) {
            // build
            let sTmp = [];
            sTmp.push('<div class="campaignInfoDiv">');
                sTmp.push('<div class="title">Candidate & Campaign Information</div>');
                sTmp.push('<div class="fieldsDiv">');

                    sTmp.push('<div class="line">');
                        sTmp.push('<div class="entry nameEntry" questionSet="bio">');
                            sTmp.push('<div class="title required">Candidate First Name</div>');
                            sTmp.push('<input type="text" class="candFirsttName" placeholder="Candidate First Name"  value=""/>');
                        sTmp.push('</div>')
                        sTmp.push('<div class="entryGap"></div>');
                        sTmp.push('<div class="entry nameEntry" questionSet="bio">');
                            sTmp.push('<div class="title required">Candidate Last Name</div>');
                            sTmp.push('<input type="text" class="candLastName" placeholder="Candidate Last Name"  value=""/>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="line">');
                        sTmp.push('<div class="entry entry50Percent" questionSet="bio">');
                            sTmp.push('<div class="title required">Gender</div>');
                            sTmp.push('<div class="genderDropDown dropDown"></div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entryGap"></div>');
                        sTmp.push('<div class="entry entry50Percent" questionSet="bio">');
                            sTmp.push('<div class="title required">Ethnicity</div>');
                            sTmp.push('<input type="text" class="candLastName" placeholder="Ethnicity"  value=""/>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="line">')
                        sTmp.push('<div class="entry entry20Percent" questionSet="bio">');
                            sTmp.push('<div class="title required">Birthdate</div>');
                            sTmp.push('<div class="birthDate dateFieldDiv">');
                                sTmp.push('<input type="text" class="dateInput" placeholder="mm/dd/yyyy">');
                                sTmp.push('<div class="dateIcon icon icon-calendar"></div>');
                            sTmp.push('</div>');
                        sTmp.push('</div>')
                        sTmp.push('<div class="entryGap"></div>');
                        sTmp.push('<div class="entry entry40Percent" questionSet="bio">');
                            sTmp.push('<div class="title">Birth City</div>');
                            sTmp.push('<input type="text" class="birthCity" placeholder="Birth City"  value=""/>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entryGap"></div>');
                        sTmp.push('<div class="entry entry40Percent" questionSet="bio">');
                            sTmp.push('<div class="title">Birth State</div>');
                            sTmp.push('<div class="birthStateDropDown dropDown"></div>');
                        sTmp.push('</div>')
                    sTmp.push('</div>');

                    sTmp.push('<div class="line">');
                        sTmp.push('<div class="entry entry40Percent" questionSet="campaign-info">');
                            sTmp.push('<div class="title required">Political Party</div>');
                            sTmp.push('<div class="politicalPartyDropDown dropDown"></div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entryGap"></div>');
                        sTmp.push('<div class="entry entry20Percent" questionSet="campaign-info">');
                            sTmp.push('<div class="title required">Election Date</div>');
                            sTmp.push('<div class="electionDate dateFieldDiv">');
                                sTmp.push('<input type="text" class="dateInput" placeholder="mm/dd/yyyy">');
                                sTmp.push('<div class="dateIcon icon icon-calendar"></div>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entryGap"></div>');
                        sTmp.push('<div class="entry entry40Percent" questionSet="campaign-info">');
                            sTmp.push('<div class="title required">Type of Office</div>');
                            sTmp.push('<div class="typeOfOfficeDropDown dropDown"></div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="line">');
                        sTmp.push('<div class="entry entry40Percent" questionSet="campaign-info">');
                            sTmp.push('<div class="title">Is the Incumbent</div>');
                            sTmp.push('<div class="isIncumbent booleanDiv"></div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entryGap"></div>');
                        sTmp.push('<div class="entry entry20Percent" questionSet="campaign-info">');
                            sTmp.push('<div class="title required">Election Cycle Start</div>');
                            sTmp.push('<div class="electionCycleDatartDate dateFieldDiv">');
                                sTmp.push('<input type="text" class="dateInput" placeholder="mm/dd/yyyy">');
                                sTmp.push('<div class="dateIcon icon icon-calendar"></div>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entryGap"></div>');
                        sTmp.push('<div class="entry entry40Percent" questionSet="campaign-info">');
                            sTmp.push('<div class="title required">Office Sought</div>');
                            sTmp.push('<div class="officeSoughtDropDown dropDown"></div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="line doubleHigh">');
                        sTmp.push('<div class="entry entry40Percent" questionSet="campaign-info">');
                            sTmp.push('<div class="title">District Number</div>');
                            sTmp.push('<input type="text" class="districtNum" placeholder="District Number"  value=""/>');
                            sTmp.push('<div class="title requiredBefore">Required Fields</div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entryGap"></div>');
                        sTmp.push('<div class="entry entry20Percent" questionSet="campaign-info">');
                            sTmp.push('<div class="title  required">State</div>');
                            sTmp.push('<div class="stateDropDown dropDown"></div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entryGap"></div>');
                        sTmp.push('<div class="entry entry40Percent" questionSet="campaign-info">');
                            sTmp.push('<div class="title">Contribution Limit</div>');
                            sTmp.push('<div class="contributionLimitDropDown dropDown"></div>');
                            sTmp.push('<input type="text" class="contributionAmt amount" placeholder="$ - Contribution Amount"  value=""/>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            $('.wizard', $configWizard).revupWizard('changeBtnLabel', 'next', "Next");

            // no arguments - just return the html
            // can add the following arguments to the class
            //      htmlClass: extra class (optional blank)
            //      disableNext: true/false (optional false)
            return {html: sTmp.join(''), disableNext: true};
        }, // display

        getData: function($wizardBody) {
            iUtils = infoUtils($configWizard);

            // the change flags
            iUtils.clearBioChangeInfo();
            iUtils.clearCampaignInfo();

            $('.entry[questionSet="bio"]', $wizardBody).each(function() {
                let $entry = $(this);

                // find the fields and set the attributes
                let label = $('.title', $entry).html();
                label = label.replace('*', '');
                let bFound = false;
                if (!$.isEmptyObject(qsBio)) {
                    for (let i = 0; i < qsBio.fields.fields.length; i++) {
                        let qsField = qsBio.fields.fields[i];
                        if (qsField.label == label) {
                            $entry.attr('fieldDisplayType', qsField.type)
                                  .attr('fieldName', qsField.expected[0].field)
                                  .attr('fieldType', qsField.expected[0].type);

                            iUtils.initField($entry, qsField, bioChoiceFields);

                            bFound = true;
                            break;
                        }
                    }
                }

                if (!bFound) {
                    console.error('Unable to find question set info for: ', label);
                }
            })

            $('.entry[questionSet="campaign-info"]', $wizardBody).each(function() {
                let $entry = $(this);

                // find the fields and set the attributes
                let label = $('.title', $entry).html();
                if (label == '' || label == undefined) {
                    return;
                }
                label = label.replace('*', '');
                let bFound = false;
                if (!$.isEmptyObject(qsCampaignInfo)) {
                    for (let i = 0; i < qsCampaignInfo.fields.fields.length; i++) {
                        let qsField = qsCampaignInfo.fields.fields[i];
                        if (qsField.label == label) {
                            $entry.attr('fieldDisplayType', qsField.type)
                                  .attr('fieldName', qsField.expected[0].field)
                                  .attr('fieldType', qsField.expected[0].type);

                            iUtils.initField($entry, qsField, campaignChoiceFields);

                            bFound = true;
                            break;
                        }
                    }
                }

                if (!bFound) {
                    console.error('Unable to find question set info for: ', label);
                }
            })

            // get the initial values
            $.when($.get(qsBio.readUrl), $.get(qsCampaignInfo.readUrl))
                .done(function(qsBioResultsRaw, qsCampaignInfoResultsRaw) {
                    // load the initial values
                    if (qsBioResultsRaw[0].results && qsBioResultsRaw[0].results.length > 0) {
                        qsBioResults = qsBioResultsRaw[0].results[0];
                        iUtils.loadInitValue(qsBioResults.data, 'bio', bioChoiceFields);
                    }

                    if (qsCampaignInfoResultsRaw[0].results && qsCampaignInfoResultsRaw[0].results.length > 0) {
                        qsCampaignInfoResults = qsCampaignInfoResultsRaw[0].results[0];
                        iUtils.loadInitValue(qsCampaignInfoResults.data, 'campaign-info', campaignChoiceFields);
                    }
                })
                .fail(function(rResult) {
                    console.error('Unable to retreive campaign or candidate information: ', arguments);

                    $('body').revupMessageBox({
                        headerText:         'Retrieve Information',
                        msg:                'Unable to retrieve candidate and campaign information',
                        zIndex:             1000,
                    })
                })
                .always(function(qsBioResultsRaw, qsCampaignInfoResultsRaw) {
                    $configWizard.revupWizard('finishGetData');

                    // set focus
                    $('.campaignInfoDiv .candFirsttName', $wizardBody).select();
                })

            return {showWaitSpinner: true}
        }, // getData

/*
        footerMsg: function() {
            // optional
            // return the markup to display in the message area in the button area, left side  (optional, default - '')
        }, // footerMsg

        okToFinish: function($wizardBody) {
            console.log('okToFinish')
            // when the finish button is press, on last page, called for each page to see if ok to finish

            // see if the next button should be enabled or not (optional, default - true)
        }, // okToFinish

/*
        okToMove: function($wizardBody) {
        }, // okToMove
*/

        save: function($wizardBody) {
            // get the bio question sets data
            let bioDataObj = {};
            $('.entry[questionSet="bio"]', $wizardBody).each(function() {
                let $entry = $(this);

                // get the values
                iUtils.getValue($entry, bioDataObj)
            });

            // get the campaign info question sets data
            let campaignInfoObj = {};
            $('.entry[questionSet="campaign-info"]', $wizardBody).each(function() {
                let $entry = $(this);

                // get the values
                iUtils.getValue($entry, campaignInfoObj)
            })

            // to post/put the updates
            let bioUpdateCreate = () => {
                let deferred = $.Deferred();

                let url = "";
                let qsId = -1;
                let ajaxType = "POST";
                if (!$.isEmptyObject(qsBioResults.data)) {
                    qsId = qsBioResults.id;
                    url = qsBio.updateUrl.replace("questionSetDataId", qsId);
                    ajaxType = "PUT";
                }
                else {
                    url = qsBio.createUrl.replace("/questionSetDataId", "");
                    ajaxType = "POST";
                }

                $.ajax({
                    type: ajaxType,
                    url: url,
                    data: JSON.stringify(bioDataObj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                })
                .done (function(r) {
                    deferred.resolve(r)
                })
                .fail (function(r) {
                    deferred.reject(r)
                })

                return deferred.promise();
            }; // bioUpdateCreate

            let campaignInfoUpdateCreate = () => {
                let deferred = $.Deferred();

                let url = "";
                let qsId = -1;
                let ajaxType = "POST";
                if (!$.isEmptyObject(qsCampaignInfoResults.data)) {
                    qsId = qsCampaignInfoResults.id;
                    url = qsCampaignInfo.updateUrl.replace("questionSetDataId", qsId);
                    ajaxType = "PUT";
                }
                else {
                    url = qsCampaignInfo.createUrl.replace("/questionSetDataId", "");
                    ajaxType = "POST";
                }

                $.ajax({
                    type: ajaxType,
                    url: url,
                    data: JSON.stringify(campaignInfoObj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                })
                .done (function(r) {
                    deferred.resolve(r)
                })
                .fail (function(r) {
                    deferred.reject(r)
                })

                return deferred.promise();
            }; // campaignInfoUpdateCreate

            // do the update
            let updateCreateFuns = [];
            if (iUtils.hasBioInfoChanged()) {
                updateCreateFuns.push(bioUpdateCreate())
            }

            if (iUtils.hasCampaignInfoChanged()) {
                updateCreateFuns.push(campaignInfoUpdateCreate())
            }

            if (updateCreateFuns.length == 0) {
                return
            }

            setTimeout(function() {
                $.when(updateCreateFuns)
                    .done (function(updateCreate1, updateCreate2) {
                        $('.wizard', $configWizard).revupWizard('finishSave');
                    })
                    .fail(function(updateCreate1, updateCreate2) {
                        console.group('Unable to save change to campaign or candidate information->');
                        console.error('create object 1: ', updateCreate1);
                        console.error('create object 2: ', updateCreate2);
                        console.groupEnd();

                        $('body').revupMessageBox({
                            headerText:         'Save Information',
                            msg:                'Unable to save/create candidate and campaign information',
                            zIndex:             1000,
                        })
                    })
                }, 10);

            return {updateAndLoad: false, showWaitSpinner: true}
        }, // save
    }
}; // campaignInfo
