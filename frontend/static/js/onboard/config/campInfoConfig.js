var campInfoConfig = (function () {
    // Globals
    var $detailsData = $();
    var currentPage = "";
    var fieldsChanged;
    var $configWizardDiv = $();
    var onboardTemplateData;

    let  displayFields = (questionSetLst, qIcon, $detailsData) =>
    {
        if (!qIcon || !$detailsData) {
            loadDetailsUnknown('No Configuration Items Found');
            return;
        }

        for (var k in questionSetLst) {
            if(questionSetLst[k]['icon'] == qIcon) {
                var $entry = $detailsData;

                let dataAttr = questionSetLst[k];
                $entry.data('fields', dataAttr.fields);
                $entry.data('readUrl', dataAttr.readUrl);
                $entry.data('readDetailUrl', dataAttr.readDetailsUrl);
                $entry.data('createUrl', dataAttr.createUrl);
                $entry.data('updateUrl', dataAttr.updateUrl);
                $entry.data('deleteUrl', dataAttr.deleteUrl);
                $entry.data('allowMultiple', (dataAttr.allowMultiple.toLowerCase() == 'True'));
                $entry.data('icon', dataAttr.icon);

                let eData = $entry.data();

                if ((!eData) || ($.isEmptyObject(eData))) {
                    loadDetailsUnknown('No Configuration Items Found');
                    return;
                }
                else {
                    loadDetails($entry);
                }
            }

        }
    }

    let loadDetailsUnknown = (msg) =>
    {
        var sTmp = [];

        sTmp.push('<div class="detailsErrorSection">');
            sTmp.push('<div class="errorMsg">' + msg + '</div>');
        sTmp.push('</div>');
        $detailsData.html(sTmp.join(''));

        // $detailsLoading.hide();
        $detailsData.show();
    }; // loadDetailsUnknown

    let  loadDetails = ($entry) =>
    {

        // clear the data

        // // initialize active fields list
        activeFields2 = [];

        // walk the fields and build the details section
        var eData = $entry.data();
        if ((eData == undefined) || ($.isEmptyObject(eData))) {
            loadDetailsUnknown('No Configuration Items Found');
            return;
        }

        // add the title
        if (eData.fields.title == 'Education' || eData.fields.title == 'Issues') {
            $detailsData.html('<div class="title"></div>');
        }
        else if (eData.fields.title !== 'Key Contributions') {
            $entry.html('<div class="title">' + eData.fields.title + '</div>');
        }


        // most types require save button and display
        var bDisplayFieldsAndSave = true;
        var bLoadData = true;

        var f = (eData.fields).fields;
        var choiceKeyLst = [];
        var bLoadCurrentValue = true;
        saveFunction = undefined;

        for (var k in f) {
            switch(f[k].type) {
                case "list":
                    loadDetailsList(eData, f[k]);
                    break;
                case "entity_search":
                    entitySearch.display(eData, f[k], $configWizardDiv, onboardTemplateData);
                    bDisplayFieldsAndSave = false;
                    bLoadData = false;
                    break;
                default:
                    //loadDetailsUnknown("Unknown Analysis Config type \"" + f[i].type + "\"");
                    console.error("Unknown Analysis Config type: ", f[i]);
                    break;
            }
        }

        // update field for choice key
        if (choiceKeyLst.length > 0) {
            for (var i = 0; i < choiceKeyLst.length; i++) {
                var fieldName = activeFields2[choiceKeyLst[i]].choiceKey; //fieldName;
                for (var a = 0; a < activeFields2.length; a++) {
                    if (fieldName == activeFields2[a].fieldName) {
                        activeFields2[a].changeFieldIndex = choiceKeyLst[i];
                    }
                }
            }
        }

        // see if there are any required fields
        var bDisplayRequiredMsg = false;
        for (var a = 0; a < activeFields2.length; a++) {
            if (activeFields2[a].bRequired) {
                bDisplayRequiredMsg = true;
            }
        }

        // display the save button and look the stored value
        if (bDisplayFieldsAndSave) {
            // add the save button
            var sTmp = [];
            sTmp.push('<div class="btnDiv">');
                if (bDisplayRequiredMsg) {
                    sTmp.push('<div class="leftSide">');
                        sTmp.push('<div class="text requiredMsg">* Required Fields</div>');
                    sTmp.push('</div>');
                }

            sTmp.push('</div>');

            var $entitySearch = $detailsData.append(sTmp.join(''));

            // add controllers and event handlers
            initDetails(eData);

            // set the default value
            if (choiceKeyLst.length > 0) {
                for (var i = 0; i < activeFields2.length; i++) {
                    if (activeFields2[i].choiceKey != undefined) {
                        var choiceKey = activeFields2[i].choiceKey;
                        for (var j = 0; j < activeFields2.length; j++) {
                            if (activeFields2[j].fieldName == choiceKey) {
                                var choiceKeyValue = activeFields2[j].$dropdown.revupDropDownField('getValue');
                                if (activeFields2[i].lstValues[choiceKeyValue] != undefined) {
                                    activeFields2[i].$dropdown.revupDropDownField({
                                        value: activeFields2[i].lstValues[choiceKeyValue],
                                        valueStartIndex: 0,
                                        updateValueList: true,
                                        overlayZIndex: 51,
                                    })
                                }
                            }
                        }
                    }
                }
            }

            // load the current values
            if (bLoadCurrentValue) {
                loadCurrentValues(eData);
            }
        }
    }

    let enableDisableList = () =>
    {
        // if the field is not required then return true
        if (!activeField.bRequired) {
            return true;
        }

        // see if any of the fields have a value
        var valFields = 0;
        $('textField', activeField.$listField).each(function() {
            var v = $(this).val();
            if ($trim(v) != "") {
                valFields += 1;
            }
        })

        return valFields > 0;
    } // enableDisableList

    let checkEmptyFields = function() {
        let totalLength = 0;
        if ($('.textField').length == 0) {
            $configWizardDiv.revupWizard('enableNext', false);
        }
        else {
            $('.textField').each(function() {
               let $element = $(this);
               totalLength += ($element.val()).length;
               if (totalLength == 0) {
                   $configWizardDiv.revupWizard('enableNext', false);
               }
               else {
                   $configWizardDiv.revupWizard('enableNext', true);
                   return;
                }
            });
        }
    }

    let initDetails = (eData) =>
    {
        for (var i = 0; i < activeFields2.length; i++) {
            if (activeFields2[i].fnInitField) {
                activeFields2[i].fnInitField(activeFields2[i]);
            }
            else {
                console.error('addDetinitDetailsailsControllersAndHandler - undefined init method: ' + activeFields2[i].fieldDisplayType);
            }
        }
    }; // initDetails

    let loadCurrentValues = (eData) =>
    {
        $.get(eData.readUrl)
            .done(function(r) {
                $detailsData.hide();

                if (r.count == 0) {
                    bUpdateConfig = false;
                }
                else {
                    bUpdateConfig = true

                    // save the question set id and number
                    var cData = r.results[0].data;
                    $detailsData.attr("questionSet", r.results[0].question_set);
                    $detailsData.attr("questionSetId", r.results[0].id);

                    // get the data to load
                    for(var key in cData) {
                        var bFound = false;
                        for (var i = 0; i < activeFields2.length; i++) {
                            if (key == activeFields2[i].fieldName) {
                                if (cData[key].length > 0) {
                                    var val = cData[key];

                                    // load the value
                                    if (activeFields2[i].fnLoadValue) {
                                        activeFields2[i].fnLoadValue(activeFields2[i], val);
                                    }
                                    else {
                                        console.error('loadCurrentValues - undefined load method: ' + activeFields2[i].fieldDisplayType);
                                    }
                                    bFound = true;
                                    break;
                                }
                            }
                        }

                        if (!bFound) {
                            // console.error('Loading Data - field not found: ' + key);
                        }
                    }


                    // disable the save button until something changes
                    $('.saveBtn', $detailsData).addClass('disabled');
                    // enableDisableSaveBtn();
                    // $detailsLoading.hide();

                }
                    checkEmptyFields();
                    $detailsData.show();
            })
            .fail(function(r) {
                bUpdateConfig = false;

                revupUtils.apiError('Analysis Configuration', r, 'Unable to load analysis config', "Unable to load analysis configuration at this time.");
            })
            .always(function (r) {
                $configWizardDiv.revupWizard('finishGetData');

                // set focus
                $('.campaignInfoDiv .candFirstName').select()
            })
    }; // loadCurrentValues

    let saveDetails = (eData, doneFunc) =>
    {

        $configWizardDiv.revupWizard('enableNext', false);

        // build the json to save/update
        var jObj = {};

        // walk the fields and see what needs to be done
        var questionSetId = '';
        if (saveFunction) {
            questionSetId = saveFunction(eData, jObj);

            // see if this sould be a delete, ranges is empty
            if (jObj.ranges && jObj.ranges.length == 0) {
                // if there is a id tile to delete
                if (jObj.id) {
                    $.ajax({
                        url: eData.deleteUrl.replace("questionSetDataId", jObj.id),
                        type: "DELETE"
                    })
                    .done(function(r) {
                        var title = "Deleted";
                        var msg = "Analysis Configuration Deleted";
                        $('body').revupMessageBox({
                            headerText: title,
                            msg: msg
                        });
                    })
                    .fail(function(r) {
                        $entry.revupMessageBox({
                            headerText: "Error - Unable to delete analysis config",
                            msg: "Unable to delete analysis config \"" + title + "\".  Please try again later",
                        })
                    })

                }

                // set the flag to update
                bUpdateConfig = true;

                // disable the save button
                $('.saveBtn', $detailsData).addClass('disabled');
            }
        }
        else {
            questionSetId = $detailsData.attr("questionSetId");
            for (var i = 0; i < activeFields2.length; i++) {
                if (activeFields2[i].fnGetValue) {
                    activeFields2[i].fnGetValue(activeFields2[i], jObj)
                }
                else {
                    console.error('saveDetails - undefined save method: ' + activeFields2[i].fieldDisplayType);
                }
            }
        }

        // disable the save button
        $('.saveBtn', $detailsData).addClass('disabled');

        // build the url
        var url = "";
        var ajaxType = "POST";
        if (bUpdateConfig) {
            url = eData.updateUrl.replace("questionSetDataId", questionSetId);
            ajaxType = "PUT";
        }
        else {
            url = eData.createUrl.replace("/questionSetDataId", "");
            ajaxType = "POST";
        }
        $.ajax({
            type: ajaxType,
            url: url,
            data: JSON.stringify(jObj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        })
        .done(function(r) {
            var title = "Saved";
            var msg = "Analysis Configuration Saved";
            if (bUpdateConfig) {
                title = "Updated";
                msg = "Analysis Configuration Updated"
            }

            // set the flag to update
            bUpdateConfig = true;

            // save the updated id
            if (r.id) {
                $detailsData.attr("questionSetId", r.id);
            }


            // see if there is a function to call after done
            if (doneFunc != undefined) {
                doneFunc();
            }

            $('.wizard', $configWizardDiv).revupWizard('finishSave');
        })
        .fail(function(r) {
            revupUtils.apiError('Analysis Configuration', r, 'Unable to save analysis config', "Unable to save analysis configuration at this time.");
        })
        .always(function(r) {
            fieldsChanged = false;
            // hide the wait overlay
            // $saveUpadateWait.hide();
        })
    } // saveDetails


    let loadDetailsList = (eData, fieldData) =>
    {
        // create the active field for this field
        var fObj = new Object();
        fObj.fieldType = fieldData.expected[0].type;
        fObj.fieldName = fieldData.expected[0].field;
        fObj.fieldDisplayType = fieldData.type;
        fObj.bRequired = true;
        if (fieldData.required != undefined) {
            fObj.bRequired = fieldData.required;
        }

        fObj.fnInitField = initFieldList;
        fObj.fnEnableDisableSaveHandler = enableDisableList;
        fObj.fnLoadValue = loadValueList;
        fObj.fnGetValue = getValueList;

        // custom fields
        fObj.label = fieldData.label;
        activeFields2.push(fObj);

        // get data
        var sTmp = [];
        sTmp.push('<div class="fieldEntry listField">');
            sTmp.push('<div class="centerDiv">');
                sTmp.push('<div class="fieldDiv listFieldDiv" fieldType="' + fieldData.expected[0].type + '" fieldName="' + fieldData.expected[0].field + '"  fieldDisplayType="' + fieldData.type + '">');
                    sTmp.push('<div class="listField">');
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        // add to the display
        $('.title', $detailsData).append(sTmp.join(''));

        fObj.$cntl = $('.fieldDiv[fieldName="' + fieldData.expected[0].field + '"]');
    } // loadDetailsList

    let initFieldList = (activeField) =>
    {
        activeField.$listField = $('.listField', activeField.$cntl);
        var labelClass = activeField.required ? 'required' : '';
        if (currentPage == 'education') {
            var currentPagePlaceholder = 'School Name';
        }

        if (currentPage == 'issues') {
            var currentPagePlaceholder = 'Issue';
        }
        activeField.$listField.revupDynLst({label: activeField.label,
                                        //   bScrollEntries: true,
                                          //entryLstMinHeight: "50px",
                                          labelClass: labelClass,
                                          inputName: 'activeField.fieldName' + 'DynLst',
                                          inputMaxLen: 48,
                                          focusColor: revupConstants.color.primaryGray4,
                                          inputPlaceHolder: currentPagePlaceholder,
                                          addBtnBelowlabel: 'Add',
                                          addBtnLabel: '',
                                          deleteBtnExtraClass: 'onboarding',
                                        });

        // the event to look for
        activeField.$cntl.on('revup.dynamicListAdd revup.dynamicListDelete revup.dynamicListEntryChange', function(e) {
            fieldsChanged = true;
            checkEmptyFields();
        });
    } // initFieldList


    let getValueList = (activeField, jObj) =>
    {
        var val = activeField.$listField.revupDynLst('getList');
        jObj[activeField.fieldName] = val;
    } // getValueList

    let loadValueList = (activeField, val) =>
    {
        activeField.$listField.revupDynLst('setList', val);
    } // loadValueList

    return {
        load: function(questionSetLst, qIcon, $qIDentry, $configWizard, templateData){

            if (templateData) {
                onboardTemplateData = templateData;
            }

            fieldsChanged = false;
            $detailsData = $qIDentry;
            currentPage = qIcon;
            $configWizardDiv = $configWizard;

            $configWizardDiv.revupWizard('enableNext', false);
            displayFields(questionSetLst, qIcon, $detailsData);
        },
        saving: function($qIDentry){
            $detailsData = $qIDentry;
            saveDetails($detailsData.data());
        },
    };

} ());
