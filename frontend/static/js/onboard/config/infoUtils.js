var infoUtils = function($configWizard) {
    let $wizardBody = $('.wizardBody', $configWizard);

    // change flages
    let bBioChanged = false;
    let bCampaignInfoChanged = false;
    let bCommitteeInfoChanged = false;
    let bTrackerChanged = false;

    let enableDisableNext = (whichQuestionSet) => {
        if (whichQuestionSet == 'bio') {
            bBioChanged = true;
        }
        else if (whichQuestionSet == 'tracker') {
            bTrackerChanged = true;
        }
        else if (whichQuestionSet == 'campaign-info') {
            bCampaignInfoChanged = true;
        }
        else if (whichQuestionSet == 'committee-info') {
            bCommitteeInfoChanged = true;
        }

        bEnable = true;
        $('.entry .title.required', $wizardBody).each(function() {
            let $entry = $(this).parent();
            let fieldDisplayType = $entry.attr('fieldDisplayType');
            // let $valField = $(':nth-child(2)', $entry);

            if (fieldDisplayType == 'text') {
                let $valField = $('input', $entry);
                let val = $valField.val();
                if ($entry.attr('fieldname') == 'zipcode') {
                    if (val.length < 5 ) {
                        bEnable = false;

                        // stop checking
                        return false;
                    }
                }
                else {
                    if (val == '') {
                        bEnable = false;

                        // stop checking
                        return false;
                    }
                }
            }
            else if (fieldDisplayType == 'dropdown') {
                let $valField = $('.dropDown', $entry)
                let val = $valField.revupDropDownField('getValue');
                if (val == '') {
                    bEnable = false;

                    // stop checking
                    return false;
                }
            }
            else if (fieldDisplayType == 'date') {
                let $valField = $('.dateInput', $entry);
                let date = $valField.val();

                // validate the data
                bDateValid = revupUtils.validDate(date)
                if (!bDateValid) {
                    bEnable = false;

                    // stop checking
                    return false;
                }
            }
            else if (fieldDisplayType == 'boolean') {
                let $valField = $('.booleanDiv', $entry);
                let bState = $valField.attr('btnChecked')
                if (bState != 'yes') {
                    bEnable = false;

                    // stop checking
                    return false;
                }
            }
            else if (fieldDisplayType == 'unlimited-or-integer') {
                let $valField = $('.dropDown', $entry);
                let val = $valField.revupDropDownField('getValue');
                if (val == '') {
                    bEnable = false;

                    // stop checking
                    return false;
                }
                else if (val == 'Amount') {
                    let amtVal = $('.amount', $entry).val();
                    if (val == '') {
                        bEnable = false;

                        // stop checking
                        return false;
                    }
                }

            }
        })

        // if false not longer need to check
        if (bEnable) {
            $configWizard.revupWizard('enableNext', true);
        }
        else {
            $configWizard.revupWizard('enableNext', false);
        }
    }; // enableDisableNext

    return {
        /*
        // the following flags watch if a question set value has change
        */
        hasBioInfoChanged: function() {
            return bBioChanged;
        }, // hasBioInfoChanged

        clearBioChangeInfo: function() {
            bBioChanged = false;
        }, // clearBioChangeInfo

        hasCampaignInfoChanged: function() {
            return bCampaignInfoChanged;
        }, // hasCampaignInfoChanged

        clearCampaignInfo: function() {
            bCampaignInfoChanged = false;
        }, // clearCampaignInfo

        hasCommitteeInfoChanged: function() {
            return bCommitteeInfoChanged;
        }, // hasCommitteeInfoChanged

        clearCommitteeInfoChanged: function() {
            bCommitteeInfoChanged = false;
        }, // clearCommitteeInfoChanged

        hasTrackerChanged: function() { //
            return bTrackerChanged;
        }, // hasTrackerChanged flags when a value in Key Contribution field has been changed

        clearTrackerChangeInfo: function() {
            bTrackerChanged = false;
        }, // clearTrackerChangeInfo refers to Key Contribution fields changes

        initField: function($entry, qsData, choiceFields) {
            let displayType = $entry.attr('fieldDisplayType');
            if (displayType == 'text') {
                // see if the values changes
                let $input = $(':nth-child(2)', $entry);
                let enableNext = false;
                if (qsData['expected'][0]['field'] == 'zipcode') { //zipcode validations
                    $input
                        .on('keydown', function(e) {
                            // make sure a number
                            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 40)) {

                                return;
                            }

                            else if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                                e.preventDefault();

                                return;
                            }
                        })
                }
                $input.on('input', function(e) {
                    enableDisableNext($entry.attr('questionSet'));
                })
            }
            else if (displayType == 'dropdown') {
                // build the data
                let dataLst = [];
                if (qsData.add_clear) {
                    dataLst.push({value: '', displayValue: ''})
                }
                for (let i = 0; i < qsData.choices.length; i++) {
                    if (typeof qsData.choices[i] == 'string') {
                        dataLst.push({value: qsData.choices[i], displayValue: qsData.choices[i]})
                    }
                    else {
                        let v = qsData.choices[i];
                        dataLst.push({value: qsData.choices[i][0], displayValue: qsData.choices[i][1]})
                    }
                }
                if (dataLst.length == 0) {
                    dataLst.push({value: '', displayValue: ''})
                }

                // other attributes
                let addNewValue = false;
                if (qsData.add_new_value) {
                    addNewValue = true;
                }

                let bSortLst = false;
                if (qsData.sort_choices) {
                    bSortLst = true;
                }

                // see if based on another field
                let changeFunc = null;
                let fieldName = qsData.expected[0].field;
                for(let i = 0; i < choiceFields.length; i++) {
                    if (fieldName == choiceFields[i].choice_key) {
                        changeFunc = function(index, val) {
                            // get the close field value
                            let choiceField = choiceFields[i].expected[0].field;
                            let choiceFieldValues = [];
                            if (val != '') {
                                choiceFieldValues = choiceFields[i].choices[val];
                            }

                            let $dropDown = $('.entry[fieldName="' + choiceField + '"] .dropDown', $wizardBody);

                            $dropDown.revupDropDownField({
                                value: choiceFieldValues,
                                valueStartIndex: 0,
                                updateValueList: true
                            })
                        }
                    }
                }

                let $dropdownDiv = $(':nth-child(2)', $entry);
                $dropdownDiv.revupDropDownField({
                                valueStartIndex: -1,
                                addNewValue: addNewValue,
                                sortList: bSortLst,
                                changeFunc: changeFunc,
                                ifStartIndexNeg1Msg: qsData.label,
                                value: dataLst,
                                })
                            .on('revup.dropDownSelected', function(e) {
                                // see if the next button should be enabled
                                enableDisableNext($entry.attr('questionSet'));
                                })
            }
            else if (displayType == 'date') {
                let $date = $(':nth-child(2)', $entry);
                let $dateInput = $('.dateInput', $date);
                let $dateIcon = $('.dateIcon', $date);
                let firstSlashAdded = false;
                let secondSlashAdded = false;

                $dateInput
                    .datepicker({
                        autoclose: true,
                        format: "mm/dd/yyyy"
                    })
                    .on('keydown', function(e) {
                        // esc - close the date picker
                        if (e.which === 13) {
                            var tabEvent = jQuery.Event("keydown");
                            tabEvent.which = 9;
                            tabEvent.keyCode = 9;
                            $(this).trigger(tabEvent);

                            return false;
                        }

                        // make sure a number
                        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 40)) {
                            return;
                        }
                        else if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105)) {
                            // 0-9 only
                            let val = $(this).val();
                            val = val.replace(/[^\d]/g, '');
                            if (val.length >= 10) {
                                // see if the first 1 and length == 10
                                if (val[0] == '1' && val.length == 10) {
                                    return true;
                                }

                                e.preventDefault();
                                return false;
                            }

                        }
                        else {
                            e.preventDefault();
                            return false;
                        }

                        return true;
                    })
                    .on('input', function(e) {
                        let val = $(this).val();
                        const lastCharIsNum = val.substr(-1) != '/';
                        val = val.replace(/[^\d]/g, '');
                        const len = val.length;

                        if (len > 1 && len <= 3) {
                            if (len == 2 && firstSlashAdded && lastCharIsNum) {
                                const month = val.charAt(0);

                                val = month;
                                firstSlashAdded = false;
                            }
                            else {
                                const month = val.substr(0, 2);
                                const day = val.substr(2, len);

                                val = month + '/' + day;
                                firstSlashAdded = true;
                            }
                        }
                        else if (len >= 4) {
                            if (len == 4 && secondSlashAdded && lastCharIsNum) {
                                const month = val.substr(0, 2);
                                const day = val.charAt(2);

                                val = month + '/' + day;
                                secondSlashAdded = false;
                            }
                            else {
                                const month = val.substr(0, 2);
                                const day = val.substr(2, 2);
                                const l = Math.min(len - 4, 4);
                                const year = val.substr(4, l);

                                val = month + '/' + day + '/' + year;
                                secondSlashAdded = true;
                            }
                        }

                        $(this).val(val);
                        e.preventDefault();

                        // see if the next button should be enabled
                        enableDisableNext($entry.attr('questionSet'));

                        return false;
                    })
                    .on('hide', function(e) {
                        // see if the next button should be enabled
                        enableDisableNext($entry.attr('questionSet'));
                    })

                $dateIcon.on('click', function(e) {
                    $dateInput.datepicker('update', $('.dateInput', $(this).parent()).val())
                    $dateInput.datepicker('show');
                });
            }
            else if (displayType == 'boolean') {
                let $booleanDiv = $(':nth-child(2)', $entry);
                let sTmp = [];
                sTmp.push('<div class="radioBtnDiv yesRadioBtn">');
                    sTmp.push('<div class="radioBtn">');
                        sTmp.push('<div class="icon background icon-radio-button-back"></div>');
                        sTmp.push('<div class="icon unselected icon-radio-button-off"></div>');
                        sTmp.push('<div class="icon selected icon-radio-button-on"></div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="radioLabel">Yes</div>');
                sTmp.push('</div>');
                sTmp.push('<div class="radioBtnDiv noRadioBtn">');
                    sTmp.push('<div class="radioBtn">');
                        sTmp.push('<div class="icon background icon-radio-button-back"></div>');
                        sTmp.push('<div class="icon unselected icon-radio-button-off"></div>');
                        sTmp.push('<div class="icon selected icon-radio-button-on"></div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="radioLabel">No</div>');
                sTmp.push('</div>');
                $booleanDiv.html(sTmp.join(''));

                // radio button click handlers
                $('.radioBtnDiv', $booleanDiv).on('click', function(e) {
                    let $clickedOn = $(this);

                    // unselect all
                    let $containingDiv = $clickedOn.parent();
                    $('.radioBtn .selected', $containingDiv).hide();
                    $('.radioBtn .unselected', $containingDiv).show();
                    $booleanDiv.attr('btnChecked', '');

                    // select the clicked on
                    $('.radioBtn .selected', $clickedOn).show();
                    $('.radioBtn .unselected', $clickedOn).hide();

                    // set flag if true or false
                    if ($clickedOn.hasClass('yesRadioBtn')) {
                        $booleanDiv.attr('btnChecked', 'yes')
                    }
                    else if ($clickedOn.hasClass('noRadionBtn')) {
                        $booleanDiv.attr('btnChecked', 'no')
                    }

                    // see if the next button should be enabled
                    enableDisableNext($entry.attr('questionSet'));
                })

            }
            else if (displayType == 'unlimited-or-integer') {
                let $dropDown = $(':nth-child(2)', $entry);
                let $amount = $(':nth-child(3)', $entry);

                function changeFunc(index, val) {
                    if (val == 'Amount') {
                        $amount.prop('disabled', false);
                        $amount.focus();
                    }
                    else if (val == 'Unlimited') {
                        $amount.prop('disabled', true)
                               .val('');
                    }

                    // see if the next button should be enabled
                    enableDisableNext($entry.attr('questionSet'));
                }

                $dropDown.revupDropDownField({
                        valueStartIndex: -1,
                        ifStartIndexNeg1Msg: qsData.label,
                        changeFunc: changeFunc,
                        value: ['Amount', 'Unlimited'],
                        overlayZIndex: 52,
                        });

                // disable the amount field and limit to digits
                $amount
                    .prop('disabled', true)
                    .on('keydown', function(e) {
                        // esc - close the date picker
                        if (e.which === 13) {
                            var tabEvent = jQuery.Event("keydown");
                            tabEvent.which = 9;
                            tabEvent.keyCode = 9;
                            $(this).trigger(tabEvent);

                            return false;
                        }

                        // make sure a number
                        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 40)) {
                            return;
                        }
                        else if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105)) {
                            // 0-9 only
                            let val = $(this).val();
                            val = val.replace(/[^\d]/g, '');
                            if (val.length >= 10) {
                                // see if the first 1 and length == 10
                                if (val[0] == '1' && val.length == 10) {
                                    return true;
                                }

                                e.preventDefault();
                                return false;
                            }

                        }
                        else {
                            e.preventDefault();
                            return false;
                        }

                        return true;
                    })
                    .on('input', function(e) {
                        let val = $(this).val();
                        val = val.replace(/[^\d]/g, '');
                        val = revupUtils.commify(val);
                        $(this).val(val);

                        // see if the next button should be enabled
                        enableDisableNext($entry.attr('questionSet'));
                    });
            }
            else {
                console.warn('Unknow display type: ' + displayType)
            }
        }, // initField

        loadInitValue:  function(dataArray, qs, choiceFields) {
                for (let key in dataArray) {
                    let $entry = $('.entry[questionSet="' + qs + '"][fieldName="' + key + '"]', $wizardBody);
                    let fieldDisplayType = $entry.attr('fieldDisplayType');

                    if ($entry.length) {

                        let $valField = $(':nth-child(2)', $entry);

                        if (fieldDisplayType == 'text') {
                            $valField.val(dataArray[key]);
                        }
                        else if (fieldDisplayType == 'dropdown') {
                            // see if dropdown depends on another field
                            for (let i = 0; i < choiceFields.length; i++) {
                                if (key == choiceFields[i].expected[0].field) {
                                    $valField.revupDropDownField({
                                        value: choiceFields[i].choices[dataArray[choiceFields[i].choice_key]],
                                        valueStartIndex: -1,
                                        updateValueList: true,
                                        overlayZIndex: 51,
                                    })
                                }

                            }
                            $valField.revupDropDownField('setValue', dataArray[key]);
                        }
                        else if (fieldDisplayType == 'date') {
                            let date = dataArray[key].split('-');
                            let d = '';
                            if (date.length == 3) {
                                d = date[1] + '/' + date[2] + '/' + date[0];
                            }
                            else {
                                d = dataArray[key];
                            }
                            $('.dateInput', $valField).val(d);
                            $('.dateInput', $valField).datepicker('update', d)
                        }
                        else if (fieldDisplayType == 'boolean') {
                            if (dataArray[key]) {
                                $('.booleanDiv', $entry).attr('btnChecked', 'yes');

                                $('.yesRadioBtn .radioBtn .unselected').hide();
                                $('.yesRadioBtn .radioBtn .selected').show();

                                $('.noRadioBtn .radioBtn .unselected').show();
                                $('.noRadioBtn .radioBtn .selected').hide();
                            }
                            else {
                                $('.booleanDiv', $entry).attr('btnChecked', '');

                                $('.yesRadioBtn .radioBtn .unselected').show();
                                $('.yesRadioBtn .radioBtn .selected').hide();

                                $('.noRadioBtn .radioBtn .unselected').hide();
                                $('.noRadioBtn .radioBtn .selected').show();
                            }
                        }
                        else if (fieldDisplayType == 'unlimited-or-integer') {
                            let amt = dataArray[key];
                            let $amtField = $(':nth-child(3)', $entry);
                            if (amt.toLowerCase() == 'unlimited') {
                                $valField.revupDropDownField('setValue', 'Unlimited');

                                $amtField.prop('disabled', true)
                                    .val('');
                            }
                            else {
                                $valField.revupDropDownField('setValue', 'Amount');

                                $amtField.prop('disabled', false)
                                    .val(revupUtils.commify(amt));
                            }
                        }
                        else {
                            console.error('loadInitValue -> unknown display field type: ' + fieldDisplayType);
                        }
                    }
                }
            // see if the next button should be enabled
            enableDisableNext();
        }, //loadInitValue

        getValue: function($entry, dataObj)
        {
            let fieldName = $entry.attr('fieldName');
            let fieldDisplayType = $entry.attr('fieldDisplayType');
            let fieldType = $entry.attr('fieldType');

            let val = '';
            if (fieldDisplayType == 'text') {
                let $valField = $('input', $entry);

                val = $valField.val();
            }
            else if (fieldDisplayType == 'dropdown') {
                let $valField = $('.dropDown', $entry);

                val = $valField.revupDropDownField('getValue');
            }
            else if (fieldDisplayType == 'boolean') {
                let $valField = $('.booleanDiv', $entry);
                val = false;
                if ($valField.attr('btnChecked') == 'yes') {
                    val = true
                }
            }
            else if (fieldDisplayType == 'date') {
                let $valField = $('.dateFieldDiv .dateInput', $entry);
                val = $valField.val();
                if (fieldType == 'date') {
                    let vParts = val.split('/');
                    val = vParts[2] + '-' + vParts[0] + '-' + vParts[1];
                }
            }
            else if (fieldDisplayType == 'unlimited-or-integer') {
                let $typeField = $('.dropDown', $entry);
                let typeVal = $typeField.revupDropDownField('getValue').toLowerCase();
                if (typeVal == 'unlimited') {
                    val = 'unlimited';
                }
                else if (typeVal == 'amount') {
                    let $amtField = $('.amount', $entry);
                    val = $amtField.val();
                    if (fieldType == 'int') {
                        val = val.replace(/,/g, "");
                    }
                }
            }

            if ($entry.attr('questionset') == 'tracker') {
                let valArray = [];
                valArray[0] = val;
                dataObj[fieldName] = valArray;

                return dataObj[fieldName];
            }
            // save value
            else {
                dataObj[fieldName] = val;
            }
        }, // getValue

    }
} // infoUtils
