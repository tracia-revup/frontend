var nonProfitInfo = function ($configWizard, templateData, questionSet) {

    // results
    let qsBioResults = {};

    // question set this page is interested in
    let qsBio = {};

    // flags if a question set value has change
    let bBioChanged = false;

    // load once stuff in the dom
    let iUtils;

    // find the question sets by looking about the icon
    let numFound = 0
    for (let i = 0; i < questionSet.length; i++) {
        if (questionSet[i] && questionSet[i].icon == 'bio') {
            qsBio = questionSet[i];
            numFound++;
        }

        if (numFound == 1) {
            break;
        }
    }

    // build list of bio choice fields
    let bioChoiceFields = [];
    if (!$.isEmptyObject(qsBio)) {
        for (let i = 0; i < qsBio.fields.fields.length; i++) {
            if (qsBio.fields.fields[i].choice_key) {
                bioChoiceFields.push(qsBio.fields.fields[i])
            }
        }
    }

    return {
        pageName: function() {
            //required
            return 'nonProfitInfoPage';
        },

        display: function($wizardBody) {
            // required
            let sTmp = [];

            sTmp.push('<div class="nonProfitInfoDiv">');
            sTmp.push('<div class="title">Organization Information</div>');
                sTmp.push('<div class="fieldsDiv">');

                    sTmp.push('<div class="line">');
                        sTmp.push('<div class="entry" questionSet="bio">');
                            sTmp.push('<div class="title required">Organization Name</div>');
                            sTmp.push('<input type="text" class="nonProfitName" placeholder="Name"  value=""/>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="line">');
                        sTmp.push('<div class="entry entry40Percent" questionSet="bio">');
                            sTmp.push('<div class="title required">City</div>');
                            sTmp.push('<input type="text" class="city" placeholder="City"  value=""/>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entryGap"></div>');
                        sTmp.push('<div class="entry entry20Percent" questionSet="bio">');
                            sTmp.push('<div class="title required">State</div>');
                            sTmp.push('<div class="stateDropDown dropDown"></div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entryGap"></div>');
                        sTmp.push('<div class="entry entry40Percent" questionSet="bio">');
                            sTmp.push('<div class="title required">Zip Code</div>');
                            sTmp.push('<input type="text" maxlength="5" placeholder="Zip Code" value=""/>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="line">');
                        sTmp.push('<div class="entry entry40Percent" questionSet="bio">');
                            sTmp.push('<div class="title required">Category NTEE Code</div>');
                            sTmp.push('<div class="NTEEGroupDropDown dropDown"></div>');
                            sTmp.push('<div class="title requiredBefore">Required Fields</div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entryGap"></div>');
                        sTmp.push('<div class="entry entry60Percent" questionSet="bio">');
                            sTmp.push('<div class="title required">Sub-category NTEE Code</div>');
                            sTmp.push('<div class="NTEECodeDropDown dropDown"></div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                sTmp.push('</div>');
            sTmp.push('</div>');
            // no arguments - just return the html
            // can add the following arguments to the class
            //      htmlClass: extra class (optional blank)
            //      disableNext: true/false (optional false)

            return {html: sTmp.join(''), disableNext: true};

        }, // display

        getData: function($wizardBody) {
            iUtils = infoUtils($configWizard);

            // the change flags
            iUtils.clearBioChangeInfo();

            $('.entry[questionSet="bio"]', $wizardBody).each(function() {
                let $entry = $(this);

                // find the fields and set the attributes
                let label = $('.title', $entry).html();
                label = label.replace('*', '');
                let bFound = false;
                if (!$.isEmptyObject(qsBio)) {
                    for (let i = 0; i < qsBio.fields.fields.length; i++) {
                        let qsField = qsBio.fields.fields[i];
                        if (qsField.label == label) {
                            $entry.attr('fieldDisplayType', qsField.type)
                                  .attr('fieldName', qsField.expected[0].field)
                                  .attr('fieldType', qsField.expected[0].type);

                            iUtils.initField($entry, qsField, bioChoiceFields);

                            bFound = true;
                            break;
                        }
                    }
                }

                if (!bFound) {
                    console.error('Unable to find question set info for: ', label);
                }
            })

            // get the initial values
            $.when($.get(qsBio.readUrl))
                .done(function(qsBioResultsRaw) {
                    // load the initial values
                    if (qsBioResultsRaw.results && qsBioResultsRaw.results[0]) {
                        qsBioResults = qsBioResultsRaw.results[0];
                        iUtils.loadInitValue(qsBioResults.data, 'bio', bioChoiceFields);
                    }
                })
                .fail(function(rResult) {
                    console.error('Unable to retreive nonprofit information: ', arguments);

                    $('body').revupMessageBox({
                        headerText:         'Retrieve Information',
                        msg:                'Unable to retrieve nonprofit information',
                        zIndex:             1000,
                    })
                })
                .always(function(qsBioResultsRaw) {
                    $configWizard.revupWizard('finishGetData');

                    // set the focus
                    $('.nonProfitInfoDiv .nonProfitName', $wizardBody).select();
                })
            return {showWaitSpinner: true}
        //     // optional
        //     // once the page is displayed go fetch data for the page (optional, default - don't do anything)
        //     // Return Formats
        //     //  1) true/false enable prev/next buttons
        //     //  2) object with the following fields
        //     //      a) showWaitSpinner - show the wait spinner, the prev/next buttons will be disabled
        }, // getData

/*
        footerMsg: function() {
            // optional
            // return the markup to display in the message area in the button area, left side  (optional, default - '')
        }, // footerMsg

        okToFinish: function($wizardBody) {
            // when the finish button is press, on last page, called for each page to see if ok to finish

            // see if the next button should be enabled or not (optional, default - true)
        }, // okToFinish

        okToMove: function($wizardBody) {
            // see if ok to move to the next page, varify data on page (option, default - true)


            // Return Formats
            //  1) true/false about advance to the new pages
            //  2) object with the following fields
            //      a) okToGo - true/false go to the next page (must have)
            //      b) historyShowError - should the history dot be done; green or blue or red in error (optonal - ok dot)
        }, // okToMove
*/

        save: function($wizardBody) {
            // get the bio question sets data
            let bioDataObj = {};
            $('.entry[questionSet="bio"]', $wizardBody).each(function() {
                let $entry = $(this);
                // get the values
                iUtils.getValue($entry, bioDataObj)
            });

            // to post/put the updates
            let bioUpdateCreate = () => {
                let deferred = $.Deferred();

                let url = "";
                let qsId = -1;
                let ajaxType = "POST";
                if (!$.isEmptyObject(qsBioResults.data)) {
                    qsId = qsBioResults.id;
                    url = qsBio.updateUrl.replace("questionSetDataId", qsId);
                    ajaxType = "PUT";
                }
                else {
                    url = qsBio.createUrl.replace("/questionSetDataId", "");
                    ajaxType = "POST";
                }

                $.ajax({
                    type: ajaxType,
                    url: url,
                    data: JSON.stringify(bioDataObj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                })
                .done (function(r) {
                    deferred.resolve(r)
                })
                .fail (function(r) {
                    deferred.reject(r)
                })

                return deferred.promise();
            }; // bioUpdateCreate

            // do the update
            let updateCreateFuns = [];
            if (iUtils.hasBioInfoChanged()) {
                updateCreateFuns.push(bioUpdateCreate())
            }
            if (iUtils.hasCommitteeInfoChanged()) {
                updateCreateFuns.push(committeeInfoUpdateCreate())
            }
            if (updateCreateFuns.length == 0) {
                return
            }

            setTimeout(function() {
                $.when(updateCreateFuns)
                    .done (function(updateCreate1, updateCreate2) {
                        $('.wizard', $configWizard).revupWizard('finishSave');
                    })
                    .fail(function(updateCreate1, updateCreate2) {
                        console.group('Unable to save change to campaign or candidate information->');
                        console.error('create object 1: ', updateCreate1);
                        console.error('create object 2: ', updateCreate2);
                        console.groupEnd();

                        $('body').revupMessageBox({
                            headerText:         'Save Information',
                            msg:                'Unable to save/create candidate and campaign information',
                            zIndex:             1000,
                        })
                    })
                }, 10);

            return {updateAndLoad: false, showWaitSpinner: true}
        }, // save
    }
}; // nonProfitInfo
