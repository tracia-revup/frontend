var issues = function ($configWizard, templateData) {
    let userName = '';
    return {
        pageName: function() {
            //required
            return 'issuesPage';
        },

        display: function($wizardBody) {
            // required
            let sTmp = [];
            sTmp.push('<h1>Candidate Issues</h1>');
            sTmp.push('<div class="issuesDes">');
                sTmp.push('Please add issues important to your candidate/organization. The issues you list should be</text></br>');
                sTmp.push('broad. Some examples are:</br>');
                sTmp.push('<ul>');
                  sTmp.push('<li>Education</li>');
                  sTmp.push('<li>Immigration</li>');
                  sTmp.push('<li>Taxes</li>');
                sTmp.push('</ul>');
            sTmp.push('</div>');
            sTmp.push('<div class="issues entry scrollSection" questionsetIcon="issues">');
                sTmp.push('<div class="issues">');
                sTmp.push('</div>');
            sTmp.push('</div>');

            return {html: sTmp.join(''), disableNext: true};

            // no arguments - just return the html
            // can add the following arguments to the class
            //      htmlClass: extra class (optional blank)
            //      disableNext: true/false (optional false)
        }, // display

        getData: function($wizardBody) {
            var $qIDentry = $('.entry', $wizardBody);
            var qIcon = $qIDentry.attr('questionsetIcon');
            campInfoConfig.load(questionSetLst, qIcon, $qIDentry, $configWizard);
            $wizardBody.revupWizard('enableNext', false);

            return {showWaitSpinner: true};

            // optional
            // once the page is displayed go fetch data for the page (optional, default - don't do anything)
            // Return Formats
            //  1) true/false enable prev/next buttons
            //  2) object with the following fields
            //      a) showWaitSpinner - show the wait spinner, the prev/next buttons will be disabled
        }, // getData
/*
        footerMsg: function() {
            // optional
            // return the markup to display in the message area in the button area, left side  (optional, default - '')
        }, // footerMsg

        okToFinish: function($wizardBody) {
            // when the finish button is press, on last page, called for each page to see if ok to finish

            // see if the next button should be enabled or not (optional, default - true)
        }, // okToFinish

        okToMove: function($wizardBody) {
            // see if ok to move to the next page, varify data on page (option, default - true)


            // Return Formats
            //  1) true/false about advance to the new pages
            //  2) object with the following fields
            //      a) okToGo - true/false go to the next page (must have)
            //      b) historyShowError - should the history dot be done; green or blue or red in error (optonal - ok dot)
        }, // okToMove
*/

        save: function($wizardBody) {
            var $qIDentry = $('.entry', $wizardBody);
            campInfoConfig.saving($qIDentry);

            return {showWaitSpinner: true}
            // save - time for the page to save the data on the page (optional - default do nothing)

            // Return Formats
            //  1) true/false save done and can go to the next page (defult - true going to next page)
            //  2) object with the following fields
            //      a) updateAndLoad - true/false if can go to the next page (optional - if missing of to go to next page)
            //      b) showWaitSpinner - tre/false should the wait spinner be displayed, if true the next and prev buttons are disabled (default, do not display wait spinner)

        }, // save
    }
}; // issues
