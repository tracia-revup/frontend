var termsOfService = function ($tosWizard, templateData) {
    let userName = '';
    return {
        pageName: function() {
            //required
            return 'termsOfServicePage';
        },

        display: function($wizardBody) {
            // required
            let sTmp = [];

            sTmp.push('<div class="termsOfServiceDiv">');
                sTmp.push('<div class="title">RevUp Terms of Use</div>');
                sTmp.push('<div class="tosDiv">');
                    sTmp.push('<div class="tosOuterDiv">');
                        sTmp.push('<div class="tosInnerDiv">');
                            sTmp.push('<div class="tos">Loading...</div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="fadeOut"></div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            $('.wizard', $tosWizard).revupWizard('changeBtnLabel', 'next', "Agree");
            return {html: sTmp.join(''), disableNext: false};
        }, // display

        getData: function($wizardBody) {
            $('.tosOuterDiv', $wizardBody).on('scroll', function(e) {
                if(($(this)[0].scrollHeight - $(this).scrollTop()) <= $(this).outerHeight()) {
                    $('.fadeOut', $wizardBody).hide();
                    atBottom = true;
                }
                else {
                    $('.fadeOut', $wizardBody).show();
                    atBottom = false;
                };
            })

            $.ajax({
                url: templateData.tosFile,
                dataType: 'text',
                type: 'GET'
            })
            .done(function(r) {
                // load the contact data
                $('.termsOfServiceDiv .tosInnerDiv .tos').html(r);
            })
        }, // getData

        save: function($wizardBody) {
            $.ajax({
                type: 'POST',
                url: templateData.tosUrl,
            })
            .done(function(r) {
            })
            .fail(function(r) {
                console.error('Agree to Terms Of Service failed: ', r)
            })
            // save - time for the page to save the data on the page (optional - default do nothing)

            // Return Formats
            //  1) true/false save done and can go to the next page (defult - true going to next page)
            //  2) object with the following fields
            //      a) updateAndLoad - true/false if can go to the next page (optional - if missing of to go to next page)
            //      b) showWaitSpinner - tre/false should the wait spinner be displayed, if true the next and prev buttons are disabled (default, do not display wait spinner)
        }, // save
    }
}; // termsOfService
