var similarToMine = function ($configWizard, templateData, eData, fieldData) {
    let userName = '';
    return {
        pageName: function() {
            //required
            return 'similarToMinePage';
        },

        display: function($wizardBody) {
            // required
            let sTmp = [];
            sTmp.push('<div class="similarToMineTitle"><h1>Candidates Similar to Mine</h1></div>');
            sTmp.push('<div class="SimilarToMineDiv entry" questionsetIcon="tracker">');
                sTmp.push('<div class="subTitle">RevUp&#39;s donor algorithm works best when you provide a list of candidates that are similar to </b>');
                    sTmp.push('yours. These candidates can be from federal, state, or local campaigns</div>')
            sTmp.push('</div>');

            sTmp.push('<div class="detailsData">');
            sTmp.push('</div>');

            // hide and disable the middle buttons
            $configWizard.revupWizard('displayMiddleBtn', false)
                         .revupWizard('enableMiddle', false);

            return sTmp.join('');

        }, // display

        getData: function($wizardBody) {
            var $qIDentry = $('.entry', $wizardBody);
            var qIcon = $qIDentry.attr('questionsetIcon');
            campInfoConfig.load(questionSetLst, qIcon, $qIDentry, $configWizard, templateData);
            return {showWaitSpinner: true};
        }
    }
            // add the event handlers
            // ($wizardBody)
}; // similarToMine

/*
        getData: function($wizardBody) {
            // optional
            // once the page is displayed go fetch data for the page (optional, default - don't do anything)
            // Return Formats
            //  1) true/false enable prev/next buttons
            //  2) object with the following fields
            //      a) showWaitSpinner - show the wait spinner, the prev/next buttons will be disabled
        }, // getData

        footerMsg: function() {
            // optional
            // return the markup to display in the message area in the button area, left side  (optional, default - '')
        }, // footerMsg

        okToFinish: function($wizardBody) {
            // when the finish button is press, on last page, called for each page to see if ok to finish

            // see if the next button should be enabled or not (optional, default - true)
        }, // okToFinish

        okToMove: function($wizardBody) {
            // see if ok to move to the next page, varify data on page (option, default - true)


            // Return Formats
            //  1) true/false about advance to the new pages
            //  2) object with the following fields
            //      a) okToGo - true/false go to the next page (must have)
            //      b) historyShowError - should the history dot be done; green or blue or red in error (optonal - ok dot)
        }, // okToMove

        save: function($wizardBody) {
            // save - time for the page to save the data on the page (optional - default do nothing)

            // Return Formats
            //  1) true/false save done and can go to the next page (defult - true going to next page)
            //  2) object with the following fields
            //      a) updateAndLoad - true/false if can go to the next page (optional - if missing of to go to next page)
            //      b) showWaitSpinner - tre/false should the wait spinner be displayed, if true the next and prev buttons are disabled (default, do not display wait spinner)
        }, // save
*/
