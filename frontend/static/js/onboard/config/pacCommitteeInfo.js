var pacCommitteeInfo = function ($configWizard, templateData, questionSet) {
    // results
    let qsBioResults = {};
    let qsCommitteeInfoResults = {};

    // question set this page is interested in
    let qsBio = {};
    let qsCommitteeInfo = {};

    // flags if a question set value has change
    let bBioChanged = false;
    let bCommitteeInfoChanged = false;

    // load once stuff in the dom
    let iUtils;

    // find the question sets by looking about the icon
    let numFound = 0
    for (let i = 0; i < questionSet.length; i++) {
        if (questionSet[i] && questionSet[i].icon == 'bio') {
            qsBio = questionSet[i];

            numFound++;
        }

        if (questionSet[i] && questionSet[i].icon == 'campaign-info') {
            qsCommitteeInfo = questionSet[i];

            numFound++;
        }

        if (numFound == 2) {
            break;
        }
    }

    // build list of bio choice fields
    let bioChoiceFields = [];
    if (!$.isEmptyObject(qsBio)) {
        for (let i = 0; i < qsBio.fields.fields.length; i++) {
            if (qsBio.fields.fields[i].choice_key && qsBio.fields.fields[i].choice_key) {
                bioChoiceFields.push(qsBio.fields.fields[i])
            }
        }
    }

    // build list of choice fields
    let committeeChoiceFields = [];
    if (!$.isEmptyObject(qsCommitteeInfo)) {
        for (let i = 0; i < qsCommitteeInfo.fields.fields.length; i++) {
            if (qsCommitteeInfo.fields.fields[i].choice_key && qsCommitteeInfo.fields.fields[i].choice_key) {
                committeeChoiceFields.push(qsCommitteeInfo.fields.fields[i])
            }
        }
    }

    // console.log('personal info question set: ', qsBio);
    // console.log('Committee Info question set: ', qsCommitteeInfo);
    // console.log('question set: ', questionSet)
    return {
        pageName: function() {
            //required
            return 'pacCommitteeInfoPage';
        },

        display: function($wizardBody) {
            // required
            let sTmp = [];

            sTmp.push('<div class="pacCommitteeInfoDiv">');
            sTmp.push('<div class="title">Committee Information</div>');
            sTmp.push('<div class="fieldsDiv">');
                sTmp.push('<div class="line sectionTitleLine">');
                    sTmp.push('<div class="sectionTitle">Information</div>')
                sTmp.push('</div>');
                sTmp.push('<div class="line">');
                    sTmp.push('<div class="entry entry60Percent" questionSet="bio">');
                        sTmp.push('<div class="title required">Committee Name</div>');
                        sTmp.push('<input type="text" class="committeeName" placeholder="Committee Name"  value=""/>');
                    sTmp.push('</div>')
                    sTmp.push('<div class="entryGap"></div>');
                    sTmp.push('<div class="entry entry40Percent" questionSet="bio">');
                        sTmp.push('<div class="title required">Political Party</div>');
                        sTmp.push('<div class="politicalPartyDropDown dropDown"></div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
                sTmp.push('<div class="line sectionTitleLine">');
                    sTmp.push('<div class="sectionTitle">Focus</div>')
                sTmp.push('</div>');
                sTmp.push('<div class="line">');
                    sTmp.push('<div class="entry entry33Percent" questionSet="committee-info">');
                        sTmp.push('<div class="title required">Type of Office</div>');
                        sTmp.push('<div class="typeOfOfficeDropDown dropDown"></div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="entryGap"></div>');
                    sTmp.push('<div class="entry entry33Percent" questionSet="committee-info">');
                        sTmp.push('<div class="title required">Election Cycle Start</div>');
                        sTmp.push('<div class="electionDate dateFieldDiv">');
                            sTmp.push('<input type="text" class="dateInput" placeholder="mm/dd/yyyy">');
                            sTmp.push('<div class="dateIcon icon icon-calendar"></div>')
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="entryGap"></div>');
                    sTmp.push('<div class="entry entry33Percent" questionSet="committee-info">');
                        sTmp.push('<div class="title">Contribution Limit</div>');
                        sTmp.push('<div class="contributionLimitDropDown dropDown"></div>');
                        sTmp.push('<input type="text" class="contributionAmt amount" placeholder="$ - Contribution Amount"  value=""/>');
                    sTmp.push('</div>')
                sTmp.push('</div>')
                sTmp.push('<div class="line">');
                    sTmp.push('<div class="entry entry50Percent up30" questionSet="committee-info">');
                        sTmp.push('<div class="title required">Office Sought</div>');
                        sTmp.push('<div class="typeOfOfficeDropDown dropDown"></div>');
                        sTmp.push('<div class="title requiredBefore">Required Fields</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            // no arguments - just return the html
            // can add the following arguments to the class
            //      htmlClass: extra class (optional blank)
            //      disableNext: true/false (optional false)
            return {html: sTmp.join(''), disableNext: true};
        }, // display

        getData: function($wizardBody) {
            iUtils = infoUtils($configWizard);

            // the change flags
            iUtils.clearBioChangeInfo();
            iUtils.clearCommitteeInfoChanged();

            $('.entry[questionSet="bio"]', $wizardBody).each(function() {
                let $entry = $(this);

                // find the fields and set the attributes
                let label = $('.title', $entry).html();
                label = label.replace('*', '');
                let bFound = false;
                if (!$.isEmptyObject(qsBio)) {
                    for (let i = 0; i < qsBio.fields.fields.length; i++) {
                        let qsField = qsBio.fields.fields[i];
                        if (qsField.label == label) {
                            $entry.attr('fieldDisplayType', qsField.type)
                                  .attr('fieldName', qsField.expected[0].field)
                                  .attr('fieldType', qsField.expected[0].type);

                            iUtils.initField($entry, qsField, bioChoiceFields);

                            bFound = true;
                            break;
                        }
                    }
                }

                if (!bFound) {
                    console.error('Unable to find question set info for: ', label);
                }
            })

            $('.entry[questionSet="committee-info"]', $wizardBody).each(function() {
                let $entry = $(this);

                // find the fields and set the attributes
                let label = $('.title', $entry).html();
                if (label == '' || label == undefined) {
                    return;
                }
                label = label.replace('*', '');
                let bFound = false;
                if (!$.isEmptyObject(qsCommitteeInfo)) {
                    for (let i = 0; i < qsCommitteeInfo.fields.fields.length; i++) {
                        let qsField = qsCommitteeInfo.fields.fields[i];
                        if (qsField.label == label) {
                            $entry.attr('fieldDisplayType', qsField.type)
                                  .attr('fieldName', qsField.expected[0].field)
                                  .attr('fieldType', qsField.expected[0].type);

                            iUtils.initField($entry, qsField, committeeChoiceFields)

                            bFound = true;
                            break;
                        }
                    }
                }

                if (!bFound) {
                    console.error('Unable to find question set info for: ', label);
                }
            })

            // get the initial values
            $.when($.get(qsBio.readUrl), $.get(qsCommitteeInfo.readUrl))
                .done(function(qsBioResultsRaw, qsCommitteeInfoResultsRaw) {
                    // load the initial values
                    if (qsBioResultsRaw[0].results && qsBioResultsRaw[0].results[0]) {
                        qsBioResults = qsBioResultsRaw[0].results[0];
                        iUtils.loadInitValue(qsBioResults.data, 'bio', bioChoiceFields);
                    }

                    if (qsCommitteeInfoResultsRaw[0].results && qsCommitteeInfoResultsRaw[0].results[0]) {
                        qsCommitteeInfoResults = qsCommitteeInfoResultsRaw[0].results[0];
                        iUtils.loadInitValue(qsCommitteeInfoResults.data, 'committee-info', committeeChoiceFields);
                    }
                })
                .fail(function(rResult) {
                    console.error('Unable to retreive committee information: ', arguments);

                    $('body').revupMessageBox({
                        headerText:         'Retrieve Information',
                        msg:                'Unable to retrieve committee information',
                        zIndex:             1000,
                    })
                })
                .always(function(qsBioResultsRaw, qsCommitteeInfoResultsRaw) {
                    $configWizard.revupWizard('finishGetData');

                    // set the focus
                    $('.pacCommitteeInfoDiv .committeeName', $wizardBody).select();
                })

            return {showWaitSpinner: true}
            // optional
            // once the page is displayed go fetch data for the page (optional, default - don't do anything)
            // Return Formats
            //  1) true/false enable prev/next buttons
            //  2) object with the following fields
            //      a) showWaitSpinner - show the wait spinner, the prev/next buttons will be disabled
        }, // getData

/*
        footerMsg: function() {
            // optional
            // return the markup to display in the message area in the button area, left side  (optional, default - '')
        }, // footerMsg

        okToFinish: function($wizardBody) {
            // when the finish button is press, on last page, called for each page to see if ok to finish

            // see if the next button should be enabled or not (optional, default - true)
        }, // okToFinish

        okToMove: function($wizardBody) {
            // see if ok to move to the next page, varify data on page (option, default - true)


            // Return Formats
            //  1) true/false about advance to the new pages
            //  2) object with the following fields
            //      a) okToGo - true/false go to the next page (must have)
            //      b) historyShowError - should the history dot be done; green or blue or red in error (optonal - ok dot)
        }, // okToMove
*/

        save: function($wizardBody) {
            // get the bio question sets data
            let bioDataObj = {};
            $('.entry[questionSet="bio"]', $wizardBody).each(function() {
                let $entry = $(this);

                // get the values
                iUtils.getValue($entry, bioDataObj)
            });

            // get the committee info question sets data
            let committeeInfoObj = {};
            $('.entry[questionSet="committee-info"]', $wizardBody).each(function() {
                let $entry = $(this);

                // get the values
                iUtils.getValue($entry, committeeInfoObj)
            })

            // to post/put the updates
            let bioUpdateCreate = () => {
                let deferred = $.Deferred();

                let url = "";
                let qsId = -1;
                let ajaxType = "POST";
                if (!$.isEmptyObject(qsBioResults.data)) {
                    qsId = qsBioResults.id;
                    url = qsBio.updateUrl.replace("questionSetDataId", qsId);
                    ajaxType = "PUT";
                }
                else {
                    url = qsBio.createUrl.replace("/questionSetDataId", "");
                    ajaxType = "POST";
                }

                $.ajax({
                    type: ajaxType,
                    url: url,
                    data: JSON.stringify(bioDataObj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                })
                .done (function(r) {
                    deferred.resolve(r)
                })
                .fail (function(r) {
                    deferred.reject(r)
                })

                return deferred.promise();
            }; // bioUpdateCreate

            let committeeInfoUpdateCreate = () => {
                let deferred = $.Deferred();

                let url = "";
                let qsId = -1;
                let ajaxType = "POST";
                if (!$.isEmptyObject(qsCommitteeInfoResults.data)) {
                    qsId = qsCommitteeInfoResults.id;
                    url = qsCommitteeInfo.updateUrl.replace("questionSetDataId", qsId);
                    ajaxType = "PUT";
                }
                else {
                    url = qsCommitteeInfo.createUrl.replace("/questionSetDataId", "");
                    ajaxType = "POST";
                }

                $.ajax({
                    type: ajaxType,
                    url: url,
                    data: JSON.stringify(committeeInfoObj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                })
                .done (function(r) {
                    deferred.resolve(r)
                })
                .fail (function(r) {
                    deferred.reject(r)
                })

                return deferred.promise();
            }; // committeeInfoUpdateCreate

            // do the update
            let updateCreateFuns = [];
            if (iUtils.hasBioInfoChanged()) {
                updateCreateFuns.push(bioUpdateCreate())
            }
            if (iUtils.hasCommitteeInfoChanged()) {
                updateCreateFuns.push(committeeInfoUpdateCreate())
            }
            if (updateCreateFuns.length == 0) {
                return
            }

            setTimeout(function() {
                $.when(updateCreateFuns)
                    .done (function(updateCreate1, updateCreate2) {
                        $('.wizard', $configWizard).revupWizard('finishSave');
                    })
                    .fail(function(updateCreate1, updateCreate2) {
                        console.group('Unable to save change to campaign or candidate information->');
                        console.error('create object 1: ', updateCreate1);
                        console.error('create object 2: ', updateCreate2);
                        console.groupEnd();

                        $('body').revupMessageBox({
                            headerText:         'Save Information',
                            msg:                'Unable to save/create candidate and campaign information',
                            zIndex:             1000,
                        })
                    })
                }, 10);

            return {updateAndLoad: false, showWaitSpinner: true}
        }, // save
    }
}; // pacCommitteeInfo
