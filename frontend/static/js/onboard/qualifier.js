var onboardQualifier = function(templateData) {

// captcha widgets
let contactCustCaptchaWidget;
let prodSelectCaptchaWidget;
let productType         = '';           // The type of product
let productId           = 0;
let productSelected     = '';           // The product the user selected
let typeOfRace          = '';
let numContstituents    = '';
let clientType          = '';
let officeType          = '';

let contactThemLater = (fundraiserType) => {
    // hide questions
    $('.qualifierDiv .quest1').hide();
    $('.qualifierDiv .quest2').hide();
    $('.qualifierDiv .quest3').hide();

    // display the submit button and disable it
    $('.bottomBtnDiv .submitBtn').show()
                                 .prop('disabled', true);

    // change the page title
    let titleText = 'We are currently hard at work on RevUp for ' + fundraiserType + ', please provide us with your ' +
                    'contact information and we will notify you when it\'s  ';
    $('.headerTextDiv .headerText').html(titleText)

    // display contact later screen
    $('.qualifierDiv .contactCustLater').show();
} // contactThemLater

// enable/disable the submit button
let enableDisableSubmit = () => {
    // make sure everything has a value
    let bEnable = true;
    let $page = $();
    let captchaWidget;
    if ($('.contactCustLater').is(':visible')) {
        $page = $('.cotactCustLater');
        captchaWidget = contactCustCaptchaWidget;
    }
    else if ($('.productSelect').is (':visible')) {
        $page = $('.productSelect');
        captchaWidget = prodSelectCaptchaWidget
    }

    $('.userInfoDiv :input', $page).each(function() {
        if ($(this).hasClass('telephone')) {
            let val = $(this).val();
            val = val.replace(/[^\d]/g, '');
            if (!((val[0] == '1' && val.length == 11) || (val.length == 10))) {
                bEnable = false;
                return false;
            }
        }
        else if ($(this).hasClass('emailAddr')) {
            //let r = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/
            let r = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            let val = $(this).val();
            // if (!validEmailAddress(val)) {
            if (!r.test(val)) {
                bEnable = false;
                return false;
            }
        }

        if ($(this).val() == '') {
            bEnable = false;
            return false;
        }
    })

    // test to see it the captcha has been selected
    if (grecaptcha.getResponse(captchaWidget) == '') {
        bEnable = false;
    }

    // if on the product select page make sure something is selected
    if ($('.qualifierDiv .line.productSelect').is(':visible') && productSelected == '') {
        bEnable = false;
    }

    // enable/disable the submit button
    $('.bottomBtnDiv .submitBtn').prop('disabled', !bEnable);
} // enableDisableSubmit

let loadEvents = () =>
{
    // radio button click handlers
    $('.radioBtnDiv').on('click', function(e) {
        let $clickedOn = $(this);
        // unselect all
        let $containingDiv = $clickedOn.parent();
        $('.radioBtn .selected', $containingDiv).hide();
        $('.radioBtn .unselected', $containingDiv).show();

        // select the clicked on
        $('.radioBtn .selected', $clickedOn).show();
        $('.radioBtn .unselected', $clickedOn).hide();

        // see what is selected
        if ($clickedOn.hasClass('politicalRadioBtn')) {
            // keep product type
            productType = 'political';

            // display the second questions
            $('.qualifierDiv .quest2').show();

            // reset selectors
            $('.quest2 .raceDropdownDiv').revupDropDownField('reset');
            $('.quest3 .numConstituentsDropdownDiv').revupDropDownField('reset');
        }
        else if ($clickedOn.hasClass('nonProfitRadioBtn')) {
            // keep product type
            productType = 'nonprofit';

            contactThemLater('Nonprofit');
        }
        else if ($clickedOn.hasClass('academicRadioBtn')) {
            // keep product type
            productType = 'academic';

            contactThemLater('Academic');
        }
    }) //radio button

    // dropdown for the race
    let typeOfRace = '';
    $('.quest2 .raceDropdownDiv')
        .revupDropDownField({
            valueStartIndex: -1,
            sortList: false,
            bDisplaySectionHeaders: true,
            ifStartIndexNeg1Msg: 'Type of Race',
            value: [{sectionHeader: 'Campaign'},
                    {value: 'federal', displayValue: 'Federal'},
                    {value: 'state', displayValue: 'State Wide'},
                    {value: 'local', displayValue: 'Local/Municipal'},
                    {sectionHeader: 'Committee'},
                    {value: 'committee-federal', displayValue: 'Federal'},
                    {value: 'committee-state', displayValue: 'State Wide'},
                    {value: 'committee-local', displayValue: 'Local/Municipal'},
                    {sectionHeader: 'PAC'},
                    {value: 'pac-federal', displayValue: 'Federal'},
                    {value: 'pac-state', displayValue: 'State Wide'},
                    {value: 'pac-local', displayValue: 'Local/Municipal'}],
        })
        .on('revup.dropDownSelected', function (e) {
            // getthe client type
            if (e.dropDownValue == 'federal' || e.dropDownValue == 'state' || e.dropDownValue == 'local') {
                clientType = "candidate";
                officeType = e.dropDownValue;
            }
            else if (e.dropDownValue == 'committee-federal' || e.dropDownValue == 'committee-state' || e.dropDownValue == 'committee-local') {
                clientType = "committee";
                officeType = e.dropDownValue.substring(10)
            }
            else if (e.dropDownValue == 'pac-federal' || e.dropDownValue == 'pac-state' || e.dropDownValue == 'pac-local') {
                clientType = "pac";
                officeType = e.dropDownValue.substring(4)
            }

            if (e.dropDownValue == 'state' || e.dropDownValue == 'local' || e.dropDownValue == 'committee-state' || e.dropDownValue == 'committee-local') {
                $('.qualifierDiv .quest3').show();
            }
            else {
                // hide the lines
                $('.qualifierDiv .line').hide();

                // change the title
                $('.headerTextDiv .headerText').html('Good news!! We have a version of RevUp that\'s right for you.');

                // get the setting need to enable/disable the correct version
                typeOfRace = $('.qualifierDiv .quest2 .raceDropdownDiv').revupDropDownField('getValue');

                // display the correct active colums
                let disableColumns = [];
                if (e.dropDownValue == 'federal' || e.dropDownValue == 'pac-federal' || e.dropDownValue == 'committee-federal') {
                    disableColumns = ['liteGrid', 'plusGrid', 'selectGrid'];
                    productSelected = 'premGrid';
                    productId = templateData.productPremium.productId;
                    numContstituents = 'Greater Than 1,000,000';

                    // display the message
                    let msg = 'If you would like a demo of RevUp, please give us your contact information and we will have someone contact you.'
                    $('.qualifierDiv .productSelect .productMsgDiv .msg').html(msg);
                }
                else if (e.dropDownValue == 'pac-state' || e.dropDownValue == 'committee-state') {
                    disableColumns = ['liteGrid', 'plusGrid'];
                }
                else if (e.dropDownValue == 'pac-local' || e.dropDownValue == 'committee-local') {
                    disableColumns = ['liteGrid'];
                }
                qualifierSpecs.load(disableColumns, productSelected, $('.qualifierDiv'), templateData);

                // display the submit button and disable it
                $('.bottomBtnDiv .submitBtn').show()
                                             .prop('disabled', true);

                // display the selector
                $('.qualifierDiv .productSelect .line').show();
                $('.qualifierDiv .productSelect').show();
            }
        })

    // dropdown for the num constituents
    $('.quest3 .numConstituentsDropdownDiv')
        .revupDropDownField({
            valueStartIndex: -1,
            ifStartIndexNeg1Msg: 'Number Of Constituents',
            sortList: false,
            value: [{value: 'constituents1', displayValue: 'Greater Than 1,000,000'},
                    {value: 'constituents2', displayValue: '500,001 to 1,000,000'},
                    {value: 'constituents3', displayValue: '100,000 to 500,000'},
                    {value: 'constituents4', displayValue: 'Less than 100,000'}],
        })
        .on('revup.dropDownSelected', function (e) {
            // hide questions
            $('.qualifierDiv .line').hide();

            // change the title
            $('.headerTextDiv .headerText').html('Good news!! We have a version of RevUp that\'s right for you.');

            // get the setting need to enable/disable the correct version
            //let typeOfRace = $('.qualifierDiv .quest2 .raceDropdownDiv').revupDropDownField('getValue');
            numContstituents =  e.dropDownDisplayValue;

            // display the correct active colums
            let disableColumns = [];
            let selectedColumn = ''
            if (e.dropDownValue == 'constituents1') {
                disableColumns = ['liteGrid', 'plusGrid', 'selectGrid'];
                productId = templateData.productPremium.productId;
                productSelected = 'political-premium';

                // display the message
                let msg = 'If you would like a demo of RevUp, please give us your contact information and we will have someone contact you.'
                $('.qualifierDiv .productSelect .productMsgDiv .msg').html(msg);
            }
            else if (e.dropDownValue == 'constituents2') {
                disableColumns = ['liteGrid', 'plusGrid'];
            }
            else if (e.dropDownValue == 'constituents3') {
                disableColumns = ['liteGrid'];
            }
            else if (e.dropDownValue == 'constituents4') {
                disableColumns = [];
            }
            qualifierSpecs.load(disableColumns, productSelected, $('.qualifierDiv'), templateData);

            // display the submit button and disable it
            $('.bottomBtnDiv .submitBtn').show()
                                         .prop('disabled', true);

            // display the selector
            $('.qualifierDiv .productSelect .line').show();
            $('.qualifierDiv .productSelect').show();
        })

    // contact customer later & product selection
    $('.contactCustLater .userInfoDiv, .productSelect .userInfoDiv').on('input', ':input', function(e) {
        // make sure everything has a value
        enableDisableSubmit();
    })

    $('.contactCustLater, .productSelect')
        .on('keydown', '.userInfoDiv .telephone', function(e) {
            // make sure a number
            // make sure a number
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 40)) {
                return;
            }
            else if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105)) {
                // 0-9 only
                let val = $(this).val();
                val = val.replace(/[^\d]/g, '');
                if (val.length >= 10) {
                    // see if the first 1 and length == 10
                    if (val[0] == '1' && val.length == 10) {
                        return true;
                    }

                    e.preventDefault();
                    return false;
                }

            }
            else {
                e.preventDefault();
                return false;
            }

            return true;
        })
        .on('input', '.userInfoDiv .telephone', function(e) {
            let val = $(this).val();
            val = val.replace(/[^\d]/g, '');
            let len = val.length;
            if (len > 4 && len <= 7) {
                let prefix = val.substr(0, len - 4);
                let suffix = val.substr(len - 4, len)
                val = prefix + '-' + suffix;
            }
            else if (len > 7 && len <= 10) {
                let areaCode = val.substr(0, len - 7);
                let areaCodeLen = areaCode.length;
                let prefix = val.substr(areaCodeLen, 3);
                let suffix = val.substr(areaCodeLen + 3, 4);
                val = '(' + areaCode + ')' + prefix + '-' + suffix;
            }
            else if (len == 11) {
                let areaCode = val.substr(1, len - 8);
                let areaCodeLen = areaCode.length + 1;
                let prefix = val.substr(areaCodeLen, 3);
                let suffix = val.substr(areaCodeLen + 3, 4);
                val = '1+(' + areaCode + ')' + prefix + '-' + suffix;
            }

            $(this).val(val);
            e.preventDefault();

            // see if the submit button should be enable or disabled
            enableDisableSubmit();

            return false;
        })

    // cancel/submit button handlers
    $('.bottomBtnDiv .cancelBtn').on('click', function(e) {
        window.close();
    })

    $('.bottomBtnDiv .submitBtn')
        .on('click', function(e) {
            // see what submit this is for
            let data = {};
            data.product_type = productType;
            if ($('.qualifierDiv .contactCustLater').is(':visible')) {
                data.first_name = $('.contactCustLater .userInfoDiv .firstName').val();
                data.last_name = $('.contactCustLater .userInfoDiv .lastName').val();
                data.email = $('.contactCustLater .userInfoDiv .emailAddr').val();
                data.phone = $('.contactCustLater .userInfoDiv .telephone').val();
                data.title = $('.contactCustLater .userInfoDiv .title').val();
                data.recaptcha = grecaptcha.getResponse(contactCustCaptchaWidget);
            }
            else if ($('.qualifierDiv .productSelect').is(':visible')) {
                data.first_name = $('.productSelect .userInfoDiv .firstName').val();
                data.last_name = $('.productSelect .userInfoDiv .lastName').val();
                data.email = $('.productSelect .userInfoDiv .emailAddr').val();
                data.phone = $('.productSelect .userInfoDiv .telephone').val();
                data.title = $('.productSelect .userInfoDiv .orgName').val();
                data.client_type = clientType;
                data.office_type = officeType;
                data.constituent_count = numContstituents;
                data.product_id = productId;
                data.recaptcha = grecaptcha.getResponse(prodSelectCaptchaWidget);
            }

            // disable the submit button
            $(this).prop('disabled', true);

            var url = templateData.onboardQualifierUrl;
            $.ajax({
                url: url,
                data: data,
                type: "POST",
                traditional: true,
                contentType: 'application/x-www-form-urlencoded; charset=utf-8',
                processData: true
            })
            .done(function(r) {
                // display the closing page
                $('section').hide();
                $('footer').hide();
                $('.messageContainer').show();
            })
            .fail(function(r) {
                $('body').revupMessageBox({
                    headerText:         'Submit Error',
                    msg:                'Unable to submit request, try again in a few minutes',
                })

                console.error('qualifier post -> fail: ', r)
            })

            return false;
        });

    $('.qualifierDiv').on('revup.onboard.selectProduct', function(e) {
        // save the selected product
        switch (e.selectedProduct) {
            case 'liteGrid':
                productSelected = 'political-lite';
                productId = templateData.productLite.productId;
                break;
            case 'plusGrid':
                productSelected = 'political-plus';
                productId = templateData.productPlus.productId;
                break;
            case 'selectGrid':
                productSelected = 'political-select';
                productId = templateData.productSelect.productId;
                break;
            case 'premGrid':
                productSelected = 'political-premium';
                productId = templateData.productPremium.productId;
                break;
        }

        // update the message
        let msg = '';
        if (productSelected == 'political-lite' || productSelected == 'political-plus') {
            msg = 'Give us your contact information and we will send you an email with a link to sign up for RevUp.'
        }
        else if (productSelected == 'political-select' || productSelected == 'political-premium') {
            msg = 'If you would like a demo of RevUp, please give us your contact information and we will have someone contact you.'
        }
        $('.qualifierDiv .productSelect .productMsgDiv .msg').html(msg);

        // see if the submit button should be enable
        enableDisableSubmit();
    })

    // done page
    $('.donePage .closeBtn').on('click', function(e) {
        window.close();
    })


}; // loadEvents

return {
    init: function() {
        // render the captcha's
        contactCustCaptchaWidget = grecaptcha.render('contactCustLaterCaptcha',
                                                     {'sitekey': "6LceMV0UAAAAAOgK4UCDegXgP358BNs-cuePvz_Y",
                                                      'callback': "recaptchaClicked",
                                                      'expired-callback': "recaptchaExpired"})
        prodSelectCaptchaWidget = grecaptcha.render('productSelectCaptcha',
                                                    {'sitekey': "6LceMV0UAAAAAOgK4UCDegXgP358BNs-cuePvz_Y",
                                                     'callback': "recaptchaClicked",
                                                     'expired-callback': "recaptchaExpired"})

        loadEvents()
    },

    recaptchaClicked: function(args) {
        enableDisableSubmit();
    }
}

}
