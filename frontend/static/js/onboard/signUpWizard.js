let signUpWizard = ($signUp, templateData) => {
    // the wizard
    //let $signUpWizard = $('.wizardDiv .signUpWizard');
    let $haveAccount = $('.wizardDiv .haveAccountDiv');

    // pages
    let sPage;      // signup page
    //let cPage;      // contract page
    let ccPage;     // credit card page

    // pages list
    let signupPages = [sPage, /*cPage,*/ ccPage];

    let loadWizard = () => {
        const accountType = templateData.accountType.toLowerCase();
        let revupLogo;

        // Get product type
        const productType = getProductType(accountType);

        // Determine logo to be displayed (green or blue)
        if (productType === 'nonprofit') {
            revupLogo = templateData.blueLogo;
        }
        else {
            revupLogo = templateData.greenLogo;
        }

        const revupLogoEl = '<div class="productLogo"><img class="revupLogo" src="' + revupLogo + '"></div>';

        sPage = signUpPage($signUp, templateData);
        //cPage = contractPage($signUp, templateData);
        ccPage = creditCardPage($signUp, templateData);

        signupPages = [sPage, /*cPage,*/ ccPage];

        let index = 0;
        // see where to start
        let page = templateData.page;
        if (page != '') {
            page = page.toLowerCase();
        }
        switch (page) {
            case 'login':
            case '':
            case 'signup':
                index = 1;
                break;
            case 'contract':
            case 'payment':
                index = 2;
                break;
            /*
            Do when there is a contract page
            case 'contract':
                index = 2;
                break;
            case 'payment':
                index = 3;
                break;
            */
        }; // end switch - templateData.page


        let $sw = $('.wizard', $signUp).revupWizard({
            pages:                  signupPages,
            startingPage:           index,
            bDisplayTitle:          true,           // should the title be displayed
            title:                  revupLogoEl,
            titleHeight:            65,
            cancelBtnText:          '',
            bAutoCloseCancel:       false,
            bNoPrevButton:          true,
            enableDotNavigation:    false,
            bCenterVertical:        true,
            width:                  1000,
            prevBtnText:            'Back',
            nextBtnText:            'Next',
            doneBtnText:            'Submit',
            bAutoCloseDone:         false,
            doneBtnHandler:         function() {
                                        return ccPage.doneSave($sw)
                                    },
            });

        //$signUp.hide();
    } // loadWizard

    const getProductType = (accountType) => {
      if (accountType === 'n') {
          return 'nonprofit'
      }
      else if (accountType === 'p') {
          return 'political';
      }
      else {
          return 'political';
      }
    }

    let loadExistingAccountEvents = () => {
        // have account password field handlers
        $('.wizardDiv .haveAccountDiv .password').revupPasswordField({bShowPasswordStrengthAbove: true,
                                                                      bEnableValidityTest: false,
                                                                      bCutCopyPaste: false});

        $('.wizardDiv .haveAccountDiv .whatToDoDiv')
            .on('click', '.accessAccount', function(e) {
                if ($(this).hasClass('disabled')) {
                    return;
                }

                if ($('.password', $signUp.parent()).val() == '') {
                    $('body').revupMessageBox({
                        headerText:         'Missing Password',
                        msg:                'You need to enter a password before trying to login.',
                        zIndex:             1500,
                    });

                    return;
                }

                let form = $('<form action="' + templateData.loginUrl + '&goto=existing" method="POST">' +
                                 '<input type="hidden" name="csrfmiddlewaretoken" + value="' + templateData.csrfToken + '" />' +
                                 '<input type="hidden" name="email" value="' + templateData.emailAddr + '"/>' +
                                 '<input type="hidden" name="password" value="' + $('.password', $signUp.parent()).val() + '"/>' +
                             '</form>');
                $('body').append(form);
                $(form).submit();
            })

            .on('click', '.newAccount', function(e) {
                // hide the new account page and display the
                $haveAccount.hide();
                $signUp.show();
            })
    } // loadExistingAccountEvents

    return {
        init: function() {
            loadWizard()

            // see where to start
            switch (templateData.page) {
                case 'login':
                    //$signUp.hide();
                    //$haveAccount.show();
                    //break;
                case '':
                case 'signup':
                    $signUp.show();
                    $haveAccount.hide();
                    break;
                /*
                case 'contract':
                    // make the pages before as loaded
                    templateData.signUpLoaded = true;

                    //$('.wizard', $signUp).revupWizard('gotoPage', 'contractPage', true);
                    $signUp.show();
                    $haveAccount.hide();

                    // make the pages before as loaded
                    templateData.signUpLoaded = true;
                    break;
                */
                case 'payment':
                    // make the pages before as loaded
                    templateData.signUpLoaded = true;
                    templateData.contractLoaded = true;

                    //$('.wizard', $signUp).revupWizard('gotoPage', 'creditCardPage', true);
                    $signUp.show();
                    $haveAccount.hide();
                    break;
            } // end switch - templateDate.page

            // existing event handlers
            loadExistingAccountEvents();
        }
    }
}
