let contractPage = ($signUp, templateData) => {
    let userAge = '';
    let atBottom = false;

    let $signUpWizard   = $('.wizard', $signUp);

    return {
        pageName: function() {
            return 'contractPage';
        },

        display: function($wizardBody) {
            let sTmp = [];

            sTmp.push('<div class="contractPageDiv">');
                sTmp.push('<div class="title">RevUp Contract</div>');
                sTmp.push('<div class="subTitle">To accept this contract please scroll to the bottom and enter your signature</div>')
                sTmp.push('<div class="contractDiv">');
                    sTmp.push('<div class="contractOuterDiv">');
                        sTmp.push('<div class="contractInnerDiv">');
                            sTmp.push('<div class="contract">Loading...</div>');
                            sTmp.push('<div class="agree"></div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="fadeOut"></div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            $signUpWizard.revupWizard('changeBtnLabel', 'next', "Agree");
            return {html: sTmp.join(''), disableNext: true};
        }, // display

        getData: function($wizardBody) {
            let sTmp = [];
            sTmp.push('<a class="gotoSignature" href="#signatureDiv">Go to signature</a>');
            sTmp.push('<div class="contractAgreeDiv">');
                sTmp.push('<div class="title">Do you agreee to this contract: </div>')
                sTmp.push('<div class="checkboxDiv">');
                    sTmp.push('<div class="icon icon-check-box-back"></div>');
                    sTmp.push('<div class="icon icon-box-unchecked"></div>');
                    sTmp.push('<div class="icon icon-check"></div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            // add the agree checkbox
            $('.contractPageDiv .contractInnerDiv .agree').html(sTmp.join(''));

            $('.contractOuterDiv', $wizardBody).on('scroll', function(e) {
                if(($(this)[0].scrollHeight - $(this).scrollTop()) <= $(this).outerHeight()) {
                    $('.fadeOut', $wizardBody).hide();
                    atBottom = true;
                }
                else {
                    $('.fadeOut', $wizardBody).show();
                    atBottom = false;
                };
            })

            $signUpWizard.on('click', '.printBtn', function(e) {
                // see if $disabled
                if ($(this).hasClass('disabled')) {
                    return;
                }

                let width = parseInt($(window).width() / 2, 10);
                let height = parseInt($(window).height() / 2, 10);
                let left = parseInt(($(window).width() - width) / 2, 10);
                let top = parseInt(($(window).height() - height) / 2, 10);

                var printPreview = window.open('', 'revupContractPrint', "titlebar=no,statue=no,location=no,width=" + width + ",height="+ height + ",left=" + left + ",top=" + top);
                var printDocument = printPreview.document;
                printDocument.open();
                printDocument.write("<!DOCTYPE html>" +
                    "<head>" +
                        "<title>CSV Errors/Warnings</title>" +
                    "</head>" +
                    "<body style='margin:0;padding:0;min-width:" + (width - 16) + "px'>" +
                        "<html style='min-width:" + (width - 16) + "px'>"+
                            $('.contractDiv .contractInnerDiv').html() +
                        "</html>" +
                    "</body>"
                );
                printDocument.close();

                $(printPreview).on('load', function() {
                    printPreview.window.print();
                })
                .on('afterprint', function () {
                    printPreview.close();
                });
            })

            if (templateData.contract != '') {
                //let parser = new DOMParser();
                //let doc = parser.parseFromString(templateData.contract, 'text/html');
                let htmlCodes = [
                                 {char: "'", esc: '&#39;'},
                                 {char: '"', esc: '&quot;'},
                                 {char: '>', esc: '&gt;'},
                                 {char: '<', esc: '&lt;'},
                                 {char: '&', esc: '&amp;'},
                             ];
                let doc = templateData.contract;
                for (let i = 0; i < htmlCodes.length; i++) {
                    let rx = new RegExp(htmlCodes[i].esc, 'g');
                    doc = doc.replace(rx, htmlCodes[i].char)
                }
                $('.contractPageDiv .contractInnerDiv .contract').html(doc);

                // if reloading a signed contract update signature and data saved
                if (templateData.contractSignature) {
                    $('.sign', $wizardBody).val(templateData.contractSignature)
                                           .prop('readonly', true);
                }

                // check; the agree button
                let $checkbox = $('.contractAgreeDiv', $wizardBody);
                $checkbox.addClass('checked');
                $('.icon-check', $checkbox).show();

                // enable the agree/next button
                $signUpWizard.revupWizard('finishGetData');
                $signUpWizard.revupWizard('enableNext', true);

                return ({enableNext: true});
            }

            $.ajax({
                url: templateData.contractFile,
                dataType: 'text',
                type: 'GET'
            })
            .done(function(r) {
                // load the contact data
                $('.contractPageDiv .contractInnerDiv .contract').html(r);

                // update contract fields
                $('.contractDoc .clientName', $wizardBody).html(templateData.firstName + ' ' + templateData.lastName);
                $('.contractDoc .campaignOrCommittee', $wizardBody).html(templateData.clientType);
                $('.contractDoc .campaignCommittee', $wizardBody).html(templateData.title);
                let d = new Date();
                d = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
                $('.contractDoc .effectDate', $wizardBody).html(d);
                $('.contractDoc .product', $wizardBody).html(templateData.productTitle);
                $('.contractDoc .price', $wizardBody).html('$' + templateData.price + '.00');
                $('.contractDoc .numContacts', $wizardBody).html(templateData.analyzeLimit);

                $('.contractDoc .printedName, .contractDoc .sign', $wizardBody).on('input', function(e) {
                    let $checkbox = $('.contractAgreeDiv', $wizardBody);
                    let $printedName = $('.printedName', $wizardBody);
                    let $signed = $('.sign', $wizardBody);

                    let bEnable = true
                    // if ($printedName.val() == '') {
                    //     bEnable = false;
                    // }
                    if ($signed.val() == '') {
                        bEnable = false;
                    }
                    if (!$checkbox.hasClass('checked')) {
                        bEnable = false;
                    }

                    if (bEnable) {
                        // if at bottom enable the agree/next button
                        if (atBottom) {
                            $signUpWizard.revupWizard('enableNext', true);
                        }
                    }
                    else {
                        // disable the agree/next button
                        $signUpWizard.revupWizard('enableNext', false);
                    }
                })

                $('.contractAgreeDiv', $wizardBody).on('click', function(e) {
                    let $checkbox = $('.contractAgreeDiv', $wizardBody);
                    // let $printedName = $('.printedName', $wizardBody);
                    let $signed = $('.sign', $wizardBody);

                    let bEnable = true
                    // if ($printedName.val() == '') {
                    //     bEnable = false;
                    // }
                    if ($signed.val() == '') {
                        bEnable = false;
                    }

                    if ($checkbox.hasClass('checked')) {
                        // if checked time to un-check
                        $checkbox.removeClass('checked');
                        $('.icon-check', $checkbox).hide();

                        bEnable = false;
                    }
                    else {
                        // if checked time to check
                        $checkbox.addClass('checked');
                        $('.icon-check', $checkbox).show();
                    }

                    if (bEnable) {
                        // if at bottom enable the agree/next button
                        if (atBottom) {
                            $signUpWizard.revupWizard('enableNext', true);
                        }
                    }
                    else {
                        // disable the agree/next button
                        $signUpWizard.revupWizard('enableNext', false);
                    }
                });

                $('.sign', $wizardBody).on('input', function() {
                    let bEnable = true;
                    // see if the agreed is checked
                    let $checkbox = $('.contractAgreeDiv', $wizardBody);
                    if (!$checkbox.hasClass('checked')) {
                        bEnable = false;
                    }

                    // see if signed
                    if ($(this).val() == '') {
                        bEnable = false;
                    }

                    // enable/disable the agree button
                    $signUpWizard.revupWizard('enableNext', bEnable);
                })


                $signUpWizard.revupWizard('finishGetData');
            })
            .fail(function(r) {
                console.error('Unable to load contract: ', r);
            })

            return {showWaitSpinner: true}
        }, // getData

        footerMsg: function() {
            let sTmp = [];

            sTmp.push('<div class="printBtn btnCntl">');
                sTmp.push('<div class="btnLabel">Print</div>')
                sTmp.push('<div class="icon icon-print">')
            sTmp.push('</div>');

            return sTmp.join('');
        }, // footerMsg

        pageHasBeenSaved: function($wizardBody) {
            return templateData.contractLoaded;
        }, // pageHasBeenSaved

        save: function($wizardBody) {
            // if data save don't resave
            if (templateData.contractLoaded) {
                return {showWaitSpinner: false};
            }

            // get the values and the contract
            let $signed = $('.sign', $wizardBody);
            let signed = $signed.val();
            let $contract = $('.contractInnerDiv .contract', $wizardBody);
            let contract = $contract.html();

            // build the data blob
            let data = {};
            data.signature = signed;
            data.contract = encodeURI(contract);

            // post the data
            $.ajax({
                url: templateData.contractUrl,
                data: data,
                type: "POST",
                traditional: true,
                contentType: 'application/x-www-form-urlencoded; charset=utf-8',
                processData: true
            })
            .done(function(r) {
                // enable the next button.
                $signUpWizard.revupWizard('finishSave');

                // turn off the input event handlers
                $wizardBody.off('input');

                // mark page loaded
                templateData.contractLoaded = true;

                // save the updated name
                templateData.contractSignature = signed;
                templateData.contract = contract;
            })
            .fail(function(r) {
                console.error('Signup failed: ', r);

                // clear the wait spinner
                $signUpWizard.revupWizard('clearWait');

                // display the message
                $('body').revupMessageBox({
                    headerText: 'Contract Error',
                    msg: 'Unable to contract: ' + r.responseText,
                })
            })

            return {showWaitSpinner: true}
        }, // save
    }
}; // contactPage
