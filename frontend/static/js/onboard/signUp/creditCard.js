let creditCardPage = ($signUp, templateData) => {
    let r = recurly.configure(templateData.recurlyKey);
    let validCardTypes = ['master', 'visa', 'american_express'];

    let currentCCType = '';

    let expMonth        = '';
    let expMonthIndex   = -1;
    let expYear         = '';
    let expYearIndex    = -1;
    let firstName       = '';
    let lastName        = '';
    let addr1           = '';
    let addr2           = '';
    let city            = '';
    let state           = '';
    let stateIndex      = -1;
    let postalCode      = '';

    let enableDisableSubmit = () => {
        let enableNext = true;

        // credit card
        let cc = $('#ccNumber').val();
        let ccType = recurly.validate.cardType(cc, true);
        cc = cc.replace(/ /g, '');
        let ccLength = 0;
        switch (ccType) {
            case 'master':
            case 'visa':
                ccLength = 16;
                break;
            case 'american_express':
                ccLength = 15;
                break;
        }
        let ccLen = cc.replace(/ /g, '');
        ccLen = ccLen.length;
        if (cc == '' || !recurly.validate.cardNumber(cc) || validCardTypes.indexOf(ccType) == -1 || ccLen != ccLength) {
            enableNext = false;
        }

        // expiration
        if (expYearIndex == -1 || expMonthIndex == -1) {
            enableNext = false;
        }

        // cvv
        let cvv = $('#ccCVV').val();
        let cvvLen = (ccType == 'visa' || ccType == 'master') ? 3 : 4;
        if (cvv.length != cvvLen) {
            enableNext = false;
        }

        // first name
        firstName = $('#ccFirstName').val();
        if (firstName == '') {
            enableNext = false;
        }

        // last name
        lastName = $('#ccLastName').val();
        if (lastName == '') {
            enableNext = false;
        }

        // address 1
        addr1 = $('#ccAddress1').val();
        if (addr1 == '') {
            enableNext = false;
        }

        // address 2
        addr2 = $('#ccAddress2').val();

        // city
        city = $('#ccCity').val();
        if (city == '') {
            enableNext = false;
        }

        // state
        if (stateIndex == -1) {
            enableNext = false;
        }

        // zip code / postal code
        postalCode = $('#ccPostalCode').val();
        if (postalCode == '' || postalCode.length != 5) {
            enableNext = false;
        }

        $signUp.revupWizard('enableNext', enableNext);
    } // enableDisableSubmit

    let displayErrors = ($ccPopupDlg, code, fields = [], message = '') => {

        let bannerMsg = '';

        if (code == 'validation') {
            bannerMsg = 'The following ';
            if (fields.length > 1) {
                bannerMsg += 'fields are invalid: ';
            }
            else {
                bannerMsg += 'field is invalid: ';
            }
        }
        else if (code == 'invalid-parameter') {
            bannerMsg = 'The following ';
            if (fields.length > 1) {
                bannerMsg += 'fields have invalid value: ';
            }
            else {
                bannerMsg += 'field has a invalid value: ';
            }
        }
        else if (code == 'api-error') {
            bannerMsg = 'A api error occured when trying to charge your credit card';
            console.error('Api error on with credit card: ' + message + ', field: ', fields)
        }

        // clear current errors
        clearErrors();

        // make the field readable
        let sTmp = [];
        for (let i = 0; i < fields.length; i++) {
            switch (fields[i]) {
                case 'number':
                    $('.creditCardInfoDiv .ccNum').addClass('has-error');
                    sTmp.push('Credit Card Number');
                    break;
                case 'month':
                    $('.ccExpMonth', $ccPopupDlg).addClass('has-error');
                    $('.ccExpYear', $ccPopupDlg).addClass('has-error');
                    sTmp.push('Expiration - Month');
                    break;
                case 'year':
                    $('.ccExpMonth', $ccPopupDlg).addClass('has-error');
                    $('.ccExpYear', $ccPopupDlg).addClass('has-error');
                    sTmp.push('Expiration - Year');
                    break;
                case 'first_name':
                    sTmp.push('First Name');
                    break;
                case 'last_name':
                    sTmp.push('Last Name');
                    break;
                case 'address1':
                    sTmp.push('Address');
                    break;
                case 'city':
                    sTmp.push("City");
                    break;
                case 'state':
                    sTmp.push('State');
                    break;
                case 'postal_code':
                    sTmp.push('Zipcode');
                    break;
                case 'cvv':
                    sTmp.push('CVV');
                    break;
                default:
                    sTmp.push(val);
                    break;
            }; // end switch - val
        };


        // load the message
        $('.wizard .errorDiv .msg').html(bannerMsg + sTmp.join(', '));

        // display the message
        $('.wizard .errorDiv', $signUp).fadeIn('slow');
    }; // displayErrors

    let clearErrors = () => {
        $('.creditCardInfoDiv .ccNum').removeClass('has-error');
        $('.creditCardInfoDiv .ccExpMonth').removeClass('has-error');
        $('.creditCardInfoDiv .ccExpYear').removeClass('has-error');
        $('.creditCardInfoDiv .ccCVV').removeClass('has-error');
        $('.creditCardInfoDiv .address').removeClass('has-error');
        $('.creditCardInfoDiv .address2').removeClass('has-error');
        $('.creditCardInfoDiv .city').removeClass('has-error');
        $('.creditCardInfoDiv .state').removeClass('has-error');
        $('.creditCardInfoDiv .zip').removeClass('has-error');
    }; // clearErrors

    let displayPostError = (errResult) => {
        console.warn('Credit Card Error: ', errResult);

        let e = errResult.split(':');
        let code = 'unknown';
        let msg = errResult;
        let msgType = '';
        if (e.length == 2) {
            code = $.trim(e[0]);
            msg = $.trim(e[1]);
        }

        if (msg.startsWith('account.base')) {
            msgType = 'account.base';
            msg = $.trim(msg.substring(13));
        }
        else if (msg.startsWith('gateway_timeout: account.base')) {
            msgType = 'gateway_timeout';
            msg = $.trim(msg.substring(30));
        }
        msg = $.trim(msg.replace('billing_info.base', ''));

        // clear current errors
        clearErrors();

        switch(code) {
            case 'declined':
                $('.creditCardInfoDiv .ccNum').addClass('has-error');
                $('.creditCardInfoDiv .ccCVV').addClass('has-error');
                break;
            case 'unknown':
                break;
            case 'fraud_address':
                $('.creditCardInfoDiv .address').addClass('has-error');
                if ($('.creditCardDiv #ccAddress2').val() != '') {
                    $('.creditCardInfoDiv .address2').addClass('has-error');
                }
                $('.creditCardInfoDiv .city').addClass('has-error');
                $('.creditCardInfoDiv .state').addClass('has-error');
                $('.creditCardInfoDiv .zip').addClass('has-error');
                break;
            case 'fraud_gateway':
                break;
            case 'invalid':
                msg = 'Invalid Credit Card. Please re-enter you card number';
                $('.creditCardInfoDiv .ccNum').addClass('has-error');
                break;
            case 'insufficient_funds':
                break;
            case 'fraud_gateway':
                break;
            case 'fraud_ip_address':
                break;
            case 'call_issuer':
                break;
            case 'declined_bad':
                // cvv
                $('.creditCardInfoDiv .ccCVV').addClass('has-error');
                msg = 'Your Credit Card Number does not match the CVV code';
                break;
            case 'invalid_data':
            case 'declined_expiration_date':
                $('.creditCardInfoDiv .ccExpMonth').addClass('has-error');
                $('.creditCardInfoDiv .ccExpYear').addClass('has-error');
                break;
            case 'duplicate_transaction':
                break;
            case 'card_type_not_accepted':
                break;
            case 'declined_saveable':
                break;
        } // end switch -> code

        // load and display the error message
        $('.wizard .errorDiv .msg').html(msg);
        $('.wizard .errorDiv', $signUp).fadeIn('slow');
    }; // displayPostError


    let display = function($wizardBody) {
        //reset the currentCCType;
        currentCCType = '';

        let sTmp = [];

        sTmp.push('<div class="creditCardDiv">');
            sTmp.push('<div class="title">Credit Card and Purchase Information</div>');
            sTmp.push('<div class="errorDiv">');
                sTmp.push('<div class="msgDiv">');
                    sTmp.push('<div class="icon icon-stop"></div>')
                    sTmp.push('<div class="msg">Warning Warning Will Robinson</div>');
                sTmp.push('</div>');
            sTmp.push('</div>')
            sTmp.push('<div class="creditCardInfoDiv">');
                sTmp.push('<div class="leftSide">');
                    sTmp.push('<form name="creditCardForm" id="creditCardForm" action="" method="post" autocomplete="on">');
                        //sTmp.push('<div type="hidden" name="csrfToken" value="' + templateData.csrfToken + '"/>');
                        sTmp.push('<div class="line">')
                            sTmp.push('<div class="entry  ccNum">');
                                sTmp.push('<div class="title reqTxt">Credit Card Number</div>');
                                sTmp.push('<input type="text" id="ccNumber" data-recurly="number" placeholder="Card Number" x-autocompletetype="cc-number"/>');
                            sTmp.push('</div>');
                            sTmp.push('<div class="entry  ccImage">');
                                sTmp.push('<div class="title"></div>');
                                sTmp.push('<div class="imgDiv"></div>');
                            sTmp.push('</div>');
                            sTmp.push('<div class="entry ccExpMonth expDate">');
                                sTmp.push('<div class="title reqTxt">Expiration</div>');
                                sTmp.push('<input type="hidden" id="ccMonth" data-recurly="month" value="' + expMonth + '"/>');
                                sTmp.push('<div class="monthDropDown"></div>')
                            sTmp.push('</div>');
                            sTmp.push('<div class="entry ccExpYear expDate">');
                                sTmp.push('<div class="title"></div>');
                                sTmp.push('<input type="hidden" id="ccYear" data-recurly="year" value="' + expYear + '"/>');
                                sTmp.push('<div class="yearDropDown"></div>')
                            sTmp.push('</div>');
                            sTmp.push('<div class="entry ccCVV">');
                                sTmp.push('<div class="title reqTxt">CVV</div>');
                                sTmp.push('<input type="text" id="ccCVV" data-recurly="cvv" placeholder="CVV" maxlength="4"/>');
                            sTmp.push('</div>');
                        sTmp.push('</div>')

                        sTmp.push('<div class="line">')
                            sTmp.push('<div class="entry firstLastName">');
                                sTmp.push('<div class="title reqTxt">First Name</div>');
                                sTmp.push('<input type="text" id="ccFirstName" data-recurly="first_name" placeholder="First Name" value="' + firstName + '"/>');
                            sTmp.push('</div>');
                            sTmp.push('<div class="entry firstLastName">');
                                sTmp.push('<div class="title reqTxt">Last Name</div>');
                                sTmp.push('<input type="text" id="ccLastName" data-recurly="last_name" placeholder="Last Name" value="' + lastName + '"/>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');

                        sTmp.push('<div class="line">')
                            sTmp.push('<div class="entry address addr">');
                                sTmp.push('<div class="title reqTxt">Address 1</div>');
                                sTmp.push('<input type="text" id="ccAddress1" data-recurly="address1" placeholder="Address" value="' + addr1 + '"/>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');

                        sTmp.push('<div class="line">')
                            sTmp.push('<div class="entry address2 addr">');
                                sTmp.push('<div class="title">Address 2</div>');
                                sTmp.push('<input type="text" id="ccAddress2" data-recurly="address2" placeholder="Address" value="' + addr2 + '"/>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');

                        sTmp.push('<div class="line">')
                            sTmp.push('<div class="entry  city addr">');
                                sTmp.push('<div class="title reqTxt">City</div>');
                                sTmp.push('<input type="text" id="ccCity" data-recurly="city" placeholder="City" value="' + city + '"/>');
                            sTmp.push('</div>');
                            sTmp.push('<div class="entry state addr">');
                                sTmp.push('<div class="title reqTxt">State</div>');
                                sTmp.push('<input type="hidden" id="ccState" data-recurly="state"  value="' + state + '"/>');
                                sTmp.push('<div class="stateDropDown"></div>')
                            sTmp.push('</div>');
                            sTmp.push('<div class="entry zip addr">');
                                sTmp.push('<div class="title reqTxt">Zip Code</div>');
                                sTmp.push('<input type="text" id="ccPostalCode" data-recurly="postal_code" placeholder="Zipcode" maxlength="5" value="' + postalCode + '"/>');
                            sTmp.push('</div>');
                        sTmp.push('</div>')
                        sTmp.push('<input type="hidden" data-recurly="country" value="US">');
                        sTmp.push('<input type="hidden" name="recurly-token" data-recurly="token">');
                    sTmp.push('</form>');
                    sTmp.push('<div class="reqFields">');
                        sTmp.push('<div class="reqTxt">Required Fields</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');

                sTmp.push('<div class="rightSide">');
                    sTmp.push('<div class="title">Purchase Details</div>')
                    sTmp.push('<div class="purchaseDetailsDiv">');
                        sTmp.push('<div class="purchaseDetails"></div>');
                        sTmp.push('<hr>');
                        sTmp.push('<div class="totalDiv">');
                            sTmp.push('<div class="left">Total today</div>');
                            sTmp.push('<div class="right">$XXX.XX</div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>')
                    sTmp.push('<div class="purchaseNotes">');
                        sTmp.push('<div class="left">*</div>');
                        sTmp.push('<div class="right">');
                            sTmp.push('Your first month\'s charge will be a prorated amount based on the date of your onboarding. ');
                            sTmp.push('You will subsequently be charged the full  monthly amount on the first of each month.')
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        // turn off generic event handlers
        $wizardBody.off('input');

        return {html: sTmp.join(''), disableNext: true};
    }; // display

    // This will be displayed in the event this account is configured to not
    // require payment during the onboarding
    let skipPaymentDisplay = function($wizardBody) {
        return {html: '<h3>Payment has been disabled for this account. Click "Submit" to proceed.</h3>',
                disableNext: false};
    };


    let getData = function($wizardBody) {
        let year = templateData.currentYear;
        let yearLst = [];
        for (let i = 0; i <= 20; i++) {
            yearLst.push(year + '');
            year++;
        }

        $('.monthDropDown', $wizardBody)
            .revupDropDownField({
                valueStartIndex: expMonthIndex,
                sortList: false,
                ifStartIndexNeg1Msg: 'Month',
                value: [{value: "01", displayValue: "01 - January"},
                        {value: "02", displayValue: "02 - Feburary"},
                        {value: "03", displayValue: "03 - March"},
                        {value: "04", displayValue: "04 - April"},
                        {value: "05", displayValue: "05 - May"},
                        {value: "06", displayValue: "06 - June"},
                        {value: "07", displayValue: "07 - July"},
                        {value: "08", displayValue: "08 - August"},
                        {value: "09", displayValue: "09 - September"},
                        {value: "10", displayValue: "10 - October"},
                        {value: "11", displayValue: "11 - November"},
                        {value: "12", displayValue: "12 - December"},
                    ],
                overlayZIndex: 51,
                })
            .on('revup.dropDownSelected', function(e) {
                // save the value
                $('#ccMonth').val(e.dropDownValue);
                expMonth = e.dropDownValue;
                expMonthIndex = e.dropDownIndex;

                // check if submit can be enabled
                enableDisableSubmit();
                $('.creditCardInfoDiv .entry.expDate').removeClass('has-error');
            });

        $('.yearDropDown', $wizardBody)
            .revupDropDownField({
                valueStartIndex: expYearIndex,
                sortList: false,
                ifStartIndexNeg1Msg: 'Year',
                value: yearLst,
                overlayZIndex: 51,
            })
            .on('revup.dropDownSelected', function(e) {
                // save the value
                $('#ccYear').val(e.dropDownValue);
                expYear = e.dropDownValue;
                expYearIndex = e.dropDownIndex;

                // check if submit can be enabled
                enableDisableSubmit();
                $('.creditCardInfoDiv .entry.expDate').removeClass('has-error');
            });

        $('.stateDropDown', $wizardBody)
            .revupDropDownField({
                valueStartIndex: stateIndex,
                sortList: false,
                ifStartIndexNeg1Msg: 'State',
                value: revupConstants.dropDownStates,
                overlayZIndex: 51,
            })
            .on('revup.dropDownSelected', function(e) {
                // save the value
                $('#ccState').val(e.dropDownValue);
                state = e.dropDownValue;
                stateIndex = e.dropDownIndex;

                // check if submit can be enabled
                enableDisableSubmit();
                $('.creditCardInfoDiv .entry.addr').removeClass('has-error');
            });

        // credit card number events
        let $creditCardImg = $('.creditCardInfoDiv .ccImage .imgDiv');
        $("#ccNumber")
            .on('keydown', function(e) {
                // make sure a number
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 40)) {
                    return;
                }

                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            })
            .on('input', function(e) {
                // display the credit card image
                var cc = $(this).val();
                var ccType = recurly.validate.cardType(cc, true);
                currentCCType = ccType;
                switch (ccType) {
                    case 'visa':
                        $creditCardImg.removeClass('masterCardGlyph')
                                      .removeClass('americanExpressGlyph')
                                      .addClass('visaGlyph');

                        break;
                    case 'master':
                        $creditCardImg.addClass('masterCardGlyph')
                                      .removeClass('americanExpressGlyph')
                                      .removeClass('visaGlyph');

                        break;
                    case 'american_express':
                        $creditCardImg.removeClass('masterCardGlyph')
                                      .addClass('americanExpressGlyph')
                                      .removeClass('visaGlyph');

                        break;
                    default:
                        currentCCType = ''
                        $creditCardImg.removeClass('masterCardGlyph')
                                      .removeClass('americanExpressGlyph')
                                      .removeClass('visaGlyph');

                        break;
                } // end switch - ccType

                // clear any error until the next blur
                $('.creditCardInfoDiv .entry.ccNum').removeClass('has-error');
                $('.creditCardInfoDiv .entry.ccCVV').removeClass('has-error');

                // format the cc number
                var v = cc.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
                if (ccType == 'american_express') {
                    v = v.replace(/ /g, '');
                    if (v.length <= 4) {
                        cc = v;
                    }
                    else if (v.length <= 10) {
                        let p1 = v.substring(0, 4);
                        let p2 = v.substring(4, 10);
                        cc = p1 + ' '+ p2;
                    }
                    else{
                        let p1 = v.substring(0, 4);
                        let p2 = v.substring(4, 10);
                        let p3 = v.substring(10, 15);
                        cc = p1 + ' '+ p2 + ' ' + p3;
                    }

                    $(this).val(cc);

                    // check if submit can be enabled
                    enableDisableSubmit();

                    return;
                }
                var matches = v.match(/\d{4,16}/g);
                var match = matches && matches[0] || '';
                var parts = []
                for (i=0, len=match.length; i<len; i+=4) {
                    parts.push(match.substring(i, i+4));
                }
                if (parts.length) {
                    $(this).val(parts.join(' '));
                } else {
                    $(this).val(cc);
                }

                // check if submit can be enabled
                enableDisableSubmit();
            })
            .on('blur', function(e) {
                var cc = $(this).val();

                // see if blank
                if (cc == '') {
                    return;
                }

                // see if a valid card
                let ccType = recurly.validate.cardType(cc, true);
                cc = cc.replace(/ /g, '');
                let ccLength = 0;
                switch (ccType) {
                    case 'master':
                    case 'visa':
                        ccLength = 16;
                        break;
                    case 'american_express':
                        ccLength = 15;
                        break;
                }
                if (!recurly.validate.cardNumber(cc) || cc.length != ccLength) {
                    $(this).parent().addClass('has-error');
                    $('.ccType', $(this).parent()).text('');
                    return;
                }
                else {
                    // see if a supported card
                    var bSupportedCard = false;
                    for (var i = 0; i < validCardTypes.length; i++) {
                        if (ccType === validCardTypes[i]) {
                            bSupportedCard = true;
                            break;
                        }
                    }

                    if (bSupportedCard) {
                        // valid card
                        $('.creditCardInfoDiv .entry.ccNum').removeClass('has-error');
                        }
                    else {
                        $('.creditCardInfoDiv .entry.ccNum').addClass('has-error');
                    }
                }
            })

        // postal code (zipcode) events
        $('#ccPostalCode')
            .on('keydown', function(e) {
                // make sure a number
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 40)) {
                    return;
                }

                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();

                    return;
                }

                let zipcode = $(this).val();
                if (zipcode.length > 5) {
                    e.preventDefault();

                    return;
                }
            })
            .on('input', function(e) {
                // check if submit can be enabled
                enableDisableSubmit();
                $('.creditCardInfoDiv .entry.addr').removeClass('has-error');
            })

        // cvv events
        $('#ccCVV')
            .on('keydown', function(e) {
                // make sure a number
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 40)) {
                    return;
                }

                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();

                    return;
                }

                let cvv = $(this).val();
                let cvvLen = (currentCCType == 'visa' || currentCCType == 'master') ? 3 : 4;
                if (cvv.length >= cvvLen) {
                    e.preventDefault();

                    return;
                }
            })
            .on('input', function(e) {
                // check if submit can be enabled
                enableDisableSubmit();
            })

        $('#ccFirstName, #ccLastName').on('input', function(e) {
            enableDisableSubmit();
        })

        $(' #ccAddress1, #ccAddress2, #ccCity').on('input', function(e) {
            enableDisableSubmit();
            $('.creditCardInfoDiv .entry.addr').removeClass('has-error');
        })

        // right side messaging
        let $rightSide = $('.rightSide .purchaseDetailsDiv', $wizardBody);

        // message
        let msg  = templateData.durationInMonths + ' months of RevUp ' + templateData.product + ' at';
            msg += ' $' + revupUtils.commify(templateData.price) + '.00 per month, billed monthly.'
            //msg += 'for a total cost of $' + (templateData.price * templateData.durationInMonths) + '.00'

        let product = templateData.product.toLowerCase();
        if (product == 'premium' || product == 'select') {
            msg += '<br><br>One time onboarding charge of $' + revupUtils.commify(templateData.setUpFee) + '.00';
        }
        $('.purchaseDetails', $rightSide).html(msg);

        // amount to be charged
        if (product == 'premium' || product == 'select') {
            msg = '$' + revupUtils.commify(templateData.setUpFee) + '.00';
        }
        else {
            msg = '$' + revupUtils.commify(templateData.price) + '.00';
        }
        $('.totalDiv .right', $rightSide).html(msg);

        // set the focus to the cc field
        $('#ccNumber').select();
    }; // getData

    return {
        pageName: function() {
            return 'creditCardPage';
        },

        // If this is a skip payment flow, we don't show the CC form
        display: templateData.skip_payment? skipPaymentDisplay : display,
        getData: templateData.skip_payment? function(){} : getData,

        footerMsg: function($wizardBody) {
            return '';
        }, // footerMsg

        doneSave: function($wizardBody) {
            let rVal = true;

            // Helper function to keep code DRY
            let submitPaymentForm = function () {
                $.ajax({
                    url: templateData.paymentUrl,
                    data: $('#creditCardForm').serialize(),
                    type: "POST",
                })
                .done(function (r) {
                    r = $.parseJSON(r);
                    if (r.redirect) {
                        window.location.href = r.redirect;
                    } else if (r.error) {
                        // clear the wait spinner
                        $wizardBody.revupWizard('clearWait');
                        $wizardBody.revupWizard('enablePrev');

                        // display the error message
                        console.error('credit card error: ', r);
                        displayPostError(r.error);
                    }
                    return;
                })
                .fail(function (r) {
                    // clear the wait spinner
                    $wizardBody.revupWizard('clearWait');
                    $wizardBody.revupWizard('enablePrev');

                    // display the error message
                    console.error('credit card error: ', r);
                    displayPostError(r.responseText);
                    return;
                })
            };

            if (templateData.skip_payment) {
                // This client was configured to not require payment. This
                // bypasses Recurly
                submitPaymentForm();
            } else {
                recurly.token($('#creditCardForm'), function (err, token) {
                    if (err) {
                        $signUp.revupWizard('finishGetData');
                        $signUp.revupWizard('enableNext', false);

                        // display the errors
                        displayErrors($wizardBody, err.code, err.fields, err.message);

                        rVal = false;
                    } else {
                        // recurly.js has filled in the 'token' field, so now we can submit the
                        // form to your server; alternatively, you can access token.id and do
                        // any processing you wish
                        // post the data
                        submitPaymentForm();
                    }
                });
            }

            return rVal;
        } // save
    }
}; // creditCardPage
