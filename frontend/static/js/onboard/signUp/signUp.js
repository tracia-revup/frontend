let signUpPage = ($signUp, templateData) => {
    // value of the fields
    let firstName   = templateData.firstName;
    let lastName    =  templateData.lastName;
    let passwd      = '';
    let vPasswd     = '';

    let $signUpWizard = $('.wizard', $signUp);

    let userName = '';
    return {
        pageName: function() {
            return 'Sign Up Page';
        },

        display: function($wizardBody) {
            let sTmp = [];

            sTmp.push('<div class="signUpDiv">');
                sTmp.push('<div class="title">Sign Up</div>');
                sTmp.push('<div class="errorMsgDiv">');
                    sTmp.push('<div class="msgDiv">');
                        sTmp.push('<div class="icon icon-stop"></div>');
                        sTmp.push('<div class="errorMsg"></div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');

                if (templateData.page == 'login') {
                    sTmp.push('<div class="welcomeMsgDiv">');
                        sTmp.push('<strong>Hello <span class="email">' + templateData.emailAddr + '</span>. We have detected that you have an existing account. Please input your existing password and then select the "Next" button below to continue.</strong>');
                    sTmp.push('</div>');
                }

                sTmp.push('<div class="fieldsDiv">');
                    sTmp.push('<form autocomplete="on">');
                        sTmp.push('<div class="line">');
                            sTmp.push('<div class="entry">');
                                sTmp.push('<div class="title">Email Address - This is your user name</div>');
                                sTmp.push('<div class="value userEmail">' + templateData.emailAddr + '</div>');
                            sTmp.push('</div>')
                            sTmp.push('<div class="entry">');
                                sTmp.push('<div class="title">Campaign/Company/Originization</div>');
                                sTmp.push('<div class="value campaignName">' + templateData.title + '</div>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');

                        sTmp.push('<div class="line">');
                            sTmp.push('<div class="entry">');
                            if (templateData.page == 'login') {
                                sTmp.push('<div class="title">First Name</div>');
                            }
                            else {
                                sTmp.push('<div class="title reqTxt">First Name</div>');
                            }
                                if (templateData.signUpLoaded | templateData.page == 'login') {
                                    sTmp.push('<div class="value firstName">' + firstName + '</div>');
                                }
                                else {
                                    sTmp.push('<input type="text" name="fname" data-lpignore="true" class="firstName userNamePart" maxlength="64" placeholder="First Name" value="' + firstName + '"/>');
                                }
                            sTmp.push('</div>')
                            sTmp.push('<div class="entry">');
                            if (templateData.page == 'login') {
                                sTmp.push('<div class="title">Last Name</div>');
                            }
                            else {
                                sTmp.push('<div class="title reqTxt">Last Name</div>');
                            }
                                if (templateData.signUpLoaded | templateData.page == 'login') {
                                    sTmp.push('<div class="value lastName">' + lastName + '</div>');
                                }
                                else {
                                    sTmp.push('<input type="text" name="lname" data-lpignore="true" class="lastName userNamePart" maxlength="150" placeholder="Last Name"  value="' + lastName + '"/>');
                                }
                            sTmp.push('</div>');
                        sTmp.push('</div>');

                        let readOnly = '';
                        if (templateData.signUpLoaded) {
                            readOnly = ' readOnly';
                            passwd = 'password';
                            vPasswd = 'password'
                        }
                        if (templateData.page == 'login') {
                            sTmp.push('<div class="line">');
                                sTmp.push('<div class="entry oneField">');
                                    sTmp.push('<div class="title reqTxt">Password</div>');
                                    sTmp.push('<input type="text" class="passwordPart password" placeholder="Password" value="' + passwd + '"' + readOnly + '/>');
                                sTmp.push('</div>')
                            sTmp.push('</div>');
                        }
                        else {
                            sTmp.push('<div class="line">');
                                sTmp.push('<div class="entry">');
                                    sTmp.push('<div class="title reqTxt">Password</div>');
                                    sTmp.push('<input type="text" class="passwordPart password" placeholder="Password" value="' + passwd + '"' + readOnly + '/>');
                                sTmp.push('</div>')
                                sTmp.push('<div class="entry">');
                                    sTmp.push('<div class="title reqTxt">Re-enter Password</div>');
                                    sTmp.push('<input type="text" class="passwordPart varifyPassword" placeholder="Re-enter Password"  value="' + vPasswd + '"' + readOnly + '/>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');
                        }

                        /*
                        sTmp.push('<div class="line">');
                            sTmp.push('<div class="msg">');
                                sTmp.push('Your password must be  at least 8 characters in length and include at least one ');
                                sTmp.push('capitol letter, one lower case letter, one number and one special character.');
                            sTmp.push('</div>');
                        sTmp.push('</div>');
                        */
                    sTmp.push('</form>');
                    sTmp.push('<div class="reqFields">');
                    if (templateData.page == 'login') {
                        sTmp.push('<div class="reqTxt">Required Field</div>');
                    }
                    else {
                        sTmp.push('<div class="reqTxt">Required Fields</div>');
                    }
                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');


            // set the label of the next button
            $('.wizard', $signUp).revupWizard('changeBtnLabel', 'next', "Next");

            // see if the next button should be disabled
            let bDisableNext = false;
            if (firstName == '' || lastName == '' ||  passwd == '' || vPasswd == '' || passwd != vPasswd) {
                bDisableNext = true;
            }

            return {html: sTmp.join(''), disableNext: bDisableNext};
        }, // display

        getData: function($wizardBody) {
            $wizardBody.on('input', 'input', function() {
                let bEnable = true;

                if (templateData.page == 'login') {
                    let p = $('.signUpDiv .password').val();
                    if (p.length < 8) {
                        bEnable = false;
                    }
                }
                else {
                    firstName = $('.firstName', $signUpWizard).val();
                    if (firstName == '') {
                        bEnable = false;
                    }

                    lastName = $('.lastName', $signUpWizard).val();
                    if (lastName == '') {
                        bEnable = false;
                    }

                    passwd = $('.password', $signUpWizard).val();
                    if (passwd == '') {
                        bEnable = false;
                    }

                    vPasswd = $('.varifyPassword', $signUpWizard).val();
                    if (vPasswd == '') {
                        bEnable = false;
                    }
                    if (passwd != vPasswd) {
                        bEnable = false;
                    }
                }

                if (!$('.signUpDiv .password').revupPasswordField("valid")) {
                    bEnable = false;
                }

                if (bEnable) {
                    $signUpWizard.revupWizard('enableNext', true);
                }
                else {
                    $signUpWizard.revupWizard('enableNext', false);
                }
            });

            if (templateData.page == 'login') {
                $('.signUpDiv .password', $wizardBody).revupPasswordField({bShowPasswordStrengthAbove: true,
                                                                           bEnableShowStrength: false,
                                                                           bCutCopyPaste: false});
            }
            else {
                $('.signUpDiv .password', $wizardBody).revupPasswordField({bShowPasswordStrengthAbove: true,
                                                                           bShowRules: true,
                                                                           bCutCopyPaste: false});
                $('.signUpDiv .varifyPassword', $wizardBody).revupPasswordField({bEnableShowStrength: false,
                                                                                 bShowPasswordStrengthAbove: true,
                                                                                 bCutCopyPaste: false});
            }

            // display a error popup if there is one
            if (templateData.loginError) {
                $('.errorMsgDiv .errorMsg').html(templateData.loginError);
                $('.errorMsgDiv', $wizardBody).show();
                $('.entry.login', $wizardBody).addClass('has-error');

            }
            else {
                $('.errorMsgDiv', $wizardBody).hide();
                $('.entry.login', $wizardBody).removeClass('has-error');
            }


            // attach the autocomplete - do a context switch to allow the fields to load
            setTimeout(function() {
                $('form').attr('autocomplete', 'on');
                $('.firstName', $signUpWizard).attr('autocomplete', 'on');
            }, 1);

            // set the focus
            if (templateData.page == 'login') {
                $('.signUpDiv .login .revupPasswordInput', $wizardBody).select();
            }
            else {
                $('.signUpDiv .firstName').select();
            }

            return {showWaitSpinner: false}
        }, // getData

        okToMove: function($wizardBody) {
            // data save the ok
            if (templateData.signUpLoaded) {
                return true;
            }

            let missingField = [];
            if (templateData.page != 'login') {
                if (firstName == '') {
                    missingField.push('First Name')
                }

                if (lastName == '') {
                    missingField.push('Last Name')
                }

                if (passwd == '') {
                    missingField.push('Password')
                }

                if (vPasswd == '') {
                    missingField.push('Re-Enter Password')
                }
                if (passwd != vPasswd && passwd != '' && vPasswd != '') {
                    missingField.push('"Password" and "Re-Enter Password" don\'t match')
                }
            }

            if (!$('.signUpDiv .password').revupPasswordField("valid")) {
                missingField.push('Not a vaild password');
            }

            if (missingField.length > 0) {
                let foll = missingField.length == 1 ? 'field is' : 'fields are'
                return {okToGo: false, errorMsg: 'The folloing ' + foll + ' missing: ' + missingField.join(', ')};
            }

            return true;
        },  // okToMove

        pageHasBeenSaved: function($wizardBody) {
            return templateData.signUpLoaded;
        },

        okToFinish: function($wizardBody) {
            let ok = true;

            if (templateData.page != 'login') {
                if (firstName == '') {
                    ok = false;
                }

                if (lastName == '') {
                    ok = false;
                }

                if (passwd == '') {
                    ok = false;
                }

                if (vPasswd == '') {
                    ok = false;
                }

                if (passwd != vPasswd && passwd != '' && vPasswd != '') {
                    ok = false;
                }
            }

            if (!$('.signUpDiv .password').revupPasswordField("valid")) {
                ok = false;
            }

            return ok;
        },

        save: function($wizardBody) {
            // if data save don't resave
            if (templateData.signUpLoaded) {
                return {showWaitSpinner: false};
            }

            // create the data for the signup post
            let data = {};
            data.first_name = firstName;
            data.last_name = lastName;
            data.email = templateData.emailAddr;
            data.password = passwd;

            if (templateData.page == 'login') {
                let form = $('<form action="' + templateData.loginUrl + '&goto=existing" method="POST">' +
                                 '<input type="hidden" name="csrfmiddlewaretoken" + value="' + templateData.csrfToken + '" />' +
                                 '<input type="hidden" name="email" value="' + templateData.emailAddr + '"/>' +
                                 '<input type="hidden" name="password" value="' + $('.password', $signUpWizard).val() + '"/>' +
                             '</form>');
                $('body').append(form);
                $(form).submit();
            }
            else {
                let form = $('<form action="' + templateData.loginUrl + '&path=signup" method="POST">' +
                                 '<input type="hidden" name="csrfmiddlewaretoken" + value="' + templateData.csrfToken + '" />' +
                                 '<input type="hidden" name="email" value="' + templateData.emailAddr + '"/>' +
                                 '<input type="hidden" name="first_name" value="' + firstName + '"/>' +
                                 '<input type="hidden" name="last_name" value="' + lastName + '"/>' +
                                 '<input type="hidden" name="password" value="' + $('.password', $signUpWizard).val() + '"/>' +
                             '</form>');
                $('body').append(form);
                $(form).submit();
                /*
                $.ajax({
                    url: templateData.signUpUrl,
                    data: data,
                    type: "POST",
                    traditional: true,
                    contentType: 'application/x-www-form-urlencoded; charset=utf-8',
                    processData: true
                })
                .done(function(r) {
                    // enable the next button.
                    $signUpWizard.revupWizard('finishSave');

                    // turn off the input event handlers
                    $wizardBody.off('input');

                    // mark page loaded
                    templateData.signUpLoaded = true;

                    // save the updated name
                    templateData.firstName = firstName;
                    templateData.lasName = lastName;

                    location.reload();
                })
                .fail(function(r) {
                    console.error('Signup failed: ', r);

                    // clear the wait spinner
                    $signUpWizard.revupWizard('clearWait');

                    // display the message
                    $('body').revupMessageBox({
                        headerText: 'Signup Error',
                        msg: 'Unable to signup: ' + r.status,
                    })
                })
                */
            }

            return {showWaitSpinner: true}
        }, // save
    }
}; // signUpPage
