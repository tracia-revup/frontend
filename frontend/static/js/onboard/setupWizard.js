let setupWizard = function($configWizard, $finishedScreen, templateData, questionSet) {
    const accountId = templateData.accountId;
    const wizardID  = 'wizard' + accountId + 'index';

    const loadWizard = () => {
        const accountType = templateData.accountType.toLowerCase();
        const clientType  = templateData.clientType.toLowerCase();
        const page        = templateData.page;
        let index         = 1;
        let revupLogo;
        let pages;

        // Get product type
        const productType = getProductType(accountType,clientType);

        // Determine logo to be displayed (green or blue)
        if (productType === 'nonprofit') {
            revupLogo = templateData.blueLogo;
        }
        else {
            revupLogo = templateData.greenLogo;
        }

        // Set the title(logo)
        const revupLogoEl = '<div class="productLogo"><img class="revupLogo" src="' + revupLogo + '"></div>';

        // Setup pages based on the product type
        if (productType === 'nonprofit') {
            let lnonProfitInfo = nonProfitInfo($configWizard, templateData, questionSet);
            let lsimilarOrgs = similarOrgs($configWizard, templateData, questionSet);

            pages = [lnonProfitInfo, lsimilarOrgs];
        }
        else {
            if (productType === 'political candidate') {
                // let lTermsOfService = termsOfService($configWizard, templateData);
                let lCampaignInfo = campaignInfo($configWizard, templateData, questionSet);
                let lEducation = education($configWizard, templateData);
                let lIssues = issues($configWizard, templateData);
                let lSimilarToMine = similarToMine($configWizard, templateData);
                // let lGivingData = givingData($configWizard, templateData);

                pages = [/*lTermsOfService,*/ lCampaignInfo, lEducation, lIssues, lSimilarToMine/*, lGivingData*/];
            }
            else if (productType === 'political committee')  {
                // let lTermsOfService = termsOfService($configWizard, templateData);
                let lPacCommitteeInfo = pacCommitteeInfo($configWizard, templateData, questionSet);
                let lIssues = issues($configWizard, templateData);
                let lSimilarToMine = similarToMine($configWizard, templateData);
                // let lGivingData = givingData($configWizard, templateData);

                pages = [/*lTermsOfService,*/ lPacCommitteeInfo, lIssues, lSimilarToMine/*, lGivingData*/];
            }
        }

        // Determine the page to be displayed upon load
        // ToDo: The controller can pass either 'settings' or 'splash' as a value for page.
        // Determine if this is still needed. If not, remove logic from controller, key from templateData, and check here.
        if (page == 'settings') {
            const wizardProgress = getWizardProgress();

            if (wizardProgress) {
                if (wizardProgress.currentStep === 'complete') {
                    $finishedScreen.show();
                }
                else {
                    const indexToNum = parseInt(wizardProgress.currentStep, 10);

                    if (indexToNum) {
                        index = indexToNum + 1;
                    }

                    $configWizard.show();
                }
            }
            else {
                $configWizard.show();
            }
        }

        const $cw = $('.wizard', $configWizard).revupWizard({
            pages:                  pages,
            startingPage:           index,
            bDisplayTitle:          true,           // should the title be displayed
            title:                  revupLogoEl,
            titleHeight:            65,
            enableIndexing:         true,
            cancelBtnText:          '',
            bAutoCloseCancel:       false,
            width:                  1000,
            bCenterVertical:        true,
            prevBtnText:            'Back',
            nextBtnText:            'Next',
            doneBtnText:            'Done',
            bAutoCloseDone:         false,
            middleBtnDisplay:       false,
            middleBtnText:          'Upload',
            doneBtnHandler:         doneBtnHandler,
            middleBtnHandler:       middleBtnHandler,
            getWizardProgress:      getWizardProgress,
            saveWizardProgress:     saveWizardProgress
        });
    } // loadWizard

    const getProductType = (accountType,clientType) => {
        if (accountType === 'n') {
            return 'nonprofit'
        }
        else if (accountType === 'p') {
            if (clientType.indexOf('candidate') !== -1) {
                return 'political candidate';
            }
            else {
                return 'political committee';
            }
        }
        else {
            return 'political candidate';
        }
    }

    const getWizardProgress = () => {
        const name = wizardID;
        const data = localStorage.getItem(name);
        const storageObj = JSON.parse(data);

        return storageObj;
    };

    const saveWizardProgress = (currentStep, overallProgress) => {
        const name  = wizardID;
        const step = currentStep.toString();
        const progress = overallProgress.toString();

        localStorage.setItem(name, JSON.stringify({
            currentStep: step,
            overallProgress: progress
        }));
    };

    const markWizardAsComplete = () => {
        saveWizardProgress('complete', 'complete');
    }

    const markWizardAsSubmitted = () => {
        const name  = 'wizard' + accountId + 'submitted';
        const value = true;

        localStorage.setItem(name, value);
    };

    const showFinishedSplash = () => {
        $configWizard.hide();
        $finishedScreen.show();
        markWizardAsComplete();
    };

    const hideFinishedSplash = () => {
        $configWizard.show();
        $finishedScreen.hide();
    };

    const doneBtnHandler = () => {
        // Tell the backend the wizard is completed, and if successful,
        // hide the wizard, show the finished screen, and persist the completed
        // status to localstorage
        showFinishedSplash();
        const wizardSubmitted = localStorage.getItem('wizard' + accountId + 'submitted');

        if (wizardSubmitted) {
            markWizardAsComplete();
        }
        else {
            $.ajax({
                url: '?path=thank_you',
                type: "POST"
            })
            .done(function() {
                markWizardAsComplete();
                markWizardAsSubmitted();
            }).fail(function(){
                hideFinishedSplash();
            })
        }
    }

    const middleBtnHandler = () => {
        console.log('time to upload');
    }

    $('.setupDoneDiv .makeChangesBtn').on('click', function(e) {
        $finishedScreen.hide();
        $configWizard.show();
        saveWizardProgress(0, 'complete');
    })

    return {
        init: function() {
            loadWizard();
        }
    };
}
