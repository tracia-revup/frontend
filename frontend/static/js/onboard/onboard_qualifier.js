var qualifierSpecs = (function () {

    // global selectors
    let $selectedGridName = $();

    /*
     *  Event handlers
     */

    $('.bodyGrid .revupBtnDefault.selectBtn').on('click', function(e) {
        var $this = $(this).parent().parent();
        if ($this.hasClass('selected')) {
            return;
        }
        else {
            var $selectedGrid = $('.grid.selected');
            $('.icon-check', $selectedGrid).hide();
            $('.revupBtnDefault.selectBtn', $selectedGrid).show();
            $selectedGrid.removeClass('selected');
            $this.addClass('selected');
            $(this).hide();
            $('.icon-check', $this).show();

            var triggerSelect = $this.prop('class').replace('grid', '').replace('selected', '').trim();
            $selectedGridName.trigger({
                type: 'revup.onboard.selectProduct',
                selectedProduct: triggerSelect},
            )
        }
    })

    $('.bodyGrid .enSearch').on('click', function(e) {
        var bSimilar = true;
        if ($(this).hasClass('opponents')) {
            bSimilar = false;
        }
        entitySearch.load($(this), bSimilar, fieldData, eData, questionSetDataId);
    })
     // Event handlers

    let disabledGrid = (disabledGridArray, selectColumn) =>
    {
        for (var i = 0; i < disabledGridArray.length; i++) {
            var $column = $('.grid.' + disabledGridArray[i]);
            $column.addClass('disabled');
            $('.bodyGrid .revupBtnDefault.selectBtn', $column).prop('disabled', true);
        }
        if (disabledGridArray.indexOf(selectColumn) != -1 ){
            return;
        }
        else {
            var $preSelected = $('.grid.' + selectColumn);
            $preSelected.addClass('selected');
            $('.revupBtnDefault.selectBtn', $preSelected).hide();
            $('.icon-check', $preSelected).show();
        }
    } // disabledGrid

    let fillGrids = (templateData) =>
    {
        // match the incoming data with each of the grids
        for (var keys in templateData){
            if (keys == 'productLite') {
                var $currentGrid = $('.liteGrid.gridBody');
            }
            else if (keys == 'productPlus'){
                var $currentGrid = $('.plusGrid.gridBody');
            }
            else if (keys == 'productPremium'){
                var $currentGrid = $('.premGrid.gridBody');
            }
            else if (keys == 'productSelect') {
                var $currentGrid = $('.selectGrid.gridBody');
            }
            else {
                continue;
                console.error('unknown grid column');
            }

            $('.gridContract', $currentGrid).html(templateData[keys]['contractTerms'] + ' months with auto-renew');
            $('.gridHelp', $currentGrid).html(templateData[keys]['helpSupport']);
            if (templateData[keys]['linkedInImport']) {
                $('.sourceIcon.linkedInImport', $currentGrid).show().css('display', 'inline-block');
            }
            if (templateData[keys]['gmailImport']) {
                $('.sourceIcon.gmailImport', $currentGrid).show().css('display', 'inline-block');
            }
            if (templateData[keys]['outlookImport']) {
                $('.sourceIcon.outlookImport', $currentGrid).show().css('display', 'inline-block');
            }
            if (templateData[keys]['vcardImport']) {
                $('.sourceIcon.vcardImport', $currentGrid).show().css('display', 'inline-block');
            }
            if (templateData[keys]['cvsImport']) {
                $('.sourceIcon.cvsImport', $currentGrid).show().css('display', 'inline-block');
            }
            if (templateData[keys]['mobileImport']) {
                $('.sourceIcon.mobileImport', $currentGrid).show().css('display', 'inline-block');
            }
            if (templateData[keys]['accessToPrem']) {
                $('.gridAccess .dot', $currentGrid).show();
            }
            if (templateData[keys]['accessToPrem']) {
                $('.gridAnalyze .dot', $currentGrid).show();
            }
            if (templateData[keys]['prospectSupport']) {
                $('.gridProspects .dot', $currentGrid).show();
            }
            if (templateData[keys]['csvExportSupport']) {
                $('.gridCSV .dot', $currentGrid).show();
            }
            $('.gridOnboard', $currentGrid).html(templateData[keys]['onboardingSupport']);

            if (templateData[keys]['mobileSupport']) {
                $('.gridMobile .dot', $currentGrid).show();
            }
            if (templateData[keys]['callTimeSupport']) {
                $('.gridCalltime .dot', $currentGrid).show();
            }
        }
    } // fillGrids

    return {
        load: function(disabledGridArray, selectColumn, $eventParent, templateData) {
            $selectedGridName = $eventParent;
            if (disabledGridArray.length > 0 && selectColumn) {
                disabledGrid(disabledGridArray, selectColumn);
            }
            else if (disabledGridArray.length > 0 && !selectColumn) {
                disabledGrid(disabledGridArray);
            }
            fillGrids(templateData);
        } // load
    }
}());
