let configWizard = ($config, cType) => {
    var $configWizard = $();

    let loadWizard = () => {
        var pages = [];
        if (cType == 'campaign') {
            pages = [termsOfService, campaignInfo, education, issues, similarToMine, givingData];
        }
        else {
            pages = [termsOfService, pacCommitteeInfo, issues, similarToMine, givingData]
        }

        $configWizard = $('.wizard', $config).revupWizard({
            pages:                  pages,
            bDisplayTitle:          true,           // should the title be displayed
            title:                  '<div style="padding-top:20px;text-align:center"><img src="/static/img/new-revup-logo.png"></div>',
            titleHeight:            65,
            cancelBtnText:          '',
            bAutoCloseCancel:       false,
            height:                 590,
            width:                  1000,
            prevBtnText:            'Back',
            nextBtnText:            'Next',
            doneBtnText:            'Add',
            bAutoCloseDone:         false,
            middleButtonDisplay:    false,
            middleButtonLabel:      'Upload',
            middleButtonHandler:    function() {
                                    },
            doneBtnHandler:         function() {
                                    },
            });
    } // loadWizard

    return {
        init: function() {
            loadWizard()
        }
    }
}
