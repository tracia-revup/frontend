var csvValidation = (function () {

let _isConfigFileLoaded      = false;    // is the config file loaded?
let configFile              = '';       // the name of the config file
let _hasAnalysisRun         = false;    // has the analysis run

// config file
let headerFields            = [];       // array header fields
let requiredFieldsLst       = [];       // array of required fields
let recommendedFieldsLst    = [];       // array of recommended fields
let fieldDefs               = [];       // associative array, indexed by field name, of behavior of field
let correctiveAction        = [];       // array of corrective act

// results from analyzing a CSV File
let columnOrder             = [];       // position of the columns in the CSV file
let invalidFields           = [];       // array of fields not in the config header
let requiredFields          = [];       // array of required fields, each entry is an object with the name and column index
let missingRequiredFields   = [];       // array of missining required fields
let recommendedFields       = [];       // array of recommended fields, each entry is an object with the name and column index
let missingRecommendedFields = [];      // array of the missing recommended fields

// errors and warnings
//  each entry is an object with the following info
//      1) lineNum - line number
//      2) colName - column name
//      3) colIndex - column index
//      4) id - error or warning id
//      5) msg - default message
//      6) eData - extra data
//
//  error and warning id's
//    1 - 99 errors
//        1 - value is blank
//        2 - value is missing
//       10 - value is not required length
//       11 - value is to short
//       12 - value is to long
//       13 - line blank
//    100 - 199 formating errors
//      100 - Invalid zipcode
//      101 - Invalid phone number
//      102 - Invalid email address
//      103 - Invalid number
//      104 - Invalid Date = YYYY-MM-DD
//    500 - 599 system errors
//      500 - config file not loaded
let errors                  = [];       // array of errors in csv file, each entry is an object with

//  CONFIGURATION FILE DEFINITIONS
//
// column definition - all the supported columns - Line 1
// required columns - all the columns that must be in the users CSV file - Line 2
// recommended columns - all the columns that are recommended to be in the CSV file - Line 3
// column fields definitions - only columns that override the defaults need to be listed - Start Line 4
// only colunms that have non default values need to be listed
// Format of each of the columns definitions
//  1) column name
//  2) type of data (default string)
//  3) maxLen (default, unlimited)
//  4) minLen (default, unlimited)
//  5) requiredLen (default, unlimited)
//  5) requirements: 'recommended', 'required', 'optional' the default
//  6) canBeBlank: true/false (default - true)
//  7) warnIfMissing: true/false (default - false)
//  8) correctiveActionId: The corrective action id, -1 or undefined no message

let parseCSV = (str) => {
    var arr = [];
    var quote = false;  // true means we're inside a quoted field

    // iterate over each character, keep track of current row and column (of the returned array)
    for (var row = col = c = 0; c < str.length; c++) {
        var cc = str[c], nc = str[c+1];        // current character, next character
        arr[row] = arr[row] || [];             // create a new row if necessary
        arr[row][col] = arr[row][col] || '';   // create a new column (start with empty string) if necessary

        // If the current character is a quotation mark, and we're inside a
        // quoted field, and the next character is also a quotation mark,
        // add a quotation mark to the current column and skip the next character
        if (cc == '"' && quote && nc == '"') {
            arr[row][col] += cc;
            ++c;

            continue;
        }

        // If it's just one quotation mark, begin/end quoted field
        if (cc == '"') {
            quote = !quote;
            continue;
        }

        // If it's a comma and we're not in a quoted field, move on to the next column
        if (cc == ',' && !quote) {
            ++col;

            continue;
        }

        // If it's a newline (CRLF) and we're not in a quoted field, skip the next character
        // and move on to the next row and move to column 0 of that new row
        if (cc == '\r' && nc == '\n' && !quote) {
            ++row;
            col = 0;
            ++c;

            continue;
        }

        // If it's a newline (LF or CR) and we're not in a quoted field,
        // move on to the next row and move to column 0 of that new row
        if (cc == '\n' && !quote) {
            ++row;
            col = 0;

            continue;
        }
        if (cc == '\r' && !quote) {
            ++row;
            col = 0;
            continue;
        }

        // Otherwise, append the current character to the current column
        arr[row][col] += cc;
    }

    return arr[0];
} // parseCSV

let _loadConfigFile = (fileName) => {
    // reset
    _hasAnalysisRun = false;
    _isConfigFileLoaded = false;
    invalidFields = [];
    requiredFields = [];
    missingRequiredFields = [];
    recommendedFields = [];
    missingRecommendedFields = [];
    correctiveAction = [];

    requiredFieldsLst = []
    recommendedFieldsLst = [];

    if (!fileName || fileName == '') {
        console.error('CSV Config File Missing')

        _isConfigFileLoaded = false;

        return;
    }

    // save the config file
    configFile = fileName

    // load the static sample file for get the correct columns
    $.ajax({
        url: fileName, // '/static/csvFormat/contacts.csv',
        dataType: 'text',
        type: 'GET'
    })
    .done(function(r) {
        // the first line is all the vaild fields
        // the second line are the required fields
        // the third line are the recommended fields
        let defLines = r.split(/\r\n|\n/);
        if (defLines.length >= 1) {
            // get the header field and convert to lower case
            headerFields = parseCSV(defLines[0]);
            for (let i = 0; i < headerFields.length; i++) {
                if (headerFields[i] != '') {
                    headerFields[i] = $.trim(headerFields[i].toLowerCase());
                }
            }

            // get the list of reqired fields
            if (defLines.length >= 2) {
                let tmp = parseCSV(defLines[1]);
                for (let i = 0; i < tmp.length; i++) {
                    requiredFieldsLst.push($.trim(tmp[i]))
                }
            }

            // get the list of recommeded fields
            if (defLines.length >= 3) {
                let tmp = parseCSV(defLines[2]);
                for (let i = 0; i < tmp.length; i++) {
                    recommendedFieldsLst.push($.trim(tmp[i]))
                }
            }

            // get the field definitions
            // Format
            //  1) column name
            //  2) type of data (default string)
            //  3) maxLen (default, unlimited)
            //  4) minLen (default, unlimited)
            //  5) requiredLen (default, unlimited)
            //  5) requirements: 'recommended', 'required', 'optional' the default
            //  6) canBeBlank: true/false (default - true)
            //  7) warnIfMissing: true/false (default - false)
            //  8) correctiveActionId: The corrective action id, -1 or undefined no message
            // default definition of the field
            for (let i = 0; i < headerFields.length; i++) {
                fieldDefs[headerFields[i]] = {
                            type: 'string',
                            maxLen: -1,
                            minLen: -1,
                            requiredLen: undefined,
                            requirements: 'optional',
                            canBeBlank: true,
                            warnIfMissing: false,
                            correctiveActionMsg: -1,
                            };
            }

            for (let i = 3; i < defLines.length; i++) {
                // if starts with a # comment line
                if (defLines[i].length == 0 || defLines[i][0] == '#') {
                    continue;
                }

                let dLine = parseCSV(defLines[i]);
                let colField = $.trim(dLine[0]);

                // see if a corrective  action definition
                if (colField.toLowerCase() == ':correctiveaction:') {
                    let id = parseInt(dLine[1], 10);
                    correctiveAction[id] = $.trim(dLine[2]);

                    continue;
                }

                // see if a required field
                for (let f = 0; f < requiredFieldsLst; f++) {
                    if (colField == requiredFieldsLst[f]) {
                        fieldDefs[colField].requirements = 'required'
                    }
                }

                // see if a recommended field
                for (let f = 0; f < recommendedFieldsLst; f++) {
                    if (colField == recommendedFieldsLst[f]) {
                        fieldDefs[colField].requirements = 'recommended'
                    }
                }

                let dLineLen = dLine.length;
                if (dLineLen >= 2) {
                    // type of column data
                    fieldDefs[colField].type = $.trim(dLine[1]);
                }
                if (dLineLen >= 3) {
                    // max length
                    fieldDefs[colField].maxLen = parseInt($.trim(dLine[2]), 10);
                }
                if (dLineLen >= 4) {
                    // min length
                    fieldDefs[colField].minLen = parseInt($.trim(dLine[3]), 10);
                }
                if (dLineLen >= 5) {
                    // required length
                    if (dLine[4] == -1) {
                        fieldDefs[colField].requiredLen = undefined;
                    }
                    else {
                        fieldDefs[colField].requiredLen = dLine[4].split('|');
                        for (let l = 0; l < fieldDefs[colField].requiredLen.length; l++) {
                            fieldDefs[colField].requiredLen[l] = parseInt($.trim(fieldDefs[colField].requiredLen[l]), 10);
                        }
                    }
                }
                if (dLineLen >= 6) {
                    // requirements
                    fieldDefs[colField].requirements = $.trim(dLine[5]);
                }
                if (dLineLen >= 7) {
                    // can be blank
                    fieldDefs[colField].canBeBlank = $.trim(dLine[6]) == 'true';
                }
                if (dLineLen >= 8) {
                    // display a warning if blank
                    fieldDefs[colField].warnIfMissing = $.trim(dLine[7]) == 'true';
                }
                if (dLineLen >= 9) {
                    // corrective action id
                    fieldDefs[colField].correctiveActionId = parseInt($.trim(dLine[8]), 10);
                }
            }

            // set flag that config file is loaded
            _isConfigFileLoaded = true;
            configFile = fileName
        }
    })
    .fail(function(r) {
        console.error('Unable to load CSV Config File: ', fileName);

        _isConfigFileLoaded = false;
        configFile = '';
    });
}; // _loadConfigFile

let _analyze = (fileResults, max, $loadingStatus) => {
        let startTime = new Date().getTime();

        // make sure the config file is loaded
        if (!_isConfigFileLoaded) {
            console.error('CSV Config File is not loaded');

            return {
                errors: [{
                    errorNum: 500,
                    errorMsg: 'CSV Config File is not loaded',
                }],
            };
        }

        _hasAnalysisRun = false;
        invalidFields = [];
        requiredFields = [];
        missingRequiredFields = [];
        recommendedFields = [];
        missingRecommendedFields = [];
        errors = [];

        // split the csv into lines
        var allTextLines = fileResults.split(/\r\n|\n/);
        var lines = [];
        for (let i = 0; i < allTextLines.length; i++) {
            let l = allTextLines[i];

            // if blank skip
            if (l == undefined || l == '') {
                continue;
            }

            // if the first character is a # ignore the list - comment
            if (l[0] == '#') {
                continue;
            }

            lines.push(parseCSV(l));
        }

        // check the header line - always the first line
        let numUsedFields = 0;
        let sTmpRequired = [];
        let sTmpRecommended = [];
        let sTmp = [];
        let headerFields = lines[0];
        for (let i = 0; i < headerFields.length; i++) {
            let field = $.trim(headerFields[i].toLowerCase());

            // array of the position of the columns
            columnOrder[i] = field;

            if (fieldDefs[field]) {
                numUsedFields++;

                if (fieldDefs[field].requirements == 'required') {
                    requiredFields.push({name: field, columnIndex: i});
                }
                else if (fieldDefs[field].requirements == 'recommended') {
                    recommendedFields.push({name: field, columnIndex: i});
                }

            }
            else {
                // keep list of invalid fields
                invalidFields.push({name: field, columnIndex: i + 1});
            }
        }

        // the number of columns
        // $('.numFields', $wizardBody).html(numUsedFields);

        // required & recommend fields in the csv file fields
        // $('.requiredLstBox', $wizardBody).html(sTmpRequired.join(''));
        // $('.recommendedLstBox', $wizardBody).html(sTmpRecommended.join(''));

        // missing required and recommeded fields
        for (let i = 0; i < requiredFieldsLst.length; i++) {
            let bFound = false;
            for (let j = 0; j < columnOrder.length; j++) {
                if (columnOrder[j] == requiredFieldsLst[i]) {
                    bFound = true;

                    break;
                }
            }

            if (!bFound) {
                missingRequiredFields.push(requiredFieldsLst[i]);
            }
        }

        for (let i = 0; i < recommendedFieldsLst.length; i++) {
            let bFound = false;
            for (let j = 0; j < columnOrder.length; j++) {
                if (columnOrder[j] == recommendedFieldsLst[i]) {
                    bFound = true;

                    break;
                }
            }

            if (!bFound) {
                missingRecommendedFields.push(recommendedFieldsLst[i]);
            }
        }

        // check the other row for missing values
        //     start at 1, line 0 is the header
        let numLines = lines.length;
        let bMaxErrorHit = false;
        let numLinesAnalyzied = 0;
        for (let iLine = 1; iLine < numLines; iLine++) {
            // see if the maximum of error hit
            if (max != -1 && errors.length > max) {
                bMaxErrorHit = true;
                numLinesAnalyzied = iLine;

                break;
            }

            if (lines[iLine] == undefined) {
                continue;
            }

            // if the line starts with a # is is a comment
            if ($.trim(lines[iLine][0]) == '#') {
                continue;
            }

            // get the line
            let line = lines[iLine];

            // get the length of the line
            let lLen = line.length;

            // see if a blank line;
            bIsBlankLine = true;
            for(let i = 0; i < lLen; i++) {
                if (line[i] != '') {
                    bIsBlankLine = false;

                    break;
                }
            }
            if (bIsBlankLine) {
                errors.push({
                    lineNum: iLine + 1,
                    id: 13,
                    msg: 'Line is blank',
                    correctiveMsg: correctiveAction[13],
                    requirements: 'warning',
                });

                continue;
            }

            // update status every 100 lines
            if ($loadingStatus && ((iLine) % 100 == 0)) {
                $loadingStatus.html(iLine + ' lines of CSV file anaylzied' );
            }

            // get the column entries for a line
            for (let i = 0; i < lLen; i++) {
                // get the column name
                let colName = columnOrder[i];
                if (colName == undefined || colName == '') {
                    continue;
                }

                // get the rules for that column
                let colRules = fieldDefs[colName];
                if (colRules == undefined) {
                    continue;
                }

                // column type
                let colType = 'optional'
                for (let c = 0; c < requiredFields.length; c++) {
                    if (requiredFields[c].columnIndex == i) {
                        colType = 'required';

                        break;
                    }
                }
                for (let c = 0; c < recommendedFields.length; c++) {
                    if (recommendedFields[c].columnIndex == i) {
                        colType = 'recommended';

                        break;
                    }
                }

                // test to see if the value is blank and it can blank it is ok
                let isBlank = false;
                if (line[i] == '' && !colRules.canBeBlank) {
                    isBlank = true;

                    errors.push({
                        lineNum: iLine + 1,
                        colName: colName,
                        colType: colType,
                        colIndex: i + 1,
                        id: 1,
                        msg: 'value is blank',
                        correctiveMsg: correctiveAction[1],
                        requirements: colRules.requirements,
                    });
                }
                if (line[i] == '' && colRules.warnIfMissing && !isBlank) {
                    errors.push({
                        lineNum: iLine + 1,
                        colName: colName,
                        colType: colType,
                        colIndex: i + 1,
                        id: 2,
                        msg: 'value is missing',
                        correctiveMsg: correctiveAction[2],
                        requirements: colRules.requirements,
                    });
                }

                // check the length of the value
                var bFieldValueSizeOk = true;
                if (colRules.requiredLen && colRules.requiredLen.length > 0 && line[i] != '') {
                    let bLenError = true;
                    let fieldLen = line[i].length;
                    for (let l = 0; l < colRules.requiredLen.length; l++) {
                        if (fieldLen == colRules.requiredLen[l]) {
                            bLenError = false;
                            break;
                        }
                    }

                    if (bLenError) {
                        errors.push({
                            lineNum: iLine + 1,
                            colName: colName,
                            colType: colType,
                            colIndex: i + 1,
                            id: 10,
                            msg: 'value is not the required length - ',
                            eData: colRules.requiredLen,
                            requirements: colRules.requirements,
                            correctiveMsg: correctiveAction[10],
                        });

                        bFieldValueSizeOk = false;
                    }
                }
                else if (colRules.minLen && colRules.minLen != '' && colRules.minLen != '-1' && line[i].length < colRules.minLen) {
                    errors.push({
                        lineNum: iLine + 1,
                        colName: colName,
                        colType: colType,
                        colIndex: i + 1,
                        id: 11,
                        msg: 'value is to short - ',
                        correctiveMsg: correctiveAction[11],
                        eData: colRules.minLen,
                        requirements: colRules.requirements,
                    });

                    bFieldValueSizeOk = false;
                }
                else if (colRules.maxLen && colRules.maxLen != '' && colRules.maxLen != '-1' && line[i].length > colRules.maxLen) {
                    errors.push({
                        lineNum: iLine + 1,
                        colName: colName,
                        colType: colType,
                        colIndex: i + 1,
                        id: 12,
                        msg: 'value is too long - ',
                        correctiveMsg: colName.toLowerCase() == 'contact id'? correctiveAction[12] + " " + parseInt(colRules.maxLen + 1) + ' characters. Otherwise, RevUp will clip your id down to 512 characters in length.' : correctiveAction[12] + " " + colRules.maxLen,
                        eData: colRules.maxLen ,
                        requirements: colRules.requirements,
                    });

                    bFieldValueSizeOk = false;
                }

                // check the format - only if the size is ok
                if (!bFieldValueSizeOk) {
                    // skip until the next line
                    continue;
                }

                let val = line[i];
                let regex = '';
                let t = colRules.type;
                if (val != '') {
                    switch (t.toLowerCase()) {
                        case 'string':
                            break;
                        case 'zipcode':
                            regex = /^\d{5}(?:[-\s]\d{4})?$/;
                            if (val.search(regex) == -1) {
                                errors.push({
                                    lineNum: iLine + 1,
                                    colName: colName,
                                    colType: colType,
                                    colIndex: i + 1,
                                    id: 100,
                                    msg: 'Invalid zipcode',
                                    correctiveMsg: correctiveAction[100],
                                    requirements: colRules.requirements,
                                });
                            }
                            break;
                        case 'phonenumber':
                            regex = /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/i;
                            if (val.search(regex) == -1) {
                                errors.push({
                                    lineNum: iLine + 1,
                                    colName: colName,
                                    colType: colType,
                                    colIndex: i + 1,
                                    id: 101,
                                    msg: 'Invalid phone number',
                                    correctiveMsg: correctiveAction[101],
                                    requirements: colRules.requirements,
                                });
                            }
                            break;
                        case 'emailaddress':
                            regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                            if (val.search(regex) == -1) {
                                errors.push({
                                    lineNum: iLine + 1,
                                    colName: colName,
                                    colType: colType,
                                    colIndex: i + 1,
                                    id: 102,
                                    msg: 'Invalid email address',
                                    correctiveMsg: correctiveAction[102],
                                    requirements: colRules.requirements,
                                });
                            }
                            break;
                        case 'integer':
                            regex = /^[\d]*$/
                            if (val.search(regex) == -1) {
                                errors.push({
                                    lineNum: iLine + 1,
                                    colName: colName,
                                    colType: colType,
                                    colIndex: i + 1,
                                    id: 103,
                                    msg: 'Invalid number',
                                    correctiveMsg: correctiveAction[103],
                                    requirements: colRules.requirements,
                                });
                            }
                            break;
                        case 'date-ymd':
                            regex = /^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/
                            if (val.search(regex) == -1) {
                                errors.push({
                                    lineNum: iLine + 1,
                                    colName: colName,
                                    colType: colType,
                                    colIndex: i + 1,
                                    id: 104,
                                    msg: 'Invalid date - YYYY-MM-DD',
                                    correctiveMsg: correctiveAction[104],
                                    requirements: colRules.requirements,
                                });
                            }
                            break;
                    } // end switch - colRules.type
                }
            }
        }

        _hasAnalysisRun = true;

        let finishTime = new Date().getTime();
        let analysisTime = finishTime - startTime;

        let r = new Object();
        r.numLines = lines.length;
        r.numColumns = headerFields.length;
        r.errors = errors;
        r.extraColumns = invalidFields;
        r.missingRequired = missingRequiredFields;
        r.missingRecommended = missingRecommendedFields;
        r.analysisTime = analysisTime;
        r.bMaxErrorsHit = bMaxErrorHit;
        r.lastLineAnalyzied = numLinesAnalyzied;

        return r;
} // _analyze

return {
    loadConfigFile: function(configFile)
    {
        return _loadConfigFile(configFile)
    }, // loadConfigFile

    analyze: function(loadedFileResults, $loadingStatus = undefined)
    {
        return _analyze(loadedFileResults, $loadingStatus);
    }, // analyze

    isConfigFileLoaded: function()
    {
        return _isConfigFileLoaded
    }, // isConfigFileLoaded

    hasAnalysisRun: function()
    {
        return _hasAnalysisRun;
    }, // hasAnalysisRun

    getConfigFile: function()
    {
        return configFile;
    }, // getConfigFile

    getInvalidFields: function()
    {
        if (!_hasAnalysisRun) {
            return [];
        }

        return invalidFields
    }, // getInvalidFields

    getRequiredFields: function ()
    {
        if (!_hasAnalysisRun) {
            return [];
        }

        return requiredFields;
    }, // getRequiredFields

    getRecommendedFields: function()
    {
        if (!_hasAnalysisRun) {
            return [];
        }

        return recommendedFields;
    }, //getRecommendFields


    getMissingRequiredFields: function ()
    {
        if (!_hasAnalysisRun) {
            return [];
        }

        return missingRequiredFields;
    }, // getMissingRequiredFields

    getMissingRecommendedFields: function()
    {
        if (!_hasAnalysisRun) {
            return [];
        }

        return missingRecommendedFields;
    }, //getMissingRecommendFields

    columnToExcel: function(colIndex)
    {
        let num = colIndex
        let sTmp = [];
        while (num != 0) {
            let rem = num % 26;
            num = ~~(num / 26);

            sTmp.push(String.fromCharCode((rem - 1) + 65))
        }

        let tmp = [];
        let j = 0;
        for (let i = sTmp.length; i >= 0; i--) {
            tmp[j++] = sTmp[i]
        }

        return tmp.join('');
    },
}

})();
