/*global $, jQuery, document*/
function toggleExtraOptions(animate) {
    if ($("input.officeCategory").val() === "Local") {
        if (animate) {
            $("#id_district").parent().fadeIn();
        }
        else {
            $("#id_district").parent().show();
        }
    }
    else {
        $("#id_district").parent().fadeOut();
    }
    if ($.inArray($("input.office").val(), ["House", "State Senate", "Assemblyman"]) !== -1) {
        if (animate) {
            $("#id_district_number").parent().fadeIn();
        }
        else {
            $("#id_district_number").parent().show();
        }
    }
    else {
        $("#id_district_number").parent().fadeOut();
    }
}

function makeYearPicker(element) {
    var initial_value = element.val();
    element.after('<div class="dateinputIcon"><div class="icon icon-calendar"></div></div>');
    /*jslint unparam: true*/
    element.map(function (undefined, input) {
        $(input).parent().find("button").click(function () {
            $(input).datepicker('show');
        });
    });
    /*jslint unparam: false*/
    /* By default, if you type a date in the datepicker and press enter,
     * the date is deleted. Fix this by intercepting the enter key and
     * changing it to the tab key, which validates the field normally. */
    element.keydown(function (event) {
        if (event.which === 13) {
            var tabEvent = jQuery.Event("keydown");
            tabEvent.which = 9;
            tabEvent.keyCode = 9;
            $(this).trigger(tabEvent);
            return false;
        }
    }).datepicker({
        autoclose: true,
        format: "yyyy",
        minViewMode: "years",
        todayHighlight: true,
        viewMode: "years",
    });
    if (initial_value) {
        element.datepicker("update", initial_value);
    }
    else {
        element.datepicker("update", String(new Date().getFullYear() + 1));
    }
}

$(document).ready(function() {
    makeYearPicker($("#id_election"));

    // get selectors
    var $opponent = $('.entryDiv.opponentDiv');
    var $oppCount = $('.count', $opponent);
    var $oppLst = $('.lstDiv .lst', $opponent);
    var $oppSet = $('.opponentSet', $opponent);
    var $oppName = $('.name', $opponent);
    var $searchBtn = $('.searchNameBtn', $opponent);
    var $searchFor = $('.searchName', $opponent);
    var $deleteBtn = $('.deleteNameBtn', $opponent);
    var $waitSpinner = $('.searchNameDiv .waitCursor', $opponent);
    var $oppLstDiv = $('.searchDiv .lstDiv', $opponent);
    var oppNameTypeTimer    = null;     // delay timer for handling fast typing
    var oppNameTypeTimeout  = 250;      // amount op delay
    var bFetchingOppData    = false;    // true while in the middle of a request
    var startSearchLen      = 2;        // number of characted need to start an auto-search
    var bLastCharBackspace  = false;    // true if last character typed is a backspace (keycode 8)

    // delete opponent button
    $deleteBtn.off('click')
              .on('click', function () {
                  $oppSet.val('');
                  $oppName.text('No Opponent selected at this time');

                  // clear the edit field
                  $searchFor.val('');

                  // make sure the list is closed
                  $oppLstDiv.slideUp('fast')
                  $oppCount.hide();
              });

    // hover delgate handler
    $oppLst.off('mouseenter mouseleave', '.opponentLstEntry')
           .on('mouseenter mouseleave', '.opponentLstEntry', function(e) {
               var $entry = $(this);

               if (e.type === 'mouseenter') {
                   $entry.addClass('over')
               }
               else if (e.type === 'mouseleave') {
                   $entry.removeClass('over')
               }
           })
           .off('click', '.opponentLstEntry')
           .on('click', '.opponentLstEntry', function(e) {
               var $opp = $(this);
               var name = $opp.text();
               var candId = $opp.data('cand-id');

               // set the new value both in the name and hidden field
               $oppSet.val(candId);
               $oppName.text('Your Opponent is: ' + name);

               // trigger and event
               $opponent.trigger('revup.autocompleteValueSelected', [{value: name, data: candId}]);

               // set the search edit field
               $searchFor.val(name);

               // close the list
               $oppLstDiv.slideUp('fast')
               $oppCount.hide();
           });

    $searchBtn.off('click')
              .on('click', function(e) {
                var currentVal = $searchFor.val();

                // if the currentVal is blank then don't make request
                if (currentVal === '') {
                    $oppLst.html('');
                    $oppCount.html('&nbsp');

                    return;
                }

                // if in the middle of requesting data don't make another request
                if (bFetchingOppData) {
                    return;
                }

                // go and fetch the data
                getLstOfOpponents(currentVal);
              });

    // get fetch list of opponents
    function getLstOfOpponents(currentVal)
    {
        // display the spinner
        $waitSpinner.show();

        // set flag reqesting data
        bFetchingOppData = true;

        // trigger event starting
        $opponent.trigger('revup.autocompleteSearchStart', {searchVal: currentVal});

        $.ajax({
            data: {'last_name': currentVal},
            //timeout: 10000,
            type: "GET",
            url: candidate_search_url,
        }).done(function(data) {
            // trigger event finsh
            $opponent.trigger('revup.autocompleteSearchFinish', {numResult: data.count});


            // update the number of opponents
            $oppCount.html(data.count + ' Opponent(s)');

            // build the list
            var sTmp = [];
            for (opp in data.results) {
                sTmp.push('<li class="opponentLstEntry" data-cand-id="' + data.results[opp]._id + '">' + data.results[opp].CAND_NAME + '</li>');
            }

            // if empty put message
            if (sTmp.length == 0) {
                sTmp.push('<li class="noMatchBlank"></li>');
                sTmp.push('<li class="noMatch">No Matches</li>')
            }

            // add the items to the list
            $oppLst.html(sTmp.join(''));

            // display the list
            if ($oppLstDiv.is(":hidden")) {
                $oppLstDiv.slideDown('fast');
                $oppCount.show();
            }
        }).fail(function() {
            // trigger event
            $opponent.trigger('revup.autocompleteFailedSearch');

            console.error("Unable to load ")
        }).always(function() {
            // clear requesting data
            bFetchingOppData = false;

            // hide the spinner
            $waitSpinner.hide();
        });
    }; // getLstOfOpponents

    // handler for getting the opponents
    $searchFor
        .off('change')
        .on('change', function(e) {
            $opponent.trigger('revup.autocompleteTextChange');

            e.preventDefault();
            return false;
        })

        .off('keyup')
        .on('keyup', function(e) {
            // get the typed in value
            var currentVal = $(this).val();

            // if timer set clear it
            if (oppNameTypeTimer != null) {
                clearTimeout(oppNameTypeTimer);
                opNameTypeTimer = null;
            }

            // if the currentVal is blank then don't make request
            if (currentVal === '') {
                clearTimeout(oppNameTypeTimer);
                oppNameTypeTimer = null;
                $oppLst.html('');
                $oppCount.hide();
                $oppLstDiv.slideUp('fast');
                bLastCharBackspace = false;

                return;
            }

            // if in the middle of requesting data don't make another request
            if (bFetchingOppData) {
                bLastCharBackspace = false;
                return;
            }

            // make sure the length is long enought
            if ((currentVal.length < startSearchLen) && (!bLastCharBackspace)) {
                return;
            }

            // clear flags
            bLastCharBackspace = false;

            // set the type timer
            oppNameTypeTimer = setTimeout(function() {
                var name = currentVal;
                getLstOfOpponents(name);
                oppNameTypeTimer = null;
            }, oppNameTypeTimeout);
        })
        .off('keydown')
        .on('keydown', function(e) {
            // catch the return/submit key
            var code = e.keyCode || e.which;
            if (code === 13) {
                var currentVal = $(this).val();
                if (currentVal != '') {
                    clearTimeout(oppNameTypeTimer);
                    oppNameTypeTimer = null;

                    getLstOfOpponents(currentVal);
                }

                e.preventDefault();
                return false;
            }
            else if (code === 8) {
                // the key just typed is a backspace
                bLastCharBackspace = true;
            }
        })
});
