var rankingDetails = (function () {
    /*
     *  Globals
     */
    var detailWidth =   310;        // width of the details section
    var spaceBetween =  20;         // width between details and list
    var debugMode = !!Cookies.get("debug"); // debug mode state

    // infinite scrolling constants
    const pageSize              = 50;                           //number of entries to fetch
    const nextPageMarkerIndex   = Math.round(pageSize * .8);    // where to put marker

    // ranking list selectors
    let $rankingLstDiv          = $();
    let $rankingLst             = $();
    let $rankingLstFooter       = $();
    let $rankingLstHeader       = $();

    // adjusting the size of the ranking lists
    let rankingLstHeightDelta   = 320;//380; // this include the top and bottom margins, tabs, top header, and table header
    let rankingHeaderNoteDelta  = 50;  // this is the height of the top message when displayed

    // partitions
    var partitionLst = [];          // list of active partitions
    var activePartition = 0;        // currently selected partition

    // contact set list
    var contactSetLst = [];
    var contactSetLstAllLabel = 'All Contacts';
    var allContactsTitle = 'all contacts';

    // tabs
    let activeTab = 'accountTab';           // which tab is active on page load

    // the selected contact set info
    // format:
    //      O: id
    //      1: title of the source
    //      2: the type of the source
    //      3: analysis id of the source
    //      4: analysis profile of the source
    var contactSetActiveId = -1;            // currently active contact set
    let personalContactId = -1;             // contact set id for the personal tab
    let accountContactId = -1;              // account contact set id

    // permission's
    contactSetPerm = {};

    // passed in values
    var analysisId = null;          // analysis Id
    var templateData = null;        // template data

    // settings
    var settingsUserId = null;              // user setting id
    var settingsPoliticalColor = true;      // display political colors
    var settingsDisplayMaxedDonors = true;  // display the maxed donors

    var yearsOfGiving;                      // year of giving based on partition

    // details constants
    var detailData = {};                    // the loaded detail data
    var maxDetailEmailAddress = 3;          // maximum number of email address to display
    var maxDetailContribCandidate = 7;      // maximum number of contributions to candidate to display
    var maxDetailContribFederal = 10;       // maximum number of federal contributions to display
    var maxDetailContribState = 5;          // maximum number of state contributions to display
    var detailFeatures = [{key: "Federal Matches", label: "Last 10 Contributions to Federal Candidates", maxDisplay: maxDetailContribFederal},
                          {key: "State Matches", label: "Last 5 Contributions to State Candidates", maxDisplay: maxDetailContribState}];
    var keyContributionsLabel = "Key Contributions";
    var paginationExtraMsg = "Maxed out donors Hidden";

    // keyboard up/down
    // var newIndex;                       // index of new selection when timer pops
    var upDownTimer = null;             // up/down time
    var upDownDelay = 500;              // amount of time to wait to fire event

    // auto scroll
    bAutoScrollDetails = false;         // true when auto scrolling

    // improve the text of the section in the contact set drop down
    var prettySectionKey = {
        shared: "Shared with Me",
        account: '<div class="title">Shared Account Contacts</div><div class="subtitle">These can be used for Call Time</div>',
        user: "My Contacts",
    }
    var prettyDropDown = {
        account: "Account Shared",
        user: "Personal"
    }

    // search mode
    var bAdvFilterMode = false;

    // fixed quick filters
    var filterData = {};
    var quickFilterIndex = 0;
    var quickFilters = [];

    /*
     * Helper methods
     */
     function contactLstDisplayName(data)
     {
         var sTmp = [];

         var colorText = false;

         // pick the icon to display
         switch (data.source ? data.source.toLowerCase() : '') {
             case 'csv':
                 sTmp.push("<img class='downloadImg imgCSV' src='" + templateData.csvImg + "'>");
                 break;
             case 'gmail':
                 sTmp.push("<img class='downloadImg imgGmail' src='" + templateData.gmailImg + "'>");
                 break;
             case 'linkedin':
                 sTmp.push("<img class='downloadImg imgLinkedIn' src='" + templateData.linkedInImg + "'>");
                 break;
             case 'vcard':
                 sTmp.push("<img class='downloadImg imgApple' src='" + templateData.appleImg + "'>");
                 break;
             case 'outlook':
                 sTmp.push("<img class='downloadImg imgOutlook' src='" + templateData.outlookImg + "'>");
                 break;
             case 'iphone':
                 sTmp.push("<img class='downloadImg imgOutlook' src='" + templateData.mobileImg + "'>")
                 break;
             case 'saved':
                 sTmp.push("<img class='downloadImg imgSaved' src='" + templateData.savedImg + "'>");
                 break;
             case 'cloned':
                 sTmp.push("<img class='downloadImg allRevup' src='" + templateData.clonedImg + "'>");
                 break;
             case '':
                 sTmp.push("<img class='downloadImg imgAll' src='" + templateData.allImg + "'>");
                 break;
             default:
                 colorText = true;
                 sTmp.push("<img class='downloadImg imgOutlook' src='" + templateData.unknownImg + "'>");
                 //sTmp.push("<div class='downloadImg imgBlank'></div>");
                 break;
         } // end switch - data.source.toLowerCase

         // name
         if (colorText) {
             sTmp.push("<span style='color:#6dab1b'>" + data.title + "</span>");
         }
         else {
             sTmp.push(data.title);
         }

         return sTmp.join('');

     } // contactLstDisplayName

    function scoreFormat(rawValue)
    {
        if (!rawValue) {
            rawValue = 0;
        }

        // get the score, if the fraction part is 0 don't show it
        score = rawValue.toFixed(1);

        // remove the fraction if fraction value is 0
        var tmp = score.toString();
        tmp = tmp.split('.');
        if ((tmp.length == 2) && (Number(tmp[1]) == 0)) {
            score = Number(tmp[0]);
        }
        else {
            score = Number(score);
        }

        if (score == 0) {
            return("&nbsp;");
        }

        return score;
    }// scoreFormat

    function scoreColorClass(rawValue, formatedScore)
    {
        var politicalSpectrumClass = "";
        if (/*!rawValue ||*/ !formatedScore) {
            return politicalSpectrumClass;
        }
        var polSpectrum = rawValue.toFixed(1);
        if ((polSpectrum >= 0) && (formatedScore > 0)) {
            if (polSpectrum == 0) {
                politicalSpectrumClass = 'political-spectrum-bar-republican'
            }
            else if ((polSpectrum > 0) && (polSpectrum < 0.4)) {
                politicalSpectrumClass = 'political-spectrum-bar-republican-lite'
            }
            else if ((polSpectrum >= 0.4) && (polSpectrum <= 0.6)) {
                politicalSpectrumClass = 'political-spectrum-bar-neutral'
            }
            else if ((polSpectrum > 0.6) && (polSpectrum < 1)) {
                politicalSpectrumClass = 'political-spectrum-bar-democrat-lite'
            }
            else {
                politicalSpectrumClass = 'political-spectrum-bar-democrat'
            }
        }

        return politicalSpectrumClass;
    } // scoreColorClass

    function pieChart(resultData)
    {
        // build the SVG for a pie chart
        //   based on article at: https://helloanselm.com/2014/add-simple-charts-to-your-page
        //
        //   arguments
        //      data -              array of slice objects
        //                              value:   numeric value of the slice
        //                              color:   color of slice
        //                              label:   if legend is display the label for the slice (optional)
        //      width, height -     width and height of the svg area
        //      cx, cy -            center of the pie chart
        //      r -                 radius of the pie chart
        //      lx, ly -            location of the legend (optional)
        function pieChartSVG(data, width, height, cx, cy, r, lx, ly)
        {
            var svgns = 'http://www.w3.org/2000/svg';
            var chart = document.createElementNS(svgns, 'svg:svg');
            var total = 0;
            var angles = [];
            var startAngle = 0;
            var i = 0;

            chart.setAttribute('width', width);
            chart.setAttribute('height', height);
            chart.setAttribute('viewBox', '0 0 ' + width + ' ' + height);

            var baseImg = document.createElementNS(svgns, "image");
            baseImg.setAttributeNS(null, "href", "../static/img/pieChartBase.png");
            baseImg.setAttributeNS(null, 'width', 117);
            baseImg.setAttributeNS(null, 'height', 26);
            baseImg.setAttributeNS(null, 'x', (width - 117) / 2);
            baseImg.setAttributeNS(null, 'y', height - (26 / 2) - 4);
            chart.appendChild(baseImg)

            // Add all data values so we know how big the whole pie is
            var numVal = 0;
            var dataIndex = -1;
            for (i = 0; i < data.length; i++) {
                if (data[i].value != 0) {
                    numVal += 1;
                    dataIndex = i;
                }
                total += data[i].value;
            }

            // Now figure out how big each slice of the pie is. Angles in radians.
            var midPoints = [];
            var percent = [];
            for (i = 0; i < data.length; i++) {
                angles[i] = data[i].value / total * Math.PI * 2;
                midPoints[i] = (data[i].value / 2) / total * Math.PI * 2;

                percent[i] = Math.round((data[i].value / total) * 100);
            }

            // Loop through each slice of pie.
            if (numVal == 0) {
                return "";
            }
            else if (numVal == 1) {
                // draw the circle
                var circle = document.createElementNS(svgns, 'circle');
                circle.setAttribute('cx', cx);
                circle.setAttribute('cy', cy);
                circle.setAttribute('r', r);
                circle.setAttribute('fill', data[dataIndex].color);

                chart.appendChild(circle);

                // draw linear gradient
                var defs = document.createElementNS (svgns, "defs");
                var grad = document.createElementNS (svgns, "linearGradient");
                grad.setAttributeNS (null, "id", "gradient");
                grad.setAttributeNS (null, "x1", "0%");
                grad.setAttributeNS (null, "x2", "0%");
                grad.setAttributeNS (null, "y1", "100%");
                grad.setAttributeNS (null, "y2", "0%");

                var stopTop = document.createElementNS (svgns, "stop");
                stopTop.setAttributeNS (null, "offset", "0%");
                stopTop.setAttributeNS(null, "stop-opacity", "1.0")
                stopTop.setAttributeNS (null, "stop-color", data[dataIndex].color);
                grad.appendChild (stopTop);

                var stopBottom = document.createElementNS (svgns, "stop");
                stopBottom.setAttributeNS (null, "offset", "100%");
                stopBottom.setAttributeNS(null, "stop-opacity", "0.5")
                stopBottom.setAttributeNS (null, "stop-color", data[dataIndex].color);
                grad.appendChild (stopBottom);
                defs.appendChild (grad);
                chart.appendChild(grad);

                // Now set attributes on the <svg:path> element
                circle.setAttribute('d', d);
                var f = "url(#gradient)";
                circle.setAttribute('fill', f) //data[i].color);
                circle.setAttribute('class', "pieSlice")

                // draw the label
                // label
                var lx1 = cx;
                var ly1 = cy + 6;  // half the label height
                var l = document.createElementNS(svgns, 'text');
                l.setAttribute('x', lx1);
                l.setAttribute('y', ly1);
                l.textContent = '100%';
                l.setAttribute("fill", "white");
                l.setAttribute('text-anchor', 'middle');
                l.setAttribute('font-size', '12');
                l.setAttribute('font-family', 'SourceSansPro');
                chart.appendChild(l);
            }
            else {
                for (i = 0; i < data.length; i++) {
                    var path = document.createElementNS(svgns, 'path');
                    var endAngle = startAngle + angles[i];
                    var x1 = cx + r * Math.sin(startAngle);
                    var y1 = cy - r * Math.cos(startAngle);
                    var x2 = cx + r * Math.sin(endAngle);
                    var y2 = cy - r * Math.cos(endAngle);
                    var big = 0;
                    var d;
                    var labelShare = Math.round(((data[i] / total) * 100)) + '%: ';

                    if (endAngle - startAngle > Math.PI) {
                        big = 1;
                    }

                    // build the path for the slices
                    d = 'M ' + cx + ',' + cy +  // Start at circle center
                        ' L ' + x1 + ',' + y1 + // Draw line to (x1,y1)
                        ' A ' + r + ',' + r +   // Draw an arc of radius r
                        ' 0 ' + big + ' 1 ' +   // Arc details...
                        x2 + ',' + y2 +         // Arc goes to to (x2,y2)
                        ' Z';                   // Close path back to (cx,cy)

                    // draw linear gradient
                    var defs = document.createElementNS (svgns, "defs");
                    var grad = document.createElementNS (svgns, "linearGradient");
                    grad.setAttributeNS (null, "id", "gradient" + i);
                    grad.setAttributeNS (null, "x1", "0%");
                    grad.setAttributeNS (null, "x2", "0%");
                    grad.setAttributeNS (null, "y1", "100%");
                    grad.setAttributeNS (null, "y2", "0%");

                    var stopTop = document.createElementNS (svgns, "stop");
                    stopTop.setAttributeNS (null, "offset", "0%");
                    stopTop.setAttributeNS(null, "stop-opacity", "1.0")
                    stopTop.setAttributeNS (null, "stop-color", data[i].color);
                    grad.appendChild (stopTop);

                    var stopBottom = document.createElementNS (svgns, "stop");
                    stopBottom.setAttributeNS (null, "offset", "100%");
                    stopBottom.setAttributeNS(null, "stop-opacity", "0.5")
                    stopBottom.setAttributeNS (null, "stop-color", data[i].color);
                    grad.appendChild (stopBottom);
                    defs.appendChild (grad);
                    chart.appendChild(grad);

                    // Now set attributes on the <svg:path> element
                    path.setAttribute('d', d);
                    var f = "url(#gradient" + i + ")";
                    path.setAttribute('fill', f) //data[i].color);
                    path.setAttribute('class', "pieSlice")

                    //var startAngleDeg = startAngle * 180 / Math.PI;
                    //var endAngleDeg = endAngle * 180 / Math.PI;
                    var animate = document.createElementNS('svgns', 'animate');
                    animate.setAttribute('attributeName', 'opacity');
                    animate.setAttribute('from', '0');
                    animate.setAttribute('to', '1');
                    animate.setAttribute('begin', '0s');
                    animate.setAttribute('end', '1s');
                    animate.setAttribute('fill', 'freeze');
                    /*
                    animate.setAttribute('attributeName', 'transform');
                    animate.setAttribute('begin', '0s');
                    animate.setAttribute('end', '20s');
                    animate.setAttribute('type', 'rotate');
                    animate.setAttribute('from', startAngleDeg + ' ' + width / 2 + ' ' + height / 2);
                    animate.setAttribute('to', endAngleDeg + ' ' + width / 2 + ' ' + height / 2);
                    animate.setAttribute('repeatDur', "30s");
                    */
                    path.appendChild(animate)
                    chart.appendChild(path);

                    // label
                    var sliceAngleDeg = (endAngle - startAngle) * 180 / Math.PI;
                    if (sliceAngleDeg > 25) {
                        var lx1 = cx + (r * .65) * Math.sin(startAngle + midPoints[i]);
                        var ly1 = cy - (r * .65) * Math.cos(startAngle + midPoints[i]);
                        var l = document.createElementNS(svgns, 'text');
                        l.setAttribute('x', lx1);
                        l.setAttribute('y', ly1);
                        l.textContent = percent[i] + '%';
                        l.setAttribute("fill", "white");
                        l.setAttribute('text-anchor', 'middle');
                        l.setAttribute('font-size', '12');
                        l.setAttribute('font-family', 'SourceSansPro');
                        chart.appendChild(l);
                    }

                    // The next wedge begins where this one ends
                    startAngle = endAngle;

                    // create the legend only if both the lx and ly (label location) are defined
                    if ((lx != undefined) && (ly != undefined)) {
                        var labelColor = document.createElementNS(svgns, 'rect');
                        labelColor.setAttribute('x', lx);
                        labelColor.setAttribute('y', ly + 30*i);
                        labelColor.setAttribute('width', 20);
                        labelColor.setAttribute('height', 20);
                        labelColor.setAttribute('fill', data[i].color);
                        chart.appendChild(labelColor);

                        // Add label to the right of the rectangle
                        var label = document.createElementNS(svgns, 'text');
                        label.setAttribute('x', lx + 30);
                        label.setAttribute('y', ly + 30*i + 18);
                        label.setAttribute('font-family', '"jaf-bernino-sans", sans-serif');
                        label.setAttribute('font-size', '16');

                        // Add DOM node for the text to the <svg:text> element
                        label.appendChild(document.createTextNode(labelShare + data[i].label));
                    }
                }
            }

            return chart;
        }; //pieChartSVG

        // analysis the features for giving break down
        if (resultData.features) {
            var features = resultData.features;

            var demGiving = 0;
            var repGiving = 0;
            var otherGiving = 0;

            // walk the list of features looking for giving breakdown
            for (var key in features) {
                for (var s_key in features[key]) {
                    if (features[key][s_key][0] && features[key][s_key][0].giving_breakdown) {
                        var gBreakdown = features[key][s_key][0].giving_breakdown;
                        if (gBreakdown.democrat_giving) {
                            demGiving += gBreakdown.democrat_giving;
                        }
                        if (gBreakdown.republican_giving) {
                            repGiving += gBreakdown.republican_giving;
                        }
                        if (gBreakdown.other_giving) {
                            otherGiving += gBreakdown.other_giving;
                        }
                    }
                }
            }

            // ServicesChart
            var data = [
                {value: demGiving, color: "#298fe6"},  // democrate
                //{value: 100, color: "#90c3f0"}, // democrate leaning
                {value: otherGiving, color: "#d6c0f0"}, // neutral
                //{value: 25, color: "#fc9090"},  // republican leaning
                {value: repGiving, color: "#fc3a3a"}    // republican
            ]
            var pieChart = pieChartSVG(data, 100, 105, 50, 50, 50);

            // Append Charts to DOM
            if (pieChart == "") {
                $('.detailDiv .contributionSpreadDiv .sectionTitle').hide();
            }
            else {
                $('.detailDiv .contributionSpreadDiv .sectionTitle').show(1);
            }
            $('.pieDivInner').html(pieChart)
        }
        else {
            $('.detailDiv .contributionSpreadDiv .sectionTitle').hide();
            $('.pieDivInner').html("");
        }
    } // pieChart

    function histogram(data)
    {
        var sTmp = [];

        // get the data
        var sf = undefined;
        var f = undefined;
        var s = data;
        if (displaySection.givingHistoryGraph.section) {
            s = data[displaySection.givingHistoryGraph.section];
        }
        if (s) {
            sf = s[displaySection.givingHistoryGraph.field];
            if ((sf) && (sf.length > 0) && (displaySection.givingHistoryGraph.subField)) {
                f = sf[displaySection.givingHistoryGraph.subField];
                if (!f) {
                    f = sf[0][displaySection.givingHistoryGraph.subField];
                }
            }
            else {
                f = sf;
            }
        }

        // get the main data
        var labels = [];
        var mainVals = [];
        //var maxGiving = 0;
        var m = 0;
        if ((f) && (Array.isArray(f)) && (f.length > 0)) {
            // build the column label and the right scale
            for (var i = 0; i < f.length; i++) {
                var v = f[i];
                mainVals[i] = v[1];

                // keep the largest year giving
                //if (v[1] > maxGiving) {
                //    maxGiving = v[1];
                //}

                // build the column labels
                var l = v[0] + '';
                labels.push(l.slice(-2));
            }
        }

        // get the other contributions
        var otherMax = 0;
        var otherVals = [];
        for (feature in data.features) {
            for (var key in data.features[feature]) {
                if (key == displaySection.givingHistoryGraph.field) {
                    if ((data.features[feature][key].length > 0) && (data.features[feature][key][0].hasOwnProperty('giving_history'))) {
                        var gh = data.features[feature][key][0].giving_history;

                        for (var i = 0; i < gh.length; i++) {
                            var year = gh[i][0] + '';
                            year = year.slice(-2);
                            var amt = gh[i][1];

                            // see if the label is in the list of label
                            var labelIndex = -1;
                            for (var l = 0; l < labels.length; l++) {
                                if (labels[l] == year) {
                                    labelIndex = l;
                                    break;
                                }
                            }

                            // see if new label
                            if (labelIndex == -1) {
                                labels.push(year);

                                labelIndex = labels.length - 1;
                            }

                            // add in the amt
                            if (otherVals[labelIndex]) {
                                otherVals[labelIndex] += parseInt(amt, 10);
                            }
                            else {
                                otherVals[labelIndex] = parseInt(amt, 10);
                            }
                            //if (otherVal[labelIndex] > otherMax) {
                            //    otherMax = otherVal[labelIndex];
                            //}
                        }
                    }
                }
            }
        }

        // only display if there is data
        if ((mainVals.length > 0) || (otherVals.length > 0)) {
            // compute the column max
            var maxGiving = 0;
            for (var i = 0; i < labels.length; i++) {
                var mVal = 0;
                var oVal = 0
                if (mainVals[i]) {
                    mVal = mainVals[i];
                }
                if (otherVals[i]) {
                    oVal = otherVals[i];
                }

                // keep the largest year giving
                if ((mVal + oVal) > maxGiving) {
                    maxGiving = mVal + oVal;
                }
            }

            // build the scale, based on the maxGiving
            var startPoint = 500;
            if (displaySection.givingHistoryGraph.scaleStartPoint) {
                startPoint = parseInt(displaySection.givingHistoryGraph.scaleStartPoint, 10);
            }

            var stepSize = 500;
            if (displaySection.givingHistoryGraph.scaleStepSize) {
                stepSize = parseInt(displaySection.givingHistoryGraph.scaleStepSize, 10);
            }

            var maxAmount = 1000000;
            if (displaySection.givingHistoryGraph.scaleMax) {
                maxAmount = parseInt(displaySection.givingHistoryGraph.scaleMax, 10);
            }

            var numBars = 4;
            if (displaySection.givingHistoryGraph.scaleNumBars) {
                numBars = parseInt(displaySection.givingHistoryGraph.scaleNumBars, 10);
            }

            var maxBarAmt = startPoint;
            while (true) {
                if ((maxBarAmt >= maxGiving) || (maxBarAmt >= maxAmount)) {
                    break;
                }

                maxBarAmt += stepSize
            }
            var maxBarDec = Math.round(maxBarAmt / numBars);

            // title
            if (displaySection.givingHistoryGraph.title) {
                sTmp.push('<div class="title">' + displaySection.givingHistoryGraph.title + '</div>');
            }
            if (displaySection.givingHistoryGraph.subtitle) {
                sTmp.push('<div class="subtitle">' + displaySection.givingHistoryGraph.subtitle + '</div>');
            }

            // legend
            if (displaySection.givingHistoryGraph.otherLegend || displaySection.givingHistoryGraph.mainLegend) {
                sTmp.push('<div class="legendSection">');
                    if (displaySection.givingHistoryGraph.otherLegend) {
                        sTmp.push('<div class="entry">');
                            var c = '#dcebf9';
                            if (displaySection.givingHistoryGraph.otherBarColor) {
                                c = displaySection.givingHistoryGraph.otherBarColor;
                            }
                            var cStyle = 'background:' + c + ';';
                            sTmp.push('<div class="blob" style="' + cStyle + '"></div>')
                            sTmp.push('<div class="text">' + displaySection.givingHistoryGraph.otherLegend + '</div>');
                        sTmp.push('</div>');
                    }

                    if (displaySection.givingHistoryGraph.mainLegend) {
                        sTmp.push('<div class="entry">');
                            var c;
                            colorRules = currentSkin.colors['scoreHighlightColor'];
                            if (colorRules) {
                                for (var cr = 0; cr < colorRules.length; cr++) {
                                    if (colorRules[cr].colorType == "background") {
                                        c = colorRules[cr].colorVal;
                                    }
                                }
                            }
                            else {
                                c = '#a4b58e';
                                if (displaySection.givingHistoryGraph.mainBarColor) {
                                    c = displaySection.givingHistoryGraph.mainBarColor;
                                }

                            }
                            var cStyle = 'background:' + c + ';';
                            sTmp.push('<div class="blob" style="' + cStyle + '"></div>')
                            sTmp.push('<div class="text">' + displaySection.givingHistoryGraph.mainLegend + '</div>');
                        sTmp.push('</div>');
                    }
                sTmp.push('</div>');
            }

            // compute the height of the graph sections
            var numGraphBars = displaySection.givingHistoryGraph.scaleBars ? displaySection.givingHistoryGraph.scaleBars.length : 4;
            var graphStyle = "";
            if (displaySection.givingHistoryGraph.scaleBars) {
                //graphStyle =  "height:" + displaySection.givingHistoryGraph.scaleBars.length * 20 + "px;";
            }
            if (graphStyle != "") {
                //graphStyle = ' style="' + graphStyle + '"';
            }

            sTmp.push('<div class="graphBody">');
                sTmp.push('<div class="graphDiv">');
                    sTmp.push('<div class="graphBars">');
                        for (var i = 0; i < numBars; i++) {
                            sTmp.push('<div class="barDiv"><div class="bar"></div></div>');
                        }
                    sTmp.push('</div>');

                    // draw the graph
                    sTmp.push('<div class="graph"' + graphStyle + '>');
                        // draw the columns -> based on the number of labels
                        for (var i = 0; i < labels.length; i++) {
                            var mVal = 0;
                            var oVal = 0
                            if (mainVals[i]) {
                                mVal = mainVals[i];
                            }
                            if (otherVals[i]) {
                                oVal = otherVals[i];
                            }

                            var mainBarHeight = Math.round(((mVal / maxBarAmt) * 100));
                            if (mainBarHeight > 100) {
                                mainBarHeight = 100;
                            }
                            var otherBarHeight = Math.round(((oVal / maxBarAmt) * 100));
                            if ((mainBarHeight + otherBarHeight) > 100) {
                                otherBarHeight = 100 - (mainBarHeight + otherBarHeight);
                            }

                            // build the style for the mainBar
                            var mainBarStyle = "";
                            mainBarStyle += 'height:' + mainBarHeight + '%;';
                            if (mainBarHeight != 100) {
                                mainBarStyle += 'top:' + (100 - (otherBarHeight + mainBarHeight)) + '%;';
                            }
                            colorRules = currentSkin.colors['scoreHighlightColor'];
                            if (colorRules) {
                                for (var cr = 0; cr < colorRules.length; cr++) {
                                    if (colorRules[cr].colorType == "background") {
                                        mainBarStyle += 'background:' + colorRules[cr].colorVal + ';';
                                    }
                                }
                            }
                            else {
                                var c = '#a4b58e';
                                if (displaySection.givingHistoryGraph.mainBarColor) {
                                    c = displaySection.givingHistoryGraph.mainBarColor;
                                }

                                mainBarStyle += 'background:' + c + ';';
                            }
                            //mainBarStyle += 'background:' + barColor[i] + ';';
                            if (otherBarHeight != 0) {
                                mainBarStyle += 'border-top-left-radius:0;border-top-right-radius:0;'
                            }

                            // build the style of the otherBar
                            var otherBarStyle = "";
                            otherBarStyle += 'height:' + otherBarHeight + '%;';
                            //if (otherBarHeight != 100) {
                                otherBarStyle += 'top:' + (100 - (otherBarHeight + mainBarHeight)) + '%;';
                            //}
                            var c = '#dcebf9';
                            if (displaySection.givingHistoryGraph.otherBarColor) {
                                c = displaySection.givingHistoryGraph.otherBarColor;
                            }
                            otherBarStyle += 'background:' + c + ';';

                            sTmp.push('<div class="barDiv">');
                                sTmp.push('<div class="otherBar" style="' + otherBarStyle +'"></div>');
                                sTmp.push('<div class="mainBar" style="' + mainBarStyle +'"></div>');
                            sTmp.push('</div>');
                        }
                    sTmp.push('</div>');

                    sTmp.push('<div class="graphScale">');
                        for (var i = 0; i < labels.length; i++) {
                            sTmp.push('<div class="text">')
                                sTmp.push(labels[i]);
                            sTmp.push('</div>');
                        }
                    sTmp.push('</div>');
                sTmp.push('</div>');

                sTmp.push('<div class="rightScale">');
                    sTmp.push('<div class="vals">');
                        for (var i = 0; i < numBars; i++) {
                            //t = '$' + revupUtils.commify(maxBarAmt);
                            var t = (maxBarAmt / 1000).toFixed(1);
                            maxBarAmt -= maxBarDec;

                            sTmp.push('<div class="text">' + t + '</div>');
                        }
                    sTmp.push('</div>');

                    sTmp.push('<div class="unitLabel">thousands $</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');
        }
        else {
            // title
            if (displaySection.givingHistoryGraph.title) {
                sTmp.push('<div class="title">' + displaySection.givingHistoryGraph.title + '</div>');
            }

            // no data message
            if (displaySection.givingHistoryGraph.noDataMsg) {
                sTmp.push('<div class="noDataMsg">' + displaySection.givingHistoryGraph.noDataMsg + '</div>');
            }
        }

        return sTmp.join('');
    } // histogram

    /*
     * Build the list info bar
     */
    function buildLstInfoBar()
    {
        let sTmp = [];

        sTmp.push('<div class="infoBarDiv">');
            sTmp.push('<div class="infoBarTextDiv">');
                sTmp.push('<div class="infoBarTextContent">');
                    sTmp.push('<div class="icon icon-info-1"></div>');
                    sTmp.push('<div class="msg">You must be viewing a list of Shared Account Contacts in order to add them to Call Time</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');
            sTmp.push('<div class="infoBarCloseDiv">');
                sTmp.push('<div class="icon icon-close"></div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        return sTmp.join('');
    } // buildLstInfoBar

    /*
     * Built the list and detail views
     */
    function buildLstHeader()
    {
        var sTmp = [];

        // list tab
        if (activeTab != -1) {
            sTmp.push('<div class="togglePages">');
                let extraPersonalClass = '';
                let extraAccountClass = '';
                if (personalContactId == -1) {
                    // disable the personal tab and make the account tab full size
                    extraPersonalClass = ' disabled';
                    extraAccountClass = ' fullSize';
                }
                if (accountContactId == -1) {
                    // disable the account tab and make the personal tab full size
                    extraAccountClass = ' disabled';
                    extraPersonalClass = ' fullSize';
                }

                if (personalContactId != -1) {
                    if (activeTab == 'personalTab') {
                        sTmp.push('<div class="contactSetTab personalTab' + extraPersonalClass + ' active">Personal Contacts </div>');
                    }
                    else {
                        sTmp.push('<div class="contactSetTab personalTab' + extraPersonalClass + '">Personal Contacts </div>');
                    }
                }

                if (accountContactId != -1) {
                    if (activeTab == 'accountTab') {
                        sTmp.push('<div class="contactSetTab accountTab' + extraAccountClass + ' active">Shared Account Contacts</div>');
                    }
                    else {
                        sTmp.push('<div class="contactSetTab accountTab' + extraAccountClass + '">Shared Account Contacts</div>');
                    }
                }
            sTmp.push('</div>');
        }


        // partition - cycle, search .....
        sTmp.push('<div class="rankingHeaderDiv">');
            sTmp.push('<div class="leftSide">');
                sTmp.push('<div class="iconBtn hamburgerIconBtn">');
                    sTmp.push('<div class="icon icon-hamburger"></div>');
                sTmp.push('</div>');

                sTmp.push('<div class="sepBar sepBarOne"></div>');
                sTmp.push('<div class="sepBar sepBarTwo"></div>')

                sTmp.push('<div class="partitionGrpDiv">');
                    sTmp.push('<div class="text" style="margin-top:6px;">Election Cycle:</div>');
                    sTmp.push('<div class="partitionDiv"></div>');
                sTmp.push('</div>');

                sTmp.push('<div class="sepBar sepBarOne"></div>');
                sTmp.push('<div class="sepBar sepBarTwo"></div>')

                sTmp.push('<div class="contactLstGrpDiv">');
                    sTmp.push('<div class="text" style="margin-top:6px;">List:</div>');
                    sTmp.push('<div class="contactLstDiv"></div>');
                sTmp.push('</div>');

                sTmp.push('<div class="sepBar sepBarOne"></div>');
                sTmp.push('<div class="sepBar sepBarTwo"></div>')
            sTmp.push('</div>');

            sTmp.push('<div class="rightSide">');
                sTmp.push('<div class="sepBar sepBarOne"></div>');
                sTmp.push('<div class="sepBar sepBarTwo"></div>');

                sTmp.push('<div class="searchSection">');
                    sTmp.push('<div class="topLine">');
                        sTmp.push('<div class="text">Search By:</div>');
                        sTmp.push('<div class="searchType"></div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="searchBar"></div>');
                sTmp.push('</div>');

                sTmp.push('<div class="sepBar sepBarOne"></div>');
                sTmp.push('<div class="sepBar sepBarTwo"></div>')

                sTmp.push('<div class="advFilterSection  advFilterBtn">');
                    sTmp.push('<div class="content">')
                        sTmp.push('<div class="side">');
                            sTmp.push('<div>Advanced</div>');
                            sTmp.push('<div>Filter</div>');
                        sTmp.push("</div>")
                        sTmp.push('<div class="side">');
                            sTmp.push('<div class="icon icon-filter"></div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        // add the old style quick filter
        // which contacts will be displayed
        var style = "";
        if (templateData.bDisplayQuickFilters) {
            style = "";
        }
        else {
            style = ' style="display:none;"';
        }
        sTmp.push('<div class="showingContactsDiv"' + style + '>');
            sTmp.push('<div class="showingLabel">Showing</div>');
            sTmp.push('<div class="showingWhat" quickFilterVal="' + quickFilters[quickFilterIndex].value + '">' + quickFilters[quickFilterIndex].label + '</div>');
            sTmp.push('<div class="showingOpenClose" openCloseState="closed">');
                sTmp.push('<div class="icon icon-double-arrow-dn"></div>')
            sTmp.push('</div>');
        sTmp.push('</div>');
        sTmp.push('<div class="showingContactsLstDiv" style="display:none;">');
            for (var i = 0; i < quickFilters.length; i++) {
                sTmp.push('<div class="entry" contactFilterId="' + quickFilters[i].id + '" quickFilterVal="' + quickFilters[i].value + '">');
                    sTmp.push('<div class="text">'+ quickFilters[i].desc + '</div>');
                    sTmp.push('<div class="icon icon-check"></div>');
                sTmp.push('</div>');

                if ((i + 1) < quickFilters.length) {
                    sTmp.push('<hr>')
                }
            }
        sTmp.push('</div>');


        // new filters
        sTmp.push('<div class="newFiltersSection"></div>')

        // header of the ranking list
        let joinPermission = undefined;
        if (contactSetPerm.call_time != null && contactSetPerm.prospects != null) {
            joinPermission = 'call_time-prospects';
        }
        sTmp.push(revupListView.buildListHeader(templateData.seatId, contactSetPerm, joinPermission));

        return sTmp.join('');
    } // buildLstHeader

    function buildLstFooter(data)
    {
        // build the html
        var sTmp = [];
        sTmp.push('<div class="lstFooterDiv">')
            sTmp.push('<text class="showing">Showing </text>')
            sTmp.push('<text> of </text>')
            sTmp.push('<text class="total"> contacts </text>')
        sTmp.push('</div>')

        return sTmp.join('');
    } // buildLstFooter

    function buildLstView(data)
    {
        var sTmp = [];

        // display the info bar
        let infoHideDate = localStorage.getItem('ranking-' + templateData.seatId + '-hideInfoDate');
        let now = Date.now();
        if (!infoHideDate) {
            infoHideDate = now + (5 * 24 * 60 * 60 * 1000);  // hide in 5 days
            localStorage.setItem('ranking-' + templateData.seatId + '-hideInfoDate', infoHideDate);
        }
        let infoClosed = localStorage.getItem('ranking-' + templateData.seatId + '-hideInfo');
        if (infoClosed != 'true' && now < infoHideDate) {
            sTmp.push(buildLstInfoBar());
        }


        // build the header
        sTmp.push(buildLstHeader());

        sTmp.push('<div class="rankingLstDiv">');
            // add the loading div - user when loading a new page
            sTmp.push('<div class="rankingDetailsLstLoading" style="height:450px;width:100%;display:none">');
                sTmp.push('<div class="loadingDiv">');
                    sTmp.push('<div class="loading"></div>');
                    sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                sTmp.push('</div>');
            sTmp.push('</div>');

            var extraClass = "";
            if (!settingsPoliticalColor) {
                extraClass = " hideColor";
            }
            sTmp.push('<div class="rankingDetailsLstDiv' + extraClass + '" tabindex="1">');
                sTmp.push(loadRankingList(data.results, data.results.length <= data.count));
            sTmp.push('</div>');
        sTmp.push('</div>');

        // build the footer
        sTmp.push(buildLstFooter(data));

        return sTmp.join('');
    } // buildLstView

    function buildDetailView(data)
    {
        var sTmp = [];

        sTmp.push('<div class="rankingDetailsDetailLoading" style="height:350px;width:100%">');
            sTmp.push('<div class="loadingDiv">');
                sTmp.push('<div class="loading"></div>');
                sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
            sTmp.push('</div>');
        sTmp.push('</div>');

        sTmp.push('<div class="rankingDetailsDetailDiv">');
            sTmp.push('<div class="rankingDetailDiv">');
                sTmp.push("Detail Div");
            sTmp.push('</div>');
        sTmp.push('</div>');

        return sTmp.join('');
    } // buildDetailView

    /*
     *
     */
    function buildLegendView(data)
    {
        var sTmp = [];


        sTmp.push('<div class="legendPopup2">');
            sTmp.push('<div class="header">');
                sTmp.push('<div class="title">Legend</div>');
                sTmp.push('<div class="closeBtn icon icon-close"></div>');
            sTmp.push('</div>');

            sTmp.push('<div class="legendBody">');
                sTmp.push('<div class="leftSide">');
                    sTmp.push('<div class="iconDiv">');
                        sTmp.push('<div class="entry">');
                            sTmp.push('<div class="leftIconSide">');
                                sTmp.push('<div class="imgIcon legendIcon">');
                                    sTmp.push('<div class="icon icon-key"></div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');
                            sTmp.push('<div class="rightIconSide">');
                                sTmp.push('<div class="text">Legend</div>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');

                        sTmp.push('<div class="entry">');
                            sTmp.push('<div class="leftIconSide">');
                                sTmp.push('<div class="imgIcon legendIcon">');
                                    sTmp.push('<div class="icon icon-settings"></div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');
                            sTmp.push('<div class="rightIconSide">');
                                sTmp.push('<div class="text">Settings</div>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="leaningDiv">');
                        sTmp.push('<div class="entry">');
                            sTmp.push('<div class="colorBlob political-spectrum-bar-democrat"></div>');
                            sTmp.push('<div class="text">Democrat</div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entry">');
                            sTmp.push('<div class="colorBlob political-spectrum-bar-democrat-lite"></div>');
                            sTmp.push('<div class="text">Democrat Leaning</div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entry">');
                            sTmp.push('<div class="colorBlob political-spectrum-bar-neutral"></div>');
                            sTmp.push('<div class="text">Neutral</div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entry">');
                            sTmp.push('<div class="colorBlob political-spectrum-bar-republican-lite"></div>');
                            sTmp.push('<div class="text">Republican Leaning</div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entry">');
                            sTmp.push('<div class="colorBlob political-spectrum-bar-republican"></div>');
                            sTmp.push('<div class="text">Republican</div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                    if (!templateData.isAcademic) {
                        sTmp.push('<div class="iconDiv">');
                            sTmp.push('<div class="entry">');
                                sTmp.push('<div class="leftIconSide">');
                                    sTmp.push('<div class="keyContribDiv">');
                                        sTmp.push('<div class="keyContribIcon"></div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                                sTmp.push('<div class="rightIconSide">');
                                    sTmp.push('<div class="text">Similar to Candidate</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');
                    }
                sTmp.push('</div>');

                if (!templateData.isAcademic) {
                    sTmp.push('<div class="rightSide">');
                        sTmp.push('<div class="contributionDiv">');
                            sTmp.push('<div class="entry">');
                                sTmp.push('<div class="imgDiv">');
                                    sTmp.push('<div class="blob political-spectrum-bar-neutral">');
                                        sTmp.push('<div class="text">Sample</div>');
                                    sTmp.push('</div>');
                                    sTmp.push('<div class="blobText">2014</div>');
                                sTmp.push('</div>');
                                sTmp.push('<div class="msgDiv">');
                                    sTmp.push('Contributions were given to your candidate.  The year of the last contribution is displaced beneath.');
                                sTmp.push('</div>');
                            sTmp.push('</div>');
                            sTmp.push('<div class="entry">');
                                sTmp.push('<div class="imgDiv">');
                                    sTmp.push('<div class="blob" style="background-color:' + revupConstants.color.grayBorder + '">' );
                                        sTmp.push('<div class="text">Sample</div>');
                                    sTmp.push('</div>');
                                    sTmp.push('<div class="blobText">MAXED</div>');
                                sTmp.push('</div>');
                                sTmp.push('<div class="msgDiv">');
                                    sTmp.push('Contributions to your candidate are maxed out for this election cycle.');
                                sTmp.push('</div>');
                            sTmp.push('</div>');
                            sTmp.push('<div class="entry">');
                                sTmp.push('<div class="imgDiv">');
                                sTmp.push('<div class="blob">');
                                    sTmp.push('<div class="text" style="color:' + revupConstants.color.titleText + '">OPPO</div>');
                                sTmp.push('</div>');
                                sTmp.push('<div class="msgDiv">');
                                    sTmp.push('Contributions were given to the opposition.');
                                sTmp.push('</div>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                }
            sTmp.push('</div>');
        sTmp.push('</div>');

        return sTmp.join('');
    } // buildLegendView

    /*
     *  Build the html for the page
     */
    function buildHtml(data, bDisplayHidden)
    {
        if (bDisplayHidden == undefined) {
            bDisplayHidden = true;
        }

        var sTmp = [];

        // add the list view
        sTmp.push('<div class="lstDiv">');
            sTmp.push(buildLstView(data));
        sTmp.push('</div>');

        // add the detail view
        sTmp.push('<div class="detailDiv">');
            sTmp.push(buildDetailView(data));
        sTmp.push('</div>');

        // add the legend
        sTmp.push(buildLegendView(data));

        return sTmp.join('');
    } // buildHtml

    /*
     * Loading Data Methods
     */
    let  loadRankingList = (data, bAddNextMarker = true) =>
    {
        var sTmp = [];

        // get the current partition
        var partition = templateData.partitionStart;

        // see if the more marks need to be added
        //let addMoreMarkerIndex = -1;
        //if (data.length >= pageSize) {
        //    addMoreMarkerIndex = nextPageMarkerIndex;
        //}

        // load the entries for the list
        for(var i = 0; i < data.length; i++) {
            var r = data[i];                // result
            var c = data[i].contact;        // contact for result
            var k = data[i].key_signals;    // key signals

            var bMaxedForCycle = false;
            var bDoNotContact = false;
            if (k && "giving_availability" in k && k.giving_availability ==  0) {
                bMaxedForCycle = true;
            }
            else if (k) {
                if (k.do_not_contact) {
                    bDoNotContact = true;
                }
            }

            var extraClass = [];
            if (bMaxedForCycle) {
                extraClass.push("maxedOut");
            }
            if (bDoNotContact) {
                extraClass.push('doNotContact')
            }
            if (i == nextPageMarkerIndex && bAddNextMarker) {
                extraClass.push('timeToAddMore');
            }

            if (extraClass.length > 0) {
                extraClass = ' ' + extraClass.join(' ');
            }
            else {
                extraClass = '';
            }
            var entryAttr = ' resultId="' + r.id + '" contactId="' + c.id + '"';
            let joinPermission = undefined;
            if (contactSetPerm.call_time != null && contactSetPerm.prospects != null) {
                joinPermission = 'call_time-prospects';
            }
            sTmp.push(revupListView.buildListEntry(extraClass, "", entryAttr, data[i], templateData.seatId, contactSetPerm, joinPermission));
        }

        // add the loading entry to the end
        if (bAddNextMarker) {
            sTmp.push('<div class="entry loadingMore" style="display:none">');
                sTmp.push('<div class="loadingMore"></div>')
            sTmp.push('</div>');
        }

        return sTmp.join('');
    } // loadRankingList

    function getData(data, section, field, subField, joinFields)
    {
        var sTmp = [];

        // get the sections
        var s = data[section];
        if (!s) {
            return sTmp;
        }

        // get the fields
        if (Array.isArray(field)) {
            for (var i = 0; i < field.length; i++) {
                if (s[field[i]]) {
                    if (subField) {
                        if ((Array.isArray(s[field[i]])) && (s[field].length > 0)) {
                            sTmp.push(s[field[i][0][subField]]);
                        }
                        else {
                            sTmp.push(s[field[i][subField]]);
                        }
                    }
                    else {
                        sTmp.push(s[field[i]]);
                    }
                }
            }

            // joinField than join and return a string
            if (joinFields) {
                sTmp = sTmp.join(joinFields);
            }
        }
        else {
            if (s[field]) {
                if (joinFields) {
                    if (subField) {
                        if ((Array.isArray(s[field[i]])) && (s[field].length > 0)) {
                            sTmp = s[field[i][0][subField]];
                        }
                        else {
                            //if (s[field].length > 0) {
                                sTmp = s[field][subField];
                            //}
                        }
                    }
                    else {
                        sTmp = s[field];
                    }
                }
                else {
                    if (subField) {
                        if ((Array.isArray(s[field])) && (s[field].length > 0)) {
                            sTmp.push(s[field][0][subField]);
                        }
                        else {
                            //if (s[field].length > 0) {
                                sTmp.push(s[field][subField]);
                            //}
                        }
                    }
                    else {
                        sTmp.push(s[field]);
                    }
                }
            }
        }

        return sTmp;
    } // getData

    /*
     * Details
     */
    let buildDetailNamePhotoSection = (data, bDoNotContact = false) => {
        let sTmp = [];

        if (data.first_name) {
            let tmp = {};
            tmp.contact = data;

            data = tmp;
        }

        // the sections to display
        displaySection = templateData.detailSection.displaySections;

        // photo section
        var photo = getAnalysisContactValue(displaySection.name.photoProfileLink, 'imgData', data);
        var profile = getAnalysisContactValue(displaySection.name.photoProfileLink, 'profileData', data);
        var hasPhotoClass = '';

        // get the profile url
        var profileUrl = undefined;
        if (profile && profile.length > 0) {
            // get the photo url
            if (displaySection.name.photoProfileLink.profileSrc) {
                for (var i = 0; i < profile.length; i++) {
                    if (profile[i].source == displaySection.name.photoProfileLink.profileSrc) {
                        profileUrl = profile[i].url;
                    }
                }
            }
            else {
                profileUrl = profile[0].url
            }
        }

        // get the photo url
        var photoUrl = undefined;
        if (photo && photo.length > 0) {
            // get the photo url
            for (var i = 0; i < photo.length; i++) {
                if (photo[i].source == displaySection.name.photoProfileLink.photoSrc) {
                    photoUrl = photo[i].url;
                }
            }

            if (!photoUrl) {
                photoUrl = photo[0].url
            }

            hasPhotoClass = ' hasPhoto';
            hasPhotoProfileClass = '';
            if (profileUrl) {
                hasPhotoProfileClass= ' hasProfile';
            }
        }


        if (photoUrl) {
            sTmp.push('<div class="photoDiv' + hasPhotoProfileClass + '">');
                var imgStyle = [];
                if (displaySection.name.photoProfileLink.photoWidth) {
                    imgStyle.push('width:' + displaySection.name.photoProfileLink.photoWidth);
                }
                if (displaySection.name.photoProfileLink.photoHeight) {
                    imgStyle.push('height:' + displaySection.name.photoProfileLink.photoHeight);
                }

                if (imgStyle.length > 0) {
                    imgStyle += ' style="' + imgStyle.join(';') + '"';
                }

                if (profileUrl) {
                    sTmp.push('<a href="' + profileUrl + '" target="_blank">');
                }

                sTmp.push('<img class="detailImg" src="' + photoUrl + '"' + imgStyle + '>')

                if (profileUrl) {
                    sTmp.push('</a>')
                }
            sTmp.push('</div>');
        }

        // get the email
        // get email data
        var email = getAnalysisContactValue(displaySection.name.email, data);
        if ((email) && (email.length > 0)) {
            emailBtnExtraClass = '';
        }

        // Cleanup the emails to remove duplicates
        var tmp_emails = [];
        if (typeof email != 'string') {
            var emails = {};
            for (var i = 0; i < email.length; i++) {
                var e = email[i];
                var lower_e = e.address.toLowerCase();
                if (emails.hasOwnProperty(lower_e)){
                    continue;
                } else {
                    emails[lower_e] = true;
                    tmp_emails.push(e);
                }
            }
        }
        email = tmp_emails;

        // build the list of address
        var emailLst = [];
        if (email) {
            if (typeof email == 'string') {
                emailLst.push(email);
            }
            else {
                for (var i = 0; i < email.length; i++) {
                    emailLst.push(email[i].address);
                }
            }
        }


        // name ,email and phone
        sTmp.push('<div class="nameEmailDiv' + hasPhotoClass + '">');
            // first name
            var firstName = getAnalysisContactValue(displaySection.name.firstName, data);
            if (firstName) {
                sTmp.push('<div class="name">' + firstName + '</div>');
            }

            // last name with degree
            var lastName = getAnalysisContactValue(displaySection.name.lastName, data);
            var deg = getAnalysisContactValue(displaySection.name.degree, data);
            if (deg != undefined) {
                var sep = displaySection.name.degree.seperator ? displaySection.name.degree.seperator : ' ';
                lastName += sep + deg;
            }
            if (lastName) {
                if (!photoUrl && profileUrl) {
                    sTmp.push('<div class="nameShorter">' + lastName + '</div>');
                    sTmp.push('<a href="' + profileUrl + '" target="_blank">');
                        sTmp.push('<div class="profileImg"></div>');
                    sTmp.push('</a>');
                }
                else {
                    sTmp.push('<div class="name">' + lastName + '</div>');
                }
            }

            // email
            if (email && !bDoNotContact) {
                if (Array.isArray(email) && email.length > 0) {
                    var max = maxDetailEmailAddress;
                    if (displaySection.name.email.maxDisplay) {
                        max = displaySection.name.email.maxDisplay;
                    }
                    var numAddress = Math.min(email.length, max);
                    for (var i = 0; i < numAddress; i++) {
                        var e = email[i];
                        if (displaySection.name.email.displayField) {
                            e = e[displaySection.name.email.displayField]
                        }
                        if (e) {
                            sTmp.push('<div class="email"><a class="mailAnchor" href="mailto:' + e + '">' + e + '</a></div>');
                        }
                    }
                }
                else {
                    sTmp.push('<div class="email"><a class="mailAnchor" href="mailto:' + email + '">' + email + '</a></div>');
                }
            }

            // phone
            var phone = getAnalysisContactValue(displaySection.name.phone, data);
            if (phone && !bDoNotContact) {
                var p = '';
                if (Array.isArray(phone) && phone.length > 0) {
                    if (displaySection.name.phone.bfirstNoneBlank) {
                        for (var i = 0; i < phone.length; i++) {
                            if (phone[i]) {
                                p = phone[i]
                            }
                        }
                    }
                    else if (displaySection.name.phone.firstOfType) {
                        labelKey = displaySection.name.phone.labelKey ? displaySection.name.phone.labelKey : 'label';
                        valKey = displaySection.name.phone.valKey ? displaySection.name.phone.valKey : 'number';
                        for (var ii = 0; ii < displaySection.name.phone.firstOfType.length; ii++) {
                            firstOfType = displaySection.name.phone.firstOfType[ii].toLowerCase();

                            // look for type
                            for (var i = 0; i < phone.length; i++) {
                                var l = phone[i][labelKey].toLowerCase();
                                var v = phone[i][valKey];

                                // match
                                if (l == firstOfType) {
                                    p = v;
                                    break;
                                }
                            }

                            // found
                            if (p) {
                                break;
                            }
                        }

                        // non of type so get the first
                        if (!p) {
                            p = phone[0][valKey];
                        }
                    }
                }

                // display the value
                if (p) {
                    var p = revupUtils.getFormattedPhone(p);
                    sTmp.push('<div class="phone">' + p + '</div>');
                }

                if (bDoNotContact) {
                    sTmp.push('<div class="doNotContact">Do Not Contact</div>');
                }
            }
        sTmp.push('</div>');

        return(sTmp.join(''));
    } // buildDetailNamePhotoSection

    function loadDetailData(data)
    {
        var sTmp = [];

        // get the data sections
        var k = data.key_signals;
        var c = data.contact;
        var f = data.features;
        var s = data.score;

        // the sections to display
        displaySection = templateData.detailSection.displaySections;

        if (displaySection.prospectDetailsBtn) {
        }
        if ((displaySection.callSheetBtn) && (displaySection.callSheetBtn.display)) {
        }

        var sendEmailBtnExtraClass = "";
        var prospectDetailsBtnExtraClass = "";
        var callSheetBtnExtraClass = "";
        var emailBtnExtraClass = ' disabled';
        let editContactBtnExtraClass = "";
        let deleteContactBtnExtraClass = "";

        // get email data
        var email = getAnalysisContactValue(displaySection.name.email, data);
        if ((email) && (email.length > 0)) {
            emailBtnExtraClass = '';
        }

        // Cleanup the emails to remove duplicates
        var tmp_emails = [];
        if (typeof email != 'string') {
            var emails = {};
            for (var i = 0; i < email.length; i++) {
                var e = email[i];
                var lower_e = e.address.toLowerCase();
                if (emails.hasOwnProperty(lower_e)){
                    continue;
                } else {
                    emails[lower_e] = true;
                    tmp_emails.push(e);
                }
            }
        }
        email = tmp_emails;

        // build the list of address
        var emailLst = [];
        if (email) {
            if (typeof email == 'string') {
                emailLst.push(email);
            }
            else {
                for (var i = 0; i < email.length; i++) {
                    emailLst.push(email[i].address);
                }
            }
        }

        // see if this detail data has a do not contact
        var bDoNotContact = false;
        if (k.do_not_contact) {
            bDoNotContact = true;

            emailBtnExtraClass = " disabled";
            callSheetBtnExtraClass = " disabled";
        }

        /*
        // at this time we are not disabling the delete button
        if (bSharedContact) {
            deleteContactBtnExtraClass = ' disabled';
        }
        */

        sTmp.push('<div class="rankingDetailHeaderDiv">');
            sTmp.push('<div class="btnDiv sendMailBtn' + emailBtnExtraClass + '" data-email="' + emailLst.join(';;') + '">');
                sTmp.push('<div class="text">Send Email</div>');
                sTmp.push('<div class="icon icon-email"></div>');
            sTmp.push('</div>');
            sTmp.push('<div class="sepBar"></div>');
            sTmp.push('<div class="btnDiv deleteContactBtn' + deleteContactBtnExtraClass + '">');
                sTmp.push('<div class="text">Delete</div>');
                sTmp.push('<div class="icon icon-delete-contact"></div>');
            sTmp.push('</div>');
            sTmp.push('<div class="sepBar"></div>');
            // sTmp.push('<div class="btnDiv editContactBtn' + editContactBtnExtraClass + '">');
            //     sTmp.push('<div class="text">Edit</div>');
            //     sTmp.push('<div class="icon icon-edit-contact"></div>');
            // sTmp.push('</div>');
            // sTmp.push('<div class="sepBar"></div>');
            sTmp.push('<div class="btnDiv prospectDetailsBtn' + prospectDetailsBtnExtraClass + '">');
                sTmp.push('<div class="text">More</div>');
                sTmp.push('<div class="icon icon-more-details"></div>');
            sTmp.push('</div>');

            sTmp.push('<div class="sepBar"></div>');
            if (features.CALL_SHEET_EXPORT_RANKING) {
                sTmp.push('<div class="btnDiv callSheetBtn' + callSheetBtnExtraClass + '">');
            }
            else {
                sTmp.push('<div class="btnDiv callSheetBtn disabled' + callSheetBtnExtraClass + '">');
            }
                sTmp.push('<div class="text">Call Sheet</div>');
                sTmp.push('<div class="icon icon-callsheet"></div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        sTmp.push('<div class="rankingDetailDiv">');
            if ((displaySection.name) && (displaySection.name.display)) {
                // top section
                sTmp.push('<div class="topSection">');
                    // add in the contact info
                    sTmp.push('<div class="namePhotoDiv">')
                        sTmp.push(buildDetailNamePhotoSection(data, bDoNotContact));
                    sTmp.push('</div>')

                    // score section
                    sTmp.push('<div class="scoreSectionDiv">');
                    // score div
                    var extraClass = "";
                    if (!settingsPoliticalColor) {
                        extraClass = " hideColor";
                    }

                    // get the score
                    var scoreNum = getAnalysisContactValue(displaySection.name.score, data);
                    var formatedScore = scoreFormat(scoreNum);
                    if (formatedScore !== "") {
                        if (displaySection.name.scoreColor) {
                            var scoreColor = getAnalysisContactValue(displaySection.name.scoreColor, data);
                            extraClass += ' ' + scoreColorClass(scoreColor, formatedScore);
                        }
                        var style = '';
                        if ((displaySection.name.score.colorStyle) && (formatedScore > 0)) {
                            if ((currentSkin.colors) && currentSkin.colors['scoreHighlightColor']) {
                                // look for a background color rule
                                colorRules = currentSkin.colors['scoreHighlightColor'];
                                for (var cr = 0; cr < colorRules.length; cr++) {
                                    if (colorRules[cr].colorType == "background") {
                                        style = 'background:' + colorRules[cr].colorVal + ';';
                                    }
                                }
                            }
                            else {
                                style += displaySection.name.score.colorStyle;
                            }
                        }
                        sTmp.push('<div class="scoreDiv' + extraClass + '" style="' + style + '">');
                            sTmp.push('<div class="text">' + formatedScore + '</div>');
                        sTmp.push('</div>');
                    }
                    sTmp.push('</div>');
                sTmp.push('</div>');
            }

            if (debugMode) {
              sTmp.push('<div class="debugSection">');
                sTmp.push('<span>Contribution records found as a result of name/alias expansion: ' + (!!k.expansion_hit) + '</span><br/>');
                if ('gender_boost_multiplier' in k) {
                  sTmp.push('<span>Gender boost multiplier: ' + k.gender_boost_multiplier + '</span><br/>');
                }
                if ('ethnicity_boost_multiplier' in k) {
                  sTmp.push('<span>Ethnicity boost multiplier: ' + k.ethnicity_boost_multiplier + '</span><br/>');
                }
                if ('gender' in k) {
                  sTmp.push('<span>Likely gender: ' + k.gender + '</span><br/>');
                }
                if ('given_name_origin' in k) {
                  var origins = [];
                  for (var origin in k.given_name_origin) {
                      // Filter out given name origins that have a false value
                      if (k.given_name_origin.hasOwnProperty(origin) && k.given_name_origin[origin]) {
                          origins.push(origin);
                      }
                  }
                  sTmp.push('<span>Given name language origins: ' + origins.join(', ') + '</span><br/>');
                }
                if ('family_name_ethnicity' in k) {
                  var ethnicities = [];
                  for (var ethnicity in k.family_name_ethnicity) {
                      // Filter out ethnicities with a population density lower than 50%
                      if (k.family_name_ethnicity.hasOwnProperty(ethnicity) && k.family_name_ethnicity[ethnicity] >= 50) {
                          ethnicities.push(ethnicity);
                      }
                  }
                  sTmp.push('<span>Distribution of ethnicities for family name: ' + ethnicities.join(', ') + '</span><br/>');
                }
              sTmp.push('</div>');
            }

            // contact details
            if ((displaySection.contactDetails) && (displaySection.contactDetails.display)) {
                var f = undefined;
                var d = undefined;
                var s = data[displaySection.contactDetails.section];
                if (s) {
                    f = s[displaySection.contactDetails.field];
                    if (f) {
                        if (displaySection.contactDetails.data) {
                            sTmp.push('<div class="contactDetailsSection">');

                            for (var dataIndex = 0; dataIndex < displaySection.contactDetails.data.length; dataIndex++) {
                                for (var i = 0; i < f.length; i++) {
                                    if (f[i].signal_set_title == displaySection.contactDetails.data[dataIndex].signalSet) {
                                        //f = f[i][displaySection.contactDetails.data[dataIndex].subField];
                                        //d = f[0];
                                        d = f[i][displaySection.contactDetails.data[dataIndex].subField][0];

                                        if (d && (displaySection.contactDetails.data[dataIndex].type == 'attr')) {
                                            var dis = displaySection.contactDetails.data[dataIndex].attr;
                                            if (dis.length > 0) {
                                                    for(var ii = 0; ii < dis.length; ii++) {
                                                        var v = d[dis[ii].key];
                                                        var l = dis[ii].label;
                                                        // make sure there is a value and label
                                                        if (v && l) {
                                                            sTmp.push('<div class="detailEntry">');
                                                                sTmp.push('<div class="textLabel">' + l + '</div>');
                                                                sTmp.push('<div class="textVal">' + v + '</div>');
                                                            sTmp.push('</div>');
                                                        }
                                                    }
                                            }
                                        }
                                        else if (d && (displaySection.contactDetails.data[dataIndex].type == 'activity')) {
                                            var attrData = f[i][displaySection.contactDetails.data[dataIndex].subField];
                                            if (displaySection.contactDetails.data[dataIndex].bDeDup) {
                                                attrData = revupUtils.dedup(attrData, function(a) {
                                                    return a.activity
                                                });
                                            }
                                            for (var ii = 0; ii < attrData.length; ii++) {
                                                var v = attrData[ii][displaySection.contactDetails.data[dataIndex].key];

                                                if (v) {
                                                    sTmp.push('<div class="detailEntry">');
                                                        var label = "";
                                                        if (ii == 0) {
                                                            label = displaySection.contactDetails.data[dataIndex].label + ':';
                                                        }

                                                        if (ii < (attrData.length - 1)) {
                                                            v += ',';
                                                        }

                                                        sTmp.push('<div class="textLabel2">' + label + '</div>');
                                                        sTmp.push('<div class="textVal">' + v + '</div>');
                                                    sTmp.push('</div>');
                                                }
                                            }

                                        }
                                    }
                                }
                            }

                            sTmp.push('</div>');
                        }
                    }
                }

            }

            if (displaySection.contribution) {
                // contribution section
                var totalGivingCand = '---';
                var givingAvailCand = '---';
                var candContLst = [];
                var candData = [];
                sTmp.push('<div class="contributionSection">');
                    // get the clients feature data
                    for(var key in data.features) {
                        if (key.startsWith("Client Matches", true)) {
                            var cd = data.features[key];

                            for (var sub_key in cd) {
                                if (cd[sub_key].length > 0) {
                                    cc = cd[sub_key][0];

                                    if (cc.giving != undefined) {
                                        totalGivingCand = cc.giving;
                                    }
                                    if (cc.giving_availability != undefined) {
                                        givingAvailCand = cc.giving_availability
                                    }

                                    if ((cc.match != undefined) && (cc.match.length > 0)) {
                                        candContLst = cc.match;
                                        candData[key] = cc;
                                    }
                                }
                            }
                        }
                    }

                    // if giving availability display
                    if (typeof givingAvailCand == 'string') {
                        givingAvailCand = givingAvailCand.toLowerCase();
                    }
                    if ((typeof givingAvailCand == 'number') || ((givingAvailCand != '') && (givingAvailCand != 'unlimited'))) {
                        sTmp.push('<div class="givingAvailDiv">');
                            // stylize if no more giving
                            var amtStyle = "";
                            //if ((givingAvailCand == '---') || (givingAvailCand == 0) || (givingAvailCand == "0")) {
                            //    amtStyle = " noMore";
                            //}

                            if (givingAvailCand != '---') {
                                givingAvailCand = '$' + revupUtils.commify(parseInt(givingAvailCand, 10));
                            }
                            sTmp.push('<div class="amt' + amtStyle + '">' + givingAvailCand + '</div>');
                            if (givingAvailCand == 0) {
                                sTmp.push('<div class="text">in giving ability</div>');
                            }
                            else {
                                sTmp.push('<div class="text">in giving availability</div>');
                            }
                        sTmp.push('</div>');
                    }

                    function displayKeyContrib(contribs, sectionType, sectionName, candName)
                    {
                        if (!bTitleDisplayed) {
                            sTmp.push('<div class="contribTitle">Contribution</div>');
                            bTitleDisplayed = true;
                        }

                        sTmp.push('<div class="contribSubTitle">' + sectionName + '</div>');
                        sTmp.push('<div class="contribSection ' + sectionType + '">')
                            for(var contribKey in contribs) {
                                sTmp.push('<div class="contribDiv">');
                                    sTmp.push('<div class="contribLst">');

                                        var contribLst = contribs[contribKey].match;
                                        for(var i = 0; i < contribLst.length; i++) {
                                            if ((maxDetailContribCandidate != undefined) && (i >= maxDetailContribCandidate)) {
                                                break;
                                            }

                                            sTmp.push('<div class="entry">');

                                                // if the first line display the name
                                                if (i == 0) {
                                                    // key contributor
                                                    if (contribs[contribKey].is_ally) {
                                                        sTmp.push('<div class="contribKeyContrib"></div>');
                                                    }
                                                    else {
                                                        sTmp.push('<div class="contribNoKeyContrib"></div>');
                                                    }
                                                    if (candName) {
                                                        sTmp.push('<div class="contribName">' + candName + '</div>');
                                                    }
                                                    else {
                                                        sTmp.push('<div class="contribName">' + contribKey + '</div>');
                                                    }
                                                }
                                                else {
                                                    // do not display the name
                                                    sTmp.push('<div class="contribNoKeyContrib"></div>');
                                                    sTmp.push('<div class="contribName"></div>');
                                                }

                                                // amount
                                                var amt = '---';
                                                if ((contribLst[i].amount != undefined) && (contribLst[i].amount != '')) {
                                                    var amt = parseInt(contribLst[i].amount, 10);
                                                    amt = "$" + revupUtils.commify(amt);
                                                }
                                                sTmp.push('<div class="contribAmount">' + amt + '</div>');

                                                // date
                                                var date = '---';
                                                if ((contribLst[i].date != undefined) && (contribLst[i].date != '')) {
                                                    date = revupUtils.formatDate(contribLst[i].date);
                                                }
                                                sTmp.push('<div class="contribDate">' + date + '</div>');
                                            sTmp.push('</div>');
                                        }
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            }
                        sTmp.push('</div>');
                    } // displayKeyContrib

                    // contributions to Client
                    var bTitleDisplayed = false;
                    if (candContLst.length > 0) {
                        sTmp.push('<div class="contribTitle">Contribution</div>');
                        bTitleDisplayed = true;

                        displayKeyContrib(candData, 'myCamp', 'My Campaign', templateData.candidateName)
                    }

                    // Key Contributuions - walk the features looking for key contribution
                    for(var key in f) {
                        if (!key.startsWith(keyContributionsLabel, true)) continue;

                        let kc_sub_key = Object.keys(f[key])[0];
                        if(f[key][kc_sub_key].length > 0) {
                            var keyContribs = f[key][kc_sub_key][0];
                            var ally = [];
                            var allyLength = 0;
                            var oppo = [];
                            var oppoLength = 0;
                            for(var contribKey in keyContribs) {
                                if (keyContribs[contribKey].is_ally) {
                                    ally[contribKey] = keyContribs[contribKey];
                                    allyLength += 1;
                                }
                                else {
                                    oppo[contribKey] = keyContribs[contribKey];
                                    oppoLength += 1;
                                }
                            }

                            if (allyLength > 0) {
                                if (bTitleDisplayed) {
                                    sTmp.push('<div class="contribSepDiv"></div>')
                                }
                                displayKeyContrib(ally, 'correlatedCamp', 'Correlated Campaigns');
                            }

                            if (oppoLength > 0) {
                                if (bTitleDisplayed) {
                                    sTmp.push('<div class="contribSepDiv"></div>')
                                }
                                displayKeyContrib(oppo, 'oppoCamp', 'Opposition');
                            }
                        }
                    }

                    // features data to display
                    /*
                    for(var d = 0; d < detailFeatures.length; d++) {
                        for(var key in data.features) {
                            if (key.startsWith(detailFeatures[d].key, true) && f[key].length > 0) {
                                sTmp.push(detailContribGrp(detailFeatures[d].label, f[key][0], detailFeatures[d].maxDisplay));
                            }
                        }
                    }
                    */
                sTmp.push('</div>');
            }

            // academic contributions
            if ((displaySection.academicContributions) && (displaySection.academicContributions.display)) {
                // get to data
                var sf = undefined;
                var f = undefined;
                var s = data[displaySection.academicContributions.section];
                if (s) {
                    sf = s[displaySection.academicContributions.field];
                    if ((sf) && (sf.length > 0) && (displaySection.academicContributions.subField)) {
                        f = sf[displaySection.academicContributions.subField];
                        if (!f) {
                            f = sf[0][displaySection.academicContributions.subField];
                        }
                    }
                }

                if (f) {
                    // sort the data by the orgBy field
                    f.sort(function(a, b) {
                        if (a[displaySection.academicContributions.orgBy] > b[displaySection.academicContributions.orgBy]) {
                            return 1;
                        }
                        else if (a[displaySection.academicContributions.orgBy] < b[displaySection.academicContributions.orgBy]) {
                            return -1;
                        }
                        else {
                            if (displaySection.academicContributions.thenBy) {
                                var da = Date.parse(a[displaySection.academicContributions.thenBy]);
                                var db = Date.parse(b[displaySection.academicContributions.thenBy]);
                                if (da < db) {
                                    return 1;
                                }
                                else if (da > db) {
                                    return -1;
                                }
                            }

                            return 0;
                        }
                    })

                    sTmp.push('<div class="contributionSection academic">');
                        if (displaySection.academicContributions.title) {
                            sTmp.push('<div class="contribTitle">');
                                sTmp.push(displaySection.academicContributions.title);
                            sTmp.push('</div>')
                        }

                        var currentGrpName = "";
                        var numDisplayed = 0
                        for (var i = 0; i < f.length; i++) {
                            var newGrp = false;
                            if (f[i][displaySection.academicContributions.orgBy] != currentGrpName) {
                                // close the old section if the currentGrpName is not blank
                                if (currentGrpName != "") {
                                            sTmp.push('</div>');
                                        sTmp.push('</div>');
                                    sTmp.push('</div>');
                                }

                                newGrp = true;
                                currentGrpName = f[i][displaySection.academicContributions.orgBy];
                                numDisplayed = 0

                                sTmp.push('<div class="contribSection">');
                                    sTmp.push('<div class="contribDiv">');
                                        sTmp.push('<div class="contribLst">');
                            }

                            if ((!displaySection.academicContributions.maxOrgByDisplay) || (numDisplayed < displaySection.academicContributions.maxOrgByDisplay)) {
                                sTmp.push('<div class="entry">');
                                    // section name
                                    if (newGrp) {
                                        sTmp.push('<div class="contribName" title="' + f[i][displaySection.academicContributions.orgBy] + '">');
                                            sTmp.push(f[i][displaySection.academicContributions.orgBy]);
                                        sTmp.push('</div>')
                                    }
                                    else {
                                        sTmp.push('<div class="contribName"></div>')
                                    }


                                    // amount
                                    // var amt = '---';
                                    var amt = '&nbsp;';
                                    if (f[i].amount)  {
                                        var amt = parseInt(f[i].amount, 10);
                                        amt = "$" + revupUtils.commify(amt);
                                    }
                                    sTmp.push('<div class="contribAmount">' + amt + '</div>');

                                    // date
                                    var date = '---';
                                    if (f[i].date) {
                                        date = revupUtils.formatDate(f[i].date);
                                    }
                                    sTmp.push('<div class="contribDate">' + date + '</div>');
                                sTmp.push('</div>');
                            }

                            numDisplayed++;
                        }

                        // if there was data close the last group
                        if (currentGrpName != "") {
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');
                        }
                    sTmp.push('</div>');
                }
                else {
                    sTmp.push('<div class="contributionSection academic">');
                        if (displaySection.academicContributions.title) {
                            sTmp.push('<div class="contribTitle">');
                                sTmp.push(displaySection.academicContributions.title);
                            sTmp.push('</div>')
                        }

                        if (displaySection.academicContributions.noContributions) {
                            sTmp.push('<div class="noContrib">');
                                sTmp.push(displaySection.academicContributions.noContributions);
                            sTmp.push('</div>')
                        }
                        else {
                            sTmp.push('<div class="noContrib">');
                                sTmp.push("No Contribitions");
                            sTmp.push('</div>')
                        }
                    sTmp.push('</div>');
                }
            }

            // giving history
            if ((displaySection.givingHistoryGraph) && (displaySection.givingHistoryGraph.display)) {
                sTmp.push('<div class="givingHistorySection">');
                    sTmp.push(histogram(data));
                sTmp.push('</div>');
            }

            // contribution graph section
            if ((displaySection.contributionGraph) && (displaySection.contributionGraph.display)) {
                sTmp.push('<div class="contributionGraphSection">');
                    if (displaySection.contributionGraph.title) {
                        sTmp.push('<div class="title">' + displaySection.contributionGraph.title + '</div>');
                    }

                    sTmp.push('<div class="graphDiv">');
                        sTmp.push('<div class="textLabel textLeft">');
                            sTmp.push(displaySection.contributionGraph.leftLabel);
                        sTmp.push('</div>');

                        for (var i = 0; i < displaySection.contributionGraph.numDots; i++) {
                            var highlight = "";
                            if (i < 3) {
                                highlight = " highlight";
                            }
                            sTmp.push('<div class="dot' + highlight + '"></div>');
                        }

                        sTmp.push('<div class="textLabel textRight">');
                            sTmp.push(displaySection.contributionGraph.rightLabel);
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            }

            // pie graphs
            if ((displaySection.pieGraph) && (displaySection.pieGraph.display)) {
                sTmp.push('<div class="graphsSection">');
                var s = data;
                if (displaySection.pieGraph.trendingSection) {
                    s = data[displaySection.pieGraph.trendingSection];
                }

                // see if there was any political spending
                if (s[displaySection.pieGraph.politicalAmount] == 0) {
                    sTmp.push('<div class="msgText">No Political Spending</div>');
                }
                else {
                        sTmp.push('<div class="subSections contributionSpreadDiv">');
                            sTmp.push('<div class="sectionTitle">Contribution Spread in Dollars</div>');
                            sTmp.push('<div class="pieDivOuter">');
                                sTmp.push('<div class="pieDivInner">');
                                    sTmp.push('<canvas class="contributionSpreadCanvas" width="100" height="100"></canvas>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');

                        sTmp.push('<div class="subSections trendChartDiv">');
                            sTmp.push('<div class="sectionTitle">Trending</div>');

                            sTmp.push('<div class="trendingDiv">');
                                sTmp.push('<div class="labelDiv">')
                                    sTmp.push('<div class="leftLabel">Dem</div>');
                                    sTmp.push('<div class="rightLabel">Rep</div>')
                                sTmp.push('</div>');
                                sTmp.push('<div class="graphDiv">');
                                    sTmp.push('<div class="circle leftCircle political-spectrum-bar-democrat"></div>');
                                    sTmp.push('<div class="circle rightCircle political-spectrum-bar-republican"></div>');
                                    sTmp.push('<div class="bar"></div>');
                                    var sliderPos = 21 + Math.round(86 * (1.0 - s[displaySection.pieGraph.trendingField]));
                                    sTmp.push('<div class="slider" style="left:' + sliderPos + 'px;">');
                                        sTmp.push('<div class="icon icon-pointer"></div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');
                            sTmp.push('<div class="imgFooter">');
                                sTmp.push('<img src="../static/img/trendingChartBase.png">');
                            sTmp.push('</div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                }
            }
        sTmp.push('</div>')

        return sTmp.join('');
    } // loadDetailData

    function loadingDetailError($cDiv, errorCode)
    {
        var sTmp = [];
        sTmp.push('<div class="errorMsg" style="height:250px;">');
            sTmp.push('<div class="msg">');
                if (errorCode == 404) {
                    sTmp.push("This contact may have been deleted from this list.<br><br>Reload the list to get the most up to date contacts")
                }
                else {
                    sTmp.push("Unable to load Detail Data at this time<br><br>Try again later");
                }
            sTmp.push('</div>')
        sTmp.push('</div>')

        // load
        $cDiv.html(sTmp.join(''));
    } // loadingDetailError

    function loadDetail(contactId, resultId)
    {
        // get the data and loading div's
        var $loadingDiv = $('.rankingNewLayout .detailDiv .rankingDetailsDetailLoading');
        var $detailDiv = $('.rankingNewLayout .detailDiv .rankingDetailsDetailDiv');
        $loadingDiv.show(1);
        $detailDiv.hide();

        // clear the detail data
        detailData = {};

        var urlArgs = [];
        let activeContactSetId = contactSetActiveId.split(';;')[0];
        var url2 = templateData.contactDetailUrlNew.replace('seatId', templateData.seatId)
                                                   .replace('contactSetId', activeContactSetId)
                                                   .replace("contactId", contactId);
        urlArgs.push('partition=' + activePartition);
        if (getUrlArgs.analysis !== undefined) {
            urlArgs.push("analysis=" + getUrlArgs.analysis);
        }

        setTimeout(function() {
            $.ajax({
                url: url2 + '?' + urlArgs.join('&'),
                type: "GET",
            })
            .done(function(r) {
                // save the detail data
                detailData = r;

                // load the details page
                $detailDiv.html(loadDetailData(r));

                $('.detailImg', $detailDiv).on('error',  function(e) {
                    var $nameDiv = $('.nameEmailDiv', $detailDiv);
                    $nameDiv.removeClass('hasPhoto')
                    $(this).hide();
                })


                // update the attribute on the detail div
                $detailDiv.attr("analysisId", analysisId);
                $detailDiv.attr("resultId", resultId);
                $detailDiv.attr("contactId", contactId);
                $detailDiv.attr("activePartition", activePartition);

                // load the pie chart
                pieChart(r);
            })
            .fail(function(r) {
                loadingDetailError($detailDiv, r.status);

                revupUtils.apiError('Ranking Details', r, 'Get Contact Details');
            })
            .always(function(r) {
                $loadingDiv.hide();
                $detailDiv.show();
                if (!bAutoScrollDetails) {
                    $("html, body").scrollTop(0);
                }
                $detailDiv.show(1, function() {
                    // see if any fields need top tip if there are ellipsis's
                    $('.rankingDetailDiv .topSection .name', $detailDiv).fieldEllipsis();
                    $('.rankingDetailDiv .topSection .email', $detailDiv).fieldEllipsis();
                    $('.rankingDetailDiv .topSection .phone', $detailDiv).fieldEllipsis();
                    $('.rankingDetailDiv .contributionSection .contribDiv .contribTitle').fieldEllipsis();
                    $('.rankingDetailDiv .contributionSection .contribDiv .contribLst .entry .contribName').fieldEllipsis();
                });
            });
        }, 1);

    } // loadDetail

    /*
     * Delete contact
     */
    let deleteContact = (contactId, contactName, bJustRemove = false) => {
        let $delInfoDlg = $();

        let removeAndFetchReplacement = (contactId) => {
            // get the entry to remove
            let $removeEntry = $('.rankingDetailsLstDiv .entry[contactId="' + contactId + '"]');
            if ($removeEntry.length > 0) {
                //remove from the ranking list
                $removeEntry.remove();
            }

            // see if the load more marker is visible
            let bLoadFullPage = false;
            let $timeToAddMore = $('.entry.timeToAddMore', $rankingLst);
            if ($timeToAddMore.length > 0) {
                let rankLstDivPos = $rankingLstDiv.position();
                let rankLstHeight = $rankingLstDiv.height();
                let lstHeight = $rankingLst.height();

                if ($timeToAddMore.length >= 0) {
                    let entryPos = $timeToAddMore.position();
                    if ((entryPos.top - rankLstDivPos.top) < rankLstHeight) {
                        bLoadFullPage = true;
                    }
                }
            }

            // update the counts
            let numEntries = parseInt($rankingLstDiv.attr('numEntries'), 10);
            let numLoaded = parseInt($rankingLstDiv.attr('numEntriesLoaded'), 10);
            $rankingLstDiv.attr('numEntries', numEntries - 1);
            $rankingLstDiv.attr('numEntriesLoaded', numLoaded - 1);
            $rankingLstDiv.attr('maxPages', Math.ceil((numEntries - 1) / pageSize));

            // see if a replacement needs to be loaded
            if (numLoaded < numEntries) {
                // fetch a single entry and append to the end
                fetchNextChunk(true, bLoadFullPage);
            }
        } // removeAndFetchReplacement

        let doDeleteContact = (contactId, bJustGetNext = false) => {
            let $entry = $('.rankingDetailsLstDiv .entry[contactId="' + contactId + '"]', $);

            // if being delete don't try again
            if ($entry.hasClass('beingDeleted')) {
                return;
            }

            // mark the contact as ranking
            $entry.removeClass('selected')
                  .addClass('beingDeleted');
            $('.nameBtn', $entry).html($('.nameBtn', $entry).html() + ' (Being Deleted)');

            // disable the delete button
            $('.newRankingDiv .detailDiv .rankingDetailsDetailDiv .deleteContactBtn').removeClass('selected')
                                                                                     .addClass('disabled');

            // get next entry
            let $nextEntry = $entry.next();

            // select the next
            let bLoadDetails = false;
            while (true) {
                if (($nextEntry.length == 0) || $nextEntry.hasClass('loadingMore')) {
                    break;
                }

                // if it is beingDeleted then skip to the next
                if ($nextEntry.hasClass('beingDeleted')) {
                    $nextEntry = $nextEntry.next();

                    continue;
                }

                // select the next entry if there is one
                $nextEntry.addClass('selected');

                // load the details
                bLoadDetails = true;

                // done
                break;
            }

            // select the next entry if there is one
            if (bLoadDetails) {
                $nextEntry.addClass('selected');

                // load the details
                let newContactId = $nextEntry.attr('contactId');
                let newResultId = $nextEntry.attr('resultId');
                loadDetail(newContactId, newResultId);
            }
            else {
                // nothing selected
                var $detailDiv = $('.rankingNewLayout .detailDiv .rankingDetailsDetailDiv');
                $detailDiv.hide();
            }

            // just remove and update the selected entry
            if (bJustGetNext) {
                return;
            }

            // see what will be deleted
            let activeContactSetId = contactSetActiveId.split(';;')[0];
            let url = '';
            if (activeTab == 'personalTab') {
                url = templateData.userContact;
            }
            else if (activeTab == 'accountTab') {
                url = templateData.accountContact;
            }
            else {
                console.error('Delete Contact - Unknown Tab');
                return;
            }
            url = url.replace("contactId", contactId);

            // display the waiting spinner
            $('.waitingDiv', $delInfoDlg).show();

            // do the delete
var startTime = new Date().getTime();
            var deleteContactId = contactId;
            $.ajax({
                url: url,
                type: 'DELETE',
            })
            .done(function(r) {
console.log('delete time(done): ' +  (new Date().getTime() - startTime) + ' ms')
                // remove the entry and fetch a replacement
                removeAndFetchReplacement(deleteContactId);

                // close the dialog
                $delInfoDlg.revupConfirmBox('close');
            })
            .fail(function(r) {
                console.error('Unable to delete contact: ' + deleteContactId + ", reasult: ", r);
                revupUtils.apiError('Delete Contact', r, 'Unable to delete contact: ' + contactId);
            })
            .always(function() {
console.log('delete time(always): ' +  (new Date().getTime() - startTime) + ' ms')
            })
        } // doDeleteContact

        // see if a raw delete
        if (bJustRemove) {
            doDeleteContact(contactId, true);
            removeAndFetchReplacement(contactId);

            return;
        }

        // see what will be deleted
        let activeContactSetId = contactSetActiveId.split(';;')[0];
        let url = '';
        if (activeTab == 'personalTab') {
            url = templateData.userContact;
        }
        else if (activeTab == 'accountTab') {
            url = templateData.accountContact;
        }
        else {
            console.error('Delete Contact - Unknown Tab');
            return;
        }
        url = url.replace("contactId", contactId);
        url += '?dryrun=true'

        let sTmp = [];

        sTmp.push('<div class="delConfirmDlg">');
            sTmp.push('<div class="waitingDiv loadingDiv">');
                sTmp.push('<div class="loading"></div>');
                sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
            sTmp.push('</div>')
            sTmp.push('<div class="content">');
                sTmp.push('<div class="fetchDeleteInfo">');
                    sTmp.push('<div class="msg">Collecting contact data for deletion</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>')

        $delInfoDlg = $('body').revupConfirmBox({
            okBtnText:          'Delete',
            headerText:         'Delete Contacts',
            msg:                sTmp.join(''),
            extraClass:         'deleteContactConfirm',
            autoCloseOk:        false,

            okHandler:          function () {
                                    doDeleteContact(contactId);
            },
        })

        // display the loading spinner
        $('.waitingDiv', $delInfoDlg).show();

        $.ajax({
            url: url,
            type: "DELETE",
        })
        .done(function(r) {
            let sTmp = [];
            sTmp.push('<div class="leftSide">');
                sTmp.push('<div class="warningIcon icon icon-warning"></div>');
            sTmp.push('</div>');
            sTmp.push('<div class="rightSide">');
                sTmp.push('<div class="msg">');
                    sTmp.push('Are you sure you want to remove the contact ');
                    sTmp.push('<div class="msgBold">' + contactName + '?' + '</div>');
                sTmp.push('<div class="msg newParagraph">');
                    sTmp.push('This will ');
                    sTmp.push('<div class="boldRed">permanently</div>');
                    sTmp.push(' remove:')
                sTmp.push('</div>');

                sTmp.push('<div class="dryRunResults">');
                    sTmp.push('<div class="entry">');
                        sTmp.push('<div class="val">' + r.Contacts + '</div>');
                        sTmp.push('<div class="eVal">contact</div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="entry">');
                        sTmp.push('<div class="val">' + r.Notes + '</div>');
                        sTmp.push('<div class="eVal">contact notes</div>');
                    sTmp.push('</div>');
                    if (activeTab == 'personalTab') {
                        sTmp.push('<div class="entry">');
                            if (r.Prospects > 0) {
                                sTmp.push('<div class="valSingle">In Prospects List</div>')
                            }
                            else {
                                sTmp.push('<div class="valSingle">Not In Prospects List</div>')
                            }
                        sTmp.push('</div>');
                    }
                    else if (activeTab == 'accountTab') {
                        sTmp.push('<div class="entry">');
                            sTmp.push('<div class="val">' + r['Call Groups'] + '</div>');
                            sTmp.push('<div class="eVal">from call groups</div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entry">');
                            sTmp.push('<div class="val">' + r['Call Logs'] + '</div>');
                            sTmp.push('<div class="eVal">call logs</div>');
                        sTmp.push('</div>');
                        /*
                        sTmp.push('<div class="entry">');
                            sTmp.push('<div class="val">' + r.Contact + '</div>');
                            sTmp.push('<div class="eVal">contact</div>');
                        sTmp.push('</div>');
                        */
                    }
                sTmp.push('</div>')

                sTmp.push('<div class="msg newParagraph">');
                    sTmp.push('<div class="boldRed">This cannot be undone.</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            // load the content and clear the spinner
            $('.content', $delInfoDlg).html(sTmp.join(''));
            $('.waitingDiv', $delInfoDlg).hide();
        })
        .fail(function(r) {
console.log('FAIL - delete dry run: ', r)
        })
    } // deleteContact

    /*
     * Build Filters
     */
    function buildBasicFilter(reloadList)
    {
        if (reloadList == undefined) {
            reloadList = true;
        }
        var filter = [];
        var filterDetails = {};

        // see if there is something to search
        var searchVal = $('.newRankingDiv .searchBar').revupSearchBox('getValue');
        if (searchVal != '') {
            var searchType = $('.newRankingDiv .searchType').revupDropDownField('getValue');

            filter.push("basic_filter=" + searchType + ":" + searchVal);

            // build the details
            filterDetails.searchValue = searchVal;
            filterDetails.searchType = searchType;
        }

        // add the quick filter
        var qFilter = $('.newRankingDiv .lstDiv .showingContactsDiv .showingWhat').attr('quickFilterVal');
        qFilter = qFilter.split(';;');
        for (var i = 0; i < qFilter.length; i++) {
            if ((qFilter != 'all') && (qFilter != '')) {
                //filter.push("filter=quick:" + qFilter[i]);
                filter.push("filter=" + qFilter[i]);

                // build the details
                filterDetails.quickFilter = qFilter.join(';;');
            }
        }

        // save the filter data
        if (activeTab == 'personalTab') {
            localStorage.setItem('rankingFilterBasicPersonalData-' + templateData.filterVersion + '-' + templateData.seatId, JSON.stringify(filterDetails));
            localStorage.setItem('rankingFilterStylePersonal-' + templateData.filterVersion + '-' + templateData.seatId, 'basic');
            localStorage.setItem('rankingFilterPersonal-' + templateData.filterVersion + '-' + templateData.seatId, filter.join('&'));
        }
        else if (activeTab == 'accountTab') {
            localStorage.setItem('rankingFilterBasicAccountData-' + templateData.filterVersion + '-' + templateData.seatId, JSON.stringify(filterDetails));
            localStorage.setItem('rankingFilterStyleAccount-' + templateData.filterVersion + '-' + templateData.seatId, 'basic');
            localStorage.setItem('rankingFilterAccount-' + templateData.filterVersion + '-' + templateData.seatId, filter.join('&'));
        }

        // apply the filter
        if (reloadList) {
            reloadRankingList(true);
        }
    } // buildBasicFilter

    /*
     * Load filter mode
     */
    function loadBasicFilterData()
    {
        // clear the button
        $('.rankingNewLayout .advFilterSection').removeClass('active');

        // hide the new/advance filter section
        $('.rankingNewLayout .newFiltersSection').hide();

        // display the old quick filter section
        if (templateData.bDisplayQuickFilters) {
            $('.rankingNewLayout .showingContactsDiv').show();
        }
        else {
            $('.rankingNewLayout .showingContactsDiv').hide();
        }
        $('.rankingNewLayout .showingContactsLstDiv').hide();

        // disable and hide the search bar and search dropdowns
        $('.newRankingDiv .searchSection').removeClass('disabled');
        $('.newRankingDiv .searchType').revupDropDownField('enable');
        $('.newRankingDiv .searchBar').revupSearchBox('enable');

        // load the current data
        let data = null;
        if (activeTab == 'personalTab') {
            data = localStorage.getItem('rankingFilterBasicPersonalData-' + templateData.filterVersion + '-' + templateData.seatId);
        }
        else if (activeTab == 'accountTab') {
            data = localStorage.getItem('rankingFilterBasicAccountData-' + templateData.filterVersion + '-' + templateData.seatId);
        }

        if (data != null) {
            data = JSON.parse(data);
            // handle empty data
            if ($.isEmptyObject(data)) {
                data.searchType = 'name';
                data.searchValue = '';
                data.quickFilter = 'all';
            }

            // handle empty data
            if ($.isEmptyObject(data)) {
                data.searchType = 'name';
                data.searchValue = '';
                data.quickFilter = 'all';
            }

            // set the search value
            $('.newRankingDiv .searchType').revupDropDownField('setValue', data.searchType);
            $('.newRankingDiv .searchBar').revupSearchBox('setValue', data.searchValue);

            // set the quick filter value
            if (templateData.bDisplayQuickFilters) {
                $('.rankingNewLayout .showingContactsLstDiv .entry .icon-check').hide();
                var $qFilterEntry = $();
                var $e = $('.rankingNewLayout .showingContactsLstDiv .entry')
                $e.each(function() {
                    var qf = $(this).attr('quickFilterVal');
                    if (qf == data.quickFilter) {
                        $qFilterEntry = $(this);
                        return false;
                    }
                });
                $('.icon-check', $qFilterEntry).show();
                $('.rankingNewLayout .showingContactsDiv .showingWhat').attr('quickFilterVal', data.quickFilter);
                var label = $('.text', $qFilterEntry).text();
                if (label == '') {
                    label = quickFilters[0].label;
                }
                $('.rankingNewLayout .showingContactsDiv .showingWhat').text(label);
            }
        }

        if (data == null) {
            var $qFilterEntry = $('.rankingNewLayout .showingContactsLstDiv .entry[quickFilterVal=all]');
            $('.icon-check', $qFilterEntry).show();
            $('.rankingNewLayout .showingContactsDiv .showingWhat').attr('quickFilterVal', 'all');
            $('.rankingNewLayout .showingContactsDiv .showingWhat').text($('.text', $qFilterEntry).text());
        }

        // once loaded build the filter
        buildBasicFilter(false);
    } // loadBasicFilterData

    /*
     * Event And Widget handlers
     */
    function reloadRankingList(isFromSearch)
    {
        // display the loading spinner list section
        var $loadingLstDiv = $('.rankingNewLayout .lstDiv .rankingDetailsLstLoading');
        var $headerDiv = $('.rankingNewLayout .lstDiv .lstColumnHeaderDiv');
        $loadingLstDiv.show(1);
        $rankingLst.hide();

        // display the loading spinner in the detail section
        var $loadingDetailDiv = $('.rankingNewLayout .detailDiv .rankingDetailsDetailLoading');
        var $detailDiv = $('.rankingNewLayout .detailDiv .rankingDetailsDetailDiv');
        $loadingDetailDiv.show(1);
        $detailDiv.hide();

        // get active filter
        let filter = '';
        if (activeTab == 'personalTab') {
            filter = localStorage.getItem('rankingFilterPersonal-' + templateData.filterVersion + '-' + templateData.seatId);
        }
        else if (activeTab == 'accountTab') {
            filter = localStorage.getItem('rankingFilterAccount-' + templateData.filterVersion + '-' + templateData.seatId);
        }

        mixpanel.track("Ranking",
                    {"analysisId": templateData.analysisId,
                     "partitionId": activePartition,
                     "filter": filter,
                    })

        // create the url
        var urlArgs = [];
        let activeContactSetId = contactSetActiveId.split(';;')[0];
        var url2 = templateData.contactDetailAnalysisUrlNew.replace('seatId', templateData.seatId)
                                                           .replace('contactSetId', activeContactSetId)

        urlArgs.push('partition=' + activePartition);
        urlArgs.push("page=1");
        urlArgs.push("page_size=" + pageSize);
        if (getUrlArgs.analysis !== undefined) {
            urlArgs.push("analysis=" + getUrlArgs.analysis);
        }

        if (filter) {
            urlArgs.push(filter);
        }

        var $currentSort = $('.lstDiv .lstColumnHeaderDiv .column.activeSort');
        var sortArg = $currentSort.attr('sortkey');
        var whatDir = $currentSort.attr('sortdir');

        if (whatDir == 'desc') {
            sortArg = '-' + sortArg;
        }

        // add sorting argument if there is one
        if (sortArg) {
            urlArgs.push('order_by=' + sortArg)
        }

        url2 += "?" + urlArgs.join('&');

        // filters to the url
        if (!templateData.isAcademic && !settingsDisplayMaxedDonors) {
            url2 += "&filter=client-maxed:false";
        }

        // update the history record
        if (history.pushState) {
            var arg = "";
            arg = "?partition=" + activePartition;
            arg += "&contact_set=" + activeContactSetId;
            arg += "&page=1";
            arg += "&page_size=" + pageSize;
            if (getUrlArgs.analysis !== undefined) {
                arg += "&analysis=" + getUrlArgs.analysis;
            }
            // if (searchVal != '') {
            //    arg += "&query=" + searchVal;
            //    arg += "&type=" + searchType;
            // }

            // save the current url in browser history
            var pathName = window.location.pathname;
            pathName = pathName.split('/');
            pathName = '/' + pathName[1] + '/';
            var newUrl = window.location.protocol + "//" + window.location.host + pathName + arg;
            window.history.pushState({path:newUrl},'',encodeURI(newUrl));
        }

        $.ajax({
            url: encodeURI(url2),
            type: "GET",
        })
        .done(function(r) {
            // load globals
            let joinPermission = undefined;
            if (contactSetPerm.call_time != null && contactSetPerm.prospects != null) {
                joinPermission = 'call_time-prospects';
            }
            // $headerDiv.html(revupListView.buildListHeader(templateData.seatId, contactSetPerm, joinPermission));
            // update header
            var headerText = 'Giving History';
            if (yearsOfGiving && yearsOfGiving > 0) {
                headerText = yearsOfGiving + '+ Years of Giving';
            }
            $('.rankingNewLayout .lstDiv .lstColumnHeaderDiv .colGiving .columnHeader').text(headerText);

            // update the ranking list div setAttribute
            $rankingLstDiv.attr('currentPage', 1);
            if (r.count == 0) {
                $rankingLstDiv.attr('maxPages', 0);
                $rankingLstDiv.attr('numEntries', 0);
                $rankingLstDiv.attr('numEntriesLoaded', 0);
            }
            else {
                $rankingLstDiv.attr('maxPages', Math.ceil(r.count / pageSize));
                $rankingLstDiv.attr('numEntries', r.count);
                $rankingLstDiv.attr('numEntriesLoaded', r.results.length);
            }

            // append to entries or replace
            $rankingLst.html(loadRankingList(r.results, r.results.length <= r.count));

            // update the pagination if from a search
            if (isFromSearch) {
                // if the number of entries is 0 - nothing match search hide pagination and header
                if (r.count == 0) {
                    // hide footer and header
                    $rankingLstFooter.hide();
                    $rankingLstHeader.hide();

                    // display message
                    var sTmp = [];
                    sTmp.push('<div class="errorMsg">');
                        sTmp.push('<div class="msg">');
                            sTmp.push("No entries match you search criteria.<br><br>Update your search and try again.");
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                    $rankingLst.html(sTmp.join(''));

                    // update the details panel - to blank
                    sTmp = [];
                    sTmp.push('<div style="width:100%;height:350px"></div>');
                    $detailDiv.html(sTmp.join(''));
                    $loadingDetailDiv.hide();
                    $("html, body").scrollTop(0);
                    $detailDiv.show(1);
                }
                else {
                    // make sure the header and footer are displayed
                    $rankingLstHeader.show(1);
                    $rankingLstFooter.show(1);
                }
            }
            else {
                // if the number of entries is 0 - nothing match search hide pagination and header
                if (r.count == 0) {
                    // hide footer and header
                    $rankingLstFooter.hide();
                    $rankingLstHeader.hide();

                    // display message
                    var sTmp = [];
                    sTmp.push('<div class="errorMsg">');
                        sTmp.push('<div class="msg">');
                            sTmp.push("No entries match the current filter.");
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                    $rankingLst.html(sTmp.join(''));

                    // update the details panel - to blank
                    sTmp = [];
                    sTmp.push('<div style="width:100%;height:350px"></div>');
                    $detailDiv.html(sTmp.join(''));
                    $loadingDetailDiv.hide();
                    $("html, body").scrollTop(0);
                    $detailDiv.show(1);
                }
                else {
                    // make sure the header and footer are displayed
                    $rankingLstHeader.show(1);
                    $rankingLstFooter.show(1);
                }
            }

            setTimeout(function() {
                // enable/disable the correct columns
                if (activeTab == 'personalTab') {
                    $('.column.colCallTimeSwitch', $rankingLst).addClass('disabled');
                    $('.column.colPropectSwitch', $rankingLst).removeClass('disabled');
                }
                else if (activeTab == 'accountTab') {
                    $('.column.colCallTimeSwitch', $rankingLst).removeClass('disabled');
                    $('.column.colPropectSwitch', $rankingLst).addClass('disabled');
                }
            }, 10);

            // trigger like clicking on the first entry
            var $firstSelected = $('.entry:first-child', $rankingLst);
            var $first = $('.rankingDetailsLstDiv .entry').first();
            $first.trigger('click');
            $firstSelected.trigger('click');

            // update the footer count
            $('.lstFooterDiv').html('<text class="showing">Showing </text>' + parseInt($rankingLstDiv.attr('numEntriesLoaded')) + '<text> of </text>' + '<text class="total">' + $rankingLstDiv.attr('numEntries') + ' contacts </text>');
        })
        .fail(function(r) {
            // load error message
            var sTmp = [];
            sTmp.push('<div class="errorMsg">');
                sTmp.push('<div class="msg">');
                    sTmp.push("Unable to load Ranking Data at this time<br><br>Try again later");
                sTmp.push('</div>')
            sTmp.push('</div>')
            $rankingLst.html(sTmp.join(''));

            if (activeTab == 'personalTab') {
                localStorage.removeItem('rankingFilterPersonal-' + templateData.filterVersion + '-' + templateData.seatId);
                localStorage.removeItem('rankingFilterBasicPersonalData-' + templateData.filterVersion + '-' + templateData.seatId);
                localStorage.removeItem('rankingFilterAdvPersonalData-' + templateData.filterVersion + '-' + templateData.seatId);
            }
            else if (activeTab == 'accountTab') {
                localStorage.removeItem('rankingFilterAccount-' + templateData.filterVersion + '-' + templateData.seatId);
                localStorage.removeItem('rankingFilterBasicAccountData-' + templateData.filterVersion + '-' + templateData.seatId);
                localStorage.removeItem('rankingFilterAdvAccountData-' + templateData.filterVersion + '-' + templateData.seatId);
            }

            // update the details panel - to blank
            sTmp = [];
            sTmp.push('<div style="width:100%;height:350px"></div>');
            $detailDiv.html(sTmp.join(''));
            $loadingDetailDiv.hide();
            $("html, body").scrollTop(0);
            $detailDiv.show(1);

            revupUtils.apiError('Ranking Details', r, 'Reload Ranking List');

            // hide footer and header
            $rankingLstFooter.hide();
            $rankingLstHeader.hide();
        })
        .always(function(r) {
            $loadingLstDiv.hide();
            $("html, body").scrollTop(0);
            $rankingLst.show(1);
        });
    } // reloadRankingList

    /*
     * Fetch the next page for infinite scrolling
     */
    let fetchNextChunk = (bSingleEntry = false) => {
        // get active filter
        let filter = '';
        if (activeTab == 'personalTab') {
            filter = localStorage.getItem('rankingFilterPersonal-' + templateData.filterVersion + '-' + templateData.seatId);
        }
        else if (activeTab == 'accountTab') {
            filter = localStorage.getItem('rankingFilterAccount-' + templateData.filterVersion + '-' + templateData.seatId);
        }

        // get the max pages
        let maxPages = parseInt($rankingLstDiv.attr('maxPages'), 10);

        // create the url
        var urlArgs = [];
        let activeContactSetId = contactSetActiveId.split(';;')[0];
        var url2 = templateData.contactDetailAnalysisUrlNew.replace('seatId', templateData.seatId)
                                                           .replace('contactSetId', activeContactSetId);

        urlArgs.push('partition=' + activePartition);
        if (bSingleEntry) {
            urlArgs.push("page_size=1");
            let page = parseInt($rankingLstDiv.attr('numEntriesLoaded'), 10) + 1;
            urlArgs.push("page=" + page);
        }
        else {
            let nextPage = parseInt($rankingLstDiv.attr('currentPage'), 10) + 1;
            $rankingLstDiv.attr('currentPage', nextPage);
            urlArgs.push("page=" +  nextPage);
            urlArgs.push("page_size=" + pageSize);
        }
        if (getUrlArgs.analysis !== undefined) {
            urlArgs.push("analysis=" + getUrlArgs.analysis);
        }

        if (filter) {
            urlArgs.push(filter);
        }

        var $currentSort = $('.lstDiv .lstColumnHeaderDiv .column.activeSort');
        var sortArg = $currentSort.attr('sortkey');
        var whatDir = $currentSort.attr('sortdir');

        if (whatDir == 'desc') {
            sortArg = '-' + sortArg;
        }

        // add sorting argument if there is one
        if (sortArg) {
            urlArgs.push('order_by=' + sortArg)
        }

        url2 += "?" + urlArgs.join('&');

        // filters to the url
        if (!templateData.isAcademic && !settingsDisplayMaxedDonors) {
            url2 += "&filter=client-maxed:false";
        }

        $.ajax({
            url: encodeURI(url2),
            type: "GET",
        })
        .done(function(r) {
            // update the attributes
            $rankingLstDiv.attr('maxPages', Math.ceil(r.count / pageSize));
            $rankingLstDiv.attr('numEntries', r.count);
            let numLoaded = parseInt($rankingLstDiv.attr('numEntriesLoaded'), 10) + r.results.length;
            $rankingLstDiv.attr('numEntriesLoaded', numLoaded);

            // load globals
            let joinPermission = undefined;
            if (contactSetPerm.call_time != null && contactSetPerm.prospects != null) {
                joinPermission = 'call_time-prospects';
            }

            // update header
            var headerText = 'Giving History';
            if (yearsOfGiving && yearsOfGiving > 0) {
                headerText = yearsOfGiving + '+ Years of Giving';
            }
            $('.rankingNewLayout .lstDiv .lstColumnHeaderDiv .colGiving .columnHeader').text(headerText);

            // append to entries or replace
            $('.entry.loadingMore', $rankingLst).remove();
            $rankingLst.append(loadRankingList(r.results, numLoaded < r.count));

            setTimeout(function() {
                // enable/disable the correct columns
                if (activeTab == 'personalTab') {
                    $('.column.colCallTimeSwitch', $rankingLst).addClass('disabled');
                    $('.column.colPropectSwitch', $rankingLst).removeClass('disabled');
                }
                else if (activeTab == 'accountTab') {
                    $('.column.colCallTimeSwitch', $rankingLst).removeClass('disabled');
                    $('.column.colPropectSwitch', $rankingLst).addClass('disabled');
                }
            }, 10);

            // update footer count
            $('.lstFooterDiv').html('<text class="showing">Showing </text>' + parseInt($rankingLstDiv.attr('numEntriesLoaded')) + '<text> of </text>' + '<text class="total">' + $rankingLstDiv.attr('numEntries') + ' contacts </text>');
        })
        .fail(function(r) {
            revupUtils.apiError('Ranking Details', r, 'Reload Ranking List');
        })
    } // fetchNextChunk

    /*
     * Prospect Details
     */
    function buildProspectDetailsFrame()
    {
        var sTmp = [];

        sTmp.push('<div class="newProspectDetail">');
            sTmp.push('<div class="mainPageLoading" style="height:500px">');
                sTmp.push('<div class="loadingDiv">');
                    sTmp.push('<div class="loading"></div>');
                    sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                sTmp.push('</div>');
            sTmp.push('</div>');
            sTmp.push('<div class="mainPage" style="display:none">');
                sTmp.push('<div class="pageHeader">');
                sTmp.push('</div>');

                sTmp.push('<div class="partitionHeaderDiv">');
                    sTmp.push('<div class="innerDiv">');
                        if (!templateData.isAcademic) {
                            sTmp.push('<div class="partitionGrpDiv">');
                                sTmp.push('<div class="textLabel">Election Cycle:</div>');
                                sTmp.push('<div class="partitionDiv"></div>');
                            sTmp.push('</div>');
                        }
                        sTmp.push('<div class="legendIconBtn" title="Legend">');
                            sTmp.push('<div class="icon icon-key"></div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');

                sTmp.push('<div class="givingSection">');
                    sTmp.push('<div class="givingLoading" style="height:450px;display:none">');
                        sTmp.push('<div class="loadingDiv">');
                            sTmp.push('<div class="loading"></div>');
                            sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="givingLoadedData">');
                        sTmp.push('<div class="leftSide">');
                            sTmp.push('<div class="givingTypeSectionOuter academicGiving">');
                                sTmp.push('<div class="givingTypeSection  opened">');
                                    sTmp.push('<div class="givingTypeSection opened">');
                                        sTmp.push('<div class="sectionHeader">');
                                            sTmp.push('<div class="openClose icon icon-triangle-dn"></div>');
                                            sTmp.push('<div class="headerText">Purdue Giving</div>');
                                            sTmp.push('<div class="headerRight">');
                                                sTmp.push('<div class="graphDiv candidates">');
                                                    sTmp.push('<div class="title">Candidates</div>');
                                                    sTmp.push('<div class="graph"></div>');
                                                sTmp.push('</div>');
                                                sTmp.push('<div class="graphDiv dollars">');
                                                    sTmp.push('<div class="title">Dollars</div>');
                                                    sTmp.push('<div class="graph"></div>');
                                                sTmp.push('</div>');
                                                sTmp.push('<div class="headerNoData">No Data</div>');
                                            sTmp.push('</div>');
                                        sTmp.push('</div>');
                                        sTmp.push('<div class="sectionBody">');
                                        sTmp.push('</div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');

                            sTmp.push('<div class="givingTypeSectionOuter academicNonProfit">');
                                sTmp.push('<div class="givingTypeSection opened">');
                                    sTmp.push('<div class="sectionHeader">');
                                        sTmp.push('<div class="openClose icon icon-triangle-dn"></div>');
                                        sTmp.push('<div class="headerText">Nonprofit</div>');
                                        sTmp.push('<div class="headerRight">');
                                            sTmp.push('<div class="headerNoData">No Data</div>');
                                        sTmp.push('</div>');
                                    sTmp.push('</div>');
                                    sTmp.push('<div class="sectionBody nonProfAcadDetails">');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');

                            sTmp.push('<div class="givingTypeSectionOuter stateCampaign">');
                                sTmp.push('<div class="givingTypeSection  opened">');
                                    sTmp.push('<div class="givingTypeSection opened">');
                                        sTmp.push('<div class="sectionHeader">');
                                            sTmp.push('<div class="openClose icon icon-triangle-dn"></div>');
                                            sTmp.push('<div class="headerText">State Campaigns</div>');
                                            sTmp.push('<div class="headerRight">');
                                                sTmp.push('<div class="graphDiv candidates">');
                                                    sTmp.push('<div class="title">Candidates</div>');
                                                    sTmp.push('<div class="graph"></div>');
                                                sTmp.push('</div>');
                                                sTmp.push('<div class="graphDiv dollars">');
                                                    sTmp.push('<div class="title">Dollars</div>');
                                                    sTmp.push('<div class="graph"></div>');
                                                sTmp.push('</div>');
                                                sTmp.push('<div class="headerNoData">No Data</div>');
                                            sTmp.push('</div>');
                                        sTmp.push('</div>');
                                        sTmp.push('<div class="sectionBody">');
                                        sTmp.push('</div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');

                            sTmp.push('<div class="givingTypeSectionOuter localCampaign">');
                                sTmp.push('<div class="givingTypeSection  opened">');
                                    sTmp.push('<div class="givingTypeSection opened">');
                                        sTmp.push('<div class="sectionHeader">');
                                            sTmp.push('<div class="openClose icon icon-triangle-dn"></div>');
                                            sTmp.push('<div class="headerText">Local Campaigns</div>');
                                            sTmp.push('<div class="headerRight">');
                                                sTmp.push('<div class="graphDiv candidates">');
                                                    sTmp.push('<div class="title">Candidates</div>');
                                                    sTmp.push('<div class="graph"></div>');
                                                sTmp.push('</div>');
                                                sTmp.push('<div class="graphDiv dollars">');
                                                    sTmp.push('<div class="title">Dollars</div>');
                                                    sTmp.push('<div class="graph"></div>');
                                                sTmp.push('</div>');
                                                sTmp.push('<div class="headerNoData">No Data</div>');
                                            sTmp.push('</div>');
                                        sTmp.push('</div>');
                                        sTmp.push('<div class="sectionBody">');
                                        sTmp.push('</div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');

                        sTmp.push('<div class="rightSide">');
                            sTmp.push('<div class="givingTypeSectionOuter federalCampaign">');
                                sTmp.push('<div class="givingTypeSection opened">');
                                    sTmp.push('<div class="sectionHeader">');
                                        sTmp.push('<div class="openClose icon icon-triangle-dn"></div>');
                                        sTmp.push('<div class="headerText">Federal Campaigns</div>');
                                        if (debugMode) {
                                            sTmp.push('<div id="federalEntityDebug" style="float: left; padding-left: 20px;">Entity Debug</div>');
                                        }
                                        sTmp.push('<div class="headerRight">');
                                            sTmp.push('<div class="graphDiv candidates">');
                                                sTmp.push('<div class="title">Candidates</div>');
                                                sTmp.push('<div class="graph"></div>');
                                            sTmp.push('</div>');
                                            sTmp.push('<div class="graphDiv dollars">');
                                                sTmp.push('<div class="title">Dollars</div>');
                                                sTmp.push('<div class="graph"></div>');
                                            sTmp.push('</div>');
                                            sTmp.push('<div class="headerNoData">No Data</div>');
                                        sTmp.push('</div>');
                                    sTmp.push('</div>');
                                    sTmp.push('<div class="sectionBody">');
                                    sTmp.push('</div>');
                                    sTmp.push('<div class="sectionFooter">');
                                        sTmp.push('<div class="fecNotice">');
                                            sTmp.push('Notice: Federal law prohibits using contributor contact information that is obtained from FEC reports');
                                            sTmp.push('for the purpose of soliciting contributions or for any commercial purpose. However, analysis of data');
                                            sTmp.push('obtained from FEC reports is permitted. RevUp does not display individual contributor contact information');
                                            sTmp.push('obtained from reports filed with the FEC.');
                                        sTmp.push('</div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');

                            sTmp.push('<div class="givingTypeSectionOuter otherCampaign">');
                                sTmp.push('<div class="givingTypeSection  opened">');
                                    sTmp.push('<div class="givingTypeSection opened">');
                                        sTmp.push('<div class="sectionHeader">');
                                            sTmp.push('<div class="openClose icon icon-triangle-dn"></div>');
                                            sTmp.push('<div class="headerText">Local Campaigns</div>');
                                            sTmp.push('<div class="headerRight">');
                                                sTmp.push('<div class="graphDiv candidates">');
                                                    sTmp.push('<div class="title">Candidates</div>');
                                                    sTmp.push('<div class="graph"></div>');
                                                sTmp.push('</div>');
                                                sTmp.push('<div class="graphDiv dollars">');
                                                    sTmp.push('<div class="title">Dollars</div>');
                                                    sTmp.push('<div class="graph"></div>');
                                                sTmp.push('</div>');
                                                sTmp.push('<div class="headerNoData">No Data</div>');
                                            sTmp.push('</div>');
                                        sTmp.push('</div>');
                                        sTmp.push('<div class="sectionBody">');
                                        sTmp.push('</div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        return sTmp.join('');

    } // buildProspectDetailsFrame

    function addEventAndWidgetHandlers(templateData)
    {
        /*
         * contact list tab
         */
        $('.newRankingDiv .lstDiv .togglePages').on('click', '.contactSetTab', function(e) {
            let $tab = $(this);

            // if the tab clicked on active done
            if ($tab.hasClass('active')) {
                return;
            }

            // if disabled return
            if ($tab.hasClass('disabled')) {
                return;
            }

            // set tab active
            $('.newRankingDiv .lstDiv .togglePages .contactSetTab').removeClass('active');
            $tab.addClass('active');

            // set the activeTab variables
            if ($tab.hasClass('personalTab')) {
                activeTab = 'personalTab';
                contactSetActiveId = personalContactId;
            }
            else if ($tab.hasClass('accountTab')) {
                activeTab = 'accountTab';
                contactSetActiveId = accountContactId;
            }
            else {
                activeTab = '';
                contactSetActiveId = '-1';
            }

            // save the active tab in local storage
            if (activeTab != '') {
                localStorage.setItem('rankActiveTab-' + templateData.seatId, activeTab);
            }
            else {
                localStorage.removeItem('rankActiveTab-' + templateData.seatId);
            }


            // reload the list
            reloadRankingList();

            // enable/disable the correct columns
            if (activeTab == 'personalTab') {
                $('.column.colCallTimeSwitch').addClass('disabled');
                $('.column.colPropectSwitch').removeClass('disabled');
            }
            else if (activeTab == 'accountTab') {
                $('.column.colCallTimeSwitch').removeClass('disabled');
                $('.column.colPropectSwitch').addClass('disabled');
            }

            // reset the drop down
            let lst = [];
            let obj = {};
            let dObj = {}
            dObj.source = contactSetActiveId.split(';;')[2];
            dObj.title = contactSetActiveId.split(';;')[1];
            obj.value = contactSetActiveId.split(';;')[0];
            obj.displayValue = contactLstDisplayName(dObj);
            lst[0] = obj;

            $('.newRankingDiv .contactLstDiv').revupDropDownField('newList', lst)

            // update search
            let currentFilter = '';
            let filterMode = '';
            if (activeTab == 'personalTab') {
                filterMode = localStorage.getItem('rankingFilterStylePersonal-' + templateData.filterVersion + '-' + templateData.seatId);
                if (filterMode == 'basic') {
                    currentFilter = localStorage.getItem('rankingFilterBasicPersonalData-' + templateData.filterVersion + '-' + templateData.seatId);
                }
                else if (filterMode == 'advance') {
                    currentFilter = localStorage.getItem('rankingFilterAdvPersonalData-' + templateData.filterVersion + '-' + templateData.seatId);
                }
            }
            if (activeTab == 'accountTab') {
                filterMode = localStorage.getItem('rankingFilterStyleAccount-' + templateData.filterVersion + '-' + templateData.seatId);
                if (filterMode == 'basic') {
                    currentFilter = localStorage.getItem('rankingFilterBasicAccountData-' + templateData.filterVersion + '-' + templateData.seatId);
                }
                else if (filterMode == 'advance') {
                    currentFilter = localStorage.getItem('rankingFilterAdvAccountData-' + templateData.filterVersion + '-' + templateData.seatId);
                }
            }
            if (currentFilter) {
                currentFilter = JSON.parse(currentFilter);
            }
            newFilters.init($('.newRankingDiv .rankingNewLayout .lstDiv .newFiltersSection'), filterData, currentFilter);

            // set to basic or advanced
            filterMode = filterMode == null ? 'basic': filterMode;
            if (filterMode == 'basic') {
                loadBasicFilterData();

                // update the basic search box
                // disable and hide the search bar and search dropdowns
                $('.newRankingDiv .searchSection').removeClass('disabled');
                $('.newRankingDiv .searchType').revupDropDownField('enable');
                $('.newRankingDiv .searchBar').revupSearchBox('enable');

                // set the values
                if (currentFilter && currentFilter.searchType) {
                    //currentFilter = JSON.parse(currentFilter)
                    $('.newRankingDiv .searchType').revupDropDownField('setValue', currentFilter.searchType);
                    $('.newRankingDiv .searchBar').revupSearchBox('setValue', currentFilter.searchValue);
                }
                else {
                    $('.newRankingDiv .searchType').revupDropDownField('setValue', 'name');
                    $('.newRankingDiv .searchBar').revupSearchBox('setValue', '');
                }
            }
            else if (filterMode == 'advance') {
                // make active
                $('.rankingNewLayout .advFilterSection').addClass('active')

                // show the new/advance filter section
                $('.rankingNewLayout .newFiltersSection').show();

                // hide the old quick filter section
                $('.rankingNewLayout .showingContactsDiv').hide();
                $('.rankingNewLayout .showingContactsLstDiv').hide();

                // disable and hide the search bar and search dropdowns
                $('.newRankingDiv .searchSection').addClass('disabled');
                $('.newRankingDiv .searchType').revupDropDownField('disable');
                $('.newRankingDiv .searchBar').revupSearchBox('disable');
                $('.newRankingDiv .searchType').revupDropDownField('setValue', 'name');
                $('.newRankingDiv .searchBar').revupSearchBox('setValue', '');
            }
        });

        /*
         * Page Menu
         */
        $('.newRankingDiv .hamburgerIconBtn').on('click', function(e) {
            var sTmp = [];

            sTmp.push('<div class="pageMenuDiv">');
                if (features.CSV_EXPORT_RANKING) {
                    sTmp.push('<div class="entry  exportLst">');
                }
                else {
                    sTmp.push('<div class="entry exportLst disabled">');
                }
                sTmp.push('<div class="text">Export a List</div>');
                sTmp.push('<div class="icon icon-export-csv"></div>');
            sTmp.push('</div>');
                /*
                sTmp.push('<div class="entry  saveLst">');
                    sTmp.push('<div class="text">Save as New List</div>');
                    sTmp.push('<div class="icon icon-save-as"></div>');
                sTmp.push('</div>');
                sTmp.push('<div class="entry  sharetLst">');
                    sTmp.push('<div class="text">Share a List</div>');
                    sTmp.push('<div class="icon icon-share"></div>');
                sTmp.push('</div>');
                sTmp.push('<div class="entry  deleteLst">');
                    sTmp.push('<div class="text">Delete List</div>');
                    sTmp.push('<div class="icon icon-trash"></div>');
                sTmp.push('</div>');
                */
                sTmp.push('<div class="entry  viewLegend">');
                    sTmp.push('<div class="text">View Legend</div>');
                    sTmp.push('<div class="icon icon-analyze"></div>');
                sTmp.push('</div>');
                sTmp.push('<div class="entry  settings">');
                    sTmp.push('<div class="text">Settings</div>');
                    sTmp.push('<div class="icon icon-settings"></div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            var $popup = $(this).revupPopup({
                extraClass: 'rankingPageMenu',
                location: 'below',
                content: sTmp.join(''),
                //minLeft: 145,
                //maxRight: 415,
                bPassThrough: false
            });

            $popup.on('click', '.entry', function(e) {
                var $entry = $(this);

                // see if the delete button should be disabled
                if ($entry.hasClass('beingDeleted')) {
                    $('.newRankingDiv .detailDiv .rankingDetailsDetailDiv .deleteContactBtn').addClass('disabled');
                }
                else {
                    $('.newRankingDiv .detailDiv .rankingDetailsDetailDiv .deleteContactBtn').removeClass('disabled');
                }

                // if the entry is disabled then skip
                if ($entry.hasClass('disabled')) {
                    $popup.revupPopup('close');

                    return;
                }

                if ($entry.hasClass('exportLst')) {
                    // close menu and display the CSV export dialog
                    $popup.revupPopup('close');
                    displayCSVExportDlg();
                }
                else if ($entry.hasClass('saveLst')) {
                    // save list
                    $popup.revupPopup('close');
                }
                else if ($entry.hasClass('sharetLst')) {
                    // share list
                    $popup.revupPopup('close');
                }
                else if ($entry.hasClass('deletetLst')) {
                    // delete list
                    $popup.revupPopup('close');
                }
                else if ($entry.hasClass('viewLegend')) {
                    // close menu and display legend
                    $popup.revupPopup('close');
                    displayLegendDlg();
                }
                if ($entry.hasClass('settings')) {
                    // close the menu and display the setting dialog
                    $popup.revupPopup('close');
                    displaySettingsDlg();
                }
            })
        })

        /*
         *  Legend
         */
        var $legendPopup = $('.legendPopup2');
        var $legendCloseBtn = $('.closeBtn', $legendPopup);
        function displayLegendDlg()
        {
            var $this = $(this);

            $(document).on('keydown', function(e) {
                if (e.keyCode == 27) {
                    $legendPopup.hide();
                    $overlay.trigger('revup.overlayClose');

                    $(document).off('keydown');

                    e.stopPropagation();
                    e.preventDefault();
                }
            })

            // add the overlay
            var $overlay = $('body').overlay();
            $('body').on('revup.overlayClick', function() {
                $legendPopup.hide();
            });

            // position and display
            var top = "100px";
            var left = ($(document).width() - $legendPopup.width()) / 2;
            $legendPopup.css('top', top)
                        .css('left', left)
                        .show(1);

            // close button handler
            $legendCloseBtn.off('click')
                           .on('click', function() {
                               $legendPopup.hide();
                               $overlay.trigger('revup.overlayClose');
                           });
        } // displayLegendDlg

        /*
         *  Settings
         */
        function displaySettingsDlg ()
        {
            var sTmp = [];
            var bHidePoliticalColor = !settingsPoliticalColor;
            var bHideMaxedDonor = !templateData.isAcademic && !settingsDisplayMaxedDonors;
            var checked;
            sTmp.push('<div class="rankingSettingsDiv">');
                sTmp.push('<div class="header">List View Options</div>');
                var extraClass = "";
                if (!templateData.bDisplayQuickFilters) {
                    extraClass = " disabled"
                }
                if (!templateData.isAcademic) {
                    sTmp.push('<div style="margin-top:5px" class="entry' + extraClass + '">');
                        checked = "";
                        if (bHideMaxedDonor) {
                            checked = " checked"
                        }
                        sTmp.push('<input id="hideMaxedId" class="checkboxHideMaxed" type="checkbox" value="hideMaxOut"' + checked + extraClass + '>');
                        sTmp.push('<label for="hideMaxedId" class="label"><span><span></span></span>Hide maxed out donors</label>');
                    sTmp.push('</div>');
                }
                sTmp.push('<div class="entry" style="margin-top:10px;">');
                    checked = "";
                    if (bHidePoliticalColor) {
                        checked = " checked"
                    }
                    sTmp.push('<input id="hidePoliticalColorId" type="checkbox" class="checkboxHidePolColor" value="hidePoliticalColor"' + checked + '>');
                    sTmp.push('<label for="hidePoliticalColorId" class="label"><span><span></span></span>Hide political party colors</label>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            var $dlg = $('body').revupDialogBox({
                headerText: "Settings",
                msg: sTmp.join(''),
                autoCloseOk: false,
                okHandler: function (e) {
                    var $hideColor = $('.checkboxHidePolColor', $dlg);
                    var $hideMaxed = $('.checkboxHideMaxed', $dlg);
                    var $settingIcon = $('.rankingNewLayout .settingsIconBtn');
                    var bSettingIconSet = false;

                    // set/unset political color
                    var $lstDiv = $('.rankingNewLayout .lstDiv .rankingDetailsLstDiv');
                    var $detailColor = $('.rankingNewLayout .detailDiv .scoreDiv');
                    if ($hideColor.prop('checked')) {
                        $lstDiv.addClass('hideColor');
                        $detailColor.addClass('hideColor');
                        bSettingIconSet = true;

                        settingsPoliticalColor = false;
                    }
                    else {
                        $lstDiv.removeClass('hideColor');
                        $detailColor.removeClass('hideColor');

                        settingsPoliticalColor = true;
                    }

                    // set/unset maxed donors
                    var bChange = false;
                    if (!templateData.isAcademic) {
                        var bCurrentMaxedSet = !settingsDisplayMaxedDonors;
                        if ($hideMaxed.prop('checked')) {
                            settingsDisplayMaxedDonors = false;
                            bSettingIconSet = true;

                            // set the footer message
                            $rankingLstFooter.html(paginationExtraMsg);

                            if (!bCurrentMaxedSet) {
                                bChange = true;
                            }
                        }
                        else {
                            settingsDisplayMaxedDonors = true;

                            // clear the footer meaage
                            $rankingLstFooter.html('');

                            if (bCurrentMaxedSet) {
                                bChange = true;
                            }
                        }
                    }

                    // update the tooltips
                    var tooltipMsg = [];
                    if (!templateData.isAcademic && !settingsDisplayMaxedDonors) {
                        tooltipMsg.push("Hide maxed out donors");
                    }
                    if (!templateData.isAcademic && !settingsDisplayMaxedDonors) {
                        tooltipMsg.push("Hide political party color")
                    }
                    if (tooltipMsg.length == 0) {
                        tooltipMsg.push('No settings')
                    }
                    $settingIcon.prop('title', tooltipMsg.join('\n'));

                    // set the color of the icon
                    if (bSettingIconSet) {
                        $settingIcon.addClass('valueSet');
                    }
                    else {
                        $settingIcon.removeClass('valueSet');
                    }

                    var pData = {};
                    pData.analysis_profile = contactSetActiveId.split(';;')[4];
                    pData.data = {};
                    if (!templateData.isAcademic) {
                        pData.data.display_maxed_donors = settingsDisplayMaxedDonors;
                    }
                    pData.data.political_colors = settingsPoliticalColor;

                    // post/put the changes
                    var url = templateData.analysisViewUserSettingsUrl;
                    var ajaxType = "POST";
                    if (settingsUserId == null) {
                        url = url.replace('userSettingsId/', '');
                    }
                    else {
                        url = url.replace('userSettingsId', settingsUserId);
                        ajaxType = "PUT";
                    }
                    url += "?analysis_profile=" + contactSetActiveId.split(';;')[4];
                    $.ajax({
                        type: ajaxType,
                        url: url,
                        data: JSON.stringify(pData),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                    })
                    .done(function(r) {
                        // save the id
                        settingsUserId = r.id;

                        // close
                        $dlg.revupDialogBox('close');

                        if (bChange) {
                            // reload the ranking list
                            setTimeout(function() {
                                reloadRankingList();
                            }, 10);
                        }
                    })
                    .fail(function(r) {
                        revupUtils.apiError('Ranking Details', r, 'Save User Settings', "Unable to save setting at this time");
                    })

                }
            });
        } // displaySettingsDlg

        /*
         * Sort Handlers
         */
        $('.lstColumnHeaderDiv .column').on('click', function(e) {
            var tempURL  = templateData.listFields.headerFields;
            var $this = $(this);
            if ($this.hasClass('sortable')) {
                // is this the active sort column and if so toggle sort Order
                let sortDir;
                if ($this.hasClass('activeSort')) {
                    // get the sort direction - ascending or descending
                    sortDir = $this.attr('sortDir');
                    if (sortDir == 'desc') {
                        sortDir = 'asc';
                    }
                    else {
                        sortDir = 'desc';
                    }
                    $this.attr('sortDir', sortDir);

                    // change the icon
                    if (sortDir == 'desc') {
                        $('.icon', $this).removeClass('icon-triangle-up')
                                         .addClass('icon-triangle-dn');
                    }
                    else {
                        $('.icon', $this).removeClass('icon-triangle-dn')
                                         .addClass('icon-triangle-up');
                    }
                    // toggle the icon, save the new sort order, redraw
                }
                else {
                    let $active = $('.lstDiv .lstColumnHeaderDiv .column.activeSort');
                    // clear the icon
                    $('.icon', $active).removeClass('icon-triangle-up')
                                       .removeClass('icon-triangle-dn')
                                       .css('display', 'none');

                    // remove the activeSort class
                    $active.removeClass('activeSort');

                    // change the label to black
                    $('.columnHeader', $active).css('color', revupConstants.color.blackText);

                    // set the new column to active
                    $this.addClass('activeSort');

                    // change the color of the newly select label
                    $('.columnHeader', $this).css('color', revupConstants.color.primaryGreen2);

                    // get the sort direction - ascending or descending
                    sortDir = $this.attr('sortDir');

                    if (!sortDir) {
                        sortDir = 'desc';
                        $this.attr('sortDir', sortDir);
                    }

                    // change the icon
                    if (sortDir == 'desc') {
                        $('.icon', $this).removeClass('icon-triangle-up')
                                         .addClass('icon-triangle-dn')
                                         .css('display', 'inline');
                    }
                    else {
                        $('.icon', $this).removeClass('icon-triangle-dn')
                                         .addClass('icon-triangle-up')
                                         .css('display', 'inline');
                    }

                    // 1) clear the icon of the old active state
                    // 2) clear the activeSort flag
                    // 3) set new column active sort
                    // 4) see what the old sort order was and use it - if not set descending
                    // 5) set icon
                }

                // reload the ranking list
                reloadRankingList()
            }
            else {
                return;
            }
        })// Sort Handlers


        /*
         * CSV Export
         */
        function displayCSVExportDlg()
        {
            var sTmp = [];
            sTmp.push('<div class="rankingCsvDownloadDiv">');
                sTmp.push('<div class="textMsg">');
                    sTmp.push('To save a copy of your ranking list as a CSV file select ');
                    sTmp.push('the options you would like included in the file and click save. The ');
                    sTmp.push('prospect\'s name is always included by default.');
                sTmp.push('</div>');
                sTmp.push('<div class="textMsg" style="margin-top:5px;">');
                    sTmp.push('Contact data that has been changed from ');
                    sTmp.push('the original import will not be saved.');
                sTmp.push('</div>');

                sTmp.push('<div class="header" style="margin-top:10px;">Format options</div>');
                var extraClass = "";
                if (!templateData.bDisplayQuickFilters) {
                    extraClass = " disabled"
                }
                sTmp.push('<div style="margin-top:5px;" class="entry' + extraClass + '">');
                    sTmp.push('<input id="checkbox1" class="csvIncludeName" type="checkbox" checked disabled>');
                    sTmp.push('<label for="checkbox1" class="label"><span><span></span></span>Name</label>');
                sTmp.push('</div>');
                sTmp.push('<div class="entry" style="margin-top:5px;">');
                    sTmp.push('<input id="checkbox2" type="checkbox" class="csvIncludeRankingScore" checked>');
                    sTmp.push('<label for="checkbox2" class="label"><span><span></span></span>Ranking score</label>');
                sTmp.push('</div>');
                if (!templateData.isAcademic) {
                    sTmp.push('<div class="entry" style="margin-top:5px;">');
                        sTmp.push('<input id="checkbox3" type="checkbox" class="csvIncludeContactInfo" checked>');
                        sTmp.push('<label for="checkbox3" class="label"><span><span></span></span>Contact information</label>');
                    sTmp.push('</div>');
                }
                sTmp.push('<div class="entry" style="margin-top:5px;">');
                    sTmp.push('<input id="checkbox4" type="checkbox" class="csvIncludeTotalDonations" checked>');
                    sTmp.push('<label for="checkbox4" class="label"><span><span></span></span>Total donation amount</label>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            var $dlg = $('body').revupDialogBox({
                headerText: "Save List as CSV File",
                msg: sTmp.join(''),
                autoCloseOk: false,
                waitingOverlay: true,
                waitingStyleShow: false,
                okHandler: function (e) {
                    // display the waiting
                    $dlg.revupDialogBox('showWaiting');
                    $dlg.revupDialogBox('disableBtn', 'okBtn');
                    $dlg.revupDialogBox('disableBtn', 'cancelBtn');

                    // build the arguments
                    var urlArgs = [];
                    urlArgs.push('show_name=' + $('.csvIncludeName', $dlg).prop('checked'));
                    urlArgs.push('show_score=' + $('.csvIncludeRankingScore', $dlg).prop('checked'));
                    urlArgs.push('show_contact_info=' + $('.csvIncludeContactInfo', $dlg).prop('checked'));
                    urlArgs.push('show_total=' + $('.csvIncludeTotalDonations', $dlg).prop('checked'));

                    // build the url
                    let activeContactSetId = contactSetActiveId.split(';;')[0];
                    var url2 = templateData.exportUrlNew.replace('seatId', templateData.seatId)
                                                        .replace('contactSetId', activeContactSetId);

                    urlArgs.push('partition=' + activePartition);
                    filter = '';
                    if (activeTab == 'personalTab') {
                        filter = localStorage.getItem('rankingFilterPersonal-' + templateData.filterVersion + '-' + templateData.seatId);
                    }
                    else if (activeTab == 'accountTab') {
                        filter = localStorage.getItem('rankingFilterAccount-' + templateData.filterVersion + '-' + templateData.seatId);
                    }
                    if (filter) {
                        urlArgs.push(filter);
                    }

                    // filters to the url
                    if (!templateData.isAcademic && !settingsDisplayMaxedDonors) {
                        urlArgs.push("filter=client-maxed:false");
                    }

                    // start the download
                    $.ajax({
                        type: 'GET',
                        url:  encodeURI(url2 + '?' + urlArgs.join('&'))
                    })
                    .done(function(result) {
                        $dlg.revupDialogBox('close');
                        $('body').revupMessageBox({
                            headerText:'CSV Export',
                            msg: "Your CSV file is being generated.<br>" +
                                 "<span style='font-weight: bold'>You will receive an email when it is ready for download.</span>" +
                                 "<br><br>Note: Depending on the size of the export, this may take several minutes."
                        });
                        updateRevupAlert();
                    })
                    .fail(function(result) {
                        // hide the waiting
                        $dlg.revupDialogBox('hideWaiting');
                        $dlg.revupDialogBox('enableBtn', 'okBtn');
                        $dlg.revupDialogBox('enableBtn', 'cancelBtn');
                        // display the error
                        revupUtils.apiError('Save CSV', result, 'Unable to create CSV file at this time',
                                            'Unable to create the CSV file at this time');
                    })
                }
            });
        } // displayCSVExportDlg

        /*
         * New filter event
         */
        $('.newRankingDiv .lstDiv').on('revup.newFilter', function(e) {
            reloadRankingList();
        });

        /*
         * Partition or cycle
         */
        var partitionLstStartIndex = 0;
        var bHidePartition = partitionLst.length == 1;
        var bHideYears = (bHidePartition && partitionLst[0].displayValue.toLowerCase() == 'all');
        for (var i = 0; i < partitionLst.length; i++) {
            if (activePartition == partitionLst[i].value) {
                partitionLstStartIndex = i;
            }
        }
        activePartition = partitionLst[partitionLstStartIndex].value;

        // update the headerfor the giving year
        $givingHistoryHeader = $('.rankingNewLayout .lstDiv .lstColumnHeaderDiv .colGiving .columnHeader');
        yearsOfGiving = partitionLst[partitionLstStartIndex].yearsOfGiving;
        var headerText = 'Giving History';
        if ((yearsOfGiving && yearsOfGiving > 0) && (!bHideYears)) {
            headerText = yearsOfGiving + '+ Years of Giving';
        }
        $givingHistoryHeader.text(headerText);

        // see if need to be hidden
        if (bHidePartition) {
            $('.newRankingDiv .partitionGrpDiv').hide();
        }

        // partitions
        $('.newRankingDiv .partitionDiv').revupDropDownField({
            value: partitionLst,
            valueStartIndex: partitionLstStartIndex,
            sortList: true,
            sortDescending: true,
            changeFunc: function(index, val) {
                // save the partition value
                activePartition = val;

                // update header
                yearsOfGiving = partitionLst[index].yearsOfGiving;
                var headerText = 'Giving History';
                if (yearsOfGiving && yearsOfGiving > 0) {
                    headerText = yearsOfGiving + '+ Years of Giving';
                }
                $givingHistoryHeader.text(headerText);

                // reload the ranking list
                reloadRankingList();

                // save the current partition in session web storage - so next time click on ranking come to this partition
                localStorage.setItem('rankPartition-' + templateData.seatId, val);
                sessionStorage.analysisId = templateData.analysisId;
            }
        });

        // contact list
        var startIndex = 0;
        let activeContactSetId = contactSetActiveId.split(';;')[0];
        for (var i = 0; i < contactSetLst.length; i++) {
            if (contactSetLst[i].value == activeContactSetId) {
                startIndex = i;
                break;
            }
        }

        $('.newRankingDiv .contactLstDiv').revupDropDownField({
            value: contactSetLst,
            valueStartIndex: startIndex,
            sortList: false,
            bDisplaySectionHeaders: true,
            lstDataUrl: templateData.lstOfContactListApi,
            //lstRefreshAfter: 300000,  // 5 min
            lstDecodeFunc: function(data) {
                // get the active tab
                /*
                let $activeTab = $('.newRankingDiv .lstDiv .togglePages .contactSetTab.active');
                let activeTab = '';
                if ($activeTab.hasClass('personalTab')) {
                    activeTab = 'personalTab';
                }
                else if ($activeTab.hasClass('accountTab')) {
                    activeTab = 'accountTab'
                }
                */

                // save the updated contact set list
                contactSetLst = data;

                var index = 0;
                var lst = [];
                for (key in data) {
                    if ((activeTab == 'personalTab' &&  key != 'user')) {
                        continue;
                    }
                    else if (activeTab == 'accountTab' && !(key == 'account' || key == 'shared')) {
                        continue;
                    }

                    // section header
                    var o = new Object;
                    var prettyKey = key;
                    let typeDropDown = "";
                    if (prettySectionKey[key]) {
                        prettyKey = prettySectionKey[key];
                    }
                    if (prettyDropDown[key]) {
                        typeDropDown = ' (' + prettyDropDown[key] + ')';
                    }

                    o.sectionHeader = prettyKey;
                    o.index = index;
                    index++;
                    lst.push(o);

                    // display All Contacts first in the list
                    // that is the source is blank
                    var a = data[key];
                    var allId = -1;
                    for (var i = 0; i < a.length; i++) {
                        if (a[i].source == "") {
                            var o = new Object;
                            o.value = a[i].id;
                            allId = a[i].id;
                            o.displayValue = contactLstDisplayName(a[i]) + typeDropDown;
                            o.index = index;
                            index++;

                            lst.push(o);
                        }
                    }

                    for(var i = 0; i < a.length; i++) {
                        if (a[i].id != allId) {
                            var o = new Object;
                            o.value = a[i].id;
                            o.displayValue = contactLstDisplayName(a[i]);
                            o.index = index;
                            index++;

                            lst.push(o);
                        }
                    }
                }

                return lst;
            },
            changeFunc: function(index, val, displayVal) {
                // get the active contact set
                var activeContactSet = -1;
                for (key in contactSetLst) {
                    var data = contactSetLst[key];
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].id == val) {
                            activeContactSet = data[i];
                            activeContactSet.grpType = key;

                            if (key == 'shared') {
                                bSharedContact = true;
                            }
                            else {
                                bSharedContact = false;
                            }

                            break;
                        }
                    }
                }
                // save the active contact set id
                if (activeTab == 'personalTab') {
                    personalContactId = val + ';;' + activeContactSet.title + ';;' + activeContactSet.source + ';;' + activeContactSet.analysis.id + ';;' + activeContactSet.analysis.analysis_profile;
                    localStorage.setItem('rankPersonalContactSetId-' + templateData.filterVersion + '-' + templateData.seatId, personalContactId);
                }
                else if (activeTab == 'accountTab') {
                    accountContactId = val + ';;' + activeContactSet.title + ';;' + activeContactSet.source + ';;' + activeContactSet.analysis.id + ';;' + activeContactSet.analysis.analysis_profile;
                    localStorage.setItem('rankAccountContactSetId-' + templateData.filterVersion + '-' + templateData.seatId, accountContactId);
                }
                contactSetActiveId = val + ';;' + displayVal + ';;' + activeContactSet.source + ';;' + activeContactSet.analysis.id + ';;' + activeContactSet.analysis.analysis_profile;

                // display the loading spinner list section
                var $loadingLstDiv = $('.rankingNewLayout .lstDiv .rankingDetailsLstLoading');
                var $listDiv = $('.rankingNewLayout .lstDiv .rankingDetailsLstDiv');
                $loadingLstDiv.show(1);
                $listDiv.hide();

                // get the filters, contact details, and settings
                var urlFilter = templateData.filterListApiNew.replace('contactSetId', val);
                var contactSetDetailUrl = templateData.constactListDetailsApi.replace('contactSetId', val);
                var urlSetting = templateData.analysisViewUserSettingsUrl.replace('userSettingsId/', '') + "?analysis_profile=" + contactSetActiveId.split(';;')[4];
                $.when(
                    $.ajax({
                        url: urlFilter,
                        type: 'GET',
                    }),
                    $.ajax({
                        url: contactSetDetailUrl,
                        type: 'GET',
                    }),
                    $.ajax({
                        url: urlSetting,
                        type: 'GET',
                    })
                )
                .done(function (rFilter, contactSetDetails, rSettings) {
                    // load the new filters
                    loadFilters(rFilter[0]);

                    // clear filter and partition data in local storage
                    if (activeTab == 'personalTab') {
                        localStorage.removeItem('rankingFilterPersonal-' + templateData.filterVersion + '-' + templateData.seatId);
                        localStorage.removeItem('rankingFilterBasicPersonalData-' + templateData.filterVersion + '-' + templateData.seatId);
                        localStorage.removeItem('rankingFilterAdvPersonalData-' + templateData.filterVersion + '-' + templateData.seatId);
                    }
                    else if (activeTab == 'accountTab') {
                        localStorage.removeItem('rankingFilterAccount-' + templateData.filterVersion + '-' + templateData.seatId);
                        localStorage.removeItem('rankingFilterBasicAccountData-' + templateData.filterVersion + '-' + templateData.seatId);
                        localStorage.removeItem('rankingFilterAdvAccountData-' + templateData.filterVersion + '-' + templateData.seatId);
                    }
                    localStorage.removeItem('rankPartition-' + templateData.seatId);
                    localStorage.removeItem('rankingColWidth-' +  templateData.filterVersion + '-' + templateData.seatId);

                    // reset the filters
                    newFilters.reset();
                    $('.newRankingDiv .searchType').revupDropDownField('setValue', "name");
                    $('.newRankingDiv .searchBar').revupSearchBox('setValue', '');

                    // update the tab url
                    var $rankingTab = $('.sideNav .rankingTab a');
                    var href = $rankingTab.attr('href');
                    var index = href.indexOf('?contact_set');
                    if (index != -1) {
                        href = href.substring(0, index);
                    }
                    let activeContactSetId = contactSetActiveId.split(';;')[0];
                    $rankingTab.attr('href', href + "?contact_set=" + activeContactSetId)

                    // time to update the partition list
                    partitionLst = [];
                    var pData = contactSetDetails[0].analysis.partition_set.partition_configs;
                    for (var i = 0; i < pData.length; i++) {
                        var y = new Object();
                        y.value = pData[i].id;
                        y.displayValue= pData[i].label;
                        y.yearsOfGiving = pData[i].years_of_giving;

                        partitionLst.push(y);
                    }
                    activePartition = partitionLst[0].value;

                    $('.newRankingDiv .partitionDiv').revupDropDownField({
                        value: partitionLst,
                        valueStartIndex: 0,
                        updateValueList: true
                    })

                    // save permissions
                    contactSetPerm = contactSetDetails[0].permissions;

                    // load the new settings
                    var settings = rSettings[0].results[0];
                    if (settings.id != null) {
                        settingsPoliticalColor = settings.data[0].political_colors;
                        if (!templateData.isAcademic) {
                            settingsDisplayMaxedDonors = settings.data[0].display_maxed_donors;
                        }
                    }
                    else {
                        sttingsPoliticalColor = true;
                        if (!templateData.isAcademic) {
                            settingsDisplayMaxedDonors = true;
                        }
                    }
                    settingsUserId = settings.id;

                    // update the list
                    reloadRankingList();
                })
                .fail(function (r) {
                    // close the waiting
                    $listDiv.show(1);
                    $loadingLstDiv.hide()

                    // display the error
                    revupUtils.apiError('Ranking Details', r, 'Load Ranking List');
                    loadDataError($containerDiv);
                })
            },
        });

        // advance filter button
        $('.newRankingDiv .advFilterBtn').on('click', function(e) {
            var $btn = $(this);

            // see if active and if so make inactive otherwise make active
            if ($btn.hasClass('active')) {
                // set the global flag
                bAdvFilterMode = false;
                if (activeTab == 'personalTab') {
                    localStorage.setItem('rankingFilterStylePersonal-' + templateData.filterVersion + '-' + templateData.seatId, 'basic');
                }
                else if (activeTab == 'accountTab') {
                    localStorage.setItem('rankingFilterStyleAccount-' + templateData.filterVersion + '-' + templateData.seatId, 'basic');
                }
                // clear the button
                $btn.removeClass('active');

                // hide the new/advance filter section
                $('.rankingNewLayout .newFiltersSection').hide();

                // display the old quick filter section
                if (templateData.bDisplayQuickFilters) {
                    $('.rankingNewLayout .showingContactsDiv').show();
                }
                else {
                    $('.rankingNewLayout .showingContactsDiv').hide();
                }
                $('.rankingNewLayout .showingContactsLstDiv').hide();

                // disable and hide the search bar and search dropdowns
                $('.newRankingDiv .searchSection').removeClass('disabled');
                $('.newRankingDiv .searchType').revupDropDownField('enable');
                $('.newRankingDiv .searchBar').revupSearchBox('enable');
            }
            else {
                // set the global flag
                bAdvFilterMode = true;
                if (activeTab == 'personalTab') {
                    localStorage.setItem('rankingFilterStylePersonal-' + templateData.filterVersion + '-' + templateData.seatId, 'advance');
                }
                else if (activeTab == 'accountTab') {
                    localStorage.setItem('rankingFilterStyleAccount-' + templateData.filterVersion + '-' + templateData.seatId, 'advance');
                }

                // make active
                $btn.addClass('active')

                // show the new/advance filter section
                $('.rankingNewLayout .newFiltersSection').show();

                // hide the old quick filter section
                $('.rankingNewLayout .showingContactsDiv').hide();
                $('.rankingNewLayout .showingContactsLstDiv').hide();

                // disable and hide the search bar and search dropdowns
                $('.newRankingDiv .searchSection').addClass('disabled');
                $('.newRankingDiv .searchType').revupDropDownField('disable');
                $('.newRankingDiv .searchBar').revupSearchBox('disable');
                $('.newRankingDiv .searchType').revupDropDownField('setValue', 'name');
                $('.newRankingDiv .searchBar').revupSearchBox('setValue', '');
            }

            if (bAdvFilterMode) {
                // reset basic controls
                $('.newRankingDiv .searchType').revupDropDownField('setValue', "name");
                $('.newRankingDiv .searchBar').revupSearchBox('setValue', '');
                $('.rankingNewLayout .showingContactsLstDiv .entry .icon-check').hide();
                var $qFilterEntry = $('.rankingNewLayout .showingContactsLstDiv .entry[quickFilterVal=all]');
                $('.icon-check', $qFilterEntry).show();
                $('.rankingNewLayout .showingContactsDiv .showingWhat').attr('quickFilterVal', 'all');
                $('.rankingNewLayout .showingContactsDiv .showingWhat').text($('.text', $qFilterEntry).text());

                newFilters.loadAdvanceFilterData();
            }
            else {
                loadBasicFilterData();
            }

            // apply the filter
            reloadRankingList(true);
        })

        // type of search
        $('.newRankingDiv .searchType').revupDropDownField({
            value: [
                {displayValue: 'Name', value: 'name'},
                {displayValue: 'Location', value: 'location'}
            ],
            valueStartIndex: 0,
            sortList: false,
            changeFunc: function(index, val) {}
        })

        // new search bar
        $('.newRankingDiv .searchBar').revupSearchBox({
            placeholderText: 'Search',
//            value: "{{ request.REQUEST.query }}"}
            })
        .on('revup.searchFor', function(e) {
            buildBasicFilter();
        })
        .on('revup.searchClear', function(e) {
            buildBasicFilter();
        });

        // old style quick filters
        /*
         * Open/close showing Contacts
         */
        $('.newRankingDiv .lstDiv .showingContactsDiv .showingOpenClose').on('click', function() {
            var $openClose = $(this);
            var $showingWhat = $('.showingWhat', $openClose.closest('.showingContactsDiv'))
            var $showingContactsLstDiv = $('.newRankingDiv .lstDiv .showingContactsLstDiv');

            // get the current
            var openCloseState = $openClose.attr('openCloseState');
            if ((openCloseState == "") | (openCloseState == "closed")) {
                // time to open
                $openClose.attr('openCloseState', 'opened');
                $openClose.css('background-color', '#777');
                $('.icon', $openClose).removeClass('icon-double-arrow-dn')
                                      .addClass('icon-double-arrow-up');

                // show the checkmark on the selected field
                $('.entry .icon', $showingContactsLstDiv).hide();
                var whatContacts = $showingWhat.attr('quickFilterVal');
                $('.entry[quickFilterVal="' + whatContacts + '"] .icon', $showingContactsLstDiv).show(1)

                $showingContactsLstDiv.slideDown();
            }
            else {
                // time to open
                $openClose.attr('openCloseState', 'closed');
                $openClose.css('background-color', revupConstants.color.grayBorder);
                $('.icon', $openClose).removeClass('icon-double-arrow-up')
                                      .addClass('icon-double-arrow-dn');

                $showingContactsLstDiv.slideUp();
            }
        });

        $('.newRankingDiv .lstDiv .showingContactsLstDiv .entry').on('click', function() {
            var $entry = $(this);

            // get the values
            var quickFilterVal = $entry.attr('quickFilterVal');
            var text = $('.text', $entry).text();

            // save in session storage the current contactFilter
            //sessionStorage.contactFilter = quickFilterVal;
            //sessionStorage.analysisId = templateData.analysisId;

            // set the values
            $('.newRankingDiv .lstDiv .showingContactsDiv .showingWhat').attr('quickFilterVal', quickFilterVal)
                                                                        .text(text);

            // close
            $('.newRankingDiv .lstDiv .showingContactsDiv .showingOpenClose').trigger('click');

            // build the filter
            buildBasicFilter();
        });


        /*
         * click on list entry to update the details for contact
         */
        var bResize = false;
        var currentPosX;
        var $slider;
        var $left, $leftGrp;
        var $right, $rightGrp;
        var minRight, minLeft;
        $('.newRankingDiv .lstDiv .rankingLstDiv')
            .on('click', '.colPropectSwitch .addToBtn', function(e) {
                // add to prospect list
                var $name = $(this);
                var $entry = $name.closest('.entry');
                var contactId = $entry.attr('contactId');

                // see if disabled
                let $col = $name.closest('.colPropectSwitch');
                if ($col.hasClass('disabled')) {
                    return;
                }

                // make the ajax request
                var data = {};
                data.contact = contactId;
                $.ajax({
                    url: templateData.eventProspectsUrl,
                    data: data,
                    type: "POST"
                }).done(function() {
                    $(".colPropectSwitch", $entry).html('<div class="invited"><div class="icon icon-check"></div></div>');
                }).fail(function(r) {
                    revupUtils.apiError('Ranking Details', r, 'Set User as Prospect', "Unable to set contact as a prospect");
                });

                // stop propagation
                e.stopPropagation();
            })
            .on('click', '.colCallTimeSwitch .addToBtn, .colCallTimeSwitch .invited', function(e) {
                // add to call time list
                var $name = $(this);
                var $entry = $name.closest('.entry');

                // see if disabled
                let $col = $name.closest('.colCallTimeSwitch');
                if ($col.hasClass('disabled')) {
                    return;
                }

                // see if processing
                if ($name.data('inprocess'))
                    return;
                $name.data('inprocess', true)

                // create the url
                var url = templateData.callTimeContact;

                // see if delete or add
                let ajaxType = 'POST';
                let data = {}
                if ($name.hasClass('invited')) {
                    ajaxType = 'DELETE';

                    let callManagerContactId = $(this).parent().attr('callManagerContactId');
                    url += callManagerContactId + '/';
                }
                else if ($name.hasClass('addToBtn')) {
                    let contactId = $entry.attr('contactId');
                    data.contact = contactId
                }

                // make the ajax request
                $.ajax({
                    url: url,
                    data: data,
                    type: ajaxType
                }).done(function(r) {
                    if ($name.hasClass('addToBtn')) {
                        $(".colCallTimeSwitch", $entry).attr('callManagerContactId', r.id)
                        $(".colCallTimeSwitch", $entry).html('<div class="invited"><div class="icon icon-check"></div></div>');

                        $name.data('inprocess', false);
                    }
                    else if (($name.hasClass('invited'))) {
                        $(".colCallTimeSwitch", $entry).removeAttr('callManagerContactId')
                        $(".colCallTimeSwitch", $entry).html('<div class="addToBtn"><div class="icon icon-plus"></div></div>');

                        $name.data('inprocess', false);
                    }
                }).fail(function(r) {
                    revupUtils.apiError('Ranking Details', r, 'Add/Delete Call Time', "Unable to add or delete contact from call time");
                });

                // stop propagation
                e.stopPropagation();
            })
            .on('dblclick', '.nameBtn', function(e) {
                // ignore the double click until there is data
                if ($.isEmptyObject(detailData)) {
                    return;
                }

                var $name = $(this);
                var $entry = $name.closest('.entry');
                var contactId = $entry.attr('contactId');
                var resultId = $entry.attr('resultId');
                var partition = $('.newRankingDiv .partitionDiv').revupDropDownField('getValue');

                // if in the process of being delete then don't do anything
                if ($entry.hasClass('beingDeleted')) {
                    return;
                }

                var wWidth = $('body').width();
                var wHeight = $('body').height()

                // create the popup
                var sTmp = [];
                sTmp.push('<div class="mainPageLoading" style="height:500px">');
                    sTmp.push('<div class="loadingDiv">');
                        sTmp.push('<div class="loading"></div>');
                        sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                    sTmp.push('</div>');
                sTmp.push('</div>');
                $('body').css('overflow', 'hidden');
                var browserHeight = $(document).height();
                var viewportHeight = $(window).height();
                var $prospectDetailsDlg = $('body').revupDialogBox({
                    cancelBtnText: '',
                    okBtnText: 'Close',
                    extraClass: 'prospectDetailsPopUp',
                    msg: sTmp.join(''),
                    top: '20',
                    width: wWidth - 200 + 'px',
                    height: Math.min(browserHeight, viewportHeight) - 30,
                    cancelHandler: function() {
                        prospectDetails.close();
                        $('body').css('overflow', '');
                    },
                    okHandler: function() {
                        prospectDetails.close();
                        $('body').css('overflow', '');
                    }
                });

                // add the content
                function displayProspectsDetails()
                {
                    if ($.isEmptyObject(detailData)) {
                        setTimeout(displayProspectsDetails, 250)

                        return;
                    }

                    var content = buildProspectDetailsFrame();
                    $('.msgBodyScroll', $prospectDetailsDlg).html(content)

                    var prospectDetailData = {
                        // template data
                        contactId:          Number(contactId),
                        seatId:             Number(templateData.seatId),
                        accountId:          Number(templateData.accountId),
                        activeContactSetId: contactSetActiveId.split(';;')[0],
                        contactSetId:       Number(activeContactSetId),


                        // which partition is displayed
                        partitionLst:   partitionLst,
                        partitionStart: partition,

                        // urls
                        prospectDetailUrl:  templateData.prospectDetailUrl,
                    };
                    prospectDetails.display(prospectDetailData, detailData);
                }

                displayProspectsDetails();
            })
            .on('click', '.entry', function(e, setFocus) {
                // Allow focus to not be set by click
                setFocus = typeof setFocus !== 'undefined' ? setFocus : true;

                // if there is a time clear it
                if (upDownTimer != null) {
                    clearTimeout(upDownTimer);
                    upDownTimer = null;
                }

                if (bResize) {
                    bResize = false;

                    // stop propagation
                    e.stopPropagation();
                    return;
                }

                // get entry
                $entry = $(this);

                // entry is being deleted don't let it be selected
                if ($entry.hasClass('beingDeleted')) {
                    return;
                }

                // set focus
                if (setFocus){
                    $('.newRankingDiv .lstDiv .rankingDetailsLstDiv').focus();
                }

                // see if selected and if so done
                if ($entry.hasClass('selected')) {
                    return;
                }

                // clean selected and update new selected
                $('.entry', $rankingLst).removeClass('selected');
                $entry.addClass('selected');

                // get the data need to fetch details for this entry
                var contactId = $entry.attr('contactId');
                var resultId = $entry.attr('resultId');
                loadDetail(contactId, resultId);

                // stop propagation
                e.stopPropagation();
            })
            /*
            .on('contextmenu', '.entry', function(e) {
                // if there is a time clear it
                if (upDownTimer != null) {
                    clearTimeout(upDownTimer);
                    upDownTimer = null;
                }

                // see if selected and if so done
                $entry = $(this);
                $lst = $entry.parent();
                if (!$entry.hasClass('selected')) {
                    // clean selected and update new selected
                    $('.entry', $lst).removeClass('selected');
                    $entry.addClass('selected');
                    // get the data need to fetch details for this entry
                    var contactId = $entry.attr('contactId');
                    var resultId = $entry.attr('resultId');
                    loadDetail(contactId, resultId);
                }

                // display context menu
                let deltaWidth = ($(window).width() - $('.backgroundDiv').width()) / 2;
                $('.rankingContextMenu').hide()
                                        .finish()
                                        .show(100)
                                        .css({
                                            top: e.pageY + 'px',
                                            left: (e.pageX - deltaWidth) + 'px'
                                        })

                e.preventDefault();
            })
            */
            .on('keydown', '.rankingDetailsLstDiv', function(e) {
                var $entries = $('.entry', $(this));

                // get the index of the selected item
                var selectedIndex = $('.selected', $(this)).index() + 1;
                var numEnties = $entries.length

                // if the control or alt key press skip
                if (e.altKey || e.ctrlKey) {
                    return;
                }

                // compute the new index;
                var newIndex = selectedIndex;
                if (e.keyCode == 38) {
                    // up
                    if (selectedIndex != 1) {
                        newIndex--;
                    }
                }
                else if (e.keyCode == 40) {
                    // down
                    if (selectedIndex < numEnties) {
                        newIndex++;
                    }
                }
                else {
                    // if not the up or down arrow key processing
                    return;
                }

                if (selectedIndex != newIndex) {
                    $('.entry:nth-child(' + selectedIndex + ')', $rankingLst).removeClass('selected');
                    $('.entry:nth-child(' + newIndex + ')', $rankingLst).addClass('selected');
                }

                // if there is a time clear it
                if (upDownTimer != null) {
                    clearTimeout(upDownTimer);
                    upDownTimer = null;
                }

                // set the timer
                upDownTimer = setTimeout(function() {
                    if (selectedIndex != newIndex) {
                        // get the data need to fetch details for this entry
                        var contactId = $('.entry:nth-child(' + newIndex + ')', $rankingLst).attr('contactId');
                        var resultId = $('.entry:nth-child(' + newIndex + ')', $rankingLst).attr('resultId');
                        loadDetail(contactId, resultId);
                    }

                    // clear the function
                    upDownTimer = null;
                }, upDownDelay);


                // stop propagation
                e.stopImmediatePropagation();

                return false;
            })
            .on('scroll', function(e) {
                let $timeToAddMore = $('.entry.timeToAddMore', $rankingLst);
                if ($timeToAddMore.length > 0 && $timeToAddMore.visible($rankingLstDiv)) {
                    $timeToAddMore.removeClass('timeToAddMore');
                    $('.entry.loadingMore', $rankingLst).show();

                    // next
                    fetchNextChunk();
                }
            })
            .on('mousewheel', function(e) {
                if($rankingLstDiv.prop('scrollHeight') - $rankingLstDiv.scrollTop() <= $rankingLstDiv.height() && (e.originalEvent.wheelDelta < 0)) {
                    e.preventDefault()
                }
            })
            ;

        $('.newRankingDiv .lstDiv .lstColumnHeaderDiv')
            .on('mousedown', '.slider', function(e)
            {
                /*
                 * slider
                 */
                // which == 1 means left mouse button
                if (e.which == 1) {
                    // get the left column
                    $slider = $(this);
                    $left = $slider.prev();
                    var c = '.' + $left.attr('columnName');
                    $leftGrp = $(c);
                    minLeft = parseInt($(c, $('.lstColumnHeaderDiv')).attr('minColWidth'), 10);

                    // get the right column
                    $right = $slider.next();
                    var c = '.' + $right.attr('columnName');
                    $rightGrp = $(c);
                    minRight = parseInt($(c, $('.lstColumnHeaderDiv')).attr('minColWidth'), 10);

                    // set the starting point
                    currentPosX = e.pageX;

                    // make sure there is room to resize
                    if (($left.width() <= minLeft) && ($right.width() <= minRight)) {
                        e.preventDefault();

                        return;
                    }

                    $slider.addClass('draggable').parents().on('mousemove', function(e) {
                        if ($slider.hasClass('draggable')) {
                            // set flag resizing
                            bResize = true;

                            var deltaX = currentPosX - e.pageX;
                            currentPosX = e.pageX;

                            // compute the new size
                            var leftWidth = $left.outerWidth();
                            var rightWidth = $right.outerWidth();
                            var newWidthLeft = leftWidth - deltaX;
                            var newWidthRight = rightWidth + deltaX;

                            // make sure the new size meets the minimum
                            var bResizeCol = true;
                            if (newWidthLeft < minLeft) {
                                var d = leftWidth - minLeft;
                                if (d == 0) {
                                    bResizeCol = false;
                                }
                                else {
                                    newWidthLeft =  minLeft;
                                    newWidthRight = rightWidth + d;
                                }
                            }
                            else if (newWidthRight < minRight) {
                                var d = rightWidth - minRight;
                                if (d == 0) {
                                    bResizeCol = false;
                                }
                                else {
                                    newWidthRight = minRight;
                                    newWidthLeft = leftWidth + d;
                                }
                            }

                            //resize
                            if (bResizeCol) {
                                $leftGrp.outerWidth(newWidthLeft);
                                $rightGrp.outerWidth(newWidthRight);

                                // save the columns width
                                var wData = {};
                                wData.listWidth = $rankingLst.outerWidth();
                                var colData = templateData.listFields.headerFields;
                                for (var i = 0; i < colData.length; i++) {
                                    // save the column width
                                    if (colData[i].css) {
                                        colData[i].css = colData[i].css.replace(' sortable', '');
                                        wData[colData[i].css] = $('.column.' + colData[i].css).outerWidth();;
                                    }
                                }
                                localStorage.setItem('rankingColWidth-' + templateData.filterVersion + '-' + templateData.seatId, JSON.stringify(wData));
                            }
                        }
                    }).on('mouseup', function(e) {
                        // stop dragging
                        $slider.removeClass('draggable')
                    })
                }

                e.preventDefault();
            })
            .on('mouseup', function() {
                $('.draggable').removeClass('draggable')
            });

        // handle the resize of the window
        var resizeTimer = undefined;
        $(window)
            .on('resize', function(e) {
                // save only if resize of a column
                if (localStorage.getItem('rankingColWidth-' + templateData.filterVersion + '-'  + templateData.seatId)) {
                    var colData = templateData.listFields.headerFields;
                    for (var j = 0; j < colData.length; j++) {
                        var $col = $('.lstDiv .column.' + colData[j].css);
                        $col.css('width', colData[j].width)
                    }

                    var wData = {};
                    wData.listWidth = $rankingLst.outerWidth();
                    var colData = templateData.listFields.headerFields;
                    for (var i = 0; i < colData.length; i++) {
                        // save the column width
                        if (colData[i].css) {
                            wData[colData[i].css] = $('.column.' + colData[i].css).outerWidth();
                        }
                    }
                    localStorage.setItem('rankingColWidth-' + templateData.filterVersion + '-' + templateData.seatId, JSON.stringify(wData));
                }

                // resize the height of the ranking table
                let headerFooterHeight = rankingLstHeightDelta;
                if (localStorage.getItem('ranking-' + templateData.seatId + '-hideInfo') == 'true') {
                    headerFooterHeight -= rankingHeaderNoteDelta;
                }
                let h = $(window).height() - headerFooterHeight;
                $rankingLstDiv.height(h);
                $rankingLst.height(h + 1);
            })

        /*
         * Details Section
         */
        $('.newRankingDiv .detailDiv .rankingDetailsDetailDiv')
            .on('click', '.sendMailBtn', function(e, eSrc) {
                var $btn = $(this);

                // see if disabled
                if ($btn.hasClass('disabled')) {
                    return;
                }

                // get the emails and see what to display
                var emailLst = $btn.data('email');
                emailLst = emailLst.split(';;');

                if ((emailLst.length == 1) || (eSrc == 'fromContextMenu')) {
                    var href = 'mailto:' + emailLst[0];
                    window.location = href;
                }
                else {
                    var popupContent = [];
                    for (var i = 0; i < emailLst.length; i++) {
                        popupContent.push('<div class="entry">');
                            popupContent.push('<div class="emailAddr" href="mailto:' + emailLst[i] + '">' + emailLst[i] + '</div>');
                        popupContent.push('</div>')
                    }

                    var $popup = $btn.revupPopup({
                        extraClass: 'detailSendEmailPopup',
                        location: 'below',
                        content: popupContent.join(''),
                        minLeft: 130,
                        maxRight: 50,
                        //bEscClose: false,
                        bPassThrough: false,
                        //bDoNotCloseOverlay: true,
                    });

                    $('.entry .emailAddr', $popup).on('click', function(e) {
                        var eAddr = $(this).attr('href');
                        window.location = eAddr;

                        $popup.revupPopup('close');
                    })

                }
            })
            .on("click", ".callSheetBtn", function() {
                var $btn = $(this);
                var $detail = $btn.closest('.rankingDetailsDetailDiv');

                // see if disabled
                if ($btn.hasClass('disabled')) {
                    return;
                }

                // get the cycle/partition
                var currentPartition = $('.newRankingDiv .partitionDiv').revupDropDownField('getBothValues');

                // get the contact and result id
                var contactId = $detail.attr('contactId');
                var resultId = $detail.attr('resultId');

                // callsheet object
                var cObj = {};
                cObj.data = detailData;
                cObj.partitionObj = $('.newRankingDiv .partitionDiv').revupDropDownField('getBothValues');
                cObj.candidateName = templateData.candidateName;
                cObj.contactId = $detail.attr('contactId');
                cObj.resultId = $detail.attr('resultId');
                cObj.accountId = templateData.accountId;
                cObj.seatId = templateData.seatId;
                let activeContactSetId = contactSetActiveId.split(';;')[0];
                cObj.contactSetId = activeContactSetId;
                cObj.saveNotes = contactSetPerm.notes;
                cObj.notesApi = templateData.callsheetNotesApi;
                if (templateData.isAcademic) {
                    cObj.config = {
                        editorSection: {
                            display: true,
                            editors: [
                                {
                                    header: "Notes",
                                    height: "12em",
                                }
                            ]
                        },
                        contact: {
                            display: true,
                            displayTop: false,

                            firstName: {
                                section: 'contact',
                                field: 'first_name',
                            },
                            lastName: {
                                section: 'contact',
                                field: 'last_name',
                            },
                            phone: {
                                label: 'Phone',
                                section: 'features',
                                fieldStartWith: 'Purdue Alum Lookup',
                                signalSet: 'Purdue Bio',
                                subField: 'match',
                                keyLabel: ['Cell', 'Home', 'Work'],
                                key: ['mobile_phone', 'home_phone', 'business_phone'],
                                trimOffFront: 5,
                            },
                            email: {
                                label: 'Email',
                                section: 'features',
                                fieldStartWith: 'Purdue Alum Lookup',
                                signalSet: 'Purdue Bio',
                                subField: 'match',
                                key: 'email'
                            },
                        },
                        contactDetails: {
                            display: true,
                            section: "features",
                            field: "Purdue Alum Lookup",
                            data: [
                                {
                                    subField: 'match',
                                    signalSet: 'Purdue Bio',
                                    type: 'attr',
                                    attr: [
                                        {
                                            key: 'school',
                                            label: 'Graduated',
                                            type: 'text',
                                        },
                                        {
                                            key: 'major',
                                            label: 'Major',
                                            type: 'text',
                                        },
                                        {
                                            key: 'degree',
                                            label: 'Degree',
                                            type: 'degree'
                                        },
                                        {
                                            key: 'graduation_year',
                                            label: 'Year Graduated',
                                            type: 'year'
                                        },
                                        {
                                            key: 'employer',
                                            label: 'Employer',
                                            type: 'text'
                                        },
                                        {
                                            key: 'title',
                                            label: 'Occupation',
                                            type: 'text'
                                        }
                                    ]
                                },
                                {
                                    subField: 'match',
                                    signalSet: 'Purdue Activities',
                                    type: 'activity',
                                    key: 'activity',
                                    bDeDup: true,
                                    label: 'Clubs/Activity'
                                }
                            ]
                        },
                        contribution: {
                            display: true,
                            leftSide: [
                                {
                                    section: 'key_signals',
                                    field: 'giving',
                                    type: 'givingTotal',
                                    title: 'Total Contributions Since %cycleYear%:',
                                    bUseTitle2: ((partitionLst.length == 1) && (partitionLst[0].displayValue.toLowerCase() == 'all')),
                                    title2: 'Total Contributions:',
                                },
                                {
                                    type: 'givingFromSrc',
                                    title: 'Total Contributions Made to %srcValue%',

                                    srcSection: 'features',
                                    srcFieldStartWith: 'Purdue Alum Lookup',
                                    srcSignalSet: 'Purdue Bio',
                                    srcSubField: 'match',
                                    srcKey: 'school',

                                    dstSection: 'features',
                                    dstFieldStartWith: 'Purdue Giving',
                                    dstSignalSet: 'Purdue Giving',
                                    dstSubField: 'match',
                                    dstSrcMatchKey: 'school',
                                    dstKey: 'amount',
                                }
                            ],
                            rightSide: [
                                {
                                    section: 'features',
                                    fieldStartWith: 'Purdue Giving',
                                    subField: 'match',
                                    type: 'givingOrgBy',
                                    label: 'Top Contributions',
                                    disaplyField: 'school',
                                    orgBy: "school",
                                    thenBy: 'date',
                                    maxOrgByDisplay: 5,
                                    bDisplayTotal: true,
                                },
                            ],
                        },
                    }
                }
                if (templateData.isPoliticalCampaign || templateData.isPoliticalCommittee) {
                    cObj.config = {
                        editorSection: {
                            display: true,
                            editors: [
                                {
                                    header: "Ask",
                                    height: "6em",
                                },
                                {
                                    header: "Results / Notes",
                                    height: "6em",
                                }
                            ]
                        },
                        contact: {
                            display: true,
                            displayTop: false,

                            firstName: {
                                section: 'contact',
                                field: 'first_name',
                            },
                            lastName: {
                                section: 'contact',
                                field: 'last_name',
                            },
                            phone: {
                                label: 'Phone',
                                section: 'contact',
                                field: 'phone_numbers',
                                trimOffFront: 7,
                            },
                            email: {
                                label: 'Email',
                                section: 'contact',
                                field: 'email_addresses'
                            },
                            employer: {
                                label: 'Employer',
                                section: 'contact',
                                field: 'organizations',
                                fieldKey: 'name',
                            },
                            occupation: {
                                label: 'Occupation',
                                section: 'contact',
                                field: 'organizations',
                                fieldKey: 'title',
                            },
                        },
                        headerRight: {
                            display: true,
                            section: "features",
                            fieldStartWith: "Client Matches",
                            fields: [
                                {
                                    key: 'giving_availability',
                                    label: 'in giving availability',
                                    type: 'amountOrMsg',
                                },
                            ]
                        },
                        contribution: {
                            display: true,
                            leftSide: [
                                {
                                    section: 'key_signals',
                                    field: 'giving',
                                    type: 'givingTotal',
                                    title: 'Total Contributions Since %cycleYear% Election Cycle:',
                                },
                                {
                                    section: 'features',
                                    fieldStartWith: 'Client Matches',
                                    subField: 'giving',
                                    type: 'givingTotal',
                                    title: 'Total Contributions to Your Candidate:'
                                },
                                {
                                    section: 'features',
                                    fieldStartWithCand: "Client Matches",
                                    fieldStartWithOther: "Key Contributions",
                                    title: "Top Contributions",
                                    type: "candidateContribution",
                                }
                            ],
                            rightSide: [
                                {
                                    section: 'features',
                                    fieldStartWith: 'Federal Matches',
                                    subField: 'match',
                                    type: 'giving',
                                    label: 'Most recent Federal Contributions',
                                    disaplyField: 'recipient',
                                    maxDisplay: 10,
                                    bDisplayTotal: true,
                                },
                                {
                                    section: 'features',
                                    fieldStartWith: 'State Matches',
                                    subField: 'match',
                                    type: 'giving',
                                    label: 'Most recent State Contributions',
                                    disaplyField: 'recipient',
                                    maxDisplay: 5,
                                    bDisplayTotal: true,
                                },
                                {
                                    section: 'features',
                                    fieldStartWith: 'Local Matches',
                                    subField: 'match',
                                    type: 'giving',
                                    label: 'Most recent Local Contributions',
                                    disaplyField: 'recipient',
                                    maxDisplay: 5,
                                    bDisplayTotal: true,
                                }
                            ],
                        },
                    }
                }

                // create the call sheet
                callSheet.create(cObj);
            })
            .on("click", ".prospectDetailsBtn", function(e) {
                var browserHeight = $(document).height();
                var viewportHeight = $(window).height();
                var $btn = $(this);
                var $detail = $btn.closest('.rankingDetailsDetailDiv');

                // get data
                var analysisId = $detail.attr('analysisId');
                var contactId = $detail.attr('contactId');
                var resultId = $detail.attr('resultId');
                var partition = $detail.attr('activePartition');

                // if in the process of being delete then don't do anything
                let $entry = $('.entry.selected', $rankingLst)
                if ($entry.hasClass('beingDeleted')) {
                    return;
                }

                var wWidth = $('body').width();
                var wHeight = $('body').height()

                // create the popup
                var sTmp = [];
                sTmp.push('<div class="mainPageLoading" style="height:500px">');
                    sTmp.push('<div class="loadingDiv">');
                        sTmp.push('<div class="loading"></div>');
                        sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                    sTmp.push('</div>');
                sTmp.push('</div>');
                $('body').css('overflow', 'hidden');
                var $prospectDetailsDlg = $('body').revupDialogBox({
                    cancelBtnText: '',
                    okBtnText: 'Close',
                    extraClass: 'prospectDetailsPopUp',
                    msg: sTmp.join(''),
                    top: '20',
                    width: wWidth - 200 + 'px',
                    // height: wHeight - 550 + 'px',
                    height: Math.min(browserHeight, viewportHeight)-30,
                    cancelHandler: function() {
                        prospectDetails.close();
                        $('body').css('overflow', '');
                    },
                    okHandler: function() {
                        prospectDetails.close();
                        $('body').css('overflow', '');
                    },
                })

                setTimeout(function() {
                    // add the content
                    var content = buildProspectDetailsFrame();
                    $('.msgBodyScroll', $prospectDetailsDlg).html(content)

                    var prospectDetailData = {
                        // template data
                        contactId:          Number(contactId),
                        seatId:             Number(templateData.seatId),
                        accountId:          Number(templateData.accountId),
                        activeContactSetId: contactSetActiveId.split(';;')[0],
                        contactSetId:       Number(activeContactSetId),

                        // which partition is displayed
                        partitionLst:   partitionLst,
                        partitionStart: partition,

                        // urls
                        prospectDetailUrl:  templateData.prospectDetailUrl,
                    };
                    prospectDetails.display(prospectDetailData, detailData);
                }, 1);
            })
            .on('click', '.editContactBtn', function(e) {
                let $entry = $('.entry.selected', $rankingLst)
                let contactId = $entry.attr('contactId');
                let contactName = $('.colName .nameBtn', $entry).html();

                editContact.load('editAContact', activeTab, contactId, contactName);

            })
            .on('click', '.deleteContactBtn', function(e) {
                let $btn = $(this);

                // if disabled
                if ($btn.hasClass('disabled')) {
                    return;
                }

                let $entry = $('.entry.selected', $rankingLst)
                let contactId = $entry.attr('contactId');
                let contactName = $('.colName .nameBtn', $entry).html();
                deleteContact(contactId, contactName);
            })

        var detailOffset = $('.rankingNewLayout .rankingDetailsDetailDiv').offset();
        $(window).on('scroll', function(e) {
            var scrollTop = $(this).scrollTop();
            var offsetTop = $('.rankingNewLayout').position().top +
                            $('.rankingNewLayout .lstDiv .rankingHeaderDiv').outerHeight(true) +
                            $('.rankingNewLayout .lstDiv .newFiltersSection').outerHeight(true);

            var deltaTop = scrollTop - offsetTop;

            // adjust for the height of the details section
            var detailHeight = $('.rankingNewLayout .rankingDetailsDetailDiv').height();
            //var detailOffset = $('.rankingNewLayout .rankingDetailsDetailDiv').offset();
            var winHeight = $(window).height();
            var deltaHeight = detailHeight - winHeight;

            // if the detail section is as tall or taller don't move it
            if ((detailHeight + offsetTop) > winHeight) {
                $('.rankingNewLayout .detailDiv').css('position', '');
                $('.rankingNewLayout .detailDiv').css('top', '');
                bAutoScrollDetails = false;

                return;
            }

            bAutoScrollDetails = true;

            if (deltaTop > 0) {
                $('.rankingNewLayout .detailDiv').css('position', 'relative');
                $('.rankingNewLayout .detailDiv').css('top', deltaTop + 44);
            }
            else {
                $('.rankingNewLayout .detailDiv').css('top', 0)
            }
        })

        // broadcast events
        $(document)
            .on('deleteContact', function(e) {
console.log('deleteContact broadcast - ' + e.contactId + ', ' + e.contactName + ', ' + e.currentTarget.nodeName)
                deleteContact(e.contactId, e.contactName, true);
            })
            .on('editContact', function(e) {
console.log('editContact broadcast - ' + e.contactId + ', ' + e.contactName + ', ' + e.currentTarget.nodeName + ', contactData: ', e.contactData)
            let $entry = $('.entry.selected', $rankingLst)
            var contactId = $entry.attr('contactId');
            var resultId = $entry.attr('resultId');

            // update the contact info
            $('.rankingDetailDiv .namePhotoDiv').html(buildDetailNamePhotoSection(e.contactData));
            });

    } // addEventAndWidgetHandlers

    function loadDataError($div)
    {
        var sTmp = [];
        sTmp.push('<div class="errorMsg">');
            sTmp.push('<div class="msg">');
                sTmp.push("Unable to load Ranking Data at this time<br><br>Try again later");
            sTmp.push('</div>')
        sTmp.push('</div>')

        //
        $('.rankingNewLayout', $div).html(sTmp.join(''));
        $('.rankingDetailsLoading', $div).hide();
        $("html, body").scrollTop(0);
        $('.rankingNewLayout', $div).show(1);
    } // loadDataError

    /*
     * Load the list of filters
     */
    function loadFilters(filterResults)
    {
        // add the new filters
        filterData = {

            // id: id of the filter
            // action: filter action (search for xxx (ie name, location...))
            // popupDesc: String to appear in the value popup - description of the value
            // desc: String to be displayed in the tooltip with that value
            filters: [
            ],

            quickFilters: [],

            // grouped filters
            grpFilters: [],

            // hide the icon at the beginning
            bHideLeadingIcon: true,
            bDisplayQuickFilters: templateData.bDisplayQuickFilters,

            seatId: templateData.seatId,
            filterVersion: templateData.filterVersion,
        };

        // id: id of the quick filter
        // value: value of the quick filter (if multi part the parts are seperated via double semicolons (ie ;;))
        // label: label displayed in the dropdown list
        // desc: String displayed as the tooltip
        if (templateData.isAcademic) {
                //{id: 0, value: 'all', label: 'All', desc: 'All Contacts'},
            filterData.quickFilters.push({id: 1, value: 'client-giving:true', label: 'Have Given', desc:'Contacts Who Gave to My Institution'})
            filterData.quickFilters.push({id: 2, value: 'client-giving:true;;client-giving-this-year:false', label: 'Not Given This Year', desc:'Contacts Who Gave to My Institution In Previous Years But Not This Year'})
            filterData.quickFilters.push({id: 3, value: 'high-potential-donor:true', label: 'High Potential', desc:'High Potential Contacts Who Gave to Other Causes'})
            filterData.quickFilters.push({id: 4, value: 'client-giving:false;;non-client-giving:false', label: 'Never Given', desc:'Contacts Who Never Gave to My Institution'})
            // filterData.quickFilters.push({id: 5, value: 'client-largest-donor:true', label: 'Largest Contributors', desc:'Largest Institution Contributors'})
            filterData.quickFilters.push({id: 6, value: 'aca-ditr:true', label: 'Diamonds in the Rough', desc: 'Diamonds in the Rough'});
        }
        else {
            //{id: 0, value: 'all', label: 'All', desc: 'All Contacts'},
            filterData.quickFilters.push({id: 1, value: 'client-giving-current:true', label: 'Current Cycle', desc: 'Contacts who gave to my candidate/committee in the current cycle'});
            filterData.quickFilters.push({id: 2, value: 'client-giving-past:true;;client-giving-current:false', label: 'Previous but not current cycles', desc: 'Contacts who gave to my candidate/committee in previous cycles but not the current cycle'});
            filterData.quickFilters.push({id: 3, value: 'ally-giving:true', label: 'Similar candidates', desc:  'Contacts who gave to candidates/committees highly similar to mine'});
            filterData.quickFilters.push({id: 4, value: 'oppo-giving:true', label: 'Opposition', desc: 'Contacts who gave to the opposition'});
            filterData.quickFilters.push({id: 5, value: 'client-giving-current:false;;client-giving-past:false', label: 'Never Gave', desc: 'Contacts who never gave to the candidate/committee'});
            filterData.quickFilters.push({id: 6, value: 'ditr:true', label: 'Diamonds in the rough', desc: 'Diamonds in the Rough'});
        }

        // the filters to the data
        var numLoadedFilters = filterData.filters.length;
        for (var i = 0; i < filterResults.count; i++) {
            var fResults = filterResults.results[i];
            var fType = fResults.type ? fResults.type.toLowerCase() : '';

            // make sure a supported type
            var bSkip = true;
            if ((fType === 'text') || (fType === 'dropdown') || (fType === 'choices') || (fType === 'autocomplete')) {
                bSkip = false;
            }
            if (bSkip) {
                continue;
            }

            var f = new Object();

            f.id = numLoadedFilters + i;

            // generic
            f.label = fResults.label;
            f.displayType = fResults.type;

            if (fResults.display_type) {
                f.displayTypeDetails = fResults.display_type.toLowerCase();
            }

            if (fResults.desc) {
                f.popupDesc = fResults.desc;
            }
            else {
                f.popupDesc = '';
            }

            if (fResults.display_length) {
                f.maxLength = fResults.display_length;
            }

            // expected
            f.action = fResults.expected[0].field;
            f.actionType = fResults.expected[0].type;

            // display type specific data
            if (f.displayType && f.displayType.toLowerCase() === 'dropdown') {
                f.dropdownData = [];

                if (fResults.dropdown_data) {
                    if (fResults.dropdown_data.toLowerCase() === 'usstates' ) {
                        f.dropdownData = revupConstants.dropDownStates;
                    }
                }
                else {
                    var c = fResults.choices;
                    for (var ci = 0; ci < c.length; ci++) {
                        var o = new Object();

                        o.value = c[ci][0];
                        o.displayValue = c[ci][1];
                        if (f.displayTypeDetails === 'date') {
                            var d = o.displayValue.split('-');
                            o.displayValue = d[1] + '/' + d[2] + '/' + d[0];
                        }

                        f.dropdownData.push(o);
                    }
                }

                // sort order
                f.bSortOrderDecending = false;
                if (fResults.sortOrder && fResults.sortOrder == 'descending') {
                    f.bSortOrderDecending = true;
                }
            }
            else if (f.displayType && f.displayType.toLowerCase() === 'choices') {
                f.choiceData = [];

                var c = fResults.choices;
                for (var ci = 0; ci < c.length; ci++) {
                    var o = new Object();

                    o.value = c[ci][0];
                    o.displayValue = c[ci][1];

                    f.choiceData.push(o);
                }
            }
            else if (f.displayType && f.displayType.toLowerCase() === 'autocomplete') {
                f.autocompleteUrl = fResults.url;
            }

            // add to the filters
            filterData.filters.push(f);

            // add which tab callback
            filterData.whichTab = function() {
                if (activeTab == 'personalTab') {
                    return 'Personal';
                }
                else if (activeTab == 'accountTab') {
                    return 'Account';
                }

                return '';
            }

            // add to the grouped filters
            var grp = fResults.group;
            if (!grp || grp == '') {
                grp = "unGrouped"
            }
            if (!filterData.grpFilters[grp]) {
                filterData.grpFilters[grp] = [];
            }
            filterData.grpFilters[grp].push(f);
        }

        let currentFilter = '';
        let filterMode = '';
        if (activeTab == 'personalTab') {
            currentFilter = localStorage.getItem('rankingFilterAdvPersonalData-' + templateData.filterVersion + '-' + templateData.seatId);
            filterMode = localStorage.getItem('rankingFilterStylePersonal-' + templateData.filterVersion + '-' + templateData.seatId);
        }
        if (activeTab == 'accountTab') {
            currentFilter = localStorage.getItem('rankingFilterAdvAccountData-' + templateData.filterVersion + '-' + templateData.seatId);
            filterMode = localStorage.getItem('rankingFilterStyleAccount-' + templateData.filterVersion + '-' + templateData.seatId);
        }
        if (currentFilter) {
            currentFilter = JSON.parse(currentFilter);
        }
        newFilters.init($('.newRankingDiv .rankingNewLayout .lstDiv .newFiltersSection'), filterData, currentFilter);

        // set to basic or advanced
        filterMode = filterMode == null ? 'basic': filterMode;
        if (filterMode == 'basic') {
            loadBasicFilterData();
        }
        else if (filterMode == 'advance') {
            // make active
            $('.rankingNewLayout .advFilterSection').addClass('active')

            // show the new/advance filter section
            $('.rankingNewLayout .newFiltersSection').show();

            // hide the old quick filter section
            $('.rankingNewLayout .showingContactsDiv').hide();
            $('.rankingNewLayout .showingContactsLstDiv').hide();

            // disable and hide the search bar and search dropdowns
            $('.newRankingDiv .searchSection').addClass('disabled');
            $('.newRankingDiv .searchType').revupDropDownField('disable');
            $('.newRankingDiv .searchBar').revupSearchBox('disable');
            $('.newRankingDiv .searchType').revupDropDownField('setValue', '');
            $('.newRankingDiv .searchBar').revupSearchBox('setValue', 'name');
        }
    } // loadFilters

    return {
        display: function(pTemplateData, $containerDiv) {
            // save the passed in values
            // analysisId = pTemplateData.analysisId;
            templateData = pTemplateData;

            // load quick filters for basic filters
            if (templateData.isAcademic) {
                quickFilters.push({id: 0, value: 'all', label: 'All', desc: 'All Contacts'});
                quickFilters.push({id: 1, value: 'client-giving:true', label: 'Have Given', desc:'Contacts Who Gave to My Institution'})
                quickFilters.push({id: 2, value: 'client-giving:true;;client-giving-this-year:false', label: 'Not Given This Year', desc:'Contacts Who Gave to My Institution In Previous Years But Not This Year'})
                quickFilters.push({id: 3, value: 'high-potential-donor:true', label: 'High Potential', desc:'High Potential Contacts Who Gave to Other Causes'})
                quickFilters.push({id: 4, value: 'client-giving:false;;non-client-giving:false', label: 'Never Given', desc:'Contacts Who Never Gave to My Institution'})
                // quickFilters.push({id: 5, value: 'client-largest-donor:true', label: 'Largest Contributors', desc:'Largest Institution Contributors'})
                quickFilters.push({id: 6, value: 'aca-ditr:true', label: 'Diamonds in the Rough', desc: 'Diamonds in the Rough'});
            }
            else {
                quickFilters.push({id: 0, value: 'all', label: 'All', desc: 'All Contacts'});
                quickFilters.push({id: 1, value: 'client-giving-current:true', label: 'Current Cycle', desc: 'Contacts who gave to my candidate/committee in the current cycle'});
                quickFilters.push({id: 2, value: 'client-giving-past:true;;client-giving-current:false', label: 'Previous but not current cycles', desc: 'Contacts who gave to my candidate/committee in previous cycles but not the current cycle'});
                quickFilters.push({id: 3, value: 'ally-giving:true', label: 'Similar candidates', desc:  'Contacts who gave to candidates/committees highly similar to mine'});
                quickFilters.push({id: 4, value: 'oppo-giving:true', label: 'Opposition', desc: 'Contacts who gave to the opposition'});
                quickFilters.push({id: 5, value: 'client-giving-current:false;;client-giving-past:false', label: 'Never Gave', desc: 'Contacts who never gave to the candidate/committee'});
                quickFilters.push({id: 6, value: 'ditr:true', label: 'Diamonds in the rough', desc: 'Diamonds in the Rough'});
            }

            // make sure there is a context set
            if (!templateData.contactSet) {
                var sTmp = [];
                sTmp.push('<div class="errorMsg">');
                    sTmp.push('<div class="msg">');
                        sTmp.push('<div class="task-update-analysis-msg"></div>');
                        sTmp.push('<div class="loadingDiv" style="margin-top:30px;width:120px;height:120px;margin-left:auto;margin-right:auto;">');
                            sTmp.push('<div class="loading" style="width:120px;height:120px; margin-left:0;left:0"></div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');

                $('.newRankingDiv').html(sTmp.join(''));

                // Once the task API responds, we can inform the user of their task status
                if ($navbarMsg){
                    $navbarMsg.on("tasksUpdate",
                        function(event, uploadingContacts, analyzingContacts, bUploadDone, bAnalysisDone, bContactDeleteDone){
                            var msg;
                            if (uploadingContacts){
                                msg = "We are uploading and analyzing your contacts right now!"
                            }
                            else if (!uploadingContacts && analyzingContacts){
                                msg = "We have your contacts, and we are analyzing them right now!"
                            }
                            else if (!uploadingContacts && !analyzingContacts && (bUploadDone && bAnalysisDone && bContactDeleteDone)){
                                location.reload()
                            }
                            else {
                                $('.errorMsg .msg').html(
                                    'You do not have any Rankings. <br />Please go to the ' +
                                    '"Contact Manager" tab to load contacts and run an analysis.');
                                return
                            }
                            $('.task-update-analysis-msg').html(msg);
                    });
                }
                return
            }

            // see if the overlay should be displayed
            var locUrl = window.location;
            var locUrlArgs = locUrl.search.match(/[^&?]*?=[^&?]*/g);
            if ( locUrlArgs != null ) {
                // see if the overlay should be displayed
                var prospectDetailsReturnUrl = false;
                var prospectDetailsId = -1;
                var prospectDetailsCloseArgs = [];
                var bShowProspectDetialsOver = false;
                var redirect = '';
                var skipLocalStorage = false;
                var contact_set = {};
                for (var i = 0; i < locUrlArgs.length; i++) {
                    var arg = locUrlArgs[i];
                    var argParts = arg.split('=');
                    if ($.trim(argParts[0]) === 'contact_set') {
                        contact_set = $.trim(argParts[1]);
                    }
                    if ($.trim(argParts[0]) === 'contact_id') {
                        bShowProspectDetialsOver = true;
                        prospectDetailsId = argParts[1];
                    }
                    if ($.trim(argParts[0]) === 'redirectFrom') {
                        if($.trim(argParts[1]) == 'listMan'){
                            skipLocalStorage = true;
                        }
                        //in case there is a redirect from a page besides the List Manager for now, we will break it at this point
                        else{
                            contact_set = -1;
                            break;
                        }
                    }
                    else {
                        prospectDetailsCloseArgs.push(arg)
                    }
                }
                var prospectDetailsReturnUrl = locUrl.origin + locUrl.pathname;
                if (prospectDetailsCloseArgs.length > 0) {
                    prospectDetailsReturnUrl += "?" + prospectDetailsCloseArgs.join('&');
                }
            }

            // build the partitions
            partitionLst = [];
            var pData = templateData.contactSet.analysis.partition_set.partition_configs;
            var pActIndex = 0;
            for (var i = 0; i < pData.length; i++) {
                var y = new Object();
                y.value = pData[i].id;
                y.displayValue= pData[i].label;
                y.yearsOfGiving = pData[i].years_of_giving;

                partitionLst.push(y);

                if (pData[i].id == templateData.partitionStart) {
                    pActIndex = pData[i].id;
                }
            }
            if (templateData.partitionStart == -1) {
                activePartition = partitionLst[0].value;
            }
            else {
                activePartition = pActIndex;
            }

            // user settings
            if ($.isEmptyObject(templateData.userSettings.data)) {
                settingsPoliticalColor = true;
                if (!templateData.isAcademic) {
                    settingsDisplayMaxedDonors = true;
                }
            }
            else {
                settingsPoliticalColor = templateData.userSettings.data.political_colors;
                if (!templateData.isAcademic) {
                    settingsDisplayMaxedDonors = templateData.userSettings.data.display_maxed_donors;
                }
            }
            if (templateData.userSettings.id) {
                settingsUserId = templateData.userSettings.id;
            }

            // save permissions
            contactSetPerm = templateData.contactSet.permissions;

            // load the list view definitions
            revupListView.loadDefinition(templateData.listFields);

            // build the div's need
            var sTmp = [];
            sTmp.push('<div class="rankingDetailsLoading" style="height:500px;width:100%">');
                sTmp.push('<div class="loadingDiv">');
                    sTmp.push('<div class="loading"></div>');
                    sTmp.push('<img class="loadingImg" src="' + loadingImg + '">')
                sTmp.push('</div>');
            sTmp.push('</div>');
            sTmp.push('<div class="rankingNewLayout" style="display:none;"></div>')
            $containerDiv.html(sTmp.join(''));

            // make sure that the forward/back button casue a reload
            window.onpopstate = function(event) {
                if(event && event.state) {
                    location.reload();
                }
            }

            // cleanup old filters
            /*
            localStorage.removeItem('rankingFilterAdvPersonalData-' + templateData.seatId);
            localStorage.removeItem('rankingFilterAdvAccountData-' + templateData.seatId);
            localStorage.removeItem('rankingFilterBasicPersonalData-' + templateData.seatId);
            localStorage.removeItem('rankingFilterBasicAccountData-' + templateData.seatId);
            localStorage.removeItem('rankingFilterPersonal-' + templateData.seatId);
            localStorage.removeItem('rankingFilterAccount-' + templateData.seetId);
            localStorage.removeItem('rankingFilterStylePersonal-' + templateData.seatId);
            localStorage.removeItem('rankingFilterStyleAccount-' + templateData.seatId);
            */

            // get the active tab before getting the ranking list
            // this is need first so the correct search filter can be loaded
            $.ajax({
                url: templateData.lstOfContactListApi,
                type: "GET"
            })
            .done(function (r) {
                var contactLstResults = r;
                    // get the saved contact id for the account and personal tab, and active tab
                    let storedPersonalContactSetId  = localStorage.getItem('rankPersonalContactSetId-' + templateData.filterVersion + '-' + templateData.seatId);
                    let storedAccountContactSetId  = localStorage.getItem('rankAccountContactSetId-' + templateData.filterVersion + '-' + templateData.seatId);
                    personalContactId = -1;
                    accountContactId = -1;
                    bSharedContact = false;
                    activeTab = localStorage.getItem('rankActiveTab-' + templateData.seatId);

                    // make sure we land on the account tab if rediected from List Manager
                    if (activeTab == null || skipLocalStorage) {
                        activeTab = 'accountTab';
                    }
                    // make sure the contact sets are still value
                    let personalActiveContactSet = {};
                    let accountActiveContactSet = {}

                    // personal
                    if (contactLstResults['user']) {
                        let allContactsIndex = -1;
                        for (let i = 0; i < contactLstResults['user'].length; i++) {
                            // index of all contacts
                            if (contactLstResults['user'][i].title.toLowerCase() == allContactsTitle) {
                                allContactsIndex = i;
                            }

                            // see if the contact id is in the list
                            if (storedPersonalContactSetId) {
                                let contactIdParts = storedPersonalContactSetId.split(';;');
                                if (contactLstResults['user'][i].id == contactIdParts[0]) {
                                    personalContactId = storedPersonalContactSetId;
                                }
                            }
                        }

                        if (personalContactId == -1 && allContactsIndex != -1) {
                            personalContactId =  contactLstResults['user'][allContactsIndex].id;
                            personalContactId += ';;' + contactLstResults['user'][allContactsIndex].title;
                            personalContactId += ';;' + contactLstResults['user'][allContactsIndex].source ;
                            personalContactId += ';;' + contactLstResults['user'][allContactsIndex].analysis.id
                            personalContactId += ';;' + contactLstResults['user'][allContactsIndex].analysis.analysis_profile;
                        }
                    }
                    else {
                        storedPersonalContactSetId = null;
                    }

                    // account
                    if (contactLstResults['account']) {
                        let allContactsIndex = -1;
                        for (let i = 0; i < contactLstResults['account'].length; i++) {
                            // index of all contacts
                            if (contactLstResults['account'][i].title.toLowerCase() == allContactsTitle) {
                                allContactsIndex = i;
                            }

                            if(!skipLocalStorage){
                                if (storedAccountContactSetId) {
                                    let contactIdParts = storedAccountContactSetId.split(';;');
                                    if (contactLstResults['account'][i].id == contactIdParts[0]) {
                                        accountContactId = storedAccountContactSetId;
                                    }
                                }
                            }

                            else if(skipLocalStorage){
                                if(contactLstResults['account'][i].id == contact_set){
                                    // skipping local storage and reinforcing the structure
                                    accountContactId = contactLstResults['account'][i].id + ';;' + contactLstResults['account'][i].title + ';;' + contactLstResults['account'][i].source + ';;' + contactLstResults['account'][i].analysis['id'] + contactLstResults['account'][i].analysis['analysis_profile'];
                                }
                            }

                            // check shared if not found
                            if (accountContactId == -1 && storedAccountContactSetId) {
                                let contactIdParts = storedAccountContactSetId.split(';;');
                                for (let i = 0; i < contactLstResults['shared'].length; i++) {
                                    // see if the contact id is in the list
                                    if (contactLstResults['shared'][i].id == contactIdParts[0]) {
                                        accountContactId = storedAccountContactSetId;
                                        bSharedContact = true;
                                    }
                                }
                            }

                            if (accountContactId == -1 && allContactsIndex != -1) {
                                accountContactId  = contactLstResults['account'][allContactsIndex].id;;
                                accountContactId += ';;' + contactLstResults['account'][allContactsIndex].title;
                                accountContactId += ';;' + contactLstResults['account'][allContactsIndex].source ;
                                accountContactId += ';;' + contactLstResults['account'][allContactsIndex].analysis.id
                                accountContactId += ';;' + contactLstResults['account'][allContactsIndex].analysis.analysis_profile;
                            }
                        }
                    }

                        // make sure the correct tab is active, based on if any section is empty
                        if (accountContactId == -1 && personalContactId == -1) {
                            activeTab = '';
                        }
                        else if (accountContactId == -1) {
                            activeTab = 'personalTab'
                            activeContactSet = personalActiveContactSet;
                        }
                        else if (personalContactId == -1) {
                            activeTab = 'accountTab'
                            activeContactSet = accountActiveContactSet;
                        }

                        // set the active contactSet
                        if (activeTab == 'accountTab' && accountContactId != -1) {
                            contactSetActiveId = accountContactId;
                        }
                        else {
                            contactSetActiveId = personalContactId;
                        }

                    // get up the initial contact set
                    let contactSetParts = contactSetActiveId.split(';;')
                    contactSetLst = [];
                    var o = new Object;
                    let dObj = {}
                    dObj.source = contactSetParts[2];
                    dObj.title = contactSetParts[1];
                    o.value = contactSetParts[0];
                    o.displayValue = contactLstDisplayName(dObj);
                    contactSetLst.push(o);

                    // filter list api
                    var urlFilter = templateData.filterListApiNew.replace('contactSetId', contactSetActiveId.split(';;')[0]);

                    // get filter
                    let filter = '';
                    if(!skipLocalStorage){ //ignoring filters if redirected
                        if (activeTab == 'personalTab') {
                            filter = localStorage.getItem('rankingFilterPersonal-' + templateData.filterVersion + '-' + templateData.seatId);
                        }
                        else if (activeTab == 'accountTab') {
                            filter = localStorage.getItem('rankingFilterAccount-' + templateData.filterVersion + '-' + templateData.seatId);
                        }
                    }

                    // mixpanel
                    mixpanel.track("Ranking",
                                {"analysisId": templateData.analysisId,
                                 "partitionId": activePartition,
                                 "filter": filter,
                                })

                    // create the url
                    var urlArgs = [];
                    var url2 = templateData.contactDetailAnalysisUrlNew.replace('seatId', templateData.seatId)
                                                                       .replace('contactSetId', contactSetActiveId.split(';;')[0]);

                    urlArgs.push('partition=' + activePartition);
                    urlArgs.push("page=1");// + templateData.paginationStart);
                    urlArgs.push("page_size=" + pageSize);
                    if (getUrlArgs.analysis !== undefined) {
                        urlArgs.push("analysis=" + getUrlArgs.analysis);
                    }

                    if (filter) {
                        if (!templateData.bDisplayQuickFilters) {
                            // in no quick filter then make sure not in the filter string
                            var f = filter.split('&');
                            filter = [];
                            for (var i = 0; i < f.length; i++) {
                                if (!f[i].startsWith('filter=')) {
                                    urlArgs.push(f[i]);
                                }
                            }
                        }
                        else {
                            urlArgs.push(filter);
                        }
                    }
                    url2 += "?" + urlArgs.join('&');

                // filters to the url
                if (!templateData.isAcademic && !settingsDisplayMaxedDonors) {
                    url2 += "&filter=client-maxed:false";
                }

                $.when(
                    $.ajax({
                        url: encodeURI(url2),
                        type: "GET",
                    }),
                    $.ajax({
                        url: urlFilter,
                        type: "GET"
                    }),
                )
                .done(function(contactResultsRaw, filterResultsRaw, contactLstResultsRaw) {
                    var contactResults = contactResultsRaw[0];
                    var filterResults = filterResultsRaw[0];

                    if (bShowProspectDetialsOver) {
                        $('.rankingDetailsLoading', $containerDiv).hide();
                        $('.rankingNewLayout', $containerDiv).show(1);

                        var wWidth = $('body').width();
                        var wHeight = $('body').height()

                        // create the popup
                        var sTmp = [];
                        sTmp.push('<div class="mainPageLoading" style="height:500px">');
                            sTmp.push('<div class="loadingDiv">');
                                sTmp.push('<div class="loading"></div>');
                                sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                            sTmp.push('</div>');
                        sTmp.push('</div>');
                        var $prospectDetailsDlg = $('body').revupDialogBox({
                            cancelBtnText: '',
                            okBtnText: 'Close',
                            okHandler: function() {
                                window.location = prospectDetailsReturnUrl;

                                prospectDetails.close();
                            },
                            cancelHandler: function() {
                                window.location = prospectDetailsReturnUrl;

                                prospectDetails.close();
                            },
                            autoCloseOk: false,
                            extraClass: 'prospectDetailsPopUp',
                            msg: sTmp.join(''),
                            top: '75',
                            width: wWidth - 200 + 'px',
                            height: wHeight - 150 + 'px'
                        })

                        var urlArgs = [];
                        let activeContactSetId = contactSetActiveId.split(';;')[0];
                        var url2 = templateData.contactDetailUrlNew.replace('seatId', templateData.seatId)
                                                                   .replace('contactSetId', activeContactSetId)
                                                                   .replace("contactId", prospectDetailsId);
                        urlArgs.push('partition=' + activePartition);
                        if (getUrlArgs.analysis !== undefined) {
                            urlArgs.push("analysis=" + getUrlArgs.analysis);
                        }
                        $.ajax({
                            url: url2 + '?' + urlArgs.join('&'),
                            type: "GET",
                        })
                        .done(function(r) {
                            // save the detail data
                            detailData = r;

                            var content = buildProspectDetailsFrame();
                            $('.msgBodyScroll', $prospectDetailsDlg).html(content)

                            var prospectDetailData = {
                                // template data
                                contactId:          Number(prospectDetailsId),
                                analysisId:         Number(templateData.analysisId),
                                accountId:          Number(templateData.accountId),


                                // which partition is displayed
                                partitionLst:   partitionLst,
                                partitionStart: 0,

                                // urls
                                prospectDetailUrl:  templateData.prospectDetailUrl,
                            };
                            prospectDetails.display(prospectDetailData, detailData);
                        })
                        .fail(function(r) {
                            revupUtils.apiError('Ranking Details', r, 'Get Contact Details Prospect Details');

                            var sTmp = [];
                            sTmp.push('<div class="errorMsgDiv">');
                                sTmp.push('<div class="errorMsg">');
                                    sTmp.push('Unable to display Prospect Details');
                                sTmp.push('</div>');
                            sTmp.push('</div>');

                            $('.msgBody', $prospectDetailsDlg).html(sTmp.join(''));
                        })

                        return
                    }

                    // build and the html for the page
                    var html = buildHtml(contactResults);
                    $('.rankingNewLayout', $containerDiv).html(html);

                    // selectors for the ranking list
                    $rankingLstDiv = $('.rankingNewLayout .lstDiv .rankingLstDiv');
                    $rankingLst = $('.rankingNewLayout .lstDiv .rankingLstDiv .rankingDetailsLstDiv');
                    $rankingLstFooter = $('.newRankingDiv .lstDiv .lstFooterDiv');
                    $rankingLstHeader = $('.newRankingDiv .lstDiv .lstColumnHeaderDiv');

                    // get the attribute for infinit scrolling
                    $rankingLstDiv.attr('currentPage', 1);
                    $rankingLstDiv.attr('maxPages', Math.ceil(contactResults.count / pageSize));
                    $rankingLstDiv.attr('numEntries', contactResults.count);
                    $rankingLstDiv.attr('numEntriesLoaded', contactResults.results.length);

                    // update footer count
                    $('.lstFooterDiv .showing').append(parseInt($rankingLstDiv.attr('numEntriesLoaded')));
                    $('.lstFooterDiv .total').prepend(parseInt($rankingLstDiv.attr('numEntries')));

                    setTimeout(function() {
                        // enable/disable the correct columns
                        if (activeTab == 'personalTab') {
                            $('.column.colCallTimeSwitch').addClass('disabled');
                            $('.column.colPropectSwitch').removeClass('disabled');
                        }
                        else if (activeTab == 'accountTab') {
                            $('.column.colCallTimeSwitch').removeClass('disabled');
                            $('.column.colPropectSwitch').addClass('disabled');
                        }
                    }, 10);

                    // set height or the ranking list
                    let headerFooterHeight = rankingLstHeightDelta;
                    // if (localStorage.getItem('ranking-' + templateData.seatId + '-hideInfo') == 'true') {
                    //     headerFooterHeight -= rankingHeaderNoteDelta;
                    // }
                    let h = $(window).height() - headerFooterHeight;
                    $rankingLstDiv.height(h);
                    $rankingLst.height(h + 1);

                    // load the details of the first entry
                    if (contactResults && contactResults.results && contactResults.results.length > 0) {
                        loadDetail(contactResults.results[0].contact.id, contactResults.results[0].id);
                        $('.entry[resultId="'+ contactResults.results[0].id + '"]', $rankingLst).addClass('selected');
                    }

                    // render the widgets and add event handlers
                    addEventAndWidgetHandlers(templateData);

                    // load filter data
                    loadFilters(filterResults);

                    // close handler for info
                    $('.infoBarDiv .infoBarCloseDiv .icon').on('click', function(e) {
                        $('.infoBarDiv').hide();

                        // store closed
                        localStorage.setItem('ranking-' + templateData.seatId + '-hideInfo', 'true');
                    })

                    if (contactResults.count == 0) {
                        var $loadingDiv = $('.rankingNewLayout .detailDiv .rankingDetailsDetailLoading');
                        var $detailDiv = $('.rankingNewLayout .detailDiv .rankingDetailsDetailDiv');
                        var $loadingDetailDiv = $('.rankingNewLayout .detailDiv .rankingDetailsDetailLoading');
                        var $listDiv = $('.rankingNewLayout .lstDiv .rankingDetailsLstDiv');

                        // hide footer and header
                        $rankingLstFooter.hide();
                        $rankingLstHeader.hide();

                        // display message
                        var sTmp = [];
                        sTmp.push('<div class="errorMsg">');
                            sTmp.push('<div class="msg">');
                                sTmp.push("No entries match the current filter.");
                            sTmp.push('</div>');
                        sTmp.push('</div>');
                        $listDiv.html(sTmp.join(''));

                        // update the details panel - to blank
                        sTmp = [];
                        sTmp.push('<div style="width:100%;height:350px"></div>');
                        $detailDiv.html(sTmp.join(''));
                        $loadingDetailDiv.hide();
                        $("html, body").scrollTop(0);
                        $detailDiv.show(1);
                    }
                    else {
                        // add the pagination control
                        if (!templateData.isAcademic && !settingsDisplayMaxedDonors) {
                            $rankingLstFooter.html(paginationExtraMsg);
                        }
                    }

                    // hide the loading div and display the details
                    $("html, body").scrollTop(0);
                    $('.rankingDetailsLoading', $containerDiv).hide();
                    $('.rankingNewLayout', $containerDiv).show(1, function() {
                        // see if the list resize since last load
                        var colWidth = localStorage.getItem('rankingColWidth-' +  templateData.filterVersion + '-' + templateData.seatId);
                        if (colWidth) {
                            colWidth = JSON.parse(colWidth);

                            var listWidth = $('.newRankingDiv .lstDiv').outerWidth();
                            if (colWidth.listWidth != listWidth) {
                                // resize the columns
                                var bMinHit = false;
                                var colData = templateData.listFields.headerFields;

                                var sWidth = 0;
                                for (var i = 0; i < colData.length; i++) {
                                    if (!colData[i].resizePercent) {
                                        var $col = $('.lstDiv .column.' + colData[i].css);
                                        var cWidth = $col.width();
                                        sWidth += cWidth;
                                    }
                                }
                                sWidth += colData.length * 20;      // margin
                                sWidth += (colData.length - 1) * 5; // sliders

                                for (var i = 0; i < colData.length; i++) {
                                    // see if a resizeable column
                                    if (colData[i].resizePercent) {
                                        var newWidth = $('.lstDiv').width();
                                        var wDelta = newWidth - (sWidth + 20);
                                        var resizePercent = colData[i].resizePercent / 100;
                                        var w = Math.floor(wDelta * resizePercent);
                                        var $col = $('.lstDiv .column.' + colData[i].css);
                                        // $col.width(w);

                                        // if the resizeable column gets to small reset columns
                                        if (w < colData[i].minColWidth) {
                                            bMinHit = true;
                                            break;
                                        }
                                        else {
                                            $col.width(w);
                                        }
                                    }
                                }

                                if (bMinHit) {
                                    for (var j = 0; j < colData.length; j++) {
                                        var $col = $('.lstDiv .column.' + colData[j].css);
                                        $col.css('width', colData[j].width)
                                    }
                                }
                            }
                        }
                        else if (templateData.listFields.autoSizeCol) {
                            //find out how wide the column needs to be
                            var $sizeCol = $('.rankingNewLayout .lstDiv .rankingLstDiv .column.' + templateData.listFields.autoSizeCol);
                            var maxSize = 0;
                            $sizeCol.each(function() {
                                var $this = $(this);

                                var content = $this.html();
                                var w = $this.textWidth();
                                if (w > maxSize) {
                                    maxSize = w;
                                }
                            });

                            var sizeWidth = $sizeCol.width();
                            if (maxSize > sizeWidth) {
                                var delta = maxSize - sizeWidth;

                                // get the columns
                                var $shinkCol = $('.rankingNewLayout .lstDiv .column.' + templateData.listFields.autoShrinkCol);
                                var $growCol = $('.rankingNewLayout .lstDiv .column.' + templateData.listFields.autoSizeCol);

                                // make sure the shink column does not get to small
                                var shinkWidth = $shinkCol.width();
                                if ((shinkWidth - delta) < templateData.listFields.autoShrinkColMin) {
                                    delta = templateData.listFields.autoShrinkColMin - (shinkWidth - delta);
                                }

                                if (delta > 0) {
                                    $shinkCol.width(shinkWidth - delta);

                                    var growWidth = $growCol.width();
                                    $growCol.width(growWidth + delta);
                                }
                            }
                        }
                    })
                })
                .fail(function(r) {
                    revupUtils.apiError('Ranking Details', r, 'Load Ranking List');

                    loadDataError($containerDiv);
                })
            })
            .fail(function(r) {
                revupUtils.apiError('Contact List', r, 'Unable to load the contact list and ranking list');

                loadDataError($containerDiv);
            })
            .always(function(r) {
                // trigger like clicking on the first entry
                var $first = $('.rankingNewLayout .rankingDetailsLstDiv .entry:first-child', $containerDiv);
                $first.trigger('click', [false]);
            });
        } // display
    };
} ());
