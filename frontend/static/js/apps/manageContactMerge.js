var manageContactMerge = (function () {
    "use strict";

    const contactsPerPage = 25;
    const percentForMarker = 0.7;
    const timeToAddMoreIndex = Math.ceil(contactsPerPage * percentForMarker);

    let $dlg = $();
    let $contactDataDlg = $();
    let $contactHeader = $();
    let $contactLst =  $();
    let $contactScroll = $();
    let $contactData = $();
    let contactDataHtml = 'Coming Soon';

let numEntries = 30;

    // load details
    let loadDetails = (contactId) => {
        // clear the details section and display the loading spinner
        $('.rightSide .detailsSection .scrollContainer', $dlg).html('');
        $('.rightSide .loadingDiv', $dlg).show();

        // build url
        let url;
        var currentlyActive = $('.togglePages .editTab.active');
        if(currentlyActive.hasClass("accountTab")){
            url = getSharedDetailsNeedAttention;
        }
        else if(currentlyActive.hasClass("personalTab")){
            url = getContactDetailsNeedAttention;
        }
        url = url.replace('contactId', contactId);

        // get details
        $.ajax({
            type: 'GET',
            url: url,
        })
        .done(function(r) {
            // update the header of the details
            let $dataInHeader = $('.dataInParentDiv', $dlg);
            if (r.notes && r.notes.lenght > 0) {
                $dataInHeader.attr('notesId', r.notes[0].id)
            }
            else {
                $dataInHeader.removeAttr('notesId');
            }

            // display the header and the buttons
            $('.rightSide .detailsSection .header .column', $dlg).show()
            $('.rightSide .detailsSection .bottomButtons .toTheLeft', $dlg).show()
            $('.rightSide .detailsSection .bottomButtons .toTheRight', $dlg).show()

            // build and load the detail data
            let contacts = buildDetailsContacts(r.contacts)
            $('.rightSide .detailsSection .scrollContainer', $dlg).html(contacts);

            // update the count
            $('.numContactMsg', $dlg).html(r.contacts.length + ' Contacts')
                                     .attr('numContacts', r.contacts.length);

            contactDataHtml = buildContactData(r.contacts, r.call_time_contact, r.notes);
        })
        .fail(function(r) {
            revupUtils.apiError('Conact Details', r, 'Contact Details',
                                "Unable to fetch contact details");
        })
        .always(function(r) {
            $('.rightSide .loadingDiv', $dlg).hide();
        })
    } // loadDetails

    // load contacts
    let loadContacts = () => {
        // get the contact type
        let contactType = 'personal';
        if ($('.editTab.accountTab', $dlg).hasClass('active')) {
            contactType = 'shared';
        }

        // see if the add more should be added at a location
        //see if the first list
        let currentPage = 1;
        let bLoadNext = true;
        if ($contactLst.attr('currentPage') == undefined) {
            $contactLst.attr('currentPage', 1);
            $contactLst.attr('numEntries', numEntries);
            $contactLst.attr('maxPages', Math.ceil(numEntries / contactsPerPage));

            // clear the list if first
            $contactLst.html('');

            // load the wait spinner
            $('.lstLoadDiv', $dlg).show();
            $('.contactLstDiv', $dlg).hide();
        }
        else {
            if ($contactLst.attr('currentPage') != $contactLst.attr('maxPages')) {
                currentPage = parseInt($contactLst.attr('currentPage'), 10) + 1;
                $contactLst.attr('currentPage', currentPage);
            }
            else {
                bLoadNext = false;
            }
        }

        // build the url
        let url = '';
        var currentlyActive = $('.togglePages .editTab.active');
        if(currentlyActive.hasClass("accountTab")){
            url = getSharedNeedAttentionLst;
        }
        else if(currentlyActive.hasClass("personalTab")){
            url = getContactNeedAttentionLst;
        }

        let searchFor = $contactLst.attr('searchFor');
        var a = [];
        a.push('page_size=' + contactsPerPage);
        if (currentPage) {
            a.push('page=' + currentPage);
        }
        if (searchFor) {
            a.push('search=' + searchFor);
        }
        if (a.length > 0) {
            url += "?" + a.join("&");
        }

        $.ajax({
            type: 'GET',
            url: url,
        })
        .done(function(r) {
            let results = r.results;

            if (results.length == 0) {
                // clear the details section and hide the loading spinner
                $('.rightSide .detailsSection .scrollContainer', $dlg).html('');
                $('.rightSide .loadingDiv', $dlg).hide();
                $('.rightSide .detailsSection .header .column', $dlg).hide()
                $('.rightSide .detailsSection .bottomButtons .toTheLeft', $dlg).hide()
                $('.rightSide .detailsSection .bottomButtons .toTheRight', $dlg).hide()

                // display the no contacts message
                // hide the lstLoadDiv and contactLstDiv
                $('.lstLoadDiv', $dlg).hide();
                $('.contactLstDiv', $dlg).hide();

                // display the message display
                $('.noContactDiv', $dlg).show();

                return;
            }

            // see of the load next should be added
            let bAddTimeToAdd = false;
            if (results.length == contactsPerPage) {
                bAddTimeToAdd = true;
            }

            // load the contactLst
            let sTmp = [];
            let selectContactId = -1;   // -1 done select
            for (let i = 0; i < results.length; i++) {
                // see if the first entry
                let extraClass = "";
                if (i === 0 && currentPage === 1) {
                    extraClass += ' selected';
                    selectContactId = results[i].id;

                    $contactLst.attr('numEntries', r.count);
                    $contactLst.attr('maxPages', Math.ceil(r.count / contactsPerPage));
                }
                if (bAddTimeToAdd && timeToAddMoreIndex == i) {
                    extraClass += ' timeToAddMore'
                }

                // build the name
                let name = '';
                if (results[i].first_name) {
                    name += results[i].first_name;
                }
                if (results[i].last_name) {
                    if (name != '') {
                        name += ' ';
                    name += results[i].last_name;
                    }
                }
                sTmp.push('<div class="entry' + extraClass + '"' + ' contactId="' + results[i].id + '">');
                    sTmp.push('<div class="column nameCol">');
                        sTmp.push('<div class="nameContainer">' + name + '</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            }

            if (bAddTimeToAdd) {
                sTmp.push('<div class="entry loadingMore" style="display:none">');
                    sTmp.push('<div class="loadingMore"></div>')
                sTmp.push('</div>');
            }

            $('.entry.loadingMore', $contactLst).remove();
            $('.contactLstDiv .contactLst', $dlg).append(sTmp.join(''));

            // reset the  the wait and display the list
            $('.lstLoadDiv', $dlg).hide();
            $('.noContactDiv', $dlg).hide();
            $('.contactLstDiv', $dlg).show();

            // if contact selected then load the details
            if (selectContactId != -1) {
                loadDetails(selectContactId);
            }
        })
        .fail(function(r) {
            revupUtils.apiError('Contact Cleanup', r, 'Contact Cleanup',
                                "Unable to list of contact needing cleanup");
        })

        // see of done
        if (!bLoadNext) {
            return;
        }

    } // loadContacts

    let buildDetailsContacts = (contactData) => {
        let sTmp = [];

        for (let i = 0; i < contactData.length; i++) {
            let data = contactData[i];
            let name = '';
            if (data.first_name) {
                name += data.first_name;
            }
            if (data.additional_name) {
                if (name != '') {
                    name += '&nbsp;'
                }
                name += data.additional_name
            }
            if (data.last_name) {
                if (name != '') {
                    name += '&nbsp;'
                }
                name += data.last_name;
            }

            sTmp.push('<div class="contactDiv" contactId="' + data.id + '">');
                sTmp.push('<div class="btnDiv">');
                    sTmp.push('<div class="leftSide">');
                        sTmp.push('<div class="addBtn">');
                            sTmp.push('<div class="icon frontIcon icon-box-unchecked"></div>');
                            sTmp.push('<div class="icon backIcon icon-check-box-back" style="color:' + revupConstants.color.whiteText + '"></div>')
                        sTmp.push('</div>')
                        sTmp.push('<div class="nameText">' + name + '</div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="rightSide">');
                        sTmp.push('<div class="rollUpDownBtn">');
                            let icon = 'icon-triangle-dn';
                            // if (i > 0) {
                            //     icon = 'icon-triangle-right';
                            // }
                            sTmp.push('<div class="icon ' + icon + '"></div>');
                        sTmp.push('</div>')
                    sTmp.push('</div>');
                sTmp.push('</div>');

                sTmp.push('<div class="detailsDiv">');
/*
                    sTmp.push('<div class="detailsGrp">')
                        // name
                        sTmp.push('<div class="line labelLine">')
                            sTmp.push('<div class="col firstName">First Name:</div>');
                            sTmp.push('<div class="col middleName">Middle Name:</div>');
                            sTmp.push('<div class="col lastName">Last Name:</div>');
                        sTmp.push('</div>')
                        sTmp.push('<div class="line dataLine">')
                            sTmp.push('<div class="col firstName">');
                                if (data.first_name)
                                    sTmp.push(data.first_name)
                                else
                                    sTmp.push('&nbsp;')
                            sTmp.push('</div>');
                            sTmp.push('<div class="col middleName">');
                                if (data.additional_name)
                                    sTmp.push(data.additional_name)
                                else
                                    sTmp.push('&nbsp;')
                            sTmp.push('</div>');
                            sTmp.push('<div class="col lastName">');
                                if (data.last_name)
                                    sTmp.push(data.last_name)
                                else
                                    sTmp.push('&nbsp;')
                            sTmp.push('</div>');
                        sTmp.push('</div>')
                    sTmp.push('</div>');
                    */

                    // contact source
                    if (data.import_record.length > 0) {
                        let importRecord = data.import_record[0];
                        sTmp.push('<div class="detailsGrp">')
                            sTmp.push('<div class="line labelLine">Contact Source:</div>')

                            let contactDetail = '';
                            let importDate = '';
                            let time = '';
                            let d = '';
                            if (importRecord) {
                                if (importRecord.import_dt) {
                                    d = 'on '+ revupUtils.formatDate(importRecord.import_dt);

                                    time = new Date(Date.parse(importRecord.import_dt));
                                    var hr = time.getHours();
                                    var min = time.getMinutes();
                                    if (min < 10) {
                                        min = "0" + min;
                                    }
                                    var ampm = "am";
                                    if( hr > 12 ) {
                                        hr -= 12;
                                        ampm = "pm";
                                    }
                                    time = ' at ' + hr + ':' + min + ampm
                                }

                                if (importRecord.label) {
                                    contactDetail = ' - ' + importRecord.label;

                                    if (time) {
                                        contactDetail += ', ' + d + time
                                    }
                                }
                            }

                            let src = [];
                            switch (importRecord.import_type.toLowerCase()) {
                                case 'li':
                                case 'linkedin-oauth2':
                                    // linkedIn
                                    src.push('<div xstyle="top:2px;" class="imgDiv">');
                                        src.push('<div class="linkedInGlyph"></div>');
                                    src.push('</div>');
                                    src.push('<div class="col imgText">LinkedIn' + contactDetail + '</div>');
                                    break;
                                case 'gm':
                                case 'google-oauth2':
                                    // google - gmail
                                    src.push('<div class="imgDiv">');
                                        src.push('<div class="gmailGlyph"></div>');
                                    src.push('</div>');
                                    src.push('<div class="col imgText">Gmail' + contactDetail + '</div>');
                                    break;
                                case 'ou':
                                case 'outlook':
                                    // cloudsponge outlook
                                    src.push('<div class="imgDiv">');
                                        src.push('<div class="outlookGlyph"></div>');
                                    src.push('</div>');
                                    src.push('<div class="col imgText">Outlook' + contactDetail + '</div>');
                                    break;
                                case 'cv':
                                case 'csv':
                                    // csv
                                    src.push('<div style="top:2px;" class="imgDiv">');
                                        src.push('<div class="csvGlyph"></div>');
                                    src.push('</div>');
                                    src.push('<div class="col imgText">CSV' + contactDetail + '</div>');
                                    break;
                                case 'ap':
                                case 'apply':
                                case 'vc':
                                case 'vcard':
                                case 'ot':
                                case 'ab':
                                    src.push('<div class="imgDiv">');
                                        src.push('<div class="appleGlyph"></div>');
                                    src.push('</div>');
                                    src.push('<div class="col imgText">vCard' + contactDetail + '</div>');
                                    break;
                                case 'ip':
                                case 'iphone':
                                case 'mb':
                                case 'mobile':
                                    src.push('<div class="imgDiv">');
                                        src.push('<div class="iPhoneGlyph"></div>');
                                    src.push('</div>');
                                    src.push('<div class="col imgText">iPhone' + contactDetail + '</div>');
                                    break;
                                default:
                                    //style = ' style="top:-1px;"';
                                    src.push('<div style="top:2px;" class="imgDiv">');
                                        src.push('<div class="unknownGlyph"></div>');
                                    src.push('</div>');
                                    src.push('<div class="col imgText">Unkown</div>');
                                    break;
                            } // end
                            sTmp.push('<div class="line dataLine">');
                                sTmp.push(src.join(''));
                            sTmp.push('</div>');
                        sTmp.push('</div>');
                    }

                    // row up & down sections
                    let exStyle = '';
                    // if (i > 0) {
                    //     exStyle = ' style="display:none;"'
                    // }
                    sTmp.push('<div class="rollUpDownDiv"' + exStyle + '>');
                        // address
                        let addr = data.addresses;
                        if (addr.length > 0) {
                            for (let j = 0; j < addr.length; j++) {
                                sTmp.push('<div class="detailsGrp">');
                                    let addrLabel = 'Address';
                                    if (addr.length > 1) {
                                        addrLabel += ' ' + (j + 1) + ':';
                                    }

                                    sTmp.push('<div class="line labelLine">')
                                        sTmp.push(addrLabel);
                                    sTmp.push('</div>')

                                    let addrTmp = [];
                                    if (addr[j].street)
                                        addrTmp.push(addr[j].street)
                                    if (addr[j].po_box)
                                        addrTmp.push(addr[j].po_box)
                                    if (addr[j].city || addr[j].region || addr[j].post_code) {
                                        let t = ''
                                        t += addr[j].city;
                                        if (addr[j].city != '' &&  (addr[j].region || addr[j].post_code))
                                            t += ','
                                        if (addr[j].region) {
                                            if (t != '')
                                                t += ' ';
                                            t += addr[j].region;
                                        }
                                        if (addr[j].post_code) {
                                            if (t != '')
                                                t += ' ';
                                            t += addr[j].post_code
                                        }

                                        addrTmp.push(t)
                                    }
                                    if (addr[j].country)
                                        addrTmp.push(addr[j].country)

                                    sTmp.push('<div class="line dataLine">');
                                        sTmp.push(addrTmp.join('<br>'));
                                    sTmp.push('</div>')
                                sTmp.push('</div>');
                            }
                        }

                        // email
                        let email = data.email_addresses;
                        if (email.length > 0) {
                            sTmp.push('<div class="detailsGrp">');
                                let label = 'Email Address';
                                if (email.length > 1)
                                    label += 'es';
                                label += ':'

                                sTmp.push('<div class="line labelLine">')
                                    sTmp.push(label);
                                sTmp.push('</div>')

                                let d = [];
                                for (let j = 0; j < email.length; j++) {
                                    let a = email[j].address;
                                    if (email[j].label) {
                                        a += ' (' + email[j].label + ')';
                                    }

                                    d.push(a)
                                }

                                sTmp.push('<div class="line dataLine">');
                                    sTmp.push(d.join('<br>'));
                                sTmp.push('</div>')
                            sTmp.push('</div>');
                        }

                        // phone
                        let phone = data.phone_numbers;
                        if (phone.length > 0) {
                            sTmp.push('<div class="detailsGrp">');
                                let label = 'Phone Number';
                                if (phone.length > 1)
                                    label += 's';
                                label += ':'

                                sTmp.push('<div class="line labelLine">')
                                    sTmp.push(label);
                                sTmp.push('</div>')

                                let d = [];
                                for (let j = 0; j < phone.length; j++) {
                                    let a = revupUtils.getFormattedPhone(phone[j].number);
                                    if (phone[j].label) {
                                        a += ' (' + phone[j].label + ')';
                                    }

                                    d.push(a)
                                }

                                sTmp.push('<div class="line dataLine">');
                                    sTmp.push(d.join('<br>'));
                                sTmp.push('</div>')
                            sTmp.push('</div>');
                        }

                        // alme mater
                        let almaMater = data.alma_maters;
                        if (almaMater.length > 0) {
                            sTmp.push('<div class="detailsGrp">');
                                let label = 'Alma Mater';
                                if (almaMater.length > 1)
                                    label += 's';
                                label += ':'

                                sTmp.push('<div class="line labelLine">')
                                    sTmp.push(label);
                                sTmp.push('</div>')

                                let d = [];
                                for (let j = 0; j < almaMater.length; j++) {
                                    let toolTip = ''
                                    if (almaMater[j].degree != '' || almaMater[j].major != '') {
                                        if (almaMater[j].degree != '') {
                                            toolTip += 'Degree - ' + almaMater[j].degree;
                                        }
                                        if (almaMater[j].major != '') {
                                            if (toolTip != '') {
                                                toolTip += ', '
                                            }

                                            toolTip += 'Major - ' + almaMater[j].major;
                                        }
                                    }

                                    if (toolTip != '') {
                                        toolTip = ' title="' + toolTip + '"';
                                    }

                                    let a = '<div class="tooltipDiv"' + toolTip + '>' + almaMater[j].name + '</div>';

                                    d.push(a)
                                }

                                sTmp.push('<div class="line dataLine">');
                                    sTmp.push(d.join(', '));
                                sTmp.push('</div>')
                            sTmp.push('</div>');
                        }

                        // organizations
                        let org = data.organizations;
                        if (org.length > 0) {
                            sTmp.push('<div class="detailsGrp">');
                                let label = 'Organizations';
                                if (org.length > 1)
                                    label += 's';
                                label += ':'

                                sTmp.push('<div class="line labelLine">')
                                    sTmp.push(label);
                                sTmp.push('</div>')

                                let d = [];
                                for (let j = 0; j < org.length; j++) {
                                    let toolTip = ''
                                    if (org[j].department != '' || org[j].title != '') {
                                        if (org[j].department != '') {
                                            toolTip += 'Department - ' + org[j].department;
                                        }
                                        if (org[j].title != '') {
                                            if (toolTip != '') {
                                                toolTip += ', '
                                            }

                                            toolTip += 'Title - ' + org[j].title;
                                        }
                                    }

                                    if (toolTip != '') {
                                        toolTip = ' title="' + toolTip + '"';
                                    }

                                    let a = '<div class="tooltipDiv"' + toolTip + '>' + org[j].name + '</div>';

                                    d.push(a)
                                }

                                sTmp.push('<div class="line dataLine">');
                                    sTmp.push(d.join(', '));
                                sTmp.push('</div>')
                            sTmp.push('</div>');
                        }
                    sTmp.push('</div>');
                sTmp.push('</div>')
            sTmp.push('</div>');
        }

        return sTmp.join('');
    } // buildDetailsContacts

    let buildContactData = (contacts, callTime = {}, notes = {}) => {
        let sTmp = [];

        sTmp.push('<div class="buildContactDataDiv">');
            sTmp.push('<div class="callLogSectionDiv">');
                sTmp.push('<div class="heading">');
                    sTmp.push('<div class="left">');
                        if (callTime.logs != undefined) {
                            sTmp.push('<div class="text">Status: In Call Time</div>');
                        }
                        else {
                            sTmp.push('<div class="text">Status: NOT In Call Time</div>');
                        }
                    sTmp.push('</div>');
                    sTmp.push('<div class="right">');
                        sTmp.push('<div class="icon icon-close contactDataBtn"></div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');

                sTmp.push('<div class="heading">');
                    sTmp.push('<div class="left">');
                        sTmp.push('<div class="text">Call Log</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');

                sTmp.push('<div class="callLogDiv">');
                    sTmp.push('<div class="callLogLst">');
                        let callLog = callTime.logs;
                        if (callLog) {
                            for (let i = 0; i < callLog.length; i++) {
                                sTmp.push('<div class="logEntry">');
                                    sTmp.push('<div class="infoLine">');
                                        // type of log event
                                        switch (callLog[i].log_type) {
                                            case 'CALL':
                                                    sTmp.push('<div class="logTypeIcon icon-called"></div>');
                                                    sTmp.push('<div class="textLabel">Called</div>');
                                                break;
                                            case 'EMAIL':
                                                sTmp.push('<div class="logTypeIcon icon-email"></div>');
                                                sTmp.push('<div class="textLabel">Emailed</div>');
                                                break;
                                            case "SMS":
                                                sTmp.push('<div class="logTypeIcon icon-texted"></div>');
                                                sTmp.push('<div class="textLabel">Texted</div>');
                                                break;
                                            default:
                                                sTmp.push('<div class="logTypeIcon icon-help"></div>');
                                                sTmp.push('<div class="textLabel">Unknown</div>');
                                                break;
                                        } // end - switch -> log_type
                                        sTmp.push('<div class="textLabel">-\></div>');

                                        // call results
                                        switch (callLog[i].call_result) {
                                            case 'VOICE_MESSAGE':
                                                sTmp.push('<div class="callResult">Left Message</div>');
                                                break;
                                            case 'NO_ANSWER':
                                                sTmp.push('<div class="callResult">No Answer</div>');
                                                break;
                                            case 'REFUSED':
                                                sTmp.push('<div class="callResult">Refushed</div>');
                                                break;
                                            case 'PLEDGED':
                                                sTmp.push('<div class="callResult">Pleged - $' + revupUtils.commify(callLog[i].pledged) + '</div>');
                                                break;
                                            case 'UNKNOWN':
                                                sTmp.push('<div class="callResult">Added Note</div>');
                                                break;
                                            case 'BAD_NUMBER':
                                                sTmp.push('<div class="callResult">Bad Number</div>');
                                                break;
                                        } // end - switch -> call_result
                                    sTmp.push('</div>');
                                    sTmp.push('<div class="dataLine">');
                                        sTmp.push(revupUtils.formatDate(callLog[i].created) + ' at ' + revupUtils.formatTime(callLog[i].created));
                                    sTmp.push('</div>')
                                sTmp.push('</div>');
                            }
                        }
                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            sTmp.push('<div class="notesSectionDiv">');
                sTmp.push('<div class="heading">Notes</div>');
                sTmp.push('<div class="subHeading">Ask</div>');
                sTmp.push('<div class="notesDiv">');
                    sTmp.push('<div class="notes notesAsk">');
                    if (notes && notes.notes1) {
                        //sTmp.push('<iframe srcdoc="<html><body>' + notes[0].notes1.replace(/\"/g, '&quot;') + '</body></html>">')
                            sTmp.push(notes.notes1);
                        //sTmp.push('</iframe>')
                    }
                    sTmp.push('</div>');
                sTmp.push('</div>');
                sTmp.push('<div class="subHeading">Results / Notes</div>');
                sTmp.push('<div class="notesDiv">');
                    sTmp.push('<div class="notes notesResults">');
                        if (notes && notes.notes2) {
                            sTmp.push(notes.notes2);
                        }
                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>')
        sTmp.push('</div>');

        return sTmp.join('');
    } // buildContactData

    let buildScreen = (hasAdminAccess) => {
        let sTmp = [];
        let showHelpStyle = '';
        let lsShowHelp = localStorage.getItem('displayContactCleanupHelp');
        if (lsShowHelp && lsShowHelp == 'false') {
            showHelpStyle = ' style="display:none;"'
        }

        sTmp.push('<div class="editContactDlg">');
            sTmp.push('<div class="helpOverlay"' + showHelpStyle + '>');
                sTmp.push('<div class="helpCloseBtn">');
                    sTmp.push('<div class="closeText">Continue</div>')
                sTmp.push('</div>');
                sTmp.push('<img src="' + contactCleanupHelpImg +'">');
            sTmp.push('</div>');

            sTmp.push('<div class="togglePages">');
                sTmp.push('<div class="editTab personalTab active">Personal Contacts </div>');
                if (hasAdminAccess) {
                    sTmp.push('<div class="editTab accountTab">Shared Account Contacts</div>');
                }
            sTmp.push('</div>');

            sTmp.push('<div class="buttonBar">')
                sTmp.push('<div class="leftSide">')
                    sTmp.push('<div class="iconBtnDiv">');
                        sTmp.push('<div class="btnDiv contactDataBtn">');
                            sTmp.push('<div class="text">Contact Data</div>');
                            sTmp.push('<div class="icon icon-edit-contact"></div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="iconBtnDiv">');
                        sTmp.push('<div class="btnDiv helpBtn">');
                            sTmp.push('<div class="text">Help</div>');
                            sTmp.push('<div class="icon icon-help"></div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
                sTmp.push('<div class="rightSide">');
                    sTmp.push('<div class="contactSearch"></div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            sTmp.push('<div class="leftSide">');
                sTmp.push('<div class="lstDiv">');
                    sTmp.push('<div class="loadingDiv lstLoadDiv" style="position: absolute; background-color: white; z-index: 2; width: 400px; height: 485px; margin: 0; padding: 0; opacity: 0.8;">');
                        sTmp.push('<div style="position: relative; top: 100px">');
                            sTmp.push('<div class="loading"></div>');
                            sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                        sTmp.push('</div>')
                    sTmp.push('</div>');

                    sTmp.push('<div class="noContactDiv" style="position: absolute; background-color: white; z-index: 2; width: 400px; height: 485px; margin: 0; padding: 0; display: none">');
                        sTmp.push('<div class="msgText">No contacts need cleanup</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="header">');
                        sTmp.push('<div class="column nameCol">Contact Label</div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="contactLstDiv" style="display:none;">');
                        sTmp.push('<div class="contactLst"></div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="contactDataDiv" style="display:none"></div>');

                sTmp.push('</div>');
            sTmp.push('</div>');

/*
            sTmp.push('<div class="middle">');
                sTmp.push('<div class="linkDiv">');
                    sTmp.push('<div class="icon icon-add"></div>');
                sTmp.push('</div>');
            sTmp.push('</div>');
*/

            sTmp.push('<div class="rightSide" style="position: absolute; left: 432px;">');
                sTmp.push('<div class="loadingDiv" style="position: absolute; background-color: white; z-index: 2; width: 570px; height: 485px; margin: 0; padding: 0; opacity: 0.8;">');
                    sTmp.push('<div style="position: relative; top: 100px">');
                        sTmp.push('<div class="loading"></div>');
                        sTmp.push('<img class="loadingImg" src="/static/img/loadingCenterIcon.svg">');
                    sTmp.push('</div>');
                sTmp.push('</div>');

                sTmp.push('<div class="detailsSection">');
                    sTmp.push('<div class="header">');
                        sTmp.push('<div class="column nameCol">Details</div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="scrollSection">');
                        sTmp.push('<div class="scrollContainer">');
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="bottomButtons">');
                        sTmp.push('<div class="toTheLeft">');
                            sTmp.push('<div class="text numContactMsg" style="margin-left:10px;">No Contacts at this time</div>');
                        sTmp.push('</div>');

                        sTmp.push('<div class="toTheRight">');
                            sTmp.push('<div class="cleanupCancelBtn">Ignore</div>');
                            sTmp.push('<button class="revupBtnDefault secondary btnSmall cleanupSaveBtn" disabled>Save</button>');
                            // sTmp.push('<button class="revupBtnDefault secondary btnSmall editSaveReleaseBtn">Save And Release</button>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>')
            sTmp.push('</div>');
        sTmp.push('</div>');

        return sTmp.join('');
    } // buildScreen

    let displayPopup = (hasAdminAccess) => {
        $dlg = $('body').revupDialogBox({
                        okBtnText:          'Close',
                        autoCloseOk:        false,
                        cancelBtnText:      '',
                        headerText:         'Contact Cleanup',
                        msg:                buildScreen(hasAdminAccess),
                        top:                5,
                        extraClass:         'manageContactMerge',
                        zIndex:             50,
                        width:              '1024px',
                        height:             '660px',
                        isDragable:         false,
                        okHandler:       function() {
                            if (!$('.cleanupSaveBtn', $dlg).prop('disabled')) {
                                $('body').revupConfirmBox({
                                    headerText: "Are you Sure?",
                                    msg: "Are you sure you want to exit Contact Cleanup? You have a contact selected to be merged that has not been saved if you exit your selected contact will not be merged.",
                                    zIndex: 55,
                                    okHandler: function() {
                                        $dlg.revupDialogBox('close')
                                    }
                                })
                            }
                            else {
                                $dlg.revupDialogBox('close')
                            }
                        }
                    });

        $contactHeader = $('.lstDiv .header', $dlg);
        $contactLst =  $('.contactLst', $dlg);
        $contactScroll = $('.contactLstDiv', $dlg);
        $contactData = $('.contactDataDiv', $dlg);

        // load the contact data
        $contactData.html(contactDataHtml);

        // toggle contact click handler
        $(".editTab").click(function () {
            if(! $(this).hasClass("active")){
                $( ".revupSearchBox .searchText" ).val("");
            }
            else {
                // if active skip
                return;
            }

            let changeTab = () => {
                // change the tabs
                $(".editTab").removeClass("active");
                $(this).addClass("active");

                // hide the data window
                $('.cleanupSaveBtn', $dlg).prop('disabled', true);
                $('.contactDataDiv', $dlg).hide();
                $('.contactLst', $dlg).show();

                // clear list
                $contactLst.removeAttr('currentPage')
                           .html('');

                // load the wait spinner
                $('.lstLoadDiv', $dlg).show();
                $('.contactLstDiv', $dlg).hide();

                // reload the contact type
                loadContacts();
            }

            // if save active ask if ok to change
            if (!$('.cleanupSaveBtn', $dlg).prop('disabled')) {
                $('body').revupConfirmBox({
                    headerText: "Are you Sure?",
                    msg: "The contact has not been saved.<br><br>Are you sure you wish to change tabs?",
                    zIndex: 55,
                    okHandler: function() {
                        changeTab();
                    }
                })
            }
            else {
                changeTab();
            }
        });



        // attach the search box
        $('.contactSearch', $dlg)
            .revupSearchBox({})
            .on('revup.searchFor', function(e) {
                let searchFor = $('.contactSearch', $dlg).revupSearchBox('getValue');
                $contactLst.removeAttr('currentPage');
                $contactLst.attr('searchFor', searchFor)

                // clear the list if first
                $contactLst.html('');

                // load the wait spinner
                $('.lstLoadDiv', $dlg).show();
                $('.contactLstDiv', $dlg).hide();

                loadContacts();
            })
            .on('revup.searchClear', function(e) {
                $contactLst.removeAttr('currentPage');
                $contactLst.removeAttr('searchFor');

                // clear the list if first
                $contactLst.html('');

                // load the wait spinner
                $('.lstLoadDiv', $dlg).show();
                $('.contactLstDiv', $dlg).hide();

                loadContacts();
            });

        // contact list - entry
        $('.contactLst', $dlg)
            .on('click', '.entry', function(e) {
                var $entry = $(this);

                // if already selected than done
                if ($entry.hasClass('selected')) {
                    return;
                }

                let doChangeSelect = () => {
                    // if in the process of saving then cannot select again or disabled
                    if (($entry.hasClass('disabled')) || ($entry.hasClass('saving'))) {
                        return;
                    }

                    // clear selected
                    $('.contactLst .entry', $dlg).removeClass('selected');

                    // select new entry
                    $entry.addClass('selected');

                    // load the selected field details
                    var cId = $entry.attr("contactId");
                    loadDetails(cId);
                }

                // see if there are
                if (!$('.cleanupSaveBtn', $dlg).prop('disabled')) {
                    $('body').revupConfirmBox({
                        headerText: "Are you Sure?",
                        msg: "The contact has not been saved before changing selection .<br><br>Press \"Ok\" change selection without saving changes",
                        zIndex: 55,
                        okHandler: function() {
                            doChangeSelect()
                        }
                    })
                }
                else {
                    doChangeSelect()
                }
            })

        $('.contactLstDiv', $dlg)
            .on('scroll', function(e) {
                let $timeToAddMore = $('.entry.timeToAddMore', $contactLst);
                if ($timeToAddMore.length > 0 && $timeToAddMore.visible()) {
                    $timeToAddMore.removeClass('timeToAddMore');
                    $('.entry.loadingMore', $contactLst).show();

                    // next
                    loadContacts();
                }
            })
            .on('mousewheel', function(e) {
                if($contactScroll.prop('scrollHeight') - $contactScroll.scrollTop() <= $contactScroll.height() && (e.originalEvent.wheelDelta < 0)) {
                    e.preventDefault()
                }
            })

        // event handler for the data details section
        $dlg
            .on('click', '.addBtn', function(e) {
                let $btn = $(this);
                let $container = $btn.closest('.contactDiv');
                let $frontBtn = $('.icon.frontIcon', $btn);

                if ($frontBtn.hasClass('icon-box-checked')) {
                    // uncheck
                    $frontBtn.removeClass('icon-box-checked')
                             .addClass('icon-box-unchecked');

                    $container.css('border-color', revupConstants.color.grayBorder)
                              .css('border-width', '1px')
                              .css('padding', '10px');

                    $btn.removeClass('selected');
                }
                else {
                    // check
                    $frontBtn.removeClass('icon-box-unchecked')
                             .addClass('icon-box-checked');


                    $container.css('border-color', revupConstants.color.greenBtnBorder)
                              .css('border-width', '3px')
                              .css('padding', '8px');

                    $btn.addClass('selected');
                }

                // enable / disable save button
                let numSelected = $('.detailsSection .scrollContainer .addBtn.selected', $dlg).length;
                let numContacts = $('.numContactMsg', $dlg).attr('numContacts');
                if (numSelected > 0) {
                    $('.detailsSection .bottomButtons .cleanupSaveBtn').prop('disabled', false);

                    // update the count
                    let msg = numSelected + ' contact';
                    if (numSelected > 1) {
                        msg += 's'
                    }
                    msg += ' of ' + numContacts + ' contacts selected'
                    $('.numContactMsg', $dlg).html(msg);
                }
                else {
                    $('.detailsSection .bottomButtons .cleanupSaveBtn').prop('disabled', true);

                    // unpdate messages
                    $('.numContactMsg', $dlg).html(numContacts + ' Contacts')
                }
            })
            .on('click', '.rollUpDownBtn', function(e) {
                let $btn = $(this);
                let $icon = $('.icon', $btn);
                let $upDownDiv = $('.detailsDiv .rollUpDownDiv', $btn.closest('.contactDiv'));

                if ($icon.hasClass('icon-triangle-dn')) {
                    // change icon
                    $icon.removeClass('icon-triangle-dn')
                         .addClass('icon-triangle-right');

                    // roll up
                    $upDownDiv.slideUp('slow', function() {});
                }
                else {
                    // change icon
                    $icon.removeClass('icon-triangle-right')
                         .addClass('icon-triangle-dn');

                    // roll down
                    $upDownDiv.slideDown('slow', function() {});
                }
            })
            .on('click', '.contactDataBtn', function () {
                let $btn = $('.buttonBar .contactDataBtn', $dlg);

                if ($btn.hasClass('selected')) {
                    $btn.removeClass('selected');

                    $contactHeader.show();
                    $contactLst.show();
                    $contactScroll.show()
                    $contactData.hide()
                }
                else {
                    $btn.addClass('selected');

                    $contactHeader.hide();
                    $contactLst.hide();
                    $contactScroll.hide()
                    $contactData.show()
                                .html(contactDataHtml);

                }
            });

        // Cancel & Save button handler
        $('.detailsSection .bottomButtons')
            .on('click', '.cleanupCancelBtn', function(e) {
                $('.detailsSection .bottomButtons .cleanupSaveBtn', $dlg).prop('disabled', true);

                // clear the selections
                $('.detailsSection .scrollContainer .contactDiv').css('border-color', revupConstants.color.grayBorder)
                                                                 .css('border-width', '1px')
                                                                 .css('padding', '10px');
                $('.detailsSection .scrollContainer .contactDiv .addBtn .icon').removeClass('icon-box-checked')
                                                                               .addClass('icon-box-unchecked');
            })
            .on('click', '.cleanupSaveBtn', function(e) {
                let $saveBtn = $(this);

                // get selected contact name
                let $selectedContact = $('.entry.selected', $contactLst);
                let selectedContactId = $selectedContact.attr('contactId');
                let selectedContactIndex = $selectedContact.index();

                // get the node to attach to contact data
                let selected = [];
                $('.detailsSection .scrollContainer .contactDiv').each(function(index, element) {
                    let $contactDiv = $(element);
                    let $addBtn = $('.addBtn', $contactDiv);
                    if ($addBtn.hasClass('selected')) {
                        selected.push($contactDiv.attr('contactId'))
                    }
                })

                // post to the Results
                let cData = {};
                cData.contacts = selected;
                // build url
                let url;
                var currentlyActive = $('.togglePages .editTab.active');
                if(currentlyActive.hasClass("accountTab")){
                    url = getSharedDetailsNeedAttention;
                }
                else if(currentlyActive.hasClass("personalTab")){
                    url = getContactDetailsNeedAttention;
                }
                url = url.replace('contactId', selectedContactId);
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: JSON.stringify(cData),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                })
                .done(function (r) {
                    // clear the section and next entry
                    let $next = $selectedContact.prev();
                    if ($next.length == 0) {
                        $next = $selectedContact.next();
                    }
                    $selectedContact.remove();

                    // see if time to load more entries
                    let $timeToAddMore = $('.entry.timeToAddMore', $contactLst);
                    if ($timeToAddMore.length > 0 && $timeToAddMore.visible()) {
                        $timeToAddMore.removeClass('timeToAddMore');
                        $('.entry.loadingMore', $contactLst).show();

                        // next
                        loadContacts();
                    }

                    // clear the save button
                    $saveBtn.prop('disabled', true);

                    if ($next.length > 0) {
                        $next.addClass('selected');

                        // load the new details
                        loadDetails($next.attr('contactId'));
                    }
                    else {
                        // list empty
                        // display the no contacts message
                        // hide the lstLoadDiv and contactLstDiv
                        $('.lstLoadDiv', $dlg).hide();
                        $('.contactLstDiv', $dlg).hide();
                        $('.rightSide .detailsSection .header .column', $dlg).hide()
                        $('.rightSide .detailsSection .bottomButtons .toTheLeft', $dlg).hide()
                        $('.rightSide .detailsSection .bottomButtons .toTheRight', $dlg).hide()
                        $('.rightSide .detailsSection .scrollContainer', $dlg).html('');

                        // display the message display
                        $('.noContactDiv', $dlg).show();
                    }
                })
                .fail(function (r) {
                    revupUtils.apiError('Manage Contact', r, 'Merge Contacts',
                                        "Unable to merge contacts to data");
                })
            })

        // close the help overlay
        $('.helpCloseBtn', $dlg).on('click', function() {
            $('.helpOverlay', $dlg).hide();

            // save the setting
            localStorage.setItem('displayContactCleanupHelp', false);
        });

        // display help
        $('.helpBtn', $dlg).on('click', function() {
            $('.helpOverlay', $dlg).show();
        })

        if (hasAdminAccess) {
            $.when($.ajax({type:'get', url: getContactNeedAttentionLst}), $.ajax({type: 'get', url: getSharedNeedAttentionLst}))
                .done(function(personalResults, sharedResult) {
                    let numShared = sharedResult[0].count;
                    let numPersonal = personalResults[0].count;
                    let activeTab = 'shared'
                    if (numPersonal > numShared) {
                        activeTab = 'personal'
                    }

                    $(".editTab", $dlg).removeClass("active");
                    $(this).addClass("active");

                    if (activeTab == 'shared') {
                        $(".editTab.accountTab", $dlg).addClass('active')
                    }
                    else {
                        $(".editTab.personalTab", $dlg).addClass('active')
                    }

                    loadContacts();
                })
                .fail(function(fResult) {
                    console.log('failed: ', fResult)
                })
            }
        else {
            loadContacts();
        }

        // after loading all the handler load the contacts with merge issues
    } // displayPopup

    return {
        display: function (hasAdminAccess)
        {
            displayPopup(hasAdminAccess);
        }
    };
} ());
