var callSheet2 = (function () {
    // Globals
    var firstTabStop =      1;
    var lastTabStop =       7;

    // template data - data passed into this closure
    var templateData =      null;

    // selectors
    var $dlg =        $([]);      // the dialog itself
    var $editor1 =    $([]);
    var $editor2 =    $([]);
    var $editorLoadingDiv = $([]);
    var $editorDiv =        $([]);
    var $callSheetLoading = $([]);
    var $msgBody =          $([]);

    // list of phone numbers & email
    var phoneLst = [];
    var phoneLstStartIndex = 0;
    let emailLst = [];
    let emailLstStartIndex = 0;

    // field values
    var bEditor1Active =      false;        // flag if the ask editor is active or not
    var bEditor2Active =  false;            // flag if the results editor is active or not

    var maxContributionsToCandDisplay = 5;

    var notesId =   -1;                     // id of the loaded notes

    var bChanged = false;                   // has anything changed

    // the feature field to and the labels to displayh
    //    the order of the list controls the order they are displayed
    var featureKeyOrder = [{key: "Federal Matches", label: "Most recent Federal Contributions", maxDisplay: 10},
                           {key: "State Matches", label: "Most recent State Contributions", maxDisplay: 5},
                           {key: "Local Matches", label: "Most recent Local Contributions", maxDisplay: 5},
                          ];

    /*
     * Header fields
     */
    function displayRightHeader(config, data)
    {
        var sTmp = [];

        // get the data
        var s = data;
        if (config.section) {
            s = data[config.section]
        }

        var f = undefined;
        if (config.field) {
            f = s[config.field];
        }
        else if (config.fieldStartWith) {
            for (var key in s) {
                if (key.startsWith(config.fieldStartWith)) {
                    var d = s[key];

                    for (var sub_key in d) {
                        if (d[sub_key].length > 0) {
                            f = d[sub_key];
                        }
                    }
                }
            }
        }

        // get the clients feature data
        if (f) {
            for(var i = 0; i < config.fields.length; i++) {
                var v = f[0][config.fields[i].key];

                if (v != undefined) {
                    switch (config.fields[i].type) {
                        case 'amountOrMsg':
                            if (typeof v == 'string') {
                                v = v.toLowerCase();
                            }

                            if ((v != '---') && (v != 'unlimited')) {
                                if (typeof v == 'string') {
                                    v = cc[config.fields[i].key];
                                }
                                else {
                                    v = '$' + revupUtils.commify(parseInt(v, 10));
                                }
                            }
                            else {
                                v = '';
                            }

                            break
                    } // end switch - confg.fields[i].type

                    // if there is a value display it
                    if (v != '') {
                        sTmp.push('<div class="amountGiven">' + v + '</div>');
                        sTmp.push('<div class="infoText">' + config.fields[i].label + '</div>');
                    }

                }
            }
        }

        return sTmp.join('');
    } // displayRightHeader

    function displayContactDetails(config, data)
    {
        var sTmp = [];

        // get the section and field
        var s = data;
        if (config.section) {
            s = data[config.section];
        }
        var f = s[config.field];
        var configData = config.data;

        sTmp.push('<div class="contactDetailsLst">');
            for (var i = 0; i < configData.length; i++) {
                for (var fieldIndex = 0; fieldIndex < f.length; fieldIndex++) {
                    if (f[fieldIndex].signal_set_title == configData[i].signalSet) {
                        if (configData[i].type == 'attr') {
                            var attrData = f[fieldIndex][configData[i].subField][0];
                            var attrConfig = configData[i].attr;

                            // display the attributes
                            for (var ii = 0; ii < attrConfig.length; ii++) {
                                if (attrData[attrConfig[ii].key]) {
                                    var v = attrData[attrConfig[ii].key];

                                    if (v) {
                                        sTmp.push('<div class="entry">');
                                            sTmp.push('<div class="label">' + attrConfig[ii].label + ':</div>');
                                            sTmp.push('<div class="value">' + v + '</div>');
                                        sTmp.push('</div>');
                                    }

                                }
                            }
                        }
                        else if (configData[i].type == 'activity') {
                            var attrData = f[fieldIndex][configData[i].subField];
                            if (configData[i].bDeDup) {
                                attrData = revupUtils.dedup(attrData, function(a) {
                                    return a.activity
                                });
                            }
                            for (var ii = 0; ii < attrData.length; ii++) {
                                var v = attrData[ii][configData[i].key];

                                if (v) {
                                    sTmp.push('<div class="entry activity">');
                                        var label = "";
                                        if (ii == 0) {
                                            label = configData[i].label + ':';
                                        }

                                        if (ii < (attrData.length - 1)) {
                                            v += ',';
                                        }

                                        sTmp.push('<div class="label">' + label + '</div>');
                                        sTmp.push('<div class="value">' + v + '</div>');
                                    sTmp.push('</div>');
                                }
                            }
                        }
                    }
                }
            }
        sTmp.push('</div>');

        return sTmp.join('');
    } // displayContactDetails

    function headerFields(config, data)
    {
        var sTmp = [];

        var contact = data.contact;

        var firstName = getAnalysisContactValue(config.contact.firstName, data);
        var lastName = getAnalysisContactValue(config.contact.lastName, data);
        /*
        sTmp.push('<div class="upsideDownHeader">');
            sTmp.push('<div class="upsideDown">');
                sTmp.push('<div class="rightSide">');
                    sTmp.push('<div class="firstName">' + firstName + '</div>');
                    sTmp.push('<div class="lastName">' + lastName + '</div>');
                    sTmp.push('<div class="phoneNum"></div>');
                sTmp.push('</div>');
            sTmp.push('</div>');
            sTmp.push('<hr class="sepBar">');
        sTmp.push('</div>');
        */

        sTmp.push('<div class="contentHeader">');
            sTmp.push('<div class="leftSide">');
                sTmp.push('<div class="personalInfo">');
                    sTmp.push('<div class="firstLine">');
                        if (contact.image_urls && contact.image_urls.length > 0) {
                            // give precedence to LinkedIn
                            let imgUrl = contact.image_urls[0].url;
                            for (let i = 0; i < contact.image_urls.length; i++) {
                                if (contact.image_urls[i].source == 'LinkedIn') {
                                    imgUrl = contact.image_urls[i].url
                                }
                            }
                            sTmp.push('<div class="imageDiv">');
                                sTmp.push('<img class="img" src="' + imgUrl + '">');
                            sTmp.push('</div>');
                        }

                        sTmp.push('<div class="name">');
                            sTmp.push('<div class="firstName">' + contact.first_name + '</div>');
                            sTmp.push('<div class="lastName">' + contact.last_name + '</div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');


                    phoneLst = [];
                    sTmp.push('<div class="secondLine">');
                        sTmp.push('<div class="leftSide">');
                            if  (config.contact && config.contact.phone) {
                                // get the phone list
                                var val = getAnalysisContactValue(config.contact.phone, data);

                                phoneLstStartIndex = 0;
                                if (config.contact.phone.section == 'contact') {
                                    for (var i = 0; i < val.length; i++) {

                                        var pn = revupUtils.getFormattedPhone(val[i].number);
                                        var o = new Object();
                                        o.value = pn + '::' + val[i].id;
                                        if (val[i].label) {
                                            o.displayValue = pn + ' (' + val[i].label + ')';
                                        }
                                        else {
                                            o.displayValue = pn;
                                        }
                                        phoneLst.push(o);

                                        // set to primary phone number
                                        if (templateData.primaryPhone && val[i].id == templateData.primaryPhone) {
                                            phoneLstStartIndex = i
                                        }
                                    }
                                }
                                else if (config.contact.phone.section == 'features') {
                                    if (val) {
                                        for (var i = 0; i < val.length; i++) {
                                            if (val[i]) {
                                                var pn = revupUtils.getFormattedPhone(val[i]);
                                                var o = new Object();
                                                o.value =  pn + '::' + val[i].id;
                                                if (config.contact.phone.keyLabel) {
                                                    o.displayValue = pn + ' (' + config.contact.phone.keyLabel[i] + ')';
                                                }
                                                else {
                                                    o.displayValue = pn
                                                }

                                                phoneLst.push(o);

                                                // set to primary phone number
                                                if (templateData.primaryPhone && val[i].id == templateData.primaryPhone) {
                                                    phoneLstStartIndex = i
                                                }
                                            }
                                        }
                                    }
                                }

                                var pNum = '<div style="font-style:italic">No Phone Number</div>';
                                if (phoneLst.length == 1) {
                                    pNum = phoneLst[0].displayValue; //phoneLst[phoneLstStartIndex].value.split('::');
                                    // pNum = pNum[0];
                                }
                                sTmp.push('<div class="dataValue">');
                                    if (phoneLst.length > 1) {
                                        sTmp.push('<div class="dataValueLine">');
                                            sTmp.push('<div class="dvLabelSmall"></div>');
                                            sTmp.push('<div class="dvValueSmall">Choose Primary Number</div>');
                                        sTmp.push('</div>');
                                    }
                                    sTmp.push('<div class="dataValueLine">');
                                        sTmp.push('<div class="dvLabel">' + config.contact.phone.label + ':</div>');
                                        if (phoneLst.length <= 1) {
                                            sTmp.push('<div class="dvValue">' + pNum + '</div>');
                                        }
                                        else {
                                            sTmp.push('<div class="phoneLst" tabIndex="2"></div>');
                                        }
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            }

                            if (config.contact.employer) {
                                // get the value
                                var employer = "";
                                var val = getAnalysisContactValue(config.contact.employer, data);
                                if (val && (val.length > 0) && val[0].name) {
                                    employer = val[0].name;
                                }

                                sTmp.push('<div class="dataValue">');
                                    sTmp.push('<div class="dvLabel">' + config.contact.employer.label + ':</div>');
                                    sTmp.push('<input class="employer" tabIndex="5" type="text" readonly  value="' + employer + '">')
                                sTmp.push('</div>');
                            }

                            if (config.contact.occupation) {
                                // get the value
                                var title = "";
                                var val = getAnalysisContactValue(config.contact.occupation, data);
                                if (val && (val.length > 0) && val[0].title) {
                                    title = val[0].title;
                                }

                                sTmp.push('<div class="dataValue">');
                                    sTmp.push('<div class="dvLabel">' + config.contact.occupation.label + ':</div>');
                                    sTmp.push('<input class="occupation" tabIndex="6" type="text" readonly xplaceholder="Occupation" value="' + title + '">')
                                sTmp.push('</div>');
                            }
                        sTmp.push('</div>');

                        emailLst = [];
                        sTmp.push('<div class="rightSide">')
                            // email address
                            if  (config.contact && config.contact.email) {
                                var val = getAnalysisContactValue(config.contact.email, data);

                                //var emailAddress = "";
                                var tabIndex = 3;
                                if ((contact.email_addresses != undefined) && (contact.email_addresses.length > 0) && (contact.email_addresses[0].address != undefined)) {
                                    if (config.contact.email.section == 'contact') {
                                        if (val.length == 0) {
                                            emailLst.push({value: '', displayValue: ''});
                                        }
                                        for (var i = 0; i < val.length; i++) {
                                            var o = new Object();
                                            o.value = val[i].address + '::' + val[i].id;
                                            if (val[i].label) {
                                                o.displayValue = val[i].address + ' (' + val[i].label + ')';
                                            }
                                            else {
                                                o.displayValue = val[i].address;
                                            }
                                            emailLst.push(o);

                                            // set to primary phone number
                                            if (templateData.primaryEmail && val[i].id == templateData.primaryEmail) {
                                                emailLstStartIndex = i
                                            }
                                        }
                                        /*
                                        if (val.length == 0) {
                                            emailLst.push('<div class="italic">No Phone Number</div>');
                                        }
                                        for (var i = 0; i < val.length; i++) {
                                            emailLst.push(val[i].address);
                                        }
                                        */
                                    }
                                    else if (config.contact.email.section == 'features') {
                                        if (val) {
                                            emailLst.push(val)
                                        }
                                        else {
                                            emailLst.push('');
                                        }
                                    }
                                }

                                sTmp.push('<div class="dataValue">');
                                    if (emailLst.length > 1) {
                                        sTmp.push('<div class="dataValueLine">');
                                            sTmp.push('<div class="dvLabelSmall"></div>');
                                            sTmp.push('<div class="dvValueSmall">Choose Primary Email</div>');
                                        sTmp.push('</div>');
                                    }
                                    sTmp.push('<div class="dataValueLine">');
                                        sTmp.push('<div class="dvLabel">' + config.contact.email.label + ':</div>');
                                        if (emailLst.length == 0) {
                                            sTmp.push('<div style="font-style:italic">No Email Address</div>');
                                        }
                                        else if (emailLst.length == 1) {
                                            sTmp.push('<div class="dvValue">' + emailLst[0].displayValue + '</div>');
                                        }
                                        else {
                                            sTmp.push('<div class="emailLst" tabIndex="4"></div>');
                                        }
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            }

                            // address
                            if (config.contact && config.contact.address) {
                                try {
                                    let conf = config.contact.address;
                                    let sect = data[conf.section];
                                    let field = sect[conf.field];
                                    let fAddr = field[0][conf.fieldKeyAddr];
                                    let fCity = field[0][conf.fieldKeyCity];
                                    let fState = field[0][conf.fieldKeyState];
                                    let fZip = field[0][conf.fieldKeyZip];
                                    let fCountry = field[0][conf.fieldKeyCountry];

                                    let addr = [];
                                    addr.push('<div class="addrSection">');
                                        if (fAddr != '') {
                                            addr.push('<div class="addrLine">')
                                                addr.push('<div class="addrEntry">' + fAddr + '</div>');
                                            addr.push('</div>');
                                        }

                                        if (fCity != '' || fState != '' || fZip != '') {
                                            addr.push('<div class="addrLine">');
                                                if (fCity != '') {
                                                    if (fState != '') {
                                                        fCity += ',';
                                                    }
                                                    addr.push('<div class="addrEntry">' + fCity + '</div>');
                                                }

                                                if (fState != '') {
                                                    addr.push('<div class="addrEntry">' + fState + '</div>');
                                                }

                                                if (fZip != '') {
                                                    addr.push('<div class="addrEntry">' + fZip + '</div>')
                                                }
                                            addr.push('</div>');
                                        }

                                        if (fCountry != '' && fCountry.toLowerCase() != 'us' && fCountry.toLowerCase() != 'usa') {
                                            addr.push('<div class="addrLine">');
                                                addr.push('<div class="addrEntry">' + fCountry + '</div>');
                                            addr.push('</div>')
                                        }
                                    addr.push('</div>');

                                    sTmp.push('<div class="dataValue">');
                                        sTmp.push('<div class="dvLabel">' + conf.label + ':</div>');
                                        sTmp.push('<div class="dvValue">' + addr.join('') + '</div>');
                                    sTmp.push('</div>');
                                }
                                catch (e) {

                                }
                            }
                        sTmp.push('</div>')
                    sTmp.push('</div>');
                sTmp.push('</div>');

                if (config.displayTip) {
                    sTmp.push('<div class="noteMsg hideWhenPrinting">');
                        sTmp.push('<div class="noteIcon">!</div>');
                        sTmp.push('<div class="noteHeader">Tip</div>');
                        sTmp.push('<div class="noteText">Additional data, such as email, phone numbers, and other notes can be entered and saved in the Ask field below.</div>')
                    sTmp.push('</div>')
                }
            sTmp.push('</div>');

            var extraClass = " amountGivenDiv";
            var bDisplay = true;
            var bContactDetails = false;
            var bHeaderRight = false;
            if (config.contactDetails) {
                extraClass = " contactDetailsDiv"
                bDisplay = config.contactDetails.display;
                bContactDetails = true;
            }
            else if (config.headerRight) {
                extraClass = " headerRightDiv";
                bDisplay = config.headerRight;
                bHeaderRight = true;
            }

            if (bDisplay) {
                sTmp.push('<div class="rightSide ' + extraClass + '">');
                    if (bContactDetails) {
                        sTmp.push(displayContactDetails(config.contactDetails, data));
                    }
                    else if (bHeaderRight) {
                        sTmp.push(displayRightHeader(config.headerRight, data));
                    }
                sTmp.push('</div>');
            }
        sTmp.push('</div>');

        return sTmp.join('');
    } // headerFields

    /*
     * Editor Fields
     */
    function editField(config)
    {
        // get the current stored ask data
        var editor1Data = ''; //localStorage.getItem('callSheetAskEditor');
        var editor2Data = ''; // localStorage.getItem('callSheetResultsEditor');

        var sTmp = [];

        // load div
        sTmp.push('<div class="editorLoadingDiv">');
            sTmp.push('<div class="loadingDiv">');
                sTmp.push('<div class="loading"></div>');
                sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
            sTmp.push('</div>');
        sTmp.push('</div>');

        sTmp.push('<div class="editorDiv" notesId="1">');
            for (var i = 0; i < config.length; i++) {
                var className = "editor" + (i + 1) + "Div";
                // ask editor
                sTmp.push('<div class="' + className + '">');
                    sTmp.push('<div class="labelBtnDiv">');
                        sTmp.push('<div class="leftSide">');
                            sTmp.push('<div class="aeLabel">' + config[i].header + '</div>');
                        sTmp.push('</div>');
                        if (config[i].enableFormating) {
                            sTmp.push('<div class="rightSide">');
                                sTmp.push('<div class="formatBtns">');
                                    sTmp.push('<button class="revupBtnDefault btnIcon editFormatBtn editBoldBtn">');
                                        sTmp.push('<div class="icon icon-bold"></div>')
                                    sTmp.push('</button>');
                                    sTmp.push('<button class="revupBtnDefault btnIcon editFormatBtn italicBoldBtn">');
                                        sTmp.push('<div class="icon icon-italic"></div>')
                                    sTmp.push('</button>');
                                    sTmp.push('<button class="revupBtnDefault btnIcon editFormatBtn underlineBoldBtn">');
                                        sTmp.push('<div class="icon icon-underline"></div>')
                                    sTmp.push('</button>');

                                    sTmp.push('<button class="revupBtnDefault btnIcon leadSpacer editFormatBtn indentBtn">')
                                        sTmp.push('<div class="icon icon-indent-more"></div>')
                                    sTmp.push('</button>');
                                    sTmp.push('<button class="revupBtnDefault btnIcon editFormatBtn outdentBtn">');
                                        sTmp.push('<div class="icon icon-indent-less"></div>')
                                    sTmp.push('</button>');

                                    sTmp.push('<button class="revupBtnDefault btnIcon leadSpacer editFormatBtn orderLstBtn">')
                                        sTmp.push('<div class="icon icon-numbered-list"></div>')
                                    sTmp.push('</button>');
                                    sTmp.push('<button class="revupBtnDefault btnIcon editFormatBtn unorderLstBtn">')
                                        sTmp.push('<div class="icon icon-bulleted-list"></div>')
                                    sTmp.push('</button>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');
                        }
                    sTmp.push('</div>');
                    var style = "";
                    if (config[i].height) {
                        style = ' style="height:' + config[i].height + ';"';
                    }
                    sTmp.push('<div class="editor' + (i + 1) + '" tabIndex="' + (7 + i) + '"' + style + ' contenteditable="true">');
                        sTmp.push(editor1Data);
                    sTmp.push('</div>')
                sTmp.push('</div>');
            }
        sTmp.push('</div>');

        return sTmp.join('');
    } // editField

    function contributionGrp(config, grpData)
    {
        var sTmp = [];
        var totalDisplayed = 0;

        function displayEntry(index, config, grpData)
        {
            // see if the max number of entries hit
            if ((config.maxDisplay != undefined) && (index >= config.maxDisplay)) {
                return;
            }

            var name = grpData[config.subField][index][config.disaplyField];
            var amount = grpData[config.subField][index].amount;
            var date = grpData[config.subField][index].date;

            // convert data
            if (name == undefined) {
                name = '---';
            }
            else {
                // handle names and commitees
                var parts = name.split(' ');
                var fname = name;
                var fname = fname.split(',');
                for (var ii = 0; ii < fname.length; ii++) {
                    fname[ii] = $.trim(fname[ii]);
                }

                // name the name first, last
                if (parts.length < 5) {
                    fname.reverse();

                    var name = revupUtils.toProperCaseName(fname[0]);
                    if (fname.length > 1) {
                        name += " " + revupUtils.toProperCaseName(fname[1]);
                    }
                }
                else {
                    var name = revupUtils.toProperCaseName(fname[0]);
                    if (fname.length > 1) {
                        for (var ii = 1; ii < fname.length; ii++) {
                            name += ", " + revupUtils.toProperCaseName(fname[ii]);
                        }
                    }
                }
            }

            if (config.type == 'givingNonprofit') {
                sTmp.push('<div class="entry">');
                    sTmp.push('<div class="name">' + name + '</div>');
                    sTmp.push('<div class="noDate">' + amount + '</div>');
                sTmp.push('</div>');
            }
            else {
                if (amount != undefined) {
                    amount = parseInt(amount, 10);
                    totalDisplayed += amount;
                    amount = "$" + revupUtils.commify(amount);
                }

                if (date != undefined) {
                    date = revupUtils.formatDate(date);
                }

                sTmp.push('<div class="entry">');
                    sTmp.push('<div class="name">' + name + '</div>');
                    sTmp.push('<div class="amount">' + amount + '</div>');
                    sTmp.push('<div class="date">' + date + '</div>');
                sTmp.push('</div>');
            }
        } // displayEntry

        function displayTotal(totalDisplayed, config, title, bDisplaySep)
        {
            // set defauults
            if (!title) {
                title = "Total"
            }

            if (bDisplaySep == undefined) {
                bDisplaySep = true
            }

            // display the total
            if (config.bDisplayTotal) {
                if (bDisplaySep) {
                    sTmp.push('<div class="entry">');
                        sTmp.push('<hr class="sepBar">');
                    sTmp.push('</div>');
                }

                //var total = grpData.giving;
                var total = totalDisplayed;
                total = "$" + revupUtils.commify(total);
                sTmp.push('<div class="entry">');
                    sTmp.push('<div class="totalLabel">' + title + '</div>');
                    sTmp.push('<div class="totalAmount">' + total + '</div>');
                sTmp.push('</div>')
            }
        } // displayTotal

        if ((grpData != undefined) && (grpData[config.subField] != undefined) && (grpData[config.subField].length > 0)) {
            sTmp.push('<div class="contributionCatDiv">');
                sTmp.push('<div class="contributionLabel">' + config.label + '</div>');
                sTmp.push('<div class="contributionCatDetailDiv">');

                    if (config.type == 'giving' || config.type == 'givingNonprofit') {
                        // display the matches
                        let len = Math.min(grpData[config.subField].length, config.maxDisplay);
                        for (var i = 0; i < len; i++) {
                            displayEntry(i, config, grpData);
                        }

                        // display the total
                        displayTotal(totalDisplayed, config);
                    }
                    else if (config.type == 'givingOrgBy') {
                        var grandTotal = 0;
                        var data = grpData[config.subField];
                        if (config.orgBy) {
                            data.sort(function(a, b) {
                                if (a[config.orgBy] > b[config.orgBy]) {
                                    return 1;
                                }
                                else if (a[config.orgBy] < b[config.orgBy]) {
                                    return -1;
                                }
                                else {
                                    if (config.thenBy) {
                                        var da = Date.parse(a[config.thenBy]);
                                        var db = Date.parse(b[config.thenBy]);
                                        if (da < db) {
                                            return 1;
                                        }
                                        else if (da > db) {
                                            return -1;
                                        }
                                    }

                                    return 0;
                                }
                            })
                        }

                        // walk the
                        var index = 0;
                        for (var i = 0; i < data.length; i++) {
                            // find where orgBy changes value
                            var cVal = data[index][config.orgBy];
                            var stopAt = index;
                            for (var j = index; j < data.length; j++) {
                                stopAt = j;

                                if (cVal != data[j][config.orgBy]) {
                                    break;
                                }
                            }

                            // no progress then the end of the list
                            if (index >= stopAt) {
                                break;
                            }

                            // display the data
                            totalDisplayed = 0;
                            var numDisplayed = 0;
                            sTmp.push ('<div class="grouping" style="margin-bottom:10px;">');
                                // display the matches
                                for (var ii = index; ii < stopAt; ii++) {
                                    // see if the max to display hit
                                    if ((config.maxOrgByDisplay) && (numDisplayed >= config.maxOrgByDisplay)) {
                                        break;
                                    }

                                    displayEntry(ii, config, grpData);
                                    numDisplayed++;
                                }

                                // display the total
                                displayTotal(totalDisplayed, config, 'Sub-total');
                            sTmp.push('</div>');

                            // build the grand total
                            grandTotal += totalDisplayed;

                            // next starting point
                            index = stopAt;
                        }

                        // display the grand total
                        displayTotal(grandTotal, config, 'Total', false);
                    }
                sTmp.push('</div>');
            sTmp.push('</div>');
        }

        return sTmp.join('');
    } // contributionGrp

    function candidateContribution(config, data, section)
    {
        function contributionToCandidate(candidateData, keyData)
        {
            var sTmp = [];
            var grandTotal = 0;
            sTmp.push('<div class="contributionCatDiv">');
                sTmp.push('<div class="contributionLabel topLevel">Contributions</div>');
                if (candidateData) {
                    sTmp.push('<div class="contributionLabel">' + config.candidateTitle + '</div>');
                    sTmp.push('<div class="contributionCatDetailDiv">');
                        var totalDisplayed = 0;
                        for (var i = 0; i < candidateData.length; i++) {
                            // see if the max number of entries hit
                            if ((maxContributionsToCandDisplay != undefined) && (i >= maxContributionsToCandDisplay)) {
                                break;
                            }

                            sTmp.push('<div class="entry">');
                                var amount = '---';
                                if (candidateData[i].amount != undefined) {
                                    amount = parseInt(candidateData[i].amount, 10);
                                    grandTotal += amount;
                                    totalDisplayed += amount;
                                    amount = "$" + revupUtils.commify(amount);
                                }

                                var date = '---';
                                if (candidateData[i].date != undefined) {
                                    date = revupUtils.formatDate(candidateData[i].date);
                                }

                                if (i == 0) {
                                    sTmp.push('<div class="name">' + templateData.candidateName + '</div>');
                                }
                                else {
                                    sTmp.push('<div class="name">&nbsp;</div>');
                                }
                                //sTmp.push('<div class="labelSpace"></div>')
                                sTmp.push('<div class="amount">' + amount + '</div>');
                                sTmp.push('<div class="date">' + date + '</div>');
                            sTmp.push('</div>');
                        }

                        if (config.bDisplaySeparator) {
                            sTmp.push('<div class="entry">');
                                sTmp.push('<hr class="sepBar">');
                            sTmp.push('</div>');
                        }

                        //var total = grpData.giving;
                        if (config.bDisplayTotal) {
                            var total = totalDisplayed;
                            total = "$" + revupUtils.commify(total);
                            sTmp.push('<div class="entry">');
                                sTmp.push('<div class="totalLabel">Sub-total</div>');
                                sTmp.push('<div class="totalAmount">' + total + '</div>');
                            sTmp.push('</div>');
                        }
                    sTmp.push('</div>')
                }

                if (keyData) {
                    var sectionCount = candidateData ? 1 : 0

                    // build a array of ally and non ally
                    let allyLst = [];
                    let nonAllyLst = [];
                    for(let keyName in keyData) {
                        if (keyData[keyName].is_ally) {
                            allyLst[keyName] = keyData[keyName].match;
                        }
                        else {
                            nonAllyLst[keyName] = keyData[keyName].match;
                        }
                    }

                    for (let xx = 0; xx < 2; xx++) {
                        let d;
                        let extraClass = "";
                        let bDisplayHeader = true;
                        if (xx == 0) {
                            d = allyLst;
                            extraClass = ' allyClass'
                        }
                        else {
                            d = nonAllyLst;
                        }
                        for(var keyName in d /*keyData*/) {
                            var data = /*keyData*/d[keyName]/*.match*/;
                            var style = "";
                            sectionCount += 1;
                            if (bDisplayHeader) {
                                if (xx == 0)
                                    sTmp.push('<div class="contributionLabel" style="margin-top:10px">' + config.allyTitle + '</div>');
                                else
                                    sTmp.push('<div class="contributionLabel" style="margin-top:10px">' + config.nonAllyTitle + '</div>');

                                bDisplayHeader = false;
                            }
                            sTmp.push('<div class="contributionCatDetailDiv' + extraClass + '"' + style + '>');
                                var totalDisplayed = 0;
                                for (var i = 0; i < data.length; i++) {
                                    // see if the max number of entries hit
                                    if ((maxContributionsToCandDisplay != undefined) && (i >= maxContributionsToCandDisplay)) {
                                        break;
                                    }

                                    sTmp.push('<div class="entry">');
                                        var amount = '---';
                                        if (data[i].amount != undefined) {
                                            amount = parseInt(data[i].amount, 10);
                                            grandTotal += amount;
                                            totalDisplayed += amount;
                                            amount = "$" + revupUtils.commify(amount);
                                        }

                                        var date = '---';
                                        if (data[i].date != undefined) {
                                            date = revupUtils.formatDate(data[i].date);
                                        }

                                        if (i == 0) {
                                            sTmp.push('<div class="name">' + keyName + '</div>');
                                        }
                                        else {
                                            sTmp.push('<div class="name">&nbsp;</div>');
                                        }
                                        //sTmp.push('<div class="labelSpace"></div>')
                                        sTmp.push('<div class="amount">' + amount + '</div>');
                                        sTmp.push('<div class="date">' + date + '</div>');
                                    sTmp.push('</div>');
                                }

                                if (config.bDisplaySeparator) {
                                    sTmp.push('<div class="entry">');
                                        sTmp.push('<hr class="sepBar">');
                                    sTmp.push('</div>');
                                }

                                //var total = grpData.giving;
                                if (config.bDisplayTotal) {
                                    var total = totalDisplayed;
                                    total = "$" + revupUtils.commify(total);
                                    sTmp.push('<div class="entry">');
                                        sTmp.push('<div class="totalLabel">Sub-total</div>');
                                        sTmp.push('<div class="totalAmount">' + total + '</div>');
                                    sTmp.push('</div>')
                                }
                            sTmp.push('</div>')
                        }
                    }
                }

                if (config.bDisplayGrandTotal) {
                    grandTotal = "$" + revupUtils.commify(grandTotal);
                    sTmp.push('<div class="contributionCatDetailDiv" style="margin-top:25px">');
                        sTmp.push('<div class="entry">');
                            sTmp.push('<div class="totalLabel">Total</div>');
                            sTmp.push('<div class="totalAmount">' + grandTotal + '</div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                }
            sTmp.push('</div>');

            return sTmp.join('');
        } // contributionToCandidate

        // see if there are contributions to the candidate, if so display
        var cc = undefined;
        var kc = undefined;
        var cd = undefined;
        var kd = undefined;

        // get the clients feature data
        if (data.result) {
            for(var key in data.result.features) {
                if (key.startsWith(config.fieldStartWithCand)) {
                    cd = data.result.features[key];
                    for (var sub_key in cd) {
                        if (cd[sub_key].length > 0) {
                            cc = cd[sub_key][0];
                        }
                    }
                }
                if (key.startsWith(config.fieldStartWithOther)) {
                    kd = data.result.features[key];
                    for (var sub_key in kd) {
                        if (kd[sub_key].length > 0) {
                            kc = kd[sub_key][0];
                        }
                    }
                }
            }
        }

        // get the data for client contributions
        if (cc && cc.match && (cc.match.length > 0)) {
            cc = cc.match;
        }
        else {
            cc = undefined;
        }

        if (cc || kc) {
            return contributionToCandidate(cc, kc);
        }

        return '';
    } // candidateContribution

    function givingTotal(config, field)
    {
        var sTmp = [];

        // get the value
        var v = field;
        if (config.fieldStartWith) {
            v = field[0][config.subField]
        }

        // build the cycle string
        var title = config.title;
        if (title.indexOf('%cycleYear%') >= 0) {
            var givingYearObj = templateData.partitionObj;
            if ((givingYearObj != undefined) && (givingYearObj.displayValue != undefined)) {
                title = title.replace('%cycleYear%', givingYearObj.displayValue);
            }
            else {
                title = title.replace('%cycleYear%', '2000');
            }
        }

        // see if title 2 should be used
        if (config.bUseTitle2 && config.title2) {
            title = config.title2;
        }

        // convert the value into amount
        v = "$" + revupUtils.commify(v);

        sTmp.push('<div class="contributionAmountDiv">');
            sTmp.push('<div class="contributionLabel">' + title + '</div>');
            sTmp.push('<div class="contributionAmount">' + v + '</div>');
        sTmp.push('</div>');

        return sTmp.join('');
    } // givingTotal

    function givingFromSrc(config, data)
    {

        // source Source
        var srcS = data;
        if (config.srcSection) {
            srcS = data[config.srcSection];
        }

        // get the source field
        var f = undefined;
        if (config.srcField) {
            f = srcS[config.srcField];
        }
        else if (config.srcFieldStartWith) {
            for (var key in srcS) {
                if (key.startsWith(config.srcFieldStartWith)) {
                    var d = srcS[key];

                    for (var sub_key in d) {
                        if (d[sub_key].length > 0) {
                            f = d[sub_key];
                        }
                    }
                }
            }
        }
        if (!f) {
            return '';
        }

        // find the signal set in the field
        var subF = undefined;
        for (var i = 0; i < f.length; i++) {
            if (f[i].signal_set_title == config.srcSignalSet) {
                subF = f[i][config.srcSubField][0];
            }
        }
        if (!subF) {
            return '';
        }

        var srcVal = undefined;
        if (subF) {
            srcVal = subF[config.srcKey]
        }

        if (!srcVal) {
            return '';
        }

        // get the amount for the srcVal from the destination
        dstS = data;
        if (config.dstSection) {
            dstS = data[config.dstSection];
        }

        // get the source field
        f = undefined;
        if (config.dstField) {
            f = dstS[config.dstField];
        }
        else if (config.dstFieldStartWith) {
            for (var key in dstS) {
                if (key.startsWith(config.dstFieldStartWith)) {
                    var d = dstS[key];

                    for (var sub_key in d) {
                        if (d[sub_key].length > 0) {
                            f = d[sub_key];
                        }
                    }
                }
            }
        }
        if (!f) {
            return '';
        }

        // get the sub field
        var dstSubField = undefined;
        if (config.dstSubField) {
            dstSubField = f[0][config.dstSubField];
        }
        if (!dstSubField) {
            return '';
        }

        // compute the giving for match key
        var total = 0;
        for (var i = 0; i < dstSubField.length; i++) {
            var matchVal = dstSubField[i][config.dstSrcMatchKey];
            if (matchVal == srcVal) {
                if (dstSubField[i][config.dstKey]) {
                    total += dstSubField[i][config.dstKey];
                }
            }
        }

    // build the cycle string
    var title = config.title;
    if (title.indexOf('%srcValue%') >= 0) {
        title = title.replace('%srcValue%', srcVal);
    }

    // convert the value into amount
    total = "$" + revupUtils.commify(total);

    var sTmp = [];
    sTmp.push('<div class="contributionAmountDiv">');
        sTmp.push('<div class="contributionLabel">' + title + '</div>');
        sTmp.push('<div class="contributionAmount">' + total + '</div>');
    sTmp.push('</div>');

    return sTmp.join('');

    } // givingFromSrc

    /*
     * Contribution Fields
     */
    function contributionColumn(configLst, data)
    {
        var sTmp = [];
        for (var c = 0; c < configLst.length; c++) {
            var config = configLst[c];

            // get the section
            var s = data;
            if (config.section) {
                s = data[config.section];
            }
            if (!s && data.result && data.result[config.section]) {
                s = data.result[config.section];
            }

            // get the fields
            var f = undefined;
            var cc = undefined;
            if (config.field && s && s[config.field]) {
                f = s[config.field];
            }
            else if (config.fieldStartWith) {
                for (var key in s) {
                    if (key.startsWith(config.fieldStartWith)) {
                        var d = s[key];

                        for (var sub_key in d) {
                            if (d[sub_key].length > 0) {
                                f = d[sub_key];
                            }
                        }
                    }
                }
            }
            else if ((config.type == 'candidateContribution') || (config.type == 'givingFromSrc')) {
                // for this type the fields are found in the helper function
                f = true;
            }

            // display based on the type
            if (f) {
                switch (config.type) {
                    case 'giving':
                    case 'givingNonprofit':
                        sTmp.push(contributionGrp(config, f[0]));
                        break;
                    case 'givingOrgBy':
                        sTmp.push(contributionGrp(config, f[0]));
                        break;
                    case 'candidateContribution':
                        sTmp.push(candidateContribution(config, data, s));
                        break;
                    case 'givingTotal':
                        sTmp.push(givingTotal(config, f));
                        break;
                    case 'givingFromSrc':
                        sTmp.push(givingFromSrc(config, data));
                        break;
                } // end switch - config.type
            }
        }

        return sTmp.join('');
    } // contributionColumn

    function contributionFields(config, data, givingYearObj, candidateName)
    {
        var sTmp = [];

        // build the cycle string
        var cycleStr = "Total Contributions Since ";
        if ((givingYearObj != undefined) && (givingYearObj.displayValue != undefined)) {
            cycleStr += givingYearObj.displayValue;
        }
        else {
            cycleStr += "2000";
        }
        cycleStr += " Election Cycle:";

        sTmp.push('<div class="contributionsDiv">');
            sTmp.push('<div class="leftSide">');
                sTmp.push(contributionColumn(config.contribution.leftSide, data));
            sTmp.push('</div>');

            sTmp.push('<div class="rightSide">');
                sTmp.push(contributionColumn(config.contribution.rightSide, data));
            sTmp.push('</div>');
        sTmp.push('</div>');

        return sTmp.join('');
    }; // contributionFields

    function loadCallSheet(tData)
    {
        var msg = [];

        // Add the header fields
        msg.push(headerFields(tData.config, tData.data));

        // notes and contrib sections
        msg.push('<div class="notesContribDiv">');
            // Add the editor
            msg.push('<div class="notesSectionDiv">');
                msg.push(editField(tData.config.editorSection.editors));
            msg.push('</div>');

            // Add the contributions/contribution section
            msg.push('<div class="contribSectionDiv">');
                msg.push(contributionFields(tData.config, tData.data, tData.partitionObj, tData.candidateName));
            msg.push('</div>');
        msg.push('</div>');

        var $callSheet = $('.callSheet2', $dlg);
        $callSheet.html(msg.join(''));

        // editor selectors
        $editorLoadingDiv = $('.editorLoadingDiv', $callSheet);
        $editorDiv = $('.editorDiv', $callSheet);
        $callSheetLoading = $('.callSheet2Loading', $dlg);
        $msgBody = $('.msgBody', $dlg);

        // add tooltip to long names
        $('.contributionsDiv .name', $callSheet).tooltipOnOverflow();

        // load the phone number drop down
        let tabIndex = 3;
        //let $phoneNumEdit = $('.personalInfo .dataValue .phoneNum');
        //let $phoneNumUpsidedown = $('.upsideDownHeader .phoneNum');
        $('.phoneLst', $dlg).revupDropDownField({
            value:              phoneLst, //pLst,
            valueStartIndex:    phoneLstStartIndex,
            sortList:           false,
            changeFunc:         function(i, v) {
                v = v.split('::');
                //$phoneNumEdit.val(v[0]);
                //$phoneNumUpsidedown.text(v[0]);

                // notify the caller of a change in the primary phone number
                if (templateData.phoneUpdateFunc && v.length == 2) {
                    templateData.phoneUpdateFunc(v[1]);
                }

                $dlg.revupDialogBox('enableBtn', 'midBtn1');
                $dlg.revupDialogBox('enableBtn', 'midBtn2');
                $dlg.attr('valueChanged', true);
                bChanged = true;
            }
        });

        // load the email drop down
        tabIndex = 4;
        $('.emailLst', $dlg).revupDropDownField({
            value: emailLst,
            valueStartIndex: emailLstStartIndex,
            changeFunc: function(i, v) {
                v = v.split('::');
                if (templateData.emailUpdateFunc && v.length == 2) {
                    templateData.emailUpdateFunc(v[1]);
                }

                $dlg.revupDialogBox('enableBtn', 'midBtn1');
                $dlg.revupDialogBox('enableBtn', 'midBtn2');
                $dlg.attr('valueChanged', true);
                bChanged = true;
            }
        })

        // adjust the size of the edit fields
        let bodyHeight = $('.msgBodyDiv', $dlg).height();
        let headerHeight = $('.contentHeader', $msgBody).height();
        // 98 - 2 headers 22 for margin, 32 editor header height, 20 extra, 30 for top and bottom
        // padding and 10 between header and content
        let editorHeight = (bodyHeight - (headerHeight + 168)) / 2;
        $('.editor1', $msgBody).height(editorHeight);
        $('.editor2', $msgBody).height(editorHeight);


        // adjust tabIndex
        $('.personalInfo .employer', $dlg).attr('tabindex', tabIndex++);
        $('.personalInfo .occupation', $dlg).attr('tabindex', tabIndex++);

        // set focus to field with tabindex 1
        var $firstField = $dlg.find("[tabindex=1]")
        $firstField.setFocusToEnd();

        // enable/disable the update of the editor1 buttons
        $editor1 = $('.editor1', $dlg);
        $editor1.attr('tabindex', tabIndex++);
        lastTabStop = tabIndex;
        $editor1
            .on('focusin', function() {
                bEditor1Active = true;
            })
            .on('focusout', function() {
                bEditor1Active = false;
            })
            .on('blur keyup paste input', function() {
                $dlg.revupDialogBox('enableBtn', 'midBtn1');
                $dlg.revupDialogBox('enableBtn', 'midBtn2');

                $dlg.attr('valueChanged', true);
                bChanged = true;
            });

        // enable/disable the update of the editor2 buttons
        $editor2 = $('.editor2', $dlg);
        $editor2.attr('tabindex', tabIndex);
        lastTabStop = tabIndex;
        $editor2
            .on('focusin', function() {
                bEditor2Active = true;
            })
            .on('focusout', function() {
                bEditor2Active = false;
            })
            .on('blur keyup paste input', function() {
                $dlg.revupDialogBox('enableBtn', 'midBtn1');
                $dlg.revupDialogBox('enableBtn', 'midBtn2');

                $dlg.attr('valueChanged', true);
                bChanged = true;
            });
    } // loadCallSheet

    function loadError(r)
    {
        var sTmp = [];
        sTmp.push('<div class="revupDialogErrorIn">');
            sTmp.push('<div class="msg">');
                sTmp.push("Unable to load Call Sheet at this time<br><br>Try again later");
            sTmp.push('</div>')
        sTmp.push('</div>')

        var $callSheet = $('.callSheet2', $dlg);
        $callSheet.html(sTmp.join(''));

        // disable the print button
        $('.okBtn', $dlg).prop('disabled', true)

    } // loadError

    return {
        create: function(arg1, arg2, arg3) {

            // reset globals
            $dlg = $([]);
            $editor1 = $([]);
            $editor2 = $([]);
            maxFedDisplay =     10;
            maxStateDisplay =   5;
            bEditor1Active =  false;
            bEditor2Active =  false;
            bChanged = false;

            // see if the data was passed in or need to fetch it
            if ($.type(arg1) == 'object') {
                templateData = arg1;
            }
            else {
                revupUtils.apiError('Prospects', undefined, 'No Data for Callsheet', "No Callsheet Data");
            }

            // see if there are notes
            // notesId =   templateData.notesId ? templateData.notesId : -1;

            var sTmp = [];

            sTmp.push('<div class="callSheet2Loading">');
                sTmp.push('<div class="loadingDiv">');
                    sTmp.push('<div class="loading"></div>');
                    sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                sTmp.push('</div>');
            sTmp.push('</div>');

            sTmp.push('<div class="callSheet2">');
                sTmp.push('<div style="width:100%;height:500px;position:relative;top:0;left:0;xbackground-color:lightgray">');
                    sTmp.push('<div class="loadingDiv">');
                        sTmp.push('<div class="loading"></div>');
                        sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            sTmp.push('<div class="callSheet2LoadingHtml">');
                sTmp.push('<div class="loadingDiv">');
                    sTmp.push('<div class="loading"></div>');
                    sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                sTmp.push('</div>');
                sTmp.push('<div class="callSheet2LoadingText">Loading Call Sheet Preview</div>')
            sTmp.push('</div>');

            sTmp.push('<div class="callSheetPreview">')
                sTmp.push('<div class="innerPreview"></div>')
            sTmp.push('</div>')

            sTmp.push('<div class="callSheetQueued">')
                sTmp.push('<div class="msg">Your Call Sheet is queued.<br> You will receive and email when the Call Sheet is available.</div>');
            sTmp.push('</div>');

            // compute the height
            var h = $(window).height();
            h = Math.round(h * 0.9);
            if (h < 600) {
                h = 600;
            }

            function saveEditorsPrint(bClose, bDontShowLoading, bGetPrintHtml)
            {
                function getPrintHtml()
                {
                    // hide the callsheet and display the preview loading
                    $('.callSheet2', $dlg).hide()
                    $('.callSheet2Loading', $dlg).hide()
                    $('.callSheet2LoadingHtml', $dlg).show();
                    $('.midBtn1', $dlg).hide();
                    $('.midBtn2', $dlg).hide();

                    url = templateData.getPrintHtml;
                    $.ajax({
                        url: url,
                        type: 'GET',
                    })
                    .done(function(r) {
                        $('.callSheet2LoadingHtml', $dlg).hide();
                        $('.callSheetPreview', $dlg).show()
                                                    .height(($('msgBodyDiv', $dlg).height()) - 10);
                        $('.callSheetPreview .innerPreview', $dlg).html(r);

                        $dlg.revupDialogBox('enableBtn', 'okBtn');
                        $('.okBtn', $dlg).html('Print');
                    })
                    .fail(function(r) {
    console.log('ERROR - getPrintHtml: ', r)
                    })
                    .always(function(r) {
                        $dlg.revupDialogBox('enableBtn', 'cancelBtn');
                    })
                } // getPrintHtml


                // disable buttons
                $dlg.revupDialogBox('disableBtn', 'cancelBtn');
                $dlg.revupDialogBox('disableBtn', 'midBtn1');
                $dlg.revupDialogBox('disableBtn', 'midBtn2');
                $dlg.revupDialogBox('disableBtn', 'okBtn');

                // display the loading overlay
                if (!bDontShowLoading) {
                    var h = $msgBody.outerHeight(true);
                    $callSheetLoading.height(h);
                    $callSheetLoading.show();
                }

                if ($dlg.attr('valueChanged') == 'true') {
                    var data = {};
                    var editor1Text = $('.editor1').html();
                    var editor2Text = $('.editor2').html();
                    data.notes1 = editor1Text;
                    data.notes2 = editor2Text;
                    var url = templateData.notesApi;
                    url = url.replace('contactId', templateData.contactId)
                             .replace('contactSetId', templateData.contactSetId);
                    ajaxType = "POST";
                    let notesId = parseInt($editorDiv.attr('notesId'), 10);
                    if (notesId != -1) {
                        url = url + notesId + "/";
                        ajaxType = "PUT";
                    }
                    $.ajax({
                        url: url,
                        type: ajaxType,
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                    })
                    .done(function(r) {
                        // update the notesId
                        $editorDiv.attr('notesId', r.id);

                        // send the caller the updated notes
                        if (templateData.saveNotesFunc) {
                            data.id = r.id;
                            templateData.saveNotesFunc(data);
                        }

                        // get the print html
                        if (bGetPrintHtml) {
                            getPrintHtml();
                        }

                        // clear the flag
                        bChanged = false;
                    })
                    .fail(function(r) {
                        revupUtils.apiError('Callsheet', r, 'Unable save notes', "Unable to save Callsheet Ask/Results notes");
                    })
                    .always(function(r) {
                        $callSheetLoading.hide();

                        if (bClose) {
                            $dlg.revupDialogBox('close');
                        }
                        else {
                            $dlg.revupDialogBox('enableBtn', 'cancelBtn');
                            $dlg.revupDialogBox('enableBtn', 'okBtn');
                        }
                    });
                }
                else {
                    if (bGetPrintHtml) {
                        getPrintHtml();
                    }
                }
            } // saveEditorsPrint

            function queueUpPrint(callTimeContactId)
            {
                $dlg.revupDialogBox('disableBtn', 'cancelBtn');
                $dlg.revupDialogBox('disableBtn', 'okBtn');

                // build the data
                let url = templateData.queuePrintUrl;
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: JSON.stringify({ids: [callTimeContactId]}),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                })
                .done(function(r) {
                    $('.cancelBtn', $dlg).hide();
                    $('.okBtn', $dlg).html('Close');
                    $dlg.revupDialogBox('enableBtn', 'okBtn');

                    $('.callSheetPreview', $dlg).hide();
                    $('.callSheetQueued', $dlg).show();
                })
                .fail(function(r) {
                    revupUtils.apiError('Call Time', r, 'Bulk Print',
                                        "Unable to Bulk Print Individual Call Sheets at this time");
                });

            } // queueUpPrint

            var callDlgConfig = {
                okBtnText: 'Print Preview',
                autoCloseOk: false,
                autoCloseCancel: false,
                cancelBtnText: 'Close',
                width: '975px',
                height: h + 'px', //'600px',
                top: '20',
                msg: sTmp.join(''),
                okHandler: function(e) {
                    if ($('.okBtn', $dlg).html() == 'Print Preview') {
                        saveEditorsPrint(false, true, true);
                    }
                    else if ($('.okBtn', $dlg).html() == 'Print') {
                        //queueUpPrint(templateData.callTimeContactId);
                        $('.backgroundDiv').addClass('printModalOpen');
                        window.print();

                    }
                    else if ($('.okBtn', $dlg).html() == 'Close') {
                        $('.backgroundDiv').removeClass('printModalOpen');
                        $dlg.revupDialogBox('close');
                    }
                },
                cancelHandler: function(e) {
                    if (bChanged) {
                        let msg = 'Your changes have not been saved, are you sure you want to close the call sheet?'
                        let $closeConfBox = $('body').revupConfirmBox({
                            headerText: 'Changes not Saved',
                            msg: msg,
                            zIndex: 25,
                            okHandler: function() {
                                clearInterval(styleBtnStateInterval);
                                $('.backgroundDiv').removeClass('printModalOpen');
                                $dlg.revupDialogBox('close');
                            }
                        })
                    }
                    else {
                        clearInterval(styleBtnStateInterval);
                        $('.backgroundDiv').removeClass('printModalOpen');
                        $dlg.revupDialogBox('close');
                    }
                }
            }

            if (templateData.saveNotes) {
                callDlgConfig.midBtn1Text = "Save";
                callDlgConfig.midBtn1Enable = false;
                callDlgConfig.midBtn1AutoClose = false;
                callDlgConfig.midBtn1Handler = function(e) {
                    saveEditorsPrint(false);
                };
                callDlgConfig.midBtn2Text = "Save & Close";
                callDlgConfig.midBtn2Enable = false;
                callDlgConfig.midBtn2AutoClose = true;
                callDlgConfig.midBtn2Handler= function(e) {
                    saveEditorsPrint(true);
                };
            }

            $dlg = $('body').revupDialogBox(callDlgConfig)

            loadCallSheet(templateData);

            // save the notesId
            $editorDiv.attr('notesId', templateData.notesId);

            // load the ask and results sections
            // display the wait overlay and disable the print button
            var h = $msgBody.outerHeight(true);
            $callSheetLoading.height(h);
            $callSheetLoading.show();
            $dlg.revupDialogBox('disableBtn', 'okBtn');

            // get the currently saved notes
            if (templateData.saveNotes) {
                if (templateData.notes1 != undefined || templateData.notes2 != undefined) {
                    // notesId = templateData.notesId;
                    $('.editor1').html(templateData.notes1);
                    $('.editor2').html(templateData.notes2);

                    // hide the wait/loading screen and enable print btn
                    $callSheetLoading.hide();
                    $dlg.revupDialogBox('enableBtn', 'okBtn');
                }
                else {
                    var url = templateData.notesApi;
                    url = url.replace('contactId', templateData.contactId)
                             .replace('contactSetId', templateData.contactSetId);
                    $.ajax({
                        url: url,
                        type: "GET",
                    })
                    .done(function(r) {
                        if (r.count > 0) {
                            var result = r.results[0];
                            $editorDiv.attr('notesId', result.id);
                            $('.editor1').html(result.notes1);
                            $('.editor2').html(result.notes2);
                        }
                        else {
                            $editorDiv.attr('notesId', -1);
                            $('.editor1').html('');
                            $('.editor2').html('');
                        }
                    })
                    .fail(function(r) {
                        revupUtils.apiError('Callsheet', r, 'Unable to retrieve Ask/Results Data', "Unable to load Ask/Results data");
                    })
                    .always(function(r) {
                        // hide the wait/loading screen and enable print btn
                        $callSheetLoading.hide();
                        $dlg.revupDialogBox('enableBtn', 'okBtn');
                    });
                }
            }
            else {
                // hide the wait/loading screen and enable print btn
                $callSheetLoading.hide();
                $dlg.revupDialogBox('enableBtn', 'okBtn');
            }


            // keep the tab order circular within the Dialog
            $dlg.on('keydown', function(e) {
                // see if the
                if (e.keyCode == 9) {
                    var currentTab = parseInt($(":focus").attr("tabindex"), 10);
                    if (e.shiftKey) {
                        // back tabindex
                        if (currentTab == firstTabStop) {
                            $dlg.find('[tabindex=' + lastTabStop + ']').setFocusToEnd();
                            return false;
                        }
                    }
                    else {
                        // tab
                        if (currentTab == lastTabStop) {
                            $dlg.find('[tabindex=' + firstTabStop + ']').setFocusToEnd();
                            return false;
                        }
                    }

                var newTab = parseInt(currentTab, 10) + 1;
                if (e.shiftKey) {
                    newTab = parseInt(currentTab, 10) - 1
                }
                $dlg.find('[tabindex=' + newTab + ']').setFocusToEnd();
                return false;
                }
            })

            // see the current styling state of the editor
            var $askBoldBtn = $('.editor1Div .editBoldBtn', $dlg);
            var $askItalicBtn = $('.editor1Div .italicBoldBtn', $dlg);
            var $askUnderlineBtn = $('.editor1Div .underlineBoldBtn', $dlg);
            var $askOrderLstBtn = $('.editor1Div .orderLstBtn', $dlg);
            var $askUnorderLstBtn = $('.editor1Div .unorderLstBtn', $dlg);

/*
            var $resultsBoldBtn = $('.editor2Div .editBoldBtn', $dlg);
            var $resultsItalicBtn = $('.editor2Div .italicBoldBtn', $dlg);
            var $resultsUnderlineBtn = $('.editor2Div .underlineBoldBtn', $dlg);
            var $resultsOrderLstBtn = $('.editor2Div .orderLstBtn', $dlg);
            var $resultsUnorderLstBtn = $('.editor2Div .unorderLstBtn', $dlg);
*/

            var styleBtnStateInterval = setInterval(function () {
                if ($askBoldBtn.length == 0) {
                    $editor1 = $('.editor1Div .editor1', $dlg);

                    $askBoldBtn = $('.editor1Div .editBoldBtn', $dlg);
                    $askItalicBtn = $('.editor1Div .italicBoldBtn', $dlg);
                    $askUnderlineBtn = $('.editor1Div .underlineBoldBtn', $dlg);
                    $askOrderLstBtn = $('.editor1Div .orderLstBtn', $dlg);
                    $askUnorderLstBtn = $('.editor1Div .unorderLstBtn', $dlg);
                }

/*
                if ($resultsBoldBtn.length == 0) {
                    $editor2 = $('.editor1Div .editor1', $dlg);

                    $resultsBoldBtn = $('.editor2Div .editBoldBtn', $dlg);
                    $resultsItalicBtn = $('.editor2Div .italicBoldBtn', $dlg);
                    $resultsUnderlineBtn = $('.editor2Div .underlineBoldBtn', $dlg);
                    $resultsOrderLstBtn = $('.editor2Div .orderLstBtn', $dlg);
                    $resultsUnorderLstBtn = $('.editor2Div .unorderLstBtn', $dlg);
                }
*/

                if (bEditor1Active) {
                    var isBold = document.queryCommandValue("Bold");
                    var isItalic = document.queryCommandValue("Italic");
                    var isUnderline = document.queryCommandValue('underline');
                    var isOrderLst = document.queryCommandValue('insertOrderedList');
                    var isUnorderLst = document.queryCommandValue('insertUnorderedList');
                    if (isBold === 'true') {
                        $askBoldBtn.addClass('active');
                    } else {
                        $askBoldBtn.removeClass('active');
                    }

                    if (isItalic === 'true') {
                        $askItalicBtn.addClass('active');
                    } else {
                        $askItalicBtn.removeClass('active');
                    }

                    if (isUnderline === 'true') {
                        $askUnderlineBtn.addClass('active');
                    } else {
                        $askUnderlineBtn.removeClass('active');
                    }

                    if (isOrderLst === 'true') {
                        $askOrderLstBtn.addClass('active');
                    } else {
                        $askOrderLstBtn.removeClass('active');
                    }

                    if (isUnorderLst === 'true') {
                        $askUnorderLstBtn.addClass('active');
                    } else {
                        $askUnorderLstBtn.removeClass('active');
                    }
                }
                else {
                    $askBoldBtn.removeClass('active');
                    $askItalicBtn.removeClass('active');
                    $askUnderlineBtn.removeClass('active');
                    $askOrderLstBtn.removeClass('active');
                    $askUnorderLstBtn.removeClass('active');
                }

/*
                if (bEditor2Active) {
                    var isBold = document.queryCommandValue("Bold");
                    var isItalic = document.queryCommandValue("Italic");
                    var isUnderline = document.queryCommandValue('underline');
                    var isOrderLst = document.queryCommandValue('insertOrderedList');
                    var isUnorderLst = document.queryCommandValue('insertUnorderedList');
                    if (isBold === 'true') {
                        $resultsBoldBtn.addClass('active');
                    } else {
                        $resultsBoldBtn.removeClass('active');
                    }

                    if (isItalic === 'true') {
                        $resultsItalicBtn.addClass('active');
                    } else {
                        $resultsItalicBtn.removeClass('active');
                    }

                    if (isUnderline === 'true') {
                        $resultsUnderlineBtn.addClass('active');
                    } else {
                        $resultsUnderlineBtn.removeClass('active');
                    }

                    if (isOrderLst === 'true') {
                        $resultsOrderLstBtn.addClass('active');
                    } else {
                        $resultsOrderLstBtn.removeClass('active');
                    }

                    if (isUnorderLst === 'true') {
                        $resultsUnorderLstBtn.addClass('active');
                    } else {
                        $resultsUnorderLstBtn.removeClass('active');
                    }
                }
                else {
                    $resultsBoldBtn.removeClass('active');
                    $resultsItalicBtn.removeClass('active');
                    $resultsUnderlineBtn.removeClass('active');
                    $resultsOrderLstBtn.removeClass('active');
                    $resultsUnorderLstBtn.removeClass('active');
                }
*/
            }, 100);

            // button handlers
            $dlg.on('click', '.editor1Div .editBoldBtn', function() {
                    var $btn = $(this);
                    $editor1.focus();
                    document.execCommand('bold', false, null);

                    $btn.toggleClass('active');
                })
                .on('click', '.editor1Div .italicBoldBtn', function() {
                    var $btn = $(this);
                    $editor1.focus();
                    document.execCommand('italic', false, null);

                    $btn.toggleClass('active');
                })
                .on('click', '.editor1Div .underlineBoldBtn', function() {
                    var $btn = $(this);
                    $editor1.focus();
                    document.execCommand('underline', false, null);

                    $btn.toggleClass('active');
                })
                .on('click', '.editor1Div .indentBtn', function() {
                    var $btn = $(this);
                    $editor1.focus();
                    document.execCommand('indent', false, null);
                })
                .on('click', '.editor1Div .outdentBtn', function() {
                    var $btn = $(this);
                    $editor1.focus();
                    document.execCommand('outdent', false, null);
                })
                .on('click', '.editor1Div .orderLstBtn', function() {
                    var $btn = $(this);
                    $editor1.focus();
                    document.execCommand('insertOrderedList', false, null);

                    $btn.toggleClass('active');
                })
                .on('click', '.editor1Div .unorderLstBtn', function() {
                    var $btn = $(this);
                    $editor1.focus();
                    document.execCommand('insertUnorderedList', false, null);

                    $btn.toggleClass('active');
                })

                .on('click', '.editor2Div .editBoldBtn', function() {
                    var $btn = $(this);
                    $editor2.focus();
                    document.execCommand('bold', false, null);

                    $btn.toggleClass('active');
                })
                .on('click', '.editor2Div .italicBoldBtn', function() {
                    var $btn = $(this);
                    $editor2.focus();
                    document.execCommand('italic', false, null);

                    $btn.toggleClass('active');
                })
                .on('click', '.editor2Div .underlineBoldBtn', function() {
                    var $btn = $(this);
                    $editor2.focus();
                    document.execCommand('underline', false, null);

                    $btn.toggleClass('active');
                })
                .on('click', '.editor2Div .indentBtn', function() {
                    var $btn = $(this);
                    $editor2.focus();
                    document.execCommand('indent', false, null);
                })
                .on('click', '.editor2Div .outdentBtn', function() {
                    var $btn = $(this);
                    $editor2.focus();
                    document.execCommand('outdent', false, null);
                })
                .on('click', '.editor2Div .orderLstBtn', function() {
                    var $btn = $(this);
                    $editor2.focus();
                    document.execCommand('insertOrderedList', false, null);

                    $btn.toggleClass('active');
                })
                .on('click', '.editor2Div .unorderLstBtn', function() {
                    var $btn = $(this);
                    $editor2.focus();
                    document.execCommand('insertUnorderedList', false, null);

                    $btn.toggleClass('active');
                });
        }
    }
} ());
