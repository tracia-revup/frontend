    "use strict";
var editContact = (function (){
    // selectors
    var $editContactDlg = $();
    var $firstName = $();
    var $firstNameLabel = $();
    var $middleName = $();
    var $lastName = $();
    var $lastNameLabel = $();
    var $emailSection = $();
    var $addressSection = $();
    var $regionSection = $();
    var $phoneSection = $();
    var $reqFieldsLabel = $();
    var $deleteContactWarning = $();
    var detailId = -1;
    var $selectedContactName =  $();
    var $selectedContactEmailCol = $();
    var $selectedContactEmail = $();
    var currentEditContact = {};        // currently loaded edit cont
    var $contactLstDiv = $();

    var bEditFieldChanged = false;
    var bEditRequired = false;

    var loadingCompleted = false;

    var preEdit = '';
    var url = '';

    const pageSize              = 50;                            //number of entries to fetch
    const nextPageMarkerIndex   = Math.round(pageSize * .80);    // where to put marker

    function displayErrorWarning(ewType, message, bHideClose = false)
    {
        var $ewDiv = $('.warningErrorDiv', $editContactDlg);

        // update the class
        var c = "warningErrorDiv " + ewType;
        $ewDiv.prop('class', c);

        // update the message
        $('.msg', $ewDiv).html(message);

        // hide the close Buttons
        if (!bHideClose) {
            $('.closeBtn', $ewDiv).hide()
        }
        else {
            $('.closeBtn', $ewDiv).show()
        }

        // display
        if (!$ewDiv.is(':visible')) {
            var $scrollSection = $('.scrollSection', $editContactDlg);
            var h = $scrollSection.height() - $ewDiv.outerHeight(true);
            $scrollSection.css('height', h);
            $ewDiv.fadeIn(400, function() {
                //var h = $scrollSection.height() - $ewDiv.outerHeight(true);
                //$scrollSection.css('height', h);
            });
        }
    } // displayErrorWarning

    function hideErrorWarning()
    {
        var $ewDiv = $('.warningErrorDiv', $editContactDlg);
        if ($ewDiv.is(':visible')) {
            $ewDiv.fadeOut(400, function () {
                var $scrollSection = $('.scrollSection', $editContactDlg);
                var h = $scrollSection.height() + $ewDiv.outerHeight(true);
                $scrollSection.css('height', h);
            });
        }
    } // hideErrorWarning

    var bNameUnique = true;
    function isNameUnique(firstName, lastName)
    {
        // first or last name are blank don't test
        if (!firstName || !lastName) {
            return;
        }

        // build the url
        var a = [];
        a.push('first_name=' + firstName);
        a.push('last_name=' + lastName);
        a.push('exact=1');
        var url = userContactsURL;
        if (a.length > 0) {
            url += "?" + a.join("&");
        }
        // fetch the data
        $.ajax({
            url: url,
            type: 'GET',
        })
        .done(function(r) {
            if (r.count > 0) {
                displayErrorWarning('warning', 'A contact of this name already exists in RevUp.');
                bNameUnique = false;
            }
            else {
                hideErrorWarning();
                bNameUnique = true;
            }
        })
        .fail(function(r) {
            console.error('Unable to test is a name exist: ', r);
            displayErrorWarning('warning', 'A contact of this name already exists in RevUp.');
        });

    } // isNameUnique

    function  updateDetails(r)
    {
        // get the selected entry
        var $selected = $('.contactLst .entry.selected', $editContactDlg);

        // update the contact id
        $selected.attr('contactId', r.id);

        // if the save of the newEntry is sucessful, make it part of the contactList
        var sTmp = [];
        sTmp.push('<div class="entry selected" contactId="' + r.id + '">');

        // name
        var name = "";
        if (r.first_name) {
            name += (r.first_name);
        }
        if (r.last_name) {
            name += (r.last_name);
        }
        sTmp.push('<div class="column nameCol">')
            sTmp.push('<div class="nameContainer">' + name + '</div>');
        sTmp.push('</div>');

        // email address
        var email = '-';
        if (r.email_addresses.length > 0) {
            email = r.email_addresses[0].address;
        }
        if (email == "-") {
            sTmp.push('<div class="column emailCol noEmailAddr">');
                sTmp.push('<div class="emailContainer">' + email + '</div>');
            sTmp.push('</div>');
        }
        else {
            sTmp.push('<div class="column emailCol">');
                sTmp.push('<div class="emailContainer">' + email + '</div>');
            sTmp.push('</div>');
        }

        // the status column
        sTmp.push('<div class="column statusCol">');
            sTmp.push('<div class="saving" style="display: none;">Saving</div>');
            sTmp.push('<div class="errorSaving" style="display: none;">Error</div>');
            sTmp.push('<div class="deleteBtn icon icon-delete-circle" style="display: none;"></div>');
        sTmp.push('</div>');

        sTmp.push('</div>');

        $('.lstDiv .contactLst', $editContactDlg).prepend(sTmp.join(''));
        $('.entryInProgress').hide().removeClass('selected');

    } // updateDetails

    var saveContactId = 0;
    function saveEdit($confirmDlg, $newSelectedEntry)
    {
        $('.editContactDlg .rightSide .loadingDiv').show();

        // get the selected entry
        var $selected = $('.entry.selected', $editContactDlg);
        var contactId = $selected.attr('contactId');
        if (contactId) {
            contactId = parseInt(contactId, 10);
        }
        saveContactId = contactId;
        let contactName = $('.nameContainer', $selected).html();

        // build the contact data
        var cData = {};
        cData = currentEditContact;
        if ((contactId) && (contactId != -1)) {
            cData.id = parseInt(contactId, 10);
        }
        cData.first_name = $firstName.val();
        cData.last_name = $lastName.val();
        cData.additional_name = $middleName.val();

        // load the email address
        cData.email_addresses = [];
        $('.emailSection .emailEntry', $editContactDlg).each(function() {
            var $e = $(this);
            var email = $('.emailAddress', $e).val();
            if (email !== '') {
                var eObj = {};
                eObj.address = email;

                // label
                //eObj.label = '';
                var label = $e.attr('emailLabel');
                if (label !== undefined) {
                    eObj.label = label;
                }

                // id
                eObj.id = null;
                var id = $e.attr('emailId');
                if (id !== undefined) {
                    eObj.id = parseInt(id, 10);
                }

                cData.email_addresses.push(eObj);
            }
        });

        // address
        cData.addresses = [];
        $('.addressSection .addrGrp', $editContactDlg).each(function() {
            var $e = $(this);

            var id = parseInt($e.attr('addrId'), 10);
            var label = $e.attr('addrLabel');

            var addr1 = $('.addr1', $e).val();
            var addr2 = $('.addr2', $e).val();
            var city = $('.city', $e).val();
            var state = $('.stateAddr', $e).revupDropDownField('getValue');
            var postalCode = $('.postalCode', $e).val();

            if (addr1 !== '' || addr2 !== '' || city !== '' || state !== '' || postalCode !== '' || state !== '') {
                var addr = {};

                if (id) {
                    addr.id = id;
                }
                addr.label = label ? label : '';

                addr.city = city ? city : '';
                addr.country = "USA";
                addr.po_box = addr2 ? addr2 : '';
                addr.post_code = postalCode ? postalCode : '';
                addr.region = state;
                addr.street = addr1 ? addr1 : '';

                cData.addresses.push(addr);
            }
        });

        // load the phone numbers
        cData.phone_numbers = [];
        $('.phoneSection .phoneEntry', $editContactDlg).each(function() {
            var $e = $(this);
            var phone = $('.phoneNumber', $e).val();
            if (phone !== '') {
                phone = revupUtils.getFormattedPhone(phone);
                var eObj = {};
                eObj.number =  phone;

                // label
                var label = $e.attr('phoneLabel');
                if (label !== undefined) {
                    eObj.label = label;
                }

                // id
                var id = $e.attr('phoneId');
                if (id !== undefined) {
                    eObj.id = parseInt(id, 10);
                }

                cData.phone_numbers.push(eObj);
            }
        });

        // see if there is missing sections
        if (!cData.organizations) {
            cData.organizations = [];
        }
        if (!cData.locations) {
            cData.locations = [];
        }
        if (!cData.alma_maters) {
            cData.alma_maters = [];
        }

        // enable the list - additional edit can be done while saving
        $('.contactLst .entry', $editContactDlg).removeClass('disabled').css('opacity', 0.999999);
        $('.newContactBtn', $editContactDlg).prop('disabled', false).removeClass('disabled');
        $('.contactSearch', $editContactDlg).revupSearchBox('enable');

        // display the saving color for selected entry
        $('.statusCol .saving', $selected).show();
        $selected.addClass('saving');

        var url = userContactsURL;
        var currentlyActive = ($('.togglePages .editTab.active'));
        if(currentlyActive.hasClass("accountTab")){
            url = accountContactsURL;
        }

        var ajaxType = "POST";
        if (contactId != -1) {
            ajaxType = "PUT";
            if(currentlyActive.hasClass("accountTab")){
                url = accountContactsURL;
            }
            url += contactId + '/';
        }

        $.ajax({
            url: url,
            type: ajaxType,
            data: JSON.stringify(cData),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        })
        .done(function(r) {
            $('.editContactDlg .rightSide .loadingDiv').hide();
            // update the alert bar
            updateRevupAlert();

            // broadcast contact being deleted
            $.event.trigger({
                type: 'editContact',
                contactId: contactId,
                contactName: contactName,
                contactData: cData,
            });


            //after a sucessful save, display the just saved contact's details
            displayContactDetails(r.id);

            $selected.removeClass('saving');

            //if we are not updating an existing contact, but creating a new contact, make it part of the contact list instead
            if(ajaxType != "PUT"){
                updateDetails(r);
                let totalContacts = parseInt($('.leftSide .lstDiv .contactLstDiv', $editContactDlg).attr('numentries'), 10);
                let loadedEntries = parseInt($('.leftSide .lstDiv .contactLstDiv', $editContactDlg).attr('numentriesloaded'), 10);

                $('.leftSide .lstDiv .contactLstDiv', $editContactDlg).attr('numentries',  totalContacts + 1);
                $('.leftSide .lstDiv .contactLstDiv', $editContactDlg).attr('maxpages',  Math.ceil((totalContacts + 1) / pageSize));
                $('.leftSide .lstDiv .contactLstDiv', $editContactDlg).attr('numentriesloaded', loadedEntries + 1);
                $('.contactCount', $editContactDlg).text(revupUtils.commify(totalContacts + 1));
            }

            // if a confirmDlg close the dialog
            if ($confirmDlg && ($confirmDlg.length > 0)) {
                $confirmDlg.revupConfirmBox('close');
            }

            // see if there is a new selection to make
            if ($newSelectedEntry && ($newSelectedEntry.length > 0)) {
                // clear selected
                $('.contactLst .entry', $editContactDlg).removeClass('selected');

                // select new entry
                $newSelectedEntry.addClass('selected');

                // get selected fields
                $selectedContactName = $('.lstDiv .entry.selected .nameContainer', $editContactDlg);
                $selectedContactEmailCol = $('.lstDiv .entry.selected .emailCol', $editContactDlg);
                $selectedContactEmail = $('.lstDiv .entry.selected .emailContainer', $editContactDlg);

                // load the selected field details
                var cId = $newSelectedEntry.attr("contactId");
                displayContactDetails(cId);
            }

            // reset the change flags
            bEditFieldChanged = false;
            bEditRequired = false;

            // clear the saving flag on the old selected line
            var $oldSelected = $('.contactLst .entry[contactId=' + contactId +']', $editContactDlg);
            $('.statusCol .saving', $oldSelected).hide();
            $('.statusCol .errorSaving', $oldSelected).hide();
            $('.statusCol .deleteBtn', $oldSelected).hide();

            // update the id
            $oldSelected.attr('contactId', r.id);

            // clear any warning/error messages
            hideErrorWarning();

            // reset button and fields
            editEnableDisableSave(false, true);
        })
        .fail(function(r) {
            console.error('Unable to Save/Update a contact at this time: ', r);
            displayErrorWarning('error', 'Failed to create/update the contact.');

            // clear the saving flag on the old selected line
            var $oldSelected = $('.contactLst .entry[contactId=' + contactId +']', $editContactDlg);
            $oldSelected.removeClass('saving');
            $('.statusCol .saving', $oldSelected).hide();
            $('.statusCol .errorSaving', $oldSelected).show();
            $('.statusCol .deleteBtn', $oldSelected).hide();
        });
    } // saveEdit

    function editEnableDisableSave(fieldChanged, reset)
    {
        var $newEntry = $('.entryInProgress');

        // reset the buttons and list area
        if (reset) {
            // disable the save and cancel buttons
            $('.editCancelBtn', $editContactDlg).attr('disabled', 'true');
            $('.editSaveBtn', $editContactDlg).prop('disabled', true);

            // enable the close button
            $editContactDlg.revupDialogBox('enableBtn', 'okBtn');

            // enable the list
            $('.contactLst .entry', $editContactDlg).removeClass('disabled').css('opacity', 0.999999);

            // disable the new connact button and search box
            $('.newContactBtn', $editContactDlg).prop('disabled', false).removeClass('disabled');
            $('.contactSearch', $editContactDlg).revupSearchBox('enable');

            // reset the edit change flag
            bEditFieldChanged = false;

            return;
        }

        // make sure there is a first and last name
        bEditRequired = true;
        var bHasFirstAndLastName = true;
        if (($firstName.val().trim() === '') || ($lastName.val().trim() === '')) {
            bEditRequired = false;
            bHasFirstAndLastName = false;
        }

        // see if any field is in error
        var $errorFields = $('.inputError, .inputFormatError', $editContactDlg);
        if ($errorFields.length > 0) {
            // disable the save button
            $('.editSaveBtn', $editContactDlg).prop('disabled', true);

            // disable the close button
            $editContactDlg.revupDialogBox('enableBtn', 'okBtn');

            // set change flag
            bEditFieldChanged = true;
        }
        else {
            // enable the save and cancel buttons
            bEditFieldChanged = false;
        }

        // if the field change change and the global change flag not set
        // set it and enable the save and cancel button, also disable the close button
        if (fieldChanged && !bEditFieldChanged && bHasFirstAndLastName) {
            bEditFieldChanged = true;
            $('.editSaveBtn', $editContactDlg).prop('disabled', false).removeClass('disabled');
            // enable the save and cancel buttons if the api of the list of contact has been completed
            if(loadingCompleted && $('.editCancelBtn', $editContactDlg).is(":visible")){
                $('.editCancelBtn', $editContactDlg).removeAttr('disabled');
            }
            else if(!loadingCompleted && $('.editCancelBtn', $editContactDlg).is(":visible")){
                $('.editCancelBtn', $editContactDlg).attr('disabled', 'true');
                $('.editSaveBtn', $editContactDlg).attr('disabled', true).addClass('disabled');
            }

            // disable the list, new contact button, and search
            var $selected = $('.contactLst .entry.selected', $editContactDlg);
            var selId = $selected.attr("contactId");
            $('.contactLst .entry:not([contactId=' + selId + '])', $editContactDlg).addClass('disabled').css('opacity', 0.3);

            // disable the new connact button and search box
            $('.newContactBtn', $editContactDlg).prop('disabled', true).addClass('disabled');
            $('.contactSearch', $editContactDlg).revupSearchBox('disable');
        }
        else {
            // enable the save and cancel buttons if none of the fields have changed
            //$('.editCancelBtn', $editContactDlg).attr('disabled', true);
            $('.editSaveBtn', $editContactDlg).prop('disabled', true);

            // disable the close button
            $editContactDlg.revupDialogBox('enableBtn', 'okBtn');
        }
    } // editEnableDisableSave

    /*
     * Edit Email
     */
    function addEditEmail($editContactDlg)
    {
        var sTmp = [];
        sTmp.push('<div class="line emailEntry sepLine">');
            sTmp.push('<input class="emailAddress deletable" placeholder="Email Address">');
            sTmp.push('<div class="deleteEmailBtn icon icon-delete-circle"></div>');
        sTmp.push('</div>');

        $('.emailSection .emailEntry', $editContactDlg).last().after(sTmp.join(''));
    } // addEditEmail

    function deleteEditEmail($delBtn, $editContactDlg)
    {
        if ($('.emailSection .emailEntry', $editContactDlg).length > 1) {
            var $e = $delBtn.closest('.line');
            $e.remove();
        }
        else {
            $('.emailSection .emailEntry .emailAddress', $editContactDlg).val('');
            $('.emailSection .emailEntry', $editContactDlg).removeAttr('emailId')
                                                           .removeAttr('emailLabel');
        }

        // make sure the first entry does not have the 'sepLine' class
        $($('.emailSection .emailEntry', $editContactDlg).get(0)).removeClass('sepLine');

        // enableDisableSave
        editEnableDisableSave(true);

        // update the list
        var $topEmail = $('.emailSection .emailAddress:first', $editContactDlg);
        var v = $topEmail.val();
        if (v === '') {
            $selectedContactEmailCol.addClass('noEmailAddr');
            $selectedContactEmail.text('-');
        }
        else {
            $selectedContactEmailCol.removeClass('noEmailAddr');
            $selectedContactEmail.text(v);
        }
    } // deleteEditEmail

    function displayContactEmail($editContactDlg, emails)
    {
        // get the nummber of emails and the email entries
        var numEmails = emails.length;
        var $emailEntries = $('.emailEntry', $emailSection);

        // see if any entries need to be delete
        var numDelete = $('.emailSection .emailAddress', $editContactDlg).length - numEmails;
        if (numDelete == $('.emailSection .emailAddress', $editContactDlg).length) {
            numDelete -= 1;
        }
        for (var d = 0; d < numDelete; d++) {
            $('.emailSection .emailEntry', $editContactDlg).last().remove();
        }

        // add extra entries
        if (numEmails > $('.emailEntry', $emailSection).length) {
            var numToAdd = numEmails - $('.emailEntry', $emailSection).length;
            for (var a = 0; a < numToAdd; a++) {
                var sTmp = [];
                sTmp.push('<div class="line emailEntry sepLine">');
                    sTmp.push('<input class="emailAddress deletable" placeholder="Email Address">');
                    sTmp.push('<div class="deleteEmailBtn icon icon-delete-circle"></div>');
                sTmp.push('</div>');

                $('.emailEntry', $emailSection).last().after(sTmp.join(''));
            }
        }

        // load the values
        if (emails.length === 0) {
            var $e = $('.emailAddress', $emailEntries.get(0));
            $e.val('');

            var $eEntry = $($e).closest('.emailEntry');
            $eEntry.removeAttr('emailId');
            $eEntry.removeAttr('emailLabel');
        }
        else {
            for (var i = 0; i < numEmails; i++) {
                var $e = $('.emailSection .emailAddress', $editContactDlg).get(i);
                $($e).val(emails[i].address);

                var $eEntry = $($e).closest('.emailEntry');
                $eEntry.attr("emailId", emails[i].id);
                $eEntry.attr('emailLabel', emails[i].label);
            }
        }

        // update the list
        var $topEmail = $('.emailSection .emailAddress:first', $editContactDlg);
        var v = $topEmail.val();
        if (v === '') {
            $selectedContactEmailCol.addClass('noEmailAddr');
            $selectedContactEmail.text('-');
        }
        else {
            $selectedContactEmailCol.removeClass('noEmailAddr');
            $selectedContactEmail.text(v);
        }
    } // displayContactEmail

    /*
     * Edit Address
     */
    function buildEditAddrHtml(addrNum)
    {
        var sTmp = [];

        sTmp.push('<div class="subSection grayBackground addrGrp" addrNum="' + addrNum +'">');
            sTmp.push('<div class="line">');
                sTmp.push('<div class="header left">Address ' + addrNum + '</div>');
                sTmp.push('<div class="right deleteAddrBtn icon icon-delete-circle"></div>');
            sTmp.push('</div>');
            sTmp.push('<div class="line">');
                sTmp.push('<div class="text">Address</div>');
            sTmp.push('</div>');
            sTmp.push('<div class="line">');
                sTmp.push('<input class="addr1" placeholder="Address">');
            sTmp.push('</div>');
            sTmp.push('<div class="line sepLine">');
                sTmp.push('<div class="text">Address continued</div>');
            sTmp.push('</div>');
            sTmp.push('<div class="line">');
                sTmp.push('<input class="addr2" placeholder="Address">');
            sTmp.push('</div>');
            sTmp.push('<div class="line sepLine">');
                sTmp.push('<div class="text cityAddr">City</div>');
                sTmp.push('<div class="text stateAddr">State</div>');
                sTmp.push('<div class="text postalCodeAddr">Zip Code</div>');
            sTmp.push('</div>');
            sTmp.push('<div class="line">');
                sTmp.push('<input class="city cityAddr" placeholder="City">');
                sTmp.push('<div class="stateDropDown stateAddr"></div>');
                sTmp.push('<input class="postalCode postalCodeAddr" placeholder="Zip">');
            sTmp.push('</div>');
        sTmp.push('</div>');

        return sTmp.join('');
    } // buildEditAddrHtml

    function addEditAddress($editContactDlg)
    {
        var numAddrSection = $('.addressSection .addrGrp', $editContactDlg).length;

        var addrHtml = buildEditAddrHtml(numAddrSection + 1);
        $('.addressSection .addrGrp', $editContactDlg).last().after(addrHtml);
        var $newAddr = $('.addressSection .addrGrp', $editContactDlg).last();

        // attached the states
        $('.stateDropDown', $newAddr).revupDropDownField({
            value:              revupConstants.dropDownStates,
            valueStartIndex:    -1,
            sortList:           true,
            bDisplayValue:      true,
            bAutoDropDownWidth: false,
            ifStartIndexNeg1Msg: "State",
            changeFunc:         function(i, v) {
                                        bEditFieldChanged = true;
                                        editEnableDisableSave(true);
                                    }
        });

        // add the validator
        $('.postalCode', $newAddr).numericOnly({bPhone: true});
    } // addEditAddress

    function deleteEditAddress($dlgBtn, $editContactDlg)
    {
        if ($('.addressSection .addrGrp', $editContactDlg).length > 1) {
            var $e = $dlgBtn.closest('.addrGrp');
            $e.remove();
        }
        else {
            $('.addressSection .addrGrp .addr1', $editContactDlg).val('');
            $('.addressSection .addrGrp .addr2', $editContactDlg).val('');
            $('.addressSection .addrGrp .city', $editContactDlg).val('');
            $('.addressSection .addrGrp .stateAddr', $editContactDlg).revupDropDownField('reset');
            $('.addressSection .addrGrp .postalCode', $editContactDlg).val('');

            $('.addressSection .addrGrp', $editContactDlg).removeAttr('addrId')
                                                          .removeAttr('addrLabel');
        }

        // make sure the first entry does not have the 'sepLine' class
        $($('.addressSection .addrGrp', $editContactDlg).get(0)).removeClass('sepLine');

        // renumber the address fields
        var addrNum = 1;
        $('.addressSection .addrGrp', $editContactDlg).each(function() {
            var $addrGrp = $(this);

            $addrGrp.attr('addrNum', addrNum);

            // new title
            var title = 'Address ' + addrNum;
            $('.header', $addrGrp).text(title);

            addrNum++;
        });


        // enableDisableSave
        editEnableDisableSave(true);
    } // deleteEditAddress

    function displayContactAddress($editContactDlg, addresses)
    {        // get the nummber of phone number ans the phone entries
            var numAddresses = addresses.length;

            // see if any entries need to be delete
            var numDelete = $('.addressSection .addrGrp', $editContactDlg).length - numAddresses;
            if (numDelete == $('.addressSection .addrGrp', $editContactDlg).length) {
                numDelete -= 1;
            }
            for (var d = 0; d < numDelete; d++) {
                $('.addressSection .addrGrp', $editContactDlg).last().remove();
            }

            // add extra entries
            function changeFunc(i, v) {
                bEditFieldChanged = true;
                editEnableDisableSave(true);
            } // changeFunc

            if (numAddresses > $('.addrGrp', $addressSection).length) {
                var currentNum = $('.addrGrp', $addressSection).length + 1;
                var numToAdd = numAddresses - $('.addrGrp', $addressSection).length;

                for (var a = 0; a < numToAdd; a++) {
                    var addrHtml = buildEditAddrHtml(a + currentNum);
                    $('.addrGrp', $addressSection).last().after(addrHtml);

                    // attached the states
                    var $newAddr = $('.addrGrp .stateAddr', $addressSection).last();
                    $newAddr.revupDropDownField({
                        value:              revupConstants.dropDownStates,
                        valueStartIndex:    -1,
                        sortList:           true,
                        bDisplayValue:      true,
                        ifStartIndexNeg1Msg: "State",
                        bAutoDropDownWidth: false,
                        changeFunc:         changeFunc,
                    });

                    // add the validator
                    $('.addrGrp .postalCode', $addressSection).last().numericOnly({bPhone: true});
                }
            }

            // load the values
            if (addresses.length === 0) {
                $('.addressSection .addrGrp .addr1', $editContactDlg).val('');
                $('.addressSection .addrGrp .addr2', $editContactDlg).val('');
                $('.addressSection .addrGrp .city', $editContactDlg).val('');
                $('.addressSection .addrGrp .stateAddr', $editContactDlg).revupDropDownField('reset');
                $('.addressSection .addrGrp .postalCode', $editContactDlg).val('');

                $('.addressSection .addrGrp', $editContactDlg).removeAttr('addrId')
                                                              .removeAttr('addrLabel');
            }
            else {
                for (var i = 0; i < numAddresses; i++) {
                    var $e = $($('.addressSection .addrGrp', $editContactDlg).get(i));
                    $('.addr1', $e).val(addresses[i].street);
                    $('.addr2', $e).val(addresses[i].po_box);
                    $('.city', $e).val(addresses[i].city);
                    if (addresses[i].region == '')
                        $('.stateAddr', $e).revupDropDownField('reset');
                    else
                        $('.stateAddr', $e).revupDropDownField('setValue', addresses[i].region);
                    $('.postalCode', $e).val(addresses[i].post_code);

                    var $eEntry = $($e).closest('.addrGrp');
                    $eEntry.attr("addrId", addresses[i].id);
                    $eEntry.attr('addrLabel', addresses[i].label);

                }
            }
        } // displayContactAddress

    /*
     * Region
     */
    function displayContactRegion($editContactDlg, regions)
    {
        // get the nummber of phone number ans the phone entries
        var numRegions = regions.length;
        var $locations = $('.locationEntry', $regionSection);

        // see if any entries need to be delete
        var numDelete = $('.regionSection .location', $editContactDlg).length - numRegions;
        if (numDelete == $('.regionSection .location', $editContactDlg).length) {
            numDelete -= 1;
        }
        for (var d = 0; d < numDelete; d++) {
            $('.regionSection .locationEntry', $editContactDlg).last().remove();
        }

        // add extra entries
        if (numRegions > $('.location', $locations).length) {
            var numToAdd = numRegions - $('.location', $locations).length;
            for (var a = 0; a < numToAdd; a++) {
                var sTmp = [];
                sTmp.push('<div class="line locationEntry sepLine">');
                    sTmp.push('<input class="location" readonly placeholder="Location">');
                sTmp.push('</div>');

                $('.locationEntry', $regionSection).last().after(sTmp.join(''));
            }
        }

        // load the values
        if (numRegions === 0) {
            // hide the location
            $regionSection.hide();

            var $e = $('.location', $locations.get(0));
            $e.val('');
        }
        else {
            // hide the location
            $regionSection.show();

            for (var i = 0; i < numRegions; i++) {
                var $e = $('.regionSection .location', $editContactDlg).get(i);
                $($e).val(regions[i].name);
            }
        }
    } // displayContactRegion

    /*
     * Edit phone
     */
    function addEditPhone($editContactDlg)
    {
        var sTmp = [];
        sTmp.push('<div class="line phoneEntry sepLine">');
            sTmp.push('<input class="phoneNumber deletable" placeholder="XXX-XXX-XXXX">');
            sTmp.push('<div class="deletePhoneBtn icon icon-delete-circle"></div>');
        sTmp.push('</div>');

        $('.phoneSection .phoneEntry', $editContactDlg).last().after(sTmp.join(''));
        // add the validator
        $('.phoneSection .phoneEntry .phoneNumber', $editContactDlg).last().numericOnly({bPhone: true});
    } // addEditPhone($editContactDlg)

    function deleteEditPhone($delBtn, $editContactDlg)
    {
        if ($('.phoneSection .phoneEntry', $editContactDlg).length > 1) {
            var $e = $delBtn.closest('.line');
            $e.remove();
        }
        else {
            $('.phoneSection .phoneEntry .phoneNumber', $editContactDlg).val('');
            $('.phoneSection .phoneEntry', $editContactDlg).removeAttr('phoneId')
                                                           .removeAttr('phoneLabel');
        }

        // make sure the first entry does not have the 'sepLine' class
        $($('.phoneSection .phoneEntry', $editContactDlg).get(0)).removeClass('sepLine');

        // enableDisableSave
        editEnableDisableSave(true);
    } // deleteEditPhone

    function displayContactPhone($editContactDlg, phoneNumbers)
    {
        // get the nummber of phone number ans the phone entries
        var numPhoneNumbers = phoneNumbers.length;
        var $phoneEntries = $('.phoneEntry', $phoneSection);

        // see if any entries need to be delete
        var numDelete = $('.phoneSection .phoneNumber', $editContactDlg).length - numPhoneNumbers;
        if (numDelete == $('.phoneSection .phoneNumber', $editContactDlg).length) {
            numDelete -= 1;
        }
        for (var d = 0; d < numDelete; d++) {
            $('.phoneSection .phoneEntry', $editContactDlg).last().remove();
        }

        // add extra entries
        if (numPhoneNumbers > $('.phoneEntry', $phoneSection).length) {
            var numToAdd = numPhoneNumbers - $('.phoneEntry', $phoneSection).length;
            for (var a = 0; a < numToAdd; a++) {
                var sTmp = [];
                sTmp.push('<div class="line phoneEntry sepLine">');
                    sTmp.push('<input class="phoneNumber deletable" placeholder="XXX-XXX-XXXX">');
                    sTmp.push('<div class="deletePhoneBtn icon icon-delete-circle"></div>');
                sTmp.push('</div>');

                $('.phoneEntry', $phoneSection).last().after(sTmp.join(''));

                // add the validator
                $('.phoneSection .phoneEntry .phoneNumber', $editContactDlg).last().numericOnly({bPhone: true});
            }
        }

        // load the values
        if (phoneNumbers.length === 0) {
            var $e = $('.phoneNumber', $phoneEntries.get(0));
            $e.val('');
            var $eEntry = $($e).closest('.phoneEntry');
            $eEntry.removeAttr('phoneId');
            $eEntry.removeAttr('phoneLabel');
        }
        else {
            for (var i = 0; i < numPhoneNumbers; i++) {
                var $e = $('.phoneSection .phoneNumber', $editContactDlg).get(i);
                var phoneNumber = phoneNumbers[i].number;
                phoneNumber = revupUtils.getFormattedPhone(phoneNumber);
                $($e).val(phoneNumber);

                var $eEntry = $($e).closest('.phoneEntry');
                $eEntry.attr("phoneId", phoneNumbers[i].id);
                $eEntry.attr('phoneLabel', phoneNumbers[i].label);
            }
        }
    } // displayContactPhone

    /*
     * reset edit details
     */
    function resetDetails($editContactDlg)
    {
        // Clear Name fields
        $('.nameSection .firstName', $editContactDlg).val('');
        $('.nameSection .middleName', $editContactDlg).val('');
        $('.nameSection .lastName', $editContactDlg).val('');


        // is more than on email remove then and then clear value
        var $emailSection = $('.emailSection .emailEntry', $editContactDlg);
        $emailSection.slice(1).remove();
        $('.emailSection .emailEntry .emailAddress', $editContactDlg).val("");
        $('.emailSection .emailEntry', $editContactDlg).removeAttr('emailId')
                                                       .removeAttr('emailLabel');

        // reset to a single address section and clear
        var $addressSection = $('.addressSection .addrGrp', $editContactDlg);
        $addressSection.slice(1).remove();
        $('.addressSection .addrGrp .addr1', $editContactDlg).val('');
        $('.addressSection .addrGrp .addr2', $editContactDlg).val('');
        $('.addressSection .addrGrp .city', $editContactDlg).val('');
        $('.addressSection .addrGrp .stateAddr', $editContactDlg).revupDropDownField('reset');
        $('.addressSection .addrGrp .postalCode', $editContactDlg).val('');
        $('.addressSection .addrGrp', $editContactDlg).removeAttr('addrId')
                                                      .removeAttr('attrLabel');
        $('.location', $editContactDlg).val('');

        // hide the location
        $regionSection.hide();

        // reset to a single phone number field
        $phoneSection = $('.phoneSection .phoneEntry', $editContactDlg);
        $phoneSection.slice(1).remove();
        $('.phoneSection .phoneEntry .phoneNumber', $editContactDlg).val("");
        $('.phoneSection .phoneEntry', $editContactDlg).removeAttr('phoneId')
                                                       .removeAttr('phoneLabel');
    } //  resetDetails

    //anytime when a new entry is being added successfully, clear the entry for the next new entry
    function resetNewEntry(){
        var $progressName = $('.lstDiv .entryInProgress .nameContainer');
        var $progressEmail = $('.lstDiv .entry.selected .emailContainer', $editContactDlg);
        var $progressStatus = $('.lstDiv .entryInProgress .statusCol');
        var $errorFields = $('.inputError, .inputFormatError', $editContactDlg);

        $('.entryInProgress').hide().removeClass('selected');
        $selectedContactEmail.html('-');
        // $progressStatus.hide();
        $progressName.html('New Contact');
        $errorFields.removeClass('inputError');
    }//resetNewEntry

    /*
     *
     *  New Contact
     *
     */
    function newContact($newBtn, $editContactDlg)
    {
        $('.deleteContactBtn', $editContactDlg).addClass('disabled');
        //if the user has clicked New Contact before the list of contacts have returned, disable the Cancel and Save button until the api returns
        if(!loadingCompleted){
            $('.editSaveBtn', $editContactDlg).attr('disabled', true).addClass('disabled');
            $('.editCancelBtn', $editContactDlg).attr('disabled', true);
            $('.entryInProgress .statusCol .deleteBtn').hide();
        }
        else if(loadingCompleted){
           $('.editSaveBtn', $editContactDlg).prop('disabled', false).removeClass('disabled');
           $('.editCancelBtn', $editContactDlg).attr('disabled', false);
           $('.entryInProgress .statusCol .deleteBtn').show();
        }

        //reset the entryInProgress again here just in case
        resetNewEntry();
        // add the new entry
        var $newEntry = $('.entryInProgress', $editContactDlg);
        $newEntry.show().addClass('selected');
        $('.editContactDlg .rightSide .loadingDiv').hide();
        $('.lstDiv .contactLst .entry', $editContactDlg).removeClass('selected');

        // get the seclected contact name and email fields
        $selectedContactName = $('.lstDiv .entry.selected .nameContainer', $editContactDlg);
        $selectedContactEmailCol = $('.lstDiv .entry.selected .emailCol', $editContactDlg);
        $selectedContactEmail = $('.lstDiv .entry.selected .emailContainer', $editContactDlg);

        // disable the new connact button and search box
        $newBtn.prop('disabled', true).addClass('disabled');
        $('.contactSearch', $editContactDlg).revupSearchBox('disable');

        // reset the details
        resetDetails($editContactDlg);
        editEnableDisableSave(false);

        // mzke sure the first, middle, and last name fields are editable
        $firstName.prop('readonly', false);
        $firstNameLabel.text('*First Name');
        $middleName.prop('readonly', false);
        $lastName.prop('readonly', false);
        $lastNameLabel.text('*Last Name');

        $('.firstName', $editContactDlg).focus();

        // load/clear the currentEditContact
        currentEditContact = {};

        var selId = -1;
        $('.contactLst .entry:not([contactId=' + selId + '])', $editContactDlg).addClass('disabled').css('opacity', 0.3);

        // display the Required Fields label
        $reqFieldsLabel.show();
     } // newContact

     function deleteContact($deleteBtn, $editContactDlg)
     {
         var $selectedContact = $('.entry.selected', $editContactDlg);
         var contactId = $selectedContact.attr('contactid');
         var contactName = $('.nameContainer', $selectedContact).text();
         let activeTab = 'personalTab';
         if ($('.editTab.accountTab.active',  $editContactDlg).length > 0) {
             activeTab = 'accountTab'
         }

         if(contactId){
             url = userContactsURL;
             var currentlyActive = $('.togglePages .editTab.active');
             if(currentlyActive.hasClass("accountTab")){
                 url = accountContactsURL;
             }
             url += contactId + '/';
             url += '?dryrun=true';
             $.ajax({
                 url: url,
                 type: "DELETE",
             })
             .done(function(r) {
                var sTmp = [];
                sTmp.push('<div class="deleteContactWarning">');
                    sTmp.push('<div class="waitingDiv loadingDiv">');
                        sTmp.push('<div class="loading"></div>');
                        sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                    sTmp.push('</div>')

                    sTmp.push('<div class=warningContent">');
                        sTmp.push('<div class="deleteWarningIcon icon icon-warning"></div>');
                        sTmp.push('<div class="deleteWarningText">');
                            sTmp.push('Are you sure you want to remove the contact ' + contactName + ' ?' );
                            sTmp.push('<br><br>');
                            sTmp.push('This will <div class="boldRed">permanently</div> remove:');
                            sTmp.push('<br><br>');
                            sTmp.push('<div class="entry">' + r['Contacts'] + ' contact</div>');
                            sTmp.push('<div class="entry">' + r['Notes'] + ' contact notes</div>');
                            if (activeTab == 'personalTab') {
                                if (r.Prospects > 0) {
                                    sTmp.push('<div class="entry">' + 'In Prospects List</div>')
                                }
                                else {
                                    sTmp.push('<div class="entry">' + 'Not In Prospects List</div>')
                                }
                            }
                            else if (activeTab == 'accountTab') {
                                sTmp.push('<div class="entry">' + r['Call Logs'] + ' call records</div>');
                                sTmp.push('<div class="entry">' + r['Call Time Contacts'] + ' call time contact</div>');
                            }
                            sTmp.push('<br><br>');

                            sTmp.push('<div class="boldRed">This cannot be undone.</div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>')
                sTmp.push('</div>');

                 var   $deleteContactWarning = $('body').revupDialogBox({
                   cancelBtnText:      'Cancel',
                   cancelHandler:      function(){
                                            $deleteContactWarning.revupDialogBox('close');
                                       },
                   okBtnText:          'OK',
                   okHandler:          function () {
                                           doDeleteContact(contactId);
                                       },
                   headerText:         'Delete Contacts',
                   msg:                sTmp.join(''),
                   top:                175,
                   zIndex:             200,
                   width:              '330px',
                   height:             '320px',
                   isDragable:         true,
                 });
            })
            .fail(function(r) {
                detailId = -1;
                console.error('Unable to delete: ', r);
                displayErrorWarning('error', 'Unable to delete Contact at this time.');
            })
         }
         else {
             console.error('Unable to load Contact Details: ', r);
         }
     }

    let removeAndFetchReplacement = (contactId) => {
        // get the entry to remove
        let $removeEntry = $('.entry[contactId="' + contactId + '"]', $editContactDlg);
        if ($removeEntry.length > 0) {
            //remove from the ranking list
            $removeEntry.remove();
        }

        // broadcast contact being deleted
        let contactName = $('.nameContainer', $removeEntry).html();
        contactName = contactName.replace(' (Being Deleted) ', '');
        $.event.trigger({
            type: 'deleteContact',
            contactId: contactId,
            contactName: contactName,
        });

        // see if the load more marker is visible
        let singleEntry = false;
        let $timeToAddMore = $('.entry.timeToAddMore', $editContactDlg);
        if ($timeToAddMore.length > 0) {
            singleEntry = true;
        }

        // update the counts
        let numEntries = parseInt($('.leftSide .lstDiv .contactLstDiv', $editContactDlg).attr('numEntries'), 10);
        let numLoaded = parseInt($('.leftSide .lstDiv .contactLstDiv', $editContactDlg).attr('numEntriesLoaded'), 10);


        $('.leftSide .lstDiv .contactLstDiv', $editContactDlg).attr('numEntries', numEntries - 1);
        $('.leftSide .lstDiv .contactLstDiv', $editContactDlg).attr('numEntriesLoaded', numLoaded - 1);
        $('.leftSide .lstDiv .contactLstDiv', $editContactDlg).attr('maxPages', Math.ceil((numEntries - 1) / pageSize));

        // see if a replacement needs to be loaded
        if (numLoaded < numEntries) {
            // fetch a single entry and append to the end
            fetchNextChunk(singleEntry);
        }
    } //removeAndFetchReplacement

    let doDeleteContact = (contactId) => {
        let $entry = $('.entry[contactId="' + contactId + '"]', $editContactDlg);
        let $deleteContactBtn = $('.editContactDlg .leftSide .newSearchDiv .left deleteContactBtn');

        // if being delete don't try again
        if ($entry.hasClass('beingDeleted')) {
            return;
        }

        $entry.removeClass('selected').addClass('beingDeleted');
        $('.nameContainer', $entry).html($('.nameContainer', $entry).html() + ' (Being Deleted) ');

        $deleteContactBtn.addClass('disabled');

        // get next entry
        let $nextEntry = $entry.next();

        let bLoadDetails = false;

        while (true) {
            if (($nextEntry.length == 0) || $nextEntry.hasClass('loadingMore')) {
                break;
            }

            // if it is beingDeleted then skip to the next
            if ($nextEntry.hasClass('beingDeleted')) {
                $nextEntry = $nextEntry.next();

                continue;
            }

            // select the next entry if there is one
            $nextEntry.addClass('selected');

            // load the details
            bLoadDetails = true;

            // done
            break;
        }

        // select the next entry if there is one
        if (bLoadDetails) {
            $nextEntry.addClass('selected');

            // load the details
            let newContactId = $nextEntry.attr('contactId');
            displayContactDetails(newContactId);
        }
        else {
            // nothing selected
            $('.rightSide', $editContactDlg).hide();
        }

        let url = userContactsURL;
        let currentlyActive = $('.togglePages .editTab.active');
        if(currentlyActive.hasClass("accountTab")){
            url = accountContactsURL;
        }
        url += contactId;

        // display the loading spinner
        $('.deleteContactWarning .waitingDiv', ).show();

        // do the delete
var startTime = new Date().getTime();
        var deleteContactId = contactId;
        $.ajax({
            url: url,
            type: 'DELETE',
        })
        .done(function(r) {
console.log('editContact - delete time(done): ' +  (new Date().getTime() - startTime) + ' ms')
            // remove the entry and fetch a replacement
            removeAndFetchReplacement(deleteContactId);

            // close the dialog
            $deleteContactWarning.revupDialogBox('close');
        })
        .fail(function(r) {
console.error('editContact - Unable to delete contact: ' + deleteContactId + ", reasult: ", r);
            revupUtils.apiError('Delete Contact', r, 'Unable to delete contact: ' + contactId);
        })
        .always(function() {
console.log('editContact - delete time(always): ' +  (new Date().getTime() - startTime) + ' ms')
        })
    } // doDeleteContact



    function displayContactDetails(contactId)
    {
        $('.deleteContactBtn', $editContactDlg).addClass('disabled');
        // clear error warning messages
        $('.rightSide').show();
        hideErrorWarning();

        // if new contact then clear
        if (contactId == -1) {
            resetDetails($editContactDlg);
            return
        }

        //get contact display contact display
        var currentlyActive = $('.togglePages .editTab.active');

        if(currentlyActive.hasClass("accountTab")){
            url = accountContactsURL;
        }
        else if(currentlyActive.hasClass("personalTab")){
            url = userContactsURL;
        }
        url += contactId + '/';

        $.ajax({
            url: url,
            type: "GET",
        })
        .done(function(r) {
            // make sure the edit contact fields are displayed
            $('.nameSection', $editContactDlg).show();
            $('.scrollSection', $editContactDlg).show();
            $('.bottomButtons', $editContactDlg).show();

            $('.deleteContactBtn', $editContactDlg).removeClass('disabled');
            $('.editContactDlg .rightSide .loadingDiv').hide();
            // save id
            detailId = r.id;

            // name section
            var val = "";
            if (r.first_name) {
                val = r.first_name;
            }
            else {
                val = "";
            }
            $firstName.val(val);

            if (r.additional_name) {
                val = r.additional_name;
            }
            else {
                val = "";
            }
            $middleName.val(val);

            if (r.last_name) {
                val = r.last_name;
            }
            else {
                val = "";
            }
            $lastName.val(val);

            // mske sure the first, middle, and last name fields are readonly
            $firstName.prop('readonly', true);
            $firstNameLabel.text('First Name');
            $middleName.prop('readonly', true);
            $lastName.prop('readonly', true);
            $lastNameLabel.text('Last Name');

            //email & phone number
            displayContactEmail($editContactDlg, r.email_addresses);
            displayContactAddress($editContactDlg, r.addresses);
            displayContactRegion($editContactDlg, r.locations);
            displayContactPhone($editContactDlg, r.phone_numbers);

            // current edit contact
            currentEditContact = r;
        })
        .fail(function(r) {
            $('.editContactDlg .rightSide .loadingDiv').hide();

            detailId = -1;
            console.error('Unable to load Contact Details: ', r);
            if (r.status == 404) {
                // hide the edit fields
                $('.nameSection', $editContactDlg).hide();
                $('.scrollSection', $editContactDlg).hide();
                $('.bottomButtons', $editContactDlg).hide();

                displayErrorWarning('error', 'Unable to load Contact Details.  This contact may have been deleted by another user.', true);
            }
            else {
                displayErrorWarning('error', 'Unable to load Contact Details at this time.');
            }
        });
    } // displayContactDetails

    function displayContactList(pageNum, bDisplayPagination, addType, contactId, contactName)
    {
        var $contactLstDiv = $('.leftSide .lstDiv .contactLstDiv', $editContactDlg);
        var $newEntry = $('.entryInProgress');
        loadingCompleted = false;

        //disable the Save and Cancel button before the list of contacts has returned
        if(!loadingCompleted){
            $('.editSaveBtn', $editContactDlg).prop('disabled', false).addClass('disabled');
            $('.editCancelBtn', $editContactDlg).attr('disabled', 'true');
            $('.deleteContactBtn', $editContactDlg).addClass('disabled');
        }

       if(!loadingCompleted && $newEntry.is(":visible")){
           $('.entryInProgress .statusCol .deleteBtn').hide();
       }

        if(addType == 'addNewContact'){
            var $newButton = $('.editContactDlg .leftSide .newSearchDiv .left .newContactBtn');
            // var $editContactDlg = $('.editContactDlg');
            newContact($newButton, $editContactDlg);
        }

        var url = userContactsURL;
        var currentlyActive = $('.togglePages .editTab.active');
        if(currentlyActive.hasClass("accountTab")){
            url = accountContactsURL;
        }

        // hide spinner and display listit
        $('.contactLstDiv', $editContactDlg).show();
        $('.loadingContacts', $editContactDlg).show();

        // see if there is a search value
        let searchFor;
        if (contactName != null) {
            searchFor = contactName
        }
        else {
            searchFor = $('.contactSearch', $editContactDlg).revupSearchBox('getValue');
        }

        // build the url
        var a = [];
        a.push('page_size=' + pageSize);
        a.push('page=1');
        if (searchFor) {
            a.push('search=' + searchFor);
        }
        if (a.length > 0) {
            url += "?" + a.join("&");
        }

        // fetch the data
        $.ajax({
            url: url,
            type: 'GET',
        })
        .done(function(r) {
            var $newContactBtn = $('.newContactBtn', $editContactDlg);
            var inputErr = ($('.rightSide .inputError', $editContactDlg)).length;

            $('.entry.loadingMore', $editContactDlg).remove();

            //enabling the Save and Cancel button when the api returns
            loadingCompleted = true;

            //api has returned but in the middle of adding a new contact
           if(loadingCompleted && $newEntry.is(":visible")){
                $('.entryInProgress .statusCol').show();
                $('.entryInProgress .statusCol .deleteBtn').css('display', 'block');
                $('.editCancelBtn', $editContactDlg).attr('disabled', false);
                if(inputErr == 0){
                    $('.editSaveBtn', $editContactDlg).prop('disabled', false).removeClass('disabled');
                }
           }
           //api has returned but not adding a new entry, only display the details of the 1st contacts
           if(loadingCompleted && $newEntry.is(":hidden")){
               $('.editCancelBtn', $editContactDlg).attr('disabled', 'true');
           }

            // update the count
            $('.contactCount', $editContactDlg).text(revupUtils.commify(r.count));

            // make sure contacts have been found
            if (r.count === 0) {
                var sTmp = [];
                sTmp.push('<div class="noContactsFoundDiv">');
                    if (searchFor) {
                        sTmp.push('<div class="msg">No Contact Found matching this search</div>');
                    }
                    else {
                        sTmp.push('<div class="msg">No Contact Found</div>');
                    }
                sTmp.push('</div>');
                $('.lstDiv .contactLst', $editContactDlg).html(sTmp.join(''));

                // hide the pageination
                $('.lstDiv .contactLstFooter', $editContactDlg).hide();

                // hide spinner and display list
                $('.loadingContacts', $editContactDlg).hide(1);
                $('.contactLstDiv', $editContactDlg).show(1);
                $('.rightSide', $editContactDlg).hide();

                $contactLstDiv.attr('maxpages', 0);
                $contactLstDiv.attr('numentries', 0);
                $contactLstDiv.attr('numentriesloaded', 0);
                $contactLstDiv.attr('currentPage', pageNum);
                return;
            }
            else{
                $contactLstDiv.attr('maxpages',  Math.ceil(r.count / pageSize));
                $contactLstDiv.attr('numentries', r.count);
                $contactLstDiv.attr('numentriesloaded', pageSize);
                $contactLstDiv.attr('currentPage', pageNum);

                // add the contacts to the list
                var contacts = r.results;
                var sTmp = [];
                for (var i = 0; i < contacts.length; i++) {
                    var extraClass = "";
                    if ((i === 0) && ($newEntry.is(":hidden")) && pageNum <= 1) {
                        extraClass = " selected";
                    }

                    if(i == nextPageMarkerIndex){
                        sTmp.push('<div class="entry timeToAddMore ' + extraClass + '" contactId="' + contacts[i].id + '">');
                    }
                    else{
                        sTmp.push('<div class="entry' + extraClass + '" contactId="' + contacts[i].id + '">');
                    }

                    // name
                    var name = "";
                    if (contacts[i].first_name) {
                        name += contacts[i].first_name;
                    }
                    if (contacts[i].last_name) {
                        if (name !== '') {
                            name += ' ';
                        }
                        name += contacts[i].last_name;
                        if( i == nextPageMarkerIndex){
                        }
                    }
                    sTmp.push('<div class="column nameCol"><div class="nameContainer">' + name + '</div></div>');

                    // email address
                    var email = '-';
                    if (contacts[i].email_addresses.length > 0) {
                        email = contacts[i].email_addresses[0].address;
                    }
                    if (email == "-") {
                        sTmp.push('<div class="column emailCol noEmailAddr"><div class="emailContainer">' + email + '</div></div>');
                    }
                    else {
                        sTmp.push('<div class="column emailCol">');
                            sTmp.push('<div class="emailContainer">');
                                //sTmp.push('<a href="mailto:' + email + '">'+ email + '</a>');
                                sTmp.push(email);
                            sTmp.push('</div>');
                        sTmp.push('</div>');
                    }

                    sTmp.push('<div class="column statusCol">');
                        sTmp.push('<div class="saving">Saving</div>');
                        sTmp.push('<div class="errorSaving">Error</div>');
                        sTmp.push('<div class="deleteBtn icon icon-delete-circle"></div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                }
                if (r.count > pageSize) {
                    sTmp.push('<div class="entry loadingMore" style="display:none">');
                        sTmp.push('<div class="entry loadingMore"></div>');
                }

                sTmp.push('</div>');
            }

            // replace list
            $('.lstDiv .contactLst', $editContactDlg).html(sTmp.join(''));

            //disable all the other entries in the list during new contact add
            if($newEntry.is(":visible")){
                var selId = -1;
                $('.contactLst .entry:not([contactId=' + selId + '])', $editContactDlg).addClass('disabled').css('opacity', 0.3);
            }
            else{
                var $selectedEntry = $('.entry.selected', $editContactDlg).attr("contactId");
                displayContactDetails($selectedEntry);
            }

            // disable the edit flags
            bEditFieldChanged = false;
            bEditRequired = false;

            //need to set the pre-edited email here, in case user edit then cancel edit on the first entry without selecting it first
            preEdit = ($('.entry.selected')[0].childNodes[1].innerText);
        })
        .fail(function(r) {
            console.error('Unable to load contact list: ', r);
            displayErrorWarning('error', 'Unable to load the contact list at this time.');
        })
        .always(function(r) {
            $('.loadingContacts', $editContactDlg).hide();
        });
    } // displayContactList

    function fetchNextChunk(singleEntry){
        var $newEntry = $('.entryInProgress');
        var url = userContactsURL;
        var currentlyActive = $('.togglePages .editTab.active');
        if(currentlyActive.hasClass("accountTab")){
            url = accountContactsURL;
        }

        // build the url
        var a = [];
        if(singleEntry){
            a.push('page_size=1');
            let page = parseInt($('.leftSide .lstDiv .contactLstDiv').attr('numEntriesLoaded'), 10) + 1;
            a.push('page=' + page);
        }
        else {
            a.push('page_size=' + pageSize);
            let nextPage = parseInt($('.leftSide .lstDiv .contactLstDiv').attr('currentPage'), 10) + 1;
            a.push("page=" +  nextPage);
        }

        if (a.length > 0) {
            url += "?" + a.join("&");
        }

        // fetch the data
        $.ajax({
            url: url,
            type: 'GET',
        })
        .done(function(r) {
            $('.leftSide .lstDiv .contactLstDiv', $editContactDlg).attr('maxpages',  Math.ceil(r.count / pageSize));
            $('.leftSide .lstDiv .contactLstDiv', $editContactDlg).attr('numentries', r.count);

            let numLoaded = parseInt($('.leftSide .lstDiv .contactLstDiv', $editContactDlg).attr('numEntriesLoaded'), 10) + r.results.length;
            $('.leftSide .lstDiv .contactLstDiv', $editContactDlg).attr('numEntriesLoaded', numLoaded);

            if(!singleEntry){
                let currentPage = parseInt($('.leftSide .lstDiv .contactLstDiv').attr('currentPage'), 10);
                $('.leftSide .lstDiv .contactLstDiv', $editContactDlg).attr('currentPage', currentPage + 1);
            }

            $('.entry.loadingMore', $editContactDlg).remove();

            // update the count
            $('.contactCount', $editContactDlg).text(revupUtils.commify(r.count));

            // add the contacts to the list
            var contacts = r.results;
            var sTmp = [];
            for (var i = 0; i < contacts.length; i++) {
                var extraClass = "";
                if(i == nextPageMarkerIndex){
                    sTmp.push('<div class="entry timeToAddMore ' + extraClass + '" contactId="' + contacts[i].id + '">');
                }
                else{
                    sTmp.push('<div class="entry' + extraClass + '" contactId="' + contacts[i].id + '">');
                }
                // name
                var name = "";
                if (contacts[i].first_name) {
                    name += contacts[i].first_name;
                }
                if (contacts[i].last_name) {
                    if (name !== '') {
                        name += ' ';
                    }
                    name += contacts[i].last_name;
                    if( i == nextPageMarkerIndex){
                    }
                }
                sTmp.push('<div class="column nameCol"><div class="nameContainer">' + name + '</div></div>');

                // email address
                var email = '-';
                if (contacts[i].email_addresses.length > 0) {
                    email = contacts[i].email_addresses[0].address;
                }
                if (email == "-") {
                    sTmp.push('<div class="column emailCol noEmailAddr"><div class="emailContainer">' + email + '</div></div>');
                }
                else {
                    sTmp.push('<div class="column emailCol">');
                        sTmp.push('<div class="emailContainer">');
                            //sTmp.push('<a href="mailto:' + email + '">'+ email + '</a>');
                            sTmp.push(email);
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                }

                sTmp.push('<div class="column statusCol">');
                    sTmp.push('<div class="saving">Saving</div>');
                    sTmp.push('<div class="errorSaving">Error</div>');
                    sTmp.push('<div class="deleteBtn icon icon-delete-circle"></div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            }
            if (r.count > pageSize) {
                sTmp.push('<div class="entry loadingMore" style="display:none">');
                    sTmp.push('<div class="entry loadingMore"></div>');
            }

            sTmp.push('</div>');

            //append to the end of page 1
            $('.lstDiv .contactLst', $editContactDlg).append(sTmp.join(''));

            if($newEntry.is(":visible")){
                var selId = -1;
                $('.contactLst .entry:not([contactId=' + selId + '])', $editContactDlg).addClass('disabled').css('opacity', 0.3);
            }
        })
        .fail(function(r) {
            console.error('Unable to load contact list: ', r);
            displayErrorWarning('error', 'Unable to load the contact list at this time.');
        });
    }//fetchNextChunk

    function displayEditContact(addType, activeTab, contactId, contactName)
    {
        var sTmp = [];
        sTmp.push('<div class="editContactDlg">');
            sTmp.push('<div class="togglePages">');
                let personalTabExtraClass = '';
                let accountTabExtraClass = '';
                if (activeTab == 'personalTab') {
                    personalTabExtraClass = ' active';
                }
                else if (activeTab == 'accountTab') {
                    accountTabExtraClass = ' active';
                }
                sTmp.push('<div class="editTab personalTab' + personalTabExtraClass + '">Personal Contacts </div>');
                if (isAdmin == 'True') {
                    sTmp.push('<div class="editTab accountTab' + accountTabExtraClass + '">Shared Account Contacts</div>');
                }
            sTmp.push('</div>');

            sTmp.push('<div class="leftSide">');
                sTmp.push('<div class="numContactsDiv">');
                    sTmp.push('<div class="right">');
                        sTmp.push('<div class="contactCount">123,456</div>');
                        sTmp.push('<div class="txt">Contacts</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');

                sTmp.push('<div class="newSearchDiv">');
                    sTmp.push('<div class="left">');
                        sTmp.push('<div class="iconBtnDiv">');
                            sTmp.push('<button class="btnDiv newContactBtn">');
                                sTmp.push('<div class="txt">New</div>');
                                sTmp.push('<div class="icon icon-raiser"></div>');
                            sTmp.push('</button>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="sepBar">&nbsp;</div>');
                        sTmp.push('<div class="iconBtnDiv">');
                            sTmp.push('<button class="btnDiv deleteContactBtn disabled">');
                                sTmp.push('<div class="txt">Delete</div>');
                                sTmp.push('<div class="icon icon-delete-contact"></div>');
                            sTmp.push('</button>');
                        sTmp.push('<div class="sepBar">&nbsp;</div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');//end of left

                    sTmp.push('<div class="right">');
                        sTmp.push('<div class="contactSearch"></div>');
                    sTmp.push('</div>');
                    sTmp.push('</div>');//end of right

                    sTmp.push('<div class="lstDiv">');
                        sTmp.push('<div class="header">');
                            /*
                            sTmp.push('<div class="column removeCol">Remove</div>');
                            */
                        sTmp.push('<div class="column nameCol">Contact Name</div>');
                        sTmp.push('<div class="column emailCol">Email Address</div>');
                    sTmp.push('</div>');

                    if(addType == 'addNewContact'){
                        sTmp.push('<div class="entry selected entryInProgress" contactId="-1" style="display: block; width: 549px;">');
                    }
                    else {
                        sTmp.push('<div class="entry entryInProgress" contactId="-1" style="display: none; width: 549px">');
                    }

              sTmp.push('<div class="column nameCol"><div class="nameContainer" style="color: ' + revupConstants.color.primaryGray4 + '">New Contact</div></div>');
                sTmp.push('<div class="column emailCol noEmailAddr"><div class="emailContainer">-</div></div>');
                    sTmp.push('<div class="column statusCol">');
                        sTmp.push('<div class="saving">Saving</div>');
                        sTmp.push('<div class="errorSaving">Error</div>');
                        sTmp.push('<div class="deleteBtn icon icon-delete-circle" style="display:block"></div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');

                 var $newButton = $('.editContactDlg .leftSide .newSearchDiv .left .newContactBtn');

                 sTmp.push('<div class="loadingContacts" style="height:387px;width:100%;display:none">');
                     sTmp.push('<div class="loadingDiv">');
                         sTmp.push('<div class="loading"></div>');
                         sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                     sTmp.push('</div>');
                 sTmp.push('</div>');
                 sTmp.push('<div class="contactLstDiv" currentpage="0" maxpages numEntries numEntriesLoaded style="display:none;">');
                     sTmp.push('<div class="contactLst"></div>');
                 sTmp.push('</div>');
                 sTmp.push('<div class="contactLstFooter"></div>');
             sTmp.push('</div>');
         sTmp.push('</div>');

            if (addType == 'editContacts' || addType == 'editAContact') {
               //if "Edit Contacts"
                sTmp.push('<div class="rightSide" xstyle="position: absolute; left: 600px;">');
                    sTmp.push('<div class="loadingDiv" style="position: absolute; background-color: white; z-index: 2; width: 410px; height: 500px; bottom: -40px; right: -42px; opacity: 0.99; margin-top: 0">');
                        sTmp.push('<div style="position: relative; top: 100px">');
                            sTmp.push('<div class="loading" nstyle="xmargin-left:5px;width:25px;height:25px;"></div>');
                            sTmp.push('<img class="loadingImg" src="/static/img/loadingCenterIcon.svg">');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="sectionHeader">Details</div>');
            }
            else if(addType == 'addNewContact'){
                //if "Add A Contact"
                sTmp.push('<div class="rightSide">');
                    sTmp.push('<div class="sectionHeader">Details</div>');
            }
                sTmp.push('<div class="warningErrorDiv warning">');
                    sTmp.push('<div class="left">');
                        sTmp.push('<div class="center">');
                            sTmp.push('<div class="iconDiv">')
                                sTmp.push('<div class="icon icon-warning"></div>');
                            sTmp.push('</div>');
                            sTmp.push('<div class="msg">Warning Will Robinson</div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="right">');
                        sTmp.push('<div class="closeBtn icon icon-close"></div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
                sTmp.push('<div class="section nameSection">');
                    sTmp.push('<div class="line sepLine">');
                         sTmp.push('<div class="leftSide">');
                             sTmp.push('<div class="text firstNameLabel">*First Name</div>');
                         sTmp.push('</div>');
                         sTmp.push('<div class="rightSide">');
                             sTmp.push('<div class="text">Middle Name or Initial</div>');
                         sTmp.push('</div>');
                     sTmp.push('</div>');
                     sTmp.push('<div class="line">');
                         sTmp.push('<div class="leftSide">');
                             sTmp.push('<input class="firstName" placeholder="First Name">');
                         sTmp.push('</div>');
                         sTmp.push('<div class="rightSide">');
                             sTmp.push('<input class="middleName" placeholder="Middle Name">');
                         sTmp.push('</div>');
                     sTmp.push('</div>');
                     sTmp.push('<div class="line sepLine">');
                         sTmp.push('<div class="text lastNameLabel">*Last Name</div>');
                     sTmp.push('</div>');
                     sTmp.push('<div class="line lastLine">');
                         sTmp.push('<input class="lastName" placeholder="Last Name">');
                     sTmp.push('</div>');
                sTmp.push('</div>');
                sTmp.push('<div class="scrollSection">');
                     sTmp.push('<div class="scrollContainer">');
                         // Email
                         sTmp.push('<div class="section emailSection">');
                             sTmp.push('<div class="line sepLine">');
                                 sTmp.push('<div class="text">Email Address</div>');
                             sTmp.push('</div>');
                             sTmp.push('<div class="line emailEntry">');
                                 sTmp.push('<input class="emailAddress deletable" placeholder="Email Address">');
                                 sTmp.push('<div class="deleteEmailBtn icon icon-delete-circle"></div>');
                             sTmp.push('</div>');
                             sTmp.push('<div class="line btnSepLine">');
                                 sTmp.push('<div class="addBtn addEmailBtn toTheRight">');
                                     sTmp.push('<div class="icon icon-add-filter"></div>');
                                     sTmp.push('<div class="btnText">Add Email</div>');
                                 sTmp.push('</div>');
                             sTmp.push('</div>');
                         sTmp.push('</div>');
                         // Address
                         sTmp.push('<div class="section addressSection">');
                             sTmp.push('<div class="subSection grayBackground addrGrp" addrNum="1">');
                                 sTmp.push('<div class="line">');
                                     sTmp.push('<div class="header left">Address 1</div>');
                                     sTmp.push('<div class="right deleteAddrBtn icon icon-delete-circle"></div>');
                                 sTmp.push('</div>');
                                 sTmp.push('<div class="line">');
                                     sTmp.push('<div class="text">Address</div>');
                                 sTmp.push('</div>');
                                 sTmp.push('<div class="line">');
                                     sTmp.push('<input class="addr1" placeholder="Address">');
                                 sTmp.push('</div>');
                                 sTmp.push('<div class="line sepLine">');
                                     sTmp.push('<div class="text">Address continued</div>');
                                 sTmp.push('</div>');
                                 sTmp.push('<div class="line">');
                                     sTmp.push('<input class="addr2" placeholder="Address">');
                                 sTmp.push('</div>');
                                 sTmp.push('<div class="line sepLine">');
                                     sTmp.push('<div class="text cityAddr">City</div>');
                                     sTmp.push('<div class="text stateAddr">State</div>');
                                     sTmp.push('<div class="text postalCodeAddr">Zip Code</div>');
                                 sTmp.push('</div>');
                                 sTmp.push('<div class="line">');
                                     sTmp.push('<input class="city cityAddr" placeholder="City">');
                                     sTmp.push('<div class="stateDropDown stateAddr"></div>');
                                     sTmp.push('<input class="postalCode postalCodeAddr" placeholder="Zip">');
                                 sTmp.push('</div>');
                             sTmp.push('</div>');
                             sTmp.push('<div class="line btnSepLine">');
                                 sTmp.push('<div class="addBtn addAddrBtn toTheRight">');
                                     sTmp.push('<div class="icon icon-add-filter"></div>');
                                     sTmp.push('<div class="btnText">Add Address</div>');
                                 sTmp.push('</div>');
                             sTmp.push('</div>');
                         sTmp.push('</div>');
                         // Region
                         sTmp.push('<div class="section regionSection">');
                             sTmp.push('<div class="line sepLine">');
                                 sTmp.push('<div class="text">Region</div>');
                             sTmp.push('</div>');
                             sTmp.push('<div class="line locationEntry">');
                                 sTmp.push('<input class="location" readonly placeholder="Location">');
                             sTmp.push('</div>');
                         sTmp.push('</div>');
                         // Phone Number
                         sTmp.push('<div class="section phoneSection">');
                             sTmp.push('<div class="line sepLine">');
                                 sTmp.push('<div class="text">Phone Number</div>');
                             sTmp.push('</div>');
                             sTmp.push('<div class="line phoneEntry">');
                                 sTmp.push('<input class="phoneNumber deletable" placeholder="XXX-XXX-XXXX">');
                                 sTmp.push('<div class="deletePhoneBtn icon icon-delete-circle"></div>');
                             sTmp.push('</div>');
                             sTmp.push('<div class="line btnSepLine">');
                                 sTmp.push('<div class="addBtn addPhoneBtn toTheRight">');
                                     sTmp.push('<div class="icon icon-add-filter"></div>');
                                     sTmp.push('<div class="btnText">Add Phone Number</div>');
                                 sTmp.push('</div>');
                             sTmp.push('</div>');
                         sTmp.push('</div>');
                     sTmp.push('</div>');
                sTmp.push('</div>');
                sTmp.push('<div class="bottomButtons">');

                if(addType == 'addNewContact') {
                     sTmp.push('<div class="toTheLeft">');
                         sTmp.push('<div class="text">Required Fields</div>');
                     sTmp.push('</div>');
                }

                else {
                     sTmp.push('<div class="toTheLeft">');
                         sTmp.push('<div class="text" style="display: none;">Required Fields</div>');
                     sTmp.push('</div>');
                }

                     sTmp.push('<div class="toTheRight">');
                         sTmp.push('<div class="editCancelBtn" disabled>Cancel</div>');
                         sTmp.push('<button class="revupBtnDefault secondary btnSmall editSaveBtn" disabled>Save</button>');
                     sTmp.push('</div>');
                sTmp.push('</div>');
             sTmp.push('</div>');
         sTmp.push('</div>');

        $editContactDlg = $('body').revupDialogBox({
            okBtnText:          'Close',
            cancelBtnText:      '',
            autoCloseOk:        false,
            headerText:         'Edit Contacts',
            msg:                sTmp.join(''),
            top:                8,
            extraClass:         'editContact',
            zIndex:             50,
            width:              '1024px',
            height:             '650px',
            isDragable:         false,
        });

        // attach the search box
        $('.contactSearch', $editContactDlg)
            .revupSearchBox({})
            .on('revup.searchFor', function(e) {
                displayContactList(0, true);
            })
            .on('revup.searchClear', function(e) {
                displayContactList(1, true);
            });

        // contact list - entry
        $('.contactLst', $editContactDlg)
            .on('click', '.entry', function(e) {
                var $entry = $(this);
                preEdit = $entry[0].childNodes[1].innerText;
                // if already selected than done
                if ($entry.hasClass('selected')) {
                    return;
                }

                // if in the process of saving then cannot select again or disabled
                if (($entry.hasClass('disabled')) || ($entry.hasClass('saving'))) {
                    return;
                }

                // clear selected
                $('.contactLst .entry', $editContactDlg).removeClass('selected');

                // select new entry
                $entry.addClass('selected');
                $selectedContactName = $('.lstDiv .entry.selected .nameContainer', $editContactDlg);
                $selectedContactEmailCol = $('.lstDiv .entry.selected .emailCol', $editContactDlg);
                $selectedContactEmail = $('.lstDiv .entry.selected .emailContainer', $editContactDlg);

                // load the selected field details
                var cId = $entry.attr("contactId");
                displayContactDetails(cId);
            })

            $('.leftSide .lstDiv .contactLstDiv', $editContactDlg)
                .on('scroll', function(e) {
                    let $timeToAddMore = $('.entry.timeToAddMore', $editContactDlg);
                    let $contactLst = $('.leftSide .lstDiv .contactLstDiv', $editContactDlg);
                    if($timeToAddMore.length > 0){
                        if($timeToAddMore.visible($contactLst)){
                            $('.entry.loadingMore', $editContactDlg).show();
                            $timeToAddMore.removeClass('timeToAddMore');
                            fetchNextChunk();
                        }
                    }
                })
                .on('mousewheel', function(e) {
                    let $contactLst = $('.leftSide .lstDiv .contactLstDiv', $editContactDlg);
                    if($contactLst.prop('scrollHeight') - $contactLst.scrollTop() <= $contactLst.height() && (e.originalEvent.wheelDelta < 0)) {
                        e.preventDefault()
                    }
                })


            $('.deleteBtn', $editContactDlg).on('click', function (e) {
                cancelNewContact(false);
            });

        // add a new contact
        $('.newContactBtn', $editContactDlg).on('click', function(e) {
          $('.rightSide').show();
            newContact($(this), $editContactDlg);
        });

        // delete a contact
        $('.deleteContactBtn', $editContactDlg).on('click', function(e) {
console.log('deleteContactBtn - editContact')
            $('.rightSide').show();
            deleteContact($(this), $editContactDlg);

            //stop propagation
            e.stopPropagation();

            return false;
        });

        // states
        $('.addrGrp .stateDropDown', $editContactDlg).revupDropDownField({
            value:              revupConstants.dropDownStates,
            valueStartIndex:    -1,
            sortList:           true,
            bDisplayValue:      true,
            ifStartIndexNeg1Msg: "State",
            bAutoDropDownWidth: false,
            changeFunc:         function(i, v) {
                                        editEnableDisableSave(true);
                                    }
        });

        // save/cancel buttons
        $('.editCancelBtn, .okBtn', $editContactDlg).on('click', function (e) {
            var closingDlg = false;
            if($(this).hasClass('okBtn')){
                closingDlg = true;
            }
            else if(loadingCompleted){
                //once the user has clicked cancel the current new user add, decide whether we will be closing the dlg or not
                closingDlg = false;
            }
            cancelNewContact(closingDlg);
        });

        function cancelNewContact(closingDlg){
            // see if disabled
            if ($(this).attr('disabled')) {
                return;
            }

            //if user is add new contact mode and all fields are filled, show user the warning dialog
            var $anyInput = $('.rightSide input', $editContactDlg);
            var itsEmpty = true;
            var $newEntry = ($('.entryInProgress', $editContactDlg).length);
            var emptyFieldsSpaces= $anyInput.filter(function() {
            if(this.value !== ""){
              itsEmpty = false;
            }
            return $.trim(this.value).length === 0;
            }).length > 0;

            if ($('.newContactBtn', $editContactDlg).hasClass('disabled') && !itsEmpty && $newEntry > 0){
                var sTmp = [];
                sTmp.push('<div class="endNewContactWarning">');
                    sTmp.push('<div class="testingWarning">');
                        sTmp.push('<p>You are in the process of creating a new contact that has not been saved. Click "Continue" to continue adding. Or "Cancel" to cancel the creation of the contact.');
                        sTmp.push('</p>');
                    sTmp.push('</div>');
                sTmp.push('</div>');

                var   $endNewContactWarning = $('body').revupDialogBox({
                  okBtnText:          'Cancel',
                  okHandler:          function(){
                                              safelyClosingNewContact(closingDlg);
                                      },
                  cancelBtnText:      'Continue',
                  cancelHandler:      function(){
                                        $endNewContactWarning.revupDialogBox('close');
                                      },
                  headerText:         'Cancel/Create - Confirmation',
                  msg:                sTmp.join(''),
                  top:                175,
                  zIndex:             200,
                  width:              '500px',
                  height:             '190px',
                  isDragable:         true,
                });
            }
            //if not closing the Edit Contacts dlg and fields are empty
            else if(!closingDlg && !itsEmpty){
                safelyClosingNewContact(false);
            }
            else if(!closingDlg && itsEmpty){
                safelyClosingNewContact(false);
            }
            //if closing the Edit Contacts dlg and fields are empty
            else if (closingDlg){
                $editContactDlg.revupDialogBox('close');
            }

            // hide the Required Fields label
            $reqFieldsLabel.hide();
        }//cancelNewContact

        function safelyClosingNewContact(closeDlg) {
            var $newEntry = $('.entryInProgress');

          //if user is doing a search and result is 0
            var foundEntries = ($('.noContactsFoundDiv', $editContactDlg)).length;

            // not closing the dlg, not doing a search or nothing found in search, and not adding a new entry but editing an existing entry
            if(!closeDlg && foundEntries == 0 && $newEntry.is(":hidden")){
                var $selected = $('.entry.selected', $editContactDlg);
                var cId = $selected.attr("contactId");
                $('.emailContainer', $selected).html(preEdit);//set back the pre-edit email vale

                editEnableDisableSave(false, true);
                $('.emailSection .emailEntry .emailAddress', $selected).val(preEdit);
                displayContactDetails(cId);
            }

            // not closing the dlg and there are still entries in the list on the left, the 1st entry needs to displayContactDetails
            if(!closeDlg && foundEntries == 0 && $newEntry.is(":visible")){
                var $selected = $('.contactLst .entry.selected', $editContactDlg);
                var cId = $selected.attr("contactId");

                // remove the new contact entry
                $selected.css('display', 'none');
                resetDetails($editContactDlg);


                $selected.removeClass('selected');
                var $remainError = $('.inputError', $editContactDlg);
                $remainError.removeClass('inputError');

                // select the first entry in the list and load the details
                var $fEntry = $($('.contactLst .entry').get(0));
                $fEntry.addClass('selected');
                cId = $fEntry.attr('contactId');

                // get selected fields
                $selectedContactName = $('.lstDiv .entry.selected .nameContainer', $editContactDlg);
                $selectedContactEmailCol = $('.lstDiv .entry.selected .emailCol', $editContactDlg);
                $selectedContactEmail = $('.lstDiv .entry.selected .emailContainer', $editContactDlg);

                displayContactDetails(cId);

                // reset button and fields
                editEnableDisableSave(false, true);
                resetNewEntry();
            }

            //not closing the dlg and is doing a search
            else if(!closeDlg && foundEntries !== 0){
                $('.contactSearch', $editContactDlg).revupSearchBox('enable');
                $('.rightSide', $editContactDlg).hide();
                resetDetails($editContactDlg);
                $('.entryInProgress', $editContactDlg).hide();
                $('.newContactBtn', $editContactDlg).prop('disabled', false).removeClass('disabled');
            }
            //user just want to close the dlg
            else if(closeDlg){
                $editContactDlg.revupDialogBox('close');
            }
              hideErrorWarning();
            }//safelyClosingNewContact

            $('.editSaveBtn', $editContactDlg).on('click', function(e) {
                saveEdit();
            });

            $(".editTab").click(function () {
                if(! $(this).hasClass("active")){
                    $( ".revupSearchBox .searchText" ).val("");
                    $('.contactLstDiv', $editContactDlg).scrollTop(0);
                }
                $(".editTab").removeClass("active");
                $(this).addClass("active");
                $('.editContactDlg .rightSide .loadingDiv').show();
                displayContactList(1, true);
            });

            // button handlers for  editing
            $('.rightSide', $editContactDlg)
                .on('click', '.addPhoneBtn', function(e) {
                    addEditPhone($editContactDlg);
                })
                .on('click', '.deletePhoneBtn', function(e) {
                    deleteEditPhone($(this), $editContactDlg);
                })

                .on('click', '.addAddrBtn', function(e) {
                    addEditAddress($editContactDlg);
                })
                .on('click', '.deleteAddrBtn', function(e) {
                    deleteEditAddress($(this), $editContactDlg);
                })

                .on('click', '.addEmailBtn', function(e) {
                    addEditEmail($editContactDlg);
                })
                .on('click', '.deleteEmailBtn', function(e) {
                    deleteEditEmail($(this), $editContactDlg);
                })

                .on('keyup', 'input', function(e) {
                    editEnableDisableSave(true);
                })

                .on('keydown keyup', '.firstName, .middleName, .lastName', function(e) {
                    // name the value of the name fields
                    var firstName = $firstName.val();
                    var middleName = $middleName.val();
                    var lastName = $lastName.val();
                    // build the name string
                    var name = [];
                    if (firstName) {
                        name.push(firstName);
                        $selectedContactName.css('color', revupConstants.color.blackText);
                    }
                    if (middleName) {
                        name.push(middleName);
                    }
                    if (lastName) {
                        name.push(lastName);
                    }
                    name = name.join(' ');
                    if (name === '') {
                        name = 'New Contact';
                        $selectedContactName.css('color', revupConstants.color.primaryGray4);
                    }

                    // change the value in the list
                    $selectedContactName.text(name);


                    if($selectedContactName.text == ''){
                      $selectedContactName.css('color', revupConstants.color.primaryGray4);
                    }
                })

                .on('keydown keyup', '.emailSection .emailAddress:first', function(e) {
                    var v = $(this).val();
                    if (v === '') {
                        $selectedContactEmailCol.addClass('noEmailAddr');
                        $selectedContactEmail.text('-');
                    }
                    else {
                        $selectedContactEmailCol.removeClass('noEmailAddr');
                        $selectedContactEmail.text(v);
                    }
                })

                .on('blur', '.firstName, .lastName', function(e) {
                    var firstName = $firstName.val();
                    var lastName = $lastName.val();

                    if ($firstName.is('[readonly]') || $lastName.is('[readonly]')) {
                        return;
                    }

                    isNameUnique(firstName, lastName);
                })

                .on('input', '.emailAddress', function(e) {
                    var $emailAddr = $(this);
                    var emailAddr = $emailAddr.val();
                    var $emailSection = $emailAddr.closest('.emailEntry');
                    var $delBtn = $('.deleteEmailBtn', $emailSection);

                    var isValid = validEmailAddress(emailAddr);
                    if ((isValid) || (emailAddr === '')) {
                        $emailAddr.removeClass('inputError');
                        $delBtn.removeClass('inputError');
                    }
                    else {
                        $emailAddr.addClass('inputError');
                        $delBtn.addClass('inputError');
                    }

                    editEnableDisableSave();
                });


        // close the error/warning div
        $('.warningErrorDiv').on('click', '.closeBtn', function(e) {
            hideErrorWarning(e);
        });

        // add validator
        $('.phoneNumber', $editContactDlg).numericOnly({bPhone: true});
        $('.postalCode', $editContactDlg).numericOnly({bUsZipCode: true});

        //get selectors
        $firstName = $('.nameSection .firstName', $editContactDlg);
        $firstNameLabel = $('.nameSection .firstNameLabel', $editContactDlg);
        $middleName = $('.nameSection .middleName', $editContactDlg);
        $lastName = $('.nameSection .lastName', $editContactDlg);
        $lastNameLabel = $('.nameSection .lastNameLabel', $editContactDlg);
        $emailSection = $('.emailSection', $editContactDlg);
        $addressSection = $('.addressSection', $editContactDlg);
        $regionSection = $('.regionSection', $editContactDlg);
        $phoneSection = $('.phoneSection', $editContactDlg);
        $reqFieldsLabel = $('.toTheLeft .text', $editContactDlg);

        // fetch the data
        if (addType == 'editContacts'){
            displayContactList(1, true);
        }
        else if(addType == 'addNewContact'){
            displayContactList(1, true, 'addNewContact');
        }
        else if (addType == 'editAContact') {
            displayContactList(1, true, 'editAContact', contactId, contactName);
        }
    } // displayEditContact



    return {
        load: function(type = 'editContacts', activeTab = 'personalTab', contactId = null, contactName = null){
            // save the type, id and name if there is one

            displayEditContact(type, activeTab, contactId, contactName);

            // if contactName is defined set the search bar to that name
            if (contactName != null) {
                 $('.contactSearch', $editContactDlg).revupSearchBox('setValue', contactName);
            }
        }

    };
} ());
