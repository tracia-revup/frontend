var listManager = (function () {
    // globals
    var templateData = undefined;   // the template setting

    const entriesPerPage        = 25;        // number of entries per page
    const nextPageMarkerIndex   = Math.round(entriesPerPage * .80);    // where to put marker

    // global selectors
    var filterData = {};

    // list of raisers
    var raiserLst = null;

    // global selectors
    var $detailsNameVal =           $('.detailsDiv .nameEntry .value');
    var $detailsFilterSet =         $('.detailsDiv .filterSetEntry');
    var $detailsFilterSetVal =      $('.value', $detailsFilterSet);
    var $detailsSharedWith =        $('.detailsDiv .sharedWithEntry');
    var $detailsSharedWithVal =     $('.value', $detailsSharedWith);
    var $detailsFilterCountVal =    $('.value', $detailsFilterSet);
    var $topTaskDiv =               $('.topTaskBarDiv');
    var $taskAnalyze =              $('.btnDiv.analyzeBtn', $topTaskDiv);
    var $taskSaveAs =               $('.btnDiv.saveAsBtn', $topTaskDiv);
    var $taskExport =               $('.btnDiv.exportBtn', $topTaskDiv);
    var $taskMerge =                $('.btnDiv.mergeBtn', $topTaskDiv);
    var $taskShare =                $('.btnDiv.shareBtn', $topTaskDiv);
    var $taskCallTimeGrp =          $('.btnDiv.createCallTimeGrpBtn', $topTaskDiv);
    var $taskDelete  =              $('.btnDiv.deleteBtn', $topTaskDiv);
    var $lstLoading =               $('.lstSection .lstLoading');
    var $lstSection =               $('.lstSection .lstDiv');
    var $lstHeader =                $('.lstHeader', $lstSection);
    var $lstBody =                  $('.lstBody', $lstSection);
    var $lstFooter =                $('.lstFooter', $lstSection);
    var $contactCount =             $('.managerWrapper .filterCountEntry .selContactSetCount');
    var $contactCountLoading =      $('.managerWrapper .lstLoading');
    let $lstBodyWrapper =           $('.listManager .lstBodyWrapper');

    var filterData = {};
    var quickFilterIndex = 0;
    var quickFilters = [];
    var requestCount = 0;

    /*
     * Save List As
     */
    function saveAs()
    {
        var sTmp = [];
        sTmp.push('<div class="fieldLabel">List Name</div>');
        sTmp.push('<input class="saveAsName" type="text" placeholder="List Name" autofocus>');

        var $saveAsDlg = $('body').revupDialogBox({
            okBtnText: 'Save',
            headerText: 'Save List As',
            msg: sTmp.join(''),
            extraClass: 'lstMgrDlg saveAsDlg',
            isDragable: false,
            autoCloseOk: false,
            enableOkBtn: false,
            waitingOverlay: true,
            waitingStyleShow: false,
            okHandler: function() {
                // display the waiting
                $saveAsDlg.revupDialogBox('showWaiting');
                $saveAsDlg.revupDialogBox('disableBtn', 'okBtn');
                $saveAsDlg.revupDialogBox('disableBtn', 'cancelBtn');

                var postData = {};
                postData.title = $('.saveAsName', $saveAsDlg).val();
                postData.query = getQueryFilters();

                // get the selected contact sets
                postData.parents = [];
                $('.entry.selected', $lstBody).each(function() {
                    var $e = $(this);
                    postData.parents.push($e.attr('contactSetId'));
                })

                var url = templateData.saveContactSetApi;
                $.ajax({
                    url: url,
                    data: JSON.stringify(postData),
                    type: "POST",
                    traditional: true,
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'text'
                })
                .done(function(r) {
                    var result = JSON.parse(r);

                    // create the new entry
                    var $otherLists = $(".lstOtherLists", $lstBody);
                    $otherLists.prepend(buildLstBodyEntry(result));
                    // Blink the new item so the user sees it
                    $(".entry:first", $otherLists).addClass("blink");
                    // Remove the pulse after 10 seconds so it's not annoying
                    window.setTimeout(function(){
                        $(".entry", $otherLists).removeClass("blink");
                    }, 10000);

                    // close the dialog
                    $saveAsDlg.revupDialogBox('close');

                    // update the attributes
                    let totalContacts = parseInt($lstBodyWrapper.attr('numentries'), 10);
                    let loadedEntries = parseInt($lstBodyWrapper.attr('numentriesloaded'), 10);

                    $lstBodyWrapper.attr('numentries',  totalContacts + 1);
                    $lstBodyWrapper.attr('numentriesloaded', loadedEntries + 1);
                    // there will be more to fetch
                    if($lstBodyWrapper.attr('numentriesloaded') < $lstBodyWrapper.attr('numentries')){
                        $lstBodyWrapper.attr('maxpages',  Math.ceil((totalContacts + 1) / entriesPerPage));
                    }

                    // update footer count
                    $('.showing', $lstFooter).text(parseInt($lstBodyWrapper.attr('numEntriesLoaded')));
                    $('.total',$lstFooter).text(parseInt($lstBodyWrapper.attr('numEntries')));

                    getCount();
                })
                .fail(function(r) {
                    // hide the waiting
                    $saveAsDlg.revupDialogBox('hideWaiting');
                    $saveAsDlg.revupDialogBox('enableBtn', 'okBtn');
                    $saveAsDlg.revupDialogBox('enableBtn', 'cancelBtn');
                    revupUtils.apiError('List Manager - Save As', r, 'Unable to save a new Contact Set');
                })

            },
        })

        $('.saveAsName', $saveAsDlg).on('input', function(e) {
            $input = $(this);

            var v = $input.val();
            if (v.length > 0) {
                $('.okBtn', $saveAsDlg).prop('disabled', false);
            }
            else {
                $('.okBtn', $saveAsDlg).prop('disabled', true);
            }
        })
    } // saveAs

    /*
     * Export as CSV
     */
    function exportCSV()
    {
        var sTmp = [];
        sTmp.push('<div class="fieldLabel">');
            sTmp.push('To export a copy of your list as a CSV file, select the attributes you want ');
            sTmp.push('included in the file and click save.  The contact\'s name is always included by default.');
        sTmp.push('</div>');
        sTmp.push('<div class="formatOptionsDiv">');
            sTmp.push('<div class="title">Format options</div>');
            sTmp.push('<div class="entry" field="name">');
                sTmp.push('<div class="checkField disabled">');
                    sTmp.push('<div class="checkbox checked">');
                        sTmp.push('<div class="icon icon-check"></div>');
                        sTmp.push('<div class="icon icon-box-unchecked"></div>');
                        sTmp.push('<div class="icon icon-check-box-back"></div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="checkboxLabel">Name</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');
            sTmp.push('<div class="entry" field="score">');
                sTmp.push('<div class="checkField">');
                    sTmp.push('<div class="checkbox checked">');
                        sTmp.push('<div class="icon icon-check"></div>');
                        sTmp.push('<div class="icon icon-box-unchecked"></div>');
                        sTmp.push('<div class="icon icon-check-box-back"></div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="checkboxLabel">Ranking score</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');
            sTmp.push('<div class="entry" field="contactInfo">');
                sTmp.push('<div class="checkField">');
                    sTmp.push('<div class="checkbox checked">');
                        sTmp.push('<div class="icon icon-check"></div>');
                        sTmp.push('<div class="icon icon-box-unchecked"></div>');
                        sTmp.push('<div class="icon icon-check-box-back"></div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="checkboxLabel">Contact information</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');
            sTmp.push('<div class="entry" field="totalDonation">');
                sTmp.push('<div class="checkField">');
                    sTmp.push('<div class="checkbox checked">');
                        sTmp.push('<div class="icon icon-check"></div>');
                        sTmp.push('<div class="icon icon-box-unchecked"></div>');
                        sTmp.push('<div class="icon icon-check-box-back"></div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="checkboxLabel">Total donation amount</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        var $exportCSVDlg = $('body').revupDialogBox({
            okBtnText: 'Save',
            headerText: 'Export as a CSV File',
            msg: sTmp.join(''),
            extraClass: 'lstMgrDlg exportCSVDlg',
            isDragable: false,
            autoCloseOk: false,
            waitingOverlay: true,
            waitingStyleShow: false,
            //enableOkBtn: false,
            okHandler: function() {
                // display the waiting
                $exportCSVDlg.revupDialogBox('showWaiting');
                $exportCSVDlg.revupDialogBox('disableBtn', 'okBtn');
                $exportCSVDlg.revupDialogBox('disableBtn', 'cancelBtn');

                // build the arguments
                var urlArgs = [];
                urlArgs.push('show_name=' + $('.entry[field=name] .checkbox', $exportCSVDlg).hasClass('checked'));
                urlArgs.push('show_score=' + $('.entry[field=score] .checkbox', $exportCSVDlg).hasClass('checked'));
                urlArgs.push('show_contact_info=' + $('.entry[field=contactInfo] .checkbox', $exportCSVDlg).hasClass('checked'));
                urlArgs.push('show_total=' + $('.entry[field=totalDonation] .checkbox', $exportCSVDlg).hasClass('checked'));

                // build the url
                var contactSetId = $('.entry.selected', $lstBody).attr('contactSetId');
                var url2 = templateData.exportUrlNew.replace('seatId', templateData.seatId)
                                                    .replace('contactSetId', contactSetId);

                // get the filters
                var filter = undefined;
                if ($('.listManager .filterModeBtn').hasClass('active')) {
                    // advance filter node
                    filter = newFilters.getFilter();
                }
                else {
                    // basic filter mode
                    var f = $('.listManager .basicFilterSection .showingContactsDiv .showingWhat').attr('quickFilterVal');
                    if (f != 'all') {
                        filter = 'filter=' + f;
                    }
                }
                if (filter) {
                    urlArgs.push(filter);
                }

                // start the download
                $.ajax({
                    type: 'GET',
                    url:  encodeURI(url2 + '?' + urlArgs.join('&'))
                })
                .done(function(result) {
                    // close the dialog
                    $exportCSVDlg.revupDialogBox('close');
                    $('body').revupMessageBox({
                        headerText:'CSV Export',
                        msg: "Your CSV file is being generated.<br>" +
                             "<span style='font-weight: bold'>You will receive an email when it is ready for download.</span>" +
                             "<br><br>Note: Depending on the size of the export, this may take several minutes."
                    });
                    updateRevupAlert();

                    getCount();
                })
                .fail(function(result) {
                    // hide the waiting
                    $exportCSVDlg.revupDialogBox('hideWaiting');
                    $exportCSVDlg.revupDialogBox('enableBtn', 'okBtn');
                    $exportCSVDlg.revupDialogBox('enableBtn', 'cancelBtn');
                    // display the error
                    revupUtils.apiError('Save CSV', result, 'Unable to create CSV file at this time',
                                        'Unable to create the CSV file at this time');
                });

            }
        });

        $('.formatOptionsDiv .entry .checkField', $exportCSVDlg).on('click', function(e) {
            var $cField = $(this);
            var $e = $(this).parent();
            var $checkbox = $('.checkbox', $e);

            if (($checkbox.hasClass('disabled')) || ($cField.hasClass('disabled'))) {
                return;
            }

            var $checkmark = $('.icon-check', $checkbox);
            if ($checkmark.is(':visible')) {
                $checkmark.hide();
                $checkbox.removeClass('checked');
            }
            else {
                $checkmark.show();
                $checkbox.addClass('checked')
            }
        })
    } // exportCSV

    /*
     * Merge selected lists
     */
    function merge()
    {
        var sTmp = [];
        sTmp.push('<div class="fieldLabel">This will merge the selected lists into a new list with the name given below.</div>');
        sTmp.push('<input class="mergeName" type="text" placeholder="Merge List Name" autofocus>');

        var $mergeDlg = $('body').revupDialogBox({
            okBtnText: 'Ok',
            headerText: 'Merge',
            msg: sTmp.join(''),
            extraClass: 'lstMgrDlg mergeDlg',
            isDragable: false,
            autoCloseOk: false,
            enableOkBtn: false,
            okHandler: function() {},
        })

        $('.mergeName', $mergeDlg).on('input', function(e) {
            $input = $(this);

            var v = $input.val();
            if (v.length > 0) {
                $('.okBtn', $saveAsDlg).prop('disabled', false);
            }
            else {
                $('.okBtn', $saveAsDlg).prop('disabled', true);
            }
        })
    } // merge

    /*
     * Share list with
     */
    function shareLst()
    {
        var sTmp = [];
        sTmp.push('<div class="fieldLabel">Please select the raiser(s) that you would like to share the currently selected list(s) with.</div>');

        sTmp.push('<div class="fieldLabel2">Raiser(s):</div>');
        sTmp.push('<div class="shareWithScroll">');
            sTmp.push('<div class="shareWithLst">');
            sTmp.push('<div class="shareLstDetailsLoading">');
                sTmp.push('<div class="loadingDiv" style="">');
                    sTmp.push('<div class="loading" style="margin-top:10px;width:75px;height:75px;"></div>');
                    //sTmp.push('<img class="loadingImg" src="' + loadingImg + '">')
                sTmp.push('</div>');
            sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>')

        var $shareDlg = $('body').revupDialogBox({
            okBtnText: 'Ok',
            headerText: 'Share',
            msg: sTmp.join(''),
            extraClass: 'lstMgrDlg shareDlg',
            isDragable: false,
            autoCloseOk: false,
            enableOkBtn: false,
            okHandler: function() {
                // get the contact set id's
                var contactSetId = $('.entry.selected', $lstBody).attr('contactSetId');

                var sharedWith = $('.entry.selected', $lstBody).attr('sharedWith');
                sharedWith = JSON.parse(sharedWith);

                // get the raiser to share with
                var shareWith = [];
                var removeShare = [];
                /*
                $('.entry .checkbox.checked', $shareDlg).each(function() {
                    var $e = $(this).closest('.entry');

                    shareWith.push(Number($e.attr('seatId')));
                })
                */

                $('.entry', $shareDlg).each(function() {
                    var $e = $(this)
                    var seatId = Number($(this).attr('seatId'));
                    // if cheched and not in the list of sharedWith add the list to share
                    if (($('.checkbox', $e).hasClass('checked')) && ($.inArray(seatId, sharedWith) == -1)) {
                        shareWith.push(seatId);
                    }

                    // if not checked and in the list of shared with add to the delete list
                    if (!($('.checkbox', $e).hasClass('checked')) && ($.inArray(seatId, sharedWith) != -1)) {
                        removeShare.push(seatId);
                    }
                })

                var addRemoveShare = [];
                if (removeShare.length != 0) {
                    var url = templateData.removeShareContactSetApi.replace('contactSetId', contactSetId);
                    var payload = {};
                    payload.seats = removeShare;
                    addRemoveShare.push(
                        $.ajax({
                            url: url,
                            data: JSON.stringify(payload),
                            type: "POST",
                            traditional: true,
                            contentType: 'application/json; charset=utf-8',
                            dataType: 'text'
                        })
                    );
                }
                if (shareWith.length != 0) {
                    var url = templateData.addShareContactSetApi.replace('contactSetId', contactSetId);
                    var payload = {};
                    payload.seats = shareWith;
                    addRemoveShare.push(
                        $.ajax({
                            url: url,
                            data: JSON.stringify(payload),
                            type: "POST",
                            traditional: true,
                            contentType: 'application/json; charset=utf-8',
                            dataType: 'text'
                        })
                    );
                }

                $.when.apply(null, addRemoveShare)
                    .done(function(r) {
                        var sharedWith = [];
                        $('.entry .checkbox.checked', $shareDlg).each(function() {
                            var $c = $(this);
                            var $e = $c.closest('.entry');

                            sharedWith.push(Number($e.attr('seatId')));
                        })

                        // update the list
                        $('.entry.selected', $lstBody).attr('sharedWith', JSON.stringify(sharedWith))

                        // close the dialog
                        $shareDlg.revupDialogBox('close');
                    })
                    .fail(function(r) {
                        for (var i = 0; i < arguments.length; i++)
                            revupUtils.apiError('List Manager - Share', arguments[i][0], 'Unable to Add or Remove Shared');
                    })
            },
        })

        function sharedLstEntry(bDisabled, bChecked, seatId, userId, name)
        {
            var sTmp = [];

            var extraClass = "";
            if (bDisabled) {
                extraClass = " disabled";
            }

            var checkedClass = "";
            if (bChecked) {
                checkedClass = " checked"
            }
            sTmp.push('<div class="entry' + extraClass + '" seatId="' + seatId + '" userId="' + userId +'">');
                sTmp.push('<div class="checkField">')
                    sTmp.push('<div class="checkbox' + extraClass + checkedClass + '">');
                        if (bChecked) {
                            sTmp.push('<div class="icon icon-check"></div>');
                        }
                        else {
                            sTmp.push('<div class="icon icon-check" style="display:none;"></div>');
                        }
                        sTmp.push('<div class="icon icon-box-unchecked"></div>');
                        sTmp.push('<div class="icon icon-check-box-back"></div>')
                    sTmp.push('</div>');
                    sTmp.push('<div class="checkboxLabel' + extraClass + '">' + name + '</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            return sTmp.join('');
        } // sharedLstEntry

        $shareDlg.on('click', '.shareWithLst .entry .checkField', function(e) {
            var $cField = $(this);
            var $e = $(this).parent();
            var $checkbox = $('.checkbox', $e);

            if (($checkbox.hasClass('disabled')) || ($cField.hasClass('disabled'))) {
                return;
            }

            var $checkmark = $('.icon-check', $checkbox);
            if ($checkmark.is(':visible')) {
                $checkmark.hide();
                $checkbox.removeClass('checked');
                $e.removeClass('checked')
            }
            else {
                $checkmark.show();
                $checkbox.addClass('checked');
                $e.addClass('checked');
            }

            $('.okBtn', $shareDlg).prop('disabled', false)
        })

        // get the orginizers/raisers
        var url = templateData.getOrganizerApi;
        $.ajax({
            url: url,
            type: 'GET',
        })
        .done(function (r) {
            var sharedWith = $('.entry.selected', $lstBody).attr('sharedWith');
            sharedWith = JSON.parse(sharedWith);

            var sTmp = [];
            var raisers = r.results;
            for(var i = 0; i < raisers.length; i++) {
                var name = '';
                var bDisabled = false;
                if (raisers[i].state.toLowerCase() === 'pending') {
                    name = raisers[i].user.email + ' (invite pending)';
                    bDisabled = true;
                }
                else if ((raisers[i].user.first_name == "") || (raisers[i].user.last_name == "")) {
                    name = raisers[i].user.email;
                }
                else {
                    name = raisers[i].user.first_name + ' ' + raisers[i].user.last_name;
                }

                // see if should be selected
                var bChecked = false;
                if ($.inArray(Number(raisers[i].id), sharedWith) != -1) {
                    bChecked = true;
                }

                sTmp.push(sharedLstEntry(bDisabled, bChecked, raisers[i].id, raisers[i].user.id, name));
            }

            $('.shareWithLst', $shareDlg).html(sTmp.join(''))
        })
        .fail(function(r) {
            revupUtils.apiError('List Manager - Share', r, 'Unable to Get List of Raised for sharing');
        })
    } // shareLst

    /*
     * Add List to Call Time Groups
     */
    function addLstToCallTimeGroup()
    {
        let $addBtn = $(this);
        let currentPage = 1;
        let totalNumGrps = -1;
        let searchFor = ''
        const grpLstDlgSize = 10;
        let bAddTo = false;

        // get the selected entries
        $selEntrys = $('.entry.selected', $lstBody);
        let selectedListId = $selEntrys.attr('contactSetId');
        let selectedListName = $('.colName .nameDiv', $selEntrys).text();

        // if one of more selected then display the "Add to Call Sheet Group" dialog
        if ($selEntrys.length > 0) {
            let msg = [];
            msg.push('<div class="addToGrpSection">');
                msg.push('<div class="addToGrpDiv">')
                    msg.push('<div class="radioBtnDiv radioBtnAddTo">');
                        msg.push('<div class="icon icon-radio-button-on disabled"></div>');
                    msg.push('</div>');
                    msg.push('<div class="radioBodyDiv">');
                        msg.push('<div class="msg">Add the selected list to an existing call group:</div>');
                        msg.push('<div class="lstHeading">List Name</div>');
                        msg.push('<div class="searchForDiv"></div>')
                        msg.push('<div class="grpLst">');
                            msg.push('<div class="loadingDiv">')
                                msg.push('<div class="loading"></div>')
                                //msg.push('<img class="loadingImg" src="' + loadingImg + '">');
                            msg.push('</div>');
                        msg.push('</div>');
                        msg.push('<div class="grpLstOverlay"></div>')
                    msg.push('</div>');
                msg.push('</div>');

                msg.push('<div class="spacerSection">')
                    msg.push('<div class="radioBtnDiv" style="height:10px"></div>');
                    msg.push('<div class="radioBodyDiv" style="height:10px"></div>');
                msg.push('</div>')

                msg.push('<div class="createNewGrpDiv">')
                    msg.push('<div class="radioBtnDiv radioBtnCreateGrp">');
                        msg.push('<div class="icon icon-radio-button-off disabled"></div>');
                    msg.push('</div>');
                    msg.push('<div class="radioBodyDiv">');
                        msg.push('<div class="msg">Add the selected list to a new call group:</div>');
                        msg.push('<input class="newGrpName" disabled type="text" value="' + selectedListName + '">');
                    msg.push('</div>');
                msg.push('</div>');
            msg.push('</div>');

            // add selected contacts to a call sheet group
            function addContactsFromLstToCallSheetGrp(grpId, grpName)
            {
                // build that data to add to the groups
                let cData = {};
                cData.contact_set = selectedListId;

                // add to call time group
                let url = templateData.callTimeBuildGrpFromLstApi.replace('CALLTIME-GRP-ID', grpId);
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: JSON.stringify(cData),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                })
                .done(function(r) {
                    // close the dialog
                    $addToCallSheetGrpDlg.revupDialogBox('close');
                    updateRevupAlert();

                    // display message that queued
                    $('body').revupMessageBox({
                        headerText:'Add To Call Group',
                        msg: "Your Contacts from \"" + selectedListName + "\"" + " are being added to the call group \"" + grpName + "\"."
                    })

                    getCount();
                })
                .fail(function(r) {
                    if (r.responseJSON.detail) {
                        let msg = r.responseJSON.detail;
                        if (msg.includes('Cannot add this list') && msg.includes('to the Call Group')) {
                            let index = msg.indexOf('Call Group:')
                            index += 'Call Group:'.length;
                            msg = msg.substring(index);
                            msg = msg.replace(/\"/g, "");
                            msg = msg.trim();

                            $('body').revupMessageBox({
                                headerText:'Add To Call Group - ERROR',
                                msg: '<div style="color: ' + revupConstants.color.redText + '">' + r.responseJSON.detail + '</div>',
                            });

                            return
                        }
                    }


                    revupUtils.apiError('Call Time', r, 'Add Contact To Call Sheet Group',
                                        "Unable to add a contact to the call sheet group at this time");
                })
            } // addContactsFromLstToCallSheetGrp

            // create a new call sheet group
            function createAndAddToCallSheeGrp(grpName)
            {
                let data = {};
                data.title = grpName;
                $.ajax({
                    type: 'POST',
                    url: templateData.callTimeSetsApi,
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                })
                .done(function(r) {
                    addContactsFromLstToCallSheetGrp(r.id, grpName);
                })
                .fail(function(r) {
                    revupUtils.apiError('Call Time', r, 'Add To Group',
                                        "Unable to create a call sheet group at this time");
                })
            }

            let $addToCallSheetGrpDlg = $('body').revupDialogBox({
                enableOkBtn: false,
                headerText: 'Add To Call Group',
                msg: msg.join(''),
                zIndex: 60,
                width: '450px',
                okHandler: function(e) {
                    // see what to do
                    if ($('.radioBtnAddTo .icon', $addToCallSheetGrpDlg).hasClass('icon-radio-button-on')) {
                        // add the selected contacts to existing call sheet group
                        let grpId = $('.entry.selected', $grpLst).attr('grpId');
                        let grpName = $('.entry.selected .text', $grpLst).text();
                        addContactsFromLstToCallSheetGrp(grpId, grpName);
                    }
                    else if ($('.radioBtnCreateGrp .icon', $addToCallSheetGrpDlg).hasClass('icon-radio-button-on')) {
                        let grpName = $('.newGrpName', $addToCallSheetGrpDlg).val();
                        createAndAddToCallSheeGrp(grpName);
                    }
                },
            });

            // selectors
            let $addLst = $('.grpLst', $addToCallSheetGrpDlg);
            let $addSelectText = $('.addToGrpDiv .msg', $addToCallSheetGrpDlg);
            let $addLstOverlay = $('.grpLstOverlay', $addToCallSheetGrpDlg);
            let $addNewText = $('.createNewGrpDiv .msg', $addToCallSheetGrpDlg);

            // add the search box
            let $addSearch = $('.searchForDiv', $addToCallSheetGrpDlg);
            $addSearch.revupSearchBox({
                placeholderText: 'Search for Group',
                })
                .on('revup.searchFor', function(e) {
                    currentPage = 1;
                    totalNumGrps = -1;
                    searchFor = e.searchValue;
                    bAddTo = false;
                    fetchCallSheetGrpForDlg();
                })
                .on('revup.searchClear', function(e) {
                    currentPage = 1;
                    totalNumGrps = -1;
                    searchFor = '';
                    bAddTo = false;
                    fetchCallSheetGrpForDlg();
                });

            // position mask
            let lstWidth = $addLst.width();
            let lstOffset = $addLst.offset();
            $addLstOverlay.width(lstWidth + 2)
                          .offset(lstOffset);

            // add the even handlers
            let $addToGrpRadioBtn = $('.radioBtnAddTo .icon', $addToCallSheetGrpDlg);
            let $newGrpRadioBtn = $('.radioBtnCreateGrp .icon', $addToCallSheetGrpDlg);
            let $newGrpInput = $('.newGrpName', $addToCallSheetGrpDlg);
            let $grpLst = $('.grpLst', $addToCallSheetGrpDlg);
            $('.radioBtnDiv .icon', $addToCallSheetGrpDlg).on('click', function(e) {
                let $btn = $(this);
                $newGrpInput.select();

                // see if disabled
                if ($btn.hasClass('disabled')) {
                    return;
                }

                // see if the currently currently selected button
                if ($btn.hasClass('icon-radio-button-on')) {
                    return;
                }

                // toggle
                if ($addToGrpRadioBtn.hasClass('icon-radio-button-on')) {
                    $('.radioBodyDiv .grpLst .entry').removeClass('selected');
                    // enable new group
                    //disable add to existing group
                    $addToGrpRadioBtn.removeClass('icon-radio-button-on')
                                     .addClass('icon-radio-button-off');
                    $newGrpRadioBtn.removeClass('icon-radio-button-off')
                                   .addClass('icon-radio-button-on');

                    // enable / disable input fields
                    $addNewText.removeClass('disabled');
                    $addSelectText.addClass('disabled');
                    $addLstOverlay.show();
                    $addSearch.revupSearchBox('disable');
                    $newGrpInput.prop('disabled', false).focus();

                    // see if the ok button should be enabled
                    var v = $newGrpInput.val();
                    if (v.length > 0) {
                        $('.okBtn', $addToCallSheetGrpDlg).prop('disabled', false);
                    }
                    else {
                        $('.okBtn', $addToCallSheetGrpDlg).prop('disabled', true);
                    }
                }
                else if ($newGrpRadioBtn.hasClass('icon-radio-button-on')) {
                    $('.radioBodyDiv .grpLst .entry').first().addClass('selected');
                    // enable use exiting group
                    //disable add to existing group
                    $newGrpRadioBtn.removeClass('icon-radio-button-on')
                                   .addClass('icon-radio-button-off');
                    $addToGrpRadioBtn.removeClass('icon-radio-button-off')
                                     .addClass('icon-radio-button-on');


                    // enable / disable input fields
                    $addNewText.addClass('disabled');
                    $addSelectText.removeClass('disabled');
                    $addLstOverlay.hide();
                    $addSearch.revupSearchBox('enable');
                    $newGrpInput.prop('disabled', true);

                    // enable the ok button
                    $('.okBtn', $addToCallSheetGrpDlg).prop('disabled', false);
                }
            })

            $grpLst.on('click', '.entry', function() {
                let $entry = $(this);

                // if selected then return
                if ($entry.hasClass('selected'))
                    return;

                // clear selection
                $('.entry.selected', $grpLst).removeClass('selected');

                // set selected
                $entry.addClass('selected');
            })

            // see of a new group has been defined
            $newGrpInput.on('input', function(e) {
                $input = $(this);

                var v = $input.val();
                if (v.length > 0) {
                    $('.okBtn', $addToCallSheetGrpDlg).prop('disabled', false);
                }
                else {
                    $('.btnContainerRight .okBtn').prop('disabled', true);
                }
            })


        // fetch the list of groups
        function fetchCallSheetGrpForDlg()
        {
            let url = templateData.callTimeSetsApi + '?page=' + currentPage + '&page_size=' + grpLstDlgSize;
            if (searchFor != '') {
                url += '&search=' + searchFor;
            }

            $.ajax({
                type: 'GET',
                url: url,
            })
            .done(function(r) {
                let sTmp = [];
                if (r.count == 0) {
                    sTmp.push('<div class="lstMsgDiv">');
                        if (searchFor == '') {
                            sTmp.push('<div class="lstMsg">No Call Sheet Groups Defined</div>');
                        }
                        else {
                            sTmp.push('<div class="lstMsg">No Call Sheet Groups start with "' + searchFor + '"</b></div>');
                        }
                    sTmp.push('</div>');

                    $addToGrpRadioBtn.removeClass('icon-radio-button-on')
                                     .addClass('icon-radio-button-off')
                                     .addClass('disabled');
                    $newGrpRadioBtn.removeClass('icon-radio-button-off')
                                   .addClass('icon-radio-button-on');
                    $newGrpInput.prop('disabled', false).focus();
                    $addSearch.revupSearchBox('disable');
                    $addNewText.addClass('disabled');
                    $addSelectText.removeClass('disabled');
                    $addLstOverlay.show();
                }
                else {
                    var grps = r.results;
                    let bShowMore = false;
                    for (let i = 0; i < r.results.length; i++) {
                        let extraClass = '';
                        if (i + ((currentPage - 1) + grpLstDlgSize) == 0)
                            extraClass = ' selected'
                        if ((r.results.length == grpLstDlgSize) && (i >= (Math.round(r.results.length * 0.7))) && !bShowMore) {
                            extraClass = ' timeToAddMore';
                            bShowMore = true;
                        }

                        //the first entry is always selected
                        if(i == 0){
                            sTmp.push('<div class="entry selected' + extraClass + '" grpId="' + grps[i].id + '">');
                                sTmp.push('<div class="text">' + grps[i].title + '</div>');
                            sTmp.push('</div>')
                        }
                        else {
                            sTmp.push('<div class="entry' + extraClass + '" grpId="' + grps[i].id + '">');
                                sTmp.push('<div class="text">' + grps[i].title + '</div>');
                            sTmp.push('</div>')
                        }
                    }

                    if (bShowMore) {
                        sTmp.push('<div class="entry loadingMore" style="display:none">');
                            sTmp.push('<div class="loadingMore"></div>')
                        sTmp.push('</div>');
                    }

                    $addToGrpRadioBtn.removeClass('icon-radio-button-off')
                                     .addClass('icon-radio-button-on')
                                     .removeClass('disabled');
                    $newGrpRadioBtn.removeClass('icon-radio-button-on')
                                   .addClass('icon-radio-button-off')
                                   .removeClass('disabled');
                    $newGrpInput.prop('disabled', true);
                    $addSearch.revupSearchBox('enable');
                    $addNewText.addClass('disabled');
                    $addSelectText.removeClass('disabled');
                    $addLstOverlay.hide();

                    // disable the ok button until an existing group has been selected, or a new create is being created
                    $('.okBtn', $addToCallSheetGrpDlg).prop('disabled', false);

                    // update the current
                    if (currentPage != 1)
                        bAddTo = true;
                    currentPage += 1;
                }

                // load the list
                if (bAddTo) {
                    // remove the loading more entry before appending the next group
                    $('.entry.loadingMore', $grpLst).remove();
                    $addLst.append(sTmp.join(''));
                }
                else {
                    $addLst.html(sTmp.join(''));
                }
            })
            .fail(function(r) {
                revupUtils.apiError('Call Time', r, 'Load prospects for group',
                                    "Unable to load prospects for group time");
            })
        } // fetchCallSheetGrpForDlg

        // start getting call sheet groups for the dialog
        fetchCallSheetGrpForDlg();
        let $gl = $('.revupDialogBox .grpLst');
        $grpLst
            .on('scroll', function(e) {
                let $timeToAddMore = $('.entry.timeToAddMore', $grpLst);
                if ($timeToAddMore.length > 0 && $timeToAddMore.visible($grpLst)) {
                    $timeToAddMore.removeClass('timeToAddMore');
                    $('.entry.loadingMore', $grpLst).show();

                    fetchCallSheetGrpForDlg();
                }
            })
            .on('mousewheel', function(e) {
                if($gl.prop('scrollHeight') - $gl.scrollTop() <= $gl.height() && (e.originalEvent.wheelDelta < 0)) {
                    e.preventDefault()
                }
            })

        }
    } // addLstToCallTimeGroup

    /*
     * Delete List(s)
     */
    function deleteLst()
    {
        var dLst = [];
        var setIdLst = [];
        $('.entry.selected', $lstBody).each(function() {
            $e = $(this);

            // build list of contact set id's
            setIdLst.push($e.attr('contactSetId'));

            // build list of names
            dLst.push($('.column.colName .nameDiv', $e).html());
        })
        var sTmp = [];
        sTmp.push('<div class="fieldLabel">Are you sure you want to delete the list.</div>');
        sTmp.push('<div class="deleteLst">');
            for(var i = 0; i < dLst.length; i++) {
                sTmp.push('<div class="deleteEntry">');
                    sTmp.push(dLst[i]);
                sTmp.push('</div>');
            }
        sTmp.push('</div>');
        sTmp.push('<div class="fieldLabel">This cannot be undone.</div>');

        var $deleteDlg = $('body').revupDialogBox({
            okBtnText: 'Ok',
            headerText: 'Are you Sure?',
            msg: sTmp.join(''),
            extraClass: 'lstMgrDlg deleteDlg',
            isDragable: false,
            autoCloseOk: false,
            //enableOkBtn: false,
            okHandler: function() {
                var url = templateData.deleteContactSetApi + setIdLst[0];
                $.ajax({
                    url: url,
                    type: 'DELETE',
                })
                .done(function (r) {
                    // remove entry
                    $('.entry.selected', $lstBody).remove();
                    // there will be more to fetch
                    if($lstBodyWrapper.attr('numentriesloaded') < $lstBodyWrapper.attr('numentries')){
                        fetchNextChunk(true);
                    }

                    // clear the count
                    $contactCount.text('--');
                    $detailsNameVal.text('--');

                    $deleteDlg.revupDialogBox('close');

                    getCount();
                })
                .fail(function(r) {
                    revupUtils.apiError('List Manager - Delete', r, 'Unable to delete a Contact Set');
                })

            },
        })
    } // deleteLst

    function deleteLstViaInlineBtn($entry)
    {
        // build list of contact set id's
        var contactSetId = $entry.attr('contactSetId');

        // build list of names
        var lstName = $('.column.colName .nameDiv', $entry).html();

        var sTmp = [];
        sTmp.push('<div class="fieldLabel">Are you sure you want to delete the list.</div>');
        sTmp.push('<div class="deleteLst">');
            sTmp.push('<div class="deleteEntry">');
                sTmp.push(lstName);
            sTmp.push('</div>');
        sTmp.push('</div>');
        sTmp.push('<div class="fieldLabel">This cannot be undone.</div>');

        var $deleteDlg = $('body').revupDialogBox({
            okBtnText: 'Ok',
            headerText: 'Are you Sure?',
            msg: sTmp.join(''),
            extraClass: 'lstMgrDlg deleteDlg',
            isDragable: false,
            autoCloseOk: false,
            //enableOkBtn: false,
            okHandler: function() {
                var url = templateData.deleteContactSetApi + contactSetId;
                $.ajax({
                    url: url,
                    type: 'DELETE',
                })
                .done(function (r) {
                    // remove entry
                    $entry.remove();

                    $deleteDlg.revupDialogBox('close');

                    // there will be more to fetch
                    if($lstBodyWrapper.attr('currentpage') < $lstBodyWrapper.attr('maxpages')){
                        $lstBodyWrapper.attr('maxpages',  Math.ceil(parseInt($lstBodyWrapper.attr('numentries')) / entriesPerPage));
                        fetchNextChunk(false, true);
                    }
                    else{
                        $lstBodyWrapper.attr(('numentriesloaded'), parseInt($lstBodyWrapper.attr('numentriesloaded')) - 1);
                    }

                    // update the attributes
                    $lstBodyWrapper.attr('numentries', parseInt($lstBodyWrapper.attr('numentries')) - 1);

                    // update footer count
                    $('.showing', $lstFooter).text(parseInt($lstBodyWrapper.attr('numEntriesLoaded')));
                    $('.total',$lstFooter).text(parseInt($lstBodyWrapper.attr('numEntries')));

                    if ($('.entry.selected', $lstBody).length == 0) {
                        $contactCount.text('--');
                        $detailsNameVal.text('--');
                        $taskExport.addClass('disabled');
                        $taskShare.addClass('disabled');
                        $taskCallTimeGrp.addClass('disabled');
                        $taskSaveAs.addClass('disabled');
                        return;
                    }
                    else{
                        getCount();
                    }
                })
                .fail(function(r) {
                    revupUtils.apiError('List Manager - Delete', r, 'Unable to delete a Contact Set');
                })
            },
        })
    } // deleteLstViaInlineBtn

    /*
     * Global event handlers
     */
    function loadEventHandlers()
    {
        $('.topTaskBarDiv .iconBtnDiv')
            .on('click', '.btnDiv', function(e) {
                var $btn = $(this);

                // see if disabled
                if ($btn.hasClass('disabled')) {
                    return;
                }

                // see which btn was pressed
                if($btn.hasClass('analyzeBtn')) {
                }
                else if ($btn.hasClass('saveAsBtn')) {
                    saveAs();
                }
                else if ($btn.hasClass('exportBtn')) {
                    exportCSV();
                }
                else if ($btn.hasClass('mergeBtn')) {
                    merge();
                }
                else if ($btn.hasClass('shareBtn')) {
                    shareLst();
                }
                else if ($btn.hasClass('createCallTimeGrpBtn')) {
                    addLstToCallTimeGroup();
                }
                else if ($btn.hasClass('deleteBtn')) {
                    deleteLst();
                }
                else if($btn.hasClass('filterModeBtn')) {
                    // toggle mode
                    var newMode;
                    if ($btn.hasClass('active')) {
                        // set basic
                        newMode = 'basic';
                    }
                    else {
                        newMode = 'advance';
                    }

                    // set the new filter mode and save it in local storage
                    setFilterMode(newMode);
                    localStorage.setItem('listMgrFilterStyle-' + templateData.filterVersion + '-' + templateData.seatId, newMode);
                }
                else {
                    console.error('List Manager - unknow button: ' + $btn.attr('class'));
                }
            });


        /*
         * Open/close showing Contacts
         */
        $('.listManager .basicFilterSection .showingContactsDiv .showingOpenClose').on('click', function() {
            var $openClose = $(this);
            var $showingWhat = $('.showingWhat', $openClose.closest('.showingContactsDiv'))
            var $showingContactsLstDiv = $('.listManager .basicFilterSection .showingContactsLstDiv');

            // see if disabled
            if ($openClose.hasClass('disabled')) {
                return;
            }

            // get the current
            var openCloseState = $openClose.attr('openCloseState');
            if ((openCloseState == "") | (openCloseState == "closed")) {
                // time to open
                $openClose.attr('openCloseState', 'opened');
                $openClose.css('background-color', '#777');
                $('.icon', $openClose).removeClass('icon-double-arrow-dn')
                                      .addClass('icon-double-arrow-up');

                // show the checkmark on the selected field
                $('.entry .icon', $showingContactsLstDiv).hide();
                var whatContacts = $showingWhat.attr('quickFilterVal');
                $('.entry[quickFilterVal="' + whatContacts + '"] .icon', $showingContactsLstDiv).show(1)

                $showingContactsLstDiv.slideDown();
            }
            else {
                // time to open
                $openClose.attr('openCloseState', 'closed');
                $openClose.css('background-color', revupConstants.color.grayBorder);
                $('.icon', $openClose).removeClass('icon-double-arrow-up')
                                      .addClass('icon-double-arrow-dn');

                $showingContactsLstDiv.slideUp();
            }
        });

        $('.listManager .basicFilterSection .showingContactsLstDiv .entry').on('click', function() {
            var $entry = $(this);

            // get the values
            var quickFilterVal = $entry.attr('quickFilterVal');
            var text = $('.text', $entry).text();

            if ((quickFilterVal == 'all') && ($('.entry.selected', $lstBody).length == 1)) {
                $taskShare.removeClass('disabled');
                if (features.CALL_TIME) {
                    $taskCallTimeGrp.removeClass('disabled');
                }
            }
            else {
                $taskShare.addClass('disabled');
                $taskCallTimeGrp.addClass('disabled');
            }

            // set the values
            $('.listManager .basicFilterSection .showingContactsDiv .showingWhat').attr('quickFilterVal', quickFilterVal)
                                                                                  .text(text);

            // close
            $('.listManager .basicFilterSection .showingContactsDiv .showingOpenClose').trigger('click');

            // build the filter
            buildBasicFilter();
        });

        // resize
        var resizeTimer = undefined;
        $(window)
            .on('resize', function(e) {
                // save only if resize of a column
                if (localStorage.getItem('listMgrColWidth-'  + templateData.seatId)) {
                    if (resizeTimer) {
                        clearTimeout(resizeTimer)
                    }

                    // delay doing anything until resize pauses
                    setTimeout(function() {
                        var colWidth = localStorage.getItem('listMgrColWidth-'  + templateData.seatId);
                        if (!colWidth) {
                            return
                        }
                        colWidth = JSON.parse(colWidth);

                        // compute delta
                        var delta = colWidth.listWidth - $lstSection.outerWidth();
                        // get the width of the name column and adjust
                        var nameWidth = $('.column.colName', $lstHeader).width();
                        var nameMinWidth = Number($('.column.colName', $lstHeader).attr('minColWidth'))
                        if ((nameWidth - delta) < nameMinWidth) {
                            // to small
                            $('.column', $lstHeader).each(function() {
                                var $col = $(this);

                                var c = $col.attr('class');
                                c = $.trim(c.replace('column', ''));

                                $('.' + c, $lstSection).width("");
                            })

                            // clear out the saved size
                            localStorage.removeItem('listMgrColWidth-'  + templateData.seatId)
                        }
                        else {
                            // change the size of the name columns
                            $('.column.colName', $lstSection).width(nameWidth - delta);

                            // save the columns width
                            var wData = {};
                            wData.listWidth = $lstSection.outerWidth();
                            $('.column', $lstHeader).each(function() {
                                var $col = $(this);

                                var c = $col.attr('class');
                                c = $.trim(c.replace('column', ''));
                                wData[c] = $col.outerWidth();
                            })
                            localStorage.setItem('listMgrColWidth-' + templateData.seatId, JSON.stringify(wData));
                        }
                    }, 250);
                }
                let newHeight = $(window).height() - ($('.lstSection').offset().top) - $('.footerDiv').height();
                $lstBodyWrapper.css('height', newHeight + 'px');
                $lstBody.css('height', (newHeight - 1) + 'px');
            });
    } // loadEventHandlers

    // basic quick filters
    /*
     * Build Filters
     */
    function buildBasicFilter(reloadList)
    {
        if (reloadList == undefined) {
            reloadList = true;
        }
        var filter = [];
        var filterDetails = {};

        // add the quick filter
        var qFilter = $('.listManager .basicFilterSection .showingContactsDiv .showingWhat').attr('quickFilterVal');
        qFilter = qFilter.split(';;');
        for (var i = 0; i < qFilter.length; i++) {
            if ((qFilter != 'all') && (qFilter != '')) {
                //filter.push("filter=quick:" + qFilter[i]);
                filter.push("filter=" + qFilter[i]);

                // build the details
                filterDetails.quickFilter = qFilter.join(';;');
            }
        }

        // save the filter data
        // localStorage.setItem('listMgrFilterStyle-' + templateData.filterVersion + '-' + templateData.seatId, 'basic');
        // localStorage.setItem('listMgrFilter-' + templateData.filterVersion + '-' + templateData.seatId, filter.join('&'));
        // localStorage.setItem('listMgrFilterBasicData-' + templateData.filterVersion + '-' + templateData.seatId, JSON.stringify(filterDetails));

        // apply the filter
        if (reloadList) {
            getCount();
        }
    } // buildBasicFilter

    /*
     * Load filter mode
     */
    function loadBasicFilterData()
    {
        // load the current data
        var data = localStorage.getItem('listMgrFilterBasicData-' + templateData.filterVersion + '-' + templateData.seatId);
        if (data != null) {
            data = JSON.parse(data);
            // handle empty data
            if ($.isEmptyObject(data)) {
                data.quickFilter = 'all';
            }

            // set the quick filter value
            if (templateData.bDisplayQuickFilters) {
                $('.listManager .basicFilterSection .showingContactsLstDiv .entry .icon-check').hide();
                var $qFilterEntry = $();
                var $e = $('.listManager .basicFilterSection .showingContactsLstDiv .entry')
                $e.each(function() {
                    var qf = $(this).attr('quickFilterVal');
                    if (qf == data.quickFilter) {
                        $qFilterEntry = $(this);
                        return false;
                    }
                });
                $('.icon-check', $qFilterEntry).show();
                $('.listManager .basicFilterSection .showingContactsDiv .showingWhat').attr('quickFilterVal', data.quickFilter);
                var label = $('.text', $qFilterEntry).text();
                if (label == '') {
                    label = quickFilters[0].label;
                }
                $('.listManager .basicFilterSection .showingContactsDiv .showingWhat').text(label);
            }
        }

        // once loaded build the filter
        buildBasicFilter(false);
    } // loadBasicFilterData

    function loadFilterData()
    {
        // add the new filters
        $.when(
            //$.ajax({
            //    url: contactResultsUrl,
            //    type: "GET",
            //}),
            $.ajax({
                url: templateData.filterListApi,
                type: "GET"
            }))
        .done(function(/*contactResultsRaw,*/ filterResultsRaw) {
            // var contactResults = contactResultsRaw[0];
            var filterResults = filterResultsRaw;//[0];

            // add the new filters
            var quickFilterIndex = 0;
            filterData = {
                // id: id of the quick filter
                // value: value of the quick filter (if multi part the parts are seperated via double semicolons (ie ;;))
                // label: label displayed in the dropdown list
                // desc: String displayed as the tooltip
                quickFilters: [],

                // id: id of the filter
                // action: filter action (search for xxx (ie name, location...))
                // popupDesc: String to appear in the value popup - description of the value
                // desc: String to be displayed in the tooltip with that value
                filters: [
                ],

                // grouped filters
                grpFilters: [],

                // filters stored under
                //filterFor: 'listMgr',
                filterFor: '',

                // hide the icon at the beginning
                bHideLeadingIcon: true,
                bDisplayQuickFilters: templateData.bDisplayQuickFilters,

                seatId: templateData.seatId,
                filterVersion: templateData.filterVersion,
            };

            // id: id of the quick filter
            // value: value of the quick filter (if multi part the parts are seperated via double semicolons (ie ;;))
            // label: label displayed in the dropdown list
            // desc: String displayed as the tooltip
            if (templateData.isAcademic) {
                    //{id: 0, value: 'all', label: 'All', desc: 'All Contacts'},
                filterData.quickFilters.push({id: 1, value: 'client-giving:true', label: 'Have Given', desc:'Contacts Who Gave to My Institution'})
                filterData.quickFilters.push({id: 2, value: 'client-giving:true;;client-giving-this-year:false', label: 'Not Given This Year', desc:'Contacts Who Gave to My Institution In Previous Years But Not This Year'})
                filterData.quickFilters.push({id: 3, value: 'high-potential-donor:true', label: 'High Potential', desc:'High Potential Contacts Who Gave to Other Causes'})
                filterData.quickFilters.push({id: 4, value: 'client-giving:false;;non-client-giving:false', label: 'Never Given', desc:'Contacts Who Never Gave to My Institution'})
                // filterData.quickFilters.push({id: 5, value: 'client-largest-donor:true', label: 'Largest Contributors', desc:'Largest Institution Contributors'})
                filterData.quickFilters.push({id: 6, value: 'aca-ditr:true', label: 'Diamonds in the Rough', desc: 'Diamonds in the Rough'});
            }
            else {
                //{id: 0, value: 'all', label: 'All', desc: 'All Contacts'},
                filterData.quickFilters.push({id: 1, value: 'client-giving-current:true', label: 'Current Cycle', desc: 'Contacts who gave to my candidate/committee in the current cycle'});
                filterData.quickFilters.push({id: 2, value: 'client-giving-past:true;;client-giving-current:false', label: 'Previous but not current cycles', desc: 'Contacts who gave to my candidate/committee in previous cycles but not the current cycle'});
                filterData.quickFilters.push({id: 3, value: 'ally-giving:true', label: 'Similar candidates', desc:  'Contacts who gave to candidates/committees highly similar to mine'});
                filterData.quickFilters.push({id: 4, value: 'oppo-giving:true', label: 'Opposition', desc: 'Contacts who gave to the opposition'});
                filterData.quickFilters.push({id: 5, value: 'client-giving-current:false;;client-giving-past:false', label: 'Never Gave', desc: 'Contacts who never gave to the candidate/committee'});
                filterData.quickFilters.push({id: 6, value: 'ditr:true', label: 'Diamonds in the rough', desc: 'Diamonds in the Rough'});
            }

            // the filters to the data
            var numLoadedFilters = filterData.filters.length;
            for (var i = 0; i < filterResults.count; i++) {
                var fResults = filterResults.results[i];
                var fType = fResults.type ? fResults.type.toLowerCase() : '';

                // make sure a supported type
                var bSkip = true;
                if ((fType === 'text') || (fType === 'dropdown') || (fType === 'choices') || (fType === 'autocomplete')) {
                    bSkip = false;
                }
                if (bSkip) {
                    continue;
                }

                var f = new Object();

                f.id = numLoadedFilters + i;

                // generic
                f.label = fResults.label;
                f.displayType = fResults.type;

                if (fResults.display_type) {
                    f.displayTypeDetails = fResults.display_type.toLowerCase();
                }

                if (fResults.desc) {
                    f.popupDesc = fResults.desc;
                }
                else {
                    f.popupDesc = '';
                }

                if (fResults.display_length) {
                    f.maxLength = fResults.display_length;
                }

                // expected
                f.action = fResults.expected[0].field;
                f.actionType = fResults.expected[0].type;

                // display type specific data
                if (f.displayType && f.displayType.toLowerCase() === 'dropdown') {
                    f.dropdownData = [];

                    if (fResults.dropdown_data) {
                        if (fResults.dropdown_data.toLowerCase() === 'usstates' ) {
                            f.dropdownData = revupConstants.dropDownStates;
                        }
                    }
                    else {
                        var c = fResults.choices;
                        for (var ci = 0; ci < c.length; ci++) {
                            var o = new Object();

                            o.value = c[ci][0];
                            o.displayValue = c[ci][1];
                            if (f.displayTypeDetails === 'date') {
                                var d = o.displayValue.split('-');
                                o.displayValue = d[1] + '/' + d[2] + '/' + d[0];
                            }

                            f.dropdownData.push(o);
                        }
                    }

                    // sort order
                    f.bSortOrderDecending = false;
                    if (fResults.sortOrder && fResults.sortOrder == 'descending') {
                        f.bSortOrderDecending = true;
                    }
                }
                else if (f.displayType && f.displayType.toLowerCase() === 'choices') {
                    f.choiceData = [];

                    var c = fResults.choices;
                    for (var ci = 0; ci < c.length; ci++) {
                        var o = new Object();

                        o.value = c[ci][0];
                        o.displayValue = c[ci][1];

                        f.choiceData.push(o);
                    }
                }
                else if (f.displayType && f.displayType.toLowerCase() === 'autocomplete') {
                    f.autocompleteUrl = fResults.url;
                }

                // add to the filters
                filterData.filters.push(f);

                // add to the grouped filters
                var grp = fResults.group;
                if (!grp || grp == '') {
                    grp = "unGrouped"
                }
                if (!filterData.grpFilters[grp]) {
                    filterData.grpFilters[grp] = [];
                }
                filterData.grpFilters[grp].push(f);
            }

            // load the filter bard
            var currentFilter = undefined;
            // currentFilter = localStorage.getItem('listMgrFilterAdvData-' + templateData.filterVersion + '-' + templateData.seatId);
            // if (currentFilter) {
            //     currentFilter = JSON.parse(currentFilter);
            // }
            newFilters.init($('.listManager .newFiltersSection'), filterData, currentFilter);

            // disable the filters
            enableDisableFilter(true);
        })
        .fail(function(r) {
            revupUtils.apiError('List Manager Details', r, 'Load List Manager List');

            loadDataError($containerDiv);
        })
        .always(function(r) {
            // trigger like clicking on the first entry
            //var $first = $('.rankingNewLayout .rankingDetailsLstDiv .entry:first-child', $containerDiv);
            //$first.trigger('click');
        });

        // build the basic filters
        // which contacts will be displayed
        var style = "";
        if (templateData.bDisplayQuickFilters) {
            style = "";
        }
        else {
            style = ' style="display:none;"';
        }

        var sTmp = [];
        sTmp.push('<div class="showingContactsDiv"' + style + '>');
            sTmp.push('<div class="showingLabel">Showing</div>');
            sTmp.push('<div class="showingWhat" quickFilterVal="' + quickFilters[quickFilterIndex].value + '">' + quickFilters[quickFilterIndex].label + '</div>');
            sTmp.push('<div class="showingOpenClose" openCloseState="closed">');
                sTmp.push('<div class="icon icon-double-arrow-dn"></div>')
            sTmp.push('</div>');
        sTmp.push('</div>');
        sTmp.push('<div class="showingContactsLstDiv" style="display:none;">');
            for (var i = 0; i < quickFilters.length; i++) {
                sTmp.push('<div class="entry" contactFilterId="' + quickFilters[i].id + '" quickFilterVal="' + quickFilters[i].value + '">');
                    sTmp.push('<div class="text">'+ quickFilters[i].label + '</div>');
                    sTmp.push('<div class="icon icon-check"></div>');
                sTmp.push('</div>');

                if ((i + 1) < quickFilters.length) {
                    sTmp.push('<hr>')
                }
            }
        sTmp.push('</div>');
        $('.basicFilterSection').html(sTmp.join(''));

        // set to basic or advanced
        var filterMode = localStorage.getItem('listMgrFilterStyle-' + templateData.filterVersion + '-' + templateData.seatId);
        filterMode = filterMode == null ? 'basic': filterMode;
        setFilterMode(filterMode);

    }

    /*
     * List sections
     */
    function loadHeaderEvents()
    {
        $lstHeader.on('mousedown', '.slider', function(e)
        {
            /*
             * slider
             */
            // which == 1 means left mouse button
            if (e.which == 1) {
                // get the left column
                $slider = $(this);
                $left = $slider.prev();
                minLeft = parseInt($left.attr('minColWidth'), 10);

                // get the right column
                $right = $slider.next();
                minRight = parseInt($right.attr('minColWidth'), 10);

                // set the starting point
                currentPosX = e.pageX;

                // make sure there is room to resize
                if (($left.width() <= minLeft) && ($right.width() <= minRight)) {
                    e.preventDefault();

                    return;
                }

                $slider.addClass('draggable').parents().on('mousemove', function(e) {
                    if ($slider.hasClass('draggable')) {
                        // set flag resizing
                        bResize = true;

                        var deltaX = currentPosX - e.pageX;
                        currentPosX = e.pageX;

                        // compute the new size
                        var leftWidth = $left.outerWidth();
                        var rightWidth = $right.outerWidth();
                        var newWidthLeft = leftWidth - deltaX;
                        var newWidthRight = rightWidth + deltaX;

                        // make sure the new size meets the minimum
                        var bResizeCol = true;
                        if (newWidthLeft < minLeft) {
                            var d = leftWidth - minLeft;
                            if (d == 0) {
                                bResizeCol = false;
                            }
                            else {
                                newWidthLeft =  minLeft;
                                newWidthRight = rightWidth + d;
                            }
                        }
                        else if (newWidthRight < minRight) {
                            var d = rightWidth - minRight;
                            if (d == 0) {
                                bResizeCol = false;
                            }
                            else {
                                newWidthRight = minRight;
                                newWidthLeft = leftWidth + d;
                            }
                        }

                        //resize
                        if (bResizeCol) {
                            var css = $left.attr('class');
                            css = $.trim(css.replace('column', ''));
                            $('.' + css, $lstSection).outerWidth(newWidthLeft);

                            css = $right.attr('class');
                            css = $.trim(css.replace('column', ''));
                            $('.' + css, $lstSection).outerWidth(newWidthRight);

                            // save the columns width
                            var wData = {};
                            wData.listWidth = $lstSection.outerWidth();
                            $('.column', $lstHeader).each(function() {
                                var $col = $(this);

                                var c = $col.attr('class');
                                c = $.trim(c.replace('column', ''));
                                wData[c] = $col.outerWidth();
                            })
                            localStorage.setItem('listMgrColWidth-' + templateData.seatId, JSON.stringify(wData));
                        }
                    }
                }).on('mouseup', function(e) {
                    // stop dragging
                    $slider.removeClass('draggable')
                })
            }

            e.preventDefault();
        })
        .on('mouseup', function() {
            $('.draggable').removeClass('draggable')
        });
    } // loadHeaderEvents

    function convertFilterQueryHeader(filterQuery)
    {
        filterQuery = JSON.parse(filterQuery);

        // if an empty object return --
        if ($.isEmptyObject(filterQuery)) {
            return '--';
        }

        return newFilters.decodeForListMgr(filterQuery);
    } // convertFilterQueryHeader

    function convertSharedWithHeader(sharedWith)
    {
        sharedWith = JSON.parse(sharedWith);

        function buildRaisersLst(shared)
        {
            var sTmp = [];

            for (var i = 0; i < shared.length; i++) {
                for (var j = 0; j < raiserLst.length; j++) {
                    if (shared[i] == raiserLst[j].id) {
                        sTmp.push(raiserLst[j].user.first_name + ' ' + raiserLst[j].user.last_name);

                        break;
                    }
                }
            }

            if (sTmp.length == 0) {
                sTmp.push('--');
            }

            return sTmp.join(', ');
        } // buildRaisersLst

        if (!raiserLst) {
            // get the orginizers/raisers
            var url = templateData.getOrganizerApi;
            $.ajax({
                url: url,
                type: 'GET',
            })
            .done(function (r) {
                raiserLst = r.results;

                $detailsSharedWithVal.text(buildRaisersLst(sharedWith));
                return buildRaisersLst(sharedWith);
            })
            .fail(function(r) {
                revupUtils.apiError('Shared With - Header', r, 'Unable to Get List of Raised');
            })
        }
        else {
            return buildRaisersLst(sharedWith)
        }

        return '';
    } // convertSharedWithHeader

    function loadLstBodyEvents()
    {
        $lstBody.on('click', '.checkboxDiv .icon-warning', function(e) {
            // build the content
            var content = [];
            content.push('<div class="analysisWarningPopup">');
            content.push('<div class="msg">');
                content.push("This list has not been analyzed. <br />Please run an analysis to access this data");
            content.push('</div>');
            content.push('</div>');
            $warningPopup = $(this).revupPopup({bAutoClose: true,
                                                     autoCloseTimeout: 10000,
                                                     bSingle: true,
                                                     location: 'right',
                                                     zIndex: 25,
                                                     content: content.join('')
                                                 });
        });
        $lstBody.on('click', '.checkboxDiv .icon-box-unchecked, .checkboxDiv .icon-check', function(e) {
            var $btn = $(this).parent();
            var $entry = $btn.closest('.entry');

            if ($btn.hasClass('selected')) {
                $btn.removeClass('selected');
                $entry.removeClass('selected');
            }
            else {
                $btn.addClass('selected');
                $entry.addClass('selected');
            }

            //see how many things are enable enable/disable the correct buttons
            var bUpdateCount = false;
            var numSelectedSaved = 0;
            $('.entry.selected', $lstBody).each(function() {
                var $e = $(this);
                if ($e.attr('srcType') == 'Saved') {
                    numSelectedSaved++;
                }
            })

            // clear filters stored localStorage
            // localStorage.removeItem('listMgrFilterBasicData-' + templateData.filterVersion + '-' + templateData.seatId);
            // localStorage.removeItem('listMgrFilterAdvData-' + templateData.filterVersion + '-' + templateData.seatId);
            // localStorage.removeItem('listMgrFilter-' + templateData.filterVersion + '-' + templateData.seatId);
            newFilters.reset()


            var numSelected = $('.entry.selected', $lstBody).length;
            if (numSelected == 0) {
                $taskSaveAs.addClass('disabled');
                $taskExport.addClass('disabled');
                $taskMerge.addClass('disabled');
                $taskShare.addClass('disabled');
                $taskCallTimeGrp.addClass('disabled');
                $taskDelete.addClass('disabled');

                // detail line
                $contactCount.text('--');
                $detailsNameVal.text('--');
                $detailsFilterSetVal.text('--');
                $detailsSharedWithVal.text('--');

                // disable the filters
                enableDisableFilter(true);

                bUpdateCount = true;
            }
            else if (numSelected == 1) {
                $taskSaveAs.removeClass('disabled');

                if (features.CSV_EXPORT_RANKING) {
                    $taskExport.removeClass('disabled');
                }

                $taskMerge.addClass('disabled');
                //$taskCallTimeGrp.removeClass('disabled');

                var bEnableShare = true
                if ($('.listManager .filterModeBtn').hasClass('active')) {
                    if (!newFilters.isEmpty()) {
                        bEnableShare = false;
                    }
                }
                else {
                    if ($('.basicFilterSection .showingWhat[quickFilterVal="all"]').length == 0) {
                        bEnableShare = false;
                    }
                }
                if (bEnableShare) {
                    $taskShare.removeClass('disabled');
                    if (features.CALL_TIME) {
                        $taskCallTimeGrp.removeClass('disabled');
                    }
                }
                else {
                    $taskShare.addClass('disabled');
                    $taskCallTimeGrp.addClass('disabled');
                }

                if (numSelectedSaved > 0) {
                    $taskDelete.removeClass('disabled');
                }
                else {
                    $taskDelete.addClass('disabled');
                }

                bUpdateCount = true;

                // detail line
                var $e = $('.entry.selected', $lstBody);
                $detailsNameVal.text($('.colName .nameDiv', $e).text());
                var filterQuery = $e.attr('filterQuery');
                $detailsFilterSetVal.text(convertFilterQueryHeader(filterQuery));
                var sharedWith = $e.attr('sharedWith');
                $detailsSharedWithVal.text(convertSharedWithHeader(sharedWith));

                // enable the filters
                enableDisableFilter(false);
            }
            else if (numSelected > 1) {
                $taskSaveAs.removeClass('disabled');
                $taskExport.addClass('disabled');
                $taskMerge.removeClass('disabled');
                $taskShare.addClass('disabled');
                $taskCallTimeGrp.addClass('disabled');
                $taskDelete.addClass('disabled');

                bUpdateCount = true;

                // detail line
                $detailsNameVal.text('--');
                $detailsFilterSetVal.text('--');
                $detailsSharedWithVal.text('--');

                // enable the filters
                enableDisableFilter(false);
            }

            if (bUpdateCount) {
                getCount();
            }
        })
        .on('dblclick', '.entry .colName .nameDiv', function(e) {
            var $name = $(this);
            var $entry = $name.closest('.entry');
            var contactSetId = $entry.attr('contactSetId');
            var redirectSource = 'listMan';
            window.location = templateData.rankingPageUrl + '?partition=4&contact_set=' + contactSetId + '&page=1&page_size=50' + '&redirectFrom=' + redirectSource;
        })
        .on('click', '.deleteBtn', function(e) {
            var $dBtn = $(this);
            var $entry = $dBtn.closest('.entry');
            deleteLstViaInlineBtn($entry);
        })

    } // loadLstBodyEvents

    function buildLstHeader()
    {
        // see if there is column sizes stored
        var colSize = localStorage.getItem('listMgrColWidth-' + templateData.seatId)
        if (colSize != null) {
            colSize = JSON.parse(colSize);

            if (colSize.listWidth != $('.lstSection').outerWidth()) {
                colSize = null;
                localStorage.removeItem('listMgrColWidth-' + templateData.seatId)
            }
        }

        var sTmp = [];
        var colStyle = "";

        // select
        colStyle = 'text-align:center;';
        if (colSize) {
            colStyle += 'width:' + colSize.colSelect + 'px;'
        }
        sTmp.push('<div class="column colSelect" style="' + colStyle + '" minColWidth="30">');
            sTmp.push('<div class="columnHeader">Select</div>');
        sTmp.push('</div>');

        sTmp.push(buildSliderBar());

        // name
        colStyle = 'text-align:left;';
        if (colSize) {
            colStyle += 'width:' + colSize.colName + 'px;'
        }
        sTmp.push('<div class="column colName" style="' + colStyle + '" minColWidth="200">');
            sTmp.push('<div class="columnHeader">List Name</div>');
        sTmp.push('</div>');

        sTmp.push(buildSliderBar());

        // number of contacts
        colStyle = 'text-align:right;';
        if (colSize) {
            colStyle += 'width:' + colSize.colNumContacts + 'px;'
        }
        sTmp.push('<div class="column colNumContacts" style="' + colStyle + '" minColWidth="60">');
            sTmp.push('<div class="columnHeader">No of Contacts</div>');
        sTmp.push('</div>');

        sTmp.push(buildSliderBar());

        // source
        colStyle = 'text-align:left;';
        if (colSize) {
            colStyle += 'width:' + colSize.colSource + 'px;'
        }
        sTmp.push('<div class="column colSource" style="' + colStyle + '" minColWidth="35">');
            sTmp.push('<div class="columnHeader">Source</div>');
        sTmp.push('</div>');

        sTmp.push(buildSliderBar());

        // data created
        colStyle = 'text-align:center;';
        if (colSize) {
            colStyle += 'width:' + colSize.colDateCreate + 'px;'
        }
        sTmp.push('<div class="column colDateCreate" style="' + colStyle + '" minColWidth="60">');
            sTmp.push('<div class="columnHeader">Date Created</div>');
        sTmp.push('</div>');

        sTmp.push(buildSliderBar());

        // date shared
        colStyle = 'text-align:center;';
        if (colSize) {
            colStyle += 'width:' + colSize.colDateShared + 'px;'
        }
        sTmp.push('<div class="column colDateShared" style="' + colStyle + '" minColWidth="60">');
            sTmp.push('<div class="columnHeader">Date Shared</div>');
        sTmp.push('</div>');

        sTmp.push(buildSliderBar());

        // delete
        colStyle = 'text-align:center;';
        if (colSize) {
            colStyle += 'width:' + colSize.colDelete + 'px;'
        }
        sTmp.push('<div class="column colDelete" style="' + colStyle + '" minColWidth="40">');
            sTmp.push('<div class="columnHeader">Delete</div>');
        sTmp.push('</div>');

        $lstHeader.html(sTmp.join(''));
    } // buildLstHeader

    function buildLstBodyEntry(d, colSize, nextPageMarkerIndex)
    {
        // see if there is column sizes stored
        if (!colSize) {
            var colSize = localStorage.getItem('listMgrColWidth-' + templateData.seatId)
            if (colSize != null) {
                colSize = JSON.parse(colSize);

                if (colSize.listWidth != $('.lstSection').outerWidth()) {
                    colSize = null;
                    localStorage.removeItem('listMgrColWidth-' + templateData.seatId);
                }
            }
        }

        var colStyle = "";
        var sTmp = [];
        var parents = JSON.stringify(d.parents);
        var sharedWith = JSON.stringify(d.shared_with);
        var filterQuery = JSON.stringify(d.query);
        if(nextPageMarkerIndex){
            sTmp.push('<div class="entry timeToAddMore" contactSetId="' + d.id + '" analysisId="' + d.analysis + '" srcType="' + d.source +'" parents="' + parents + '" sharedWith=\'' + sharedWith + '\'  filterQuery=\'' + filterQuery + '\'>');
        }
        else{
            sTmp.push('<div class="entry" contactSetId="' + d.id + '" analysisId="' + d.analysis + '" srcType="' + d.source +'" parents="' + parents + '" sharedWith=\'' + sharedWith + '\'  filterQuery=\'' + filterQuery + '\'>');
        }
            // select
            colStyle = 'text-align:center;';
            if (colSize) {
                colStyle += 'width:' + colSize.colSelect + 'px;'
            }
            sTmp.push('<div class="column colSelect" style="' + colStyle + '">');
                sTmp.push('<div class="checkboxDiv">');
                    if (d.analysis) {
                        sTmp.push('<div class="icon icon-box-unchecked" ></div>');
                        sTmp.push('<div class="icon icon-check"></div>');
                    } else {
                        sTmp.push('<div class="icon icon-warning"></div>');

                    }
                sTmp.push('</div>');
            sTmp.push('</div>');
            sTmp.push(buildSliderBar(true));

            // name
            colStyle = 'text-align:left;';
            if (colSize) {
                colStyle += 'width:' + colSize.colName + 'px;'
            }
            sTmp.push('<div class="column colName" style="' + colStyle + '">');
            if(d.source == null){
                d.source = '';
            }
                // pick the icon to display
                switch (d.source.toLowerCase()) {
                    case 'csv':
                        sTmp.push("<img class='downloadImg imgCSV' src='" + templateData.csvImg + "'>");
                        break;
                    case 'gmail':
                        sTmp.push("<img class='downloadImg imgGmail' src='" + templateData.gmailImg + "'>");
                        break;
                    case 'linkedin':
                        sTmp.push("<img class='downloadImg imgLinkedIn' src='" + templateData.linkedInImg + "'>");
                        break;
                    case 'vcard':
                        sTmp.push("<img class='downloadImg imgApple' src='" + templateData.appleImg + "'>");
                        break;
                    case 'outlook':
                        sTmp.push("<img class='downloadImg imgOutlook' src='" + templateData.outlookImg + "'>");
                        break;
                    case 'iphone':
                        sTmp.push("<img class='downloadImg imgOutlook' src='" + templateData.mobileImg + "'>")
                        break;
                    case 'saved':
                        sTmp.push("<img class='downloadImg allRevup' src='" + templateData.savedImg + "'>");
                        break;
                    case 'cloned':
                        sTmp.push("<img class='downloadImg allRevup' style='width: 18px; 'src='" + templateData.clonedImg + "'>");
                        break;
                    case null:
                        sTmp.push("<img class='downloadImg allRevup' src='" + templateData.savedImg + "'>");
                        break;
                    case '':
                        sTmp.push("<img class='downloadImg allRevup' src='" + templateData.allImg + "'>");
                        break;
                    default:
                        colorText = true;
                        sTmp.push("<img class='downloadImg imgUnknow' src='" + templateData.unknownImg + "'>");
                        break;
                } // switch -- data.source
            sTmp.push('<div class="nameDiv">' + d.title + '</div>');
            sTmp.push('</div>');
            sTmp.push(buildSliderBar(true));

            // number of contacts
            colStyle = 'text-align:right;';
            if (colSize) {
                colStyle += 'width:' + colSize.colNumContacts + 'px;'
            }
            //var numContacts = '-';
            //if (d.count > 0) {
                numContacts = revupUtils.commify(d.count);
            //}
            sTmp.push('<div class="column colNumContacts" style="' + colStyle + '">');
                sTmp.push('<div class="numContacts">' + numContacts + '</div>');
            sTmp.push('</div>');
            sTmp.push(buildSliderBar(true));

            // source
            colStyle = 'text-align:left;';
            if (colSize) {
                colStyle += 'width:' + colSize.colSource + 'px;'
            }
            sTmp.push('<div class="column colSource" style="' + colStyle + '">');
                sTmp.push('<div class="srcDiv">' + d.source + '</div>');
            sTmp.push('</div>');
            sTmp.push(buildSliderBar(true));

            // date created
            colStyle = 'text-align:center;';
            if (colSize) {
                colStyle += 'width:' + colSize.colDateCreate + 'px;'
            }
            var date = revupUtils.formatDate(d.created);
            sTmp.push('<div class="column colDateCreate" style="' + colStyle + '">');
                sTmp.push('<div class="dateDiv">' + date + '</div>');
            sTmp.push('</div>');
            sTmp.push(buildSliderBar(true));

            // date shared
            colStyle = 'text-align:center;';
            if (colSize) {
                colStyle += 'width:' + colSize.colDateShared + 'px;'
            }
            var date = '-';
            if (d.last_shared) {
                date = revupUtils.formatDate(d.created);
            }
            sTmp.push('<div class="column colDateShared" style="' + colStyle + '">');
                sTmp.push('<div class="dateDiv">' + date + '</div>');
            sTmp.push('</div>');
            sTmp.push(buildSliderBar(true));

            sTmp.push('<div class="column colDelete" style="' + colStyle + '">');
                if (d.source.toLowerCase() == 'saved') {
                    sTmp.push('<div class="deleteBtn icon icon-delete-circle"></div>');
                }
            sTmp.push('</div>');
        sTmp.push('</div>')

        return (sTmp.join(''));
    } // buildLstBodyEntry

    ($lstBodyWrapper)
        .on('scroll', function(e) {
            let $timeToAddMore = $('.entry.timeToAddMore', $lstBodyWrapper);
            if($timeToAddMore.length == 0){
                return;
            }
            else if($timeToAddMore.length > 0){
                if($timeToAddMore.visible($lstBodyWrapper)){
                    $('.entry.loadingMore', $lstBody).show();
                    $timeToAddMore.removeClass('timeToAddMore');
                    fetchNextChunk();
                }
            }
        })

    function buildLstBody(lstData, appendFetch)
    {
        // see if there is column sizes stored
        var colSize = localStorage.getItem('listMgrColWidth-' + templateData.seatId)
        if (colSize != null) {
            colSize = JSON.parse(colSize);

            if (colSize.listWidth != $('.lstSection').outerWidth()) {
                colSize = null;
                localStorage.removeItem('listMgrColWidth-' + templateData.seatId);
            }
        }

        var rootList = "";

        // display the All Contacts
        // the is the source is blank
        for (var i = 0; i < lstData.length; i++) {
            var d = lstData[i];

            if (d.source == '') {
                //sTmp.push(buildLstBodyEntry(d, colSize));
                rootList = (['<div class="lstRootList">',
                                buildLstBodyEntry(d, colSize),
                                "</div>"]).join('');
            }
        }

        // display except 'All Contacts'
        var sTmp = [];
        for (var i = 0; i < lstData.length; i++) {
            var d = lstData[i];
            if (d.source != '') {
                if(i == nextPageMarkerIndex){
                    if(lstData.length > nextPageMarkerIndex){
                        sTmp.push(buildLstBodyEntry(d, colSize, true));
                    }
                }
                else{
                    sTmp.push(buildLstBodyEntry(d, colSize, false));
                }
            }

        }
        sTmp.push('<div class="entry loadingMore" style="display:none">');
            sTmp.push('<div class="loadingMore"></div>')
        sTmp.push('</div>');

        if(!appendFetch){
            $lstBody.html(([rootList,
                            '<div class="lstOtherLists">',
                            sTmp.join('')
                            ]).join(''));
        }
        else{
            $('.lstOtherLists', $lstBodyWrapper).append(sTmp.join(''));
        }
    } // buildLstBody

    /*
     * Load list of contact sets
     */
    function loadList(pageNumber)
    {
        // load the loading spinner
        $lstSection.hide();
        $lstLoading.show(1);
        $("html, body").scrollTop(0);

        var urlArg = [];
        urlArg.push('page_size=' + entriesPerPage);
        if (pageNumber) {
            urlArg.push("page=" + pageNumber);
        }
        var url = templateData.contactSetListApi + '?' + urlArg.join('&');
        $.ajax({
            url: url,
            type: "GET",
        })
        .done(function(r) {
            buildLstBody(r.results);
            $("html, body").scrollTop(0);
            $lstHeader.show();
            $lstBody.show();
            $lstFooter.show();
            $lstSection.show(1);
            $lstLoading.hide();
        })
        .fail (function(r) {
            revupUtils.apiError('List Manager Details', r, 'Load List Manager List');

            loadDataError($containerDiv);
        })
    } // loadList

    /*
     * fetchNextChunk
     */
    let fetchNextChunk = (bSingleEntry = false, deleted) => {;
        $('.entry.loadingMore', $lstBody).remove();
        var urlArgs = [];
        if($('.lstBodyWrapper').attr('numentries') != $('.lstBodyWrapper').attr('numentriesloaded')){
            if (bSingleEntry) {
                urlArgs.push("page_size=1");
                let page = parseInt($lstBodyWrapper.attr('numEntriesLoaded'), 10) + 1;
                urlArgs.push("page=" + page);
            }
            else {
                if (deleted){
                    urlArgs.push("page_size=1");
                    let nextPage = parseInt($lstBodyWrapper.attr('currentPage')) * entriesPerPage;
                    urlArgs.push("page=" +  nextPage);
                }
                else {
                    urlArgs.push("page_size=" + entriesPerPage);
                    let nextPage = parseInt($lstBodyWrapper.attr('currentPage'), 10) + 1;
                    urlArgs.push("page=" +  nextPage);
                }
            }

            var url = templateData.contactSetListApi + '?' + urlArgs.join('&');

            $.ajax({
                url: url,
                type: "GET",
            })
            .done(function(r) {
                buildLstBody(r.results, true);
                $("html, body").scrollTop(0);
                $lstHeader.show();
                $lstBody.show();
                $lstFooter.show();
                $lstSection.show(1);
                $lstLoading.hide();

                // update the attributes
                if(bSingleEntry){
                    let totalContacts = parseInt($lstBodyWrapper.attr('numentries'), 10);
                    $lstBodyWrapper.attr('numentries',  totalContacts - 1);
                    $lstBodyWrapper.attr('maxpages',  Math.ceil(parseInt($lstBodyWrapper.attr('numentries')) / entriesPerPage));
                }
                if(!bSingleEntry){
                    if (!deleted){
                        let nextPage = parseInt($lstBodyWrapper.attr('currentPage'), 10) + 1;
                        $lstBodyWrapper.attr('currentPage', nextPage);
                        var addedNumEntriesLoaded = (parseInt($lstBodyWrapper.attr('numEntriesLoaded')) + (r.results).length);
                        $lstBodyWrapper.attr('numEntriesLoaded', addedNumEntriesLoaded);
                    }
                }

                // update footer count
                $('.showing', $lstFooter).text(parseInt($lstBodyWrapper.attr('numEntriesLoaded')));
                $('.total',$lstFooter).text(parseInt($lstBodyWrapper.attr('numEntries')));
            })
            .fail (function(r) {
                revupUtils.apiError('List Manager Details', r, 'Load List Manager List');
                loadDataError($containerDiv);
            })
        }
    } // fetchNextChunk

    /*
     * Helper methods
     */
    function setFilterMode(filterMode, bReset)
    {
        if (bReset == undefined) {
            bReset = true;
        }

        // reset to all
        // get the values
        if (bReset) {
            // clear the filters and reset the count
            newFilters.reset();

            var $allEntry = $('.listManager .basicFilterSection .showingContactsLstDiv .entry').first();
            var quickFilterVal = $allEntry.attr('quickFilterVal');
            var text = $('.text', $allEntry).text();
            $('.listManager .basicFilterSection .showingContactsDiv .showingWhat').attr('quickFilterVal', quickFilterVal)
                                                                                  .text(text);
            getCount();
        }

        if (filterMode == 'basic') {
            // make active
            $('.listManager .filterModeBtn').removeClass('active')

            // show the new/advance filter section
            $('.listManager .newFiltersSection').hide();

            // hide the old quick filter section
            $('.listManager .showingContactsDiv').show();
            $('.listManager .showingContactsLstDiv').hide();

            // loadBasicFilterData();
        }
        else if (filterMode == 'advance') {
            // make active
            $('.listManager .filterModeBtn').addClass('active')

            // show the new/advance filter section
            $('.listManager .newFiltersSection').show();

            // hide the old quick filter section
            $('.listManager .showingContactsDiv').hide();
            $('.listManager .showingContactsLstDiv').hide();
        }
    } // setFilterMode

    function buildSliderBar(bBlank)
    {
        var sTmp = [];

        var blank = "";
        if (bBlank) {
            blank = ' blank';
        }
        sTmp.push('<div class="slider' + blank + '">');
            if (!bBlank) {
                sTmp.push('<div class="sOuter"></div>');
                    sTmp.push('<div class="sInner"></div>');
                        sTmp.push('<div class="sCenter"></div>')
                    sTmp.push('<div class="sInner"></div>');
                sTmp.push('<div class="sOuter"></div>');
            }
        sTmp.push('</div>');

        return sTmp.join('')
    } // buildSliderBar

    function getQueryFilters()
    {
        if ($('.listManager .filterModeBtn').hasClass('active')) {
            // advance filters
            return newFilters.getFilterForContactSet();
        }
        else {
            // basic filter
            var query = {};

            var $basicFilter = $('.basicFilterSection .showingWhat');
            var qFilter = $basicFilter.attr('quickFilterVal');
            if (qFilter != 'all') {
                var v = qFilter.split(';;');

                for (var i = 0; i < v.length; i++) {
                    var vv = v[i].split(':');

                    query[vv[0]] = vv[1] == 'true';
                }
            }

            return query;
        }
    } // getQueryFilters

    function enableDisableFilter(bDisable)
    {
        // basic filters
        var $basicFilterDiv = $('.listManager .basicFilterSection .showingContactsDiv');
        var $openClose = $('.showingOpenClose', $basicFilterDiv);
        if (bDisable) {
            $basicFilterDiv.addClass('disabled');
            $openClose.addClass('disabled');

            // make sure the list is closed
            // time to open
            $openClose.attr('openCloseState', 'closed');
            $openClose.css('background-color', revupConstants.color.grayBorder);
            $('.icon', $openClose).removeClass('icon-double-arrow-up')
                                  .addClass('icon-double-arrow-dn');

            $('.listManager .basicFilterSection .showingContactsLstDiv').slideUp();

            // reset to all
            // get the values
            var $allEntry = $('.listManager .basicFilterSection .showingContactsLstDiv .entry').first();
            var quickFilterVal = $allEntry.attr('quickFilterVal');
            var text = $('.text', $allEntry).text();
            $('.listManager .basicFilterSection .showingContactsDiv .showingWhat').attr('quickFilterVal', quickFilterVal)
                                                                                  .text(text);
        }
        else {
            $basicFilterDiv.removeClass('disabled');
            $openClose.removeClass('disabled');
        }

        // advance filters
        newFilters.enableDisable(bDisable);
    } // enableDisableFilter

    function getCount()
    {
        //make sure something is selected
        if ($('.entry.selected', $lstBody).length == 0) {
            $contactCount.text('--');
            $detailsNameVal.text('--');

            return;
        }

        $contactCount.hide();
        $('.managerWrapper .title').hide();
        $contactCountLoading.show();

        var postData = {};
        postData.query = getQueryFilters();
        postData.parents = [];
        $('.entry.selected', $lstBody).each(function() {
            var $e = $(this);
            postData.parents.push($e.attr('contactSetId'));
        })

        let requestId = requestCount++;

        var url = templateData.getCountContactSetApi;
        $.ajax({
            url: url,
            data: JSON.stringify(postData),
            type: "POST",
            traditional: true,
            contentType: 'application/json; charset=utf-8',
            dataType: 'text'
        })
        .done(function(r) {
            r = JSON.parse(r);
            var count = r.count;
            var remainSelectedEntries = $('.entry.selected', $lstBody).length;
            if(requestId == (requestCount - 1) && remainSelectedEntries != 0){ // displaying the last request
                count = revupUtils.commify(count);

                $contactCountLoading.hide();
                $contactCount.show().text(count);
                $('.managerWrapper .title').show();

                $taskSaveAs.removeClass('disabled');

                if(remainSelectedEntries == 1){
                    if (features.CSV_EXPORT_RANKING) {
                        $taskExport.removeClass('disabled');
                    }

                    $taskShare.removeClass('disabled');

                    if (features.CALL_TIME) {
                        $taskCallTimeGrp.removeClass('disabled');
                    }
                }
                else if(remainSelectedEntries > 1){
                    $taskExport.addClass('disabled');
                    $taskShare.addClass('disabled');
                    $taskCallTimeGrp.addClass('disabled');
                }
            }
            else if(remainSelectedEntries == 0){
                $contactCountLoading.hide();
                $contactCount.show().text('--');
                $detailsNameVal.text('--');
                $('.managerWrapper .title').show();
            }
        })
        .fail(function(r) {
            console.error('Error get contact count: ', r)
        })
    } // getCount

    function loadDataError($div)
    {
        var sTmp = [];
        sTmp.push('<div class="errorMsg">');
            sTmp.push('<div class="msg">');
                sTmp.push("Unable to load list of Contact Set at this time<br><br>Try again later");
            sTmp.push('</div>')
        sTmp.push('</div>')

        //
        $lstBody.html(sTmp.join(''));
        $lstLoading.hide();
        $("html, body").scrollTop(0);
        $lstHeader.hide();
        $lstSection.show(1);
        $lstFooter.hide();
    } // loadDataError

    return {
        load: function(initData)
        {
            templateData = initData;

            // handler for closing the page info
            $('.pageInfoDiv .closeBtn').on('click', function(e) {
                $('.pageInfoDiv').hide();
            })

            // see if the diamonds in the rough filter should be added
            // load quick filters for basic filters
            if (templateData.isAcademic) {
                quickFilters.push({id: 0, value: 'all', label: 'All', desc: 'All Contacts'});
                quickFilters.push({id: 1, value: 'client-giving:true', label: 'Have Given', desc:'Contacts Who Gave to My Institution'})
                quickFilters.push({id: 2, value: 'client-giving:true;;client-giving-this-year:false', label: 'Not Given This Year', desc:'Contacts Who Gave to My Institution In Previous Years But Not This Year'})
                quickFilters.push({id: 3, value: 'high-potential-donor:true', label: 'High Potential', desc:'High Potential Contacts Who Gave to Other Causes'})
                quickFilters.push({id: 4, value: 'client-giving:false;;non-client-giving:false', label: 'Never Given', desc:'Contacts Who Never Gave to My Institution'})
                // quickFilters.push({id: 5, value: 'client-largest-donor:true', label: 'Largest Contributors', desc:'Largest Institution Contributors'})
                quickFilters.push({id: 6, value: 'aca-ditr:true', label: 'Diamonds in the Rough', desc: 'Diamonds in the Rough'});
            }
            else {
                quickFilters.push({id: 0, value: 'all', label: 'All', desc: 'All Contacts'});
                quickFilters.push({id: 1, value: 'client-giving-current:true', label: 'Current Cycle', desc: 'Contacts who gave to my candidate/committee in the current cycle'});
                quickFilters.push({id: 2, value: 'client-giving-past:true;;client-giving-current:false', label: 'Previous but not current cycles', desc: 'Contacts who gave to my candidate/committee in previous cycles but not the current cycle'});
                quickFilters.push({id: 3, value: 'ally-giving:true', label: 'Similar candidates', desc:  'Contacts who gave to candidates/committees highly similar to mine'});
                quickFilters.push({id: 4, value: 'oppo-giving:true', label: 'Opposition', desc: 'Contacts who gave to the opposition'});
                quickFilters.push({id: 5, value: 'client-giving-current:false;;client-giving-past:false', label: 'Never Gave', desc: 'Contacts who never gave to the candidate/committee'});
                quickFilters.push({id: 6, value: 'ditr:true', label: 'Diamonds in the rough', desc: 'Diamonds in the Rough'});
            }

            // see if any values are stored
            var tmp = localStorage.getItem('listMgrEntriesPerPage-' + templateData.seatId);

            // see if any values are stored
            // add the new filters
            $.when(
                $.ajax({
                    url: templateData.contactSetListApi + "?page_size=" + entriesPerPage + "&page=1",
                    type: "GET",
                }),
                $.ajax({
                    url: templateData.filterListApi,
                    type: "GET"
                }))
            .done(function(contactSetListRaw, filterResultsRaw) {
                var contactSetLst = contactSetListRaw[0];
                var filterResults = filterResultsRaw[0];
                // add the new filters
                var quickFilterIndex = 0;
                filterData = {
                    // id: id of the quick filter
                    // value: value of the quick filter (if multi part the parts are seperated via double semicolons (ie ;;))
                    // label: label displayed in the dropdown list
                    // desc: String displayed as the tooltip
                    quickFilters: [],

                    // id: id of the filter
                    // action: filter action (search for xxx (ie name, location...))
                    // popupDesc: String to appear in the value popup - description of the value
                    // desc: String to be displayed in the tooltip with that value
                    filters: [],

                    // grouped filters
                    grpFilters: [],

                    // filters stored under
                    //filterFor: 'listMgr',
                    filterFor: '',

                    // hide the icon at the beginning
                    bHideLeadingIcon: true,
                    bDisplayQuickFilters: templateData.bDisplayQuickFilters,

                    seatId: templateData.seatId,
                    filterVersion: templateData.filterVersion,
                };

                // id: id of the quick filter
                // value: value of the quick filter (if multi part the parts are seperated via double semicolons (ie ;;))
                // label: label displayed in the dropdown list
                // desc: String displayed as the tooltip
                if (templateData.isAcademic) {
                        //{id: 0, value: 'all', label: 'All', desc: 'All Contacts'},
                    filterData.quickFilters.push({id: 1, value: 'client-giving:true', label: 'Have Given', desc:'Contacts Who Gave to My Institution'})
                    filterData.quickFilters.push({id: 2, value: 'client-giving:true;;client-giving-this-year:false', label: 'Not Given This Year', desc:'Contacts Who Gave to My Institution In Previous Years But Not This Year'})
                    filterData.quickFilters.push({id: 3, value: 'high-potential-donor:true', label: 'High Potential', desc:'High Potential Contacts Who Gave to Other Causes'})
                    filterData.quickFilters.push({id: 4, value: 'client-giving:false;;non-client-giving:false', label: 'Never Given', desc:'Contacts Who Never Gave to My Institution'})
                    // filterData.quickFilters.push({id: 5, value: 'client-largest-donor:true', label: 'Largest Contributors', desc:'Largest Institution Contributors'})
                    filterData.quickFilters.push({id: 6, value: 'aca-ditr:true', label: 'Diamonds in the Rough', desc: 'Diamonds in the Rough'});
                }
                else {
                    //{id: 0, value: 'all', label: 'All', desc: 'All Contacts'},
                    filterData.quickFilters.push({id: 1, value: 'client-giving-current:true', label: 'Current Cycle', desc: 'Contacts who gave to my candidate/committee in the current cycle'});
                    filterData.quickFilters.push({id: 2, value: 'client-giving-past:true;;client-giving-current:false', label: 'Previous but not current cycles', desc: 'Contacts who gave to my candidate/committee in previous cycles but not the current cycle'});
                    filterData.quickFilters.push({id: 3, value: 'ally-giving:true', label: 'Similar candidates', desc:  'Contacts who gave to candidates/committees highly similar to mine'});
                    filterData.quickFilters.push({id: 4, value: 'oppo-giving:true', label: 'Opposition', desc: 'Contacts who gave to the opposition'});
                    filterData.quickFilters.push({id: 5, value: 'client-giving-current:false;;client-giving-past:false', label: 'Never Gave', desc: 'Contacts who never gave to the candidate/committee'});
                    filterData.quickFilters.push({id: 6, value: 'ditr:true', label: 'Diamonds in the rough', desc: 'Diamonds in the Rough'});
                }

                // the filters to the data
                var numLoadedFilters = filterData.filters.length;
                for (var i = 0; i < filterResults.count; i++) {
                    var fResults = filterResults.results[i];
                    var fType = fResults.type ? fResults.type.toLowerCase() : '';

                    // make sure a supported type
                    var bSkip = true;
                    if ((fType === 'text') || (fType === 'dropdown') || (fType === 'choices') || (fType === 'autocomplete')) {
                        bSkip = false;
                    }
                    if (bSkip) {
                        continue;
                    }

                    var f = new Object();

                    f.id = numLoadedFilters + i;

                    // generic
                    f.label = fResults.label;
                    f.displayType = fResults.type;

                    if (fResults.display_type) {
                        f.displayTypeDetails = fResults.display_type.toLowerCase();
                    }

                    if (fResults.desc) {
                        f.popupDesc = fResults.desc;
                    }
                    else {
                        f.popupDesc = '';
                    }

                    if (fResults.display_length) {
                        f.maxLength = fResults.display_length;
                    }

                    // expected
                    f.action = fResults.expected[0].field;
                    f.actionType = fResults.expected[0].type;

                    // display type specific data
                    if (f.displayType && f.displayType.toLowerCase() === 'dropdown') {
                        f.dropdownData = [];

                        if (fResults.dropdown_data) {
                            if (fResults.dropdown_data.toLowerCase() === 'usstates' ) {
                                f.dropdownData = revupConstants.dropDownStates;
                            }
                        }
                        else {
                            var c = fResults.choices;
                            for (var ci = 0; ci < c.length; ci++) {
                                var o = new Object();

                                o.value = c[ci][0];
                                o.displayValue = c[ci][1];
                                if (f.displayTypeDetails === 'date') {
                                    var d = o.displayValue.split('-');
                                    o.displayValue = d[1] + '/' + d[2] + '/' + d[0];
                                }

                                f.dropdownData.push(o);
                            }
                        }

                        // sort order
                        f.bSortOrderDecending = false;
                        if (fResults.sortOrder && fResults.sortOrder == 'descending') {
                            f.bSortOrderDecending = true;
                        }
                    }
                    else if (f.displayType && f.displayType.toLowerCase() === 'choices') {
                        f.choiceData = [];

                        var c = fResults.choices;
                        for (var ci = 0; ci < c.length; ci++) {
                            var o = new Object();

                            o.value = c[ci][0];
                            o.displayValue = c[ci][1];

                            f.choiceData.push(o);
                        }
                    }
                    else if (f.displayType && f.displayType.toLowerCase() === 'autocomplete') {
                        f.autocompleteUrl = fResults.url;
                    }

                    // add to the filters
                    filterData.filters.push(f);

                    // add to the grouped filters
                    var grp = fResults.group;
                    if (!grp || grp == '') {
                        grp = "unGrouped"
                    }
                    if (!filterData.grpFilters[grp]) {
                        filterData.grpFilters[grp] = [];
                    }
                    filterData.grpFilters[grp].push(f);
                }

                // update the attributes
                $lstBodyWrapper.attr('currentPage', 1);
                if(contactSetLst.count == 0){
                    $lstBodyWrapper.attr('maxpages',  0);
                    $lstBodyWrapper.attr('numentries', 0);
                    $lstBodyWrapper.attr('numentriesloaded', 0)
                }

                // load the list
                else if (contactSetLst.count > 0) {
                    $lstBodyWrapper.attr('maxpages',  Math.ceil(contactSetLst.count/ entriesPerPage));
                    $lstBodyWrapper.attr('numentries', contactSetLst.count);
                    $lstBodyWrapper.attr('numentriesloaded', contactSetLst.results.length)// pageSize

                    // update the footer count
                    $lstFooter.show();
                    $lstFooter.html('<text>Showing </text>' + '<text class="showing">' + parseInt($lstBodyWrapper.attr('numEntriesLoaded')) +  '</text> '  + '<text> of </text>' + '<text class="total">' + $lstBodyWrapper.attr('numEntries') + '</text>' + '<text> lists</text>');

                    // build the list
                    buildLstHeader();
                    buildLstBody(contactSetLst.results);
                    // buildLstFooter(contactSetLst)

                    // display
                    $lstSection.show();
                    $lstHeader.show();
                    $lstBody.show();
                    // $lstFooter.show();
                    $lstLoading.hide();

                    // load event handlers for lstBody
                    loadHeaderEvents();
                    loadLstBodyEvents();
                    // loadLstFooterEvents();
                }

                // load the filter bar
                var currentFilter = undefined;
                //var currentFilter = localStorage.getItem('listMgrFilterAdvData-' + templateData.filterVersion + '-' + templateData.seatId);
                //if (currentFilter) {
                //    currentFilter = JSON.parse(currentFilter);
                //}
                newFilters.init($('.listManager .newFiltersSection'), filterData, currentFilter);

                // disable the filters
                enableDisableFilter(true);
            })
            .fail(function(r) {
                revupUtils.apiError('List Manager Details', r, 'Load List Manager List');

                loadDataError($containerDiv);
            })
            .always(function(r) {
                // trigger like clicking on the first entry
                //var $first = $('.rankingNewLayout .rankingDetailsLstDiv .entry:first-child', $containerDiv);
                //$first.trigger('click');
            });

            // monitor when the filter changes to update the count
            $('.listManager').on('revup.newFilter', '.newFiltersSection', function(e) {
                var numSelected = $('.entry.selected', $lstBody).length;
                if (numSelected >= 1) {
                    getCount();
                }

                // see if the shared button need to be disabled
                if ((numSelected == 1) && (newFilters.isEmpty())) {
                    $taskShare.removeClass('disabled');
                    if (features.CALL_TIME) {
                        $taskCallTimeGrp.removeClass('disabled');
                    }
                }
                else {
                    $taskShare.addClass('disabled');
                    $taskCallTimeGrp.addClass('disabled');
                }
            })

            // build the basic filters
            // which contacts will be displayed
            var style = "";
            if (templateData.bDisplayQuickFilters) {
                style = "";
            }
            else {
                style = ' style="display:none;"';
            }

            var sTmp = [];
            sTmp.push('<div class="showingContactsDiv"' + style + '>');
                sTmp.push('<div class="showingLabel">Showing</div>');
                sTmp.push('<div class="showingWhat" quickFilterVal="' + quickFilters[quickFilterIndex].value + '">' + quickFilters[quickFilterIndex].label + '</div>');
                sTmp.push('<div class="showingOpenClose" openCloseState="closed">');
                    sTmp.push('<div class="icon icon-double-arrow-dn"></div>')
                sTmp.push('</div>');
            sTmp.push('</div>');
            sTmp.push('<div class="showingContactsLstDiv" style="display:none;">');
                for (var i = 0; i < quickFilters.length; i++) {
                    sTmp.push('<div class="entry" contactFilterId="' + quickFilters[i].id + '" quickFilterVal="' + quickFilters[i].value + '">');
                        sTmp.push('<div class="text">'+ quickFilters[i].desc + '</div>');
                        sTmp.push('<div class="icon icon-check"></div>');
                    sTmp.push('</div>');

                    if ((i + 1) < quickFilters.length) {
                        sTmp.push('<hr>')
                    }
                }
            sTmp.push('</div>');
            $('.basicFilterSection').html(sTmp.join(''));

            // set to basic or advanced
            var filterMode = localStorage.getItem('listMgrFilterStyle-' + templateData.filterVersion + '-' + templateData.seatId);
            filterMode = filterMode == null ? 'basic': filterMode;
            setFilterMode(filterMode, false);

            loadEventHandlers();

            let newHeight = $(window).height() - ($('.lstSection').offset().top - $('.footerDiv').height());
            $lstBodyWrapper.css('height', newHeight + 'px');
        } // load
    }
} ());
