var events = (function () {
    /*
     * Globals
     */
    var templateData = {};      // data passed in

    var ddFundraisersLst = [];
    var fundraiserTypes = [{displayValue: 'Lead Fundraiser', value: 1}, {displayValue: 'Fundraiser', value: 0}];
    var fundraiserLeadIndex = 0;
    var fundraiserIndex = 1;
    var activeFundRaisers = [];
    var ddFundraisersLst = [];
    var orgRolesLst = [];


    /*
     * size the event data
     */
    function resizeEvents()
    {
        var sizeCols = [{className: 'col1', bFixed: false, percent: 0.6},
                        {className: 'col2', bFixed: false, percent: 0.4},
                        {className: 'col3', bFixed: true},
                        {className: 'col4', bFixed: true},
                        {className: 'col5', bFixed: true}];

        sizeColumns($('.eventSection'), sizeCols);

        // see if any of the fields on the header need tooltips
        $('.pageHeader .dataText').tooltipOnOverflow();

        // see if any of the price points, col2, need to be hidden
        var ppWidth = $('.eventSection .col2').width();
        var ppCellWidth = $('.eventSection .col2 .pricePointCell').outerWidth(true);
        $('.eventsDataDiv .eventData').each(function(index, val) {
            $pp = $('.col2', $(this));
            $ppCells = $('.pricePointCell', $pp);
            var numPPCells = $ppCells.length;
            var ppTotalWidth = numPPCells * ppCellWidth;

            // remove the rollupHide class
            $ppCells.removeClass('rollupHide')

            if (ppTotalWidth > ppWidth) {
                // make the price points to hide if rolled up
                var numVisible = Math.floor(ppWidth / ppCellWidth);
                for(var i = numVisible; i < numPPCells; i++) {
                    $('.eventSection .col2 .pricePointCell:nth-child(' + (i + 1) + ')').addClass('rollupHide')
                }

            }
        })
    } // resizeEvents

    var $addEventDiv = $('.eventSection .addNewEventDiv');
    function resetAddNewEventDiv()
    {
        // clear name
        $('.newEventName', $addEventDiv).val("");

        // reset organizer
        var sTmp = [];
        sTmp.push('<div class="organizerEntry">');
            sTmp.push('<div class="neworganizer" orgId="-1">no</div>');
            sTmp.push('<div class="newfundraisertype"></div>')
            sTmp.push('<div class="organizerDeleteBtn icon icon-delete-circle"></div>');
        sTmp.push('</div>');
        $('.organizerDiv', $addEventDiv).html(sTmp.join(''));


        // reset price points
        var sTmp = [];
        sTmp.push('<input type="text" class="newPricePoint" placeholder="Dollars">');
        sTmp.push('<input type="text" class="newPricePoint" placeholder="Dollars">');
        $('.pricePointContainer', $addEventDiv).html(sTmp.join(''));
        $('.newPricePoint', $addEventDiv).numericOnly();

        // reset the goal and date
        $('.newGoal', $addEventDiv).val('')
                                   .removeClass('inputFormatError')

        $('.newEventDate', $addEventDiv).val('')
                                        .datepicker('update');

        // update the dom
        $addEventDiv.show();

        // add the dropdown
        $('.eventSection .addNewEventDiv .neworganizer').revupDropDownField({value: ddFundraisersLst,
                                                                             ifStartIndexNeg1Msg: '<span class="clearMsg">Add Fundraiser</span>',
                                                                             valueStartIndex: -1});
        $('.eventSection .addNewEventDiv .newfundraisertype').revupDropDownField({value: fundraiserTypes,
                                                                                  valueStartIndex: -1,
                                                                                  ifStartIndexNeg1Msg: '<span class="clearMsg">Select</span>',
                                                                                  sortList: false});


        // reset height
        var addEventHeight = $('.eventSection .addNewEventDiv .fieldSectionDiv .col1').outerHeight(true);
        $('.eventSection .addNewEventDiv .fieldSectionDiv').height(addEventHeight);
        $('.eventSection .addNewEventDiv').outerHeight($('.eventSection .addNewEventDiv .btnSectionDiv').height() + addEventHeight + 10);

        // hide
        $addEventDiv.hide();

        // disable the buttons
        $('.eventDeleteEventBtn', $addEventDiv).addClass('disabled');
        $('.saveBtn', $addEventDiv).addClass('disabled');
    }; // resetAddNewEventDiv

    /*
     * format and display the event data
     */
    function formatEvent(eventData)
    {
        var sTmp = [];

        sTmp.push('<div class="eventData" eventId="' + eventData.id + '" openHeight="130">');
            sTmp.push('<div class="eventDetailDiv">')
                sTmp.push('<div class="fieldSectionDiv" openHeight="70">')
                    //col1 - event
                    sTmp.push('<div class="column col1">');
                        sTmp.push('<div class="eventName">');
                            //var url = eventDetailsUrl.replace("eventId", eventData.id);
                            //sTmp.push('<a href="' + url + '">');

                                sTmp.push(eventData.event_name)
                            //sTmp.push('</a>');
                        sTmp.push('</div>');

                        sTmp.push('<div class="organizerTitle">Fundraiser:</div>');
                        sTmp.push('<div class="organizerLstDiv">');
                            sTmp.push('<div class="loadingOrganizers">Loading...</div>')
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                    // col2 - price points
                    var pp = [];
                    if (eventData.price_points != undefined) {
                        for(var jj = 0; jj < eventData.price_points.length; jj++) {
                            pp.push('<div class="pricePointCell">');
                                pp.push(revupUtils.commify(eventData.price_points[jj]));
                            pp.push('</div>');
                        }
                    }
                    pp = pp.length == 0 ? '<div class="pricePointCell">-</div>' : pp.join('');
                    sTmp.push('<div class="column col2">');
                        sTmp.push(pp)
                    sTmp.push('</div>');


                    // col3 - goal
                    var goal = '-';
                    if (eventData.goal > 0) {
                        goal = '$' + revupUtils.commify(eventData.goal);
                    }
                    sTmp.push('<div class="column col3">');
                        sTmp.push(goal);
                    sTmp.push('</div>');

                    // col4 - date
                    sTmp.push('<div class="column col4">');
                        var dTmp = '-';
                        var d = eventData.event_date;
                        d = d.split('T');
                        if (d.length == 2) {
                            d = d[0].split('-');
                            if (d.length == 3) {
                                dTmp = d[1] + '/' + d[2] + '/' + d[0];
                            }
                        }
                        sTmp.push(dTmp);
                    sTmp.push('</div>');

                    // col5 - open/close
                    sTmp.push('<div class="column col5">');
                        sTmp.push('<div class="openClose closed icon icon-triangle-rounded-dn"></div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            sTmp.push('<div class="eventEditDiv">');
                sTmp.push('<div class="fieldSectionDiv" openHeight="100">');
                    sTmp.push('<div class="column col1">');
                        sTmp.push('<input type="text" class="newEventName" placeholder="Enter Event Name" value="' + eventData.event_name + '">');
                        sTmp.push('<div class="requiredField">*Required</div>');

                        sTmp.push('<div class="newOrgHeaderDiv">');
                            sTmp.push('<div class="title">Fundraiser:</div>');
                            sTmp.push('<button class="revupBtnDefault btnSmall eventAddOrganizerBtn">Add Another</button>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="organizerDiv">');
                            sTmp.push('<div class="organizerEntry">')
                                sTmp.push('<div class="neworganizer" xx style="height:22px;"></div>');
                                sTmp.push('<div class="newfundraisertype"></div>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                    // col2 - price points
                    var pp = [];
                    if (eventData.price_points != undefined) {
                        for(var jj = 0; jj < eventData.price_points.length; jj++) {
                            pp.push('<input type="text" class="newPricePoint" placeholder="Dollars" value="' + eventData.price_points[jj] + '">');
                        }
                    }
                    if (pp.length == 0) {
                        pp.push('<input type="text" class="newPricePoint" placeholder="Dollars">');
                        pp.push('<input type="text" class="newPricePoint" placeholder="Dollars">');
                    }
                    else if (pp.length < 8) {
                        pp.push('<input type="text" class="newPricePoint" placeholder="Dollars">');
                    }
                    sTmp.push('<div class="column col2">');
                        sTmp.push('<div class="pricePointContainer">');
                            sTmp.push(pp.join(''));
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="column col3">');
                        var goal = '';
                        if (eventData.goal > 0) {
                            goal = eventData.goal;
                        }
                        sTmp.push('<input type="text" class="newGoal" placeholder="$ Amount" value="' + goal + '">');
                    sTmp.push('</div>');

                    sTmp.push('<div class="column col4">');
                        sTmp.push('<div class="eventDateContainer">');
                            var dTmp = '-';
                            var d = eventData.event_date;
                            d = d.split('T');
                            if (d.length == 2) {
                                d = d[0].split('-');
                                if (d.length == 3) {
                                    dTmp = d[1] + '/' + d[2] + '/' + d[0];
                                }
                            }
                            sTmp.push('<input type="text" class="newEventDate dateinput" placeholder="Date" value="' + dTmp + '">');
                            sTmp.push('<div class="icon icon-calendar"></div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="requiredField">*Required</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="column col5">');
                        sTmp.push('<div class="openClose opened icon icon-triangle-rounded-up"></div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            sTmp.push('<div class="btnSectionDiv">');
                sTmp.push('<div class="column col1">');
                    sTmp.push('<button class="revupBtnDefault btnSmall eventDeleteEventBtn secondary">Delete Event</button>');
                sTmp.push('</div>');
                sTmp.push('<div class="column col2"></div>');
                sTmp.push('<div class="column col3"></div>');
                sTmp.push('<div class="column col4">');
                    sTmp.push('<div class="cancelBtn disabled revupLnk">Cancel</div>');
                    sTmp.push('<button class="revupBtnDefault btnSmall editBtn">Edit</button>');
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        return sTmp.join('');
    }; // formatEvent

    function formatEventResults(r)
    {
        var sTmp = [];
        if (r.count == 0) {
            sTmp.push('<div class="noEventData">');
                sTmp.push('<div class="msg">No Events Scheduled</div>');
            sTmp.push('</div>');
        }
        else {
            for (var i = 0; i < r.count; i++) {
                var event = r.results[i];

                sTmp.push(formatEvent(event));
            }
        }

        // display the results
        $('.eventsDataDiv').html(sTmp.join(''));

        // after loading attach datepicker
        $('.eventSection .eventDateContainer input.dateinput').datepicker({
                                                                        autoclose: true,
                                                                        clearBtn: true,
                                                                        todayBtn: true,
                                                                        format: "mm/dd/yyyy",
                                                                        todayHighlight: true,
                                                                        })

        // add the numericeOnly handlers
        $('.eventSection .pricePointContainer .newPricePoint').numericOnly();
        $('.eventSection  .newGoal').numericOnly();
    }; // formatEventResults

    /*
     * Load data
     */
    function loadFundraiser(lst)
    {
        var activeFundRaisers = [];
        $.each(lst, function(index, value) {
            //if ((value.user) && (value.user.is_active)) {
                var obj = new Object();

                obj.email = value.user.email;
                obj.firstName = value.user.first_name;
                obj.lastName = value.user.last_name;
                obj.fundraiserId = value.user.id;

                activeFundRaisers.push(obj);
            //}
        });

        ddFundraisersLst = [];
        $.each(activeFundRaisers, function(index, val) {
            obj = new Object();
            obj.value = val.fundraiserId;
            obj.displayValue  = val.firstName;
            obj.displayValue += obj.displayValue == '' ? '' : ' ';
            obj.displayValue += val.lastName;
            obj.displayValue += obj.displayValue == '' ? '' : ' ';
            obj.displayValue += (obj.displayValue != '') && (val.email != '') ? '(' : '';
            obj.displayValue += val.email;
            if (((val.firstName != '') || (val.lastName != '')) && (val.email != '')) {
                obj.displayValue += ')';
            }

            ddFundraisersLst.push(obj);
        });
    } // loadFundraiser

    /*
     * Helper Functions
     */
    function editOrganizers($eventData, bAddNew, eventId)
    {
        // get the 2 list
        var $detailOrgLst = $('.eventDetailDiv .organizerName', $eventData);
        var $editOrgLst = $('.eventEditDiv .organizerName', $eventData);

        // build a list of organizer
        var detailLst = [];
        var editLst = [];
        if (bAddNew) {
            var $addNewLst = $('.organizerDiv .neworganizer', $eventData)
            $addNewLst.each(function() {
                var $orgName = $(this);
                var val = $orgName.revupDropDownField('getBothValues');

                var $orgFund = $('.newfundraisertype', $orgName.parent());
                var fund = $orgFund.revupDropDownField('getValue');
                val.isLead = fund == 1 ? true : false;

                if (val.value != -1) {
                    editLst.push(val);
                }
            })
        }
        else {
            $detailOrgLst.each(function() {
                var $orgName = $(this);
                var val = {};
                val.value = parseInt($orgName.attr('orgId'), 10);
                val.displayValue = $orgName.html();

                var fund = $orgName.attr('orgIsLead');
                val.isLead = fund == 1 ? true : false;

                detailLst.push(val);
            })
            $editOrgLst.each(function() {
                var $orgName = $(this);
                var val = $orgName.revupDropDownField('getBothValues');

                var $orgFund = $('.fundraiserType', $orgName.parent());
                var fund = $orgFund.revupDropDownField('getValue');
                val.isLead = fund == 1 ? true : false;

                if (val.value != -1) {
                    editLst.push(val);
                }
            })
        }

        // build the delete list and add list
        var delLst = [];
        var addLst = [];
        var dupLst = [];

        // delete list see if orginizers in the detail list are still in the edit list - if not time to delete
        $.each(detailLst, function(i, v) {
            var indexIn = -1;
            $.each(editLst, function(ii, vv) {
                if ((v.value == vv.value) && (v.isLead == vv.isLead)) {
                    indexIn = ii;

                    return false;
                }

            })

            // don't add a entry/dummy user (-1) to the delLst
            if ((indexIn == -1) && (v.value != -1)) {
                delLst.push(v);
            }
        });

        // add list see if orginizer is only in the edit list and if so add to the add list - need to dedup
        $.each(editLst, function(i, v) {
            var indexIn = -1;
            $.each(detailLst, function(ii, vv) {
                if ((v.value == vv.value) && (v.isLead == vv.isLead)) {
                    indexIn = ii;

                    return false;
                }

            })
            if (indexIn == -1) {
                var bAdd = true;

                // dedup
                for (var index = 0; index < addLst.length; index++) {
                    if (addLst[index].value == v.value) {
                        // only once in the dup list
                        var addToDup = true;
                        for (var di = 0; di < dupLst.length; di++) {
                            if (dupLst[di] == v.displayValue) {
                                addToDup = false;

                                break;
                            }
                        }
                        if (addToDup) {
                            dupLst.push(v.displayValue);
                        }

                        bAdd = false;

                        break;
                    }
                }

                if (bAdd) {
                    addLst.push(v);
                }
            }
        });

        // see if dup's in the edit list
        for (var i = 0; i < editLst.length; i++) {
            for (var j = i + 1; j < editLst.length; j++) {
                if (editLst[i].value == editLst[j].value) {
                    // dup
                    var addToDup = true;
                    for (var di = 0; di < dupLst.length; di++) {
                        if (dupLst[di] == editLst[j].displayValue) {
                            addToDup = false;

                            break;
                        }
                    }
                    if (addToDup) {
                        dupLst.push(editLst[j].displayValue);
                    }
                }
            }
        }

        // build the list of deferred ajax calls
        //var eventId = parseInt($eventData.attr("eventId"), 10);
        var deferred = [];
        var delay = 1;

        // do the delete first
        for (var i = 0; i < delLst.length; i++) {
            var url = templateData.eventUsersDeleteToEventApi.replace('eventId', eventId).replace('userId', parseInt(delLst[i].value, 10));

            deferred.push(
                $.ajax({
                    url: url,
                    type: "DELETE",
                    delay: delay,
                }));

            delay++;
        }

        // next add users
        for (var i = 0; i < addLst.length; i++) {
            var url = templateData.eventUsersAddToEventApi.replace('eventId', eventId);
            var data = {};
            data.user = parseInt(addLst[i].value, 10);
            data.is_lead = addLst[i].isLead;

            deferred.push(
                $.ajax({
                    url: url,
                    type: 'POST',
                    delay: delay,
                    data: JSON.stringify(data),
                    traditional: true,
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'text'
                }));

            delay++;
        }

        // do it
        $.when.apply($, deferred).then(function(objects) {
            for(var i = 0; i < arguments.length; i++) {
                var result = arguments[i]
            }
        }, function(obj) {
            console.error('Event - add fundraiser to event error: ', obj);
        });

        // if dups - display the message box
        if (dupLst.length != 0) {
            $('.body').revupMessageBox({
                headerText: 'Duplicate Fundraisers',
                msg: 'The follow Orginizer(s) are duplicates:<br>&nbsp;&nbsp;&nbsp;&nbsp;' + dupLst.join('<br>&nbsp;&nbsp;&nbsp;&nbsp;') + '<br><br>Only a single fundraiser by the names in the list will be added to the event'
            })
        }
    }; // editOrganizers

    function buildNewEvent(eventData)
    {
        var sTmp = [];

        sTmp.push('<div class="eventData" eventId="' + eventData.id + '" openHeight="130">');
            sTmp.push('<div class="eventDetailDiv">')
                sTmp.push('<div class="fieldSectionDiv" openHeight="70">')
                    //col1 - event
                    sTmp.push('<div class="column col1">');
                        sTmp.push('<div class="eventName">');
                            //var url = eventDetailsUrl.replace("eventId", eventData.id);
                            //sTmp.push('<a href="' + url + '">');

                                sTmp.push(eventData.event_name)
                            //sTmp.push('</a>');
                        sTmp.push('</div>');

                        sTmp.push('<div class="organizerTitle">Fundraiser:</div>');
                        sTmp.push('<div class="organizerLstDiv">');
                            sTmp.push('<div class="loadingOrganizers">Loading...</div>')
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                    // col2 - price points
                    var pp = [];
                    if (eventData.price_points != undefined) {
                        for(var jj = 0; jj < eventData.price_points.length; jj++) {
                            pp.push('<div class="pricePointCell">');
                                pp.push(revupUtils.commify(eventData.price_points[jj]));
                            pp.push('</div>');
                        }
                    }
                    pp = pp.length == 0 ? '<div class="pricePointCell">-</div>' : pp.join('');
                    sTmp.push('<div class="column col2">');
                        sTmp.push(pp)
                    sTmp.push('</div>');


                    // col3 - goal
                    var goal = '-';
                    if (eventData.goal > 0) {
                        goal = '$' + revupUtils.commify(eventData.goal);
                    }
                    sTmp.push('<div class="column col3">');
                        sTmp.push(goal);
                    sTmp.push('</div>');

                    // col4 - date
                    sTmp.push('<div class="column col4">');
                        var dTmp = '-';
                        var d = eventData.event_date;
                        d = d.split('T');
                        if (d.length == 2) {
                            d = d[0].split('-');
                            if (d.length == 3) {
                                dTmp = d[1] + '/' + d[2] + '/' + d[0];
                            }
                        }
                        sTmp.push(dTmp);
                    sTmp.push('</div>');

                    // col5 - open/close
                    sTmp.push('<div class="column col5">');
                        sTmp.push('<div class="openClose closed icon icon-triangle-rounded-dn"></div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            sTmp.push('<div class="eventEditDiv">');
                sTmp.push('<div class="fieldSectionDiv" openHeight="100">');
                    sTmp.push('<div class="column col1">');
                        sTmp.push('<input type="text" class="newEventName" placeholder="Enter Event Name" value="' + eventData.event_name + '">');
                        sTmp.push('<div class="requiredField">*Required</div>');

                        sTmp.push('<div class="newOrgHeaderDiv">');
                            sTmp.push('<div class="title">Fundraiser:</div>');
                            sTmp.push('<button class="revupBtnDefault btnSmall eventAddOrganizerBtn">Add Another</button>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="organizerDiv">');
                            sTmp.push('<div class="organizerEntry">')
                                sTmp.push('<div class="neworganizer" xx style="height:22px;"></div>');
                                sTmp.push('<div class="newfundraisertype"></div>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                    // col2 - price points
                    var pp = [];
                    if (eventData.price_points != undefined) {
                        for(var jj = 0; jj < eventData.price_points.length; jj++) {
                            pp.push('<input type="text" class="newPricePoint" placeholder="Dollars" value="' + eventData.price_points[jj] + '">');
                        }
                    }
                    if (pp.length == 0) {
                        pp.push('<input type="text" class="newPricePoint" placeholder="Dollars">');
                        pp.push('<input type="text" class="newPricePoint" placeholder="Dollars">');
                    }
                    else if (pp.length < 8) {
                        pp.push('<input type="text" class="newPricePoint" placeholder="Dollars">');
                    }
                    sTmp.push('<div class="column col2">');
                        sTmp.push('<div class="pricePointContainer">');
                            sTmp.push(pp.join(''));
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="column col3">');
                        var goal = '';
                        if (eventData.goal > 0) {
                            goal = eventData.goal;
                        }
                        sTmp.push('<input type="text" class="newGoal" placeholder="$ Amount" value="' + goal + '">');
                    sTmp.push('</div>');

                    sTmp.push('<div class="column col4">');
                        sTmp.push('<div class="eventDateContainer">');
                            var dTmp = '-';
                            var d = eventData.event_date;
                            d = d.split('T');
                            if (d.length == 2) {
                                d = d[0].split('-');
                                if (d.length == 3) {
                                    dTmp = d[1] + '/' + d[2] + '/' + d[0];
                                }
                            }
                            sTmp.push('<input type="text" class="newEventDate dateinput" placeholder="Date" value="' + dTmp + '">');
                            sTmp.push('<div class="icon icon-calendar"></div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="requiredField">*Required</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="column col5">');
                        sTmp.push('<div class="openClose opened icon icon-triangle-rounded-up"></div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            sTmp.push('<div class="btnSectionDiv">');
                sTmp.push('<div class="column col1">');
                    sTmp.push('<button class="revupBtnDefault btnSmall eventDeleteEventBtn secondary">Delete Event</button>');
                sTmp.push('</div>');
                sTmp.push('<div class="column col2"></div>');
                sTmp.push('<div class="column col3"></div>');
                sTmp.push('<div class="column col4">');
                    sTmp.push('<div class="cancelBtn disabled revupLnk">Cancel</div>');
                    sTmp.push('<button class="revupBtnDefault btnSmall editBtn">Edit</button>');
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        return sTmp.join('');
    }; // buildNewEvent

    /*
     * fetch the event data and load
     */
    function load()
    {
        // fetch the
        $('.eventSection .headerDiv').hide();
        $('.eventSection .addNewEventDiv').hide();
        $('.eventSection .preHeaderDiv').hide();
        $('.eventSection .eventsDataDiv').html('<div style="width:100%;height:450px;position:relative;top:0;left:0;background-color:lightgray"><div class="loadingDiv"><div class="loading"><img class="loadingImg" src="' + loadingImg + '"></div></div></div>');

        function getEventsAjax() {
            return $.ajax({
                    url: templateData.eventGetListApi,
                    type: "GET"
                })
        }

        function getFundraisersAjax() {
            return $.ajax({
                        url: templateData.getOrganizerApi,
                        type: "GET"
                    })
        }

        $.when(getEventsAjax(), getFundraisersAjax())
            .done(function(eventResults, fundraiserResults) {
console.log('eventResults: ', eventResults[0])
console.log('fundraiserResults: ', fundraiserResults[0])
                // load the fundraiser data
                loadFundraiser(fundraiserResults[0].results);

                // handle the events results
                formatEventResults(eventResults[0]);

                $('.eventSection .headerDiv').show();
                $('.eventSection .addNewEventDiv').show();
                $('.eventSection .preHeaderDiv').show();

                // size everything
                resizeEvents();
                resetAddNewEventDiv();

                // add the resize handler
                $(window).off('resize').on('resize', resizeEvents);

            })
            .fail(function() {
                $('.eventsDataDiv').revupMessageBox({
                    headerText: 'Error Loading Events and Fundraisers',
                    msg: 'The following error occured when loading the events and fundraisers: <div style="width:80%;margin: 0 auto;">' + arguments[2] + '</div>Try again in a few minutes.'
                });
            });
    } // load

    /*
     *
     * Event Handlers Functions
     *
     */
    function closeEventData($openClose)
    {
        var $eventData = $openClose.closest('.eventData');

        // time to close
        $openClose.removeClass('opened')
                  .addClass('closed')
                  .removeClass('icon-triangle-rounded-up')
                  .addClass('icon-triangle-rounded-dn');

        // mark the line closed
        $eventData.animate({height: "27px"}, function() {
            if ($('.eventEditDiv', $eventData).is(":visible")) {
                $('.eventEditDiv', $eventData).fadeOut();
                $('.eventDetailDiv', $eventData).fadeIn();

                // change the arrow
                $('.eventDetailDiv .fieldSectionDiv .openClose', $eventData).removeClass('opened')
                                                                            .addClass('closed')
                                                                            .removeClass('icon-triangle-rounded-up')
                                                                            .addClass('icon-triangle-rounded-dn');

                // update the buttons
                $('.btnSectionDiv .cancelBtn', $eventData).addClass('disabled');
                $('.btnSectionDiv .saveBtn', $eventData).removeClass('disabled')
                                                        .text('Edit')
                                                        .removeClass('saveBtn')
                                                        .addClass('editBtn');
            }
        });
    } // closeEventData

    function openEventData($openClose)
    {
        var $eventData = $openClose.closest('.eventData');

        // time to open
        $openClose.removeClass('closed')
                  .addClass('opened')
                  .removeClass('icon-triangle-rounded-dn')
                  .addClass('icon-triangle-rounded-up');

        // get the organizers
        var eventId = $eventData.attr("eventId");
        var orgUrl = templateData.eventUsersApi.replace("eventId", eventId);
        // fetch the organizers
        $.ajax({
            url: orgUrl,
            type: "GET"
        }).done(function(r) {
            // build the list of organizers
            var sTmpEdit = [];
            var sTmpDetails = [];
            var names = r.results;
            $.each(names, function(index, val) {
                // format the name
                var n = val.first_name;
                n += n == '' ? '' : ' ';
                n += val.last_name;
                n += n == '' ? '' : ' ';
                n += (n != '') && (val.email != '') ? '(' : '';
                n += val.email;
                if (((val.first_name != '') || (val.last_name != '')) && (val.email != '')) {
                    n += ')';
                }

                // add name to list
                sTmpEdit.push('<div class="organizerEntry">');
                    sTmpEdit.push('<div class="organizerName" orgId="' + val.id + '" orgIsLead="' + (val.is_lead ? "1" : "0" ) + '">' + n + '</div>');
                    sTmpEdit.push('<div class="fundraiserType"></div>')
                    sTmpEdit.push('<div class="organizerDeleteBtn icon icon-delete-circle"></div>');
                sTmpEdit.push('</div>');
                sTmpDetails.push('<div class="organizerName" orgId="' + val.id + '" orgIsLead="' + (val.is_lead ? "1" : "0" ) + '">' + n + '</div>')
            });

            // add to the details section
            if (sTmpDetails.length == 0) {
                // if no name add message there is no name
                $('.organizerLstDiv', $eventData).html('<div class="organizerName noNames" orgId="-1">No Fundraisers For This Event</div>')
            }
            else {
                $('.organizerLstDiv', $eventData).html(sTmpDetails.join(''));
            }

            // add to the edit section
            if (sTmpEdit.length == 0) {
                // no organizers
                var sTmp = [];
                sTmp.push('<div class="organizerEntry">');
                    sTmp.push('<div class="organizerName" orgId="-1"></div>');
                    sTmp.push('<div class="fundraiserType"></div>')
                    sTmp.push('<div class="organizerDeleteBtn icon icon-delete-circle"></div>');
                sTmpEdit.push('</div>');
                $('.eventEditDiv .organizerDiv', $eventData).children().replaceWith(sTmp.join(''));
                $('.eventEditDiv .organizerDiv .organizerName', $eventData).revupDropDownField({value: ddFundraisersLst,
                                                                                                valueStartIndex: -1,
                                                                                                ifStartIndexNeg1Msg: '<span class="clearMsg">Add Fundraiser</span>'
                                                                                                });
                $('.eventEditDiv .organizerDiv .fundraiserType', $eventData).revupDropDownField({value: fundraiserTypes,
                                                                                                 valueStartIndex: -1,
                                                                                                 ifStartIndexNeg1Msg: '<span class="clearMsg">Select</span>',
                                                                                                 sortList: false});
            }
            else {
                $('.eventEditDiv .organizerDiv', $eventData).html(sTmpEdit.join(''));
                $('.eventEditDiv .organizerDiv .organizerName', $eventData).each(function() {
                    var $orgName = $(this);
                    var organizerId = $orgName.attr('orgId');
                    orgFundType = fundraiserIndex;
                    var orgFundTypeLead = parseInt($orgName.attr('orgIsLead'), 10);
                    if  (orgFundTypeLead == 1) {
                        orgFundType = fundraiserLeadIndex;
                    }
                    var organizerIndex = 0;
                    for (var i = 0; i < ddFundraisersLst.length; i++) {
                        if (ddFundraisersLst[i].value == organizerId) {
                            organizerIndex = i;
                            break
                        }
                    }

                    $orgName.revupDropDownField({value: ddFundraisersLst, valueStartIndex: organizerIndex});

                    var $fundType = $('.fundraiserType', $orgName.parent());
                    $fundType.revupDropDownField({value: fundraiserTypes, valueStartIndex: orgFundType, sortList: false})
                });
            }
        }).fail(function(r) {
            console.error('Event - loading fundraiser for an event: ', r);
            $('.eventsDataDiv').revupMessageBox({
                headerText: 'Error Loading Events fundraisers',
                msg: 'The following error occured when loading the events fundraisers: <div style="width:80%;margin: 0 auto;">' + arguments[2] + '</div>Try again in a few minutes.'
            });

            // get organizers (fail)
        }).always(function (r) {
            var $detailFieldSectionDiv = $('.eventDetailDiv .fieldSectionDiv', $eventData);
            var $editFieldSectionDiv = $('.eventEditDiv .fieldSectionDiv', $eventData)

            // compute extra space - if needed
            if (r.count > 1) {
                var num = r.count - 1;

                // details
                var extra = 18 * num;
                $detailFieldSectionDiv.attr('openHeight', 70 + extra);

                // edit
                extra = 27 * num;
                $editFieldSectionDiv.attr('openHeight', 100 + extra);
            }
            else {
                // details
                $detailFieldSectionDiv.attr('openHeight', 70);

                // edit
                $editFieldSectionDiv.attr('openHeight', 100);
            }

            var h = parseInt($detailFieldSectionDiv.attr('openHeight'), 10);
            $detailFieldSectionDiv.height(h)

            // take the height of the detail section plus the height of the buttons
            var fullHeight = (h + 32) + 'px';
            $eventData.animate({height: fullHeight});
        });
    } // openEventData

    function openCloseEventData(e, $btn)
    {
        var $openClose = $btn;
        var $eventData = $openClose.closest('.eventData');

        if ($openClose.hasClass('opened')) {
            closeEventData($openClose);
        }
        else {
            // before opening a new see if any already open and close them
            var $eventLst = $eventData.closest('.eventsDataDiv');
            var canOpen = true;
            $('.eventData', $eventLst).each(function() {
                // get the open close buttom
                var $eventD = $(this)
                var $openCloseBtn = $('.eventDetailDiv .fieldSectionDiv .openClose', $eventD);
                if ($openCloseBtn.hasClass('opened')) {
                    if (($('.eventEditDiv', $eventD).is(":visible")) && (!$('.btnSectionDiv .saveBtn', $eventD).hasClass('disabled'))) {
                        $openCloseBtn.revupMessageBox({
                            headerText: 'Auto Close Event',
                            msg: 'You must save or cancel your editing of an event before opening another event'
                        });
                        canOpen = false;
                    }
                    else {
                        closeEventData($openCloseBtn);
                    }

                    return false;
                }
            })

            if (canOpen) {
                openEventData($openClose)
            }
        }
    }; // openCloseEventData

    function loadEventHandlers()
    {
        $('.eventSection')
            // add new organizer
            .off('click', '.eventAddOrganizerBtn')
            .on('click', '.eventAddOrganizerBtn', function(e) {
                // get the parent
                var $btn = $(this);
                var $parent = $btn.closest('.fieldSectionDiv');

                // add a new entry
                addNewFundraiser($parent);

                // resize
                var addEventHeight = $('.col1', $parent).outerHeight(true);
                $parent.height(addEventHeight);
                var $p = $parent.closest('.addNewEventDiv');
                if ($p.length == 0) {
                    $p = $parent.closest('.eventData');
                }
                $p.outerHeight($('.eventSection .addNewEventDiv .btnSectionDiv').height() + addEventHeight + 10);
            })
            .off('click', '.eventData .openClose')
            .on('click', '.eventData .openClose', function(e) {
                openCloseEventData(e, $(this));
            })
            .off('click', '.eventData .eventDeleteEventBtn')
            .on('click', '.eventData .eventDeleteEventBtn', function(e) {
                var $btn = $(this);
                if ($btn.hasClass('disabled')) {
                    e.preventDefault();
                    return false;
                }

                // display the confirmation box before deleting
                var $eventData = $btn.closest('.eventData');
                var eventName = $('.fieldSectionDiv .col1 .eventName', $eventData).text();
                var msg = 'Are you sure you want to delete event "' + eventName + '"';
                $(this).clone(true).revupConfirmBox({
                    headerText: 'Delete Event - Confirmation',
                    msg: msg,
                    okHandler: function() {
                        deleteEventHandler(e, $btn)
                    }

                });
            })
            .off('click', '.eventData .editBtn')
            .on('click', '.eventData .editBtn', function(e) {
                var $btn = $(this);
                if ($btn.hasClass('disabled')) {
                    e.preventDefault();
                    return false;
                }

                // get selectors
                var $eventData = $btn.closest('.eventData');
                var $viewSection = $('.eventDetailDiv', $eventData);
                var $editSection = $('.eventEditDiv', $eventData);

                // change the height of the edit section
                var $editFieldSectionDiv = $('.fieldSectionDiv', $editSection);
                var h = parseInt($editFieldSectionDiv.attr('openHeight'), 10);
                $editFieldSectionDiv.height(h);
                h = $editFieldSectionDiv.outerHeight(true);
                var eventDataHeight = (h + 32) + 'px';

                $viewSection.fadeOut("slow", function() {
                    $eventData.animate({height: eventDataHeight});
                    $editSection.fadeIn("slow", function() {
                        // enable the cancel button cancel button and change the edit botton to saved
                        $('.cancelBtn', $eventData).removeClass('disabled');
                        $btn.text('Save').addClass('disabled').removeClass('editBtn').addClass('saveBtn');
                    });
                })
            })
            .off('click', '.eventData .saveBtn')
            .on('click', '.eventData .saveBtn', function(e) {
                var $btn = $(this);
                if ($btn.hasClass('disabled')) {
                    e.preventDefault();
                    return false;
                }

                // get the values
                var data = {};
                var $eventData = $btn.closest('.eventData');
                var eventId = parseInt($eventData.attr('eventId'), 10);
                var $eventEditData = $('.eventEditDiv', $eventData);

                // name
                data.event_name = $('.newEventName', $eventEditData).val();

                // date
                var date = $('.newEventDate', $eventEditData).val();
                date = date.split('/');
                data.event_date = date[2] + '-' + date[0] + '-' + date[1];

                // goal
                var goal = $('.newGoal', $eventEditData).val();
                data.goal = goal == '' ? 0 : parseInt(goal, 10);

                // get the price points
                data.price_points = [];
                $('.pricePointContainer .newPricePoint', $eventEditData).each(function() {
                    var $pp = $(this);

                    var pricePoint = parseInt($pp.val(), 10);
                    if (!isNaN(pricePoint)) {
                        data.price_points.push(pricePoint);
                    }
                })

                var url = templateData.eventUpdateApi.replace('eventId', eventId)
                $.ajax({
                    url: url,
                    type: "PUT",
                    data: JSON.stringify(data),
                    traditional: true,
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'text'
                }).done(function(r) {
                    // edit organizers
                    editOrganizers($eventData, false, parseInt($eventData.attr("eventId"), 10));

                    $eventData.animate({height: "27px"}, function() {
                        // remove the entry
                        var sTmp = buildNewEvent($.parseJSON(r));
                        $eventData.replaceWith(sTmp);
                        resizeEvents();
                    });
                }).fail(function(r) {
                    console.error('Event - unable to update event: ', r);
                    $('.eventsDataDiv').revupMessageBox({
                        headerText: 'Error Updating Event',
                        msg: 'The following error occured when updating event: <div style="width:80%;margin: 0 auto;">' + r.msg + '</div>Try again in a few minutes.'
                    });
                })
            })
            .off('click', '.eventData .cancelBtn')
            .on('click', '.eventData .cancelBtn', function(e) {
                var $btn = $(this);
                if ($btn.hasClass('disabled')) {
                    e.preventDefault();
                    return false;
                }

                // display the confirmation box before deleting
                var $eventData = $btn.closest('.eventData');
                var $viewSection = $('.eventDetailDiv', $eventData);
                var $editSection = $('.eventEditDiv', $eventData);
                var $btnSection = $('.btnSectionDiv', $eventData);
                var $editFieldSection = $('.fieldSectionDiv', $editSection);

                function cancelEventEdit() {
                    // change the height of the details section
                    var $viewFieldSectionDiv = $('.fieldSectionDiv', $viewSection);
                    var h = parseInt($viewFieldSectionDiv.attr('openHeight'), 10);
                    $viewFieldSectionDiv.height(h);
                    h = $viewFieldSectionDiv.outerHeight(true);
                    var eventDataHeight = (h + 32) + 'px';

                    // disable the cancel button cancel button and change the save botton to edit
                    $btn.addClass('disabled');
                    $('.saveBtn', $eventData).removeClass('disabled').text('Edit').removeClass('saveBtn').addClass('editBtn')
                    $editSection.fadeOut("slow");
                    $viewSection.fadeIn("slow")
                    $eventData.animate(
                        {height: eventDataHeight},
                        {
                            done: function() {
                                // time to reset the fundraiser list
                                var $detailOrgLst = $('.eventDetailDiv .organizerLstDiv', $eventData)
                                var sTmpEdit = [];
                                $('.organizerName', $detailOrgLst).each(function() {
                                    var $org = $(this);
                                    var orgId = parseInt($org.attr("orgId"), 10);
                                    var orgIsLeader = $org.attr('orgIsLead');

                                    if (orgId != -1) {
                                        sTmpEdit.push('<div class="organizerEntry">');
                                            sTmpEdit.push('<div class="organizerName" orgId="' + orgId + '" orgIsLead="' + orgIsLeader + '"></div>');
                                            sTmpEdit.push('<div class="fundraiserType"></div>')
                                            sTmpEdit.push('<div class="organizerDeleteBtn icon icon-delete-circle"></div>');
                                        sTmpEdit.push('</div>');
                                    }
                                    else {
                                        sTmpEdit.push('<div class="organizerEntry">');
                                            sTmpEdit.push('<div class="organizerName" orgId="-1"></div>');
                                            sTmpEdit.push('<div class="fundraiserType"></div>')
                                            sTmpEdit.push('<div class="organizerDeleteBtn icon icon-delete-circle"></div>');
                                        sTmpEdit.push('</div>');
                                    }
                                });

                                // reload the list
                                var $editOrgLst = $('.eventEditDiv .organizerDiv', $eventData);
                                $editOrgLst.html(sTmpEdit.join(''));

                                // reattach the dropdown's
                                $('.organizerEntry', $editOrgLst).each(function() {
                                    $orgEntry = $(this);

                                    //get the fields
                                    var $orgName = $('.organizerName', $orgEntry);
                                    var $fundType = $('.fundraiserType', $orgEntry);
                                    var orgId = parseInt($orgName.attr('orgId'));
                                    if (orgId == -1) {
                                        $orgName.revupDropDownField({value: ddFundraisersLst,
                                                                     valueStartIndex: -1,
                                                                     ifStartIndexNeg1Msg: '<span class="clearMsg">Add Fundraiser</span>'
                                                                    });
                                        $fundType.revupDropDownField({value: fundraiserTypes,
                                                                      valueStartIndex: -1,
                                                                      ifStartIndexNeg1Msg: '<span class="clearMsg">Select</span>',
                                                                      sortList: false});
                                    }
                                    else {
                                        var orgIndex;
                                        for (orgIndex = 0; orgIndex < ddFundraisersLst.length; orgIndex++) {
                                            if (ddFundraisersLst[orgIndex].value == orgId) {
                                                break;
                                            }
                                        }
                                        $orgName.revupDropDownField({value: ddFundraisersLst,
                                                                     valueStartIndex: orgIndex,
                                                                    });

                                        var isLeader = parseInt($orgName.attr('orgIsLeader'))
                                        orgFundType = fundraiserIndex;
                                        var isLead = parseInt($orgName.attr('orgIsLead'), 10);
                                        if  (isLead == 1) {
                                            orgFundType = fundraiserLeadIndex;
                                        }
                                        $fundType.revupDropDownField({value: fundraiserTypes,
                                                                      valueStartIndex: orgFundType,
                                                                      sortList: false});
                                    }
                                })
                            },
                            step: function(now, tween) {
                                $editFieldSection.height(now - 42);
                            }
                        }
                    )
                }; // cancelEventEdit

                // see if the save button is enabled and if so display a confirmation box
                if ($('.saveBtn', $btnSection).hasClass('disabled')) {
                    cancelEventEdit();
                }
                else {
                    $(this).revupConfirmBox({
                        headerText: 'Cancel Edit Event',
                        msg: "Are you sure you want to cancel \"Edit Event\"?  This will cause all changes to be lose",
                        okHandler: cancelEventEdit,
                    });
                }
            })
            .off('keydown', '.eventDateContainer input.dateinput')
            .on('keydown', '.eventDateContainer input.dateinput', function (event) {
                // trap the return key
                if (event.which === 13) {
                    /*
                    var tabEvent = jQuery.Event("keydown");
                    tabEvent.which = 9;
                    tabEvent.keyCode = 9;
                    $(this).trigger(tabEvent);
                    */

                    event.preventDefault();

                    return false;
                }
            })
            .off('change, changeDate, changeYear, changeMonth, clearDate, hide', '.eventDateContainer input.dateinput')
            .on('change, changeDate, changeYear, changeMonth, clearDate, hide', '.eventDateContainer input.dateinput', function(e) {
                var $ed = $(this).closest('.eventData');

                // if any field in error disable saveBtn
                if (($('.eventEditDiv .pricePointContainer .newPricePoint', $ed).hasClass('inputFormatError')) ||
                 ($('.eventEditDiv .newGoal', $ed).hasClass('inputFormatError'))) {
                    $('.btnSectionDiv .saveBtn', $ed).addClass('disabled');
                }
                else {
                    $('.btnSectionDiv .saveBtn', $ed).removeClass('disabled');
                }
            })
            .off('click', '.eventDateContainer .icon')
            .on('click', '.eventDateContainer .icon', function(e) {
                var $dateContainer = $(this).parent();
                $('input.dateinput', $dateContainer).datepicker('show');
            })
            .off('revup.dropDownSelected', '.organizerName, .fundraiserType')
            .on('revup.dropDownSelected', '.organizerName, .fundraiserType', function(e) {
                var $dropDownLst = $(this);
                var $eventData = $dropDownLst.closest('.eventData');
                var $btnSectionDiv = $('.btnSectionDiv', $eventData);
                var $entry = $dropDownLst.closest('.organizerEntry');

                var orgNameIndex = $('.organizerName', $entry).revupDropDownField('getIndex');
                var fundraiserTypeIndex = $('.fundraiserType', $entry).revupDropDownField('getIndex');
                if ((orgNameIndex != -1) && (fundraiserTypeIndex != -1)) {
                    $('.saveBtn', $btnSectionDiv).removeClass("disabled");
                }
            })
        .off('focus', '.pricePointContainer .newPricePoint')
        .on('focus', '.pricePointContainer .newPricePoint', function(e) {
            // price point
            // If last one and less than 8 add a new one
            var $container = $(this).closest('.pricePointContainer');

            // see how many price points there are
            var numPricePoints = $('.newPricePoint', $container).length;
            if (numPricePoints >= 8) {
                return;
            }

            // get the index of the location getting focus
            var index = $('.newPricePoint', $container).index(e.currentTarget);
            if (numPricePoints == (index + 1)) {
                var $newElement = $('<input type="text" class="newPricePoint" placeholder="Dollars">');
                $container.append($newElement);

                // make numeric only
                $newElement.numericOnly();
            }
        })
        .off('keyup', '.newEventName, .newPricePoint, .newGoal, .newEventDate')
        .on('keyup', '.newEventName, .newPricePoint, .newGoal, .newEventDate', function(e) {
            // enable the save button when any field changes
            var $ed = $(this).closest('.eventData');

            // if any field in error disable saveBtn
            if (($('.eventEditDiv .pricePointContainer .newPricePoint', $ed).hasClass('inputFormatError')) ||
             ($('.eventEditDiv .newGoal', $ed).hasClass('inputFormatError'))) {
                $('.btnSectionDiv .saveBtn', $ed).addClass('disabled');
            }
            else {
                $('.btnSectionDiv .saveBtn', $ed).removeClass('disabled');
            }
        })


        // save/create new event
        $('.eventSection .addNewEventDiv .saveBtn').off('click').on('click', function(e) {
            var $btn = $(this);

            // if disabled ignore
            if ($btn.hasClass('disabled')) {
                return;
            }

            // get the values
            var data = {};
            var $newEvent = $('.eventSection .addNewEventDiv');

            // name
            data.event_name = $('.newEventName', $newEvent).val();

            // date
            var date = $('.newEventDate', $newEvent).val();
            date = date.split('/');
            data.event_date = date[2] + '-' + date[0] + '-' + date[1];

            // goal
            var goal = $('.newGoal', $newEvent).val();
            data.goal = goal == '' ? 0 : parseInt(goal, 10);

            // get the price points
            data.price_points = [];
            $('.pricePointContainer .newPricePoint', $newEvent).each(function() {
                var $pp = $(this);

                var pricePoint = parseInt($pp.val(), 10);
                if (!isNaN(pricePoint)) {
                    data.price_points.push(pricePoint);
                }
            })

            // send to the back end.
            $.ajax({
                url: templateData.eventCreateApi,
                type: "POST",
                data: JSON.stringify(data),
                traditional: true,
                contentType: 'application/json; charset=utf-8',
                dataType: 'text'
            }).done(function(r) {
                // add the new entry
                var result = $.parseJSON(r);
                var newEvent = buildNewEvent(result);

                // add organizers
                editOrganizers($newEvent, true, result.id);

                // close and reset add new event
                $('.eventSection .addNewEventDiv').slideUp('slow', function() {
                    resetAddNewEventDiv();
                });

                $('.eventsDataDiv').prepend(newEvent);
                resizeEvents();
            }).fail(function(r) {
                // eventCreateApi (fail)
            })
        });

        // cancel 'add new event'
        $('.eventSection .addNewEventDiv .cancelBtn').off('click').on('click', function(e) {
            // see if the save button is enabled and if so display a confirmation box
            if ($('.eventSection .addNewEventDiv .saveBtn').hasClass('disabled')) {
                $('.eventSection .addNewEventDiv').slideUp('slow', function() {
                    resetAddNewEventDiv();
                });
            }
            else {
                $(this).revupConfirmBox({
                    headerText: 'Cancel Add New Event',
                    msg: "Are you sure you want to cancel \"Add New Event\"?  This will cause all changes to be lose",
                    okHandler: function() {
                        $('.eventSection .addNewEventDiv').slideUp('slow', function() {
                            resetAddNewEventDiv();
                        });
                    }

                });
            }
        });

        // datapicker for 'add new event'
        $('.eventSection .addNewEventDiv .eventDateContainer input.dateinput')
            .on('keydown', function (event) {
                // trap the return key
                if (event.which === 13) {
                    /*
                    var tabEvent = jQuery.Event("keydown");
                    tabEvent.which = 9;
                    tabEvent.keyCode = 9;
                    $(this).trigger(tabEvent);
                    */
                    event.preventDefault();

                    return false;
                }
            })
            .on('keyup', function(e) {
                enableDisableNewEventSaveBtn();
            })
            .on('change, changeDate, changeYear, changeMonth, clearDate, hide', function(e) {
                enableDisableNewEventSaveBtn();
            });

        // Enable/disable the save button for add
        $('.eventSection .addNewEventDiv').on('keyup', '.newEventName, .newPricePoint, .newGoal', function() {
            enableDisableNewEventSaveBtn();
        });

        // addNewEventBtn button handler
        $('.addNewEventBtn').on('click', function(e) {
            $('.eventSection .addNewEventDiv').slideDown();
        });

        /*
         * Event Search
         */
        $('.eventSection .eventSearch').revupSearchBox();
        $('.eventSection .eventSearch')
            .on('revup.searchClear', function(e) {
                console.log('revup.searchClear')
            })
            .on('revup.searchFor', function(e) {
                console.log('revup.searchFor: ' + e.searchValue)
            })
            .on('revup.searchValue', function(e) {
                console.log('revup.searchValue: ' + e.searchValue)
            })
    }; // loadEventHandlers

    function deleteEventHandler(e, $btn)
    {
        var $eventData = $btn.closest('.eventData');
        var eventId = $eventData.attr('eventId');

        var url = templateData.eventDeleteApi.replace('eventId', eventId)
        $.ajax({
            url: url,
            type: "DELETE"
        }).done(function(r) {
            // remove the entry
            $eventData.remove();
        }).fail(function(r) {
            console.error("Event - unable to delete an event: ", r)
            $('.eventsDataDiv').revupMessageBox({
                headerText: 'Error Deleting Event',
                msg: 'The following error occured when deleting the events and fundraisers: <div style="width:80%;margin: 0 auto;">' + r.smg + '</div>Try again in a few minutes.'
            });
        })
    }; // deleteEventHandler

    function addNewFundraiser($parent)
    {
        // get the class name
        var className = "neworganizer";
        var fundClassName = 'newfundraisertype';
        if ($parent.parent().hasClass('eventEditDiv')) {
            className = "organizerName";
            fundClassName = 'fundraiserType';
        }

        // add the new field
        var sTmp = [];
        sTmp.push('<div class="organizerEntry">');
            sTmp.push('<div class="' + className + '" orgId="-1"></div>');
            sTmp.push('<div class="' + fundClassName + '"></div>');
            sTmp.push('<div class="organizerDeleteBtn icon icon-delete-circle"></div>');
        sTmp.push('</div>');

        var $el = $(sTmp.join(''));
        $('.organizerDiv', $parent).append($el);

        $('.' + className, $el).revupDropDownField({value: ddFundraisersLst,
                                                     valueStartIndex: -1,
                                                     ifStartIndexNeg1Msg: '<span class="clearMsg">Add Fundraiser</span>'});
        $('.' + fundClassName, $el).revupDropDownField({value: fundraiserTypes,
                                                        valueStartIndex: -1,
                                                        ifStartIndexNeg1Msg: '<span class="clearMsg">Select</span>',
                                                        sortList: false});
    } // addNewFundraiser

    function enableDisableNewEventSaveBtn()
    {
        var eventName = $('.newEventName', $addEventDiv).val();
        var eventDate = $('.newEventDate', $addEventDiv).val();

        // make sure there is both a name and date
        var enable = true
        if ((eventName == '') || (eventDate == '')) {
            enable = false;
        }

        // valid the date
        var isValidDate = revupUtils.validDate(eventDate);
        if (!isValidDate) {
            enable = false;
        }

        // see if any fields are in error
        if ($('.eventSection .pricePointContainer .newPricePoint').hasClass('inputFormatError')) {
            enable = false;
        }
        if ($('.eventSection .newGoal').hasClass('inputFormatError')) {
            enable = false;
        }

        if (enable) {
            $('.saveBtn', $addEventDiv).removeClass('disabled')
        }
        else {
            $('.saveBtn', $addEventDiv).addClass('disabled')
        }
    }; // enableDisableNewEventSaveBtn

    return {
        loadAndDisplay: function(pTemplateData)
        {
            // save the template data
            templateData = pTemplateData;
console.log('events->loadAndDisplay: ', templateData);

            // load the events
            load();

            // load event handlers
            loadEventHandlers();
        } // loadAndDisplay
    }
} ());
