var newFilters = (function () {
    // globals
    var templateData = undefined;           // the template setting

    // global selectors
    var $newFilterSection = undefined;      // container holding the new filters
    var $newFiltersDiv = undefined;         // the div holding all the new filters
    var $addFilterBtn = undefined;          // selector for the add button
    var $addFilterMsg = undefined;          // selector to the add filter message (shown when not filters defined);

    var newFiltersDiv = $();
    var $newFilterBtn = $();
    var $newFilterMsg = $();

    function getFilterData(fId)
    {
        var filterData = undefined;
        for (var i = 0; i < templateData.filters.length; i++) {
            if (templateData.filters[i].id == fId) {
                filterData = templateData.filters[i];
                break;
            }
        }

        return filterData;
    } // getFilterData

    function getGrpFilterData(groupType, fId)
    {
        // get the filter definitions for the type
        var gf = templateData.grpFilters[groupType];
        var filter = {};

        // fine the filter
        for (var i = 0; i < gf.length; i++) {
            if (gf[i].id == fId) {
                filter = gf[i];
                break;
            }
        }

        return filter;
    } // getGrpFilterData

    function buildFilterSection(currentFilter)
    {
        var sTmp = [];

        sTmp.push('<div class="newFilterSectionDiv">');
            var extStyle = ' style="width:calc(100% - 10px);"';
            if (!templateData.bHideLeadingIcon) {
                sTmp.push('<div class="icon icon-filter"></div>');
                extStyle = "";
            }
            sTmp.push('<div class="newFiltersDiv"' + extStyle + '>');
                var extraStyle = '';
                if ((currentFilter != null) && (currentFilter.length > 0)) {
                    extraStyle += ' style="display:none;"';
                }
                sTmp.push('<div class="newFilterCntl addFilterDiv"' + extraStyle + '><div class="text"></div></div>');
                if ((currentFilter != null) && (currentFilter.length > 0)) {
                    for (var i = 0; i < currentFilter.length; i++) {
                        // if quick filter are not being displayed then skip any
                        if ((!templateData.bDisplayQuickFilters) && (currentFilter[i].fType == "quick")) {
                            continue;
                        }

                        sTmp.push('<div class="newFilterCntl filterDiv set"');
                            sTmp.push(' filterType="' + currentFilter[i].fType + '"');
                            if (currentFilter[i].fAction) {
                                sTmp.push(' filterAction="' + currentFilter[i].fAction + '"');
                            }
                            if (currentFilter[i].fGroup) {
                                sTmp.push(' filterGroup="' + currentFilter[i].fGroup + '"');
                            }

                            sTmp.push(' filterId="' + currentFilter[i].fId + '"');
                            sTmp.push(' filterValue="' + currentFilter[i].fValue + '"');
                            if (currentFilter[i].fTitle) {
                                sTmp.push(' title="' + currentFilter[i].fTitle + '"');
                            }

                            if (currentFilter[i].fType == 'quick') {
                                sTmp.push(' style="cursor:default;"')
                            }
                        sTmp.push('>');
                            sTmp.push('<div class="text" orgLabel="' + currentFilter[i].fText + '">' + currentFilter[i].fText + '</div>');
                            sTmp.push('<div class="deleteFilter icon icon-close"></div>');
                        sTmp.push('</div>');
                    }
                }
                sTmp.push('<div class="newFilterCntl addFilterBtn"><div class="icon icon-add-filter"></div></div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        $newFilterSection.html(sTmp.join(''));

        // global selectors
        $newFiltersDiv = $('.newFilterSectionDiv .newFiltersDiv', $newFilterSection);
        $newFilterBtn = $('.addFilterBtn', $newFiltersDiv);
        $newFilterMsg = $('.addFilterDiv', $newFiltersDiv);
    }; // buildFilterSection

    // showAsGroup:     display the items in the group as one sub popup
    // showIndividual:  if not display as a group should it be display as individual entries
    // displayTitle:    the title of the group popup
    // displayMenu:     the text to display in the first level popup
    // displayType:     how the popup should be displayed
    //                      a) choice -> use picks which type to display
    //                      b) multi -> each entry is display
    //                      c) single -> only one type is to be displayed
    var grpDesc = {
        imports: {
            showAsGroup: true,
            displayTitle: 'Import Source',
            displayMenu: 'Import',
            displayType: 'choice',
        },
        demographics: {
            showAsGroup: true,
            showIndividual: true,
            displayTitle: 'Demographics',
            displayMenu: 'Demographics',
            displayType: 'choice',
        },
        location: {
            showAsGroup: true,
            displayTitle: 'Location',
            displayMenu: 'Location',
            displayType: 'choice'
        },
        biography: {
            showAsGroup: true,
            //showIndividual: true,
            displayTitle: 'Biography',
            displayMenu: 'Biography',
            displayType: 'choice'
        },
        key_contribs: {
            showAsGroup: true,
            displayTitle: 'Specific Correlated Campaign',
            displayMenu: 'Correlated Campaign',
            displayType: 'single',
        },
    }

    var importIcons = [
        {
            label: 'vcard',
            icon: '/static/img/contactsourceicons/apple.svg',
            iconDisabled: '/static/img/contactsourceicons/apple-disabled.svg',
            value: 'addressbook',
        },
        {
            label: 'Gmail',
            icon: '/static/img/contactsourceicons/gmail.svg',
            iconDisabled: '/static/img/contactsourceicons/gmail-disabled.svg',
            value: 'google-oauth2',
        },
        {
            label: 'Outlook',
            icon: '/static/img/contactsourceicons/outlook.svg',
            iconDisabled: '/static/img/contactsourceicons/outlook-disabled.svg',
            value: 'outlook',
        },
        {
            label: 'LinkedIn',
            icon: '/static/img/contactsourceicons/linkedin.svg',
            iconDisabled: '/static/img/contactsourceicons/linkedin-disabled.svg',
            value: 'linkedin-oauth2',

        },
        {
            label: 'CSV',
            icon: '/static/img/contactsourceicons/csv.svg',
            iconDisabled: '/static/img/contactsourceicons/csv-disabled.svg',
            value: 'csv',
        },
        {
            label: 'Mobile',
            icon: '/static/img/contactsourceicons/mobile.svg',
            iconDisabled: '/static/img/contactsourceicons/mobile-disabled.svg',
            value: 'iphone',
        },
    ]

    function buildFilterPopupContent(disableQuickFilters, indFilterLst)
    {
        var sTmp = [];

        sTmp.push('<div class="filterPopupContent">');
            sTmp.push('<div class="title">Select Filter</div>');

            // scroll up
            sTmp.push('<div class="scrollDiv scrollUp disabled">');
                sTmp.push('<div class="icon icon-triangle-rounded-up"></div>');
            sTmp.push('</div>')

            sTmp.push('<div class="scrollContentOuter">');
                sTmp.push('<div class="scrollContentInner">');
                    // make sure quick filters can be displayed
                    var qf = templateData.quickFilters;
                    if (templateData.bDisplayQuickFilters) {
                        // quick filters
                        var disableClass = "";
                        if (disableQuickFilters) {
                            disableClass = ' disabled';
                        }
                        sTmp.push('<div class="sectionTitle">Quick Filter</div>');
                        sTmp.push('<div class="quickFilterLst">');
                            for (var i = 0; i < qf.length; i++) {
                                sTmp.push('<div class="entry' + disableClass + '" filterType="quick" filterId="' + qf[i].id + '">')
                                    sTmp.push('<div class="text">' + qf[i].desc + '</div>');
                                sTmp.push('</div>');
                            }
                        sTmp.push('</div>');

                        sTmp.push('<div class="sectionSeperator"></div>');
                    }

                    // filters
                    var f = templateData.filters;
                    var gf = templateData.grpFilters;
                    sTmp.push('<div class="sectionTitle">Individual Filter</div>');
                    sTmp.push('<div class="filterLst">');
                        /*
                        for (var i = 0; i < f.length; i++) {
                            disableClass = '';
                            if (indFilterLst.indexOf(f[i].id) != -1) {
                                disableClass = ' disabled'
                            }
                            sTmp.push('<div class="entry' + disableClass + '" filterType="indFilter" filterAction="' + f[i].action + '" filterId="' + f[i].id + '">')
                                sTmp.push('<div class="text">' + f[i].label + '</div>');
                            sTmp.push('</div>');
                        }

                        sTmp.push('<div class="entry">')
                            sTmp.push('<div>------------------------</div>')
                        sTmp.push('</div>')
                        */

                        for (var grp in gf) {
                            if (grpDesc[grp].showAsGroup) {
                                sTmp.push('<div class="entry" filterType="group" grpName="' + grp + '">')
                                    sTmp.push('<div class="text group">' + grpDesc[grp].displayMenu + '</div>');
                                sTmp.push('</div>');
                            }
                            else if (grpDesc[grp].showIndividual) {
                                var gFilter = gf[grp];
                                for (var i = 0; i < gFilter.length; i++) {
                                    sTmp.push('<div class="entry' + disableClass + '" filterType="indFilter" filterAction="' + gFilter[i].action + '" filterId="' + gFilter[i].id + '">')
                                        sTmp.push('<div class="text">' + gFilter[i].label + '</div>');
                                    sTmp.push('</div>');
                                }
                            }
                        }
                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            // scroll down
            sTmp.push('<div class="scrollDiv scrollDown">');
                sTmp.push('<div class="icon icon-triangle-rounded-dn"></div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        return sTmp.join('');
    } // buildFilterPopupContent

    function buildIndependentFilterContent(fId, fVal, fTitle)
    {
        var filterData = getFilterData(fId);

        var sTmp = [];
        sTmp.push('<div class="filterPopupContent">');
            sTmp.push('<div class="returnLnk revupLnk">Select Type</div>');
            sTmp.push('<div class="sectionTitle">' + filterData.label + '</div>');
            sTmp.push('<div class="desc">' + filterData.popupDesc + '</div>');
            if (filterData.displayType === "dropdown") {
                sTmp.push('<div class="dropdown"></div>');
            }
            else if (filterData.displayType === 'choices') {
                var c = filterData.choiceData;
                for (var i = 0; i < c.length; i++) {
                    var bType = 'icon-radio-button-off';
                    if ((fVal != undefined && fVal === "") && (i === 0)) {
                        bType = 'icon-radio-button-on selected';
                    }
                    else if (fVal != undefined && fVal == c[i].value) {
                        bType = 'icon-radio-button-on selected';
                    }
                    sTmp.push('<div class="radioEntry" val="' + c[i].value + '" displayVal="' + c[i].displayValue +'">');
                        sTmp.push('<div class="icon ' + bType + ' radioBtn" radioGrp="' + filterData.action + '"></div>');
                        sTmp.push('<div class="icon icon-radio-button-back radioBackground"></div>');
                        sTmp.push('<div class="radioLabel" radioGrp="' + filterData.action + '">' + c[i].displayValue + '</div>');
                    sTmp.push('</div>')
                }
            }
            else if (filterData.displayType === 'date') {
                sTmp.push('<div class="datefield">');
                    sTmp.push('<input class="dateInput" placeholder="mm/dd/yyyy" type="text">');
                    sTmp.push('<div class="dateInputIcon">');
                        sTmp.push('<div class="icon icon-calendar"></div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            }
            else if (filterData.displayType === 'autocomplete') {
                var resultId = ''
                if (fVal !== '') {
                    resultId = ' resultId="' + fVal + '"';
                }
                sTmp.push('<input type="text" class="autocomplete" value="' + fTitle + '" ' + resultId + 'placeholder="' + filterData.label + '" autofocus>');
            }
            else {
                var extraStuff = '';
                if (filterData.maxLength) {
                    extraStuff += ' maxlength="' + filterData.maxLength + '"';
                }
                sTmp.push('<input type="text" class="filterValue"' + extraStuff + ' value="' + fVal + '" autofocus onfocus="this.value = this.value;">');
            }
            sTmp.push('<div class="btnDiv">');
                sTmp.push('<div class="rightSide">');
                    sTmp.push('<div class="cancelBtn revupLnk">Cancel</div>');
                    sTmp.push('<button class="revupBtnDefault okBtn" disabled>Done</button>')
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        return sTmp.join('');
    } // buildIndependentFilterContent

    function buildGroupFilterContent($filterDiv, groupType, currentFilter, fType, fAction, fId, fValue, fTitle)
    {
        var sTmp = [];

        sTmp.push('<div class="returnLnk revupLnk">&lt;&nbsp;Filter</div>');
        sTmp.push('<div class="filterGroupContent" fGroup="' + groupType + '">');
            sTmp.push('<div class="title">' + grpDesc[groupType].displayTitle + "</div>");

            if (grpDesc[groupType].displayType === 'choice') {
                sTmp.push('<div class="choiceDiv filterChoice">')
                    var gf = templateData.grpFilters[groupType];
                    var nextFree = true;
                    for (var i = 0; i < gf.length; i++) {
                        var extraClass = '';
                        if (gf[i].id == fId) {
                            extraClass = ' selected';
                            nextFree = false;

                            // set the text of the filter div
                            $('.text', $filterDiv).html(gf[i].label);
                        }
                        else if (currentFilter.indexOf(gf[i].id) != -1) {
                            extraClass = ' disabled" style= "pointer-events: none;';
                        }
                        else if (!fAction && nextFree) {
                            extraClass = ' selected';
                            nextFree = false;

                            // set the text of the filter div
                            $('.text', $filterDiv).html(gf[i].label);
                        }
                        else if (fAction === gf[i].action) {
                            extraClass = ' selected';
                            nextFree = false;

                            // set the text of the filter div
                            $('.text', $filterDiv).html(gf[i].label);
                        }

                        sTmp.push('<div class="choiceEntry' + extraClass + '" fId="' + gf[i].id +'" fAction="' + gf[i].action + '">' + gf[i].label + '</div>');
                    }
                sTmp.push('</div>');
            }
            else if (grpDesc[groupType].displayType === 'multi') {}
            else if (grpDesc[groupType].displayType === 'single') {}

            // description div
            sTmp.push('<div class="descDiv"></div>');

            // filter details
            sTmp.push('<div class="filterDetailsDiv"></div>')
        sTmp.push('</div>');

            // add the buttons
            sTmp.push('<div class="btnDiv">');
                sTmp.push('<div class="rightSide">');
                    sTmp.push('<div class="cancelBtn revupLnk">Cancel</div>');
                    sTmp.push('<button class="revupBtnDefault okBtn" disabled>Done</button>')
                sTmp.push('</div>');
            sTmp.push('</div>');
        //sTmp.push('</div>');

        return sTmp.join('');
    } // buildGroupFilterContent

    /*
     * Display filter select popup
     */
    var $popup;
    var $verifyDlg;
    function displaySelectFilter($filterDiv)
    {
        // build the content of the popup
        var $quickFilters = $('.filterDiv[filterType="quick"]', $newFiltersDiv);
        var $indFilter = $('.filterDiv[filterType="group"]', $newFiltersDiv);
        var currentFilter = [];
        $indFilter.each(function() {
            var fId = parseInt($(this).attr('filterId'), 10);
            currentFilter.push(fId);
        });
        var popupContent = buildFilterPopupContent(($quickFilters.length > 0), currentFilter);

        // get the new element just add and display pooup
        $popup = $filterDiv.revupPopup({
            extraClass: 'newFilterPopup',
            location: 'below',
            content: popupContent,
            minLeft: 10,
            maxRight: 415,
            //bEscClose: false,
            //bDoNotCloseOverlay: true,
            bPassThrough: false,
        });

        // event handlers for the popup
        $('.entry', $popup)
            .off('mouseover')
            .on('mouseover', function(e) {
                if (!$(this).hasClass('disabled')) {
                    $(this).addClass('over pageLabel');
                }
            })
            .off('mouseout')
            .on('mouseout', function(e) {
                $(this).removeClass('over pageLabel');
            })
            .off('click')
            .on('click', function(e) {
                if ($(this).hasClass('disabled')) {
                    e.stopPropagation();
                    return;
                }

                // get the filter type and id
                var $entry = $(this);
                var filterType = $entry.attr('filterType');
                var filterAction = $entry.attr('filterAction')
                var filterId = $entry.attr('filterId');

                // close the filter popup
                $popup.revupPopup('close');

                // set the filter attributes
                if (filterType == "quick") {
                    setQuickFilter($filterDiv, filterType, filterId);
                }
                else if (filterType == 'indFilter') {
                    setIndependentFilter($filterDiv, filterType, filterAction, filterId);
                }
                else if (filterType === 'group') {
                    var groupType = $entry.attr('grpName');
                    setGroupFilter($filterDiv, groupType, currentFilter);
                }
            });

        // scrolling
        var $scrollContentOuter = $('.scrollContentOuter', $popup);
        var $scrollContentInner = $('.scrollContentInner', $popup);
        var maxOffset = $scrollContentInner.height() - $scrollContentOuter.height();
        var scrollTimer = null;
        var $div = $();
        if (maxOffset <= 0) {
            $('.scrollDiv.scrollUp').addClass('disabled');
            $('.scrollDiv.scrollDown').addClass('disabled');
        }

        var bMouseScrollDown = false;
        function scroll()
        {
            // see if disabled
            if ($div.hasClass('disabled')) {
                return;
            }

            // get the current location
            var currentOffset = $scrollContentOuter.attr('currentOffset');
            if (currentOffset === undefined) {
                currentOffset = 0;
            }
            else {
                currentOffset = parseInt(currentOffset, 10);
            }

            // update offset
            if ($div.hasClass('scrollUp')) {
                currentOffset += 19;
            }
            else if ($div.hasClass('scrollDown')) {
                currentOffset -= 19;
            }
            else {
                console.error('unknown scrolling direction')
            }


            // set the limits and enable/disable arrows when needed
            if (Math.abs(currentOffset) >= maxOffset) {
                currentOffset = -maxOffset;
                $('.scrollDiv.scrollUp').removeClass('disabled');
                $('.scrollDiv.scrollDown').addClass('disabled');
            }
            else if (currentOffset >= 0) {
                currentOffset = 0;
                $('.scrollDiv.scrollUp').addClass('disabled');
                $('.scrollDiv.scrollDown').removeClass('disabled');
            }
            else {
                $('.scrollDiv.scrollUp').removeClass('disabled');
                $('.scrollDiv.scrollDown').removeClass('disabled');
            }

            // scroll
            $scrollContentInner.animate({top: currentOffset + 'px'}, 250, 'linear', function() {
                if (bMouseScrollDown) {
                    scroll();
                }
            })

            // update current position
            $scrollContentOuter.attr('currentOffset', currentOffset);
        } // scroll

        $('.scrollDiv', $popup)
            .on('mousedown', function(e) {
                $div = $(this);

                scroll();

                bMouseScrollDown = true;

                // start the timer
                //scrollTimer = setInterval(scroll, 450);

                return false;
            })
            .on('mouseup', function(e) {
                //clearInterval(scrollTimer);
                bMouseScrollDown = false;

                return false;
            })
    } // displaySelectFilter

    /*
     *  Build the filter string used for query the filter results.  Also store it in
     *  local storage so it came be reloaded
     */
    function buildFilterQueryString()
    {
        var filterLst = [];
        var storageLst = [];

        // walk the filter divs
        var $filterDivs = $('.filterDiv.set', $newFiltersDiv);
        $filterDivs.each(function() {
            // get the filter attributes
            var $filter = $(this);
            var fType = $filter.attr('filterType');
            var fGroup = $filter.attr('filterGroup');
            var fId = $filter.attr('filterId');
            var fValue = $filter.attr('filterValue');
            var fAction = $filter.attr('filterAction');
            var fTitle = $filter.attr('title');
            var fText = $('.text', $filter).html();

            // build a save object
            var o = new Object;
            o.fType = fType;
            if (fGroup) {
                o.fGroup = fGroup;
            }
            o.fId = fId;
            o.fValue = fValue;
            if (fAction) {
                o.fAction = fAction;
            }
            if (fTitle) {
                o.fTitle = fTitle;
            }
            o.fText = fText;
            storageLst.push(o);

            // build the filter string parts
            var filterString = 'filter=';
            if ((fType == 'indFilter')  || (fType == 'group')) {
                var filterString = 'filter=' + fAction + ':' + fValue;
                filterLst.push(filterString);
            }
            else if (fType == 'quick') {
                var v = fValue.split(';;');
                for (var i = 0; i < v.length; i++) {
                    //var filterString = 'filter=quick:' + v[i];
                    var filterString = 'filter=' + v[i];
                    filterLst.push(filterString)
                }
            }
        });

        // store in localStorage
        if (templateData.filterFor != '') {
            let forTab = '';
            if (templateData.whichTab) {
                forTab = templateData.whichTab();
            }
            localStorage.setItem(templateData.filterFor + 'FilterAdv' + forTab + 'Data-' + templateData.filterVersion + '-' + templateData.seatId, JSON.stringify(storageLst));
            localStorage.setItem(templateData.filterFor + 'Filter' + forTab + '-' + templateData.filterVersion + '-' + templateData.seatId, filterLst.join('&'));
        }

        // send an event to the parent
        $newFilterSection.trigger({
            type: 'revup.newFilter',
            filterString: filterLst.join('&'),
        })

        return filterLst.join('&');
    } // buildFilterQueryString

    /*
     * Group Filter - loadDetail
     */
    function loadFilter(groupType, fId, fVal, fTitle)
    {
        if (!fVal) {
            fVal = '';
        }
        if (!fTitle) {
            fTitle = '';
        }

        // get the description section and empty them
        var $descDiv = $('.filterGroupContent .descDiv');
        var $detailDiv = $('.filterGroupContent .filterDetailsDiv');
        $descDiv.empty();
        $detailDiv.empty();

        // get the filter definitions for the type
        var filter = getGrpFilterData(groupType, fId);

        // load the description
        $descDiv.html(filter.popupDesc);

        // load the filter display type
        var sTmp = [];
        if (filter.displayType === 'text') {
            var extraStuff = '';
            if (filter.maxLength) {
                extraStuff = ' maxLength="' + filter.maxLength + '"';
            }

            // create the details
            sTmp.push('<input type="text" class="filterValue"' + extraStuff + ' value="' + fVal + '" autofocus onfocus="this.value = this.value;">');

            // display
            $detailDiv.html(sTmp.join(''));

            // add any handlers is needed
            if (filter.displayTypeDetails === 'integer') {
                $('.filterValue', $popup).numericOnly({bInteger: true});
            }

            // set focus
            $('.filterValue', $popup).focus()
            $('.filterValue', $popup).val($('.filterValue', $popup).val());
        }
        else if (filter.displayType === 'dropdown') {
            //if (filter.displayTypeDetails === 'contact_icon_dropdown') {}
            //else if (filter.displayTypeDetails === 'date') {}
            //else {
                sTmp.push('<div class="dropdown"></div>');
            //}

            // set the the init value
            var valIndex = -1;
            if (fVal) {
                for (var i = 0; i < filter.dropdownData.length; i++) {
                    if (filter.dropdownData[i].value === fVal) {
                        valIndex = i;
                        break;
                    }
                }
            }

            // display
            $detailDiv.html(sTmp.join(''));

            $('.dropdown', $popup).revupDropDownField({
                value: filter.dropdownData,
                valueStartIndex: valIndex,
                sortDescending: filter.bSortOrderDecending,
                ifStartIndexNeg1Msg: " ",
                changeFunc: function() {
                    $('.okBtn', $popup).prop('disabled', false);
                }
            })

            // set focus
            $('.dropdown', $popup).focus();
        }
        else if (filter.displayType === 'choices') {
            if (filter.displayTypeDetails === 'contact_src_icon') {
                sTmp.push('<div class="importTypeDiv">');
                    for (var i = 0; i < importIcons.length; i++) {
                        // get the correct icon and see if enabled
                        var icon = importIcons[i].iconDisabled;
                        var extraClass = ' disabled';
                        var extraIconClass = '';
                        // see if in the choices list and if so enable
                        for (var j = 0; j < filter.choiceData.length; j++) {
                            if (filter.choiceData[j].value === importIcons[i].value) {
                                icon = importIcons[i].icon;
                                extraClass = "";

                                if (filter.choiceData[j].value === fVal) {
                                    extraIconClass = " selected";
                                }

                                break;
                            }
                        }

                        sTmp.push('<div class="importTypeEntry' + extraClass + '" val="' + importIcons[i].value + '">');
                            sTmp.push('<div class="importIconDiv' + extraIconClass + '">');
                                sTmp.push('<img class="importTypeIcon" src="' + icon + '" alt="' + importIcons[i].label + '">');
                            sTmp.push('</div>');
                            sTmp.push('<div class="importTypeLabel">' + importIcons[i].label + '</div>');
                        sTmp.push('</div>');
                    }
                sTmp.push('</div>');
            }
            else {
                var c = filter.choiceData;
                for (var i = 0; i < c.length; i++) {
                    var bType = 'icon-radio-button-off';
                    /*
                    if ((fVal != undefined && fVal === "") && (i === 0)) {
                        bType = 'icon-radio-button-on selected';
                    }
                    */

                    if (fVal != undefined && fVal == c[i].value) {
                        bType = 'icon-radio-button-on selected';
                    }

                    sTmp.push('<div class="radioEntry" val="' + c[i].value + '" displayVal="' + c[i].displayValue +'">');
                        sTmp.push('<div class="icon ' + bType + ' radioBtn" radioGrp="' + filter.action + '"></div>');
                        sTmp.push('<div class="icon icon-radio-button-back radioBackground"></div>');
                        sTmp.push('<div class="radioLabel" radioGrp="' + filter.action + '">' + c[i].displayValue + '</div>');
                    sTmp.push('</div>')
                }
            }

            // display
            $detailDiv.html(sTmp.join(''));
        }
        else if (filter.displayType === 'autocomplete') {
            var resultId = ''
            if (fVal !== '') {
                resultId = ' resultId="' + fVal + '"';
            }
            sTmp.push('<input type="text" class="autocomplete" value="' + fTitle + '" ' + resultId + 'placeholder="' + filter.label + '" autofocus>');

            // display
            $detailDiv.html(sTmp.join(''));

            // add handler
            $('.autocomplete', $popup).revupAutocomplete({
                url: filter.autocompleteUrl,
                resultDisplay: 'label',
                resultId: 'id'
            })

            // set focus
            $('.autocomplete', $popup).focus();
            $('.autocomplete', $popup).val($('.autocomplete', $popup).val());
        }
    } // loadFilter

    function loadSingleFilter($filterDiv, groupType, fId, fVal, fTitle)
    {
        if (!fVal) {
            fVal = '';
        }
        if (!fTitle) {
            fTitle = '';
        }

        // get the description section and empty them
        var $descDiv = $('.filterGroupContent .descDiv');
        var $detailDiv = $('.filterGroupContent .filterDetailsDiv');
        $descDiv.empty();
        $detailDiv.empty();

        // get the filter definitions for the type
        var filter = templateData.grpFilters[groupType];
        filter = filter[0];

        // save the filter id in the group content
        $('.filterGroupContent').attr('fId', filter.id);

        // set the text of the filter div
        $('.text', $filterDiv).html(filter.label);

        var sTmp = [];
        if (filter.displayType === 'choices') {
            sTmp.push('<div class="choiceDiv cols2">')
            var c = filter.choiceData;
                for (var i = 0; i < c.length; i++) {
                    var bType = 'icon-radio-button-off';
                    /*
                    if ((fVal != undefined && fVal === "") && (i === 0)) {
                        bType = 'icon-radio-button-on selected';
                    }
                    */

                    if (fVal != undefined && fVal == c[i].value) {
                        bType = 'icon-radio-button-on selected';
                    }

                    sTmp.push('<div class="choiceEntry" val="' + c[i].value + '">' + c[i].displayValue +'</div>');
                }
            sTmp.push('</div>');
            // display
            $detailDiv.html(sTmp.join(''));
        }

        // load the description
        $descDiv.html(filter.popupDesc);
    } // loadSingleFilter

    /*
     * build the filter string
     */
    function buildFilterString($filterDiv, filterData, filterGroup)
    {
        // done button
        var val = "";
        var displayVal = "";
        if (filterData.displayType == 'dropdown') {
            var v = $('.dropdown', $popup).revupDropDownField('getBothValues');
            val = v.value;
            displayVal = v.displayValue;
        }
        else if (filterData.displayType === 'date') {
            val = $('.dateInput', $popup).val();
            displayVal = val;

            if (val != '') {
                val = val.split('/');
                val = val[2] + '-' + val[0] + '-' + val[1];
            }
        }
        else if (filterData.displayType == 'choices') {
            if (filterData.displayTypeDetails == "contact_src_icon") {
                var $selected = $('.importTypeDiv .importTypeEntry .importIconDiv.selected', $popup);
                var $importTypeEntry = $selected.closest('.importTypeEntry');

                val = $importTypeEntry.attr('val');
                displayVal = $('.importTypeLabel', $importTypeEntry).text();
            }
            else {
                var $selected = $('.radioBtn.selected', $popup).parent();
                if ($selected.length == 0) {
                    $selected = $('.choiceEntry.selected', $popup);
                }

                val = $selected.attr('val');
                displayVal = $selected.attr('displayVal');
            }
        }
        else if (filterData.displayType === 'autocomplete') {
            var $autoComp = $('.autocomplete', $popup);

            val = $autoComp.attr('resultId');
            displayVal = $autoComp.val();
        }
        else if (filterData.displayType === 'text') {
            val = $('.filterValue', $popup).val();
            displayVal = val;
        }
        else {
            console.warn('Unknow filter: ', filterData);
            return;
        }

        // set the attributes
        //var $filterDiv = $(this).closest('.filterDiv');
        $filterDiv.addClass('set');
        $filterDiv.attr('filterType', 'group');
        $filterDiv.attr('filterGroup', filterGroup);
        $filterDiv.attr('filterAction', filterData.action);
        $filterDiv.attr('filterId', filterData.id);
        $filterDiv.attr('filterValue', val);

        // create the title (tooltip text)
        var titleText = filterData.label + ': ' + displayVal;
        $filterDiv.attr('title', titleText)

        // build the filter query string
        buildFilterQueryString();
    } // buildFilterString

    /*
     * Set filters
     */
    function setQuickFilter($filterDiv, qfType, qfTypeId)
    {
        // set the color and attributes
        $filterDiv.addClass('set');
        $filterDiv.attr('filterType', qfType);
        $filterDiv.attr('filterId', qfTypeId);

        //get the index to the quick filters
        var qf = templateData.quickFilters;
        var qfData = undefined
        for (var i = 0; i < qf.length; i++) {
            if (qf[i].id == qfTypeId) {
                qfData = qf[i];
                break;
            }
        }

        // get the attributes/properties based on the specific quick filter
        $filterDiv.attr('filterValue', qfData.value);
        $filterDiv.prop('title', qfData.desc);
        $('.text', $filterDiv).html(qfData.label);

        // because the quick filter entries are not editable change the cursors
        $filterDiv.css('cursor', 'default');

        // build the filter query string
        buildFilterQueryString();
    } // setQuickFilter

    function setGroupFilter($filterDiv, groupType, currentFilter, filterType, filterAction, filterId, filterValue, filterTitle)
    {
        // build the group filter content
        var popupContent = buildGroupFilterContent($filterDiv, groupType, currentFilter, filterType, filterAction, filterId, filterValue, filterTitle);

        // get the new element just add and display popup
        if ($popup && $popup.length > 0) {
            $popup.revupPopup('close');
        }
        $popup = $filterDiv.revupPopup({
            extraClass: 'newGroupFilterPopup',
            location: 'below',
            content: popupContent,
            minLeft: 10, //130,
            maxRight: 415,
            //bEscClose: false,
            //bDoNotCloseOverlay: true,
            bPassThrough: false,
        });

        if (grpDesc[groupType].displayType === 'choice') {
            // get the selected choice
            var $selected = $('.filterGroupContent .choiceDiv .choiceEntry.selected');
            var fId = $selected.attr('fId');

            loadFilter(groupType, fId, filterValue, filterTitle);
        }
        else if (grpDesc[groupType].displayType === 'multi') {}
        else if (grpDesc[groupType].displayType === 'single') {
            loadSingleFilter($filterDiv, groupType, fId);
        }

        // choose handler
        $('.filterGroupContent .filterChoice', $popup).on('click', '.choiceEntry', function(e) {
            var $btn = $(this);

            // update the screen
            var grpType = $('.filterGroupContent').attr('fGroup');
            var fId = $btn.attr('fId');
            loadFilter(grpType, fId)

            // set the text of the filter div
            var filterData = getGrpFilterData(grpType, fId);
            $('.text', $filterDiv).html(filterData.label);
        })

        // handlers for different filter types
        $popup
            .on('click', '.returnLnk', function(e) {
                // back button
                // close detail popup
                $popup.revupPopup('close');

                // reset the text of the filter div
                $('.text', $filterDiv).html('New Filter');

                // reset / clear attributes and classes
                $filterDiv.removeClass('set');
                $filterDiv.removeAttr('filterType');
                $filterDiv.removeAttr('filterAction');
                $filterDiv.removeAttr('filterId');
                $filterDiv.removeAttr('filterValue');
                $filterDiv.removeAttr('title');
                $filterDiv.removeAttr('style');

                // display the select filter popup
                displaySelectFilter($filterDiv);

                // build the filter query string
                buildFilterQueryString();
            })
            .on('click', '.cancelBtn', function(e) {
                // see if the ok button is enabled and if so then put up a message box
                var $okBtn = $('.okBtn', $popup);
                var disabled = $okBtn.prop('disabled');
                if (!disabled) {
                    $verifyDlg = $popup.revupConfirmBox({
                        okBtnText: "Yes",
                        headerText: "Cancel Filter",
                        msg: "You have not saved your changes.<br><br>Are you sure you wish to cancel?",
                        zIndex: 25,
                        isDragable: false,
                        autoCloseOk: false,
                        okHandler: function() {
                            // close the popup
                            $popup.revupPopup('close');

                            // see if a new or an existing 0 if new delete the created div and if existing close with no change
                            var fId = $filterDiv.attr('filterId');
                            if (fId === undefined) {
                                // remove the new filter div
                                $filterDiv.remove();

                                // see of the add message should be displayed
                                var $filterDivs = $('.filterDiv', $newFiltersDiv);
                                if ($filterDivs.length == 0) {
                                    $newFilterMsg.show();
                                };
                            }

                            // see if the title needs to be reset
                            var orgLabel = $('.text', $filterDiv).attr('orgLabel');
                            if (orgLabel) {
                                $('.text', $filterDiv).html(orgLabel);
                            }

                            $verifyDlg.revupConfirmBox('close');

                            return true;
                        },
                        cancelHandler: undefined,
                        cancelBtnText: '',
                        midBtnText: 'No',
                        midBtnHandler: function () {
                            return false;
                        }
                    });

                    return;
                }

                // cancel button
                // close the popup
                $popup.revupPopup('close');

                // see if a new or an existing 0 if new delete the created div and if existing close with no change
                var fId = $filterDiv.attr('filterId');
                if (fId === undefined) {
                    // remove the new filter div
                    $filterDiv.remove();

                    // see of the add message should be displayed
                    var $filterDivs = $('.filterDiv', $newFiltersDiv);
                    if ($filterDivs.length == 0) {
                        $newFilterMsg.show();
                    };
                }

                // see if the title needs to be reset
                var orgLabel = $('.text', $filterDiv).attr('orgLabel');
                if (orgLabel) {
                    $('.text', $filterDiv).html(orgLabel);
                }

                // stop the default from happening
                e.stopPropagation();

                return false;
            })
            .on('click', '.okBtn', function(e) {
                var $btn = $(this);
                var $filterGroupContent = $('.filterGroupContent', $popup);

                // get the group type
                var fGroup = $filterGroupContent.attr('fGroup');
                var fId;
                var gd = grpDesc[fGroup];
                if (gd.displayType === 'choice') {
                    var $selected = $('.choiceDiv.filterChoice .choiceEntry.selected')
                    fId = $selected.attr('fId');
                }
                else if (grpDesc[groupType].displayType === 'single') {
                    fId = $('.filterGroupContent', $popup).attr('fId');
                }

                // get the filter definitions for the type and build the filter string
                var filter = getGrpFilterData(groupType, fId);
                buildFilterString($filterDiv, filter, groupType)

                // close the popup
                $popup.revupPopup('close');
            })
            .on('click', '.choiceDiv .choiceEntry', function(e) {
                var $btn = $(this);

                // if disabled done
                if ($btn.hasClass('disabled')) {
                    return;
                }

                // unselect and select the new button
                $('.filterGroupContent .choiceDiv .choiceEntry').removeClass('selected');
                $btn.addClass('selected');

                // see if the ok button should be enabled
                var $p = $btn.parent();
                if ($p.hasClass('filterChoice')) {
                    return;
                }

                $('.okBtn', $popup).prop('disabled', false);
            })
            .on('keyup cut paste', '.filterValue', function(e) {
                var $this = $(this);

                // get current value
                var v = $this.val();

                // make sure there is data, no error, and the correct length
                var maxlength = $this.prop('maxlength');
                var bOk = true;
                if ((v.length <= 0) || ($this.hasClass('inputFormatError'))) {
                    $('.okBtn', $popup).prop('disabled', true);
                    bOk = false;
                }
                else if ((maxlength != -1) && (v.length !== maxlength)) {
                    $('.okBtn', $popup).prop('disabled', true);
                    bOk = false;
                }
                else {
                    $('.okBtn', $popup).prop('disabled', false);
                }

                // handle return when there is a value
                if ((e.keyCode == 13) && (bOk)) {
                    if ($('.okBtn', $popup).prop('disabled')) {
                        // close the popup
                        $popup.revupPopup('close');
                    }
                    else {
                        $('.okBtn', $popup).trigger('click');
                    }

                    e.stopPropagation();
                    return;
                }

                // stop the default from happening
                e.stopPropagation();
            })
            .on('click', '.radioEntry', function(e) {
                var $e = $(this);
                var $rBtn = $('.radioBtn', $e);

                if ($rBtn.hasClass('selected')) {
                    return;
                }

                // clear current selected
                $parent = $rBtn.parent().parent();
                $selected = $('.selected', $parent);
                $selected.removeClass('selected')
                         .removeClass('icon-radio-button-on')
                         .addClass('icon-radio-button-off');

                // set selected
                $rBtn.addClass('selected')
                     .addClass('icon-radio-button-on')
                     .removeClass('icon-radio-button-off');

                // enable the ok button
                $('.okBtn', $popup).prop('disabled', false);
            })
            .on('click', '.importTypeDiv .importTypeEntry', function(e) {
                var $btn = $(this);

                // if disabled
                if ($btn.hasClass('disabled')) {
                    return;
                }

                //unselect
                $parent = $btn.parent();
                $('.importTypeEntry .importIconDiv', $parent).removeClass('selected');

                // select
                $('.importIconDiv', $btn).addClass('selected');

                // enable the ok button
                $('.okBtn', $popup).prop('disabled', false);
            })
            .on('revup.autocomplete.inputChanged', '.autocomplete', function(r) {
                // disable the ok button
                $('.okBtn', $popup).prop('disabled', true);
            })
            .on('revup.autocomplete.valueSelected', '.autocomplete', function(r) {
                // disable the ok button
                $('.okBtn', $popup).prop('disabled', false);
            });

    } // setGroupFilter

    function setIndependentFilter($filterDiv, fType, fAction, fId, fVal, fTitle)
    {
        // if no value is passed in then make it blank
        if (!fVal) {
            fVal = '';
        }
        if (!fTitle) {
            fTitle = '';
        }

        // set the text of the filter div
        var filterData = getFilterData(fId);
        $('.text', $filterDiv).html(filterData.label);

        // create the new popup
        var popupMsg = buildIndependentFilterContent(fId, fVal, fTitle);

        // get the new element just add and display popup
        if ($popup && $popup.length > 0) {
            $popup.revupPopup('close');
        }
        $popup = $filterDiv.revupPopup({
            extraClass: 'newIndependentFilterPopup',
            location: 'below',
            content: popupMsg,
            minLeft: 130,
            maxRight: 415,
        });
        $('.filterValue', $popup).focus();

        if (filterData.displayType === 'dropdown') {
            var valIndex = -1;
            if (fVal !== '') {
                for (var i = 0; i < filterData.dropdownData.length; i++) {
                    if (filterData.dropdownData[i].value == fVal) {
                        valIndex = i;
                        break;
                    }
                }
            }

            $('.dropdown', $popup).revupDropDownField({
                value: filterData.dropdownData,
                valueStartIndex: valIndex,
                sortDescending: true,
                ifStartIndexNeg1Msg: " ",
                changeFunc: function() {
                    $('.okBtn', $popup).prop('disabled', false);
                }
            })
        }
        else if (filterData.displayType === 'date') {
            $('.dateInput', $popup)
                .keydown(function (event) {
                    if (event.which === 13) {
                        var tabEvent = jQuery.Event("keydown");
                        tabEvent.which = 9;
                        tabEvent.keyCode = 9;
                        $(this).trigger(tabEvent);

                        return false;
                    }
                })
                .datepicker({
                    autoclose: true,
                    //clearBtn: true,
                    //todayBtn: true,
                    todayHighlight: true,
                    orientation: 'auto top',
                    format: "mm/dd/yyyy",
                })
                .on('show', function(e) {
                    $('.dateInput', $popup).css('border-color', revupConstants.color.primaryGray4)
                    $('.dateInputIcon .icon', $popup).css('color', revupConstants.color.primaryGray4)
                })
                .on('hide', function(e) {
                    $('.dateInput', $popup).css('border-color', '')
                    $('.dateInputIcon .icon', $popup).css('color', '')
                });

            $('.dateInputIcon', $popup).on('click', function(e) {
                $('.dateInput', $popup).datepicker('show');
            });

            // input filter
            $('.dateInput', $popup).numericOnly({bDate: true});

            $('.dateInput', $popup).on('input change changeDate', function(e) {
                /*
                if (e.type == 'changeDate') {
                    $(this).removeClass('inputFormatError');
                }
                else*/ if ((e.type == 'change') || (e.type == 'input')) {
                    var val = $(this).val();
                    var regex = /^((0?[13578]|10|12)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[01]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1}))|(0?[2469]|11)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[0]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1})))$/

                    // test the number - trigger even as way display different at different fields
                    if ((val == '') || (val.search(regex) != -1)) {
                        $(this).removeClass('inputFormatError');
                    }
                    else {
                        $(this).addClass('inputFormatError');
                    }
                }

                if ($('.dateInput', $popup).hasClass('inputFormatError')) {
                    $('.okBtn', $popup).prop('disabled', true);
                }
                else {
                    $('.okBtn', $popup).prop('disabled', false);
                }
            })

            // if there is a value set it
            if (fVal != '') {
                var date = revupUtils.formatDate(fVal);
                $('.dateInput', $popup).datepicker('setDate', date).datepicker('update');
            }

        }
        else if (filterData.displayType == 'choices') {
            // enable the ok button
            $('.okBtn', $popup).prop('disabled', false);
        }
        else if (filterData.displayType === 'autocomplete') {
            $('.autocomplete', $popup).revupAutocomplete({
                url: filterData.autocompleteUrl,
                resultDisplay: 'label',
                resultId: 'id'
            })
        }
        else if (filterData.displayType === 'text' && filterData.displayTypeDetails) {
            if (filterData.displayTypeDetails === 'integer') {
                $('.filterValue', $popup).numericOnly({bInteger: true});
            }
        }

        // event handlers
        $popup
            .on('click', '.returnLnk', function(e) {
                // back button
                // close detail popup
                $popup.revupPopup('close');

                // reset the text of the filter div
                $('.text', $filterDiv).html('New Filter');

                // reset / clear attributes and classes
                $filterDiv.removeClass('set');
                $filterDiv.removeAttr('filterType');
                $filterDiv.removeAttr('filterAction');
                $filterDiv.removeAttr('filterId');
                $filterDiv.removeAttr('filterValue');
                $filterDiv.removeAttr('title');
                $filterDiv.removeAttr('style');

                // display the select filter popup
                displaySelectFilter($filterDiv);

                // build the filter query string
                buildFilterQueryString();
            })
            .on('click', '.cancelBtn', function(e) {
                // cancel button
                // close the popup
                $popup.revupPopup('close');

                // see if a new or an existing 0 if new delete the created div and if existing close with no change
                var fId = $filterDiv.attr('filterId');
                if (fId === undefined) {
                    // remove the new filter div
                    $filterDiv.remove();

                    // see of the add message should be displayed
                    var $filterDivs = $('.filterDiv', $newFiltersDiv);
                    if ($filterDivs.length == 0) {
                        $newFilterMsg.show();
                    };
                }

                // stop the default from happening
                e.stopPropagation();

                // build the filter query string
                // buildFilterQueryString();

                return false;
            })
            .on('click', '.okBtn', function(e) {
                var filterData = getFilterData(fId);

                // done button
                var val = "";
                var displayVal = "";
                if (filterData.displayType == 'dropdown') {
                    var v = $('.dropdown', $popup).revupDropDownField('getBothValues');
                    val = v.value;
                    displayVal = v.displayValue;
                }
                else if (filterData.displayType === 'date') {
                    val = $('.dateInput', $popup).val();
                    displayVal = val;

                    if (val != '') {
                        val = val.split('/');
                        val = val[2] + '-' + val[0] + '-' + val[1];
                    }
                }
                else if (filterData.displayType == 'choices') {
                    var $selected = $('.radioBtn.selected', $popup).parent();

                    val = $selected.attr('val');
                    displayVal = $selected.attr('displayVal');
                }
                else if (filterData.displayType === 'autocomplete') {
                    var $autoComp = $('.autocomplete', $popup);

                    val = $autoComp.attr('resultId');
                    displayVal = $autoComp.val();
                }
                else {
                    val = $('.filterValue', $popup).val();
                    displayVal = val;
                }

                // set the attributes
                $filterDiv.addClass('set');
                $filterDiv.attr('filterType', fType);
                $filterDiv.attr('filterAction', filterData.action);
                $filterDiv.attr('filterId', fId);
                $filterDiv.attr('filterValue', val);

                // create the title (tooltip text)
                var titleText = filterData.label + ': ' + displayVal;
                $filterDiv.attr('title', titleText)

                // close the popup
                $popup.revupPopup('close');

                // build the filter query string
                buildFilterQueryString();
            })
            .on('keyup cut paste', '.filterValue', function(e) {
                var $this = $(this);

                // get current value
                var v = $this.val();

                // make sure there is data, no error, and the correct length
                var maxlength = $this.prop('maxlength');
                var bOk = true;
                if ((v.length <= 0) || ($this.hasClass('inputFormatError'))) {
                    $('.okBtn', $popup).prop('disabled', true);
                    bOk = false;
                }
                else if ((maxlength != -1) && (v.length !== maxlength)) {
                    $('.okBtn', $popup).prop('disabled', true);
                    bOk = false;
                }
                else {
                    $('.okBtn', $popup).prop('disabled', false);
                }

                // handle return when there is a value
                if ((e.keyCode == 13) && (bOk)) {
                    if ($('.okBtn', $popup).prop('disabled')) {
                        // close the popup
                        $popup.revupPopup('close');
                    }
                    else {
                        $('.okBtn', $popup).trigger('click');
                    }

                    e.stopPropagation();
                    return;
                }

                // stop the default from happening
                e.stopPropagation();
            })
            .on('click', '.radioEntry', function(e) {
                var $e = $(this);
                var $rBtn = $('.radioBtn', $e);

                if ($rBtn.hasClass('selected')) {
                    return;
                }

                // clear current selected
                $parent = $rBtn.parent().parent();
                $selected = $('.selected', $parent);
                $selected.removeClass('selected')
                         .removeClass('icon-radio-button-on')
                         .addClass('icon-radio-button-off');

                // set selected
                $rBtn.addClass('selected')
                     .addClass('icon-radio-button-on')
                     .removeClass('icon-radio-button-off');


                // enable the ok button
                $('.okBtn', $popup).prop('disabled', false);
            })
            .on('revup.autocomplete.inputChanged', '.autocomplete', function(r) {
                // disable the ok button
                $('.okBtn', $popup).prop('disabled', true);
            })
            .on('revup.autocomplete.valueSelected', '.autocomplete', function(r) {
                // disable the ok button
                $('.okBtn', $popup).prop('disabled', false);
            });
    } // setIndependentFilter

    /*
     * Add Filter methods
     */
    function addFilterHandler(e)
    {
        var sTmp = [];

        // make sure the message div is hidden
        $newFilterMsg.hide();

        // create the entry
        sTmp.push('<div class="newFilterCntl filterDiv">')
            sTmp.push('<div class="text">New Filter</div>');
            sTmp.push('<div class="deleteFilter icon icon-close"></div>')
        sTmp.push('</div>');

        // add to the div
        $newFilterBtn.before(sTmp.join(''));

        // get the new element just added
        var cntls = $('.newFilterCntl', $newFiltersDiv);
        var $newFilter = $(cntls.get(cntls.length - 2));

        // display the new filter popup
        displaySelectFilter($newFilter);
    }

    /*
     * Event handlers
     */
     var $filterDiv;
    function loadEventHandlers()
    {
        // add filters Button
        $newFilterBtn
            .on('click',
                function(e) {
                    if (!$(this).hasClass('disabled')) {
                        addFilterHandler(e);
                    }

                    // stop the default from happening
                    e.stopPropagation();
                });

        // delegates for newFiltersDiv
        $newFiltersDiv
            .on('click', '.filterDiv .deleteFilter', function(e) {
                $filterDiv = $(this).closest('.filterDiv');
                $filterDiv.remove();

                // see of the add message should be displayed
                var $filterDivs = $('.filterDiv', $newFiltersDiv);
                if ($filterDivs.length == 0) {
                    $newFilterMsg.show();
                };

                // build the filter string
                buildFilterQueryString();

                // stop the default from happening
                e.stopPropagation();
            })
            .on('click', '.filterDiv', function(e) {
                var $filter = $(this);

                // see what type of filter this is
                var filterType = $filter.attr('filterType');
                var filterGroup = $filter.attr('filterGroup');
                var filterId = $filter.attr('filterId');
                var filterValue = $filter.attr('filterValue');
                var filterAction = $filter.attr('filterAction');
                var filterTitle = $filter.attr('title');
                if (filterTitle) {
                    var t = filterTitle.split(':');
                    if (t.length == 2) {
                        filterTitle = $.trim(t[1]);
                    }
                    else {
                        filterTitle = $.trim($filter.attr('title'));
                    }
                }
                else {
                    filterTitle = "";
                }
                if (!filterType) {
                    // no filter defined so display the select filter
                    displaySelectFilter($filter)
                }
                else if (filterType == 'indFilter') {
                    setIndependentFilter($filter, filterType, filterAction, filterId, filterValue, filterTitle)
                }
                else if (filterType == 'group') {
                    var $indFilter = $('.filterDiv[filterType="group"]', $newFiltersDiv);
                    var currentFilter = [];
                    $indFilter.each(function() {
                        var fId = parseInt($(this).attr('filterId'), 10);
                        currentFilter.push(fId);
                    });
                    setGroupFilter($filter, filterGroup, currentFilter, filterType, filterAction, filterId, filterValue, filterTitle);
                }

                // stop the default from happening
                e.stopPropagation();
            })
            /*
            .on('click', function(e) {
                addFilterHandler(e);

                // stop the default from happening
                e.stopPropagation();
            })
            */
    } // loadEventHandlers

    return {
        init: function($container, initData, currentFilter)
        {
            $newFilterSection = $container;
            templateData = initData;

            if (!templateData.filterFor) {
                templateData.filterFor = 'ranking';
            }

            // build/display
            buildFilterSection(currentFilter);

            // attach/load event handlers
            loadEventHandlers();
        }, // init

        reset: function()
        {
            var $filterDivs = $('.filterDiv.set', $newFiltersDiv);
            $filterDivs.each(function() {
                $(this).remove();
            });

            // display add message should be displayed
            $newFilterMsg.show();

            // reset local storage
            if (templateData.filterFor != '') {
                let forTab = '';
                if (templateData.whichTab) {
                    forTab = templateData.whichTab();
                }
                localStorage.removeItem(templateData.filterFor + 'FilterAdv' + forTab + 'Data-' + templateData.filterVersion + '-' + templateData.seatId);
                localStorage.removeItem(templateData.filterFor + 'Filter' + forTab + '-' + templateData.filterVersion + '-' + templateData.seatId);
            }
        }, // reset

        loadAdvanceFilterData: function()
        {
            var currentFilter = undefined;
            if (templateData.filterFor != '') {
                let forTab = '';
                if (templateData.whichTab) {
                    forTab = templateData.whichTab();
                }
                currentFilter = localStorage.getItem(templateData.filterFor + 'FilterAdv' + forTab + 'Data-' + templateData.filterVersion + '-' + templateData.seatId);
                if (currentFilter) {
                    currentFilter = JSON.parse(currentFilter);
                }
            }
            buildFilterSection(currentFilter);
            buildFilterQueryString();

            // attach/load event handlers
            loadEventHandlers();
        }, // loadAdvanceFilterData

        getFilterForContactSet: function()
        {
            var filter = {};

            // walk the filter divs
            var $filterDivs = $('.filterDiv.set', $newFiltersDiv);
            $filterDivs.each(function() {
                // get the filter attributes
                var $filter = $(this);
                var fType = $filter.attr('filterType');
                var fGroup = $filter.attr('filterGroup');
                var fId = $filter.attr('filterId');
                var fValue = $filter.attr('filterValue');
                var fAction = $filter.attr('filterAction');
                var fTitle = $filter.attr('title');
                var fText = $('.text', $filter).html();

                if (fType == 'quick') {
                    var v = fValue.split(';;');

                    for (var i = 0; i < v.length; i++) {
                        var vv = v[i].split(':');

                        filter[vv[0]] = vv[1] == 'true';
                    }
                }
                else {
                    filter[fAction] = [];
                    filter[fAction].push(fValue);
                }
            });

            return filter;
        }, // getFilterForContactSet

        decodeForListMgr: function(filterQuery)
        {
            var filters = templateData.filters;
            var qFilters = templateData.quickFilters;

            var sTmp = [];
            var qTmp = [];
            for (var key in filterQuery) {
                // first walk the list of filters
                var bFound = false;
                for (var i = 0; i < filters.length; i++) {
                    if (key == filters[i].action) {
                        sTmp.push(filters[i].label + ': ' + filterQuery[key])

                        bFound = true;
                    }
                }

                // if not found the add to the quick filter list
                if (!bFound) {
                    qTmp.push(key + ':' + filterQuery[key]);
                }
            }

            // see if there are any quick filter parts
            var qLabel = '';
            if (qTmp.length > 0) {
                for (var i = 0; i < qFilters.length; i++) {
                    val = qFilters[i].value;
                    val = val.split(';;');

                    var numMatch = 0;
                    for(var q = 0; q < qTmp.length; q++) {
                        if (val.length == qTmp.length) {
                            for(var v = 0; v < val.length; v++) {
                                if (val[v] == qTmp[q]) {
                                    numMatch++
                                }
                            }
                        }

                        // see if found
                        if (numMatch == val.length) {
                            qLabel = qFilters[i].label;
                            break;
                        }
                    }
                }
            }

            var tmp = [];
            if (qLabel != '') {
                tmp.push(qLabel);
            }
            if (sTmp.length > 0) {
                tmp.push(sTmp.join(', '));
            }

            return tmp.join(', ');
        }, //decodeForListMgr

        enableDisable: function(bDisable)
        {
            this.reset();

            if (bDisable) {
                $newFilterBtn.addClass('disabled');
                $newFiltersDiv.addClass('disabled')
            }
            else {
                $newFilterBtn.removeClass('disabled');
                $newFiltersDiv.removeClass('disabled')
            }
        }, // enableDisable

        getFilter: function()
        {
            return buildFilterQueryString();
        }, // buildFilterQueryString

        isEmpty: function()
        {
            var $filterDivs = $('.filterDiv.set', $newFiltersDiv);

            return $filterDivs.length == 0;
        } // isEmpty
    }
} ());
