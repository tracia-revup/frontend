var fundraisers = (function () {
    // globals
    var templateData = null;

    var rolesLst = [];              // list of roles

    // timer used to control arrow keys
    var upDownTimer = null;         // time for arrow keys
    var upDownDelay = 500;              // amount of time to wait to fire event

    // selectors
    var $raiser                 = $('.newRaiserDiv');
    var $raisersLoading         = $('.newRaisersLoading', $raiser);

    // selectors - list side
    var $raisersLstLayout       = $('.newRaiserLayout .lstSection', $raiser);
    var $raisersLstDivHeader    = $('.newRaiserHeader', $raisersLstLayout);
    var $raisersLstLoading      = $('.lstLoadDiv', $raisersLstLayout);
    var $raisersLst             = $('.lstDiv', $raisersLstLayout);
    var $raisersHeader          = $('.headerDiv', $raisersLstLayout);
    var $raisersLstFooter       = $('.newRaiserFooter', $raisersLstLayout);

    // selectors - details side
    var $raisersDetailsLayout   = $('.newRaiserLayout .detailSection', $raiser);
    var $raisersDetailsLoading  = $('.detailLoading', $raisersDetailsLayout);
    var $raisersDetails         = $('.outerDetailDiv .detailDiv', $raisersDetailsLayout);
    var $notesEditor            = $([]);

    // notes editor
    var bNotesActive      =  false;      // flag if the notes editor is active or not
    var $notesBoldBtn           = $([]);
    var $notesItalicBtn         = $([]);
    var $notesUnderlineBtn      = $([]);
    var $notesOrderLstBtn       = $([]);
    var $notesUnorderLstBtn     = $([]);

    // event lists
    var orgEventsLst            = [];       // orginal events from the backend
    var orgEventsDetailLst      = [];       // orginal events detail
    var eventDeleteLst          = [];       // list of events to delete from user
    var eventAddLst             = [];       // list of events to add to the user

    // raiser list pagination
    var entriesPerPage          = 20;       // number of entries returned.

    /*
     *
     * List Side
     *
     */

    /*
    * formatOrganizers
    */

    function buildSliderBar(bBlank)
    {
        var sTmp = [];

        var blank = "";
        if (bBlank) {
            blank = ' blank';
        }
        sTmp.push('<div class="slider' + blank + '">');
            if (!bBlank) {
                sTmp.push('<div class="sOuter"></div>');
                    sTmp.push('<div class="sInner"></div>');
                        sTmp.push('<div class="sCenter"></div>')
                    sTmp.push('<div class="sInner"></div>');
                sTmp.push('<div class="sOuter"></div>');
            }
        sTmp.push('</div>');

        return sTmp.join('')
    } // buildSliderBar

    function addRaiserEntry(entry)
    {
        var sTmp = [];
        var user = entry.user;
        // console.log('entry: ', entry)
        sTmp.push('<div class="entry" orgId="' + entry.id + '" userId="' + user.id + '">');
            // name
            sTmp.push('<div class="column colName">');
                var name = ""
                if ((user.first_name != "") || (user.last_name != "")) {
                    if (user.first_name != "") {
                        name += user.first_name;
                    }

                    if (name != "") {
                        name += " ";
                    }

                    if (user.last != "") {
                        name += user.last_name;
                    }
                }

                if ((name == '') && (user.is_active)) {
                    name = user.email
                }

                if (entry.user.id === templateData.accountId) {
                    name += ' (Account Head)';
                }

                if (name == '') {
                    name = '-'
                }

                sTmp.push(name);
            sTmp.push('</div>');

            sTmp.push(buildSliderBar(true));

            // email
            sTmp.push('<div class="column colEmail">');
                sTmp.push(user.email);
            sTmp.push('</div>');

            sTmp.push(buildSliderBar(true));

            // last activity
            sTmp.push('<div class="column colStatus">');
                if ((entry.last_activity ==  undefined) || (entry.last_activity == "")) {
                    var iDate = entry.date_assigned.split('-');
                    iDate = iDate[1] + '/' + iDate[2] + '/' + iDate[0];
                    iDate = 'Invitation Pending (invited ' + iDate + ')';
                    sTmp.push(iDate)
                }
                else {
                    var lLogin = entry.last_activity;
                    //lLogin = lLogin.split('T');
                    //lLogin = lLogin[0];
                    //lLogin = lLogin.split('-');
                    lLogin = 'Last login - ' + revupUtils.formatDate(lLogin);
                    sTmp.push(lLogin);
                }
            sTmp.push('</div>');
        sTmp.push('</div>');

        // append to list
        $raisersLst.append(sTmp.join(''));

        // get the entry and add the data to it
        var $entry = $('.entry', $raisersLst).last().data(entry);
        $entry.data('entryData', entry);
    } // addRaiserEntry

    function formatNewRaiserEntry()
    {
        var sTmp = [];
        sTmp.push('<div class="entry selected newRaiser">');
            // name
            sTmp.push('<div class="column colName">');
                sTmp.push('New Raiser');
            sTmp.push('</div>');

            // email
            sTmp.push('<div class="column colEmail">');
                sTmp.push('');
            sTmp.push('</div>');

            // last activity
            sTmp.push('<div class="column colStatus">');
                sTmp.push('--');
            sTmp.push('</div>');
        sTmp.push('</div>');

        return sTmp.join('');
    } // formatNewRaiserEntry

    function formatOrganizers(orgLst, bReload)
    {
        var sTmp = [];

        // if a reload - clear out the list first
        if (bReload) {
            $raisersLst.html('');
        }

        for (var i = 0; i < orgLst.length; i++) {
            addRaiserEntry(orgLst[i]);
        }

        // mark the first entry as selected
        var $f = $('.entry', $raisersLst).first();
        $f.addClass('selected');

        // load the details of the selected entry
        var orgId = $f.attr('orgId');
        var userId = $f.attr('userId');
        loadDetail(orgId, userId);
    }; // formatOrganizers

    /*
     *
     * Details Side
     *
     */
    /*
     * Load Details
     */
    function formatHeaderSection(entry)
    {
        var sTmp = [];

        // set the state of the resend and revoke invite buttons
        var resendExtra = " disabled";
        var revokeExtra = "";
        var sendExtra = "";
        var emailAddr = "";
        if (!entry) {
            resendExtra = " disabled";
            revokeExtra = " disabled";
            sendExtra = " disabled";
        }
        else {
            if ((entry.state.toLowerCase() == "pending") || (entry.state.toLowerCase() == "pn")) {
                resendExtra = "";
            }
            else if ((entry.state.toLowerCase() == "active") || (entry.state.toLowerCase() == "ac")) {
                // Can't remove yourself and can't remove account head
                if ((entry.user.id === templateData.userId) || (entry.user.id === templateData.accountId)) {
                    revokeExtra = " disabled"
                }
            }
            else {
                revokeExtra = " disabled";
            }

            emailAddr = entry.user.email;
        }

        sTmp.push('<div class="raiserDetailHeaderDiv">');
            sTmp.push('<div class="btnDiv' + resendExtra + ' raiserResendInvitationBtn">');
                sTmp.push('<div class="text">Resend Invite</div>');
                sTmp.push('<div class="icon icon-invite"></div>');
            sTmp.push('</div>');
            sTmp.push('<div class="sepBar"></div>');
            sTmp.push('<div class="btnDiv' + revokeExtra + ' raiserRevokeInvitationBtn">');
                sTmp.push('<div class="text">Revoke Invite</div>');
                sTmp.push('<div class="icon icon-revoke-invite"></div>');
            sTmp.push('</div>');
            sTmp.push('<div class="sepBar"></div>');
            sTmp.push('<div class="btnDiv' + sendExtra + ' sendMailBtn" email="' + emailAddr + '">');
                sTmp.push('<div class="text">Send Email</div>');
                sTmp.push('<div class="icon icon-email"></div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        return sTmp.join('');
    } // formatHeaderSection

    function formatCreateRaiserNameSection()
    {
        // name and email
        var sTmp = [];

        sTmp.push('<div class="createNameEmailDiv">');
            sTmp.push('<div class="nameSection">');
                sTmp.push('<div class="namePart firstNameDiv">');
                    sTmp.push('<div class="text">First Name:</div>');
                    sTmp.push('<input type="text" class="input firstName" placeholder="First Name">')
                sTmp.push('</div>');
                sTmp.push('<div class="namePart lastNameDiv">');
                    sTmp.push('<div class="text">Last Name:</div>');
                    sTmp.push('<input type="text" class="input lastName" placeholder="Last Name">')
                sTmp.push('</div>');
            sTmp.push('</div>');

            sTmp.push('<div class="emailSection">');
                sTmp.push('<div class="emailPart">');
                    sTmp.push('<div class="text">Email Address:</div>');
                    sTmp.push('<input type="text" class="input emailAddr" placeholder="Email Address">')
                sTmp.push('</div>');
            sTmp.push('</div>');

            sTmp.push('<div class="msg">');
                sTmp.push('Click &quot;Create&quot; to add the raiser and send an invitation to join your RevUp account.')
            sTmp.push('</div>');
        sTmp.push('</div>');

        return sTmp.join('');
    } // ormatCreateRaiserNameSection

    function formatNameSection(entry)
    {
        // user
        var user = entry.user;

        // name and email
        var sTmp = [];

        sTmp.push('<div class="nameEmailDiv">');
            // create the name
            var name = [];
            if ((user.first_name != undefined) && (user.first_name != '')) {
                name.push(user.first_name);
            }
            if ((user.last_name != undefined) && (user.last_name != '')) {
                name.push(user.last_name);
            }
            if (name.length > 0) {
                name = name.join(' ');
            }
            else {
                name = '-';
            }

            sTmp.push('<div class="nameEmailView">');
                if (name != "") {
                    sTmp.push('<div class="name">' + name + '</div>');
                }

                if ((user.email != undefined) && (user.email != '')) {
                    sTmp.push('<div class="email"><a class="mailAnchor" href="mailto:' + user.email + '">' + user.email + '</a></div>');
                }

                // last activity
                sTmp.push('<div class="statusMsg">');
                    if ((entry.last_activity ==  undefined) || (entry.last_activity == "")) {
                        var iDate = entry.date_assigned.split('-');
                        iDate = iDate[1] + '/' + iDate[2] + '/' + iDate[0];
                        iDate = 'Invitation Pending (invited ' + iDate + ')';
                        sTmp.push(iDate)
                    }
                    else {
                        var lLogin = entry.last_activity;
                        lLogin = 'Last login - ' + revupUtils.formatDate(lLogin);
                        sTmp.push(lLogin);
                    }
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        return sTmp.join('');
    } // formatNameSection

    function formatRoleSection(entry)
    {
        var sTmp = [];

        // find the default role
        var numRoles = rolesLst.length;
        var defRoleIndex = 0;
        for (var i = 0; i < numRoles; i++) {
            if (rolesLst[i].is_a_seat_default) {
                defRoleIndex = i;
                break;
            }
        }

        // Can't remove yourself and can't remove account head
        var btnDisabledClass = ""
        if (entry && ((entry.user.id === templateData.userId) || (entry.user.id === templateData.accountId))) {
            btnDisabledClass = " disabled";
        }

        sTmp.push('<div class="roleSection">');
            var roleId = rolesLst[defRoleIndex].id;
            if (entry) {
                var userRoles = entry.roles;
                var role;
                var roleName = '<span class="noRole">No Roles</span>';
                if ((userRoles == undefined) || (userRoles.length == 0)) {
                    roleId = rolesLst[defRoleIndex].id;
                }
                else {
                    var role = userRoles[0];
                    roleId = role.id;
                }
            }

            sTmp.push('<div class="roleView">');
                sTmp.push('<div class="title">');
                    sTmp.push('Role');
                    if (btnDisabledClass != "") {
                        if (entry.user.id === templateData.userId){
                            sTmp.push(' (Cannot modify your own role)');
                        }
                        else if (entry.user.id === templateData.accountId){
                            sTmp.push(' (Cannot modify Account Head)');
                        }
                    sTmp.push(':');
                    }
                sTmp.push('</div>');

                sTmp.push('<div class="roleBtnDiv"  initialRoleId="' + roleId + '">');
                    for (var i = 0; i < numRoles; i++) {
                        var c = "roleBtn";
                        if (roleId == rolesLst[i].id) {
                            c += " selected revupPrimaryBtnColor";
                        }
                        c += btnDisabledClass;
                        sTmp.push('<div class="' + c + '" roleId="' + rolesLst[i].id + '" roleIndex="' + i + '" title="' + rolesLst[i].description +'">');
                            sTmp.push(rolesLst[i].name);
                        sTmp.push('</div>')
                    }
                sTmp.push('</div>');
            sTmp.push('</div>')
        sTmp.push('</div>');

        return sTmp.join('');
    } // formatRoleSection

    function formatNotesSection(notes)
    {
        var sTmp = [];

        sTmp.push('<div class="notesSection">');
            sTmp.push('<div class="notesView">');
                sTmp.push('<div class="labelBtnDiv">');
                    sTmp.push('<div class="leftSide">');
                        sTmp.push('<div class="title">Notes:</div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="rightSide">');
                        sTmp.push('<div class="formatBtns">');
                            sTmp.push('<button class="revupBtnDefault btnIcon editFormatBtn editBoldBtn">');
                                sTmp.push('<div class="icon icon-bold"></div>')
                            sTmp.push('</button>');
                            sTmp.push('<button class="revupBtnDefault btnIcon editFormatBtn italicBoldBtn">');
                                sTmp.push('<div class="icon icon-italic"></div>')
                            sTmp.push('</button>');
                            sTmp.push('<button class="revupBtnDefault btnIcon editFormatBtn underlineBoldBtn">');
                                sTmp.push('<div class="icon icon-underline"></div>')
                            sTmp.push('</button>');

                            sTmp.push('<button class="revupBtnDefault btnIcon leadSpacer editFormatBtn indentBtn">')
                                sTmp.push('<div class="icon icon-indent-more"></div>')
                            sTmp.push('</button>');
                            sTmp.push('<button class="revupBtnDefault btnIcon editFormatBtn outdentBtn">');
                                sTmp.push('<div class="icon icon-indent-less"></div>')
                            sTmp.push('</button>');

                            sTmp.push('<button class="revupBtnDefault btnIcon leadSpacer editFormatBtn orderLstBtn">')
                                sTmp.push('<div class="icon icon-numbered-list"></div>')
                            sTmp.push('</button>');
                            sTmp.push('<button class="revupBtnDefault btnIcon editFormatBtn unorderLstBtn">')
                                sTmp.push('<div class="icon icon-bulleted-list"></div>')
                            sTmp.push('</button>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
                sTmp.push('<div class="notesEditor" tabIndex="2" contenteditable="true">');
                    sTmp.push(notes);
                sTmp.push('</div>')
            sTmp.push('</div>');
        sTmp.push('</div>');

        return sTmp.join('');
    } // formatNotesSection

    function formatEventsSection(events)
    {
        // console.log('Events: ', events);

        var sTmp = [];

        // initialize the orginal list of events
        orgEventsLst = [];
        orgEventsDetailLst = [];

        sTmp.push('<div class="eventSection">');
            sTmp.push('<div class="eventsView">');
                sTmp.push('<div class="eventLstDiv">');
                    sTmp.push('<div class="header">');
                        sTmp.push('<div class="colName">Events:</div>');
                        sTmp.push('<div class="colDate">Date:</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="lstDiv">');
                        sTmp.push('<div class="lst">')
                            var displayNoEvents = "display:none;";
                            if ((events == undefined) || (events.length == 0)) {
                                displayNoEvents = "";
                            }
                            sTmp.push('<div class="noEvents" style="' + displayNoEvents + '">No Events for this Raiser</div>');
                            for(var i = 0; i < events.length; i++) {
                                sTmp.push('<div class="entry" eventId="' + events[i].id + '" eventLead="' + events[i].is_lead + '">');
                                    sTmp.push('<div class="name">');
                                        sTmp.push(events[i].event_name);
                                    sTmp.push('</div>');

                                    sTmp.push('<div class="date">');
                                        sTmp.push(revupUtils.formatDate(events[i].event_date));
                                    sTmp.push('</div>');
                                sTmp.push('</div>');

                                // add the events to the orginal event list
                                orgEventsLst.push(events[i].id);

                                // original event details
                                var o = new Object();
                                o.id = events[i].id;
                                o.name = events[i].event_name;
                                o.date = revupUtils.formatDate(events[i].event_date);
                                o.eventLead = events[i].is_lead;
                                orgEventsDetailLst.push(o);
                            }
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            sTmp.push('<div class="eventsEdit">');
                sTmp.push('<div class="btnDiv">');
                    sTmp.push('<button class="revupBtnDefault secondary raiserAddEventBtn" type="button">Add to Event</button>');
                sTmp.push('</div>');
                sTmp.push('<div class="eventLstDiv">');
                    sTmp.push('<div class="header">');
                        sTmp.push('<div class="colName">Events:</div>');
                        sTmp.push('<div class="colDate">Date:</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="lstDiv">');
                        sTmp.push('<div class="lst">')
                            var displayNoEvent = "display:none;";
                            if ((events == undefined) || (events.length == 0)) {
                                displayNoEvent = "";
                            }
                            sTmp.push('<div class="noEvents" style="' + displayNoEvent + '">No Events for this Raiser</div>');

                            var len = 0;
                            if (events != undefined) {
                                len = events.length
                            }
                            for(var i = 0; i < len; i++) {
                                sTmp.push('<div class="entry" eventId="' + events[i].id + '" eventLead="' + events[i].is_lead + '">');
                                    sTmp.push('<div class="colName">');
                                        sTmp.push(events[i].event_name);
                                    sTmp.push('</div>');

                                    sTmp.push('<div class="colDate">');
                                        sTmp.push(revupUtils.formatDate(events[i].event_date));
                                    sTmp.push('</div>');

                                    sTmp.push('<div class="colDelete">');
                                        sTmp.push('<div class="deleteBtn">');
                                            sTmp.push('<div class="icon icon-delete-circle"></div>');
                                        sTmp.push('</div>')
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            }
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        return sTmp.join('');
    } // formatEventsSection

    function formatEditBtnSection()
    {
        var sTmp = [];

        sTmp.push('<div class="createRaisedBtnSection">')
            sTmp.push('<div class="btnDiv">');
                sTmp.push('<div class="btnContainerRight">');
                    sTmp.push('<div class="createRaiserCancelBtn revupLnk">Cancel</div>');
                    sTmp.push('<button class="revupBtnDefault createRaiserOkBtn" disabled="">Create</button>');
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        return sTmp.join('');
    } // formatEditBtnSection

    function loadCreateRaisedDetails()
    {
        var sTmp = [];

        // add the names, roles section
        sTmp.push('<div class="newRaiserSection">')
            sTmp.push(formatHeaderSection());
            sTmp.push(formatCreateRaiserNameSection());
            sTmp.push(formatRoleSection());
            sTmp.push(formatNotesSection());
            sTmp.push(formatEditBtnSection());
        sTmp.push('</div>');

        // display
        $raisersDetails.html(sTmp.join(''));

    } // loadCreateRaisedDetails

    function loadDetail(orgId, userId)
    {
        // display loading
        $raisersDetails.hide();
        $raisersDetailsLoading.show();

        var url = templateData.getOrganizerDetails.replace('orgId', orgId) + "?verbosity=2";
        $.ajax({
            url: url,
            type: "GET",
        })
        .done(function(r) {
            var sTmp = [];

            // add the names, roles section
            sTmp.push(formatHeaderSection(r));
            sTmp.push(formatNameSection(r));
            sTmp.push(formatRoleSection(r));
            sTmp.push(formatNotesSection(r.notes));
            if ((templateData.isPoliticalCampaign) || (templateData.isPoliticalCommittee)) {
                sTmp.push(formatEventsSection(r.user.events));
            }

            // display
            $raisersDetails.html(sTmp.join(''));

            // save the user and org id
            $raisersDetails.attr('orgId', orgId);
            $raisersDetails.attr('userId', userId);

            // see if tooltips need to add
            $('.name', $raisersDetails).tooltipOnOverflow();
            $('.email', $raisersDetails).tooltipOnOverflow();

            // enable/disable the update of the notesEditor buttons
            bNotesActive = false;
            $notesBoldBtn = $([]);
            $notesItalicBtn = $([]);
            $notesUnderlineBtn = $([]);
            $notesOrderLstBtn = $([]);
            $notesUnorderLstBtn = $([]);
        })
        .fail(function(r) {
            revupUtils.apiError('Fundraisers', r, 'Unable to load fundraiser details.', 'Unable to load fundraiser details.');
        })
        .always(function(r) {
            // hide loading
            $raisersDetailsLoading.hide()
            $raisersDetails.show();
        })
    }

    /*
     *
     * Popup Functions
     *
     */
    /*
     * add new fundraiser
     */
    function addNewRaiser($b)
    {
        // disable the button
        var $btn = $b;
        $btn.prop('disabled', true);

        // clear selection
        $('.entry', $raisersLst).removeClass('selected');

        // mark disabled
        $('.entry', $raisersLst).addClass('disabled');

        // disable pagination
        $raisersLstFooter.revupPagination('disabled', true);

        // prepend the new raiser
        var newRaiser = formatNewRaiserEntry()
        $raisersLst.prepend(newRaiser);
        // display create detail
        loadCreateRaisedDetails();
    }; // addNewRaiser

    function updateEventsEditLst(lst)
    {
        // Update the events list
        var sTmp = [];

        var displayNoEvents = "display:none;";
        if (lst.length == 0) {
            displayNoEvents = "";
        }
        sTmp.push('<div class="noEvents" style="'+ displayNoEvents + '">No Events for this Raiser</div>');

        for(var i = 0; i < lst.length; i++) {
            sTmp.push('<div class="entry" eventId="' + lst[i].id + '" eventLead="' + lst[i].eventLead + '">');
                sTmp.push('<div class="colName">');
                    sTmp.push(lst[i].name);
                sTmp.push('</div>');

                sTmp.push('<div class="colDate">');
                    sTmp.push(lst[i].date);
                sTmp.push('</div>');

                sTmp.push('<div class="colDelete">');
                    sTmp.push('<div class="deleteBtn">');
                        sTmp.push('<div class="icon icon-delete-circle"></div>');
                    sTmp.push('</div>')
                sTmp.push('</div>');
            sTmp.push('</div>');
        }

        // update the list
        $('.eventSection .eventsEdit .eventLstDiv .lst', $raisersDetails).html(sTmp.join(''));
    } // updateEventsEditLst

    function addFundraiserDlgOkHandler(e, $dlg)
    {
        // build the list of events to delete the user from
        eventDeleteLst = [];
        var $orgSelectedLst = $('.eventLst .eventEntry .selectedEvent[orgSelected=true]', $dlg);
        $orgSelectedLst.each(function() {
            var $selectedEvent = $(this);
            if (!$selectedEvent.attr('checked')) {
                var obj = new Object();

                // get the event id
                var $eventEntry = $selectedEvent.closest('.eventEntry');
                var eventId = parseInt($eventEntry.attr('eventId'), 10);
                obj.id = eventId;

                // get the event name
                var eventName = $('.nameEventDiv', $eventEntry).text();
                obj.name = eventName;

                eventDeleteLst.push(obj);
            }
        })

        // build the list of events to add to the user
        var $currentSelectedLst = $('.eventLst .eventEntry .selectedEvent[checked]', $dlg);
        //var addLst = [];
        //var addNameLst = [];
        eventAddLst = [];
        var updateLst = [];
        $currentSelectedLst.each(function() {
            var $selectedEvent = $(this);
            var orgSelected = $selectedEvent.attr('orgSelected');
            if ((typeof orgSelected).toLowerCase() == 'string') {
                orgSelected = (orgSelected.toLowerCase() == 'true') ? true : false;
            }

            // build the update list
            var o = new Object();
            var $eventEntry = $selectedEvent.closest('.eventEntry');

            // id
            var eventId = parseInt($eventEntry.attr('eventId'), 10);
            o.id = eventId;

            // name
            var eventName = $('.nameEventDiv', $eventEntry).text();
            o.name = eventName;

            // date
            var eventDate = $('.dateEventDiv', $eventEntry).text();
            o.date = eventDate;

            // is the user a lead for this event
            var $leadSwitch = $('.leadSwitchDiv', $eventEntry);
            var switchState = $leadSwitch.revupSlideSwitch('getState');
            o.eventLead = switchState == "on";

            // update list
            updateLst.push(o);

            // new events
            if (!orgSelected) {
                var obj = new Object();
                var $eventEntry = $selectedEvent.closest('.eventEntry');
                var eventId = parseInt($eventEntry.attr('eventId'), 10);
                obj.id = eventId;

                // get the event name
                var eventName = $('.nameEventDiv', $eventEntry).text();
                obj.name = eventName;

                // date
                var eventDate = $('.dateEventDiv', $eventEntry).text();
                obj.date = eventDate;

                // is the user a lead for this event
                var $leadSwitch = $('.leadSwitchDiv', $eventEntry);
                var switchState = $leadSwitch.revupSlideSwitch('getState');
                obj.eventLead = switchState == "on";

                eventAddLst.push(obj);
            }
        });

        // Update the events list
        updateEventsEditLst(updateLst)

        // mark the section change
        $('.eventSection', $raisersDetails).attr('sectionChanged', true);
        $('.btnSection .btnEdit .editDetailsSaveBtn').prop('disabled', false);
    } // addFundraiserDlgOkHandler

    function addFundraiserToEventDlg2(organizerId, userId, $raisersDetailsOwner)
    {
        // build and display the dialog with screen
        var sTmp = [];

        sTmp.push('<div class="searchBar">');
            sTmp.push('<div class="rightSide">')
                sTmp.push('<div class="eventSearch"></div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        // build the header
        sTmp.push('<div class="lstHeader">');
            sTmp.push('<div class="column eventName">Name</div>');
            sTmp.push('<div class="column eventDate">Event Date</div>');
            sTmp.push('<div class="leadSwitch">Lead</div>');
        sTmp.push('</div>');

        sTmp.push('<div class="eventLst">');
            sTmp.push('<div class="loadingDiv">');
                sTmp.push('<div class="loading"></div>');
                sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
            sTmp.push('</div>');
        sTmp.push('</div>');

        var $dlg = $('body').revupDialogBox({
            okBtnText:          'Done',
            cancelBtnText:      'Cancel',
            headerText:         'Add Fundraiser to Event',
            msg:                sTmp.join(''),
            width:              '600px',
            height:             '500px',
            top:                '75',
            extraClass:         'addFundraiserToEventDlg',
            okHandler:          addFundraiserDlgOkHandler,
        });

        // attach the searchbar
        /*
        $('.searchBar .eventSearch', $dlg).revupSearchBox();
        $('.searchBar .eventSearch', $dlg)
            .on('revup.searchClear', function(e) {
                console.log('revup.searchClear')
            })
            .on('revup.searchFor', function(e) {
                console.log('revup.searchFor: ' + e.searchValue)
            })
            .on('revup.searchValue', function(e) {
                console.log('revup.searchValue: ' + e.searchValue)
            })
        */

        // check / uncheck the
        $('.eventLst', $dlg).on('click', '.selectedEvent', function() {
            var $checkBox = $(this);
            var isChecked = $checkBox.attr('checked');
            if (isChecked == undefined) {
                isChecked = false;
            }
            else {
                isChecked = isChecked.toLowerCase() == 'checked' ? true : false;
            }

            var $entry = $checkBox.closest('.eventEntry');
            var $leadSwitch = $('.leadSwitchDiv', $entry);
            if (isChecked) {
                $('.activeEventChecked', $checkBox).remove();
                $checkBox.removeAttr('checked');

                $leadSwitch.revupSlideSwitch('disabled', true);
                $leadSwitch.removeClass('disabled');
            }
            else {
                $checkBox.append('<div class="activeEventChecked icon icon-check"></div>');
                $checkBox.attr('checked', '');

                $leadSwitch.revupSlideSwitch('disabled', false);
                $leadSwitch.addClass('disabled');
            }
        })

        // get the latest events and the user events
        function getEvents() {
            return $.ajax({
                url: templateData.eventGetListApi,
                type: "GET"
            });
        } // getEvents

        // load the dialog
        $.when(getEvents())
            .done(function(eventsResults) {
                    // console.log("eventsResults: ", eventsResults);
                    // build array of events for this raiser from the details list
                    var eventsUserIn = [];
                    $('.eventSection .eventsEdit .eventLstDiv .lst .entry:visible', $raisersDetails).each(function() {
                        var $entry = $(this);
                        var eventId = parseInt($entry.attr("eventId"), 10);
                        eventsUserIn.push(eventId);
                    });

                    var eTmp = [];

                    // build the list of events
                    var events = eventsResults.results;
                    $.each(events, function(i, v) {
                        // is this in the list
                        var checked = "";
                        var isSelectedEvent = false;
                        if (eventsUserIn.indexOf(v.id) != -1) {
                            checked = 'checked="checked"';
                            isSelectedEvent = true;
                        }

                        // is this an orginal event - already selected
                        var orgSelectedEvent = false;
                        if (orgEventsLst.indexOf(v.id) != -1) {
                            orgSelectedEvent = true;
                        }

                        eTmp.push('<div class="eventEntry" eventId="' + v.id + '">');
                            // checkbox - add to event
                            eTmp.push('<div class="activeEventDiv">');
                                eTmp.push('<div class="selectedEvent" ' + checked + ' orgSelected="' + orgSelectedEvent + '">');
                                    eTmp.push('<div class="activeEvent icon icon-box-unchecked"></div>');
                                    if (isSelectedEvent) {
                                        eTmp.push('<div class="activeEventChecked icon icon-check"></div>');
                                    }
                                eTmp.push('</div>');
                            eTmp.push('</div>');

                            // event name
                            eTmp.push('<div class="nameEventDiv">');
                                eTmp.push(v.event_name);
                            eTmp.push('</div>');

                            // event date
                            var d = v.event_date;
                            d = d.split('T');
                            d = d[0].split('-');
                            d = d[1] + '/' + d[2] + '/' + d[0];
                            eTmp.push('<div class="dateEventDiv">' + d + '</div>');

                            // lead switch
                            var disableLeaderSwitchClass = " disabled";
                            if (isSelectedEvent) {
                                disableLeaderSwitchClass = "";
                            }
                            eTmp.push('<div class="leadSwitchDiv' + disableLeaderSwitchClass + '"></div>');
                        eTmp.push('</div>');
                    })
                    $('.eventLst', $dlg).html(eTmp.join(''));

                    $('.eventLst .eventEntry', $dlg).each(function() {
                        var $entry = $(this);
                        var eventId = parseInt($entry.attr("eventId"), 10);
                        var $leadSwitchDiv = $('.leadSwitchDiv', $entry);

                        // see if should be disabled
                        var bDisabled = true;
                        if (eventsUserIn.indexOf(eventId) != -1) {
                            bDisabled = false;

                            var $e = $('.eventSection .eventsEdit .eventLstDiv .lst .entry[eventId=' + eventId + ']', $raisersDetails);
                            var initState = "off";
                            if ($e.attr('eventLead').toLowerCase() == 'true') {
                                initState = "on";
                            }
                        }

                        $leadSwitchDiv.revupSlideSwitch({
                            disabled: bDisabled,
                            initState: initState,
                        });
                    })
            })
            .fail(function() {
                revupUtils.apiError('Fundraisers', r, 'Unable to load events and fundraiser events', "Unable to load the events and fundraiser events.");
            })
    } // addFundraiserToEventDlg2

    /*
     * Update methods
     */
    function updateRole(oldRoleId, newRoleId)
    {
        // get the user and org id
        var orgId = $raisersDetails.attr('orgId');
        var userId = $raisersDetails.attr('userId');

        // build the list of deferred ajax calls
        var deferred = [];
        var delay = 1;

        // update the role
        // build urls
        var delRoleUrl = templateData.deleteRoleFromUserUrl.replace("seatId", orgId).replace("roleId", oldRoleId);
        var addRoleUrl = templateData.addRoleToUserApi.replace("seatId", orgId);

        // delete the old role
        $.ajax({
            url: delRoleUrl,
            type: "DELETE",
        })
        .done (function(r) {
            console.log('role delete: ', r)

            // add the new role
            $.ajax({
                url: addRoleUrl,
                data: {role: newRoleId},
                type: "POST"
            })
            .done(function(r) {
                console.log('add role: ', r)
            })
            .fail(function(r) {
                revupUtils.apiError('Fundraisers', r, 'Unable to create role');
            });

        })
        .fail(function(r) {
            revupUtils.apiError('Fundraisers', r, 'Unable to delete Role');
        })


    } // updateRole

    function updateNotes()
    {
        var orgId = $raisersDetails.attr('orgId');

        // get the notes text
        var notes = $('.notesSection .notesView .notesEditor', $raisersDetails).html();

        // update the notes
        var url = templateData.getOrganizerDetails.replace('orgId', orgId);
        $.ajax({
            url: url,
            type: "PUT",
            data: {notes: notes},
        })
        .done(function(r) {
            // change the change flag
            $('.notesSection', $raisersDetails).attr('sectionChanged', false);

            console.warn('Update Notes Complete: ', r)
        })
        .fail(function(r) {
            revupUtils.apiError('Fundraisers', r, 'Unable to update notes');
        })
    } // updateNotes

    /*
     * Create New Raiser Event methods
     */
    function createEnableDisableCreateBtn()
    {
console.log('edit field change')
        var bEnableCreateBtn = false;

        // get values
        var firstName = $('.detailDiv .newRaiserSection .nameSection .firstName').val();
        var lastName = $('.detailDiv .newRaiserSection .nameSection .lastName').val();
        var emailAddr = $('.detailDiv .newRaiserSection .emailSection .emailAddr').val();
        var notes = $('.detailDiv .newRaiserSection .notesSection .notesView .notesEditor').html();

        // see if there are any values or the roles have changed
        if ((firstName != '') || (lastName != '') || (emailAddr != '') || (notes != '')) {
            $('.detailDiv .newRaiserSection').attr('newRaiserVal', 'true');
        }
        else {
            $('.detailDiv .newRaiserSection').removeAttr('newRaiserVal');
        }

        // see if a valid email address
        if (emailAddr) {
            var isValidEmail = validEmailAddress(emailAddr);
            if (isValidEmail) {
                bEnableCreateBtn = true;
                $('.detailDiv .newRaiserSection .emailSection input').css('border', '1px solid #e2e2e2');
            }
            else{
                $('.detailDiv .newRaiserSection .emailSection input').css('border', '1px solid #a94442');
            }
        }

        // enable/disable create button
        if (bEnableCreateBtn) {
            $('.detailDiv .newRaiserSection .createRaisedBtnSection .createRaiserOkBtn').prop('disabled', false);
        }
        else {
            $('.detailDiv .newRaiserSection .createRaisedBtnSection .createRaiserOkBtn').prop('disabled', true);
        }
    } // createEnableDisableCreateBtn

    function closeCreateNewRaiser()
    {
console.log('closeCreateNewRaiser')
        // remove the entry in the list, if there is one
        $('.entry.newRaiser', $raisersLst).remove();

        // make the first entry in the list as selected and load the details
        var $e = $('.entry:first');
        $e.addClass('selected');

        // get the data need to fetch details for this entry
        var orgId = $e.attr('orgId');
        var userId = $e.attr('userId');
        loadDetail(orgId, userId);

        // enable the add button
        $('.addNewRaiserBtn').prop('disabled', false);

        // clear the disabled of the list entries
        $('.entry', $raisersLst).removeClass('disabled');

        // enable pagination
        $raisersLstFooter.revupPagination('disabled', false);
    } // closeCreateNewRaiser

    function cancelNewRaiser()
    {
        // get values
        var firstName = $('.detailDiv .newRaiserSection .nameSection .firstName').val();
        var lastName = $('.detailDiv .newRaiserSection .nameSection .lastName').val();
        var emailAddr = $('.detailDiv .newRaiserSection .emailSection .emailAddr').val();
        var notes = $('.detailDiv .newRaiserSection .notesSection .notesView .notesEditor').html();
        var bRoleChanged = $('.detailDiv .newRaiserSection .roleSection').attr('sectionChanged');

        // if anything has a value ask the user if they wish to cancel
        if ((firstName != '') || (lastName != '') || (emailAddr != '') || (notes != '') || bRoleChanged) {
            var $conf = $('body').revupConfirmBox({
                headerText: 'Confirm Cancel Create',
                msg: 'Are you sure you want to cancel creation of a new raiser?',
                okBtnText: 'Yes',
                cancelBtnText: 'No',
                okHandler: function() {
                    closeCreateNewRaiser();
                }
            })
        }
        else {
            closeCreateNewRaiser();
        }
    } // cancelNewRaiser

    function createNewRaiser()
    {
        // get values
        var firstName = $('.detailDiv .newRaiserSection .nameSection .firstName').val();
        var lastName = $('.detailDiv .newRaiserSection .nameSection .lastName').val();
        var emailAddr = $('.detailDiv .newRaiserSection .emailSection .emailAddr').val();
        var notes = $('.detailDiv .newRaiserSection .notesSection .notesView .notesEditor').html();
        var role = $('.detailDiv .newRaiserSection .roleSection .roleBtn.selected').attr('roleId');

        var newOrg = {};
        newOrg.email = emailAddr;
        newOrg.role = role;
        if (firstName != '') {
            newOrg.first_name = firstName;
        }
        if (lastName != '') {
            newOrg.last_name = lastName;
        }
        if (notes != '') {
            newOrg.notes = notes;
        }

        // Post the email to the campaign users API to add/create the user
        //disable the Add Raiser button before the ajax is ready
        $('.addNewRaiserBtn').prop('disabled', true);
        $.ajax({
            url: templateData.newOrganizerApi,
            data: newOrg,
            type: "POST"
        })
        .done(function(r) {
          console.log('new raiser: ', r)
            // update the date format
            var d = r.date_assigned;
            d = d.split("T");
            r.date_assigned = d[0];
            // prepend the new raiser
            //var newRaiser = addRaiserEntry(r)
            //$raisersLst.prepend(newRaiser);

            // update the count
            var $seatsFree = $('.newRaiserHeader .seatMsgDiv .seatsFree');
            var seatsFree = Number($seatsFree.text());
            $seatsFree.text(seatsFree - 1);

            // update the new raiser entry
            var $newEntry = $('.entry.newRaiser', $raisersLst);
            $newEntry.attr('orgId', r.id);
            $newEntry.attr('userId', r.user.id);

            // update the name
            var name = [];
            name.push(r.user.first_name);
            name.push(r.user.last_name);
            if (name.length == 0) {
                name = '-'
            }
            else {
                name = name.join(' ');
            }
            $('.colName', $newEntry).html(name);

            // update the email address
            $('.colEmail', $newEntry).html(r.user.email);

            // status
            var iDate = r.date_assigned.split('-');
            iDate = iDate[1] + '/' + iDate[2] + '/' + iDate[0];
            iDate = 'Invitation Pending (invited ' + iDate + ')';
            $('.colStatus', $newEntry).html(iDate);

            // change to selected
            $newEntry.removeClass('newRaiser');

            // update the details page
            loadDetail(r.id, r.user.id);

            // enable the add button
            $('.addNewRaiserBtn').prop('disabled', false);

            // clear the disabled of the list entries
            $('.entry', $raisersLst).removeClass('disabled');

            // enable pagination
            $raisersLstFooter.revupPagination('disabled', false);
        })
        .fail(function(r) {
            var msg = [];
            msg.push("Unable to create new fundraiser for the following reason:");
            msg.push('<div style="width:90%;margin:5px auto">');
                msg.push(r.responseJSON.detail);
            msg.push('</div>');
            msg.push('You can correct the problem and try again');

            revupUtils.apiError('Fundraisers', r, 'Unable to create new fundraisers', msg.join(''));

            // hide the loading
            $raisersDetailsLoading.hide();
            $raisersDetails.show();

        })
        .always(function() {
          //disable the Add Raiser button after the ajax call and between the API responds
          // $('.addNewRaiserBtn').prop('disabled', true);
            // enable the add fundraiser button
            //btn.prop('disabled', false);
        });
    } // createNewRaiser

    /*
     * Event Handlers
     */
    function loadEventHandlers()
    {
        $('.addNewRaiserBtn').on('click', function(e) {
            addNewRaiser($(this));
        });

        /*
         * pagination
         */
        $raisersLstFooter.on('revup.pagination', function(e) {
            // get a new page
            $.ajax({
                    url: templateData.getOrganizerApi + "&page=" + e.pageNumber,
                    type: "GET"
            })
                .done(function(r) {
                    // organizers
                    formatOrganizers(r.results, true);
                })
                .fail(function(r) {
                    revupUtils.apiError('Fundraisers', r, 'Unable to load fundraiser page: ' + e.pageNumber, "Unable to load fundraiser page.");
                })
        })


        // list events
        var bResize = false;
        $raisersLst
            .on('click', '.entry', function(e) {
console.log('change selected')
                // if there is a time clear it
                if (upDownTimer != null) {
                    clearTimeout(upDownTimer);
                    upDownTimer = null;
                }

                // if coming from a resize don't selected
                if (bResize) {
                    bResize = false;

                    e.stopPropagation();

                    return;
                }

                $entry = $(this);
                $lst = $entry.parent();

                // if disabled skip
                if ($entry.hasClass('disabled')) {
                    return;
                }

                // set focus - to the details
                $raisersDetails.focus();

                // see if selected and if so done
                if ($entry.hasClass('selected')) {
                    return;
                }

                // clean selected and update new selected
                $('.entry', $lst).removeClass('selected');
                $entry.addClass('selected');

                // get the data need to fetch details for this entry
                var orgId = $entry.attr('orgId');
                var userId = $entry.attr('userId');
                loadDetail(orgId, userId);

                // stop propagation
                e.stopPropagation();
            })
            .on('keydown', function(e) {
console.log('change selected - keyboard')
                var $entries = $('.entry', $(this));

                // if disabled skip
                if ($entry.hasClass('disabled')) {
                    return;
                }

                // get the index of the selected item
                var selectedIndex = $('.selected', $(this)).index() + 1;
                var numEnties = $entries.length

                // if the control or alt key press skip
                if (e.altKey || e.ctrlKey) {
                    return;
                }

                // compute the new index;
                var newIndex = selectedIndex;
                if (e.keyCode == 38) {
                    // up
                    if (selectedIndex != 1) {
                        newIndex--;
                    }
                }
                else if (e.keyCode == 40) {
                    // down
                    if (selectedIndex < numEnties) {
                        newIndex++;
                    }
                }
                else {
                    // if not the up or down arrow key processing
                    return;
                }

                if (selectedIndex != newIndex) {
                    $('.entry:nth-child(' + selectedIndex + ')', $lst).removeClass('selected');
                    $('.entry:nth-child(' + newIndex + ')', $lst).addClass('selected');
                }

                // if there is a time clear it
                if (upDownTimer != null) {
                    clearTimeout(upDownTimer);
                    upDownTimer = null;
                }

                // set the timer
                upDownTimer = setTimeout(function() {
                    if (selectedIndex != newIndex) {
                        // get the data need to fetch details for this entry
                        var $entry = $('.entry.selected', $raisersLst)
                        var orgId = $entry.attr('orgId');
                        var userId = $entry.attr('userId');
                        loadDetail(orgId, userId);
                    }

                    // clear the function
                    upDownTimer = null;
                }, upDownDelay);


                // stop propagation
                e.stopImmediatePropagation();

                return false;
            });

        $raisersHeader
            .on('mousedown', '.slider', function(e)
            {
                /*
                 * slider
                 */
                // which == 1 means left mouse button
                if (e.which == 1) {
                    // get the left column
                    var $slider = $(this);
                    var $left = $slider.prev();
                    var c = $left.attr('class');
                    c = c.split(' ');
                    for (var i = 0; i < c.length; i++) {
                        c[i] = '.' + $.trim(c[i]);
                    }
                    var $leftGrp = $(c.join(''));
                    var minLeft = parseInt($(c.join(''), $('.headerDiv')).attr('minColWidth'), 10);

                    // get the right column
                    var $right = $slider.next();
                    var c = $right.attr('class');
                    c = c.split(' ');
                    for (var i = 0; i < c.length; i++) {
                        c[i] = '.' + $.trim(c[i]);
                    }
                    var $rightGrp = $(c.join(''));
                    var minRight = parseInt($(c.join(''), $('.headerDiv')).attr('minColWidth'), 10);

                    // make sure there is room to resize
                    if (($left.width() <= minLeft) && ($right.width() <= minRight)) {
                        e.preventDefault();

                        return;
                    }

                    // set the starting point
                    var currentPosX = e.pageX;

                    $slider.addClass('draggable').parents().on('mousemove', function(e) {
                        if ($slider.hasClass('draggable')) {
                            bResize = true;

                            var deltaX = currentPosX - e.pageX;
                            currentPosX = e.pageX;

                            // compute the new size
                            var leftWidth = $left.outerWidth();
                            var rightWidth = $right.outerWidth();
                            var newWidthLeft = leftWidth - deltaX;
                            var newWidthRight = rightWidth + deltaX;

                            // make sure the new size meets the minimum
                            var bResizeCol = true;
                            if (newWidthLeft < minLeft) {
                                var d = leftWidth - minLeft;
                                if (d == 0) {
                                    bResizeCol = false;
                                }
                                else {
                                    newWidthLeft =  minLeft;
                                    newWidthRight = rightWidth + d;
                                }
                            }
                            else if (newWidthRight < minRight) {
                                var d = rightWidth - minRight;
                                if (d == 0) {
                                    bResizeCol = false;
                                }
                                else {
                                    newWidthRight = minRight;
                                    newWidthLeft = leftWidth + d;
                                }
                            }

                            //resize
                            if (bResizeCol) {
                                $leftGrp.outerWidth(newWidthLeft);
                                $rightGrp.outerWidth(newWidthRight);

                                // save the columns width
                                var wData = {};
                                wData.listWidth = $('.newRaiserDiv .lstSection').outerWidth();
                                for(var key in templateData.resizeMinCol) {
                                    // save the column width
                                    wData[key] = $('.column.' + key).outerWidth();
                                }
                                localStorage.setItem('fundraiserColWidth-' + templateData.seatId, JSON.stringify(wData));
                            }
                        }
                    }).on('mouseup', function(e) {
                        // stop dragging
                        $slider.removeClass('draggable')
                    })
                }

                e.preventDefault();
            })
            .on('mouseup', function() {
                $('.draggable').removeClass('draggable')
            });

        // handle the resize of the window
        var startingLstWidth = $('.newRaiserDiv .lstSection').outerWidth();
        var resizeTimer = undefined;
        $(window)
            .on('resize', function(e) {
                // save only if resize of a column
                if (localStorage.getItem('fundraiserColWidth-' + templateData.seatId)) {
                    if (resizeTimer) {
                        clearTimeout(resizeTimer);
                    }

                    resizeTimer = setTimeout(function() {
                        var newWidth = $('.newRaiserDiv .lstSection').outerWidth();
                        var delta = newWidth - startingLstWidth;
                        startingLstWidth = newWidth;

                        // walk and resize the columns
                        for (key in templateData.winResizeCol) {
                            var $c = $('.newRaiserDiv .dataDiv .column.' + key);
                            var newWidth = $c.width();
                            if (delta != 0) {
                                if (templateData.winResizeCol[key]) {
                                    var adjDelta = Math.round(delta * templateData.winResizeCol[key]);
                                    newWidth += adjDelta;
                                }
                            }
                            $c.outerWidth(newWidth);
                        }
                    }, 100);
                }
            })


        // details event handlers
        $raisersDetails
            .on('click', '.roleBtnDiv .roleBtn', function(e) {
                // if the selected button then do nothing
                var $btn = $(this);
                if ($btn.hasClass('selected')) {
                    return;
                }
                else if ($btn.hasClass('disabled')) {
                    return;
                }

                // get the role id's
                var oldRoleId = $('.roleBtnDiv .roleBtn.selected').attr('roleId');
                var newRoleId = $btn.attr('roleId');
console.log('old: ' + oldRoleId +', new: ' + newRoleId)

                // clear all the buttons
                $('.roleBtnDiv .roleBtn', $raisersDetails).removeClass('selected');
                $('.roleBtnDiv .roleBtn', $raisersDetails).removeClass('revupPrimaryBtnColor');

                // make the clicked button as selected
                $btn.addClass('selected');
                $btn.addClass('revupPrimaryBtnColor');

                // mark section as changed and enable the save button
                $('.roleSection', $raisersDetails).attr('sectionChanged', true);

                // see if create new raiser
                if ($btn.closest('.newRaiserSection').length != 0) {
                    createEnableDisableCreateBtn();
                }
                else {
                    // update the role
                    updateRole(oldRoleId, newRoleId)
                }
                // $('.btnSection .btnEdit .editDetailsSaveBtn').prop('disabled', false);
            })
            // invite
            .on('click', '.raiserDetailHeaderDiv .raiserResendInvitationBtn', function() {
                // see if disabled
                if ($(this).hasClass('disabled')) {
                    return;
                }

                // resend an invite
                var $orgEntry = $('.entry.selected', $raisersLst);
                var orgId = $orgEntry.attr('orgId');
                var emailAddr = $('.colEmail', $orgEntry).text();

                var url = templateData.resendOrgInvitApi.replace("seatId", orgId);
                $.ajax({
                    url: url,
                    type: "GET"
                }).done(function(r) {
                    $orgEntry.revupMessageBox({
                        headerText: "Resend Invitation",
                        msg: "Invitation re-sent to " + emailAddr,
                    })
                }).fail(function(r) {
                    revupUtils.apiError('Fundraisers', r, 'Unable to send raiser invite', "Unable to send invitation to fundraiser.");
                })
            })
            .on('click', '.raiserDetailHeaderDiv .raiserRevokeInvitationBtn', function() {
                // see if disabled
                if ($(this).hasClass('disabled')) {
                    return;
                }

                // revoke a invite or delete a user
                var $orgEntry = $('.entry.selected', $raisersLst);
                var orgId = $orgEntry.attr('orgId');
                var email = $('.colEmail', $orgEntry).text();
                var $prev;

                $orgEntry.revupConfirmBox({
                    headerText: "Revoke Invitation",
                    msg: "Are you sure you want to revoke the invitation sent to the follow email address: \"" + email + "\"",
                    okHandler: function() {
                        $.ajax({
                            url: templateData.revokeOrgInvitApi.replace("orgId", orgId),
                            type: "DELETE"
                        }).done(function(r) {
                            $prev = $orgEntry.prev();
                            $orgEntry.animate(
                                {height: 0},
                                function()
                                {
                                    // get the index of the selected entry and adjust for the nth-child
                                    var index = $orgEntry.index();
                                    if (index === 1){
                                    }
                                    // if ((index + 1) == $('.entry', $raisersLst).length)//if deleting the last one
                                    //     index -= 1;
                                    // else
                                    //     index += 1;

                                    if (index == 0)//when deleting the top entry, index = 0 but select the 1st child
                                      index = 1;//1st child

                                    // remove the entry from the list
                                    $orgEntry.remove();

                                    // select the next entry and update details
                                    var $newSelected = $('.entry:nth-child(' + index + ')', $raisersLst)
                                    $newSelected.addClass('selected')

                                    // get the data need to fetch details for this entry
                                    var orgId = $newSelected.attr('orgId');
                                    var userId = $newSelected.attr('userId');
                                    loadDetail(orgId, userId);

                                    // update the count
                                    var $seatsFree = $('.newRaiserHeader .seatMsgDiv .seatsFree');
                                    var seatsFree = Number($seatsFree.text());
                                    $seatsFree.text(seatsFree + 1);
                                }
                            );
                        }).fail(function(r) {
                            revupUtils.apiError('Fundraisers', r, 'Unable to revoke invitation', "Unable to revoke fundraiser invitation.");
                        })
                    }
                })
            })

            // send email
            .on('click', '.raiserDetailHeaderDiv .sendMailBtn', function(e) {
                // revoke a invite or delete a user
                var $orgEntry = $('.entry.selected', $raisersLst);
                var email = $('.colEmail', $orgEntry).text();

                var href = 'mailto:' + email;
                window.location = href;
            })

            // create name and email address
            .on('keyup paste input', '.nameSection .firstName, .nameSection .lastName, .emailSection .emailAddr', function(e) {
                // see if create new raiser
                if ($(this).closest('.newRaiserSection').length != 0) {
                    var firstName = $('.detailDiv .newRaiserSection .nameSection .firstName');
                    var lastName = $('.detailDiv .newRaiserSection .nameSection .lastName');
                    var emailAddr = $('.detailDiv .newRaiserSection .emailSection .emailAddr');

                    //the mac OS has a keyboard shortcut that adds a period with a double-space (system-preference > keyboard > text).
                    //We dont want to allow this ' . ', but anything with a dot is allowed, for example 'R.'
                    firstName.val(firstName.val().replace(' . ', ''));
                    lastName.val(lastName.val().replace(' . ', ''));
                    emailAddr.val(emailAddr.val().replace(' . ', ''));

                    //no empty spaces in the front is allowed
                    firstName.val(firstName.val().replace(/^\s+/g, ''));
                    lastName.val(lastName.val().replace(/^\s+/g, ''));
                    emailAddr.val(emailAddr.val().replace(/^\s+/g, ''));

                    emailAddr.val(emailAddr.val().trim());

                    createEnableDisableCreateBtn();
                }
            })

            .on('focusout', '.nameSection .firstName, .nameSection .lastName, .emailSection .emailAddr', function() {
                var firstName = $('.detailDiv .newRaiserSection .nameSection .firstName');
                var lastName = $('.detailDiv .newRaiserSection .nameSection .lastName');
                var emailAddr = $('.detailDiv .newRaiserSection .emailSection .emailAddr');

                //remove any empty space (in the back at this point) when user has moved to another field
                firstName.val(firstName.val().trim());
                lastName.val(lastName.val().trim());

                //replace any stand alone dots
                firstName.val(firstName.val().replace(' .', ''));
                lastName.val(lastName.val().replace(' .', ''));

                createEnableDisableCreateBtn();
            })

            // notes editor
            .on('focusin', '.notesSection .notesView .notesEditor', function() {
                bNotesActive = true;
            })
            .on('focusout', '.notesSection .notesView .notesEditor', function() {
                bNotesActive = false;
            })
            .on('keyup paste input', '.notesSection .notesView .notesEditor', function() {
                // mark section as changed and enable the save button
                $('.notesSection', $raisersDetails).attr('sectionChanged', true);
                // $('.btnSection .btnEdit .editDetailsSaveBtn').prop('disabled', false);

                // see if create new raiser
                if ($(this).closest('.newRaiserSection').length != 0) {
                    createEnableDisableCreateBtn();
                }
            })
            .on('blur', '.notesSection .notesView .notesEditor', function() {
                if ($('.notesSection', $raisersDetails).attr('sectionChanged')) {
console.log('notes changed')
                    if ($(this).closest('.newRaiserSection').length == 0) {
                        updateNotes();
                    }
                }
            })
            .on('click', '.notesSection .notesView .editBoldBtn', function() {
                var $btn = $(this);
                $notesEditor.focus();
                document.execCommand('bold', false, null);

                $btn.toggleClass('active');
            })
            .on('click', '.notesSection .notesView .italicBoldBtn', function() {
                var $btn = $(this);
                $notesEditor.focus();
                document.execCommand('italic', false, null);

                $btn.toggleClass('active');
            })
            .on('click', '.notesSection .notesView .underlineBoldBtn', function() {
                var $btn = $(this);
                $notesEditor.focus();
                document.execCommand('underline', false, null);

                $btn.toggleClass('active');
            })
            .on('click', '.notesSection .notesView .indentBtn', function() {
                var $btn = $(this);
                $notesEditor.focus();
                document.execCommand('indent', false, null);
            })
            .on('click', '.notesSection .notesView .outdentBtn', function() {
                var $btn = $(this);
                $notesEditor.focus();
                document.execCommand('outdent', false, null);
            })
            .on('click', '.notesSection .notesView .orderLstBtn', function() {
                var $btn = $(this);
                $notesEditor.focus();
                document.execCommand('insertOrderedList', false, null);

                $btn.toggleClass('active');
            })
            .on('click', '.notesSection .notesView .unorderLstBtn', function() {
                var $btn = $(this);
                $notesEditor.focus();
                document.execCommand('insertUnorderedList', false, null);

                $btn.toggleClass('active');
            })

            // events buttons
            .on('click', '.eventSection .btnDiv .raiserAddEventBtn', function(e) {
                var $btn = $(this);
                var $orgEntry = $('.entry.selected', $raisersLst);
                var orgId = parseInt($orgEntry.attr('orgId'), 10);
                var userId = parseInt($orgEntry.attr('userId'), 10);

                addFundraiserToEventDlg2(orgId, userId, $btn);
            })
            .on('click', '.eventSection .eventLstDiv .entry .deleteBtn', function(e) {
                var $btn = $(this);
                var $entry = $btn.closest('.entry');
                var $lst = $entry.closest('.lst');

                // add the deleted item to the eventDeleteLst
                var obj = new Object();
                var eventId = parseInt($entry.attr('eventId'), 10);
                obj.id = eventId;
                var eventName = $('.colName', $entry).text();
                obj.name = eventName;
                eventDeleteLst.push(obj);

                // hide the entry
                $entry.hide();

                // get the number of visible entry and if 0 display the no events message
                var $entryVisible = $('.entry:visible', $lst);
                if ($entryVisible.length == 0) {
                    $('.noEvents', $lst).show();
                }

                // mark the section change
                $('.eventSection', $raisersDetails).attr('sectionChanged', true);
                // $('.btnSection .btnEdit .editDetailsSaveBtn').prop('disabled', false);
            })
            .on('click', '.createRaiserCancelBtn', function(e) {
                cancelNewRaiser();
console.log('cancel create raiser')
            })
            .on('click', '.createRaiserOkBtn', function(e) {

                // display loading
                $raisersDetails.hide();
                $raisersDetailsLoading.show();
                createNewRaiser();
console.log('create raiser')
            });

        // see the current styling state of the editor
        var styleBtnStateInterval = setInterval(function () {
            if ($notesBoldBtn.length == 0) {
                $notesEditor = $('.notesSection .notesView .notesEditor', $raisersDetails);

                $notesBoldBtn = $('.notesSection .notesView .editBoldBtn', $raisersDetails);
                $notesItalicBtn = $('.notesSection .notesView .italicBoldBtn', $raisersDetails);
                $notesUnderlineBtn = $('.notesSection .notesView .underlineBoldBtn', $raisersDetails);
                $notesOrderLstBtn = $('.notesSection .notesView .orderLstBtn', $raisersDetails);
                $notesUnorderLstBtn = $('.notesSection .notesView .unorderLstBtn', $raisersDetails);
            }

            if (bNotesActive) {
                var isBold = document.queryCommandValue("Bold");
                var isItalic = document.queryCommandValue("Italic");
                var isUnderline = document.queryCommandValue('underline');
                var isOrderLst = document.queryCommandValue('insertOrderedList');
                var isUnorderLst = document.queryCommandValue('insertUnorderedList');
                if (isBold === 'true') {
                    $notesBoldBtn.addClass('active');
                } else {
                    $notesBoldBtn.removeClass('active');
                }

                if (isItalic === 'true') {
                    $notesItalicBtn.addClass('active');
                } else {
                    $notesItalicBtn.removeClass('active');
                }

                if (isUnderline === 'true') {
                    $notesUnderlineBtn.addClass('active');
                } else {
                    $notesUnderlineBtn.removeClass('active');
                }

                if (isOrderLst === 'true') {
                    $notesOrderLstBtn.addClass('active');
                } else {
                    $notesOrderLstBtn.removeClass('active');
                }

                if (isUnorderLst === 'true') {
                    $notesUnorderLstBtn.addClass('active');
                } else {
                    $notesUnorderLstBtn.removeClass('active');
                }
            }
            else {
                $notesBoldBtn.removeClass('active');
                $notesItalicBtn.removeClass('active');
                $notesUnderlineBtn.removeClass('active');
                $notesOrderLstBtn.removeClass('active');
                $notesUnorderLstBtn.removeClass('active');
            }
        }, 100);


        // search
        /*
        $('.newRaiserDiv .organizerSearch').revupSearchBox();
        $('.newRaiserDiv .organizerSearch')
            .on('revup.searchClear', function(e) {
                console.log('fundraiser - evup.searchClear')
            })
            .on('revup.searchFor', function(e) {
                console.log('fundraiser - revup.searchFor: ' + e.searchValue)
            })
            .on('revup.searchValue', function(e) {
                console.log('fundraiser - revup.searchValue: ' + e.searchValue)
            })
        */
    }; // loadEventHandlers

    /*
     * Load Organizers
     */
    function load()
    {
        function getOrganizers() {
            return $.ajax({
                    url: templateData.getOrganizerApi,
                    type: "GET"
                })
        }

        function getRoles() {
            return $.ajax({
                    url: templateData.getRolesListApi,
                    type: "GET"
                })
        }

        function getEvents() {
            return $.ajax({
                    url: templateData.eventGetListApi,
                    type: "GET"
                })
        }

        // load the data and display
        $.when(getOrganizers(), getRoles(), getEvents())
            .done(function(orgResults, rolesResults, eventsResults) {
                // organizers
                formatOrganizers(orgResults[0].results);

                // see if columns need to be resized
                var colSize = localStorage.getItem('fundraiserColWidth-' + templateData.seatId);
                if (colSize) {
                    colSize = JSON.parse(colSize);
                    var dataLstWidth = colSize.listWidth;
                    var listWidth = $('.newRaiserDiv .lstSection').outerWidth();
                    var delta = listWidth - dataLstWidth;

                    // walk and resize the columns
                    for (key in colSize) {
                        // skip the width
                        if (key == 'listWidth') {
                            continue;
                        }

                        var $c = $('.newRaiserDiv .dataDiv .column.' + key);
                        var newWidth = colSize[key];
                        if (delta != 0) {
                            if (templateData.winResizeCol[key]) {
                                var adjDelta = Math.floor(delta * templateData.winResizeCol[key]);
                                newWidth += adjDelta;
                            }
                        }
                        $c.outerWidth(newWidth);
                    }
                }

                // setup pagination
                var numPages = Math.ceil(orgResults[0].count / entriesPerPage);
                $raisersLstFooter.revupPagination({numOfPages: numPages,
                                                   numOfEntries: orgResults[0].count,
                                                   entriesPerPage: entriesPerPage,
                                                   bDisplayWhatsShowing: true});

                // roles
                rolesLst = rolesResults[0].results;
            })
            .fail(function(r) {
                revupUtils.apiError('Fundraisers', r, 'Unable to load fundraiser data', "Unable to load fundraiser list and user roles.");
            });

        // add the min column width to the header cells
        var $header = $('.newRaiserDiv .headerDiv .column');
        $header.each(function() {
            var $h = $(this);

            var c = $h.attr('class');
            c = c.split(' ');
            for (var i = 0; i < c.length; i++) {
                if (templateData.resizeMinCol[c[i]]) {
                    $h.attr('minColWidth', templateData.resizeMinCol[c[i]]);
                }
            }
        })

        // load the event handlers
        loadEventHandlers();
    } // load

    return {
        loadAndDisplay: function(pTemplateData)
        {
            // keep passed in data
            templateData = pTemplateData;

            // load the data needed to display fundraisers
            load();
        } // loadAndDisplay
    }
} ());
