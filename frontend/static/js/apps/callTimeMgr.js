var callTimeMgr = (function () {
    /*
     *  Globals
     */
    const entriesPerPageIndividual = 50;      // number of entries per page for individuals
    const entriesPerPageGrp = 50;             // number of entries per page for groups
    const nextPageMarkerIndex   = Math.round(entriesPerPageIndividual * .8);    // where to put marker

    // search
    let individualSearchVal = '';           // search value for individual list
    let grpSearchVal = '';                  // search value for group list

    // selectors
    let $callTimeMgrDiv = $('.callTimeMgrDiv');
    let $callTimeMgrLoading = $('.callTimeMgrLoading', $callTimeMgrDiv);
    let $callTimeMgrlstSection = $('.lstSection', $callTimeMgrDiv);
    let $callTimeMgrLstLoading = $('.callTimeMgrLstLoading', $callTimeMgrlstSection);

    // tab selectors
    let $lstTabs = $('.tabDiv', $callTimeMgrlstSection);
    let $lstTab = $('.tab', $lstTabs);
    let $lstTabIndividual = $('.tab.individualTab', $lstTabs);
    let $lstTabGroup = $('.tab.groupTab', $lstTabs);

    // btn selectors
    let $btnsIndividual = $('.buttonBar .individualBtnDiv', $callTimeMgrlstSection);
    let $btnIndividualAddToGrp = $('.addToGrpBtn', $btnsIndividual);
    let $btnIndividualPrint = $('.printBtn', $btnsIndividual);
    let $btnIndividualDelete = $('.deleteBtn', $btnsIndividual);
    let $individualSearchBar = $('.searchBar', $btnsIndividual);
    let $btnsGrp = $('.buttonBar .grpBtnDiv', $callTimeMgrlstSection);
    let $btnGrpPopupMenu = $('.hamburgerBtn', $btnsGrp);
    let $btnGrpPrint = $('.printBtn', $btnsGrp);
    let $btnGrpAssign = $('.assignBtn', $btnsGrp);
    let $btnGrpUnassign = $('.unassignBtn', $btnsGrp);
    let $btnGrpDelete = $('.deleteBtn', $btnsGrp);
    let $grpSearchBar = $('.searchBar', $btnsGrp);

    // list selectors
    let $lstSection = $('.lstSection', $callTimeMgrDiv);
    let $lstLoading = $('.callTimeMgrLstLoading', $lstSection);
    let $lstHeader = $('.lstHeader', $lstSection);
    let $individualLstHeader = $('.lstHeader.individualHeader', $lstSection);
    let $grpLstHeader = $('.lstHeader.grpHeader', $lstSection);
    var $individualLst = $('.lstDiv.individualLst', $lstSection);
    let $indivWrapper = $('.individualWrapper', $lstSection)
    let $grpLst = $('.lstDiv.grpLst', $lstSection);
    let $grpWrapper = $('.grpWrapper', $lstSection);
    let $individualFooter = $('.individualFooter', $lstSection);
    let $grpFooter = $('.grpFooter', $lstSection);

    // details section
    let $detailsSection = $('.detailsSection', $callTimeMgrDiv);
    let $detailsLoading = $('.detailsLoading', $detailsSection);
    let $detailsDiv = $('.detailsDiv', $detailsSection);
    let $individualDetails = $('.detailsIndividualDiv', $detailsDiv);
    let $individualDetailsLoading = $('.individualLoading', $individualDetails);
    let $individualDetailsHeader = $('.detailsHeaderDiv', $individualDetails);
    let $individualDetailsBtnSendMail = $('.detailsHeaderDiv .sendMailBtn', $individualDetails);
    let $individualDetailsBtnMoreDetails = $('.detailsHeaderDiv .prospectDetailsBtn', $individualDetails);
    let $individualDetailsBtnCallSheet = $('.detailsHeaderDiv .callSheetBtn', $individualDetails);
    let $individualDetailsStuff = $('.individualDetailsDiv', $individualDetails);
    let $individualDetailsContactDiv = $('.contactDiv', $individualDetailsStuff);
    let $individualDetailsGivingDiv = $('.givingAvailDiv', $individualDetailsStuff);
    let $individualDetailsGivingAmt = $('.amt', $individualDetailsGivingDiv);
    let $individualDetailsAsk = $('.notesDiv .askNotesDiv', $individualDetailsStuff);

    let $grpDetails = $('.detailsGrpDiv', $detailsDiv);
    let $grpDetailsLoading = $('.grpLoading', $grpDetails);
    let $grpDetailsLstOuter = $('.lstOuter', $grpDetails)
    let $grpDetailsLst = $('.lstOuter .lst', $grpDetails);
    let $grpDetailsLstFooter = $('.lstFooter', $grpDetails);
    let $grpDetailsLstHeader = $('.detailsHeaderDiv', $grpDetails);
    let $grpDetailsBtnCallResults = $('.detailsHeaderDiv .callResultsBtn', $grpDetails);
    let $grpDetailsBtnCallSheet = $('.detailsHeaderDiv .callSheetBtn', $grpDetails);

    // display callsheet
    function displayCallSheet(contactId, callTimeContactId,
                              primaryPhone = undefined, primaryPhoneUpdateFunc = undefined,
                              primaryEmail = undefined, primaryEmailUpdateFunc = undefined)
    {
        // display a loading overlay - long wait
        let sTmp = [];
        sTmp.push('<div class="loadingApi" style="height:100%;width:100%;position:absolute;z-index:1000;top:0;opacity:0.4;background-color:' + revupConstants.color.blackText + ';>');
            sTmp.push('<div class="loadingDiv">');
                sTmp.push('<div class="loading"></div>');
                sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
            sTmp.push('</div>');
        sTmp.push('</div>');
        $('body').append(sTmp.join(''));
        var resultId = $('.rankingDetailsDetailDiv').attr('resultId');

        // get the contact details
        //let url = templateData.contactDetailApi.replace('contactId', contactId);
        let url = templateData.callTimeContactsApi + callTimeContactId + '/';
        $.ajax({
            url: url,
            type: 'GET',
        })
        .done(function(r) {
            // clear the loading overlay
            $('.loadingApi').remove();

            // callsheet object
            var cObj = {};
            cObj.data = r;
            cObj.candidateName = templateData.candidateName;
            cObj.contactId = contactId;
            cObj.resultId = resultId;
            cObj.accountId = templateData.accountId;
            cObj.seatId = templateData.seatId;
            cObj.contactSetId = 'aroot';
            cObj.callTimeContactId = callTimeContactId;
            cObj.notes1 = '';
            cObj.notes2 = '';
            cObj.notesId = -1;
            if (r.notes && r.notes.length > 0) {
                cObj.notesId = r.notes[0].id;
                cObj.notes1 = r.notes[0].notes1;
                cObj.notes2 = r.notes[0].notes2;
            }

            cObj.saveNotes = true;
            cObj.saveNotesFunc = function(notes) {
                $individualDetailsAsk.html(notes.notes1);
            };
            cObj.notesApi = templateData.callsheetNotesApi;
            cObj.getPrintHtml = templateData.callTimeContactPrintHtml.replace('CALLTIME-CONTACT-ID', callTimeContactId);
            cObj.queuePrintUrl = templateData.callTimeContactCallSheetApi;

            cObj.phoneUpdateFunc = function(newPhoneId) {
                // change the new primary phone value
                if (primaryPhoneUpdateFunc) {
                    primaryPhoneUpdateFunc(newPhoneId)
                }

                // save the new primary phone number via it is
                let url = templateData.primaryPhoneApi + callTimeContactId + '/';
                $.ajax({
                    url: url,
                    type: 'PUT',
                    data: JSON.stringify({
                        primary_phone: newPhoneId,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                })
                .done(function(r) {
                })
                .fail(function(r) {
                    revupUtils.apiError('Call Time', r, 'Change Primary Phone Number',
                                        "Unable to change the Primary Phone Number at this time");
                })
                .always(function(r) {
                    bMovementHappening = false;
                });
            }

            cObj.emailUpdateFunc = function(newEmailId) {
                // change the new primary phone value
                if (primaryEmailUpdateFunc) {
                    primaryEmailUpdateFunc(newEmailId)
                }

                // save the new primary phone number via it is
                let url = templateData.primaryEmailApi + callTimeContactId + '/';
                $.ajax({
                    url: url,
                    type: 'PUT',
                    data: JSON.stringify({
                        primary_email: newEmailId,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                })
                .done(function(r) {
                })
                .fail(function(r) {
                    revupUtils.apiError('Call Time', r, 'Change Primary Email Address',
                                        "Unable to change the Primary Email Address at this time");
                })
                .always(function(r) {
                    bMovementHappening = false;
                });
            };
            if (primaryPhone) {
                cObj.primaryPhone = primaryPhone;
            }
            if (primaryEmail) {
                cObj.primaryEmail = primaryEmail;
            }

            if (templateData.isAcademic) {
                cObj.config = {
                    editorSection: {
                        display: true,
                        editors: [
                            {
                                header: "Notes",
                                height: "12em",
                            }
                        ]
                    },
                    contact: {
                        display: true,
                        displayTop: false,

                        firstName: {
                            section: 'contact',
                            field: 'first_name',
                        },
                        lastName: {
                            section: 'contact',
                            field: 'last_name',
                        },
                        phone: {
                            label: 'Phone',
                            section: 'features',
                            fieldStartWith: 'Purdue Alum Lookup',
                            signalSet: 'Purdue Bio',
                            subField: 'match',
                            keyLabel: ['Cell', 'Home', 'Work'],
                            key: ['mobile_phone', 'home_phone', 'business_phone'],
                            trimOffFront: 5,
                        },
                        email: {
                            label: 'Email',
                            section: 'features',
                            fieldStartWith: 'Purdue Alum Lookup',
                            signalSet: 'Purdue Bio',
                            subField: 'match',
                            key: 'email'
                        },
                    },
                    contactDetails: {
                        display: true,
                        section: "features",
                        field: "Purdue Alum Lookup",
                        data: [
                            {
                                subField: 'match',
                                signalSet: 'Purdue Bio',
                                type: 'attr',
                                attr: [
                                    {
                                        key: 'school',
                                        label: 'Graduated',
                                        type: 'text',
                                    },
                                    {
                                        key: 'major',
                                        label: 'Major',
                                        type: 'text',
                                    },
                                    {
                                        key: 'degree',
                                        label: 'Degree',
                                        type: 'degree'
                                    },
                                    {
                                        key: 'graduation_year',
                                        label: 'Year Graduated',
                                        type: 'year'
                                    },
                                    {
                                        key: 'employer',
                                        label: 'Employer',
                                        type: 'text'
                                    },
                                    {
                                        key: 'title',
                                        label: 'Occupation',
                                        type: 'text'
                                    }
                                ]
                            },
                            {
                                subField: 'match',
                                signalSet: 'Purdue Activities',
                                type: 'activity',
                                key: 'activity',
                                bDeDup: true,
                                label: 'Clubs/Activity'
                            }
                        ]
                    },
                    contribution: {
                        display: true,
                        leftSide: [
                            {
                                section: 'key_signals',
                                field: 'giving',
                                type: 'givingTotal',
                                title: 'Total Contributions Since %cycleYear%:',
                                bUseTitle2: ((partitionLst.length == 1) && (partitionLst[0].displayValue.toLowerCase() == 'all')),
                                title2: 'Total Contributions:',
                            },
                            {
                                type: 'givingFromSrc',
                                title: 'Total Contributions Made to %srcValue%',

                                srcSection: 'features',
                                srcFieldStartWith: 'Purdue Alum Lookup',
                                srcSignalSet: 'Purdue Bio',
                                srcSubField: 'match',
                                srcKey: 'school',

                                dstSection: 'features',
                                dstFieldStartWith: 'Purdue Giving',
                                dstSignalSet: 'Purdue Giving',
                                dstSubField: 'match',
                                dstSrcMatchKey: 'school',
                                dstKey: 'amount',
                            }
                        ],
                        rightSide: [
                            {
                                section: 'features',
                                fieldStartWith: 'Purdue Giving',
                                subField: 'match',
                                type: 'givingOrgBy',
                                label: 'Top Contributions',
                                disaplyField: 'school',
                                orgBy: "school",
                                thenBy: 'date',
                                maxOrgByDisplay: 5,
                                bDisplayTotal: true,
                            },
                        ],
                    },
                }
            }
            if (templateData.isPoliticalCampaign || templateData.isPoliticalCommittee) {
                cObj.config = {
                    editorSection: {
                        display: true,
                        editors: [
                            {
                                header: "Ask",
                                height: "6em",
                                enableFormating: true,
                            },
                            {
                                header: "Results / Notes",
                                height: "6em",
                                enableFormating: false,
                            }
                        ]
                    },
                    contact: {
                        display: true,
                        displayTop: false,

                        firstName: {
                            section: 'contact',
                            field: 'first_name',
                        },
                        lastName: {
                            section: 'contact',
                            field: 'last_name',
                        },
                        phone: {
                            label: 'Phone',
                            section: 'contact',
                            field: 'phone_numbers',
                            trimOffFront: 7,
                        },
                        email: {
                            label: 'Email',
                            section: 'contact',
                            field: 'email_addresses'
                        },
                        employer: {
                            label: 'Employer',
                            section: 'contact',
                            field: 'organizations',
                            fieldKey: 'name',
                        },
                        occupation: {
                            label: 'Occupation',
                            section: 'contact',
                            field: 'organizations',
                            fieldKey: 'title',
                        },
                        address: {
                            label: 'Address',
                            section: 'contact',
                            field: 'addresses',
                            fieldKeyAddr: 'street',
                            fieldKeyCity: 'city',
                            fieldKeyState: 'region',
                            fieldKeyZip: 'post_code',
                            fieldKeyCountry: 'country',
                        }
                    },
                    headerRight: {
                        display: true,
                        section: "features",
                        fieldStartWith: "Client Matches",
                        fields: [
                            {
                                key: 'giving_availability',
                                label: 'in giving availability',
                                type: 'amountOrMsg',
                            },
                        ]
                    },
                    contribution: {
                        display: true,
                        leftSide: [
                        ],
                        rightSide: [
                            {
                                section: 'key_signals',
                                field: 'giving',
                                type: 'givingTotal',
                                title: 'Total Contributions Since %cycleYear% Election Cycle:',
                            },
                            {
                                section: 'features',
                                fieldStartWith: 'Client Matches',
                                subField: 'giving',
                                type: 'givingTotal',
                                title: 'Total Contributions to Your Candidate:',
                                bDisplayTotal: false,
                            },
                            {
                                section: 'features',
                                fieldStartWithCand: "Client Matches",
                                fieldStartWithOther: "Key Contributions",
                                title: "Contributions",
                                type: "candidateContribution",
                                bDisplayTotal: false,
                                bDisplayGrandTotal: false,
                                bDisplaySeparator: false,
                                candidateTitle: 'My Campaign',
                                allyTitle: 'Correlated Campaigns',
                                nonAllyTitle: 'Opposition',
                            },
                            {
                                section: 'features',
                                fieldStartWith: 'Federal Matches',
                                subField: 'match',
                                type: 'giving',
                                label: 'Most recent Federal Contributions',
                                disaplyField: 'recipient',
                                maxDisplay: 10,
                                bDisplayTotal: false,
                            },
                            {
                                section: 'features',
                                fieldStartWith: 'State Matches',
                                subField: 'match',
                                type: 'giving',
                                label: 'Most recent State Contributions',
                                disaplyField: 'recipient',
                                maxDisplay: 5,
                                bDisplayTotal: false,
                            },
                            {
                                section: 'features',
                                fieldStartWith: 'Local Matches',
                                subField: 'match',
                                type: 'giving',
                                label: 'Most recent Local Contributions',
                                disaplyField: 'recipient',
                                maxDisplay: 5,
                                bDisplayTotal: false,
                            },
                            {
                                section: 'features',
                                fieldStartWith: 'Nonprofit',
                                subField: 'match',
                                type: 'givingNonprofit',
                                label: 'Most recent Nonprofit Contributions',
                                disaplyField: 'recipient',
                                maxDisplay: 5,
                                bDisplayTotal: false,
                            }
],
                    },
                }
            }

            // create the call sheet
            callSheet2.create(cObj);
        })
        .fail(function (r) {
            // clear the loading overlay
            $('.loadingApi').remove();
            revupUtils.apiError('Call Time', r, 'Display Callsheet',
                                "Unable to retrieve data for the Callsheet at this time");
        })
    } // displayCallSheet()


    // event handlers
    function btnTabEventHandlers()
    {
        // tab
        $lstTab.on('click', function(e) {
            $t = $(this);

            // if active table return
            if ($t.hasClass('active')) {
                return;
            }

            // remove active call from all tabs
            $lstTab.removeClass('active');

            // set active
            $t.addClass('active');

            // do the things for the tab
            if ($t.hasClass('individualTab')) {
                $btnsGrp.hide();
                $btnsIndividual.show();

                // disable buttons
                $btnIndividualAddToGrp.addClass('disabled');
                $btnIndividualPrint.addClass('disabled');
                $btnIndividualDelete.addClass('disabled');

                // list
                $grpLstHeader.hide();
                $grpFooter.hide();
                $grpLst.hide();
                $individualLstHeader.show();
                $individualLst.hide();
                $callTimeMgrLstLoading.show();

                // details
                $grpDetails.hide();
                $detailsLoading.hide();
                $individualDetails.show();
                $individualDetailsLoading.show();
                $individualDetailsStuff.hide();

                // fetch the data
                fetchIndividualCallSheetLst();

                // save the tab
                localStorage.setItem('callTimeStartingTab-' + templateData.seatId, 'individual');
            }
            else if ($t.hasClass('groupTab')) {
                $btnsIndividual.hide();
                $btnsGrp.show();

                // disable buttons
                $btnGrpPrint.addClass('disabled');
                $btnGrpAssign.addClass('disabled');
                $btnGrpUnassign.addClass('disabled');
                $btnGrpDelete.addClass('disabled');
                $btnGrpPopupMenu.addClass('disabled');

                // list
                $individualLstHeader.hide();
                $individualFooter.hide();
                $individualLst.hide();
                $grpLstHeader.show();
                $grpLst.hide();
                $callTimeMgrLstLoading.show();

                // details
                $grpDetails.hide();
                $individualDetails.hide();
                $detailsLoading.show();

                // fetch the data
                fetchCallSheetGrpsLst(1, true);

                // save the tab
                localStorage.setItem('callTimeStartingTab-' + templateData.seatId, 'group');
            }
        })

        /*
            button handlers
        */
        // Add individual(s) to a call group
        $individualSearchBar
            .on('revup.searchFor', function(e) {
                individualSearchVal = e.searchValue;
                fetchIndividualCallSheetLst();

                // save the local storage
                localStorage.setItem('callTimeMgrIndividualSearch-' + templateData.seatId, individualSearchVal);
            })
            .on('revup.searchClear', function(e) {
                individualSearchVal = "";
                fetchIndividualCallSheetLst();

                // save the local storage
                localStorage.removeItem('callTimeMgrIndividualSearch-' + templateData.seatId);
            });

        $grpSearchBar
            .on('revup.searchFor', function(e) {
                grpSearchVal = e.searchValue;
                fetchCallSheetGrpsLst(1, true);

                // save the local storage
                localStorage.setItem('callTimeMgrGroupSearch-' + templateData.seatId, grpSearchVal);
            })
            .on('revup.searchClear', function(e) {
                grpSearchVal = "";
                fetchCallSheetGrpsLst(1, true);

                // save the local storage
                localStorage.removeItem('callTimeMgrGroupSearch-' + templateData.seatId);
            });


        $btnIndividualAddToGrp.on('click', function(e) {
            let $addBtn = $(this);
            let currentPage = 1;
            let totalNumGrps = -1;
            let searchFor = ''
            const grpLstDlgSize = 10;
            let bAddTo = false;

            // see if disabled
            if ($addBtn.hasClass('disabled')) {
                return;
            }

            // get the selected entries
            $selEntrys = $('.entry .colSelect .icon-box-checked', $individualLst);

            // if one of more selected then display the "Add to Call Group" dialog
            if ($selEntrys.length > 0) {
                let msg = [];
                msg.push('<div class="addToGrpSection">');
                    msg.push('<div class="addToGrpDiv">')
                        msg.push('<div class="radioBtnDiv radioBtnAddTo">');
                            msg.push('<div class="icon icon-radio-button-on disabled"></div>');
                        msg.push('</div>');
                        msg.push('<div class="radioBodyDiv">');
                            msg.push('<div class="msg">Add the selected call sheet(s) to an existing Call Sheet Group:</div>');
                            msg.push('<div class="lstHeading">List Name</div>');
                            msg.push('<div class="searchForDiv"></div>')
                            msg.push('<div class="grpLstOuter">');
                                msg.push('<div class="grpLst">');
                                    msg.push('<div class="loadingDiv">')
                                        msg.push('<div class="loading"></div>')
                                        //msg.push('<img class="loadingImg" src="' + loadingImg + '">');
                                    msg.push('</div>');
                                msg.push('</div>');
                            msg.push('</div>');
                            msg.push('<div class="grpLstOverlay"></div>')
                        msg.push('</div>');
                    msg.push('</div>');

                    msg.push('<div class="spacerSection">')
                        msg.push('<div class="radioBtnDiv" style="height:10px"></div>');
                        msg.push('<div class="radioBodyDiv" style="height:10px"></div>');
                    msg.push('</div>')

                    msg.push('<div class="createNewGrpDiv">')
                        msg.push('<div class="radioBtnDiv radioBtnCreateGrp">');
                            msg.push('<div class="icon icon-radio-button-off disabled"></div>');
                        msg.push('</div>');
                        msg.push('<div class="radioBodyDiv">');
                            msg.push('<div class="msg">Add the selected Contact(s) to new Call Sheet Group:</div>');
                            msg.push('<input class="newGrpName" disabled type="text" placeholder="Group Name" autofocus>');
                        msg.push('</div>');
                    msg.push('</div>');
                msg.push('</div>');

                // add selected contacts to a Call Group
                function addContactsToCallSheetGrp(grpId, grpName)
                {
                    // get the selcected individauls and add to data
                    let cData = {};
                    let $sel = $('.entry .colSelect .icon-box-checked', $individualLst);
                    if ($sel.length == 1) {
                        let $e = $sel.closest('.entry');
                        cData.id = $e.attr('callTimeContactId');
                    }
                    else {
                        cData.id = [];
                        for (let i = 0; i < $sel.length; i++) {
                            let $e = $($sel[i].closest('.entry'));
                            cData.id.push($e.attr('callTimeContactId'));
                        }
                    }

                    // add to Call Group
                    let url = templateData.addToCallTimeSetApi.replace('CALLTIME-SET-ID', grpId);
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: JSON.stringify(cData),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                    })
                    .done(function(r) {
                        // close the dialog
                        $addToCallSheetGrpDlg.revupDialogBox('close');

                        // update the "Member Of Group" column
                        $sel.each(function() {
                            // get any existing labels
                            let $e = $(this).closest('.entry');
                            let $memberGrpCol = $('.colMemberGrp', $e);
                            let memGrpText = $memberGrpCol.text();

                            // dedup
                            let inLst = false;
                            let memLst = memGrpText.split(', ');
                            for (let i = 0; i < memLst.length; i++) {
                                if (memLst[i] == grpName) {
                                    inLst = true;

                                    break;
                                }
                            }

                            if (!inLst) {
                                if (memGrpText !== '') {
                                    memGrpText += ', ';
                                }
                                memGrpText += grpName;

                                // update text and popup text
                                $memberGrpCol.text(memGrpText);
                                $memberGrpCol.prop('title', memGrpText);
                            }
                        })

                        // clear the checkbox
                        $('.entry .colSelect .icon-box-checked', $individualLst).removeClass('icon-box-checked')
                                                                                .addClass('icon-box-unchecked');
                        $btnIndividualAddToGrp.addClass('disabled');
                        $btnIndividualPrint.addClass('disabled');
                        $btnIndividualDelete.addClass('disabled');
                    })
                    .fail(function(r) {
                        console.error('Add to Call Group Error: ', r);
                        $('body').revupMessageBox({
                            headerText: 'Add to Call Group Error',
                            msg: r.responseJSON.detail,
                        })
                        // revupUtils.apiError('Call Time', r, 'Add Contact To Call Sheet Group',
                        //                     "Unable to add a contact to the Call Sheet Group at this time");
                    })
                } // addContactsToCallSheetGrp

                // create a new Call Sheet Group
                function createAndAddToCallSheeGrp(grpName)
                {
                    let data = {};
                    data.title = grpName;
                    $.ajax({
                        type: 'POST',
                        url: templateData.callTimeSetsApi,
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                    })
                    .done(function(r) {
                        addContactsToCallSheetGrp(r.id, grpName);
                    })
                    .fail(function(r) {
                        revupUtils.apiError('Call Time', r, 'Add Call Sheet Group',
                                            "Unable to create a Call Sheet Group at this time");
                    })
                }

                let $addToCallSheetGrpDlg = $('body').revupDialogBox({
                    enableOkBtn: false,
                    headerText: 'Add Call Sheet To Group',
                    msg: msg.join(''),
                    zIndex: 60,
                    width: '450px',
                    okHandler: function(e) {
                        // see what to do
                        if ($('.radioBtnAddTo .icon', $addToCallSheetGrpDlg).hasClass('icon-radio-button-on')) {
                            // add the selected contacts to existing Call Sheet Group
                            let grpId = $('.entry.selected', $grpLst).attr('grpId');
                            let grpName = $('.entry.selected .text', $grpLst).text();
                            addContactsToCallSheetGrp(grpId, grpName);
                        }
                        else if ($('.radioBtnCreateGrp .icon', $addToCallSheetGrpDlg).hasClass('icon-radio-button-on')) {
                            let grpName = $('.newGrpName', $addToCallSheetGrpDlg).val();
                            createAndAddToCallSheeGrp(grpName);
                        }
                    },
                });

                // selectors
                let $addLst = $('.grpLst', $addToCallSheetGrpDlg);
                let $addSelectText = $('.addToGrpDiv .msg', $addToCallSheetGrpDlg);
                let $addLstOverlay = $('.grpLstOverlay', $addToCallSheetGrpDlg);
                let $addNewText = $('.createNewGrpDiv .msg', $addToCallSheetGrpDlg);

                // add the search box
                let $addSearch = $('.searchForDiv', $addToCallSheetGrpDlg);
                $addSearch.revupSearchBox({
                    placeholderText: 'Search for Group',
                    })
                    .on('revup.searchFor', function(e) {
                        currentPage = 1;
                        totalNumGrps = -1;
                        searchFor = e.searchValue;
                        bAddTo = false;
                        fetchCallSheetGrpForDlg();
                    })
                    .on('revup.searchClear', function(e) {
                        currentPage = 1;
                        totalNumGrps = -1;
                        searchFor = '';
                        bAddTo = false;
                        fetchCallSheetGrpForDlg();
                    });

                // position mask
                let lstWidth = $addLst.width();
                let lstOffset = $addLst.offset();
                $addLstOverlay.width(lstWidth + 2)
                              .offset(lstOffset);

                // add the even handlers
                let $addToGrpRadioBtn = $('.radioBtnAddTo .icon', $addToCallSheetGrpDlg);
                let $newGrpRadioBtn = $('.radioBtnCreateGrp .icon', $addToCallSheetGrpDlg);
                let $newGrpInput = $('.newGrpName', $addToCallSheetGrpDlg);
                let $grpLst = $('.grpLst', $addToCallSheetGrpDlg);
                let $grpLstOuter = $('.grpLstOuter', $addToCallSheetGrpDlg);
                $('.radioBtnDiv .icon', $addToCallSheetGrpDlg).on('click', function(e) {
                    let $btn = $(this);

                    // see if disabled
                    if ($btn.hasClass('disabled')) {
                        return;
                    }

                    // see if the currently currently selected button
                    if ($btn.hasClass('icon-radio-button-on')) {
                        return;
                    }

                    // toggle
                    if ($addToGrpRadioBtn.hasClass('icon-radio-button-on')) {
                        $('.radioBodyDiv .grpLst .entry').removeClass('selected');
                        // enable new group
                        //disable add to existing group
                        $addToGrpRadioBtn.removeClass('icon-radio-button-on')
                                         .addClass('icon-radio-button-off');
                        $newGrpRadioBtn.removeClass('icon-radio-button-off')
                                       .addClass('icon-radio-button-on');

                        // enable / disable input fields
                        $addNewText.removeClass('disabled');
                        $addSelectText.addClass('disabled');
                        $addLstOverlay.show();
                        $addSearch.revupSearchBox('disable');
                        $newGrpInput.prop('disabled', false).focus();

                        // see if the ok button should be enabled
                        var v = $newGrpInput.val();
                        if (v.length > 0) {
                            $('.okBtn', $addToCallSheetGrpDlg).prop('disabled', false);
                        }
                        else {
                            $('.okBtn', $addToCallSheetGrpDlg).prop('disabled', true);
                        }
                    }
                    else if ($newGrpRadioBtn.hasClass('icon-radio-button-on')) {
                        $('.radioBodyDiv .grpLst .entry').first().addClass('selected');
                        // enable use exiting group
                        //disable add to existing group
                        $newGrpRadioBtn.removeClass('icon-radio-button-on')
                                       .addClass('icon-radio-button-off');
                        $addToGrpRadioBtn.removeClass('icon-radio-button-off')
                                         .addClass('icon-radio-button-on');


                        // enable / disable input fields
                        $addNewText.addClass('disabled');
                        $addSelectText.removeClass('disabled');
                        $addLstOverlay.hide();
                        $addSearch.revupSearchBox('enable');
                        $newGrpInput.prop('disabled', true);

                        // enable the ok button
                        $('.okBtn', $addToCallSheetGrpDlg).prop('disabled', false);
                    }
                })

                $grpLst.on('click', '.entry', function() {
                    let $entry = $(this);

                    // if selected then return
                    if ($entry.hasClass('selected'))
                        return;

                    // clear selection
                    $('.entry.selected', $grpLst).removeClass('selected');

                    // set selected
                    $entry.addClass('selected');
                })

                // see of a new group has been defined
                $newGrpInput.on('input', function(e) {
                    $input = $(this);

                    var v = $input.val();
                    if (v.length > 0) {
                        $('.okBtn', $addToCallSheetGrpDlg).prop('disabled', false);
                    }
                    else {
                        $('.btnContainerRight .okBtn').prop('disabled', true);
                    }
                })


            // fetch the list of groups
            function fetchCallSheetGrpForDlg()
            {
                let url = templateData.callTimeSetsApi + '?page=' + currentPage + '&page_size=' + grpLstDlgSize;
                if (searchFor != '') {
                    url += '&search=' + searchFor;
                }

                $.ajax({
                    type: 'GET',
                    url: url,
                })
                .done(function(r) {
                    let sTmp = [];
                    if (r.count == 0) {
                        sTmp.push('<div class="lstMsgDiv">');
                            if (searchFor == '') {
                                sTmp.push('<div class="lstMsg">No Call Sheet Groups Defined</div>');
                            }
                            else {
                                sTmp.push('<div class="lstMsg">No Call Sheet Groups start with "' + searchFor + '"</b></div>');
                            }
                        sTmp.push('</div>');

                        $addToGrpRadioBtn.removeClass('icon-radio-button-on')
                                         .addClass('icon-radio-button-off')
                                         .addClass('disabled');
                        $newGrpRadioBtn.removeClass('icon-radio-button-off')
                                       .addClass('icon-radio-button-on');
                        $newGrpInput.prop('disabled', false).focus();
                        $addSearch.revupSearchBox('disable');
                        $addNewText.addClass('disabled');
                        $addSelectText.removeClass('disabled');
                        $addLstOverlay.show();
                    }
                    else {
                        var grps = r.results;
                        let bShowMore = false;
                        for (let i = 0; i < r.results.length; i++) {
                            let extraClass = '';
                            if (i + ((currentPage - 1) + grpLstDlgSize) == 0)
                                extraClass = ' selected'
                            if ((r.results.length == grpLstDlgSize) && (i >= (Math.round(r.results.length * 0.7))) && !bShowMore) {
                                extraClass = ' timeToAddMore';
                                bShowMore = true;
                            }

                            //the first entry is always selected
                            if(i == 0){
                                sTmp.push('<div class="entry selected' + extraClass + '" grpId="' + grps[i].id + '">');
                                    sTmp.push('<div class="text">' + grps[i].title + '</div>');
                                sTmp.push('</div>')
                            }
                            else {
                                sTmp.push('<div class="entry' + extraClass + '" grpId="' + grps[i].id + '">');
                                    sTmp.push('<div class="text">' + grps[i].title + '</div>');
                                sTmp.push('</div>')
                            }
                        }

                        if (bShowMore) {
                            sTmp.push('<div class="entry loadingMore" style="display:none">');
                                sTmp.push('<div class="loadingMore"></div>')
                            sTmp.push('</div>');
                        }

                        $addToGrpRadioBtn.removeClass('icon-radio-button-off')
                                         .addClass('icon-radio-button-on')
                                         .removeClass('disabled');
                        $newGrpRadioBtn.removeClass('icon-radio-button-on')
                                       .addClass('icon-radio-button-off')
                                       .removeClass('disabled');
                        $newGrpInput.prop('disabled', true);
                        $addSearch.revupSearchBox('enable');
                        $addNewText.addClass('disabled');
                        $addSelectText.removeClass('disabled');
                        $addLstOverlay.hide();

                        // disable the ok button until an existing group has been selected, or a new create is being created
                        $('.okBtn', $addToCallSheetGrpDlg).prop('disabled', false);

                        // update the current
                        if (currentPage != 1)
                            bAddTo = true;
                        currentPage += 1;
                    }

                    // load the list
                    if (bAddTo) {
                        // remove the loading more entry before appending the next group
                        $('.entry.loadingMore', $grpLst).remove();
                        $addLst.append(sTmp.join(''));
                    }
                    else {
                        $addLst.html(sTmp.join(''));
                    }
                })
                .fail(function(r) {
                    revupUtils.apiError('Call Time', r, 'Load Call Sheet Groups',
                                        "Unable to load Call Sheet Groups at this time");
                })
            } // fetchCallSheetGrpForDlg

            // start getting Call Sheet Groups for the dialog
            fetchCallSheetGrpForDlg();
            let $gl = $('.revupDialogBox .grpLst');
            $grpLstOuter
                .on('scroll', function(e) {
                    let $timeToAddMore = $('.entry.timeToAddMore', $grpLst);
                    if ($timeToAddMore.length > 0 && $timeToAddMore.visible($grpLstOuter)) {
                        $timeToAddMore.removeClass('timeToAddMore');
                        $('.entry.loadingMore', $grpLst).show();

                        fetchCallSheetGrpForDlg();
                    }
                })
                .on('mousewheel', function(e) {
                    if($glOuter.prop('scrollHeight') - $grpLstOuter.scrollTop() <= $gl.height() && (e.originalEvent.wheelDelta < 0)) {
                        e.preventDefault()
                    }
                })

            }
        });

        // bulk print individaul(s)
        $btnIndividualPrint.on('click', function(e) {
            let callTimeContactId = [];
            let contactId = [];

            // get the selected entries
            let $selEntrys = $('.entry .colSelect .icon-box-checked', $individualLst);
            $selEntrys.each(function() {
                $entry = $(this).closest('.entry');
                callTimeContactId.push($entry.attr('callTimeContactId'));
                contactId.push($entry.attr('contactId'))
            })

            // display are you sure messages
            let sTmp = [];
            sTmp.push('<div class="bulkPrint">');
                sTmp.push('<div class="areYouSureDiv">');
                    sTmp.push('<div class="msgText">');
                        sTmp.push('Are you sure that you want to print ' + callTimeContactId.length + ' call sheets?');
                        sTmp.push('<br><br>Press the Print button to continue.')
                    sTmp.push('</div>');
                sTmp.push('</div>');
                sTmp.push('<div class="waitingDiv">');
                    sTmp.push('<div class="loadingDiv">');
                        sTmp.push('<div class="loading"></div>');
                        sTmp.push('<div class="msgText">Building Call Sheets</div>')
                    sTmp.push('</div>');
                sTmp.push('</div>');
                sTmp.push('<div class="goodResults">');
                    sTmp.push('<div class="msgText">')
                        sTmp.push('RevUp is currently generating a PDF from your selected call sheets. ');
                        sTmp.push('We will send you an email with a download link once it’s completed.');
                        sTmp.push('<br><br>');
                        sTmp.push('For optimum quality when printing, ensure \“Fit to page\” is set in your print dialog prior to printing.');
                    sTmp.push('</div>')
                sTmp.push('</div>');
            sTmp.push('</div>')
            let $bulkPrint = $('body').revupDialogBox({
                okBtnText: 'Print',
                cancelBtnText: 'Cancel',
                headerText: 'Print Call Sheets',
                msg: sTmp.join(''),
                extraClass: 'bulkPrintCallSheets',
                width: '350px',
                height: '300px',
                isDragable: false,
                escClose: true,
                autoCloseOk: false,
                autoCloseCancel: false,
                cancelHandler: function() {
                    $bulkPrint.revupDialogBox('close')
                },
                okHandler: function() {
                    // so the print button was pressed
                    if($('.areYouSureDiv', $bulkPrint).is(':visible')) {
                        // hide the current page and display the waiting
                        $('.areYouSureDiv', $bulkPrint).hide();
                        $('.waitingDiv', $bulkPrint).show();


                        // disable the buttons
                        $bulkPrint.revupDialogBox('disableBtn', 'cancelBtn');
                        $bulkPrint.revupDialogBox('disableBtn', 'okBtn');

                        // build the data
                        let url = templateData.callTimeContactCallSheetApi;
                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: JSON.stringify({ids: callTimeContactId}),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                        })
                        .done(function(r) {
                            $('.waitingDiv', $bulkPrint).hide();
                            $('.goodResults', $bulkPrint).show();

                            $('.cancelBtn', $bulkPrint).hide();
                            $('.okBtn', $bulkPrint).html('Close');
                            $bulkPrint.revupDialogBox('enableBtn', 'okBtn');
                            updateRevupAlert();
                        })
                        .fail(function(r) {
                            // hide the waiting
                            $('.waitingDiv', $bulkPrint).hide();
                            $('.areYouSureDiv', $bulkPrint).show();
                            $bulkPrint.revupDialogBox('enableBtn', 'okBtn');
                            $bulkPrint.revupDialogBox('enableBtn', 'cancelBtn');
                            revupUtils.apiError('Call Time', r, 'Bulk Print',
                                                "Unable to Bulk Print Individual Call Sheets at this time");
                        });
                    }
                    else if($('.goodResults', $bulkPrint).is(':visible')) {
                        // clear the checkbox
                        $('.entry .colSelect .icon-box-checked', $individualLst).removeClass('icon-box-checked')
                                                                                .addClass('icon-box-unchecked');
                        $btnIndividualAddToGrp.addClass('disabled');
                        $btnIndividualPrint.addClass('disabled');
                        $btnIndividualDelete.addClass('disabled');

                        $bulkPrint.revupDialogBox('close')
                    }
                }
            })
        })

        // delete individaul(s)
        $btnIndividualDelete.on('click', function(e) {
            let $addBtn = $(this);

            // see if disabled
            if ($addBtn.hasClass('disabled')) {
                return;
            }

            // get the selected entries
            let $selEntrys = $('.entry .colSelect .icon-box-checked', $individualLst);

            let msg = 'Are you sure you want to remove the ' + $selEntrys.length + ' selected contact(s) from Call Time?'
            msg += '<br><br>If the contact is in any Call Sheet Groups, the contact will be removed from them as well.'
            let $delConfBox = $('body').revupConfirmBox({
                headerText: 'Delete Call Sheet Group',
                msg: msg,
                zIndex: 25,
                isDragable: false,
                autoCloseOk: false,
                okHandler: function() {
                    // build the deferred deletes
                    let defArray = [];
                    let arrayIds = [];
                    $selEntrys.each(function() {
                        let $e = $(this).closest('.entry');
                        let callTimeContactId = $e.attr('callTimeContactId');
                        url = templateData.callTimeContactsApi + callTimeContactId + '/';

                        defArray.push(
                            $.ajax({
                                type: 'DELETE',
                                url: url,
                                data: {callTimeContactId: callTimeContactId},
                            }, {callTimeContactId2: callTimeContactId})
                        )

                        arrayIds.push(callTimeContactId);
                    })
                    $.when.apply($, defArray).then(
                        function(obj, id) {
                            let $lst = $('.lstDiv.individualLst');
                            let bSelected = false;   // see if the selected entry has been deleted
                            if (arrayIds.length == 1) {
                                var $e = $('.entry[callTimeContactId=' + arrayIds[0] + ']', $lst);
                                if ($e.hasClass('selected')) {
                                    bSelected = true;
                                }
                                $e.remove();
                            }
                            else {
                                for(var i = 0; i < arguments.length; i++) {
                                    var $e = $('.entry[callTimeContactId=' + arrayIds[i] + ']', $lst);
                                    if ($e.hasClass('selected')) {
                                        bSelected = true;
                                    }
                                    $e.remove();
                                }
                            }

                            $delConfBox.revupConfirmBox('close');

                            // update the attributes
                            let numentries = (parseInt($individualLst.attr('numentries')) - arrayIds.length);
                            $individualLst.attr('numentries', numentries);

                            // $individualLst.attr('maxpages', Math.ceil(numentries / entriesPerPageIndividual));
                            if (parseInt($individualLst.attr('currentpage')) == parseInt($individualLst.attr('maxPages'))) { // if at the last page, no need to fetch more entries
                                $timeToAddMore.removeClass('timeToAddMore');
                                $individualLst.attr('numentriesloaded', $individualLst.attr('numentriesloaded') - 1);

                                // update the footer count
                                $grpFooter.hide();
                                $individualFooter.show()
                                $individualFooter.html('<text>Showing </text>' + '<text class="showing">' + parseInt($individualLst.attr('numEntriesLoaded')) +  '</text> '  + '<text> of </text>' + '<text class="total">' + $individualLst.attr('numEntries') + '</text>' + '<text> contacts</text>');
                            }

                            else {
                                if (parseInt($individualLst.attr('numentries')) == parseInt($individualLst.attr('numentriesloaded'))) {
                                    $timeToAddMore.removeClass('timeToAddMore');
                                    $individualLst.attr('currentpage', $individualLst.attr('maxPages'));
                                }
                                else if (parseInt($individualLst.attr('numentries')) > parseInt($individualLst.attr('numentriesloaded'))) { // there is more to fetch after the delete
                                    $('.entry.loadingMore', $individualLst).remove();
                                    let maxPages = Math.ceil($individualLst.attr('numentries') / entriesPerPageIndividual);
                                    $individualLst.attr('maxPages', maxPages);
                                    fetchNextChunk(arrayIds.length);
                                }
                            }


                            // if (parseInt($individualLst.attr('numentries')) > parseInt($individualLst.attr('numentriesloaded'))) {
                            //     $('.entry.loadingMore', $individualLst).remove();
                            //     fetchNextChunk(arrayIds.length);
                            // }

                            // if the selected entry removed then select the first entry
                            if (bSelected && $('.entry', $lst).length != 0) {
                                $eFirst = $('.entry:first-child', $lst);
                                $eFirst.addClass('selected');

                                let callTimeContactId = $eFirst.attr('callTimeContactId');
                                let contactId = $eFirst.attr('contactId');
                                fetchContactDetails(callTimeContactId, contactId);
                            }

                            // display the no individividual Call Sheets to Manager if no entries
                            if ($('.entry', $lst).length == 0) {
                                let sTmp = [];
                                sTmp.push('<div class="noResultsDiv">');
                                    sTmp.push('<div class="text">No Individual Call Sheets to Manage</div>');
                                sTmp.push('</div>');
                                $indivWrapper.html(sTmp.join(''));

                                // don't display the header
                                //$individualLstHeader.hide();
                                $individualDetailsLoading.hide();
                                $individualDetailsStuff.hide();

                                $individualDetailsBtnSendMail.addClass('disabled');
                                $individualDetailsBtnMoreDetails.addClass('disabled');
                                $individualDetailsBtnCallSheet.addClass('disabled');
                            }

                             // disable add to group and delete buttons
                             $btnIndividualDelete.addClass('disabled');
                             $btnIndividualPrint.addClass('disabled');
                             $btnIndividualAddToGrp.addClass('disabled');
                        },
                        function(obj) {
                            revupUtils.apiError('Call Time', r, 'Delete Contacts from Call Time',
                                                "Unable to delete Contacts from Call Time at this time");
                        }
                    );
                }
            });

        });

        // list events
        $individualLst
            .on('click', '.entry', function(e) {
                let $entry = $(this);

                // if select there nothing to do
                if ($entry.hasClass('selected'))
                    return;

                // clear and set selected
                $('.entry', $individualLst).removeClass('selected');
                $entry.addClass('selected');

                // get the new selected details
                let selectedCallTimeContactId = $entry.attr('callTimeContactId');
                let selectedContactId = $entry.attr('contactId');
                fetchContactDetails(selectedCallTimeContactId, selectedContactId);
            })
            .on('click', '.entry .colSelect .icon', function(e) {
                let $icon = $(this);

                if ($icon.hasClass('icon-box-unchecked')) {
                    $icon.removeClass('icon-box-unchecked').addClass('icon-box-checked');
                }
                else {
                    $icon.removeClass('icon-box-checked').addClass('icon-box-unchecked');
                }

                // is there is anything selected and if so enable the add to group and delete buttons
                let $selected = $('.entry .colSelect .icon-box-checked');
                if ($selected.length > 0) {
                    $btnIndividualAddToGrp.removeClass('disabled');
                    $btnIndividualPrint.removeClass('disabled');
                    $btnIndividualDelete.removeClass('disabled');

                }
                else {
                    $btnIndividualAddToGrp.addClass('disabled');
                    $btnIndividualPrint.addClass('disabled');
                    $btnIndividualDelete.addClass('disabled');
                }
            })

        $indivWrapper
            .on('scroll', function(e) {
                let $timeToAddMore = $('.entry.timeToAddMore', $indivWrapper);
                if ($timeToAddMore.length > 0) {
                    if($timeToAddMore.visible($individualLst)) {
                        $('.entry.loadingMore', $individualLst).show();
                        $('.entry.timeToAddMore', $indivWrapper).removeClass('timeToAddMore');
                        fetchNextChunk();
                    }
                }
            });

        $grpWrapper
            .on('scroll', function(e) {
                let $timeToAddMore = $('.entry.timeToAddMore', $grpWrapper);
                if ($timeToAddMore.length > 0){
                    if($timeToAddMore.visible($grpLst) && parseInt($grpLst.attr('numentriesloaded')) != parseInt($grpLst.attr(('numentries')))){
                        $('.entry.loadingMore', $grpLst).show();
                        $('.entry.timeToAddMore', $grpWrapper).removeClass('timeToAddMore');
                        fetchNextChunk(false, true);
                    }
                }
            });

        /*
         * Call Sheet Groups
         */
        // group list button handlers
        $btnGrpDelete.on('click', function(e) {
            $btn = $(this);

            // see if disabled
            if ($btn.hasClass('disabled'))
                return;

            // display the confirm box
            // display the confermation box
            let callSheetGrpName = $('.entry.selected .name', $grpLst).text();
            let msg = 'Are you sure you want to remove the Call Sheet Group "<b>' + callSheetGrpName + '</b>"?'
            let $delConfBox = $('body').revupConfirmBox({
                headerText: 'Delete Call Sheet Group',
                msg: msg,
                zIndex: 25,
                isDragable: false,
                autoCloseOk: false,
                okHandler: function() {
                    let $entry = $('.entry.selected', $grpLst);

                    // delete the Call Sheet Group
                    let url = templateData.callTimeSetsApi + $entry.attr('callTimeGrpId') + '/';
                    $.ajax({
                        url: url,
                        type: "DELETE",
                    })
                    .done(function(r) {
                        var remain = (($('.grpLst .entry')).length);
                        let $timeToAddMore = $('.entry.timeToAddMore', $grpWrapper);

                        if(remain > 1){
                            let $entry = $('.entry.selected', $grpLst);
                            let $nextEntry = $entry.next('.entry');
                            if ($nextEntry.length == 0) {
                                $nextEntry = $entry.prev('.entry');
                            }

                            // remove the old entry
                            $entry.remove();

                            $nextEntry.addClass('selected')

                            // get the list entris in the list
                            var callTimeGrpId = $nextEntry.attr('callTimeGrpId');

                            fetchCallTimeGrp(callTimeGrpId);

                        }
                        else if (remain == 1){
                            // remove the old entry
                            $entry.remove();
                            fetchCallSheetGrpsLst(1, true);

                            $('.detailsGrpDiv .lstFooter').hide();
                        }

                        $delConfBox.revupConfirmBox('close');

                        // there will be more to fetch
                        if (parseInt($grpLst.attr('currentpage')) < parseInt($grpLst.attr('maxPages'))) { // if at the last page, no need to fetch more entries
                            $grpLst.attr('maxpages',  Math.ceil(parseInt($grpLst.attr('numentries')) / entriesPerPageGrp));
                            fetchNextChunk(1, true);
                        }
                        else {
                            $grpLst.attr(('numentriesloaded'), parseInt($grpLst.attr('numentriesloaded')) - 1);
                        }

                        // update the attributes
                        $grpLst.attr('numentries', parseInt($grpLst.attr('numentries')) - 1);
                        $grpLst.attr('maxpages',  Math.ceil(parseInt($grpLst.attr('numentries')) / entriesPerPageGrp));

                        // update the footer count
                        $individualFooter.hide();
                        $grpFooter.show()
                        $grpFooter.html('<text>Showing </text>' + '<text class="showing">' + parseInt($grpLst.attr('numentriesloaded')) +  '</text> '  + '<text> of </text>' + '<text class="total">' + $grpLst.attr('numEntries') + '</text>' + '<text>  groups</text>');
                    })
                    .fail(function(r) {
                        revupUtils.apiError('Call Time', r, 'Delete Call Sheet Group',
                                            "Unable to delete Call Sheet Group at this time");
                    })
                }
            });
        }); // $btnGrpDelete

        $btnGrpPrint.on('click', function(e) {
            let callSheetGrpId = $('.entry.selected', $grpLst).attr('callTimeGrpId');
            let grpName = $('.entry.selected .name', $grpLst).text();

            // display are you sure messages
            let sTmp = [];
            sTmp.push('<div class="bulkPrint">');
                sTmp.push('<div class="areYouSureDiv">');
                    sTmp.push('<div class="msgText">');
                        sTmp.push('Are you sure that you want to print call sheets for group ' + grpName + '.');
                        sTmp.push('<br><br>Press the Print button to print.');
                    sTmp.push('</div>');
                sTmp.push('</div>');
                sTmp.push('<div class="waitingDiv">');
                    sTmp.push('<div class="loadingDiv">');
                        sTmp.push('<div class="loading"></div>');
                        sTmp.push('<div class="msgText">Building Call Sheets</div>')
                    sTmp.push('</div>');
                sTmp.push('</div>');
                sTmp.push('<div class="goodResults">');
                    sTmp.push('<div class="msgText">')
                        sTmp.push('RevUp is currently generating a PDF from your selected call sheets. We will send you an email with ');
                        sTmp.push('a download link once it’s completed. ');
                        sTmp.push('<br><br>')
                        sTmp.push('For optimum quality when printing, ensure “Fit to page” is set in your print dialog prior to printing.');
                    sTmp.push('</div>');
            sTmp.push('</div>')
            let $bulkPrint = $('body').revupDialogBox({
                okBtnText: 'Print',
                cancelBtnText: 'Cancel',
                headerText: 'Print Call Sheets',
                msg: sTmp.join(''),
                extraClass: 'bulkPrintCallSheets',
                width: '350px',
                height: '300px',
                isDragable: false,
                escClose: true,
                autoCloseOk: false,
                autoCloseCancel: false,
                cancelHandler: function() {
                    $bulkPrint.revupDialogBox('close')
                },
                okHandler: function() {
                    // so the print button was pressed
                    if($('.areYouSureDiv', $bulkPrint).is(':visible')) {
                        // hide the current page and display the waiting
                        $('.areYouSureDiv', $bulkPrint).hide();
                        $('.waitingDiv', $bulkPrint).show();

                        // disable the buttons
                        $bulkPrint.revupDialogBox('disableBtn', 'cancelBtn');
                        $bulkPrint.revupDialogBox('disableBtn', 'okBtn');

                        // build the data
                        let url = templateData.callTimeGrpCallSheetApi;
                        url = url.replace('CALLTIME-GRP-ID', callSheetGrpId);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                        })
                        .done(function(r) {
                            $('.waitingDiv', $bulkPrint).hide();
                            $('.goodResults', $bulkPrint).show();

                            $('.cancelBtn', $bulkPrint).hide();
                            $('.okBtn', $bulkPrint).html('Close');
                            $bulkPrint.revupDialogBox('enableBtn', 'okBtn');
                            updateRevupAlert();
                        })
                        .fail(function(r) {
                            // hide the waiting
                            $('.waitingDiv', $bulkPrint).hide();
                            $('.areYouSureDiv', $bulkPrint).show();
                            $bulkPrint.revupDialogBox('enableBtn', 'okBtn');
                            $bulkPrint.revupDialogBox('enableBtn', 'cancelBtn');
                            revupUtils.apiError('Call Time', r, 'Bulk Print',
                                                "Unable to Bulk Print Individual Call Sheets at this time");
                        });
                    }
                    else if($('.goodResults', $bulkPrint).is(':visible')) {
                        $bulkPrint.revupDialogBox('close')
                    }
                }
            })
        })

        $btnGrpAssign.on('click', function(e) {
            $btn = $(this);

            // see if disabled
            if ($btn.hasClass('disabled'))
                return;

            // get the name of the Call Sheet Group
            let callSheetGrpId = $('.entry.selected', $grpLst).attr('callTimeGrpId');
            let callSheetGrpName = $('.entry.selected .name', $grpLst).text();

            // build the dialog
            let sTmp = [];
            sTmp.push('<div class="assignGrpToDlg" callTimeGrpId="' + callSheetGrpId + '">')
                sTmp.push('<div class="text">Assign the Call Sheet Group "' + callSheetGrpName +'" to the raiser in the following list</div>');
                sTmp.push('<div class="titleText">Adminstrator Raiser:</div>');
                sTmp.push('<div class="raiserLst"></div>')
            sTmp.push('</div>')
            let $grpAssignDlg = $('body').revupDialogBox({
                extraClass: 'grpAssignDlg',
                enableOkBtn: false,
                headerText: 'Assign Call Sheet Group',
                msg: sTmp.join(''),
                zIndex: 60,
                width: '400px',
                okHandler: function(e) {
                    let callSheetGrpId = $('.assignGrpToDlg', $grpAssignDlg).attr('callTimeGrpId');
                    let seatId = $('.raiserLst .entry.selected', $grpAssignDlg).attr('seatId');
                    let name = $('.raiserLst .entry.selected .name', $grpAssignDlg).text();

                    let url = templateData.callTimeGrpAssignApi;
                    url = url.replace('CALLTIME-GRP-ID', callSheetGrpId);
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: JSON.stringify({seat: seatId}),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                    })
                    .done(function(r) {
                        // update the selected entry
                        let $grpLstEntry = $('.entry.selected', $grpLst)
                        let seatId = $grpLstEntry.attr('seatId');
                        if (seatId) {
                            seatId = r.shared_seats[0] + ',' + seatId;
                        }
                        else {
                            seatId = r.shared_seats[0];
                        }
                        $grpLstEntry.attr('seatId', seatId);

                        let today = new Date();
                        var dd = today.getDate();
                        var mm = today.getMonth()+1; //January is 0!
                        var yyyy = today.getFullYear();
                        if(dd<10) {
                            dd='0'+dd
                        }
                        if(mm<10) {
                            mm='0'+mm
                        }
                        today = mm+'/'+dd+'/'+yyyy;
                        let assignedToDateAttr = $('.colDateAssigned', $grpLstEntry).attr('assignedToDate');
                        if (assignedToDateAttr) {
                            assignedToDateAttr = today + ',' + assignedToDateAttr;
                        }
                        else {
                            assignedToDateAttr = today;
                        }
                        $('.colDateAssigned', $grpLstEntry).attr('assignedToDate', assignedToDateAttr)
                                                           .text(today);

                        let assignedTo = $('.colAssignedTo', $grpLstEntry).text();
                        let assignedToAttr = $('.colAssignedTo', $grpLstEntry).attr('assignedTo')
                        if (assignedTo != '--') {
                            assignedTo = name + ', ' + assignedTo;
                            assignedToAttr = name + ',' + assignedToAttr;
                        }
                        else {
                            assignedTo = name;
                            assignedToAttr = name;
                        }
                        $('.colAssignedTo', $grpLstEntry).text(assignedTo)
                                                          .attr('assignedTo', assignedToAttr)

                        // build the title tag
                        let title = $('.colDateAssigned', $grpLstEntry).attr('title');
                        if (title) {
                            title = today + '  ' + name + '\n' + title;
                        }
                        else {
                            title = today + '  ' + name;
                        }
                        $('.colDateAssigned', $grpLstEntry).attr('title', title);
                        $('.colAssignedTo', $grpLstEntry).attr('title', title);

                        // make sure the unassign button is enabled
                        $btnGrpUnassign.removeClass('disabled');
                    })
                    .fail(function(r) {
                        revupUtils.apiError('Call Time', r, 'Assign Call Sheet Group',
                                            "Unable to Assign Call Sheet Group to a raiser at this time");
                    });

                    //createNewCallSheetGrp();
                }
            });

            // load the admin raiser that can be assigned to a group
            $.ajax({
                type: 'GET',
                url: templateData.seatsToAssignToApi,
            })
            .done(function(results) {
                // get the seat id for the selected group
                let $grpLstEntry = $('.entry.selected', $grpLst)
                let seatId = $grpLstEntry.attr('seatId');
                if (seatId) {
                    seatId = seatId.split(',');
                }
                else {
                    seatId = [];
                }

                // add the names to the list
                let sTmp = [];
                let r = results.results;
                for (let i = 0; i < r.length; i++) {
                    if (r[i].state === 'Active') {
                        // see if the raiser is already assigned to this group
                        let extraClass = ''
                        if (seatId.indexOf(r[i].id + '') != -1) {
                            extraClass = ' disabled';
                        }

                        sTmp.push('<div class="entry' + extraClass + '" seatId="' + r[i].id + '" userId="' + r[i].user.id + '">');
                            let name = r[i].user.email;
                            if (r[i].user.first_name !== '' || r[i].user.last_name !== '') {
                                name = r[i].user.first_name;
                                if (name !== '') {
                                    name += ' ';
                                name += r[i].user.last_name;
                                }
                            }
                            sTmp.push('<div class="name">' + name + '</div>')
                        sTmp.push('</div>');
                    }
                }

                $('.raiserLst', $grpAssignDlg).html(sTmp.join(''));
            })
            .fail(function(r) {
                revupUtils.apiError('Call Time', r, 'Load Raisers',
                                    "Unable to load the list of Raisers at this time");
            });

            $grpAssignDlg.on('click', '.raiserLst .entry', function(e) {
                let $entry = $(this);

                // skip disabled entries
                if ($entry.hasClass('disabled')) {
                    return;
                }

                // clear the selected
                let $lst = $('.raiserLst', $grpAssignDlg);
                $('.entry', $lst).removeClass('selected');

                // select the new entry
                $entry.addClass('selected');

                // enable the ok button
                $('.okBtn', $grpAssignDlg).prop('disabled', false);
            })
        }); // $btnGrpAssign

        /*
         * Unassign
         */
        $btnGrpUnassign.on('click', function(e) {
            $btn = $(this);

            // see if disabled
            if ($btn.hasClass('disabled'))
                return;

            // there will be 2 flavors of this, single assignee and multiple assignees

            // get the name of the Call Sheet Group
            let $selEntry = $('.entry.selected', $grpLst);
            let callSheetGrpName = $selEntry.text();
            let callSheetAssignedSeat = $selEntry.attr('seatId');

            // get array of assigned seats
            callSheetAssignedSeat = callSheetAssignedSeat.split(',');

            function doUnassign(assignedSeat, $selEntry, bClearAll, $unassignDlg)
            {
                let callSheetGrpId = $selEntry.attr('callTimeGrpId');

                // build the data
                let data = {};
                if (Array.isArray(assignedSeat)) {
                    data.seats = assignedSeat
                }
                else {
                    data.seat = assignedSeat;
                }

                let url = templateData.callTimeGrpUnassignApi;
                url = url.replace('CALLTIME-GRP-ID', callSheetGrpId);
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                })
                .done(function(r) {
                    // cleanup - single assigned easy, just clear assigned date and assigned to and the title props
                    if (bClearAll) {
                        $selEntry.removeAttr('seatId');
                        $('.colDateAssigned', $selEntry).text('--')
                                                        .removeAttr('assignedToDate')
                                                        .removeAttr('title');
                        $('.colAssignedTo', $selEntry).text('--')
                                                      .removeAttr('assignedTo')
                                                      .removeAttr('title');

                        $btnGrpUnassign.addClass('disabled');
                    }
                    else {
                        // rebuild the assigned to and assigned date
                        let seatId = $selEntry.attr('seatId');
                        seatId = seatId.split(',');
                        let assignedToDate = $('.colDateAssigned', $selEntry).attr('assignedToDate');
                        assignedToDate = assignedToDate.split(',')
                        let assignedTo = $('.colAssignedTo', $selEntry).attr('assignedTo');
                        assignedTo = assignedTo.split(',')

                        let newSeatId = [];
                        let newAssignedTo = [];
                        let newAssignedToDate = [];
                        let newTitle = [];
                        for (let i = 0; i < seatId.length; i++) {
                            // see if one of the item to remove and if so skip
                            if (assignedSeat.indexOf(parseInt(seatId[i], 10)) == -1) {
                                newSeatId.push(seatId[i]);
                                newAssignedTo.push(assignedTo[i]);
                                newAssignedToDate.push(assignedToDate[i])
                                newTitle.push(assignedToDate[i] + ' ' + assignedTo[i])
                            }
                        }

                        // display the new values
                        $selEntry.attr('seatId', newSeatId.join(','));
                        $('.colDateAssigned', $selEntry).attr('assignedToDate', newAssignedToDate.join(','))
                                                        .text(newAssignedToDate[0])
                                                        .attr('title', newTitle.join('\n'))
                        $('.colAssignedTo', $selEntry).attr('assignedTo', newAssignedTo.join(','))
                                                      .text(newAssignedTo.join(', '))
                                                      .attr('title', newTitle.join('\n'))
                    }

                    if ($unassignDlg) {
                        $unassignDlg.revupDialogBox('close');
                    }
                })
                .fail(function(r) {
                    revupUtils.apiError('Call Time', r, 'Unassign Call Sheet Group',
                                        "Unable to Unassign Call Sheet Group from Raisers at this time");
                })
            } // doUnassign

            // if a single just do the unassign
            if (callSheetAssignedSeat.length == 1) {
                doUnassign(callSheetAssignedSeat, $selEntry, true);
            }
            else {
                let sTmp = [];
                sTmp.push('<div class="unassignDiv">');
                    sTmp.push('<div class="unassignAllGrpDiv">')
                        sTmp.push('<div class="radioBtnDiv radioBtnUnassignAll">');
                            sTmp.push('<div class="icon icon-radio-button-off"></div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="radioBodyDiv">');
                            sTmp.push('<div class="msg">Unassign all of the Raisers assigned to this Call Sheet Group.</div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="unassignSelectGrpDiv">')
                        sTmp.push('<div class="radioBtnDiv radioBtnUnassignSelect">');
                            sTmp.push('<div class="icon icon-radio-button-off"></div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="radioBodyDiv">');
                            sTmp.push('<div class="msg">Select the Raiser to remove form the list below:</div>');
                            sTmp.push('<div class="unassignRaiserLst"></div>');
                            sTmp.push('<div class="unassignRaiserLstOverlay"></div>')
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');

                let $unassignDlg = $('body').revupDialogBox({
                    extraClass: 'unassignDlg',
                    autoCloseOk: false,
                    // enableOkBtn: false,
                    headerText: 'Unassign Rasier from Group',
                    msg: sTmp.join(''),
                    zIndex: 60,
                    width: '450px',
                    okHandler: function(e) {
                        // see what form of unassign
                        let seats = [];
                        let bClearAll = false;
                        if ($('.unassignAllGrpDiv .icon').hasClass('icon-radio-button-on')) {
                            // unassigned all
                            $('.entry', $unassignLst).each(function() {
                                let $entry = $(this);
                                let seatId = parseInt($entry.attr('seatId'), 10);

                                seats.push(seatId);
                            })

                            // set the clear all flag
                            bClearAll = true;
                        }
                        else if ($('.unassignSelectGrpDiv .icon').hasClass('icon-radio-button-on')) {
                            // unassigned selected
                            $('.entry', $unassignLst).each(function() {
                                let $entry = $(this);

                                // add only the selected entries
                                if ($('.col.colSelect .icon', $entry).hasClass('icon-box-checked')) {
                                    let seatId = parseInt($entry.attr('seatId'), 10);

                                    seats.push(seatId)
                                }
                            })

                            // set the clear all flag
                            if (seats.length == $('.entry', $unassignLst).length) {
                                bClearAll = true;
                            }
                            else {
                                bClearAll = false;
                            }
                        }
                        else {
                            console.error('Unassign Rasier - unknown state')
                        }

                        doUnassign(seats, $selEntry, bClearAll, $unassignDlg);
                    },
                });

                // get the date you need
                let callSheetAssignedName = $('.colAssignedTo', $selEntry).attr('assignedTo');
                callSheetAssignedName = callSheetAssignedName.split(',');
                let callSheetAssignedDate = $('.colDateAssigned', $selEntry).attr('assignedToDate');
                callSheetAssignedDate = callSheetAssignedDate.split(',');

                // build the list
                sTmp = [];
                for (let i = 0; i < callSheetAssignedSeat.length; i++) {
                    sTmp.push('<div class="entry" seatId="' + callSheetAssignedSeat[i] + '">');
                        sTmp.push('<div class="col colSelect">');
                            sTmp.push('<div class="icon icon-box-unchecked"></div>');
                        sTmp.push('</div>');

                        sTmp.push('<div class="col colDate">' + callSheetAssignedDate[i] + '</div>');

                        sTmp.push('<div class="col colName" title="' + callSheetAssignedName[i] + '">' + callSheetAssignedName[i] + '</div>');
                    sTmp.push('</div>');
                }
                $('.unassignRaiserLst', $unassignDlg).html(sTmp.join(''));

                // selectors
                let $unassignAllText = $('.unassignAllGrpDiv .msg', $unassignDlg);
                let $unassignLst = $('.unassignRaiserLst', $unassignDlg);
                let $unassignSelectText = $('.unassignSelectGrpDiv .msg', $unassignDlg);
                let $unassignLstOverlay = $('.unassignRaiserLstOverlay', $unassignDlg);

                // position mask
                let lstWidth = $unassignLst.width();
                let lstOffset = $unassignLst.offset();
                $unassignLstOverlay.width(lstWidth + 2)
                                   .offset(lstOffset);

                // set the initial value
                $('.radioBtnUnassignAll .icon', $unassignDlg).removeClass('icon-radio-button-off')
                                                             .addClass('icon-radio-button-on');
                $unassignAllText.removeClass('disabled');
                $unassignLstOverlay.show();
                $unassignSelectText.addClass('disabled')

                // radio buttons
                $('.radioBtnDiv', $unassignDlg).on('click', function(e) {
                    let $btnDiv = $(this);

                    // if the button is currently selected skip
                    if ($('.icon', $btnDiv).hasClass('icon-radio-button-on')) {
                        return;
                    }

                    // clear the radio buttons
                    $('.radioBtnDiv .icon', $unassignDlg).removeClass('icon-radio-button-on')
                                                         .addClass('icon-radio-button-off');

                    // select the current buttons
                    $('.icon', $btnDiv).removeClass('icon-radio-button-off')
                                       .addClass('icon-radio-button-on');

                    // see which radio button was selected and do the correct action
                    if ($('.radioBtnUnassignAll .icon', $unassignDlg).hasClass('icon-radio-button-on')) {
                        // unassign all
                        $unassignAllText.removeClass('disabled');
                        $unassignLstOverlay.show();
                        $unassignSelectText.addClass('disabled')
                    }
                    else if ($('.radioBtnUnassignSelect .icon', $unassignDlg).hasClass('icon-radio-button-on')) {
                        // select assignee's to remove
                        $unassignAllText.addClass('disabled');
                        $unassignLstOverlay.hide();
                        $unassignSelectText.removeClass('disabled')
                    }

                });

                // checkbox in the entry list
                $('.unassignRaiserLst', $unassignDlg).on('click', '.entry .col.colSelect .icon', function(e) {
                    let $sel = $(this);

                    // check / uncheck
                    if ($sel.hasClass('icon-box-unchecked')) {
                        $sel.removeClass('icon-box-unchecked')
                            .addClass('icon-box-checked');
                    }
                    else {
                        $sel.removeClass('icon-box-checked')
                            .addClass('icon-box-unchecked');
                    }
                })
            }

        }); // $btnGrpUnassignAssign

        /*
         * Call Sheet Groups
         */
        $btnGrpPopupMenu.on('click', function(e) {
            $btn = $(this);

            // see if disabled
            if ($btn.hasClass('disabled'))
                return;

            var sTmp = [];

            sTmp.push('<div class="callSheetGrpMenuDiv">');
                sTmp.push('<div class="entry newCallSheetGrp">');
                    sTmp.push('<div class="text">Create a New Call Sheet Group</div>');
                    sTmp.push('<div class="icon icon-new-call-group"></div>');
                sTmp.push('</div>');
                /*
                sTmp.push('<div class="entry disabled removeUncalledFromCallSheetGrp">');
                    sTmp.push('<div class="text">Remove Uncalled from Group</div>');
                    sTmp.push('<div class="icon icon-remove-uncalled"></div>');
                sTmp.push('</div>');
                */
                if (features.CSV_EXPORT_CALL_TIME) {
                    sTmp.push('<div class="entry exportCallSheetGrp">');
                }
                else {
                    sTmp.push('<div class="entry exportCallSheetGrp disabled">');
                }

                    sTmp.push('<div class="text">Export to CSV</div>');
                    sTmp.push('<div class="icon icon-export-csv"></div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            let $popup = $btn.revupPopup({
                extraClass: 'callSheetGrpMenu',
                location: 'below',
                content: sTmp.join(''),
                //minLeft: 145,
                //maxRight: 415,
                bPassThrough: false
            });
            var totalGroups = ($('.grpLst .entry').length);
            if(totalGroups == 0){
                $('.callSheetGrpMenuDiv .exportCallSheetGrp').addClass('disabled').css('pointer-events', 'none');
            }

            $('.newCallSheetGrp', $popup).on('click', function(e) {
                // close the menu
                $popup.revupPopup('close');

                // display the dialog
                let msg = [];
                msg.push('<div class="bodyDiv">');
                    msg.push('<div class="msg">Add the selected Contact(s) to a new group:</div>');
                    msg.push('<input class="newGrpName" autofocus tabIndex="1" type="text" placeholder="Group Name">')
                msg.push('</div>');
                $newGrpDlg = $('body').revupDialogBox({
                    extraClass: 'newCallSheetGrpDlg',
                    enableOkBtn: false,
                    headerText: 'Create Call Sheet Group',
                    msg: msg.join(''),
                    zIndex: 60,
                    width: '450px',
                    okHandler: function(e) {
                        createNewCallSheetGrp();
                    }
                });

                function createNewCallSheetGrp()
                {
                    let data = {};
                    data.title = $newGrpInput.val();
                    $.ajax({
                        type: 'POST',
                        url: templateData.callTimeSetsApi,
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                    })
                    .done(function(r) {
                        // add to the beginning of list
                         $('.noResultsDiv').hide();
                        $grpDetailsLstFooter.hide();
                        let grps = [];
                        grps.push(r);
                        let grpEntry = buildCallSheetGrps(grps, 1, true);

                        // update the attributes
                        let numentries = (parseInt($grpLst.attr('numentries')) + 1);
                        $grpLst.attr('numentries', numentries);
                        let maxPages = Math.ceil($grpLst.attr('numentries') / entriesPerPageIndividual);
                        $grpLst.attr('maxpages', maxPages);

                        // update the footer count
                        $individualFooter.hide();
                        $grpFooter.show()
                        $grpFooter.html('<text>Showing </text>' + '<text class="showing">' + parseInt($grpLst.attr('numentriesloaded')) +  '</text> '  + '<text> of </text>' + '<text class="total">' + $grpLst.attr('numEntries') + '</text>' + '<text> groups</text>');

                        $('.entry.selected', $grpLst).removeClass('selected');

                        $grpWrapper.scrollTop(0);
                        $grpWrapper.prepend(grpEntry);

                        tmp = localStorage.getItem('callTimeGroupColWidth-' + templateData.seatId);
                        if (tmp) {
                            tmp = JSON.parse(tmp);
                            if ($('.lstsSection').outerWidth() == tmp.listWidth) {
                                let col = templateData.grpCol;
                                for (let i = 0; i < col.length; i++) {
                                    $('.column.' + col[i].css).width(tmp[col[i].css] + 'px');
                                }
                            }
                        }

                        $('.detailsSection').show();

                        $newCallGroupId = $('.entry.selected', $grpLst).attr('calltimegrpid');

                        fetchCallTimeGrp($newCallGroupId);

                        $('.detailsSection .detailsDiv .lst').show();

                        $grpDetailsBtnCallSheet.addClass('disabled');
                        $grpDetailsBtnCallResults.addClass('disabled');

                        $btnGrpPrint.removeClass('disabled');
                        $btnGrpAssign.removeClass('disabled');
                        $btnGrpDelete.removeClass('disabled');

                    })
                    .fail(function(r) {
                        revupUtils.apiError('Call Time', r, 'Create Call Sheet Group',
                                            "Unable to Create a new Call Sheet Group at this time");
                    })
                } // createNewCallSheetGrp

                let $newGrpInput = $('.newGrpName', $newGrpDlg);
                $('.newGrpName', $newGrpDlg)
                    .on('input', function(e) {
                        // see if the ok button should be enabled
                        var v = $newGrpInput.val();
                        if (v.length > 0) {
                            $('.okBtn', $newGrpDlg).prop('disabled', false);
                        }
                        else {
                            $('.okBtn', $newGrpDlg).prop('disabled', true);
                        }
                    })
                    .on('keydown', function(e) {
                        if (e.keyCode == 13) {
                            var v = $newGrpInput.val();
                            if (v != '') {
                                createNewCallSheetGrp();

                                $newGrpDlg.revupDialogBox('close');
                            }

                        }
                    })
            });

            $('.removeUncalledFromCallSheetGrp', $popup).on('click', function(e) {
                console.log('remove unused')
            });

            $('.exportCallSheetGrp', $popup).on('click', function(e) {
                // close the menu
                $popup.revupPopup('close');

                var sTmp = [];
                sTmp.push('<div class="callTuneGrpCsvDownloadDiv">');
                    sTmp.push('<div class="textMsg">');
                        sTmp.push('To export a copy of your Call Sheet Group entries as a CSV file, select ');
                        sTmp.push('the attributes you want included in the file and click save.  The ');
                        sTmp.push('Contact\'s name is always included by default.')
                    sTmp.push('</div>');

                    sTmp.push('<div class="header" style="margin-top:10px;">Format options</div>');
                    var extraClass = "";
                    if (!templateData.bDisplayQuickFilters) {
                        extraClass = " disabled"
                    }
                    sTmp.push('<div style="margin-top:5px;" class="entry' + extraClass + '">');
                        sTmp.push('<input id="checkbox1" class="csvIncludeName" type="checkbox" checked disabled>');
                        sTmp.push('<label for="checkbox1" class="label"><span><span></span></span>Name</label>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="entry" style="margin-top:5px;">');
                        sTmp.push('<input id="checkbox2" type="checkbox" class="csvIncludeScore" checked>');
                        sTmp.push('<label for="checkbox2" class="label"><span><span></span></span>Score</label>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="entry" style="margin-top:5px;">');
                        sTmp.push('<input id="checkbox3" type="checkbox" class="csvIncludeContactInfo" checked>');
                        sTmp.push('<label for="checkbox3" class="label"><span><span></span></span>Contact Infomation</label>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="entry" style="margin-top:5px;">');
                        sTmp.push('<input id="checkbox4" type="checkbox" class="csvIncludeTotalDonations" checked>');
                        sTmp.push('<label for="checkbox4" class="label"><span><span></span></span>Total donation amount</label>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="entry" style="margin-top:5px;">');
                        sTmp.push('<input id="checkbox5" type="checkbox" class="csvIncludeResultLog" checked>');
                        sTmp.push('<label for="checkbox5" class="label"><span><span></span></span>Results Log</label>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="entry" style="margin-top:5px;">');
                        sTmp.push('<input id="checkbox6" type="checkbox" class="csvIncludeTotalNotes" checked>');
                        sTmp.push('<label for="checkbox6" class="label"><span><span></span></span>Notes</label>');
                    sTmp.push('</div>');
                sTmp.push('</div>');

                var $dlg = $('body').revupDialogBox({
                    headerText: "Save Call Sheet Group as a CSV File",
                    msg: sTmp.join(''),
                    autoCloseOk: false,
                    waitingOverlay: true,
                    waitingStyleShow: false,
                    okHandler: function (e) {
                        // display the waiting
                        $dlg.revupDialogBox('showWaiting');
                        $dlg.revupDialogBox('disableBtn', 'okBtn');
                        $dlg.revupDialogBox('disableBtn', 'cancelBtn');

                        // build the arguments
                        var urlArgs = [];
                        urlArgs.push('show_name=' + $('.csvIncludeName', $dlg).prop('checked'));
                        urlArgs.push('show_score=' + $('.csvIncludeScore', $dlg).prop('checked'));
                        urlArgs.push('show_contact_info=' + $('.csvIncludeContactInfo', $dlg).prop('checked'));
                        urlArgs.push('show_total=' + $('.csvIncludeTotalDonations', $dlg).prop('checked'));
                        urlArgs.push('show_log=' + $('.csvIncludeResultLog', $dlg).prop('checked'));
                        urlArgs.push('show_notes=' + $('.csvIncludeTotalNotes', $dlg).prop('checked'));

                        // build the url
                        let callSheetGrpId = $('.entry.selected', $grpLst).attr('callTimeGrpId');
                        var url2 = templateData.callTimeExportApi.replace('CALLTIME-GRP-ID', callSheetGrpId);

                        // start the download
                        $.ajax({
                            type: 'GET',
                            url:  encodeURI(url2 + '?' + urlArgs.join('&'))
                        })
                        .done(function(result) {
                            $dlg.revupDialogBox('close');
                            updateRevupAlert();
                            $('body').revupMessageBox({
                                headerText:'CSV Export',
                                msg: "Your CSV file is being generated.<br>" +
                                     "<span style='font-weight: bold'>You will receive an email when it is ready for download.</span>" +
                                     "<br><br>Note: Depending on the size of the export, this may take several minutes."
                            })
                        })
                        .fail(function(result) {
                            // hide the waiting
                            $dlg.revupDialogBox('hideWaiting');
                            $dlg.revupDialogBox('enableBtn', 'okBtn');
                            $dlg.revupDialogBox('enableBtn', 'cancelBtn');
                            // display the error
                            revupUtils.apiError('Save CSV', result, 'Unable to create CSV file at this time',
                                                'Unable to create the CSV file at this time');
                        })
                    }
                });
            }) // exportCSV click handler;
        }); // $btnGrpPopupMenu

        // group list event handlers
        $grpLst.on('click', '.entry', function(e) {
            let $entry = $(this);

            // if select there nothing to do
            if ($entry.hasClass('selected'))
                return;

            // clear and set selected
            $('.entry', $grpLst).removeClass('selected');
            $entry.addClass('selected');

            // see if the new entry has someone assigned to it
            if ($entry.attr('seatId')) {
                $btnGrpUnassign.removeClass('disabled');
            }
            else {
                $btnGrpUnassign.addClass('disabled');
            }

            // get the list entris in the list
            let callTimeGrpId = $entry.attr('callTimeGrpId');
            fetchCallTimeGrp(callTimeGrpId);
        });
    } // btnTabEventHandlers

    function sliderListEvents()
    {
        $individualLstHeader.add($grpLstHeader)
            .on('mousedown', '.slider', function(e)
            {
                // see what header for
                let $header = $individualLstHeader;
                let storageSection = 'callTimeIndividualColWidth';
                let $lst = $individualLst;
                let colData = templateData.individualCol;

                if ($(this).closest('.lstHeader').hasClass('grpHeader')) {
                    $header = $grpLstHeader;
                    storageSection = 'callTimeGroupColWidth';
                    $lst = $grpLst;
                    colData = templateData.grpCol;
                }
                /*
                 * slider
                 */
                // which == 1 means left mouse button
                if (e.which == 1) {
                    // get the left column
                    $slider = $(this);
                    $left = $slider.prev();
                    var c = $left.attr('class');
                    c = c.split(' ');
                    for (var i = 0; i < c.length; i++) {
                        c[i] = '.' + $.trim(c[i]);
                    }
                    $leftGrp = $(c.join(''));
                    minLeft = parseInt($(c.join(''), $header).attr('minColWidth'), 10);

                    // get the right column
                    $right = $slider.next();
                    var c = $right.attr('class');
                    c = c.split(' ');
                    for (var i = 0; i < c.length; i++) {
                        c[i] = '.' + $.trim(c[i]);
                    }
                    $rightGrp = $(c.join(''));
                    minRight = parseInt($(c.join(''), $header).attr('minColWidth'), 10);

                    // set the starting point
                    currentPosX = e.pageX;

                    // make sure there is room to resize
                    if (($left.width() <= minLeft) && ($right.width() <= minRight)) {
                        e.preventDefault();

                        return;
                    }

                    $slider.addClass('draggable').parents().on('mousemove', function(e) {
                        if ($slider.hasClass('draggable')) {
                            // set flag resizing
                            bResize = true;

                            var deltaX = currentPosX - e.pageX;
                            currentPosX = e.pageX;

                            // compute the new size
                            var leftWidth = $left.outerWidth();
                            var rightWidth = $right.outerWidth();
                            var newWidthLeft = leftWidth - deltaX;
                            var newWidthRight = rightWidth + deltaX;

                            // make sure the new size meets the minimum
                            var bResizeCol = true;
                            if (newWidthLeft < minLeft) {
                                var d = leftWidth - minLeft;
                                if (d == 0) {
                                    bResizeCol = false;
                                }
                                else {
                                    newWidthLeft =  minLeft;
                                    newWidthRight = rightWidth + d;
                                }
                            }
                            else if (newWidthRight < minRight) {
                                var d = rightWidth - minRight;
                                if (d == 0) {
                                    bResizeCol = false;
                                }
                                else {
                                    newWidthRight = minRight;
                                    newWidthLeft = leftWidth + d;
                                }
                            }

                            //resize
                            if (bResizeCol) {
                                $leftGrp.outerWidth(newWidthLeft);
                                $rightGrp.outerWidth(newWidthRight);

                                // save the columns width
                                var wData = {};
                                wData.listWidth = $lst.outerWidth();
                                for (var i = 0; i < colData.length; i++) {
                                    // save the column width
                                    if (colData[i]) {
                                        wData[colData[i].css] = $('.column.' + colData[i].css, $lst).outerWidth();
                                    }
                                }
                                localStorage.setItem(storageSection + '-' + templateData.seatId, JSON.stringify(wData));
                            }
                        }
                    }).on('mouseup', function(e) {
                        // stop dragging
                        $slider.removeClass('draggable')
                    })
                }

                e.preventDefault();
            })
            .on('mouseup', function() {
                $('.draggable').removeClass('draggable')
            });

        // handle the resize of the window
        var resizeTimer = undefined;
        $(window)
            .on('resize', function(e) {
                // save only if resize of a column
                let storageSection = 'callTimeIndividualColWidth';
                if ($lstTabGroup.hasClass('active')) {
                    storageSection = 'callTimeGroupColWidth';
                }

                if (localStorage.getItem(storageSection + '-'  + templateData.seatId)) {
                    if (resizeTimer) {
                        clearTimeout(resizeTimer)
                    }

                    setTimeout(function() {
                        let storageSection = 'callTimeIndividualColWidth';
                        let $lst = $individualLst;
                        let colData = templateData.individualCol;
                        if ($('.tab.active', $lstTabs).hasClass('groupTab')) {
                            storageSection = 'callTimeGroupColWidth';
                            $lst = $grpLst;
                            colData = templateData.grpCol;
                        }

                        // resize the columns
                        var bMinHit = false;

                        var sWidth = 0;
                        for (var i = 0; i < colData.length; i++) {
                            if (!colData[i].resizePercent) {
                                var $col = $('.lstSection .column.' + colData[i].css);
                                var cWidth = $col.width();
                                sWidth += cWidth;
                            }
                        }
                        sWidth += colData.length * 20;      // margin
                        sWidth += (colData.length - 1) * 5; // sliders

                        for (var i = 0; i < colData.length; i++) {
                            if ($('.tab.active', $lstTabs).hasClass('groupTab')) {
                                var $col = $('.lstSection .column.' + colData[i].css);
                                $col.removeAttr("style");
                            }
                            // see if a resizeable column
                            else if (colData[i].resizePercent) {
                                var newWidth = $('.lstSection').width();
                                var wDelta = newWidth - (sWidth + 20);
                                var resizePercent = colData[i].resizePercent / 100;
                                var w = Math.floor(wDelta * resizePercent);
                                var $col = $('.lstSection .column.' + colData[i].css);
                                // $col.width(w);

                                // if the resizeable column gets to small reset columns
                                if (w < colData[i].minColWidth) {
                                    bMinHit = true;
                                    break;
                                }
                                else {
                                    $col.width(w);
                                }
                            }
                        }

                        if (bMinHit) {
                            for (var j = 0; j < colData.length; j++) {
                                var $col = $('.lstSection .column.' + colData[j].css);
                                $col.css('width', colData[j].width)
                                $col.css('background-color', 'yellow');
                            }
                        }

                        // save the columns width
                        var wData = {};
                        wData.listWidth = $lst.outerWidth();
                        for (var i = 0; i < colData.length; i++) {
                            // save the column width
                            if (colData[i].css) {
                                wData[colData[i].css] = $('.column.' + colData[i].css).outerWidth();
                            }
                        }
                        if ($('.tab.active', $lstTabs).hasClass('groupTab')) {
                            localStorage.removeItem('callTimeGroupColWidth-' + templateData.seatId);
                        }
                        else {
                            localStorage.setItem(storageSection + '-' + templateData.seatId, JSON.stringify(wData));
                        }
                    }, 250)
                }

                let newHeight = $(window).height() - ($('.lstsSection').offset().top) - $('.footerDiv').height();
                $individualLst.height(newHeight);
                $indivWrapper.height(newHeight - 1);

                $grpLst.height(newHeight);
                $grpWrapper.height(newHeight - 1);
                $grpDetailsLst.height(newHeight);
            })
    } // sliderListEvents

    function detailsEvents() {
        // individual call sheet details button handlers
        $individualDetailsBtnSendMail.on('click', function(e) {
            let $btn = $(this);

            // see if disabled
            if ($btn.hasClass('disabled')) {
                return;
            }

            let bSingle = $('.email', $individualDetailsContactDiv).attr('bSingle');
            let emailAddr = '';
            if (bSingle == 'true') {
                emailAddr = $('.email', $individualDetailsContactDiv).text()
            }
            else {
                let bValue = $('.email', $individualDetailsContactDiv).revupDropDownField('getBothValues');
                emailAddr = bValue.displayValue;
            }
            let href = 'mailto:' + emailAddr;
            window.location = href;

            let contactId = $individualDetailsHeader.attr('contactId');
            let callTimeContactId = $individualDetailsHeader.attr('callTimeContactId')

            // log the email
            let data = {};
            data.log_type = "CAL";
            data.ct_contact = callTimeContactId;

            let url = templateData.callTimeLogApi;
            url = url.replace("CALLTIME-CONTACT-ID", callTimeContactId)
            $.ajax({
                url: url,
                type: "POST",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
            })
            .done(function(r) {
            })
            .fail(function(r) {
                revupUtils.apiError('Call Time', r, 'Log Email Sent',
                                    "Unable to log email sent at this time");
            })
        });

        $individualDetailsBtnMoreDetails.on('click', function(e) {
            let $btn = $(this);

            // see if disabled
            if ($btn.hasClass('disabled')) {
                return;
            }

            let contactId = $individualDetailsHeader.attr('contactId');
            let callTimeContactId = $individualDetailsHeader.attr('callTimeContactId');

            // display a loading overlay - long wait
            let sTmp = [];
            sTmp.push('<div class="loadingApi" style="height:100%;width:100%;position:absolute;z-index:1000;top:0;opacity:0.4;background-color:' + revupConstants.color.blackText + ';>');
                sTmp.push('<div class="loadingDiv">');
                    sTmp.push('<div class="loading"></div>');
                    sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                sTmp.push('</div>');
            sTmp.push('</div>');
            $('body').append(sTmp.join(''));

            // get the contact details
            // let url = templateData.contactDetailApi.replace('contactId', contactId);
            let url = templateData.callTimeContactsApi + callTimeContactId + '/';
            $.ajax({
                url: url,
                type: 'GET',
            })
            .done(function(r) {
                var browserHeight = $(document).height();
                var viewportHeight = $(window).height();
                var $btn = $(this);
                var $detail = $btn.closest('.rankingDetailsDetailDiv');

                // get data
                var analysisId = $detail.attr('analysisId');
                var contactId = $detail.attr('contactId');
                var resultId = $detail.attr('resultId');
                var partition = $detail.attr('activePartition');

                var wWidth = $('body').width();
                var wHeight = $('body').height()

                // create the popup
                var sTmp = [];
                sTmp.push('<div class="mainPageLoading" style="height:500px">');
                    sTmp.push('<div class="loadingDiv">');
                        sTmp.push('<div class="loading"></div>');
                        sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                    sTmp.push('</div>');
                sTmp.push('</div>');
                $('body').css('overflow', 'hidden');
                var $prospectDetailsDlg = $('body').revupDialogBox({
                    cancelBtnText: '',
                    okBtnText: 'Close',
                    extraClass: 'prospectDetailsPopUp',
                    msg: sTmp.join(''),
                    top: '20',
                    width: wWidth - 200 + 'px',
                    // height: wHeight - 550 + 'px',
                    height: Math.min(browserHeight, viewportHeight)-30,
                    cancelHandler: function() {
                        prospectDetails.close();
                        $('body').css('overflow', '');
                    },
                    okHandler: function() {
                        prospectDetails.close();
                        $('body').css('overflow', '');
                    },
                })

                setTimeout(function() {
                    let contactId = $individualDetailsHeader.attr('contactId');
                    let callTimeContactId = $individualDetailsHeader.attr('callTimeContactId')
                    let primaryPhone = $individualDetailsHeader.attr('primaryPhone');
                    let primaryEmail = $individualDetailsHeader.attr('primaryEmail');

                    var content = buildProspectDetailsFrame();
                    $('.msgBodyScroll', $prospectDetailsDlg).html(content)

                    // build an object with the data for prospect details
                    var prospectDetailData = {
                        // template data
                        contactId:          Number(contactId),
                        seatId:             Number(templateData.seatId),
                        accountId:          Number(templateData.accountId),
                        callTimeContactId:  Number(callTimeContactId),

                        // which partition is displayed
                        partitionLst:   [],

                        // set the new primary phone
                        phoneShowMulti: true,
                        //primaryPhoneApi: templateData.primaryPhoneApi,
                        phoneUpdateFunc: function(newPhoneId) {
                            // change the new primary phone value
                            $individualDetailsHeader.attr('primaryPhone', newPhoneId);
                            $('.phone', $individualDetailsContactDiv).revupDropDownField('setValue', newPhoneId);

                            // save the new primary phone number via it is
                            let url = templateData.primaryPhoneApi + callTimeContactId + '/';
                            $.ajax({
                                url: url,
                                type: 'PUT',
                                data: JSON.stringify({
                                    primary_phone: newPhoneId,
                                }),
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                            })
                            .done(function(r) {
                            })
                            .fail(function(r) {
                                revupUtils.apiError('Call Time', r, 'Set Primary Phone Number',
                                                    "Unable set Primary Phone Number for the prospect details popup at this time");
                            })
                            .always(function(r) {
                                bMovementHappening = false;
                            });
                        },

                        // set the new email
                        emailShowMulti: true,
                        emailUpdateFunc: function(newEmailId) {
                            // change the new primary phone value
                            $individualDetailsHeader.attr('primaryEmail', newEmailId);
                            $('.email', $individualDetailsContactDiv).revupDropDownField('setValue', newEmailId);

                            // save the new primary phone number via it is
                            let url = templateData.primaryEmailApi + callTimeContactId + '/';
                            $.ajax({
                                url: url,
                                type: 'PUT',
                                data: JSON.stringify({
                                    primary_email: newEmailId,
                                }),
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                            })
                            .done(function(r) {
                            })
                            .fail(function(r) {
                                revupUtils.apiError('Call Time', r, 'Set Primary Email Address',
                                                    "Unable set Primary Email Address for the prospect details popup at this time");
                            })
                            .always(function(r) {
                                bMovementHappening = false;
                            });
                        },

                        // urls
                        prospectDetailUrl:  templateData.prospectDetailUrl,
                    };

                    // add extra attr to the prospect details object
                    if (primaryPhone) {
                        prospectDetailData.primaryPhone = primaryPhone;
                    }
                    if (primaryEmail) {
                        prospectDetailData.primaryEmail = primaryEmail;
                    }

                    prospectDetails.display(prospectDetailData, r);
                }, 1);
            })
            .fail(function(r) {
                revupUtils.apiError('Call Time', r, 'Prospect Details',
                                    "Unable to detail data at this time");
            }).always(function(r){
                // clear the loading overlay
                $('.loadingApi').remove();
            })
        });

        $individualDetailsBtnCallSheet.on('click', function(e) {
            let $btn = $(this);

            // see if disabled
            if ($btn.hasClass('disabled')) {
                return;
            }

            // see if disabled
            if ($btn.hasClass('disabled')) {
                return;
            }

            // get the contact and result id
            let contactId = $individualDetailsHeader.attr('contactId');
            let callTimeContactId = $individualDetailsHeader.attr('callTimeContactId');
            let primaryPhone = $individualDetailsHeader.attr('primaryPhone');
            let primaryEmail = $individualDetailsHeader.attr('primaryEmail');

            // display the call sheet
            function primaryPhoneUpdateFunc(newPhoneId)
            {
                $individualDetailsHeader.attr('primaryPhone', newPhoneId);
                $('.phone', $individualDetailsContactDiv).revupDropDownField('setValue', newPhoneId);
            } // primaryPhoneUpdateFunc
            function primaryEmailUpdateFunc(newEmailId)
            {
                $individualDetailsHeader.attr('primaryEmail', newEmailId);
                $('.email', $individualDetailsContactDiv).revupDropDownField('setValue', newEmailId);
            } // primaryEmailUpdateFunc
            displayCallSheet(contactId, callTimeContactId, primaryPhone, primaryPhoneUpdateFunc, primaryEmail, primaryEmailUpdateFunc);
        });

        $individualDetailsAsk
            .on('keypress keyup paste', function(e) {
                $individualDetailsAsk.attr('notesChanged', true)
            })
            .on('blur', function(e) {
                if ($individualDetailsAsk.attr('notesChanged') == 'true') {
                    let contactId = $individualDetailsHeader.attr('contactId');
                    let contactNotesUrl = templateData.callsheetNotesApi.replace("contactId", contactId)
                                                                        .replace('contactSetId', 'aroot');
                    let nPayload = {};
                    nPayload.notes1 = $individualDetailsAsk.html();
                    let notesId = $individualDetailsAsk.attr('notesId');
                    let aType = 'POST'
                    if (notesId) {
                        aType = 'PUT';
                        contactNotesUrl = contactNotesUrl + notesId + '/';
                    }

                    $.ajax({
                        type: aType,
                        url: contactNotesUrl,
                        data: JSON.stringify(nPayload),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                    })
                    .done(function(r) {
                    })
                    .fail(function(r) {
                        revupUtils.apiError('Individual Details', r, 'Change/Create Ask',
                                            "Unable to save/create the Ask from Individual Details at this time");
                    })

                    }
            })


        function clearMovement($entry) {
            var $e = $entry
            setTimeout(function() {
                $e.removeClass('movementHighlight');
            }, 2500);
        }
        let bMovementHappening = false
        let $dragStart = $();
        let $dragOver = $();

        $grpDetailsLst
            .on('mouseenter', '.entry', function() {
                let $entry = $(this);
                $entry.addClass('highlight');
            })
            .on('mouseleave', '.entry', function() {
                let $entry = $(this);
                $entry.removeClass('highlight');
            })
            /*
            .on('draggable', '.entry', function(e) {
console.log('ondraggable')
                //e.preventDefault();
                //e.stopPropagation();
            })
            .on('draginit', '.entry', function(e) {
console.log('DRAGINIT')
            })
            .on('dragstart', '.entry', function(e) {
console.log('ondragstart')
                //e.preventDefault();
                //e.stopPropagation();

                e.originalEvent.dataTransfer.effectAllowed = 'move';

                $dragStart = $(this);

                $(this).addClass('moving')
            })
            .on('dragend', '.entry', function(e) {
console.log('ondragend')
                //e.preventDefault();
                //e.stopPropagation();

                $(this).removeClass('over');
                $dragStart.removeClass('moving');
                $dragStart = $();

                $('.entry', $grpDetailsLst).removeClass('highlight');
                $(this).addClass('highlight');
            })
            */


            .on('dragstart', '.entry', function(e) {
                // when dragging start - disable pointer events on sub-parts
                $('.entry .whenDragging', $grpDetailsLst).css('pointer-events', 'none');

                $dragStart = $(this);
                $dragStart.addClass('moving');
            })

            .on('dragend', '.entry', function(e) {
                // re-enable pointer events
                $('.entry .whenDragging', $grpDetailsLst).css('pointer-events', 'auto');

                // done if the same
                if ($dragOver.length == 0 || $dragOver.is($()) || $dragStart.is($dragOver)) {
                    $dragStart.removeClass('moving');

                    $dragStart = $();
                    $dragEnd = $();

                    return;
                }

                let startClone = $dragStart.clone();
                let overClone = $dragOver.clone();
                // $dragStart.replaceWith(overClone);
                // $dragOver.replaceWith(startClone);
                $dragStart.insertBefore($dragOver)//.prev('.entry'));
                $dragStart.removeClass('moving');

                $dragOver.removeClass('moving');

                // save the new position
                let callTimeId = $dragStart.attr('callTimeId');
                let callTimeGrpId = $('.entry.selected', $grpLst).attr('callTimeGrpId');
                let url = templateData.moveContactInSetApi;
                url = url.replace('CALLTIME-SET-ID', callTimeGrpId);
                url = url.replace('CALLTIME-CONTACT-ID', callTimeId);
                url += 'ordering/'
                $.ajax({
                    url: url,
                    type: 'PUT',
                    data: JSON.stringify({
                        "mode": "relative",
                        "position": "before",
                        "id": $dragOver.attr('callTimeId')
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                })
                .done(function(r) {
                })
                .fail(function(r) {
                    revupUtils.apiError('Call Time', r, 'Change Order of ',
                                        "Unable to move the prospect up at this time");
                })
                .always(function(r) {
                    bMovementHappening = false;
                });
            })

            .on('dragenter', '.entry', function(e) {
                //e.preventDefault();
                //e.stopPropagation();

                $dragOver.removeClass('over');
                $(this).addClass('over')

                $dragOver = $(this)
            })
            .on('dragleave', '.entry', function(e) {
                //e.preventDefault();
                //e.stopPropagation();

                $dragOver.removeClass('over')
                $dragOver = $();

            })

            .on('dragover', '.entry', function(e) {
                if (e.isPropagationStopped())
                    e.stopPropagation();

                e.originalEvent.dataTransfer.dropEffect = 'move';

                //$(this).addClass('over')
                return false;
            })

            .on('drop', '.entry', function(e) {
                //e.preventDefault();
                //e.stopPropagation();

                $dragOver.removeClass('over')
            })
            .on('click', ".moveUp", function(e) {
                // limit to only one movement at a time
                if (bMovementHappening)
                    return;

                bMovementHappening = true;

                // skip if the first entry
                let $entry = $(this).closest('.entry');
                if ($entry.is('.entry:first-of-type', $grpDetailsLst)) {
                    return;
                }

                // move it
                let $prevEntry = $entry.prev();
                //$entry.insertBefore($prevEntry);
                $entry.fadeOut('slow', function() {
                    $entry.insertBefore($prevEntry).fadeIn('slow');
                })

                // change the highlight to movementHighlight
                $entry.removeClass('highlight').addClass('movementHighlight');
                $prevEntry.addClass('highlight');

                // clear the movement highling is 10 seconds
                //clearMovement($entry);

                // save the movement
                let callTimeId = $entry.attr('callTimeId');
                let callTimeGrpId = $('.entry.selected', $grpLst).attr('callTimeGrpId');;
                let url = templateData.moveContactInSetApi;
                url = url.replace('CALLTIME-SET-ID', callTimeGrpId);
                url = url.replace('CALLTIME-CONTACT-ID', callTimeId);
                url += 'ordering/'
                $.ajax({
                    url: url,
                    type: 'PUT',
                    data: JSON.stringify({
                        "mode": "shift",
                        "direction": "up",
                        "places": "1"
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                })
                .done(function(r) {
                    $entry.removeClass('movementHighlight');
                })
                .fail(function(r) {
                    revupUtils.apiError('Call Time', r, 'Change Order of ',
                                        "Unable to move the prospect up at this time");
                })
                .always(function(r) {
                    bMovementHappening = false;
                });

                e.preventDefault()
                return false;
            })
            .on('click', ".moveDown", function(e) {
                // limit to only one movement at a time
                if (bMovementHappening)
                    return;

                bMovementHappening = true;

                // skip if the last entry
                let $entry = $(this).closest('.entry');
                if ($entry.is('.entry:last-of-type', $grpDetailsLst)) {
                    return;
                }

                // move it
                let $nextEntry = $entry.next();
                //$entry.insertAfter($nextEntry);
                $entry.fadeOut('slow', function() {
                    $entry.insertAfter($nextEntry).fadeIn('slow');
                })

                // change the highlight to movementHighlight
                $entry.removeClass('highlight').addClass('movementHighlight');
                $nextEntry.addClass('highlight')

                // clear the movement highling is 10 seconds
                //clearMovement($entry);

                // save the movement
                let callTimeId = $entry.attr('callTimeId');
                let callTimeGrpId = $('.entry.selected', $grpLst).attr('callTimeGrpId');
                let url = templateData.moveContactInSetApi;
                url = url.replace('CALLTIME-SET-ID', callTimeGrpId);
                url = url.replace('CALLTIME-CONTACT-ID', callTimeId);
                url += 'ordering/'
                $.ajax({
                    url: url,
                    type: 'PUT',
                    data: JSON.stringify({
                        "mode": "shift",
                        "direction": "down",
                        "places": "1"
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                })
                .done(function(r) {
                    $entry.removeClass('movementHighlight');
                })
                .fail(function(r) {
                    revupUtils.apiError('Call Time', r, 'Change Order of ',
                                        "Unable to move the prospect down at this time");
                })
                .always(function(r) {
                    bMovementHappening = false;
                });

                e.preventDefault()
                return false;
            })
            .on('click', '.entry', function(e) {
                // clear and set new selection
                $('.entry', $grpDetailsLst).removeClass('selected')
                $(this).addClass('selected');

                $grpDetailsBtnCallSheet.removeClass('disabled');
                $grpDetailsBtnCallResults.removeClass('disabled');
            })
            .on('click', ".removeContactBtn", function(e) {
                // limit to only one movement at a time
                if (bMovementHappening)
                    return;

                let $entry = $(this).closest('.entry');

                // get the id's
                let callTimeId = $entry.attr('callTimeId');
                let callTimeName = $('.nameDiv', $entry).text();
                let callTimeGrpId = $('.entry.selected', $grpLst).attr('callTimeGrpId');
                let callSheetGrpName = $('.entry.selected .name', $grpLst).text();

                // display the confermation box
                let msg = 'Are you sure you want to remove <b>' + callTimeName + '</b> from the Call Sheet Group <b>"' + callSheetGrpName + '"</b>?'
                let $delConfBox = $('body').revupConfirmBox({
                    headerText: 'Delete From Call Sheet Group',
                    msg: msg,
                    zIndex: 25,
                    isDragable: false,
                    autoCloseOk: false,
                    okHandler: function() {
                        // get the next entry and hightligh
                        let $prevEntry = $entry.next();
                        $prevEntry.addClass('highlight');

                        /// remove from list
                        //$entry.remove();

                        // remove from the list
                        let url = templateData.addToCallTimeSetApi;
                        url = url.replace("CALLTIME-SET-ID", callTimeGrpId);
                        url += callTimeId + '/';
                        $.ajax({
                            url: url,
                            type: 'DELETE'
                        })
                        .done(function(r) {
                            // remove from list
                            $entry.remove();


                            //disable the Call Results and Call Sheet buttons when it is a successful delete. User has to select another entry to enable them again
                            $('.detailsHeaderDiv .callSheetBtn').addClass('disabled');
                            $('.detailsHeaderDiv .callResultsBtn').addClass('disabled');

                            $delConfBox.revupConfirmBox('close');

                            // see if the list needs to be reloaded
                            let currentPage = parseInt($grpDetailsLst.attr('currentPage'), 10);
                            let maxPages = parseInt($grpDetailsLst.attr('maxPages'), 10);
                            if (currentPage < maxPages) {
                                reloadCallSheetGrpDetails();
                            }

                            // update count
                            let numTotal = parseInt($('.numTotal', $grpDetailsLstFooter).text(), 10);
                            $('.numTotal', $grpDetailsLstFooter).text(numTotal - 1);
                            $('.numLoaded', $grpDetailsLstFooter).text(numTotal - 1);

                            $newCallGroupId = $('.entry.selected', $grpLst).attr('calltimegrpid');
                            fetchCallTimeGrp($newCallGroupId);

                        })
                        .fail(function(r) {
                            revupUtils.apiError('Call Time', r, 'Remove Prospect',
                                                "Unable to remove prospect at this time");
                        })
                        .always(function(r) {
                            bMovementHappening = false;
                        });
                    },
                    cancelHandler: undefined
                });

                e.preventDefault();
                return false;
            });
        $grpDetailsLstOuter
            .on('scroll', function() {
                let $timeToAddMore = $('.entry.timeToAddMore', $grpDetailsLst);
                if ($timeToAddMore.length > 0) {
                    if ($timeToAddMore.visible($grpDetailsLstOuter)) {
                        $timeToAddMore.removeClass('timeToAddMore');
                        $('.entry.loadingMore', $grpDetailsLst).show();
                        let callTimeGrpId = $('.entry.selected', $grpLst).attr('callTimeGrpId');
                        fetchCallTimeGrp(callTimeGrpId, true);
                    }
                }
            })
        $grpDetailsLstOuter
            .on('mousewheel', function(e) {
                if($grpDetailsLstOuter.prop('scrollHeight') - $grpDetailsLstOuter.scrollTop() <= $grpDetailsLst.height() && (e.originalEvent.wheelDelta < 0)) {
                    e.preventDefault()
                }
            });

        $('window')
            .on('resize', function(e) {
console.log('height: ', $('.entry["contactId=35011"], $grpDetailsLst').height())
            });

        $grpDetailsBtnCallResults.on('click', function(e) {
            let $btn = $(this);

            // see if disabled
            if ($btn.hasClass('disabled')) {
                return;
            }

            // selectors
            let $selEntry = $('.entry.selected', $grpDetailsLst);
            let contactId = $selEntry.attr('contactId');
            let callTimeId = $selEntry.attr('callTimeId');
            let primaryPhone = $selEntry.attr('primaryPhone');
            let primaryEmail = $selEntry.attr('primaryEmail');
            let callSheetGrpId = $('.entry.selected', $grpLst).attr('callTimeGrpId');

            // flags
            let pledgeSet = false;
            let notesChanged = false;
            let callStatusEnabled = true;
            let callStatusChanged = false;

            // build the template
            let sTmp = [];
            sTmp.push('<div class="callResultsDlg">');
                sTmp.push('<div class="userInfoDiv">');

                    sTmp.push('<div class="imageDiv"></div>');

                    sTmp.push('<div class="nameDiv">');
                        sTmp.push('<div class="name firstName"></div>');
                        sTmp.push('<div class="name lastName"></div>');
                        sTmp.push('<div class="primaryEmail"></div>');
                        sTmp.push('<div class="primaryPhone"></div>')
                    sTmp.push('</div>');
                sTmp.push('</div>');
                sTmp.push('<div class="pledgeDiv">');
                    sTmp.push('<div class="title">Pledged Amount</div>');
                    sTmp.push('<div class="pledgeAmountDiv">');
                        sTmp.push('<div class="dollarSign"></div>');
                        sTmp.push('<input class="pledgeAmount">');
                    sTmp.push('</div>');
                sTmp.push('</div>')
                sTmp.push('<div class="statusDiv" style="display:none">');
                    sTmp.push('<div class="title">Call Status</div>');
                    sTmp.push('<div class="statusLst"></div>');
                sTmp.push('</div>');
                sTmp.push('<div class="notesDiv">');
                    sTmp.push('<div class="title">Notes</div>');
                    sTmp.push('<div class="notes" contenteditable=true></div>');
                sTmp.push('</div>');

                sTmp.push('<div class="callTypeDiv">');
                    sTmp.push('<div class="title">Call Type</div>');
                    sTmp.push('<div class="radioBtnGrpDiv">')
                        sTmp.push('<div class="radioEntry">');
                            sTmp.push('<div class="rBtn">');
                                sTmp.push('<div class="icon radioBtn icon-radio-button-on" radioGrp="callType" value="CAL"></div>');
                                sTmp.push('<div class="icon icon-radio-button-back radioBackground"></div>');
                            sTmp.push('</div>');
                            sTmp.push('<div class="rLabel">Phone Call</div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="radioEntry">');
                            sTmp.push('<div class="rBtn">');
                                sTmp.push('<div class="icon radioBtn icon-radio-button-off" radioGrp="callType" value="SMS"></div>');
                                sTmp.push('<div class="icon icon-radio-button-back radioBackground"></div>');
                            sTmp.push('</div>');
                            sTmp.push('<div class="rLabel">Text</div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="radioEntry">');
                            sTmp.push('<div class="rBtn">');
                                sTmp.push('<div class="icon radioBtn icon-radio-button-off" radioGrp="callType" value="EML"></div>');
                                sTmp.push('<div class="icon icon-radio-button-back radioBackground"></div>');
                            sTmp.push('</div>');
                            sTmp.push('<div class="rLabel">Email</div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>')

                sTmp.push('<div class="statusBtnDiv">');
                    sTmp.push('<div class="row">');
                        sTmp.push('<div class="statusBtn noAnswerBtn" value="NOA">No Answer</div>');
                        sTmp.push('<div class="statusBtn leftMessageBtn" value="MSG">Left Message</div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="row">');
                        sTmp.push('<div class="statusBtn refusedBtn" value="REF">Refused</div>');
                        sTmp.push('<div class="statusBtn badNumberBtn" value="BAD">Bad Number</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            let $callSheetResultDlg = $('body').revupDialogBox({
                enableOkBtn: false,
                headerText: 'Call Sheet Results',
                msg: sTmp.join(''),
                top: 50,
                zIndex: 60,
                width: '450px',
                okHandler: function() {
                    saveCallSheetResults();
                }
            });

            // get the contact details
            let contactUrl = templateData.callTimeContactDetailApi.replace("CALLTIME-CONTACT-ID", callTimeId);
            let contactNotes = templateData.callsheetNotesApi.replace("contactId", contactId)
                                                             .replace('contactSetId', 'aroot');
            $.ajax({
                type: 'GET',
                url:contactUrl})
            .done(function(r) {
                let c = r.contact;
                let notes = r.notes[0];

                // fill in the name, photo, phone, email
                let $imgDiv = $('.imageDiv', $callSheetResultDlg);
                if (c.image_urls && c.image_urls.length > 0) {
                    let imgUrl = c.image_urls[0].url;
                    if (c.image_urls.length > 1) {
                        for(let i = 0; i < c.image_urls.length; i++) {
                            if (c.image_urls[i].source == 'LinkedIn') {
                                imgUrl = c.image_urls[i].url;
                                break;
                            }
                        }
                    }

                    imgUrl = '<img class=img src="' + imgUrl + '">';
                    $imgDiv.html(imgUrl);
                }
                else {
                    $imgDiv.hide();
                }

                let $nameDiv = $('.nameDiv', $callSheetResultDlg);

                if (c.first_name)
                    $('.firstName', $nameDiv).text(c.first_name);
                else
                    $('.firstName', $nameDiv).hide()

                if (c.last_name)
                    $('.lastName', $nameDiv).text(c.last_name);
                else
                    $('.lastName', $nameDiv).hide();

                if (c.email_addresses && c.email_addresses.length > 0) {
                    let email = '';
                    if (primaryEmail) {
                        for (let i = 0; i < c.email_addresses.length; i++) {
                            if (c.email_addresses[i].id == primaryEmail) {
                                email = c.email_addresses[i].address;
                                break;
                            }
                        }
                    }
                    else {
                        email = c.email_addresses[0].address;
                    }

                    $('.primaryEmail', $nameDiv).text(email)
                }
                else {
                    $('.primaryEmail', $nameDiv).hide();
                }

                if (c.phone_numbers && c.phone_numbers.length > 0) {
                    let phone = '';
                    if (primaryPhone) {
                        for (let i = 0; i < c.phone_numbers.length; i++) {
                            if (c.phone_numbers[i].id == primaryPhone) {
                                phone = c.phone_numbers[i].number;
                                break;
                            }
                        }
                    }
                    else {
                        phone = c.phone_numbers[0].number;
                    }

                    $('.primaryPhone', $nameDiv).text(revupUtils.getFormattedPhone(phone))
                }
                else {
                    $('.primaryPhone', $nameDiv).hide();
                }

                // display the notes
                if (notes && notes.id) {
                    $('.notesDiv .notes', $callSheetResultDlg).html(notes.notes2)
                                                              .attr('notesId', notes.id);

                    // scroll to bottm
                    let h = $('.notesDiv .notes', $callSheetResultDlg).height();
                    let scrollHeight = $('.notesDiv .notes', $callSheetResultDlg).prop('scrollHeight');
                    $('.notesDiv .notes', $callSheetResultDlg).scrollTop(scrollHeight - h);
                }
            })
            .fail(function(r) {
                revupUtils.apiError('Call Results', r, 'Contact Details',
                                    "Unable to retreive contact details at this time");
            })

            // switch call results
            $('.statusBtn', $callSheetResultDlg).on('click', function(e) {
                let $btn = $(this);

                if ($btn.hasClass('disabled')) {
                    return;
                }

                // if selected is clicked ignore
                if ($btn.hasClass('selected')) {
                    return;
                }

                // clear current selection
                $('.statusBtn', $callSheetResultDlg).removeClass('selected');

                // select the new button
                $btn.addClass('selected');

                // update the ok button
                callStatusChanged = true;
                enableDisableOk();
            })

            // click anywhere in the edit field
            $('.pledgeAmountDiv', $callSheetResultDlg).on('click', function() {
                $('.pledgeAmount', $callSheetResultDlg).focus();
            })

            // limit input to number only
            // based on https://stackoverflow.com/questions/2632359/can-jquery-add-commas-while-user-typing-numbers
            $('.pledgeAmount', $callSheetResultDlg).on('input', function(event) {
                // skip for arrow keys
                if(event.which >= 37 && event.which <= 40) return;

                // format number
                $(this).val(function(index, value) {
                    return value
                        .replace(/\D/g, "")
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                });

                // see if there is a value see if the ok button should be enabled
                let v = $(this).val();
                pledgeSet = (v != '');

                // see if the status should be enable or disabled
                if (v != '') {
                    // there is a pledge
                    $('.statusBtn', $callSheetResultDlg).removeClass('selected')
                                                        .addClass('disabled');
                    callStatusEnabled = false;
                }
                else {
                    // there is a pledge
                    $('.statusBtn', $callSheetResultDlg).removeClass('disabled');
                    callStatusEnabled = true;
                    callStatusChanged = false;
                }

                enableDisableOk();
            });

            // see if the notes have changed
            $('.notesDiv .notes', $callSheetResultDlg).on('keypress keyup paste', function(e) {
                notesChanged = true;

                enableDisableOk();
            })

            $('.callTypeDiv .radioBtnGrpDiv .radioEntry .rBtn', $callSheetResultDlg).on('click', function(e) {
                let $rBtn = $(this);

                // see if disabled
                if ($rBtn.hasClass('disabled')) {
                    return;
                }

                // if selected then nothing to do
                if ($('.radioBtn', $rBtn).hasClass('icon-radio-button-on')) {
                    return;
                }

                // clear the buttons
                let rGrp = $('.radioBtn', $rBtn).attr('radioGrp');
                $('.radioBtn[radioGrp='+ rGrp + ']', $callSheetResultDlg).removeClass('icon-radio-button-on')
                                                                         .addClass('icon-radio-button-off');

                // set the button clicked
                $('.radioBtn', $rBtn).removeClass('icon-radio-button-off')
                                     .addClass('icon-radio-button-on');
            })

            function enableDisableOk()
            {
                let bEnableOk = false;

                // check pledges
                if (pledgeSet) {
                    bEnableOk = true;
                }

                // check if notes changed
                if (notesChanged) {
                    bEnableOk = true;
                }

                // see if the state changed
                if (callStatusEnabled && callStatusChanged) {
                    bEnableOk = true;
                }

                if (bEnableOk) {
                    $('.okBtn', $callSheetResultDlg).prop('disabled', false);
                }
                else {
                    $('.okBtn', $callSheetResultDlg).prop('disabled', true);
                }
            } // enableDisableOk

            function saveCallSheetResults() {
                // get the type of call
                let $callType = $('.callTypeDiv .radioEntry .rBtn .icon-radio-button-on', $callSheetResultDlg);
                let callType = $callType.attr('value');

                // get the pledge and call result
                let pledge = '';
                let callResult = '';
                if (pledgeSet || (callStatusEnabled && callStatusChanged)) {
                    pledge = $('.pledgeAmountDiv .pledgeAmount').val();
                    if (pledge != '') {
                        pledge = pledge.replace(/,/g, '');
                        callResult = 'PLD'
                    }
                    else {
                        callResult = $('.statusBtnDiv .statusBtn.selected').attr('value');
                    }
                }
                else {
                    callResult = 'UNK';
                }

                // build the payload
                let pLog = {};
                pLog.ct_contact = callTimeId;
                pLog.call_group = callSheetGrpId;
                pLog.log_type = callType;
                pLog.call_result = callResult;
                pLog.call_back = null;
                if (pledge != '') {
                    pLog.pledged = pledge
                }
                if (notesChanged) {
                    pLog.see_notes = 'true';
                }
                else {
                    pLog.see_notes = 'false';
                }

                // log the results
                let ajaxCalls = [];
                let url = templateData.callTimeLogApi.replace('CALLTIME-CONTACT-ID', callTimeId);
                ajaxCalls.push($.ajax({
                                    type: 'POST',
                                    url: url,
                                    data: JSON.stringify(pLog),
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                })
                            )

                // see if the notes changed
                if (notesChanged) {
                    let contactNotesUrl = templateData.callsheetNotesApi.replace("contactId", contactId)
                                                                        .replace('contactSetId', 'aroot');
                    let nPayload = {};
                    nPayload.notes2 = $('.notesDiv .notes', $callSheetResultDlg).html();
                    let notesId = $('.notesDiv .notes', $callSheetResultDlg).attr('notesId');
                    let aType = 'POST'
                    if (notesId) {
                        aType = 'PUT';
                        contactNotesUrl = contactNotesUrl + notesId + '/';
                    }
                    ajaxCalls.push($.ajax({
                                            type: aType,
                                            url: contactNotesUrl,
                                            data: JSON.stringify(nPayload),
                                            contentType: "application/json; charset=utf-8",
                                            dataType: "json",
                                        })
                                    );
                }

                // make the ajax call(s)
                $.when.apply(undefined, ajaxCalls)
                .done (function (logResults, notesResult) {
                    let logEntry;
                    if (Array.isArray(logResults)) {
                        logEntry = logResults[0];
                    }
                    else if (typeof logResults == 'object') {
                        logEntry = logResults;
                    }
                    if (Array.isArray(notesResult)) {}

                    // update the status and call results
                    logDisplay = buildCallSheetGrpDetailsLogStatus(logEntry);
                    logCallResults = buildCallSheetGrpDetailsLogCallResults(logEntry);

                    $('.statusDiv', $selEntry).html(logDisplay);
                    $('.logCallResultDiv', $selEntry).html(logCallResults);
                })
                .fail (function (r) {
                    revupUtils.apiError('Call Sheet Results', r, 'Create/Modify Call Sheet',
                                        "Unable create/update call sheet results and/or notes at this time");
                })
            } // saveCallSheetResults
        })

        $grpDetailsBtnCallSheet.on('click', function(e) {
            let $btn = $(this);

            // see if disabled
            if ($btn.hasClass('disabled')) {
                return;
            }

            let $selEntry = $('.entry.selected', $grpDetailsLst);
            let contactId = $selEntry.attr('contactId');
            let callTimeId = $selEntry.attr('callTimeId');
            let primaryPhone = $selEntry.attr('primaryPhone');
            let primaryEmail = $selEntry.attr('primaryEmail');

            function primaryPhoneUpdateFunc(newPhoneId)
            {
                $selEntry.attr('primaryPhone', newPhoneId);
            } // primaryPhoneUpdateFunc
            function primaryEmailUpdateFunc(newEmailId)
            {
                $selEntry.attr('primaryEmail', newEmailId);
            } // primaryEmailUpdateFunc
            displayCallSheet(contactId, callTimeId, primaryPhone, primaryPhoneUpdateFunc, primaryEmail, primaryEmailUpdateFunc)
        })
    } // detailsEvents

    // build the lists
    function buildIndividualCallSheets(lstData, initialLoad)
    {
        let sTmp = [];

        for (let i = 0; i < lstData.length; i++) {
            let d = lstData[i];

            let extraEntryClass = "";
            if(typeof(initialLoad) == 'boolean' && initialLoad == true){
                if (i == 0){
                    extraEntryClass = ' selected';
                }
            }
            if (i == nextPageMarkerIndex && (parseInt($individualLst.attr('currentpage')) < parseInt($individualLst.attr('maxPages')))) {
                sTmp.push('<div class="entry timeToAddMore' + extraEntryClass + '" callTimeContactId="' + d.id + '" contactId="' + d.contact.id + '">');
            }
            else{
                sTmp.push('<div class="entry' + extraEntryClass + '" callTimeContactId="' + d.id + '" contactId="' + d.contact.id + '">');
            }
                // select - colSelect
                sTmp.push('<div class="column colSelect">')
                    sTmp.push('<div class="icon icon-box-unchecked"></div>')
                sTmp.push('</div>');

                // slider
                sTmp.push('<div class="slider blank"></div>');

                // name - colIndividualName
                sTmp.push('<div class="column colIndividualName">')
                    sTmp.push('<div class="name" contactId="' + d.contact.id + '">' + d.contact.first_name + ' ' + d.contact.last_name + '</div>')
                sTmp.push('</div>');

                // slider
                sTmp.push('<div class="slider blank"></div>');

                // name - colMemberGrp
                let grps = [];
                if (d.call_groups) {
                    for (let j = 0; j < d.call_groups.length; j++) {
                        grps.push(d.call_groups[j].title)
                    }
                }
                sTmp.push('<div class="column colMemberGrp" title="' + grps.join('\n') + '">');
                    sTmp.push(grps.join(', '));
                sTmp.push('</div>');

                // slider
                sTmp.push('<div class="slider blank"></div>');

                // name - colDate
                sTmp.push('<div class="column colDate">')
                    sTmp.push(revupUtils.formatDate(d.created));
                sTmp.push('</div>');
            sTmp.push('</div>');
        }

        if (typeof(initialLoad) == 'boolean') {
            if(parseInt($individualLst.attr('currentpage')) < parseInt($individualLst.attr('maxPages'))) {
                sTmp.push('<div class="entry loadingMore" style="display:none">');
                    sTmp.push('<div class="loadingMore"></div>')
                sTmp.push('</div>');
            }
        }

        // update the attributes
        let maxPages = Math.ceil($individualLst.attr('numentries') / entriesPerPageIndividual);
        let entriesLoaded = $individualLst.attr('numentries') + lstData.length;
        $individualLst.attr('maxPages', maxPages);

        if (typeof(initialLoad) == 'boolean' && initialLoad == true) { // 1st load of the page
            $individualLst.attr('numEntriesLoaded', lstData.length);
            return sTmp.join('');
        }
        else if (typeof(initialLoad) == 'number') {
            sTmp.push('<div class="entry loadingMore" style="display:none">');
                sTmp.push('<div class="loadingMore"></div>')
            sTmp.push('</div>');
            $('.entry:last-of-type', $individualLst).after(sTmp.join(''));

            // update the footer count
            $grpFooter.hide();
            $individualFooter.show()
            $individualFooter.html('<text>Showing </text>' + '<text class="showing">' + parseInt($individualLst.attr('numEntriesLoaded')) +  '</text> '  + '<text> of </text>' + '<text class="total">' + $individualLst.attr('numEntries') + '</text>' + '<text> contacts</text>');
        }
        else if (typeof(initialLoad) == 'boolean' && initialLoad == false) { // fetching the entriesPerPageIndividual number of pages from scrolling
            $('.entry:last-of-type', $individualLst).after(sTmp.join(''));
            $individualLst.attr('numEntriesLoaded', parseInt($individualLst.attr('numEntriesLoaded')) + lstData.length);

            // update the footer count
            $grpFooter.hide();
            $individualFooter.show()
            $individualFooter.html('<text>Showing </text>' + '<text class="showing">' + parseInt($individualLst.attr('numEntriesLoaded')) +  '</text> '  + '<text> of </text>' + '<text class="total">' + $individualLst.attr('numEntries') + '</text>' + '<text> contacts</text>');
            }
    } // buildIndividualCallSheets

    // fetchNextChunk
    let fetchNextChunk = (bSingleEntry = false, callGrps = false) =>
    {
        // build the url
        let url = callGrps? templateData.callTimeSetsApi : templateData.callTimeContactsApi;


        if (typeof(bSingleEntry) == 'boolean') { // fetching for infinite scrolling
            let nextPage = callGrps? (parseInt($grpLst.attr('currentpage')) + 1) : (parseInt($individualLst.attr('currentPage')) + 1);
            url += '?page=' + nextPage;
            url += '&page_size=' + entriesPerPageIndividual;
        }

        else if (typeof(bSingleEntry) == 'number') { // from a deletion
            let nextPage = callGrps? (parseInt($grpLst.attr('currentpage')) * entriesPerPageGrp) : (parseInt($individualLst.attr('currentPage')) * entriesPerPageIndividual);
            url += '?page=' + nextPage;
            url += '&page_size=' + bSingleEntry;
        }

        $.ajax({
            url: encodeURI(url),
            type: 'GET'
        })
        .done(function(r) {
            let d = '';
            callGrps? (currentPage = parseInt($grpLst.attr('currentPage')) + 1) : (currentPage = parseInt($individualLst.attr('currentPage')) + 1);

            if (callGrps) { // if on the Call Sheet Groups tab
                if(typeof(bSingleEntry) == 'boolean') { // fetching for infinite scrolling
                    $('.entry.loadingMore', $grpLst).remove();
                    $grpLst.attr('currentPage', currentPage);
                    // $grpLst.attr('numEntriesLoaded', r.count);
                }
                d = buildCallSheetGrps(r.results, bSingleEntry);
            }

            else { // if on the Individual Call Sheets tab
                if(typeof(bSingleEntry) == 'boolean') { // fetching for infinite scrolling
                    $('.entry.loadingMore', $individualLst).remove()
                    $individualLst.attr('currentPage', currentPage);
                }
                d = buildIndividualCallSheets(r.results, bSingleEntry);
            }
        })
        .fail(function(r) {
            $individualDetailsLoading.hide();
            revupUtils.apiError('Call Time', r, 'Load Individual Call Sheet List',
                                "Unable to load list of individuals at this time");
        })
        .always(function() {
            // size columns
            let storageSection = callGrps? 'callTimeGroupColWidth-' : 'callTimeIndividualColWidth-';
            tmp = localStorage.getItem(storageSection + templateData.seatId);
            if (tmp) {
                tmp = JSON.parse(tmp);
                if ($('.lstsSection').outerWidth() == tmp.listWidth) {
                    let col = callGrps? templateData.grpCol : templateData.individualCol;
                    for (let i = 0; i < col.length; i++) {
                        $('.column.' + col[i].css).width(tmp[col[i].css] + 'px');
                    }

                }
            }
        });
    } // fetchNextChunk

    /*
     * build and fetch individual details
     */
    /*
     * Helper methods
     */
    function scoreFormat(rawValue)
    {
        if (!rawValue) {
            rawValue = 0;
        }

        // get the score, if the fraction part is 0 don't show it
        score = rawValue.toFixed(1);

        // remove the fraction if fraction value is 0
        var tmp = score.toString();
        tmp = tmp.split('.');
        if ((tmp.length == 2) && (Number(tmp[1]) == 0)) {
            score = Number(tmp[0]);
        }
        else {
            score = Number(score);
        }

        if (score == 0) {
            return("&nbsp;");
        }

        return score;
    }// scoreFormat

    function scoreColorClass(rawValue, formatedScore)
    {
        var politicalSpectrumClass = "";
        if (/*!rawValue ||*/ !formatedScore) {
            return politicalSpectrumClass;
        }
        var polSpectrum = rawValue.toFixed(1);
        if ((polSpectrum >= 0) && (formatedScore > 0)) {
            if (polSpectrum == 0) {
                politicalSpectrumClass = 'political-spectrum-bar-republican'
            }
            else if ((polSpectrum > 0) && (polSpectrum < 0.4)) {
                politicalSpectrumClass = 'political-spectrum-bar-republican-lite'
            }
            else if ((polSpectrum >= 0.4) && (polSpectrum <= 0.6)) {
                politicalSpectrumClass = 'political-spectrum-bar-neutral'
            }
            else if ((polSpectrum > 0.6) && (polSpectrum < 1)) {
                politicalSpectrumClass = 'political-spectrum-bar-democrat-lite'
            }
            else {
                politicalSpectrumClass = 'political-spectrum-bar-democrat'
            }
        }

        return politicalSpectrumClass;
    } // scoreColorClass

    const photoPriority = 'LinkedIn'
    const photoWidth = '60px';
    const photoHeight = '50px';

    // timer to hide the checkmarks for primary change
    let primaryPhoneChangeTimer = null;
    let primaryEmailChangeTimer = null;

    function buildIndividualDetails(r)
    {
        // clear the times for the primary changes
        if (primaryPhoneChangeTimer) {
            clearTimeout(primaryPhoneChangeTimer);
            primaryPhoneChangeTimer = null;
        }
        if (primaryEmailChangeTimer) {
            clearTimeout(primaryEmailChangeTimer);
            primaryEmailChangeTimer = null;
        }

        // build the contact
        let bPhoto = false;
        let sTmp = [];

        // see if photo
        if (r.contact && r.contact.image_urls && r.contact.image_urls.length > 0) {
            bPhoto = true;

            // get the profile url
            let profileUrl = undefined;
            let hasPhotoProfileClass = '';
            if (r.external_profiles && r.external_profiles.length > 0) {
                for (var i = 0; i < r.external_profiles.length; i++) {
                    if (profile[i].source == photoPriority) {
                        profileUrl = profile[i].url;
                    }
                }

                if (!profileUrl) {
                    profileUrl = profile[0].url
                }

                if (profileUrl) {
                    hasPhotoProfileClass = ' hasProfile';
                }
            }

            // get the photo url
            let photoUrl = undefined;
            for (var i = 0; i < r.contact.image_urls.length; i++) {
                if (r.contact.image_urls[i].source == photoPriority) {
                    photoUrl = r.contact.image_urls[i].url;
                }
            }

            if (!photoUrl) {
                photoUrl = r.contact.image_urls[0].url;
            }

            if (photoUrl) {
                sTmp.push('<div class="photoDiv' + hasPhotoProfileClass + '">');
                    var imgStyle = [];
                    imgStyle.push('width:' + photoWidth);
                    imgStyle.push('height:' + photoHeight);

                    if (imgStyle.length > 0) {
                        imgStyle += ' style="' + imgStyle.join(';') + '"';
                    }

                    if (profileUrl) {
                        sTmp.push('<a href="' + profileUrl + '" target="_blank">');
                    }

                    sTmp.push('<img class="detailImg" src="' + photoUrl + '"' + imgStyle + '>')

                    if (profileUrl) {
                        sTmp.push('</a>')
                    }
                sTmp.push('</div>');
            }
        }

        // name, email & phone
        let bMultiPhone = false;
        let bMultiEmail = false;
        let extraClass = '';
        if (bPhoto) {
            extraClass = ' hasPhoto';
        }
        sTmp.push('<div class="nameEmailDiv' + extraClass + '">');
            // name
            if (r.contact && r.contact.first_name)
                sTmp.push('<div class="name">' + r.contact.first_name + '</div>')
            if (r.contact && r.contact.last_name)
                sTmp.push('<div class="name">' + r.contact.last_name + '</div>')

            // email
            if (r.primary_email) {
                let emailAddr = '';
                let bSingle = true;
                if (r.contact.email_addresses[0].length == 1) {
                    emailAddr = ct.email_addresses[0].address;
                }
                else {
                    bSingle = false;
                    bMultiEmail = true;
                }
                sTmp.push('<div class="email" bSingle="' + bSingle + '" primaryEmail="' + r.primary_email + '">' + emailAddr + '</div>');

                $individualDetailsBtnSendMail.removeClass('disabled');
            }
            else if (r.contact && r.contact.email_addresses && r.contact.email_addresses.length > 0) {
                if (r.contact.email_addresses.length > 1) {
                    bMultiEmail = true;
                    sTmp.push('<div class="email" bSingle="false"></div>');
                }
                else {
                    sTmp.push('<div class="email" bSingle="true">' + r.contact.email_addresses[0].address + '</div>')
                }

                $individualDetailsBtnSendMail.removeClass('disabled');
            }
            else {
                $individualDetailsBtnSendMail.addClass('disabled');
            }
            if (bMultiEmail) {
                sTmp.push('<div class="primaryEmailChanged icon icon-check"></div>')
            }

            // phone
            if (r.primary_phone) {
                let phoneNum = '';
                if (r.contact.phone_numbers[0].length == 1) {
                    phoneNum = revupUtils.getFormattedPhone(r.contact.phone_numbers[0].number);
                }
                else {
                    bMultiPhone = true;
                }
                sTmp.push('<div class="phone" primaryPhone="' + r.primary_phone + '">' + phoneNum + '</div>')
            }
            else if (r.contact && r.contact.phone_numbers && r.contact.phone_numbers.length > 0) {
                if (r.contact.phone_numbers.length > 1) {
                    bMultiPhone = true;
                    sTmp.push('<div class="phone"></div>');
                }
                else {
                    sTmp.push('<div class="phone">' + revupUtils.getFormattedPhone(r.contact.phone_numbers[0].number) + '</div>')
                }
            }
            if (bMultiPhone) {
                sTmp.push('<div class="primaryPhoneChanged icon icon-check"></div>')
            }
        sTmp.push('</div>');

        // score section
        let settingsPoliticalColor = true;  // temp
        sTmp.push('<div class="scoreSectionDiv">');
            // score div
            extraClass = "";
            if (!settingsPoliticalColor) {
                extraClass = " hideColor";
            }

            // get the score
            let scoreNum = undefined;
            if (r.result && r.result.score)
                scoreNum = r.result.score;
            var formatedScore = scoreFormat(scoreNum);
            if (formatedScore !== "") {
                //if (displaySection.name.scoreColor) {
                if (r.result){
                    var scoreColor = r.result.key_signals.political_spectrum;
                    extraClass += ' ' + scoreColorClass(scoreColor, formatedScore);
                }
                var style = '';
                /*
                if ((displaySection.name.score.colorStyle) && (formatedScore > 0)) {
                    if ((currentSkin.colors) && currentSkin.colors['scoreHighlightColor']) {
                        // look for a background color rule
                        colorRules = currentSkin.colors['scoreHighlightColor'];
                        for (var cr = 0; cr < colorRules.length; cr++) {
                            if (colorRules[cr].colorType == "background") {
                                style = 'background:' + colorRules[cr].colorVal + ';';
                            }
                        }
                    }
                    else {
                        style += displaySection.name.score.colorStyle;
                    }
                }
                */
                sTmp.push('<div class="scoreDiv' + extraClass + '" style="' + style + '">');
                    sTmp.push('<div class="text">' + formatedScore + '</div>');
                sTmp.push('</div>');
            }
        sTmp.push('</div>');

        // load the contact
        $individualDetailsContactDiv.html(sTmp.join(''));

        // clear the primary change marker
        let $indPrimaryEmailChanged = $('.nameEmailDiv .primaryEmailChanged', $individualDetailsContactDiv);
        let $indPrimaryPhoneChanged = $('.nameEmailDiv .primaryPhoneChanged', $individualDetailsContactDiv);
        $indPrimaryEmailChanged.hide();
        $indPrimaryPhoneChanged.hide();

        // add list to the contact div
        if (bMultiPhone) {
            let phoneNum = [];
            let primaryPhoneId = $('.nameEmailDiv .phone', $individualDetailsContactDiv).attr('primaryPhone');
            let startIndex = 0;
            for (let i = 0; i < r.contact.phone_numbers.length; i++) {
                let o = new Object();
                o.value = r.contact.phone_numbers[i].id;
                o.displayValue = revupUtils.getFormattedPhone(r.contact.phone_numbers[i].number)
                phoneNum.push(o);

                if (primaryPhoneId && primaryPhoneId == r.contact.phone_numbers[i].id)
                    startIndex = i;
            }
            let $phone = $('.phone', $individualDetailsContactDiv);
            $phone.revupDropDownField({
                value: phoneNum,
                valueStartIndex: startIndex,
                bAutoDropDownPadding: true,
                changeFunc: function(index, val) {
                    let contactId = $individualDetailsHeader.attr('contactId');
                    let callTimeContactId = $individualDetailsHeader.attr('callTimeContactId')
                    let url = templateData.primaryPhoneApi + callTimeContactId + '/';

                    $.ajax({
                        url: url,
                        type: 'PUT',
                        data: JSON.stringify({
                            primary_phone: val,
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                    })
                    .done(function(r) {
                        $indPrimaryPhoneChanged.show();

                        // set the time to fade out
                        primaryPhoneChangeTimer = setTimeout(function() {
                            $indPrimaryPhoneChanged.fadeOut();

                            primaryPhoneChangeTimer = null;
                        }, 5000)
                    })
                    .fail(function(r) {
                        revupUtils.apiError('Call Time', r, 'Change Primary Phone Number',
                                            "Unable to change the primary phone number from the details panel at this time");
                    })
                    .always(function(r) {
                        bMovementHappening = false;
                    });
                }
            });
        }
        if (bMultiEmail) {
            let emailAddr = [];
            let primaryEmailId = $('.nameEmailDiv .email', $individualDetailsContactDiv).attr('primaryEmail');
            let startIndex = 0;
            for (let i = 0; i < r.contact.email_addresses.length; i++) {
                let o = new Object();
                o.value = r.contact.email_addresses[i].id;
                o.displayValue = r.contact.email_addresses[i].address;
                emailAddr.push(o);

                if (primaryEmailId && primaryEmailId == r.contact.email_addresses[i].id)
                    startIndex = i;
            }
            let $email = $('.email', $individualDetailsContactDiv);
            $email.revupDropDownField({
                value: emailAddr,
                valueStartIndex: startIndex,
                bAutoDropDownPadding: true,
                changeFunc: function(index, val) {
                    let contactId = $individualDetailsHeader.attr('contactId');
                    let callTimeContactId = $individualDetailsHeader.attr('callTimeContactId')
                    let url = templateData.primaryEmailApi + callTimeContactId + '/';

                    $.ajax({
                        url: url,
                        type: 'PUT',
                        data: JSON.stringify({
                            primary_email: val,
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                    })
                    .done(function(r) {
                        $indPrimaryEmailChanged.show();

                        // set the time to fade out
                        primaryEmailChangeTimer = setTimeout(function() {
                            $indPrimaryEmailChanged.fadeOut();

                            primaryEmailChangeTimer = null;
                        }, 5000)
                    })
                    .fail(function(r) {
                        revupUtils.apiError('Call Time', r, 'Change Primary Email Address',
                                            "Unable to change the primary email address from the details panel at this time");
                    })
                    .always(function(r) {
                        bMovementHappening = false;
                    });
                }
            });
        }

        // load the ask notes
        if (r.result && r.result.key_signals && r.result.key_signals.giving_availability) {
            $individualDetailsGivingDiv.show();
            let amt = revupUtils.commify(r.result.key_signals.giving_availability);
            $individualDetailsGivingAmt.text('$' + amt);
        }
        else {
            $individualDetailsGivingDiv.hide();
        }

        if (r.notes && r.notes.length > 0) {
            $individualDetailsAsk.html(r.notes[0].notes1)
                                 .attr('notesId', r.notes[0].id)
                                 .attr('notesChanged', false);
        }
        else {
            $individualDetailsAsk.html('')
                                 .removeAttr('notesId')
                                 .removeAttr('notesChanged');
        }
    } // buildIndividualDetails

    function buildCallSheetGrps(lstData, initialLoad, addNewGrp)
    {
        let sTmp = [];

        // should the unassign butten be enable?
        let bEnableUnassign = false;

        for (let i = 0; i < lstData.length; i++) {
            let d = lstData[i];
            let extraEntryClass = "";
            if(initialLoad == true){
                if (i == 0 && typeof(initialLoad) == 'boolean' || addNewGrp){
                    extraEntryClass = ' selected';
                }
            }

            // assigned to
            let assignedToDate = '--';
            let assignedToDateTitle = [];
            let assignedToDateAttr = '';
            let assignedTo = '--';
            let assignedToTitle = [];
            let assignedToAttr = '';
            let assignedToSeatId = [];
            if (d.shared_with && d.shared_with.length > 0) {
                // if first entry is shared then enable the unassign button
                if (i == 0)
                    bEnableUnassign = true;

                let sLst = d.shared_with;

                assignedToDate = '--';
                assignedToDateTitle = [];
                assignedTo = '--';
                assignedToTitle = [];
                assignedToSeatId = [];

                for (let j = 0; j < sLst.length; j++) {
                    let s = sLst[j];

                    // date - show first
                    if (assignedToDate === '--') {
                        assignedToDate = revupUtils.formatDate(s.shared_dt);
                    }

                    // date title tag
                    assignedToDateTitle.push(revupUtils.formatDate(s.shared_dt));

                    // name & title tag
                    if (assignedTo === '--')
                        assignedTo = [];
                    let name = s.seat.user.email;
                    if (s.seat.user.first_name != '' || s.seat.user.last_name != '') {
                        name = s.seat.user.first_name;
                        if (name != '' && s.seat.user.last_name != '')
                            name += ' ';
                        name += s.seat.user.last_name;
                    }
                    assignedTo.push(name);
                    assignedToTitle.push(name);

                    // seat id
                    assignedToSeatId.push(s.seat.id);
                }

                if (typeof assignedTo != 'string') {
                    assignedToAttr = assignedTo.join(',');
                    // assignedTo = assignedTo.join(', ');
                    // assignedToTitle = 'title="' + assignedToTitle.join(',&#013;') + '"';
                }
                assignedToDateAttr = assignedToDateTitle.join(',');
                // assignedToDateTitle = 'title="' + assignedToDateTitle.join(',&#013;') + '"';
                assignedToSeatId = 'seatId="' + assignedToSeatId.join(',') + '"';

                var tmp = [];
                for(let t = 0; t < assignedTo.length; t++) {
                    tmp.push(assignedToDateTitle[t] + '  ' + assignedTo[t]);
                }
                assignedToTitle = ' title="' + tmp.join(',&#013;') + '"';
                assignedTo = assignedTo.join(', ');
            }

            if (i == nextPageMarkerIndex && (parseInt($grpLst.attr('currentpage')) < parseInt($grpLst.attr('maxpages')))) {
                sTmp.push('<div class="entry timeToAddMore' + extraEntryClass + '" callTimeGrpId="' + d.id + '" '+ assignedToSeatId + '>');
            }
            else {
                sTmp.push('<div class="entry' + extraEntryClass + '" callTimeGrpId="' + d.id + '" '+ assignedToSeatId + '>');
            }
                // select - colGrpName
                sTmp.push('<div class="column colGrpName">')
                    sTmp.push('<div class="name">' + d.title + '</div>')
                sTmp.push('</div>');


                // slider
                sTmp.push('<div class="slider blank"></div>');

                // count - colCount
                sTmp.push('<div class="column colGrpCount">')
                    sTmp.push(d.count);
                sTmp.push('</div>');



                // slider
                sTmp.push('<div class="slider blank"></div>');

                // date - colDateCreated
                sTmp.push('<div class="column colDateCreated">')
                    sTmp.push(revupUtils.formatDate(d.created));
                sTmp.push('</div>');

                // slider
                sTmp.push('<div class="slider blank"></div>');

                // date - colDateAssigned
                sTmp.push('<div class="column colDateAssigned"' + assignedToTitle + ' assignedToDate="' + assignedToDateAttr + '">');
                    if (d.dateAssigned && d.dateAssigned != '')
                        sTmp.push(revupUtils.formatDate(d.dateAssigned));
                    else
                        sTmp.push(assignedToDate);
                sTmp.push('</div>');

                // slider
                sTmp.push('<div class="slider blank"></div>');

                // name - colAssignedTo
                sTmp.push('<div class="column colAssignedTo"' + assignedToTitle + 'assignedTo="' + assignedToAttr +'">');
                    if (d.assignedTo && d.assignedTo != '')
                        sTmp.push('<div class="name">' + d.assignedTo + '</div>');
                    else
                        sTmp.push(assignedTo)
                sTmp.push('</div>');
            sTmp.push('</div>');
        }

        if (typeof(initialLoad) == 'boolean') {
                if(parseInt($grpLst.attr('currentpage')) < parseInt($grpLst.attr('maxpages'))) {
                sTmp.push('<div class="entry loadingMore" style="display:none">');
                    sTmp.push('<div class="loadingMore"></div>')
                sTmp.push('</div>');
            }
        }

        // enable/disable the unassign button
        if (bEnableUnassign) {
            $btnGrpUnassign.removeClass('disabled');
        }
        else {
            $btnGrpUnassign.addClass('disabled');
        }

        if (typeof(initialLoad) == 'boolean' && initialLoad == true) { // initial load of page
            $grpLst.attr('numEntriesLoaded', lstData.length);
        }
        else if (addNewGrp) {
            $grpLst.attr('numEntriesLoaded', parseInt($grpLst.attr('numentriesloaded')) + lstData.length);
        }

        if (initialLoad == false) { // fetching the entriesPerPageIndividual number of pages from scrolling
            var addedNumEntriesLoaded = (parseInt($grpLst.attr('numEntriesLoaded')) + lstData.length);
            $grpLst.attr('numEntriesLoaded', addedNumEntriesLoaded);

        }

        // update the footer count
        $individualFooter.hide();
        $grpFooter.show()
        $grpFooter.html('<text>Showing </text>' + '<text class="showing">' + (parseInt($grpLst.attr('numentriesloaded'))) +  '</text> '  + '<text> of </text>' + '<text class="total">' + (parseInt($grpLst.attr('numentries'))) + '</text>' + '<text> groups</text>');

        if (typeof(initialLoad) == 'boolean' &&  initialLoad == true || addNewGrp) { // initial load of page or adding a new entry
            return sTmp.join('');
        }
        else if (typeof(initialLoad) == 'number') { // from a delete
            $('.entry.loadingMore', $grpWrapper).remove();
            sTmp.push('<div class="entry loadingMore" style="display:none">');
                sTmp.push('<div class="loadingMore"></div>')
            sTmp.push('</div>');
            $('.entry:last-of-type', $grpWrapper).after(sTmp.join(''));
            // $grpLst.attr('numEntriesLoaded', parseInt($grpLst.attr('numentriesloaded')) + 1);
        }
        else if (typeof(initialLoad) == 'boolean' && initialLoad == false) { // fetching the entriesPerPageIndividual number of pages from scrolling
            $grpWrapper.append(sTmp.join(''));
        }
    } // buildCallSheetGrps

    function buildCallSheetGrpDetailsLogStatus(logEntry)
    {
        let tmp = [];

        tmp.push('<div class="logEntryDiv whenDragging">');
            // icon
            switch (logEntry.log_type) {
                case 'CALL':
                    tmp.push('<div class="icon icon-called whenDragging"></div>');
                    break;
                case 'SMS':
                    tmp.push('<div class="icon icon-texted whenDragging"></div>');
                    break;
                case 'EMAIL':
                    tmp.push('<div class="icon icon-email whenDragging"></div>');
                    break;
            } // end switch - logEntry.log_type

            // date
            let d = logEntry.created;
            d = d.split('T');
            d = d[0].split('-')
            tmp.push('<div class="date whenDragging">' + d[1] + '/' + d[2] + '</div>');

            // add no answser
            if (logEntry.call_result == 'NO_ANSWER') {
                tmp.push('<div class="noAnswer whenDragging">No Answer</div>');
            }
        tmp.push('</div>');

        return tmp.join('');
    } //buildCallSheetGrpDetailsLogStatus

    function buildCallSheetGrpDetailsLogCallResults(logEntry)
    {
        // build the log results
        let bgStyle = '';
        let line1 = '';
        let line2 = '';
        let line1Style = 'large';
        let line2Style = 'large';
        switch (logEntry.call_result) {
            case 'PLEDGED':
                bgStyle = 'pledgedBg';
                line1 = 'Pledged';
                line1Style = 'small'
                line2 = '$' + revupUtils.commify(logEntry.pledged);

                break;
            /*
            case 'NO_ANSWER':
                bgStyle = 'noAnswerBg';
                line1 = 'No';
                line2 = 'Answer'
                break;
            */
            case 'VOICE_MESSAGE':
                bgStyle = 'voiceMsgBg';
                line1 = "Left";
                line2 = 'Message';
                break;
            case 'REFUSED':
                bgStyle = 'refusedBg';
                line1 = '';
                line1Style = 'blank';
                line2 = 'Refused';
                break;
            case 'BAD_NUMBER':
                bgStyle = 'badNumBg';
                line1 = 'Bad';
                line2 = 'Number';
                break;
        } // end switch - logEntry.call_result

        if (bgStyle == '' && logEntry.see_notes) {
            bgStyle = 'notesBg';
            line1 = 'See';
            line2 = 'Notes'
        }

        if (bgStyle != '') {
            let tmp = [];
            tmp.push('<div class="callResultDiv whenDragging ' + bgStyle + '">');
                tmp.push('<div class="line1 whenDragging ' + line1Style + '">' + line1 + '</div>');
                tmp.push('<div class="line2 whenDragging ' + line2Style + '">' + line2 + '</div>');
            tmp.push('</div>')

            return tmp.join('');
        }

        return '';
    } // buildCallSheetGrpDetailsLogCallResults

    function buildCallSheetGrpDetails(data, bAddScrollMarker = false, startAt = 12)
    {
        let sTmp = [];

        for (let i = 0; i < data.length; i++) {
            let extraClass = ""
            if (bAddScrollMarker && i == startAt) {
                extraClass = ' timeToAddMore'
            }

            let primaryEmail = ''
            if (data[i].primary_email) {
                primaryEmail = ' primaryEmail="' + data[i].primary_email + '"';
            }
            let primaryPhone = ''
            if (data[i].primary_phone) {
                primaryPhone = ' primaryPhone="' + data[i].primary_phone + '"';
            }

            // build the log entry
            let logDisplay = '';
            let logCallResults = '';
            if (data[i].logs && data[i].logs.length > 0) {
                // build based on the first entry (most recent)
                let logEntry = data[i].logs[0];
                logDisplay = buildCallSheetGrpDetailsLogStatus(logEntry);

                // build the log results
                logCallResults = buildCallSheetGrpDetailsLogCallResults(logEntry);

            }

            sTmp.push('<div class="entry' + extraClass + '" draggable="true" callTimeId="' + data[i].id + '" contactId="' + data[i].contact.id +'"' + primaryEmail + primaryPhone + '>');
                sTmp.push('<div class="movementDiv whenDragging">');
                    sTmp.push('<div class="moveBtn whenDragging moveUp icon icon-triangle-up"></div>');
                    sTmp.push('<div class="moveBtn whenDragging moveDown icon icon-triangle-dn"></div>');
                sTmp.push('</div>');

                sTmp.push('<div class="nameInfoDiv whenDragging">');
                    sTmp.push('<div class="nameDiv whenDragging">');
                        sTmp.push(data[i].contact.first_name + ' ' + data[i].contact.last_name);
                    sTmp.push('</div>');
                    sTmp.push('<div class="statusDiv whenDragging">');
                        sTmp.push(logDisplay);
                    sTmp.push('</div>');
                sTmp.push('</div>');

                sTmp.push('<div class="logCallResultDiv whenDragging">');
                    sTmp.push(logCallResults);
                sTmp.push('</div>')

                sTmp.push('<div class="deleteDiv whenDragging">');
                    sTmp.push('<div class="removeContactBtn whenDragging icon icon-delete-circle"></div>');
                sTmp.push('</div>');
            sTmp.push('</div>');
        }

        if (bAddScrollMarker) {
            sTmp.push('<div class="entry loadingMore" style="display:none">');
                sTmp.push('<div class="loadingMore"></div>')
            sTmp.push('</div>');
        }

        return sTmp.join('');
    } // buildCallSheetGrpDetails

    /*
     * Prospect Details
     */
    function buildProspectDetailsFrame()
    {
        var sTmp = [];

        sTmp.push('<div class="newProspectDetail">');
            sTmp.push('<div class="mainPageLoading" style="height:500px">');
                sTmp.push('<div class="loadingDiv">');
                    sTmp.push('<div class="loading"></div>');
                    sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                sTmp.push('</div>');
            sTmp.push('</div>');
            sTmp.push('<div class="mainPage" style="display:none">');
                sTmp.push('<div class="pageHeader">');
                sTmp.push('</div>');

                sTmp.push('<div class="partitionHeaderDiv">');
                    sTmp.push('<div class="innerDiv">');
                        sTmp.push('<div class="legendIconBtn" title="Legend">');
                            sTmp.push('<div class="icon icon-key"></div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');

                sTmp.push('<div class="givingSection">');
                    sTmp.push('<div class="givingLoading" style="height:450px;display:none">');
                        sTmp.push('<div class="loadingDiv">');
                            sTmp.push('<div class="loading"></div>');
                            sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="givingLoadedData">');
                        sTmp.push('<div class="leftSide">');
                            sTmp.push('<div class="givingTypeSectionOuter academicGiving">');
                                sTmp.push('<div class="givingTypeSection  opened">');
                                    sTmp.push('<div class="givingTypeSection opened">');
                                        sTmp.push('<div class="sectionHeader">');
                                            sTmp.push('<div class="openClose icon icon-triangle-dn"></div>');
                                            sTmp.push('<div class="headerText">Purdue Giving</div>');
                                            sTmp.push('<div class="headerRight">');
                                                sTmp.push('<div class="graphDiv candidates">');
                                                    sTmp.push('<div class="title">Candidates</div>');
                                                    sTmp.push('<div class="graph"></div>');
                                                sTmp.push('</div>');
                                                sTmp.push('<div class="graphDiv dollars">');
                                                    sTmp.push('<div class="title">Dollars</div>');
                                                    sTmp.push('<div class="graph"></div>');
                                                sTmp.push('</div>');
                                                sTmp.push('<div class="headerNoData">No Data</div>');
                                            sTmp.push('</div>');
                                        sTmp.push('</div>');
                                        sTmp.push('<div class="sectionBody">');
                                        sTmp.push('</div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');

                            sTmp.push('<div class="givingTypeSectionOuter academicNonProfit">');
                                sTmp.push('<div class="givingTypeSection opened">');
                                    sTmp.push('<div class="sectionHeader">');
                                        sTmp.push('<div class="openClose icon icon-triangle-dn"></div>');
                                        sTmp.push('<div class="headerText">Academic/Nonprofit</div>');
                                        sTmp.push('<div class="headerRight">');
                                            sTmp.push('<div class="headerNoData">No Data</div>');
                                        sTmp.push('</div>');
                                    sTmp.push('</div>');
                                    sTmp.push('<div class="sectionBody">');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');

                            sTmp.push('<div class="givingTypeSectionOuter stateCampaign">');
                                sTmp.push('<div class="givingTypeSection  opened">');
                                    sTmp.push('<div class="givingTypeSection opened">');
                                        sTmp.push('<div class="sectionHeader">');
                                            sTmp.push('<div class="openClose icon icon-triangle-dn"></div>');
                                            sTmp.push('<div class="headerText">State Campaigns</div>');
                                            sTmp.push('<div class="headerRight">');
                                                sTmp.push('<div class="graphDiv candidates">');
                                                    sTmp.push('<div class="title">Candidates</div>');
                                                    sTmp.push('<div class="graph"></div>');
                                                sTmp.push('</div>');
                                                sTmp.push('<div class="graphDiv dollars">');
                                                    sTmp.push('<div class="title">Dollars</div>');
                                                    sTmp.push('<div class="graph"></div>');
                                                sTmp.push('</div>');
                                                sTmp.push('<div class="headerNoData">No Data</div>');
                                            sTmp.push('</div>');
                                        sTmp.push('</div>');
                                        sTmp.push('<div class="sectionBody">');
                                        sTmp.push('</div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');

                            sTmp.push('<div class="givingTypeSectionOuter localCampaign">');
                                sTmp.push('<div class="givingTypeSection  opened">');
                                    sTmp.push('<div class="givingTypeSection opened">');
                                        sTmp.push('<div class="sectionHeader">');
                                            sTmp.push('<div class="openClose icon icon-triangle-dn"></div>');
                                            sTmp.push('<div class="headerText">Local Campaigns</div>');
                                            sTmp.push('<div class="headerRight">');
                                                sTmp.push('<div class="graphDiv candidates">');
                                                    sTmp.push('<div class="title">Candidates</div>');
                                                    sTmp.push('<div class="graph"></div>');
                                                sTmp.push('</div>');
                                                sTmp.push('<div class="graphDiv dollars">');
                                                    sTmp.push('<div class="title">Dollars</div>');
                                                    sTmp.push('<div class="graph"></div>');
                                                sTmp.push('</div>');
                                                sTmp.push('<div class="headerNoData">No Data</div>');
                                            sTmp.push('</div>');
                                        sTmp.push('</div>');
                                        sTmp.push('<div class="sectionBody">');
                                        sTmp.push('</div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');

                        sTmp.push('<div class="rightSide">');
                            sTmp.push('<div class="givingTypeSectionOuter federalCampaign">');
                                sTmp.push('<div class="givingTypeSection opened">');
                                    sTmp.push('<div class="sectionHeader">');
                                        sTmp.push('<div class="openClose icon icon-triangle-dn"></div>');
                                        sTmp.push('<div class="headerText">Federal Campaigns</div>');
                                        sTmp.push('<div class="headerRight">');
                                            sTmp.push('<div class="graphDiv candidates">');
                                                sTmp.push('<div class="title">Candidates</div>');
                                                sTmp.push('<div class="graph"></div>');
                                            sTmp.push('</div>');
                                            sTmp.push('<div class="graphDiv dollars">');
                                                sTmp.push('<div class="title">Dollars</div>');
                                                sTmp.push('<div class="graph"></div>');
                                            sTmp.push('</div>');
                                            sTmp.push('<div class="headerNoData">No Data</div>');
                                        sTmp.push('</div>');
                                    sTmp.push('</div>');
                                    sTmp.push('<div class="sectionBody">');
                                    sTmp.push('</div>');
                                    sTmp.push('<div class="sectionFooter">');
                                        sTmp.push('<div class="fecNotice">');
                                            sTmp.push('Notice: Federal law prohibits using contributor contact information that is obtained from FEC reports');
                                            sTmp.push('for the purpose of soliciting contributions or for any commercial purpose. However, analysis of data');
                                            sTmp.push('obtained from FEC reports is permitted. RevUp does not display individual contributor contact information');
                                            sTmp.push('obtained from reports filed with the FEC.');
                                        sTmp.push('</div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');

                            sTmp.push('<div class="givingTypeSectionOuter otherCampaign">');
                                sTmp.push('<div class="givingTypeSection  opened">');
                                    sTmp.push('<div class="givingTypeSection opened">');
                                        sTmp.push('<div class="sectionHeader">');
                                            sTmp.push('<div class="openClose icon icon-triangle-dn"></div>');
                                            sTmp.push('<div class="headerText">Local Campaigns</div>');
                                            sTmp.push('<div class="headerRight">');
                                                sTmp.push('<div class="graphDiv candidates">');
                                                    sTmp.push('<div class="title">Candidates</div>');
                                                    sTmp.push('<div class="graph"></div>');
                                                sTmp.push('</div>');
                                                sTmp.push('<div class="graphDiv dollars">');
                                                    sTmp.push('<div class="title">Dollars</div>');
                                                    sTmp.push('<div class="graph"></div>');
                                                sTmp.push('</div>');
                                                sTmp.push('<div class="headerNoData">No Data</div>');
                                            sTmp.push('</div>');
                                        sTmp.push('</div>');
                                        sTmp.push('<div class="sectionBody">');
                                        sTmp.push('</div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        return sTmp.join('');

    } // buildProspectDetailsFrame


    // fetch data
    function fetchContactDetails(callTimeContactId, contactId)
    {
        // set the loading spinner
        $individualDetails.show();
        $individualDetailsBtnSendMail.addClass('disabled');
        $individualDetailsBtnMoreDetails.addClass('disabled');
        $individualDetailsBtnCallSheet.addClass('disabled');
        $individualDetailsLoading.show();
        $individualDetailsStuff.hide();

        // get the data
        let url = templateData.callTimeContactsApi + callTimeContactId + '/';
        $.ajax({
            url: url,
            type: 'GET',
        })
        .done(function(r) {
            // save the contactId and callTimeContactId
            $individualDetailsHeader.attr('callTimeContactId', r.id)
                                    .attr('contactId', r.contact.id);
            if (r.primary_email) {
                $individualDetailsHeader.attr('primaryEmail', r.primary_email);
            }
            if (r.primary_phone) {
                $individualDetailsHeader.attr('primaryPhone', r.primary_phone);
            }

            // build and indivudual details
            buildIndividualDetails(r);

            $individualDetailsStuff.show();
        })
        .fail(function(r) {
            revupUtils.apiError('Call Time', r, 'Load Prospect Detail Panel',
                                "Unable to load prospect details data for the details panel up at this time");
        }).always(function(r){
            //$individualDetailsBtnSendMail.removeClass('disabled');
            $individualDetailsBtnCallSheet.removeClass('disabled');
            $individualDetailsBtnMoreDetails.removeClass('disabled');
            $individualDetailsLoading.hide();
        })

    } // fetchContactDetails

    function fetchIndividualCallSheetLst()
    {
        // build the url
        let url = templateData.callTimeContactsApi;
        url += '?page=1';
        url += '&page_size=' + entriesPerPageIndividual;

        // add search
        if (individualSearchVal != '') {
            url += '&search=' + individualSearchVal;
        }

        // display the spinner
        $individualLstHeader.show();
        $individualLst.hide();
        $lstLoading.show();

        $.ajax({
            url: encodeURI(url),
            type: 'GET'
        })
        .done(function(r) {
            let d = '';
            let selectedContactId = -1;
            let selectedCallTimeContactId = -1;
            if (r.count == 0) {
                let sTmp = [];
                sTmp.push('<div class="noResultsDiv">');
                    sTmp.push('<div class="text">No Individual Call Sheets to Manage</div>');
                sTmp.push('</div>');
                d = sTmp.join('');

                // update the attributes
                $individualLst.attr('currentPage', 0);
                $individualLst.attr('maxPages', 0);
                $individualLst.attr('numentries', 0);
                $individualLst.attr('numEntriesLoaded', 0);

            }
            else {
                // update the attributes
                let maxPages = Math.ceil(r.count / entriesPerPageIndividual);
                $individualLst.attr('numentries', r.count);
                $individualLst.attr('currentPage', 1);
                $individualLst.attr('maxPages', maxPages);

                d = buildIndividualCallSheets(r.results, true);

                // display the header
                $individualLstHeader.show();

                // set the selected contact to the first entry in the newly loaded list
                selectedContactId = r.results[0].contact.id;
                selectedCallTimeContactId = r.results[0].id;

                // update the footer count
                $grpFooter.hide();
                $individualFooter.show()
                $individualFooter.html('<text>Showing </text>' + '<text class="showing">' + parseInt($individualLst.attr('numEntriesLoaded')) +  '</text> '  + '<text> of </text>' + '<text class="total">' + $individualLst.attr('numEntries') + '</text>' + '<text> contacts</text>');
            }

            $individualLst.show()
            $indivWrapper.html(d);

            // get the details
            if (selectedContactId != -1) {
                fetchContactDetails(selectedCallTimeContactId, selectedContactId);
            }
            else {
                $individualDetailsLoading.hide();
            }
            $indivWrapper.scrollTop(0);
        })
        .fail(function(r) {
            $individualDetailsLoading.hide();
            revupUtils.apiError('Call Time', r, 'Load Individual Call Sheet List',
                                "Unable to load list of individuals at this time");
        })
        .always(function() {
            // hide the spinner
            $lstLoading.hide();

            // size columns
            tmp = localStorage.getItem('callTimeIndividualColWidth-' + templateData.seatId);
            if (tmp) {
                tmp = JSON.parse(tmp);
                if ($('.lstsSection').outerWidth() == tmp.listWidth) {
                    let col = templateData.individualCol;
                    for (let i = 0; i < col.length; i++) {
                        $('.column.' + col[i].css).width(tmp[col[i].css] + 'px');
                    }

                }
            }
        })
    } // fetchIndividualCallSheetLst

    function reloadCallSheetGrpDetails(entriesPerPage = 50)
    {
        //get the current scroll location
        let currentScrollLocation = $grpDetailsLst.scrollTop();

        // see what page current on
        let currentPage = $grpDetailsLst.attr('currentPage');
        currentPage = parseInt(currentPage, 10);

        // get the number to load
        let numToLoad = currentPage * entriesPerPage;

        // display the spinner
        $grpDetailsLoading.show();
        $grpDetailsLst.hide();

        let callTimeGrpId = $('.entry.selected', $grpLst).attr('callTimeGrpId');

        let url = templateData.addToCallTimeSetApi.replace('CALLTIME-SET-ID', callTimeGrpId);
        url += '?page=' + 1 + '&page_size=' + numToLoad;
        $.ajax({
            url: url,
            type: 'GET',
        })
        .done(function(r) {
            let d = '';
            let bThereIsNextPage = false;
            let bNextPage = false;
            /*
            if (r.count == 0) {
                // clear the details section
                $grpDetailsLoading.hide()

                $grpDetailsBtnCallSheet.addClass('disabled');
                $grpDetailsBtnCallResults.addClass('disabled');
            }
            else {
            */
                bNextPage = numToLoad < r.count;
                if (!bNextPage) {
                    if (r.count > entriesPerPage ) {
                        bThereIsNextPage = true;
                    }
                }
                else {
                    let currentPage = parseInt($grpDetailsLst.attr('currentPage'));
                    let maxPages = parseInt($grpDetailsLst.attr('maxPages'));
                    bThereIsNextPage = currentPage < maxPages;
                }
                let scrollAfter = Math.round(entriesPerPage * 0.75);
                scrollAfter += (currentPage - 1);
                d = buildCallSheetGrpDetails(r.results, bThereIsNextPage, scrollAfter);

                // display the footer values
                $grpDetailsLstFooter.show();
                $('.numLoaded', $grpDetailsLstFooter).text(numToLoad);
                $('.numTotal', $grpDetailsLstFooter).text(r.count)
            /*
            }
            */

            // add page info
            if (!bNextPage) {
                numPages = Math.ceil(r.count / entriesPerPage);

                $grpDetailsLst.html('')
                              .attr('currentPage', currentPage)
                              .attr('maxPages', numPages)
                              .scrollTop(0);

                $grpDetailsBtnCallSheet.addClass('disabled');
                $grpDetailsBtnCallResults.addClass('disabled');
            }

            $detailsSection.show();
            $grpDetailsLoading.hide();
            $grpDetailsLst.show()
            $('.entry.loadingMore', $grpDetailsLst).remove();
            $grpDetailsLst.html(d)
            $grpDetailsLst.scrollTop(currentScrollLocation);
        })
        .fail(function(r) {
            // clear the details section
            $grpDetailsLoading.hide();
            $grpDetailsLst.hide();

            revupUtils.apiError('Call Time', r, 'Load Call Sheet Group',
                                "Unable to load the Call Sheet Group at this time");
        })
        .always(function(r) {
            $detailsLoading.hide();
            $grpDetails.show();
        })
    } // reloadCallSheetGrpDetails


    function fetchCallTimeGrp(callTimeGrpId, bNextPage = false, entriesPerPage = 50)
    {
        $grpDetails.show();
        if (!bNextPage) {
            $grpDetailsLoading.show();
            $grpDetailsLst.hide();
        }

        let nextPage = 1;
        if (bNextPage) {
            nextPage = parseInt($grpDetailsLst.attr('currentPage')) + 1;
            $grpDetailsLst.attr('currentPage', nextPage);
        }

        let url = templateData.addToCallTimeSetApi.replace('CALLTIME-SET-ID', callTimeGrpId);
        url += '?page=' + nextPage + '&page_size=' + entriesPerPage;

        $.ajax({
            url: url,
            type: 'GET',
        })
        .done(function(r) {
            let d = '';
            let bThereIsNextPage = false;
            if (r.count == 0) {
                // clear the details section
                $grpDetailsLoading.hide()

                $grpDetailsBtnCallSheet.addClass('disabled');
                $grpDetailsBtnCallResults.addClass('disabled');

                //if there is no one in the Call Sheet Group, hide the footer count instead of showing xxx of xxx
                $grpDetailsLstFooter.hide();
                let sTmp = [];
                sTmp.push('<div class="noResultsDiv">');
                    sTmp.push('<div class="text">No Contacts in this Call Sheet Group</div>');
                sTmp.push('</div>');
                d = sTmp.join('');
                $grpDetailsLst.height(620).html(d);
            }
            else {
                if (!bNextPage) {
                    if (r.count > entriesPerPage ) {
                        bThereIsNextPage = true;
                    }
                }
                else {
                    let currentPage = parseInt($grpDetailsLst.attr('currentPage'));
                    let maxPages = parseInt($grpDetailsLst.attr('maxPages'));
                    bThereIsNextPage = currentPage < maxPages;
                }
                let scrollAfter = Math.round(entriesPerPage * 0.75)
                d = buildCallSheetGrpDetails(r.results, bThereIsNextPage, scrollAfter);

                // display the footer values
                $grpDetailsLstFooter.show();
                let numLoaded = r.results.length;
                if (bNextPage) {
                    numLoaded = parseInt($('.numLoaded', $grpDetailsLstFooter).text(), 10);
                    numLoaded += r.results.length;
                }
                else {
                    let h = 58;
                    h = h * r.results.length;
                    if (h < 620)
                        $grpDetailsLst.height(h);
                    else if (h > 620)
                        $grpDetailsLst.height(620);
                }

                $('.numLoaded', $grpDetailsLstFooter).text(numLoaded);
                $('.numTotal', $grpDetailsLstFooter).text(r.count)
            }

            // add page info
            if (!bNextPage) {
                numPages = Math.ceil(r.count / entriesPerPage);

                $grpDetailsLst.html('')
                              .attr('currentPage', 1)
                              .attr('maxPages', numPages)
                              .scrollTop(0);

                $grpDetailsBtnCallSheet.addClass('disabled');
                $grpDetailsBtnCallResults.addClass('disabled');
            }

            $detailsSection.show();
            if (!bNextPage)
                $grpDetailsLoading.hide();
            $grpDetailsLst.show()
            $('.entry.loadingMore', $grpDetailsLst).remove();
            $grpDetailsLst.append(d);
            if (!bNextPage) {
                $grpDetailsLst.scrollTop(0);
            }
        })
        .fail(function(r) {
            // clear the details section
            $grpDetailsLoading.hide();
            $grpDetailsLst.hide();

            revupUtils.apiError('Call Time', r, 'Load Call Sheet Group',
                                "Unable to load the Call Sheet Group at this time");
        })
        .always(function(r) {
            $detailsLoading.hide();
            $grpDetails.show();
        })
    } // fetchCallTimeGrp

    function fetchCallSheetGrpsLst(page)
    {
        // disable the header Buttons
        $btnGrpPopupMenu.addClass('disabled');
        $btnGrpPrint.addClass('disabled');
        $btnGrpAssign.addClass('disabled');
        $btnGrpUnassign.addClass('disabled');
        $btnGrpDelete.addClass('disabled');

        // build the url
        let url = templateData.callTimeSetsApi;
        url += '?page=1';
        url += '&page_size=' + entriesPerPageGrp;
        // add search
        if (grpSearchVal != '') {
            url += '&search=' + grpSearchVal;
        }

        // display the spinner
        //$individualLstHeader.hide();
        //$individualLst.hide();
        //$lstLoading.show();

        $.ajax({
            url: encodeURI(url),
            type: 'GET'
        })
        .done(function(r) {
            let d = '';
            if (r.count == 0) {
                $grpDetails.show();
                $grpDetailsLoading.hide();
                $grpDetailsLst.hide();
                $grpDetailsLstFooter.hide();

                $grpDetailsBtnCallSheet.addClass('disabled');
                $grpDetailsBtnCallResults.addClass('disabled');

                // enable the popup menu
                $btnGrpPopupMenu.removeClass('disabled');

                $grpFooter.hide();

                let sTmp = [];
                sTmp.push('<div class="noResultsDiv">');
                    sTmp.push('<div class="text">No Call Sheet Groups</div>');
                sTmp.push('</div>');
                d = sTmp.join('');

                // update the attributes
                $grpLst.attr('numentries', 0);
                $grpLst.attr('currentpage', 0);
                $grpLst.attr('maxpages', 0);
                $grpLst.attr('numentriesloaded', 0);
            }
            else {
                // update the attributes
                let maxPages = Math.ceil(r.count / entriesPerPageIndividual);
                $grpLst.attr('numentries', r.count);
                $grpLst.attr('currentPage', 1);
                $grpLst.attr('maxpages', maxPages);

                d = buildCallSheetGrps(r.results, true);

                // update the footer count
                $individualFooter.hide();
                $grpFooter.show()
                $grpFooter.html('<text>Showing </text>' + '<text class="showing">' + parseInt($grpLst.attr('numentriesloaded')) +  '</text> '  + '<text> of </text>' + '<text class="total">' + $grpLst.attr('numEntries') + '</text>' + '<text> groups</text>');

                // display the header
                $grpLstHeader.show();

                // fetch the contact info for the first entry
                let callTimeGrpId = r.results[0].id;
                fetchCallTimeGrp(callTimeGrpId);

                // enable the header buttons
                $btnGrpPopupMenu.removeClass('disabled');
                $btnGrpPrint.removeClass('disabled');
                $btnGrpAssign.removeClass('disabled');
                $btnGrpDelete.removeClass('disabled');
            }

            $grpLst.show();
            $grpWrapper.html(d);
            $grpWrapper.scrollTop(0);

        })
        .fail(function(r) {
            $grpDetailsLoading.hide();
            $grpDetailsLstHeader.hide();
            $grpFooter.hide();

            revupUtils.apiError('Call Time', r, 'Load Call Sheet Groups',
                                "Unable to load the list of Call Sheet Groups at this time");
        })
        .always(function() {
            // hide the spinner
            $lstLoading.hide();
            $detailsSection.hide();

            // size columns
            tmp = localStorage.getItem('callTimeGroupColWidth-' + templateData.seatId);
            if (tmp) {
                tmp = JSON.parse(tmp);
                if ($('.lstsSection').outerWidth() == tmp.listWidth) {
                    let col = templateData.grpCol;
                    for (let i = 0; i < col.length; i++) {
                        $('.column.' + col[i].css).width(tmp[col[i].css] + 'px');
                    }

                }
            }
        });
    } //fetchCallSheetGrpsLst

    return {
        display: function(pTemplateData, $containerDiv)
        {
            // hide the loading div
            $callTimeMgrLoading.hide();

            // display the list and details
            $callTimeMgrlstSection.show();
            $callTimeMgrLstLoading.show();

            // details
            $individualDetails.hide();
            $detailsDiv.show();


            // save the template data
            templateData = pTemplateData;

            // set the starting point
            let startingTab = templateData.startingTab;
            tmp = localStorage.getItem('callTimeStartingTab-' + templateData.seatId);
            if (tmp) {
                startingTab = tmp;
            }

            let newHeight = $(window).height() - ($('.lstsSection').offset().top) - $('.footerDiv').height();
            $individualLst.height(newHeight);
            $indivWrapper.height(newHeight - 1);

            $grpLst.height(newHeight);
            $grpWrapper.height(newHeight - 1);
            $grpDetailsLst.height(newHeight);

            if (startingTab === 'individual') {
                // set tab
                $lstTabIndividual.addClass('active');
                $lstTabGroup.removeClass('active');

                // display the correct button group
                $btnsGrp.hide();
                $btnsIndividual.show();

                // disable buttons
                $btnIndividualAddToGrp.addClass('disabled');
                $btnIndividualPrint.addClass('disabled');
                $btnIndividualDelete.addClass('disabled');

                // list
                $grpLstHeader.hide();
                $grpLst.hide();

                $individualLstHeader.show();
                $individualLst.show();

                // details
                $individualDetails.show();
                $individualDetailsLoading.show();
                $individualDetailsStuff.hide();
                $grpDetails.hide();
            }
            else if (startingTab === 'group') {
                // set tab
                $lstTabIndividual.removeClass('active');
                $lstTabGroup.addClass('active');

                // display the correct button group
                $btnsIndividual.hide();
                $btnsGrp.show();

                // disable buttons
                $btnGrpPopupMenu.addClass('disabled');
                $btnGrpPrint.addClass('disabled');
                $btnGrpAssign.addClass('disabled');
                $btnGrpUnassign.addClass('disabled');
                $btnGrpDelete.addClass('disabled');

                // list
                $individualLstHeader.hide();
                $individualLst.hide();
                $grpLstHeader.show();
                $grpLst.show();

                // details
                $individualDetails.hide();
                $individualDetailsLoading.hide();
                $individualDetailsStuff.hide();
                $grpDetails.show();
            }

            // add the search bars
            individualSearchVal = localStorage.getItem('callTimeMgrIndividualSearch-' + templateData.seatId);
            if (!individualSearchVal) {
                individualSearchVal = '';
            }
            grpSearchVal = localStorage.getItem('callTimeMgrGroupSearch-' + templateData.seatId);
            if (!grpSearchVal) {
                grpSearchVal = '';
            }

            // new search bar
            $individualSearchBar.revupSearchBox({
                placeholderText: 'Search for Contact',
                value: individualSearchVal,
                })
            $grpSearchBar.revupSearchBox({
                placeholderText: 'Search for Group Name',
                value: grpSearchVal,
                })


            // button and tab event handlers
            btnTabEventHandlers();

            // slider and window event handlers
            sliderListEvents();

            // details events
            detailsEvents();

            // fetch the data
            if (startingTab === 'individual') {
                $grpDetailsLoading.hide();
                fetchIndividualCallSheetLst();
            }
            else if (startingTab === 'group') {
                $individualDetailsLoading.hide();
                fetchCallSheetGrpsLst(1, true);
            }
        }
    }
 } ());
