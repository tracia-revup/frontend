    "use strict";
var entitySearch = (function (){
    /*
     *
     * List of variables needed
     *
     */
    // $btn : we might need to identify the source later for more development
    // bSimilar: whether we are retrieving the Candidates Similar to Me or To Opponents Popup
    // eData.createUrl : creating a new list of cadidate profile
    // eData.deleteUrl: deleting an existing list of candidate profile
    // eData.updateUrl: updating an existing list of candidate profile
    // eData.readUrl : loading up the entitySearch popup
    // fieldData.query_url : searching for candidates in the entitySearch popup
    // fieldData.query_arg : searching for candidates in the entitySearch popup

    // Globals
    var $detailsData        = $('.configAnalysis .configAnalysisSection .detailsSection .detailsData');
    var $detailsLoading     = $('.configAnalysis .configAnalysisSection .detailsSection .detailsLoading');
    var maxNumSearchResults = 25;       // max number of search results to display
    var searchedBefore      = 0;
    var $configWizardDiv    = $();
    let onboardMode;

    var timeout = null;

    function entitySearchDisplayPopup($btn, bSimilar, fieldData, eData, questionSetDataId)
    {
        var bError = false;  // error loading data

        // see if update or create new
        var bUpdate = true;
        if (questionSetDataId == undefined) {
            bUpdate = false;
            questionSetDataId = "";
        }

        var sTmp = [];
        sTmp.push('<div class="entitySearchPopup">');
            sTmp.push('<div class="dlgTitleBar">');
                if (bSimilar) {
                    sTmp.push('<div class="titleText">To Candidates Similar to Mine</div>');
                }
                else {
                    sTmp.push('<div class="titleText">To Opponent(s)')
                }
            sTmp.push('</div>');

            sTmp.push('<div class="dlgInner">');
                sTmp.push('<div class="innerMsg1">')
                    sTmp.push('A campaign similar to mine or correlated campaign, a PAC, or a political organization you consider similar to your own. We consider that a proxy/correlated candidate. For example, ');
                    sTmp.push('<ul">');
                            sTmp.push('<li>If your candidate is a South Asian male pick another existing or historical South Asian male candidate or elected official, who is of your same party.</li>');
                            sTmp.push('<li>If your candidate is a Caucasian woman in California, then pick another Caucasian woman in California of your same party. </li>');
                            sTmp.push('<li>If your candidate is a military veteran from Texas, then pick another military veteran from Texas of your same party.  </li>');
                    sTmp.push('</ul>');
                    sTmp.push(' Think of federal or statewide candidates, past candidates in the district, and candidates in other states who are cut from a similar cloth.');
                sTmp.push('</div>');

                sTmp.push('<div class="innerMsg2">')
                    sTmp.push('Steps: ');
                    sTmp.push('<ol">');
                            sTmp.push('<li>In the left-hand column, search for the name of the candidate you would like to add and hit "enter." Be aware our matches are based on complete words only. Pro-tip: Put quotes around their last name. Quotes might make it easier to find. </li>');
                            sTmp.push('<div class="stepsNote">Please note - you may see "duplicates" of the same candidate or committee. Don&apos;t worry! These are just different reports that have been filed. Select ALL that apply (most campaigns have multiple "names" of their committee).</div>');
                            sTmp.push('<li>Label the campaign or candidate with candidate or committee name.</li>');
                            sTmp.push('<li>We suggest starting with three correlated campaigns. You can always change and add more to them later.</li>');
                            sTmp.push('<li>Repeat the process for any opponents as needed.  </li>');
                    sTmp.push('</ol>');
                sTmp.push('</div>');

                sTmp.push('<div class="innerBody">');
                    sTmp.push('<div class="searchDiv">');
                        sTmp.push('<div class="header">');
                            sTmp.push('<div class="searchFor">');
                                sTmp.push('<div class="revupSearchBox">');
                                    sTmp.push('<div class="searchIcon icon icon-search"></div>');
                                    sTmp.push('<input class="searchText" style="width:calc(100% - 30px);" type="text" placeholder="Search" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" value="">');
                                        sTmp.push('<div class="clearIcon icon icon-close"></div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');

                        sTmp.push('<div class="searchForWait" style="display:none;">');
                            sTmp.push('<div class="loadingDiv">');
                                sTmp.push('<div class="loading"></div>');
                                sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                            sTmp.push('</div>');
                        sTmp.push('</div>');


                        sTmp.push('<div class="entryHeader" style="display: none;">');
                            sTmp.push('<div class="nameHeader">Name</div>');
                            sTmp.push('<div class="addBtnHeader">Add To List</div>');
                        sTmp.push('</div>');


                        sTmp.push('<div class="resultsWrap">');
                            sTmp.push('<div class="searchForResults">');
                                sTmp.push('<div class="searchForNoResults">No Results</div>');

                            sTmp.push('</div>');
                        sTmp.push('</div>');

                        sTmp.push('<div class="footerCount" style="display:none;">');
                            sTmp.push('<div class="showing"></div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="separatorDiv">');
                        sTmp.push('<div class="arrowPtRight"></div>');
                    sTmp.push('</div>');



                    sTmp.push('<div class="profileResults">');
                        sTmp.push('<div class="resultDiv">');
                            sTmp.push('<div class="header">');
                                sTmp.push('<div class="text">Candidate Profile</div>');
                            sTmp.push('</div>');

                            sTmp.push('<div class="innerResults">');
                                sTmp.push('<div class="candidateLabelDiv">');
                                    sTmp.push('<div class="text reqTxt">Candidate label:</div>');
                                    sTmp.push('<input class="candidateLabel" placeholder="Label">');
                                sTmp.push('</div>');

                                sTmp.push('<div class="text">Matches</div>');
                                sTmp.push('<div class="matchesDiv">');

                                    sTmp.push('<div class="lst">');
                                        if (bUpdate) {
                                            sTmp.push('<div class="loadingDiv">');
                                                sTmp.push('<div class="loading"></div>');
                                                sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                                            sTmp.push('</div>');
                                        }
                                        else {
                                            sTmp.push('<div class="noMatch">No Matches</div>');
                                        }
                                    sTmp.push('</div>');
                                sTmp.push('</div>');

                                sTmp.push('<div>');
                                    sTmp.push('<div class="text reqLabel reqTxt">Required Field</div>');
                                sTmp.push('</div>');

                            sTmp.push('</div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');



                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        // selectors
        var addBtn = '<div class="addBtn"><div class="text">Add</div></div>';
        var addCheckmark = '<div class="icon icon-check"></div>';
        var noSearchResults = '<div class="searchForNoResults">No Results</div>';

        var $dlg = $btn.revupDialogBox({
            okBtnText: 'Save',
            cancelBtnText: 'Cancel',
            width: '1025px',
            height: '740px',
            top: '20',
            msg: sTmp.join(''),
            enableOkBtn: false,
            extraClass: 'entitySearchDlg',
            autoCloseOk: false,
            okHandler: function(e) {
                // if an error do not save
                if (bError) {
                    return;
                }

                // build the json object
                var jObj = {};
                // ally or opponent
                jObj.is_ally = bSimilar;

                // label
                var label = $candidateLabel.val();
                jObj.label = label;

                // matches
                jObj.entities = [];
                $('.entry', $matchesLst).each(function() {
                    var $entry = $(this);

                    var eid = $entry.attr('eId');
                    var recordSetConfig = $entry.attr('recordSetConfig');

                    var eObj = new Object();
                    eObj.eid = eid;
                    eObj.record_set_config = recordSetConfig;
                    jObj.entities.push(eObj);
                })

                // build the
                var url = "";
                var ajaxType = "POST";
                if (bUpdate) {
                    url = eData.updateUrl.replace("questionSetDataId", questionSetDataId);
                    ajaxType = "PUT";
                }
                else {
                    url = eData.createUrl.replace("/questionSetDataId", "");
                    ajaxType = "POST";
                }

                $.ajax({
                    type: ajaxType,
                    url: url,
                    data: JSON.stringify(jObj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                })
                .done(function(r) {
                    // update the alert status
                    // do not run analysis if in onboarding
                    if (!onboardMode) {
                        updateRevupAlert();
                    }

                    // display load & hide data
                    $detailsLoading.show();
                    $detailsData.hide();

                    // update the display
                    $detailsData.html("");
                    loadDetailsEntitySearch(eData, fieldData);

                    // Close
                    $dlg.trigger('revup.dialogClose');
                })
                .fail(function(r) {
                    revupUtils.apiError('Analysis Configuration', r, 'Unable to update analysis config', "Unable to update analysis configuration at this time.");
                })
                .always(function(r) {
                })
            }
        });

        // if update load the data
        if (bUpdate) {
            var url = eData.readDetailUrl;
            if (questionSetDataId) {
                var url = eData.readDetailUrl.replace("questionSetDataId", questionSetDataId);
            }

            $.get(url)
                .done(function(r) {
                    var data = r.data;

                    // set the label
                    $candidateLabel.val(data.label);

                    // add the matches/entries
                    var entities = data.entities;
                    var sTmp = [];
                    for (var i = 0; i < entities.length; i++) {
                        sTmp.push('<div class="entry" eId="' + entities[i].eid + '" recordSetConfig="' + entities[i].record_set_config + '">');
                            sTmp.push('<div class="name">' + entities[i].name + '</div>');
                            sTmp.push('<div class="deleteBtn">');
                                sTmp.push('<div class="icon icon-delete-circle"></div>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');
                    }
                    $matchesLst.html(sTmp.join(''));
                })
                .fail(function(r) {
                    // set error
                    bError = true;
                    $candidateLabel.prop('disabled', true);

                    var sTmp = [];
                    sTmp.push('<div class="errorDiv">');
                        sTmp.push('<div class="msg">Unable to retrieve Key Contributors.</div>')
                        sTmp.push('<div class="msg">Please reload popup and try again.</div>');
                    sTmp.push('</div>');
                    $matchesLst.html(sTmp.join(''));

                    revupUtils.apiError('Analysis Configuration', r, 'Unable to load key contribution');
                });
        }

        // selectors
        var $searchForResults = $('.searchForResults', $dlg);
        var $searchForWait = $('.searchForWait', $dlg);
        var $matchesLst = $('.matchesDiv .lst', $dlg);
        var $okBtn = $('.okBtn', $dlg);
        var $candidateLabel = $('.innerResults .candidateLabelDiv .candidateLabel', $dlg);
        var $pagination = $('.searchForPagination', $dlg);

        // enable/disable save btn
        function enableDisableSaveBtn()
        {
            // both the label and the matches list values
            var labelValLen = $candidateLabel.val().length;
            var numMatches = $('.entry', $matchesLst).length

            if ((labelValLen == 0) || (numMatches == 0)) {
                $okBtn.prop('disabled', true);
            }
            else {
                $okBtn.prop('disabled', false);
            }
        } // enableDisableSaveBtn

        function formatSearchResult(result)
        {
            var sTmp = [];

            sTmp.push('<div class="entryDetail">');
                var party = result.party;
                if ((party == undefined) || (party == "")) {
                    party = '';
                }
                var partyColor = "";
                if (party.startsWith('rep', true)) {
                    partyColor = " political-spectrum-bar-republican";
                }
                else if (party.startsWith('dem', true)) {
                    partyColor = " political-spectrum-bar-democrat";
                }
                else if (party != '') {
                    partyColor = " political-spectrum-bar-neutral";
                }

                sTmp.push('<div class="partySection">');
                    sTmp.push('<div class="partyColorBlob ' + partyColor + '"></div>');
                sTmp.push('</div>');

                sTmp.push('<div class="detailSection">');
                    sTmp.push('<div class="boldLine">');
                        // name
                        sTmp.push('<div class="name">' + result.name + '</div>');

                        // position
                        var office = result.office;
                        if ((office == undefined) || (office == "")) {
                            office = '';
                        }
                        sTmp.push('<div class="position">' + office + '</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="normalLine">');
                        // party
                        sTmp.push('<div class="party">' + party + '</div>');

                        // district
                        var district = result.district;
                        if ((district == undefined) || (district == "")) {
                            district = '-';
                        }
                        else {
                            district = 'District: ' + district;
                        }
                        sTmp.push('<div class="district">' + district + '</div>');

                        // city
                        var city = result.city;
                        if ((city == undefined) || (city == "")) {
                            city = '-';
                        }
                        sTmp.push('<div class="city">' + city + '</div>');

                        // state
                        var state = result.state;
                        if ((state == undefined) || (state == "")) {
                            state = '-';
                        }
                        sTmp.push('<div class="state">' + state + '</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');

                sTmp.push('<div class="addBtnSection">');
                    var addImg = addBtn;
                    $('.entry', $matchesLst).each(function() {
                        var $entry = $(this);
                        var matchEId = $entry.attr('eId');
                        var matchRecSetConfig = $entry.attr('recordSetConfig');

                        if ((matchEId == result.eid) && (matchRecSetConfig == result.record_set_config)) {
                            addImg = addCheckmark
                        }
                    })
                        sTmp.push(addImg);
                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            return sTmp.join('');
        }; // formatSearchResult

        // events - search
        $('.revupSearchBox .searchText', $dlg).keyup(function() {
            var $currentSearchLength = $('.revupSearchBox .searchText', $dlg).val().length;
            if ($currentSearchLength > 2 || $currentSearchLength <= 2 && searchedBefore > 0) {
                // display waiting
                $searchForResults.hide();
                $searchForWait.show();

                // setting a count-down on each keyup that has more than 2 chars in the first search, and any number of chars after that
                // if there is already a count-down running, clear it and set a new one for the search to execute after 3 seconds
                if (timeout !== null) {
                    clearTimeout(timeout);
                }
                timeout = setTimeout(function () {
                        // if an error do not save
                        if (bError) {
                            return;
                        }
                        var url =  fieldData.query_url + '?' + fieldData.query_arg + '=' + $('.revupSearchBox .searchText', $dlg).val();

                        $.get(url)
                            .done(function(r) {
                                var results = r.results;

                                function displaySearchResults(startingAtPage, bAddNextMarker)
                                {
                                    var num = r.count;
                                    var startAt = (startingAtPage - 1) * maxNumSearchResults;
                                    var num = num - startAt;
                                    num = Math.min(num, maxNumSearchResults);
                                    var endAt = startAt + num;
                                    var nextPageMarkerIndex = Math.round(num * .8) + startAt;

                                    $('.entry.loadingMore', $searchForResults).remove();


                                    $('.resultsWrap .header').css('display', 'block');

                                    // header
                                    var sTmp = [];
                                    if (startingAtPage == 1) {
                                        var nextPageMarkerIndex = Math.round(num * .8) + 0;

                                    }


                                    for (var i = startAt; i < endAt; i++) {
                                        if (i == nextPageMarkerIndex && bAddNextMarker) {
                                            sTmp.push('<div class="entry timeToAddMore" eId="' + results[i].eid + '" recordSetConfig="' + results[i].record_set_config + '">');
                                                sTmp.push(formatSearchResult(results[i]));
                                            sTmp.push('</div>');
                                        }
                                        else {
                                            sTmp.push('<div class="entry" eId="' + results[i].eid + '" recordSetConfig="' + results[i].record_set_config + '">');
                                                sTmp.push(formatSearchResult(results[i]));
                                            sTmp.push('</div>');
                                        }
                                    }

                                        //     // add the loading entry to the end
                                    if (bAddNextMarker) {
                                        sTmp.push('<div class="entry loadingMore" style="display:none">');
                                            sTmp.push('<div class="loadingMore"></div>')
                                        sTmp.push('</div>');
                                    }

                                    if (startingAtPage > 1) { // append to the bottom on infinite scroll

                                        $('.entry:last-child', $searchForResults).after(sTmp.join(''));
                                    }
                                    else { // join on the first load of the list
                                        $searchForResults.html(sTmp.join(''));
                                    }

                                    $('.searchDiv .entryHeader').show();

                                    $resultsWrap.attr('numEntriesLoaded', endAt);
                                    $resultsWrap.attr('currentPage', startingAtPage);

                                    // update the footer count
                                    $('.footerCount .showing').html('showing ' + $resultsWrap.attr('numEntriesLoaded') + ' of ' + $resultsWrap.attr('numentries') + ' results');
                                    $('.searchDiv .footerCount').show();

                                    // add ellipsis to clipped columns
                                    $('.entry .nameCol', $searchForResults).fieldEllipsis();
                                    $('.entry .partyCol', $searchForResults).fieldEllipsis();
                                    $('.entry .officeCol', $searchForResults).fieldEllipsis();
                                } // displaySearchResults

                                $('.entitySearchDlg .resultsWrap').off('scroll').on('scroll', function(e) {
                                $resultsWrap = $('.entitySearchDlg .resultsWrap');
                                let $timeToAddMore = $('.entry.timeToAddMore', $searchForResults);
                                    if ($timeToAddMore.length > 0 && $timeToAddMore.visible($resultsWrap)) {
                                        $timeToAddMore.removeClass('timeToAddMore');
                                        $('.entry.loadingMore', $searchForResults).css('display', 'block');
                                        var nextPage = parseInt($resultsWrap.attr('currentPage'), 10) + 1;

                                         // if the number of results > number to be shown on each page, we will need a there will be a next page coming up
                                        if (r.count > (maxNumSearchResults * nextPage)) {
                                            displaySearchResults(nextPage, true);
                                        }
                                        else {
                                            displaySearchResults(nextPage);
                                        }
                                    }
                                })

                                // first load of the results (not from infinite scrolling)
                                // see if there are results
                                var $resultsWrap = $('.entitySearchDlg .resultsWrap');
                                if (r.count == 0) {
                                    $searchForResults.html(noSearchResults);

                                    // update the counts
                                    $resultsWrap.attr('numEntries', 0);
                                    $resultsWrap.attr('numEntriesLoaded', 0);
                                    $resultsWrap.attr('currentPage', 0);
                                    $resultsWrap.attr('maxPages', 0);

                                    // hide the footer count
                                    $('.searchDiv .footerCount').hide();
                                    $('.searchDiv .entryHeader').hide();

                                    return;
                                }
                                else {
                                    var numPages = Math.ceil(r.count / maxNumSearchResults);

                                    // update the counts
                                    $resultsWrap.attr('numEntries', r.count);
                                    $resultsWrap.attr('maxPages', numPages);
                                    $resultsWrap.attr('currentpage', 1);

                                    if (r.count > maxNumSearchResults) {
                                        displaySearchResults(1, true);
                                    }
                                    else {
                                        displaySearchResults(1);
                                    }
                                }
                            })
                            .fail(function(r) {
                                var sTmp = [];
                                sTmp.push('<div class="errorDiv">');
                                    sTmp.push('<div class="msg">Unable to search at this time</div>')
                                    sTmp.push('<div class="msg">Please try again later</div>');
                                sTmp.push('</div>');
                                $searchForResults.html(sTmp.join(''));

                                revupUtils.apiError('Analysis Configuration', r, 'Unable to search for an entity');
                            })
                            .always(function(r) {
                                // show results
                                $searchForWait.hide();
                                $searchForResults.show();

                                searchedBefore++;
                            })
                }, 3000);
            }
        })


        $('.revupSearchBox .clearIcon', $dlg)
            .on('click', function(e) {
                $('.revupSearchBox .searchText', $dlg).val('');
                $searchForResults.html(noSearchResults);
                $('.searchDiv .entryHeader').hide();

                var $resultsWrap = $('.entitySearchDlg .resultsWrap');

                // update the counts
                $resultsWrap.attr('numEntries', 0);
                $resultsWrap.attr('numEntriesLoaded', 0);
                $resultsWrap.attr('currentpage', 0);
                $resultsWrap.attr('maxPages', 0);

                // hide the footer count
                $('.searchDiv .footerCount').hide();
            });

        // event handler - add and entry
        $searchForResults.on('click', '.entry .addBtn', function(e) {
            // if an error do not save
            if (bError) {
                return;
            }

            var $addBtn = $(this);
            var $entry = $addBtn.closest('.entry');

            // get the data
            var eId = $entry.attr('eId');
            var recordSetConfig = $entry.attr('recordSetConfig')
            var name = $('.boldLine .name', $entry).text();

            // build the matchs list entry
            var sTmp=[];
            sTmp.push('<div class="entry" eId="' + eId + '" recordSetConfig="' + recordSetConfig + '">');
                sTmp.push('<div class="name">' + name + '</div>');
                sTmp.push('<div class="deleteBtn">');
                    sTmp.push('<div class="icon icon-delete-circle"></div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            // see if need to replace or append
            if ($('.noMatch', $matchesLst).length > 0) {
                $matchesLst.html(sTmp.join(''));
            }
            else {
                $matchesLst.append(sTmp.join(''));
            }

            // add the ellipsis if needed
            $('.entry .name', $matchesLst).fieldEllipsis();

            // change the add button to a check
            var $btnCol = $addBtn.closest('.addBtnSection')
            $btnCol.html(addCheckmark);

            // enable/disable save btn
            enableDisableSaveBtn();
        })

        // event handler - delete a entry in the match list
        $matchesLst.on('click', '.entry .deleteBtn', function(e) {
            // if an error do not save
            if (bError) {
                return;
            }

            var $btn = $(this);
            var $entry = $btn.closest('.entry');
            var eId = $entry.attr('eId');

            // clear the entry
            $entry.remove();

            // see if the no matches needs to be displayed
            if ($('.entry', $matchesLst).length == 0) {
                $matchesLst.html('<div class="noMatch">No Matches</div>');
            }

            // walk the results list and see if a check mark needs to be change to an add button
            $('.entry', $searchForResults).each(function() {
                var $entry = $(this);
                var searchEId = $entry.attr('eId');

                if (searchEId == eId) {
                    var $addBtnSection =  $('.addBtnSection', $entry);
                    $addBtnSection.html(addBtn);
                }
            })

            // enable/disable save btn
            enableDisableSaveBtn();
        })

        // event hander - see if lable changed
        $candidateLabel.on('change, keyup', function() {
            // enable/disable save btn
            enableDisableSaveBtn();
        })
    } // entitySearchDisplayPopup

    function loadDetailsEntitySearch(eData, fieldData)
    {
        // get data
        var sTmp = [];
        var url = eData.readUrl;

        // get the data
        $.get(url)
            .done(function(r) {
                // build a list of similar and opponents
                var results = r.results;
                var similar = [];
                var opponents = [];
                for (var i = 0; i < results.length; i++) {
                    if (results[i].data.is_ally) {
                        similar.push(results[i]);
                    }
                    else {
                        opponents.push(results[i]);
                    }
                }

                // get the list of
                sTmp.push('<div class="detailsDataInner detailsEntitySearch">');
                    sTmp.push('<div class="grouping similar">');
                        sTmp.push('<div class="header">');
                            sTmp.push('<div class="subTitle">Candidates similar to mine, local, state or federal.</div>');
                            sTmp.push('<div class="addBtn entitySearch">');
                                sTmp.push('<div class="icon icon-add-filter"></div>');
                                sTmp.push('<div class="btnText">Add'  + '</div>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');

                        sTmp.push('<div class="lst">');
                            for (var i = 0; i < similar.length; i++) {
                                sTmp.push('<div class="entry" questionSetDataId="' + similar[i].id + '" questionSetId="' + similar[i].question_set + '">');
                                    sTmp.push('<div class="title">' + similar[i].data.label + '</div>')
                                    sTmp.push('<div class="deleteBtn">');
                                        sTmp.push('<div class="icon icon-delete-circle"></div>');
                                    sTmp.push('</div>')
                                sTmp.push('</div>');
                            }
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="similarToMineTitle">');
                        sTmp.push('<h1>My Opponent</h1>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="grouping opponents">');
                        sTmp.push('<div class="header">');
                            sTmp.push('<div class="subTitle">To candidates opponent(s).</div>');
                            sTmp.push('<div class="addBtn entitySearch">');
                                sTmp.push('<div class="icon icon-add-filter"></div>');
                                sTmp.push('<div class="btnText">Add'  + '</div>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');

                        sTmp.push('<div class="lst">');
                            for (var i = 0; i < opponents.length; i++) {
                                sTmp.push('<div class="entry" questionSetDataId="' + opponents[i].id + '" questionSetId="' + opponents[i].question_set + '">');
                                    sTmp.push('<div class="title">' + opponents[i].data.label + '</div>')
                                    sTmp.push('<div class="deleteBtn">');
                                        sTmp.push('<div class="icon icon-delete-circle"></div>');
                                    sTmp.push('</div>')
                                sTmp.push('</div>');
                            }
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');

                // display
                $('.detailsData').html("");
                var $entitySearch = $('.detailsData').append(sTmp.join(''));

                // add the event handlers
                $($entitySearch)
                    .off('click', '.addBtn.entitySearch')
                    .on('click', '.addBtn.entitySearch', function(e) {
                        var $btn = $(this);
                        var $grouping = $btn.closest('.grouping');
                        var bSimilar = true;
                        if ($grouping.hasClass('opponents')) {
                            bSimilar = false;
                        }

                        // display the Popup
                        entitySearchDisplayPopup($btn, bSimilar, fieldData, eData);
                    })
                    .off('click', '.entry .title')
                    .on('click', '.entry .title', function(e) {
                        var $title = $(this);
                        var $entry = $title.closest('.entry');
                        var questionSetDataId = $entry.attr('questionSetDataId');
                        var $grouping = $title.closest('.grouping')
                        var bSimilar = true;
                        if ($grouping.hasClass('opponents')) {
                            bSimilar = false;
                        }

                        // display the Popup
                        entitySearchDisplayPopup($title, bSimilar, fieldData, eData, questionSetDataId);
                    })
                    .off('click', '.entry .deleteBtn')
                    .on('click', '.entry .deleteBtn', function(e) {
                        var $btn = $(this);
                        var $entry = $btn.closest('.entry');
                        var questionSetDataId = $entry.attr('questionSetDataId');
                        var title = $('.title', $entry).text();

                        // confim before the delete
                        $btn.revupConfirmBox({
                            headerText: "Delete Key Contribution",
                            msg: "Are you sure you want to delete key contribution \"" + title + "\"",
                            okHandler: function() {
                                $.ajax({
                                    url: eData.deleteUrl.replace("questionSetDataId", questionSetDataId),
                                    type: "DELETE"
                                })
                                .done(function(r) {
                                    $entry.remove();

                                    // do not run analysis if in onboarding
                                    if (!onboardMode) {
                                        updateRevupAlert();
                                    }
                                })
                                .fail(function(r) {
                                    $entry.revupMessageBox({
                                        headerText: "Error - Unable to delete Key Contributuin",
                                        msg: "Unable to delete key contribution \"" + title + "\".  Please try again later",
                                    })
                                })
                            }
                        })
                    })
            })
            .fail(function(r) {
                var sTmp = [];

                sTmp.push('<div class="detailsErrorSection">');
                    sTmp.push('<div class="errorMsg">Unable to load entity search data</div>');
                    sTmp.push('<div class="errorMsg">Please try back later by reloading this page</div>');
                sTmp.push('</div>');
                $detailsData.html(sTmp.join(''));

                revupUtils.apiError('Analysis Configuration', r, 'Unable to load entity search data');
            })
            .always(function(r) {
                // hide waiting and display data
                $detailsData.show();
                $detailsLoading.hide();

                if (onboardMode) {
                    $configWizardDiv.revupWizard('finishGetData');
                }

                $('.wizardBtnDiv .nextBtn').prop('disabled', false);
            });
    }; // loadDetailsEntitySearch

    return {
        load: function($btn, bSimilar, fieldData, eData, questionSetDataId){
            // display the Popup
            entitySearchDisplayPopup($btn, bSimilar, fieldData, eData, questionSetDataId);
        },

        display: function(eData, f, $configWizard, onboardTemplateData){
            if (onboardTemplateData) {
                if (onboardTemplateData['onboard']) {
                    onboardMode = true;
                }
            }
            $configWizardDiv = $configWizard;
            loadDetailsEntitySearch(eData, f);
        }
    };
} ());
