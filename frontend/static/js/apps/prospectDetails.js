var prospectDetails = (function () {
    /*
     *  Globals
     */
    // selectors
    var $mainPageLoading        = $('.newProspectDetail .mainPageLoading');
    var $mainPage               = $('.newProspectDetail .mainPage');
    var $header                 = $('.pageHeader', $mainPage);
    var $partition              = $('.partitionHeaderDiv .partitionDiv', $mainPage);
    var $givingLoading          = $('.givingSection .givingLoading', $mainPage);
    var $givingData             = $('.givingSection .givingLoadedData', $mainPage);
    var $givingSectionLocal     = $('.localCampaign', $givingData);
    var $givingSectionState     = $('.stateCampaign', $givingData);
    var $givingSectionFederal   = $('.federalCampaign', $givingData);
    var $givingSectionNonProf   = $('.academicNonProfit', $givingData);
    var $givingSectionAcademic  = $('.academicGiving', $givingData);
    var $givingSectionOther     = $('.otherCampaign', $givingData);
    var debugMode = !!Cookies.get("debug"); // debug mode state

    // overlay for legend
    var $legendOverlay = $();

    // globals
    var templateData;
    var $givingDetailsPopup = undefined;

    // partition
    var activePartition;

    // primary change timers
    let primaryPhoneChangeTimer = null;
    let primaryEmailChangeTimer = null;

    // graph constants
    var marginBetweenGraphSlices = 2;
    var graphBackground = ["political-spectrum-bar-democrat",
                           "political-spectrum-bar-republican",
                           "political-spectrum-bar-neutral"];

    /*
     *
     */
    function reloadProspectDetails()
    {
        // display Loading
        $givingLoading.show(1);
        $givingData.hide();

        // mixpanel
        mixpanel.track("Prospect Details",
                    {"analysisId": templateData.analysisId,
                     "partitionId": activePartition,
                     "contactId": templateData.contactId,
                    });


        // get the Data
        var url = templateData.prospectDetailUrl.replace('seatId', templateData.seatId)
                                               .replace('contactSetId', templateData.activeContactSetId)
                                               .replace("contactId", templateData.contactId);
        url += '?partition=' + activePartition;

        $.get(url)
            .done(function(r) {
                loadData(r.features, r.feature_result_map);
            })
            .fail(function(r) {
                revupUtils.apiError('Prospect Details', r, 'Reload Prospects List', "Unable to load Prospect Details");

            })
            .always(function(r) {
                $givingLoading.hide();
                $givingData.show(1);
            });
    } // reloadProspectDetails


    /*
     * load functions
     */
    function loadHeader(headerData, keySignals, detailData)
    {
        var sTmp = [];

        // images & profile
        var imageUrl = undefined;
        if (headerData.image_urls && headerData.image_urls.length > 0) {
            // look for the LinkIn image
            for (var i = 0; i < headerData.image_urls.length; i++) {
                if (headerData.image_urls[i].source == 'LinkedIn') {
                    imageUrl = headerData.image_urls[i].url;

                    break;
                }
            }
        }

        var profileUrl = undefined;
        if (headerData.external_profiles && headerData.external_profiles.length > 0) {
            // look for the LinkIn profile
            for (var i = 0; i < headerData.external_profiles.length; i++) {
                if (headerData.external_profiles[i].source == 'LinkedIn') {
                    profileUrl = headerData.external_profiles[i].url;

                    break;
                }
            }
        }

        var imageDisplaySectionClass = "";
        if (imageUrl) {
            imageDisplaySectionClass = " imageClassExtra";
            sTmp.push('<div class="section photoDiv">');
                if (profileUrl) {
                    sTmp.push('<a href="' + profileUrl + '" target="_blank">');
                }
                    sTmp.push('<img class="detailImg" src="' + imageUrl + '">')
                if (profileUrl) {
                    sTmp.push('</a>')
                }
            sTmp.push('</div>');
        }

        // load the Name
        sTmp.push('<div class="section prospectNameDiv' + imageDisplaySectionClass + '">');
            sTmp.push('<div class="heading">prospect</div>');
            sTmp.push('<div class="data">');
                sTmp.push('<div class="dataText">');
                    sTmp.push(headerData.first_name + ' ' + headerData.last_name);
                sTmp.push('</div>');
                if (!imageUrl && profileUrl) {
                    sTmp.push('<a href="' + profileUrl + '" target="_blank">');
                        sTmp.push('<div class="profileImg"></div>');
                    sTmp.push('</a>')
                }
            sTmp.push('</div>');
        sTmp.push('</div>');

        if (headerData.phone_numbers.length > 0) {
            let p = '';
            if (!templateData.phoneShowMulti || headerData.phone_numbers.length == 1) {
                p = headerData.phone_numbers[0].number;
                p = revupUtils.getFormattedPhone(p);
            }
            sTmp.push('<div class="section phoneDiv' + imageDisplaySectionClass + '">');
                sTmp.push('<div class="heading">phone</div>');
                sTmp.push('<div class="data">');
                    sTmp.push('<div class="dataText phoneLst">');
                        sTmp.push(p);
                    sTmp.push('</div>');
                    if (templateData.phoneShowMulti && headerData.phone_numbers.length > 1) {
                        sTmp.push('<div class="primaryPhoneChanged icon icon-check"></div>')
                    }
                sTmp.push('</div>');
            sTmp.push('</div>');
        }

        if (headerData.email_addresses.length > 0) {
            let e = ''
            if (!templateData.emailShowMulti || headerData.email_addresses.length == 1) {
                e = '<a href="mailto:' + headerData.email_addresses[0].address + '">' + headerData.email_addresses[0].address + '</a>';
            }
            sTmp.push('<div class="section emailDiv' + imageDisplaySectionClass + '">');
                sTmp.push('<div class="heading">email</div>');
                sTmp.push('<div class="data">');
                    sTmp.push('<div class="dataText emailLst">');
                        sTmp.push(e);
                    sTmp.push('</div>');
                    if (templateData.emailShowMulti && headerData.email_addresses.length > 1) {
                        sTmp.push('<div class="primaryEmailChanged icon icon-check"></div>')
                    }
                sTmp.push('</div>');
            sTmp.push('</div>');
        }

        if (headerData.addresses.length > 0) {
            var addr = [];
            if (headerData.addresses[0].street.length > 0) {
                addr.push(headerData.addresses[0].street);
            }
            else if (headerData.addresses[0].po_box.length > 0) {
                addr.push(headerData.addresses[0].po_box);
            }
            if (headerData.addresses[0].city.length > 0) {
                addr.push("&nbsp;&nbsp;&nbsp;" + headerData.addresses[0].city);
            }
            if (headerData.addresses[0].region.length > 0) {
                addr.push(",&nbsp;" + headerData.addresses[0].region);
            }
            if (headerData.addresses[0].post_code.length > 0) {
                addr.push("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + headerData.addresses[0].post_code);
            }

            sTmp.push('<div class="section addressDiv' + imageDisplaySectionClass + '">');
                sTmp.push('<div class="heading">address</div>');
                sTmp.push('<div class="data">');
                    sTmp.push('<div class="dataText">');
                        sTmp.push(addr.join(''));
                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');
        }

        if (headerData.addresses.length == 0) {
            var location;
            if (headerData.locations.length > 0) {
                location=headerData.locations[0];

                sTmp.push('<div class="section addressDiv' + imageDisplaySectionClass + '">');
                    sTmp.push('<div class="heading">location</div>');
                    sTmp.push('<div class="data">');
                        sTmp.push('<div class="dataText">');
                            sTmp.push(location.name);
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            }
        }

        if (debugMode && keySignals != undefined) {
          if ('expansion_hit' in keySignals) {
              sTmp.push('<div class="section debugDiv expansionHitDiv">');
                  sTmp.push('<div class="heading">Expansion Hit</div>');
                  sTmp.push('<div class="data">');
                      sTmp.push('<div class="dataText">');
                          sTmp.push(keySignals.expansion_hit);
                      sTmp.push('</div>');
                  sTmp.push('</div>');
              sTmp.push('</div>');
          }
          if ('gender' in keySignals) {
              sTmp.push('<div class="section debugDiv genderDiv">');
                  sTmp.push('<div class="heading">gender</div>');
                  sTmp.push('<div class="data">');
                      sTmp.push('<div class="dataText">');
                          sTmp.push(keySignals.gender);
                      sTmp.push('</div>');
                  sTmp.push('</div>');
              sTmp.push('</div>');
          }
          if ('given_name_origin' in keySignals) {
              var origins = [];
              for (var origin in keySignals.given_name_origin) {
                  // Filter out given name origins that have a false value
                  if (keySignals.given_name_origin.hasOwnProperty(origin) && keySignals.given_name_origin[origin]) {
                      origins.push(origin);
                  }
              }
              sTmp.push('<div class="section debugDiv originDiv">');
                  sTmp.push('<div class="heading">given name origin</div>');
                  sTmp.push('<div class="data">');
                      sTmp.push('<div class="dataText">');
                          sTmp.push(origins.join(', '));
                      sTmp.push('</div>');
                  sTmp.push('</div>');
              sTmp.push('</div>');
          }
          if ('family_name_ethnicity' in keySignals) {
              var ethnicities = [];
              for (var ethnicity in keySignals.family_name_ethnicity) {
                  // Filter out ethnicities with a population density lower than 50%
                  if (keySignals.family_name_ethnicity.hasOwnProperty(ethnicity) && keySignals.family_name_ethnicity[ethnicity] >= 50) {
                      ethnicities.push(ethnicity);
                  }
              }
              sTmp.push('<div class="section debugDiv ethnicityDiv">');
                  sTmp.push('<div class="heading">family name ethnicity</div>');
                  sTmp.push('<div class="data">');
                      sTmp.push('<div class="dataText">');
                          sTmp.push(ethnicities.join(', '));
                      sTmp.push('</div>');
                  sTmp.push('</div>');
              sTmp.push('</div>');
          }
        }

        // load the data
        $header.html(sTmp.join(''));

        let $primaryPhoneChanged = $('.phoneDiv .data .primaryPhoneChanged', $header);
        let $primaryEmailChanged = $('.emailDiv .data .primaryEmailChanged', $header);
        $primaryEmailChanged.hide();
        $primaryPhoneChanged.hide();
        if (primaryEmailChangeTimer) {
            clearTimeout(primaryEmailChangeTimer);

            primaryEmailChangeTimer = null;
        }
        if (primaryPhoneChangeTimer) {
            clearTimeout(primaryPhoneChangeTimer);

            primaryPhoneChangeTimer = null;
        }

        if (templateData.phoneShowMulti && headerData.phone_numbers.length > 1) {
            let phoneNum = [];
            let primaryPhoneId = undefined//$('.nameEmailDiv .phone', $individualDetailsContactDiv).attr('primaryPhone');
            let startIndex = 0;
            for (let i = 0; i < headerData.phone_numbers.length; i++) {
                let o = new Object();
                o.value = headerData.phone_numbers[i].id;
                o.displayValue = revupUtils.getFormattedPhone(headerData.phone_numbers[i].number)
                phoneNum.push(o);

                if (templateData.primaryPhone == headerData.phone_numbers[i].id)
                    startIndex = i;
            }

            let $phone = $('.phoneDiv .phoneLst', $header);
            $phone.revupDropDownField({
                value: phoneNum,
                valueStartIndex: startIndex,
                bAutoDropDownPadding: true,
                changeFunc: function(index, val) {
                    // call the update function
                    if (templateData.phoneUpdateFunc) {
                        templateData.phoneUpdateFunc(val);

                        $primaryPhoneChanged.show();
                        primaryPhoneChangeTimer = setTimeout(function() {
                            $primaryPhoneChanged.fadeOut();
                        }, 5000);
                    }
                }
            });
        }

        if (templateData.emailShowMulti && headerData.email_addresses.length > 1) {
            let emailAddr = [];
            //let primaryPhoneId = undefined//$('.nameEmailDiv .phone', $individualDetailsContactDiv).attr('primaryPhone');
            let startIndex = 0;
            for (let i = 0; i < headerData.email_addresses.length; i++) {
                let o = new Object();
                o.value = headerData.email_addresses[i].id;
                o.displayValue = headerData.email_addresses[i].address;
                emailAddr.push(o);

                if (templateData.primaryEmail == headerData.email_addresses[i].id)
                    startIndex = i;
            }

            let $email = $('.emailDiv .emailLst', $header);
            $email.revupDropDownField({
                value: emailAddr,
                valueStartIndex: startIndex,
                bAutoDropDownPadding: true,
                changeFunc: function(index, val) {
                    // call the update function
                    if (templateData.emailUpdateFunc) {
                        templateData.emailUpdateFunc(val);

                        $primaryEmailChanged.show();
                        primaryEmailChangeTimer = setTimeout(function() {
                            $primaryEmailChanged.fadeOut();
                        }, 5000);
                    }
                }
            });

        }

    } // loadHeader

    function loadData(featureData, featureResultMap)
    {
        function findFeatureEntry(featureName, data)
        {
            var lookupData = (data === undefined) ? featureData : data;
            for(var key in lookupData) {
                if (key.startsWith(featureName, true)) {
                    for (var sub_key in lookupData[key]) {
                        return (lookupData[key][sub_key]);
                    }
                }
            }

            return undefined
        } // findFeatureEntry

        function formatName(name)
        {
            if (name === undefined) {
                name = '---';
            }
            else {
                var fname = name;
                fname = fname.split(',');
                for (var ii = 0; ii < fname.length; ii++) {
                    fname[ii] = $.trim(fname[ii]);
                }

                // name the name first, last
                fname.reverse();
                var name = revupUtils.toProperCaseName(fname[0]);
                if (fname.length > 1) {
                    name += " " + revupUtils.toProperCaseName(fname[1]);
                }

            }

            return name;
        }

        function formatAmount(amount)
        {
            if (amount !== undefined) {
                amount = parseInt(amount, 10);
                amount = "$" + revupUtils.commify(amount);
            }
            else {
                amount = '&nbsp;';
            }

            return amount;
        } // formatAmount

        function formatDate(date)
        {
            if (date !== undefined) {
                date = revupUtils.formatDate(date);
            }
            else {
                date = '---';
            }

            return date;
        } // formatDate

        function buildHeaderGraph($loc, dataArray, dataTotal)
        {
            // compute the sections of the graph
            var wDiv = $loc.width();
            var num = 0;
            var slice = [];
            for (var i = 0; i < dataArray.length; i++) {
                if (dataArray[i] != 0) {
                    num += 1
                    slice[i] = dataArray[i] / dataTotal;
                }
                else {
                    slice[i] = -1;
                }
            }
            // build the div's
            wDiv -= (marginBetweenGraphSlices * (num - 1));
            var sTmp = [];
            var numSliceDisplayed = 0;
            for (var i = 0; i < slice.length; i++) {
                if (slice[i] != -1) {
                    numSliceDisplayed += 1;
                    var w = Math.floor(wDiv * slice[i]);
                    var lMargin = 0;
                    var rMargin = 0;

                    // get the amount of the margin
                    if ((num > 1) && ((numSliceDisplayed == 0) || (numSliceDisplayed != num))) {
                        rMargin = (marginBetweenGraphSlices / 2);
                    }
                    if ((num > 1) && (numSliceDisplayed > 1)) {
                        lMargin = (marginBetweenGraphSlices / 2);
                    }

                    // build the style
                    var s = "";
                    s = "width:" + w + "px;"
                    if (rMargin != 0) {
                        s += "margin-right:" + rMargin + "px;";
                    }
                    if (lMargin != 0) {
                        s += "margin-left:" + lMargin + "px;";
                    }

                    sTmp.push('<div class="graphSlice ' + graphBackground[i] +'" style="' + s +'"></div>')
                }
            }
            $('.graph', $loc).html(sTmp.join(''));
        }

        function formatFeatureData(data, $section, bNoPartyData, bNonProfAcad, bAcademic)
        {
            // default values
            if (bNoPartyData == undefined) {
                bNoPartyData = false;
            }
            if (bNonProfAcad == undefined) {
                bNonProfAcad = false;
            }

            if ((data == undefined) || (data.match == undefined) || (data.match.length == 0)) {
                // show no data
                $('.sectionHeader .headerNoData', $section).show(1);
                $('.sectionHeader .graphDiv.candidates', $section).hide();
                $('.sectionHeader .graphDiv.dollars', $section).hide();

                return;
            }

            var match = data.match;

            // hide no data
            $('.sectionHeader .headerNoData', $section).hide();
            $('.sectionHeader .graphDiv.candidates', $section).hide();
            $('.sectionHeader .graphDiv.dollars', $section).hide();

            // counts used for graphs
            //      0 - democrats, 1 - republican, 2 - other
            var amtFor = [0, 0, 0];
            var numOf = [0, 0, 0];
            var totalNumOf = 0;
            var totalAmtFor = 0
            var sectAmount = 0;

            // load the
            var sTmp = [];
            var dataObj = [];
            for (var i = 0; i < match.length; i++) {
                // build the contribution data
                if (match[i].contributor != undefined) {
                    // look for 4 pieces of data
                    dataObj[i] = new Array;
                    for (var d = 0; d < 4; d++) {
                        if ((match[i].contributor[d] != undefined) && (match[i].contributor[d] != '')) {
                            dataObj[i][d] = match[i].contributor[d];
                        }
                    }
                }
                sTmp.push('<div class="entry" ' + (debugMode ? 'style="height: 100%"' : '') + ' >');
                    // party
                    var blobColor = "";//"political-spectrum-bar-none";
                    var blobColorStyle = "";
                    if (!bNoPartyData) {
                        var p = '';
                        if (match[i].party) {
                            p = match[i].party.toLowerCase();
                        }
                        var amt = 0;
                        if (match[i].amount) {
                            amt = parseInt(match[i].amount, 10);
                        }
                        totalAmtFor += amt;
                        totalNumOf += 1;
                        if (p == "dem") {
                            amtFor[0] += amt;
                            numOf[0] += 1;
                            blobColor = "political-spectrum-bar-democrat";
                        }
                        else if (p == "rep") {
                            amtFor[1] += amt;
                            numOf[1] += 1;
                            blobColor = "political-spectrum-bar-republican";
                        }
                        else {
                            amtFor[2] += amt;
                            numOf[2] += 1;
                            blobColor = "political-spectrum-bar-neutral";
                        }
                    }
                    else if (bAcademic) {
                        // skin color takes precedence
                        blobColorStyle = ' style="background-color:#a4b58e;"';
                        if ((currentSkin.colors) && currentSkin.colors['scoreHighlightColor']) {
                            // look for a background color rule
                            colorRules = currentSkin.colors['scoreHighlightColor'];
                            for (var cr = 0; cr < colorRules.length; cr++) {
                                if (colorRules[cr].colorType == "background") {
                                    blobColorStyle = ' style="background:' + colorRules[cr].colorVal + ';"';
                                }
                            }
                        }
                        if (match[i].amount) {
                            sectAmount += parseInt(match[i].amount, 10);
                        }
                    }
                    sTmp.push('<div class="party">');
                        sTmp.push('<div class="partyBlob ' + blobColor + '"' + blobColorStyle + '></div>');
                    sTmp.push('</div>');

                    if (bNonProfAcad) {
                        sTmp.push('<div class="name nonProfAcadName">');
                    }
                    else {
                        sTmp.push('<div class="name">');
                    }
                        if (match[i].school) {
                            sTmp.push(match[i].school)
                        }
                        else {
                            sTmp.push(match[i].recipient);
                        }
                    sTmp.push('</div>');

                    let nfpAmount = "&nbsp;";
                    if (bNonProfAcad) {
                        sTmp.push('<div class="amount nonProfAcadAmount">');
                    }
                    else {
                        sTmp.push('<div class="amount">');
                    }

                    if (match[i].amount) {
                        isNaN(match[i].amount)? nfpAmount = match[i].amount : nfpAmount = formatAmount(match[i].amount);
                    }
                        sTmp.push(nfpAmount);
                    sTmp.push('</div>');

                    date = "&nbsp;";
                    if (bNonProfAcad) {
                        sTmp.push('<div class="date nonProfAcadDate">');
                        if (match[i].date) {
                            date = match[i].date.slice(0, 4);
                        }
                    }
                    else {
                        sTmp.push('<div class="date">');
                        if (match[i].date) {
                            var fDate = formatDate(match[i].date);
                            if (fDate != '') {
                                date = fDate;
                            }
                        }
                    }
                        sTmp.push(date);
                    sTmp.push('</div>');

                    // contribution Details
                    if (bNonProfAcad) {
                        sTmp.push('<div class="contribDetails nonProfAcadDetails">');
                    }
                    else {
                        sTmp.push('<div class="contribDetails">');
                    }
                        if (match[i].contributor != undefined) {
                            sTmp.push('<div class="icon icon-info-1"></div>');
                        }
                        else {
                            sTmp.push('&nbsp;')
                        }
                    sTmp.push('</div>');
                    if (debugMode) {
                      sTmp.push('<br/><span>Contributor given name: ' + match[i].given_name + '. Family name: ' + match[i].family_name + '. Expansion hit: ' + match[i].expansion_hit + '</span>');
                    }
                sTmp.push('</div>');
            }

            // add the total entry
            if (!bNonProfAcad) {
                var bDisplay = true;
                if (bAcademic && sectAmount == 0) {
                    bDisplay = false;
                }
                if (bDisplay) {
                    sTmp.push('<div class="entry total">');
                        sTmp.push('<div class="party"></div>');
                        sTmp.push('<div class="name">Total</div>');
                        sTmp.push('<div class="amount">');
                            if (bAcademic) {
                                sTmp.push(formatAmount(sectAmount));
                            }
                            else {
                                sTmp.push(formatAmount(data.giving));
                            }
                        sTmp.push('</div>');
                        sTmp.push('<div class="date"></div>');
                    sTmp.push('</div>');
                }
            }
            else {
                sTmp.push('<div class="entry last"></div>');
            }

            $('.sectionBody', $section).html(sTmp.join(''));

            var entries = $('.sectionBody .entry', $section);
            $.each(entries, function(index, val) {
                if (index < match.length) {
                    $(this).data('contributor', dataObj[index]);
                }
            });

            // display the graphs
            if (!bNoPartyData) {
                var $candidates = $('.sectionHeader .graphDiv.candidates', $section);
                var $dollars = $('.sectionHeader .graphDiv.dollars', $section);
                $candidates.show();
                $dollars.show();

                buildHeaderGraph($candidates, numOf, totalNumOf);
                buildHeaderGraph($dollars, amtFor, totalAmtFor);
            }
        } // formatFeatureData


        // academic - prudue
        var academicData = findFeatureEntry("Purdue Giving")
        if (academicData != undefined) {
            // display the section
            $givingSectionAcademic.show(1);

            formatFeatureData(academicData[0], $givingSectionAcademic, true, false, true)
        }
        else {
            $givingSectionAcademic.hide();
        }

        // academic/nonprofit
        var nonProfitData = findFeatureEntry("nonprofit")
        if (nonProfitData != undefined) {
            // display the section
            $givingSectionNonProf.show(1);

            formatFeatureData(nonProfitData[0], $givingSectionNonProf, true, true)
        }
        else {
            $givingSectionNonProf.hide();
        }

        // state
        var stateData = findFeatureEntry("state matches");
        var irsPolitical = findFeatureEntry('irs political contributions');
        var irsClient = findFeatureEntry('irs client contributions');
        if ((stateData != undefined) || (irsPolitical != undefined) || (irsClient != undefined))  {
            // display the section
            $givingSectionState.show(1);

            // merge the data sets
            var mData = {};
            mData.match = [];
            mData.giving = 0;
            if ((stateData != undefined) && (stateData.length > 0) && (stateData[0].match != undefined)) {
                mData.giving += stateData[0].giving;
                for(var i = 0; i < stateData[0].match.length; i++) {
                    mData.match.push(stateData[0].match[i]);
                }
            }
            if ((irsPolitical != undefined) && (irsPolitical.length > 0) && (irsPolitical[0].match != undefined)) {
                mData.giving += irsPolitical[0].giving;
                for(var i = 0; i < irsPolitical[0].match.length; i++) {
                    mData.match.push(irsPolitical[0].match[i]);
                }
            }
            if ((irsClient != undefined) && (irsClient.length > 0) && (irsClient[0].match != undefined)) {
                mData.giving += irsClient[0].giving;
                for(var i = 0; i < irsClient[0].match.length; i++) {
                    mData.match.push(irsClient[0].match[i]);
                }
            }

            // sort via date then recipient
            mData.match.sort(function(a, b){
                if (a.date < b.date) {
                    return 1;
                }
                else if (a.date > b.date) {
                    return -1
                }
                else {
                    if (a.recipient < b.recipient) {
                        return -1;
                    }
                    else if (a.recipient > b.recipient) {
                        return 1;
                    }
                    else {
                        return 0;
                    }
                }
            });

            formatFeatureData(mData, $givingSectionState)
        }
        else {
            $givingSectionState.hide();
        }

        // local
        var localData = findFeatureEntry("Local Matches");
        if (localData != undefined) {
            // display the section
            $givingSectionLocal.show(1);

            formatFeatureData(localData[0], $givingSectionLocal, true)

        }
        else {
            $givingSectionLocal.hide();
        }

        // federal
        var fedData = findFeatureEntry("Federal Matches");
        if (fedData != undefined) {
            if (debugMode) {
                var fedFeatureResultId = findFeatureEntry("Federal Matches", featureResultMap);
                $('#federalEntityDebug', $givingSectionFederal).html('<a href="/rankings/entity/' + fedFeatureResultId + '/">Entity Debug</a>');
            }
            // display the section
            $givingSectionFederal.show(1);

            formatFeatureData(fedData[0], $givingSectionFederal);
        }
        else {
            $givingSectionFederal.hide();
        }

        // other
        var otherData = findFeatureEntry("Other Associations");
        if ((otherData != undefined) && (otherData.length > 0)) {
            // display the section
            $givingSectionOther.show(1);

            formatFeatureData(otherData[0], $givingSectionOther);
        }
        else {
            $givingSectionOther.hide();
        }

        // add tool tip to ellipsis names
//console.log('start tooltip')
//        $('.entry .name', $givingData).fieldEllipsis();
//        $('.entry .nonProfAcadamount', $givingData).fieldEllipsis();
//console.log('end tooltip')
    } // loadData

    function loadCntlsEvents()
    {
        /*
         * Partition or cycle
         */
        if (templateData.partitionLst && templateData.partitionLst.length > 0) {
            var partitionLstStartIndex = 0;
            for (var i = 0; i < templateData.partitionLst.length; i++) {
                if (activePartition == templateData.partitionLst[i].value) {
                    partitionLstStartIndex = i;
                }
            }
            activePartition = templateData.partitionLst[partitionLstStartIndex].value;

            $partition.revupDropDownField({
                value: templateData.partitionLst,
                valueStartIndex: partitionLstStartIndex,
                sortList: false,
                changeFunc: function(index, val) {
                    // save the partition value
                    activePartition = val;
                    //empty divs
                    $(".sectionBody .entry").remove();
                    // reload the prospect details
                    reloadProspectDetails();

                    // save the new partition
                    //localStorage.setItem('rankPartition', val);
                }
            });
        }

        /*
         * open/close of section
         */
        $givingData.on('click', ".sectionHeader .openClose", function() {
            var $section = $(this).closest('.givingTypeSection');
            if ($section.hasClass('closed')) {
                // time to open things
                // change the icon to down arrpw
                var $icon = $('.openClose', $section);
                $icon.removeClass('icon-triangle-right').addClass('icon-triangle-dn');

                // slide down the body
                var $sectionBody = $('.sectionBody', $section);
                $sectionBody.slideDown('slow', function() {
                    // completion
                    $section.removeClass('closed').addClass('opened');
                });

                // open the footer
                $('.sectionFooter', $section).slideDown('fast');
            }
            else {
                // time to close things
                // change the icon to down arrpw
                var $icon = $('.openClose', $section);
                $icon.removeClass('icon-triangle-dn').addClass('icon-triangle-right');

                // slide down the body
                var $sectionBody = $('.sectionBody', $section);
                $sectionBody.slideUp('slow', function() {
                    // completionn
                    $section.removeClass('opened').addClass('closed');
                });

                // open the footer
                $('.sectionFooter', $section).slideUp('fast');
            }
        });

        /*
         * legend event handlers
         */
        var $legendPopup = $('.prospectDetailsPopUp .legendPopup2');
        var $legendCloseBtn = $('.closeBtn', $legendPopup);
        //var $legendOverlay = $();
        $('.prospectDetailsPopUp').on('click', '.legendIconBtn', function() {
            var $this = $(this);

            if ($legendPopup.is(':visible')) {
                $legendPopup.hide();
                //$legendOverlay.trigger('revup.overlayClose');
                //$legendOverlay = $();

                return;
            }

            // add the overlay
            //$legendOverlay = $this.overlay();
            //$this.on('revup.overlayClick', function() {
            //    $legendPopup.hide();
            //});

            // see if the esc button is pressed
            $(document).on('keydown', function(e) {
                if (e.keyCode == 27) {
                    $legendPopup.hide();
                    //$legendOverlay.trigger('revup.overlayClose');
                    //$legendOverlay = $();

                    $(document).off('keydown');

                    e.stopPropagation();
                    e.preventDefault();
                }
            })

            // position and display
            var pos = $this.position();
            var top = pos.top + $this.height() + 5;
            var left = pos.left - (($legendPopup.width() - $this.width()) / 2);
            left += 50; // width of left nav
            $legendPopup.css('top', top)
                        .css('left', left)
                        .show();

            // close button handler
            $legendCloseBtn.off('click')
                           .on('click', function() {
                               $legendPopup.hide();
                              // $legendOverlay.trigger('revup.overlayClose');
                              // $legendOverlay = $();
                           });
        })

        /*
         * display donor contributuon detail
         */
        $givingData.on('click', '.givingTypeSection .contribDetails', function(e) {
            var $btn = $(this);
            var $entry = $btn.closest(".entry");
            var contribData = $entry.data('contributor');
            if (contribData == undefined) {
                return;
            }

            // see if left or right side
            var popupLoc = 'left';
            if ($btn.closest(".leftSide").length == 0) {
                popupLoc = 'left';
            }
            else if ($btn.closest(".rightSide").length == 0) {
                popupLoc = 'right';
            }

            // build the content
            var content = [];
            content.push('<div class="prospectDetailsContribPopup">');
                for (var i = 0; i < contribData.length; i++) {
                    content.push('<div class="msg">');
                        content.push(contribData[i]);
                    content.push('</div>');
                }
            content.push('</div>')

            $givingDetailsPopup = $(this).revupPopup({bAutoClose: true,
                                                     autoCloseTimeout: 10000,
                                                     bSingle: true,
                                                     location: popupLoc,
                                                     zIndex: 25,
                                                     content: content.join(''),
                                                 });
        });

        $('.mainPage .detailImg').on('error', function(e) {
            $('.mainPage .photoDiv').hide();
            $('.mainPage .section').removeClass('imageClassExtra')
        })

    } // loadCntlsEvents

    /*
     * legend
     */
    function buildLegendView()
    {
        var sTmp = [];


        sTmp.push('<div class="legendPopup2">');
            sTmp.push('<div class="header">');
                sTmp.push('<div class="title">Legend</div>');
                sTmp.push('<div class="closeBtn icon icon-close"></div>');
            sTmp.push('</div>');

            sTmp.push('<div class="legendBody">');
                sTmp.push('<div class="leftSide">');
                    sTmp.push('<div class="iconDiv">');
                        sTmp.push('<div class="entry">');
                            sTmp.push('<div class="leftIconSide">');
                                sTmp.push('<div class="imgIcon legendIcon">');
                                    sTmp.push('<div class="icon icon-key"></div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');
                            sTmp.push('<div class="rightIconSide">');
                                sTmp.push('<div class="text">Legend</div>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="leaningDiv">');
                        sTmp.push('<div class="entry">');
                            sTmp.push('<div class="colorBlob political-spectrum-bar-democrat"></div>');
                            sTmp.push('<div class="text">Democrat</div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entry">');
                            sTmp.push('<div class="colorBlob political-spectrum-bar-democrat-lite"></div>');
                            sTmp.push('<div class="text">Democrat Leaning</div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entry">');
                            sTmp.push('<div class="colorBlob political-spectrum-bar-neutral"></div>');
                            sTmp.push('<div class="text">Neutral</div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entry">');
                            sTmp.push('<div class="colorBlob political-spectrum-bar-republican-lite"></div>');
                            sTmp.push('<div class="text">Republican Leaning</div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entry">');
                            sTmp.push('<div class="colorBlob political-spectrum-bar-republican"></div>');
                            sTmp.push('<div class="text">Republican</div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        return sTmp.join('');
    } // buildLegendView

    /*
     * external functions
     */
    return {
        display: function(pTemplateData, detailData)
        {
            // selectors
            $mainPageLoading        = $('.newProspectDetail .mainPageLoading');
            $mainPage               = $('.newProspectDetail .mainPage');
            $header                 = $('.pageHeader', $mainPage);
            $partition              = $('.partitionHeaderDiv .partitionDiv', $mainPage);
            $givingLoading          = $('.givingSection .givingLoading', $mainPage);
            $givingData             = $('.givingSection .givingLoadedData', $mainPage);
            $givingSectionLocal     = $('.localCampaign', $givingData);
            $givingSectionState     = $('.stateCampaign', $givingData);
            $givingSectionFederal   = $('.federalCampaign', $givingData);
            $givingSectionNonProf   = $('.academicNonProfit', $givingData);
            $givingSectionAcademic  = $('.academicGiving', $givingData);
            $givingSectionOther     = $('.otherCampaign', $givingData);
            debugMode = !!Cookies.get("debug"); // debug mode state

            // clear the timers
            primaryEmailChangeTimer = null;
            primaryPhoneChangeTimer = null;

            // keep locals
            templateData = pTemplateData;

            // active partition
            activePartition = templateData.partitionStart;

            // make sure that the forward/back button casue a reload
            window.onpopstate = function(event) {
                if(event && event.state) {
                    location.reload();
                }
            }

            // mixpanel
            mixpanel.track("Prospect Details",
                        {"analysisId": templateData.analysisId,
                         "partitionId": templateData.partitionStart,
                         "contactId": templateData.contactId,
                        });

            // get the Data
            if (!detailData) {
                var url = templateData.prospectDetailUrl.replace('analysisId', templateData.analysisId)
                                                        .replace('partitionVal', templateData.partitionStart)
                                                        .replace('contactId', templateData.contactId);
                $.get(url)
                    .done(function(r) {
                        // add legend
                        $('.mainPage').append(buildLegendView());

                        // load header data
                        if (r.results)
                        loadHeader(r.contact, r.key_signals);

                        // load data
                        loadData(r.features, r.feature_result_map);

                        // load controls and event handlers
                        loadCntlsEvents();
                    })
                    .fail(function(r) {
                        revupUtils.apiError('Prospect Details', r, 'Load Ranking List', "Unable to retreive Prospect Details");
                    })
                    .always(function(r) {
                        $mainPageLoading.hide();
                        $("html, body").scrollTop(0);
                        $mainPage.show(1);
                    });
            }
            else {
                // add legend
                $('.mainPage').append(buildLegendView());

                if (detailData.result) {
                    // load header data
                    loadHeader(detailData.contact, detailData.result.key_signals, detailData);

                    // load data
                    loadData(detailData.result.features, detailData.feature_result_map);
                }
                else {
                    // load header data
                    loadHeader(detailData.contact, detailData.key_signals, detailData);

                    // load data
                    loadData(detailData.features, detailData.feature_result_map);
                }

                // load controls and event handlers
                loadCntlsEvents();

                $mainPageLoading.hide();
                //$("html, body").scrollTop(0);
                $mainPage.show(1);
            }
        }, // display

        close: function() {
            $legendOverlay.trigger('revup.overlayClose');
            $legendOverlay = $();

            if (($givingDetailsPopup != undefined) && ($givingDetailsPopup.length > 0)) {
                $givingDetailsPopup.revupPopup('close');

                $givingDetailsPopup = undefined;
            }
        }
    };
} ());
