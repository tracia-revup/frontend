var editBillingInfo = function(ccTemplate) {
    // Globals
    let $creditCardDlg              = $();
    let $creditCardSave             = $();
    let $creditCardErrors           = $();
    let $detailsLoading             = $('.detailsSection .detailsLoading');
    let $detailsData                = $('.detailsSection .detailsData');
    let submitSuccess               = false;

    let validCardTypes  = ['master', 'visa', 'american_express'];

    let currentCCType   = '';
    let expMonth        = '';
    let expMonthIndex   = -1;
    let expYear         = '';
    let expYearIndex    = -1;
    let firstName       = '';
    let lastName        = '';
    let addr1           = '';
    let addr2           = '';
    let city            = '';
    let state           = '';
    let stateIndex      = -1;
    let postalCode      = '';

    const recurlyJsFile =   "https://js.recurly.com/v3/recurly.js";

    let displayValidations = (code, fields = [], message = '') => {
        let bannerMsg = '';
        let invalidFields = true;

        if (code == 'submit success') {
            bannerMsg = 'Your credit card info has been successfully submitted';
            invalidFields = false;
        }
        else if (code == 'validation') {
            bannerMsg = 'The following ';
            if (fields.length > 1) {
                bannerMsg += 'fields are invalid: ';
            }
            else {
                bannerMsg += 'field is invalid: ';
            }
        }
        else if (code == 'invalid-parameter') {
            bannerMsg = 'The following ';
            if (fields.length > 1) {
                bannerMsg += 'fields have invalid value: ';
            }
            else {
                bannerMsg += 'field has a invalid value: ';
            }
        }
        else if (code == 'api-error') {
            bannerMsg = 'An api error occured when trying to charge your credit card';
            console.error('Api error on with credit card: ' + message + ', field: ', fields)
        }

        // make the field readable
        let sTmp = [];
        if (fields.length > 0) {
            for (let i = 0; i < fields.length; i++) {
                switch (fields[i]) {
                    case 'number':
                        $('.creditCardInfoDiv .ccNum').addClass('has-error');
                        sTmp.push('Credit Card Number');
                        break;
                    case 'month':
                        $('.ccExpMonth', $creditCardDlg).addClass('has-error');
                        $('.ccExpYear', $creditCardDlg).addClass('has-error');
                        sTmp.push('Expiration - Month');
                        break;
                    case 'year':
                        $('.ccExpMonth', $creditCardDlg).addClass('has-error');
                        $('.ccExpYear', $creditCardDlg).addClass('has-error');
                        sTmp.push('Expiration - Year');
                        break;
                    case 'first_name':
                        sTmp.push('First Name');
                        break;
                    case 'last_name':
                        sTmp.push('Last Name');
                        break;
                    case 'address1':
                        sTmp.push('Address');
                        break;
                    case 'city':
                        sTmp.push("City");
                        break;
                    case 'state':
                        sTmp.push('State');
                        break;
                    case 'postal_code':
                        sTmp.push('Zip Code')
                        break;
                    case 'cvv':
                        sTmp.push('CVV');
                        break;
                    default:
                        sTmp.push(val);
                        break;
                }; // end switch - val
            };
        }

        if (!invalidFields) {
            $creditCardErrors.addClass('noErrors');
            $('.icon', $creditCardErrors).html('<div class="icon icon-check"></div>');
        }
        else {
            $('.icon', $creditCardErrors).html('<div class="icon icon-stop"></div>');
        }

        // load the message
        $('.msg', $creditCardErrors).html(bannerMsg + sTmp.join(', '));

        // display the message
        $creditCardDlg.show();
        $creditCardErrors.show();
        $creditCardErrors.fadeIn('slow');
    }; // displayValidations

    let displayPostError = (errResult) => {
        console.warn('Credit Card Error: ', errResult);

        let e = errResult.split(':');
        let code = 'unknown';
        let msg = errResult;
        let msgType = '';
        if (e.length == 2) {
            code = $.trim(e[0]);
            msg = $.trim(e[1]);
        }

        if (msg.startsWith('account.base')) {
            msgType = 'account.base';
            msg = $.trim(msg.substring(13));
        }
        else if (msg.startsWith('gateway_timeout: account.base')) {
            msgType = 'gateway_timeout';
            msg = $.trim(msg.substring(30));
        }
        msg = $.trim(msg.replace('billing_info.base', ''));

        switch(code) {
            case 'declined':
                $('.creditCardInfoDiv .ccNum').addClass('has-error');
                $('.creditCardInfoDiv .ccCVV').addClass('has-error');
                break;
            case 'unknown':
                break;
            case 'fraud_address':
                $('.creditCardInfoDiv .address').addClass('has-error');
                if ($('.creditCardDiv #ccAddress2').val() != '') {
                    $('.creditCardInfoDiv .address2').addClass('has-error');
                }
                $('.creditCardInfoDiv .city').addClass('has-error');
                $('.creditCardInfoDiv .state').addClass('has-error');
                $('.creditCardInfoDiv .zip').addClass('has-error');
                break;
            case 'fraud_gateway':
                break;
            case 'invalid':
                msg = 'Invalid Credit Card. Please re-enter you card number';
                $('.creditCardInfoDiv .ccNum').addClass('has-error');
                break;
            case 'insufficient_funds':
                break;
            case 'fraud_gateway':
                break;
            case 'fraud_ip_address':
                break;
            case 'call_issuer':
                break;
            case 'declined_bad':
                // cvv
                $('.creditCardInfoDiv .ccCVV').addClass('has-error');
                msg = 'Your Credit Card Number does not match the CVV code';
                break;
            case 'invalid_data':
            case 'declined_expiration_date':
                $('.creditCardInfoDiv .ccExpMonth').addClass('has-error');
                $('.creditCardInfoDiv .ccExpYear').addClass('has-error');
                break;
            case 'duplicate_transaction':
                break;
            case 'card_type_not_accepted':
                break;
            case 'declined_saveable':
                break;
        } // end switch -> code

        // load and display the error message
        $('.msg', $creditCardErrors).html(msg);

        $creditCardDlg.show();
        $creditCardErrors.show();
        $creditCardErrors.fadeIn('slow');

    }; // displayPostError

    let enableDisableOk = () => {
        let enable = true;
        let cc = $('#ccNumber').val();
        let ccType = recurly.validate.cardType(cc, true);

        switch (ccType) {
            case 'master':
            case 'visa':
                ccLength = 16;

                break;
            case 'american_express':
                ccLength = 15;
                break;
        }

        if (cc == '' || !recurly.validate.cardNumber(cc) || validCardTypes.indexOf(ccType) == -1 || cc.replace(/\s/g, "").length != ccLength) {
            enable = false;
        }

        // expiration
        if (expYearIndex == -1 || expMonthIndex == -1) {
            enable = false;
        }

        // cvv
        let cvv = $('#ccCVV').val();
        let cvvLen = (ccType == 'visa' || ccType == 'master') ? 3 : 4;
        if (cvv.length != cvvLen) {
            enable = false;
        }

        // first name
        firstName = $('#ccFirstName').val();
        if (firstName == '') {
            enable = false;
        }

        // last name
        lastName = $('#ccLastName').val();
        if (lastName == '') {
            enable = false;
        }

        // address 1
        addr1 = $('#ccAddress1').val();
        if (addr1 == '') {
            enable = false;
        }

        // address 2
        addr2 = $('#ccAddress2').val();

        // city
        city = $('#ccCity').val();
        if (city == '') {
            enable = false;
        }

        // state
        if (stateIndex == -1) {
            enable = false;
        }

        // zip code / postal code
        postalCode = $('#ccPostalCode').val();
        if (postalCode == '' || postalCode.length != 5) {
            enable = false;
        }

        if (enable) {
            $creditCardSave.removeClass('disabled');
        }
        else {
            $creditCardSave.addClass('disabled');
        }
    }; // enableDisableOk

    let loadDropDowns = () => {
        if (ccTemplate.recurlyAccountId == '') {
            return;
        }

        $('.monthDropDown', $creditCardDlg)
            .revupDropDownField({
                valueStartIndex: -1,
                sortList: false,
                ifStartIndexNeg1Msg: 'Month',
                value: [{value: "01", displayValue: "01 - January"},
                        {value: "02", displayValue: "02 - Feburary"},
                        {value: "03", displayValue: "03 - March"},
                        {value: "04", displayValue: "04 - April"},
                        {value: "05", displayValue: "05 - May"},
                        {value: "06", displayValue: "06 - June"},
                        {value: "07", displayValue: "07 - July"},
                        {value: "08", displayValue: "08 - August"},
                        {value: "09", displayValue: "09 - September"},
                        {value: "10", displayValue: "10 - October"},
                        {value: "11", displayValue: "11 - November"},
                        {value: "12", displayValue: "12 - December"},
                        ]
                })
            .on('revup.dropDownSelected', function(e) {
                // save the value
                $('#ccMonth').val(e.dropDownValue);
                expMonth = e.dropDownValue;
                expMonthIndex = e.dropDownIndex;

                // check if submit can be enabled
                enableDisableOk();
                $('.creditCardInfoDiv .entry.expDate').removeClass('has-error');
            });

        // build the list of year's - current year for 20 years
        let currentTime = new Date();
        let year = currentTime.getFullYear()
        let yearLst = [];
        for (let i = 0; i <= 20; i++) {
            yearLst.push(year + '');
            year++;
        }

        $('.yearDropDown', $creditCardDlg)
            .revupDropDownField({
                valueStartIndex: -1,
                sortList: false,
                ifStartIndexNeg1Msg: 'Year',
                value: yearLst,
            })
            .on('revup.dropDownSelected', function(e) {
                // save the value
                $('#ccYear').val(e.dropDownValue);
                expYear = e.dropDownValue;
                expYearIndex = e.dropDownIndex;

                // check if submit can be enabled
                enableDisableOk();
                $('.creditCardInfoDiv .entry.expDate').removeClass('has-error');
            });

        $('.stateDropDown', $creditCardDlg)
            .revupDropDownField({
                valueStartIndex: -1,
                sortList: false,
                ifStartIndexNeg1Msg: 'State',
                value: revupConstants.dropDownStates,
            })
            .on('revup.dropDownSelected', function(e) {
                // save the value
                $('#ccState').val(e.dropDownValue);
                state = e.dropDownValue;
                stateIndex = e.dropDownIndex;

                // check if submit can be enabled
                enableDisableOk();
                $('.creditCardInfoDiv .entry.addr').removeClass('has-error');
            });
    }; // loadDropDowns

    let loadEventHandlers = () => {
        if (ccTemplate.recurlyAccountId == '') {
            return;
        }

        // credit card number events
        let $creditCardImg = $('.creditCardInfoDiv .ccImage .imgDiv');

        $('.textInput')
            .on('blur', function(e) {
            let $this = $(this);

            if (($this).hasClass('textInput')) {
                if (!($this.is('#ccAddress2'))) {
                    if (($this).val() == "") {
                        $this.parent().addClass('has-error');
                    }
                    else {
                        $this.parent().removeClass('has-error');
                    }
                }


                if ($this.is('#ccCVV')) {
                    let cc = $('#ccNumber').val();
                    let ccType = recurly.validate.cardType(cc, true);
                    c = cc.replace(/ /g, '');

                    switch (ccType) {
                        case 'master':
                        case 'visa':
                            ccLength = 16;

                            break;
                        case 'american_express':
                            ccLength = 15;
                            break;
                    }

                    let cvv = $('#ccCVV').val();
                    let cvvLen = (ccType == 'visa' || ccType == 'master') ? 3 : 4;
                    if (cvv.length != cvvLen) {
                        $this.parent().addClass('has-error');
                    }
                }
            }
        })





        $("#ccNumber")
            .on('keydown', function(e) {
                // make sure a number
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 40)) {
                    return;
                }

                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            })
            .on('input', function(e) {
                // display the credit card image
                var cc = $(this).val();
                var ccType = recurly.validate.cardType(cc, true);
                currentCCType = ccType;
                switch (ccType) {
                    case 'visa':
                        $creditCardImg.removeClass('masterCardGlyph')
                                      .removeClass('americanExpressGlyph')
                                      .addClass('visaGlyph');

                        break;
                    case 'master':
                        $creditCardImg.addClass('masterCardGlyph')
                                      .removeClass('americanExpressGlyph')
                                      .removeClass('visaGlyph');

                        break;
                    case 'american_express':
                        $creditCardImg.removeClass('masterCardGlyph')
                                      .addClass('americanExpressGlyph')
                                      .removeClass('visaGlyph');

                        break;
                    default:
                        currentCCType = ''
                        $creditCardImg.removeClass('masterCardGlyph')
                                      .removeClass('americanExpressGlyph')
                                      .removeClass('visaGlyph');

                        break;
                } // end switch - ccType

                // clear any error until the next blur
                $('.creditCardInfoDiv .entry.ccNum').removeClass('has-error');
                $('.creditCardInfoDiv .entry.ccCVV').removeClass('has-error');

                // check if submit can be enabled
                enableDisableOk();

                // format the cc number
                var v = cc.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
                var matches = v.match(/\d{4,16}/g);
                if (ccType == 'american_express') {
                    //v = v.replace(/\b(\d{4})(\d{6})(\d{5})\b/, '$1 $2 $3');
                    v = v.replace(/\b(\d{4})/, '$1 ');
                    v = v.replace(/\b(\d{6})/, ' $1 ');
                    $(this).val(v);
                    return;
                    /*
                    v = v.replace(/\b(\d{4})/, '$1-');
                    v = v.replace(/-(\d{6})/, '-$1-');
                    v = v.substring(0,17);
                    */
                }
                var match = matches && matches[0] || ''
                var parts = []
                for (i=0, len=match.length; i<len; i+=4) {
                    parts.push(match.substring(i, i+4))
                }
                if (parts.length) {
                    $(this).val(parts.join(' '))
                } else {
                    $(this).val(cc)
                }
            })
            .on('blur', function(e) {
                var cc = $(this).val();

                // see if a valid card
                if (!recurly.validate.cardNumber(cc)) {
                    $(this).parent().addClass('has-error');
                    $('.ccType', $(this).parent()).text('');
                    return;
                }
                else {
                    var ccType = recurly.validate.cardType(cc, true);

                    // see if a supported card
                    var bSupportedCard = false;
                    for (var i = 0; i < validCardTypes.length; i++) {
                        if (ccType === validCardTypes[i]) {
                            bSupportedCard = true;
                            break;
                        }
                    }

                    if (bSupportedCard) {
                        // valid card
                        $('.creditCardInfoDiv .entry.ccNum').removeClass('has-error');
                        }
                    else {
                        $('.creditCardInfoDiv .entry.ccNum').addClass('has-error');
                    }
                }


            })

        // postal code (zipcode) events
        $('#ccPostalCode')
            .on('keydown', function(e) {
                // make sure a number
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 40)) {
                    return;
                }

                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();

                    return;
                }

                let zipcode = $(this).val();
                if (zipcode.length > 5) {
                    e.preventDefault();

                    return;
                }
            })
            .on('input', function(e) {
                // check if submit can be enabled
                enableDisableOk();
                $('.creditCardInfoDiv .entry.addr').removeClass('has-error');
            })

        // cvv events
        $('#ccCVV')
            .on('keydown', function(e) {
                // make sure a number
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 40)) {
                    return;
                }

                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();

                    return;
                }

                let cvv = $(this).val();
                let cvvLen = (currentCCType == 'visa' || currentCCType == 'master') ? 3 : 4;
                if (cvv.length >= cvvLen) {
                    e.preventDefault();

                    return;
                }
            })
            .on('input', function(e) {
                // check if submit can be enabled
                enableDisableOk();
            })

        $('#ccFirstName, #ccLastName').on('input', function(e) {
            enableDisableOk();
        })

        $(' #ccAddress1, #ccAddress2, #ccCity').on('input', function(e) {
            enableDisableOk();
            $('.creditCardInfoDiv .entry.addr').removeClass('has-error');
        })

        $('.btnDiv .rightSide .saveBtn',  $creditCardDlg).on('click', function (e) {
            if ($(this).hasClass('disabled')) {
                return;
            }
            else {
                submitCreditCard();
            }
        })


    }; // loadEventHandlers

    let submitCreditCard = () => {
        let rVal = true;
        recurly.token($('#creditCardForm'), function (err, token) {
            if (err) {
                // display the errors
                displayValidations(err.code, err.fields, err.message);

                rVal = false;
            } else {
                // recurly.js has filled in the 'token' field, so now we can submit the
                // form to your server; alternatively, you can access token.id and do
                // any processing you wish
                // post the data
                $detailsData.hide();
                $detailsLoading.show();

                $.ajax({
                    url: ccTemplate.ccUpdateUrl,
                    data: $('#creditCardForm').serialize(),
                    type: "PUT",
                    traditional: true,
                    contentType: 'application/x-www-form-urlencoded; charset=utf-8',
                    processData: true
                })
                .done(function(r) {
                    displayValidations('submit success');
                    $('body').revupMessageBox({
                        headerText: "Saved",
                        msg: "Analysis Configuration Updated"
                    });

                    $detailsLoading.hide();
                    $detailsData.show();

                    // empty all the fields in credit card form
                    $('#creditCardForm').get(0).reset();
                    $('#creditCardForm .monthDropDown').revupDropDownField('reset');
                    $('#creditCardForm .yearDropDown').revupDropDownField('reset');
                    $('#creditCardForm .stateDropDown').revupDropDownField('reset');

                    $creditCardSave.addClass('disabled');

                    return;
                })
                .fail(function(r) {
                    // clear the wait spinner

                    $detailsLoading.hide();

                    // display the error message
                    console.warn('error: ', r.responseJSON.detail)
                    displayPostError(r.responseJSON.detail);

                    return;
                })
            }
        })
    }; // submitCreditCard

    let displayCreditCardForm = () => {
        let sTmp = [];

        sTmp.push('<div class="errorDiv" style="display: none;">');
            sTmp.push('<div class="msgDiv">');
                sTmp.push('<div class="icon"></div>')
                sTmp.push('<div class="msg"></div>');
            sTmp.push('</div>');
        sTmp.push('</div>');
        sTmp.push('<div class="creditCardDiv">');
        if (ccTemplate.recurlyAccountId == '') {
            sTmp.push('<div class="contactRevUpMsg">');
                sTmp.push('To renew your RevUp Contract contact support at ');
                sTmp.push('<a href="mailto:support@revup.com">support@revup.com</a>');
            sTmp.push('</div>')
        }
        else {
            sTmp.push('<div class="creditCardInfoDiv">');
                sTmp.push('<form name="creditCardForm" id="creditCardForm" action="" method="post" autocomplete="on">');
                    sTmp.push('<div class="line">')
                        sTmp.push('<div class="entry ccNum">');
                            sTmp.push('<div class="title required ">Credit Card Number</div>');
                            sTmp.push('<input class="textInput" type="text" id="ccNumber" data-recurly="number" placeholder="Card Number"/>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entry  ccImage">');
                            sTmp.push('<div class="title"></div>');
                            sTmp.push('<div class="imgDiv"></div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entry ccExpMonth expDate">');
                            sTmp.push('<div class="title required">Expiration</div>');
                            sTmp.push('<input type="hidden" id="ccMonth" data-recurly="month" value=""/>');
                            sTmp.push('<div class="monthDropDown"></div>')
                        sTmp.push('</div>');
                        sTmp.push('<div class="entry ccExpYear expDate">');
                            sTmp.push('<div class="title required">Year</div>');
                            sTmp.push('<input type="hidden" id="ccYear" data-recurly="year" value=""/>');
                            sTmp.push('<div class="yearDropDown"></div>')
                        sTmp.push('</div>');
                        sTmp.push('<div class="entry ccCVV">');
                            sTmp.push('<div class="title required">CVV</div>');
                            sTmp.push('<input class="textInput" type="text" id="ccCVV" data-recurly="cvv" placeholder="CVV" maxlength="4"/>');
                        sTmp.push('</div>');
                    sTmp.push('</div>')

                    sTmp.push('<div class="line">')
                        sTmp.push('<div class="entry firstLastName">');
                            sTmp.push('<div class="title required">First Name</div>');
                            sTmp.push('<input class="textInput" type="text" id="ccFirstName" data-recurly="first_name" placeholder="First Name" value=""/>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entry firstLastName">');
                            sTmp.push('<div class="title required">Last Name</div>');
                            sTmp.push('<input class="textInput" type="text" id="ccLastName" data-recurly="last_name" placeholder="Last Name" value=""/>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="line">')
                        sTmp.push('<div class="entry address addr">');
                            sTmp.push('<div class="title required">Address 1</div>');
                            sTmp.push('<input class="textInput" type="text" id="ccAddress1" data-recurly="address1" placeholder="Address" value=""/>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="line">')
                        sTmp.push('<div class="entry address2 addr">');
                            sTmp.push('<div class="title">Address 2</div>');
                            sTmp.push('<input class="textInput" type="text" id="ccAddress2" data-recurly="address2" placeholder="Address" value=""/>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="line">')
                        sTmp.push('<div class="entry  city addr">');
                            sTmp.push('<div class="title required">City</div>');
                            sTmp.push('<input class="textInput" type="text" id="ccCity" data-recurly="city" placeholder="City" value=""/>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entry state addr">');
                            sTmp.push('<div class="title required">State</div>');
                            sTmp.push('<input type="hidden" id="ccState" data-recurly="state"  value=""/>');
                            sTmp.push('<div class="stateDropDown"></div>')
                        sTmp.push('</div>');
                        sTmp.push('<div class="entry zip addr">');
                            sTmp.push('<div class="title required">Zip Code</div>');
                            sTmp.push('<input class="textInput" type="text" id="ccPostalCode" data-recurly="postal_code" placeholder="Zipcode" maxlength="5" value=""/>');
                        sTmp.push('</div>');
                    sTmp.push('</div>')
                    sTmp.push('<input type="hidden" data-recurly="country" value="US">');
                    sTmp.push('<input type="hidden" name="recurly-token" data-recurly="token">');
                sTmp.push('</form>');

                sTmp.push('<div class="btnDiv">');
                    sTmp.push('<div class="leftSide">');
                        sTmp.push('<div class="text requiredMsg">Required Fields</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="rightSide">');
                        sTmp.push('<button class="revupBtnDefault creditCardSaveBtn saveBtn disabled">Save</button>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>'); // creditCardInfoDiv
        }
        sTmp.push('</div>'); // creditCardDiv

        $creditCardDlg = $detailsData.html(sTmp.join(''));

        $creditCardSave = $('.rightSide .creditCardSaveBtn', $creditCardDlg);
        $creditCardErrors = $('.errorDiv', $creditCardDlg);

        $detailsLoading.hide();
        $detailsData.show();

        // load the drop downs
        loadDropDowns();

        // load event handlers
        loadEventHandlers()
    }; // displayCreditCardForm

    let r;
    let recurlyLoaded = () => {
        // initialize recurly
        r = recurly.configure(ccTemplate.recurlyKey);

        displayCreditCardForm();
    }; // recurlyLoaded

    return {
        init: function() {
            // load the recurly js file
            revupUtils.removeScript(recurlyJsFile);
            revupUtils.addScript(recurlyJsFile, recurlyLoaded);
        }, // init
    }
};
