var accountSettings = (function () {
    // Globals
    var $settingsLst        = $('.configAnalysis .configAnalysisSection .lstSection');
    var $detailsLoading     = $('.configAnalysis .configAnalysisSection .detailsSection .detailsLoading');
    var $detailsData        = $('.configAnalysis .configAnalysisSection .detailsSection .detailsData');
    var $saveUpadateWait    = $('.configAnalysis .configAnalysisSection .saveUpadateWait');
    var $resultsWrap        = $();
    var $searchForResults   = $('.entitySearchDlg .searchForResults');

    var searchedBefore = 0;
    var timeout = null;

    var currentQuestionSet  = 0;
    var bUpdateConfig       = false;    // true if there is a current value - an updata
    var currentPageFields   = [];       // a list of the active field.  Each entry will have information need to
                                        // get the value of the field, the type of field, ...

    /*
     * Load events and data into fields
     */
    function loadEvents()
    {
        // settings list
        $settingsLst.on('click', '.entry', function(e) {
            var $entry = $(this);
            $detailsData.hide();

            // if current selection done
            if ($entry.hasClass('.selected')) {
                return;
            }

            // see if there is a save button and if enabled
            var $accountSettingsArea = $entry.closest('.configAnalysisSection')
            var $saveBtn = $('.detailsSection .saveBtn', $accountSettingsArea);
            let warnUser = false;
            let warnUserIncompCreditCard = false;

             if ($('.creditCardDiv', $detailsData).length > 0 && ccTemplate.recurlyAccountId) {

                // check if there are entered values from each of the dropdowns on the page
                $( ".revupDropDown .valDiv" ).each(function() {
                    let dropdownIndexes = -2;
                    let valueEntered = $(this).attr('index') != -1;
                    if (valueEntered) {
                        warnUser = true;
                    }
                })

                // check if there are entered values from each of the text fields on the page
                $( ".creditCardDiv .textInput" ).each(function() {
                    let valueEntered =  $(this).val();
                    if (valueEntered) {
                        warnUser = true;
                    }
                })

                if ($saveBtn.hasClass('disabled')) {
                    warnUserIncompCreditCard = true;
                }

                if (!$saveBtn.hasClass('disabled')) {
                    warnUser = true;
                }
             }

            if (warnUser) {
                var msg = "Your settings have not been saved.<br><br>" +
                          "If you wish to save your settings before changing tabs press the <b>Save</b> button. " +
                          "To continue to the next tab without saving press the <b>Continue</b> button. " +
                          "Otherwise press <b>Cancel</b> to stay on the current tab.";

                if (warnUserIncompCreditCard) {
                    var msg = "Your settings have not been saved.<br><br>" +
                        "To continue to the next tab without saving press the <b>Continue</b> button. " +
                        "Otherwise press <b>Cancel</b> to stay on the current tab.";
                }

                function changeFunc() {
                    // clear selections and select the new entry
                    $('.entry', $settingsLst).removeClass('selected');
                    $entry.addClass('selected');

                    // load the details
                    loadDetails($entry);
                };

                $('body').revupDialogBox({
                    headerText: "Setting Not Saved",
                    msg: msg,
                    okBtnText: "Save",
                    enableOkBtn: warnUserIncompCreditCard? false:true,
                    okHandler: function () {
                        var eData = $('.entry.selected', $settingsLst).data();

                        if ($('.creditCardDiv', $detailsData).length == 0) {
                            saveDetails(eData, changeFunc);
                        }

                        else {
                           $('.rightSide .creditCardSaveBtn').trigger('click');
                        }

                    },
                    bDisplayContinueBtn: true,
                    continueHandler: function () {
                        changeFunc();
                    },
                    cancelHandler: function () {
                        $detailsData.show();
                    }
                });
                return;
            }

            // clear selections and select the new entry
            $('.entry', $settingsLst).removeClass('selected');
            $entry.addClass('selected');

            $detailsData.hide();
            $detailsLoading.show();

            // load the details
            loadDetails($entry);
        })
    } // loadEvents

    function loadFieldLstData(data)
    {
        for(var k in data) {
            var $entry = $(".entry[questionSetId=" + k + "]", $settingsLst);
            $entry.data('fields', $.parseJSON(data[k].fields));
            $entry.data('readUrl', data[k].readUrl);
            $entry.data('readDetailUrl', data[k].readDetailsUrl);
            $entry.data('createUrl', data[k].createUrl);
            $entry.data('updateUrl', data[k].updateUrl);
            $entry.data('deleteUrl', data[k].deleteUrl);
            $entry.data('allowMultiple', (data[k].allowMultiple.toLowerCase() == 'True'));
            $entry.data('title', data[k].title);
        }
    } // loadFieldLstData

    /*
     * Detail Section
     */
    function loadDetailsUnknown(msg)
    {
        var sTmp = [];

        sTmp.push('<div class="detailsErrorSection">');
            sTmp.push('<div class="errorMsg">' + msg + '</div>');
//            sTmp.push('<div class="errorMsg">Reload page and try again</div>');
        sTmp.push('</div>');
        $detailsData.html(sTmp.join(''));

        $detailsLoading.hide();
        $detailsData.show();
    }; // loadDetailsUnknown

    function initDetails(eData)
    {
        for (var i = 0; i < currentPageFields.length; i++) {
            if (currentPageFields[i].fnInitField) {
                currentPageFields[i].fnInitField(currentPageFields[i]);
            }
            else {
                console.error('addDetinitDetailsailsControllersAndHandler - undefined init method: ' + currentPageFields[i].fieldDisplayType);
            }
        }

        // add the save handler
        $('.saveBtn', $detailsData).on('click', function (e) {
            if ($(this).hasClass('disabled')) {
                return;
            }

            saveDetails(eData);

            // trigger status checking
            updateRevupAlert()
        });
    }; // initDetails

    function enableDisableSaveBtn()
    {
        var bEnable = true;
        for (var i = 0; i < currentPageFields.length; i++) {
            if (currentPageFields[i].fnEnableDisableSaveHandler) {
                if (!currentPageFields[i].fnEnableDisableSaveHandler(currentPageFields[i])) {
                    bEnable = false;
                }
            }
            else {
                console.error('enableDisableSaveBtn - undefined enableDisable method: ' + currentPageFields[i].fieldDisplayType);
            }

            if (!bEnable) {
                break;
            }
        }

        if (bEnable) {
            $('.saveBtn', $detailsData).removeClass('disabled');
        }
        else {
            $('.saveBtn', $detailsData).addClass('disabled');
        }
    } // enableDisableSaveBtn

    function loadCurrentValues(eData)
    {
        $.get(eData.readUrl)
            .done(function(r) {
                $detailsData.hide();
                $detailsLoading.show();
                if (r.count == 0) {
                    bUpdateConfig = false;
                }
                else {
                    bUpdateConfig = true

                    // save the question set id and number
                    var cData = r.results[0].data;
                    $detailsData.attr("questionSet", r.results[0].question_set);
                    $detailsData.attr("questionSetId", r.results[0].id);

                    // get the data to load
                    for(var key in cData) {
                        var bFound = false;
                        for (var i = 0; i < currentPageFields.length; i++) {
                            if (key == currentPageFields[i].fieldName) {
                                var val = cData[key];

                                // load the value
                                if (currentPageFields[i].fnLoadValue) {
                                    currentPageFields[i].fnLoadValue(currentPageFields[i], val);
                                }
                                else {
                                    console.error('loadCurrentValues - undefined load method: ' + currentPageFields[i].fieldDisplayType);
                                }

                                bFound = true;
                                break;
                            }
                        }

                        if (!bFound) {
                            console.error('Loading Data - field not found: ' + key);
                        }
                    }
                }

                // disable the save button until something changes
                $('.saveBtn', $detailsData).addClass('disabled');
                // enableDisableSaveBtn();
                $detailsLoading.hide();
                $detailsData.show();
            })
            .fail(function(r) {
                bUpdateConfig = false;

                revupUtils.apiError('Analysis Configuration', r, 'Unable to load analysis config', "Unable to load analysis configuration at this time.");
            })
            .always(function (r) {
            })
    }; // loadCurrentValues

    function saveDetails(eData, doneFunc)
    {
        // build the json to save/update
        var jObj = {};

        // display the wait overlay
        $saveUpadateWait.show();

        // walk the fields and see what needs to be done
        var questionSetId = '';
        if (saveFunction) {
            questionSetId = saveFunction(eData, jObj);

            // see if this should be a delete, ranges is empty
            if (jObj.ranges && jObj.ranges.length == 0) {
                // if there is a id tile to delete
                if (jObj.id) {
                    $.ajax({
                        url: eData.deleteUrl.replace("questionSetDataId", jObj.id),
                        type: "DELETE"
                    })
                    .done(function(r) {
                        var title = "Deleted";
                        var msg = "Analysis Configuration Deleted";
                        $('body').revupMessageBox({
                            headerText: title,
                            msg: msg
                        });
                    })
                    .fail(function(r) {
                        $entry.revupMessageBox({
                            headerText: "Error - Unable to delete analysis config",
                            msg: "Unable to delete analysis config \"" + title + "\".  Please try again later",
                        })
                    })

                }

                // set the flag to update
                bUpdateConfig = true;

                // disable the save button
                $('.saveBtn', $detailsData).addClass('disabled');

                return;
            }
        }
        else {
            questionSetId = $detailsData.attr("questionSetId");
            for (var i = 0; i < currentPageFields.length; i++) {
                if (currentPageFields[i].fnGetValue) {
                    currentPageFields[i].fnGetValue(currentPageFields[i], jObj)
                }
                else {
                    console.error('saveDetails - undefined save method: ' + currentPageFields[i].fieldDisplayType);
                }
            }
        }

        // build the url
        var url = "";
        var ajaxType = "POST";
        if (bUpdateConfig) {
            url = eData.updateUrl.replace("questionSetDataId", questionSetId);
            ajaxType = "PUT";
        }
        else {
            url = eData.createUrl.replace("/questionSetDataId", "");
            ajaxType = "POST";
        }
        $.ajax({
            type: ajaxType,
            url: url,
            data: JSON.stringify(jObj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        })
        .done(function(r) {
            var title = "Saved";
            var msg = "Analysis Configuration Saved";
            if (bUpdateConfig) {
                title = "Updated";
                msg = "Analysis Configuration Updated"
            }

            $('body').revupMessageBox({
                headerText: title,
                msg: msg
            });

            // set the flag to update
            bUpdateConfig = true;

            // save the updated id
            if (r.id) {
                $detailsData.attr("questionSetId", r.id);
            }

            // disable the save button
            $('.saveBtn', $detailsData).addClass('disabled');

            // see if there is a function to call after done
            if (doneFunc != undefined) {
                doneFunc();
            }
        })
        .fail(function(r) {
            revupUtils.apiError('Analysis Configuration', r, 'Unable to save analysis config', "Unable to save analysis configuration at this time.");
        })
        .always(function(r) {
            // hide the wait overlay
            $saveUpadateWait.hide();
        })
    } // saveDetails

    var saveFunction = undefined;
    function loadDetails($entry)
    {
        // clear the data
        $detailsData.html("");

        // initialize active fields list
        currentPageFields = [];

        if ($entry.hasClass('editBillingDetails')) {
            editBillingInfo(ccTemplate).init();
        }

        else {

            // walk the fields and build the details section
            var eData = $entry.data();
            if ((eData == undefined) || ($.isEmptyObject(eData))) {
                loadDetailsUnknown('No Configuration Items Found');
                return;
            }

            // add the title
            $detailsData.html('<div class="title">' + eData.title + '</div>');

            // most types require save button and display
            var bDisplayFieldsAndSave = true;
            var bLoadData = true;

            var f = eData.fields;
            var dropdownChoicesLst = [];
            var bLoadCurrentValue = true;
            saveFunction = undefined;
            for (var i = 0; i < f.length; i++) {
                switch (f[i].type) {
                    case "contrib_range":
                        loadContribRange(eData, f[i]);
                        saveFunction = contribRangeSave;
                        bLoadCurrentValue = false;
                        break;
                    case "entity_search":
                        entitySearch.display(eData, f[i]);
                        bDisplayFieldsAndSave = false;
                        bLoadData = false;
                        break;
                    case "date":
                        loadDetailsDate(eData, f[i]);
                        break;
                    case "unlimited-or-integer":
                        loadDetailsInteger(eData, f[i], true);
                        break;
                    case "text":
                        loadDetailsText(eData, f[i]);
                        break;
                    case "float":
                        loadDetailsText(eData, f[i]);
                        break;
                    case "dropdown":
                        loadDetailsDropdown(eData, f[i]);
                        if (f[i].choice_key != undefined) {
                            // if the questionSet data has a field that is a dropdown, we save it's index in dropdownChoicesLst
                            dropdownChoicesLst.push(i);
                        }
                        break;
                    case "list":
                        loadDetailsList(eData, f[i]);
                        break;
                    case "boolean":
                        loadDetailsBoolean(eData, f[i]);
                        break;
                    default:
                        //loadDetailsUnknown("Unknown Analysis Config type \"" + f[i].type + "\"");
                        console.error("Unknown Analysis Config type: ", f[i]);
                        break;
                } // end switch - type of details
            }

            // update field for choice key
            if (dropdownChoicesLst.length > 0) {
                for (var i = 0; i < dropdownChoicesLst.length; i++) {
                    // find where in the data is the dropdown we want to display, and give it a changeFieldIndex
                    var fieldName = currentPageFields[dropdownChoicesLst[i]].choiceKey;
                    for (var a = 0; a < currentPageFields.length; a++) {
                        if (fieldName == currentPageFields[a].fieldName) {
                            currentPageFields[a].changeFieldIndex = dropdownChoicesLst[i];
                        }
                    }
                }
            }

            // see if there are any required fields
            var bDisplayRequiredMsg = false;
            for (var a = 0; a < currentPageFields.length; a++) {
                if (currentPageFields[a].bRequired) {
                    bDisplayRequiredMsg = true;
                }
            }

            // display the save button and look the stored value
            if (bDisplayFieldsAndSave) {
                // add the save button
                var sTmp = [];
                sTmp.push('<div class="btnDiv">');
                if (bDisplayRequiredMsg) {
                    sTmp.push('<div class="leftSide">');
                    sTmp.push('<div class="text requiredMsg">Required Fields</div>');
                    sTmp.push('</div>');
                }
                sTmp.push('<div class="rightSide">');
                sTmp.push('<button class="revupBtnDefault saveBtn disabled">Save</button>');
                sTmp.push('</div>');
                sTmp.push('</div>');
                var $entitySearch = $detailsData.append(sTmp.join(''));

                // add controllers and event handlers
                initDetails(eData);

                // set the choices for the dropdown
                if (dropdownChoicesLst.length > 0) { // if there are dropdowns in this page to be loaded
                    for (var i = 0; i < currentPageFields.length; i++) { // find this dropdown in the data
                        if (currentPageFields[i].choiceKey != undefined) {
                            var choiceKey = currentPageFields[i].choiceKey;
                            for (var j = 0; j < currentPageFields.length; j++) {
                                // if the data contains a key that matches a key of a dropdown field we want to display on this page
                                if (currentPageFields[j].fieldName == choiceKey) {
                                    // start loading the dropdown's values by calling revup dropdown's widget
                                    var choiceKeyValue = currentPageFields[j].$dropdown.revupDropDownField('getValue');
                                    if (currentPageFields[i].lstValues[choiceKeyValue] != undefined) {
                                        currentPageFields[i].$dropdown.revupDropDownField({
                                            value: currentPageFields[i].lstValues[choiceKeyValue],
                                            valueStartIndex: 0,
                                            updateValueList: true,
                                        })
                                    }
                                }
                            }
                        }
                    }
                }

                // load the current values
                if (bLoadCurrentValue) {
                    loadCurrentValues(eData);
                }
            }
        }
    } // loadDetails

    function formatSearchResult(result)
    {
        var sTmp = [];

        // sTmp.push('<div class="entry" eId="' + result.eid + '" recordSetConfig="' + result.record_set_config + '">');
        sTmp.push('<div class="entryDetail">');
            var party = result.party;
            if ((party == undefined) || (party == "")) {
                party = '';
            }
            partyColor = "";
            if (party.startsWith('rep', true)) {
                partyColor = " political-spectrum-bar-republican";
            }
            else if (party.startsWith('dem', true)) {
                partyColor = " political-spectrum-bar-democrat";
            }
            else if (party != '') {
                partyColor = " political-spectrum-bar-neutral";
            }

            sTmp.push('<div class="partySection">');
                sTmp.push('<div class="partyColorBlob ' + partyColor + '"></div>');
            sTmp.push('</div>');

            sTmp.push('<div class="detailSection">');
                sTmp.push('<div class="boldLine">');
                    // name
                    sTmp.push('<div class="name">' + result.name + '</div>');

                    // position
                    var office = result.office;
                    if ((office == undefined) || (office == "")) {
                        office = '';
                    }
                    sTmp.push('<div class="position">' + office + '</div>');
                sTmp.push('</div>');

                sTmp.push('<div class="normalLine">');
                    // party
                    sTmp.push('<div class="party">' + party + '</div>');

                    // district
                    var district = result.district;
                    if ((district == undefined) || (district == "")) {
                        district = '-';
                    }
                    else {
                        district = 'District: ' + district;
                    }
                    sTmp.push('<div class="district">' + district + '</div>');

                    // city
                    var city = result.city;
                    if ((city == undefined) || (city == "")) {
                        city = '-';
                    }
                    sTmp.push('<div class="city">' + city + '</div>');

                    // state
                    var state = result.state;
                    if ((state == undefined) || (state == "")) {
                        state = '-';
                    }
                    sTmp.push('<div class="state">' + state + '</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            sTmp.push('<div class="addBtnSection">');
                var addImg = '<div class="addBtn"><div class="text">Add</div></div>';
                var addCheckmark = '<div class="icon icon-check"></div>';
                var noSearchResults = '<div class="searchForNoResults">No Results</div>';

                var $searchForResults = $('.entitySearchDlg .searchForResults');
                var $searchForWait = $('.entitySearchDlg .searchForWait');
                var $matchesLst = $('.entitySearchDlg .matchesDiv .lst');
                var $okBtn = $('.entitySearchDlg .okBtn');
                var $candidateLabel = $('.entitySearchDlg .innerResults .candidateLabelDiv .candidateLabel');
                var $pagination = $('.entitySearchDlg .searchForPagination');

                $('.entry', $matchesLst).each(function() {
                    var $entry = $(this);
                    var matchEId = $entry.attr('eId');
                    var matchRecSetConfig = $entry.attr('recordSetConfig');

                    if ((matchEId == result.eid) && (matchRecSetConfig == result.record_set_config)) {
                        addImg = addCheckmark
                    }
                })
                    sTmp.push(addImg);
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        return sTmp.join('');
        $('.normalLine .district').tooltipOnOverflow();
    }; // formatSearchResult

    /*
     * Information Display Options
     */
    var infoDisplayOptionId = '';
    function loadContribRange(eData, fieldData)
    {
        function addContribRangeEntry(floorVal, ceilingVal, bDisplayLabel, displayLabelVal, bFirst, bLast)
        {
            var sTmp = [];

            sTmp.push('<div class="contribRangeEntry">');
                sTmp.push('<div class="line">');
                    sTmp.push('<div class="text">Between</div>')
                    if (bFirst) {
                        sTmp.push('<div class="text spaceBefore">$</div>');
                        sTmp.push('<div class="text floor">0</div>');
                    }
                    else {
                        if (!floorVal) {
                            floorVal = '';
                        }
                        sTmp.push('<div class="text spaceBefore">$</div>');
                        //sTmp.push('<input type="text" class="floor commaNum" value="' + floorVal + '">');
                        sTmp.push('<div class="text floor">' + floorVal + '</div>');
                    }
                    sTmp.push('<div class="text spaceBefore spaceAfter">and</div>');
                    if (!ceilingVal) {
                        ceilingVal = ''
                    }
                    var ceilingDisabled = '';
                    var checkboxIcon = "icon-box-unchecked";
                    if (!ceilingVal) {
                        ceilingDisabled = ' disabled';
                        checkboxIcon = "icon-box-checked";
                    }
                    sTmp.push('<div class="text spaceBefore ceilingLabel' + ceilingDisabled + '">$</div>');
                    sTmp.push('<input type="text" class="ceiling commaNum" ' + ceilingDisabled + ' value="' + ceilingVal +'">');
                    var s = ' style="display:none;"'
                    if (bLast) {
                        s = '';
                    }
                    sTmp.push('<div class="checkbox infiniteAmount icon ' + checkboxIcon + '"' + s + '></div>');
                    sTmp.push('<div class="text"' + s + '>Infinite</div>');

                    // add the close button if not the first entry
                    if (!bFirst) {
                        sTmp.push('<div class="deleteEntry icon icon-delete-circle"></div>');
                    }
                sTmp.push('</div>');

                if (!displayLabelVal) {
                    displayLabelVal = ''
                }
                var displayAmountIcon = 'icon-radio-button-off';
                var displayLabelIcon = 'icon-radio-button-on';
                var labelDisabled = '';
                if (bDisplayLabel) {
                    displayLabelIcon = 'icon-radio-button-off';
                    displayAmountIcon = 'icon-radio-button-on';

                    labelDisabled = ' disabled';
                    displayLabelVal = '';
                }
                sTmp.push('<div class="line">');
                    sTmp.push('<div class="radioDisplay displayAmountBtn icon ' + displayAmountIcon + '"></div>');
                    sTmp.push('<div class="text spaceBefore spaceAfterLarge">Display amount</div>');
                    sTmp.push('<div class="radioDisplay displayLabelBtn icon ' + displayLabelIcon + '"></div>')
                    sTmp.push('<div class="text spaceBefore spaceAfter">Use Label</div>');
                    sTmp.push('<input type="text" class="displayLabel" value="' + displayLabelVal +'"' + labelDisabled + '>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            return sTmp.join('');
        } // addContribRangeEntry

        function checkStatus() {
            // enter a ceiling or floor value
            var bEnable = true;

            $('.contribRangeEntry', $detailsData).each(function () {
                var $entry = $(this);

                // floor value
                var fVal = $('.floor', $entry).text();
                if (fVal != '') {
                    fVal = fVal.replace(',', '');
                    fVal = parseInt(fVal, 10);
                }
                else {
                    fVal = 0;
                }

                // ceiling
                if (($('.ceiling', $entry).val() == '') && !($('.infiniteAmount', $entry).is(':visible'))) {
                    bEnable = false;

                    return false;
                }
                var cVal = $('.ceiling', $entry).val();
                cVal = cVal.replace(',', '');
                cVal = parseInt(cVal, 10);

                // make sure the floor is less then the ceiling
                if (fVal >= cVal) {
                    bEnable = false;

                    return false;
                }

                // if use label make sure there is a label
                if ($('.displayLabelBtn', $entry).hasClass('icon-radio-button-on')) {
                    if ($('.displayLabel', $entry).val() == '') {
                        bEnable = false;

                        return false;
                    }
                }
            })

            if (bEnable) {
                $('.saveBtn', $detailsData).removeClass('disabled');
            }
            else {
                $('.saveBtn', $detailsData).addClass('disabled');
            }
        } // checkStatus

        // get data
        var sTmp = [];

        // get the data
        $.get(eData.readUrl)
            .done(function(r) {
                sTmp.push('<div class="detailsDataInner detailsContribRange">');
                    sTmp.push('<div class="enableDisableCheckDiv">');
                        sTmp.push('<div class="enableDisableCheckbox icon icon-box-checked"></div>')
                        sTmp.push('<div class="text">Display large donation amounts to Volunteers as labels</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="contribRangeEntrySection">');
                        if (r.count == 0) {
                            infoDisplayOptionId = '';
                            bUpdateConfig = false;
                            sTmp.push(addContribRangeEntry(undefined, undefined, undefined, undefined, true, true))
                        }
                        else {
                            infoDisplayOptionId = r.results[0].id;
                            bUpdateConfig = true;
                            var ranges = r.results[0].data.ranges;
                            var len = ranges.length;
                            for (var i = 0; i < len; i++) {
                                sTmp.push(addContribRangeEntry(ranges[i].floor, ranges[i].ceiling,
                                          ranges[i].display_amount, ranges[i].label, i == 0, (len == (i + 1))));
                            }
                        }
                    sTmp.push('</div>');

                    /*
                    sTmp.push('<div class="addBtnDiv">');
                        sTmp.push('<div class="addBtn">');
                            sTmp.push('<div class="btnText">Add</div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                    */
                sTmp.push('</div>');

                // display
                var $configRange = $('.title', $detailsData).after(sTmp.join(''));

                // event handlers
                // checkbox
                $('.enableDisableCheckbox', $detailsData).on('click', function(e) {
                    var $btn = $(this);

                    if ($btn.hasClass('icon-box-checked')) {
                        $btn.removeClass('icon-box-checked')
                            .addClass('icon-box-unchecked');

                        // delete the entries and reset the first one
                        $('.contribRangeEntrySection .contribRangeEntry:not(:first)', $detailsData).remove();

                        // clear out the first entry
                        var $fEntry = $('.contribRangeEntrySection .contribRangeEntry:first', $detailsData)
                        $('.ceilingLabel', $fEntry).addClass('disabled');
                        $('.ceiling', $fEntry).prop('disabled', true)
                                             .val('');
                        $('.infiniteAmount', $fEntry).show()
                                                     .removeClass('icon-box-unchecked')
                                                     .addClass('icon-box-checked');
                        $('.infiniteAmount', $fEntry).next().show();
                        $('.displayAmountBtn', $fEntry).removeClass('icon-radio-button-on')
                                                       .addClass('icon-radio-button-off');
                        $('.displayLabelBtn', $fEntry).removeClass('icon-radio-button-off')
                                                      .addClass('icon-radio-button-on');
                        $('.displayLabel', $fEntry).prop('disabled', false)
                                                   .val('');

                        // disable
                        $('.contribRangeEntrySection', $detailsData).addClass('disabled');

                        // enable the save button
                        $('.saveBtn', $detailsData).removeClass('disabled');
                    }
                    else {
                        $btn.addClass('icon-box-checked')
                            .removeClass('icon-box-unchecked');

                        // disable
                        $('.contribRangeEntrySection', $detailsData).removeClass('disabled');

                        // disable the save button
                        $('.saveBtn', $detailsData).addClass('disabled');

                    }
                })

                // add another entry
                $('.addBtn', $detailsData).on('click', function(e) {
                    // get the ceiling value for the last entry before adding the new entry
                    var $lastEntry = $('.contribRangeEntrySection .contribRangeEntry', $detailsData).last();
                    var ceilingVal = $('.ceiling', $lastEntry).val();
                    if (ceilingVal != '') {
                        ceilingVal = ceilingVal.replace(',', '')
                        ceilingVal = parseInt(ceilingVal, 10);
                        ceilingVal += 1;
                    }

                    var newEntry = addContribRangeEntry(ceilingVal);
                    $('.contribRangeEntrySection', $detailsData).append(newEntry);

                    // disable the save button
                    $('.saveBtn', $detailsData).addClass('disabled');
                })

                $detailsData
                    // delete an entry
                    .on('click', '.deleteEntry', function(e) {
                        var $entry = $(this).closest('.contribRangeEntry')
                        var $beforeEntry = $entry.prev();
                        var $afterEntry = $entry.next();

                        // see if the last entry
                        if ($afterEntry.length == 0) {
                            // display the infinite
                            $('.infiniteAmount', $beforeEntry).removeClass('icon-box-unchecked')
                                                              .addClass('icon-box-checked')
                                                              .show()
                                                              .next().show();

                            $('.ceiling', $beforeEntry).val('')
                                                       .prop('disabled', true);
                            $('.ceilingLabel', $beforeEntry).addClass('disabled');
                        }
                        else {
                            var c = $('.ceiling', $beforeEntry).val();
                            if (c != '') {
                                var c = c.replace(',', '');
                                c = parseInt(c, 10) + 1;
                                c = revupUtils.commify(c);
                                $('.floor', $afterEntry).text(c);
                            }
                            else {
                                $('.floor', $afterEntry).text('0');
                            }
                        }

                        // remove the current entry
                        $entry.remove();

                        // check status
                        checkStatus();
                    })
                    .on('keyup paste', '.ceiling, .displayLabel', function(e) {
                        var $field = $(this);
                        var $entry = $field.closest('.contribRangeEntry');
                        var $nextEntry = $entry.next();

                        // skip the arrow keys
                        if (e.which >= 37 && e.which <= 40) {
                            e.preventDefault();
                        }

                        // get the current value
                        var v = $('.ceiling', $entry).val();
                        v = v.replace(',', '');

                        // commify and return
                        $('.ceiling', $entry).val(v.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ","));

                        // set the next floor value
                        if (v == '') {
                            v = '0';
                        }
                        v = parseInt(v, 10);
                        v += 1;
                        v = revupUtils.commify(v);
                        $('.floor', $nextEntry).text(v);

                        checkStatus();
                    })
                    .on('click', '.infiniteAmount', function(e) {
                        // check/uncheck infinite amount
                        var $btn = $(this);
                        var $entry = $btn.closest('.contribRangeEntry');

                        // see if checked
                        var $cLabel = $('.ceilingLabel', $entry);
                        var $cEdit = $('.ceiling', $entry);
                        if ($btn.hasClass('icon-box-checked')) {
                            var $label = $btn.next();
                            $btn.hide();
                            $label.hide();

                            $cLabel.removeClass('disabled');
                            $cEdit.prop('disabled', false);

                            // add the new entry
                            var fVal = $cEdit.val();
                            if (fVal == '') {
                                fVal = 1;
                            }
                            var newEntry = addContribRangeEntry(fVal, undefined, undefined, undefined, false, true);
                            $entry.after(newEntry);
                        }
                        /*
                        else {
                            $btn.removeClass('icon-box-unchecked')
                                .addClass('icon-box-checked');

                            $cLabel.addClass('disabled');
                            $cEdit.prop('disabled', true)
                                  .val('');
                        }
                        */

                        // check status
                        checkStatus();
                    })
                    .on('click', '.radioDisplay', function(e) {
                        var $rBtn = $(this);
                        var $entry = $rBtn.closest('.contribRangeEntry');

                        // clear the bottons
                        $('.radioDisplay ', $entry).removeClass('icon-radio-button-on')
                                                   .addClass('icon-radio-button-off');

                        // set the clicked on button
                        $rBtn.removeClass('icon-radio-button-off')
                             .addClass('icon-radio-button-on');

                        // see if the label edit enable
                        var $eEditLabel = $('.displayLabel', $entry);
                        if ($rBtn.hasClass('displayLabelBtn')) {
                            $eEditLabel.prop('disabled', false);
                        }
                        else {
                            $eEditLabel.prop('disabled', true)
                                       .val('');
                        }

                        // check status
                        checkStatus();
                    });
            })
            .fail(function(r) {
                var sTmp = [];

                sTmp.push('<div class="detailsErrorSection">');
                    sTmp.push('<div class="errorMsg">Unable to load entity search data</div>');
                    sTmp.push('<div class="errorMsg">Please try back later by reloading this page</div>');
                sTmp.push('</div>');
                $detailsData.html(sTmp.join(''));

                revupUtils.apiError('Analysis Configuration', r, 'Unable to load entity search data');
            })
            .always(function(r) {
                // hide waiting and display data
                $detailsData.show();
                $detailsLoading.hide();
            });
    } // loadContribRange

    function contribRangeSave(eData, jObj)
    {
        jObj.ranges = [];
        var tmp = [];
        var lastCeiling = 0;

        if (infoDisplayOptionId) {
            jObj.id = infoDisplayOptionId;
        }

        // make sure the display large donation is checked
        if ($('.enableDisableCheckbox', $detailsData).hasClass('icon-box-checked')) {
            $('.contribRangeEntrySection .contribRangeEntry', $detailsData).each(function() {
                var $entry = $(this);
                var obj = new Object();

                // floor
                var floor = $('.floor', $entry).text();
                floor = floor.replace(',', '');
                floor = parseInt(floor, 10);
                obj.floor = floor;

                // ceiling
                v = $('.ceiling', $entry).val();
                if (v != '') {
                    v = v.replace(',', '');
                    obj.ceiling = parseInt(v, 10);

                    lastCeiling = obj.ceiling;
                }

                // label
                if ($('.displayLabelBtn', $entry).hasClass('icon-radio-button-on')) {
                    v = $('.displayLabel', $entry).val();

                    obj.display_amount = false;
                    obj.label = v;
                }
                else {
                    obj.display_amount = true;
                }

                // save the entry
                jObj.ranges.push(obj);
            })
        }

        return infoDisplayOptionId;
    } // contribRangeSave

    /*
     *
     *  load details for each of the supported field types
     *      NOTE:   each field needs to build the currentPageFields object
     *              which has the follow fields
     *                  fieldType               - return type
     *                  fieldName               - field name
     *                  fieldDisplayType        - display type
     *                  bRequired               - if true required (default true)
     *                  $cntl                   - selector to the field
     *                  fnInitField             - function to init the field
     *                  fnEnableDisableSaveHandler - function that should the save button be enable or disabled
     *                  fnLoadValue             - function to load the value from the back end
     *                  fnGetValue              - function to get value as part of json blob
     *
     *                  The can also be other fields specific to the type
     *
     *                  the object is added to the activeField2 array
     *
     */
    function loadDetailsText(eData, fieldData)
    {
        // create the active field for this field
        var fObj = new Object();
        fObj.fieldType = fieldData.expected[0].type;
        fObj.fieldName = fieldData.expected[0].field;
        fObj.fieldDisplayType = fieldData.type;
        fObj.bRequired = true;
        if (fieldData.required != undefined) {
            fObj.bRequired = fieldData.required;
        }

        fObj.fnInitField = initFieldText;
        fObj.fnEnableDisableSaveHandler = enableDisableText;
        fObj.fnLoadValue = loadValueText;
        fObj.fnGetValue = getValueText;
        currentPageFields.push(fObj);

        // get data
        var sTmp = [];
        var labelExClass = "";
        if (fObj.bRequired) {
            labelExClass += " required";
        }
        sTmp.push('<div class="fieldEntry textField">');
            sTmp.push('<div class="labelSideDiv">');
                sTmp.push('<div class="label' + labelExClass + '" title="' + fieldData.label + '">' + fieldData.label + '</div>');
            sTmp.push('</div>');

            sTmp.push('<div tabindex="6" class="fieldSideDiv">');
                sTmp.push('<div class="fieldDiv textFieldDiv" fieldType="' + fieldData.expected[0].type + '" fieldName="' + fieldData.expected[0].field + '"  fieldDisplayType="' + fieldData.type + '">');
                    sTmp.push('<input class="textInput" type="text" placeholder="' + (fieldData.placeholder ? fieldData.placeholder : "") + '">');
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        // add to the display
        var $entitySearch = $detailsData.append(sTmp.join(''));
        fObj.$cntl = $('.fieldDiv[fieldName="' + fieldData.expected[0].field + '"]', $detailsData);
    } // loadDetailsText

    function loadDetailsBoolean(eData, fieldData)
    {
        // create the active field for this field
        var fObj = new Object();
        fObj.fieldType = fieldData.expected[0].type;
        fObj.fieldName = fieldData.expected[0].field;
        fObj.fieldDisplayType = fieldData.type;
        fObj.bRequired = true;
        if (fieldData.required != undefined) {
            fObj.bRequired = fieldData.required;
        }

        fObj.fnInitField = initFieldBoolean;
        fObj.fnEnableDisableSaveHandler = enableDisableBoolean;
        fObj.fnLoadValue = loadValueBoolean;
        fObj.fnGetValue = getValueBoolean;
        currentPageFields.push(fObj);

        // get data
        var sTmp = [];
        var labelExClass = "";
        if (fObj.bRequired) {
            labelExClass += " required";
        }
        sTmp.push('<div class="fieldEntry booleanField">');
            sTmp.push('<div class="labelSideDiv">');
                sTmp.push('<div class="label' + labelExClass + '">' + fieldData.label + '</div>');
            sTmp.push('</div>');

            sTmp.push('<div class="fieldSideDiv">');
                sTmp.push('<div class="fieldDiv booleanFieldDiv" fieldType="' + fieldData.expected[0].type + '" fieldName="' + fieldData.expected[0].field + '"  fieldDisplayType="' + fieldData.type + '">');
                    sTmp.push('<input class="booleanCheckbox" type="checkbox">');
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        // add to the display
        var $entitySearch = $detailsData.append(sTmp.join(''));
        fObj.$cntl = $('.fieldDiv[fieldName="' + fieldData.expected[0].field + '"]', $detailsData);
    } // loadDetailsBoolean

    function loadDetailsDate(eData, fieldData)
    {
        // create the active field for this field
        var fObj = new Object();
        fObj.fieldType = fieldData.expected[0].type;
        fObj.fieldName = fieldData.expected[0].field;
        fObj.fieldDisplayType = fieldData.type;
        fObj.bRequired = true;
        if (fieldData.required != undefined) {
            fObj.bRequired = fieldData.required;
        }

        fObj.fnInitField = initFieldDate;
        fObj.fnEnableDisableSaveHandler = enableDisableDate;
        fObj.fnLoadValue = loadValueDate;
        fObj.fnGetValue = getValueDate;
        currentPageFields.push(fObj);

        // get data
        var sTmp = [];
        var labelExClass = "";
        if (fObj.bRequired) {
            labelExClass += " required";
        }
        sTmp.push('<div class="fieldEntry dateField">');
            sTmp.push('<div class="labelSideDiv">');
                sTmp.push('<div class="label' + labelExClass + '">' + fieldData.label + '</div>');
            sTmp.push('</div>');

            sTmp.push('<div class="fieldSideDiv">');
                sTmp.push('<div class="fieldDiv dateFieldDiv" fieldType="' + fieldData.expected[0].type + '" fieldName="' + fieldData.expected[0].field + '"  fieldDisplayType="' + fieldData.type + '">');
                    sTmp.push('<input class="dateInput" placeholder="mm/dd/yyyy" type="text">');
                    sTmp.push('<div class="dateInputIcon">');
                        sTmp.push('<div class="icon icon-calendar"></div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        // add to the display
        var $entitySearch = $detailsData.append(sTmp.join(''));
        fObj.$cntl = $('.fieldDiv[fieldName="' + fieldData.expected[0].field + '"]', $detailsData)
    }; // loadDetailsDate

    function loadDetailsInteger(eData, fieldData, bDisplayUnlimited)
    {
        // field type and name
        var fObj = new Object();
        fObj.fieldType = fieldData.expected[0].type;
        fObj.fieldName = fieldData.expected[0].field;
        fObj.fieldDisplayType = fieldData.type;
        fObj.bRequired = true;
        if (fieldData.required != undefined) {
            fObj.bRequired = fieldData.required;
        }

        // field functions
        fObj.fnInitField = initFieldInteger;
        fObj.fnEnableDisableSaveHandler = enableDisableInteger;
        fObj.fnLoadValue = loadValueInteger;
        fObj.fnGetValue = getValueInteger;

        // field extra
        fObj.bDisplayUnlimited = bDisplayUnlimited;
        currentPageFields.push(fObj);

        // set the default
        if (bDisplayUnlimited == undefined) {
            bDisplayUnlimited = false;
        }

        // set field type
        var fieldDisplayType = bDisplayUnlimited ? "intUnlimitedField" : "intField";

        // get data
        var sTmp = [];
        var labelExClass = "";
        if (fObj.bRequired) {
            labelExClass += " required";
        }
        sTmp.push('<div class="fieldEntry ' + fieldDisplayType + '">');
            sTmp.push('<div class="labelSideDiv">');
                sTmp.push('<div class="label' + labelExClass + '">' + fieldData.label + '</div>');
            sTmp.push('</div>');
            sTmp.push('<div class="fieldSideDiv">');
                sTmp.push('<div class="fieldDiv ' + fieldDisplayType + '" fieldType="' + fieldData.expected[0].type + '" fieldName="' + fieldData.expected[0].field + '"  fieldDisplayType="' + fieldData.type + '">');
                    if (bDisplayUnlimited) {
                        sTmp.push('<div class="intUnlimitedDropDown"></div>');
                    }
                    sTmp.push('<input class="intInput" type="text" placeholder="0">')
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        // add to the display
        var $entitySearch = $detailsData.append(sTmp.join(''));
        fObj.$cntl = $('.fieldDiv[fieldName="' + fieldData.expected[0].field + '"]', $detailsData)
    } // loadDetailsInteger


    function loadDetailsDropdown(eData, fieldData)
    {
        // create the active field for this field
        var fObj = new Object();
        fObj.fieldType = fieldData.expected[0].type;
        fObj.fieldName = fieldData.expected[0].field;
        fObj.fieldDisplayType = fieldData.type;
        fObj.bRequired = true;
        if (fieldData.required != undefined) {
            fObj.bRequired = fieldData.required;
        }

        fObj.fnInitField = initFieldDropdown;
        fObj.fnEnableDisableSaveHandler = enableDisableDropdown;
        fObj.fnLoadValue = loadValueDropdown;
        fObj.fnGetValue = getValueDropdown;

        // custom fields
        fObj.lstValues = fieldData.choices;
        fObj.bSortLst = false;
        if (fieldData.expected[0].sort_choices != undefined) {
            fObj.bSortLst = fieldData.expected[0].sort_choices;
        }
        fObj.bNewValue = false;
        if (fieldData.add_new_value != undefined) {
            fObj.bNewValue = fieldData.add_new_value;
        }
        fObj.bAddClear = false;
        if (fieldData.add_clear != undefined) {
            fObj.bAddClear = fieldData.add_clear;
        }
        if (fieldData.choice_key != undefined) {
            fObj.choiceKey = fieldData.choice_key;
        }
        currentPageFields.push(fObj);

        // get data
        var sTmp = [];
        var labelExClass = "";
        if (fObj.bRequired) {
            labelExClass += " required";
        }
        sTmp.push('<div class="fieldEntry dropdownField">');
            sTmp.push('<div class="labelSideDiv">');
                sTmp.push('<div class="label' + labelExClass + '">' + fieldData.label + '</div>');
            sTmp.push('</div>');

            sTmp.push('<div class="fieldSideDiv">');
                sTmp.push('<div class="fieldDiv dropdownFieldDiv" fieldType="' + fieldData.expected[0].type + '" fieldName="' + fieldData.expected[0].field + '"  fieldDisplayType="' + fieldData.type + '">');
                    sTmp.push('<div class="' + fieldData.expected[0].field + 'DropDown"></div>');
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        // add to the display
        var $entitySearch = $detailsData.append(sTmp.join(''));
        fObj.$cntl = $('.fieldDiv[fieldName="' + fieldData.expected[0].field + '"]', $detailsData)
    } // loadDetailsDropdown

    function loadDetailsList(eData, fieldData)
    {
        // create the active field for this field
        var fObj = new Object();
        fObj.fieldType = fieldData.expected[0].type;
        fObj.fieldName = fieldData.expected[0].field;
        fObj.fieldDisplayType = fieldData.type;
        fObj.bRequired = true;
        if (fieldData.required != undefined) {
            fObj.bRequired = fieldData.required;
        }

        fObj.fnInitField = initFieldList;
        fObj.fnEnableDisableSaveHandler = enableDisableList;
        fObj.fnLoadValue = loadValueList;
        fObj.fnGetValue = getValueList;

        // custom fields
        fObj.label = fieldData.label;
        currentPageFields.push(fObj);

        // get data
        var sTmp = [];
        sTmp.push('<div class="fieldEntry listField">');
            sTmp.push('<div class="centerDiv">');
                sTmp.push('<div class="fieldDiv listFieldDiv" fieldType="' + fieldData.expected[0].type + '" fieldName="' + fieldData.expected[0].field + '"  fieldDisplayType="' + fieldData.type + '">');
                    sTmp.push('<div class="listField">');
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        // add to the display
        var $entitySearch = $detailsData.append(sTmp.join(''));
        fObj.$cntl = $('.fieldDiv[fieldName="' + fieldData.expected[0].field + '"]', $detailsData);
    } // loadDetailsLabel

    /*
     *
     * Init the supported fields
     *
     */
    function initFieldText(activeField)
    {
        activeField.$field = $('.textInput', activeField.$cntl);

        // the event to look for
        activeField.$field.on('keyup change', function(e) {
            enableDisableSaveBtn();
        });
    } // initFieldText

    function initFieldBoolean(activeField)
    {
        activeField.$field = $('.booleanCheckbox', activeField.$cntl);

        // the event to look for
        activeField.$field.on('change', function(e) {
            enableDisableSaveBtn();
        });
    } // initFieldBoolean

    function initFieldDate(activeField)
    {
        $('.dateInput', activeField.$cntl)
            .keydown(function (event) {
                if (event.which === 13) {
                    var tabEvent = jQuery.Event("keydown");
                    tabEvent.which = 9;
                    tabEvent.keyCode = 9;
                    $(this).trigger(tabEvent);

                    return false;
                }
            })
            .datepicker({
                autoclose: true,
                //clearBtn: true,
                //todayBtn: true,
                format: "mm/dd/yyyy",
            })
            .on('show', function(e) {
                $('.dateInput', activeField.$cntl).css('border-color', revupConstants.color.primaryGray4)
                $('.dateInputIcon .icon', activeField.$cntl).css('color', revupConstants.color.primaryGray4)
            })
            .on('hide', function(e) {
                $('.dateInput', activeField.$cntl).css('border-color', '')
                $('.dateInputIcon .icon', activeField.$cntl).css('color', '')
            });

        $('.dateInputIcon', activeField.$cntl).on('click', function(e) {
            $('.dateInput', activeField.$cntl).datepicker('show');
        });

        // update the active field object
        activeField.$field = $('.dateInput', activeField.$cntl);

        // input filter
        activeField.$field.numericOnly({bDate: true});

        activeField.$field.on('keyup change changeDate', function(e) {
            if (e.type == 'changeDate') {
                $(this).removeClass('inputFormatError');
            }
            else if (e.type == 'change') {
                var val = $(this).val();
                var regex = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/
                // test the number - trigger even as way display different at different fields
                if ((val == '') || (val.search(regex) != -1)) {
                    $(this).removeClass('inputFormatError');
                }
                else {
                    $(this).addClass('inputFormatError');
                }
            }

            enableDisableSaveBtn();
        })
    } // initFieldDate

    function initFieldInteger(activeField)
    {
        var $amt = $('.intInput', activeField.$cntl);
        if (activeField.bDisplayUnlimited) {
            var $dropdown = $('.intUnlimitedDropDown', activeField.$cntl);
            $dropdown.revupDropDownField({
                value: ['Amount', 'Unlimited'],
                changeFunc: function(index, val) {
                    if (val.toLowerCase() == 'amount') {
                        $amt.prop('readonly', false);
                        $amt.val('');
                    }
                    else {
                        $amt.prop('readonly', true);
                        $amt.val('unlimited');
                    }
                },
            });

            // set default
            $amt.prop('readonly', false);
            $amt.val('');
        }

        // input filter
        $amt.numericOnly({bInteger: true});

        // event handlers
        $amt.on('keyup change paste dblclick', function(e) {
            enableDisableSaveBtn();
        })

        // update the active field object
        activeField.$dropdown = $dropdown;
        activeField.$field = $amt;
    } // initFieldInteger

    function initFieldDropdown(activeField)
    {
        var ddClassName = activeField.fieldName + "DropDown";
        var $dropdown = $("." + ddClassName, activeField.$cntl);

        // build the list
        var lst = [];
        if (activeField.lstValues instanceof Array) {
            if (activeField.bAddClear) {
                var o = new Object();
                o.displayValue = "";
                o.value = " ";

                lst.push(o);
            }

            for (var i = 0; i < activeField.lstValues.length; i++) {
                var o = new Object();
                if (typeof activeField.lstValues[i] == "object") {
                    o.displayValue = activeField.lstValues[i][1];
                    o.value = activeField.lstValues[i][0];
                }
                else {
                    o.displayValue = o.value = activeField.lstValues[i]
                }

                lst.push(o);
            }
        }

        var changeFunc = null;
        if (activeField.changeFieldIndex != undefined) {
            changeFunc = function(index, val) {
                var updateField = currentPageFields[activeField.changeFieldIndex];
                var newLst = updateField.lstValues[val];
                var newLstStart = 0;
                if (newLst == undefined) {
                    newLst = [];
                    newLstStart = -1;
                }

                updateField.$dropdown.revupDropDownField({
                    value: newLst,
                    valueStartIndex: newLstStart,
                    updateValueList: true
                })
            }
        }

        // build the data
        $dropdown.revupDropDownField({
            value: lst,
            valueStartIndex: 0,
            addNewValue: activeField.bNewValue,
            sortList: activeField.bSortLst,
            changeFunc: changeFunc,
        });

        // event handler
        $dropdown.on('revup.dropDownSelected', function(e) {
            enableDisableSaveBtn();
        });

        // save extras
        activeField.$dropdown = $dropdown;
        activeField.className = ddClassName;
    } // initFieldDropdown

    function initFieldList(activeField)
    {
        activeField.$listField = $('.listField', activeField.$cntl);
        var labelClass = activeField.required ? 'required' : '';
        activeField.$listField.revupDynLst({label: activeField.label,
                                          //bScrollEntries: true,
                                          //entryLstMinHeight: "50px",
                                          labelClass: labelClass,
                                          inputName: 'activeField.fieldName' + 'DynLst',
                                          inputMaxLen: 48,
                                        });

        // the event to look for
        activeField.$cntl.on('revup.dynamicListAdd revup.dynamicListDelete revup.dynamicListEntryChange', function(e) {
            enableDisableSaveBtn();
        });
    } // initFieldList


    /*
     *
     * Enable / Disable save button for supported fields
     *
     */
    function enableDisableText(activeField)
    {
        // if the field is not required then return true
        if (!activeField.bRequired) {
            return true;
        }

        var bEnable = true;

        var val = activeField.$field.val();
        if (val == '') {
            bEnable = false;
        }

        return bEnable;
    } // enableDisableText

    function enableDisableBoolean(activeField)
    {
        return true;
    } // enableDisableText

    function enableDisableDate(activeField)
    {
        // if the field is not required then return true
        if (!activeField.bRequired) {
            return true;
        }

        var bEnable = true;

        // see if a format error
        if (activeField.$field.hasClass('inputFormatError')) {
            bEnable = false;
        }

        var val = activeField.$field.val();
        if (val == '') {
            bEnable = false;
        }

        return bEnable;
    } // enableDisableDate

    function enableDisableInteger(activeField)
    {
        // if the field is not required then return true
        if (!activeField.bRequired) {
            return true;
        }

        var bEnable = true;

        // see if a format error
        if (activeField.$field.hasClass('inputFormatError')) {
            bEnable = false;
        }

        var val = activeField.$field.val();
        if (activeField.bDisplayUnlimited) {
            var dVal = activeField.$dropdown.revupDropDownField('getValue');
            if ((dVal.toLowerCase() == 'unlimited') && (val.toLowerCase() != 'unlimited')) {
                bEnable = false;
            }
            else if ((dVal.toLowerCase() == 'amount') && (val == '')) {
                bEnable = false;
            }
        }
        else {
            if (val == '') {
                bEnable = false;
            }
        }

        return bEnable;
    } // enableDisableInteger

    function enableDisableDropdown(activeField)
    {
        // if the field is not required then return true
        if (!activeField.bRequired) {
            return true;
        }

        var bEnable = true;
        var val = activeField.$dropdown.revupDropDownField('getValue');
        if (val == "") {
            bEnable = false;
        }

        return bEnable;
    }

    function enableDisableList(activeField)
    {
        // if the field is not required then return true
        if (!activeField.bRequired) {
            return true;
        }

        // see if any of the fields have a value
        var valFields = 0;
        $('textField', activeField.$listField).each(function() {
            var v = $(this).val();
            if ($trim(v) != "") {
                valFields += 1;
            }
        })

        return valFields > 0;
    } // enableDisableList


    /*
     *
     * Load Current Value for the supported fields
     *
     */
    function loadValueText(activeField, val)
    {
        activeField.$field.val(val);
    } // loadValueText

    function loadValueBoolean(activeField, val)
    {
        activeField.$field.prop('checked', val);
    } // loadValueBoolean

    function loadValueDate(activeField, val)
    {
        date = revupUtils.formatDate(val);
        activeField.$field.datepicker('setDate', date).datepicker('update');
    } // loadValueDate

    function loadValueInteger(activeField, val)
    {
        activeField.$field.val(val);

        if (activeField.bDisplayUnlimited) {
            if (typeof val == 'string') {
                if (val.toLowerCase() == 'unlimited') {
                    activeField.$dropdown.revupDropDownField('setValue', 'Unlimited');
                    activeField.$field.val('unlimited');
                    activeField.$field.prop('readonly', true);
                }
            }
            else {
                activeField.$dropdown.revupDropDownField('setValue', 'Amount');
                activeField.$field.val('');
                activeField.$field.prop('readonly', false);
            }
        }
    } // loadValueInteger

    function loadValueDropdown(activeField, val)
    {
        activeField.$dropdown.revupDropDownField('setValue', val);
        activeField.initValue = val;

        // see if this value change another dropdown
        if (activeField.changeFieldIndex != undefined) {
            var $dropdown = currentPageFields[activeField.changeFieldIndex].$dropdown;
            var dropdownLst = currentPageFields[activeField.changeFieldIndex].lstValues[val];
            startIndex = 0;
            if (currentPageFields[activeField.changeFieldIndex].initValue != undefined) {
                for(var i = 0; i < dropdownLst.length; i++) {
                    if (dropdownLst[i] == currentPageFields[activeField.changeFieldIndex].initValue) {
                        startIndex = i;
                        break;
                    }
                }
            }
            var initValue = currentPageFields[activeField.changeFieldIndex].initValue;
            $dropdown.revupDropDownField({
                value: dropdownLst,
                valueStartIndex: startIndex,
                updateValueList: true,
            })
        }
    } // loadValueDropdown

    function loadValueList(activeField, val)
    {
        activeField.$listField.revupDynLst('setList', val);
    } // loadValueList

    /*
     *
     * Get the current Value for the supported fields
     *
     */
    function getValueText(activeField, jObj)
    {
        var val = activeField.$field.val();

        jObj[activeField.fieldName] = val;
    } // getValueText

    function getValueBoolean(activeField, jObj)
    {
        var val = activeField.$field.prop('checked');

        jObj[activeField.fieldName] = val;
    } // getValueBoolean

    function getValueDate(activeField, jObj)
    {
            var val = activeField.$field.val();
            if (val != '') {
                val = val.split('/');
                val = val[2] + '-' + val[0] + '-' + val[1];
            }

            jObj[activeField.fieldName] = val;
    } // getValueDate

    function getValueInteger(activeField, jObj)
    {
        var val = activeField.$field.val();
        if (val == '') {
            val = 0;
        }

        jObj[activeField.fieldName] = val;
    } // getValueInteger

    function getValueDropdown(activeField, jObj)
    {
        var val = activeField.$dropdown.revupDropDownField('getValue');
        jObj[activeField.fieldName] = val;
    } // getValueDropdown

    function getValueList(activeField, jObj)
    {
        var val = activeField.$listField.revupDynLst('getList');
        jObj[activeField.fieldName] = val;
    } // getValueList

    return {
        load: function(fieldData) {
            // set the first entry selected in the settings
            $('.entry', $settingsLst).removeClass('selected');
            var $firstEntry = $('.entry:first-of-type', $settingsLst);
            $firstEntry.addClass('selected');

            // load settingLst data
            loadFieldLstData(fieldData);

            // load the details
            loadDetails($firstEntry);

            //load event handlers
            loadEvents();
        }
    }
} ());
