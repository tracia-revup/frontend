    var prospects = (function () {
    // globals
    var templateData = undefined;

    // selectors
    var $loading = $();
    var $prospectLst = $();
    var $header = $();
    var $lst = $();
    var $totalProspects = $();

    /*
     * Build/display
     */
    function buildSliderBar(bBlank)
    {
        var sTmp = [];

        var blank = "";
        if (bBlank) {
            blank = ' blank';
        }
        sTmp.push('<div class="slider' + blank + '">');
            if (!bBlank) {
                sTmp.push('<div class="sOuter"></div>');
                    sTmp.push('<div class="sInner"></div>');
                        sTmp.push('<div class="sCenter"></div>')
                    sTmp.push('<div class="sInner"></div>');
                sTmp.push('<div class="sOuter"></div>');
            }
        sTmp.push('</div>');

        return sTmp.join('')
    } // buildSliderBar

    function buildHeader()
    {
        var sTmp = [];

        var colWidth = localStorage.getItem('prospectsColWidth-' + templateData.seatId);
        if (colWidth) {
            colWidth = JSON.parse(colWidth);
        }

        // walk the header data and build the header
        var headerData = templateData.columns;
        for (var i = 0; i < headerData.length; i++) {
            // build classStuff
            var classStuff = [];
            classStuff.push('column');
            if (headerData[i].css) {
                classStuff.push(headerData[i].css);
            }

            // build style
            var style = [];
            if (headerData[i].position) {
                style.push("text-align:" + headerData[i].position);
            }
            if ((colWidth) && (colWidth[headerData[i].css])) {
                style.push("width:" + colWidth[headerData[i].css] + "px");
            }
            else if (headerData[i].width) {
                style.push("width:" + headerData[i].width);
            }
            if (headerData[i].headerStyle) {
                style.push(headerData[i].headerStyle);
            }

            // minimum column width when resizing
            var minColWidth = '';
            if (headerData[i].minColWidth) {
                minColWidth = ' minColWidth="' + headerData[i].minColWidth + '"'
            }

            sTmp.push('<div class="' + classStuff.join(' ') + '" style="' + style.join(';') + '"' + minColWidth + '>');
                var text = headerData[i].text;
                if (text == '') {
                    if (currentSkin.prospectTitle) {
                        text = currentSkin.prospectTitle[headerData[i].loadSkinField];
                    }
                    if (text == '') {
                        switch (headerData[i].loadSkinField) {
                            case 'softTitle':
                                text = 'Soft';
                                break;
                            case 'hardTitle':
                                text = 'Hard';
                                break;
                            case 'inTitle':
                                text = 'In';
                                break;
                        }
                    }
                text += '($)'
                }

                sTmp.push('<div class="columnHeader">' + text + '</div>');
            sTmp.push('</div>');

            if (headerData[i].sliderAfter) {
                sTmp.push(buildSliderBar(false));
            }
        }

        $header.append(sTmp.join(''));
    }; // buildHeader

    function buildLstData(data)
    {
        var sTmp = [];

        var colWidth = localStorage.getItem('prospectsColWidth-' + templateData.seatId);
        if (colWidth) {
            colWidth = JSON.parse(colWidth);
        }

        var colData = templateData.columns;
        for (var i = 0; i < data.length; i++) {
            var contactId = data[i].contact.id;
            var userId = data[i].user.id;
            sTmp.push('<div class="entry" prospectId="' + data[i].id + '" contactId="' + contactId + '" userId="' + data[i].user.id + '">');
                for (var j = 0; j < colData.length; j++) {
                    // build the content
                    var divContent = "Content Goes Here";
                    var titleText = "";
                    var inputType = "";
                    var dataFor = "";
                    switch (colData[j].dataType) {
                        case "contactName":
                            // build the url
                            //var url = templateData.prospectsDetails;
                            //url = url.replace('contactId', contactId)

                            // build the name
                            name = "name";
                            if (data[i].contact.first_name) {
                                name = data[i].contact.first_name;
                            }
                            if (data[i].contact.last_name) {
                                if (name != "") {
                                    name += ' ';
                                }
                                name += data[i].contact.last_name;
                            }

                            var extra = '';
                            if (userId != templateData.userId) {
                                extra += ' disabled';
                            }
                            divContent = '<div class="nameBtn' + extra + '" contactName="' + contactId + '">' + name + '</div>'
                            break;
                        case "userInitials":
                            var userFirstName = data[i].user.first_name;
                            var userLastName = data[i].user.last_name;

                            // build the title text
                            titleText = userFirstName;
                            if (userLastName) {
                                if (titleText != "") {
                                    titleText += ' ';
                                titleText += userLastName;
                                }
                            }

                            // build the initials
                            var initials = "";
                            if (userFirstName) {
                                initial = userFirstName.charAt(0);
                            }
                            if (userLastName) {
                                initial += userLastName.charAt(0);
                            }
                            divContent = initial.toUpperCase();
                            break;
                        case "integer":
                            divContent = parseInt(data[i][colData[j].dataField]);
                            dataFor = colData[j].dataFor;
                            break;
                        case "dollar":
                            divContent = parseFloat(data[i][colData[j].dataField]);
                            divContent = divContent.toFixed(2);
                            dataFor = colData[j].dataFor;
                            break;
                        case "iconCallsheet":
                        case "iconEmail":
                        case "iconDelete":
                            divContent = colData[j].dataType;
                            break;
                        default:
                            console.error("Unknow column type: " + colData[j].dataType);
                            break;
                    } // end switch - colData[j].dataType

                    // build classStuff
                    var classStuff = [];
                    classStuff.push('column');
                    if (colData[j].css) {
                        classStuff.push(colData[j].css);
                    }

                    // build classStuff
                    var classStuff = [];
                    classStuff.push('column');
                    if (colData[j].css) {
                        classStuff.push(colData[j].css);
                    }

                    // build style
                    var style = [];
                    if (colData[j].position) {
                        style.push("text-align:" + colData[j].position);
                    }
                    if ((colWidth) && (colWidth[colData[j].css])) {
                        style.push("width:" + colWidth[colData[j].css] + "px");
                    }
                    else if (colData[j].width) {
                        style.push("width:" + colData[j].width);
                    }
                    if (colData[j].style) {
                        style.push(colData[j].style);
                    }

                    // add the new column entry
                    var extraAttr = "";
                    switch (colData[j].displayType) {
                        case "textLink":
                        case "text":
                            // build extra attributs
                            if (titleText) {
                                extraAttr += ' title="' + titleText + '"';
                            }
                            if (dataFor != '') {
                                extraAttr += ' dataFor="' + dataFor + '"';
                            }

                            sTmp.push('<div class="' + classStuff.join(' ') + '" style="' + style.join(';') + '"' + extraAttr +'>');
                                sTmp.push(divContent);
                            sTmp.push('</div>');
                            break;
                        case "dollarInput":
                        case "intInput":
                            sTmp.push('<div class="' + classStuff.join(' ') + '" style="' + style.join(';') + '"' + extraAttr +'>');
                                sTmp.push('<input type="text" dataFor="' + dataFor + '" dataType="' + colData[j].dataType + '" value="' + divContent + '" initVal="' + divContent + '">');
                            sTmp.push('</div>');
                            break;
                        case "icon-notes":
                            var extra = '';
                            if (userId != templateData.userId) {
                                extra += ' disabled';
                            }
                            sTmp.push('<div class="' + classStuff.join(' ') + '" style="' + style.join(';') + '"' + extraAttr +'>');
                                sTmp.push('<div class="' + divContent + ' icon ' + colData[j].displayType + extra + '"></div>');
                            sTmp.push('</div>');
                        break;
                        case "icon-email":
                        case "icon-delete-circle":
                            sTmp.push('<div class="' + classStuff.join(' ') + '" style="' + style.join(';') + '"' + extraAttr +'>');
                                sTmp.push('<div class="' + divContent + ' icon ' + colData[j].displayType + '"></div>');
                            sTmp.push('</div>');
                            break;
                        default:
                            console.error("Unknow display type: " + colData[j].displayType);
                            break;
                    } // end switch - colData[j].displayType

                    // slider
                    if (colData[j].sliderAfter) {
                        sTmp.push(buildSliderBar(true));
                    }
                }
            sTmp.push('</div>');
        }

       // bottom 2 rows
        for (var row = 0; row < 2; row++) {
            row == 0 ? sTmp.push('<div class="entryFooter regularTotal">') : sTmp.push('<div class="entryFooter hardInTotal">');
            // sTmp.push('<div class="entryFooter">');
                for (var j = 0; j < colData.length; j++) {

                    // build the content
                    var divContent = "";
                    var computeTotalFor = "";
                    switch (colData[j].dataType) {
                        // debugger;
                        case "contactName":
                            if (row == 0) {
                                divContent = colData[j].footer1Label;
                                if (colData[j].footer1SkinFields) {
                                    var title = [];
                                    for(var t = 0; t < colData[j].footer1SkinFields.length; t++) {
                                        if ((currentSkin.prospectTitle) && (currentSkin.prospectTitle[colData[j].footer2SkinFields[t]] != '')) {
                                            title.push(currentSkin.prospectTitle[colData[j].footer2SkinFields[t]]);
                                        }
                                        else {
                                            switch (colData[j].footer2SkinFields[t]) {
                                                case 'hardTitle':
                                                    title.push('Hard');
                                                    break;
                                                case 'softTitle':
                                                    title.push('Soft');
                                                    break;
                                                case 'inTitle':
                                                    title.push('In');
                                                    break;
                                                default:
                                                    title.push(colData[j].footer2SkinFields[t]);
                                                    break;
                                            }
                                        }
                                    }
                                    divContent = title.join(' + ');
                                }
                            }
                            else if (row == 1) {
                                divContent = colData[j].footer2Label;
                                if (colData[j].footer2SkinFields) {
                                    var title = [];
                                    for(var t = 0; t < colData[j].footer2SkinFields.length; t++) {
                                        if ((currentSkin.prospectTitle) && (currentSkin.prospectTitle[colData[j].footer2SkinFields[t]] != '')) {
                                            title.push(currentSkin.prospectTitle[colData[j].footer2SkinFields[t]]);
                                        }
                                        else {
                                            switch (colData[j].footer2SkinFields[t]) {
                                                case 'hardTitle':
                                                    title.push('Hard');
                                                    break;
                                                case 'softTitle':
                                                    title.push('Soft');
                                                    break;
                                                case 'inTitle':
                                                    title.push('In');
                                                    break;
                                                default:
                                                    title.push(colData[j].footer2SkinFields[t]);
                                                    break;
                                            }
                                        }
                                    }
                                    divContent = title.join(' + ');
                                }
                            }
                            break;
                        case "userInitials":
                            break;
                        case "integer":
                            break;
                        case "dollar":
                            if (((row == 0) && colData[j].footer1Display) || ((row == 1) && colData[j].footer2Display)) {
                                divContent = "$0.00";
                            }

                            // what data type to add up
                            if ((row == 0) && (colData[j].footer1Display)) {
                                computeTotalFor = colData[j].dataFor
                            }
                            else if ((row == 1) && (colData[j].footer2Display)) {
                                computeTotalFor = colData[j].footerSumDisplay
                            }
                            break;
                        case "iconCallsheet":
                        case "iconEmail":
                        case "iconDelete":
                            break;
                        default:
                            console.error("Unknow column type: " + colData[j].dataType);
                            break;
                    } // end switch - colData[j].dataType

                    // build classStuff
                    var classStuff = [];
                    classStuff.push('column');
                    if (colData[j].css) {
                        classStuff.push(colData[j].css);
                    }

                    // build style
                    var style = [];
                    if (colData[j].position) {
                        style.push("text-align:" + colData[j].position);
                    }
                    if ((colWidth) && (colWidth[colData[j].css])) {
                        style.push("width:" + colWidth[colData[j].css] + "px");
                    }
                    else if (colData[j].width) {
                        style.push("width:" + colData[j].width);
                    }
                    if (colData[j].style) {
                        style.push(colData[j].style);
                    }
                    if ((colData[j].dataType == "dollar") && (colData[j].displayType == 'dollarInput')) {
                        style.push('padding-right:2px');
                    }

                    var extraAttr = "";
                    if (computeTotalFor != "") {
                        extraAttr += 'computeTotalFor="' + computeTotalFor + '"';
                        // debugger;
                    }
                    if (extraAttr != "") {
                        extraAttr = " " + extraAttr
                    }

                    // build the entry
                    sTmp.push('<div class="' + classStuff.join(' ') + '" style="' + style.join(';') + '"' + extraAttr +'>');
                        sTmp.push(divContent);
                    sTmp.push('</div>');

                    // slider
                    if (colData[j].sliderAfter) {
                        sTmp.push(buildSliderBar(true));
                    }
                }
            sTmp.push('</div>');
        }

        $lst.append(sTmp.join(''));

        // totals
        computeTotals();

        $totalProspects.attr('prospecttotalcount', data.length);
    } // buildLstData

    function buildListSection($containerDiv)
    {
        var sTmp = [];

        // build and
        sTmp.push('<div class="prospectsLoading" style="height:500px;width:100%">');
            sTmp.push('<div class="loadingDiv">');
                sTmp.push('<div class="loading"></div>');
                sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
            sTmp.push('</div>');
        sTmp.push('</div>');
        sTmp.push('<div class="prospectsNewLayout" style="display:none;">');
            sTmp.push('<div class="prospectsLstDiv">');
                sTmp.push('<div class="headerDiv" prospectTotalCount></div>');
                sTmp.push('<div class="lstDiv"></div>');
            sTmp.push('</div>');
        sTmp.push('</div>')
        $containerDiv.html(sTmp.join(''));

        // global selectors
        $loading = $('.prospectsLoading', $containerDiv);
        $prospectLst = $('.prospectsNewLayout', $containerDiv);
        $header = $('.prospectsLstDiv .headerDiv', $prospectLst);
        $lst = $('.prospectsLstDiv .lstDiv', $prospectLst);
        $totalProspects = $('.prospectsLstDiv .headerDiv');


        // make sure that the forward/back button casue a reload
        window.onpopstate = function(event) {
            if(event && event.state) {
                location.reload();
            }
        }

        // mixpanel
        mixpanel.track("Prospects", {});

        // build the header
        buildHeader();

        // hide loading and display
        $loading.hide();
        $prospectLst.show();
    }; // buildListSection

    /*
     * compute totals
     */
    function computeTotals()
    {
        $computeFields = $('[computeTotalFor]', $lst);
        $computeFields.each(function() {
            // get the dataFor field type to search for
            var $field = $(this);
            var field = $field.attr('computeTotalFor');

            // see if some of serveral fields
            field = field.split(',');

            // compute the total
            total = 0.0;
            for (var f = 0; f < field.length; f++) {
                $fields = $('[dataFor=' + field[f] + ']', $lst);
                $fields.each(function() {
                    var amt;
                    if ($(this).is('input')) {
                        amt = $(this).val();
                    }
                    else if ($(this).is('div')) {
                        amt = $(this).text();
                    }
                    amt = parseFloat(amt);
                    total += amt;
                })
            }
            total = '$' + revupUtils.commify(total.toFixed(2));

            // display the total
            $field.text(total);
        })
    } // computeTotals

    /*
     * Prospect Details
     */
    function buildProspectDetailsFrame()
    {
        var sTmp = [];

        sTmp.push('<div class="newProspectDetail">');
            sTmp.push('<div class="mainPageLoading" style="height:500px">');
                sTmp.push('<div class="loadingDiv">');
                    sTmp.push('<div class="loading"></div>');
                    sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                sTmp.push('</div>');
            sTmp.push('</div>');
            sTmp.push('<div class="mainPage" style="display:none">');
                sTmp.push('<div class="pageHeader">');
                sTmp.push('</div>');

                sTmp.push('<div class="partitionHeaderDiv">');
                    sTmp.push('<div class="innerDiv">');
                        sTmp.push('<div class="partitionGrpDiv">');
                            sTmp.push('<div class="textLabel">Election Cycle:</div>');
                            sTmp.push('<div class="partitionDiv"></div>');
                            //sTmp.push('<div class="text">election cycle</div>');
                        sTmp.push('</div>');

                        sTmp.push('<div class="legendIconBtn" title="Legend">');
                            sTmp.push('<div class="icon icon-key"></div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');

                sTmp.push('<div class="givingSection">');
                    sTmp.push('<div class="givingLoading" style="height:450px;display:none">');
                        sTmp.push('<div class="loadingDiv">');
                            sTmp.push('<div class="loading"></div>');
                            sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="givingLoadedData">');
                        sTmp.push('<div class="leftSide">');
                            sTmp.push('<div class="givingTypeSectionOuter academicGiving">');
                                sTmp.push('<div class="givingTypeSection  opened">');
                                    sTmp.push('<div class="givingTypeSection opened">');
                                        sTmp.push('<div class="sectionHeader">');
                                            sTmp.push('<div class="openClose icon icon-triangle-dn"></div>');
                                            sTmp.push('<div class="headerText">Purdue Giving</div>');
                                            sTmp.push('<div class="headerRight">');
                                                sTmp.push('<div class="graphDiv candidates">');
                                                    sTmp.push('<div class="title">Candidates</div>');
                                                    sTmp.push('<div class="graph"></div>');
                                                sTmp.push('</div>');
                                                sTmp.push('<div class="graphDiv dollars">');
                                                    sTmp.push('<div class="title">Dollars</div>');
                                                    sTmp.push('<div class="graph"></div>');
                                                sTmp.push('</div>');
                                                sTmp.push('<div class="headerNoData">No Data</div>');
                                            sTmp.push('</div>');
                                        sTmp.push('</div>');
                                        sTmp.push('<div class="sectionBody">');
                                        sTmp.push('</div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');

                            sTmp.push('<div class="givingTypeSectionOuter academicNonProfit">');
                                sTmp.push('<div class="givingTypeSection opened">');
                                    sTmp.push('<div class="sectionHeader">');
                                        sTmp.push('<div class="openClose icon icon-triangle-dn"></div>');
                                        sTmp.push('<div class="headerText">Academic/Nonprofit</div>');
                                        sTmp.push('<div class="headerRight">');
                                            sTmp.push('<div class="headerNoData">No Data</div>');
                                        sTmp.push('</div>');
                                    sTmp.push('</div>');
                                    sTmp.push('<div class="sectionBody">');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');

                            sTmp.push('<div class="givingTypeSectionOuter stateCampaign">');
                                sTmp.push('<div class="givingTypeSection  opened">');
                                    sTmp.push('<div class="givingTypeSection opened">');
                                        sTmp.push('<div class="sectionHeader">');
                                            sTmp.push('<div class="openClose icon icon-triangle-dn"></div>');
                                            sTmp.push('<div class="headerText">State Campaigns</div>');
                                            sTmp.push('<div class="headerRight">');
                                                sTmp.push('<div class="graphDiv candidates">');
                                                    sTmp.push('<div class="title">Candidates</div>');
                                                    sTmp.push('<div class="graph"></div>');
                                                sTmp.push('</div>');
                                                sTmp.push('<div class="graphDiv dollars">');
                                                    sTmp.push('<div class="title">Dollars</div>');
                                                    sTmp.push('<div class="graph"></div>');
                                                sTmp.push('</div>');
                                                sTmp.push('<div class="headerNoData">No Data</div>');
                                            sTmp.push('</div>');
                                        sTmp.push('</div>');
                                        sTmp.push('<div class="sectionBody">');
                                        sTmp.push('</div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');

                            sTmp.push('<div class="givingTypeSectionOuter localCampaign">');
                                sTmp.push('<div class="givingTypeSection  opened">');
                                    sTmp.push('<div class="givingTypeSection opened">');
                                        sTmp.push('<div class="sectionHeader">');
                                            sTmp.push('<div class="openClose icon icon-triangle-dn"></div>');
                                            sTmp.push('<div class="headerText">Local Campaigns</div>');
                                            sTmp.push('<div class="headerRight">');
                                                sTmp.push('<div class="graphDiv candidates">');
                                                    sTmp.push('<div class="title">Candidates</div>');
                                                    sTmp.push('<div class="graph"></div>');
                                                sTmp.push('</div>');
                                                sTmp.push('<div class="graphDiv dollars">');
                                                    sTmp.push('<div class="title">Dollars</div>');
                                                    sTmp.push('<div class="graph"></div>');
                                                sTmp.push('</div>');
                                                sTmp.push('<div class="headerNoData">No Data</div>');
                                            sTmp.push('</div>');
                                        sTmp.push('</div>');
                                        sTmp.push('<div class="sectionBody">');
                                        sTmp.push('</div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');

                        sTmp.push('<div class="rightSide">');
                            sTmp.push('<div class="givingTypeSectionOuter federalCampaign">');
                                sTmp.push('<div class="givingTypeSection opened">');
                                    sTmp.push('<div class="sectionHeader">');
                                        sTmp.push('<div class="openClose icon icon-triangle-dn"></div>');
                                        sTmp.push('<div class="headerText">Federal Campaigns</div>');
                                        sTmp.push('<div class="headerRight">');
                                            sTmp.push('<div class="graphDiv candidates">');
                                                sTmp.push('<div class="title">Candidates</div>');
                                                sTmp.push('<div class="graph"></div>');
                                            sTmp.push('</div>');
                                            sTmp.push('<div class="graphDiv dollars">');
                                                sTmp.push('<div class="title">Dollars</div>');
                                                sTmp.push('<div class="graph"></div>');
                                            sTmp.push('</div>');
                                            sTmp.push('<div class="headerNoData">No Data</div>');
                                        sTmp.push('</div>');
                                    sTmp.push('</div>');
                                    sTmp.push('<div class="sectionBody">');
                                    sTmp.push('</div>');
                                    sTmp.push('<div class="sectionFooter">');
                                        sTmp.push('<div class="fecNotice">');
                                            sTmp.push('Notice: Federal law prohibits using contributor contact information that is obtained from FEC reports');
                                            sTmp.push('for the purpose of soliciting contributions or for any commercial purpose. However, analysis of data');
                                            sTmp.push('obtained from FEC reports is permitted. RevUp does not display individual contributor contact information');
                                            sTmp.push('obtained from reports filed with the FEC.');
                                        sTmp.push('</div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');

                            sTmp.push('<div class="givingTypeSectionOuter otherCampaign">');
                                sTmp.push('<div class="givingTypeSection  opened">');
                                    sTmp.push('<div class="givingTypeSection opened">');
                                        sTmp.push('<div class="sectionHeader">');
                                            sTmp.push('<div class="openClose icon icon-triangle-dn"></div>');
                                            sTmp.push('<div class="headerText">Local Campaigns</div>');
                                            sTmp.push('<div class="headerRight">');
                                                sTmp.push('<div class="graphDiv candidates">');
                                                    sTmp.push('<div class="title">Candidates</div>');
                                                    sTmp.push('<div class="graph"></div>');
                                                sTmp.push('</div>');
                                                sTmp.push('<div class="graphDiv dollars">');
                                                    sTmp.push('<div class="title">Dollars</div>');
                                                    sTmp.push('<div class="graph"></div>');
                                                sTmp.push('</div>');
                                                sTmp.push('<div class="headerNoData">No Data</div>');
                                            sTmp.push('</div>');
                                        sTmp.push('</div>');
                                        sTmp.push('<div class="sectionBody">');
                                        sTmp.push('</div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        return sTmp.join('');

    } // buildProspectDetailsFrame

    /*
     *  Event handlers
     */
    function addEventHandlers()
    {
        // add the data type handlers
        $('input[dataType=integer]', $lst).numericOnly();
        $('input[dataType=dollar]', $lst).numericOnly({bDollar: true});

        // event on input fields
        $lst
            .on('focus', 'input[dataType=integer], input[dataType=dollar]', function(e) {
                // save the current value
                var $this = $(this);

                // if there is an error then skip
                if ($this.hasClass("inputFormatError")) {
                    return;
                }

                // update the initial value
                var val = $this.val();
                $this.attr("initVal", val);
            })
            .on('blur', 'input[dataType=integer], input[dataType=dollar]', function(e) {
                var $this = $(this);
                updateValues($this);
            })
            .on('keydown', 'input[dataType=integer], input[dataType=dollar]', function(e) {
                if (e.keyCode == 13) {
                    var $this = $(this);
                    updateValues($this);

                    e.preventDefault();
                }
            })
            .on('dblclick', '.nameBtn', function(e) {
                // see if disabled
                if ($(this).hasClass('disabled')) {
                    return;
                }

                var wWidth = $('body').width();
                var wHeight = $('body').height();
                var browserHeight = $(document).height();
                var viewportHeight = $(window).height();

                // create the popup
                var sTmp = [];
                sTmp.push('<div class="mainPageLoading" style="height:500px">');
                    sTmp.push('<div class="loadingDiv">');
                        sTmp.push('<div class="loading"></div>');
                        sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                    sTmp.push('</div>');
                sTmp.push('</div>');
                $('body').css('overflow', 'hidden');
                var $prospectDetailsDlg = $('body').revupDialogBox({
                    cancelBtnText: '',
                    okBtnText: 'Close',
                    extraClass: 'rankingPageMenu',
                    msg: sTmp.join(''),
                    top: '20',
                    width: wWidth - 200 + 'px',
                    height: Math.min(browserHeight, viewportHeight)-30,
                })

                var $this = $(this);
                var $entry = $this.closest('.entry');
                var contactId = $entry.attr('contactId');
                var partitionId = localStorage.getItem('rankPartition-' + templateData.seatId);

                // find the partition in the list
                var partition = {};
                if ((partitionId == undefined) || (partitionId == '')) {
                    var pLstLen = templateData.partitionLst.length - 1;
                    partition = templateData.partitionLst[pLstLen];
                    partitionId = partition.value;
                }
                else {
                    for (var p = 0; p < templateData.partitionLst.length; p++) {
                        if (partitionId == templateData.partitionLst[p].value) {
                            partition = templateData.partitionLst[p];
                            break;
                        }
                    }
                }

                // which partition is displayed
                var partitionId = localStorage.getItem('rankPartition-' + templateData.seatId);
                if (partitionId == null) {
                    partitionId = templateData.partitionLst[0].value;
                }

                var url = templateData.contactDetailUrl.replace('seatId', templateData.seatId)
                                                       .replace('contactSetId', templateData.contactSetId)
                                                       .replace("contactId", contactId);
                url += '?partition=' + partitionId;
                $.ajax({
                    url: url,
                    type: "GET",
                })
                .done(function(r) {
                    var content = buildProspectDetailsFrame();
                    $('.msgBodyScroll', $prospectDetailsDlg).html(content)

                    var prospectDetailData = {
                        // template data
                        contactId:          Number(contactId),
                        seatId:             Number(templateData.seatId),
                        accountId:          Number(templateData.accountId),
                        contactSetId:       Number(templateData.contactSetId),


                        partitionLst:   templateData.partitionLst,
                        partitionStart: partitionId,

                        // urls
                        prospectDetailUrl:  templateData.contactDetailUrl,
                    };
                    prospectDetails.display(prospectDetailData, r);
                    $('body').css('overflow', '');
                })
                .fail(function(r) {
                    revupUtils.apiError('Prospects', r, 'Unable to retrieve Callsheet Data', "Unable to retrive Callsheet Data");
                })
            })
            .on('click', '.iconCallsheet', function(e) {
                var $this = $(this);

                // see if disabled
                if ($this.hasClass('disabled')) {
                    return;
                }

                var $entry = $this.closest('.entry');
                var contactId = $entry.attr('contactId');
                var partitionId = localStorage.getItem('rankPartition-' + templateData.seatId);

                // which partition is displayed
                var partitionId = localStorage.getItem('rankPartition-' + templateData.seatId);
                if (partitionId == null) {
                    partitionId = templateData.partitionLst[0].value;
                }

                // find the partition in the list
                var partition = {};
                if ((partitionId == undefined) || (partitionId == '')) {
                    var pLstLen = templateData.partitionLst.length - 1;
                    partition = templateData.partitionLst[pLstLen];
                    partitionId = partition.value;
                }
                else {
                    for (var p = 0; p < templateData.partitionLst.length; p++) {
                        if (partitionId == templateData.partitionLst[p].value) {
                            partition = templateData.partitionLst[p];
                            break;
                        }
                    }
                }

                var url = templateData.contactDetailUrl.replace('seatId', templateData.seatId)
                                                       .replace('contactSetId', templateData.contactSetId)
                                                       .replace("contactId", contactId);
                url += '?partition=' + partitionId;
                $.ajax({
                    url: url,
                    type: "GET",
                })
                .done(function(r) {
                    // callsheet object
                    var cObj = {};
                    cObj.data = r;
                    cObj.partitionObj = partition;
                    cObj.candidateName = templateData.candidateName;
                    cObj.contactId = contactId;

                    cObj.accountId = templateData.accountId;
                    cObj.seatId = templateData.seatId;
                    cObj.contactSetId = templateData.contactSetId;
                    cObj.saveNotes = true;
                    cObj.notesApi = templateData.callsheetNotesApi;

                    cObj.notesApi = templateData.callsheetNotesApi;
                    if (templateData.isAcademic) {
                        cObj.config = {
                            editorSection: {
                                display: true,
                                editors: [
                                    {
                                        header: "Notes",
                                        height: "12em",
                                    }
                                ]
                            },
                            contact: {
                                display: true,
                                displayTop: false,

                                firstName: {
                                    section: 'contact',
                                    field: 'first_name',
                                },
                                lastName: {
                                    section: 'contact',
                                    field: 'last_name',
                                },
                                phone: {
                                    label: 'Phone',
                                    section: 'features',
                                    fieldStartWith: 'Purdue Alum Lookup',
                                    signalSet: 'Purdue Bio',
                                    subField: 'match',
                                    keyLabel: ['Cell', 'Home', 'Work'],
                                    key: ['mobile_phone', 'home_phone', 'business_phone'],
                                    trimOffFront: 5,
                                },
                                email: {
                                    label: 'Email',
                                    section: 'features',
                                    fieldStartWith: 'Purdue Alum Lookup',
                                    signalSet: 'Purdue Bio',
                                    subField: 'match',
                                    key: 'email'
                                },
                            },
                            contactDetails: {
                                display: true,
                                section: "features",
                                field: "Purdue Alum Lookup",
                                data: [
                                    {
                                        subField: 'match',
                                        signalSet: 'Purdue Bio',
                                        type: 'attr',
                                        attr: [
                                            {
                                                key: 'school',
                                                label: 'Graduated',
                                                type: 'text',
                                            },
                                            {
                                                key: 'major',
                                                label: 'Major',
                                                type: 'text',
                                            },
                                            {
                                                key: 'degree',
                                                label: 'Degree',
                                                type: 'degree'
                                            },
                                            {
                                                key: 'graduation_year',
                                                label: 'Year Graduated',
                                                type: 'year'
                                            },
                                            {
                                                key: 'employer',
                                                label: 'Employer',
                                                type: 'text'
                                            },
                                            {
                                                key: 'title',
                                                label: 'Occupation',
                                                type: 'text'
                                            }
                                        ]
                                    },
                                    {
                                        subField: 'match',
                                        signalSet: 'Purdue Activities',
                                        type: 'activity',
                                        key: 'activity',
                                        bDeDup: true,
                                        label: 'Clubs/Activity'
                                    }
                                ]
                            },
                            contribution: {
                                display: true,
                                leftSide: [
                                    {
                                        section: 'key_signals',
                                        field: 'giving',
                                        type: 'givingTotal',
                                        title: 'Total Contributions Since %cycleYear%:',
                                        bUseTitle2: ((templateData.partitionLst.length == 1) && (templateData.partitionLst[0].displayValue.toLowerCase() == 'all')),
                                        title2: 'Total Contributions:',
                                    },
                                    {
                                        type: 'givingFromSrc',
                                        title: 'Total Contributions Made to %srcValue%',

                                        srcSection: 'features',
                                        srcFieldStartWith: 'Purdue Alum Lookup',
                                        srcSignalSet: 'Purdue Bio',
                                        srcSubField: 'match',
                                        srcKey: 'school',

                                        dstSection: 'features',
                                        dstFieldStartWith: 'Purdue Giving',
                                        dstSignalSet: 'Purdue Giving',
                                        dstSubField: 'match',
                                        dstSrcMatchKey: 'school',
                                        dstKey: 'amount',
                                    }
                                ],
                                rightSide: [
                                    {
                                        section: 'features',
                                        fieldStartWith: 'Purdue Giving',
                                        subField: 'match',
                                        type: 'givingOrgBy',
                                        label: 'Top Contributions',
                                        disaplyField: 'school',
                                        orgBy: "school",
                                        thenBy: 'date',
                                        maxOrgByDisplay: 5,
                                        bDisplayTotal: true,
                                    },
                                ],
                            },
                        }
                    }
                    if (templateData.isPoliticalCampaign || templateData.isPoliticalCommittee) {
                        cObj.config = {
                            editorSection: {
                                display: true,
                                editors: [
                                    {
                                        header: "Ask",
                                        height: "6em",
                                    },
                                    {
                                        header: "Results / Notes",
                                        height: "6em",
                                    }
                                ]
                            },
                            contact: {
                                display: true,
                                displayTop: false,

                                firstName: {
                                    section: 'contact',
                                    field: 'first_name',
                                },
                                lastName: {
                                    section: 'contact',
                                    field: 'last_name',
                                },
                                phone: {
                                    label: 'Phone',
                                    section: 'contact',
                                    field: 'phone_numbers',
                                    trimOffFront: 7,
                                },
                                email: {
                                    label: 'Email',
                                    section: 'contact',
                                    field: 'email_addresses'
                                },
                                employer: {
                                    label: 'Employer',
                                    section: 'contact',
                                    field: 'organizations',
                                    fieldKey: 'name',
                                },
                                occupation: {
                                    label: 'Occupation',
                                    section: 'contact',
                                    field: 'organizations',
                                    fieldKey: 'title',
                                },
                            },
                            headerRight: {
                                display: true,
                                section: "features",
                                fieldStartWith: "Client Matches",
                                fields: [
                                    {
                                        key: 'giving_availability',
                                        label: 'in giving availability',
                                        type: 'amountOrMsg',
                                    },
                                ]
                            },
                            contribution: {
                                display: true,
                                leftSide: [
                                    {
                                        section: 'key_signals',
                                        field: 'giving',
                                        type: 'givingTotal',
                                        title: 'Total Contributions Since %cycleYear% Election Cycle:',
                                    },
                                    {
                                        section: 'features',
                                        fieldStartWith: 'Client Matches',
                                        subField: 'giving',
                                        type: 'givingTotal',
                                        title: 'Total Contributions to Your Candidate:'
                                    },
                                    {
                                        section: 'features',
                                        fieldStartWithCand: "Client Matches",
                                        fieldStartWithOther: "Key Contributions",
                                        title: "Top Contributions",
                                        type: "candidateContribution",
                                    }
                                ],
                                rightSide: [
                                    {
                                        section: 'features',
                                        fieldStartWith: 'Federal Matches',
                                        subField: 'match',
                                        type: 'giving',
                                        label: 'Most recent Federal Contributions',
                                        disaplyField: 'recipient',
                                        maxDisplay: 10,
                                        bDisplayTotal: true,
                                    },
                                    {
                                        section: 'features',
                                        fieldStartWith: 'State Matches',
                                        subField: 'match',
                                        type: 'giving',
                                        label: 'Most recent State Contributions',
                                        disaplyField: 'recipient',
                                        maxDisplay: 5,
                                        bDisplayTotal: true,
                                    },
                                    {
                                        section: 'features',
                                        fieldStartWith: 'Local Matches',
                                        subField: 'match',
                                        type: 'giving',
                                        label: 'Most recent Local Contributions',
                                        disaplyField: 'recipient',
                                        maxDisplay: 5,
                                        bDisplayTotal: true,
                                    }
                                ],
                            },
                        }
                    }

                    // create the call sheet
                    callSheet.create(cObj);
                })
                .fail(function(r) {
                    revupUtils.apiError('Prospects', r, 'Unable to retrieve Callsheet Data', "Unable to retrive Callsheet Data");
                })
            })
            .on('click', '.iconEmail', function(e) {
                var $this = $(this);
                var $entry = $this.closest('.entry');
                var prospectId = $entry.attr('prospectId');

                // get the cached email address
                var eAddr = $this.data("emailAddrees")

                var popup = [];
                popup.push('<div class="content">');
                    popup.push('<div class="title">Send Email To</div>');
                    popup.push('<div class="lst">');
                        if (eAddr == undefined) {
                            popup.push('<div class="loadingMsg">Loading...</div>');
                        }
                        else {
                            if (eAddr.length > 0) {
                                var sTmp = [];
                                for (var e = 0; e < eAddr.length; e++) {
                                    popup.push('<div class="entry">')
                                        popup.push('<a href="mailto:' + eAddr[e] + '">');
                                            popup.push('<div class="addr">' + eAddr[e] + '</div>');
                                        popup.push('</a>');
                                    popup.push('</div>');
                                }
                            }
                            else {
                                popup.push('<div class="msg">No email address for this contact</div>');
                            }
                        }
                    popup.push('</div>');
                popup.push('</div>');

                // get the new element just add and display pooup
                $popup = $this.revupPopup({
                    extraClass: 'prospectEmailPopup',
                    location: 'below',
                    adjUpDown: -4,
                    content: popup.join(''),
                });

                // event for the popup
                $('.lst', $popup)
                    .on('mouseenter', '.entry', function(e) {
                        $(this).addClass('selected');
                    })
                    .on('mouseleave', '.entry', function(e) {
                        $(this).removeClass('selected')
                    })
                    .on('click', '.entry a', function(e) {
                        // close the filter popup
                        $popup.revupPopup('close');
                    })


                if (eAddr == undefined) {
                    var url = templateData.getProspectDetails;
                    url = url.replace("prospectId", prospectId);
                    $.ajax({
                        url: url,
                        type: "GET"
                    })
                    .done(function(r) {
                        emailAddr = [];
                        var contactEmail = r.contact.email_addresses;
                        for (var e = 0; e < contactEmail.length; e++) {
                            emailAddr.push(contactEmail[e].address)
                        }
                        $this.data("emailAddrees", emailAddr);
                        if (emailAddr.length > 0) {
                            var sTmp = [];
                            for (var e = 0; e < contactEmail.length; e++) {
                                sTmp.push('<div class="entry">')
                                    sTmp.push('<a href="mailto:' + emailAddr[e] + '">');
                                        sTmp.push('<div class="addr">' + emailAddr[e] + '</div>');
                                    sTmp.push('</a>');
                                sTmp.push('</div>');
                            }

                            $('.content .lst', $popup).html(sTmp.join(''));
                        }
                        else {
                            $('.content .lst', $popup).html('<div class="msg">No email address for this contact</div>');
                        }
                    })
                    .fail(function(r) {
                        revupUtils.apiError('Prospects', r, 'Unable to retrieve contact emails address', "Unable to retrive contacts email address");
                        $('.content .lst', $popup).html('<div class="msg">Unable to retrieve contact email addresses at this time</div>');
                    })
                }
            })
            .on('click', '.iconDelete', function(e) {
                var $this = $(this);
                var $entry = $this.closest('.entry');
                var userName = $('.colName', $entry).text();
                var prospectId = $entry.attr('prospectId');

                $('body').revupConfirmBox({
                    headerText: 'Delete Contact',
                    msg: "Would you like to remove \"" + userName + "\" from the prospects list",
                    okHandler: function() {
                        var url = templateData.deleteProspect;
                        url = url.replace("prospectId", prospectId);
                        $.ajax({
                            url: url,
                            type: "DELETE",
                            })
                        .done(function(r) {
                            $entry.fadeOut(400, function() {
                                $entry.remove();
                                // $('.prospectsLstDiv .entry').hide().show();

                                //recount sums - BMS
                                //must recalculate each column
                                //after that, update the hard+in

                                //update hard
                                var hardSum = 0;
                                $('.prospectsLstDiv .entry').each(function(x){
                                    val = parseFloat($(".colHard input", $(this)).val());
                                    hardSum += val;
                                });
                                $(".entryFooter .colHard").text("$" + hardSum.toLocaleString());
                                // $(".entryFooter .colHard").css('font-weight', 'bold');


                                //update soft
                                var softSum = 0;
                                $('.prospectsLstDiv .entry').each(function(x){
                                    val = parseFloat($(".colSoft input", $(this)).val());
                                    softSum += val;
                                });
                                $(".entryFooter .colSoft").text("$" + softSum.toLocaleString());
                                // $(".entryFooter .colSoft").css('font-weight', 'bold');

                                //update In
                                var inSum = 0;
                                $('.prospectsLstDiv .entry').each(function(x){
                                    val = parseFloat($(".colIn input", $(this)).val());
                                    inSum += val;
                                });
                                $(".entryFooter .colIn").text("$" + inSum.toLocaleString());
                                // $(".entryFooter .colIn").css('font-weight', 'bold');


                                //update hardIn
                                var hardInSum = hardSum + inSum;
                                $(".entryFooter.hardInTotal .colSoft").text('');
                                $(".entryFooter.hardInTotal .colHard").text('');
                                $(".entryFooter.hardInTotal .colIn").text("$" + hardInSum.toLocaleString());
                                // $(".entryFooter.hardInTotal .colIn").css('font-weight', 'bold');
                            });
                            var $deletedTotal = (Number.parseInt($totalProspects.attr('prospecttotalcount')) - 1);
                            $totalProspects.attr('prospecttotalcount', $deletedTotal)
                        })
                        .fail(function(r) {
                            revupUtils.apiError('Prospects', r, 'Unable to remove prospect', "Unable to delete contact from prospects list");
                        });
                    }
                })
            });

var $slider = $();
var $left = $();
var $right = $();
var $leftGrp = $();
var $rightGrp = $();
var currentPosX;
var minLeft, minRight;
var windowWidth = $prospectLst.outerWidth();//$(window).width();
        $header
            .on('mousedown', '.slider', function(e)
            {
                // which == 1 means left mouse button
                if (e.which == 1) {
                    // get the left column
                    $slider = $(this);
                    $left = $slider.prev();
                    var c = $left.attr('class');
                    c = c.split(' ');
                    for (var i = 0; i < c.length; i++) {
                        c[i] = '.' + $.trim(c[i]);
                    }
                    $leftGrp = $(c.join(''));
                    minLeft = parseInt($(c.join(''), $('.headerDiv')).attr('minColWidth'), 10);

                    // get the right column
                    $right = $slider.next();
                    var c = $right.attr('class');
                    c = c.split(' ');
                    for (var i = 0; i < c.length; i++) {
                        c[i] = '.' + $.trim(c[i]);
                    }
                    $rightGrp = $(c.join(''));
                    minRight = parseInt($(c.join(''), $('.headerDiv')).attr('minColWidth'), 10);

                    // make sure there is room to resize
                    if (($left.width() <= minLeft) && ($right.width() <= minRight)) {
                        e.preventDefault();

                        return;
                    }

                    // set the starting point
                    currentPosX = e.pageX;

                    $slider.addClass('draggable').parents().on('mousemove', function(e) {
                        if ($slider.hasClass('draggable')) {
                            var deltaX = currentPosX - e.pageX;
                            currentPosX = e.pageX;

                            // compute the new size
                            var leftWidth = $left.outerWidth();
                            var rightWidth = $right.outerWidth();
                            var newWidthLeft = leftWidth - deltaX;
                            var newWidthRight = rightWidth + deltaX;

                            // make sure the new size meets the minimum
                            var bResizeCol = true;
                            if (newWidthLeft < minLeft) {
                                var d = leftWidth - minLeft;
                                if (d == 0) {
                                    bResizeCol = false;
                                }
                                else {
                                    newWidthLeft =  minLeft;
                                    newWidthRight = rightWidth + d;
                                }
                            }
                            else if (newWidthRight < minRight) {
                                var d = rightWidth - minRight;
                                if (d == 0) {
                                    bResizeCol = false;
                                }
                                else {
                                    newWidthRight = minRight;
                                    newWidthLeft = leftWidth + d;
                                }
                            }

                            //resize
                            if (bResizeCol) {
                                $leftGrp.outerWidth(newWidthLeft);
                                $rightGrp.outerWidth(newWidthRight);

                                // save the columns width
                                var wData = {};
                                wData.listWidth = $lst.outerWidth();
                                var colData = templateData.columns;
                                for (var i = 0; i < colData.length; i++) {
                                    // save the column width
                                    if (colData[i].css) {
                                        wData[colData[i].css] = $('.column.' + colData[i].css).outerWidth();
                                    }
                                }
                                localStorage.setItem('prospectsColWidth-' + templateData.seatId, JSON.stringify(wData));
                            }
                        }
                    }).on('mouseup', function(e) {
                        // stop dragging
                        $slider.removeClass('draggable')
                    })
                }

                e.preventDefault();
            })
            .on('mouseup', function() {
                $('.draggable').removeClass('draggable')
            });

        var resizeTimer = undefined;
        $(window)
            .on('resize', function(e) {
                if (localStorage.getItem('prospectsColWidth-' + templateData.seatId)) {
                    if (resizeTimer) {
                        clearTimeout(resizeTimer);
                    }

                    resizeTimer = setTimeout(function() {
                        // resize the columns
                        var bMinHit = false;
                        var colData = templateData.columns;

                        // get the width of the static columns
                        var sWidth = 0;
                        for (var i = 0; i < colData.length; i++) {
                            if (!colData[i].resizePercent) {
                                var $col = $('.lstDiv .column.' + colData[i].css);
                                var cWidth = $col.width();
                                sWidth += cWidth;
                            }
                        }

                        // adjust for margins and sliders
                        sWidth += colData.length * 20;      // margin
                        sWidth += (colData.length - 1) * 5; // sliders

                        for (var i = 0; i < colData.length; i++) {
                            // see if a resizeable column
                            if (colData[i].resizePercent) {
                                var newWidth = $('.lstDiv').width();
                                var wDelta = newWidth - (sWidth + 20);
                                var resizePercent = colData[i].resizePercent / 100;
                                var w = Math.floor(wDelta * resizePercent);
                                var $col = $('.prospectsLstDiv .column.' + colData[i].css);
                                // $col.width(w);

                                // if the resizeable column gets to small reset columns
                                if (w < colData[i].minColWidth) {
                                    bMinHit = true;
                                    break;
                                }
                                else {
                                    $col.width(w);
                                }
                            }
                        }

                        if (bMinHit) {
                            for (var j = 0; j < colData.length; j++) {
                                var $col = $('.prospectsLstDiv .column.' + colData[j].css);
                                $col.css('width', colData[j].width)
                            }
                        }

                        // save the columns width
                        var wData = {};
                        wData.listWidth = $lst.outerWidth();
                        var colData = templateData.columns;
                        for (var i = 0; i < colData.length; i++) {
                            // save the column width
                            if (colData[i].css) {
                                wData[colData[i].css] = $('.column.' + colData[i].css).outerWidth();
                            }
                        }
                        localStorage.setItem('prospectsColWidth-' + templateData.seatId, JSON.stringify(wData));
                    }, 100);
                }
            })
    } // addEventHandlers

    function updateValues($field)
    {
        // if there is an error then skip
        if ($field.hasClass("inputFormatError")) {
            return;
        }

        // get the current value and the initial value and if the same do nothing
        var val = $field.val();
        var initVal = $field.attr("initVal");
        if (val == initVal) {
            return;
        }

        // get the prospect id
        var $entry = $field.closest(".entry");
        var prospectId = $entry.attr("prospectId");

        // build the data to send with the update
        var data = {}
        var val = $field.val();
        switch ($field.attr('dataFor')) {
            case "numAttending":
                data.num_attending = val;
                break;
            case "softAmount":
                data.soft_amt = val;
                break;
            case "hardAmount":
                data.hard_amt = val;
                break;
            case "inAmount":
                data.in_amt = val;
                break;
            default:
                console.error("Unknow input type field: " + $field.attr('dataFor'));
                break;
        } // end switch - $field.attr('dataFor')

        // make the ajax update call
        var url = templateData.updateProspect;
        url = url.replace('prospectId', prospectId);
        $.ajax({
            url: url,
            type: "PUT",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            })
        .done(function(r) {
            // update the attend, soft, hard, in fields
            // attend
            $('input[dataFor=numAttending]', $entry).val(r.num_attending)
                                                     .attr("initVal", r.num_attending);

            // soft amount
            $('input[dataFor=softAmount]', $entry).val(r.soft_amt)
                                                   .attr("initVal", r.soft_amt);

            // hard amount
            $('input[dataFor=hardAmount]', $entry).val(r.hard_amt)
                                                   .attr("initVal", r.hard_amt);

            // in amount
            $('input[dataFor=inAmount]', $entry).val(r.in_amt)
                                                 .attr("initVal", r.in_amt);

            // recompute totals
            computeTotals();
        })
        .fail(function(r) {
            // revupSecondaryBtnColor the attend, soft, hard, in fields
            // attend
            var $input = $('input[dataFor=numAttending]', $entry);
            $input.val($input.attr("initVal"));

            // soft amount
            var $input = $('input[dataFor=softAmount]', $entry);
            $input.val($input.attr("initVal"));

            // hard amount
            var $input = $('input[dataFor=hardAmount]', $entry);
            $input.val($input.attr("initVal"));

            // soft amount
            var $input = $('input[dataFor=inAmount]', $entry);
            $input.val($input.attr("initVal"));

            revupUtils.apiError('Prospects', r, 'Unable to update contact prospects data', "Unable to save changes to contact");
        });
    } // updateValues

    /*
     * Button bar
     */
    function buttonBarEvenHandlers()
    {
        $('.buttonBar .hamburgerBtn').on('click', function(e) {
            $btn = $(this);

            // see if disabled
            if ($btn.hasClass('disabled'))
                return;

            var sTmp = [];

            sTmp.push('<div class="callSheetGrpMenuDiv">');
                if(features.CSV_EXPORT_PROSPECTS) {
                    sTmp.push('<div class="entry exportCallSheetGrp">');
                }
                else {
                    sTmp.push('<div class="entry exportCallSheetGrp disabled">');
                }
                sTmp.push('<div class="entry exportCallSheetGrp">');
                    sTmp.push('<div class="text">Export to CSV</div>');
                    sTmp.push('<div class="icon icon-export-csv"></div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            let $popup = $btn.revupPopup({
                extraClass: 'prospectsMenu',
                location: 'below',
                content: sTmp.join(''),
                minLeft: 145,
                //maxRight: 415,
                bPassThrough: false,
            });

            if($totalProspects.attr('prospecttotalcount') == 0){
                ($('.exportCallSheetGrp').addClass('disabled')).css('pointer-events', 'none');
            }

            $('.exportCallSheetGrp', $popup).on('click', function(e) {
                // close the menu
                $popup.revupPopup('close');

                var sTmp = [];
                sTmp.push('<div class="prospectsCsvDownloadDiv">');
                    sTmp.push('<div class="textMsg">');
                        sTmp.push('To export a copy of your prospects as a CSV file, select ');
                        sTmp.push('the attributes you want included in the file and click save.  The ');
                        sTmp.push('prospect\'s name is always included by default.');
                    sTmp.push('</div>');

                    sTmp.push('<div class="header" style="margin-top:10px;">Format options</div>');
                    var extraClass = "";
                    if (!templateData.bDisplayQuickFilters) {
                        extraClass = " disabled"
                    }
                    sTmp.push('<div style="margin-top:5px;" class="entry' + extraClass + '">');
                        sTmp.push('<input id="checkbox1" class="csvIncludeName" type="checkbox" checked disabled>');
                        sTmp.push('<label for="checkbox1" class="label"><span><span></span></span>Name</label>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="entry" style="margin-top:5px;">');
                        sTmp.push('<input id="checkbox2" type="checkbox" class="csvIncludeScore" checked>');
                        sTmp.push('<label for="checkbox2" class="label"><span><span></span></span>Score</label>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="entry" style="margin-top:5px;">');
                        sTmp.push('<input id="checkbox3" type="checkbox" class="csvIncludeContactInfo" checked>');
                        sTmp.push('<label for="checkbox3" class="label"><span><span></span></span>Contact Infomation</label>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="entry" style="margin-top:5px;">');
                        sTmp.push('<input id="checkbox4" type="checkbox" class="csvProspectColumns" checked>');
                        sTmp.push('<label for="checkbox4" class="label"><span><span></span></span>Plege Data</label>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="entry" style="margin-top:5px;">');
                        sTmp.push('<input id="checkbox5" type="checkbox" class="csvIncludeTotalNotes" checked>');
                        sTmp.push('<label for="checkbox5" class="label"><span><span></span></span>Notes</label>');
                    sTmp.push('</div>');
                sTmp.push('</div>');

                var $dlg = $('body').revupDialogBox({
                    headerText: "Save Call Sheet Group as a CSV File",
                    msg: sTmp.join(''),
                    autoCloseOk: false,
                    waitingOverlay: true,
                    waitingStyleShow: false,
                    okHandler: function (e) {
                        // display the waiting
                        $dlg.revupDialogBox('showWaiting');
                        $dlg.revupDialogBox('disableBtn', 'okBtn');
                        $dlg.revupDialogBox('disableBtn', 'cancelBtn');

                        // build the arguments
                        var urlArgs = [];
                        urlArgs.push('show_name=' + $('.csvIncludeName', $dlg).prop('checked'));
                        urlArgs.push('show_score=' + $('.csvIncludeScore', $dlg).prop('checked'));
                        urlArgs.push('show_contact_info=' + $('.csvIncludeContactInfo', $dlg).prop('checked'));
                        urlArgs.push('show_prospect_data=' + $('.csvProspectColumns', $dlg).prop('checked'));
                        urlArgs.push('show_notes=' + $('.csvIncludeTotalNotes', $dlg).prop('checked'));

                        // build the url
                        var url2 = templateData.exportCSVProspects;

                        // start the download
                        $.ajax({
                            type: 'GET',
                            url:  encodeURI(url2 + '?' + urlArgs.join('&'))
                        })
                        .done(function(result) {
                            $dlg.revupDialogBox('close');
                            $('body').revupMessageBox({
                                headerText:'CSV Export',
                                msg: "Your CSV file is being generated.<br>" +
                                     "<span style='font-weight: bold'>You will receive an email when it is ready for download.</span>" +
                                     "<br><br>Note: Depending on the size of the export, this may take several minutes."
                            });
                            updateRevupAlert();
                        })
                        .fail(function(result) {
                            // hide the waiting
                            $dlg.revupDialogBox('hideWaiting');
                            $dlg.revupDialogBox('enableBtn', 'okBtn');
                            $dlg.revupDialogBox('enableBtn', 'cancelBtn');
                            // display the error
                            revupUtils.apiError('Save CSV', result, 'Unable to create CSV file at this time',
                                                'Unable to create the CSV file at this time');
                        })
                    }
                });
            }) // exportCSV click handler;

        })
    } // buttonBarEvenHandlers


    return {
        load: function(pTemplateData, $containerDiv) {
            templateData = pTemplateData;

            // button bar event handlers
            buttonBarEvenHandlers();

            // build the list section
            buildListSection($containerDiv);

            // get the data
            $.ajax({
                type: 'GET',
                url: pTemplateData.getProspects,
            })
            .done(function(r) {
                // display
                buildLstData(r.results);

                // add the event handlers
                addEventHandlers();
            })
            .fail(function(r) {
                console.error("Unable to load prospects data: ", r)
                var sTmp = [];

                sTmp.push('<div class="loadingError">');
                    sTmp.push('<div class="msg">Unable to load Prospects data at this time.<br>Please try again shortly.</div>')
                sTmp.push('</div>');

                $containerDiv.html(sTmp.join(''));
            })
            .always(function(r) {
                var colWidth = localStorage.getItem('prospectsColWidth-' + templateData.seatId)
                colWidth = JSON.parse(colWidth);
                if (colWidth && (colWidth.listWidth != $header.width())) {
                    // resize the columns
                    var bMinHit = false;
                    var colData = templateData.columns;

                    // get the width of the static columns
                    var sWidth = 0;
                    for (var i = 0; i < colData.length; i++) {
                        if (!colData[i].resizePercent) {
                            var $col = $('.lstDiv .column.' + colData[i].css);
                            var cWidth = $col.width();
                            sWidth += cWidth;
                        }
                    }

                    // adjust for margins and sliders
                    sWidth += colData.length * 20;      // margin
                    sWidth += (colData.length - 1) * 5; // sliders

                    for (var i = 0; i < colData.length; i++) {
                        // see if a resizeable column
                        if (colData[i].resizePercent) {
                            var newWidth = $('.lstDiv').width();
                            var wDelta = newWidth - (sWidth + 20);
                            var resizePercent = colData[i].resizePercent / 100;
                            var w = Math.floor(wDelta * resizePercent);
                            var $col = $('.prospectsLstDiv .column.' + colData[i].css);
                            // $col.width(w);

                            // if the resizeable column gets to small reset columns
                            if (w < colData[i].minColWidth) {
                                bMinHit = true;
                                break;
                            }
                            else {
                                $col.width(w);
                            }
                        }
                    }

                    if (bMinHit) {
                        for (var j = 0; j < colData.length; j++) {
                            var $col = $('.prospectsLstDiv .column.' + colData[j].css);
                            $col.css('width', colData[j].width)
                        }
                    }
                }
            })
        }
    }
} ());
