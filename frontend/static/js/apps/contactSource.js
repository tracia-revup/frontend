"use strict";
var contactSource = (function (){
    /*
     *
     * List of variables needed
     *
     */
    // templateData.revupLogo
    // templateData.loadContactsGmail : importing from the gmail source

    var templateData = {};
    let userContactsMsg = '';               // what type of message
    let adminContactsMsg = '';              // the type of admin contact message

    let personalContactsAccess = false;
    let accountContactsAccess = false;

    /*
     * Display the Add Contact Dialog
     */

    function displayAddContact(src, templateData)
    {
        var sTmp = [];
        sTmp.push('<div class="addContactDlg">');
            sTmp.push('<div class="header">');
                sTmp.push('<div class="logo">');
                    sTmp.push('<img class="revupLogo" src="' + templateData.revupLogo + '">');
                sTmp.push('</div>');
                sTmp.push('<div class="msg">');
                    sTmp.push('Adding contacts to RevUp is safe and easy.  Start by selecting your contact source.');
                sTmp.push('</div>');
                sTmp.push('<div class="selector">');
                    sTmp.push('<div style="display:inline-block">');
                    if (!features.IMPORT_GMAIL) {
                        sTmp.push('<div class="app appGmail disabled">');
                            sTmp.push('<div class="appContainer">');
                                sTmp.push('<img class="appIcon" src="/static/img/contactsourceicons/gmail-disabled.svg"></>');
                    }
                    else {
                        sTmp.push('<div class="app appGmail">');
                            sTmp.push('<div class="appContainer">');
                                sTmp.push('<img class="appIcon" src="/static/img/contactsourceicons/gmail.svg"></>');
                    }
                            sTmp.push('</div>');
                        sTmp.push('<div class="appLabel">Gmail</div>');
                    sTmp.push('</div>');

                    if (!features.IMPORT_OUTLOOK) {
                        sTmp.push('<div class="app appOutlook disabled">');
                            sTmp.push('<div class="appContainer">');
                                sTmp.push('<img class="appIcon" class="appIcon" src="/static/img/contactsourceicons/outlook-disabled.svg"></>');
                    }
                    else {
                        sTmp.push('<div class="app appOutlook">');
                            sTmp.push('<div class="appContainer">');
                                sTmp.push('<img class="appIcon" class="appIcon" src="/static/img/contactsourceicons/outlook.svg"></>');
                    }
                            sTmp.push('</div>');
                        sTmp.push('<div class="appLabel">Outlook</div>');
                    sTmp.push('</div>');

                    if (!features.IMPORT_VCARD) {
                        sTmp.push('<div class="app appVCard disabled">');
                            sTmp.push('<div class="appContainer">');
                                sTmp.push('<img class="appIcon" src="/static/img/contactsourceicons/apple-disabled.svg"></>');
                    }
                    else {
                        sTmp.push('<div class="app appVCard">');
                            sTmp.push('<div class="appContainer">');
                                sTmp.push('<img class="appIcon" src="/static/img/contactsourceicons/apple.svg"></>');
                    }
                            sTmp.push('</div>');
                        sTmp.push('<div class="appLabel">VCard</div>');
                    sTmp.push('</div>');

                    if (!features.IMPORT_CSV) {
                        sTmp.push('<div class="app appCSV disabled">');
                            sTmp.push('<div class="appContainer">');
                                sTmp.push('<img class="appIcon" src="/static/img/contactsourceicons/csv-disabled.svg"></>');
                    }
                    else {
                        sTmp.push('<div class="app appCSV">');
                            sTmp.push('<div class="appContainer">');
                                sTmp.push('<img class="appIcon" src="/static/img/contactsourceicons/csv.svg"></>');
                    }
                            sTmp.push('</div>');
                        sTmp.push('<div class="appLabel">CSV</div>');
                    sTmp.push('</div>');

                    if (!features.IMPORT_MOBILE) {
                        sTmp.push('<div class="app appMobile disabled">');
                            sTmp.push('<div class="appContainer">');
                                sTmp.push('<img class="appIcon" src="/static/img/contactsourceicons/mobile-disabled.svg"></>');
                    }
                    else {
                        sTmp.push('<div class="app appMobile">');
                            sTmp.push('<div class="appContainer">');
                                sTmp.push('<img class="appIcon" src="/static/img/contactsourceicons/mobile.svg"></>');
                    }
                            sTmp.push('</div>');
                        sTmp.push('<div class="appLabel">Mobile</div>');
                    sTmp.push('</div>');
                        /*
                        sTmp.push('<div class="app appFacebook">');
                            sTmp.push('<div class="appContainer">');
                                sTmp.push('<div class="appIcon facebookGlyph"></div>');
                            sTmp.push('</div>');
                            sTmp.push('<div class="appLabel">Facebook</div>');
                        sTmp.push('</div>');
                        */
                    sTmp.push('</div>');
                sTmp.push('</div>');
                sTmp.push('<div class="bottonMsg">');
                    sTmp.push('Your contacts are your contacts! By using RevUp we understand that these are ');
                    sTmp.push('all people that you personally know, and don&#39;t want us to be careless with. We will ');
                    sTmp.push('never copy, retain, or do anything with any of your data.');
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        var $addContactDlg = $('body').revupDialogBox({
            okBtnText:          'Close',
            okHandler:          function() {
                                    $addContactDlg.revupDialogBox('close');
                                },
            cancelBtnText:      '',
            msg:                sTmp.join(''),
            extraClass:         'addContact',
            zIndex:             50,
            width:              '500px',
            isDragable:         false,
        });

        $addContactDlg
            .on('click', '.app.appGmail', function(e) {
                var x = screen.width/2 - 600/2;
                var y = screen.height/2 - 650/2;
                y = 100;
                //TODO: implement a radio button so that user can choose to share the contacts
                var temp_url = templateData.loadContactsGmail + '?account_contacts=false'
                window.open(temp_url, 'auth', "toolbars=0, scrollbars=1, location=0, statusbars=0, menubars=0, resizable=0, width=600,height=650,left=" + x + ",top=" + y);

                $addContactDlg.revupDialogBox('close');
                //getSyncStatus();
            })
            .on('click', '.app.appOutlook', function(e) {
                cloudsponge.init({csvProviders: {outlook: true}});
                cloudsponge.launch('outlook');

                $addContactDlg.revupDialogBox('close');
            })
            .on('click', '.app.appVCard', function(e) {
                cloudsponge.init({csvProviders: {addressbook: true}});
                cloudsponge.launch('addressbook');

                $addContactDlg.revupDialogBox('close');
            })
            .on('click', '.app.appCSV', function(e) {
                if (userContactsMsg == '' && adminContactsMsg == '') {
                    if (!personalContactsAccess && !accountContactsAccess) {
                        console.error('User has no permission to this operation.');
                        $('body').revupMessageBox({
                            headerText: 'Permission Error',
                            msg: 'Permission error. Please contact support at ' + '<a href="mailto:support@revup.com">support@revup.com</a>'
                        });
                    }
                    else {
                        csvValidator.load(false, templateData);
                    }

                    $addContactDlg.revupDialogBox('close');
                }
                else {
                    let sTmp = [];
                    sTmp.push('<div class="contactUploadWarningDlg">');
                        sTmp.push('<div class="header">');
                            sTmp.push('<div class="logo">');
                                sTmp.push('<img class="revupLogo" src="' + templateData.revupLogo + '">');
                            sTmp.push('</div>');
                            let msg = '';
                            let icon = '';

                            // user contact message
                            if (userContactsMsg == 'error') {
                                icon = 'icon-stop';

                                msg = 'You have reached your limit of ' + revupUtils.commify(userContactsLimits) + ' personal contacts. You man still add details to ';
                                msg += 'existing contact information via upload, but no new contacts will be created.'
                            }
                            else if (userContactsMsg = 'warning') {
                                icon = 'icon-warning';
                                msg = 'You are nearing your limit of ' + revupUtils.commify(userContactsLimits) + ' analyzed personal account contacts.';
                            }

                            if (userContactsMsg != '') {
                                sTmp.push('<div class="contactErrorWarningMsg ' + userContactsMsg + '">');
                                    sTmp.push('<div class="msgBox">');
                                        sTmp.push('<div class="textOuterDiv">');
                                            sTmp.push('<div class="textCenterDiv">')
                                                sTmp.push('<div class="textDiv">');
                                                    sTmp.push('<div class="icon ' + icon + '"></div>');
                                                    sTmp.push('<div class="text">');
                                                        sTmp.push(msg);
                                                    sTmp.push('</div>');
                                                sTmp.push('</div>');
                                            sTmp.push('</div>')
                                        sTmp.push('</div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            }

                            // account/admin contact message
                            if (adminContactsMsg == 'error') {
                                icon = 'icon-stop';

                                msg = 'You have reached your limit of ' + revupUtils.commify(adminContactsLimits) + ' shared contacts. You man still add details to ';
                                msg += 'existing contact information via upload, but no new contacts will be created.'
                            }
                            else if (adminContactsMsg == 'warning') {
                                icon = 'icon-warning';
                                msg = 'You are nearing your limit of ' + revupUtils.commify(adminContactsLimits) + ' analyzed shared account contacts.';
                            }

                            if (adminContactsMsg != '') {
                                sTmp.push('<div class="contactErrorWarningMsg ' + adminContactsMsg + '">');
                                    sTmp.push('<div class="msgBox">');
                                        sTmp.push('<div class="textOuterDiv">');
                                            sTmp.push('<div class="textCenterDiv">')
                                                sTmp.push('<div class="textDiv">');
                                                    sTmp.push('<div class="icon ' + icon + '"></div>');
                                                    sTmp.push('<div class="text">');
                                                        sTmp.push(msg);
                                                    sTmp.push('</div>');
                                                sTmp.push('</div>');
                                            sTmp.push('</div>')
                                        sTmp.push('</div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            }

                            sTmp.push('<div class="msg">');
                                sTmp.push('By uploading this file you may exceed our analyzed contact limits.  Any duplicate ');
                                sTmp.push('contacts to existing contacts do not count against the limit.  Once the limit is reached ');
                                sTmp.push('any additional contacts will be ignored.')
                            sTmp.push('</div>');
                        sTmp.push('</div>')
                    sTmp.push('</div>');

                    let $contactUploadWarningDlg = $('body').revupDialogBox({
                        okBtnText: 'Upload',
                        okHandler: function() {
                            csvValidator.load(false, templateData);

                            $addContactDlg.revupDialogBox('close');
                            $contactUploadWarningDlg.revupDialogBox('close');
                        },
                        msg: sTmp.join(''),
                        extraClass: 'contactUploadWarning',
                        zIndex: 75,
                        width: '600px',
                        isDragable: false,

                    })

                }
                //getSyncStatus();
            })
            .on('click', '.app.appFacebook', function(e) {
                facebookDlg();

                $addContactDlg.revupDialogBox('close');
                //getSyncStatus();
            })
            .on('click', '.app.appMobile', function(e) {
                mobileDlg();

                $addContactDlg.revupDialogBox('close');
                //getSyncStatus();
            });
    } // displayAddContact

    function facebookDlg()
    {
        var sTmp = [];
        sTmp.push('<div class="facebookDlg">');
            sTmp.push('<div class="appIconDiv">');
                sTmp.push('<div class="iconFacebook"></div>');
            sTmp.push('</div>');
            sTmp.push('<div class="boldMsg">Exporting Facebook contacts is done<br>utilizing the RevUp app on your iPhone</div>')
            sTmp.push('<div class="appleAppStoreDiv">');
                sTmp.push('<a href="https://itunes.apple.com/us/app/revup-mobile/id1201381264?mt=8" target="_blank">');
                    sTmp.push('<div class="iconAppleAppStore"></div>');
                sTmp.push('</a>');
            sTmp.push('</div>');
            sTmp.push('<ul>');
                sTmp.push('<li>Go to "Settings" on your iPhone.  Scroll down to "Facebook" settings and open it.</li>');
                sTmp.push('<li>Enter your Facebook email and password.  Then click "Sign In".</li>');
                sTmp.push('<li>Tap "Update All Contacts" to sync Facebook contacts with your iPhone.');
                sTmp.push('<li>Go to the App store on your iPhone, and install the RevUp iOS app.</li>');
                sTmp.push('<li>Using the RevUp app, upload your Apple contacts. Your Facebook contacts will be included.</li>');
            sTmp.push('</ul>');
        sTmp.push('</div>');

        var $dlg = $('body').revupDialogBox({
            okBtnText:          'Done',
            okHandler:          function() {
                                    $dlg.revupDialogBox('close');
                                },
            cancelBtnText:      'Back',
            cancelHandler:      function() {
                                    displayAddContact(false, templateData)
                                },
            msg:                sTmp.join(''),
            //extraClass:         'addContact',
            zIndex:             50,
            width:              '510px',
            isDragable:         false,
        });
    } // facebookDlg


    function mobileDlg()
    {
        var sTmp = [];
        sTmp.push('<div class="mobileDlg">');
            sTmp.push('<div class="appIconDiv">');
                sTmp.push('<div class="iconMobile"></div>');
            sTmp.push('</div>');
            sTmp.push('<div class="boldMsg">If you have not done so already, download and install the RevUp iPhone application from the iTunes store.</div>')
            sTmp.push('<div class="appleAppStoreDiv">');
                sTmp.push('<a href="https://itunes.apple.com/us/app/revup-mobile/id1201381264?mt=8" target="_blank">');
                    sTmp.push('<div class="iconAppleAppStore"></div>');
                sTmp.push('</a>');
            sTmp.push('</div>');
            sTmp.push('<ul>');
                sTmp.push('<li>Once installed login to your account.</li>');
                sTmp.push('<li>Tap on the upload contacts button.</li>');
                sTmp.push('<li>Tap on the apple button and your contacts will be uploaded.</li>')
                sTmp.push('</ul>');
        sTmp.push('</div>');

        var $dlg = $('body').revupDialogBox({
            okBtnText:          'Done',
            okHandler:          function() {
                                    $dlg.revupDialogBox('close');
                                },
            cancelBtnText:      'Back',
            cancelHandler:      function() {
                                    displayAddContact(false, templateData)
                                },
            msg:                sTmp.join(''),
            //extraClass:         'addContact',
            zIndex:             50,
            width:              '510px',
            isDragable:         false,
        });
    } // mobileDlg

    return {
        load: function(src, ptemplateData){
            templateData = ptemplateData;

            personalContactsAccess = features.PERSONAL_CONTACTS;
            accountContactsAccess = features.ACCOUNT_CONTACTS && templateData.hasAdminAccess;

            if(templateData) {
                displayAddContact(src, templateData);
            }
        }
    }
} ());
