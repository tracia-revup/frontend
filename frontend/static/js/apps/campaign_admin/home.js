/*global $, document, add_user_link, upload_external_data_link, FormData*/

$(document).ready(function() {
    var $submit_email_alert = $('#id-email-submit-alert');
    var submit_email_button = $("#id_email_submit_button");
    var add_user_form = $("#id-add-user-form");
    var $email_input = $('#id_email_input');

    $submit_email_alert.hide();

    $('#id_email_input')
        .off('blur')
        .on('blur', function() {
            if ($(this).val() == '') {
                $submit_email_alert.hide();
                $submit_email_alert.removeClass('alert-danger');
                $('.alert-content', $submit_email_alert).html('');

                return;
            }

            var isValidEmail = validEmailAddress($(this).val());
            if (!isValidEmail) {
                $submit_email_alert.show();
                $submit_email_alert.addClass('alert-danger');
                $('.alert-content', $submit_email_alert).html('Invalid Email Address');
            }
            else {
                $submit_email_alert.hide();
                $submit_email_alert.removeClass('alert-danger');
                $('.alert-content', $submit_email_alert).html('');
            }
        });

    $email_input.off('keypress')
                .on('keypress', function() {
                    $submit_email_alert.hide();
                    $submit_email_alert.removeClass('alert-danger');
                    $('.alert-content', $submit_email_alert).html('');
                })

    add_user_form.submit(function(e){
        // Disable the button so they don't click it multiple times
        submit_email_button.prop('disabled', true);

        function updateAlert(msg, alertType, text){
            if (msg.responseJSON){
                msg = ": " + msg.responseJSON.detail;
            }else if (msg.hasOwnProperty('detail')){
                msg = ": " + msg.detail;
            }else{
                msg = '';
            }
            $submit_email_alert.removeClass();
            $submit_email_alert.find(".alert-content").html(text + msg);
            $submit_email_alert.addClass(alertType + " in alert");
            $submit_email_alert.show();
        }

        // validate the email address
        var eAddr = $('#id_email_input').val();
        if (eAddr == '') {
            submit_email_button.prop('disabled', false);
            e.preventDefault();
            return false;
        }
        var isValidEmail = validEmailAddress(eAddr);
        if (!isValidEmail) {
            $submit_email_alert.show();
            $submit_email_alert.addClass('alert-danger');
            $submit_email_alert.find(".alert-content").html('Invalid Email Address');

            submit_email_button.prop('disabled', false);
            e.preventDefault();

            return false;
        }

        // Post the email to the campaign users API to add/create the user
        $.ajax({
            url: account_seats_api,
            data: add_user_form.serialize(),
            type: "POST"
        })
        .done(function(msg){
            updateAlert(msg, "alert-success", "Success");
            add_user_form.find('#id_email_input').val("");
        })
        .fail(function(msg){
            updateAlert(msg, "alert-danger", "Failed to add fundraiser");
        })
        .always(function(){
            // Reenable the submit button now that the query is done
            submit_email_button.prop('disabled', false);
        });

        // Stops the default submit behavior (would reload page)
        return false;
    });

    $('#id-add-fundraiser-modal').on('hidden.bs.modal', function () {
        $submit_email_alert.hide();
        add_user_form.find('#id_email_input').val("");
    });

    if ($.fn.select2) {
        $("#upload-external-data select").select2({
            dropdownCssClass: "hide-icon",
            width: "160px",
        });
    }

    $("#upload-external-data form").submit(function(e) {
        e.preventDefault();
        var source = $(this).find("select").val();
        var files = $(this).find("input[type='file']").get(0).files;
        if (!files.length) {
            $("#upload-external-data-status")
                .text("Please select a file.")
                .css("color", "darkred");
            return;
        }
        $(this).find("button[type='submit']")
            .prop("disabled", true);
        $("#upload-external-data-status")
            .text("Uploading data, please wait.")
            .css("color", "darkblue");
        var data = new FormData();
        data.append("source", source);
        data.append("file", files[0]);
        $.ajax({
            cache       : false,
            contentType : false,
            data        : data,
            processData : false,
            timeout     : 10000,
            type        : "POST",
            url         : upload_external_data_link,
        }).done(function() {
            $("#upload-external-data-status")
                .text("Sucessfully uploaded data.")
                .css("color", "darkgreen");
            $("#upload-external-data input[type='file']")
                .val("");
        }).fail(function() {
            $("#upload-external-data-status")
                .text("Failed to upload data.")
                .css("color", "darkred");
        }).always(function() {
            $("#upload-external-data button[type='submit']")
                .prop("disabled", false);
        });
    });
});
