"use strict";
var csvValidator = (function (){
    /*
     *
     * List of variables needed
     *
     */
    // templateData.csvConfigFile :
    // templateData.hasAdminAccess: whether the user has admin access
    // templateData.accountTitle : campaign title
    // templateData.csvSample : opening a new browser tab for a sample csv file
    // templateData.csvUpload : to start uploading a validated csv file
    // templateData.csvHelp : opening a new browser tab for csv help

    var templateData = {};
    let personalContactsAccess = false;
    let accountContactsAccess = false;

    // csv config validate
    let contactCSV = null;

    // csv analysis
    let maxCsvToAnalyze = 2500;         // maximum number of to find
    let maxCsvToDisplay = 2500;         // maximum number of error to display

    // build the CSV upload dialog
   function csvUploadDlg(errorMsgLst)
   {
       var msg = [];

       let modeClass = "noAdmin";
       if (templateData.hasAdminAccess) {
           modeClass = "hasAdmin";
       }

       msg.push('<div class="csvUploadDiv ' + modeClass + '">');
           msg.push('<div class="contentVisibilityDiv">');
               msg.push('<div class="header">CSV Validation</div>')
               msg.push('<div class="stepsBlockDiv">')
                   msg.push('<div class="leftDiv">');
                       msg.push('<div class="stepDiv">');
                           msg.push('<div class="stepNum">1.</div>');
                           msg.push('<div class="stepContentDiv">');
                               msg.push('<div class="stepContentMsg">');
                                   msg.push('Start by selecting your csv file via the "Choose File" button.');
                               msg.push('</div>');
                           msg.push('</div>');
                       msg.push('</div>');

                       msg.push('<div class="stepDiv">');
                           msg.push('<div class="stepNum">2.</div>');
                           msg.push('<div class="stepContentDiv">');
                               msg.push('<div class="stepContentMsg">');
                                   msg.push('Once selected we will automatically check your files for errors.');
                               msg.push('</div>');
                           msg.push('</div>');
                       msg.push('</div>');

                       msg.push('<div class="stepDiv">');
                           msg.push('<div class="stepNum">3.</div>');
                           msg.push('<div class="stepContentDiv">');
                               msg.push('<div class="stepContentMsg">');
                                   msg.push('If we find any incompatibilities we will also display what action you can take to correct them.');
                               msg.push('</div>');
                           msg.push('</div>');
                       msg.push('</div>');

                       msg.push('<div class="stepDiv">');
                           msg.push('<div class="stepNum">4.</div>');
                           msg.push('<div class="stepContentDiv">');
                               msg.push('<div class="stepContentMsg">');
                                   msg.push('If you choose to correct the issues, please be sure to reselect your csv file again for revalidation and upload.');
                               msg.push('</div>');
                           msg.push('</div>');
                       msg.push('</div>');
                   msg.push('</div>');
                   if (personalContactsAccess && accountContactsAccess) {
                       msg.push('<div class="rightDiv">');
                           msg.push('<div class="stepDiv">');
                               msg.push('<div class="stepNum">5.</div>');
                               msg.push('<div class="stepContentDiv">');
                                   msg.push('<div class="selectorDiv">');
                                       msg.push('<div class="stepContentMsg">');
                                           msg.push('<strong>Select contact visibility, these contacts should be visible</strong>');
                                       msg.push('</div>');
                                       msg.push('<div class="stepContentBox">');
                                           msg.push('<div class="radioEntry">');
                                               msg.push('<div class="icon icon-radio-button-off radioBtn" radioGrp="contactVis" value="onlyMe"></div>');
                                               msg.push('<div class="icon icon-radio-button-back radioBackground"></div>');
                                               msg.push('<div class="radioLabel" radioGrp="contactVis">To only me, with the ability to share them at a later date.</div>');
                                           msg.push('</div>');
                                           msg.push('<div class="radioEntry">');
                                               msg.push('<div class="icon icon-radio-button-on radioBtn selected" radioGrp="contactVis" value="everyone"></div>');
                                               msg.push('<div class="icon icon-radio-button-back radioBackground"></div>');
                                               msg.push('<div class="radioLabel" radioGrp="contactVis" style="line-height:1.2em;">');
                                                   msg.push('To all administrators of the "');
                                                   msg.push(templateData.accountTitle);
                                                   msg.push('" account and I will have the ability to share them with other admins and utilize them in Call Time.');
                                               msg.push('</div>');
                                           msg.push('</div>');
                                       msg.push('</div>');
                                   msg.push('</div>');
                               msg.push('</div>');
                           msg.push('</div>');
                       msg.push('</div>');
                   }
               msg.push('</div>');
           msg.push('</div>');
       msg.push('</div>');
       msg.push('<form method="post" xaction="' + templateData.csvUpload + '" enctype="multipart/form-data" id="id_csv_form2" role="form">');
           /*
           // display error message is there are any
           if ((errorMsgLst !== undefined) && (errorMsgLst.length > 0)) {
               msg.push('<div class="errorMsgDiv">');
                   msg.push('<div class="msgIconDiv">');
                       msg.push('<div class="icon icon-close"></div>');
                   msg.push('</div>');
                   msg.push('<div class="textDiv">');
                       for(var i = 0; i < errorMsgLst.length; i++) {
                           msg.push('<div class="msgText">');
                               msg.push(errorMsgLst[i]);
                           msg.push('</div>');
                       }
                   msg.push('</div>');
               msg.push('</div>');
           }
           */

           msg.push('<div class="csvAnalysisDiv">');
               msg.push('<div class="buttonBar">');
                   msg.push('<div class="leftSide">')
                       msg.push('<input type="file" name="file" style="display:none;" id="id_file" onclick="this.value=null;" accept=".csv"/>');
                       msg.push('<div class="btnCntl csvChooseFile" onclick="stylizedFilePressed(this, $(\'#id_file\'))">');
                           msg.push('<div class="txt">Choose File</div>');
                           msg.push('<div class="icon icon-choose-file"></div>');
                       msg.push('</div>');
                       msg.push('<div class="btnCntl csvPrint">');
                           msg.push('<div class="txt">Print</div>');
                           msg.push('<div class="icon icon-print"></div>');
                       msg.push('</div>');
                       msg.push('<div class="btnCntl csvFormatHelp">');
                           msg.push('<div class="txt">Format Help</div>');
                           msg.push('<div class="icon icon-format-help"></div>');
                       msg.push('</div>');
                       msg.push('<div class="btnCntl csvSampleFile">');
                           msg.push('<a href="' + templateData.csvSample + '" target="_blank" download>');
                               msg.push('<div class="txt">Sample</div>');
                               msg.push('<div class="txt">CSV</div>');
                               msg.push('<div class="txt">File</div>');
                           msg.push('</a>');
                       msg.push('</div>');
                       msg.push('<div class="filterDiv">');
                           msg.push('<div class="header">Hide</div>');
                           msg.push('<div class="filterSection">');
                               msg.push('<div class="checkboxDiv checkboxHideError disabled">');
                                   msg.push('<div class="checkboxIcon icon icon-box-unchecked"></div>');
                                   msg.push('<div class="checkboxIconBkg icon icon-check-box-back"></div>')
                                   msg.push('<div class="checkboxLabel">Errors</div>');
                               msg.push('</div>');
                               msg.push('<div class="checkboxDiv checkboxHideWarning disabled">');
                                   msg.push('<div class="checkboxIcon icon icon-box-unchecked"></div>');
                                   msg.push('<div class="checkboxIconBkg icon icon-check-box-back"></div>')
                                   msg.push('<div class="checkboxLabel">Warnings</div>');
                               msg.push('</div>');
                               msg.push('<div class="checkboxDiv checkboxHideInfo disabled">');
                                   msg.push('<div class="checkboxIcon icon icon-box-unchecked"></div>');
                                   msg.push('<div class="checkboxIconBkg icon icon-check-box-back"></div>')
                                   msg.push('<div class="checkboxLabel">Info</div>');
                               msg.push('</div>');
                           msg.push('</div>');
                       msg.push('</div>');
                       msg.push('<div class="fileNameDiv">');
                           msg.push('<div class="fileNameLabel">Results for file:&nbsp;</div>')
                           msg.push('<div class="fileName">No File Selected</div>');
                       msg.push('</div>');
                   msg.push('</div>');
                   msg.push('<div class="rightSide">');
                       msg.push('<div class="statusAreaDiv">');
                           msg.push('<div class="statusLineDiv">');
                               msg.push('<div class="statusLine1"></div>');
                               msg.push('<div class="statusLine2"></div>');
                               msg.push('<div class="statusLine3"></div>');
                           msg.push('</div>');
                       msg.push('</div>');
                   msg.push('</div>');
               msg.push('</div>');

               msg.push('<div class="loadingDiv">');
                   msg.push('<div class="loading"></div>');
                   msg.push('<img class="loadingImg" src="' + loadingImg + '">');
                   msg.push('<div id="loadingStatusMsg" class="loadingStatusMsg">Status Msg</div>');
               msg.push('</div>')

               msg.push('<div class="csvAnalysisOuterResultsDiv">');
                   msg.push('<div class="csvAnalysisMsgDiv">');
                       msg.push('<div class="msg">');
                           msg.push('Press the "Choose File" button and select the CSV file you wish to load');
                       msg.push('</div>')
                   msg.push('</div>');
               msg.push('</div>')
           msg.push('</div>');
       msg.push('</form>');

       let bMissingReqiuredFields = false;
       let bMissingRecommendFields = false;
       var $csvUploadDlg = $('body').revupDialogBox({
                       extraClass: 'csvUploadDlg',
                       okBtnText: 'Upload CSV',
                       enableOkBtn: false,
                       autoCloseOk: false,
                       editCancelBtnText: 'Close',
                       headerText: 'CSV Upload Contacts',
                       msg: msg.join(''),
                       zIndex: 60,
                       top: 75,
                       width: (Math.min($(window).width(), 1700) - 80) + 'px',//'450px',
                       height: (Math.min($(window).height(), 1600) - 150) + 'px',
                       okHandler: function(e) {
                           $('#id_csv_form2').submit();
                       },
                   });

       // do a context switch to all time to load
       setTimeout(function () {
           // set the height tof the results area
           $('.csvAnalysisDiv').height($('.msgBodyDiv').height()  - ($('.csvUploadDiv').height() + 20));

           // set the width of the div holding the file name
           //$('410ameDiv', $csvUploadDlg).width($csvUploadDlg.width() - 75);

           // disable the print button
           $('.btnCntl.csvPrint', $csvUploadDlg).addClass('disabled')
       }, 10);

       // get the loading status msg
       let $loadingMsg = $('.loadingDiv .loadingStatusMsg', $csvUploadDlg);

       // get the status lines
       let $statusLine1 = $('.statusAreaDiv .statusLine1', $csvUploadDlg);
       let $statusLine2 = $('.statusAreaDiv .statusLine2', $csvUploadDlg);
       let $statusLine3 = $('.statusAreaDiv .statusLine3', $csvUploadDlg);

       $("form", $csvUploadDlg).on('submit', function(e) {
           e.preventDefault();

           var formData = new FormData($(this)[0]);

           var url = templateData.csvUpload;
           if (personalContactsAccess && accountContactsAccess) {
               var $radioBtns = $('.contentVisibilityDiv .selectorDiv');
               var $selected = $('.selected', $radioBtns);
               var selectedValue = $selected.attr('value');
               if (selectedValue == 'everyone') {
                   url += '?account_contacts=true';
               }
           }
           else if (!personalContactsAccess && accountContactsAccess) {
               url += '?account_contacts=true';
           }

           $.ajax({
               url: url,
               type: 'POST',
               data: formData,
               //async: false,
               cache: false,
               contentType: false,
               enctype: 'multipart/form-data',
               processData: false,
           })
           .done(function() {
               $csvUploadDlg.revupDialogBox('close');

           })
           .fail(function(r) {
               revupUtils.apiError('Contact Manager', r, 'Unable to load CSV file at this time');
               /*
               if (r.responseText.length > 0){
                   $('.csvErrorMsgDiv', $csvUploadDlg).html(r.responseText)
                                                      .css('margin-bottom', '5px')
               }
               */
           });

           return false;
       });

       let displayErrorsAndWarnings = (eResults, max, $loadingMsg) => {
           let sTmp = [];

           let currentLine = -1;
           let linesInError = 0;
           let linesInWarning = 0;

           let eLst = eResults.errors;
           let numErrors = eLst.length;

           for (let eIndex = 0; eIndex < numErrors; eIndex++) {
               if (max != -1 && eIndex >= max) {
                   sTmp.push('<div class="entry maxErrors">');
                       sTmp.push('<div class="entryMsg">Maximum number of errors reached.</div>');
                       if (eResults.bMaxErrorsHit) {
                           sTmp.push('<div class="entryMsg">' + eResults.lastLineAnalyzied + ' of ' + eResults.numLines + ' analyzied.');
                       }
                   sTmp.push('</div>');

                   break;
               }
               // every 100 errors update status
               if ($loadingMsg && ((eIndex + 1) % 100 == 0)) {
                   //setTimeout(function () {
                       document.getElementById("loadingStatusMsg").innerHTML = Math.round((eIndex / numErrors) * 100) + '% of error built for display';
                   //}, 1);
               }

               let err = eLst[eIndex];

               // icon column
               let icon = [];
               let lineType = '';
               if (err.colType == 'required') {
                   icon.push('<div class="icon icon-stop"></div>');

                   // line type
                   lineType = 'lineError';

                   // keep a count of unique errors
                   if (currentLine != err.lineNum) {
                       linesInError++;

                       currentLine = err.lineNum;
                   }
               }
               else if (err.colType == 'recommended') {
                   icon.push('<div class="icon icon-warning"></div>');

                   // line type
                   lineType = 'lineWarning';

                   // keep a count of unique warnings
                   if (currentLine != err.lineNum) {
                       linesInWarning++;

                       currentLine = err.lineNum;
                   }
               }
               else {
                   icon.push('<div class="icon icon-info-1"></div>');

                   // line type
                   lineType = 'lineInfo';
               }

               // error Messages
               sTmp.push('<div class="entry errorWarningEntry" lineType="' + lineType + '" errLine="' + err.lineNum + '" errCol="' + err.colIndex + '" errCode="' + err.id + '">');
                   // icon columns
                   sTmp.push('<div class="column iconCol">');
                       sTmp.push(icon.join(''));
                   sTmp.push('</div>');

                   // issues column
                   let eData = undefined;
                   if (err.eData && err.eData != -1) {
                       if (Array.isArray(err.eData)) {
                           let t = [];
                           if (err.eData.length == 1) {
                               eData = err.eData[0];
                           }
                           else {
                               for (let tt = 0; tt < err.eData.length; tt++) {
                                   t.push(err.eData[tt]);
                                   if (tt < err.eData.length - 2) {
                                       t.push(', ');
                                   }
                                   else if (tt < err.eData.length - 1) {
                                       t.push(' or ')
                                   }
                               }
                           }

                           eData = t.join('');
                       }
                       else {
                           eData = err.eData;
                       }
                   }
                   sTmp.push('<div class="column issueCol">');
                       sTmp.push('<div class="msg">');
                           sTmp.push('Line: ' + (err.lineNum));
                           if (err.colName != undefined && err.colIndex != undefined) {
                               sTmp.push(' - Column: ' + err.colName + ' (' + contactCSV.columnToExcel(err.colIndex) + ') ');
                           }
                           else {
                               sTmp.push(' - ');
                           }
                           sTmp.push(err.msg);
                           if (eData) {
                               sTmp.push(eData);
                           }
                       sTmp.push('</div>');
                   sTmp.push('</div>');

                   // recommeded action
                   sTmp.push('<div class="column actionCol">');
                       let cMsg = "Nothing";
                       if (err.correctiveMsg) {
                           cMsg = err.correctiveMsg;

                           // if there is extra data
                           if (eData) {
                               cMsg = cMsg.replace(/##extraData##/g, eData);
                           }
                       }
                       sTmp.push('<div class="msg">' + cMsg + '</div>');
                   sTmp.push('</div>');
               sTmp.push('</div>');
           }

           // add the filter warning message
           sTmp.push('<div class="entry filterWarning">');
               sTmp.push('<div class="entryMsg">The following entries types have been filtered out</div>');
               sTmp.push('<div class="entryMsg filterTypes">Errors, Warnings and Info</div>')
           sTmp.push('</div>');

           return {html: sTmp.join(''), linesInError: linesInError, linesInWarning: linesInWarning};
       } // displayErrorsAndWarnings

       let csvFile = '';
       // once the image is loaded then place into a temp location and get size
       const readUploadedFileAsText = (inputFile) => {
           const temporaryFileReader = new FileReader();

           return new Promise((resolve, reject) => {
               temporaryFileReader.onerror = () => {
                   temporaryFileReader.abort();
                   reject(new DOMException("Problem parsing input file."));
               };

               temporaryFileReader.onload = () => {
                   resolve(temporaryFileReader.result);
               };

               temporaryFileReader.readAsText(inputFile);
           });
       };

       const loadCsvFile = async (event) => {
           const file = event.target.files[0];
           const fileContentDiv = document.querySelector('div#file-content')

           try {
               const fileContents = await readUploadedFileAsText(file)

               csvFile = fileContents;

               $('#loadingStatusMsg').html('Analyzing CSV File');
               setTimeout(function() {
                   csvAnalysisStart(file)
               }, 1)
           }
           catch (e) {
console.log('help!!!!: ', e.message)
               //fileContentDiv.innerHTML = e.message
           }
       } // loadCsvFile

       $('#id_file').on('revup.changeFile', function(e) {
           var $okBtn = $('.okBtn', $csvUploadDlg);
           var fileName = $(this).val();

           // if no file name then disable ok button
           if (fileName === '') {
               $okBtn.prop('disabled', true);
               return;
           }

           // show the wait spinner and reset buttons and status
           let $outerResultsDiv = $('.csvAnalysisOuterResultsDiv', $csvUploadDlg);
           $outerResultsDiv.hide();
           $('.loadingDiv', $csvUploadDlg).show();
           $loadingMsg.html('Loading CSV File');
           $('.btnDiv .okBtn', $csvUploadDlg).prop('disabled', true);
           $statusLine1.show()
                       .text('');
           $statusLine2.text('')
                       .css('align-items', '');
           $statusLine3.text('')
                       .css('align-items', '');
           $('.checkboxDiv.checkboxHideError', $csvUploadDlg).addClass('disabled');
           $('.checkboxDiv.checkboxHideError .checkboxIcon', $csvUploadDlg).removeClass('icon-box-checked')
                                                                           .addClass('icon-box-unchecked');
           $('.checkboxDiv.checkboxHideWarning', $csvUploadDlg).addClass('disabled');
           $('.checkboxDiv.checkboxHideWarning .checkboxIcon', $csvUploadDlg).removeClass('icon-box-checked')
                                                                             .addClass('icon-box-unchecked');
           $('.checkboxDiv.checkboxHideInfo', $csvUploadDlg).addClass('disabled');
           $('.checkboxDiv.checkboxHideInfo .checkboxIcon', $csvUploadDlg).removeClass('icon-box-checked')
                                                                          .addClass('icon-box-unchecked');


           // load the csv file and then start the analysis
           loadCsvFile(e)
       });

       let analysisResults = [];
       let csvAnalysisStart = (fileName) => {
           analysisResults = contactCSV.analyze(csvFile, maxCsvToAnalyze, $loadingMsg);

           // loading message
           $loadingMsg.html('Loading Results to Display');

           var fName = fileName.name;
           setTimeout((fileName) => {
               csvDisplayAnalysis(fName)

           }, 1)
       } // csvAnalysisStart

       let csvDisplayAnalysis = (fileName) => {
           let $outerResultsDiv = $('.csvAnalysisOuterResultsDiv', $csvUploadDlg);

           // see if ok to enable the upload button
           bMissingReqiuredFields = analysisResults.missingRequired.length > 0;
           bMissingRecommendFields = analysisResults.missingRecommended.length > 0;
   //console.log('csv analysisResults: ', analysisResults)

           // display the results
           let sTmp = [];
           let bEnable = true;
           if (analysisResults.errors.length == 0  && analysisResults.missingRequired.length == 0 && analysisResults.missingRecommended.length == 0) {
               sTmp.push('<div class="csvAnalysisMsgDiv">');
                   sTmp.push('<div class="msg">');
                       sTmp.push('No Errors Found in the CSV File: ' + fileName.replace("C:\\fakepath\\", ""));
                   sTmp.push('</div>')
               sTmp.push('</div>');

               // disable the print button
               $('.btnCntl.csvPrint', $csvUploadDlg).addClass('disabled')
           }
           else {
               // loading message
               $('#loadingStatusMsg').html('Building Results').hide().show();

               // build the filter overlay
               sTmp.push('<div class="filteringOverlay">');
                   sTmp.push('<div class="filterOverlay"></div>');
                   sTmp.push('<div class="loadingDiv">');
                       sTmp.push('<div class="loading"></div>');
                       sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                       sTmp.push('<div id="loadingStatusMsg" class="loadingStatusMsg">Applying Filter</div>');
                   sTmp.push('</div>');
               sTmp.push('</div>');

               // build the header
               sTmp.push('<div class="csvResultsHeader">');
                   sTmp.push('<div class="iconCol"></div>')
                   sTmp.push('<div class="issueCol">');
                       sTmp.push('<div class="headerLabel">Issues</div>');
                   sTmp.push('</div>');
                   sTmp.push('<div class="actionCol">');
                       sTmp.push('<div class="headerLabel">Corrective Action</div>');
                   sTmp.push('</div>');
               sTmp.push('</div>')

               // build the results
               sTmp.push('<div class="csvAnalysisResultsDiv">');
                   // display missing required columns
                   if (analysisResults.missingRequired.length > 0) {
                       bEnable = false;
                       let missing = analysisResults.missingRequired;
                       sTmp.push('<div class="entry missingRequired">');
                           sTmp.push('<div class="column iconCol">')
                               sTmp.push('<div class="icon icon-stop"></div>');
                           sTmp.push('</div>');

                           sTmp.push('<div class="column issueCol">');
                               sTmp.push('<div class="msg">Missing required column header</div>')
                           sTmp.push('</div>');

                           let sTmp2 = [];
                           let num =  missing.length;
                           let line1 = num == 1 ? 'The required column header ' : 'The required column headers ';
                           let line2 = num == 1 ? ' is  missing.  You will not be about to upload this file without it.' : ' are missing.  You will not be about to upload this file without it.';
                           for (let i = 0; i < missing.length; i++) {
                               sTmp2.push('"' + '<div class="msgHighlight">' + missing[i] + '</div>"');

                               if (num > 1) {
                                   if (i == (num - 2)) {
                                       sTmp2.push(' and ');
                                   }
                                   else if (i != (num - 1)) {
                                       sTmp2.push(', ');
                                   }
                               }
                           }
                           sTmp.push('<div class="column actionCol">');
                               sTmp.push('<div class="msg">');
                                   sTmp.push(line1);
                                   sTmp.push(sTmp2.join(''));
                                   sTmp.push(line2)
                               sTmp.push('</div>');
                           sTmp.push('</div>');
                       sTmp.push('</div>');
                   }

                   // display missing recommended columns
                   if (analysisResults.missingRecommended.length > 0) {
                       let missing = analysisResults.missingRecommended;
                       sTmp.push('<div class="entry missingRecommended">');
                           sTmp.push('<div class="column iconCol">')
                               sTmp.push('<div class="icon icon-warning"></div>');
                           sTmp.push('</div>');

                           sTmp.push('<div class="column issueCol">');
                               sTmp.push('<div class="msg">Missing recommended column header</div>')
                           sTmp.push('</div>');

                           let sTmp2 = [];
                           let num =  missing.length;
                           let line1 = num == 1 ? 'The recommeded column header ' : 'The recommeded column headers ';
                           let line2 = num == 1 ? ' is missing.  With this your contact matches will be better.' : ' are missing.  With these your contact matches will be better.';
                           for (let i = 0; i < missing.length; i++) {
                               sTmp2.push('"' + '<div class="msgHighlight">' + missing[i] + '</div>"');

                               if (num > 1) {
                                   if (i == (num - 2)) {
                                       sTmp2.push(' and ');
                                   }
                                   else if (i != (num - 1)) {
                                       sTmp2.push(',&nbsp;');
                                   }
                               }
                           }
                           sTmp.push('<div class="column actionCol">');
                               sTmp.push('<div class="msg">');
                                   sTmp.push(line1);
                                   sTmp.push(sTmp2.join(''));
                                   sTmp.push(line2)
                               sTmp.push('</div>');
                           sTmp.push('</div>');
                       sTmp.push('</div>');
                   }

                   // display extra columns
                   if (analysisResults.extraColumns.length > 0) {
                       let extra = analysisResults.extraColumns;
                       sTmp.push('<div class="entry extraColumns">');
                           sTmp.push('<div class="column iconCol">')
                               sTmp.push('<div class="icon icon-info-1"></div>');
                           sTmp.push('</div>');

                           sTmp.push('<div class="column issueCol">');
                               sTmp.push('<div class="msg">Extra Column(s)</div>')
                           sTmp.push('</div>');

                           let sTmp2 = [];
                           let num =  extra.length;
                           let line1 = num == 1 ? 'The following column ' : 'The following columns ';
                           let line2 = num == 1 ? ' is extra.  ' : ' are extra.';
                           let line3 = num == 1 ? '  This column ' : ' These columns ';
                           let line4 = '<b>will be ignored,</b> but check the spelling just in case it is a column which can provide information about a contact.'
                           for (let i = 0; i < extra.length; i++) {
                               sTmp2.push('"' + '<div class="msgHighlight">' + extra[i].name + ' (' + contactCSV.columnToExcel(extra[i].columnIndex) + ') </div>"');

                               if (num > 1) {
                                   if (i == (num - 2)) {
                                       sTmp2.push(' and ');
                                   }
                                   else if (i != (num - 1)) {
                                       sTmp2.push(',&nbsp;&nbsp;');
                                   }
                               }
                           }
                           sTmp.push('<div class="column actionCol">');
                               sTmp.push('<div class="msg">');
                                   sTmp.push(line1);
                                   sTmp.push(sTmp2.join(''));
                                   sTmp.push(line2)
                                   sTmp.push(line3)
                                   sTmp.push(line4)
                               sTmp.push('</div>');
                           sTmp.push('</div>');
                       sTmp.push('</div>');
                   }

                   // display errors and warnings
                   let result = displayErrorsAndWarnings(analysisResults, maxCsvToDisplay, $loadingMsg);
                   sTmp.push(result.html);

                   // status
                   if (bMissingReqiuredFields) {
                       $statusLine1.hide();
                       $statusLine2.text('Do to missung required fields')
                                   .css('align-items', 'flex-end');
                       $statusLine3.text('you are not able to upload this file')
                                   .css('align-items', 'flex-start');
                   }
                   else {
                       $statusLine1.text('Number of Contact: ' + (analysisResults.numLines - 1));
                       $statusLine2.text('Contact With Errors: ' + result.linesInError);
                       $statusLine3.text('Contact With Warnings: ' + result.linesInWarning);
                   }
               sTmp.push('</div>');

               // enable the print button
               $('.btnCntl.csvPrint', $csvUploadDlg).removeClass('disabled');

               // enable the filter buttons
               $('.checkboxDiv.checkboxHideError', $csvUploadDlg).removeClass('disabled');
               $('.checkboxDiv.checkboxHideWarning', $csvUploadDlg).removeClass('disabled');
               $('.checkboxDiv.checkboxHideInfo', $csvUploadDlg).removeClass('disabled');
           }
           // loading message
           $('#loadingStatusMsg').html('Loading Result for display').hide().show();

           $outerResultsDiv.show()
                           .html(sTmp.join(''));
           $('.loadingDiv', $csvUploadDlg).hide();

           // make sure one of the radio button is set
           if (personalContactsAccess && accountContactsAccess) {
               var $radioBtns = $('.contentVisibilityDiv .selectorDiv .radioEntry');
               var $selected = $('.selected', $radioBtns);
               if ($selected.length === 0) {
                   bEnable = false;
               }
           }

           if (bEnable && !bMissingReqiuredFields) {
               $('.btnDiv .okBtn', $csvUploadDlg).prop('disabled', false);
           }
           else {
               $('.btnDiv .okBtn', $csvUploadDlg).prop('disabled', true);
           }
       } // csvDisplayAnalysis

       $csvUploadDlg.on('click', '.radioEntry', function(e) {
           var $e = $(this);
           var $rBtn = $('.radioBtn', $e);

           if ($rBtn.hasClass('selected')) {
               return;
           }

           // clear current selected
           var $parent = $rBtn.parent().parent().parent();
           var $selected = $('.selected', $parent);
           $selected.removeClass('selected')
                    .removeClass('icon-radio-button-on')
                    .addClass('icon-radio-button-off');

           // set selected
           $rBtn.addClass('selected')
                .addClass('icon-radio-button-on')
                .removeClass('icon-radio-button-off');

           // make sure there is a file name before enabling the ok button
           var fileName = $('#id_file').val();
           if (fileName === '') {
               return;
           }

           fileName = fileName.split('.');
           if ((fileName.length >= 2) && (fileName[(fileName.length - 1)].toLowerCase() == 'csv') && !bMissingReqiuredFields) {
               var $okBtn = $('.okBtn', $csvUploadDlg);
               $okBtn.prop('disabled', false);
           }
       })

       $('.buttonBar .filterSection', $csvUploadDlg).on('click', '.checkboxDiv', function(e) {
           // see if disabled
           if ($(this).hasClass('disabled')) {
               return;
           }

           // hide/show stuff
           // check/uncheck
           let $cBox = $('.checkboxIcon', $(this))
           let bHide = false;
           if ($cBox.hasClass('icon-box-unchecked')) {
               $cBox.removeClass('icon-box-unchecked')
                    .addClass('icon-box-checked');

               bHide = true;
           }
           else {
               $cBox.removeClass('icon-box-checked')
                    .addClass('icon-box-unchecked');

               bHide = false;
           }

           // get field to hide or show
           let hideShowField = '';
           let $cField = $(this);
           if ($cField.hasClass('checkboxHideError')) {
               hideShowField = 'lineError';
           }
           else if ($cField.hasClass('checkboxHideWarning')) {
               hideShowField = 'lineWarning';
           }
           else if ($cField.hasClass('checkboxHideInfo')) {
               hideShowField = 'lineInfo';
           }

           // show the filtering overlay
           $('.csvAnalysisOuterResultsDiv .filteringOverlay', $csvUploadDlg).show();

           // do the hiding and showing
           $('.csvAnalysisResultsDiv .entry[lineType="' + hideShowField + '"]', $csvUploadDlg).each(function() {
               if (bHide) {
                   $(this).hide();
               }
               else {
                   $(this).show();
               }
           })

           // see how many are checked
           let numChecked = 0;
           let $checked = $('.checkboxDiv .checkboxIcon.icon-box-checked', $csvUploadDlg);
           numChecked = $checked.length;
           if (numChecked == 0) {
               // nothing checked
               $('.csvAnalysisOuterResultsDiv .csvAnalysisResultsDiv .entry.filterWarning').hide();
           }
           else {
               let selected = [];
               $checked.each(function() {
                   selected.push($('.checkboxLabel', $(this).parent()).text());
               });
               let s = [];
               let sLen = selected.length;
               for (let i = 0; i < sLen; i++) {
                   s.push(selected[i]);
                   if (sLen > 1) {
                       if (i == (sLen - 2)) {
                           s.push(' and ');
                       }
                       else if (i != (sLen - 1)) {
                           s.push(', ');
                       }
                   }
               }
               $('.csvAnalysisOuterResultsDiv .csvAnalysisResultsDiv .entry.filterWarning .filterTypes').html(s.join(''));

               $('.csvAnalysisOuterResultsDiv .csvAnalysisResultsDiv .entry.filterWarning').show();
           }

           // zybra strip visible lines
           $('.csvAnalysisResultsDiv .entry:visible', $csvUploadDlg).each(function(index) {
               if (index % 2) {
                   $(this).css('background-color', revupConstants.color.whiteText);
               }
               else {
                   $(this).css('background-color', revupConstants.color.secondaryGray3);
               }
           });

           // hide the filtering overlay
           $('.csvAnalysisOuterResultsDiv .filteringOverlay', $csvUploadDlg).hide();
       })

       $('.btnCntl.csvPrint', $csvUploadDlg).on('click', function(e) {
           // see if $disabled
           if ($(this).hasClass('disabled')) {
               return;
           }

           let width = parseInt($(window).width() / 2, 10);
           let height = parseInt($(window).height() / 2, 10);
           let left = parseInt(($(window).width() - width) / 2, 10);
           let top = parseInt(($(window).height() - height) / 2, 10);

           var printPreview = window.open('', 'csvAnalysisPrint', "titlebar=no,statue=no,location=no,width=" + width + ",height="+ height + ",left=" + left + ",top=" + top);
           var printDocument = printPreview.document;
           printDocument.open();
           printDocument.write("<!DOCTYPE html>" +
               "<head>" +
                   "<title>CSV Errors/Warnings</title>" +
               "</head>" +
               "<body style='margin:0;padding:0;min-width:" + (width - 16) + "px'>" +
                   "<html style='min-width:" + (width - 16) + "px'>"+
                       '<link href="/static/css/apps/contactManager.css" rel="stylesheet">' +
                       '<link href="/static/css/revupicons.css" rel="stylesheet">' +
                       '<link href="/static/css/styles.css" rel="stylesheet">' +
                       '<div class="csvUploadDlg printDlg">' +
                           '<div class="csvAnalysisDiv">' +
                               '<div class="csvAnalysisOuterResultsDiv">' +
                                   $('.csvAnalysisOuterResultsDiv', $csvUploadDlg).html() +
                               '</div>' +
                           '</div>' +
                       '</div>' +
                   "</html>" +
               "</body>"
           );
           printDocument.close();

           $(printPreview).on('load', function() {
               printPreview.window.print();
           })
           .on('afterprint', function () {
               printPreview.close();
           });
       })

       $('.btnCntl.csvFormatHelp', $csvUploadDlg).on('click', function(e) {
            window.open(templateData.csvHelp, "_blank");
       })

   } // csvUploadDlg

   return {
       load: function(errorMsgLst, ptemplateData){
           templateData = ptemplateData;
           // load the csv config file
           contactCSV = csvValidation;
           contactCSV.loadConfigFile(templateData.csvConfigFile);

           personalContactsAccess = features.PERSONAL_CONTACTS;
           accountContactsAccess = features.ACCOUNT_CONTACTS && templateData.hasAdminAccess;

           if(templateData) {
               csvUploadDlg(errorMsgLst);
           }

       }
   }

} ());
