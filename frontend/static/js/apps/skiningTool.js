var skiningTool = (function () {
    var templateData = undefined;       // pointer to the template data
    var bChangeFlag = false;            // global change flag

    var $dlg = undefined;               // the pop
    var $confDlg = $();           // save confirm box
    var $themeName = $();
    var themeNameActive = -1;

    // theme list
    var themeLst = [];
    var themeCurrentActive = -1;
    var prospectTitles = ["Soft, Hard, In", "Pleged, In Transit, In Bank", "Custom"];

    // prospect title
    var prospectTitlesIndex = 0;
    var $prospectTitleSoft = $();
    var $prospectTitleHard = $();
    var $prospectTitleIn = $();
    var $$prospectTitleCustomMsg = $();

    // handle changes
    function somethingChanged(e)
    {
        // set the change flag
        bChangeFlag = true;

        // enable the ok button
        bEnable = true;
        var name = $themeName.val();
        if (name == '') {
            bEnable = false
        }

        if ($prospectTitleSoft.val() == '') {
            bEnable = false
        }
        if ($prospectTitleHard.val() == '') {
            bEnable = false
        }
        if ($prospectTitleIn.val() == '') {
            bEnable = false
        }

        if (bEnable) {
            // enable the dialog save button
            $dlg.revupDialogBox('enableBtn', 'okBtn');
        }
        else {
            // enable the dialog save button
            $dlg.revupDialogBox('disableBtn', 'okBtn');
        }
    } // somethingChanged

    function prepSkinSettings(skin){
        // Prepare the skin settings object for usage by creating all of
        // the expected objects
        if (!skin.settings){
            skin.settings = {}
        }
        if (!skin.settings.cssRules) {
            skin.settings.cssRules = {}
        }
        if (!skin.settings.candidateIcon) {
            skin.settings.candidateIcon = {};
        }
        if (!skin.settings.prospectTitle) {
            skin.settings.prospectTitle = {};
        }
    }

    function clearBtn($btn)
    {
        // get the current color and break apart
        $btn.removeAttr('style');
        var color = $btn.css('color');
        color = color.replace('rgb(', '');
        color = color.replace(')', '');
        color = color.split(',')

        // create the new color
        color = 'color:rgba(' + $.trim(color[0]) + ', ' + $.trim(color[1]) + ', ' + $.trim(color[2]) + ', .5) !important';

        $btn.attr('style', color);
        $btn.addClass('disabled');
        $('.defaultBannerOrg').show();
    } // clearBtn

    function loadTheme(themeData, dontEnableOk)
    {
        // load the name of the theme
        $('.themeName', $dlg).val(themeData.title);

        // load images
        if (themeData.branding_logo) {
            $('.logoImg', $dlg).attr('src', themeData.branding_logo);

            // enable the clear button
            $('.clearLogoBtn', $dlg).removeAttr('style');
            $('.clearLogoBtn', $dlg).removeClass('disabled');
        }
        else {
            $('.logoImg', $dlg).attr('src', templateData.logoUrl);

            // disable the clear button
            var $clearBtn = $('.clearLogoBtn', $dlg);
            clearBtn($clearBtn);
        }

        if (themeData.banner) {
            if ($('.bannerImgDiv').length > 0) {
                $('.bannerImgDiv').replaceWith('<img class="bannerImg">');
            }
            $('.bannerImg', $dlg).attr('src', themeData.banner);

            // enable the clear button
            $('.clearBannerBtn', $dlg).removeAttr('style');
            $('.clearBannerBtn', $dlg).removeClass('disabled');
        }
        // Staff skinning tool does not know which image is the correct one
        // to display, so we will display a reminder instead, so it is obvious.
        else if (templateData.bannerUrl === "staff-skinning") {
            $('.bannerImg', $dlg).replaceWith('<div class="bannerImgDiv" style="width: 869px; height: 216px; border-style: solid; border-width: 1px;">' +
                '<h3 style="position: relative; top: 35%; left: 275px;">Account-type Default Image</h3></div>')

            // disable the clear button
            var $clearBtn = $('.clearBannerBtn', $dlg);
            clearBtn($clearBtn);
        }
        else {
            if ($('.bannerImgDiv').length > 0) {
                $('.bannerImgDiv').replaceWith('<img class="bannerImg">');
            }
            $('.bannerImg', $dlg).attr('src', templateData.bannerUrl);

            // disable the clear button
            var $clearBtn = $('.clearBannerBtn', $dlg);
            clearBtn($clearBtn);
        }

        // Prepare the skin settings object for usage
        prepSkinSettings(themeData);
        // load colors
        var cssRules = themeData.settings.cssRules;
        if (cssRules && (cssRules.backgroundColor) && (cssRules.backgroundColor.field[0].colorVal)) {
            $('.backgroundColor', $dlg).revupColorCntl('setColor', cssRules.backgroundColor.field[0].colorVal);
        }
        else {
            $('.backgroundColor', $dlg).revupColorCntl('clear');
        }
        if (cssRules && (cssRules.primBtnColor) && (cssRules.primBtnColor.field)) {
            var color = cssRules.primBtnColor.field[0].colorVal;
            for (var i = 0; i < cssRules.primBtnColor.field.length; i++) {
                if ((cssRules.primBtnColor.field[i].colorType == 'background') || (cssRules.primBtnColor.field[i].colorType == 'color')) {
                    color = cssRules.primBtnColor.field[i].colorVal
                }
            }
            $('.primaryBtnColor', $dlg).revupColorCntl('setColor', color);
        }
        else {
            $('.primaryBtnColor', $dlg).revupColorCntl('clear');
        }
        if (cssRules && (cssRules.secBtnColor) && (cssRules.secBtnColor.field)) {
            var color = cssRules.secBtnColor.field[0].colorVal;
            for (var i = 0; i < cssRules.secBtnColor.field.length; i++) {
                if ((cssRules.secBtnColor.field[i].colorType == 'background') || (cssRules.secBtnColor.field[i].colorType == 'color')) {
                    color = cssRules.secBtnColor.field[i].colorVal
                }
            }
            $('.secBtnColor', $dlg).revupColorCntl('setColor', color);
        }
        else {
            $('.secBtnColor', $dlg).revupColorCntl('clear');
        }
        if (cssRules && (cssRules.lnkColor) && (cssRules.lnkColor.field[0].colorVal)) {
            $('.linkColor', $dlg).revupColorCntl('setColor', cssRules.lnkColor.field[0].colorVal);
        }
        else {
            $('.linkColor', $dlg).revupColorCntl('clear');
        }
        /*
        if (cssRules && (cssRules.inactiveTabColor) && (cssRules.inactiveTabColor.field)) {
            var color = cssRules.inactiveTabColor.field[0].colorVal;
            for (var i = 0; i < cssRules.inactiveTabColor.field.length; i++) {
                if ((cssRules.inactiveTabColor.field[i].colorType == 'background') || (cssRules.inactiveTabColor.field[i].colorType == 'color')) {
                    color = cssRules.inactiveTabColor.field[i].colorVal
                }
            }
            $('.inactiveTabColor', $dlg).revupColorCntl('setColor', color);
        }
        else {
            $('.inactiveTabColor', $dlg).revupColorCntl('clear');
        }
        */

        // academic score color
        if (templateData.isAcademic || templateData.isStaff) {
            if (cssRules && (cssRules.scoreHighlightColor) && (cssRules.scoreHighlightColor.field[0].colorVal)) {
                $('.scoreHighlightColor', $dlg).revupColorCntl('setColor', cssRules.scoreHighlightColor.field[0].colorVal);
            }
            else {
                $('.scoreHighlightColor', $dlg).revupColorCntl('clear');
            }
        }

        // candidate icon
        if (templateData.isPoliticalCampaign || templateData.isPoliticalCommittee) {
            if ((themeData.settings.candidateIcon) && (themeData.settings.candidateIcon.candidateIconColor)) {
                var color = themeData.settings.candidateIcon.candidateIconColor;
                $('.candidateIconColor', $dlg).revupColorCntl('setColor', color);
                $('.settingSection .candidateIconBox').attr('background-color', color);

                var name = themeData.settings.candidateIcon.candidateIconName;
                //if (name == "") {
                //    name = currentSkin.defaultAccountIconText;
                //}
                $('.settingSection .candidateIconBox .text', $dlg).text(name);
                $('.settingSection .candidateIconName', $dlg).val(name);
            }
            else {
                $('.candidateIconColor', $dlg).revupColorCntl('clear');

                $('.settingSection .candidateIconBox').attr('background-color', '');
                $('.settingSection .candidateIconBox .text', $dlg).text(templateData.defaultIconText);
                $('.settingSection .candidateIconName', $dlg).val(''); //templateData.defaultIconText);
            }
        }

        // prospect title
        if (!$.isEmptyObject(themeData.settings.prospectTitle)) {
            var index = 0;
            var pTitle = themeData.settings.prospectTitle;
            if ((pTitle.softTitle == 'Soft') && (pTitle.hardTitle == 'Hard') && (pTitle.inTitle == 'In')) {
                setProspectTitle(0);
            }
            else if ((pTitle.softTitle == 'Pleged($)') && (pTitle.hardTitle == 'In Transit($)') && (pTitle.inTitle == 'In Bank($)')) {
                setProspectTitle(1);
            }
            else {
                setProspectTitle(2, pTitle.softTitle, pTitle.hardTitle, pTitle.inTitle)
            }
        }
        else {
            setProspectTitle(0);
        }

        if ((!dontEnableOk) && (templateData.currentSkinId) && (templateData.currentSkinId != themeLst[themeCurrentActive].id)) {
            $dlg.revupDialogBox('enableBtn', 'okBtn');
        }
        else {
            // reset the ok/save button
            $dlg.revupDialogBox('disableBtn', 'okBtn');
        }

        // reset the change flag
        bChangeFlag = false;
    } // loadTheme

    /*
     *  Save
     */
    function saveStepOne(tObj, tName)
    {
        // get name
        var tName = $themeName.val();

        // see if in the list of theme's
        var tObj = undefined;
        var nameUserIndex = -1;
        for(var i = 0; i < themeLst.length; i++) {
            if (themeLst[i].title == tName) {
                nameUserIndex = i;
                break;
            }
        }

        if (nameUserIndex != -1) {
            var msg = 'Message';
            var header = 'Header';
            var midBtnLabel = "";
            var okBtnLabel = "OK";
            var updateFlag = true;
            if ((!templateData.isStaff) && (themeLst[nameUserIndex].shared)) {
                msg = "\"" + themeLst[themeCurrentActive].title + "\" is a global theme.  Do you want to create \"" + themeLst[themeCurrentActive].title + "\" (n), where n is the next free name.",
                header = "Create Confirmation";
                updateFlag = false;
            }
            else {
                msg = "Do you want to update the template \"" + themeLst[nameUserIndex].title + "\".";
                header = "Update Confirmation";
                midBtnLabel = "Create";
                okBtnLabel = "Update";
            }

            // if nothing change then just update the active theme
            if (!bChangeFlag) {
                setActiveThemeAndClose(themeLst[nameUserIndex].id, tName);
                return;
            }

            $confDlg = $dlg.revupConfirmBox({
                okBtnText: okBtnLabel,
                headerText: header,
                msg: msg,
                zIndex: 25,
                isDragable: false,
                autoCloseOk: false,
                okHandler: function() {
                    saveTheme(tObj, tName, updateFlag, true);
                },
                cancelHandler: undefined,
                autoCloseMid: false,
                midBtnText: midBtnLabel,
                midHandler: function() {
                    saveTheme(tObj, tName, false, true);
                }

            });
        }
        else {
            saveTheme(undefined, tName);
        }
    } // saveStepOne

    function saveStepThree()
    {} // saveStepThree

    function saveTheme(tObj, tName, bUpdate, fromSave)
    {
        if (!tObj) {
            tObj = new Object();
        }

        if (tName) {
            tObj.title = tName;
        }

        // Prepare the skin settings object for usage
        prepSkinSettings(tObj);

        // get the colors
        $('.colorCntl', $dlg).each(function() {
            var $cntl = $(this);

            var valObj = $cntl.revupColorCntl("get");
            if (!$.isEmptyObject(valObj)) {
                var cntlFor = valObj.field[0].colorFor;
                switch(cntlFor) {
                    case 'backgroundDiv':
                        tObj.settings.cssRules.backgroundColor = valObj;
                        tObj.settings.cssRules.backgroundColor.cssRule = '.backgroundDiv';
                        break;
                    case 'primaryBtnField':
                        tObj.settings.cssRules.primBtnColor = valObj;
                        tObj.settings.cssRules.primBtnColor.cssRule = ['.revupBtnDefault', '.revupPrimaryBtnColor(important)', '.revupPagination .paginationDivInner .paginationPageNum.active']
                        break;
                    case 'secBtnField':
                        tObj.settings.cssRules.secBtnColor = valObj;
                        tObj.settings.cssRules.secBtnColor.cssRule = ['.revupBtnDefault.secondary', '.revupSecondaryBtnColor(important)']
                        break;
                    case 'lnkField':
                        tObj.settings.cssRules.lnkColor = valObj;
                        tObj.settings.cssRules.lnkColor.cssRule = ['a', 'a:hover(important)', '.revupLnk(important)', '.revupPagination .paginationDivInner .paginationPageNum']
                        break;
                    case 'tabInactive':
                        tObj.settings.cssRules.inactiveTabColor = valObj;
                        tObj.settings.cssRules.inactiveTabColor.cssRule = ".mainContent .container .sideNav .tab"
                        break;
                    case 'scoreHighlight':
                        tObj.settings.cssRules.scoreHighlightColor = valObj;
                        tObj.settings.cssRules.scoreHighlightColor.cssRule = '.academicScoreColor';
                        break;
                    case 'candidateIcon':
                        tObj.settings.candidateIcon.candidateIconColor = valObj.field[0].colorVal;
                        var val = $('.settingSection .candidateIconName', $dlg).val();
                        //if (val == templateData.defaultIconText) {
                        //    val = '';
                        //}
                        tObj.settings.candidateIcon.candidateIconName = val;
                        break
                    } // end switch - cntlFor
            }
        });

        // get the prospect titles
        tObj.settings.prospectTitle.softTitle = $prospectTitleSoft.val();
        tObj.settings.prospectTitle.hardTitle = $prospectTitleHard.val();
        tObj.settings.prospectTitle.inTitle = $prospectTitleIn.val();

        // send the changes to the back end
        var data = {};
        data.title = tObj.title;
        data.id = tObj.id;
        data.settings = {};
        data.settings.candidateIcon = tObj.settings.candidateIcon;
        data.settings.cssRules = tObj.settings.cssRules;
        data.settings.prospectTitle = tObj.settings.prospectTitle;

        // add the images to the data to post/put if new
        if (($('.logoImg', $dlg).attr('src') == '') || ($('.logoImg', $dlg).attr('src') == templateData.logoUrl)) {
            data.branding_logo = null;
        }
        if (($('.bannerImg', $dlg).attr('src') == '') || ($('.bannerImg', $dlg).attr('src') == templateData.bannerUrl)) {
            data.banner = null;
        }

        var logoChange = $('.loadLogoBtn', $dlg).attr('imageChanged') == 'true' ? true  : false ;
        var bannerChange = $('.loadBannerBtn', $dlg).attr('imageChanged') == 'true' ? true  : false ;
        var logoCleared = $('.loadLogoBtn', $dlg).attr('imageCleared') == 'true' ? true : false;
        var bannerCleared = $('.loadBannerBtn', $dlg).attr('imageCleared') == 'true' ? true : false;

        if (logoChange && !logoCleared) {
            data.branding_logo = revupUtils.getBase64Image($('.logoImg').get(0));
        }
        if (bannerChange && !bannerCleared) {
            data.banner = revupUtils.getBase64Image($('.bannerImg').get(0));
        }
        if (logoCleared) {
            data.branding_logo = null;
        }
        if (bannerCleared) {
            data.banner = null;
        }

        var url = templateData.themeSaveUrl;
        var ajaxType = "POST";
        if (bUpdate) {
            data.id = themeLst[themeCurrentActive].id;
            url = url.replace('themeId', data.id);
            ajaxType = "PUT";
        }
        else {
            url = url.replace('themeId/', '');
        }
        $.ajax({
            type: ajaxType,
            url: url,
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        })
        .done(function(r) {
            if (templateData.isStaff) {
                // make sure the confirm box is closed
                $confDlg.trigger('revup.confirmClose');

                var $msgBox = $dlg.revupMessageBox({
                    headerText: 'Add To Account Profile',
                    msg: 'To add a global skin to an account profile, press the "Django Admin" button.  ' +
                         'Once open, select the "Account profiles" in the Campaign section.  ' +
                         'From the "Change account profile" page, in the "Account skin settings links" ' +
                         'section add/delete the skins you wish to add/delete for this profile.'
                });

                $msgBox.on('revup.messageBoxClose', function() {
                    // close the dialog
                    $dlg.revupDialogBox('close');
                });
            }
            else {
                setActiveThemeAndClose(r.id, r.title);
            }
        })
        .fail(function(r) {
            revupUtils.apiError('Skinning', r, 'Unable to save theme', "Unable to save theme at this time.");
        });
    } // saveTheme

    function buildThemeList(r)
    {
        // keep the theme list
        themeLst = r.results;

        // load the current templates
        themeNameLst = [];

        // build local list
        themeCurrentActive = 0;
        for (var i = 0; i < r.results.length; i++) {
            var o = new Object();

            // display value
            o.displayValue = themeLst[i].title;
            if (themeLst[i].shared) {
                o.displayValue += ' (global)';
            }

            // save the value
            o.value = themeLst[i].id;

            // add to list
            themeNameLst.push(o);

            // see if the active theme
            if ((templateData.currentTemplate) && (themeLst[i].id == templateData.currentTemplate.id)){
                themeCurrentActive = i;
            }
        }

        $('.themeDropDown', $dlg).revupDropDownField({
            value: themeNameLst,
            valueStartIndex: themeCurrentActive,
            //sortList: false,
            changeFunc: function(index, value) {
                themeCurrentActive = index;
                loadTheme(themeLst[index]);
            },
        });

        // handle testing before select and item from the dropdown template
        $('.themeDropDown', $dlg).on('revup.beforeDropDownSelect', function() {
            if (bChangeFlag) {
                $dlg.revupConfirmBox({
                    headerText: 'Ok to switch',
                    msg: 'Your changes have not been saved.</br><br>Press "OK" to go to the new template without saving.',
                    okHandler: function(e) {
                        $('.themeDropDown', $dlg).trigger('revup.dropDownOkToSelect', true)
                    },
                    cancelHandler: function(e) {
                        $('.themeDropDown', $dlg).trigger('revup.dropDownOkToSelect', false)
                    }
                });

                return true;
            }
            else {
                return false;
            }
        });

        // load the active theme
        if (themeLst.length > 0) {
            loadTheme(themeLst[themeCurrentActive], true);
        }
    } // buildThemeList

    function setActiveThemeAndClose(skinId, skinTitle)
    {
        // make sure the confirm box is closed
        $confDlg.trigger('revup.confirmClose');

        // if the current theme is the same as the set theme then close and refresh
        if (skinId == templateData.currentSkinId) {
            // close the dialog
            $dlg.revupDialogBox('close');

            // reload the page
            location.reload(true);

            return;
        }

        $dlg.revupConfirmBox({
            okBtnText: 'OK',
            headerText: 'Set Theme',
            msg: "Are you sure about setting \"" + skinTitle + "\" as the active theme?",
            zIndex: 25,
            isDragable: false,
            okHandler: function() {
                data = {};
                data.current_skin = skinId;

                $.ajax({
                    url: templateData.setActiveSkinUrl,
                    type: "PUT",
                    data: data
                })
                .done(function(r) {
                    // close the dialog
                    $dlg.revupDialogBox('close');

                    // reload the page
                    location.reload(true);
                })
                .fail(function(r) {
                    revupUtils.apiError('Skinning', r, 'Unable to set active theme, name: ' + skinTitle + " {" + skinId + "}", "You cannot modify Global Themes.<br><br>If you wish to base a theme off a global theme just rename the theme before saving.");
                })
            },
        })
    } // setActiveThemeAndClose

    function setProspectTitle(index, softT, hardT, inT)
    {
        if (!index) {
            index = 0;
        }

        if (index == 0) {
            $prospectTitleSoft.val('Soft');
            $prospectTitleSoft.prop('readonly', true);

            $prospectTitleHard.val('Hard');
            $prospectTitleHard.prop('readonly', true);

            $prospectTitleIn.val('In');
            $prospectTitleIn.prop('readonly', true);

            $('.prospectTitleLst', $dlg).revupDropDownField('setValue', prospectTitles[0]);

            $prospectTitleCustomMsg.hide();
        }
        else if (index == 1) {
            $prospectTitleSoft.val('Pleged');
            $prospectTitleSoft.prop('readonly', true);

            $prospectTitleHard.val('In Transit');
            $prospectTitleHard.prop('readonly', true);

            $prospectTitleIn.val('In Bank');
            $prospectTitleIn.prop('readonly', true);

            $('.prospectTitleLst', $dlg).revupDropDownField('setValue', prospectTitles[1]);

            $prospectTitleCustomMsg.hide();
        }
        else {
            if (softT) {
                $prospectTitleSoft.val(softT);
            }
            else {
                $prospectTitleSoft.val('');
            }
            $prospectTitleSoft.prop('readonly', false);

            if (hardT) {
                $prospectTitleHard.val(hardT);
            }
            else {
                $prospectTitleHard.val('');
            }
            $prospectTitleHard.prop('readonly', false);

            if (inT) {
                $prospectTitleIn.val(inT);
            }
            else {
                $prospectTitleIn.val('');
            }
            $prospectTitleIn.prop('readonly', false);

            $('.prospectTitleLst', $dlg).revupDropDownField('setValue', prospectTitles[2]);

            $prospectTitleCustomMsg.show();
        }
    } // setProspectTitle

    function buildProspectTitlesList()
    {
        // prospect screen title dropdown
        prospectTitlesIndex = 0;
        $prospectTitleSoft = $('.softInput', $dlg);
        $prospectTitleHard = $('.hardInput', $dlg);
        $prospectTitleIn = $('.inInput', $dlg);
        $prospectTitleCustomMsg = $('.customMsg', $dlg);
        $('.prospectTitleLst', $dlg).revupDropDownField({
            value: prospectTitles,
            valueStartIndex: prospectTitlesIndex,
            sortList: false,
            changeFunc: function(index, value) {
                prospectTitlesIndex = index;

                setProspectTitle(index);

                somethingChanged();
            },
        })
    } // buildProspectTitlesList

    return {
        load: function(template)
        {
            // keep the template data
            templateData = template;

            // build the template for the message
            var sTmp = [];

            sTmp.push('<div class="skiningDlg">');
                sTmp.push('<div class="previewSection">');
                    // header
                    sTmp.push('<div class="headerDiv">');
                        sTmp.push('<img class="revupLogo dynamicLogo logoImg" src="' + templateData.logoUrl + '">');
                    sTmp.push('</div>');

                    // content
                    sTmp.push('<div class="contentContainerDiv">');
                        sTmp.push('<div class="contentContainer">');
                            sTmp.push('<div class="sideNav">');
                                sTmp.push('<div class="tab active tabActive">');
                                    sTmp.push('<div>');
                                        sTmp.push('<div class="icon icon-home"></div>');
                                        sTmp.push('<div class="text">Active</div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                                sTmp.push('<div class="tab tabInactive">');
                                    sTmp.push('<div>');
                                        sTmp.push('<div class="icon icon-home"></div>');
                                        sTmp.push('<div class="text">Inactive</div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');
                            sTmp.push('<div class="contentDiv">');
                                sTmp.push('<div class="imgDiv">');
                                    sTmp.push('<img class="bannerImg" src=' + templateData.bannerUrl + '>');
                                        if (!templateData.currentTemplate.bannerUrl || templateData.currentTemplate.bannerUrl == "") {
                                            sTmp.push('<div class="defaultBannerOrg">' + templateData.candidateName + '</div>');
                                        }
                                        else {
                                            sTmp.push('<div class="defaultBannerOrg" style="display: none">' + templateData.candidateName + '</div>');
                                        }
                                sTmp.push('</div>');

                                sTmp.push('<div class="cntlDiv">');
                                    sTmp.push('<div class="leftSide">');
                                        if (templateData.isAcademic || templateData.isStaff) {
                                            sTmp.push('<div class="rankingBox">');
                                                sTmp.push('<div class="text">99.9</div>');
                                            sTmp.push('</div>');
                                        }
                                    sTmp.push('</div>');

                                    sTmp.push('<div class="rightSide">');
                                        sTmp.push('<div class="lnkField">Text Link Color</div>');
                                        sTmp.push('<button class="secBtnField skinBtn secondary">Secondary Button</button>');
                                        sTmp.push('<button class="primaryBtnField skinBtn">Primary Button</button>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>')
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');

                sTmp.push('<div class="settingSection">');
                    sTmp.push('<div class="col1">');
                        // theme
                        sTmp.push('<div class="header">Theme</div>');
                        sTmp.push('<div class="fieldGrp">');
                            // theme drop down
                            sTmp.push('<div class="fieldDiv">');
                                sTmp.push('<div class="labelSideDiv">');
                                    sTmp.push('<div class="text">Load Theme</div>');
                                sTmp.push('</div>');
                                sTmp.push('<div class="fieldSideDiv">');
                                    sTmp.push('<div class="fieldDiv themeDropDown"></div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');

                            // save theme
                            sTmp.push('<div class="fieldDiv">');
                                sTmp.push('<div class="labelSideDiv">');
                                    sTmp.push('<div class="text">Theme Name</div>');
                                sTmp.push('</div>');
                                sTmp.push('<div class="fieldSideDiv">');
                                    sTmp.push('<input type="text" class="themeName">');
                                sTmp.push('</div>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');

                        // images
                        sTmp.push('<div class="header" style="margin-top:5px;">Images</div>')
                        sTmp.push('<div class="fieldGrp">');
                            // logo
                            sTmp.push('<div class="fieldDiv">');
                                sTmp.push('<div class="labelSideDiv">');
                                    sTmp.push('<div class="text" style="top:8px;">Logo</div>');
                                sTmp.push('</div>');
                                sTmp.push('<div class="fieldSideDiv">');
                                    sTmp.push('<input type="file" class="logoIcon" name="icon" style="display:none;">');
                                    sTmp.push('<button type="button" class="fileButton loadLogoBtn revupBtnDefault btnXSmall btnLtGray">Choose Logo</button>');
                                    sTmp.push('<div class="clearLogoBtn revupLnk">Clear</div>');
                                    sTmp.push('<div class="msg">Recommend Size: 90x35</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');

                            // banner
                            sTmp.push('<div class="fieldDiv">');
                                sTmp.push('<div class="labelSideDiv">');
                                    sTmp.push('<div class="text" style="top:8px;">Banner</div>');
                                sTmp.push('</div>');
                                sTmp.push('<div class="fieldSideDiv">');
                                    sTmp.push('<input type="file" class="bannerIcon" name="icon" style="display:none;">');
                                    sTmp.push('<button type="button" class="fileButton loadBannerBtn revupBtnDefault btnXSmall btnLtGray">Choose Banner</button>');
                                    sTmp.push('<div class="clearBannerBtn revupLnk">Clear</div>');
                                    sTmp.push('<div class="msg">Recommend Size: 945x231</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="col2">');
                        sTmp.push('<div class="header" style="margin-top:5px;">Colors</div>')
                        sTmp.push('<div class="fieldGrp">');
                            // background
                            sTmp.push('<div class="fieldDiv">');
                                sTmp.push('<div class="labelSideDiv">');
                                    sTmp.push('<div class="text">Background color</div>');
                                sTmp.push('</div>');
                                sTmp.push('<div class="fieldSideDiv">');
                                    sTmp.push('<div class="colorCntl  backgroundColor"></div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');

                            // primary button color
                            sTmp.push('<div class="fieldDiv">');
                                sTmp.push('<div class="labelSideDiv">');
                                    sTmp.push('<div class="text">Primary Button color</div>');
                                sTmp.push('</div>');
                                sTmp.push('<div class="fieldSideDiv">');
                                    sTmp.push('<div class="colorCntl primaryBtnColor"></div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');

                            // secondary button color
                            sTmp.push('<div class="fieldDiv">');
                                sTmp.push('<div class="labelSideDiv">');
                                    sTmp.push('<div class="text">Secondary Button color</div>');
                                sTmp.push('</div>');
                                sTmp.push('<div class="fieldSideDiv">');
                                    sTmp.push('<div class="colorCntl secBtnColor"></div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');

                            // link color
                            sTmp.push('<div class="fieldDiv">');
                                sTmp.push('<div class="labelSideDiv">');
                                    sTmp.push('<div class="text">Link color</div>');
                                sTmp.push('</div>');
                                sTmp.push('<div class="fieldSideDiv">');
                                    sTmp.push('<div class="colorCntl linkColor"></div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');

                            // inactive tab color
                            // sTmp.push('<div class="fieldDiv">');
                            //     sTmp.push('<div class="labelSideDiv">');
                            //         sTmp.push('<div class="text">Inactive Tab color</div>');
                            //     sTmp.push('</div>');
                            //     sTmp.push('<div class="fieldSideDiv">');
                            //         sTmp.push('<div class="colorCntl inactiveTabColor"></div>');
                            //     sTmp.push('</div>');
                            // sTmp.push('</div>');

                            // academic - score hightlight color
                            if (templateData.isAcademic || templateData.isStaff) {
                                sTmp.push('<div class="fieldDiv">');
                                    sTmp.push('<div class="labelSideDiv">');
                                        sTmp.push('<div class="text">Score highlight color</div>');
                                    sTmp.push('</div>');
                                    sTmp.push('<div class="fieldSideDiv">');
                                        sTmp.push('<div class="colorCntl scoreHighlightColor"></div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            }
                        sTmp.push('</div>');
                    sTmp.push('</div>');

                    sTmp.push('<div class="col3">');
                        if (templateData.isPoliticalCampaign || templateData.isPoliticalCommittee || templateData.isStaff) {
                            sTmp.push('<div class="header" style="margin-top:5px;">Candidate Icon</div>')
                            sTmp.push('<div class="fieldGrp">');
                                sTmp.push('<div class="infoText">');
                                    sTmp.push('Enter candidates initials or single name and choose a background color.  There is a limit of six characters.');
                                sTmp.push('</div>');
                                sTmp.push('<div class="singleLine">');
                                    sTmp.push('<div class="labelText">Initials or single name:</div>');
                                    sTmp.push('<input type="text" class="candidateIconName" maxlength=6 placeholder="' + templateData.defaultIconText + '">');
                                    sTmp.push('<div class="labelText">Sample:</div>');
                                    sTmp.push('<div class="candidateIconBox">');
                                        sTmp.push('<div class="text">Name</div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                                sTmp.push('<div class="singleLine">');
                                    sTmp.push('<div class="ltText">Example:</div>');
                                    sTmp.push('<div class="medText">Initial, First name, Last name, Nick name</div>');
                                sTmp.push('</div>');
                                sTmp.push('<div class="fieldDiv">');
                                    sTmp.push('<div class="labelSideDiv">');
                                        sTmp.push('<div class="text">Choose a background color</div>');
                                    sTmp.push('</div>');
                                    sTmp.push('<div class="fieldSideDiv">');
                                        sTmp.push('<div class="colorCntl candidateIconColor"></div>');
                                    sTmp.push('</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>');
                        }

                        sTmp.push('<div class="header" style="margin-top:5px;">Prospects Screen Contribution Display Titles</div>')
                        sTmp.push('<div class="fieldGrp">');
                            sTmp.push('<div class="prospectTitleLst"></div>');
                            sTmp.push('<div class="singleLine">');
                                sTmp.push('<input type="text" class="prospectInput softInput" maxlength=20 placeholder="">');
                                sTmp.push('<input type="text" class="prospectInput hardInput" maxlength=20 placeholder="">');
                                sTmp.push('<input type="text" class="prospectInput inInput" maxlength=20 placeholder="">');
                            sTmp.push('</div>');
                            sTmp.push('<div class="customMsg">All 3 values must be set before save is enabled</div>')
                        sTmp.push('</div>');

                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            var okBtnText = 'Save';
            var cancelText = 'Cancel';
            var enableOkBtn = false;
            //if (templateData.isStaff) {
            //    okBtnText = 'Close';
            //    cancelText = '';
            //    enableOkBtn = true;
            //}
            $dlg = $('body').revupDialogBox({
                title: 'Skin Settings',
                msg: sTmp.join(''),
                okBtnText: okBtnText,
                enableOkBtn: enableOkBtn,
                cancelBtnText: cancelText,
                autoCloseOk: false,
                isDragable: false,
                width: '1120px',
                height: '685px',
                top: '10',
                autoCloseCancel: false,
                cancelHandler: function () {
                    if (!$('.okBtn', $dlg).prop('disabled')) {
                        $dlg.revupConfirmBox({
                            headerText: 'Ok to cancel',
                            msg: 'Your changes have not been saved.<br><br>By pressing \"OK\", your changes will be lost.',
                            okHandler: function(e) {
                                // make sure all the color controls are hidden
                                $('.colorCntl', $dlg).each(function() {
                                    var $cntl = $(this);
                                    $cntl.revupColorCntl('hide');

                                    // close the dialog
                                    $dlg.revupDialogBox('close');
                                });
                            },
                        });
                    }
                    else {
                        // make sure all the color controls are hidden
                        $('.colorCntl', $dlg).each(function() {
                            var $cntl = $(this);
                            $cntl.revupColorCntl('hide');

                            // close the dialog
                            $dlg.revupDialogBox('close');
                        });
                    }
                },
                okHandler: function() {
                    saveStepOne();
                }
            });

            // get the list of theme
            $.ajax({
                url: templateData.themeLstUrl,
                type: "GET"
            })
            .done(function(r) {
                buildThemeList(r)
            })
            .fail(function(r) {
                revupUtils.apiError('Skinning', r, 'Unable to load list of theme', "Unable to load list of theme at this time.");
            })

            // build prospects titles List
            buildProspectTitlesList();

            // load file load handlers
            $('.loadLogoBtn', $dlg)
                .on('click', function(e) {
                    stylizedFilePressed(this, $('.logoIcon'), $('.logoImg'));
                })
                .on('revup.fileLoaded', function(e) {
                    $(this).attr('imageChanged', true);
                    $(this).attr('imageCleared', false);
                    somethingChanged(e);

                    $('.clearLogoBtn').removeAttr('style').removeClass('disabled');
                });

            $('.loadBannerBtn', $dlg)
                .on('click', function(e) {
                    if ($('.bannerImg').length == 0) {
                        $('.bannerImgDiv').replaceWith('<img class="bannerImg">');
                    }
                    stylizedFilePressed(this, $('.bannerIcon'), $('.bannerImg'));
                })
                .on('revup.fileLoaded', function(e) {
                    $(this).attr('imageChanged', true);
                    $(this).attr('imageCleared', false);
                    somethingChanged(e);

                    $('.clearBannerBtn').removeAttr('style').removeClass('disabled');
                });

            $('.clearLogoBtn', $dlg)
                .on('click', function(e) {
                    if (!$(this).hasClass('disabled')) {
                        $('.logoImg').attr('src', templateData.logoUrl);
                        //$('.loadLogoBtn').attr('imageChanged', true);
                        $('.loadLogoBtn').attr('imageCleared', true);
                        somethingChanged(e);

                        clearBtn($(this));
                    }
                });

            $('.clearBannerBtn', $dlg)
                .on('click', function(e) {
                    if (!$(this).hasClass('disabled')) {
                        $('.bannerImg').attr('src', templateData.bannerUrl);
                        //$('.loadBannerBtn').attr('imageChanged', true);
                        $('.loadBannerBtn').attr('imageCleared', true);
                        somethingChanged(e);

                        clearBtn($(this));
                    }
                });

            // load color controls
            var inactiveTabColor = $('.previewSection .tabInactive', $dlg).css('backgroundColor');
            $('.backgroundColor')
                .on('revup.newColor', function(e) {
                    somethingChanged(e)
                })
                .revupColorCntl({
                    $connectedField: $('.previewSection', $dlg),
                    colorVal: $('.previewSection', $dlg).css('backgroundColor'),
                    colorType: 'background',
                    colorFor: 'backgroundDiv',
                    cssRule: '.backgroundDiv',
                    focusColor: revupConstants.color.primaryGray4,
                    notInFocusColor: '#ededed',
                });
            $('.primaryBtnColor')
                .revupColorCntl({
                    $connectedField: $('.previewSection .primaryBtnField', $dlg),
                    colorVal: $('.previewSection .primaryBtnField', $dlg).css('backgroundColor'),
                    colorType: 'background, border',
                    colorFor: 'primaryBtnField',
                    focusColor: revupConstants.color.primaryGray4,
                    notInFocusColor: '#ededed',
                }).on('revup.newColor', function(e) {
                    somethingChanged(e)
                });
            $('.secBtnColor')
                .revupColorCntl({
                    $connectedField: $('.previewSection .secBtnField', $dlg),
                    colorVal: $('.previewSection .secBtnField', $dlg).css('backgroundColor'),
                    colorType: 'background, border',
                    colorFor: 'secBtnField',
                    focusColor: revupConstants.color.primaryGray4,
                    notInFocusColor: '#ededed',
                }).on('revup.newColor', function(e) {
                    somethingChanged(e)
                });
            $('.linkColor')
                .revupColorCntl({
                    $connectedField: $('.previewSection .lnkField', $dlg),
                    colorVal: $('.previewSection .lnkField', $dlg).css('color'),
                    colorType: 'color',
                    colorFor: 'lnkField',
                    focusColor: revupConstants.color.primaryGray4,
                    notInFocusColor: '#ededed',
                }).on('revup.newColor', function(e) {
                    somethingChanged(e)
                });
            $('.inactiveTabColor')
                .revupColorCntl({
                    $connectedField: $('.previewSection .tabInactive', $dlg),
                    colorVal:       $('.previewSection .tabInactive', $dlg).css('backgroundColor'),
                    colorFor:        'tabInactive',
                    otherFields:     [{$field: $(), colorType: 'color', colorVal: inactiveTabColor, colorFor: 'tabActive', cssRule: '.pageLabel'},
                                      {$field: $('.previewSection .tabActive', $dlg), colorVal: inactiveTabColor, colorType: 'color', colorFor: 'tabActive'},
                                      {$field: $('.previewSection .tabActive .icon', $dlg), colorVal: inactiveTabColor, colorType: 'clearBorderTab', colorFor: 'tabInactive', cssRule: '.mainContent .container .sideNav .tab.active'},
                                      {$field: $('.previewSection .tabActive .icon', $dlg), colorVal: inactiveTabColor, colorType: 'color', colorFor: 'tabActive icon', cssRule: '.mainContent .container .sideNav .tab.active a .icon'},
                                      {$field: $('.previewSection .tabActive .text', $dlg), colorVal: inactiveTabColor, colorType: 'color', colorFor: 'tabActive text', cssRule: ['.mainContent .container .sideNav .tab.active a .text', '.mainContent .container .sideNav .tab.active a:hover .text']}],
                    colorType: 'background, borderTab',
                    focusColor: revupConstants.color.primaryGray4,
                    notInFocusColor: '#ededed',
                }).on('revup.newColor', function(e) {
                    somethingChanged(e)
                });
            $('.scoreHighlightColor')
                .revupColorCntl({
                    $connectedField: $('.previewSection .rankingBox', $dlg),
                    colorVal: " #a4b58e",
                    colorFor: 'scoreHighlight',
                    colorType: 'background',
                    focusColor: revupConstants.color.primaryGray4,
                    notInFocusColor: '#ededed',
                }).on('revup.newColor', function(e) {
                    somethingChanged(e)
                });
            $('.candidateIconColor')
                .revupColorCntl({
                    $connectedField: $('.settingSection .candidateIconBox', $dlg),
                    colorVal: " #90c3f0",
                    colorFor: 'candidateIcon',
                    colorType: 'background',
                    focusColor: revupConstants.color.primaryGray4,
                    notInFocusColor: '#ededed',
                }).on('revup.newColor', function(e) {
                    somethingChanged(e)
                });

            // candidate icon handlers
            var $candidateIconName = $('.settingSection .candidateIconName', $dlg);
            var $candidateIconBoxText = $('.settingSection .candidateIconBox .text', $dlg);
            if (templateData.isPoliticalCampaign || templateData.isPoliticalCommittee || templateData.isStaff) {
                $candidateIconName
                    .on('change keyup paste cut', function(e) {
                        var val = $candidateIconName.val();
                        //if (val == '') {
                        //    val = currentSkin.defaultAccountIconText;
                        //}

                        $candidateIconBoxText.text(val);
                        somethingChanged(e);
                    })
            }

            // prospect titles
            $prospectTitleSoft.on('change keyup paste cute', function(e) {
                somethingChanged(e)
            });

            $prospectTitleHard.on('change keyup paste cute', function(e) {
                somethingChanged(e)
            });

            $prospectTitleIn.on('change keyup paste cute', function(e) {
                somethingChanged(e)
            });

            // theme name handlers
            $themeName = $('.themeName', $dlg);
            $themeName.on('keyup', function(e) {
                somethingChanged(e);
            });
        }
    }
} ());
