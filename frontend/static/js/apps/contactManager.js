/* exported contactManager */

var contactManager = (function () {
    "use strict";

    var templateData = {};

    // selectors
    var $btnBar =               $('.contactManager .buttonBar');
    var $contactContainer =     $('.contactManager .contactConnectionDiv');
    var $contactPersonalContainer = $('.contactManager .contactConnectionDiv.userContacts');
    var $contactAccountContainer = $('.contactManager .contactConnectionDiv.accountContacts');
    var $selectedContactName =  $();
    var $selectedContactEmailCol = $();
    var $selectedContactEmail = $();
    let $clonebuck = $();

    var taskFetchTimer = null;

    // contact limit for warning message
    const contactLimitsWarning = 0.85;      // display warning when 85% of contacts consumed
    let userContactsMsg = '';               // what type of message
    let adminContactsMsg = '';              // the type of admin contact message
    let totalUserContacts = '0';            // the maximum user contacts
    let totalAdminContacts = '0';           // the maximum admin contacts

    // onboarding
    var onBoarding = false;

    // csv config validate
    let contactCSV = null;

    // update the status
    var numContactsToFetch = 25;

    var currentEditContact = {};        // currently loaded edit contact

    var bEnterPage = false;             // when entering page a flag to make sure to check both the user and account status

    /*
     * Helper functions
     */
    function humanReadableType(imType)
    {
        var humanType = "";
        switch (imType) {
            case 'CV':
            case 'cv':
            case 'CSV':
                humanType = "CSV";
                break;
            case 'GM':
            case 'gm':
            case 'Gmail':
                humanType = 'Gmail';
                break;
            case 'li':
            case 'LI':
            case 'LinkedIn':
                humanType = 'LinkedIn';
                break;
            case 'ou':
            case 'OU':
            case 'Outlook':
                humanType = 'Outlook';
                break;
            case 'ap':
            case 'AP':
            case 'vc':
            case 'VC':
            case 'ot':
            case 'OT':
            case 'ab':
            case 'AB':
            case 'VCard':
                humanType = 'Apple';
                break;
            case 'ip':
            case 'IP':
            case 'mb':
            case 'MB':
            case 'iPhone':
                humanType = "Mobile";
                break;
            case 'cloned':
            case 'CC':
                humanType = 'Cloned';
                break;
            default:
                humanType = 'Undefined';
                break;
        } // end switch - imType

        return humanType;
    } // humanReadableType

    let displayContactLimits = (r) => {
        // USER - PERSONAL SECTION
        if (r.user) {
            const $contactUserSection = $('.contactUserSection .contactInfoDiv');

            // commify the counts
            $('.numContacts', $contactUserSection).html(revupUtils.commify(r.user.total_analyzed));
            $('.totalContacts', $contactUserSection).html(revupUtils.commify(r.user.analyze_limit));
            totalUserContacts = revupUtils.commify(r.user.analyze_limit);

            // display the number of deleted, active and available contacts
            $('.deletedContacts .count', $contactUserSection).html(revupUtils.commify(r.user.total_deleted));
            $('.activeContacts .count', $contactUserSection).html(revupUtils.commify(r.user.total_count));
            if ((r.user.total_deleted + r.user.total_count) > r.user.analyze_limit) {
                $('.remainingContacts .count', $contactUserSection).html('0');
            }
            else {
                let freeSpace = r.user.analyze_limit - (r.user.total_deleted + r.user.total_count);
                $('.remainingContacts .count', $contactUserSection).html(revupUtils.commify(freeSpace));
            }

            let percentDeleted = Math.round((r.user.total_deleted / r.user.analyze_limit) * 100);
            percentDeleted = percentDeleted > 100 ? 100 : percentDeleted;
            let percentActive = Math.round((r.user.total_count / r.user.analyze_limit) * 100);
            percentActive = percentActive > 100 ? 100 : percentActive;

            if ((percentDeleted + percentActive) > 100) {
                percentActive = 100 - percentDeleted;
            }
            let percentRemaining = 100 - (percentDeleted + percentActive)

            if (percentDeleted == 0) {
                $('.contactGraphDiv .deletedContacts', $contactUserSection).css('width', percentDeleted + '%')
                                                                                          .addClass('leftSide')
            }
            else {
                $('.contactGraphDiv .deletedContacts', $contactUserSection).css('width', percentDeleted + '%')
                                                                                          .addClass('leftEnd')
                                                                                          .show();
                if (percentActive == 0 && percentRemaining == 0) {
                    $('.contactGraphDiv .deletedContacts', $contactUserSection).addClass('rightEnd')
                }
            }

            if (percentActive == 0) {
                $('.contactGraphDiv .activeContacts', $contactUserSection).hide();
            }
            else {
                $('.contactGraphDiv .activeContacts', $contactUserSection).css('width', percentActive + '%')
                                                                                          .show();
                if (percentDeleted == 0) {
                    $('.contactGraphDiv .activeContacts', $contactUserSection).addClass('leftEnd')
                }
                if (percentRemaining == 0) {
                    $('.contactGraphDiv .activeContacts', $contactUserSection).addClass('rightEnd')
                }
            }

            if (percentRemaining == 0) {
                $('.contactGraphDiv .remainingContacts', $contactUserSection).hide();
            }
            else {
                $('.contactGraphDiv .remainingContacts', $contactUserSection).css('width', percentRemaining + '%')
                                                                                            .addClass('rightEnd')
                                                                                            .show();
                if (percentDeleted == 0 && percentActive == 0) {
                    $('.contactGraphDiv .remainingContacts', $contactUserSection).addClass('leftEnd')
                }
            }

            // see if the message box should be display and which message to display
            let $msgBox = $('.contactUserSection .contactErrorWarningMsg');
            if (r.user.total_analyzed >= r.user.analyze_limit) {
                // error
                userContactsMsg = 'error';

                $msgBox.removeClass('warning')
                       .addClass('error')
                       .show();

                let msg = 'You have reached your limit of ' + revupUtils.commify(r.user.analyze_limit) + ' contacts. You may still add details to ';
                msg += 'existing contact information via upload, but no new contacts will be created.'
                $('.msgBox .textDiv .text', $msgBox).html(msg);
            }
            else if (((r.user.total_analyzed / r.user.analyze_limit)) > contactLimitsWarning) {
                // warning
                userContactsMsg = 'warning';

                $msgBox.removeClass('error')
                       .addClass('warning')
                       .show();

                let msg = 'You are nearing your limit of ' + revupUtils.commify(r.user.analyze_limit) + ' analyzed personal account contacts.';
                $('.msgBox .textDiv .text', $msgBox).html(msg);
            }
            else {
                userContactsMsg = '';

                $msgBox.hide();
            }

            // event handler
            $('.contactErrorWarningMsg .msgBox .closeBtn').off('click')
                                                          .on('click', function() {
                                                              let $msgBox = $(this).closest('.contactErrorWarningMsg');
                                                              $msgBox.hide();
                                                          });
        }

        // ADMIN - SHARED SECTION
        if (r.account) {
            const $contactAccountSection = $('.contactAccountSection .contactInfoDiv');

            // commify the counts
            $('.numContacts', $contactAccountSection).html(revupUtils.commify(r.account.total_analyzed));
            $('.totalContacts', $contactAccountSection).html(revupUtils.commify(r.account.analyze_limit));
            totalAdminContacts = revupUtils.commify(r.account.analyze_limit);

            // display the number of deleted, active and available contacts
            $('.deletedContacts .count', $contactAccountSection).html(revupUtils.commify(r.account.total_deleted));
            $('.activeContacts .count', $contactAccountSection).html(revupUtils.commify(r.account.total_count));
            if ((r.account.total_deleted + r.account.total_count) > r.account.analyze_limit) {
                $('.remainingContacts .count', $contactAccountSection).html('0');
            }
            else {
                let freeSpace = r.account.analyze_limit - (r.account.total_deleted + r.account.total_count);
                $('.remainingContacts .count', $contactAccountSection).html(revupUtils.commify(freeSpace));
            }

            let percentDeleted = Math.round((r.account.total_deleted / r.account.analyze_limit) * 100);
            percentDeleted = percentDeleted > 100 ? 100 : percentDeleted;
            let percentActive = Math.round((r.account.total_count / r.account.analyze_limit) * 100);
            percentActive = percentActive > 100 ? 100 : percentActive;

            if ((percentDeleted + percentActive) > 100) {
                percentActive = 100 - percentDeleted;
            }
            let percentRemaining = 100 - (percentDeleted + percentActive)

            if (percentDeleted == 0) {
                $('.contactGraphDiv .deletedContacts', $contactAccountSection).css('width', percentDeleted + '%')
                                                                                          .addClass('leftSide')
            }
            else {
                $('.contactGraphDiv .deletedContacts', $contactAccountSection).css('width', percentDeleted + '%')
                                                                                          .addClass('leftEnd')
                                                                                          .show();
                if (percentActive == 0 && percentRemaining == 0) {
                    $('.contactGraphDiv .deletedContacts', $contactAccountSection).addClass('rightEnd')
                }
            }

            if (percentActive == 0) {
                $('.contactGraphDiv .activeContacts', $contactAccountSection).hide();
            }
            else {
                $('.contactGraphDiv .activeContacts', $contactAccountSection).css('width', percentActive + '%')
                                                                                          .show();
                if (percentDeleted == 0) {
                    $('.contactGraphDiv .activeContacts'. $contactAccountSection).addClass('leftEnd')
                }
                if (percentRemaining == 0) {
                    $('.contactGraphDiv .activeContacts', $contactAccountSection).addClass('rightEnd')
                }
            }

            if (percentRemaining == 0) {
                $('.contactGraphDiv .remainingContacts', $contactAccountSection).hide();
            }
            else {
                $('.contactGraphDiv .remainingContacts', $contactAccountSection).css('width', percentRemaining + '%')
                                                                                            .addClass('rightEnd')
                                                                                            .show();
                if (percentDeleted == 0 && percentActive == 0) {
                    $('.contactGraphDiv .remainingContacts', $contactAccountSection).addClass('leftEnd')
                }
            }

            // see if the message box should be display and which message to display
            let $msgBox = $('.contactAccountSection .contactErrorWarningMsg');
            if (r.account.total_analyzed >= r.account.analyze_limit) {
                // error
                adminContactsMsg = 'error';

                $msgBox.removeClass('warning')
                       .addClass('error')
                       .show();

                let msg = 'You have reached your limit of ' + revupUtils.commify(r.account.analyze_limit) + ' contacts. You may still add details to ';
                msg += 'existing contact information via upload, but no new contacts will be created.'
                $('.msgBox .textDiv .text', $msgBox).html(msg);
            }
            else if (((r.account.total_analyzed / r.account.analyze_limit)) > contactLimitsWarning) {
                // warning
                adminContactsMsg = 'warning';

                $msgBox.removeClass('error')
                       .addClass('warning')
                       .show();

                let msg = 'You are nearing your limit of ' + revupUtils.commify(r.account.analyze_limit) + ' analyzed personal account contacts.';
                $('.msgBox .textDiv .text', $msgBox).html(msg);
            }
            else {
                adminContactsMsg = '';
                $msgBox.hide();
            }
        }
    } // displayContactLimits

    function refreshContactStatus()
    {
        function updateStatus(contactData, $container)
        {
            // get the container
            var $c = $();
            $c = $('[importId="' + contactData.id + '"]', $container);
            if ($c.length === 0) {
                // newly added entry
                if (contactData.source == 'Cloned') {
                    $c = $('[importLabel="' + contactData.title + '"]', $container);
                }
                else {
                    $c = $('[importType="' + contactData.import_record.import_type.toLowerCase() + '"][importLabel="' + contactData.title + '"]', $container);
                }

                // add the importId
                $c.attr('importId', contactData.id);
                $c.attr('uId', contactData.uid);
            }

            // update count and date
            if ($c.length > 0) {
                // update count
                var total = "No";
                $c.attr('importId', contactData.id);
                if (contactData.count && contactData.count > 0) {
                    total = revupUtils.commify(contactData.count);
                    $('.numContactsText', $c).show();
                }
                else {
                    $('.numContactsText', $c).hide();
                }

                $(".numContacts", $c).text(total);

                // update date
                let date = 'N/A';
                if (contactData.import_record && contactData.import_record.import_dt) {
                    date = revupUtils.formatDateText(contactData.import_record.import_dt);
                }
                else {
                    date = revupUtils.formatDateText(contactData.modified);
                }
                $('.bottomPart .syncDate', $c).text(revupUtils.formatDateText(date));

            }
        } // updateStatus

        // get the list of contacts
        $.ajax({
            url: templateData.getImportRecordsLst2,
            type: "GET",
        })
        .done(function(r) {
            // display the contacts limits
            displayContactLimits(r);

            // update the icons
            if (r.user && r.user.results) {
                for (var i = 0; i < r.user.results.length; i++) {
                    updateStatus(r.user.results[i], $('.contactConnectionDiv.userContacts'));
                }
            }

            if (r.account && r.account.results) {
                for (var i = 0; i < r.account.results.length; i++) {
                    updateStatus(r.account.results[i],  $('.contactConnectionDiv.accountContacts'));
                }
            }
        })
        .fail(function(r) {
            console.error('Unable to load celery status: ', r);
            revupUtils.apiError('Contact Manager', r, 'Unable to loading existing Contact Sources for status update');
        });
    } // refreshContactStatus

    let deleteContactSource = (contactSrcId, contactSrcLabel, section) => {
        let $delInfoDlg = $();
        let doDeleteContactSource = (contactSrcId) => {
var startTime = new Date().getTime();
            var deleteContactSrcId = contactSrcId;
            let url = templateData.deleteImportRecordsLst2.replace('IMPORTID', contactSrcId)
            $.ajax({
                url: url,
                type: 'DELETE',
            })
            .done(function(r) {
console.log('delete source time(done): ' +  (new Date().getTime() - startTime) + ' ms')
                // remove the entry and fetch a replacement
                let $contactSrc = $('.contactContainer[importId="' + contactSrcId + '"]');
                $contactSrc.remove();

                // close the dialog
                $delInfoDlg.revupConfirmBox('close');
            })
            .fail(function(r) {
                console.error('Unable to delete contact source: ' + deleteContactSrcId + ", reasult: ", r);
                revupUtils.apiError('Delete Contact Source', r, 'Unable to delete contact source: ' + contactId);
            })
        } // doDeleteContactSource

        let url = templateData.deleteImportRecordsLst2.replace('IMPORTID', contactSrcId)
        url += '?dryrun=true'

        let sTmp = [];

        sTmp.push('<div class="delConfirmDlg">');
            sTmp.push('<div class="waitingDiv loadingDiv">');
                sTmp.push('<div class="loading"></div>');
                sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
            sTmp.push('</div>')
            sTmp.push('<div class="content">');
                sTmp.push('<div class="fetchDeleteInfo">');
                    sTmp.push('<div class="loadingMsg">Collecting contact data for deletion</div>');
                    sTmp.push('<div class="loadingDiv">');
                        sTmp.push('<div class="loading"></div>');
                        sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                    sTmp.push('</div>')
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>')

        $delInfoDlg = $('body').revupConfirmBox({
            okBtnText:          'Delete',
            enableOkBtn:        false,
            headerText:         'Delete Contacts',
            msg:                sTmp.join(''),
            extraClass:         'deleteContactConfirm',
            autoCloseOk:        false,
            zIndex:             30,
            okHandler:          function () {
                                    doDeleteContactSource(contactSrcId);
            },
            cancelHandler:      function() {
                let $contactSrc = $('.contactContainer[importId="' + contactSrcId + '"]');
                $('.deleteContactSrcBtn', $contactSrc).removeClass('disabled');
            }
        })

        // display the loading spinner
        //$('.waitingDiv', $delInfoDlg).show();

        $.ajax({
            url: url,
            type: "DELETE",
        })
        .done(function(r) {
            let sTmp = [];
            sTmp.push('<div class="leftSide">');
                sTmp.push('<div class="warningIcon icon icon-warning"></div>');
            sTmp.push('</div>');
            sTmp.push('<div class="rightSide">');
                sTmp.push('<div class="msg">');
                    sTmp.push('Are you sure you want to remove the contact source ');
                    sTmp.push('<div class="msgBold limit">' + contactSrcLabel + '?' + '</div>');
                sTmp.push('<div class="msg newParagraph">');
                    sTmp.push('This will ');
                    sTmp.push('<div class="boldRed">permanently</div>');
                    sTmp.push(' remove:')
                sTmp.push('</div>');

                sTmp.push('<div class="dryRunResults">');
                    sTmp.push('<div class="entry">');
                        sTmp.push('<div class="val">' + revupUtils.commify(r.Contacts) + '</div>');
                        sTmp.push('<div class="eVal">contacts</div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="entry">');
                        sTmp.push('<div class="val">' + revupUtils.commify(r.Notes) + '</div>');
                        sTmp.push('<div class="eVal">contact notes</div>');
                    sTmp.push('</div>');

                    if (section == 'account') {
                        sTmp.push('<div class="entry">');
                            sTmp.push('<div class="val">' + revupUtils.commify(r.Shares) + '</div>');
                            sTmp.push('<div class="eVal">shares</div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entry">');
                            sTmp.push('<div class="val">' + revupUtils.commify(r['Call Time Contacts']) + '</div>');
                            sTmp.push('<div class="eVal">call time contacts</div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entry">');
                            sTmp.push('<div class="val">' + revupUtils.commify(r['Call Logs']) + '</div>');
                            sTmp.push('<div class="eVal">call records</div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="entry">');
                            sTmp.push('<div class="val">' + revupUtils.commify(r['Call Groups']) + '</div>');
                            sTmp.push('<div class="eVal">from call groups</div>');
                        sTmp.push('</div>');
                    }
                    else if (section == 'personal') {
                        sTmp.push('<div class="entry">');
                            sTmp.push('<div class="val">' + revupUtils.commify(r['Prospects']) + '</div>');
                            sTmp.push('<div class="eVal">Prospects</div>');
                        sTmp.push('</div>');
                    }
                sTmp.push('</div>')

                sTmp.push('<div class="msg newParagraph">');
                    sTmp.push('<div class="boldRed">This cannot be undone.</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            // load the content and clear the spinner
            $('.content', $delInfoDlg).html(sTmp.join(''));
            $('.btnDiv .okBtn').removeClass('disabled');
            $('.waitingDiv', $delInfoDlg).hide();
        })
        .fail(function(r) {
            console.error('FAIL - delete dry run: ', r);

            let sTmp = [];
            sTmp.push('<div class="leftSide">');
                sTmp.push('<div class="warningIcon icon icon-warning"></div>');
            sTmp.push('</div>');
            sTmp.push('<div class="rightSide">');
                sTmp.push('<div class="msg">');
                    sTmp.push('Are you sure you want to remove the contact source ');
                    sTmp.push('<div class="msgBold limit">' + contactSrcLabel + '?' + '</div>');
                sTmp.push('<div class="msg newParagraph">');
                    sTmp.push('This will ');
                    sTmp.push('<div class="boldRed">permanently</div>');
                    sTmp.push(' remove:')
                sTmp.push('</div>');

                sTmp.push('<div class="dryRunResults">');
                    sTmp.push('<div class="entry paragraph">');
                        sTmp.push('<div class="failMsg">');
                            sTmp.push('We are unable to display a complete list of what you will be deleting.');
                        sTmp.push('</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>')

                sTmp.push('<div class="msg newParagraph">');
                    sTmp.push('<div class="boldRed">This cannot be undone.</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            // load the content and clear the spinner
            $('.content', $delInfoDlg).html(sTmp.join(''));
            // $('.content', $delInfoDlg).html('<div class="errorMsg">Unable to load information about what you will be deleting.</div>');
            $('.btnDiv .okBtn').removeClass('disabled');
            //$('.waitingDiv', $delInfoDlg).hide();
        })
    } // deleteContactSource

    /*
     * shareContactList
     */
    let shareContactList = ($cloneParent, importId) => {
        let contactString = "'s Contacts";
        let $eachCloneBuck = $('.contactContainer.cloneDiv[importlabel="' + ($('.contactAccountSection').attr('username')) + contactString + '"]', $contactAccountContainer);
        let url = templateData.getImportRecordsLst2 + importId + '/clone/';
        $('.cloneBtn', $contactPersonalContainer).addClass('disabled');
        $('.deleteContactSrcBtn', $cloneParent).addClass('disabled');
        $('.deleteContactSrcBtn', $eachCloneBuck).addClass('disabled');

        var sTmp = [];
        sTmp.push('<div class="loadingDiv">');
            sTmp.push('<div class="loading" nstyle="xmargin-left:5px;width:25px;height:25px"></div>');
        sTmp.push('</div>');

        $('.contactLoadingDiv', $clonebuck).html(sTmp.join(''))
                                         .show();

        $.ajax({
            url: url,
            type: 'POST',
        })
        .done(function(r) {
        })
        .fail(function(r) {
            console.error('Unable to share list: ', r);

            var sTmp = [];
            sTmp.push('<div class="failed">');
                sTmp.push('<div class="icon icon-warning"></div>');
                sTmp.push('<div class="txt">Unable to Clone</div>');
            sTmp.push('</div>');

            $('.contactCountDiv', $cloneParent).hide();
            $('.contactLoadingDiv', $cloneParent).html(sTmp.join(''))
                                             .show();

            // put up a cloning error message for 30 seconds, then remove the message and re-enable the buttons
            setTimeout(function() {
                let $eachCloneBuck = $('.contactContainer.cloneDiv[importlabel="' + ($('.contactAccountSection').attr('username')) + contactString + '"]', $contactAccountContainer);
                $('.contactLoadingDiv', $cloneParent).html('');
                $('.contactCountDiv', $cloneParent).removeClass('disabled').show();
                $('.cloneBtn', $contactPersonalContainer).removeClass('disabled');
                $('.deleteContactSrcBtn', $cloneParent).removeClass('disabled');
                $('.deleteContactSrcBtn', $eachCloneBuck).removeClass('disabled');
            }, 30000);
        });
    } // shareContactlist

    /*
     * Event handlers
     */
    function loadEventHanders()
    {
        $('.addContactBtn').on('click', function(e) {
            // make sure the onboard class has been removed
            $(this).removeClass('onBoarding');
            // passing the possible error messages to contactSource
            templateData.userContactsMsg = userContactsMsg;
            templateData.adminContactsMsg = adminContactsMsg;
            contactSource.load('btnBar', templateData);
        });

        $('.editContactBtn').on('click', function(e) {
            editContact.load('editContacts');
        });

        $('.addAContactBtn').on('click', function(e) {
          editContact.load('addNewContact');
        });

        $('.manageContactMergeBtn', $btnBar).on('click', function(e) {
            manageContactMerge.display(templateData.hasAdminAccess);
        });

        $('.contactInfoDiv .infoBtn').on('click', function(e) {
            let $btn = $(this);

            let sTmp = [];
            sTmp.push('<div class="aboutContactsDiv">');
                sTmp.push('<div class="heading">Personal Contacts - A maximum of ' + totalUserContacts + ' analyzed contacts</div>');
                sTmp.push('<div class="msg">')
                    sTmp.push('Personal Contacts are visible only to you, and cannot be shared or ');
                    sTmp.push('utilized in Call Time. If you want to share them you must export them into ');
                    sTmp.push('a CSV formatted file and then reimport them as Shared Account Contacts.');
                sTmp.push('</div>');
                sTmp.push('<div class="heading">Shared Account Contacts - A maximum of ' + totalAdminContacts + ' analyzed contacts</div>');
                sTmp.push('<div class="msg">')
                    sTmp.push('Shared Account Contacts are visible to all administrators of the account. ');
                    sTmp.push('They can be utilized in Call Time as well as shared with regular users via ');
                    sTmp.push('the List Manager > Share function.');
                sTmp.push('</div>');
                sTmp.push('<div class="msg" style="margin-bottom:0px">')
                    sTmp.push('If you have mistakenly uploaded an incorrectly formatted CSV file which ');
                    sTmp.push('was subsequently deleted you may request a contact count reset by ');
                    sTmp.push('emailing <a href="mailto:clients@revup.com"  target="_top">clients@revup.com</a>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            let $info;
            $info = $btn.revupPopup({bAutoClose: true,
                                     autoCloseTimeout: 20000,
                                     bSingle: true,
                                     location: 'right',
                                     zIndex: 25,
                                     content: sTmp.join(''),
                                    });
        })

        $('.contactUserSection, .contactAccountSection')
            .on('click', '.deleteContactSrcBtn', function(e) {
                // see if disabled
                if ($(this).hasClass('disabled')) {
                    return;
                }

                // disable the delete button
                $(this).addClass('disabled');

                // get the contact source id
                let $contactContainer = $(this).closest('.contactContainer');
                let id = $contactContainer.attr('importId');
                let label = $contactContainer.attr('importLabel');
                let section = '';
                let $contactContainerDiv = $contactContainer.closest('.contactConnectionDiv');
                if ($contactContainerDiv.hasClass('accountContacts')) {
                    section = 'account';
                }
                else if ($contactContainerDiv.hasClass('userContacts')) {
                    section = 'personal';
                }

                // delete the contact source
                deleteContactSource(id, label, section);
            })
            .on('click', '.addContact', function(e) {
                // passing the possible error messages to contactSource
                templateData.userContactsMsg = userContactsMsg;
                templateData.adminContactsMsg = adminContactsMsg;
                contactSource.load('contactContainer', templateData);
            })
            .on('click', '.settingsBtn', function(e) {
                var $container = $(this).closest('.contactContainer');

                var importType = $container.attr('importType');
                var importLabel = $container.attr('importLabel');

                var sTmp = [];
                sTmp.push('<div class="settingDlg">');
                    sTmp.push('<div class="txt">List Name</div>');
                    sTmp.push('<div class="nameDiv">');
                        sTmp.push('<div class="nameLabel">' + humanReadableType(importType) + '&nbsp;-&nbsp;</div>');
                        sTmp.push('<input class="listName" value="' + importLabel + '">');
                    sTmp.push('</div>');
                sTmp.push('</div>');
                var $settingsDlg = $('body').revupConfirmBox({
                    headerText: "Settings",
                    msg: sTmp.join(''),
                    zIndex: 50,
                    autoCloseOk: false,
                    okBtnText: 'Save',
                    okHandler: function () {
                        // close
                        $settingsDlg.trigger('revup.confirmClose');
                    }
                });

                var $okBtn = $('.btnDiv .okBtn', $settingsDlg);
                if (importLabel === '') {
                    $okBtn.prop('disabled', true);
                }

                $settingsDlg.on('keyup', '.listName', function(e) {
                    var $input = $(this);
                    if ($input.val() === '') {
                        $okBtn.prop('disabled', true);
                    }
                    else {
                        $okBtn.prop('disabled', false);
                    }
                });
            })
            .on('click', '.cloneBtn', function(e) {
                if($(this).hasClass('disabled')){
                    return;
                }
                let $this = $(this).parent();

                let sTmp = [];
                sTmp.push('<div class="shareSourcePopup">');
                sTmp.push('<div class="waitingDiv loadingDiv">');
                    sTmp.push('<div class="loading"></div>');
                    sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                sTmp.push('</div>')

                sTmp.push('<div class=shareContent">');
                    sTmp.push('<div class="deleteWarningIcon icon icon-warning">');
                    sTmp.push('</div>');
                    sTmp.push('<div class="shareSourcePopupText">');
                        sTmp.push('<p>Are you sure you want to</p>');
                        sTmp.push('<p>share the contact list?</p>');
                        sTmp.push('<br>');
                        sTmp.push('<div class="sourceContainer">');
                            sTmp.push('<div class="sourceIcon">');
                                sTmp.push('<div class="' + importSourceIcon($this.attr('importtype'))+ '"></div>');
                            sTmp.push('</div>');
                            sTmp.push('<div class="fileName">' + $this.attr('importtype') + ' - ' + $this.attr('importlabel').replace('.csv', '') + '</div>');
                        sTmp.push('</div>');
                        sTmp.push('<br>');
                        sTmp.push('<p>This will create a new shared list that </p>');
                        sTmp.push('<p>contains copies of these Personal Contacts.</p>');
                        sTmp.push('<p>It will be contained in a new list titled</p>');
                        sTmp.push('<p>with your user name.</p>');
                    sTmp.push('</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');

                let $shareSourcePopup = $('body').revupDialogBox({
                                                                cancelBtnText:      'Cancel',
                                                                cancelHandler:      function(){
                                                                                           console.log('shareSource cancel btn');
                                                                                      },
                                                                okBtnText:          'OK',
                                                                okHandler:          function () {
                                                                                            shareContactList($this, $this.attr('importid'));
                                                                                      },
                                                                headerText:         'Share Contact List',
                                                                msg:                sTmp.join(''),
                                                                top:                175,
                                                                zIndex:             200,
                                                                width:              '340px',
                                                                height:             '280px',
                                                                isDragable:         true,
                                                            });
            })
    } // loadEventHanders

    /*
     * display contact sources
     */
    function updateStatus(data)
    {
        function deleteTask(taskData)
        {
            // We check for import_dt because it's an easy way of comparing
            // a task record from an import record. We don't want to send delete
            // signals for an import record.
            var status = taskData.status.toLowerCase();

            if (!taskData.import_dt && (status === 'completed' || status === 'failed')) {
                if (taskData.task_type == 'CC') {
                    let $eachClonebuck = $('.contactContainer.cloneDiv[importlabel="' + data[i].destination_label + '"]', $contactAccountContainer);
                    let $parentList = $('.contactContainer[importlabel="' + data[i].label + '"]', $contactPersonalContainer);

                    $('.contactLoadingDiv', $eachClonebuck).hide();
                    $('.contactCountDiv', $eachClonebuck).show();
                    $('.deleteContactSrcBtn', $eachClonebuck).removeClass('disabled');
                    $('.numContacts', $eachClonebuck).show();
                    $('.text', $eachClonebuck).show();
                    $('.cloneBtn', $contactPersonalContainer).removeClass('disabled');
                    $('.deleteContactSrcBtn', $parentList).removeClass('disabled');
                }
                if (taskData.id) {
                    var delUrl = templateData.deleteTask;
                    $.ajax({
                        url: delUrl + taskData.id + "/",
                        type: "DELETE"
                    })
                    .done(function(r) {
                        if ($contact.length > 0) {
                            $contact.removeAttr("taskId");
                        }
                    })
                    .fail(function(r) {
                        console.error('Unable to delete celery task 1: ', r);
                    });
                }
            }
        } // deleteTask

        // walk the list of status updates
        var numComplete = 0;
        if (data.length === 0) {
            return;
        }

        for (var i = 0; i < data.length; i++) {
            var $contact = $();
            var task_type = data[i].task_type.toUpperCase();

            // Non-import tasks
            if (['NS', 'DC', 'CE', 'GG', 'RE', 'GC', 'PE', 'GL', 'CM'].indexOf(task_type) > -1) {
                switch (data[i].status.toLowerCase()) {
                    case 'queued':
                    case 'started':
                        break;
                    case 'completed':
                        // increase the number complete
                        numComplete++;

                        // delete task from celery queue
                        deleteTask(data[i]);

                        break;
                    case 'failed':
                        // increase the number complete
                        numComplete++;

                        // delete task from celery queue
                        deleteTask(data[i]);
                        console.error('Running analysis, task_type: ' + task_type + ' --> failed: ', data);
                        return;
                } // end switch - data[i].status
            }
            // Import tasks
            else if (['FB', 'GM', 'LI', 'TW', 'OU', 'AB', 'CV', 'IP', 'AD', 'OT', 'CC'].indexOf(task_type) > -1){
                // update status of an upload of contacts
                if (data[i].account == null && data[i].task_type !== 'CC') {
                    $contactContainer = (".contactConnectionDiv.userContacts");
                }
                else{
                    $contactContainer = (".contactConnectionDiv.accountContacts");
                }
                $('.contactContainer', $contactContainer).each(function() {
                    var $c = $(this);
                    // skip the addContact - end of list
                    if ($c.hasClass('addContact')) {
                        return;
                    }

                    // see if the contact container has a task id
                    var taskId = $c.attr('taskId');
                    if (data[i].task_type == 'CC') {
                        // if there is a clone task, we are matching the task with any divs that have the same importlabel
                        if ($c.attr('importlabel') == data[i].destination_label) {
                            $c.attr('taskId', data[i].id);
                            $contact = $c;

                            return;
                        }
                    }
                    else{
                        if ((taskId) && (taskId == data[i].id)) {
                            $contact = $c;

                            return;
                        }
                    }

                    // if not taskId then check the type and label
                    if (!taskId) {
                        // NOTE:  no account id - personal, otherwise admin
                        var iType = $c.attr('importType');
                        var iLabel = $c.attr('importLabel');
                        // convert the iType
                        switch (iType.toLowerCase()) {
                            case 'linkedin':
                                iType = 'li';
                                break;
                            case 'linkedin-oauth2':
                                iType = 'li';
                                break;
                            case 'gmail':
                                iType = 'gm';
                                break;
                            case 'google-oauth2':
                                iType = 'gm';
                                break;
                            case 'outlook':
                                iType = 'ou';
                                break;
                            case 'csv':
                            case 'cv':
                                // csv
                                iType = 'cv'
                                break;
                            case 'apply':
                                iType = 'ap'
                                break
                            case 'vcard':
                                iType = 'vc';
                                break;
                            //case 'ot':
                            //case 'ab':
                            //    break;
                            case 'iphone':
                                iType = 'ip';
                                break;
                            case 'cloned':
                            case 'CC':
                                iType = 'CC';
                                break;
                            default:
                                console.error('Unknow import type: ' + iType);
                                break
                        }

                        // below is to give a file a task id if it has not started as a running task yet
                        if ((iType == (data[i].task_type.toString()).toLowerCase()) && (iLabel == data[i].label || iLabel == data[i].destination_label)) {

                            $contact = $c;

                            $contact.attr('taskId', data[i].id);

                            return;
                        }
                        else if ((iType == 'OT') && (data[i].task_type == 'AB')) {
                            // special case for vcard
                            $contact = $c;

                            $contact.attr('taskId', data[i].id);

                            return;
                        }
                    }
                });

                // update the status
                switch (data[i].status.toLowerCase()) {
                    case 'queued' :
                        var sTmp = [];
                        sTmp.push('<div class="loadingDiv">');
                            sTmp.push('<div class="loading" nstyle="xmargin-left:5px;width:25px;height:25px"></div>');
                        sTmp.push('</div>');

                        if (data[i].task_type == "CC") {
                            let $eachClonebuck = $('.contactContainer.cloneDiv[importlabel="' + data[i].destination_label + '"]', $contactAccountContainer);
                            let $parentList = $('.contactContainer[importlabel="' + data[i].label + '"]', $contactPersonalContainer);
                            $('.deleteContactSrcBtn', $eachClonebuck).addClass('disabled');
                            $('.contactCountDiv', $eachClonebuck).hide();
                            $('.deleteContactSrcBtn', $parentList).addClass('disabled');
                            $('.cloneBtn', $contactPersonalContainer).addClass('disabled');
                            $('.contactLoadingDiv', $eachClonebuck).html(sTmp.join(''))
                                                              .show();
                        }

                        else{
                            $('.contactCountDiv', $contact).hide();
                            $('.contactLoadingDiv', $contact).html(sTmp.join(''))
                                                             .show();
                        }
                        break;
                    case 'started':
                        var sTmp = [];

                        if (data[i].task_type == "CC") {
                            let $eachClonebuck = $('.contactContainer.cloneDiv[importlabel="' + data[i].destination_label + '"]', $contactAccountContainer);
                            let $parentList = $('.contactContainer[importlabel="' + data[i].label + '"]', $contactPersonalContainer);
                            $('.contactLoadingDiv', $eachClonebuck).html("");
                            $('.deleteContactSrcBtn', $eachClonebuck).addClass('disabled');
                            $('.contactCountDiv', $eachClonebuck).hide();
                            $('.deleteContactSrcBtn', $parentList).addClass('disabled');
                            $('.cloneBtn', $contactPersonalContainer).addClass('disabled');
                        }

                        if ((!data[i].percent) || (data[i].percent === 0)) {
                            sTmp.push('<div class="loadingDiv">');
                                sTmp.push('<div class="loading" nstyle="xmargin-left:5px;width:25px;height:25px"></div>');
                            sTmp.push('</div>');
                        }
                        else {
                            var slice = Math.round((360 * (data[i].percent / 100)) / 5);
                            sTmp.push('<div class="started pifont pifont-pi-' + slice + '">');
                                sTmp.push('<div class="startedInner icon icon-arrow-upload"></div>');
                            sTmp.push('</div>');
                        }

                        if (data[i].account == null){
                            // go to top lane
                        }

                        else{
                            // go to bottom
                        }
                        if (data[i].task_type == "CC") {
                            let $eachClonebuck = $('.contactContainer.cloneDiv[importlabel="' + data[i].destination_label + '"]', $contactAccountContainer);
                            $('.contactCountDiv', $eachClonebuck).hide();
                            $('.contactLoadingDiv', $eachClonebuck).html(sTmp.join(''))
                                                         .show();
                        }
                        else{
                            $('.contactCountDiv', $contact).hide();
                            $('.contactLoadingDiv', $contact).html(sTmp.join(''))
                                                         .show();
                        }
                        break;
                    case 'completed':
                        // increase the number complete
                        numComplete++;
                        if (data[i].task_type !== "CC") {
                            $('.deleteContactSrcBtn', $contact).removeClass('disabled');
                            $('.contactCountDiv', $contact).show();
                            $('.contactLoadingDiv', $contact).hide();
                            $('.cloneBtn', $contactPersonalContainer).removeClass('disabled');
                        }

                        // refresh status
                        refreshContactStatus();

                        // delete task from celery queued
                        deleteTask(data[i]);

                        // remove the task id attribute
                        $contact.removeAttr('taskId');
                        let $multipleClones = $('[importType="' + $contact.attr('importtype') + '"][importLabel="' + $contact.attr('importlabel') + '"]');
                        $multipleClones.removeAttr('taskId');

                        break;
                    case 'failed':
                        // increase the number complete
                        let $eachClonebuck = $('.contactContainer.cloneDiv[importlabel="' + data[i].destination_label + '"]', $contactAccountContainer);
                        numComplete++;

                        var sTmp = [];
                        sTmp.push('<div class="failed">');
                            sTmp.push('<div class="icon icon-warning"></div>');
                            if (data[i].task_type == 'CC') {
                                sTmp.push('<div class="txt">Share Failed</div>');
                            }
                            else{
                                sTmp.push('<div class="txt">Upload Failed</div>');
                            }
                        sTmp.push('</div>');

                        $('.deleteContactSrcBtn', $contact).removeClass('disabled');
                        $('.contactCountDiv', $contact).hide();

                        if (data[i].task_type == 'CC') {
                            $('.contactLoadingDiv', $eachClonebuck).html(sTmp.join(''))
                                                             .show();
                        }
                        else{
                            $('.contactLoadingDiv', $contact).html(sTmp.join(''))
                                                             .show();
                            }

                        // delete task from celary queued
                        deleteTask(data[i]);
                        break;
                } // end switch - status

                // new source
                if ($contact.length === 0) {
                    var contactData = {};
                    contactData.taskId = data[i].id;
                    if (data[i].task_type == 'CC') {
                        contactData.label = data[i].destination_label;
                    }
                    else{
                        contactData.label = data[i].label;
                    }
                    contactData.import_type = data[i].task_type;
                    contactData.percent = data[i].percent;
                    contactData.status = data[i].status;
                    if (data[i].account != null ){
                        contactData.account = data[i].account;
                    }
                    contactData.addContact = true;
                    if (contactData.account == null && data[i].task_type !== 'CC') {
                        addContactSource(contactData, 'personal');
                    }
                    else{
                        addContactSource(contactData, 'account');
                    }
                }
            }
        }
    } //updateStatus

    function addContactSource(contactSrcData, destSection)
    {
        var sTmp = [];
        let userName = $('.contactAccountSection').attr('username');

        // build the container
        var attr = [];
        if (contactSrcData.addContact) {
            attr.push('taskId="' + contactSrcData.taskId + '"');
        }
        else {
            attr.push('importId="' + contactSrcData.id + '"');
        }

        let srcType = '';
        let srcLabel = '';
        if (contactSrcData.source) {
            srcType = contactSrcData.source;
            srcLabel =  contactSrcData.title;
        }
        else {
            srcType = contactSrcData.import_type;
            srcLabel = contactSrcData.label;
        }
        attr.push('importType="' + srcType.toLowerCase() + '"');
        attr.push('importLabel="' + srcLabel + '"');
        attr = " " + attr.join(' ');

        if (contactSrcData.import_type === 'CC' || contactSrcData.source == 'Cloned') {
            sTmp.push('<div class="contactContainer cloneDiv"' + attr + '>');
        }
        else{
            sTmp.push('<div class="contactContainer"' + attr + '>');
        }

        sTmp.push('<div class="topPart">');
            sTmp.push('<div class="nameDiv">');
                sTmp.push('<div class="leftSide">');

                if (contactSrcData.source == 'Cloned') {
                    sTmp.push(contactSrcData.title);
                }
                else if (contactSrcData.import_type == "CC") {
                    sTmp.push(contactSrcData.label);
                }
                else {
                    sTmp.push(humanReadableType(srcType));
                    sTmp.push(' - ');
                    sTmp.push(srcLabel);
                }
                sTmp.push('</div>');

                sTmp.push('<div class="rightSide">');
                if (templateData.hasAdminAccess == false) {
                    if (contactSrcData.import_type == 'CC' || contactSrcData.source == 'Cloned') {
                        $('.contactAccountSection .contactInfoDiv').html('<div class="title">Your Shared Contacts</div>');
                    }
                    else{
                        sTmp.push('<div class="deleteContactSrcBtn icon icon-close"></div>');
                    }
                }
                else if (templateData.hasAdminAccess == true) {
                    sTmp.push('<div class="deleteContactSrcBtn icon icon-close"></div>');
                }
                sTmp.push('</div>');
            sTmp.push('</div>');

            sTmp.push('<div class="leftSide">');
                var style = "";
                sTmp.push('<div class="middleStuff">');
                    sTmp.push('<div class="imgDiv">');
                        sTmp.push('<div class="' + importSourceIcon(srcType) + '"></div>');
                    sTmp.push('</div>');
                    sTmp.push('<div class="textDiv"' + style +'>');

                    if (contactSrcData.addContact) {
                        var countStyle = ' style="display:none"';
                        var loadingStyle = '';
                        var loadingTmp = [];
                        if(contactSrcData.status){
                            switch (contactSrcData.status.toLowerCase()) {
                                case 'queued' :
                                    loadingTmp.push('<div class="started pifont pifont-pi-0">');
                                        loadingTmp.push('<div class="startedInner icon icon-arrow-upload"></div>');
                                    loadingTmp.push('</div>');
                                    break;
                                case 'started':
                                    var slice = Math.round((360 * (contactSrcData.percent / 100)) / 5);
                                    loadingTmp.push('<div class="started pifont pifont-pi-' + slice + '">');
                                        loadingTmp.push('<div class="startedInner icon icon-arrow-upload"></div>');
                                    loadingTmp.push('</div>');
                                    break;
                                case 'completed':
                                    countStyle = "";
                                    loadingStyle = ' style="display:none"';
                                    break;
                                case 'failed':
                                    loadingTmp.push('<div class="failed icon icon-arrow-upload"></div>');
                                    break;
                            } // end switch - status
                        }
                    }

                    if (contactSrcData.import_type === 'CC') {
                        sTmp.push('<div class="contactCountDiv" style="display: none;">');
                            var total = "";
                            sTmp.push('<div class="numContacts" style="display: none;">' + total + '</div>');
                            sTmp.push('<div class="text" style="display: none;">Contacts</div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="contactLoadingDiv" style="">');
                            sTmp.push('<div class="loadingDiv">');
                                sTmp.push('<div class="loading" nstyle="xmargin-left:5px;width:25px;height:25px"></div>');
                            sTmp.push('</div>');
                        sTmp.push('</div>');
                    }

                    else {
                        sTmp.push('<div class="contactCountDiv"' + loadingStyle + '>');
                            var total = "N/A";
                            var textStyle = ' style="display:none;"';
                            if (contactSrcData.count && contactSrcData.count > 0) {
                                total = revupUtils.commify(contactSrcData.count);
                                textStyle = '';
                            }
                            sTmp.push('<div class="numContacts">' + total + '</div>');
                            sTmp.push('<div class="text">Contacts</div>');
                        sTmp.push('</div>');
                        sTmp.push('<div class="contactLoadingDiv"></div>');
                    }

                    sTmp.push('</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');

            sTmp.push('<div class="rightSide">');
                sTmp.push('<div class="btnCntl settingsBtn">');
                    sTmp.push('<div class="txt">Settings</div>');
                    sTmp.push('<div class="icon icon-admin"></div>');
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        sTmp.push('<div class="bottomPart">');
        if (contactSrcData.import_type == 'CC' || contactSrcData.source == 'Cloned') {
            sTmp.push('<div class="syncLabel">Last cloned:&nbsp</div>');
        }
        else{
            sTmp.push('<div class="syncLabel">Last sync\'d:&nbsp</div>');
        }
            sTmp.push('<div class="syncDate">');
                var date = 'N/A';
                if (contactSrcData.source == 'Cloned') {
                    date = revupUtils.formatDateText(contactSrcData.modified);
                }
                else if (contactSrcData.import_record && contactSrcData.import_record.import_dt) {
                    date = revupUtils.formatDateText(contactSrcData.import_record.import_dt);
                }

                sTmp.push(date);
            sTmp.push('</div>');
        sTmp.push('</div>');

        if (destSection == 'user' || destSection == 'personal') {
            sTmp.push('<button class="cloneBtn">Share This List</button>');
        }
            sTmp.push('</div>');
        sTmp.push('</div>');

        // display
        //two sections in dom - contactAccountSection and contactUserSection
        var exists = null;
        if (destSection == 'account') {
            $('.contactAccountSection').show();
            $('.contactContainer.addContact', $contactAccountContainer).before(sTmp.join(''));
        }
        else {
            // personal contacts
            $('.contactUserSection').show();
            $('.contactContainer.addContact', $contactPersonalContainer).before(sTmp.join(''));
        }
    } // addContactSource

    // pull the status
    function getSyncStatus(bFromInitLoad)
    {
        fetchSyncStatus();
    } // getSyncStatus

    function fetchSyncStatus()
    {
        var url = templateData.getTasksLst;
        $.ajax({
            url: url,
            type: "GET"
        }).done(function(r) {
            updateStatus(r.results);

            // update the new header
            displayRevupAlert(r, true);
        }).fail(function(r) {
            console.error('Unable to getTasksList - FetchSyncStatus: ', r);
        }).always(function() {
            // schedule the new sync - always short as the async nature of some of the uploads
            var syncTime = getStatusTimeShort;

            // check again
            if (taskFetchTimer != null) {
                clearTimeout(taskFetchTimer);
            }
            taskFetchTimer = setTimeout(fetchSyncStatus, syncTime);
        });
    } // fetchSyncStatus

    function loadExistingContactSources()
    {
        $.ajax({
            url: templateData.getImportRecordsLst2,
            type: "GET",
        })
        .done(function(r) {
            // display the contacts limits
            //displayContactLimits(r);
            if (features.PERSONAL_CONTACTS) {
                if (r.user && r.user.results) {
                    for (var i = 0; i < r.user.results.length; i++) {
                        addContactSource(r.user.results[i], 'user');
                    }
                }
            }

            if (features.ACCOUNT_CONTACTS) {
                if (r.account && r.account.results) {
                    for (var i = 0; i < r.account.results.length; i++) {
                        addContactSource(r.account.results[i], 'account');
                    }
                }
            }

            if (r.account.total_analyzed == 0 && r.user.total_analyzed == 0) {
                // display the add contacts dialog
                contactSource.load('parameter', templateData);
            }

            // hide the waiting spinner
            $('.loadingConatactsDiv').hide();

            // display the contacts limits
            displayContactLimits(r);

            // start fetching status
            getSyncStatus();
        })
        .fail(function(r) {
            console.error('Error Loading existing contact sources: ', r);
            revupUtils.apiError('Contact Manager', r, 'Loading Existing Contact Sources');
        });
    } // loadExistingContactSources

    function importSourceIcon(iconString){
        switch (iconString.toLowerCase()){
            case 'li':
            case 'linkedin':
            case 'linkedin-oauth2':
                // linkedIn
                // icon
                return 'linkedInGlyph';
                break;
            case 'gm':
            case 'gmail':
            case 'google-oauth2':
                // google - gmail
                return 'gmailGlyph';
                break;
            case 'ou':
            case 'outlook':
                // cloudsponge outlook
                return 'outlookGlyph';
                break;
            case 'cv':
            case 'csv':
                // csv
                return 'csvGlyph';
                // style = ' style="margin-top:2px;"';
                break;
            case 'ap':
            case 'apply':
            case 'vc':
            case 'vcard':
            case 'ot':
            case 'ab':
                return 'appleGlyph';
                // style = ' style="top:-2px"';
                break;
            case 'ip':
            case 'iphone':
            case 'mb':
            case 'mobile':
                return 'iPhoneGlyph';
                break;
            case 'cloned':
            case 'cc':
                return 'cloneGlyph';
                break;
            default:
                return 'unknownGlyph';
                // sTmp.push('<div style="top:2px;" class="imgDiv">');
                break;
        }

    } // importSourceIcon

    return {
        fetchSyncStatus: function (r)
        {
            getSyncStatus();
        }, // fetchSyncStatus

        load: function (pTemplateData)
        {
            templateData = pTemplateData;

            if (templateData.displayAddContacts) {
                // display the add contacts dialog
                contactSource.load('parameter', templateData);
            }
            else {
                onBoarding = false;
            }

            // load the csv config file
            contactCSV = csvValidation;
            contactCSV.loadConfigFile(templateData.csvConfigFile);

            // load the event handlers
            loadEventHanders();

            // load exiting contact list and update there status
            bEnterPage = true;
            loadExistingContactSources();
        } // load
    };
} ());
