(function (jQuery, window, undefined) {
    /*
     * Wizard Methods - can be called after the wizard is created
     *  1) close - close the wizard and clear the overlay
     *  2) enableNext - enable the next page button if disable
     *  3) finishSave - if the save method for the page is display an wait spinner close it
     *                  and enable the next/done button
     *  4) finishGetData - if the get data for the page is display a wait spinner close it
     *                     and enable the previous and next buttons
     *
     *  Methods for each of the page
     *      pageName - return the name of the page (Must Have)
     *      display - return the html for the page. (Must Have)
     *                Return Formats
     *                  1) the html for the page
     *                  2) object with the following fields
     *                      a) html - html for the page (Must Have)
     *                      b) htmlClass - extra call for the page (optional)
     *                      c) disableNext - should the next button be disabled (optional)
     *      getData - once the page is displayed go fetch data for the page (optional, default - don't do anything)
     *                Return Formats
     *                  1) true/false enable prev/next buttons
     *                  2) object with the following fields
     *                      a) showWaitSpinner - show the wait spinner, the prev/next buttons will be disabled
     *      footerMsg - return the markup to display in the message area in the button area, left side  (optional, default - '')
     *      okToFinish - see if the next button should be enabled or not (optional, default - true)
     *      okToMove - see if ok to move to the next page, verify data on page (option, default - true)
     *                  Return Formats
     *                    1) true/false about advance to the new pages
     *                    2) object with the following fields
     *                          a) okToGo - true/false go to the next page (must have)
     *                          b) historyShowError - should the history dot be done; green or blue or red in error (optonal - ok dot)
     *      pageHasBeenSaved - has the data on the page been saved (optional - false)
     *      save - time for the page to save the data on the page (optional - default do nothing)
     *                  Return Formats
     *                     1) true/false save done and can go to the next page (defult - true going to next page)
     *                     2) object with the following fields
     *                           a) updateAndLoad - true/false if can go to the next page (optional - if missing of to go to next page)
     *                           b) showWaitSpinner - tre/false should the wait spinner be displayed, if true the next and prev buttons are disabled (default, do not display wait spinner)
     *
     *
     */

    jQuery.fn.revupWizard = function(options, arg1, arg2)
    {
        let opts            = {};
        let wizWidth        = 1320;
        let wizHeight       = 650;
        let autoMaxWidth    = 1620;
        let autoMaxHeight   = 1190;
        let $wizard         = $();

        let wizardBodyPadding = 10;
        let wizardHistoryHeight = 30;
        let wizardButtonHeight  = 60;

        // make sure there is a dom object
        if ($(this).length < 1) {
            return;
        }

        let $this = $(this);

        /*
         *
         * Helper functions
         *
         */
        let loadPage = (pageNum, $wiz = $wizard, options = opts) => {
            let disableNext = false;
            let disableNextPrev = false;

            let opts = options;

            if (typeof opts.pages[pageNum].display == 'function') {
                // load and display the page
                let displayData = opts.pages[pageNum].display($('.wizardBody', $wiz));
                let html = '';
                let htmlClass = 'wizardBody';
                if (typeof displayData == 'object') {
                    html = displayData.html;

                    if (displayData.htmlClass) {
                        htmlClass += ' ' + displayData.htmlClass;
                    }

                    if (displayData.disableNext) {
                        disableNext = displayData.disableNext;
                    }
                }
                else {
                    html = displayData;
                }

                // update the wizard
                $('.wizardBody', $wiz).html(html)
                                      .prop('class', htmlClass);

                // mark the history as loaded
                if (opts.bShowHistory) {
                    $('.wizardHistoryDiv .historyDotsDiv .historyDot[pageNum="' + pageNum + '"]', $wiz).removeClass('ok')
                                                                                                       .removeClass('error')
                                                                                                       .addClass('onThisPage')
                    if (opts.enableDotNavigation) {
                        $('.wizardHistoryDiv .historyDotsDiv .historyDot[pageNum="' + pageNum + '"]', $wiz).removeClass('enableDotNav');
                    }
                }

                // see if there is a need to load page data
                if (typeof opts.pages[pageNum].getData == 'function') {
                    let getPageData = opts.pages[pageNum].getData($('.wizardBody', $wiz));
                    if (getPageData && getPageData.showWaitSpinner) {
                        // display the wait spinner
                        $('.wizardBodyWait', $wiz).show();

                        // disable the the next/prev buttons
                        $('.wizardBtnDiv .wizardBtn', $wiz).prop('disabled', true);

                        // disable the next and prev buttons
                        disableNextPrev = true;
                        disableNext = true;

                        // save the disableNext flag
                        $wiz.data('disableNext', disableNext);
                    }
                    else {
                        disableNextPrev = false;
                    }

                    if (getPageData && getPageData.enableNext) {
                        disableNext = !getPageData.enableNext
                    }

                }

                // see if the next button should be disabled
                if (disableNextPrev  && !opts.bNoPrevButton) {
                    $('.wizardBtnDiv .prevBtn', $wiz).prop('disabled', true);
                }
                $('.wizardBtnDiv .nextBtn', $wiz).prop('disabled', disableNext);
            }
            else {
                // load and display the page
                let sTmp = [];
                sTmp.push('<div class="errorDiv" style="width:100%;height:' + ($('.wizardBodyOuter', $wiz).height() - 20) + 'px">');
                    sTmp.push('<div class="msg">Page ' + pageNum + ' is missing a display method</div>');
                sTmp.push('</div>')
                $('.wizardBody', $wiz).html(sTmp.join(''))
                                         .prop('class', 'wizardBody');

                // mark the history as loaded
                if (opts.bShowHistory) {
                    $('.wizardHistoryDiv .historyDotsDiv .historyDot[pageNum="' + pageNum + '"]', $wiz).removeClass('ok')
                                                                                                       .addClass('error')
                                                                                                       .removeClass('onThisPage')
                }

                // make sure the next button is enabled
                $('.wizardBtnDiv .nextBtn').prop('disabled', false);
            }

            // display the footer message
            let footerMsg = '';
            if (typeof opts.pages[pageNum].footerMsg == 'function') {
                footerMsg = opts.pages[pageNum].footerMsg();
            }
            $('.wizardBtnDiv .leftSide .msgDiv').html(footerMsg);

            // update attributes
            $('.revupWizard', $wiz).attr('currentPage', pageNum);

            // make sure the prev button is enabled
            if (!opts.bNoPrevButton) {
                $('.wizardBtnDiv .prevBtn', $wiz).prop('disabled', false);
            }

            // update the button
            const numPages = parseInt($('.revupWizard', $wiz).attr('numPages'), 10);

            const resetDoneBtn = $('.wizardBtnDiv .nextBtn', $wiz).show()
                                             .removeClass('doneBtn')
                                             .html(opts.nextBtnText);

            if (pageNum == 0) {
                // the beginning of the list of pages
                if (!opts.bNoPrevButton) {
                    $('.wizardBtnDiv .prevBtn', $wiz).hide();
                }
                resetDoneBtn;
            }
            else if (pageNum == (numPages - 1)) {

                // see if ok to finish - enable/disable the done button
                bOkToFinish = true;
                for (let i = 0; i < opts.pages.length; i++ ) {
                    if (typeof opts.pages[i].okToFinish == 'function') {
                        if (!opts.pages[i].okToFinish('$.wizardBody', $wizard)) {
                            bOkToFinish = false;

                            break;
                        }
                    }
                }

                // end of the list of pages
                if (!opts.bNoPrevButton) {
                    $('.wizardBtnDiv .prevBtn', $wiz).show();
                }
                $('.wizardBtnDiv .nextBtn', $wiz).html(opts.doneBtnText)
                                                 .prop('disabled', !bOkToFinish)
                                                 .addClass('doneBtn');
            }
            else {
                if (!opts.bNoPrevButton) {
                    $('.wizardBtnDiv .prevBtn', $wiz).show();
                }
                resetDoneBtn;
            }

            // see if the next button should be disabled
            if (disableNextPrev) {
                if (!opts.bNoPrevButton) {
                    $('.wizardBtnDiv .prevBtn', $wiz).prop('disabled', true);
                }
                $('.wizardBtnDiv .nextBtn', $wiz).prop('disabled', true);
            }
            else {
                $('.wizardBtnDiv .nextBtn', $wiz).prop('disabled', disableNext);
            }
        } // loadPage

        let okAndSave = (page, nextPage) => {
            // see if ok
            let historyDotClass = '';
            if ((typeof opts.pages[page].pageHasBeenSaved == 'function') && (opts.pages[page].pageHasBeenSaved($('.wizardBody', $wiz)))) {
                historyDotClass = 'ok';
            }

            if (typeof opts.pages[page].okToMove == 'function') {
                let isOk = true;
                let okToMoveData = opts.pages[page].okToMove($('.wizardBody', $wizard));
                if (typeof okToMoveData == 'object') {
                    isOk = okToMoveData.okToGo;
                    if (okToMoveData.historyShowError) {
                        historyDotClass = 'error';
                    }
                }
                else {
                    isOk = okToMoveData;
                }

                // see if we can continue
                if (!isOk) {
                    return;
                }
            }

            // save the data from the page.
            let bOkAndSave = true;
            if (typeof opts.pages[page].save == 'function') {
                let sData = opts.pages[page].save($('.wizardBody', $wizard));
                if (typeof sData == 'object') {
                    historyDotClass = 'ok';

                    if (sData.updateAndLoad) {
                        //should the next button be enable
                        bOkAndSave = sData.updateAndLoad;
                    }

                    if (sData.showWaitSpinner) {
                        // display the wait spinner
                        $('.wizardBodyWait', $wizard).show();

                        // disable the the next/prev buttons
                        $('.wizardBtnDiv .wizardBtn').prop('disabled', true);

                        // do not go on to the next page
                        bOkAndSave = false;

                        // save the next page
                        $wizard.data('nextPage', nextPage)
                    }

                    if (sData.error) {
                        // change the history dot to error
                        historyDotClass = 'error';
                    }
                }
                else if (sData != undefined) {
                    // there is a return value
                    bOkAndSave = sData;
                    if (bOkAndSave) {
                        historyDotClass = 'ok';
                    }
                }
                else {
                    historyDotClass = 'ok';
                }
            }
            else {
                historyDotClass= 'ok';
            }

            // update the history button
            if (opts.bShowHistory) {
                if (historyDotClass == 'ok') {
                    $('.wizardHistoryDiv .historyDotsDiv .historyDot[pageNum="' + page + '"]', $wiz).addClass('ok')
                                                                                                    .removeClass('error')
                                                                                                    .removeClass('onThisPage');

                    if (opts.enableDotNavigation) {
                        $('.wizardHistoryDiv .historyDotsDiv .historyDot[pageNum="' + page + '"]', $wizard).addClass('enableDotNav')
                    }

                    // handle the back/prev button
                    if (nextPage < page) {
                        $('.wizardHistoryDiv .historyDotsDiv .historyDot[pageNum="' + nextPage + '"]', $wiz).addClass('ok')
                                                                                                            .removeClass('onThisPage');
                    }
                }
            }

            return bOkAndSave
        } // okAndSave

        let updateIndexing = (nextPage, page) => {
            const wizardProgress = opts.getWizardProgress();

            if (wizardProgress) {
                const overallProgress = wizardProgress.overallProgress;

                if (overallProgress === 'complete' || overallProgress > page) {
                    opts.saveWizardProgress(nextPage, overallProgress);
                }
                else {
                    opts.saveWizardProgress(nextPage, page);
                }
            }
            else {
                opts.saveWizardProgress(nextPage, page);
            }
        }

        // see if an command
        if (typeof options == 'string') {
            var command = options;
            var $parent = jQuery(this).parent();
            var $wiz = $(this);//jQuery('.revupWizard', $parent);

            switch (command) {
                case 'close':
                    console.error('revupWizard - No longer supported');

                    break;
                case 'enableNext':
                    bEnable = true;
                    if (arguments[1] != undefined) {
                        bEnable = arguments[1];
                    }

                    $('.wizardBtnDiv .nextBtn').prop('disabled', !bEnable);
                    break;
                case 'enablePrev':
                    bEnable = true;
                    if (arguments[1] != undefined) {
                        bEnable = arguments[1];
                    }

                    if (!opts.bNoPrevButton) {
                        $('.wizardBtnDiv .prevBtn').prop('disabled', !bEnable);
                    }
                    break;
                case 'enableMiddle':
                    bEnable = true;
                    if (arguments[1] != undefined) {
                        bEnable = arguments[1];
                    }

                    $('.wizardBtnDiv .middleBtn').prop('disabled', !bEnable);
                    break;
                case 'changeBtnLabel':
                    let btnClass = arguments[1];
                    let btnLabel = arguments[2];

                    // get the opts
                    opts = $wiz.data('opts');

                    if (btnClass == 'prev' && !opts.bNoPrevButton) {
                        btnClass = 'prevBtn';
                        opts.prevBtnText = btnLabel;
                    }
                    else if (btnClass == 'next') {
                        btnClass = 'nextBtn';
                        opts.nextBtnText = btnLabel;
                    }
                    else if (btnClass == 'middleBtn') {
                        btnClass = 'middleBtn';
                        opts.middleBtnText = btnLabel;
                    }
                    else {
                        console.error('revupWizard->changeBtnLabel, unknown button')
                        return;
                    }

                    $('.revupWizard .wizardBtnDiv .' + btnClass, $wiz).html(btnLabel);
                    break;
                case 'finishSave':
                    // get the opts
                    opts = $wiz.data('opts');

                    // get the current page and where next/prev page
                    let currentPage = parseInt($('.revupWizard', $wiz).attr('currentPage'), 10);
                    let numPages = parseInt($('.revupWizard', $wiz).attr('numPages'), 10);
                    let nextPage = $wiz.data('nextPage')

                    // clear the waiting spinner
                    $('.wizardBodyWait', $wiz).hide();

                    // Tell wizard that current page has been completed
                    if (opts.enableIndexing) {
                        updateIndexing(nextPage, currentPage);
                    }

                    // go to the next page
                    loadPage(nextPage, $wiz);

                    break;
                case 'clearWait':
                    // clear the waiting spinner
                    $('.wizardBodyWait', $wiz).hide();

                    break;
                case 'displayMiddleBtn':
                    if (arguments[1] == undefined || arguments[1]) {
                        $('.revupWizard .wizardBtnDiv .middleBtn', $wiz).show();
                    }
                    else {
                        $('.revupWizard .wizardBtnDiv .middleBtn', $wiz).hide();
                    }

                    break;
                case 'finishGetData':
                    // clear the waiting spinner
                    $('.wizardBodyWait', $wiz).hide();

                    // enable the prev button
                    if (!opts.bNoPrevButton) {
                        $('.wizardBtnDiv .prevBtn', $wiz).prop('disabled', false);
                    }

                    // see if the next button is enabled or disabled
                    let disableNext = $wiz.data('disableNext');
                    if (typeof disableNext == 'string') {
                        disableNext = disableNext == 'true';
                    }
                    $('.wizardBtnDiv .nextBtn', $wiz).prop('disabled', disableNext);

                    break;
                case 'gotoPage':
                    // get the opts
                    opts = $wiz.data('opts');
                    let page = arguments[1].toLowerCase();
                    let markLoaded = arguments[2] == undefined ? false : arguments[2];

                    // find the page
                    bFound = false;
                    for (let i = 0; i < opts.pages.length; i++) {
                        let wPage = opts.pages[i].pageName();
                        wPage = wPage.toLowerCase();
                        if (wPage == page) {
                            loadPage(i, $wiz, opts);
                            bFound = true;

                            break;
                        }

                        if (opts.bShowHistory) {
                            if ((typeof opts.pages[i].pageHasBeenSaved == 'function') && (opts.pages[i].pageHasBeenSaved($('.wizardBody', $wiz)))) {
                                $('.wizardHistoryDiv .historyDotsDiv .historyDot[pageNum="' + i + '"]', $wiz).addClass('ok')
                                                                                                             .removeClass('error')
                                                                                                             .removeClass('onThisPage')

                                if (opts.enableDotNavigation) {
                                    $('.wizardHistoryDiv .historyDotsDiv .historyDot[pageNum="' + i + '"]', $wizard).addClass('enableDotNav')
                                }
                            }
                            else {
                                $('.wizardHistoryDiv .historyDotsDiv .historyDot[pageNum="' + i + '"]', $wiz).removeClass('ok')
                                                                                                             .removeClass('error')
                                                                                                             .removeClass('onThisPage');
                            }
                        }
                    }

                    if (!bFound) {
                        console.error('revupWizard - gotoPage --> could not find page: ' + arguments[1].toLowerCase())
                    }

                    break;
                default:
                    console.error("revupWizard - invalid command: " + command);
                    break;
            } // end switch - command

            return this;
        }

        // Extend our default options with those provided.
        opts = $.extend( {}, $.fn.revupWizard.defaults, options );

        // make sure each page has a pageName and display method
        let bPagesError = false;
        if (opts.pages.length == 0) {
            console.warn('Wizard has no pages defined');
            bPagesError = true;
        }

        for (let i = 0; i < opts.pages.length; i++) {
            if (typeof opts.pages[i].pageName != 'function') {
                console.warn('Page ' + i + ' does not a pageName method');
                // bPagesError = true;
            }
            else if (opts.pages[i].pageName() == '') {
                console.warn('Page ' + i + ' pageName is blank');
                // bPagesError = true;
            }

            if (typeof opts.pages[i].display != 'function') {
                console.warn('Page ' + i + ' has no display method, which is required');
                // bPagesError = true;
            }
        }

        if (bPagesError) {
            return;
        }

        // container to build in
        let sTmp = [];

        // get the parent
        let $root = jQuery(this);
        let rootClass = $root.prop('class');

        // build the extra styling
        let extraStyle = '';
        if (opts.width != '' && !opts.bAutoWidth) {
            extraStyle += "width:" + opts.width + 'px;';

            wizWidth = opts.width;
        }
        else if (opts.bAutoWidth) {
            let winWidth = $this.width();
            if (opts.autoMaxWidth != '') {
                autoMaxWidth = opts.autoMaxWidth;
            }

            winWidth = Math.min(winWidth, autoMaxWidth);
            extraStyle += 'width:' + winWidth + 'px;';

            wizWidth = winWidth;
        }

        if (opts.height != '' && !opts.bAutoHeight) {
            extraStyle += "height:" + opts.height + 'px;';

            wizHeight = opts.height;
        }
        else if (opts.bAutoHeight) {
            let winHeight = $this.height();
            if (opts.autoMaxHeight != '') {
                autoMaxHeight = opts.autoMaxHeight;
            }

            winHeight = Math.min(winHeight, autoMaxHeight);
            extraStyle += 'height:' + winHeight + 'px;';

            wizHeight = winHeight;
        }

        if (opts.zIndex != '') {
            extraStyle += "z-index:" + opts.zIndex + ';';
        }

        // get the extra class
        let extraClass = ''
        if (opts.extraClass != '') {
            extraClass += ' ' + opts.extraClass;
        }

        // center the Wizard
        if (opts.bCenterHorizontal) {
            let winWidth = $this.width();
            let left = (winWidth - wizWidth) / 2;
            if (left > 0) {
                extraStyle += 'left:' + left + 'px;';
            }
        }

        let top = '';
        if (opts.top != '') {
            top = opts.top;
        }
        else if (opts.bCenterVertical) {
            let winHeight = $(document).height(); // $this.height();
            top = (winHeight - wizHeight) / 2;
        }
        if (top != '') {
            extraStyle += 'top:' + top + 'px;position:absolute;';
        }

        // create the extra style
        if (extraStyle != '') {
            extraStyle = 'style="' + extraStyle + '"';
        }

        // compute the height of the wizard body;
        let wizardPartsHeight = wizardButtonHeight;
        if (opts.bDisplayTitle) {
            wizardPartsHeight += opts.titleHeight
        }
        if (opts.bShowHistory) {
            wizardPartsHeight += wizardHistoryHeight
        }

        // page data
        let startingPage = opts.startingPage - 1;
        let numPages = opts.pages.length;

        // build the wizard frame
        sTmp.push('<div class="revupWizard' + extraClass + '"' + extraStyle + '" numPages="' + numPages + '" currentPage="' + startingPage + '">');
            // Title
            if (opts.bDisplayTitle) {
                sTmp.push('<div class="wizardHeaderDiv" style="height:' + opts.titleHeight + 'px">');
                    sTmp.push('<div class="wizardHeader">');
                        sTmp.push(opts.title);
                    sTmp.push('</div>');
                sTmp.push('</div>');
            }

            // Body
            let extraBodyStyle = '';
            if (!opts.bDisplayTitle) {
                extraBodyStyle += 'padding-top:' + wizardBodyPadding + 'px;';
            }
            if (opts.zIndex != '') {
                extraBodyStyle += 'z-index:' + opts.zIndex + ';';
            }
            let loadingTop = 0;
            if (opts.bDisplayTitle) {
                loadingTop += opts.titleHeight;
            }
            sTmp.push('<div class="wizardBodyOuter" style="height:calc(100% - ' + wizardPartsHeight + 'px);' + extraBodyStyle + '">');
                let zIndex = parseInt(opts.zIndex, 10) + 1;
                sTmp.push('<div class="wizardBodyWait" style="z-index:' + zIndex + ';top:' + loadingTop + 'px;height:calc(100% - ' + wizardPartsHeight + 'px);">');
                    sTmp.push('<div class="loadingDiv">');
                        sTmp.push('<div class="loading"></div>');
                        sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                    sTmp.push('</div>');
                sTmp.push('</div>');

                sTmp.push('<div class="wizardBody"></div>');
            sTmp.push('</div>');

            // History
            if (opts.bShowHistory) {
                sTmp.push('<div class="wizardHistoryDiv">');
                    sTmp.push('<div class="historyDotsDiv" style="margin-left:calc((100% - (20px * ' + numPages + ')) / 2)">');
                        // do the dots
                        for (let i = 0; i < numPages; i++) {
                            let titleAttr = '';
                            if (opts.enableDotTooltip) {
                                let tt = 'Page ' + (i + 1);
                                if (typeof opts.pages[i].pageName == 'function' && opts.pages[i].pageName() != '') {
                                    tt = opts.pages[i].pageName();
                                }

                                titleAttr = ' title="' + tt + '"'
                            }

                            let extraClass = '';

                            if (opts.enableIndexing) {
                                // If the wizard's progress is tracked, retrieve it
                                // and use to determine which pages are 'ok' and accessible
                                // through dot navigation.
                                const wizardProgress = opts.getWizardProgress();

                                if (wizardProgress) {
                                    const overallProgress = wizardProgress.overallProgress;

                                    if (i <= overallProgress || overallProgress === 'complete') {
                                        extraClass = ' ok';
                                        if (opts.enableDotNavigation) {
                                            extraClass += ' enableDotNav'
                                        }
                                    }
                                }
                            }
                            else {
                                // Mark all pages before starting page as 'ok'.
                                if (i < startingPage) {
                                    extraClass = ' ok';
                                    if (opts.enableDotNavigation) {
                                        extraClass += ' enableDotNav'
                                    }
                                }
                            }

                            sTmp.push('<div class="historyDot' + extraClass + '" pageNum="' + i +'"' + titleAttr + '></div>');
                        }
                    sTmp.push('</div>');
                sTmp.push('</div>');
            }

            // Buttons & footer message
            let footerMsg = '';
            let middleBtnStyle = ' style="display:none;"';
            if (opts.middleBtnDisplay) {
                middleBtnStyle = '';
            }
            sTmp.push('<div class="wizardBtnDiv">');
                sTmp.push('<div class="leftSide">');
                    sTmp.push('<div class="msgDiv">' + footerMsg + '</div>')
                sTmp.push('</div>');
                sTmp.push('<div class="rightSide">');
                    sTmp.push('<div class="cancelBtn">' + opts.cancelBtnText + '</div>');
                    if (!opts.bNoPrevButton) {
                        sTmp.push('<button class="revupBtnDefault wizardBtn prevBtn">' + opts.prevBtnText + '</button>');
                    }
                    sTmp.push('<button class="revupBtnDefault wizardBtn middleBtn"' + middleBtnStyle + '>' + opts.middleBtnText + '</button>');
                    sTmp.push('<button class="revupBtnDefault wizardBtn nextBtn">' + opts.nextBtnText + '</button>');
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        // display the modal
        $wizard = $this.html(sTmp.join(''));

        // position the waiting spinner
        $('.wizardBodyWait', $this).width($('.revupWizard', $this).width())
                                   .height($('.revupWizard', $this).height())
                                   //.css('left', $this.css('margin-left'))
                                   .css('top', 0);

        // save opts
        $this.data('opts', opts);

        // load the starting page
        loadPage(startingPage);

        // event handlers
        $this
            // history dot handlers
            .on('click', '.wizardHistoryDiv .historyDot', function() {
                // make sure dot navigation is enabled
                if (!opts.enableDotNavigation) {
                    return;
                }

                // can only go to dot that are in error or ok
                let $dot = $(this);
                if ($dot.hasClass('ok') || $dot.hasClass('error')) {
                    let currentPage = parseInt($('.revupWizard', $wizard).attr('currentPage'), 10);

                    // update the dot
                    $('.wizardHistoryDiv .historyDotsDiv .historyDot[pageNum="' + currentPage + '"]', $wiz).addClass('ok')
                                                                                                           .removeClass('error')
                                                                                                           .removeClass('onThisPage');
                    if (opts.enableDotNavigation) {
                        $('.wizardHistoryDiv .historyDotsDiv .historyDot[pageNum="' + currentPage + '"]', $wiz).addClass('enableDotNav');
                    }


                    // go to the clicked on page
                    let nextPage = parseInt($dot.attr('pageNum'), 10);

                    if (okAndSave(currentPage, nextPage)) {
                        // go to the next page
                        loadPage(nextPage);

                        if (opts.enableIndexing) {
                            updateIndexing(nextPage, currentPage);
                        }
                    }
                }
            })
            // next prev handler
            .on('click', '.wizardBtnDiv .rightSide .wizardBtn', function() {
                // see if done
                if ($(this).hasClass('doneBtn')) {
                    // see if there is a doneBtnHandler and if so call it
                    if (opts.doneBtnHandler) {

                        let r = opts.doneBtnHandler($('.wizardBody', $wizard));

                        if (opts.enableIndexing) {
                            let currentPage = parseInt($('.revupWizard', $wizard).attr('currentPage'), 10);
                            // update the dot
                            $('.wizardHistoryDiv .historyDotsDiv .historyDot[pageNum="' + currentPage + '"]', $wiz).addClass('ok')
                                                                                                                   .removeClass('error')
                                                                                                                   .removeClass('onThisPage');
                            if (opts.enableDotNavigation) {
                                $('.wizardHistoryDiv .historyDotsDiv .historyDot[pageNum="' + currentPage + '"]', $wizard).addClass('enableDotNav');
                            }

                            loadPage(0);
                        }

                        if (r) {
                            // display the wait spinner
                            $('.wizardBodyWait', $wizard).show();

                            // disable the the next/prev buttons
                            $('.wizardBtnDiv .wizardBtn', $wizard).prop('disabled', true);

                            return;
                        }
                    }

                    // see if should auto close
                    if (!opts.bAutoCloseDone) {
                        return;
                    }

                    // close the dialog box
                    $wizard.remove();

                    // cancel the esc close
                    $(document).off('keydown');

                    return;
                }

                // middle button
                if ($(this).hasClass('middleBtn')) {
                    if (opts.middleBtnHandler) {
                        opts.middleBtnHandler($('.wizardBody', $wizard));

                        return;
                    }
                }

                // get the current page and where next/prev page
                let currentPage = parseInt($('.revupWizard', $wizard).attr('currentPage'), 10);
                let numPages = parseInt($('.revupWizard', $wizard).attr('numPages'), 10);
                let nextPage = currentPage;
                if ($(this).hasClass('prevBtn')) {
                    nextPage -= 1;
                }
                else if ($(this).hasClass('nextBtn')) {
                    nextPage += 1;
                }

                // see if ok to move on
                let okToMoveOn = okAndSave(currentPage, nextPage);
                // if don't update and load
                if (okToMoveOn) {
                    // Tell wizard that current page has been completed
                    if (opts.enableIndexing) {
                        updateIndexing(nextPage, currentPage);
                    }

                    // Go to the next page
                    loadPage(nextPage);
                }
            })
            // handle the close event
            .on('click', '.wizardBtnDiv .rightSide .cancelBtn', function() {
                // see if the on cancel function
                if (opts.cancelBtnHandler) {
                    let r = opts.cancelBtnHandler($('.wizardBody', $wizard));
                    if (r) {
                        return;
                    }
                }

                // see if auto close on cancel
                if (!opts.bAutoCloseCancel) {
                    return;
                }

                // close the dialog box
                $wizard.remove();

                // cancel the esc close
                $(document).off('keydown');
            });

        if (opts.escClose) {
            $(document).on('keydown', function(e) {
                // esc key
                if (e.keyCode == 27) {
                    // see if there is a confirm dialog
                    var $confDlg = $('.revupConfirmBox');
                    if ($confDlg.length >= 1) {
                        $confDlg.trigger('revup.confirmClose');
                        return;
                    }

                    // call the handler if there is one - done this way just encase no
                    // cancel button
                    if (opts.cancelHandler != undefined) {
                        opts.cancelHandler();
                    }

                    if (opts.autoCloseCancel) {
                        // close the dialog box
                        $dlg.remove();

                        // cancel the esc close
                        $(document).off('keydown');

                        // close the overlay
                        $revupDialgBoxOverlay.trigger('revup.overlayClose');
                    }

                    // trigger the event
                    $this.trigger('revup.dialogCancel');

                    e.preventDefault();
                    e.stopPropagation();
                }
            })
        }

        return $wizard;
    }, // revupWizard


    jQuery.fn.revupWizard.defaults = {
        pages:                  [],             // array of page objects
        startingPage:           1,              // start page (1 based)
        extraClass:             "",             // extra class for the full widget
        zIndex:                 '',             // z-index of the wizard (20)
        width:                  '',             // width of the wizard (1320px)
        height:                 '',             // height of the wizard (650px)
        bAutoWidth:             false,          // auto compute the width
        bAutoHeight:            false,          // auto compute the height
        autoMaxWidth:           '',             // max width when in auto mode (1620px)
        autoMaxHeight:          '',             // max height when in auto mode (1190px)
        bCenterHorizontal:      true,           // should the wizard be horizontal centered
        bCenterVertical:        false,          // should the wizard be vertical centered
        top:                    '',             // offset from the top
        bDisplayTitle:          false,          // should the title be displayed
        title:                  '',             // the title, this could be a div/image or text
        titleHeight:            35,             // height of the title area
        escClose:               false,          // close on esc
        enableIndexing:         false,          // allow for wizard progress to be stored locally

        bNoPrevButton:          false,          // should a previous button be displayed
        bShowHistory:           true,           // display the history dots
        enableDotNavigation:    true,           // enable dot navation for ok and error dots (default true)
        enableDotTooltip:       true,           // enable tooltip/title dtag for history dots (default true)

        cancelBtnText:          'Cancel',       // label for the cancel button (default 'Cancel')
        bAutoCloseCancel:       true,           // auto close when cancel button pressed (default true)
        cancelBtnHandler:       undefined,      // external function to call when the cancel button is pressed
                                                // if return true cancel button processing stop (default undefined)
        prevBtnText:            'Back',         // label for the previous button, go back (default 'Back')
        nextBtnText:            'Next',         // label for the next button, go forward (default 'Next')
        doneBtnText:            'Done',         // label for the done button
        bAutoCloseDone:         true,           // auto close when the done button is pressed
        doneBtnHandler:         undefined,      // external function to call when the done button is pressed
                                                // if true display the wait spinner
        middleBtnDisplay:       false,          // should the middle button be displayed
        middleBtnText:          'middle',       // label for the middle button
        middleBtnHandler:       undefined,      // function to call if the middle button is pressed
    }

}(jQuery, window));
