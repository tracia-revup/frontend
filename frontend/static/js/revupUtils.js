var revupUtils = (function () {
    jQuery.browser = {};
    jQuery.browser.mozilla = /mozilla/.test(navigator.userAgent.toLowerCase()) && !/webkit/.test(navigator.userAgent.toLowerCase());
    jQuery.browser.webkit = /webkit/.test(navigator.userAgent.toLowerCase());
    jQuery.browser.opera = /opera/.test(navigator.userAgent.toLowerCase());
    jQuery.browser.msie = /msie/.test(navigator.userAgent.toLowerCase());

    var fullYear = ["", "January", "Feburary", "March", "April", "May", "June",
                    "July", "August", "September", "October", "November", "December"];

    return {
        addScript: function(file, loadedCallback)
        {
            let headID = document.getElementsByTagName("head")[0];
            let newScript = document.createElement('script');
            newScript.type = 'text/javascript';
            newScript.src = file;
            if (loadedCallback != undefined && typeof loadedCallback == 'function') {
                newScript.async = true;
                newScript.onreadystatechange = function () { // for IE
                  if (this.readyState == 'complete')
                    loadedCallback();
                };
                newScript.onload = loadedCallback; // for other browsers
            }
            let x   = document.getElementsByTagName('script')[0]
            x.parentNode.insertBefore(newScript, x);           }, // addScript

        removeScript: function(file)
        {
            var headID = document.getElementsByTagName("head")[0].children;
            for(var i in headID)
                if(headID[i].tagName == "SCRIPT")
                    if(headID[i].getAttribute('src') == file)
                        headID[i].parentNode.removeChild(headID[i]);
        }, // removeScript

        dedup: function(a, fieldCheck)
        {
            var sTmp = [];
            var cnt = 0;

            for (var i = 0; i < a.length; i++) {
                var current = a[i];

                for (var j = 0; j < sTmp.length; j++) {
                    if (fieldCheck(current) != fieldCheck(sTmp[j])) {
                        cnt++;
                    }
                }

                // if the cnt and length of the storage array are the same
                // it is not a dup so add to the list
                if (cnt == sTmp.length) {
                    sTmp.push(current);
                }

                // reset count
                cnt = 0;
            }

            // return the de-duped arrary
            return sTmp;
        }, // Array.prototype.dedup

        commify: function(val)
        {
            // commify the value
            var sVal = '' + val;

            // see if a floating point number
            var bFloat = false;
            var sValParts = sVal.split(".");
            if (sValParts.length >= 2) {
                bFloat = true;
                sVal = sValParts[0];
            }

            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(sVal)) {
                sVal = sVal.replace(rgx, '$1' + ',' + '$2');
            }

            // if floating point put back together
            if (bFloat)
                sVal += "." + sValParts[1];

            return sVal;
        }, // commify

        validDate: function(date, isYYYYMMDD)
        {
            if(date == '') {
                return false;
            }
            var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
            if (isYYYYMMDD) {
                rxDatePattern = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/; //Declare Regex
            }
            var dtArray = date.match(rxDatePattern); // is format OK?

            if (dtArray == null) {
                return false;
            }

            //Checks for mm/dd/yyyy format.
            var dtMonth = dtArray[1];
            var dtDay= dtArray[3];
            var dtYear = dtArray[5];
            if (isYYYYMMDD) {
                dtMonth = dtArray[3];
                dtDay= dtArray[4];
                dtYear = dtArray[1];
            }

            if (dtMonth < 1 || dtMonth > 12) {
                return false;
            }
            else if (dtDay < 1 || dtDay> 31) {
                return false;
            }
            else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) {
                return false;
            }
            else if (dtMonth == 2) {
                var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
                if (dtDay> 29 || (dtDay ==29 && !isleap))
                    return false;
                }

            return true;
        }, // validDate

        // from this articial http://stackoverflow.com/questions/196972/convert-string-to-title-case-with-javascript
        toProperCaseName: function(s)
        {
            return s.toLowerCase().replace( /\b((m)(a?c))?(\w)/g,
                function($1, $2, $3, $4, $5) {
                    if($2) {
                        return $3.toUpperCase()+$4+$5.toUpperCase();
                        }

                    return $1.toUpperCase();
                    }
                );
        }, // toProperCaseName

        // format a date form the backend to something that is readable
        //  backend format - YYYY-MM-DDTHH:MM:SS (ie "2013-12-27T00:00:00")
        formatDate: function(d)
        {
            var dTmp = '';

            d = d.split('T');
            if (d.length > 0) {
                d = d[0].split('-');
                if (d.length == 3) {
                    dTmp = d[1] + '/' + d[2] + '/' + d[0];
                }
                else {
                    dTmp = d;
                }
            }

            return dTmp;
        }, // formatDate

        formatDateText: function(d, bShortMonth)
        {
            var dTmp = '';

            d = d.split('T');
            if (d.length > 0) {
                d = d[0].split('-');
                if (d.length == 3) {
                    var month = parseInt(d[1], 10);
                    var day = parseInt(d[2], 10);
                    if (bShortMonth) {
                        dTmp = shortYear[month];
                    }
                    else {
                        dTmp = fullYear[month];
                    }
                    dTmp += ' ';
                    dTmp += day;
                    dTmp += ', ';
                    dTmp += d[0];
                }
                else {
                    dTmp = d;
                }
            }

            return dTmp;
        }, // formatDate

        formatTime: function(dateTime) {
            time = new Date(Date.parse(dateTime));
            var hr = time.getHours();
            var min = time.getMinutes();
            if (min < 10) {
                min = "0" + min;
            }
            var ampm = "am";
            if( hr > 12 ) {
                hr -= 12;
                ampm = "pm";
            }
            time = hr + ':' + min + ampm

            return time;
        }, // formatTime

        getBase64Image: function(img)
        {
            // Create an empty canvas element
            var canvas = document.createElement("canvas");
            canvas.width = img.width;
            canvas.height = img.height;

            // Copy the image contents to the canvas
            var ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0, img.width, img.height);

            // Get the data-URL formatted image
            // Firefox supports PNG and JPEG. You could check img.src to
            // guess the original format, but be aware the using "image/jpg"
            // will re-encode the image.
            var dataURL = canvas.toDataURL("image/png");
            return dataURL;
            // dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
        }, // getBase65Image

        getFormattedPhone: function(phone)
        {
            phone = phone.replace('tel:','');
            if ((phone[0] === '1') || (phone[0] === '+' && phone[1] === '1')){
                phone = phone.substring(3);
            }
            return phone;
        },

        apiError: function(module, results, consoleMsg, userMsg)
        {
            // getting the error line number and file is based on an article in stack overflow
            //  http://stackoverflow.com/questions/1340872/how-to-get-javascript-caller-function-line-number-how-to-get-javascript-caller
            function getErrorObject(){
                try { throw Error('') } catch(err) { return err; }
            }

            // get the error string
            var err = getErrorObject();
            var caller;

            if ($.browser.mozilla) {
                caller = err.stack.split("\n")[2];
            } else {
                caller = err.stack.split("\n")[4];
            }

            var index = caller.indexOf('.js');

            var str = caller.substr(0, index + 3);
            index = str.lastIndexOf('/');
            str = str.substr(index + 1, str.length);

            var info = " File: " + str;

            if ($.browser.mozilla) {
                str = caller;
            } else {
                index = caller.lastIndexOf(':');
                str = caller.substr(0, index);
            }
            index = str.lastIndexOf(':');
            str = str.substr(index + 1, str.length);
            info += " Line At: " + $.trim(str);

            // build the console error message
            var msg = [];
            if (module) {
                msg.push(module + ' (' + info + ')');
            }
            else {
                msg.push("Error (" + info + ")")
            }
            if (consoleMsg) {
                msg.push("Api Error-->" + consoleMsg);
            }
            else {
                msg.push('Api Error')
            }

            if (results) {
                msg.push('Error Results,')
            }
            if (results) {
                console.error(msg.join('\n'), results);
            }
            else {
                console.error(msg.join('\n'));
            }


            // build the popup message
            if (userMsg) {
                $('body').revupMessageBox({
                    headerText: "Api Error",
                    msg: userMsg + "<br><br>Please try again later.",
                    zIndex: 250,
                });
            }
        }, // apiError

        tHiSIsThElAsT: function() {}
    }
} ());

// http://stackoverflow.com/questions/831030/how-to-get-get-request-parameters-in-javascript
// URL: http://www.example.com/test.php?abc=123&def&xyz=&something%20else
// console.log(_GET);
// > Object {abc: "123", def: true, xyz: "", something else: true}
// console.log(_GET['something else']);
// > true
// console.log(_GET.abc);
// > 123
var getUrlArgs = (function() {
    var _get = {};
    var re = /[?&]([^=&]+)(=?)([^&]*)/g;
    while (m = re.exec(location.search))
        _get[decodeURIComponent(m[1])] = (m[2] == '=' ? decodeURIComponent(m[3]) : true);
    return _get;
})();
