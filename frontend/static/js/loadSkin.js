var skinData = {
    bannerImage: "",
    logoImage: "",
    candidateIconName: undefined,
    candidateIconColor: undefined,
    colors: {},
}

function loadSkin(template)
{
    function buildCss(field, bImpRule)
    {
        function buildCssProperty(cssType, bImportantRule)
        {
            var imp = bImportantRule ? ' !important' : '';

            switch (cssType.colorType) {
                case 'background':
                    return 'background:' + cssType.colorVal + imp;
                    break;
                case 'borderTab':
                case 'clearBorderTab':
                    var tmp = [];
                    tmp.push('border-top-color:' + cssType.colorVal + imp);
                    tmp.push('border-bottom-color:' + cssType.colorVal + imp);
                    tmp.push('border-left-color:' + cssType.colorVal + imp);
                    tmp.push('border-right-color:none' + imp);

                    return tmp.join(';');
                    break;
                case 'border':
                    return 'border-color:' + cssType.colorVal + imp;
                    break;
                case 'color':
                    return 'color:' + cssType.colorVal  + imp;
                    break;
                default:
                    console.error('skinning - unknown colorType: ' + cssType.colorType + ' (' + cssType.colorFor + ')');
            }

        }

        var sTmp = [];

        if (Array.isArray(field)) {
            for (var i = 0; i < field.length; i++) {
                sTmp.push(buildCssProperty(field[i], bImpRule))
            }
        }
        else {
            sTmp.push(buildCssProperty(field, bImpRule));
        }

        return sTmp.join(';');
    }

    // build the color rules
    var colors = template.cssRules;
    var sTmp = [];
    for (rule in colors) {
        var cssRule = colors[rule].cssRule;
        if (!Array.isArray(cssRule)) {
            var r = cssRule;
            cssRule = [];
            cssRule[0] = r;
        }

        // create the css rules
        for (var i = 0; i < cssRule.length; i++) {
            var cRule = cssRule[i];

            // see if the rule ends with '(important)'
            var bImportantRule = false;
            if (cRule.endsWith('(important)')) {
                bImportantRule = true;

                cRule = cRule.substring(0, cRule.length - 11);
            }

            // build the css rules
            var cssField = buildCss(colors[rule].field, bImportantRule);

            sTmp.push(cRule + " {" + cssField + "}");
        }

        // extra fields
        if (colors[rule].otherFields) {
            var extra = colors[rule].otherFields;
            for(var i = 0; i < extra.length; i++) {
                if (extra[i].cssRule) {
                    // build the css rules
                    var extraCssField = buildCss(extra[i], false);

                    var extraCssRule = extra[i].cssRule;
                    if (!Array.isArray(extraCssRule)) {
                        var r = extraCssRule;
                        extraCssRule = [];
                        extraCssRule[0] = r;
                    }

                    // create the css rules
                    for (var j = 0; j < extraCssRule.length; j++) {
                        if (extraCssRule[j] == '.mainContent .container .sideNav .tab.active') {
                            var tmp = [];
                            tmp.push('border-top-color:#e6e7e8');
                            tmp.push('border-bottom-color:#e6e7e8');
                            tmp.push('border-left-color:#e6e7e8');
                            tmp.push('border-right-color:none');

                            sTmp.push(extraCssRule[j] + " {" + tmp.join(';') + "}");
                        }
                        else {
                            sTmp.push(extraCssRule[j] + " {" + extraCssField + "}");
                        }
                    }
                }
            }
        }

        // build the color rules - stored in skin data
        var colorRules = [];
        for (var i = 0; i < colors[rule].field.length; i++) {
            var cObj = new Object();
            cObj.colorType = colors[rule].field[i].colorType;
            cObj.colorVal = colors[rule].field[i].colorVal;

            var o = {};
            o[rule]
            colorRules.push(cObj);
        }
        if (!currentSkin.colors) {
            currentSkin.colors = [];
        }
        currentSkin.colors[rule] = {};
        currentSkin.colors[rule] = colorRules;

        // load images
        //skinData.bannerImage = template.banner;
        //skinData.logoImage = template.branding_logo;

        // see if there is cadidateIcon settings
        if (template.candidateIcon) {
            if (template.candidateIcon && template.candidateIcon.candidateIconName) {
                currentSkin.candidateIconName = template.candidateIcon.candidateIconName;
            }
            else {
                currentSkin.candidateIconName = '';
            }

            if (template.candidateIcon && template.candidateIcon.candidateIconColor) {
                currentSkin.candidateIconColor = template.candidateIcon.candidateIconColor;
            }
            else {
                currentSkin.candidateIconColor = '';
            }
        }
        else {
            currentSkin.candidateIconName = '';
            currentSkin.candidateIconColor = '';
        }

        // load prospects labels
        currentSkin.prospectTitle = {};
        if (template.prospectTitle) {
            currentSkin.prospectTitle.softTitle = template.prospectTitle.softTitle;
            currentSkin.prospectTitle.hardTitle = template.prospectTitle.hardTitle;
            currentSkin.prospectTitle.inTitle = template.prospectTitle.inTitle;
        }
        else {
            currentSkin.prospectTitle.softTitle = '';
            currentSkin.prospectTitle.hardTitle = '';
            currentSkin.prospectTitle.inTitle = '';
        }
    }

    return sTmp.join(" ");
} // loadSkin
