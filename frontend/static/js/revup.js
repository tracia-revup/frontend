/*global $, site_url, document*/

/* use notFirstRun instead of isFirstRun to be backwards-compatible */
var $navbarMsg      = $('.navbarMsg');
var $navbarMsgBox   = $('.msgBox', $navbarMsg);
var $navbarMsgText  = $('.theMsg', $navbarMsg);
var $navbarTaskList = $('.taskList', $navbarMsgBox);
var $navbarMsgIcon  = $('.msgTextDiv .icon', $navbarMsg);

var currentAlertDisplayValues = {
    completed:  0,
    queued:     0,
    started:    0,
    failed:     0,

    tasks:      [],

    msg:        '',
    color:      '',
    icon:       '',
    bDisplayed:     false,
    bDisplayClose:      '',
    bDisplayMoreLess:   '',
    bJustRanAnalysis: false,
};

var getStatusTimeShort  = 5000;     // if there is status data fetch again in 5 seconds
var getStatusTimeLong   = 30000;    // if there is no status check again in 30 seconds
var updateTaskAlertTimer = null;    // timer for fetching alerts

var bUploadDone = false;
var bAnalysisDone = false;
var bTaskDone = false;
var bContactDeleteDone = false;
var importTasks = ['FB', 'GM', 'LI', 'TW', 'OU', 'AB', 'CV', 'IP', 'AD', 'OT'];

var tasksLst    = [];       // list of running tasks, used to check tasks not completed if this code

$('.btnClose', $navbarMsgBox).on('click', function() {
    // hide
    $navbarMsg.hide();

    // reset
    currentAlertDisplayValues.completed = 0;
    currentAlertDisplayValues.queued = 0;
    currentAlertDisplayValues.started = 0;
    currentAlertDisplayValues.failed = 0;

    currentAlertDisplayValues.tasks = [];

    currentAlertDisplayValues.msg = '';
    currentAlertDisplayValues.color = '';
    currentAlertDisplayValues.icon = '';
    currentAlertDisplayValues.bDisplayed = false;
    currentAlertDisplayValues.bDisplayClose = false;
    currentAlertDisplayValues.bDisplayMoreLess = false;
    currentAlertDisplayValues.bJustRanAnalysis = false;

    bUploadDone = false;
    bContactDeleteDone = false;
    bAnalysisDone = false;
    bTaskDone = false;
}); // close btn handler

$('.btnMore, .btnLess', $navbarMsgBox).off('click').on('click', function(e) {
    var $btn = $(this);

    if ($btn.hasClass('btnMore')) {
        // display the list
        $navbarTaskList.show();

        // Udate the button
        $btn.removeClass('btnMore')
            .addClass('btnLess')
            .text('less');
    }
    else {
        // display the list
        $navbarTaskList.hide();

        // Udate the button
        $btn.removeClass('btnLess')
            .addClass('btnMore')
            .text('more');
    }
}); // more less button handlers

$navbarMsgBox.on('click', '.reloadPage', function(e) {
    location.reload();
})

function updateAlertDisplay(msg, color, icon, bDisplayMoreLess, bDisplayClose)
{
    // keep warnings and error messages
    if ($navbarMsgBox.hasClass('msgYellow') && ((color == 'blue') || (color == 'green'))) {
        return;
    }
    else if ($navbarMsgBox.hasClass('msgRed') && ((color == 'bBlue') || (color == 'green') || (color == 'yellow'))) {
        return;
    }

    // see if color needs to change
    if (color != currentAlertDisplayValues.color) {
        // reset color
        $navbarMsgBox.removeClass('msgBlue msgGreen msgYellow msgRed');

        // set new color
        switch (color) {
            case 'blue':
                $navbarMsgBox.addClass('msgBlue');
                break;
            case 'green':
                $navbarMsgBox.addClass('msgGreen');
                break;
            case 'yellow':
                $navbarMsgBox.addClass('msgYellow');
                break;
            case 'red':
                $navbarMsgBox.addClass('msgRed');
                break;
        }

        // save the value
        currentAlertDisplayValues.color = color;
    }

    // see if the icon needs to change
    if (icon != currentAlertDisplayValues.icon) {
        // reset icon
        $navbarMsgIcon.removeClass('icon-info-1 icon-check icon-warning icon-stop')

        // set new icon
        switch(icon) {
            case 'info':
                $navbarMsgIcon.addClass('icon-info-1');
                break;
            case 'checked':
                $navbarMsgIcon.addClass('icon-check');
                break;
            case 'warning':
                $navbarMsgIcon.addClass('icon-warning');
                break;
            case 'stop':
                $navbarMsgIcon.addClass('icon-stop');
                break;
        }

        // save the new value
        currentAlertDisplayValues.icon = icon
    }

    // see if the message has changed
    if (msg != currentAlertDisplayValues.msg) {
        $navbarMsgText.html(msg);

        currentAlertDisplayValues.msg = msg;
    }

    // hide or show the more / less button
    if ((currentAlertDisplayValues.bDisplayMoreLess == '') || (bDisplayMoreLess != currentAlertDisplayValues.bDisplayMoreLess)) {
        if (bDisplayMoreLess) {
            $('.btnMore, .btnLess', $navbarMsgBox).show();
        }
        else {
            $('.btnMore, .btnLess', $navbarMsgBox).hide();

            // if the less button reset to more
            $('.btnLess', $navbarMsgBox).removeClass('btnLess')
                                        .addClass('btnMore')
                                        .text('more');

        }

        currentAlertDisplayValues.bDisplayMoreLess = bDisplayMoreLess;
    }

    // hide or show the close button
    if ((currentAlertDisplayValues.bDisplayClose == '') || (bDisplayClose != currentAlertDisplayValues.bDisplayClose)) {
        if (bDisplayClose) {
            $('.btnClose', $navbarMsgBox).show();
        }
        else {
            $('.btnClose', $navbarMsgBox).hide();
        }

        currentAlertDisplayValues.bDisplayClose = bDisplayClose;
    }
} // updateAlertDisplay

function updateRevupAlert(product) {

    if ((typeof doNoRunRevupAlert != 'undefined') && (doNoRunRevupAlert)) {
        return;
    }

    $.getJSON(site_url('task'), function (data) {
        // format the data
        var refreshIn = displayRevupAlert(product, data, true);

        // next update
        if (updateTaskAlertTimer != null) {
            clearTimeout(updateTaskAlertTimer)
        }
        updateTaskAlertTimer = setTimeout(function() {updateRevupAlert(product);}, refreshIn);
    }).fail(function() {
        $navbarMsg.trigger("tasksUpdate", [false, false, bUploadDone, bAnalysisDone, bContactDeleteDone]);
  });
} // updateRevupAlert

function displayRevupAlert(product, data, bDoDelete) {
    function addToTaskLst(task) {
        var bFound = false;
        // look for the task in the list
        for (var i = 0; i < tasksLst.length; i++) {
            if (tasksLst[i].id == task.id) {
                tasksLst[i].status = task.status;

                return;
            }
        }

        // add the task to the list
        var taskObj = new Object();
        taskObj.id = task.id;
        taskObj.label = task.label;
        taskObj.status = task.status;
        taskObj.verbose_task_type = task.verbose_task_type;
        taskObj.task_type = task.task_type;
        taskObj.account = task.account;
        tasksLst.push(taskObj);
    } // addToTaskLst

    function removeFromTaskLst(taskId) {
        for (var i = 0; i < tasksLst.length; i++) {
            if (tasksLst[i].id == taskId) {
                tasksLst.splice(i, 1);

                return;
            }
        }
    } // removeFromTaskLst

    if (window.message === 'permission_failure') {
        data['count']++;
        data['results'].push({
            'status': 'Failed',
            'verbose_task_type': 'You do not currently have permission to analyze contacts, Analysis',
        });
    }

    // If the interval is fixed, automatically set it to run on the short interval, regardless of task status.
    if (product === 'nonprofit') {
        var refreshTimer = getStatusTimeShort;
    } else {
        var refreshTimer = getStatusTimeLong;
    }

    // update the status bar
    var msg;
    if (data && data.count == 0) {
        // update the message
        if (bUploadDone && !bAnalysisDone) {
            msg = 'Your contacts have been uploaded by RevUp'
        }
        else if (!bUploadDone && bAnalysisDone && !bContactDeleteDone) {
            msg = 'Your contacts have been analyzed by RevUp'
        }
        else if (!bUploadDone && bContactDeleteDone && !bAnalysisDone) {
            msg = 'The contact source has been deleted';
        }
        else if (bTaskDone){
            msg = 'RevUp has finished processing your tasks';
        }
        else {
            if (bContactDeleteDone) {
                msg = 'Your contacts were succesfully deleted and analyzed by RevUp'
            }
            else {
                msg = 'Your contacts were succesfully uploaded and analyzed by RevUp';
            }
            $navbarMsg.trigger("tasksUpdate", [false, false, bUploadDone, bAnalysisDone, bContactDeleteDone]);
        }
        if (bOnRankingPage) {
            msg += '<button class="revupBtnDefault btnSmall reloadPage">Refresh</button>'
        }
        updateAlertDisplay(msg, 'green', 'checked', false, true);

        // clear the task list
        $navbarTaskList.hide().html('');

        // blink the ranking tab
        if (currentAlertDisplayValues.bJustRanAnalysis) {
            if (!bOnRankingPage) {
                $('.sideNav .rankingTab').addClass('blink');
                currentAlertDisplayValues.bJustRanAnalysis = false;
            }
        }

        if ((data.count == 0) && (tasksLst.length > 0)) {
            // walk the list of left over tasks
            for (var i = 0; i < tasksLst.length; i++) {
                if (tasksLst[i].task_type === 'NS') {
                    // blick the rankings tab
                    if (!bOnRankingPage) {
                        $('.sideNav .rankingTab').addClass('blink');
                        currentAlertDisplayValues.bJustRanAnalysis = false;
                    }
            }
                else {
                    // contact container
                    if (result && result.id) {
                        var $contactDiv = $('.contactUserSection .contactContainer[taskId=' + result.id +']');
                        if ($contactDiv.length > 0) {
                            // hide the spinner
                            $('.contactLoadingDiv', $contactDiv).hide();

                            // show the count
                            $('.contactCountDiv', $contactDiv).show();
                        }
                    }
                }
            }
        }
    }
    else if (data && data.count) {
        // see if there is a transition from finished to starting again
        if ($navbarMsgBox.hasClass('msgGreen')) {
            bUploadDone = false;
            bAnalysisDone = false;
            bContactDeleteDone = false;
        }

        // display the correct status message
        var nAnalysis = 0;
        var nUpload = 0;
        var nTasks = 0;
        var nDeleteContact = 0;
        let cloneTask = false;
        for (i in data['results']) {
            if (data['results'].hasOwnProperty(i)) {
                var result = data['results'][i];
                if (result.task_type === "NS") {
                    nAnalysis += 1;
                    bAnalysisDone = true;
                }
                else if (result.task_type == 'DC') {
                    nDeleteContact += 1;
                    bContactDeleteDone = true;
                }
                else if (result.verbose_task_type == 'Clone Contacts to Account'){
                    cloneTask = true;
                }
                // Import tasks
                else if (importTasks.indexOf(result.task_type) > -1){
                    nUpload += 1;
                    bUploadDone = true;
                }
                else {
                    nTasks += 1;
                    bTaskDone = true;
                }
            }
        }

        var uploadingContacts = false;
        var analyzingContacts = false;
        var deleteContactContacts = false;
        if (cloneTask){
            if(nAnalysis == 0){
            msg = 'Cloning ' + result.label;
            }
            else{
                msg = 'RevUp is processing your tasks';
            }
        }
        else if (nTasks > 0){
            msg = 'RevUp is processing your tasks';
        }
        else if (nUpload > 0 && nAnalysis == 0) {
            msg = 'RevUp is uploading your contacts';
            uploadingContacts = true;
        }
        else if (nUpload == 0 && nAnalysis > 0) {
            msg = 'RevUp is analyzing your contacts';
            analyzingContacts = true;
        }
        else if (nUpload == 0 && nDeleteContact > 0) {
            msg = 'RevUp is deleting a contact source';
            deleteContactContacts = true;
        }
        else {
            msg = 'Your contacts are currently being uploaded and analyzed by RevUp';
            uploadingContacts = true;
            analyzingContacts = true;
        }

        $navbarMsg.trigger("tasksUpdate", [uploadingContacts, deleteContactContacts, bUploadDone, bAnalysisDone, analyzingContacts]);
        updateAlertDisplay(msg, 'blue', 'info', true, false);

        // set the flag that upload and analysis are running
        if (analyzingContacts)
            currentAlertDisplayValues.bJustRanAnalysis = true;
        /* initialize progress bar on first run */
        if ($navbarMsg.is(":hidden")) {
            // display the message box
            $navbarMsg.show();

            bDisplayed = true;
        }

        // refresh every 5 seconds as long as there is data
        refreshTimer = 5000;

        // analysis the dataType
        var i;
        var sTmp = [];
        var numUploadFail = 0;
        var numAnalysisFail = 0;
        var numOtherTaskFail = 0;
        for (i in data['results']) {
            if (data['results'].hasOwnProperty(i)) {
                var result = data['results'][i];
                var task_type = result.task_type === "NS" ? "analysis" : "import";
                var taskName = '';
                dTmp = [];
                if (result.task_type === "NS") {
                var msg = 'Analysis for ' + result.label;
                    dTmp.push("<div class='downloadImg'></div>");
                    dTmp.push('<div class="msgText" title="' + msg +'">' + msg + '</div>');

                    taskName = dTmp.join('');
                }
                else if (result.task_type == "DC") {
                    let msg = 'Deleting Source ' + result.label;
                    dTmp.push("<div class='downloadImg'></div>");
                    dTmp.push('<div class="msgText" title="' + msg +'">' + msg + '</div>');

                    taskName = dTmp.join('');
                }
                else if (task_type == 'import' && result.verbose_task_type == "Clone Contacts to Account") {
                    let msg = 'Cloning ' + result.label;
                    dTmp.push("<div class='downloadImg'></div>");
                    dTmp.push('<div class="msgText" title="' + msg +'">' + msg + '</div>');
                    taskName = dTmp.join('');

                }
                else {
                    switch (result.task_type) {
                        case 'csv':
                        case 'CV':
                        case 'cv':
                            dTmp.push("<img class='downloadImg imgCSV' src='" + taskBarImages.csvImg + "'>");
                            break;
                        case 'gmail':
                        case 'GM':
                        case 'gm':
                            dTmp.push("<img class='downloadImg imgGmail' src='" + taskBarImages.gmailImg + "'>");
                            break;
                        case 'linkedin':
                        case 'li':
                        case 'LI':
                            dTmp.push("<img class='downloadImg imgLinkedIn' src='" + taskBarImages.linkedInImg + "'>");
                            break;
                        case 'vcard':
                        case 'ap':
                        case 'AP':
                        case 'vc':
                        case 'VC':
                        case 'ot':
                        case 'OT':
                        case 'ab':
                        case 'AB':
                            dTmp.push("<img class='downloadImg imgApple' src='" + taskBarImages.appleImg + "'>");
                            break;
                        case 'outlook':
                        case 'ou':
                        case 'OU':
                            dTmp.push("<img class='downloadImg imgOutlook' src='" + taskBarImages.outlookImg + "'>");
                            break;
                        case 'iphone':
                        case 'ip':
                        case 'IP':
                        case 'mb':
                        case 'MB':
                            dTmp.push("<img class='downloadImg imgOutlook' src='" + taskBarImages.mobileImg + "'>")
                            break;
                        default:
                            dTmp.push("<div class='downloadImg'></div>")
                            break;
                    } // end switch - result.task_type

                    dTmp.push('<div class="msgText">' + result.label + '</div>')
                    taskName = dTmp.join('');
                }

                // build the percent column
                var percentColumn = '0%';
                var pDone = 'width:0%;';
                var pDoneExtra = '';
                if (result.status === 'Queued') {
                    percentColumn = 'Queued';
                }
                else if (result.status === 'Failed') {
                    percentColumn = 'Failed';

                    if (result.task_type === "NS") {
                        numAnalysisFail += 1;
                    }
                    else if (importTasks.indexOf(result.task_type) > -1) {
                        numUploadFail += 1;
                    }
                    else {
                        numOtherTaskFail += 1;
                    }
                }
                else if (result.status === 'Completed') {
                    percentColumn = '100%';
                    pDone = 'width:100%;border-top-right-radius:5px;border-bottom-right-radius:5px;';
                }
                else {
                    var percent = result.percent == null ? 0 : result.percent;
                    // There can be a long delay where the task is technically started, but is waiting
                    // for other tasks ahead of it. Let's call that Queued
                    if (percent === 0){
                        percentColumn = 'Queued';
                    } else {
                        percentColumn = percent + '%';

                        pDone = 'width:' + percent + '%;';
                        if (percent > 97) {
                            pDone += 'border-top-right-radius:5px;border-bottom-right-radius:5px';
                        }
                    }
                }

                // build an entry into the list of tasks
                sTmp.push('<div class="taskEntry">');
                    sTmp.push('<div class="taskName">' + taskName + '</div>');
                    sTmp.push('<div class="taskStatusBar">');
                        sTmp.push('<div class="statusBar" style="' + pDone +'"></div>')
                    sTmp.push('</div>');
                    sTmp.push('<div class="taskPercent">');
                        sTmp.push('<div class="msgText">' + percentColumn + '</div>')
                    sTmp.push('</div>')
                sTmp.push('</div>')

                if (result.status === "Completed") {
                    // increase count of completed tasks
                    currentAlertDisplayValues.completed += 1;

                    // remove from the tasks list
                    removeFromTaskLst(result.id);

                    if (bDoDelete) {
                        // remove the task from celery
                        $.ajax({
                            url: site_url("task") + result.id + "/",
                            type: "DELETE"
                        })
                        .fail(function( responseText ){
                            console.error("Delete task(Completed) - ERROR: " + responseText);
                        });
                    }
                }
                else if (result.status === "Failed") {
                    // increate the count of failed tasks
                    currentAlertDisplayValues.failed += 1;

                    // remove from the tasks list
                    removeFromTaskLst(result.id);

                    if (bDoDelete) {
                        $.ajax({
                            url: site_url("task") + result.id + "/",
                            type: "DELETE"
                        })
                        .fail(function( responseText ){
                            console.error("Delete task(Failed) - ERROR: " + responseText);
                        });
                    }
                }
                else {
                    // add to the tasks list
                    addToTaskLst(result);
                }
            }

            // display the task list
            $navbarTaskList.html('<div class="taskListInner">' + sTmp.join('') + '</div>');
        }

        // if there is a failure change the message
        if (currentAlertDisplayValues.failed > 0) {
            var msg = 'Failed';
            if (numOtherTaskFail > 0){
                msg = "Task Failed";
            }
            else if (numAnalysisFail > 0 && numUploadFail == 0) {
                msg = 'Analysis failed';
            }
            else if (numAnalysisFail == 0 && numUploadFail > 0) {
                msg = 'Contact upload failed';
            }
            else if (numAnalysisFail > 0 && numUploadFail > 0) {
                msg = 'Contact upload and Analysis failed';
            }
            updateAlertDisplay(msg, 'red', 'stop', true, true);
        }
    }

    return refreshTimer;
} // displayRevupAlert

function forceAnalysisBtnLoad()
{
    $('.forceAnalysis').on('click', function(e) {
        $('body').revupConfirmBox({
            okBtnText: 'Force Analysis',
            cancelBtnText: 'Cancel',
            headerText: 'Analysis Not Needed',
            msg: 'Analysis is not needed at this time. <br />If you would like to force an Analysis anyways, press the \"Force Analysis\" button',
            isDragable: false,
            zIndex: 30,
            okHandler: function() {
                $.ajax({
                    url: site_url('runAnalysis'),
                    type: "GET",
                })
                .done(function(r) {
                    updateRevupAlert();
                })
                .fail(function(r) {
                })

            }
        });

        return false;
    })
}

function contactCleanupBtnLoad(hasAdmin) {
    $('.contactCleanupBtn').on('click', function(e) {
        manageContactMerge.display(hasAdmin);
    })
}

$(document).ready(function () {

    $('.rankingLegend').off('click')
                       .on('click', function(e) {
                           $('.rankingLegendModal').modal('show');
                           e.stopPropagation();

                           return false;
                       })

    // context menus
    // context menu handlers
    $(document).on("mousedown", function (e) {
        // If the clicked element is not the menu
        if (!$(e.target).parents(".contextMenu").length > 0) {

            // Hide it
            $(".contextMenu").hide(100);
        }
    });

    // If the menu element is clicked
    $(".contextMenu li").click(function() {

        let btn = $(this).attr('btnId');
        let sel = $(this).attr('targetSelector');
        $(sel + ' .' + btn).trigger('click', 'fromContextMenu');

        // Hide it AFTER the action was triggered
        $(".contextMenu").hide(100);
    });
});



// CSRF protection enabled AJAX
// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});


function validEmailAddress(addr)
{
    //var sEmailValRegEx = '^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$';
    // The following regex follows RFC 822
    // The regex for is this article
    //      http://stackoverflow.com/questions/46155/validate-email-address-in-javascript
    var sQtext = '[^\\x0d\\x22\\x5c\\x80-\\xff]';
    var sDtext = '[^\\x0d\\x5b-\\x5d\\x80-\\xff]';
    var sAtom = '[^\\x00-\\x20\\x22\\x28\\x29\\x2c\\x2e\\x3a-\\x3c\\x3e\\x40\\x5b-\\x5d\\x7f-\\xff]+';
    var sQuotedPair = '\\x5c[\\x00-\\x7f]';
    var sDomainLiteral = '\\x5b(' + sDtext + '|' + sQuotedPair + ')*\\x5d';
    var sQuotedString = '\\x22(' + sQtext + '|' + sQuotedPair + ')*\\x22';
    var sDomain_ref = sAtom;
    var sSubDomain = '(' + sDomain_ref + '|' + sDomainLiteral + ')';
    var sWord = '(' + sAtom + '|' + sQuotedString + ')';
    var sDomain = sSubDomain + '(\\x2e' + sSubDomain + ')*';
    var sLocalPart = sWord + '(\\x2e' + sWord + ')*';
    var sAddrSpec = sLocalPart + '\\x40' + sDomain; // complete RFC822 email address spec
    var sValidEmail = '^' + sAddrSpec + '$'; // as whole string

    var reValidEmail = new RegExp(sValidEmail);
    //var reValidEmail = new RegExp(sEmailValRegEx);

    if (reValidEmail.test(addr)) {
        return true;
    }

    return false;
} // validEmailAddress


// handlers for the stylized file button
function stylizedFilePressed(btnThis, $hiddenFileTag, $previewImg, $sampleMsg)
{
    // get the parent and the hidden file tag
    var $parentDiv = $(btnThis).parent();
    //var $hiddenFileTag = $(hiddenFileTag);

    // pass the click to the hidden file tag
    $hiddenFileTag.click();

    // handle the selection of a new file
    $hiddenFileTag.off('change')
                  .on('change', function(e) {
                        // get the parent
                        var $fileName = $('.fileName', $parentDiv);

                        // get the file and remove c:\fakepath\
                        var p = $(this).val();
                        p = p.replace("C:\\fakepath\\", "");

                        // update the file name displayed
                        $fileName.text(p);
                        $fileName.attr('title', p);

                        // trigger message that the file was uploaded
                        $hiddenFileTag.trigger({type: 'revup.changeFile', fileName: this.files[0]});

                        // get the size of the new file
                        if (this.files && this.files[0] && $previewImg) {
                            var reader = new FileReader();

                            // once the image is loaded then place into a temp location and get size
                            reader.onload = function(eLoaded) {
                                var image = new Image();
                                image.src = eLoaded.target.result;

                                image.onload = function(eImage) {
                                    // display the warning message
                                    var imgWidth = parseInt($parentDiv.attr("imgWidth"));
                                    var imgHeight = parseInt($parentDiv.attr('imgHeight'));
                                    if ((imgWidth != this.width) || (imgHeight != this.height)) {
                                        $("#revup_alert_template button").after('<span>Selected Image is not of the correct format of ' + imgWidth + 'x' + imgHeight + '</span>');
                                        $('#revup_alert_template').fadeIn('slow', function() {
                                            setTimeout(function() {
                                                // close after 10 seconds by simulating clicking on close
                                                $('#revup_alert_template .close').click();
                                            }, 10000);
                                        });
                                    }

                                    // preview the image
                                    if ($previewImg.length > 0) {
                                        $previewImg.attr('src', eLoaded.target.result);
                                    }

                                    // hide the sample message is image changes
                                    if (($sampleMsg != undefined) && ($sampleMsg.length > 0)) {
                                        $sampleMsg.hide();
                                    }
                                }
                            }

                            $(btnThis).trigger({
                                type: 'revup.fileLoaded',
                                fileName: $(this).val(),
                            })

                            // read the file
                            reader.readAsDataURL(this.files[0])
                        }
                    });

}

/*
 *
 * Compute the width of columns
 *
 */
function sizeColumns($containerSelector, columns)
{
    // get the width of the container - done this way to prevent round up that jQuery does
    var totalWidth = Math.ceil($containerSelector[0].getBoundingClientRect().width);
    totalWidth -= 1

    // get the width of the fixed sections
    var fixedWidth = 0;
    for (var i = 0; i < columns.length; i++) {
        if (columns[i].bFixed) {
            if (columns[i].orgWidth == undefined) {
                var oWidth = $('.' + columns[i].className, $containerSelector).outerWidth(true);
                columns[i].orgWidth = oWidth;
            }

            fixedWidth += columns[i].orgWidth;
        }
    }

    var varPool = totalWidth - fixedWidth;

    // get the left and right margin of the variable columns and adjust variable pool
    var totalVarMargin = 0
    var lastColumn = -1;
    for (var i = 0; i < columns.length; i++) {
        if (!columns[i].bFixed) {
            var $c = $('.' + columns[i].className, $containerSelector);
            var margin = parseInt($c.css('margin-left'), 10) + parseInt($c.css('margin-right'), 10);
            totalVarMargin += margin;

            lastColumn = i;
        }
    }
    varPool -= totalVarMargin;

    // compute column widths
    var usedWidth = fixedWidth + totalVarMargin;
    for (var i = 0; i < columns.length; i++) {
        // if ifxed skip it
        if (columns[i].bFixed) {
            continue;
        }

        var $c = $('.' + columns[i].className, $containerSelector);

        var w;
        if (i == lastColumn) {
            w = totalWidth - usedWidth;
        }
        else {
            var w = Math.floor(varPool * columns[i].percent);
            if ((columns[i].min != undefined) && (w < columns[i].min)) {
                w = columns[i].min;
            }
            else if ((columns[i].max != undefined) && (w > columns[i].max)) {
                w = columns[i].max;
            }
            usedWidth += w;
        }

        // remove a border
        w -= parseInt($c.css('borderLeftWidth'), 10) + parseInt($c.css('borderRightWidth'), 10)
        if ((usedWidth + w) >= totalWidth) {
            w -= 1;
        }
        $c.width(w);
    }
} // sizeColumns


function getOS(){
    function contains(a, b) {
        return a.indexOf(b) != -1
    }

    user_agent = navigator.userAgent;
    platform = navigator.platform;

    if (contains(user_agent, "Win")) {
        return "Windows"
    } else if (contains(platform, "Mac")){
        return "Mac"
    } else if (contains(platform, "Linux")){
        return "Linux"
    } else if (contains(user_agent, "iPad")){
        return "iPad"
    } else if (contains(user_agent, "iPhone")){
        return "iPhone"
    } else {
        return "Unknown"
    }
} // getOS

/*
 * prototype
 *
 */
//if(typeof String.prototype.startsWith != 'function'){
    String.prototype.startsWith = function(str, bIgnoreCase) {
        if(str == null)
            return false;

        if (bIgnoreCase == undefined)
            bIgnoreCase = false;

        var srcStr = this;
        if (bIgnoreCase) {
            str = str.toLowerCase();
            srcStr = srcStr.toLowerCase();
        }

        var i = str.length;
        if(this.length < i)
            return false;
        for(--i; (i >= 0) && (srcStr[i] === str[i]); --i)
            continue;
        return i < 0;
    }
//}

/*
 * Helper functions
 */
function getAnalysisContactValue(config)
{
    var data = undefined;
    var fieldName = undefined;
    if (arguments.length == 2) {
        data = arguments[1];
    }
    else if (arguments.length == 3) {
        fieldName = arguments[1];
        data = arguments[2];
    }

    var val = undefined;

    // make sure there is a configuration
    if (!config) {
        return undefined
    }

    var s = data;
    if (config.section) {
        s = data[config.section];
    }

    // get the source field
    var f = undefined;
    if (fieldName) {
        f = s[config[fieldName]];
    }
    else if (config.field) {
        f = s[config.field];
    }
    else if (config.fieldStartWith) {
        for (var key in s) {
            if (key.startsWith(config.fieldStartWith)) {
                var d = s[key];

                for (var sub_key in d) {
                    if (d[sub_key].length > 0) {
                        f = d[sub_key];
                    }
                }
            }
        }
    }
    if (f == undefined) {
        return undefined;
    }

    // get the value
    if (config.signalSet) {
        // find the signal set in the field
        var subF = undefined;
        for (var i = 0; i < f.length; i++) {
            if (f[i].signal_set_title == config.signalSet) {
                subF = f[i][config.subField][0];
            }
        }
        if (!subF) {
            return undefined;
        }

        if (subF) {
            if (Array.isArray(config.key)) {
                val = [];
                for (var i = 0; i < config.key.length; i++) {
                    val.push(subF[config.key[i]]);
                }
            }
            else {
                val = subF[config.key]
            }
        }
    }
    else {
        if (Array.isArray(config.key)) {
            val = [];
            for (var i = 0; i < config.key.length; i++) {
                val.push(f[i]);
            }
        }
        else {
            val = f;
        }
    }

    return val
} // getAnalysisContactValue

/*
 *
 * CSS - Hooks
 *
 */
// have backgroundColor return hex value instead of rgb(r, g, b)
// based on stackoverflow - Can I force jQuery.css(“backgroundColor”) returns on hexadecimal format?
$.cssHooks.backgroundColor = {
    get: function(elem) {
        if (elem.currentStyle)
            var bg = elem.currentStyle["backgroundColor"];
        else if (window.getComputedStyle)
            var bg = document.defaultView.getComputedStyle(elem,
                null).getPropertyValue("background-color");
        if (bg.search("rgb") == -1)
            return bg;
        else {
            bg = bg.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
            function hex(x) {
                return ("0" + parseInt(x).toString(16)).slice(-2);
            }
            return "#" + hex(bg[1]) + hex(bg[2]) + hex(bg[3]);
        }
    }
};


/****************************/
/*                          */
/*     jQuery Plug-ins      */
/*                          */
/****************************/
(function (jQuery, window, undefined) {
    keyCode = {
        BACKSPACE: 8,
        COMMA: 188,
        DASH: 189,
        SUBTRACT: 109,
        DELETE: 46,
        DOWN: 40,
        END: 35,
        ENTER: 13,
        ESCAPE: 27,
        HOME: 36,
        LEFT: 37,
        PAGE_DOWN: 34,
        PAGE_UP: 33,
        PERIOD: 190,
        DECIMAL_POINT: 110,
        RIGHT: 39,
        SPACE: 32,
        TAB: 9,
        UP: 38
    };

    jQuery.escapeHtml = function(str)
    {
        return jQuery('<div/>').text(str).html();
    };

    /**
     * Copyright 2012, Digital Fusion
     * Licensed under the MIT license.
     * http://teamdf.com/jquery-plugins/license/
     *
     * @author Sam Sehnert
     * @desc A small plugin that checks whether elements are within
     *     the user visible viewport of a web browser.
     *     only accounts for vertical position, not horizontal.
     */

    $.fn.visibleOld = function(partial) {
        var $t            = $(this),
            $w            = $(window),
            viewTop       = $w.scrollTop(),
            viewBottom    = viewTop + $w.height(),
            _top          = $t.offset().top,
            _bottom       = _top + $t.height(),
            compareTop    = partial === true ? _bottom : _top,
            compareBottom = partial === true ? _top : _bottom;

        return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
    };

    $.fn.visible = function(boundingSelector) {
        let $t            = $(this);
        let $w            = $(window);
        if (boundingSelector) {
            $w = boundingSelector;
        }
        let viewTop       = $w.scrollTop();
        let viewBottom    = viewTop + $w.height();
        let top           = $t.offset().top;
        let parentTop     = $t.parent().offset().top;
        top = top - parentTop;
        let bottom       = top + $t.height();

        return ((top <= viewBottom) && (bottom >= viewTop));
    };

    $.fn.redraw = function(){
      $(this).each(function(){
        var redraw = this.offsetHeight;
      });
    };

    $.fn.dialogVisible = function(partial) {
        var $t            = $(this),
            $w            = $('.revupDialogBox'),
            viewTop       = $w.scrollTop(),
            viewBottom    = viewTop + $w.height(),
            _top          = $t.offset().top,
            _bottom       = _top + $t.height(),
            compareTop    = partial === true ? _bottom : _top,
            compareBottom = partial === true ? _top : _bottom;

        return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

    };

    jQuery.fn.textWidth = function()
    {
        var htmlCalc = jQuery('<span>' + jQuery(this).html() + '</span>');
        htmlCalc.css('font-size',jQuery(this).css('font-size')).hide();
        htmlCalc.prependTo('body');
        var width = htmlCalc.width();
        htmlCalc.remove();

        return width;
    };

    jQuery.fn.textHeight = function(w)
    {
        var htmlCalc = jQuery('<div>' + jQuery(this).html() + '</div>');
        htmlCalc.css('font-size',jQuery(this).css('font-size')).hide();
        htmlCalc.css('width', w);
        htmlCalc.css('word-wrap', 'break-word');
        htmlCalc.css('margin', jQuery(this).css('margin'));
        htmlCalc.css('padding', jQuery(this).css('padding'));
        htmlCalc.prependTo('body');
        var height = htmlCalc.outerHeight(true);
        htmlCalc.remove();
        return height;
    };

    jQuery.fn.fieldEllipsis = function()
    {
        return this.each(function() {
            var width = jQuery(this).innerWidth();
            var stringWidth = jQuery(this).textWidth();
            if (stringWidth >= width) {
                jQuery(this).attr("title", jQuery.trim(jQuery(this).text()));
            }
        });
    };

    jQuery.fn.setFocusToEnd = function()
    {
        return this.each(function() {
            var $field = $(this);

            if ($field.attr('contenteditable') == 'true') {
                $field.focus();
                if ((typeof window.getSelection != "undefined") && (typeof document.createRange != "undefined")) {
                    var range = document.createRange();
                    range.selectNodeContents($field[0]);
                    range.collapse(false);
                    var sel = window.getSelection();
                    sel.removeAllRanges();
                    sel.addRange(range);
                }
                else if (typeof document.body.createTextRange != "undefined") {
                    var textRange = document.body.createTextRange();
                    textRange.moveToElementText($field[0]);
                    textRange.collapse(false);
                    textRange.select();
                }
            }
            else if ($field.is('div') && ($(':first-child', $field).hasClass('revupDropDown'))) {
                $field.focus();
                $field.revupDropDownField('setFocus');
            }
            else {
                // get current value
                var tmp = $field.val();

                $field.focus().val('').val(tmp);
            }
        });
    }; // setFocusToEnd

    // based on artical from stackover - http://stackoverflow.com/questions/536814/insert-ellipsis-into-html-tag-if-content-too-wide
    jQuery.fn.ellipsis = function()
    {
        return this.each(function() {
            var el = jQuery(this);

            if(el.css("overflow") == "hidden") {
                var text = el.html();
                var multiline = el.hasClass('multiline');
                var t = jQuery(this.cloneNode(true)).hide()
                                                    .css('position', 'absolute')
                                                    .css('overflow', 'visible')
                                                    .css('word-wrap', 'break-word')
                                                    .css('top', -1000)
                                                    .css('left', -1000)
                                                    .width(multiline ? el.width() : 'auto')
                                                    .height(multiline ? 'auto' : el.height());

                el.after(t);

                function height()
                {
                    return t.height() > el.height();
                };

                function width()
                {
                    return t.width() > el.width();
                };

                var func = multiline ? height : width;
                while (text.length > 0 && func()) {
                    text = text.substr(0, text.length - 1);
                    t.html(text + "...");
                }

                el.html(t.html());
                t.remove();
            }
        });
    };

    // build a transparant overlay
    jQuery.fn.overlay = function(bDoNotCloseOnClick, zIndex, bClearOverlay, parentTo, bPassThrough)
    {
        var $caller = $(this);
        // get the options, if any
        // var opts = $.extend({}, jQuery.fn.overlay.defaults, options);

        if (zIndex == undefined) {
            zIndex = 10;
        }

        // if zIndex is auto look for "revupDlg" and make the zIndex one more than it
        if (zIndex == 'auto') {
            var $dlg = $(this).closest('.revupDlg');
            if ($dlg.length == 0) {
                $dlg = $(this).closest('.revupPopup');
            }
            if ($dlg.length != 0) {
                var z = $dlg.css('z-index');
                z = parseInt(z, 10);
                if (!isNaN(z)) {
                    zIndex = z + 1;
                }
            }
            else {
                zIndex = 10;
            }
        }
        var extraStyle = "z-index:" + zIndex + ";xopacity:.99;";

        // get the parent of the overlay
        if (parentTo == undefined) {
            parentTo = 'body';
        }

        // should the overlay be clear
        if (bClearOverlay == undefined) {
            bClearOverlay = false;
        }
        if (bClearOverlay) {
            extraStyle += "background-color:transparent;opacity:1";
        }

        // create the overlay
        var $overlay = $('<div class="revupOverlay" style="' + extraStyle + ';"/>')
                       .appendTo(parentTo);
        $overlay.prop('id', "revupOverlay" + zIndex);
        $overlay.css('height', $(document).height() + 'px')
        $overlay.css('width', $(document).width() + 'px')

        $(window).on('resize', function() {
            $overlay.css('height', $(document).height() + 'px')
            $overlay.css('width', $(document).width() + 'px')
        });

        // see if background should be closed on click
        if ((bDoNotCloseOnClick == undefined) || (!bDoNotCloseOnClick)) {
            $overlay.on('click', function(e) {
                var bCancelPassThrough = $caller.delay(500).triggerHandler('revup.overlayClick');
                $(this).remove();
                if (bPassThrough) {
                    if (bCancelPassThrough) {
                        return;
                    }

                    // get element at point of click
                    starter = document.elementFromPoint(e.clientX, e.clientY);

                    // send click to element at finger point
                    $(starter).click();
                }
            })
        }

        $overlay.on('revup.overlayClose', function() {
            $overlay.remove();
        })

        return $overlay;
    }

    jQuery.fn.commify = function()
    {
        jQuery(this).each(function() {
            // commify the value
            var sVal = '' + $(this).text();

            // see if a floating point number
            var bFloat = false;
            var sValParts = sVal.split(".");
            if (sValParts.length >= 2) {
                bFloat = true;
                sVal = sValParts[0];
            }

            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(sVal)) {
                sVal = sVal.replace(rgx, '$1' + ',' + '$2');
            }

            // if floating point put back together
            if (bFloat) {
                sVal += "." + sValParts[1];
            }

            $(this).text(sVal);
        })

        return this;
    }; // commify

    jQuery.fn.tooltipOnOverflow = function()
    {
        jQuery(this).each(function() {
            $(this).on("mouseenter", function() {
                if (this.offsetWidth < this.scrollWidth) {
                    $(this).attr('title', jQuery.trim($(this).text()));
                }
                else {
                    $(this).removeAttr("title");
                }
            });
        });
    }; // tooltipOnOverflow


    /*
    *
    * NumericOnly
    *
    */
    jQuery.fn.numericOnly = function(options)
    {
        // get the options, if any
        var opts = $.extend({}, jQuery.fn.numericOnly.defaults, options);

        // iterate each element
        return this.each(function() {
            $(this).on('input', function(e) {
                var val = $(this).val().trim();

                var regex = /^\d+$/;
                if (opts.bFloat) {
                    regex = /^[-+]?(\d+\.?\d*)$|(\d*\.?\d+)$/;
                }
                else if (opts.bInteger) {
                    regex = /^[-+]?\d+$/;
                }
                else if (opts.bDollar) {
                    regex = /^[-+]?((\d+(\.\d\d)?)|(\.\d\d))$/;
                }
                else if (opts.bCreditCard) {
                    regex = /^[0-9 -]+$/;
                }
                else if (opts.bPhone) {
                    regex = /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/i;
                }
                else if (opts.bDate) {
                    regex = /^((0?[13578]|10|12)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[01]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1}))|(0?[2469]|11)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[0]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1})))$/
                }
                else if (opts.bUsZipCode) {
                    regex = /^\d{5}(?:[-\s]\d{4})?$/;
                }

                // test the number - trigger even as way display different at different fields
                if ((val == '') || (val.search(regex) != -1)) {
                    if ($(this).hasClass('inputFormatError')) {
                        $(this).trigger('revup.clearInputFormatError')
                    }
                    $(this).removeClass('inputFormatError');
                }
                else {
                    if (!$(this).hasClass('inputFormatError')) {
                        $(this).trigger('revup.setInputFormatError')
                    }
                    $(this).addClass('inputFormatError');
                }
            });
        });
    } // numericOnly

    // default is number []
    jQuery.fn.numericOnly.defaults = {
        bCreditCard: false,     // digit, space or dash
        bFloating: false,       // floating point numbers
        bDollar: false,         // dollar amount, 2 digits after the decimal point
        bInteger: false,        // +|- [0-9]
        bPhone: false,          // phone number
        bDate: false,           // date - M/D/YY, M/D/YYY, MM/DD/YY, MM/DD/YYYY
        bUsZipCode: false,      // us zip code DDDDD or DDDDD-DDDD
    };

    /********************************/
    /*                              */
    /*  Revup Dynamic List Widget   */
    /*                              */
    /********************************/
    jQuery.fn.revupDynLst = function(options, arg1, arg2)
    {
        // make sure there is a dom object
        if ($(this).length < 1) {
            return;
        }

        // see if an command
        if (typeof options == 'string') {
            var command = options;
            var $parent = jQuery(this).parent();
            var $cntl = jQuery('.revupDynamicLst', $parent);
            var $lst = $('.dataSect', $cntl);

            switch (command) {
                case 'getList':
                    var vLst = [];
                    $('.dataEntryDiv', $lst).each(function() {
                        var $textField = $('.textField', jQuery(this));
                        var v = $.trim($textField.val());
                        if (v != '') {
                            vLst.push(v);
                        }
                    });

                    return vLst;
                    break;
                case 'setList':
                    // Extend our default options with those provided.
                    var opts = $.extend( {}, $.fn.revupDynLst.defaults, arg2 );

                    var sTmp = [];
                    if (typeof arg1 == 'string') {
                        sTmp.push(addEntry(arg1));
                    }
                    else {
                        for(var i = 0; i < arg1.length; i++) {
                            sTmp.push(addEntry(arg1[i]));
                        }
                    }

                    if (sTmp.length > 0) {
                        $lst.html(sTmp.join(''));
                    }

                    // update the entries count
                    var count = jQuery('.dataEntryDiv', $lst).length
                    if (opts.bDisplayNumEntries) {
                        var msg = "1 Entry";
                        if (count > 1) {
                            msg = count + " Entries";
                        }
                        jQuery('.dataSectCount .count').text(msg);
                    }

                    break;
                default:
                    console.error("revupDynLst - invalid command: " + command);
                    break;
            } // end switch - command

            return;
        }

        // Extend our default options with those provided.
        var opts = $.extend( {}, $.fn.revupDynLst.defaults, options );

        // container to build in
        var sTmp = [];

        // get the parent
        var $root = jQuery(this);
        var rootClass = $root.prop('class');

        function addEntry(val)
        {
            // if undefined add a empty string
            if (val == undefined) {
                val = '';
            }

            var sTmp2 = [];
            sTmp2.push('<div class="dataEntryDiv">');
                // edit field frame
                if (opts.deleteBtnExtraClass == "onboarding") {
                    sTmp2.push('<div class="textFieldFrame onboarding">');
                }
                else {
                    sTmp2.push('<div class="textFieldFrame">');
                }

                    // edit field
                    sTmp2.push('<input type="text" class="textField')
                    // extra class (optional)
                    if (opts.inputClass !== "") {
                        sTmp2.push(' opts.inputClass')
                    }
                    sTmp2.push('"')

                    // form name (optional)
                    if (opts.inputName !== '') {
                        sTmp2.push(' name="' + opts.inputName + '"')
                    }

                    // max length (optional)
                    if ((opts.inputMaxLen !== 0) || (opts.inputMaxLen !== '')) {
                        sTmp2.push(' maxlength="' + opts.inputMaxLen + '"');
                    }

                    // placeholder (optional)
                    if ((opts.inputPlaceholder !== 0) || (opts.inputPlaceholder !== '')) {
                        sTmp2.push(' placeholder="' + opts.inputPlaceHolder + '"');
                    }

                    // value and close div
                    if (val !== '') {
                        sTmp2.push(' value="' + val + '"');
                    }

                    sTmp2.push('>');

                    // delete field
                    sTmp2.push('<div class="deleteBtn icon icon-delete-circle"></div>');
                    //sTmp2.push('<input type="button" class="btn btn-xs btn-danger deleteBtn revupBtn" value="' + opts.deleteBtnLabel + '" title="Remove this entry from list">')
                sTmp2.push('</div>');
            sTmp2.push('</div>');

            return sTmp2.join('');
        } // addEntry

        // widget container
        var count;      // number of entries
        if (opts.deleteBtnExtraClass == 'onboarding') {
            sTmp.push('<div class="' + rootClass + ' revupDynamicLst noHover">');
        }
        else {
            sTmp.push('<div class="' + rootClass + ' revupDynamicLst">');
        }
            // add the title and add button
            sTmp.push('<div class="headerLine">');
                if (opts.label != '') {
                    var cVal = 'title';
                    if (opts.labelClass !== '') {
                        cVal += ' ' + opts.labelClass
                    }
                    sTmp.push('<div class="' + cVal + '">' + opts.label + '</div>');
                }

                // add the add button
                if (opts.addBtnLabel) {//sTmp.push('<input type="button" class="btn btn-xs btn-success addBtn revupBtn" value="' + opts.addBtnLabel + '" title="Add a new value">')
                    sTmp.push('<div class="addBtn">' + opts.addBtnLabel + '</div>');
                }
            sTmp.push('</div>');

            // container around the data
            var style = [];
            style.push("min-height:" + opts.entryLstMinHeight);
            if (opts.bScrollEntries) {
                style.push('overflow-y:scroll');
                style.push('max-height:' + opts.entryLstMaxHeight);
            }

            // add the values - if none add an empty entry
            sTmp.push('<div class="dataSect" style="' + style.join(';') + '">');
                var $lstValues = $('.lstValue', $root);
                if ($lstValues.length == 0) {
                    sTmp.push(addEntry(''));
                }
                else {
                    $lstValues.each(function() {
                        var v = $(this).text();

                        sTmp.push(addEntry(v));
                    })
                }
            sTmp.push('</div>');

            if (opts.addBtnBelowlabel){
                sTmp.push('<div class="addBtn toTheRight">');
                    sTmp.push('<div class="icon icon-add-filter"></div>');
                    sTmp.push('<div class="btnText">' + opts.addBtnBelowlabel + '</div>');
                sTmp.push('</div>');
            }

            // add the list counts
            count = $lstValues.length
            if (opts.bDisplayNumEntries) {
                var msg = "1 Entry";
                if ($lstValues.length > 1) {
                    msg = count + " Entries";
                }
                sTmp.push('<div class="dataSectCount ' + opts.numEntriesExtraClass + '">');
                    sTmp.push('<div class="count">' + msg + '</div>');
                sTmp.push('</div>')
            }
        sTmp.push('</div>');

        // hide the orgin list
        jQuery(this).hide();

        // add a new sibling, the stylized list
        var $newCntl = jQuery(sTmp.join(''));
        $root.after($newCntl);

        // see if there is a maximum number of entries and if so and exceeded
        // disable the add button
        if ((opts.maxEntries > 0) && (count >= opts.maxEntries)) {
            jQuery('.addBtn', $newCntl).prop('disabled', true);
        }
        else {
            jQuery('.addBtn', $newCntl).prop('disabled', false);
        }

        // event handlers
        // add button
        jQuery('.addBtn', $newCntl).on('click', function(e) {
            var $cntl = jQuery(this).closest('.revupDynamicLst');

            // look for a blank
            var bBlank = false;
            jQuery('.dataEntryDiv', $cntl).each(function() {
                var $input = jQuery('input[type=text]', $(this))
                if ($input.val() === '') {
                    $input.focus();
                    bBlank = true;
                    return false;
                }
            })
            if (bBlank) {
                return;
            }

            // get the last entry and add
            var $next = jQuery('.dataSect', jQuery($(this).parent().parent()));

            var $newEntry = addEntry('');
            $next.append($newEntry);

            // send event
            $cntl.parent().trigger('revup.dynamicListAdd')

            // find the first blank and set focus there, then scroll into view
            var offset;
            var $input;
            jQuery('.dataEntryDiv', $cntl).each(function() {
                $input = $('input[type=text]', $(this))
                if ($input.val() === '') {
                    $input.focus();
                }
            });
            offset = $input.offset();

            // update count
            var count = jQuery('.dataSect .dataEntryDiv', $cntl).length;
            if (opts.bDisplayNumEntries) {
                var msg = '1 Entry'
                if (count > 1) {
                    msg = count + ' Entries';
                }

                jQuery('.dataSectCount .count', $cntl).text(msg);
            }

            // see if there is a maximum number of entries and if so and exceeded
            // disable the add button
            if ((opts.maxEntries > 0) && (count >= opts.maxEntries)) {
                jQuery(this).prop('disabled', true);
            }
        });

        // send a event when the text changes
        $newCntl.on('keyup change', '.textField', function(e) {
            var $cntl = jQuery(this).closest('.revupDynamicLst');

            $cntl.parent().trigger('revup.dynamicListEntryChange');
        })

        // delete buttons - delegate because don't know how many field will and
        $newCntl.on('click', '.deleteBtn', function(e) {
            // get the entry and the list the entry is in - do before deleting
            var $delBtn = $(this)
            var $entry = $delBtn.closest('.dataEntryDiv');
            var $lst = $delBtn.closest('.dataSect');
            var $cntl = $delBtn.closest('.revupDynamicLst');

            // remove the entry
            $entry.remove();

            // send event
            $cntl.parent().trigger('revup.dynamicListDelete')

            // see if the last entry and if so add a blank entry
            if (jQuery('.dataEntryDiv', $lst).length == 0) {
                var $newEntry = addEntry('');
                $lst.append($newEntry);
            }


            // update count
            var count = jQuery('.dataSect .dataEntryDiv', $cntl).length;
            if (opts.bDisplayNumEntries) {
                var msg = '1 Entry'
                if (count > 1) {
                    msg = count + ' Entries';
                }

                jQuery('.dataSectCount .count', $cntl).text(msg);
            }

            // see if there is a maximum number of entries and if so and exceeded
            // disable the add button
            if ((opts.maxEntries > 0) && (count >= opts.maxEntries)) {
                jQuery('.addBtn', $cntl).prop('disabled', true);
            }
            else {
                jQuery('.addBtn', $cntl).prop('disabled', false);
            }
        })

        $newCntl.on('focus', '.textField', function(e) {
            var $this = $(this);
            var $border = $this.closest('.textFieldFrame');
            $border.css('border-color', revupConstants.color.primaryGray4);
        })
        .on('blur', '.textField', function(e) {
            var $this = $(this);
            var $border = $this.closest('.textFieldFrame');
            $border.css('border-color', revupConstants.color.secondaryGray11)
        })

        // hide/show the delete button
        $newCntl.on('mouseleave', '.textFieldFrame', function() {
            if (!($newCntl.hasClass('noHover'))) {
                $('.deleteBtn', $(this)).hide();
            }
        })
        $newCntl.on('mouseenter', '.textFieldFrame', function() {
            if (!($newCntl.hasClass('noHover'))) {
                $('.deleteBtn', $(this)).show();
            }
        })
    }; // revupDynLst

    jQuery.fn.revupDynLst.defaults = {
        addBtnLabel:            "Add Another",          // label for the add button. IF THIS STRING IS "" IT WILL NOT SHOW
        addBtnBelowlabel:       "",                     // label for the add button. IF THIS STRING IS "" OR THE ADDBTNBELOWLABEL KEY DOES NOT EXIST IT WILL NOT SHOW
        deleteBtnLabel:         "-",                    // label for delete button
        deleteBtnExtraClass:    "",
        maxEntries:             0,                      // maximum number of entries - 0 unlimited
        bScrollEntries:         false,                  // should the entry list scroll
        entryLstMinHeight:      "30px",                 // min height of the entry list
        entryLstMaxHeight:      "120px",                // max height of the entry list - when scrolling
        focusColor:             "",                     // border/background color when in focus, if blank use css
        notInFocusColor:        "",                     // border/background color when not in focus,
        bDisplayNumEntries:     true,                   // display the number of entries
        numEntriesExtraClass:   "",                     // extra count class
        extraClass:             "",                     // extra class for the full widget
        labelClass:             "",                     // class for the label
        label:                  '',                     // label for widget
        inputLstClass:          "",                     // class for list div
        inputName:              '',                     // name value for input field
        inputClass:             "",                     // class for the edit control
        inputMaxLen:            128,                    // max length of input field
        inputPlaceHolder:       "Enter Value"           // default place holder text
    };


    /*
     *
     * Revup - up - down field
     *   This is a stylized/specials scroll control that walks up and down a list of values
     *   after a pause a function is call with the new value
     *
     */
    jQuery.fn.revupUpDownField = function(options)
    {
        // get the options, if any
        var opts = $.extend({}, jQuery.fn.revupUpDownField.defaults, options);

        var $this = $(this);

        // build the control
        var sTmp = [];
        sTmp.push('<div class="revupUpDown">');
            // value section
            sTmp.push('<div class="valDiv"></div>');

            // incrementer div
            sTmp.push('<div class="incrementerDiv">');
                sTmp.push('<div class="upDiv">');
                    sTmp.push('<div class="arrow">&#9650;</div>');
                sTmp.push('</div>');
                sTmp.push('<div class="downDiv">');
                    sTmp.push('<div class="arrow">&#9660;</div>');
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>')
        $this.html(sTmp.join(''));

        // get the selectors for the up/down arrows and value
        var $valDiv = $('.valDiv', $this)
        var $upDiv = $('.incrementerDiv .upDiv', $this);
        var $downDiv = $('.incrementerDiv .downDiv', $this);

        // range check the the startIndex
        var index = opts.valueStartIndex;
        if (opts.valueStartIndex <= 0) {
            index = 0;
            $upDiv.addClass('disable');
        }
        else if (opts.valueStart >= opts.value.length) {
            index = opts.value.length - 1;
            $downDiv.addClass('disable');
        }

        // set the initial value
        $valDiv.html(opts.value[opts.valueStartIndex]);
        $valDiv.attr('valIndex', opts.valueStartIndex);

        // function for changing the value
        var upDownTimer = null;
        function incDecVal(bInc)
        {
            var index = parseInt($valDiv.attr('valIndex'), 10);
            if (isNaN(index)) {
                index = 0;
            }

            if (bInc) {
                index++;
            }
            else {
                index--;
            }

            // make sure the index is in range
            if (index <= 0) {
                index = 0;
                $upDiv.addClass('disable');
            }
            else {
                $upDiv.removeClass('disable');
            }
            if (index >= (opts.value.length - 1)) {
                index = opts.value.length - 1;
                $downDiv.addClass('disable')
            }
            else {
                $downDiv.removeClass('disable')
            }

            // set new value
            $valDiv.attr('valIndex', index);
            $valDiv.text(opts.value[index]);

            // set the timer to do the increament / decreament
            if (upDownTimer != null) {
                clearTimeout(upDownTimer);
                upDownTimer = null
            }
            upDownTimer = setTimeout(function() {
                // call the user function
                if ((opts.changeFunc != undefined) && (opts.changeFunc != null)) {
                    opts.changeFunc(index, opts.value[index]);
                }
                // clear the timer
                upDownTimer = null;
            }, opts.upDownDelay);
        }

        // event handlers for the up/down arrows
        $upDiv.on('mouseenter', function(e) {
            if (!$(this).hasClass('disable')) {
                $(this).addClass('over');
            }
        })
        .on('mouseleave', function(e) {
            $(this).removeClass('over');
        })
        .on('click', function(e) {
            if (!$(this).hasClass('disable')) {
                incDecVal(false);
            }
        })
        $downDiv.on('mouseenter', function(e) {
            if (!$(this).hasClass('disable')) {
                $(this).addClass('over');
            }
        })
        .on('mouseleave', function(e) {
            $(this).removeClass('over');
        })
        .on('click', function(e) {
            if (!$(this).hasClass('disable')) {
                incDecVal(true);
            }
        })

    }; // revupUpDownField

    jQuery.fn.revupUpDownField.defaults = {
        upDownDelay:        500,                    // the delay to tell the up/down scrolling is done
        value:              [10, 20, 30, 40, 50],
        valueStartIndex:    0,
        changeFunc:         null                    // function to call when scrolling stops
                                                    // the functions is call with 2 parameters
                                                    //      index -> index of the value
                                                    //      value -> value displayed
    }; // revupUpDownField.defaults

    /*
     *
     * Revup - dropdown
     *   This is a stylized/special scroll control that walks up and down a list of values.
     *   After a pause, a function is called with the new value.
     *
     */
    jQuery.fn.revupDropDownField = function(options, arg1, arg2)
    {
        // make sure there is a dom object
        if ($(this).length < 1) {
            return;
        }

        // see if an command
        if (typeof options == 'string') {
            var command = options;
            var $dropDown = $(this);

            switch (command) {
                case 'getIndex':
                    // get the current value
                    var $valDiv = $('.revupDropDown .cntlBody .valDiv', $dropDown)
                    var v = $valDiv.attr('index');

                    return v;

                    break;
                case 'getValue':
                    // get the current value
                    var $valDiv = $('.revupDropDown .cntlBody .valDiv', $dropDown)
                    var v = '';
                    if ($valDiv.attr('index') != -1) {
                        v = $valDiv.attr('value');
                    }

                    return v;

                    break;
                case 'getBothValues':
                    // get the current value
                    var $valDiv = $('.revupDropDown .cntlBody .valDiv', $dropDown)
                    var v = $valDiv.attr('value');
                    var dVal = $valDiv.attr('displayValue');

                    return {value: v, displayValue: dVal};

                    break;
                case 'setFocus':
                    // set focus by dropping down the list
                    var $cntlBody = $('.revupDropDown .cntlBody', $dropDown);
                    $cntlBody.trigger('click');
                    break;
                case 'reset':
                    var $dropDownCntl = $('.revupDropDown', $dropDown)
                    var startIndex = $dropDownCntl.data('valueStartIndex');
                    var ifStartNeg1 = $dropDownCntl.data('ifStartIndexNeg1Msg');
                    var valueLst = $dropDownCntl.data('dropDownLst');
                    var bDisplayValue = $dropDownCntl.data('bDisplayValue');

                    var $valDiv = $('.revupDropDown .cntlBody .valDiv', $dropDown)
                    $valDiv.attr('index', startIndex)
                           .attr('value', valueLst[0].value)
                           .attr('displayValue', valueLst[0].displayValue)
                    if (startIndex == -1) {
                        $valDiv.text(ifStartNeg1);
                    }
                    else {
                        $valDiv.text(bDisplayValue ? valueLst[0].value : valueLst[0].displayValue)
                    }

                    break;
                case 'setValue':
                    // set value by dropping down the list
                    var $cntlBody = $('.revupDropDown .cntlBody', $dropDown);
                    var $dropdownLst = $('.revupDropDown .dropDownLst', $dropDown);

                    var bLoaded = false;
                    $('.lstEntry', $dropdownLst).each(function() {
                        var $entry = $(this);
                        var val = $entry.attr('value');

                        if (val == arg1) {
                            // clear selection and select new
                            $('.lstEntry', $dropdownLst).removeClass('selected');
                            $entry.addClass('selected');

                            var index = $entry.attr('index');
                            var dVal = $entry.attr('displayValue');

                            var $val = $('.valDiv', $cntlBody);
                            $val.attr('value', val);
                            $val.attr('displayValue', dVal);
                            $val.attr('index', index);

                            var $dropDownCntl = $('.revupDropDown', $dropDown);
                            var bDisplayValue = $dropDownCntl.data('bDisplayValue');
                            $val.html(bDisplayValue ? val : dVal);

                            // set the loaded flag
                            bLoaded = true;

                            return false;
                        }
                    })

                    // see if the value needs to be added to the list
                    var bAddNew = $('.dropDownAddNewValue', $dropdownLst).length > 0;
                    if ((!bLoaded) && (bAddNew)) {
                        var index = $('.lstEntry', $dropdownLst).length;
                        var sTmp = '<div style="z-index:11010;" class="lstEntry selected" index="' + index + '" value="' + arg1 + '" displayValue="' + arg1 + '">' + arg1 + '</div>';
                        $('.scrollSection', $dropdownLst).append(sTmp);

                        // put into the drop down box
                        var $val = $('.valDiv', $cntlBody);
                        $val.attr('value', arg1);
                        $val.attr('displayValue', arg1);
                        $val.attr('index', index);
                        $val.html(arg1);
                        }
                    break;
                case 'newList': {
                    // arg1 - new list of entries
                    // arg2 - selected index if missing 0
                    let selectedIndex = arg2 ? Number(arg2) : 0;
                    let $dropdownLst = $('.revupDropDown .dropDownLst', $dropDown);
                    var $cntlBody = $('.revupDropDown .cntlBody', $dropDown);

                    let newLst = [];
                    let index = 0;
                    for (let i = 0; i < arg1.length; i++) {
                        newLst.push('<div style="z-index:11010" ');

                        // class
                        if (i == selectedIndex)
                            newLst.push('class="lstEntry selected" ');
                        else
                            newLst.push('class="lstEntry" ');

                        // index and value
                        newLst.push('index="' + i + '" ');
                        newLst.push('value="' + arg1[i].value + '" ');

                        // display value
                        let dVal = arg1[i].value
                        if (arg1[i].displayValue)
                            dVal = arg1[i].displayValue;
                        newLst.push('displayValue="' + dVal + '">' + dVal)

                        // Done
                        newLst.push('</div>');

                        // put into the drop down box
                        if (i == selectedIndex) {
                            var $val = $('.valDiv', $cntlBody);
                            $val.attr('value', arg1[i].value);
                            $val.attr('displayValue', dVal);
                            $val.attr('index', i);
                            $val.html(dVal);
                        }
                    }

                    //replace list
                    $('.scrollSection', $dropdownLst).html(newLst.join(''));

                    break;
                    }
                case 'enable':
                    bEnabled = true;
                    $dropDown.data('enabled', true)
                    $('.revupDropDown', $dropDown).removeClass('disabled');
                    break;
                case 'disable':
                    $dropDown.data('enabled', false)
                    $('.revupDropDown', $dropDown).addClass('disabled');

                    break;
                default:
                    // unknown command
                    // console.error('revupDropDownField - Unknown Command - ' + command);
                    return false;
                    break;
            } // end switch - command

            return true;
        }

        // get the options, if any
        var opts = $.extend({}, jQuery.fn.revupDropDownField.defaults, options);

        // make sure options are within range
        //if (opts.valueStartIndex < 0) {
        //    opts.valueStartIndex = 0;
        //}
        if (opts.valueStartIndex > opts.value.length) {
            opts.valueStartIndex = opts.value.length - 1;
        }

        var $this = $(this);

        // set the enabled flag default
        $this.data('enabled', true);

        // function to build the valueLst - convert the values internal array
        // the val can be either an array of strings or and array of objects
        //          value - this is the value when selected
        //          displayValue - this is the string displayed in the drop down
        function buildValueLst(dataLst) {
            var lst = [];

            $.each(dataLst, function(index, val) {
                var listItem = {};

                if (typeof val == 'object') {

                    // See if item is an array. If it is, convert to an object.
                    if (Array.isArray(val)) {
                        val = {
                            value: val[0],
                            displayValue: val[1]
                        };
                    }

                    if (val.sectionHeader) {
                        listItem.sectionHeader = val.sectionHeader;
                    }
                    else {
                        // make sure the value is not a number - add a blank
                        var vHtml = $.parseHTML(val.value + '')
                        listItem.value = $(vHtml).text();

                        if (typeof val.displayValue == 'number') {
                            val.displayValue = val.displayValue + '';
                        }
                        else if (typeof val.displayValue == 'boolean') {
                            val.displayValue = val.displayValue ? 'true' : 'false';
                        }
                        listItem.displayValue = (val.displayValue == "") ? "" : val.displayValue.replace(/\"/g, "'");
                    }
                }
                else {
                    // make sure the value is not a number - add a blank
                    var vHtml = $.parseHTML(val + '');
                    listItem.value = $(vHtml).text();
                    if (typeof val == 'number') {
                        val = val + '';
                    }
                    else if (typeof val == 'boolean') {
                        val = val ? 'true' : 'false';
                    }
                    listItem.displayValue = (val == "") ? "" : val.replace(/\"/g, "'");
                }
                listItem.index = index;

                lst.push(listItem);
            });

            // see if the data needs to be sorted
            if (opts.sortList) {
                function compareAscending(a, b) {
                    if (a.displayValue < b.displayValue) {
                        return -1;
                    }
                    else if (a.displayValue > b.displayValue) {
                        return 1;
                    }

                    return 0;
                }

                function compareDescending(a, b) {
                    if (a.displayValue > b.displayValue) {
                        return -1;
                    }
                    else if (a.displayValue < b.displayValue) {
                        return 1;
                    }

                    return 0;
                }

                if (opts.sortDescending) {
                    lst = lst.sort(compareDescending);
                }
                else {
                    lst = lst.sort(compareAscending);
                }
            }

            return lst;
        } // buildValueLst

        // function to build drop down list entries
        function buildDropDownListEntries(lst, selectedEntryIndex)
        {
            var tmp = [];

            if (selectedEntryIndex == undefined) {
                selectedEntryIndex = -1;
            }

            $.each(lst, function(index, val) {
                if (val.sectionHeader) {
                    tmp.push('<div style="z-index:11010" class="lstEntry sectionHeader">' + val.sectionHeader + '</div>');
                }
                else {
                    var extraClass = "";
                    if (val.index == selectedEntryIndex) {
                        extraClass = ' selected';
                    }
                    if (opts.bDisplaySectionHeaders) {
                        extraClass += ' sectionIndent'
                    }
                    tmp.push('<div style="z-index:11010;" class="lstEntry' + extraClass + '" index="' + val.index + '" value="' + val.value + '" displayValue="' + val.displayValue + '">' + val.displayValue + '</div>');
                }
            })

            return(tmp.join(''));
        } // buildDropDownListEntries

        // see if update the list
        if (opts.updateValueList) {
            // make sure the control has been initialized
            var $scrollSection = $('.revupDropDown .dropDownLst .scrollSection', $this);
            if ($scrollSection.length == 0) {
                // console.error('revupDropDown not yet created');
                return false;
            }

            // normalize the list, build it and replace it
            var valueLst = buildValueLst(opts.value);
            $scrollSection.html(buildDropDownListEntries(valueLst, opts.valueStartIndex));

            // replace the selected value
            // find the starting value
            var startingValIndex = 0;
            for (var i = 0; i < valueLst.length; i++) {
                if (opts.valueStartIndex == valueLst[i].index) {
                    startingValIndex = i;
                    break;
                }
            }
            var $valDiv = $('.revupDropDown .cntlBody .valDiv', $this);
            $valDiv.attr('index', opts.valueStartIndex);
            if (valueLst.length == 0) {
                $valDiv.attr('value', '');
                $valDiv.attr('displayValue', '');
                $valDiv.html('');
            }
            else {
                $valDiv.attr('value', valueLst[startingValIndex].value);
                $valDiv.attr('displayValue', valueLst[startingValIndex].displayValue);
                if (opts.bDisplayValue) {
                    $valDiv.html(valueLst[startingValIndex].value);
                }
                else {
                    $valDiv.html(valueLst[startingValIndex].displayValue);
                }
            }

            // clear the add new value, if there is one
            $('.revupDropDown .dropDownLst .dropDownAddNewValue', $this).val('');

            return true;
        }

        // build the data/value list
        var valueLst = buildValueLst(opts.value);

        // find the starting value
        var startingValIndex = 0;
        for (var i = 0; i < valueLst.length; i++) {
            if (opts.valueStartIndex == valueLst[i].index) {
                startingValIndex = i;
                break;
            }
        }


        // build the control
        var sTmp = [];
        var style = '';
        var style2 = '';
        sTmp.push('<div tabindex=0 class="revupDropDown">');
            sTmp.push('<div class="cntlBody">')
                // value section
                if (valueLst.length == 0) {
                    sTmp.push('<div class="valDiv" index="-1" value="" displayValue=""></div>');
                }
                else {
                    var displayValue = valueLst[startingValIndex].displayValue;
                    if (opts.bDisplayValue) {
                        displayValue = valueLst[startingValIndex].value;
                    }
                    if ((opts.valueStartIndex == -1) && (opts.ifStartIndexNeg1Msg != "")) {
                        displayValue = opts.ifStartIndexNeg1Msg;
                    }
                    sTmp.push('<div class="valDiv" index="' + opts.valueStartIndex + '" value="' + valueLst[startingValIndex].value + '" displayValue="' +  valueLst[startingValIndex].displayValue + '">' + displayValue + '</div>');
                }

                // incrementer div
                sTmp.push('<div class="arrowDiv"' + style2 + '>');
                    sTmp.push('<div class="icon icon-triangle-dn"' + style2 + '></div>');
                sTmp.push('</div>');
            sTmp.push('</div>')

            // selection list
            sTmp.push('<div class="dropDownLst" style="z-index:11000;" tabindex="-1">');
                // see if a input field should be added to displyed to allow the user to add new values
                if (opts.addNewValue) {
                    sTmp.push('<input class="dropDownAddNewValue" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">')
                }
                sTmp.push('<div class="scrollSection">');
                    sTmp.push(buildDropDownListEntries(valueLst, opts.valueStartIndex));
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>')
        $this.html(sTmp.join(''));

        // event handlers
        var $overlay;
        var orgIndex;
        var orgValue;
        var orgDisplayValue;

        function resetOrginal($dropDown)
        {
            // clear the selected
            var $lst = $('.dropDownLst', $dropDown);
            $('.lstEntry', $lst).removeClass('selected');

            // select the old entry
            $('.lstEntry[index=' + orgIndex + ']', $lst).addClass('selected');

            // reset displayed value
            var $valDiv = $('.valDiv', $dropDown);
            if (opts.bDisplayValue) {
                $valDiv.html(orgValue);
            }
            else {
                if (orgIndex == -1) {
                    $valDiv.html(opts.ifStartIndexNeg1Msg);
                }
                else {
                    $valDiv.html(orgDisplayValue);
                }
            }

            $valDiv.attr('index', orgIndex);
            $valDiv.attr('value', orgValue);
            $valDiv.attr('displayValue', orgDisplayValue);
        } // resetOrginal

        function maxHeightLstEntry($dropDown)
        {
            var h = Math.max.apply(null, $('.lstEntry', $dropDown).map(function ()
                            {
                                return $(this).outerHeight();
                            }).get());

            return h;
        } // maxHeightLstEntry

        function displayDropDown($dropDown) {
            // make sure the dropdown list is in the dom
            $dropDown.show().hide();

            // see if the list should be above or below
            var windowHeight = $(window).height();
            var scrollTop = $('body').scrollTop();
            var footerHeight = $('footer').height();
            var dropHeight = $dropDown.outerHeight();
            var $cntl =  $dropDown.closest('.revupDropDown');
            var cntlHeight = $cntl.outerHeight();
            var cntlWidth  = $cntl.outerWidth();
            var offset = $cntl.offset();
            var position = $cntl.position();

            if ((offset.top + dropHeight + cntlHeight) > (windowHeight - footerHeight + scrollTop)) {
                $dropDown.css('border-bottom-left-radius', 0);
                $dropDown.css('border-bottom-right-radius', 0);

                $dropDown.css('margin-top', -(cntlHeight + dropHeight));
                $dropDown.css('border-top-left-radius', 3);
                $dropDown.css('border-top-right-radius', 3);
                $dropDown.css('border-bottom', '0 none');
            }
            else {
                $dropDown.css('border-top-left-radius', 0);
                $dropDown.css('border-top-right-radius', 0);

                $dropDown.css('margin-top', 0);
                $dropDown.css('border-bottom-left-radius', 3);
                $dropDown.css('border-bottom-right-radius', 3);
                $dropDown.css('border-top', '0 none');
            }

            // display the list
            $dropDown.css('width', (cntlWidth) + 'px');
            $dropDown.css('position', 'absolute');

            if (opts.bAutoDropDownWidth) {
                var bWidth = parseInt($cntl.css('border-width'), 10);

                if (offset.left == position.left && !opts.bAutoDropDownPadding) {
                    $dropDown.css('left', ((offset.left + bWidth) - 1) + 'px')
                    $dropDown.width(cntlWidth - (bWidth * 2));
                }
                else {
                    $dropDown.css('left', position.left);
                }
            }

            $dropDown.show();
            $dropDown.focus();
            $dropDown.css('top', position.top + cntlHeight);

            // scroll selected into view - do after display so list in dom
            var found = $('.lstEntry.selected', $dropDown).index();
            // get the max height of a list item
            var h = maxHeightLstEntry($dropDown);
            $('.scrollSection', $dropDown).scrollTop(found * h);

            // if need fetch the list
            if (opts.lstDataUrl) {
                var $scrollSection = $('.scrollSection', $dropDown);
                $scrollSection.height(150);

                // load the waiting div
                var sTmp = [];
                sTmp.push('<div class="rankingDetailsLstLoading" style="width:100%">');
                    sTmp.push('<div class="loadingDiv" style="margin-top:30px;width:60px;height:60px;margin-left:auto;margin-right:auto;">');
                        sTmp.push('<div class="loading" style="width:60px;height:60px; margin-left:0;left:0"></div>');
                        //sTmp.push('<img class="loadingImg" src="' + loadingImg + '">');
                    sTmp.push('</div>');
                sTmp.push('</div>');
                $scrollSection.html(sTmp.join(''));

                $.ajax({
                    url: opts.lstDataUrl,
                    type: "GET",
                })
                .done(function(r) {
                    if (opts.lstDecodeFunc) {
                        var dLst = opts.lstDecodeFunc(r);

                        // get the index of the item to select
                        var $valDiv = $('.valDiv', $this);
                        var val = $valDiv.attr('value');
                        var selectedIndex = 0;
                        for (var i = 0; i < dLst.length; i++) {
                            if (dLst[i].value == val) {
                                selectedIndex = i;
                                break;
                            }
                        }

                        // adjust height
                        var h = dLst.length * 23;
                        if (h < 150) {
                            $scrollSection.height(h);
                        }

                        $('.scrollSection', $dropDown).html(buildDropDownListEntries(dLst, selectedIndex));
                    }
                })
                .fail(function(r) {
                    console.error("unable to fetch dropDownField data: ", r)
                })
            }
        } // displayDropDown

        // open/close the dropdown list
        $this.on('click', '.cntlBody', function(e) {
            // if disabled ignore
            if (!$this.data('enabled')) {
                e.stopPropagation();

                return;
            }
            // is disabled don't open
            if ($this.hasClass('disabled')) {
                return;
            }

            var $dropLst = $('.dropDownLst', $this);
            if ($dropLst.is(':visible')) {
                $dropLst.hide();
                $overlay.trigger('revup.overlayClose');

                // reset focus
                $dropLst.closest('revupDropDown').focus();
            }
            else {
                // add the overlay
                var p = undefined;
                if ($dropLst.closest('.revupDlg').length > 0) {
                    p = $dropLst.closest('.revupDlg');
                }
                if (p == undefined) {
                    if ($dropLst.closest('.revupConfirmBox').length > 0) {
                        p = $dropLst.closest('.revupConfirmBox');
                    }
                }
                if (p == undefined) {
                    if ($dropLst.closest('.revupMessageBox').length > 0) {
                        p = $dropLst.closest('.revupMessageBox');
                    }
                }
                if (p == undefined) {
                    if ($dropLst.closest('.revupDialogBox').length > 0) {
                        p = $dropLst.closest('.revupDialogBox');
                    }
                    if ($dropLst.closest('.revupPopup').length > 0) {
                        p = $dropLst.closest('.revupPopup');
                    }
                }
                $overlay = $this.overlay(false, opts.overlayZIndex, true, p);
                $this.one('revup.overlayClick', function() {
                    // reset the orginal value
                    resetOrginal($(this));

                    $dropLst.hide();

                    // reset focus
                    $dropLst.closest('revupDropDown').focus();
                });

                // see if there is an overlay and make sure the list z-index is one more than that
                $olay = $('.revupOverlay');
                if ($olay.length > 0) {
                    var z = 0;
                    for (var ii = 0; ii < $olay.length; ii++) {
                        var zz = parseInt($($olay[ii]).css('z-index'), 10) + 1;
                        if (zz > z) {
                            z = zz;
                        }
                    }

                    $dropLst.css('position', 'relative')
                    $dropLst.css('z-index', z + '');
                }

                // display the dropdown list
                displayDropDown($dropLst);

                // get the current value
                var $valDiv = $('.valDiv', $(this));
                orgValue = $valDiv.attr('value');
                orgDisplayValue = $valDiv.attr('displayValue');
                orgIndex = $valDiv.attr('index');
            }
        })

        // click on an entry in the list
        var $entry;
        $($this)
            .on('click', '.dropDownLst .lstEntry', function() {
                $entry = $(this);
                if ($entry.hasClass('selected')) {
                    // close the drop down
                    $('.dropDownLst', $this).hide();
                    $overlay.trigger('revup.overlayClose');

                    // reset focus
                    $('.dropDownLst', $this).closest('.revupDropDown').focus();
                }
                else if (!$entry.hasClass('sectionHeader')) {
                    if ($this.triggerHandler('revup.beforeDropDownSelect') != true) {
                        $this.trigger('revup.dropDownOkToSelect', true)
                    }
                }
            })
            .on('revup.dropDownOkToSelect', function(e, bSelect) {
                if (bSelect) {
                    // not selected
                    var val = $entry.html();
                    var index = $entry.attr('index');
                    var value = $entry.attr('value');
                    var displayValue = $entry.attr('displayValue');

                    // change selected
                    $('.dropDownLst .lstEntry', $this).removeClass('selected');
                    $entry.addClass('selected');

                    // change the value
                    if (opts.bDisplayValue) {
                        $('.valDiv', $this).html(value);
                    }
                    else {
                        $('.valDiv', $this).html(val);
                    }
                    $('.valDiv', $this).attr('index', index);
                    $('.valDiv', $this).attr('value', value);
                    $('.valDiv', $this).attr('displayValue', displayValue);

                    // trigger select
                    $this.trigger({
                        type: 'revup.dropDownSelected',
                        dropDownValue: value,
                        dropDownDisplayValue: displayValue,
                        dropDownIndex: index,
                    });

                    // call the user function
                    if ((opts.changeFunc != undefined) && (opts.changeFunc != null)) {
                        var $cntl = $this;
                        opts.changeFunc(index, value, displayValue, $cntl);
                    }
                }

                // close the drop down
                $('.dropDownLst', $this).hide();
                $overlay.trigger('revup.overlayClose');

                // reset focus
                $('.dropDownLst', $this).closest('revupDropDown').focus();
            })



        /*
        At this time don't limit the typing to just the add new input
        $('.dropDownAddNewValue', $this).on('keydown', function(e) {
            e.stopPropagation();
        })
        */

        // key typed
        $this.attr('tabindex', -1)
        $this.on('focusin', function(e) {
            $('.revupDropDown', $this).addClass('hasFocus');

            // see of the list is displayed and index is -1 and there is addNewValue
            let $lst = $('.dropDownLst .scrollSection', $(this));
            let $lstEnties = $('.lstEntry', $lst);
            let selectIndex = $lstEnties.index($('.selected', $lst));
            let $dropDownLst = $('.dropDownLst', $(this));
            if ($dropDownLst.is(':visible') && opts.addNewValue && selectIndex == -1) {
                $('.dropDownLst .dropDownAddNewValue', $(this)).select();
            }
        })

        $('.dropDownLst .dropDownAddNewValue', $this).on('focusout', function(e) {
            e.stopImmediatePropagation();
            e.preventDefault();

            $('.dropDownLst', $this.parent()).focus();
        })

        $this.on('focusout', function(e) {
            // close the drop down
            if (!$('.dropDownLst', $this).is(':visible')) {
                $('.dropDownLst', $this).hide();

                if ($('.valDiv', $this).attr('index') == '-1') {
                    $this.closest('.entry').addClass('has-error');
                }


                if ($overlay != undefined && $overlay.length > 0) {
                    $overlay.trigger('revup.overlayClose');
                }
            }

            $('.revupDropDown', $this).removeClass('hasFocus');
        })
        $this.on('keydown keyup', function(e) {
            // if disabled ignore
            if (!$this.data('enabled')) {
                e.preventDefault();
                return false;
            }

            if (e.type == "keydown") {
                var $lst = $('.dropDownLst .scrollSection', $(this));
                var $dropdownLst = $('.dropDownLst', $(this));
                var $lstEnties = $('.lstEntry', $lst);
                var selectIndex = $lstEnties.index($('.selected', $lst));
                if ((e.which == 38) || (e.which == 40)) {
                    // up / down arrow
                    // if not visible make visible
                    let bLstDisplayed = true;
                    if (!$lst.is(':visible')) {
                        // add the overlay
                        $overlay = $this.overlay(false, "auto", true);
                        $this.one('revup.overlayClick', function() {
                            // reset the orginal value
                            resetOrginal($(this));

                            // hide the list
                            $dropdownLst.hide();

                            // reset focus
                            $this.closest('.revupDropDown').focus();
                        })

                        // get the current value
                        var $valDiv = $('.valDiv', $(this));
                        orgValue = $valDiv.attr('value');
                        orgDisplayValue = $valDiv.attr('displayValue');
                        orgIndex = $valDiv.attr('index');

                        // display the list
                        displayDropDown($('.dropDownLst', $(this)));

                        bLstDisplayed = false;
                    }

                    // arrow up/down 38/40
                    var newIndex = selectIndex;
                    if (bLstDisplayed) {
                        if (e.which == 38) {
                            newIndex--;
                        }
                        else {
                            newIndex++;
                        }
                    }

                    // see if there is an add new value
                    if (opts.addNewValue) {
                        if (newIndex == -1) {
                            $('.dropDownLst .dropDownAddNewValue', $(this)).select();
                        }
                        else {
                            $('.dropDownLst .dropDownAddNewValue', $(this)).blur();
                        }
                    }

                    // make sure in range
                    if ((newIndex < 0) || (newIndex >= $lstEnties.length)) {
                        e.preventDefault();
                        return;
                    }

                    // get the selectors
                    var $currentEntry = $($lstEnties[selectIndex]);
                    var $newEntry = $($lstEnties[newIndex]);

                    // unselect the current and select the new
                    // change selected
                    $currentEntry.removeClass('selected');
                    $newEntry.addClass('selected');

                    // change the value
                    if (opts.bDisplayValue) {
                        $('.valDiv', $this).html($newEntry.attr('value'));
                    }
                    else {
                        $('.valDiv', $this).html($newEntry.html());
                    }
                    $('.valDiv', $this).attr('index', newIndex);
                    $('.valDiv', $this).attr('value', $newEntry.attr('value'));
                    $('.valDiv', $this).attr('displayValue', $newEntry.attr('displayValue'));

                    // scroll into view
                    var h = maxHeightLstEntry($lst);
                    $lst.animate({scrollTop: (newIndex * h)}, 'fast');

                    e.preventDefault();
                    return false;
                }
                else if (e.which == 13) {
                    // return key

                    // See if the list is visible, and if not visible,
                    // match the native HTML5 behavior and prevent the user
                    // from opening the list by pressing enter. Only the
                    // arrow keys can open the list.
                    if ($lst.is(':visible')) {
                        if (opts.addNewValue) {
                            var v = $('.dropDownAddNewValue', $this).val();
                            var $entry;
                            var index;
                            var val;
                            var displayVal;
                            if (v != '') {
                                // see if the value is in the current list
                                var isFound = false;
                                for(var i = 0; i < $lstEnties.length; i++) {
                                    $entry = $($lstEnties[i]);
                                    var cVal = $entry.attr('displayValue');
                                    if (cVal == v) {
                                        isFound = true;
                                        index = i;
                                        val = $entry.attr('value');
                                        displayVal = $entry.attr('displayValue');

                                        break;
                                    }
                                }

                                // if in the list then select it
                                if (!isFound) {
                                    // add to the list
                                    index = $('.lstEntry', $lst).length;
                                    val = v;
                                    displayVal = v;
                                    var sTmp = '<div style="z-index:11010;" class="lstEntry selected" index="' + index + '" value="' + v + '" displayValue="' + v + '">' + v + '</div>';

                                    // add to the list
                                    if (opts.sortList) {
                                        var $entry;
                                        var isBefore = false;
                                        for(var i = 0; i < $lstEnties.length; i++) {
                                            $entry = $($lstEnties[i]);
                                            var cVal = $entry.attr('displayValue');
                                            if (v < cVal) {
                                                isBefore = true;
                                                break;
                                            }
                                        }

                                        // add after
                                        if (isBefore) {
                                            $(sTmp).insertBefore($entry);
                                        }
                                        else {
                                            $(sTmp).insertAfter($entry);
                                        }
                                    }
                                    else {
                                        $lst.append(sTmp);
                                    }
                                }

                                // clear current selection and set new
                                var $currentEntry = $($lstEnties[selectIndex]);
                                $currentEntry.removeClass('selected');

                                // change the value
                                $('.valDiv', $this).html(val);
                                $('.valDiv', $this).attr('index', index);
                                $('.valDiv', $this).attr('value', val);
                                $('.valDiv', $this).attr('displayValue', displayVal);

                                // scroll into view
                                var h = maxHeightLstEntry($lst);
                                var selectIndex = $lstEnties.index($('.selected', $lst));
                                $lst.animate({scrollTop: (selectIndex * h)}, 'fast');

                                // clear the edit field
                                $('.dropDownAddNewValue', $this).val("");
                            }
                        }

                        // return
                        // get the control value
                        var cValue = $('.valDiv', $this).attr('value');
                        var cDisplayValue = $('.valDiv', $this).attr('displaValue');
                        var cIndex = $('.valDiv', $this).attr('index');

                        // trigger select
                        $this.trigger({
                            type: 'revup.dropDownSelected',
                            dropDownValue: cValue,
                            dropDownDisplayValue: cDisplayValue,
                            dropDownIndex: cIndex,
                        });

                        // if different call the user function
                        if ((cValue != orgValue) || (cIndex != orgIndex)) {
                            if ((opts.changeFunc != undefined) && (opts.changeFunc != null)) {
                                var $cntl = $this;
                                opts.changeFunc(cIndex, cValue, cDisplayValue, $cntl);
                            }
                        }

                        // close the dropdown
                        $dropdownLst.hide();
                        $overlay.trigger('revup.overlayClose');
                        $lst.closest('.revupDropDown').focus();

                        e.preventDefault();
                        return false;
                    }
                    else {
                        return;
                    }
                }
                else if (e.which == 27) {
                    // esc key
                    // reset the orginal value
                    resetOrginal($this);

                    // close the dropdown
                    $dropdownLst.hide();
                    $overlay.trigger('revup.overlayClose');
                    $lst.closest('.revupDropDown').focus();

                    e.preventDefault();
                    return false;
                }
                else if ((e.which >= 65) && (e.which <= 90)) {
                    // a through z
                    // if the control key, alt key or meta key are press return
                    if (e.altKey || e.ctrlKey || e.metaKey) {
                        // do the default thing
                        return;
                    }

                    // if not visible make visible
                    if (!$lst.is(':visible')) {
                        // add the overlay
                        $overlay = $this.overlay(false, opts.overlayZIndex, true);
                        $this.one('revup.overlayClick', function() {
                            // reset the orginal value
                            resetOrginal($this);

                            $dropdownLst.hide();
                            $lst.closest('revupDropDown').focus();
                        })

                        // display the list
                        displayDropDown($('.dropDownLst', $this));

                        // get the current value
                        var $valDiv = $('.valDiv', $this);
                        orgValue = $valDiv.attr('value');
                        orgDisplayValue = $valDiv.attr('displayValue');
                        orgIndex = $valDiv.attr('index');
                    }

                    // make sure the charactor is lower case
                    var c = String.fromCharCode(e.which);
                    c = c.toLowerCase();

                    // get the current value
                    var found = -1;
                    var currentValue = $($lstEnties[selectIndex]).attr('value');

                    // see if the letter type is the same as the current selected
                    let currentFirst = '';
                    if (currentValue && currentValue != '') {
                        currentFirst = currentValue.charAt(0);
                    }
                    currentFirst = currentFirst.toLowerCase();
                    if (currentFirst == c) {
                        // walk down the list looking for the next entry to start
                        // with that lettler, may looking until hit the index again
                        var index = selectIndex + 1;
                        var len = $lstEnties.length;
                        var lastIndex = len;
                        for (var ii = 0; ii < len; ii++) {
                            // end of the list start over
                            if (index >= lastIndex) {
                                index = 0;
                            }

                            // if hit the starting point break
                            if (index == selectIndex) {
                                break;
                            }

                            // see if the first letter matches
                            var v = $($lstEnties[index]).attr('value');
                            var vStart = v.charAt(0);
                            vStart = vStart.toLowerCase();

                            if (vStart == c) {
                                found = index;
                                break;
                            }

                            index++;
                        }

                    }
                    else {
                        // find the first  value starting with this letter;
                        for (var ii = 0; ii < $lstEnties.length; ii++) {
                            var v = $($lstEnties[ii]).attr('value');
                            var vStart = v.charAt(0);
                            vStart = vStart.toLowerCase();

                            if (vStart == c) {
                                found = ii;
                                break;
                            }
                        }
                    }

                    if (found != -1) {
                        // get the selectors
                        var $currentEntry = $($lstEnties[selectIndex]);
                        var $newEntry = $($lstEnties[found]);

                        // unselect the current and select the new
                        // change selected
                        $currentEntry.removeClass('selected');
                        $newEntry.addClass('selected');

                        // change the value
                        if (opts.bDisplayValue) {
                            $('.valDiv', $this).html($newEntry.attr('value'));
                        }
                        else {
                            $('.valDiv', $this).html($newEntry.html());
                        }
                        $('.valDiv', $this).attr('index', newIndex);
                        $('.valDiv', $this).attr('value', $newEntry.attr('value'));
                        $('.valDiv', $this).attr('displayValue', $newEntry.attr('displayValue'));

                        // scroll into view
                        var h = $('.dropDownLst .lstEntry', $this).outerHeight();
                        $('.dropDownLst .scrollSection', $this).animate({scrollTop: (found * h)}, 'fast');
                    }
                }
                else if (e.which == 9) {
                    // tab key

                    // If the list is visible, match the native HTML5 behavior and prevent the user
                    // from tabbing away until they have made a selection or press escape.
                    if ($dropdownLst.is(":visible")) {
                        return false;
                    }
                }
            }
            else if (e.type == 'keyup') {
                if ($.inArray(e.which, [13, 27, 38, 40])) {
                    e.preventDefault();
                    return false;
                }
            }
        });

        // save some vales with the control
        var $dropDownCntl = $('.revupDropDown', $this);
        $dropDownCntl.data('valueStartIndex', opts.valueStartIndex);
        $dropDownCntl.data('ifStartIndexNeg1Msg', opts.ifStartIndexNeg1Msg);
        $dropDownCntl.data('dropDownLst', valueLst);
        $dropDownCntl.data('bDisplayValue', opts.bDisplayValue);

        return this;
    }; // revupDropDownField


    jQuery.fn.revupDropDownField.defaults = {
        value:              [10, 20, 30, 40, 50],
        valueStartIndex:    0,
        sortList:           true,                   // if the values in the drop down list sorted at the beginning
        sortDescending:     false,                  // search order
        addNewValue:        false,                  // allow the user to add an new value that is not in the list
        updateValueList:    false,                  // replace the value list and active value
                                                    // if addNewValue clear that out
        bDisplayValue:      false,                  // only display the value when selected, not display value
        bAutoDropDownWidth: true,                   // compute the width to be the same size as the control
        bAutoDropDownPadding: false,                // pad the width of the drop down
        ifStartIndexNeg1Msg:"",                     // message string to display if start index -1
        overlayZIndex:      "auto",                 // the z-index of the overlay
        changeFunc:         null,                   // function to call when scrolling stops
                                                    // the functions is call with 3 parameters
                                                    //      index -> index of the value
                                                    //      value -> value displayed
                                                    //      displayValue -> display value
        bDisplaySectionHeaders: false,              // display section headers
        lstDataUrl:         '',                     // url to fetch data from (only if not blank)
        lstRefreshAfter:    300000,                 // millisecond to age fetched data (default 5 minutes)
        lstDecodeFunc:      null                   // function to decode data
    }; // revupDropDownField.defaults


    /*
     *
     * Revup - search box
     *   This is a stylized search box used throughout the product.  When the user hits return an event
     *   is triggered with the search value.  There is also an event triggered when the search box is
     *   cleared.
     *
     */
    jQuery.fn.revupSearchBox = function(options, arg1, arg2)
    {
        if (typeof options == 'string') {
            var command = options;
            var $searchDiv = $(this);

            switch (command) {
                case 'getValue':
                    // get the current value
                    var $valDiv = $('.revupSearchBox .searchText', $searchDiv);
                    var v = $valDiv.val();

                    return v;

                    break;
                case 'setValue':
                    var $valDiv = $('.revupSearchBox .searchText', $searchDiv);
                    $valDiv.val(arg1);
                    break;
                case 'enable':
                    $('.revupSearchBox', $searchDiv).removeClass('disabled');
                    $('.revupSearchBox .searchText', $searchDiv).prop('disabled', false);
                    break;
                case 'disable':
                    $('.revupSearchBox', $searchDiv).addClass('disabled');
                    $('.revupSearchBox .searchText', $searchDiv).prop('disabled', true);
                    break;
            }

            return true;
        }

        // get the options, if any
        var opts = $.extend({}, jQuery.fn.revupSearchBox.defaults, options);

        var $this = $(this);

        // compute the input width
        var inputWidth = 2;
        if (opts.showSearchIcon) {
            inputWidth += 14;
        }
        if (opts.showClearIcon) {
            inputWidth += 14;
        }
        inputWidth = "calc(100% - " + inputWidth + "px)";
        var style = 'style="width:'+ inputWidth + ';"';

        // build the control
        var sTmp = [];
        sTmp.push('<div class="revupSearchBox">');
            if (opts.showSearchIcon) {
                sTmp.push('<div class="searchIcon icon icon-search"></div>');
            }

            var placeholder
            if (opts.placeholderText != '') {
                placeholder = ' placeholder="' + opts.placeholderText + '"';
            }
            sTmp.push('<input class="searchText" ' + style + ' type="text"' + placeholder +
                      ' autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"' +
                      ' value="' + opts.value +'">');

            if (opts.showClearIcon) {
                sTmp.push('<div class="clearIcon icon icon-close"></div>')
            }
        sTmp.push('</div>');
        $this.html(sTmp.join(''));

        var $input = $('.searchText', $this);

        // the event handlers
        if (opts.showClearIcon) {
            $('.clearIcon', $this).on('click', function(e) {
                // clear
                $input.val('');

                // trigger message
                $this.trigger('revup.searchClear')
            });
        }

        if (opts.showSearchIcon) {
            $('.searchIcon', $this).on('click', function(e) {
                // get the value
                var v = $input.val();

                // trigger event
                $this.trigger({
                    type: "revup.searchFor",
                    searchValue: v
                })

            })
        }

        // enter pressed
        if (opts.triggerAsTyping) {
            $input.on('keyup', function(e) {
                var v = $input.val();

                if (e.which == 13) {
                    // trigger event
                    $this.trigger({
                        type: "revup.searchFor",
                        searchValue: v
                    })
                }
                else {
                    // trigger event
                    $this.trigger({
                        type: "revup.searchValue",
                        searchValue: v
                    })
                }
            })
        }
        else {
            $input.on('keypress', function(e) {
                if (e.which == 13) {
                    // get the value
                    var v = $input.val();

                    // trigger event
                    $this.trigger({
                        type: "revup.searchFor",
                        searchValue: v
                    })
                }
            });
        }

        return this;
    }; // revupSearchBox

    jQuery.fn.revupSearchBox.defaults = {
        value:                  "",         // initial value to display in the search box
        showSearchIcon:         true,       // display the magnify glass icon
        showClearIcon:          true,       // display the clear 'x' icon
        placeholderText:        "Search",   // placeholder text
        triggerAsTyping:        false,      // trigger an event each time a character is pressed
    }; // revupSearchBox.defaults

    /*
     *
     * Revup message box
     *
     */
    jQuery.fn.revupMessageBox = function(options)
    {
        // get the options, if any
        var opts = $.extend({}, jQuery.fn.revupMessageBox.defaults, options);

        var $this = $(this);

        var extraClass = opts.extraClass != "" ? " " + opts.extraClass : "";
        var extraStyle = "";
        if (opts.width != '') {
            extraStyle += "width:" + opts.width + ';';
        }
        if (opts.height != '') {
            extraStyle += "height:" + opts.height + ';';
        }
        if (opts.zIndex != '') {
            extraStyle += "z-index:" + opts.zIndex + ';';
        }
        // build the box
        var sTmp = [];
        sTmp.push('<div class="revupMessageBox' + extraClass + '" style="' + extraStyle + '">');
            sTmp.push('<div class="headerDiv">');
                sTmp.push('<div class="headerText">')
                    sTmp.push(opts.headerText);
                sTmp.push('</div>');
            sTmp.push('</div>');

            sTmp.push('<div class="msgBodyDiv">');
                sTmp.push('<div class="msgBody">');
                    sTmp.push(opts.msg);
                sTmp.push('</div>');
            sTmp.push('</div>');

            sTmp.push('<div class="btnDiv">');
                sTmp.push('<div class="btnContainerRight">');
                    sTmp.push('<button class="revupBtnDefault okBtn">');
                        sTmp.push(opts.okBtnText);
                    sTmp.push('</button>');
                sTmp.push("</div>");
            sTmp.push("</div>");
        sTmp.push('</div>')

        // see if part of something else
        var bInsideOther = false;
        if ($('.revupOverlay').length >= 1) {
            bInsideOther = true;
        }

        // create the overlay
        var $overlay = $this.overlay(true, opts.zIndex - 1);

        // display the modal
        $msgBox = $(sTmp.join('')).appendTo('body');

        if ((opts.escClose)  && (!bInsideOther)) {
            $(document).on('keydown', function(e) {
                if (e.keyCode == 27) {
                    $('.cancelBtn', $confirm).trigger('click');

                    e.stopPropagation();
                    e.preventDefault();
                }
            })
        }

        // add the click handlers
        $('.okBtn', $msgBox).on('click', function(e) {
            // close the nessage box
            $msgBox.remove();

            // turn off keydown
            if (!bInsideOther) {
                $(document).off('keydown');
            }

            // close the overlay
            $overlay.trigger('revup.overlayClose');

            // trigger the event
            $this.trigger('revup.messageBoxClose');
        });

        if (opts.isDragable) {
            // change the cursor of the header
            $('.headerDiv', $msgBox).css('cursor', "move");

            var bMouseDown = false;
            var offsetX = 0;
            var offsetY = 0;
            $('.headerDiv', $msgBox)
                .on('mousedown', function(e) {
                    bMouseDown = true;

                    offsetX = e.offsetX;
                    offsetY = e.offsetY;
                })
                .on('mouseup', function(e) {
                    bMouseDown = false;
                })
                .on('mouseleave', function(e) {
                    bMouseDown = false;
                })
                .on('mousemove', function(e) {
                    if (bMouseDown) {
                        var x = e.pageX - offsetX;
                        var y = e.pageY - offsetY;

                        $msgBox.offset({left: x, top: y})
                    }
                })
        }

        // position
        var scrollTop = $(document).scrollTop();
        var pos = $msgBox.position();
        var l = ($(document).width() - $msgBox.width()) / 2;
        $msgBox.css('left', l).css('top', pos.top + scrollTop);

        return this;
    }; // revupMessageBox

    jQuery.fn.revupMessageBox.defaults = {
        okBtnText:          'OK',                   // text for the ok button
        headerText:         'Message Box',     // the title of the confirmation box
        msg:                'This is a message and only a message',
        zIndex:             25,                     // z index of the message box
        escClose:           true,                   // close if the esc key is pressed
        width:              '',                     // width of the message box -  - option otherwise in class
        height:             '',                     // height of the message box - option otherwise in class
        extraClass:         '',                     // extra class to add to the message box
        isDragable:         true,                   // can the box me moved
    }; // revupMessageBox - defaults


    /*
     *
     * RevUp confirmation box
     *
     */
    jQuery.fn.revupConfirmBox = function(options)
    {
        if (typeof options == "string") {
            var command = options;
            var $dlg = $(this);

            switch (command) {
                case 'close':
                    // close the dialog box
                    var $overlay = $dlg.data('overlay');
                    $dlg.remove();

                    // cancel the esc close
                    $(document).off('keydown');

                    // close the overlay
                    if ($overlay) {
                        $overlay.trigger('revup.overlayClose');
                    }

                    break;
            } // end switch -- command

            return;
        }

        // get the options, if any
        var opts = $.extend({}, jQuery.fn.revupConfirmBox.defaults, options);

        var $this = $(this);

        // build the box
        var sTmp = [];
        var enableOkBtn = opts.enableOkBtn ? "" : " disabled"
        var extraClass = opts.extraClass ? " " + opts.extraClass : "";
        sTmp.push('<div class="revupConfirmBox' + extraClass + '" style="z-index:' + opts.zIndex + '">');
            sTmp.push('<div class="headerDiv">');
                sTmp.push('<div class="headerText">')
                    sTmp.push(opts.headerText);
                sTmp.push('</div>');
            sTmp.push('</div>');

            sTmp.push('<div class="msgBodyDiv">');
                sTmp.push('<div class="msgBody">');
                    sTmp.push(opts.msg);
                sTmp.push('</div>');
            sTmp.push('</div>');

            sTmp.push('<div class="btnDiv">');
                sTmp.push('<div class="btnContainerRight">');
                    sTmp.push('<div class="cancelBtn revupLnk">');
                        sTmp.push(opts.cancelBtnText);
                    sTmp.push('</div>');

                    if (opts.midBtnText != '') {
                        sTmp.push('<button class="revupBtnDefault secondary midBtn' + enableOkBtn + '">');
                            sTmp.push(opts.midBtnText);
                        sTmp.push('</button>');
                    }

                    sTmp.push('<button class="revupBtnDefault okBtn' + enableOkBtn + '">');
                        sTmp.push(opts.okBtnText);
                    sTmp.push('</button>');
                sTmp.push("</div>");
            sTmp.push("</div>");
        sTmp.push('</div>')

        // see if part of something else
        var bInsideOther = false;
        if ($('.revupOverlay').length >= 1) {
            bInsideOther = true;
        }

        // create the overlay
        var $overlay = $this.overlay(true, opts.zIndex - 1);

        // display the modal
        $confirm = $(sTmp.join('')).appendTo('body');

        // save with the dialog
        $confirm.data('overlay', $overlay);

        // handle the close event
        $confirm.on('revup.confirmClose', function() {
            // close the confirm box
            $confirm.remove();

            // turn off keydown
            if (!bInsideOther) {
                $(document).off('keydown');
            }

            // close the overlay
            $overlay.trigger('revup.overlayClose');
        })

        if ((opts.escClose)  && (!bInsideOther)) {
            $(document).on('keydown', function(e) {
                if (e.keyCode == 27) {
                    $('.cancelBtn', $confirm).trigger('click');

                    e.stopPropagation();
                    e.preventDefault();
                }
            })
        }

        // add the click handlers
        $('.cancelBtn', $confirm).on('click', function(e) {
            // close the confirm box
            $confirm.remove();

            // turn off keydown
            if (!bInsideOther) {
                $(document).off('keydown');
            }

            // close the overlay
            $overlay.trigger('revup.overlayClose');

            // trigger the event
            $this.trigger('revup.confirmCancel');

            // call the handler if there is one
            if (opts.cancelHandler != undefined) {
                opts.cancelHandler();
            }
        })

        $('.midBtn', $confirm).on('click', function(e) {
            if (opts.autoCloseMid) {
                // close the confirm box
                $confirm.remove();

                // turn off keydown
                if (!bInsideOther) {
                    $(document).off('keydown');
                }

                // close the overlay
                $overlay.trigger('revup.overlayClose');
            }

            // trigger the event
            $this.trigger('revup.confirmMid');

            // call the handler if there is one
            if (opts.midHandler != undefined) {
                opts.midHandler();
            }
        });

        $('.okBtn', $confirm).on('click', function(e) {
            if (opts.autoCloseOk) {
                // close the confirm box
                $confirm.remove();

                // turn off keydown
                if (!bInsideOther) {
                    $(document).off('keydown');
                }

                // close the overlay
                $overlay.trigger('revup.overlayClose');
            }

            // trigger the event
            $this.trigger('revup.confirmOK');

            // call the handler if there is one
            if (opts.okHandler != undefined) {
                opts.okHandler();
            }
        });

        if (opts.isDragable) {
            // change the cursor of the header
            $('.headerDiv', $confirm).css('cursor', "move");

            var bMouseDown = false;
            var offsetX = 0;
            var offsetY = 0;
            $('.headerDiv', $confirm)
                .on('mousedown', function(e) {
                    bMouseDown = true;

                    offsetX = e.offsetX;
                    offsetY = e.offsetY;
                })
                .on('mouseup', function(e) {
                    bMouseDown = false;
                })
                .on('mouseleave', function(e) {
                    bMouseDown = false;
                })
                .on('mousemove', function(e) {
                    if (bMouseDown) {
                        var x = e.pageX - offsetX;
                        var y = e.pageY - offsetY;

                        $confirm.offset({left: x, top: y})
                    }
                })
        }

        // position
        var scrollTop = $(document).scrollTop();
        var pos = $confirm.position();
        var l = ($(document).width() - $confirm.width()) / 2;
        $confirm.css('left', l).css('top', pos.top + scrollTop);

        return $confirm;
    }; // revupConfirmBox

    jQuery.fn.revupConfirmBox.defaults = {
        okBtnText:          'OK',                   // text for the ok button
        midBtnText:         '',                     // text for the mid button
        cancelBtnText:      'Cancel',               // text for the cancel button
        escClose:           true,                   // should the esc key cancel the confirm box
        headerText:         'Confirmation Box',     // the title of the confirmation box
        msg:                'Are you sure you want to do this',
        extraClass:         '',                     // extra class to apply to the dialog
        zIndex:             20,                     // z index of the message box
        isDragable:         true,                   // can the box me moved
        autoCloseOk:        true,                   // automaticly close the confirm box when the ok button is pressed
        autoCloseMid:       true,                   // automaticly close the confirm box when the mid button is pressed
        enableOkBtn:        true,                   // is the ok button enable when displayed
        okHandler:          undefined,              // handler for the ok button
        midHandler:         undefined,              // handler for the mid button
        cancelHandler:      undefined,              // handler for the cancel button
    }

    /*
     *
     * RevUp dialog box
     *
     */
//    var $revupDialgBoxOverlay = undefined;       // selector for overlay
    jQuery.fn.revupDialogBox = function(options, arg1, arg2)
    {
        // handle command
        if (typeof options == "string") {
            var command = options;
            var $dlg = $(this);

            switch (command) {
                case 'close':
                    // close the dialog box
                    var $overlay = $dlg.data('overlay');
                    $dlg.remove();

                    // cancel the esc close
                    $(document).off('keydown');

                    // close the overlay
                    if ($overlay) {
                        $overlay.trigger('revup.overlayClose');
                    }

                    break;
                case 'enableBtn':
                    // enable a button
                    var $btn = $('.' + arg1, $dlg);
                    if ($btn.is("button")) {
                        $btn.prop('disabled', false);
                    }
                    else if ($btn.is('div')) {
                        $btn.removeClass('disabled')
                    }
                    break;
                case 'disableBtn':
                    // disable a button
                    var $btn = $('.' + arg1, $dlg);
                    if ($btn.is("button")) {
                        $btn.prop('disabled', true);
                    }
                    else if ($btn.is('div')) {
                        $btn.addClass('disabled')
                    }
                    break;
                case 'showWaiting':
                    $('.waitingOverlayDiv', $dlg).show();
                    break;
                case 'hideWaiting':
                    $('.waitingOverlayDiv', $dlg).hide();
                    break;
            } // end switch - command

            return this;
        }

        // get the options, if any
        var opts = $.extend({}, jQuery.fn.revupDialogBox.defaults, options);
        var $revupDialgBoxOverlay = undefined;       // selector for overlay

        var $this = $(this);

        // build the box
        var sTmp = [];
        var enableOkBtn = opts.enableOkBtn ? "" : " disabled"
        var extraClass = opts.extraClass != "" ? " " + opts.extraClass : "";
        var extraStyle = "";
        var msgBodyHeight = 0;
        if (opts.width != '') {
            extraStyle += "width:" + opts.width + ';';
        }
        if (opts.height != '') {
            extraStyle += "height:" + opts.height + ';';

            // if provided compute the height of the msgBody
            var msgBodyHeight = parseInt(opts.height, 10);
        }
        if (opts.zIndex != '') {
            extraStyle += "z-index:" + opts.zIndex + ';';
        }

        // adjust the message body height
        if (opts.headerText != '') {
            msgBodyHeight -= 30;
        }
        if ((opts.bHelpIcon) || (opts.cancelBtnText != '') || (opts.okBtnText != '')) {
            msgBodyHeight -= 50;
        }

        sTmp.push('<div class="revupDialogBox' + extraClass + '" style="' + extraStyle + '">');
            if (opts.waitingOverlay) {
                let waitingStyle = 'display:none'
                if (opts.waitingStyleShow) {
                    waitingStyle = '';
                }
                sTmp.push('<div class="waitingOverlayDiv" style="' + waitingStyle + '">');
                    sTmp.push('<div class="loadingDiv">');
                        sTmp.push('<div class="loading"></div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            }
            if (opts.headerText != '') {
                sTmp.push('<div class="headerDiv">');
                    sTmp.push('<div class="headerText">')
                        sTmp.push(opts.headerText);
                    sTmp.push('</div>');
                sTmp.push('</div>');
            }

            var msgBodyDivStyle = "";
            var msgBodyStyle = "";
            if (msgBodyHeight != 0) {
                msgBodyDivStyle = ' style="height:' + (msgBodyHeight) + 'px;"'
                msgBodyStyle = ' style="height:' + (msgBodyHeight) + 'px;"'
            }
            sTmp.push('<div class="msgBodyDiv"' + msgBodyDivStyle + '>');
                sTmp.push('<div class="msgBody"' + msgBodyStyle + '>');
                    if (msgBodyHeight != 0) {
                        sTmp.push('<div class="msgBodyScroll">')
                    }

                    sTmp.push(opts.msg);

                    if (msgBodyHeight != 0) {
                        sTmp.push('</div>')
                    }

                sTmp.push('</div>');
            sTmp.push('</div>');

            if ((opts.bHelpIcon) || (opts.cancelBtnText != '') || (opts.okBtnText != '') || ((opts.bDisplayContinueBtn) && (opts.continueBtnText != ''))) {
                sTmp.push('<div class="btnDiv">');
                    sTmp.push('<div class="btnContainerLeft">');
                        if (opts.bHelpIcon) {
                            sTmp.push('<img class="helpBtn helpIcon" src="/static/img/getting-started-icon-blue.svg">')
                        }
                    sTmp.push('</div>');

                    sTmp.push('<div class="btnContainerRight">');
                        if (opts.cancelBtnText != '') {
                            sTmp.push('<div class="cancelBtn revupLnk">');
                                sTmp.push(opts.cancelBtnText);
                            sTmp.push('</div>');
                        }

                        if (opts.midBtn1Text != '') {
                            var enableBtn = opts.midBtn1Enable ? "" : " disabled"
                            if (opts.midBtn1Type.toLowerCase() == 'button') {
                                var btnStyle = opts.midBtn1Style == '' ? 'revupBtnDefault secondary' : opts.midBtn1Style;
                                sTmp.push('<button class="' + btnStyle + ' midBtn midBtn1"' + enableBtn + '>');
                                    sTmp.push(opts.midBtn1Text);
                                sTmp.push('</button>');
                            }
                            else if (opts.midBtn1Type.toLowerCase() == 'anchor') {
                                sTmp.push('<div class="midBtn1 midAnchorBtn' + enableBtn + '">');
                                    sTmp.push(opts.midBtn1Text);
                                sTmp.push('</div>');
                            }
                        }

                        if (opts.midBtn2Text != '') {
                            var enableBtn = opts.midBtn2Enable ? "" : " disabled"
                            if (opts.midBtn2Type.toLowerCase() == 'button') {
                                var btnStyle = opts.midBtn2Style == '' ? 'revupBtnDefault secondary' : opts.midBtn2Style;
                                sTmp.push('<button class="' + btnStyle + ' midBtn midBtn2"' + enableBtn + '>');
                                    sTmp.push(opts.midBtn2Text);
                                sTmp.push('</button>');
                            }
                            else if (opts.midBtn2Type.toLowerCase() == 'anchor') {
                                sTmp.push('<div class="midBtn2 midAnchorBtn' + enableBtn + '">');
                                    sTmp.push(opts.midBtn2Text);
                                sTmp.push('</div>');
                            }
                        }

                        if (opts.midBtn3Text != '') {
                            var enableBtn = opts.midBtn3Enable ? "" : " disabled"
                            if (opts.midBtn3Type.toLowerCase() == 'button') {
                                var btnStyle = opts.midBtn3Style == '' ? 'revupBtnDefault secondary' : opts.midBtn1Style;
                                sTmp.push('<button class="' + btnStyle + ' midBtn midBtn3"' + enableBtn + '>');
                                    sTmp.push(opts.midBtn3Text);
                                sTmp.push('</button>');
                            }
                            else if (opts.midBtn3Type.toLowerCase() == 'anchor') {
                                sTmp.push('<div class="midBtn3 midAnchorBtn' + enableBtn + '">');
                                    sTmp.push(opts.midBtn3Text);
                                sTmp.push('</div>');
                            }
                        }

                        if ((opts.bDisplayContinueBtn) && (opts.continueBtnText != "")) {
                            sTmp.push('<div class="continueBtn">');
                                sTmp.push(opts.continueBtnText);
                            sTmp.push('</div>');
                        }

                        if (opts.okBtnText != '') {
                            sTmp.push('<button class="revupBtnDefault okBtn"' + enableOkBtn + '>');
                                sTmp.push(opts.okBtnText);
                            sTmp.push('</button>');
                        }
                    sTmp.push("</div>");
                sTmp.push("</div>");
            }
        sTmp.push('</div>')

        // create the overlay
        $revupDialgBoxOverlay = $this.overlay(true, opts.zIndex - 1);

        // display the modal
        $dlg = $(sTmp.join('')).appendTo('body');

        // save with the dialog
        $dlg.data('overlay', $revupDialgBoxOverlay);

        // handle the close event
        $dlg.on('revup.dialogClose', function() {
            // close the dialog box
            $dlg.remove();

            // cancel the esc close
            $(document).off('keydown');

            // close the overlay
            $revupDialgBoxOverlay.trigger('revup.overlayClose');
        })

        if (opts.escClose) {
            $(document).on('keydown', function(e) {
                // esc key
                if (e.keyCode == 27) {
                    // see if there is a confirm dialog
                    var $confDlg = $('.revupConfirmBox');
                    if ($confDlg.length >= 1) {
                        $confDlg.trigger('revup.confirmClose');
                        return;
                    }

                    // call the handler if there is one - done this way just encase no
                    // cancel button
                    if (opts.cancelHandler != undefined) {
                        opts.cancelHandler();
                    }

                    if (opts.autoCloseCancel) {
                        // close the dialog box
                        $dlg.remove();

                        // cancel the esc close
                        $(document).off('keydown');

                        // close the overlay
                        $revupDialgBoxOverlay.trigger('revup.overlayClose');
                    }

                    // trigger the event
                    $this.trigger('revup.dialogCancel');

                    e.preventDefault();
                    e.stopPropagation();
                }
            })
        }

        // add the click handlers
        $('.cancelBtn', $dlg).on('click', function(e) {
            // see if disabled
            if ($(this).hasClass('disabled')) {
                return;
            }

            // call the handler if there is one
            if (opts.cancelHandler != undefined) {
                opts.cancelHandler();
            }

            if (opts.autoCloseCancel) {
                // close the dialog box
                $dlg.remove();

                // cancel the esc close
                $(document).off('keydown');

                // close the overlay
                $revupDialgBoxOverlay.trigger('revup.overlayClose');
            }

            // trigger the event
            $this.trigger('revup.dialogCancel');

            e.preventDefault;
            return false;
        });


        $('.midBtn1', $dlg).on('click', function(e) {
            // see if disabled and return
            if ($(this).hasClass('disabled')) {
                return;
            }

            // trigger the event
            $this.trigger('revup.dialogMidBtn1');

            // call the handler if there is one
            if (opts.midBtn1Handler != undefined) {
                opts.midBtn1Handler(e);
            }

            if (opts.midBtn1AutoClose) {
                // close the dialog box
                $dlg.remove();

                // cancel the esc close
                $(document).off('keydown');

                // close the overlay
                $revupDialgBoxOverlay.trigger('revup.overlayClose');
            }

            e.preventDefault;
            return false;
        });

        $('.midBtn2', $dlg).on('click', function(e) {
            // see if disabled and return
            if ($(this).hasClass('disabled')) {
                return;
            }

            // trigger the event
            $this.trigger('revup.dialogMidBtn2');

            // call the handler if there is one
            if (opts.midBtn2Handler != undefined) {
                opts.midBtn2Handler(e);
            }

            if (opts.midBtn2AutoClose) {
                // close the dialog box
                $dlg.remove();

                // cancel the esc close
                $(document).off('keydown');

                // close the overlay
                $revupDialgBoxOverlay.trigger('revup.overlayClose');
            }

            e.preventDefault;
            return false;
        });

        $('.midBtn3', $dlg).on('click', function(e) {
            // see if disabled and return
            if ($(this).hasClass('disabled')) {
                return;
            }

            // trigger the event
            $this.trigger('revup.dialogMidBtn3');

            // call the handler if there is one
            if (opts.midBtn3Handler != undefined) {
                opts.midBtn3Handler(e);
            }

            if (opts.midBtn1AutoClose) {
                // close the dialog box
                $dlg.remove();

                // cancel the esc close
                $(document).off('keydown');

                // close the overlay
                $revupDialgBoxOverlay.trigger('revup.overlayClose');
            }

            e.preventDefault;
            return false;
        });

        $('.continueBtn', $dlg).on('click', function(e) {
            // trigger the event
            $this.trigger('revup.dialogContinue');

            // call the handler if there is one
            if (opts.continueHandler != undefined) {
                opts.continueHandler(e);
            }

            if (opts.autoCloseOk) {
                // close the dialog box
                $dlg.remove();

                // cancel the esc close
                $(document).off('keydown');

                // close the overlay
                $revupDialgBoxOverlay.trigger('revup.overlayClose');
            }

            e.preventDefault;
            return false;
        });

        $('.okBtn', $dlg).on('click', function(e) {
            // trigger the event
            $this.trigger('revup.dialogOK');

            // call the handler if there is one
            if (opts.okHandler != undefined) {
                opts.okHandler(e, $dlg);
            }

            if (opts.autoCloseOk) {
                // close the dialog box
                $dlg.remove();

                // cancel the esc close
                $(document).off('keydown');

                // close the overlay
                $revupDialgBoxOverlay.trigger('revup.overlayClose');
            }

            e.preventDefault;
            return false;
        });

        $('.helpBtn', $dlg).on('click', function(e) {
            // trigger the event
            $this.trigger('revup.dialogHelp');

            // call the handler if there is one
            if (opts.helpHandler != undefined) {
                opts.helpHandler(e);
            }
        })

        if (opts.isDragable) {
            // change the cursor of the header
            $('.headerDiv', $dlg).css('cursor', "move");

            var bMouseDown = false;
            var offsetX = 0;
            var offsetY = 0;
            $('.headerDiv', $dlg)
                .on('mousedown', function(e) {
                    bMouseDown = true;

                    offsetX = e.offsetX;
                    offsetY = e.offsetY;
                })
                .on('mouseup', function(e) {
                    bMouseDown = false;
                })
                .on('mouseleave', function(e) {
                    bMouseDown = false;
                })
                .on('mousemove', function(e) {
                    if (bMouseDown) {
                        var x = e.pageX - offsetX;
                        var y = e.pageY - offsetY;

                        $dlg.offset({left: x, top: y})
                    }
                })
        }

        // position
        var scrollTop = $(document).scrollTop();
        //var pos = $dlg.position();
        var l = ($(document).width() - $dlg.width()) / 2;
        l = l + opts.leftOffset;
        var t = parseInt(opts.top, 10);
        $dlg.css('left', l)
            .css('top', t + scrollTop);

        return $dlg;
    }; // revupDialogBox

    jQuery.fn.revupDialogBox.defaults = {
        okBtnText:          'OK',                   // text for the ok button
        cancelBtnText:      'Cancel',               // text for the cancel button
        headerText:         '',                     // the title of the confirmation box
        msg:                'Are you sure you want to do this',
        extraClass:         '',                     // extra class to apply to the dialog
        zIndex:             20,                     // z index of the message box
        top:                150,                    // starting top of dialog
        leftOffset:         0,                      // amount to adjust to the left
        width:              '',                     // dialog width
        height:             '',                     // dialog height
        isDragable:         true,                   // can the box me moved
        escClose:           true,                   // pressing esc will close the dialog
        autoCloseOk:        true,                   // automaticly close the dialog box when the ok button is pressed
        autoCloseCancel:    true,                   // automaticly close the dialog box when the cancel button is pressed
        enableOkBtn:        true,                   // is the ok button enable when displayed
        cancelHandler:      undefined,              // handler for the cancel button
        bDisplayContinueBtn: false,                 // should the contiune button be displayed
        continueBtnText:    "Continue",             // text for the continue button
        continueHandler:    undefined,              // handler for the continue button
        okHandler:          undefined,              // handler for the ok button
        bHelpIcon:          false,                  // display the help icon in the button bar
        helpHandler:        undefined,              // handler forthe help button

        waitingOverlay:     false,                  // add a waiting overlay to the dialog
        waitingStyleShow:   false,                  // should the waiting overlay be displayed on load

        midBtn1Text:        "",                     // middle button 1 default label
        midBtn1Type:        "button",               // display as a "button" or "anchor" tag
        midBtn1Style:       "revupBtnDefault secondary", // if a button the style of the button
        midBtn1Enable:      true,                   // is the button enable (default)
        midBtn1AutoClose:   true,                   // auto close when the middle button 1 is pressed
        midBtn1Handler:     undefined,              // button handler

        midBtn2Text:        "",                     // middle button 1 default label
        midBtn2Type:        "button",               // display as a "button" or "anchor" tag
        midBtn2Style:       "revupBtnDefault secondary", // if a button the style of the button
        midBtn2Enable:      true,                   // is the button enable (default)
        midBtn2AutoClose:   true,                   // auto close when the middle button 2 is pressed
        midBtn2Handler:     undefined,              // button handler

        midBtn3Text:        "",                     // middle button 1 default label
        midBtn3Type:        "button",               // display as a "button" or "anchor" tag
        midBtn3Style:       "revupBtnDefault secondary", // if a button the style of the button
        midBtn3Enable:      true,                   // is the button enable (default)
        midBtn3AutoClose:   true,                   // auto close when the middle button 3 is pressed
        midBtn3Handler:     undefined,              // button handler
    };

    /*
     *
     * RevUp Pagination
     *
     */
    jQuery.fn.revupPagination = function(options, arg1, arg2)
    {
        if (typeof options == 'string') {
            var command = options;
            var $revupPagination = $('.revupPagination', $(this));
            var $revupPaginationOverlay = $('.revupPaginationOverlay', $(this));

            switch (command) {
                case 'setMessage':
                    $('.whatsShowingDiv .extraMsg').html(arg1);
                    break;
                case 'clearMessage':
                    $('.whatsShowingDiv .extraMsg').html('');
                    break;
                case 'disabled':
                    if (arg1) {
                        $revupPagination.addClass('disabled');
                        $revupPaginationOverlay.show();
                    }
                    else if (!arg1) {
                        $revupPagination.removeClass('disabled')
                        $revupPaginationOverlay.hide();
                    }
                    else {
                        return $revupPagination.hasClass('disabled') ;
                    }

            } // end switch - command

            return;
        }

        // get the options, if any
        var opts = $.extend({}, jQuery.fn.revupPagination.defaults, options);

        // to display whats showing need the entriesPerPage and numOfEntries
        if (opts.bDisplayWhatsShowing && ((opts.entriesPerPage == -1) || (opts.numOfEntries == -1))) {
            opts.bDisplayWhatsShowing = false;
        }

        // get the selector of the dom object attached to
        var $this = $(this);

        function buildPageButtons()
        {
            var sTmp = [];

            // left arrow - previous page - initially disabled
            sTmp.push('<div class="paginationArrow previous disabled">');
                sTmp.push('<div class="icon icon-triangle-left"></div>');
            sTmp.push('</div>');

            // page 1 - always
            sTmp.push('<div class="paginationPageNum fixed active" pageNum="1">');
                sTmp.push('<div class="pageNum">1</div>');
            sTmp.push('</div>');

            if (opts.numOfPages > opts.numMaxNumBoxes) {
                sTmp.push('<div class="paginationSpacer leftSpacer" style="display:none;"></div>')
            }

            // display the pages
            var dNumPages = opts.numOfPages - opts.numFixedNumBoxes; // 2 for the number of fixed elements
            if (dNumPages > opts.numMiddleNumBoxes) {
                dNumPages = opts.numMiddleNumBoxes;
            }
            for (var i = 0; i < dNumPages; i++) {
                var pNum = i + 1;
                sTmp.push('<div class="paginationPageNum" boxNum="' + (i + 1) + '" pageNum="' + (i + 2) + '">');
                    sTmp.push('<div class="pageNum">' + (i + 2) + '</div>');
                sTmp.push('</div>');
            }

            if (opts.numOfPages > opts.numMaxNumBoxes) {
                sTmp.push('<div class="paginationSpacer rightSpacer"></div>')
            }

            // last page - always if more than 2 pages
            if (opts.numOfPages > 1) {
                sTmp.push('<div class="paginationPageNum fixed" pageNum="' + opts.numOfPages + '">');
                    sTmp.push('<div class="pageNum">' + opts.numOfPages + '</div>');
                sTmp.push('</div>');
            }

            // right arrow - next page
            var extraClass = '';
            if (opts.numOfPages == 1) {
                extraClass = " disabled";
            }
            sTmp.push('<div class="paginationArrow next' + extraClass + '">');
                sTmp.push('<div class="icon icon-triangle-right"></div>');
            sTmp.push('</div>');

            // add the goto a typed in page
            if (opts.displayGoToPage) {
                sTmp.push('<div class="pickPageDiv">');
                    sTmp.push('<div class="text">go to page</div>');
                    sTmp.push('<input type="text" class="pageNum" value="" maxlength=4>');
                    sTmp.push('<div class="roundBtn">');
                        sTmp.push('<div class="roundBtnText">go</div>');
                    sTmp.push('</div>');
                sTmp.push('</div>');
            }

            return sTmp.join('');
        } // buildPageButtons

        // build the html for pagination
        var sTmp = [];
        sTmp.push('<div class="revupPaginationOverlay"></div>')
        sTmp.push('<div class="revupPagination" currentPage="' + opts.startingPage + '" numPages="' + opts.numOfPages + '">');
            sTmp.push('<div class="whatsShowingDiv">');
                if (opts.bDisplayWhatsShowing) {
                    var start = ((opts.startingPage - 1) * opts.entriesPerPage) + 1;
                    var end = opts.startingPage * opts.entriesPerPage;
                    if (end > opts.numOfEntries) {
                        end = opts.numOfEntries;
                    }

                    sTmp.push('Showing&nbsp');
                    sTmp.push('<div class="whatsShowingVal start">' + start + '</div>');
                    sTmp.push('&nbsp;to&nbsp;');
                    sTmp.push('<div class="whatsShowingVal end">' + end + '</div>');
                    sTmp.push('&nbsp;of&nbsp;' + opts.numOfEntries + '&nbsp;entries&nbsp;');
                    sTmp.push('<div class="whatsShowingVal extraMsg">' + opts.extraDisplayWhatText + '</div>');
                }
            sTmp.push('</div>');

            sTmp.push('<div class="paginationDivInner">');
                sTmp.push('<div class="paginationGutsDiv">');
                    sTmp.push(buildPageButtons());
                sTmp.push('</div>')
            sTmp.push('</div>');

            sTmp.push('<div class="numPerPageDiv">');
                sTmp.push('<div class="numPerPageGutsDiv">')
                    if (opts.bDisplayNumPerPage) {
                        for (var i = 0; i < opts.numPerPageLst.length; i++) {
                            var extraClass = "";
                            if (opts.entriesPerPage == opts.numPerPageLst[i]) {
                                extraClass = ' active';
                            }
                             sTmp.push('<div class="numPerPage' + extraClass + '" numPerPage="' + opts.numPerPageLst[i] + '">');
                                sTmp.push('<div class="text">' + opts.numPerPageLst[i] + '</div>');
                            sTmp.push('</div>');
                        }
                    }
                sTmp.push('</div>');
            sTmp.push('</div>');
        sTmp.push('</div>');

        //display the controls
        var $pag = $this.html(sTmp.join(''));
        $('.revupPaginationOverlay', $pag).width($('.revupPagination', $pag).width())
        $('.revupPaginationOverlay', $pag).height($('.revupPagination', $pag).outerHeight(true))
        var offset = $pag.position();
        $('.revupPaginationOverlay', $pag).css('left', offset.left);
        $('.revupPaginationOverlay', $pag).css('top', offset.top);

        if (opts.startingPage != 1) {
            updatePagination($('.revupPagination', $this));
        }

        function updatePagination($paginationCntl)
        {
            // get the info about the pagination
            var currentPage = Number($paginationCntl.attr('currentPage'));
            var numPages = Number($paginationCntl.attr('numPages'));

            // enable/disable the arrows
            var $arrow = $('.paginationArrow.previous', $paginationCntl);
            if (currentPage == 1) {
                $arrow.addClass('disabled');
            }
            else {
                $arrow.removeClass('disabled');
            }
            $arrow = $('.paginationArrow.next', $paginationCntl);
            if (currentPage >= numPages) {
                $arrow.addClass('disabled');
            }
            else {
                $arrow.removeClass('disabled');
            }

            // the middle page number boxes
            var newNum = currentPage - Math.floor(opts.numMiddleNumBoxes / 2);
            if (currentPage < 2) {
                newNum = 2;
            }
            if (currentPage > (numPages - Math.floor(opts.numMiddleNumBoxes / 2))) {
                newNum = numPages - opts.numMiddleNumBoxes;
            }
            if (newNum < 2) {
                newNum = 2;
            }
            $('.paginationPageNum[boxNum]', $paginationCntl).each(function() {
                var $pageNum = $(this);

                // update the page number
                $pageNum.attr("pageNum", newNum);
                $('.pageNum', $pageNum).text(newNum);

                // next
                newNum++;
            });

            // see if the spacer should be displayed
            var firstBox = Number($('.paginationPageNum[boxnum="1"]', $paginationCntl).attr('pageNum'))
            var $spacer = $('.paginationSpacer.leftSpacer', $paginationCntl);
            if (firstBox == 2) {
                $spacer.hide();
            }
            else {
                $spacer.show();
            }
            var lastBox = Number($('.paginationPageNum[boxnum="' + opts.numMiddleNumBoxes + '"]', $paginationCntl).attr('pageNum'))
            var $spacer = $('.paginationSpacer.rightSpacer', $paginationCntl);
            if (lastBox == (numPages - 1)) {
                $spacer.hide();
            }
            else {
                $spacer.show();
            }

            // update the active page number
            $('.paginationPageNum', $paginationCntl).each(function() {
                var $pageNum = $(this);

                var pageNum = Number($pageNum.attr("pageNum"));
                if (pageNum == currentPage) {
                    $pageNum.addClass('active');
                }
                else {
                    $pageNum.removeClass('active');
                }
            });
        } // updatePagination

        /*
         * Event handlers - click on the different controls
         */
        // arrow key
        $this.off('click', '.paginationArrow');
        $this.on('click', '.paginationArrow', function () {
            var $arrow = $(this);

            // see if disabled
            if ($arrow.hasClass('disabled')) {
                return false;
            }

            // get the current state of things
            var $pDiv = $arrow.closest('.revupPagination');
            var currentPage = Number($pDiv.attr("currentPage"));
            var numPages = Number($pDiv.attr('numPages'));

            // see if the number should be incremented or decreamented
            if ($arrow.hasClass('previous')) {
                currentPage--;
            }
            else if ($arrow.hasClass('next')) {
                currentPage++;
            }

            // check the range
            if (currentPage < 1) {
                currentPage = 1;
            }
            else if (currentPage > numPages) {
                currentPage = numPages;
            }

            // save the new page, update the pagination and load new page
            $pDiv.attr('currentPage', currentPage);
            updatePagination($pDiv);

            // trigger new page
            $this.trigger({
                type: 'revup.pagination',
                pageNumber: currentPage,
                entriesPerPage: opts.entriesPerPage,
            });

            // update the counts if displayed
            if (opts.bDisplayWhatsShowing) {
                var start = ((currentPage - 1) * opts.entriesPerPage) + 1;
                var end = currentPage * opts.entriesPerPage;
                if (end > opts.numOfEntries) {
                    end = opts.numOfEntries;
                }

                $('.whatsShowingVal.start', $pDiv).text(start);
                $('.whatsShowingVal.end', $pDiv).text(end);
            }
        });

        // number button
        $this.off('click', '.paginationPageNum');
        $this.on('click', '.paginationPageNum', function (e) {
            var $pNum = $(this);
            var $pDiv = $pNum.closest('.revupPagination');

/*
            if ($pDiv.hasClass('disabled')) {
                e.stopImmediatePropagation();

                return false;
            }
*/

            // see if active
            if ($pNum.hasClass('active')) {
                return false;
            }

            // get the page number of the box clicked on
            var newPageNum = Number($pNum.attr('pageNum'));

            // save the new page, update the pagination and load new page
            $pDiv.attr('currentPage', newPageNum);
            updatePagination($pDiv);

            // trigger new page
            $this.trigger({
                type: 'revup.pagination',
                pageNumber: newPageNum,
                entriesPerPage: opts.entriesPerPage,
            });

            // update the counts if displayed
            if (opts.bDisplayWhatsShowing) {
                var start = ((newPageNum - 1) * opts.entriesPerPage) + 1;
                var end = newPageNum * opts.entriesPerPage;
                if (end > opts.numOfEntries) {
                    end = opts.numOfEntries;
                }

                $('.whatsShowingVal.start', $pDiv).text(start);
                $('.whatsShowingVal.end', $pDiv).text(end);
            }
        });

        // go to page handlers
        // make sure only number are typed and handle return like clicking on go button
        $this.off('keypress', '.pickPageDiv .pageNum');
        $this.on('keypress', '.pickPageDiv .pageNum', function(e) {
            if (e.charCode == 13) {
                // on return act like a click
                var $editor = $(this);
                var $pDiv = $editor.closest('.revupPagination');
                var $goBtn = $('.pickPageDiv .roundBtn', $pDiv);
                $goBtn.trigger('click');

                return;
            }

            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) {
                return false;
            }
        })
        .off("cut copy paste", '.pickPageDiv .pageNum')
        .on("cut copy paste", '.pickPageDiv .pageNum', function(e){
            e.preventDefault();
        });

        // go to page
        $this.off('click', '.pickPageDiv .roundBtn');
        $this.on('click', '.pickPageDiv .roundBtn', function() {
            var $btn = $(this);
            var $pDiv = $btn.closest('.revupPagination');
            var numPages = Number($pDiv.attr('numPages'));
            var currentPage = Number($pDiv.attr('currentPage'));

            // get the new page
            var newPage = $('.pickPageDiv .pageNum', $pDiv).val();
            if (newPage == "") {
                newPage = 0;
            }
            newPage = Number(newPage);

            // range check
            if ((newPage == 0) || (newPage > numPages)) {
                $pDiv.revupMessageBox({
                    headerText: "Error - Out of Range",
                    msg: "The page number entered is out of range.",
                    zIndex: opts.zIndex + 1
                });
                return;
            }

            // reset the editor
             $('.pickPageDiv .pageNum', $pDiv).val('');

            // if same page do nothing
            if (newPage == currentPage) {
                console.warning('same page')
                return;
            }

            // save the new page, update the pagination and load new page
            $pDiv.attr('currentPage', newPage);
            updatePagination($pDiv);

            // trigger new page
            $this.trigger({
                type: 'revup.pagination',
                pageNumber: newPage,
                entriesPerPage: opts.entriesPerPage
            });

            // update the counts if displayed
            if (opts.bDisplayWhatsShowing) {
                var start = ((newPage - 1) * opts.entriesPerPage) + 1;
                var end = newPage * opts.entriesPerPage;
                if (end > opts.numOfEntries) {
                    end = opts.numOfEntries;
                }

                $('.whatsShowingVal.start', $pDiv).text(start);
                $('.whatsShowingVal.end', $pDiv).text(end);
            }
        });

        $this.off('click', '.numPerPageDiv .numPerPage');
        $this.on('click', '.numPerPageDiv .numPerPage', function(e) {
            var $btn = $(this);

            // see if active
            if ($btn.hasClass('active')) {
                return;
            }

            // switch active
            $('.numPerPageDiv .numPerPage.active', $this).removeClass('active');
            $btn.addClass('active');

            // recompute the number of pages
            opts.numPerPage = Number($btn.attr('numPerPage'));
            opts.numOfPages = Math.ceil(opts.numOfEntries / opts.numPerPage);
            opts.entriesPerPage = opts.numPerPage;

            $('.revupPagination', $this).attr('numPages', opts.numOfPages)
                                        .attr('currentPage', 1);

            // update the pages displayed
            var sTmp = buildPageButtons();
            $('.paginationGutsDiv', $this).html(sTmp);

            // trigger new page
            setTimeout(function() {
                $this.trigger({
                    type: 'revup.pagination',
                    pageNumber: 1,
                    entriesPerPage: opts.entriesPerPage
                });
            }, 1);
        });

        return this;
    }; // revupPagination

    jQuery.fn.revupPagination.defaults = {
        // pagination constants
        numMaxNumBoxes:         7,              // total number of page number boxes - best if odd number
        numMiddleNumBoxes:      5,              // number of middle - moving boxes - best if odd number
        numFixedNumBoxes:       2,              // number of fixed boxes - normally the first and last page
        zIndex:                 25,             // zIndex for message box

        entriesPerPage:         -1,             // number of entries per page
        numOfEntries:           -1,             // total number of entries
        bDisplayWhatsShowing:   false,          // should the whats show be display, also requires entriesPerPage and numOfEntries
                                                // to be set
        extraDisplayWhatText:   "",             // extra text/msg to display if displayWhatsShowing
        numOfPages:             1,              // number of page
        startingPage:           1,              // starting page
        displayGoToPage:        true,           // display the go to page

        bDisplayNumPerPage:     false,          // display the maximum number of entry to display
        numPerPage:             25,             // active number of items per page
        numPerPageLst:          [25, 50, 100],  // number per page
    };

    /*
     *
     * Revup popup - next to a control
     *
     */
    var $lastPopup = undefined;
    var clearPopupTimer = undefined;

    var $currentPopup = undefined;
    var $currentOverlay = undefined;
    jQuery.fn.revupPopup = function(options)
    {
        var $this = $(this);

        if (typeof options == 'string') {
            switch (options) {
                case 'close':
                    $('.revupPopup').hide();
                    $('.revupPopup').remove();

                    // close the overlay
                    $('.revupOverlay').trigger('revup.overlayClose');

                    // cancel the auto close timer
                    if (autoCloseTimer != null) {
                        clearTimeout(autoCloseTimer);
                        autoCloseTimer = null;
                    }

                    // add a delay to clearing the source of popup
                    //clearPopupTimer = setTimeout(function() {
                        //if (!$this.is($lastPopup)) {
                            $lastPopup = undefined;
                            clearPopupTimer = undefined;
                        //}
                    //}, 50)

                    break;
            }

            return;
        }

        // timer
        var autoCloseTimer = null;

        // get the options, if any
        var opts = $.extend({}, jQuery.fn.revupPopup.defaults, options);
        $this.data('opts', opts);

        // keep selector so don't display the samething twice
        if ($this.is($lastPopup)) {
            $lastPopup = undefined;
            return
        }

        // if single close
        if (opts.bSingle) {
            if ($currentPopup != undefined) {
                $currentPopup.hide().remove();
            }
            if ($currentOverlay != undefined) {
                $currentOverlay.trigger('revup.overlayClose');
            }
        }

        // set the popup and clear the timer for delaying the clearing
        $lastPopup = $this;
        if (clearPopupTimer != undefined) {
            clearTimeout(clearPopupTimer);
            clearPopupTimer = undefined;
        }

        // get the location and size of the anchor point
        var anchorWidth = $this.outerWidth();
        var anchorHeight = $this.outerHeight();
        var offset = $this.offset();

        // build the style of the popup
        var style = [];
        style.push("z-index:" + opts.zIndex);

        // build the popup and add to the dom
        var arrowDirection = opts.location.toLowerCase();
        if (arrowDirection == 'below') {
            arrowDirection = 'top'
        }
        else if (arrowDirection == 'above') {
            arrowDirection = 'bottom'
        }

        var extraClass = "";
        if (opts.extraClass) {
            extraClass = " " + opts.extraClass;
        }

        var html = [];
        html.push('<div class="revupPopup ' + arrowDirection + extraClass + '" style="' + style.join(';') + '">');
            // left arrow
            if (arrowDirection == "left") {
                html.push('<div class="arrow" style="top: 50%"></div>');
            }
            if (arrowDirection == "top") {
                html.push('<div class="arrow" style="left: 50%"></div>');
            }

            // content
            html.push(opts.content)

            // right arrow
            if (arrowDirection == "right") {
                html.push('<div class="arrow" style="top: 50%"></div>');
            }
        html.push('</div>');
        $(html.join('')).appendTo('body');
        var $popUp = $('.revupPopup');
        $currentPopup = $popUp;

        // get popup width and height
        var popWidth = $popUp.outerWidth();
        var popHeight = $popUp.outerHeight();

        // compute offset
        var offsetX, offsetY;
        if (arrowDirection == 'left') {
            offsetX = -popWidth;
            offsetY = -((popHeight - anchorHeight) / 2);
        }
        else if (arrowDirection == 'right') {
            offsetX = anchorWidth;
            offsetY = -((popHeight - anchorHeight) / 2);
        }
        else if (arrowDirection == 'top') {
            // remember to adjust for the with of the arrow
            offsetX = -((popWidth - anchorWidth) / 2) - 6;
            offsetY = anchorHeight;

            // adjust left/right
            var xPos = $this.position().left - ((popWidth - anchorWidth) / 2);
            if (xPos < opts.minLeft) {
                var offsetXDelta = xPos - opts.minLeft;
                offsetX -= offsetXDelta;

                // adjust the arrow
                var arrowX = (popWidth / 2) + offsetXDelta ;
                $('.arrow', $popUp).css('left', arrowX);
            }
            else if ((xPos + popWidth) > ($('body').width() - opts.maxRight)) {
                var offsetXDelta = (xPos + popWidth) - ($('body').width() - opts.maxRight);
                offsetX -= offsetXDelta; //(xPos + ($('body').width() - opts.maxRight));

                // adjust the arrow
                var arrowX = (popWidth / 2) + offsetXDelta;
                $('.arrow', $popUp).css('left', arrowX);
            }
        }
        offsetX += offset.left + opts.adjLeftRight;
        offsetY += offset.top + opts.adjUpDown;

        // position
        $popUp.css('left', offsetX);
        $popUp.css('top', offsetY);

        // create clear overlay
        var $overlay;
        if (opts.bDoNotCloseOverlay) {
            $overlay = $this.overlay(true, opts.zIndex - 2, true, 'body', true);
        }
        else {
            $overlay = $this.overlay(false, opts.zIndex - 2, true, 'body', opts.bPassThrough);
            $currentOverlay = $overlay;
            $this.one('revup.overlayClick', function(e) {
                // close the overlay
                $overlay.trigger('revup.overlayClose');

                $popUp.hide();
                $popUp.remove();

                if (autoCloseTimer != null) {
                    clearTimeout(autoCloseTimer);
                    autoCloseTimer = null;
                }

                // add a delay to clearing the source of popup
                clearPopupTimer = setTimeout(function() {
                    //if (!$this.is($lastPopup)) {
                        $lastPopup = undefined;
                        clearPopupTimer = undefined;
                    //}
                }, 50)
            });
        }

        if (opts.bEscClose) {
            $(document).on('keydown', function(e) {
                if (e.keyCode == 27) {
                    $overlay.trigger('revup.overlayClose');
                    $popUp.hide();
                    $popUp.remove();

                    $(document).off('keydown');

                    e.stopPropagation();
                    e.preventDefault();
                }
            })
        }

        // set the auto close
        if (opts.bAutoClose) {
            autoCloseTimer = setTimeout(function() {
                $popUp.hide();
                $popUp.remove();
                autoCloseTimer = null;

                // reset the last popup flag
                $lastPopup = undefined;

                // close the overlay
                $overlay.trigger('revup.overlayClose');
            }, opts.autoCloseTimeout);
        }

        // display
        $popUp.show();

        return $popUp;
    }; // revupPopup

    jQuery.fn.revupPopup.defaults = {
        extraClass:     "",                 // extra class added to the popup
        location:       "right",            // where to display the popup - right, left
        minLeft:        0,                  // the min x location of the Popup
        maxRight:       0,                  // offset of the width of the viewport
        adjLeftRight:   0,                  // adjust x
        adjUpDown:      0,
        zIndex:         20,                 // z-index of the popup
        bAutoClose:     false,              // enable auto close
        bEscClose:      true,               // close when the esc button is pressed
        bDoNotCloseOverlay: false,          // do not close when the overlay is clicked
        bPassThrough:   true,               // should the click to close overlay be passed below
        bSingle:        false,              // should there only be one popup
        autoCloseTimeout: 15000,        // time before close - 15 seconds
        content:    '<div style="height: 50px;width:100px;text-align:center;"><div style="margin-top:20px">RevUp Popup</div></div>',
    }

    /*
     *
     * Splash Screen
     *
     */
    jQuery.fn.revupSplashPage = function(options) {
        // get the options, if any
        var opts = $.extend({}, jQuery.fn.revupSplashPage.defaults, options);

        var $this = $(this);

        var iSrc = "";
        if (opts.splashUrl == '') {
            iSrc = "srcdoc='" + opts.splashDoc + "'";
        }
        else {
            iSrc = "src='" + opts.splashUrl + "'";
        }

        // style for the popup
        var style = ""
        style = "z-index:" + opts.zIndex + ";";

        // build the popup
        var sTmp = [];
        sTmp.push('<div class="revupSplashPage" style="' + style +'">');
            sTmp.push('<div class="btnDiv">');
                sTmp.push('<div class="btnContainerRight">');
                    sTmp.push('<div class="closeBtn icon icon-close"></div>');
                sTmp.push('</div>')
            sTmp.push('</div>');

            sTmp.push('<div class="theIFrame">');
                sTmp.push('<iframe ' + iSrc + ' width="' + opts.width + '" height="' + opts.height + '" scrolling="no"></iframe>');
            sTmp.push('</div>');
        sTmp.push('</div>');
        $(sTmp.join('')).appendTo('body');
        var $splashPage = $('.revupSplashPage');


        // create clear overlay
        var $overlay = $this.overlay(false, opts.zIndex - 2, false, 'body');

        // position
        var scrollTop = $(document).scrollTop();
        var l = ($(document).width() - $splashPage.width()) / 2;
        var t = parseInt(opts.top, 10);
        $splashPage.css('left', l).css('top', t + scrollTop);

        // close clicking on overlay
        $this.one('revup.overlayClick', function(e) {
            $splashPage.hide();
            $splashPage.remove();
        });

        // close button handler
        $('.closeBtn', $splashPage).on('click', function () {
            // remove the splash screen
            $splashPage = $('.revupSplashPage');
            $splashPage.hide();
            $splashPage.remove();

            // remove the overlay
            $overlay.trigger('revup.overlayClose');
        });
    }

    jQuery.fn.revupSplashPage.defaults = {
        width:              500,
        height:             400,
        top:                20,
        zIndex:             1000,
        closeBtnLabel:      'Close',
        splashUrl:          '',
        splashDoc:          '<div style="width:100%;height:100%;background-color:#0f0"><div style="left:50%; top:50%; transform:translate(-50%,-50%); -webkit-transform:translate(-50%,-50%); background-color:gray; color:white; position:absolute;">This is a test</div></div>',
    }

    /*
     *
     * Revup Slide switch
     *
     */
    jQuery.fn.revupSlideSwitch = function(options, arg1, arg2)
    {
        if (typeof options == 'string') {
            var command = options;
            var $revupSlideSwitch = $('.revupSlideSwitch', $(this));

            switch (command) {
                case 'disabled':
                    var bDisabled = true;
                    if (typeof arg1 == 'string') {
                        if (arg1.toLowerCase() == 'false') {
                            bDisabled = false;
                        }
                        else {
                            bDisabled = true;
                        }
                    }
                    else {
                        bDisabled = arg1;
                    }

                    if (bDisabled) {
                        $revupSlideSwitch.addClass('disabled');
                        $('.sliderDiv', $revupSlideSwitch).removeClass('left').addClass('right');
                        $('.sliderDiv .switchDiv', $revupSlideSwitch).css('left', $revupSlideSwitch.data('onSwitchRightPos'));
                        $('.textDiv', $revupSlideSwitch).html($revupSlideSwitch.data('offText'));
                    }
                    else {
                        $revupSlideSwitch.removeClass('disabled');
                    }

                    break;
                case 'getState':
                    var $slideDiv = $('.sliderDiv', $revupSlideSwitch);
                    if ($slideDiv.hasClass('left')) {
                        return "on";
                    }

                    return "off";
                    break;
            } // end switch - command

            return;
        }

        // get the options, if any
        var opts = $.extend({}, jQuery.fn.revupSlideSwitch.defaults, options);

        var $this = $(this);
        var sTmp = [];

        // inital position of switch
        var switchSide = " right";
        var switchMsg = opts.offText;
        var switchStyle = "left:" + opts.onSwitchRightPos + ";";
        if (opts.initState.toLowerCase() == "on") {
            switchSide = " left revupPrimaryBtnColor";
            switchStyle = "left:" + opts.onSwitchLeftPos + ";";
            switchMsg = opts.onText;
        }

        // add extra class
        var mainClass = "revupSlideSwitch";
        if (opts.extraClass != '') {
            mainClass += " " + opts.extraClass;
        }

        // if disabled set the class and turn off
        if (opts.disabled) {
            mainClass += " disabled";

            switchSide = " right";
            switchMsg = opts.offText;
        }

        // create the markup
        sTmp.push('<div class="' + mainClass + '">');
            sTmp.push('<div class="sliderDiv' + switchSide + '">');
                sTmp.push('<div class="switchDiv" style="' + switchStyle + '"></div>');
            sTmp.push('</div>');

            sTmp.push('<div class="textDiv">');
                sTmp.push(switchMsg);
            sTmp.push('</div>');
        sTmp.push('</div>');

        $this.html(sTmp.join(''));

        // save the on/off messages
        $('.revupSlideSwitch', $this).data('onText', opts.onText);
        $('.revupSlideSwitch', $this).data('offText', opts.offText);
        $('.revupSlideSwitch', $this).data('onSwitchLeftPos', opts.onSwitchLeftPos);
        $('.revupSlideSwitch', $this).data('onSwitchRightPos', opts.onSwitchRightPos);

        // add the event handlers
        $('.revupSlideSwitch', $this).on('click', '.sliderDiv', function(e) {
            var $slider = $(this)
            var $revupSliderSwitch = $slider.closest('.revupSlideSwitch');

            // see if disabled
            if ($revupSliderSwitch.hasClass('disabled')) {
                return;
            }

            // see if on or off
            if ($slider.hasClass('left')) {
                // on to off
                $('.switchDiv', $slider).animate({left: opts.onSwitchRightPos}, "fast", function() {
                    $slider.removeClass('left').removeClass('revupPrimaryBtnColor').addClass('right');
                });

                $('.textDiv', $revupSliderSwitch).html(opts.offText)
            }
            else {
                // off to on
                $('.switchDiv', $slider).animate({left: opts.onSwitchLeftPos}, "fast", function() {
                    $slider.removeClass('right').addClass('left').addClass('revupPrimaryBtnColor');
                });

                $('.textDiv', $revupSliderSwitch).html(opts.onText)
            }
        })

    } // revupSlideSwitch

    jQuery.fn.revupSlideSwitch.defaults = {
        disabled:           false,
        initState:          "off",      // on - left, off - right
        onSwitchLeftPos:    "1px",
        onSwitchRightPos:   "15px",
        onText:             "Yes",
        offText:            "No",
        extraClass:         "",
    }

    /*
     *
     * Revup Color Control - allows user to both use a color picker/spectrum and enter the color value
     *
     */
    jQuery.fn.revupColorCntl = function(options, arg1, arg2)
    {
        var $this = $(this);

        function borderColor(bColor, borderChange)
        {
            if (bColor.length == 4) {
                r = parseInt(bColor.substring(1, 2), 16);
                g = parseInt(bColor.substring(2, 3), 16);
                b = parseInt(bColor.substring(3, 4), 16);
            }
            else {
                r = parseInt(bColor.substring(1, 3), 16);
                g = parseInt(bColor.substring(3, 5), 16);
                b = parseInt(bColor.substring(5, 7), 16);
            }
            var hsl = rgbToHsl(r, g, b);

            // default for borderChange
            if (!borderChange) {
                borderChange = -20;
            }

            var l = (100.0 * hsl.l) + ((hsl.l * 100.0) * (borderChange / 100.0));
            if (l > 100.0) {
                l = 100.0;
            }
            else if (l < 0.0) {
                l = 0;
            }
            l += '%';
            var s = (hsl.s * 100.0) + '%';

            return 'hsl(' + (hsl.h * 360) + ', ' + s + ', ' + l + ')';
        } // borderColor

        if (typeof options == 'string') {
            var command = options;
            var $revupSlideSwitch = $('.revupSlideSwitch', $(this));

            switch (command) {
                case 'hide':
                    $('.colorSample', $this).spectrum('hide');
                    break;
                case 'get':
                    var o = {};
                    var d = $('.revupColorCntl', $(this)).data('field');
                    if (d) {
                        o.field = d;
                    }
                    else {
                        return o;
                    }

                    d = $('.revupColorCntl', $(this)).data('otherFields');
                    if (d) {
                        o.otherFields = d;
                    }

                    return o;

                    break;
                case 'setColor':
                    // set the field in the control
                    $('.colorSample', $this).css('background-color', arg1);
                    $('.colorInput', $this).val(arg1);

                    // set the color for the color picker
                    $('.colorSample', $this).spectrum('set', arg1);

                    // set the fields with the color
                    var $connectedField = $('.revupColorCntl', $this).data('connectedField');
                    var colorType = $('.revupColorCntl', $this).data('colorType');

                    // convert the colorType into a array
                    var changeParts = colorType.split(',');
                    for (var i = 0; i < changeParts.length; i++) {
                        changeParts[i] = $.trim(changeParts[i].toLowerCase());
                    }

                    var newColor = "";
                    var newBorderColor = "";
                    if (changeParts.indexOf('border') != -1) {
                        var bColor = borderColor(arg1, $('.revupColorCntl', $this).data('borderChange'));
                        $connectedField.css('border-color', bColor);
                        newBorderColor = rgbToHex($connectedField.css('border-color'));
                    }
                    if (changeParts.indexOf('bordertab') != -1) {
                        var bColor = borderColor(arg1, $('.revupColorCntl', $this).data('borderChange'));
                        $connectedField.css('border-top-color', bColor);
                        $connectedField.css('border-bottom-color', bColor);
                        $connectedField.css('border-left-color', bColor);
                        $connectedField.css('border-right-color', 'none');
                        newBorderColor = rgbToHex($connectedField.css('border-left-color'));
                    }
                    if (changeParts.indexOf('clearbordertab') != -1) {
                        var bColor = borderColor(arg1, $('.revupColorCntl', $this).data('borderChange'));
                        $connectedField.css('border-top-color', 'transparent');
                        $connectedField.css('border-bottom-color', 'transparent');
                        $connectedField.css('border-left-color', 'transparent');
                        $connectedField.css('border-right-color', 'none');
                        newBorderColor = rgbToHex($connectedField.css('border-left-color'));
                    }
                    if (changeParts.indexOf('background') != -1) {
                        $connectedField.css('background-color', arg1);
                        newColor = arg1;
                    }
                    if (changeParts.indexOf('color') != -1) {
                        $connectedField.css('color', arg1)
                        newColor = arg1;
                    }

                    // update the colorVal of the field data
                    var field = $('.revupColorCntl', $this).data('field');
                    for (var i = 0; i < field.length; i++) {
                        if ((field[i].colorType == 'border') || (field[i].colorType == 'borderTab')) {
                            field[i].colorVal = newBorderColor;
                        }
                        else if ((field[i].colorType == 'background') || (field[i].colorType == 'color')) {
                            field[i].colorVal = newColor;
                        }
                    }
                    $('.revupColorCntl', $this).data('field', field);

                    var otherFields = $('.revupColorCntl', $this).data('otherFields');
                    if ((otherFields) && (otherFields.length > 0)) {
                        for (var i = 0; i < otherFields.length; i++) {
                            // convert the colorType into a array
                            //var cParts = otherFields[i].colorType.split(',');
                            var cParts = otherFields[i].colorType.split(',');
                            for (var j = 0; j < cParts.length; j++) {
                                cParts[j] = $.trim(cParts[j].toLowerCase());
                            }

                            if (cParts.indexOf('border') != -1) {
                                var bColor = borderColor(arg1, $('.revupColorCntl', $this).data('borderChange'));
                                otherFields[i].$field.css('border-color', bColor);
                                otherFields[i].colorVal = bColor;
                            }
                            if (cParts.indexOf('bordertab') != -1) {
                                var bColor = borderColor(arg1, $('.revupColorCntl', $this).data('borderChange'));
                                otherFields[i].$field.css('border-top-color', bColor);
                                otherFields[i].$field.css('border-bottom-color', bColor);
                                otherFields[i].$field.css('border-left-color', bColor);
                                otherFields[i].$field.css('border-right-color', 'none');
                                otherFields[i].colorVal = bColor;
                            }
                            if (cParts.indexOf('clearbordertab') != -1) {
                                var bColor = borderColor(arg1, $('.revupColorCntl', $this).data('borderChange'));
                                otherFields[i].$field.css('border-top-color', 'transparent');
                                otherFields[i].$field.css('border-bottom-color', 'transparent');
                                otherFields[i].$field.css('border-left-color', 'transparent');
                                otherFields[i].$field.css('border-right-color', 'none');
                                otherFields[i].colorVal = bColor;
                            }
                            if (cParts.indexOf('background') != -1) {
                                otherFields[i].$field.css('background-color', arg1);
                                otherFields[i].colorVal = arg1;
                            }
                            if (cParts.indexOf('color') != -1) {
                                otherFields[i].$field.css('color', arg1);
                                otherFields[i].colorVal = arg1;
                            }
                        }

                        $('.revupColorCntl', $this).data('otherFields', otherFields);
                    }

                    break;
                case 'clear':
                    // set the fields with the color
                    var $connectedField = $('.revupColorCntl', $this).data('connectedField');
                    var colorType = $('.revupColorCntl', $this).data('colorType');

                    if (!colorType) {
                        return;
                    }

                    // convert the colorType into a array
                    var changeParts = colorType.split(',');
                    for (var i = 0; i < changeParts.length; i++) {
                        changeParts[i] = $.trim(changeParts[i].toLowerCase());
                    }

                    var sampleColor;
                    var sampleColorPriority = -1;
                    var newBorderColor = undefined;
                    var newColor = undefined;
                    if (changeParts.indexOf('border') != -1) {
                        $connectedField.css('border-color', '');

                        newBorderColor = rgbToHex($connectedField.css('border-color'));
                        if (sampleColorPriority <= 3) {
                            sampleColor = newBorderColor;
                            sampleColorPriority = 3;
                        }
                    }
                    if ((changeParts.indexOf('bordertab') != -1) || (changeParts.indexOf('clearbordertab') != -1)) {
                        $connectedField.css('border-top-color', '');
                        $connectedField.css('border-bottom-color', '');
                        $connectedField.css('border-left-color', '');
                        $connectedField.css('border-right-color', 'none');

                        newBorderColor = rgbToHex($connectedField.css('border-top-color'));
                        if (sampleColorPriority <= 1) {
                            sampleColor = newBorderColor;
                            sampleColorPriority = 1;
                        }
                    }
                    if (changeParts.indexOf('background') != -1) {
                        $connectedField.css('background-color', '');

                        newColor = rgbToHex($connectedField.css('background-color'));
                        if (sampleColorPriority <= 10) {
                            sampleColor = newColor;
                            sampleColorPriority = 10;
                        }
                    }
                    if (changeParts.indexOf('color') != -1) {
                        $connectedField.css('color', '');

                        newColor = rgbToHex($connectedField.css('color'));
                        if (sampleColorPriority <= 2) {
                            sampleColor = newColor;
                            sampleColorPriority = 2;
                        }
                    }
                    $('.revupColorCntl .colorInput', $this).val(sampleColor);
                    $('.revupColorCntl .colorSample', $this).css('background-color', sampleColor);

                    // set the color for the color picker
                    $('.colorSample', $this).spectrum('set', sampleColor);

                    // update the colorVal of the field data
                    var field = $('.revupColorCntl', $this).data('field');
                    for (var i = 0; i < field.length; i++) {
                        if ((field[i].colorType == 'border') || (field[i].colorType == 'borderTab')) {
                            field[i].colorVal = newBorderColor;
                        }
                        else if ((field[i].colorType == 'background') || (field[i].colorType == 'color')) {
                            field[i].colorVal = newColor;
                        }
                    }
                    $('.revupColorCntl', $this).data('field', field);

                    // clear the other fields - if any
                    var otherFields = $('.revupColorCntl', $this).data('otherFields');
                    if ((otherFields) && (otherFields.length > 0)) {
                        for (var i = 0; i < otherFields.length; i++) {
                            // convert the colorType into a array
                            // var cParts = otherFields[i].colorType.split(',');
                            var cParts = otherFields[i].colorType.split(',');
                            for (var j = 0; j < cParts.length; j++) {
                                cParts[j] = $.trim(cParts[j].toLowerCase());
                            }

                            if (cParts.indexOf('border') != -1) {
                                otherFields[i].$field.css('border-color', '');
                            }
                            if ((cParts.indexOf('bordertab') != -1) || (cParts.indexOf('clearbordertab') != -1)) {
                                otherFields[i].$field.css('border-top-color', '');
                                otherFields[i].$field.css('border-bottom-color', '');
                                otherFields[i].$field.css('border-left-color', '');
                                otherFields[i].$field.css('border-right-color', 'none');
                            }
                            if (cParts.indexOf('background') != -1) {
                                otherFields[i].$field.css('background-color', '');
                            }
                            if (cParts.indexOf('color') != -1) {
                                otherFields[i].$field.css('color', '');
                            }
                        }
                    }

                    break;
            } // end switch - command

            return;
        }

        function setColor(color)
        {
            var fieldsChangeVal = [];

            // does the border need changing
            var r, g, b;
            if ((changeParts.indexOf('border') != -1) || (changeParts.indexOf('bordertab') != -1)) {
                if (color.length == 4) {
                    r = parseInt(color.substring(1, 2), 16);
                    g = parseInt(color.substring(2, 3), 16);
                    b = parseInt(color.substring(3, 4), 16);
                }
                else {
                    r = parseInt(color.substring(1, 3), 16);
                    g = parseInt(color.substring(3, 5), 16);
                    b = parseInt(color.substring(5, 7), 16);
                }
                var hsl = rgbToHsl(r, g, b);

                var l = (100.0 * hsl.l) + ((hsl.l * 100.0) * (opts.borderChange / 100.0));
                if (l > 100.0) {
                    l = 100.0;
                }
                else if (l < 0.0) {
                    l = 0;
                }
                l += '%';
                var s = (hsl.s * 100.0) + '%';

                if (changeParts.indexOf('border') != -1) {
                    opts.$connectedField.css('border-color', 'hsl(' + (hsl.h * 360) + ', ' + s + ', ' + l + ')');
                }
                else {
                    opts.$connectedField.css('border-top-color', 'hsl(' + (hsl.h * 360) + ', ' + s + ', ' + l + ')');
                    opts.$connectedField.css('border-bottom-color', 'hsl(' + (hsl.h * 360) + ', ' + s + ', ' + l + ')');
                    opts.$connectedField.css('border-left-color', 'hsl(' + (hsl.h * 360) + ', ' + s + ', ' + l + ')');
                    opts.$connectedField.css('border-right-color', 'none');
                }

                if (opts.colorFor) {
                    var o = new Object();
                    o.colorFor = opts.colorFor;
                    if (changeParts.indexOf('border') != -1) {
                        o.colorType = "border";
                        o.colorVal = rgbToHex(opts.$connectedField.css('border-color'));
                    }
                    else {
                        o.colorType = "borderTab";
                        o.colorVal = rgbToHex(opts.$connectedField.css('border-top-color'));
                    }
                    //o.cssRule = o.cssRule;
                    fieldsChangeVal.push(o);
                }
            }

            // change for clearBorderTab
            if (changeParts.indexOf('clearbordertab') != -1) {
                opts.$connectedField.css('border-top-color', 'transparent');
                opts.$connectedField.css('border-bottom-color', 'transparent');
                opts.$connectedField.css('border-left-color', 'transparent');
                opts.$connectedField.css('border-right-color', 'none');

                if (opts.colorFor) {
                    var o = new Object();
                    o.colorType = "clearBorderTab";
                    o.colorFor = opts.colorFor;
                    o.colorVal = 'transparent';
                    fieldsChangeVal.push(o);
                }
            }

            // change the background color
            if (changeParts.indexOf('background') != -1) {
                opts.$connectedField.css('background-color', color);

                if (opts.colorFor) {
                    var o = new Object();
                    o.colorType = "background";
                    o.colorFor = opts.colorFor;
                    o.colorVal = rgbToHex(color);
                    //o.cssRule = o.cssRule;
                    fieldsChangeVal.push(o);
                }
            }

            // change the color
            if (changeParts.indexOf('color') != -1) {
                opts.$connectedField.css('color', color);

                if (opts.colorFor) {
                    var o = new Object();
                    o.colorType = "color";
                    o.colorFor = opts.colorFor;
                    o.colorVal = rgbToHex(color);
                    //o.cssRule = o.cssRule;
                    fieldsChangeVal.push(o);
                }
            }

            if (fieldsChangeVal.length > 0) {
                $('.revupColorCntl', $this).data('field', fieldsChangeVal);
            }

            // other fields to change
            var otherFieldsChangeVal = [];
            if ((opts.otherFields) && (opts.otherFields.length > 0)) {
                for (var i = 0; i < opts.otherFields.length; i++) {
                    // convert the colorType into a array
                    //var cParts = opts.otherFields[i].colorType.split(',');
                    var cParts = opts.otherFields[i].colorType.split(',');
                    for (var j = 0; j < cParts.length; j++) {
                        cParts[j] = $.trim(cParts[j].toLowerCase());
                    }

                    if ((cParts.indexOf('border') != -1) || (cParts.indexOf('bordertab') != -1)) {
                        if (color.length == 4) {
                            r = parseInt(color.substring(1, 2), 16);
                            g = parseInt(color.substring(2, 3), 16);
                            b = parseInt(color.substring(3, 4), 16);
                        }
                        else {
                            r = parseInt(color.substring(1, 3), 16);
                            g = parseInt(color.substring(3, 5), 16);
                            b = parseInt(color.substring(5, 7), 16);
                        }
                        var hsl = rgbToHsl(r, g, b);

                        var l = (hsl.l * 100) + ((hsl.l * 100.0) * (opts.borderChange / 100.0));
                        if (l > 100.0) {
                            l = 100.0;
                        }
                        else if (l < 0.0) {
                            l = 0;
                        }
                        l += '%';
                        var s = (hsl.s * 100) + '%';

                        if (cParts.indexOf('border') != -1) {
                            opts.otherFields[i].$field.css('border-color', 'hsl(' + hsl.h + ', ' + s + ', ' + l + ')');
                        }
                        else {
                            opts.otherFields[i].$field.css('border-top-color', 'hsl(' + hsl.h + ', ' + s + ', ' + l + ')');
                            opts.otherFields[i].$field.css('border-bottom-color', 'hsl(' + hsl.h + ', ' + s + ', ' + l + ')');
                            opts.otherFields[i].$field.css('border-left-color', 'hsl(' + hsl.h + ', ' + s + ', ' + l + ')');
                            opts.otherFields[i].$field.css('border-right-color', 'none');
                        }

                        if (opts.otherFields[i].colorFor) {
                            var o = Object();
                            o.colorFor = opts.otherFields[i].colorFor;
                            if (opts.otherFields[i].cssRule) {
                                o.cssRule = opts.otherFields[i].cssRule;
                            }
                            if (cParts.indexOf('border') != -1) {
                                o.colorType = "border";
                                o.colorVal = rgbToHex(opts.otherFields[i].$field.css('border-color'));
                            }
                            else {
                                o.colorType = "borderTab";
                                o.colorVal = rgbToHex(opts.otherFields[i].$field.css('border-top-color'));
                            }
                            o.$field = opts.otherFields[i].$field;
                            otherFieldsChangeVal.push(o);
                        }

                    }

                    if (cParts.indexOf('clearbordertab') != -1) {
                        opts.otherFields[i].$field.css('border-top-color', 'transparent');
                        opts.otherFields[i].$field.css('border-bottom-color', 'transparent');
                        opts.otherFields[i].$field.css('border-left-color', 'transparent');
                        opts.otherFields[i].$field.css('border-right-color', 'none');

                        if (opts.otherFields[i].colorFor) {
                            var o = Object();
                            o.colorFor = opts.otherFields[i].colorFor;
                            if (opts.otherFields[i].cssRule) {
                                o.cssRule = opts.otherFields[i].cssRule;
                            }
                            o.colorType = "clearBorderTab";
                            o.colorVal = 'transparent';
                            o.$field = opts.otherFields[i].$field;
                            otherFieldsChangeVal.push(o);
                        }
                    }

                    if (cParts.indexOf('background') != -1) {
                        opts.otherFields[i].$field.css('background-color', color);

                        if (opts.otherFields[i].colorFor) {
                            var o = Object();
                            o.colorFor = opts.otherFields[i].colorFor;
                            if (opts.otherFields[i].cssRule) {
                                o.cssRule = opts.otherFields[i].cssRule;
                            }
                            o.colorType = "background";
                            o.colorVal = color;
                            o.$field = opts.otherFields[i].$field;
                            otherFieldsChangeVal.push(o);
                        }
                    }

                    if (cParts.indexOf('color') != -1) {
                        opts.otherFields[i].$field.css('color', color);

                        if (opts.otherFields[i].colorFor) {
                            var o = Object();
                            o.colorFor = opts.otherFields[i].colorFor;
                            if (opts.otherFields[i].cssRule) {
                                o.cssRule = opts.otherFields[i].cssRule;
                            }
                            o.colorType = "color";
                            o.colorVal = color;
                            o.$field = opts.otherFields[i].$field;
                            otherFieldsChangeVal.push(o);
                        }
                    }
                }

                if (otherFieldsChangeVal.length > 0) {
                    $('.revupColorCntl', $this).data('otherFields', otherFieldsChangeVal);
                }
            }
        } // setColor

        // get the options, if any
        var opts = $.extend({}, jQuery.fn.revupColorCntl.defaults, options);

        var sTmp = [];
        var currentColor = opts.colorVal;

        // create the control
        sTmp.push('<div class="revupColorCntl">');
            sTmp.push('<input type="text" class="colorInput" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">');
            sTmp.push('<div class="colorSample"></div>');
        sTmp.push('</div>');
        $this.html(sTmp.join(''));

        var $colorSample = $('.colorSample', $this);
        var $colorInput = $('.colorInput', $this);

        // convert the colorType into a array
        var changeParts = opts.colorType.split(',');
        for (var i = 0; i < changeParts.length; i++) {
            changeParts[i] = $.trim(changeParts[i].toLowerCase());
        }

        // save the connectedField and colorType
        $('.revupColorCntl', $this).data('borderChange', opts.borderChange);
        $('.revupColorCntl', $this).data('connectedField', opts.$connectedField);
        $('.revupColorCntl', $this).data('colorType', opts.colorType);
        setColor(opts.colorVal);
        if (opts.otherFields.length > 0) {
            $('.revupColorCntl', $this).data('otherFields', opts.otherFields);
        }

        if (opts.$connectedField) {
            if (changeParts.indexOf('background') != -1) {
                $colorSample.css('background-color' , opts.$connectedField.css('background-color'));
                $colorInput.val(opts.$connectedField.css('backgroundColor'));
            }
            else if (changeParts.indexOf('color') != -1) {
                $colorSample.css('background-color' , rgbToHex(opts.$connectedField.css('color')));
                $colorInput.val(rgbToHex(opts.$connectedField.css('color')));
            }
        }

        $colorSample.spectrum({
            color: currentColor,
            showInput: true,
            preferredFormat: "hex",
            className: "full-spectrum",
            showInitial: true,
            change: function(c) {
                var newColorHsl = c.toHsl();

                // set the color
                setColor(c.toHexString());

                // trigger event
                $this.trigger({
                    type: 'revup.newColor',
                    newColor: c.toHexString(),
                });

                // update the current color
                currentColor = c.toHexString();
            },
            move: function(c) {
                $colorSample.css('background-color', c.toHexString());
                $colorInput.val(c.toHexString());
            },
            show: function(c) {
                currentColor = c.toHexString();
            },
            hide: function(c) {
                $colorSample.css('background-color', currentColor);
                $colorInput.val(currentColor);
            }

        });


        function rgbToHex(colVal)
        {
            if (colVal.search("rgb") == -1)
                return colVal;
            else {
                colVal = colVal.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
                function hex(x) {
                    return ("0" + parseInt(x).toString(16)).slice(-2);
                }
                return "#" + hex(colVal[1]) + hex(colVal[2]) + hex(colVal[3]);
            }
        }

        /**
         * from this web site
         *     form the spectrum plugin
         *
         * Converts an RGB color value to HSL. Conversion formula
         * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
         * Assumes r, g, and b are contained in the set [0, 255] and
         * returns h, s, and l in the set [0, 1].
         *
         * @param   Number  r       The red color value
         * @param   Number  g       The green color value
         * @param   Number  b       The blue color value
         * @return  Array           The HSL representation
         */
        function rgbToHsl(r, g, b){
            // Need to handle 1.0 as 100%, since once it is a number, there is no difference between it and 1
            // <http://stackoverflow.com/questions/7422072/javascript-how-to-detect-number-as-a-decimal-including-1-0>
            function isOnePointZero(n) {
                return typeof n == "string" && n.indexOf('.') != -1 && parseFloat(n) === 1;
            }

            // Check to see if string passed in is a percentage
            function isPercentage(n) {
                return typeof n === "string" && n.indexOf('%') != -1;
            }

            // Take input from [0, n] and return it as [0, 1]
            function bound01(n, max) {
                if (isOnePointZero(n)) { n = "100%"; }

                var processPercent = isPercentage(n);
                n = Math.min(max, Math.max(0, parseFloat(n)));

                // Automatically convert percentage into number
                if (processPercent) {
                    n = parseInt(n * max, 10) / 100;
                }

                // Handle floating point rounding errors
                if ((Math.abs(n - max) < 0.000001)) {
                    return 1;
                }

                // Convert into [0, 1] range if it isn't already
                return (n % max) / parseFloat(max);
            }

            r = bound01(r, 255);
            g = bound01(g, 255);
            b = bound01(b, 255);

            var max = Math.max(r, g, b), min = Math.min(r, g, b);
            var h, s, l = (max + min) / 2;

            if(max == min) {
                h = s = 0; // achromatic
            }
            else {
                var d = max - min;
                s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
                switch(max) {
                    case r: h = (g - b) / d + (g < b ? 6 : 0); break;
                    case g: h = (b - r) / d + 2; break;
                    case b: h = (r - g) / d + 4; break;
                }

                h /= 6;
            }

            return { h: h, s: s, l: l };

        }

        $colorInput.on('keyup', function(e) {
            var $this2 = $(this);

            var v = $this2.val();
            var p = /^#([0-9a-f]{3}|[0-9a-f]{6})$/i;
            if (p.test(v)) {
                $this2.removeClass('formatError');
                $this2.parent().removeClass('formatError');

                // update the sample
                $colorSample.css('background-color', v)

                // set the color in sample
                setColor(v);

                // update the current color
                currentColor = v
                $colorSample.spectrum('set', v);

                // trigger event
                $this.trigger({
                    type: 'revup.newColor',
                    newColor: v,
                });

            }
            else {
                $this2.addClass('formatError');
                $this2.parent().addClass('formatError');
            }
        })
        .on('focus', function(e) {
            if (opts.focusColor) {
                var $cntl = $(this).closest('.revupColorCntl');
                $cntl.css('border-color', opts.focusColor);
            }
        })
        .on('blur', function(e) {
            if (opts.notInFocusColor) {
                var $cntl = $(this).closest('.revupColorCntl');
                $cntl.css('border-color', opts.notInFocusColor);
            }
        });

        return this;
    }, // revupColorCntl

    jQuery.fn.revupColorCntl.defaults = {
        $connectedField: undefined,     // the selector of the field the control is connected to
        colorVal: '#fff',              // initial color of the control
        colorType: 'background',   // what parts need changing. Comma seperated string
                                        // valid parts: background, border, color
        borderChange: '-20',            // percentage change of the lightness for the borderColor
        focusColor: '',                 // border color on focus
        notInFocusColor: '',            // border color not in focus
        otherFields: [],                // array of object to also change each object has the following
                                        //      field: selector of field to change
                                        //      colorType:  the things to change
    }

    /*
     *
     * Revup Autocomplete - allow the user to enter part of a name and find the matches
     *
     */
    jQuery.fn.revupAutocomplete = function(options, arg1, arg2)
    {
        if (typeof options == 'string') {
            var command = options;
            var $revupSlideSwitch = $('.revupSlideSwitch', $(this));

            switch (command) {
                case 'disabled':
                    break;
            } // end switch - command

            return;
        }

        // get the options, if any
        var opts = $.extend({}, jQuery.fn.revupAutocomplete.defaults, options);

        var $this = $(this);
        var sTmp = [];

        // make sure a input field of type text
        var tagName = $this.prop('tagName');
        tagName = tagName != undefined ? tagName.toLowerCase() : '';
        var tagType = $this.prop('type');
        tagType = tagType != undefined ? tagType.toLowerCase() : '';
        if ((tagName != 'input') || (tagType != 'text')) {
            console.error('revupAutocomplete -> invalid tag for autocomplete.  tag: ' + tagName + ', tagType: ' + tagType);

            return;
        }

        // add the dropdown list
        //var $aLst = $('body').append('<div class="revupAutocompleteLst"><div>');
        //$aLst = $('.revupAutocompleteLst', $aLst.parent());

        // set aLst styling
        var w = $this.outerWidth(true) - 2;
        var h = $this.outerHeight();

        // style the input field
        $this.css('position', 'relative');
        $this.css('z-index', opts.zIndex);

        // position the lst
        var pos = $this.offset();

        // make sure there is a url to fetch the data from
        if (opts.url === '') {
            console.error('revupAutocomplete -> missing autocomplete url');

            return;
        }

        var fetchTimer = null;
        var $overlay = $();
        var $aLst = $();
        var bFetching = false;

        function fetchValues(val)
        {
            // build the url
            var url = opts.url;
            url += '?query=' + val;
            url += '&page_size=' + opts.numToFetch

            $this.trigger({
                type: 'revup.autocomplete.fetchingData',
                inputValue: val
            })

            // setthe fetching flag
            bFetching = true;

            // get the data
            $.ajax({
                url: url,
                type: 'GET'
            })
            .done(function(r) {
                // add the dropdown list
                if ($aLst.length == 0) {
                    $aLst = $('body').append('<div class="revupAutocompleteLst"><div>');
                    $aLst = $('.revupAutocompleteLst', $aLst.parent());

                    // size and position
                    $aLst.width(w);
                    $aLst.css('z-index', opts.zIndex);
                    $aLst.css('left', pos.left);
                    $aLst.css('top', pos.top + h);

                    $aLst
                        .on('click', '.entry', function(r) {
                            // get the values
                            $this.val($(this).attr('resultVal'));
                            if (opts.resultId != '') {
                                $this.attr('resultId', $(this).attr('resultId'));
                            }

                            // close the list
                            $overlay.trigger('revup.overlayClose');
                            $aLst.remove();
                            $aLst = $();

                            // set focus back to the input field
                            $this.focus();

                            // trigger and event that value changed
                            var tObj = {
                                type: 'revup.autocomplete.valueSelected',
                                selectedValue: $(this).attr('resultVal'),
                            };
                            if (opts.resultId != '') {
                                tObj.selectedId = $(this).attr('resultId');
                            }
                            $this.trigger(tObj);

                        });

                    // create the overlay
                    $overlay = $this.overlay(false, opts.zIndex - 1, true);
                    $this.one('revup.overlayClick', function() {
                        // hide the list
                        $aLst.remove();
                        $aLst = $();

                        // set focus to the input field
                        $this.focus();

                        // get rid of the overlay
                        $overlay.trigger('revup.overlayClose');
                    });
                }

                var sTmp = [];
                var results = r.results;
                if (results.length == 0) {
                    sTmp.push('<div class="noResults">' + opts.noResultMsg + '</div>');
                }
                else {
                    for(var i = 0; i < results.length; i++) {
                        // get the result id if there is one
                        var resultId = '';
                        if (opts.resultId != '') {
                            resultId = ' resultId=' + results[i][opts.resultId];
                        }

                        // get the result value
                        var resultDisplay = 'result value goes here';
                        if (opts.resultDisplay) {
                            resultDisplay = results[i][opts.resultDisplay];
                        }

                        sTmp.push('<div class="entry" style="z-index:' + opts.zIndex + ';position:relative;"' + resultId + ' resultVal="' + resultDisplay + '">');
                            sTmp.push(resultDisplay);
                        sTmp.push('</div>');
                    }
                }

                // update the list and display it
                $aLst.html(sTmp.join(''));
            })
            .fail(function(r) {
                console.error('results failed: ', r)
            })
            .always(function(r) {
                // clear fetching flag
                bFetching = false;
            })
        } // fetchValues

        // event handlers
        $this
            .on('input', function(e) {
                $input = $(this);

                var v = $input.val();
                if ((v.length >= opts.numCharToStart) && (!bFetching)) {
                    if (fetchTimer) {
                        clearTimeout(fetchTimer);
                    }
                    fetchTimer = setTimeout(
                        function() {
                            fetchValues(v)
                        }, opts.fetchDelay);
                }
                else if ($aLst.length > 0) {
                    // the results list is showing so close it
                    $aLst.remove();
                    $aLst = $();

                    // get rid of the overlay
                    $overlay.trigger('revup.overlayClose');
                }

                // clear the result id because not an exact match
                if (opts.resultId != '') {
                    $this.removeAttr('resultId');
                }

                $this.trigger({
                    type: 'revup.autocomplete.inputChanged',
                    inputValue: v
                })
            });
    },

    jQuery.fn.revupAutocomplete.defaults = {
        url: '',                // url of the auto complete handler
        numCharToStart: 3,      // minimum number of character needed to start searching
        fetchDelay: 250,        // milseconds to wait in typing to fetch data
        zIndex: 100,            // z-index of the dropdown list
        numToFetch: 10,         // num values to fetch from url
        resultDisplay: '',      // the field to display in the data
        resultId: '',           // the id to save
        noResultMsg: 'No Matches Found',
    },

    jQuery.fn.revupPasswordField = function(options, arg1, arg2)
    {
        let $this = $(this);

        //$(this).each(function() {
            // get the options, if any
            let opts = $.extend({}, jQuery.fn.revupPasswordField.defaults, options);
            let sTmp = [];

            // create the special character regular expression
            let specialCharReg = new RegExp('[' + opts.specialCharacters + ']');

            if (typeof options == 'string') {
                var command = options;
                var $revupPassword = $this.next();

                switch (command) {
                    case 'valid':
                        let numDiffParts = 0;

                        // test for parts
                        let val = $('.revupPasswordInput', $revupPassword).val();
                        if (val == undefined || val.length < opts.minPasswordLength) {
                            return false;
                        }

                        if (/[a-z]/.test(val)) {
                            numDiffParts++;
                        }
                        if (/[A-Z]/.test(val)) {
                            numDiffParts++;
                        }
                        if (/[0-9]/.test(val)) {
                            numDiffParts++;
                        }
                        if (specialCharReg.test(val)) {
                            numDiffParts++;
                        }


                        if (numDiffParts >=  3) {
                            return true;
                        }
                        else {
                            return false;
                        }
                        break;
                } // end switch - command

                return;
            }

            // get the width and height
            let fieldWidth = $this.outerWidth();
            let fieldHeight = $this.outerHeight();
            let allCss = $this.css(['borderImage', 'color', 'background', 'font', 'padding', 'margin', 'outline', '-webkit-appearance'])

            // build the style
            let sStyle = [];
            sStyle.push('-webkit-appearance:' + allCss['-webkit-appearance']);
            sStyle.push('height:' + fieldHeight + 'px');
            sStyle.push('width:' + fieldWidth + 'px');
            sStyle.push('color:' + allCss['color']);
            sStyle.push('background:' + allCss['background']);
            sStyle.push('font:' + allCss['font'].replace(/"/g, "'"));
            sStyle.push('padding:' + allCss['padding']);
            sStyle.push('margin:' + allCss['margin']);
            sStyle.push('outline:' + allCss['outline']);

            // get attributes
            let attrs = '';
            let passwd = '';
            if ($this.attr('placeholder')) {
                attrs += ' placeholder="' + $this.attr('placeholder') + '"';
            }
            if ($this.attr('value')) {
                attrs += ' value="' + $this.attr('value') + '"';
                passwd = $this.attr('value');
            }
            if (opts.maxPasswordLength != '') {
                attrs += ' maxlength="' + opts.maxPasswordLength + '"';
            }
            if ($this.attr('readonly')) {
                attrs += ' readonly=' + $this.attr('readonl');
            }
            if (attrs != '') {
                attrs += ' ';
            }

            let inputIconsWidth = 0;
            if (opts.bEnableShowPassword) {
                inputIconsWidth += 20;
            }
            if (opts.bEnableShowStrength) {
                if (inputIconsWidth != 0) {
                    inputIconsWidth += 5
                }

                inputIconsWidth += 20;
            }

            let inputWidth = "100%";
            if (!opts.bShowPasswordStrengthAbove) {
                inputWidth = "calc(100% - " + inputIconsWidth + "px)"
            }

            let passwdType = 'password';
            let bHideIcon = true;
            if (opts.bShowPasswordInitState) {
                passwdType = 'text';
                bHideIcon = false;
            }

            // helper functions
            let testStrength = (val, $parent) => {
                if (opts.bEnableShowStrength) {
                    let strIcon = '';
                    if (val.length == 0) {
                        strIcon = '';
                    }
                    else if (val.length < opts.minPasswordLength) {
                        strIcon = 'weak';
                    }
                    else {
                        let numDiffParts = 0;

                        // test for parts
                        if (/[a-z]/.test(val)) {
                            numDiffParts++;
                        }
                        if (/[A-Z]/.test(val)) {
                            numDiffParts++;
                        }
                        if (/[0-9]/.test(val)) {
                            numDiffParts++;
                        }
                        if (specialCharReg.test(val)) {
                            numDiffParts++;
                        }

                        if (numDiffParts >= 4) {
                            strIcon = 'strong';
                        }
                        else if (numDiffParts ==  3) {
                            strIcon = 'moderate';
                        }
                        else {
                            strIcon = 'weak';
                        }
                    }

                    // change the icon
                    if (strIcon == 'strong') {
                        $('.passwordStrengthDiv', $parent.next()).show();
                        $('.passwordStrengthDiv .icon-strong', $parent.next()).show();
                        $('.passwordStrengthDiv .icon-moderate', $parent.next()).hide();
                        $('.passwordStrengthDiv .icon-weak', $parent.next()).hide();
                    }
                    else if (strIcon == 'moderate') {
                        $('.passwordStrengthDiv', $parent.next()).show();
                        $('.passwordStrengthDiv .icon-strong', $parent.next()).hide();
                        $('.passwordStrengthDiv .icon-moderate', $parent.next()).show();
                        $('.passwordStrengthDiv .icon-weak', $parent.next()).hide();
                    }
                    else if (strIcon == 'weak') {
                        $('.passwordStrengthDiv', $parent.next()).show();
                        $('.passwordStrengthDiv .icon-strong', $parent.next()).hide();
                        $('.passwordStrengthDiv .icon-moderate', $parent.next()).hide();
                        $('.passwordStrengthDiv .icon-weak', $parent.next()).show();
                    }
                    else if (strIcon == '') {
                        $('.passwordStrengthDiv', $parent.next()).hide();
                        $('.passwordStrengthDiv .icon-strong', $parent.next()).hide();
                        $('.passwordStrengthDiv .icon-moderate', $parent.next()).hide();
                        $('.passwordStrengthDiv .icon-weak', $parent.next()).hide();
                    }
                }
            } // testStrength

            // enable rules in passoword
            let enableRules = (val, $parent) => {
                if (!opts.bShowRules) {
                    return;
                }

                // test for parts
                if (/[a-z]/.test(val)) {
                    $('.rule.lowerCase', $parent.next()).removeClass('disabled');
                }
                else {
                    $('.rule.lowerCase', $parent.next()).addClass('disabled');
                }

                if (/[A-Z]/.test(val)) {
                    $('.rule.upperCase', $parent.next()).removeClass('disabled');
                }
                else {
                    $('.rule.upperCase', $parent.next()).addClass('disabled');
                }

                if (/[0-9]/.test(val)) {
                    $('.rule.numChar', $parent.next()).removeClass('disabled');
                }
                else {
                    $('.rule.numChar', $parent.next()).addClass('disabled');
                }

                if (specialCharReg.test(val)) {
                    $('.rule.specialChar', $parent.next()).removeClass('disabled');
                }
                else {
                    $('.rule.specialChar', $parent.next()).addClass('disabled');
                }
            } // enableRules

            // build the new widget
            sTmp.push('<div class="revupPassword" style="' + sStyle.join(';') + '">');
                sTmp.push('<input type="' + passwdType + '" class="revupPasswordInput" style="margin:0;padding:0;width:' + inputWidth + ';"' + attrs + '>')

                if ((opts.bEnableShowPassword || opts.bEnableShowStrength) && opts.bShowPasswordStrengthAbove) {
                    let extra = ' style="top:' + opts.posPasswordStrengthTop + 'px;"';
                    sTmp.push('<div class="iconDiv"'+ extra +'>');
                }

                // see if show password strength
                if (opts.bEnableShowStrength) {
                    let extra = ''
                    if (opts.bEnableShowPassword) {
                        extra = ' style="margin-left: 5px;"';
                    }

                    sTmp.push('<div class="passwordStrengthDiv"'+ extra +'>');
                        sTmp.push('<div class="icon icon-weak"></div>')
                        sTmp.push('<div class="icon icon-moderate"></div>')
                        sTmp.push('<div class="icon icon-strong"></div>')
                    sTmp.push('</div>')
                }

                // see if hide/show password
                if (opts.bEnableShowPassword) {

                    sTmp.push('<div class="showPasswordDiv">');
                        if (bHideIcon) {
                            sTmp.push('<div class="icon icon-reveal" style="display:none;"></div>');
                            sTmp.push('<div class="icon icon-hide" style="display:block;"></div>');
                        }
                        else {
                            sTmp.push('<div class="icon icon-reveal" style="display:block;"></div>');
                            sTmp.push('<div class="icon icon-hide" style="display:none;"></div>');
                        }
                    sTmp.push('</div>')
                }

                if ((opts.bEnableShowPassword || opts.bEnableShowStrength) && opts.bShowPasswordStrengthAbove) {
                    sTmp.push('</div>');
                }

                // show the rules
                if (opts.bShowRules) {
                    sTmp.push('<div class="showRulesDiv">');
                        sTmp.push('<div class="rulesTitle">');
                            sTmp.push(opts.rulesMsg);
                        sTmp.push('</div>');
                        sTmp.push('<div class="rulesDiv">');
                            sTmp.push('<div class="leftSide">');
                                sTmp.push('<div class="rule lowerCase disabled">');
                                    sTmp.push('<div class="dot"></div>');
                                    sTmp.push('<div class="txt">One lowercase character</div>')
                                sTmp.push('</div>');
                                sTmp.push('<div class="rule upperCase disabled">');
                                    sTmp.push('<div class="dot"></div>');
                                    sTmp.push('<div class="txt">One uppercase character</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>')
                            sTmp.push('<div class="rightSide">');
                                sTmp.push('<div class="rule specialChar disabled">');
                                    sTmp.push('<div class="dot"></div>');
                                    sTmp.push('<div class="txt">One special character (' + opts.specialCharacters + ')</div>')
                                sTmp.push('</div>');
                                sTmp.push('<div class="rule numChar disabled">');
                                    sTmp.push('<div class="dot"></div>');
                                    sTmp.push('<div class="txt">One number</div>');
                                sTmp.push('</div>');
                            sTmp.push('</div>')
                    sTmp.push('</div>');
                }
            sTmp.push('</div>');

            // hide current field and add new field
            $this.hide();
            $this.after(sTmp.join(''));

            // set password
            testStrength(passwd, $this);

            // handle the input
            var $parent = $this;
            $this.next()
                .on('keydown', '.revupPasswordInput', function(e) {
                    // see if validity testing is enabled
                    if (opts.bEnableValidityTest) {
                        // test for parts
                        let valid = false;
                        let char = e.originalEvent.key;
                        if (char.length > 1) {
                            // special character
                            valid = true;
                        }
                        else if (/[a-z]/.test(char)) {
                            valid = true;
                        }
                        else if (/[A-Z]/.test(char)) {
                            valid = true;
                        }
                        else if (/[0-9]/.test(char)) {
                            valid = true;
                        }
                        else if (specialCharReg.test(char)) {
                            valid = true;
                        }

                        if (!valid) {
                            e.preventDefault();

                            return false;
                        }
                    }
                })
                .on('input', '.revupPasswordInput', function(e) {
                    let val = $(this).val();

                    // store in the orginal dom object
                    $parent.val(val);

                    // test strength
                    testStrength(val, $parent);

                    // enable the rules
                    enableRules(val, $parent);
                })
                .on('cut copy paste', function(e) {
                    if (!opts.bCutCopyPaste) {
                        e.preventDefault();
                    }
                })
                .on('click', '.showPasswordDiv', function(e) {
                    if ($('.icon-reveal',$(this)).is(":visible")) {
                        // hide the password
                        $('.icon-reveal', $parent.next()).hide();
                        $('.icon-hide', $parent.next()).show();
                        $('.revupPasswordInput', $parent.next()).prop('type', 'password')
                                                                .focus();
                    }
                    else {
                        // show the password
                        $('.icon-reveal', $parent.next()).show();
                        $('.icon-hide', $parent.next()).hide();
                        $('.revupPasswordInput', $parent.next()).prop('type', 'text')
                                                                .focus();
                    }
                })
        //})

        return this;
    } // revupPasswordField

    jQuery.fn.revupPasswordField.defaults = {
        bEnableValidityTest:        true,           // test if the validity of password should be checked
        bEnableShowPassword:        true,           // add icon to hide/show password
        bShowPasswordInitState:     false,          // the initial state of the show password, if displayed
        bEnableShowStrength:        true,           // show icon with password strength
        bShowPasswordStrengthAbove: false,          // show the password and/or strength icons above the input
        posPasswordStrengthTop:     '-45',          // number of pixels to move the icon
        bShowRules:                 false,          // show the rules info and state
        bCutCopyPaste:              true,           // if true allow cut, copy, paste
        rulesMsg:                   'Your password must be at least 8 characters in length and include 3 of the following rules:',
        showStrengthHandler:        undefined,      // external password strength function
        specialCharacters:          '-!@#$%^&*',    // allowed special characters
        minPasswordLength:          8,              // min length of password
        maxPasswordLength:          '',             // max length of password
    }

    /*
     * Radio Buttons
     */
    jQuery.fn.revupRadioBtn = function(options, arg1, arg2) {
        let $this = $(this);

        // get the options, if any
        let opts = $.extend({}, jQuery.fn.revupRadioBtn.defaults, options);

        // build the buttons to display
        let sTmp = [];
        let extraClass = '';
        if (opts.extraClass != '') {
            extraClass += ' ' + opts.extraClass;
        }
        extraClass += ' ' + opts.direction;
        sTmp.push('<div class="revupRadioBtnDiv' + extraClass + '" tabindex="-1">');
            for (let i = 0; i < opts.btns.length; i++) {
                sTmp.push('<div class="radioBtnDiv ' + opts.btns[i].value + '" btnValue="' + opts.btns[i].value + '" btnLabel="' + opts.btns[i].label + '">')
                    sTmp.push('<div class="radioBtn">');
                        sTmp.push('<div class="icon background icon-radio-button-back"></div>');
                        if (opts.btns[i].value == opts.initSelect) {
                            sTmp.push('<div class="icon unselected icon-radio-button-off" style="display:none;"></div>');
                            sTmp.push('<div class="icon selected icon-radio-button-on" style="display:inline-block;"></div>');
                        }
                        else {
                            sTmp.push('<div class="icon unselected icon-radio-button-off" style="display:inline-block;"></div>');
                            sTmp.push('<div class="icon selected icon-radio-button-on" style="display:none;"></div>');
                        }
                    sTmp.push('</div>');
                    sTmp.push('<div class="radioLabel">' + opts.btns[i].label + '</div>');
                sTmp.push('</div>')
            }
        sTmp.push('</div>');

        // display
        $this.html(sTmp.join(''));

        //$('.revupRadioBtnDiv', $this).attr('tabindex', -1);

        // add event handlers
        $('.revupRadioBtnDiv', $this)
            .on('click', '.radioBtnDiv', function(e) {
                let $btn = $(this);
                let $parent = $btn.parent().parent();

                // set focus on the parent
                $this.focus();

                // see if the active radio button
                if ($('.selected', $btn).is(':visible')) {
                    return;
                }

                // clear the radio buttons
                $('.radioBtn .unselected', $parent).css('display', 'inline-block');
                $('.radioBtn .selected', $parent).css('display', 'none');

                // select the clicked on button
                $('.unselected', $btn).css('display', 'none');
                $('.selected', $btn).css('display', 'inline-block');

                // trigger an event
                $this.trigger({type: 'revup.radioBtnSelected',
                               btnValue: $btn.attr('btnValue'),
                               btnLabel: $btn.attr('btnLabel')
                              });
            })
            .on('keydown', function(e) {
                console.log('key')
            })


    } // revupRadioBtn

    jQuery.fn.revupRadioBtn.defaults = {
        btns:       [{label: 'Button1', value: 'btn1'},     // buttons in this radio button group
                     {label: 'Button2', value: 'btn2'},     //  each entry is an object with
                     {label: 'Button3', value: 'btn3'}      //      label: what is display,
                    ],                                      //      value: value to return for selected button
        initSelect: 'btn1',                                 // the label of the initial selection
        direction:  'horizontal',                           // display direction, vertical or horizontal
        extraClass: '',                                     // extra class for these buttons
    } // revupRadioBtn - defaults
}(jQuery, window));
