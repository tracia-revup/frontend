//the purpose of this script is to remove any duplicate color variables in ../../static/css/stylesDefs.scss, and also
// point the duplicated color variables to the new variable in the specified SCSS files.
fs = require('fs');
var path = require("path");
var files = fs.readdirSync(path.join(__dirname, "../../static/css/apps/"));
var fileContents = {};
var fd;
var dsStoreIndex;
var SDcontents = fs.readFileSync(path.join(__dirname, "../../static/css/stylesDefs.scss"), {encoding: "utf-8"});
var splitByLines = SDcontents.split('\n');
var oldVarsWithoutComments = [];
var newVarsWithoutComments = [];
var pushtarget = oldVarsWithoutComments;
for (var j = 0; j < splitByLines.length; j++){
    if (splitByLines[j] === "// primary palette colors"){
        pushtarget = newVarsWithoutComments;
    }
    else if (splitByLines[j] === "//primary palette colors end"){
        break;
    }
    if (splitByLines[j].slice(0 , 2) !== '//' && splitByLines[j] !== ""){
        pushtarget.push(splitByLines[j]);
    }
}
var oldColor = oldVarsWithoutComments.join(" ").split(" ");
var newColor = newVarsWithoutComments.join(" ").split(" ");
var oldVarsObj = {};
for (var i = 0; i < oldColor.length; i++){
    if(oldColor[i][0] ===  "$" && oldColor[i + 1][0] === "#"){
        var tempKey = oldColor[i+1].replace(";", "").toLowerCase();
        if(!oldVarsObj[tempKey]){
            oldVarsObj[tempKey] = [];
        }
        oldVarsObj[tempKey].push(oldColor[i].replace(":", ""));
        i++;
    }
}
oldVarsObj["#e6e7e8"].unshift("$separatorBorder");
for(var i = 0; i < files.length; i++){
    if(files[i] == ".DS_Store"){
        dsStoreIndex= i;
    }
}
files.splice(dsStoreIndex, 1);
for(var f = 0; f < files.length; f++){
    fileContents[files[f]] = fs.readFileSync(path.join(__dirname, "../../static/css/apps/" + files[f]), "utf-8");
}
fileContents["revupWidgets.scss"] = fs.readFileSync(path.join(__dirname, "../../static/css/revupWidgets.scss"),
    {encoding: "utf-8"});
fileContents["styles.scss"] = fs.readFileSync(path.join(__dirname, "../../static/css/styles.scss"), "utf-8");
fileContents["stylesAcademic.scss"] = fs.readFileSync(path.join(__dirname,
    "../../static/academic/css/stylesAcademic.scss"), {encoding: "utf-8"});
files.push("revupWidgets.scss");
files.push("styles.scss");
files.push("stylesAcademic.scss");
for (var k = 0; k < files.length; k++){
    for (var hexColor in oldVarsObj){
        for(var i = 0; i < oldVarsObj[hexColor].length; i++){
            var regexSC = new RegExp("\\" + oldVarsObj[hexColor][i] + "(;)", "ig");
            fileContents[files[k]] = fileContents[files[k]].replace(regexSC, oldVarsObj[hexColor][0] + ";");
            var regexSp = new RegExp("\\" + oldVarsObj[hexColor][i] + "(\\s)", "ig");
            fileContents[files[k]] = fileContents[files[k]].replace(regexSp, oldVarsObj[hexColor][0] + " ");
        }
    }
}
for (var i = 0; i < files.length; i++){
    var filePath;
    if (files[i] === "styles.scss" || files[i] === "revupWidgets.scss") {
        filePath = "../../static/css/" + files[i];
    }

    else if (files[i] === "stylesAcademic.scss"){
        filePath = "../../static/academic/css/" + files[i];
    }
    else {
        filePath = "../../static/css/apps/" + files[i];
    }

    fs.writeFile(filePath, fileContents[files[i]], {encoding: "utf-8"}, function(err) {
        if (err) {
            console.log(err);
    }
    });
}
