var fs = require('fs');
var path = require("path");
var files = fs.readdirSync(path.join(__dirname, "work/frontend/frontend/static/css/apps"));
var fileContents = {};
for(var f = 0; f < files.length; f++)
{
    fileContents[files[f]] = fs.readFileSync(path.join(__dirname, "work/frontend/frontend/static/css/apps/" + files[f]),
		{encoding: "utf-8"});
}
fileContents["revupWidgets.scss"] = fs.readFileSync(path.join(__dirname,
	"work/frontend/frontend/static/css/revupWidgets.scss"), {encoding: "utf-8"});
fileContents["styles.scss"] = fs.readFileSync(path.join(__dirname, "work/frontend/frontend/static/css/styles.scss"),
	{encoding: "utf-8"});
fileContents["stylesAcademic.scss"] = fs.readFileSync(path.join(__dirname,
	"work/frontend/frontend/static/academic/css/stylesAcademic.scss"), {encoding: "utf-8"});
files.push("stylesAcademic.scss");
files.push("revupWidgets.scss");
files.push("styles.scss");
var remainingFloatingHex ={};
for (var i = 0; i < files.length; i++){
	remainingFloatingHex[files[i]] = {};
	var filesLines = fileContents[files[i]].split("\n");
	for (var k = 0; k < filesLines.length; k++){
		var regex = /#[0-9a-f]{6}|#[0-9a-f]{3}/gi;
		var toMatch = filesLines[k].match(regex);
		if((toMatch || []).length > 0){
			remainingFloatingHex[files[i]][toMatch[0]] = ("line " + (k + 1));
		}
	}
}
fs.writeFile("remainingFloatsList/remainingFloats.txt", "OLD COLORS NOT FOUND IN NEW PALETTE:\n"
	+ JSON.stringify(remainingFloatingHex, null, 2), function(err) {
    if(err){
        console.log(err);
    }
});
