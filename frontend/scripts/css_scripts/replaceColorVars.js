//the purpose of this script is to replace any existing old color variables of all scss files in ../../static/css/apps/,
// that overlap with the new color palette block in ../../static/css/stylesDefs.scss
var fs = require('fs');
var path = require("path");
var files = fs.readdirSync(path.join(__dirname, "../../static/css/apps/"));
var fileContents = {};
var fd;
var dsStoreIndex;
for(var i = 0; i < files.length; i++){
  if(files[i] == ".DS_Store"){
    dsStoreIndex= i;
  }
}
files.splice(dsStoreIndex, 1);
for(var f = 0; f < files.length; f++){
    fileContents[files[f]] = fs.readFileSync(path.join(__dirname, "../../static/css/apps/" + files[f]), "utf-8");
}
fileContents["revupWidgets.scss"] = fs.readFileSync(path.join(__dirname, "../../static/css/revupWidgets.scss"),
    {encoding: "utf-8"});
fileContents["styles.scss"] = fs.readFileSync(path.join(__dirname, "../../static/css/styles.scss"), "utf-8");
files.push("revupWidgets.scss");
files.push("styles.scss");
var contents = fs.readFileSync(path.join(__dirname, "../../static/css/stylesDefs.scss"), "utf-8");
var splitByLines = contents.split('\n');
var oldVarsWithoutComments = [];
var newVarsWithoutComments = [];
var pushtarget = oldVarsWithoutComments;
for (var j = 0; j < splitByLines.length; j++){
    if (splitByLines[j] === "// primary palette colors"){
        pushtarget = newVarsWithoutComments;
    }
    else if (splitByLines[j] === "//primary palette colors end"){
        break;
    }
    if (splitByLines[j].slice(0 , 2) !== '//' && splitByLines[j] !== ""){
        pushtarget.push(splitByLines[j]);
    }
}
var oldColor = oldVarsWithoutComments.join(" ").split(" ");
var newColor = newVarsWithoutComments.join(" ").split(" ");
var oldColorVars = {};
var newColorVars = {};
for (var i = 0; i < oldColor.length; i++){
    if(oldColor[i][0] ===  "$"){
        oldColorVars[oldColor[i]] = oldColor[i + 1];
        i++;
    }
}
for (var k = 0; k < newColor.length; k++){
    if(newColor[k][0] ===  "$"){
        newColorVars[newColor[k]] = newColor[k + 1];
        i++;
    }
}
var replacements = {};
for (var oldvar in oldColorVars){
    for (var newvar in newColorVars){
        if (oldColorVars[oldvar] === newColorVars[newvar]){
          replacements[oldvar.replace(":", ";").toLowerCase()] =  newvar.replace(":", ";").toLowerCase();
        }
    }
}
for (var e = 0; e < files.length; e++){
    for(var toReplace in replacements){
        var regex = new RegExp(`(${toReplace.replace("$", "\\$")})`, "g");
        fileContents[files[e]] = fileContents[files[e]].replace(regex, replacements[toReplace]);
    }
  }
for (var i = 0; i < files.length; i++){
    var filePath;
    if (files[i] === "styles.scss" || files[i] === "revupWidgets.scss"){
      filePath = "../../static/css/" + files[i];
    }
    else {
      filePath = "../../static/css/apps/" + files[i];
    }

    fs.writeFile(filePath, fileContents[files[i]], {encoding: "utf-8"}, function(err) {
       if(err){
           console.log(err);
       }
     })
  }
