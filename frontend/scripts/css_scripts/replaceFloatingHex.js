//the purpose of this script is to replace any floating hex in the specified directories with variables that are defined
// in ../../static/css/stylesDefs.scss
var fs = require('fs');
var path = require("path");
var files = fs.readdirSync(path.join(__dirname, "../../static/css/apps/"));
var fileContents = {};
var fd;
var dsStoreIndex;
var SDcontents = fs.readFileSync(path.join(__dirname, "../../static/css/stylesDefs.scss"), {encoding: "utf-8"});
var splitByLines = SDcontents.split('\n');
var commentsRemoved = [];
for (var j = 0; j < splitByLines.length; j++){
    if (splitByLines[j].slice(0 , 2) !== '//' && splitByLines[j] !== ""){
        commentsRemoved.push(splitByLines[j]);
    }
}
var varsArr = commentsRemoved.join(" ").split(" ");
var varsObj = {};
for (var i = 0; i < varsArr.length; i++){
    if(varsArr[i][0] ===  "$" && varsArr[i + 1][0] === "#"){
        varsObj[varsArr[i].replace(":", "")] = varsArr[i + 1].replace(";", "");
        i++;
    }
}
for(var i = 0; i < files.length; i++){
    if(files[i] == ".DS_Store"){
        dsStoreIndex= i;
    }
}
files.splice(dsStoreIndex, 1);
for(var f = 0; f < files.length; f++){
    fileContents[files[f]] = fs.readFileSync(path.join(__dirname, "../../static/css/apps/" + files[f]), "utf-8");
}
fileContents["revupWidgets.scss"] = fs.readFileSync(path.join(__dirname, "../../static/css/revupWidgets.scss"),
    {encoding: "utf-8"});
fileContents["styles.scss"] = fs.readFileSync(path.join(__dirname, "../../static/css/styles.scss"), "utf-8");
fileContents["stylesAcademic.scss"] = fs.readFileSync(path.join(__dirname,
    "../../static/academic/css/stylesAcademic.scss"), {encoding: "utf-8"});
files.push("revupWidgets.scss");
files.push("styles.scss");
files.push("stylesAcademic.scss");
function myReplace(string, toReplace, replacement){
    if(replacement === "$whiteTextfff"){
    }
    return string.split(toReplace).join(replacement);
}
for (var k = 0; k < files.length; k++){
    for (var varName in varsObj){
        fileContents[files[k]] = myReplace(fileContents[files[k]], varsObj[varName], varName);
        fileContents[files[k]] = myReplace(fileContents[files[k]], varsObj[varName].toUpperCase(), varName);
        fileContents[files[k]] = myReplace(fileContents[files[k]], "#fff;", "$whiteText;");
    }
}
for (var i = 0; i < files.length; i++){
    var filePath;
    if (files[i] === "styles.scss" || files[i] === "revupWidgets.scss"){
        filePath = "../../static/css/" + files[i];
    }
    else if (files[i] === "stylesAcademic.scss"){
        filePath = "../../static/academic/css/" + files[i];
    }
    else {
        filePath = "../../static/css/apps/" + files[i];
    }

    fs.writeFile(filePath, fileContents[files[i]], {encoding: "utf-8"}, function(err) {
        if(err){
            console.log(err);
        }
    })
}
