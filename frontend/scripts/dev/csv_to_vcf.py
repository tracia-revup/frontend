#!/usr/bin/env python
"""This script will accept a csv file (in our standard format) and
  export a vcard file.

  To execute this script from main "frontend" directory:
    python -m frontend.scripts.dev.csv_to_vcf <path to csv file>
"""
from io import open
import sys

from frontend.libs.utils.general_utils import DictReaderInsensitive


VCARD_TEMPLATE = """
BEGIN:VCARD
VERSION:3.0
PRODID:-//Apple Inc.//Mac OS X 10.10.5//EN
N:{last_name};{first_name};;;
FN:{first_name} {last_name}
TEL;type=CELL;type=VOICE;type=pref:{phone}
EMAIL;type=INTERNET;type=HOME;type=pref:{email}
ADR;type=HOME;type=pref:;;{street};{city};{state_abbr};{zipcode};
END:VCARD
"""


def main(filepath):
    out_filepath = ".".join(filepath.split(".")[0:-1]) + ".vcf"
    in_file = open(filepath, "rb")
    out_file = open(out_filepath, "wb")

    in_csv = DictReaderInsensitive(in_file)
    for line in in_csv:
        vcard = VCARD_TEMPLATE.format(
            first_name=line.get("first name", ""),
            last_name=line.get("last name", ""),
            phone=line.get("phone number", ""),
            email=line.get("email address", ""),
            street=line.get("address street", ""),
            city=line.get("address city", ""),
            state_abbr=line.get("address state", ""),
            zipcode=line.get("address postal code", ""),
        )
        out_file.write(vcard)

    in_file.close()
    out_file.close()


if __name__ == "__main__":
    main(sys.argv[1])