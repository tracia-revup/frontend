#!/usr/bin/env python
"""This script will accept a csv file and print out NTEE categories and NTEE
sub-categories.

To execute this script from main "frontend" directory:
python -m frontend.scripts.dev.ntee_csv_to_dict -i <path to csv file>
"""
import argparse
from collections import defaultdict
import csv
from os.path import dirname, exists, isfile, join
import pprint
import sys
import html

p_printer = pprint.PrettyPrinter(indent=4)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i", "--input", required=True, help="Path to csv input file")
    args = parser.parse_args()

    if not isfile(args.input):
        print("Invalid input file")
        sys.exit(1)

    # Open the necessary files
    with open(args.input, "r") as csv_file:
        # Read the csv file
        csv_reader = csv.DictReader(csv_file)
        csv_fieldnames = csv_reader.fieldnames
        # Get the name of the csv column that has NTEE codes
        ntee_code_field = csv_fieldnames[0]
        # Get the name of the csv column that has NTEE code names
        ntee_name_field = csv_fieldnames[1]

        main_categories = []
        sub_categories = defaultdict(list)

        for row in csv_reader:
            ntee_code = row.get(ntee_code_field)
            # Use html safe characters in the ntee name
            ntee_name = html.escape(row.get(ntee_name_field))
            # If the NTEE code is a single character then it is a primary
            # category. If the NTEE code has more than one character, the
            # first character is the primary category and the remaining
            # characters is the sub-category.
            category_code = ntee_code[0]
            sub_category_code = ntee_code[1:] if len(ntee_code) > 1 else None
            if sub_category_code:
                sub_categories[category_code].append(
                    (sub_category_code, ntee_name))
            else:
                main_categories.append(
                    (ntee_code, ntee_name))

        # Print the primary categories
        print("Primary NTEE categories:\n")
        p_printer.pprint(main_categories)

        # Print the sub categories
        print("\nNTEE sub-categories: \n")
        p_printer.pprint(sub_categories)


if __name__ == "__main__":
    main()
