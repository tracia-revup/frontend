const paths = require('./config/paths');

module.exports = {
    babelrcRoots: [
        ".",
        "emerge_nfp",
        paths.emergeAppSrc,
    ]
}
