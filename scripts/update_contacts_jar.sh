#!/usr/bin/env bash
set -euo pipefail

# Rebrand CloudSponge applet as per https://www.cloudsponge.com/developer

if [ 3 -ne $# ]
then
    echo "Usage: update_contacts_jar.sh [keystore] [password] [destination]"
    exit 1
fi

tmpdir=`mktemp -d 2>/dev/null || mktemp -d -t 'tmpdir'`
cd $tmpdir
wget https://api.cloudsponge.com/objects/ContactsApplet_signed.jar
unzip ContactsApplet_signed.jar
rm ContactsApplet_signed.jar
rm META-INF/*.{RSA,SF} # strip previous signature
sed -i 's/Application-Name: CloudSponge/Application-Name: RevUp Contact Import\nApplication-Library-Allowable-Codebase: *.revup.com api.cloudsponge.com/' META-INF/MANIFEST.MF
zip -r ContactsApplet_signed.jar .
jarsigner -tsa http://timestamp.digicert.com -keystore $1 -storepass $2 ContactsApplet_signed.jar server
jarsigner -verify ContactsApplet_signed.jar
cp ContactsApplet_signed.jar $3
rm -r $tmpdir
