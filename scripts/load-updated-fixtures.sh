#!/bin/bash

set -ex

ROTATE_OLD_ENTITIES_SCRIPT='
db.getCollectionNames().forEach(function(coll) {
if (coll.slice(-9) === "_entities") {
    db[coll + "_old"].drop();
    db[coll].renameCollection(coll + "_old");
}
});
'

cd `dirname "$0"`/..

mongo revup --eval "$ROTATE_OLD_ENTITIES_SCRIPT"

mongorestore --gzip
