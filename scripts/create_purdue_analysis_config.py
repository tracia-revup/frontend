"""Usage:
    python manage.py runscript scripts.create_purdue_analysis_config --script-args $ACCOUNT_ID

    Where $ACCOUNT_ID is the id of education account the analysis config should
    be associated with.
"""
from django.db import transaction

from frontend.apps.analysis.models import (
    AnalysisConfigTemplate, DataConfig, FeatureConfig, DataSetConfig)
from frontend.apps.campaign.models import Account


@transaction.atomic
def run(account_id):
    dc = DataConfig.objects.get(title='Base Data Configuration')
    base_dc_id = dc.pk
    dc.copy(new_label='purdue')
    purdue_dc_id = dc.pk
    assert base_dc_id != purdue_dc_id
    dsc = DataSetConfig.objects.get(title='Purdue Data Configuration')
    dc.data_set_configs.add(dsc)

    fc_purdue_giving = FeatureConfig.objects.get(
        title='Purdue Giving')
    fc_purdue_alum = FeatureConfig.objects.get(
        title='Purdue Alum Lookup')
    fc_purdue_summary = FeatureConfig.objects.get(
        title='Purdue Summary')

    acct = Account.objects.get(pk=account_id)
    act = AnalysisConfigTemplate.objects.get(
        title='default analysis config for academia')
    ac = act.generate_analysis_config(account=acct, title='Purdue analysis')
    ac.data_config = dc
    ac.feature_configs.add(fc_purdue_giving, fc_purdue_alum, fc_purdue_summary)
    try:
        fc_summary = ac.feature_configs.get(title='Summary signals')
    except FeatureConfig.DoesNotExist:
        pass
    else:
        ac.feature_configs.remove(fc_summary)
    ac.save()
