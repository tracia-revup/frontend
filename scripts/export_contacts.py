
"""Usage:
    python manage.py runscript scripts.copy_contacts --script-args $SRC_USER_ID $DEST_USER_ID

    $SRC_USER_ID is the id of the user whose contacts should be copied.

    $USER_ID is the id of the user that should be set as the owner of the new
    contacts.
"""

from itertools import chain
from logging import getLogger
import sys

from django.core.serializers import serialize

from frontend.apps.contact.models import (
    Address, EmailAddress, Organization, AlmaMater, ImageUrl, ExternalProfile,
    PhoneNumber, RawContact, Contact
)


LOGGER = getLogger(__name__)


def run(from_user_id):
    from_user_id = int(from_user_id)

    to_serialize = chain(
        Contact.objects.filter(user_id=from_user_id, parent=None),
        Contact.objects.filter(user_id=from_user_id).exclude(parent=None),
        Address.objects.filter(contact__user_id=from_user_id),
        EmailAddress.objects.filter(contact__user_id=from_user_id),
        Organization.objects.filter(contact__user_id=from_user_id),
        AlmaMater.objects.filter(contact__user_id=from_user_id),
        ImageUrl.objects.filter(contact__user_id=from_user_id),
        ExternalProfile.objects.filter(contact__user_id=from_user_id),
        PhoneNumber.objects.filter(contact__user_id=from_user_id),
        RawContact.objects.filter(contact__user_id=from_user_id),
    )
    sys.stdout.write(serialize('json', to_serialize))
