"""
Creates special files necessary for identity resolution from the dump from
fused_entities made in the `dump_entities` script.
"""
import argparse
import gzip
from operator import itemgetter
from os.path import join as pjoin
from os import makedirs

import bson
from funcy import *
import pandas as pd


def grouper(record):
    inits = record['inits']
    if isinstance(inits, list):
        inits = inits.pop()
    return inits


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-o', '--out', default='dump', type=str)
    args = parser.parse_args()
    output_dir = args.out
    makedirs(pjoin(output_dir, 'fused_nodes'), exist_ok=True)

    data = bson.decode_file_iter(
        gzip.open(pjoin(output_dir, 'revup', 'fused_entities.bson.gz'), 'rb'))

    grouped_data = group_by(grouper, data)

    for initials, records in grouped_data.items():
        df = pd.DataFrame(records)
        df.to_msgpack(pjoin(output_dir, 'fused_nodes',
                            '{}.msgpack'.format(initials)))


if __name__ == '__main__':
    main()
