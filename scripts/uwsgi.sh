#!/bin/bash
if [ "${UWSGI_ASYNC:-0}" -eq 1 ]; then
    exec uwsgi uwsgiasync.ini
else
    exec uwsgi uwsgi.ini
fi
