"""Utility Script to be run to ensure all indexes are available in Mongo"""

from django.conf import settings
from pymongo import MongoClient, collection, ASCENDING

CLIENT = MongoClient(settings.MONGODB_HOST)
DB = CLIENT[settings.MONGODB_NAME]

def create_indexes():
    """This function creates the indexes"""
    # indexes for records collection
    collection.Collection(DB, 'records').ensure_index(
        [("LASTNAME", ASCENDING), ("GIVENNAME", ASCENDING)],
        background=True)
    collection.Collection(DB, 'records').ensure_index(
        [("TRANS_DATE", ASCENDING)],
        background=True)

    # indexes on misp collection
    collection.Collection(DB, 'misp').ensure_index(
        [("LASTNAME", ASCENDING), ("GIVENNAME", ASCENDING)],
        background=True)
    collection.Collection(DB, 'misp').ensure_index(
        [("CFS_Date", ASCENDING)],
        background=True)
    collection.Collection(DB, 'misp').ensure_index(
        [("ElectionEntityEID", ASCENDING)],
        background=True)

    # index for nonprofit and educational contributions
    collection.Collection(DB, 'nonprofit_contributions').ensure_index(
        [("surname", ASCENDING)], background=True)
    collection.Collection(DB, 'nonprofit_contributions').ensure_index(
        [("forename", ASCENDING)], background=True)
    collection.Collection(DB, 'academic_contributions').ensure_index(
        [("surname", ASCENDING)], background=True)
    collection.Collection(DB, 'academic_contributions').ensure_index(
        [("forename", ASCENDING)], background=True)

    # indexes on location collection
    collection.Collection(DB, 'locations').ensure_index(
        [("loc_key", ASCENDING)], background=True)
