from itertools import chain
import logging
from time import sleep
import sys

from celery.app.control import Control

from frontend.celery import app

logger = logging.getLogger(__name__)
logger.propagate = False

logger.setLevel(logging.INFO)
loggerHandler = logging.StreamHandler(sys.stdout)
loggerHandler.setLevel(logging.INFO)
loggerFormatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s')
loggerHandler.setFormatter(loggerFormatter)
logger.addHandler(loggerHandler)

MAX_RETRIES = 10


def on_connection_error(exc, interval):
    logger.info('Connection Error: {0!r}. Retry in {1}s.'.format(
        exc, interval))


def ensure(obj, func):
    return app.pool.connection.ensure(obj, func, errback=on_connection_error,
                                      max_retries=MAX_RETRIES)


def run():
    control = Control(app)
    inspect = control.inspect(app=app)
    # Find active queues running on all workers
    check_active_queues = ensure(inspect, inspect.active_queues)
    active_queues = check_active_queues()
    if not active_queues:
        logger.info("No workers or queues to cancel found")
        return

    active_workers = list(active_queues.keys())

    cancel_consumer = ensure(control, control.cancel_consumer)
    for worker, queues in active_queues.items():
        for queue in queues:
            if queue['name'] == '_heartbeat':
                continue
            cancel_consumer(queue['name'], destination=[worker])

    inspect = control.inspect(app=app, destination=active_workers)
    check_active_tasks = ensure(inspect, inspect.active)
    while True:
        active = check_active_tasks()
        if not active:
            break
        iter(active.values())
        running_jobs = list(chain.from_iterable(iter(active.values())))
        if running_jobs:
            logger.info("{} jobs running: {}".format(
                len(running_jobs),
                ", ".join(job["name"] for job in running_jobs)))
            sleep(10)
        else:
            break

    logger.info("No running jobs")
