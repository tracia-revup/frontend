#!/usr/bin/env bash

set -e
set -x

HEROKU_APP=$1

if [ -z "$HEROKU_APP" ]; then
    echo "ERROR: Missing argument: Heroku app name"
    exit 1
fi

[[ ! -s "$(git rev-parse --git-dir)/shallow" ]] || git fetch --unshallow

# Enable maintenance mode to stop writes to database for safe migration
heroku maintenance:on --app ${HEROKU_APP}

# Stop celery worker dynos to ensure no more writes to database for safe migration
heroku scale worker=0 --app ${HEROKU_APP} || echo "WARNING: Problems scaling worker dyno"
heroku scale analysis_heavy=0 --app ${HEROKU_APP} || echo "WARNING: Problems scaling worker dyno"

# Deploy to heroku
git push -f git@heroku.com:${HEROKU_APP}.git $CIRCLE_SHA1:refs/heads/master

# Run migration
heroku run --app ${HEROKU_APP} --exit-code -- python manage.py migrate --no-input

# Turn small worker back on
heroku scale worker=1 --app ${HEROKU_APP}

# Disable maintenance mode
heroku maintenance:off --app ${HEROKU_APP}

