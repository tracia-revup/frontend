#!/usr/bin/env bash

exec > >(awk '{ print strftime("[%Y-%m-%d %H:%M:%S]"), $0 }') 2>&1

set -e
set -x

BUILD_APP=$1
HEROKU_APP=$2

if [ -z "$BUILD_APP" ]; then
    echo "ERROR: Missing argument: Heroku build app name"
    exit 1
fi

if [ -z "$HEROKU_APP" ]; then
    echo "ERROR: Missing argument: Heroku app name"
    exit 1
fi

[[ ! -s "$(git rev-parse --git-dir)/shallow" ]] || git fetch --unshallow

# Push code to heroku app X to compile slug of application
git push -f git@heroku.com:${BUILD_APP}.git ${CIRCLE_SHA1:-HEAD}:refs/heads/master

# Disable hirefire scaling
python scripts/toggle_hirefire_manager.py -t ${HIREFIRE_API_TOKEN} ${HEROKU_APP} off

# Stop celery worker dynos to ensure no more writes to database for safe migration
heroku scale worker=0 --app ${HEROKU_APP} || echo "WARNING: Problems scaling worker dyno"
heroku scale heavy=0 --app ${HEROKU_APP} || echo "WARNING: Problems scaling heavy dyno"
heroku scale background=0 --app ${HEROKU_APP} || echo "WARNING: Problems scaling background dyno"
heroku scale analysis_single_contact=0 --app ${HEROKU_APP} || echo "WARNING: Problems scaling single contact analysis dyno"

# Enable maintenance mode to stop writes to database for safe migration
heroku maintenance:on --app ${HEROKU_APP}

# Deploy to heroku
# Promote build from BUILD_APP to HEROKU_APP
heroku pipelines:promote --app ${BUILD_APP} --to ${HEROKU_APP}

# Run migration
heroku run --app ${HEROKU_APP} --exit-code -- python manage.py migrate --no-input

# Disable maintenance mode
heroku maintenance:off --app ${HEROKU_APP}

# Restore any tasks that were killed by the shutdown
heroku run --app ${HEROKU_APP} python manage.py runscript frontend.scripts.restore_unacknowledged_jobs

# Turn small worker back on
heroku scale worker=1 analysis_single_contact=1 --app ${HEROKU_APP}

# Enable hirefire scaling
python scripts/toggle_hirefire_manager.py -t ${HIREFIRE_API_TOKEN} ${HEROKU_APP} on
