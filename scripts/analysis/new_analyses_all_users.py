"""Run a new network analysis for each user that already has an analysis.

To run:
open a shell (./manage.py shell)
import this script
    (from scripts.analysis.new_analyses_all_users import script)
execute it, using queue=True to queue analyses (script(queue=True))
"""

from django.conf import settings
from pymongo import MongoClient

import frontend.apps.analysis.analyses.legacy as network
from frontend.apps.authorize.models import RevUpUser, Analysis
from frontend.apps.campaign.models import Event
from frontend.apps.contact.tasks import network_search_task


def script(queue=False):
    """
    Old function for adding network_search tasks to the queue for all users
    """
    for user in RevUpUser.objects:
        if Analysis.objects(user=user).count() > 0:
            print("New analysis needed for", user)
            if queue:
                print("Queuing analysis for", user)
                network_search_task.delay(user.id, user.get_default_event().id)


def new_user_analysis(user_id, event_id):
    """
    This function will run a network_search independent of the celery backend
    for one user.
    """
    network.network_search(user_id, event_id, is_task=False)


def get_latest_analyses_user_event(
        campaign_ids=None, cutoff=None, query=None):
    """
    Returns user and event of most recent analysis for each unique
    user and event. Optionally takes a list of campaign ids to consider (since
    some campaigns may not need to have analyses re-run), a cutoff date that
    analyses run since do not need to be re-run, and a query dict for network
    results to further narrow which analyses must be re-run.
    """
    re_run = {}
    if not query:
        query = {}

    client = MongoClient(settings.MONGODB_HOST)
    db = client[settings.MONGODB_NAME]

    if campaign_ids:
        events = Event.objects.filter(account__in=campaign_ids).values('id')
    else:
        events = Event.objects.all().values('id')
    event_ids = [event['id'] for event in events]

    if cutoff:
        user_events = Analysis.objects.filter(
            event__in=event_ids, timestamp__lt=cutoff).order_by(
                'user', 'event').distinct(
                    'user', 'event').values(
                        'user', 'event')
    else:
        user_events = Analysis.objects.filter(event__in=event_ids).order_by(
            'user', 'event').distinct(
                'user', 'event').values('user', 'event')

    analyses = []
    for user_event in user_events:
        analysis = Analysis.objects.filter(user=user_event['user'],
                                           event=user_event['event'],
                                           status='COMPLETE').latest(
                                               'modified')
        query['_analysis'] = analysis.id
        results_count = db.network_result.find(query).count()
        if results_count:
            if cutoff:
                if analysis.modified < cutoff:
                    analyses.append((analysis.modified, user_event['user'],
                                     user_event['event']))
            else:
                analyses.append((analysis.modified, user_event['user'],
                                 user_event['event']))

    analyses = sorted(analyses, reverse=True)
    return analyses


def new_analysis_all_nonprofit_old_schema(campaign_ids=None, cutoff=None):
    """ This function is a utility to run new analysis for all user/events
    returned from get_latest_analyses_user_event"""

    query = {"nonprofit_contribs": {"$ne": []}}
    user_events = get_latest_analyses_user_event(cutoff=cutoff,
                                                 query=query,
                                                 campaign_ids=campaign_ids)
    for timestamp, user, event in user_events:
        new_user_analysis(user, event)
