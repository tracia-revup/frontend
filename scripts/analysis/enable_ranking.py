"""Add a user group to each member of a given account.

This is meant for use with django-waffle that allows for feature flipping.
By adding the group to the user, they will gain access to the feature.

The feature this script was created for is the new ranking page.
"""
from django.contrib.auth.models import Group

from frontend.apps.campaign.models import Account


def script(account, group_name="New Ranking"):
    if isinstance(account, int):
        account = Account.objects.get(id=account)

    group = Group.objects.get(name=group_name)

    for seat in account.seats.assigned().select_related("user"):
        seat.user.groups.add(group)
