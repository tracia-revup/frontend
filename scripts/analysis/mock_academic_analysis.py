
import random

from frontend.apps.analysis.models import Analysis


SCHOOLS = [
    "Engineering", "Psychology", "Arts & Humanities", "Biological Sciences",
    "Civil Engineering", "Physics"
]

GRAD_YEARS = (1965, 2014)


def script(academic_account):
    # Grab the most recent analysis
    analysis = Analysis.objects.all().order_by("-modified").first()
    print(analysis.id)
    analysis.account = academic_account
    analysis.save()

    partition = analysis.partition_set.partition_configs.first()
    # Grab the first 50 results from the first partition
    results = analysis.results.filter(partition=partition)\
                              .select_related("contact")\
                              .order_by("-score")[0:50]
    # Delete any results that aren't in that 50 (to keep things small)
    analysis.results.exclude(id__in=(r.id for r in results)).delete()

    for result in results:
        r_ks = result.key_signals
        key_signals = {
            "giving": r_ks.get("giving"),
            "score": r_ks.get("score"),
            "normalized_score": r_ks.get("normalized_score"),
            "name": result.contact.name,
            "school": random.choice(SCHOOLS),
            "grad_year": random.randrange(GRAD_YEARS[0], GRAD_YEARS[1])
        }
        result.key_signals = key_signals
        result.save()

    u = academic_account.account_head
    s = u.seats.filter(account=academic_account).first()
    u.current_seat = s
    e = u.events.filter(account=academic_account).first()
    u.current_event = e
    u.current_analysis = analysis
    u.save()
    print(u, u.id)
