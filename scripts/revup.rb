#
# You will need to:
#
# - install https://github.com/rainforestapp/rainforest-cli before this will work
# - change "xxx" on line 9 of this file to your API token
#

# Get your key from https://app.rainforestqa.com/settings/integrations
RAINFOREST_API_TOKEN = "96a4d7ad04241e639fddd7677ba759e9"

# This is the filter that will be run, if you want to change it
FILTER_ID = 4107


#
#
#
require "net/http"
require "json"

install_check = system("rainforest -v")
raise "Please install rainforest cli from https://github.com/rainforestapp/rainforest-cli" unless $?.exitstatus == 0

uri = URI("https://app.rainforestqa.com/api/1/folders/#{FILTER_ID}?page=1&page_size=100")
req = Net::HTTP::Get.new(uri)
req["CLIENT_TOKEN"] = RAINFOREST_API_TOKEN

res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) {|http| http.request(req) }

if res.code != '200'
  raise "The API isn't working; probably check your API token. Error: #{res.body.inspect}"
end

tests = JSON.parse(res.body)['tests']

tests.each do |test|
  next unless test['state'] == 'enabled'
  puts "#{test['id']}\t#{test['title']}\t#{test['state']}"

  `rainforest run #{test['id']} --token #{RAINFOREST_API_TOKEN}`
end
