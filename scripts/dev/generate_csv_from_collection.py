"""
Use a mongo collection to generate a csv using the mongo records as
source material for the contacts.

Usage:
    python manage.py runscript scripts.dev.generate_csv_from_collection [--script-args $COUNT [$COLLECTION]]

    Where $COUNT is the number of contacts to add to the csv
"""
from csv import DictWriter
from io import open

from django.conf import settings


COLLECTION_MAP = dict(
    purdue_bio={
            "add1": "address street",
            "city": "address city",
            "company": "employer",
            "email": "email address",
            "gname": "first name",
            "hphone": "phone number",
            "lname": "last name",
            "mi": "middle name",
            "mobphn": "phone2 number",
            "occupat": "occupation",
            "state": "address state",
            "title": "occupation",
            "zip": "address postal code"
    },
    st={
            "LASTNAME": "last name",
            "EmployerName": "employer",
            "GIVENNAME": "first name",
            "SAT_State": "address state",
            "SAT_City": "address city",
            "SAT_Street": "address street",
            "SAT_Zip": "address postal code",
            "Occupation": "occupation"
    }
)


def run(count=100, collection="purdue_bio"):
    db_collection = settings.MONGO_DB[collection]
    c_map = COLLECTION_MAP[collection]

    # Name format: purdue_bio_100.csv
    f = open("{}_{}.csv".format(collection, count), "wb")
    writer = DictWriter(f, list(c_map.values()))
    writer.writeheader()

    names = set()

    # Iterate over the items in the collection and extract them into csv rows
    for item in db_collection.find().limit(count):
        row = {}
        for k, v in c_map.items():
            value = item.get(k, "")
            if value == r"\N":
                continue
            row[v] = value

        name_key = (row["first name"], row["last name"])
        if name_key in names:
            continue
        else:
            names.add(name_key)

        if row:
            writer.writerow(row)

    f.close()
