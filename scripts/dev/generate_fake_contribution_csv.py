
from csv import DictWriter

from faker import Factory as FakeFactory

faker = FakeFactory.create()


def make_record():
    return {
        "First Name": faker.first_name(),
        "Middle Initial": None,
        "Last Name": faker.last_name(),
        "Occupation": faker.job(),
        "Employer": faker.company(),
        "Street": faker.street_address(),
        "City": faker.city(),
        "State": faker.state(),
        "Zip": faker.postalcode(),
        "Received On": faker.date(),
        "Amount": faker.pyint(),
    }


def generate_csv(filename, count):
    record = make_record()
    with open(filename, 'w') as f:
        csvwriter = DictWriter(f, list(record.keys()))
        csvwriter.writeheader()
        csvwriter.writerow(record)
        for _ in range(1, int(count)):
            csvwriter.writerow(make_record())


if __name__ == '__main__':
    import sys
    generate_csv(sys.argv[1], sys.argv[2])
