from django.db import transaction

from frontend.apps.call_time.factories import CallTimeContactFactory
from frontend.apps.contact.factories import ContactFactory, ContactNotesFactory


def _build_contacts(count, user, ct_account, notes_account):
    parents = []
    children = []
    with transaction.atomic():
        for i in range(count):
            parent = ContactFactory.build(
                contact_type="merged", user=user, email_addresses=[],
                phone_numbers=[], addresses=[],
                needs_attention=True, is_primary=False)

            child1 = ContactFactory.create(first_name=parent.first_name,
                                           last_name=parent.last_name,
                                           user=user)
            child2 = ContactFactory.create(first_name=parent.first_name,
                                           last_name=parent.last_name,
                                           user=user)

            child_ids = [child1.id, child2.id]
            parent.attention_payload = {'demerge': child_ids}
            parent.save()

            if ct_account:
                CallTimeContactFactory.create(contact=parent,
                                              account=ct_account)
            if notes_account:
                ContactNotesFactory.create(contact=parent,
                                           account=notes_account)

            parents.append(parent.id)
            children.extend(child_ids)
    return parents, children


def account_contacts(account, count=10):
    user = account.account_user
    parents, children = _build_contacts(count, user, account, account)
    print("Parents:", parents)
    print("Children:", children)


def user_contacts(user, count=10):
    account = user.current_seat.account
    parents, children = _build_contacts(count, user, None, account)
    print("Parents:", parents)
    print("Children:", children)



