import csv
from io import open

from faker import Factory as FakeFactory

faker = FakeFactory.create()


def make_contact():
    return {
        "first name": faker.first_name(),
        "middle name": None,
        "last name": faker.last_name(),
        "name prefix": faker.prefix(),
        "name suffix": faker.suffix(),
        "occupation": faker.job(),
        "employer": faker.company(),
        "department": None,
        "address street": faker.street_address(),
        "address street2": None,
        "address po box": None,
        "address city": faker.city(),
        "address state": faker.state(),
        "address postal code": faker.postalcode(),
        "address country": 'US',
        "address type": 'Home',
        "address2 street": None,
        "address2 street2": None,
        "address2 po box": None,
        "address2 city": None,
        "address2 state": None,
        "address2 postal code": None,
        "address2 country": None,
        "address2 type": None,
        "phone number": faker.phone_number(),
        "phone type": 'cell',
        "phone2 number": None,
        "phone2 type": None,
        "phone3 number": None,
        "phone3 type": None,
        "email address": faker.email(),
        "email type": 'personal',
        "email2 address": None,
        "email2 type": None,
        "alma mater name": None,
        "alma mater major": None,
        "alma mater degree": None,
        "alma mater2 name": None,
        "alma mater2 major": None,
        "alma mater2 degree": None,
    }


def generate_csv(filename, contact_count):
    contact = make_contact()
    with open(filename, 'w') as f:
        csvwriter = csv.DictWriter(f, list(contact.keys()))
        csvwriter.writeheader()
        csvwriter.writerow(contact)
        for _ in range(1, int(contact_count)):
            csvwriter.writerow(make_contact())


if __name__ == '__main__':
    import sys
    generate_csv(sys.argv[1], sys.argv[2])

