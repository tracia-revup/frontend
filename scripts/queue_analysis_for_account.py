"""
Queue analysis with analysis config for everyone with an active seat on an
account. One of the primary use-cases for this is to be able to generate new
analysis for everyone using a new non-default analysis config, and aren't ready
to show the analysis to everyone yet.

Note: This queues the analysis on the background queue, which has more workers
than the default queue, and increases the size of the batches to process per
task. Only use this script for entity analysis so we don't blow up the cluster.

Usage:
    python manage.py runscript scripts.queue_analysis_for_account --script-args $ANALYSIS_CONFIG_ID $ACCOUNT_ID

    $ANALYSIS_CONFIG_ID id of analysis config to use to generate analysis.

    $ACCOUNT_ID is the id of the account to operate on.
"""

from frontend.apps.analysis.tasks import perform_analysis_task
from frontend.apps.campaign.models import Account
from frontend.apps.seat.models import Seat


BATCH_SIZE_OVERRIDE = 200

FEATURE_FLAGS = {
    'Analysis Name Expansion': False,
    'Analysis Alias Expansion': True,
    'Analysis Parallelization': True,
    'Analysis Zips Update': True,
    'FEC Use Daily Ids': False,
    'Entity Analysis Enabled': False
}


def iter_account_user_ids(account_id):
    '''Generator that yields the user_id of every user with an active seat, as
    well as the id of the account user.
    '''
    yield Account.objects.filter(id=account_id).values_list('account_user',
                                                            flat=True).get()
    user_ids = Seat.objects.filter(
        account_id=account_id).assigned().values_list('user', flat=True)
    for user_id in user_ids:
        yield user_id


def queue_analysis_for_account(analysis_config_id, account_id):
    '''Queue new analysis using specified analysis config for everyone with an
    active seat on given account.
    '''
    user_ids = iter_account_user_ids(account_id)
    for user_id in user_ids:
        queue_analysis(analysis_config_id, account_id, user_id)


def queue_analysis(analysis_config_id, account_id, user_id,
                   batch_size_override=BATCH_SIZE_OVERRIDE):
    '''Queue an analysis for specified user on specified account using the
    given analysis config (even if it isn't the current default).

    This queues the analysis on the background queue, and increases the size of
    the batch to process. No RevUpTask is created for this analysis.
    '''
    return perform_analysis_task.apply_async(
        args=(analysis_config_id, user_id, account_id),
        kwargs=dict(
            batch_size_override=batch_size_override,
            queue_override='background',
            skip_revup_task=True,
            force_new_analysis=True,
            feature_flags=FEATURE_FLAGS,),
        queue='background',
    )


def run(analysis_config_id, account_id):
    queue_analysis_for_account(int(analysis_config_id), int(account_id))
