
import argparse

import requests

DOMAIN = 'https://api.hirefire.io'
LIST_APPLICATIONS = '/applications'
LIST_APP_MANAGERS = '/applications/{}/managers'
SPECIFIC_MANAGER = '/managers/{}'


def set_manager_state(headers, manager_id, state):
    payload = {"manager[enabled]": str(state).lower()}

    r = requests.patch(DOMAIN + SPECIFIC_MANAGER.format(manager_id),
                       headers=headers, params=payload)
    assert r.json()['manager']['enabled'] == state


def main():
    parser = argparse.ArgumentParser(
        description='Toggle hirefire scaling managers for '
                    'a heroku application')
    parser.add_argument('-t', '--api-token', dest='api_token')
    parser.add_argument('-w', '--worker', action='append')
    parser.add_argument('-x', '--exclude', action='append')
    parser.add_argument('heroku_app_name')
    # add manager name filter
    parser.add_argument('state', choices=['on', 'off'])
    ns = parser.parse_args()
    state_to_set = ns.state == 'on'

    headers = {'Accept': 'application/vnd.hirefire.v1+json',
               'Content-Type': 'application/json',
               'Authorization': 'Token {}'.format(ns.api_token)}

    # get list of applications on hirefire
    r = requests.get(DOMAIN + LIST_APPLICATIONS, headers=headers)
    
    # get application id for our application
    application_id = None
    for application in r.json()['applications']:
        if application['name'] == ns.heroku_app_name:
            application_id = application['id']
            break

    assert application_id is not None

    # get managers for our application
    r = requests.get(DOMAIN + LIST_APP_MANAGERS.format(application_id),
                     headers=headers)

    # set state on each manager
    for manager in r.json()['managers']:
        if ns.worker and manager['name'] not in ns.worker:
            continue
        if ns.exclude and manager['name'] in ns.exclude:
            continue
        set_manager_state(headers, manager['id'], state_to_set)


if __name__ == '__main__':
    main()
