"""
Change the active analysis for everyone on an account to one generated using a
different analysis config. It can be used to set the active analysis to entity
analysis for a rollout, or to quickly roll back to non-entity analysis results.

Usage:
    python manage.py runscript scripts.set_active_analysis_for_account --script-args $ANALYSIS_CONFIG_ID $ACCOUNT_ID

    $ANALYSIS_CONFIG_ID id of analysis config that was used to generate the
    analysis we want to switch everyone to.

    $ACCOUNT_ID is the id of the account to operate on.
"""

from frontend.apps.analysis.api.views import ContactSetAnalysisContactResultsViewSet
from frontend.apps.analysis.models import Analysis
from frontend.apps.authorize.models import RevUpUser
from frontend.apps.campaign.models import Account
from frontend.apps.contact_set.models import ContactSet
from frontend.apps.seat.models import Seat


def iter_account_user_ids(account_id):
    '''Generator that yields the user_id of every user with an active seat, as
    well as the id of the account user.
    '''
    yield Account.objects.filter(id=account_id).values_list('account_user',
                                                            flat=True).get()
    user_ids = Seat.objects.filter(
        account_id=account_id).assigned().values_list('user', flat=True)
    for user_id in user_ids:
        yield user_id


def set_active_analysis_for_account(analysis_config_id, account_id):
    '''Set active analysis for everyone with an active seat on the account
    specified with one that was generated using the given analysis config.
    '''
    user_ids = iter_account_user_ids(account_id)
    for user_id in user_ids:
        set_active_analysis_for_user(analysis_config_id, account_id, user_id)
        print("User {} done".format(user_id))


def set_active_analysis_for_user(analysis_config_id, account_id, user_id):
    '''Set the active analysis for a specific user to the most recently
    generated analysis using the given analysis config, on the specified
    account.
    '''
    print(
        "Setting active analysis config for user {} on account {} to {}".format(
            user_id, account_id, analysis_config_id))
    # Find the most recent successful analysis generated using the specified
    # analysis config for the user.
    analysis = Analysis.objects.filter(
        user_id=user_id, analysis_config_id=analysis_config_id,
        account_id=account_id,
        status=Analysis.STATUSES.complete).order_by('-modified').first()
    if analysis is None:
        # This can happen if no analysis has been created for the user, or the
        # user doesn't have any contacts.
        print("No new analysis for user {} found. skipping".format(user_id))
        return
    RevUpUser.objects.filter(id=user_id).update(current_analysis=analysis)
    ContactSet.bulk_update_analysis(analysis)
    # Invalidate cache so the ranking page shows the new analysis.
    ContactSetAnalysisContactResultsViewSet.invalidate_cache(analysis.user)


def run(analysis_config_id, account_id):
    set_active_analysis_for_account(int(analysis_config_id), int(account_id))
