"""
Regenerate `contact_composite_ids.json` file, re-calculating composite ids for
every contact with a different user id.

The purpose of this script is to enable dumping updated entity fixtures from
different environments, without requiring new contact fixtures to be created.
This simplifies the loading process for developers by not requiring they load
new contacts every time, and prevents the build up of junk contacts that will
never get a score.

This script is necessary because the current (as of the time this is being
written) `contact_fixtures.json` and `contact_composite_ids.json` came from the
production environment, and the composite ids in the
`contact_composite_ids.json` won't match against any records in staging's
`person_entities`. To work around this issue, the fixture contacts were loaded
into a staging user, and this script was written to re-calculate the composite
ids with the staging user's id.

Usage:
    python manage.py runscript regenerate_fixture_composite_ids --script-args $USER_ID

    $USER_ID id of the user the fixture contacts were loaded into.

    Pre-requisites:
    The fixture contacts must be loaded into a user in the env from which you
    wish to dump entities. The fixture contacts must then be exported to the
    backend via kafka. This script can then be run to generate the new
    `contact_composite_ids.json` file, and then the `dump_entities` script can
    finally be run.
"""

import json
from frontend.apps.contact.models import Contact


FIXTURE_PATH = 'dump/contact_fixtures.json'
COMPOSITE_IDS_PATH = 'dump/contact_composite_ids.json'


def run(user_id):
    composite_ids = set()
    with open(FIXTURE_PATH, 'r') as fh:
        fixtures = json.load(fh)
    for fixture in fixtures:
        if fixture['model'] != 'contact.contact':
            continue
        composite_id = Contact._calc_composite_id(
            user_id,
            fixture['fields']['contact_id'],
            fixture['fields']['contact_type'])
        if composite_id:
            composite_ids.add(composite_id)

    with open(COMPOSITE_IDS_PATH, 'w') as fh:
        json.dump(list(composite_ids), fh)
