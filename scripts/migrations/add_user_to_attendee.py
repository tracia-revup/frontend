"""Migrate the RevUpUser object into existing Attendees.

To run:
open a shell (./manage.py shell)
import this script
    (from scripts.migrations.add_user_to_attendee import script)
execute it. Use save=True to commit changes (script(save=True))
"""

from frontend.apps.campaign.models import Attendee


def script(save=False):
    for attendee in Attendee.objects:
        attendee.user_id = attendee.contact.user_id

        print(attendee, attendee.user_id)
        if save:
            attendee.save()