"""
Discovers all entity records and contribution records associated with the
contacts dumped in the `export_top_100_contacts` management command, and runs
`mongodump` for those records.
"""
from logging import getLogger
import argparse
import json
import pathlib
import subprocess
from os.path import join as pjoin
from os import makedirs

from funcy import *
from pymongo import MongoClient
import yaml


LOGGER = getLogger(__name__)


CONFIG = '''
fused_ids:
  #discovery_key: node_quality
  collection: fused_entities
  contains:
    - fec_entity_ids
    - misp_entity_ids
    - municipal_entity_ids
    - non_profit_entity_ids
    - real_estate_entity_ids
    - opencorp_entity_ids
    - dbusa_entity_ids
    - sec_entity_ids
    - client_entity_ids

fec_entity_ids:
  discovery_key: fec_daily_id
  collection: fec_entities
  contains:
    - fec_contrib_ids

misp_entity_ids:
  discovery_key: misp_id
  collection: misp_entities
  contains:
    - misp_contrib_ids

municipal_entity_ids:
  discovery_key: municipal_id
  collection: municipal_entities
  contains:
    - municipal_contrib_ids

non_profit_entity_ids:
  discovery_key: non_profit_id
  collection: non_profit_entities
  contains:
    - non_profit_contrib_ids

real_estate_entity_ids:
  discovery_key: real_estate_id
  collection: real_estate_entities
  contains:
    - real_estate_contrib_ids

opencorp_entity_ids:
  discovery_key: opencorp_id
  collection: opencorp_entities
  contains:
    - opencorp_contrib_ids

dbusa_entity_ids:
  discovery_key: dbusa_id
  collection: dbusa_entities
  contains:
    - dbusa_contrib_ids

sec_entity_ids:
  discovery_key: sec_id
  collection: sec_entities
  contains:
    - sec_contrib_ids

client_entity_ids:
  discovery_key: client_data.Nd3e5__lmnd_rds_fndtn._id
  collection: client_entities__Nd3e5__lmnd_rds_fndtn
  contains:
    - client_contrib_ids

fec_contrib_ids:
  discovery_key: record_links
  collection: fec_daily

misp_contrib_ids:
  discovery_key: record_links
  collection: misp_v3

municipal_contrib_ids:
  discovery_key: record_links
  collection: municipal

real_estate_contrib_ids:
  discovery_key: record_links
  collection: real_estate

non_profit_contrib_ids:
  discovery_key: record_links
  collection: non_profit

opencorp_contrib_ids:
  discovery_key: record_links
  collection: opencorp

dbusa_contrib_ids:
  discovery_key: record_links
  collection: dbusa

sec_contrib_ids:
  discovery_key: record_links
  collection: sec

client_contrib_ids:
  discovery_key: record_links
  collection: client_contribs__Nd3e5__lmnd_rds_fndtn
'''


def discover_entity_ids(db, gconfig, composite_ids):
    fused_ids = get_fused_ids_from_person_entities(db, composite_ids)
    return _discover_entity_ids(db, gconfig, 'fused_ids', fused_ids)


def get_fused_ids_from_person_entities(db, composite_ids):
    """Query PersonEntities that map to input composite ids and extract fused
    entity ids from those person entities.
    """
    coll = db.person_entities
    results = coll.find({'composite_id': {'$in': composite_ids},
                         'node_quality': {'$exists': True}},
                        {'node_quality': 1})
    return list(set(flatten(mapcat(itervalues, pluck('node_quality', results)))))


def _discover_entity_ids(db, gconfig, key, ids, result=None):
    """Recursive function to discover entity ids

    This function uses the configuration in CONFIG to recursively discover ids
    of entities/records to later dump.
    """
    if result is None:
        result = {}
    result[key] = ids
    config = gconfig[key]
    child_map = {get_in(gconfig, [child_key, 'discovery_key']): child_key
                 for child_key in config.get('contains', [])}
    if not child_map:
        return result

    projection = zipdict(child_map.keys(), repeat(1))

    records = db[config['collection']].find({'_id': {'$in': ids}}, projection)
    if len(child_map) > 1:
        records = list(records)

    for disc_key, child_key in child_map.items():
        child_ids = compact(list(set(
            mapcat(rpartial(get_in, disc_key.split('.')), records))))

        result = _discover_entity_ids(
            db, gconfig, child_key, child_ids, result)
    return result


def dump_records(mongo_uri, output_dir, collection_name, record_ids,
                 dryrun=False):
    qryfile = pjoin(output_dir, '{}_ids.json'.format(collection_name))
    with open(qryfile, 'w') as fh:
        json.dump({'_id': {'$in': record_ids}}, fh)

    cmd = ['mongodump',
           '--gzip',
           '-o', output_dir,
           '--uri', mongo_uri,
           '-c', collection_name,
           '--queryFile', qryfile]
    if dryrun:
        print('Dump command: ' + ' '.join(cmd))
        return
    subprocess.run(cmd, check=True)


def main():
    parser = argparse.ArgumentParser(
        description=("Script to dump entity and contribution records "
                     "associated with input composite ids. Meant to be used "
                     "to create dev env fixtures, and to be run after the "
                     "`export_top_100_contacts` command."))
    parser.add_argument(
        '-d', '--dry-run',
        action='store_true', default=False)
    parser.add_argument(
        '-u', '--uri', type=str,
        help='MongoDB URI (default: %(default)s)',
        default='mongodb://engine:password@localhost:27017/revup')
    parser.add_argument(
        '-o', '--out', default='dump', type=str,
        help='Directory where mongodb dumps will be '
             'saved (default: %(default)s)')
    args = parser.parse_args()
    output_dir = args.out
    try:
        makedirs(output_dir, exist_ok=True)
    except FileExistsError as err:
        return parser.error(str(err))

    composite_ids = []
    composite_id_files = distinct(invoke(
        pathlib.Path(output_dir).glob('contact_composite_ids*.json'),
        'resolve'))
    for path in composite_id_files:
        with path.open() as fh:
            composite_ids.extend(json.load(fh))
    if not composite_ids:
        return parser.error("Found no contact composite ids.")
    client = MongoClient(args.uri)
    db = client.get_default_database()
    gconfig = yaml.load(CONFIG)
    result = discover_entity_ids(db, gconfig, composite_ids)
    for key, ids in result.items():
        dump_records(args.uri, output_dir,
                     get_in(gconfig, [key, 'collection']),
                     ids, args.dry_run)


if __name__ == '__main__':
    main()
