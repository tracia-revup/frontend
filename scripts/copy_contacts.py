"""Usage:
    python manage.py runscript scripts.copy_contacts --script-args $SRC_USER_ID $DEST_USER_ID

    $SRC_USER_ID is the id of the user whose contacts should be copied.

    $USER_ID is the id of the user that should be set as the owner of the new
    contacts.
"""

from logging import getLogger

from django.db import transaction

from frontend.apps.contact.models import (
    Address, EmailAddress, Organization, AlmaMater, ImageUrl, ExternalProfile,
    PhoneNumber, RawContact, Contact
)


LOGGER = getLogger(__name__)


def _bulk_copy(contact_id_map, qs, batch_size=1000):
    attribs = qs.iterator()
    model = qs.model

    def _collect(attribs):
        to_create = []
        for _ in range(batch_size):
            try:
                attrib = next(attribs)
            except StopIteration:
                break
            old_parent = attrib.contact_id
            new_parent = contact_id_map[old_parent]
            attrib.pk = None
            attrib.contact_id = new_parent
            to_create.append(attrib)
        return to_create

    while 1:
        to_create = _collect(attribs)
        if to_create:
            model.objects.bulk_create(to_create)
        else:
            break


@transaction.atomic
def run(from_user_id, to_user_id):
    from_user_id = int(from_user_id)
    to_user_id = int(to_user_id)
    contact_id_map = {}

    def _copy_contacts(contacts):
        for contact in contacts.iterator():
            old_id = contact.pk
            old_locations = list(contact.locations.all())
            contact.pk = None
            if contact.parent_id:
                if contact.parent_id == old_id:
                    contact.parent_id = None
                else:
                    old_parent = contact.parent_id
                    new_parent = contact_id_map[old_parent]
                    contact.parent_id = new_parent
            contact.user_id = to_user_id
            contact.composite_id = Contact.calc_composite_id(contact)
            contact.import_record = None
            contact.save()
            if old_locations:
                contact.locations = old_locations
            contact_id_map[old_id] = contact.pk

    LOGGER.info("Copying parentless contacts")
    top_contacts = Contact.objects.filter(user_id=from_user_id, parent=None)
    _copy_contacts(top_contacts)

    LOGGER.info("Copying subcontacts")
    sub_contacts = Contact.objects.filter(user_id=from_user_id).exclude(parent=None)
    _copy_contacts(sub_contacts)

    LOGGER.info("Copying Addresses")
    _bulk_copy(contact_id_map,
               Address.objects.filter(contact__user_id=from_user_id))
    LOGGER.info("Copying Emails")
    _bulk_copy(contact_id_map,
               EmailAddress.objects.filter(contact__user_id=from_user_id))
    LOGGER.info("Copying Organizations")
    _bulk_copy(contact_id_map,
               Organization.objects.filter(contact__user_id=from_user_id))
    LOGGER.info("Copying AlmaMaters")
    _bulk_copy(contact_id_map,
               AlmaMater.objects.filter(contact__user_id=from_user_id))
    LOGGER.info("Copying ImageUrl")
    _bulk_copy(contact_id_map,
               ImageUrl.objects.filter(contact__user_id=from_user_id))
    LOGGER.info("Copying ExternalProfile")
    _bulk_copy(contact_id_map,
               ExternalProfile.objects.filter(contact__user_id=from_user_id))
    LOGGER.info("Copying PhoneNumber")
    _bulk_copy(contact_id_map,
               PhoneNumber.objects.filter(contact__user_id=from_user_id))
    LOGGER.info("Copying RawContact")
    _bulk_copy(contact_id_map,
               RawContact.objects.filter(contact__user_id=from_user_id))
