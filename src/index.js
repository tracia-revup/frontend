import React from "react";
import ReactDOM from "react-dom";
import _ from "lodash";
import Root from 'emerge_nfp';

const [mountNode, mountArgs] = (() => {
  // Get the DOM element we mount the app on
  let node = document.getElementById("app");
  // Extract data parameters from element to pass to our app.
  let mArgs =  _.mapValues(node.dataset,
                           val => /^\d+$/.test(val) ? parseInt(val) : val);
  // Delete the data parameters from the element so they aren't passed through
  // and added on to every react element.
  for (let name of _.filter(node.getAttributeNames(),
                            (name_) => {_.startsWith(name_, 'data-')})) {
    node.removeAttribute(name);
  }
  return [node, mArgs];
})();

ReactDOM.render(
  <Root {...mountArgs} />,
  mountNode);
