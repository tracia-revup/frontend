const sass = require('node-sass');

module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    copy: {
      main: {
        expand: true,
        cwd: 'frontend/static/',
        src: ['img/**', 'js/**', 'fonts/**', 'samples/**', 'help/**/**', 'admin/**', 'django_extensions/**', 'rest_framework/**', 'jars/**', 'csvFormat/**', 'contract/**'],
        dest: 'frontend/assets/'
      },
      academic: {
        expand: true,
        cwd: 'frontend/static/academic/',
        src: ['img/**', 'js/**', 'fonts/**'],
        dest: 'frontend/assets/academic'
      },
      getting_started: {
        expand: true,
        cwd: 'frontend/static/',
        src: ['getting_started/**/**'],
        dest: 'frontend/assets'
      },
      splash: {
        expand: true,
        cwd: 'frontend/static/',
        src: ['splash/**'],
        dest: 'frontend/assets'
      }
    },
    sass: {
      options: {
          implementation: sass,
      },
      dist: {
        files: [
          {
            expand: true,
            cwd: 'frontend/static/css',
            src: '**/*.scss',
            dest: 'frontend/assets/css',
            ext: '.css'
          }, {
            'frontend/assets/academic/css/stylesAcademic.css': 'frontend/static/academic/css/stylesAcademic.scss'
          }, {
            expand: true,
            cwd: 'frontend/static/css/apps',
            src: '**/*.scss',
            dest: 'frontend/assets/css/apps',
            ext: '.css'
          }
        ],
        options: {
          outputStyle: 'compressed'
        }
      },
      dev: {
        files: [
          {
            expand: true,
            cwd: 'frontend/static/css',
            src: '**/*.scss',
            dest: 'frontend/assets/css',
            ext: '.css'
          }, {
            'frontend/assets/academic/css/stylesAcademic.css': 'frontend/static/academic/css/stylesAcademic.scss'
          }, {
            expand: true,
            cwd: 'frontend/static/css/apps',
            src: '**/*.scss',
            dest: 'frontend/assets/css/apps',
            ext: '.css'
          }
        ],
        options: {
          sourceMap: true,
          lineNumbers: true,
          outputStyle: 'expanded'
        }
      }
    },
    watch: {
      sass: {
        files: ['frontend/static/css/**/*.{scss,sass}', 'frontend/static/academic/css/**/*.{scss,sass}'],
        tasks: 'sass:dev',
        options: {
          events: ['changed']
        }
      },
      copy: {
        files: ['frontend/static/css/**/*.css', 'frontend/static/img/**/*.{png,gif,jpg,svg}', 'frontend/static/js/**/*.js', 'frontend/static/samples/*.csv', 'frontend/static/csvFormat/*.csv', 'frontend/static/contract/*.html', 'frontend/static/help/*.html', 'frontend/static/fonts/**/*.{eot,svg,ttf,woff}', 'frontend/static/academic/css/**/*.css', 'frontend/static/academic/img/**/*.{png,gif,jpg,svg}', 'frontend/static/academic/js/**/*.js', 'frontend/static/academic/fonts/**/*.{eot,svg,ttf,woff}'],
        tasks: ['copy']
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-sass');
  grunt.registerTask('default', ['sass:dist', 'copy']);
  grunt.registerTask('dev', ['sass:dev', 'copy', 'watch']);
};
